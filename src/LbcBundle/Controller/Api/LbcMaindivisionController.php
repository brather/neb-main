<?php
/**
 * User: agolodkov
 * Date: 09.08.2016
 * Time: 18:20
 */

namespace LbcBundle\Controller\Api;

use LbcBundle\Form\LbcMaindivisionType;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * Class LbcMaindivisionController
 *
 * @Route("/api/bbk")
 * @DI\Service("app.api.maindivision")
 */
class LbcMaindivisionController extends AbstractCategoryController
{
    /**
     * @return LbcMaindivisionType
     */
    public function getFormType()
    {
        return new LbcMaindivisionType();
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcMaindivision';
    }
}