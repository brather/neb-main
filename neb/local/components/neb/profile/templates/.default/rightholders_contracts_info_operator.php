<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="">
    <?
    $APPLICATION->IncludeComponent(
        "neb:rightholder.contracts",
        "",
        array(
            "SEF_MODE" => "Y",
            "SEF_FOLDER" => $arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['rightholders_contracts'],
            "SEF_URL_TEMPLATES" => array(
                "list" => "",
                "element" => "#ID#/",
            )
        ),
        $component
    );
    ?>
</div>