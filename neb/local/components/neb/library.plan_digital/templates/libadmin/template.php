<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

CJSCore::Init(array('date'));

$context = Application::getInstance()->getContext();
$arRequest = $context->getRequest()->toArray();
$arServer = $context->getServer()->toArray();

$preffixPlan = 'plan-' . $arParams['DATA_PLAN']["ID"] . '-';
$codeStatusCurrentPlan = '';
$arStatusCurrentPlan = array();
foreach($arParams['DATA_PLAN']['LIST_STATUS_PLAN'] as $codeStatus => $arStatus) {
    if($arStatus['ID'] == $arParams['DATA_PLAN']['STATUS']) {
        $codeStatusCurrentPlan = $codeStatus;
        $arStatusCurrentPlan = $arStatus;
        break;
    }
}
if($arParams['DATA_PLAN']["STATUS"] >= $arParams['DATA_PLAN']["LIST_STATUS_PLAN"]["AGREED"]["ID"]){
    $agreed = true;
} else {
    $agreed = false;
}?>

<!-- Загрузка изданий в план. Начало -->
<?if(!$arParams['DATA_PLAN']["STATUS"]) {
    $clsLoadBook = '';
} else {
    $clsLoadBook = ' style="display: none;"';
}

$sPlanLink = str_replace('#ELEMENT_ID#', $arParams['DATA_PLAN']['ID'], $arParams['DETAIL_PAGE_URL']);

?>
<div class="container-fluid"<?= $clsLoadBook ?> id="load-book-in-<?= $arParams['DATA_PLAN']['ID'] ?>">
    <div class="row">
        <h3><?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_TITLE") ?></h3>
        <p><?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_DESCR") ?></p>
    </div>
    <div class="row">
        <div class="col-md-3">
            <h4><?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_EXALEAD") ?></h4>
            <div class="row">
                <button class="digit-plan-load__add-book btn btn-default js-modal-search-books"
                        data-text="<?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_BOOK") ?>"
                        data-plan-name="<?= $arParams["DATA_PLAN"]["NAME"] ?>"
                        data-url-books="<?= $arParams['ADD_PAGE_URL'] ?>"
                        data-plan-id="<?= $arParams['DATA_PLAN']["ID"] ?>"
                        data-plan-f="<?= $arParams['DATA_PLAN']["DATA_F_PLAN"] ?>">
                    <?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_BOOKS") ?>
                </button>
            </div>
        </div>
        <div class="col-md-4">
            <h4><?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_EXCEL") ?></h4>
            <div class="row">

                <form id="<?= $preffixPlan ?>add_excel"
                      class="digit-plan-load__add-excel-form"
                      name="<?= $preffixPlan ?>add_excel"
                      action="<?=$sPlanLink?>"
                      data-preffix="<?= $preffixPlan ?>"
                      method="post"
                      enctype="multipart/form-data">
                    <div class="blk-lang hidden"
                         data-resp-count="<?= Loc::getMessage("LIBRARY_PLAN_LOAD_RECORDS") ?>"
                         data-resp-added="<?= Loc::getMessage("LIBRARY_PLAN_LOAD_SUCCESS") ?>"
                         data-resp-error-try="<?= Loc::getMessage('LIBRARY_PLAN_LOAD_ERROR_TRY') ?>"
                         data-resp-error="<?= Loc::getMessage("LIBRARY_PLAN_ERRORS") ?>"
                         data-resp-already="<?= Loc::getMessage("LIBRARY_PLAN_ALREADY_INCLUDED") ?>"
                         data-resp-notfound="<?= Loc::getMessage("LIBRARY_PLAN_NO_PUBLICATION_NEL") ?>"></div>
                    <input type="hidden" name="plan" value="<?= $arParams["DATA_PLAN"]["NAME"] ?>">
                    <input type="hidden" name="plan-id" value="<?= $arParams["DATA_PLAN"]["ID"] ?>">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-default btn-file">
                            <span><?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_CHOISE_FILE") ?></span>
                            <input data-upload-digitizing-plan="" type="file" name="excel"
                                   data-url="<?=$sPlanLink?>">
                        </span>
                        <span class="fileinput-filename"></span>
                        <span class="fileinput-new">
                            <br>
                            <small><?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_FILE_EMPTY") ?></small>
                        </span>
                    </div>
                    <input data-upload-digitizing-plan-date="" type="hidden" name="plan-date" value="29.10.2016">
                </form>

                <img class="digit-plan-load__progress" src="/local/templates/.default/markup/i/horizontal-loader.gif" alt="">
                <div class="digit-plan-load__message"></div>
                <form id="<?= $preffixPlan ?>get_excel"
                      class="digit-plan-load__get-excel-form"
                      name="<?= $preffixPlan ?>get_excel"
                      action="<?= $sPlanLink ?>"
                      method="post"
                      enctype="multipart/form-data">
                    <input type="hidden" name="plan" value="<?= $arParams["DATA_PLAN"]["NAME"] ?>">
                    <input type="hidden" name="plan-id" value="<?= $arParams["DATA_PLAN"]["ID"] ?>">
                    <input type="hidden"
                           class="digit-plan-load__get-excel-input"
                           name="digitizing-plan"
                           value="">
                    <button type="submit"
                            class="btn btn-default digit-plan-load__get-excel-btn"
                            style="position: relative; z-index: 100; display: none;">
                        <?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_PROTOCOL_EXPORT") ?>
                    </button>
                </form>
            </div>
        </div>
        <div class="col-md-5">
            <h4><?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_FROM_USER") ?></h4>
            <div class="row">
                <button class="btn btn-default js-modal-search-books"
                        data-text="<?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_BOOK") ?>"
                        data-plan-name="<?= $arParams["DATA_PLAN"]["NAME"] ?>"
                        data-url-books="<?= $arParams["ADD_PAGE_URL_FROM_READER"] ?>"
                        data-plan-id="<?= $arParams['DATA_PLAN']["ID"] ?>"
                        data-plan-f="<?= $arParams['DATA_PLAN']["DATA_F_PLAN"] ?>">
                    <?= Loc::getMessage("T_LIB_PLAN_ADD_EDITION_MODE_BOOKS") ?>
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <!--script type="text/javascript" src="/local/templates/adaptive/vendor/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="/local/templates/adaptive/vendor/jquery.fileupload.js"></script>
        <script type="text/javascript" src="/local/templates/adaptive/js/widget-dialog.js"></script-->
    </div>
    <div class="row"><hr></div>
</div>

<? if ($arRequest['digitization_plan_update'] == $arParams['DATA_PLAN']["ID"])
    $APPLICATION->RestartBuffer();
?>

<!-- Загрузка изданий в план. Конец -->
<div class="container-fluid" id="digitization_plan_list-<?= $arParams['DATA_PLAN']["ID"] ?>">
    <div class="row">
        <h3><?= Loc::getMessage("T_LIB_FILTER_TITLE") ?></h3>
        <p><?= Loc::getMessage("T_LIB_FILTER_COUNT_BOOKS") ?><?= count($arResult["BOOKS"]) ?></p>
        <form action="<?= $sPlanLink ?>">
            <input type="hidden" name="plan" value="<?= $arParams["DATA_PLAN"]["NAME"] ?>">
            <input type="hidden" name="plan-id" value="<?= $arParams["DATA_PLAN"]["ID"] ?>">
            <div class="col-xs-6">
                <h4><?= Loc::getMessage("T_LIB_FILTER_PERIOD_STATISTICS") ?></h4>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="<?= $preffixPlan ?>" class="col-xs-2"><?= Loc::getMessage("T_LIB_FROM") ?></label>
                                <div class="col-xs-10">
                                    <div class="input-group">
                                        <input type="text"
                                               id="<?= $preffixPlan ?>date-from"
                                               name="<?= $preffixPlan ?>from"
                                               size="10"
                                               value="<?= $arResult['dateFrom'] ?>"
                                               class="form-control"
                                               data-masked="99.99.9999"	/>
                                        <span class="input-group-btn">
                                            <a class="btn btn-default"
                                               id="<?= $preffixInput ?>calendarlabel"
                                               onclick="BX.calendar({node: '<?= $preffixPlan ?>date-from', field: '<?= $preffixPlan ?>date-from',  form: '', bTime: false, value: ''});">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <div class="row">
                                <label for="<?= $preffixPlan ?>" class="col-xs-2"><?= Loc::getMessage("T_LIB_TO") ?></label>
                                <div class="col-xs-10">
                                    <div class="input-group">
                                        <input type="text"
                                               name="<?= $preffixPlan ?>to"
                                               id="<?= $preffixPlan ?>date-to"
                                               size="10"
                                               value="<?= date('d.m.Y', $arResult['dateTo']) ?>"
                                               class="form-control"
                                               data-masked="99.99.9999"	/>
                                        <span class="input-group-btn">
                                            <a class="btn btn-default"
                                               id="<?= $preffixPlan ?>calendarlabel"
                                               onclick="BX.calendar({node: '<?= $preffixPlan ?>date-to', field: '<?= $preffixPlan ?>date-to',  form: '', bTime: false, value: ''});">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <h4><?= Loc::getMessage("T_LIB_FILTER_USER_ADD") ?></h4>
                <div class="btn-group" data-toggle="buttons">
                    <?
                    $vUser = '';
                    $clsFirstActive = ' active';
                    $checkFirst = 'checked';
                    if(!empty($arRequest[$preffixPlan . "NEB_USER"])) {
                        $vUser = $arRequest[$preffixPlan . "NEB_USER"];
                        if($vUser != 'ALL') {
                            $clsFirstActive = '';
                            $checkFirst = '';
                        }
                    }?>
                    <label class="btn btn-default<?= $clsFirstActive ?>">
                        <input type="radio"
                               name="<?= $preffixPlan ?>NEB_USER"
                               id="<?= $preffixPlan ?>NEB_USER_ALL"
                               value="ALL" checked="<?= $checkFirst ?>">
                        <?= Loc::getMessage("T_LIB_FILTER_USER_ADD_ALL") ?>
                    </label>
                    <label class="btn btn-default<?if($vUser == "READER"){?> active<?}?>">
                        <input type="radio"
                               name="<?= $preffixPlan ?>NEB_USER"
                               id="<?= $preffixPlan ?>NEB_USER_READER"
                               value="READER" checked="<?if($vUser == "READER"){?>checked<?}?>">
                        <?= Loc::getMessage("T_LIB_FILTER_USER_ADD_USER") ?>
                    </label>
                    <label class="btn btn-default<?if($vUser == "ADMIN"){?> active<?}?>">
                        <input type="radio"
                               name="<?= $preffixPlan ?>NEB_USER"
                               id="<?= $preffixPlan ?>NEB_USER_ADMIN"
                               value="ADMIN" checked="<?if($vUser == "ADMIN"){?>checked<?}?>">
                        <?= Loc::getMessage("T_LIB_FILTER_USER_ADD_ADMIN") ?>
                    </label>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary">
                        <?= Loc::getMessage("T_LIB_FILTER_BTN_SUBMIT") ?>
                    </button>
                    <a href="<?= $sPlanLink ?>" class="btn btn-default">
                        <?= Loc::getMessage("T_LIB_FILTER_BTN_RESET") ?>
                    </a>
                </div>                
            </div>
        </form>
    </div>
    <div class="row"><hr></div>
    <div class="row">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th><?= Loc::getMessage("T_LIB_TABLE_EDITION_TITLE") ?></th>
                <th><?= Loc::getMessage("T_LIB_TABLE_EDITION_AUTHOR") ?></th>
                <th><?= Loc::getMessage("T_LIB_TABLE_EDITION_YEAR") ?></th>
                <th><?= Loc::getMessage("T_LIB_TABLE_EDITION_DATE_CREATE") ?></th>
                <th><?= Loc::getMessage("T_LIB_TABLE_EDITION_DATE_F_PLAN") ?></th>
                <th>
                    <?if(!$arParams['DATA_PLAN']["STATUS"]) {?>
                        <?= Loc::getMessage("T_LIB_TABLE_EDITION_DELETE") ?>                        
                    <?} elseif($arStatusCurrentPlan['XML_ID'] == 'AGREED') {?>
                        Загрузка файлов
                    <?} else {?>
                        <?= Loc::getMessage('T_LIB_PLAN_ADD_EDITION_MODE_CHOISE_FILE_PDF') ?>
                    <?}?>
                </th>
            </tr>
            </thead>
            <tbody class="books-in-plan" id="tab-plan-<?= $arParams['DATA_PLAN']["ID"] ?>">
            <?foreach ($arResult["BOOKS"] as $iBook => $arBook){?>
                <?$dIdPlan = $arParams['DATA_PLAN']["ID"];
                if($agreed){
                    $subDirectory = substr($arBook['BOOK_ID'], 0, strpos($arBook['BOOK_ID'], '_'));
                    $dIdPlan = $subDirectory;
                }?>
                <tr class="book-in-plan"
                    data-id-plan="<?= $dIdPlan ?>"
                    data-id-book="<?= checkStringForSetAttr($arBook['BOOK_ID']) ?>">
                    <td>
                        <?= $arBook["UF_BOOK_NAME"] ?>
                        <span class="hidden"><i class="fa fa-refresh fa-spin"></i></span>
                        <br>
                        <a href="#book-<?= checkStringForSetAttr($iBook) ?>"
                           class="js-show-info"
                           data-parent="tab-plan-<?= $arParams['DATA_PLAN']["ID"] ?>">
                            <?= Loc::getMessage("T_LIB_TABLE_EDITION_DESCRIPTION_TITLE") ?>
                            <i class="fa fa-caret-down"></i>
                        </a>
                    </td>
                    <td>
                        <?if(!empty($arBook['UF_BOOK_AUTHOR'])) {?>
                            <?= $arBook['UF_BOOK_AUTHOR'] ?>
                        <?} else {?>
                            <?= $arResult['ITEMS'][$arBook['BOOK_ID']]['authorbook'] ?>
                        <?}?>
                    </td>
                    <td>
                        <?if(!empty($arBook["UF_BOOK_YEAR_PUBLIC"])){?>
                            <?= $arBook["UF_BOOK_YEAR_PUBLIC"] ?><?= Loc::getMessage("T_LIB_POSTFIX_YEAR") ?>
                        <?} else {?>
                            <?= $arResult['ITEMS'][$arBook['BOOK_ID']]['year'] ?><?= Loc::getMessage("T_LIB_POSTFIX_YEAR") ?>
                        <?}?>
                    </td>
                    <td><?= $arBook["DATE_ADD"] ?></td>
                    <td class="<?= $preffixPlan ?>date-finish"><?= $arBook["DATE_FINISH"] ?></td>
                    <td class="text-center">
                        <?/* Если статус плана не установлен, то добавляем в план издания и добавляем возможность их удалить*/
                        if(!$arParams['DATA_PLAN']["STATUS"]
                            || $arParams['DATA_PLAN']["STATUS"] == $arParams['DATA_PLAN']['LIST_STATUS_PLAN']['IN_AGREEING_NEL_OPERATOR']['ID']) {?>
                            <?$styleUrlOnDelete = '';
                            if($arParams['DATA_PLAN']["STATUS"] == $arParams['DATA_PLAN']['LIST_STATUS_PLAN']['IN_AGREEING_NEL_OPERATOR']['ID']) {
                                $styleUrlOnDelete = ' style="display: none;"';
                            }?>
                            <a href="/local/tools/plan_digitization/remove.php"
                               data-plan-date="<?= $arParams["DATA_PLAN"]["DATA_F_PLAN"] ?>"
                               data-plan-id="<?= $arParams['DATA_PLAN']["ID"] ?>"
                               data-ssid="<?= bitrix_sessid() ?>"
                               data-text-title="<?= Loc::getMessage("T_LIB_CONFIRM_DELETE_EDITION_TITLE") ?>"
                               data-text-confirm-title="<?= Loc::getMessage("T_LIB_CONFIRM_DELETE_EDITION_CONFIRM_TITLE") ?>"
                               data-text-confirm-text="<?= Loc::getMessage("T_LIB_CONFIRM_DELETE_EDITION_CONFIRM_TEXT") ?>"
                               data-status="<?= $arBook['UF_REQUEST_STATUS'] ?>"
                               data-book="<?= $iBook ?>"
                               data-parent-id="<?= $arBook["UF_EXALEAD_PARENT"] ?>"
                               class="js-delete-row"<?= $styleUrlOnDelete ?>>
                                <i class="fa fa-remove fa-2x"></i>
                            </a>
                        <?}

                        /* Если статус плана "Согласовано"*/
                        elseif($arStatusCurrentPlan['XML_ID'] == 'AGREED') {?>
                            <div class="col-xs-12">
                                <div class="row">
                                    <?$directoryForFile = COption::GetOptionString("nota.exalead", "viewer_pdf_directory");
                                    $APPLICATION->IncludeComponent(
                                        'neb:file.upload',
                                        '',
                                        array(
                                            'AJAX_ADD_FILES' => '/local/tools/plan_digitization/add.file.ajax.php',     /* Путь к файлу обработчику */
                                            'AJAX_DEL_FILES' => '/local/tools/plan_digitization/delete.file.ajax.php',  /* Путь к файлу обработчику на удаление */
                                            'SEF_FOLDER' => $sPlanLink,                                                 /* Корневая папка компонента (необязательно) */
                                            'FOLDER_FOR_FILES' => $directoryForFile,                                    /* Корневая папка для сохранения файлов */
                                            'SUB_FOLDER' => $dIdPlan,                                                   /* Папка раздела для файлов (не указана, файлы будут сохраняться в корневую папку) */
                                            'FILE_NAME' => $arBook['BOOK_ID'],                                          /* Имя файла (присваивается при сохранении) */
                                            'FILE_ALIAS' => $arBook['UF_BOOK_NAME'],                                    /* Алиас файла (необязательный) */
                                            'URL_DOWNLOAD' => false,                                                    /* Возможность скачать файл (по умолчанию true) */
                                            'TYPE_FILE' => 'pdf',                                                       /* Строка со списком расширений файлов (указывать через запятую) */
                                            'MAX_FILE_SIZE' => 1000000000,                                              /* Максимальный размер файла (в байтах) */
                                            'COUNT_FILES_IN_DIRECTORY' => 1,                                            /* Количество файлов в директории (необязательный, если не определено, то бесконечное) */
                                            'EMPTY_MESS' => Loc::getMessage('T_LIB_EMPTY_PDF_FILE'),              /* Сообщение при отсутствующих файлах (необязательный параметр) */
                                            'ADD_PARAMS_FOR_REQUEST' => array(
                                                'original-name' => $arBook['BOOK_ID']
                                            )
                                        )
                                    )?>
                                </div>
                            </div>
                        <?}
                        /* Если у текущего статуса индекс сортировки выше чем у статуса "Согласовано", т.е. текущий статус м.б. "Отменено", "Оцифровано", "Выполнено"*/
                        elseif($arStatusCurrentPlan['SORT'] > $arParams['DATA_PLAN']['LIST_STATUS_PLAN']['AGREED']['SORT']) {?>
                            <p><?= $arStatusCurrentPlan['VALUE'] ?></p>
                        <?}
                        ?>
                    </td>
                </tr>
                <tr class="row-switch" id="book-<?= checkStringForSetAttr($iBook) ?>" style="display: none;">
                    <td colspan="6">
                        <div class="col-xs-8">
                            <div class="row">
                                <div class="b-infobox rel b-infoboxdescr"  data-link="descr">
                                    <a href="#" class="close"></a>
                                    <?if(!empty($arResult['ITEMS'][$arBook['BOOK_ID']]['authorbook'])){?>
                                        <div class="b-infoboxitem">
                                            <span class="tit iblock"><?= Loc::getMessage('T_LIB_TABLE_EDITION_AUTHOR_D') ?></span>
                                            <span class="iblock val"><?= $arResult['ITEMS'][$arBook['BOOK_ID']]['authorbook'] ?></span>
                                        </div>
                                    <?}?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= Loc::getMessage('T_LIB_TABLE_EDITION_TITLE_D') ?></span>
                                        <span class="iblock val"><?= $arResult['ITEMS'][$arBook['BOOK_ID']]['title'] ?></span>
                                    </div>
                                    <?if(!empty($arResult['ITEMS'][$arBook['BOOK_ID']]['year'])) {?>
                                        <div class="b-infoboxitem">
                                            <span class="tit iblock"><?= Loc::getMessage('T_LIB_TABLE_EDITION_YEAR_D') ?></span>
                                            <span class="iblock val"><?= $arResult['ITEMS'][$arBook['BOOK_ID']]['year'] ?><?= Loc::getMessage("T_LIB_POSTFIX_YEAR") ?></span>
                                        </div>
                                    <?}?>
                                    <?if(!empty($arResult['ITEMS'][$arBook['BOOK_ID']]['countpages'])){?>
                                        <div class="b-infoboxitem">
                                            <span class="tit iblock"><?= Loc::getMessage('T_LIB_TABLE_EDITION_DESC_D') ?></span>
                                            <span class="iblock val"><?= $arResult['ITEMS'][$arBook['BOOK_ID']]['countpages'] ?><?= Loc::getMessage("T_LIB_FROM") ?></span>
                                        </div>
                                    <?}?>
                                    <?if(!empty($arResult['ITEMS'][$arBook['BOOK_ID']]['library'])){?>
                                        <div class="b-infoboxitem">
                                            <span class="tit iblock"><?= Loc::getMessage('T_LIB_TABLE_EDITION_LIBRARY_D') ?></span>
                                            <span class="iblock val"><?= $arResult['ITEMS'][$arBook['BOOK_ID']]['library'] ?></span>
                                        </div>
                                    <?}?>
                                    <?if(!empty($arBook['COMMENT'])) {?>
                                        <div class="b-infoboxitem">
                                            <span class="tit iblock"><?= Loc::getMessage('T_LIB_TABLE_EDITION_COMMENT_D') ?></span>
                                            <span class="iblock val"><?= $arBook['COMMENT'] ?></span>
                                        </div>
                                    <?}?>
                                </div>
                                <!-- /b-infobox -->
                                <?if(!empty($arResult['BOOKS_OTHER'][$arBook['BOOK_ID']])){?>
                                    <div class="b-infobox rel" data-link="digital">
                                        <a href="#" class="close"></a>
                                        <?foreach($arResult['BOOKS_OTHER'][$arBook['BOOK_ID']] as $arOther) {?>
                                            <div class="b-infoboxitem b-infoboxmain">
                                                <span class="tit iblock"><?= Loc::getMessage('T_LIB_TABLE_EDITION_WHO_DIGITIZATION_D');?></span>
										<span class="iblock val">
											<strong><?= $arOther['NAME'] ?></strong>
											<span class="date"><?= Loc::getMessage("T_LIB_TO") ?><?= $arOther['DATE'] ?></span>
										</span>
                                            </div>
                                        <?}?>
                                    </div>
                                <?}?>
                                <!-- /b-infobox -->
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="row">
                                <?if(!empty($arParams["DATA_PLAN"]["STATUS"]) && $arParams["DATA_PLAN"]["STATUS"] >= $arParams["DATA_PLAN"]["LIST_STATUS_PLAN"]["AGREED"]) {?>
                                    <span><?= Loc::getMessage("T_LIB_TABLE_EDITION_STATUS_D") ?></span><br>
                                    <?
                                    /* Если текущий статус == На согласовании оператором НЭБ то показываем лишь только значение статуса */
                                    if($arResult["PROP_LIST_STATUS"][$arBook["UF_REQUEST_STATUS"]]["VALUE"] == DIGITIZATION_STATUS_ON_AGREEMENT){?>
                                        <?= DIGITIZATION_STATUS_ON_AGREEMENT ?>
                                    <?}
                                    /* Если текущий статус == Выполнено то показываем лишь только значение статуса */
                                    if($arResult["PROP_LIST_STATUS"][$arBook["UF_REQUEST_STATUS"]]["VALUE"] == DIGITIZATION_STATUS_DONE) {?>
                                        <?= DIGITIZATION_STATUS_DONE ?>
                                    <?}
                                    /* Если текущий статус != "На согласовании..." и "Выполнено" то позволяем пользователю изменять настройки статуса и добавлять файл */
                                    else {?>
                                        <select name="<?= $preffixPlan ?>DATA_UF_REQUEST_STATUS"
                                                id="<?= $preffixPlan ?>DATA_UF_REQUEST_STATUS"
                                                class="form-control">
                                            <?foreach($arResult["PROP_LIST_STATUS"] as $idStatus => $arStatus) {?>
                                                <?if($arStatus['VALUE'] != DIGITIZATION_STATUS_ON_AGREEMENT && $arStatus['VALUE'] != DIGITIZATION_STATUS_DONE) {?>
                                                    <?$selected = '';
                                                    if($idStatus == $arBook["UF_REQUEST_STATUS"]){
                                                        $selected = ' selected';
                                                    }?>
                                                    <option value="<?= $idStatus ?>"<?= $selected ?>><?= $arStatus["VALUE"] ?></option>
                                                <?}?>
                                            <?}?>
                                        </select>
                                        <!-- Кнопка загрузки pdf файлов -->
                                    <?}
                                    ?>
                                <?}?>
                            </div>
                        </div>
                    </td>
                </tr>
                <?if($agreed){?>
                    <tr>
                        <td colspan="6">
                            <div class="col-xs-12">
                                <div class="row" id="container-plan-<?= $dIdPlan ?>-book-<?= checkStringForSetAttr($arBook['BOOK_ID']) ?>">

                                </div>
                            </div>
                        </td>
                    </tr>
                <?}?>
            <?}?>
            </tbody>
        </table>
        <?= $arResult["STR_NAV"] ?>
    </div>
</div>

<? if ($arRequest['digitization_plan_update'] == $arParams['DATA_PLAN']["ID"])
    exit();
?>