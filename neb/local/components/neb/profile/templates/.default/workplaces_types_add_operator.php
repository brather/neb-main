<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Добавление типа ЭЧЗ');

/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
$APPLICATION->IncludeComponent(
    'neb:workplaces.types',
    '',
    [
        "ID" => '0',
        "LIST_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['workplaces_types']
    ],
    $component
);