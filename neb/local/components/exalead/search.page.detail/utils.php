<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * Class BookUtils - утилиты для карточки книги и создания микроразметки http://schema.org/Book
 */
class BookUtils
{
    /**
     * Выбирает максимальное число из строки
     *
     * @param $sPage
     * @return mixed
     */
    public static function getCountPages($sPage) {
        $arPages = [];
        $sWord = '';
        for ($i = 0; $i < mb_strlen($sPage); $i++) {
            $char = self::char($sPage, $i);
            if (is_numeric($char)) {
                $sWord .= $char;
            } elseif (!empty($sWord)) {
                $arPages[] = intval($sWord);
                $sWord = '';
            }
        }
        if (!empty($sWord)) {
            $arPages[] = intval($sWord);
        }

        return max($arPages);
    }

    /**
     * Получает ISBN-строку, ограниченную 13-ю цифрами
     *
     * @param $sIbsn
     * @return string
     */
    public static function getIsbn($sIbsn) {
        $sResult = '';
        $iCount = 0;
        $sIbsn = preg_replace('/[^0-9xXхХ\-]/ui', '', $sIbsn);
        for ($i = 0; $i < mb_strlen($sIbsn); $i++) {
            $sResult .= $char = self::char($sIbsn, $i);
            if (is_numeric($char))
                $iCount++;
            if ($iCount == 13)
                break;
        }
        return $sResult;
    }

    /**
     * Получает год из строки (первые 4 численных символа)
     *
     * @param $sYear
     * @return int
     */
    public static function getYear($sYear) {
        $sResult = '';
        $iCount = 0;
        for ($i = 0; $i < mb_strlen($sYear); $i++) {
            $char = self::char($sYear, $i);
            if (is_numeric($char)) {
                $sResult .= $char;
                $iCount++;
            }
            if ($iCount == 4) {
                break;
            }
        }
        return intval($sResult);
    }

    /**
     * Получение массива фамилий авторов издания
     *
     * @param $arAuthors
     * @return array
     */
    public static function getAuthorsSurname($arAuthors) {

        $arResult = [];

        if (empty($arAuthors)) {
            return $arResult;
        }
        
        foreach ($arAuthors as $sAuthor) {

            $sAuthor = trim($sAuthor);

            // если автор приведен без сокращений - берем вкачестве фамилии первое слово
            if (false === mb_strpos($sAuthor, '.')) {
                $arFullName = explode(' ', $sAuthor);
                reset($arFullName);
                $arResult[$sAuthor] = current($arFullName);
            }
            // если сокращен - фамилией является слово без точки
            else {
                $arFullName = explode(' ', $sAuthor);
                foreach ($arFullName as $sName) {
                    if (!empty($sName) && false === mb_strpos($sName, '.')) {
                        $arResult[$sAuthor] = $sName;
                    }
                }
            }
        }

        return $arResult;
    }

    /**
     * Получение символа в строке
     * @param $str
     * @param $i
     * @return string
     */
    private static function char($str, $i) {
        return mb_substr($str, $i, 1);
    }
}