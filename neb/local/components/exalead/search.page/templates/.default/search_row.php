<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$numBook = $arResult['START_ITEM'] + $index + 1; ?>

<li class="search-result__content-list-kind clearfix">
    <div class="search-result__content-list-sidebar">
        <span class="search-result__content-list-number"><?= $numBook ?><?= '.' ?></span>
        <?// иконка
        if ($arItem['fileExt'] && in_array('pdf',$arItem['fileExt']) && ($arItem['IS_READ'] == 'Y') ) {?>
            <?if($arItem['isprotected'] == 2) {?>
                <span class="search-result__content-list-status-icon search-result__content-list-status-icon--close"
                      title="<?=Loc::getMessage('COPYRIGHT_PUBLICATINON') ?>">
                </span>
            <?} elseif($arItem['isprotected'] == 1 && !$arParams['IS_SHOW_PROTECTED']) {?>
                <span class="search-result__content-list-status-icon search-result__content-list-status-icon--close"
                      title="<?=Loc::getMessage('PROTECTED_PUBLICATINON') ?>">
                </span>
            <?} else {?>
                <span class="search-result__content-list-status-icon search-result__content-list-status-icon--open"
                      title="<?=Loc::getMessage('PUBLIC_PUBLICATION')?>">
                </span>
            <?}?>
        <?} elseif('10003' == $arItem['idlibrary']) {?>
            <span class="search-result__content-list-status-icon search-result__content-list-status-icon--el"
                  title="<?=Loc::getMessage('EKBSON_CATALOG_RECORD')?>">
            </span>
        <?} elseif ( isset($arItem['fileExt']) && $arItem['IS_MEDIA'] === "Y") {?>
            <? if ($arItem['IS_MEDIA_VIDEO'] === true) { ?>
                <span class="search-result__content-list-status-icon search-result__content-list-status-icon--video"
                      title="<?=Loc::getMessage('VIDEO_VIEW')?>">
                </span>
            <? } elseif ($arItem['IS_MEDIA_AUDIO'] === true) { ?>
                <span class="search-result__content-list-status-icon search-result__content-list-status-icon--audio"
                      title="<?=Loc::getMessage('AUDIO_LISTEN')?>">
                </span>
            <? } ?>
        <?} else {?>
            <span class="search-result__content-list-status-icon search-result__content-list-status-icon--read"
                  title="<?=Loc::getMessage('CATALOG_RECORD')?>">
            </span>
        <?}?>
    </div>
    <div class="search-result__content-main">
        <?if ($arItem['source'] == 'VideoData') {?>
            <a  href="#"
                class="btn btn-primary search-result__content-main-read-btn"
                onclick="popupVideoWindow('<?= $arItem['VIDEO_URL'] ?>', '<?= $arItem['title'] ?>'); return false;">
                <?= Loc::getMessage('VIDEO_VIEW'); ?>
            </a>
        <?} elseif ( $arItem['IS_MEDIA'] === "Y" && $arItem['IS_READ'] === "N" ) {?>
            <a href="#modal-media-player-<?= $arItem['NUM_ITEM']?>" class="btn btn-primary search-result__content-main-read-btn" data-toggle="modal">
                <?=($arItem['IS_MEDIA_VIDEO'] === true) ? Loc::getMessage("VIDEO_VIEW") : Loc::getMessage('AUDIO_LISTEN') ?>
            </a>
        <?} elseif ($arItem['fileExt'] && in_array('pdf', $arItem['fileExt']) && ($arItem['IS_READ'] == 'Y')) {?>
            <a class="btn btn-primary search-result__content-main-read-btn"
               onclick="readBook(event, this); return false;"
               data-link="<?= $arItem['readBookId'] ?>"
               href="javascript:void(0);"
               data-options="<?= htmlspecialchars(json_encode($arItem['viewerOptions']),ENT_QUOTES,'UTF-8') ?>"
               data-strict-search="<?= (int)$arItem["STRICT_QUERY_SEARCH"] ?>"
               data-full-search="<?= (int)$arItem["FULL_QUERY_SEARCH"] ?>">
                <?if('museum' !== $_REQUEST['librarytype']) {?>
                    <?= Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_READ') ?>
                <?} else {?>
                    <?= Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_VIEW') ?>
                <?}?>
            </a>
        <?}?>
        <div class="search-result__content-main-links">
            <?// название
            if ($arItem['fileExt'] && in_array('pdf', $arItem['fileExt']) && ($arItem['IS_READ'] == 'Y') ) {?>
                <?if($arItem['isprotected'] == 2) {?>
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                       class="search-result__content-link-title search-result__content-link-title--close">
                        <?= $arItem['title'] ?>
                    </a>
                <?} elseif($arItem['isprotected'] == 1 && !$arParams['IS_SHOW_PROTECTED']) {?>
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                       class="search-result__content-link-title search-result__content-link-title--close">
                        <?= $arItem['title'] ?>
                    </a>
                <?} else {?>

                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="search-result__content-link-title">
                        <?= $arItem['title'] ?>
                    </a>
                <?}?>

            <?} elseif('10003' == $arItem['idlibrary']) {?>
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                   class="search-result__content-link-title search-result__content-link-title--el">
                    <?= $arItem['title'] ?>
                </a>
            <?} else {?>
                <a href="<?=$arItem['DETAIL_PAGE_URL']?>"
                   class="search-result__content-link-title search-result__content-link-title--read">
                    <?= $arItem['title'] ?>
                </a>
            <?}?>
            <p class="search-result__content-link-info">
                <?if(!empty($arItem['authorbook'])) {?>
                    <?foreach ($arItem['authorbook_for_link'] as $key => $author) {
                        $author = trim($author);
                        echo $key ? ', ' : ''; ?>
                        <a href="javascript:void(0);"
                           data-tag-add-submit
                           data-tag-name="f_field[authorbook]"
                           data-tag-text="<?php echo strip_tags($author) ?>"
                           data-tag-value="f/authorbook/<?php echo mb_strtolower(strip_tags($author))?>">
                            <?php echo $author ?>
                        </a>
                    <?}?>
                <?}?>

                <?if(!empty($arItem['authorbook']) && (!empty($arItem['publisher']) || !empty($arItem['year']))) {?>
                    —
                <?}?>

                <?if(!empty($arItem['publisher'])) {?>
                    <a href="javascript:void(0);"
                         data-tag-add-submit
                         data-tag-name="f_field[publisher]"
                         data-tag-text="<?= $arItem['publisher']?>"
                         data-tag-value="f/publisher/<?= mb_strtolower(str_replace('"', '&quot;', $arItem['publisher'])) ?>">
                        <?=$arItem['publisher']?>
                    </a><?
                    if(!empty($arItem['year'])) {
                        ?>, <?
                    }?>
                <?}?>
                <?if(!empty($arItem['year'])) {?>
                    <a href="javascript:void(0);"
                       data-tag-add-submit
                       data-tag-name="f_publishyear"
                       data-tag-text="<?= $arItem['year'] ?>"
                       data-tag-value="<?= $arItem['year'] ?>">
                        <?= $arItem['year'] ?>
                    </a>
                <?}?>

                <?if(intval($arItem['countpages']) > 0) {?>
                    <span>
                        <?if(!empty($arItem['authorbook']) || !empty($arItem['publisher']) || !empty($arItem['year'])) {?>
                            —
                        <?}?>
                        <?= $arItem['countpages'] ?>
                    </span>
                <?}?>
            </p>
        </div>
        <?if(!empty($arItem['ispart'])) {?>
            <p class="search-result__content-main-include">
                <?=Loc::getMessage('INCLUDED_IN')?>: <a href="/catalog/<?= $arItem['idbook'] ?>/"><?= $arItem['namebook'] ?></a>
            </p>
        <?}?>
        <?if ('Y' !== $arParams['arParams']['HIDE_LIST_TEXT']
            && stripos($arItem['text'], '<strong>') !== false) // показ только в случае наличии разметки
        { ?>
            <p class="search-result__content-main-desc">
                <?= $arItem['text'] ?>
            </p>
        <? } ?>
        <?if($arItem['library']) {?>
            <p class="search-result__content-main-source">
                <?= Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SOURCE') ?>:
                <?if(!empty($arItem['library_url'])) {?>
                    <a href="<?= $arItem['library_url'] ?>" class="b-sorcelibrary"><?= $arItem['library'] ?></a>
                <?} else {?>
                    <?= $arItem['library'] ?>
                <?}?>
            </p>
        <?}?>
        <?if(!empty($arItem['bbkfull'])) {?>
            <a href="#" class="search-result__content-main-link-category"
               data-open="<?= Loc::getMessage('CATEGORIES') ?>" data-close="<?= Loc::getMessage('CATEGORIES_CLOSE') ?>">
                <?= Loc::getMessage('CATEGORIES') ?>
            </a>
            <div class="search-result__content-main-category">
                <?foreach($arItem['bbkfull'] as $key => $bbkfull) {?>
                    <p>
                        <?$r_bbkfull = '';?>
                        <?foreach($bbkfull as $k => $bbk) {?>
                            <?$r_bbkfull .= '/'.urlencode(trim($arItem['r_bbkfull'][$key][$k]));?>
                            <?if ($k != 0){?>
                                <?= ' > ' ?>
                            <?}?>
                            <a href="/search/?f_field[bbkfull]=f/bbkfull<?= $r_bbkfull.'&'.nfDeleteParam(array( 'bbkfull', 'dop_filter', 'PAGEN_'.$arResult['NavNum'])) ?>">
                                <?= $bbk ?>
                            </a>
                        <?}?>
                    </p>
                <?}?>
            </div><br />
        <?}?>
        <?if(!empty($arItem['keywords'])) {?>
            <a href="#" class="search-result__content-main-link-category"
               data-open="<?= Loc::getMessage('KEYWORDS') ?>" data-close="<?= Loc::getMessage('KEYWORDS_CLOSE') ?>">
                <?= Loc::getMessage('KEYWORDS') ?>
            </a>
            <div class="search-result__content-main-category">
                <?=implode(', ', $arItem['keywords'])?>
            </div>
        <?}?>
    </div>
</li>