<?
$MESS['NOTES_SORT'] = 'Sort';
$MESS['NOTES_SORT_AUTHOR'] = 'Author';
$MESS['NOTES_SORT_NAME'] = 'Name';
$MESS['NOTES_SORT_DATE'] = 'Date';
$MESS['NOTES_SHOW'] = 'Show';
$MESS['NOTES_SETTINGS'] = 'Settings';
$MESS['NOTES_REMOVE'] = 'Remove';
$MESS['NOTES_FROM_NOTES'] = 'from my notes';
$MESS['NOTES_AUTHOR'] = 'Author';
$MESS['NOTES_BOOK'] = 'Book';
$MESS['NOTES_TEXT'] = 'Note on the page';
$MESS['NOTES_MY_COLLECTIONS'] = 'my collections';
$MESS['NOTES_CREATE_COLLECTION'] = 'create a collection';
?>