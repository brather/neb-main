<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

error_reporting(E_ALL ^ E_NOTICE);

use \Neb\Main\Helper\MainHelper;

$password = $_POST['password'];
$idType = $_POST['idType'];
$value = $_POST['value'];

$url = 'http://195.68.154.68:20280/GetPrivateDataWebPage/Default.aspx?login_type=' . urlencode($idType) . '&login=' . urlencode($value) . '&password=' . urlencode($password);

$error = true;
$result = $arJson = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($password) && !empty($idType) && !empty($value)) {

    set_time_limit('60');

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FAILONERROR, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, MainHelper::checkSslVerification($url));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
    $response = curl_exec($ch);
    #$info = curl_getinfo($ch);
    curl_close($ch);

    if (!empty($response) and strpos($response, 'error>') === false) {
        $response = str_replace("utf-16", "utf-8", $response);
        $xml = simplexml_load_string($response);

        if (!empty($xml)) {
            $result = (array)$xml;

            //debugLog($result, '/debugEsia.log'); // запишем логи, для анализа адресов

            if (!empty($result['MobilePhone7Digits'])) {
                $result['MobilePhone7Digits'] = substr_replace($result['MobilePhone7Digits'], '-', 3, 0);
                $result['MobilePhone7Digits'] = substr_replace($result['MobilePhone7Digits'], '-', 6, 0);
            }

            if (!empty($result['RegistrationAddress'])) {
                $arAdress = explode(',', $result['RegistrationAddress']);
                $result['Registration']['index'] = trim($arAdress[0]);
                $result['Registration']['city'] = trim(str_replace(array('город'), '', $arAdress[1]));
                $result['Registration']['street'] = trim(str_replace(array('улица'), '', $arAdress[2]));
                $result['Registration']['house'] = trim(str_replace(array('д.'), '', $arAdress[3]));
                $result['Registration']['korp'] = trim(str_replace(array('корп.'), '', $arAdress[4]));
                $result['Registration']['kv'] = trim(str_replace(array('кв.'), '', $arAdress[5]));
            }

            if (!empty($result['LivingAddress'])) {
                $arAdress = explode(',', $result['LivingAddress']);
                $result['Living']['index'] = trim($arAdress[0]);
                $result['Living']['city'] = trim(str_replace(array('город'), '', $arAdress[1]));
                $result['Living']['street'] = trim(str_replace(array('улица'), '', $arAdress[2]));
                $result['Living']['house'] = trim(str_replace(array('д.'), '', $arAdress[3]));
                $result['Living']['korp'] = trim(str_replace(array('корп.'), '', $arAdress[4]));
                $result['Living']['kv'] = trim(str_replace(array('кв.'), '', $arAdress[5]));
            }

            if (!empty($result['BirthDate'])) {
                $timestamp = strtotime($result['BirthDate']);
                $result['arBirthDate']['d'] = intval(date('d', $timestamp));
                $result['arBirthDate']['m'] = intval(date('m', $timestamp));
                $result['arBirthDate']['Y'] = intval(date('Y', $timestamp));
            }

            $error = false;
        } else {
            $error = true;
        }
    } else {
        $error = true;
    }
}

$arJson['error'] = $error;
$arJson['result'] = $result;

MainHelper::showJson($arJson);
