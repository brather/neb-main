<?php
/**
 * User: agolodkov
 * Date: 12.08.2016
 * Time: 16:11
 */

namespace LbcBundle\Controller;


use AppBundle\Security\UserProvider;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Gedmo\Tree\Entity\Repository\ClosureTreeRepository;
use JMS\DiExtraBundle\Annotation as DI;
use LbcBundle\DependencyInjection\LbcCategory;
use LbcBundle\Entity\AbstractLbcCategory;
use LbcBundle\Entity\Traits\CategoryCodeTrait;
use LbcBundle\LbcBundle;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use \LbcBundle\Controller\Api;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use BitrixBundle\Annotations\Template as BitrixTemplate;
use BitrixBundle\Annotations\IncludeBitrix;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


abstract class AbstractCategoryController
{

    /**
     * @var Api\AbstractCategoryController
     */
    public $api;

    /**
     * @var Api\AbstractCategoryController
     */
    public $ownerApi;

    /**
     * @DI\Inject("doctrine")
     * @var Registry
     */
    public $doctrine;

    /**
     * @DI\Inject("router")
     * @var Router
     */
    public $router;

    /**
     * @DI\Inject("neb.user_provider")
     * @var UserProvider
     */
    public $userProvider;

    /**
     * @DI\Inject("lbc.category")
     * @var LbcCategory
     */
    public $categoryHelper;

    /**
     * @var array
     */
    protected $_routerMaps;

    /**
     *
     * @Method("GET")
     * @BitrixTemplate("LbcBundle:category:category.html.php")
     * @IncludeBitrix()
     *
     * @return array
     */
    public function categoryIndexAction()
    {
        $result = $this->api->categoryAction(0);
        $result['category'] = [
            'name' => $this->_getIndexName(),
        ];
        $result['crumbs'] = $this->_getMainCrumbs();
        $this->_prepareEntities($result['entities']);
        $this->_prepareDescriptions($result['entities']);
        $result['userCanEdit'] = $this->userProvider->getCurrentUser()->hasRole(LbcBundle::BBK_EDITOR_ROLE);
        $result['canSort'] = $result['userCanEdit'];

        return $result;
    }

    /**
     * Lists LbcMaindivision child entities.
     *
     * @param int $id
     *
     * @Method("GET")
     * @BitrixTemplate("LbcBundle:category:category.html.php")
     * @IncludeBitrix()
     *
     * @return array
     */
    public function categoryAction($id)
    {
        /** @var ClosureTreeRepository $repository */
        $repository = $this->api->getRepository();

        $result = $this->api->categoryAction($id);
        $result['category'] = $repository->find($id);
        $result['crumbs'] = [];
        if ($result['category']) {
            $result['crumbs'] = $repository->getPath($result['category']);
        }
        $this->_prepareEntities($result['entities']);
        $this->_prepareEntities($result['crumbs']);
        $this->_prepareDescriptions($result['entities']);

        $mainCrumbs = $this->_getMainCrumbs();
        foreach ($mainCrumbs as $crumb) {
            array_unshift($result['crumbs'], $crumb);
        }
        $entityType = $this->api->getFormType();
        $form = $this->api->formFactory->create(
            $entityType
        );
        $fieldList = [];
        foreach ($form->all() as $child) {
            $fieldList[$child->getName()] = true;
        }
        $result['userCanEdit'] = $this->userProvider->getCurrentUser()->hasRole(LbcBundle::BBK_EDITOR_ROLE);
        $result['canSort'] = $result['userCanEdit'] && $id > 0;
        $result['canCreate'] = $result['userCanEdit'];
        $result['categoryFormName'] = $entityType->getName();
        $result['fieldListJson'] = json_encode($fieldList);
        $categoryName = $this->categoryHelper->getCategoryNameByEntityName($this->api->getEntityName());
        $result['url'] = [
            'createCategory' => $this->router->generate(
                'api_bbk_category_create_actions',
                [
                    'category' => $categoryName
                ]
            ),
            'categoryExport' => $this->router->generate(
                'api_bbk_category_export',
                [
                    'category' => $categoryName,
                    'id'       => $id,
                    'token'    => $this->userProvider->getCurrentUser()->getToken(),
                ]
            )
        ];


        return $result;
    }

    /**
     * @param $id
     *
     * @Method("GET")
     * @BitrixTemplate("LbcBundle:category:category.html.php")
     * @IncludeBitrix()
     *
     * @return array
     * @throws \Exception
     */
    public function categoryChildsAction($id)
    {
        if (!$this->ownerApi instanceof Api\AbstractCategoryController) {
            throw new \Exception('Not found category owner api');
        }
        /** @var ClosureTreeRepository $repository */
        $repositoryOwner = $this->ownerApi->getRepository();
        if (!$categoryOwner = $repositoryOwner->find($id)) {
            throw new NotFoundHttpException('Category owner not found');
        }

        $result = $this->api->categoryChildsAction($id);
        $result['category'] = [
            'name' => $this->_getIndexName(),
        ];
        $result['crumbs'] = $repositoryOwner->getPath($categoryOwner);

        $this->_prepareEntities($result['entities']);
        $this->_prepareEntities($result['crumbs']);
        $this->_prepareDescriptions($result['entities']);

        array_unshift($result['crumbs'], $this->_getMainPageCrumb());
        $result['crumbs'][] = [
            'name'       => $this->_getIndexName(),
            'currentUrl' => true,
        ];
        $result['userCanEdit'] = $this->userProvider->getCurrentUser()->hasRole(LbcBundle::BBK_EDITOR_ROLE);


        return $result;
    }

    /**
     * @param AbstractLbcCategory[] $entities
     *
     * @throws \Exception
     */
    protected function _prepareEntities($entities)
    {
        foreach ($entities as $entity) {
            $this->prepareEntity($entity);
        }
    }

    /**
     * @param AbstractLbcCategory[] $entities
     */
    protected function _prepareDescriptions($entities)
    {
        $codes = [];
        $linksStarts = [
            'См. в соответствующих подразделениях индекса',
            'см. в соответствующих подразделениях индекса',
            'См. методические указания к индексу',
            'см. методические указания к индексу',
            'cм. также:',
            'См. также:',
            'см.',
            'См.'
        ];
        foreach ($entities as $entity) {
            $links = $entity->getLinks();
            $links = preg_split('/' . implode('|', $linksStarts) . '/', $links);
            if (count($links) > 1) {
                foreach ($links as $link) {
                    $link = trim($link);
                    if (preg_match('/^[А-Я]/u', $link)) {
                        $link = preg_replace('/[\_\[\]\;\,]/', ' ', $link);
                        $link = preg_replace('/\.\s/', ' ', $link);
                        $code = substr($link, 0, strpos($link, ' '));
                        if ($code == '') {
                            $code = $link;
                        }
                        $code = trim($code, '.');
                        if (!empty($code)) {
                            if (!isset($codes[$code])) {
                                $codes[$code] = [];
                            }
                            $codes[$code][] = $entity;
                            $arSort[] = array(
                                'CODE'        => $code,
                                'STRLEN_CODE' => strlen($code)
                            );
                        }
                    }
                }
            }
        }
        $linkedEntities = $this->api->getRepository()->findBy(['code' => array_keys($codes)]);
        $codesLinks = [];
        /** @var CategoryCodeTrait|AbstractLbcCategory $entity */
        foreach ($linkedEntities as $entity) {
            $codesLinks[$entity->getCode()] = $this->router->generate(
                $this->getRouteName('category'),
                ['id' => $entity->getId()]
            );
        }
        //region Отсортируем массив по длине кода
        usort(
            $arSort, function ($a, $b) {
            return ($b['STRLEN_CODE'] - $a['STRLEN_CODE']);
        }
        );
        $codesTmp = array();
        foreach ($arSort as $arCode) {
            $codesTmp[$arCode['CODE']] = $codes[$arCode['CODE']];
        }
        $codes = $codesTmp;
        //endregion
        /** @var AbstractLbcCategory[] $entites */
        foreach ($codes as $code => $entities) {
            if (isset($codesLinks[$code])) {
                foreach ($entities as $entity) {
                    $links = $entity->getLinks();
                    foreach ($linksStarts as $linkStart) {
                        $links = str_replace($linkStart . ' ' . $code, $linkStart . $code, $links);
                        $links = str_replace(
                            $linkStart . $code,
                            $linkStart . ' <a target="_blank" href="' . $codesLinks[$code] . '">' . $code . '</a>',
                            $links
                        );
                    }
                    $entity->setLinks($links);
                }
            }
        }
    }

    /**
     * @todo Выпилить в хелпер
     *
     * @param AbstractLbcCategory $entity
     */
    public function prepareEntity($entity)
    {
        $userRoles = $this->userProvider->getCurrentUser()->getRoles();
        if ( !$controller = $this->categoryHelper->getEntityControllerService(
            $entity
        )
        )
        {
            $controller = $this;
        }
        $entity->options = [
            'urlDetail'       => $this->router->generate(
                $controller->getRouteName( 'api_category_detail' ),
                [ 'id' => $entity->getId() ]
            ),
            'urlUpdate'       => $this->router->generate(
                $controller->getRouteName( 'api_category_update' ),
                [ 'id' => $entity->getId() ]
            ),
            'urlImages'       => $this->router->generate(
                $controller->getRouteName( 'api_category_images' ),
                [ 'id' => $entity->getId() ]
            ),
            'urlCategory'     => $this->router->generate(
                $controller->getRouteName( 'category' ),
                [ 'id' => $entity->getId() ]
            ),
            'urlCheckActions' => $this->router->generate(
                'api_bbk_category_check_actions',
                [
                    'category' => $this->categoryHelper->getEntityCategoryName(
                        $entity
                    ),
                    'id'       => $entity->getId()
                ]
            ),
            'urlMoveUp'       => $this->router->generate(
                'api_bbk_category_change_sorting_up',
                [
                    'category' => $this->categoryHelper->getEntityCategoryName(
                        $entity
                    ),
                    'id'       => $entity->getId()
                ]
            ),
            'urlMoveDown'     => $this->router->generate(
                'api_bbk_category_change_sorting_down',
                [
                    'category' => $this->categoryHelper->getEntityCategoryName(
                        $entity
                    ),
                    'id'       => $entity->getId()
                ]
            ),
        ];
        $entity->options['formName'] = $this->api->getFormType()->getName();
        $entity->optionsJson = json_encode( $entity->options );

        $entity->actions = array_merge(
            $this->_getActionParams(), $this->_getCommonActionParams()
        );
        foreach ( $entity->actions as $key => $action )
        {
            if ( isset( $action['roles'] ) )
            {
                $allow = false;
                if ( !empty( $userRoles ) )
                {
                    $roles = array_flip( $action['roles'] );
                    foreach ( $userRoles as $role )
                    {
                        if ( isset( $roles[ $role ] ) )
                        {
                            $allow = true;
                            break;
                        }
                    }
                }
                if ( false === $allow )
                {
                    unset( $entity->actions[ $key ] );
                }
            }

            if ( $entity->gr === 1 )
            {
                if ( $action['code'] === 'delete'
                    || $action['code'] === 'approve'
                )
                {
                    unset( $entity->actions[ $key ] );
                }
            }

            if ( isset( $action['route'] )
                && isset( $entity->actions[ $key ] )
            )
            {
                $entity->actions[ $key ]['url'] = $this->router->generate(
                    $action['route'],
                    [ 'id' => $entity->getId() ]
                );
            }
        }
        if ( $entity->gr === 1 )
        {
            $entity->actions[] = array(
                'name' => 'Восстановить',
                'code' => 'recovery',
                'roles' => array(LbcBundle::BBK_EDITOR_ROLE)
            );
        }

        $entity->actions = array_values( $entity->actions );
    }

    public function checkEntityActions($entity)
    {
    }

    /**
     * @param string $name
     *
     * @return null | string
     * @throws \Exception
     */
    protected function _getRouterName($name)
    {
        if (null === $this->_routerMaps) {
            $this->_routerMaps = $this->_getRouterMaps();
            if (!is_array($this->_routerMaps)) {
                throw new \Exception('$this->_getRouterMaps() must be return array');
            }
        }
        if (isset($this->_routerMaps[$name])) {
            return $this->_routerMaps[$name];
        }

        return null;
    }

    /**
     * @param $name
     *
     * @return null|string
     * @throws \Exception
     */
    public function getRouteName($name)
    {
        return $this->_getRouterName($name);
    }

    /**
     * @return array
     */
    protected function _getMainPageCrumb()
    {
        return [
            'name'    => 'ББК',
            'options' => [
                'urlCategory' => $this->router->generate(
                    'bbk_category_index'
                )
            ],
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function _getMainCrumbs()
    {
        $crumbs = [
            $this->_getMainPageCrumb()
        ];
        if ($indexRoute = $this->_getRouterName('category_index')) {
            $crumbs[] = [
                'name'    => $this->_getIndexName(),
                'options' => [
                    'urlCategory' => $this->router->generate(
                        $indexRoute
                    )
                ],
            ];
        }

        return $crumbs;
    }

    /**
     * @return array
     */
    protected function _getActionParams()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function _getCommonActionParams()
    {
        return [
            [
                'name'  => 'Пометить как удалённое',
                'code'  => 'delete',
                'roles' => [
                    LbcBundle::BBK_EDITOR_ROLE,
                    LbcBundle::BBK_OPERATOR_BBK_ROLE,
                ]
            ],
            /*[
                'name'  => 'Утвердить',
                'code'  => 'approve',
                'roles' => [
                    LbcBundle::BBK_EDITOR_ROLE,
                    LbcBundle::BBK_OPERATOR_BBK_ROLE,
                ]
            ],*/
            [
                'name' => 'Карточка раздела',
                'code' => 'division-card',
            ],
            [
                'name'  => 'Просмотр истории',
                'code'  => 'history',
                'roles' => [
                    LbcBundle::BBK_EDITOR_ROLE,
                    LbcBundle::BBK_OPERATOR_BBK_ROLE,
                ]
            ],
        ];
    }

    abstract protected function _getRouterMaps();

    abstract protected function _getIndexName();

}