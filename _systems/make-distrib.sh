#!/bin/bash
date=$(date +"%d-%b-%Y")

#Bitrix backup
#zip -r ../backups/distrib/neb-$date.zip ../* -x "*.log" "*/log.txt" "*/.git/*" "../backups/*" "../app/cache/dev/*" "../app/logs/*" "../neb/dev/*" "../neb/log.txt" "../neb/upload/*" "../neb/bitrix/backup/*" "../neb/bitrix/cache/*" "../neb/bitrix/stack_cache/*" "../neb/bitrix/managed_cache/*" "../neb/bitrix/upload/*"
tar -cvzf /home/bitrix/www/backups/distrib/neb-$date.tar.gz --exclude="*.log" --exclude="*/log.txt" --exclude="*/.git/*" --exclude="/home/bitrix/www/backups/*" --exclude="/home/bitrix/www/app/cache/dev/*" --exclude="/home/bitrix/www/app/logs/*" --exclude="/home/bitrix/www/neb/dev/*" --exclude="/home/bitrix/www/neb/log.txt" --exclude="/home/bitrix/www/neb/upload/*" --exclude="/home/bitrix/www/neb/bitrix/backup/*" --exclude="/home/bitrix/www/neb/bitrix/cache/*" --exclude="/home/bitrix/www/neb/bitrix/stack_cache/*" --exclude="/home/bitrix/www/neb/bitrix/managed_cache/*" --exclude="/home/bitrix/www/neb/bitrix/upload/*" /home/bitrix/www/*

#Stats backup
tar -cvzf /home/bitrix/www/backups/distrib/stats-api-$date.tar.gz --exclude="*.log" --exclude="*/.git/*" /home/bitrix/stats-api*