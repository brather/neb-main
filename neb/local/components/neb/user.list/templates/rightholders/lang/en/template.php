<?php
$MESS['DATE_REGISTER'] = 'Register date';
$MESS['ACTION'] = 'Action';
$MESS['RIGHTHOLDER_BOOKS'] = 'Books list';
$MESS['RIGHTHOLDER_BOOKS_TITLE'] = 'Rightholder books';
$MESS["READERS_LIST_VERIFY"] = "Verify";
$MESS["READERS_LIST_VERIFIED"] = "Verified";

$MESS["WORK_COMPANY"] = "Work company";
$MESS["STATUS_ALL"] = "All statuses";
$MESS["STATUS_VERIFIED"] = "Verified";
$MESS["STATUS_REGISTERED"] = "Not verified";