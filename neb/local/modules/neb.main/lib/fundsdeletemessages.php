<?php
/**
 * User: agolodkov
 * Date: 02.09.2015
 * Time: 11:30
 */

namespace Neb\Main;


use Bitrix\Main\Entity\DataManager;

class FundsDeleteMessagesTable extends DataManager
{
    public static function getTableName()
    {
        return 'neb_funds_delete_messages';
    }

    public static function getMap()
    {
        return array(
            'ID'    => array(
                'data_type'    => 'integer',
                'primary'      => true,
            ),
            'BOOK_ID'    => array(
                'data_type' => 'string',
            ),
            'MESSAGE'    => array(
                'data_type' => 'string',
            ),
            'TIMESTAMMP' => array(
                'data_type' => 'datetime',
            ),
        );
    }
}