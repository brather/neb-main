$(function(){
    $(".d-map-list-info").tooltip();
    var container = $('.lk-container');
    container.removeClass('container').addClass('container-fluid');
    //region Добавление и удаление книг из плана по одиночке
    var button = $('.btn-js'),
        clsLbl = '.lbl',
        checkbox = $('.js-checkbox'),
        tools = {
            add : '/local/tools/plan_digitization/update.php',
            remove : '/local/tools/plan_digitization/remove.php'
        };
    checkbox.on('change', function() {
        var $this = $(this),
            containmess = $this.closest('label').find(clsLbl),
            current = $this.data('current'),
            send = {
                sessid : $this.data('ssid'),
                BOOK_ID: {
                    0 : $this.data('exalead-id')
                },
                date: $this.data('date'),
                plan_id: $this.data('plan-id')
            },
            bodyThis = $this.closest('body');
        startLoader(bodyThis);
        //region Добавляем издание в план
        if(current == 'plus') {
            send.STATUS = $this.data('status-add');
            $.post(tools.add, send).done(function(data){
                containmess.html($this.data('label-remove'));
                $this.data('current', 'minus');
                stopLoader();
            });
        }
        //endregion
        //region Удаляем издание из плана
        else {
            send.STATUS = $this.data('status-remove');
            $.post(tools.remove, send).done(function(data){
                containmess.html($this.data('label-add'));
                $this.data('current', 'plus');
                stopLoader();
            });
        }
        //endregion
    });
    //endregion
    //region Механизм раскрытия комментириев от пользователя
    var btnShowRow = $('.js-next'),
        clsShow = 'active',
        speedToggle = 100;
    btnShowRow.on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            thisTable = $this.closest('table'),
            thisRow = $this.closest('tr'),
            nextRow = thisRow.next();
        if(thisRow.hasClass(clsShow)) {
            thisRow.removeClass(clsShow);
            nextRow.toggle(speedToggle);
        } else {
            if(thisTable.find('.' + clsShow).length > 0) {
                thisTable.find('.' + clsShow).removeClass(clsShow).next().toggle(speedToggle);
            }
            thisRow.addClass(clsShow);
            nextRow.toggle(speedToggle);
        }
    });
    //endregion
    //region Добавление в план изданий всей страницей
    var btnAll = '.js-all',
        tableList = $('#list-books'),
        clsCheckbox = '.js-checkbox';
    $(document).on('click', btnAll, function(e){
        e.preventDefault();
        var $this = $(this),
            curentAction = $this.data('action'),
            label = $this.data('label'),
            bodyThis = $this.closest('body'),
            send = {
                sessid : $this.data('ssid'),
                date: $this.data('date'),
                plan_id: $this.data('plan-id'),
                STATUS : $this.data('status'),
                BOOK_ID: {}
            };
        startLoader(bodyThis);
        tableList.find(clsCheckbox + '[data-current=' + curentAction + ']').each(function(i,y){
            send.BOOK_ID[i] = $(y).data('exalead-id');
        });

        function afterPost() {
            tableList.find(clsCheckbox + '[data-current=' + curentAction + ']').each(function(i,y){
                if(curentAction == 'plus') {
                    $(y).attr('data-current', 'minus');
                    $(y).prop('checked', true);
                } else {
                    $(y).attr('data-current', 'plus');
                    $(y).prop('checked', false);
                }
                $(y).closest('label').find(clsLbl).html(label);
            });
        }

        if(curentAction == 'plus') {
            $.post(tools.add, send).done(function(data){
                afterPost();
                stopLoader();
            });
        } else {
            $.post(tools.remove, send).done(function(data){
                afterPost();
                stopLoader();
            });
        }
    });
    //endregion
});
//region Функция запуска лоадера
function startLoader(block) {
    stopLoader();
    var blkLoader = '<div id="blk-loader" class="blk-milk-shadow"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
    $(block).append(blkLoader);
}
//endregion
//region Функция остановки лоадера
function stopLoader() {
    $('#blk-loader').animate({ 'opacity' : 0 }, 300, function(){
        $(this).remove();
    });
}
//endregion