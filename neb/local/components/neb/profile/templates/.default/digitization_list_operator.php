<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);
$context = Application::getInstance()->getContext();
$arServer = $context->getServer()->toArray();
$arRequest = $context->getRequest()->toArray();

global $APPLICATION;
$sefFolder = '/profile/digitization_list/';

if(!empty($arRequest['idplan'])) {?>
    <div class="container-fluid">
        <div class="row">
            <a class="btn btn-default" href="<?= $sefFolder ?>">
                <i class="fa fa-reply"></i> Вернуться к списку планов
            </a>
        </div>
    </div><?
    $APPLICATION->IncludeComponent(
        'neb:digitization.plan.edit',
        '',
        array(
            'SEF_FOLDER' => $sefFolder,
            'COUNT_ON_PAGE' => 15,
            'AJAX_PLAN_EDIT' => '/local/tools/plan_digitization/plan.update.ajax.php',
            'AJAX_BOOK_EDIT' => '/local/tools/plan_digitization/update.book.ajax.php',
            'DOWNLOAD_PATH' => '/local/tools/plan_digitization/download.book.php?path=',
            'AJAX_COMMENT_BOOK' => '/local/tools/plan_digitization/plan.book.comment.ajax.php',
            'BY_SORT_DEFAULT' => 'UF_BOOK_NAME',
            'BY_ORDER_DEFAULT' => 'asc',
            'BY_SORT_LIST' => array(
                'UF_BOOK_NAME',
                'UF_BOOK_AUTHOR',
                'UF_DATE_ADD'
            )
        )
    );?>
    <div class="container-fluid">
        <div class="row">
            <a class="btn btn-default" href="<?= $sefFolder ?>">
                <i class="fa fa-reply"></i> Вернуться к списку планов
            </a>
        </div>
    </div><?
} else {
    $APPLICATION->IncludeComponent(
        'neb:digitization.plan.list',
        '',
        array(
            'COUNT_ON_PAGE' => 15,
            'SEF_FOLDER' => $sefFolder,
            'AJAX_PLAN_EDIT' => '/local/tools/plan_digitization/plan.update.ajax.php',
            'BY_SORT_DEFAULT' => 'UF_PLAN_NAME',
            'BY_ORDER_DEFAULT' => 'asc',
            'BY_SORT_LIST' => array(
                'UF_LIBRARY',
                'UF_PLAN_NAME',
                'UF_COUNT_BOOKS',
                'UF_DATE_F_PLAN',
                'UF_REQUEST_STATUS',
                'UF_DATE_AGREEMENT',
                'UF_DATE_F_ACTUAL',
            )
        )
    );
}