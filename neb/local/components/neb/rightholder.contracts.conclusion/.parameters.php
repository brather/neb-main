<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
			"SEF_MODE" => array(
					"list" => array(
							"NAME" => "LIST PAGE",
							"DEFAULT" => "",
							"VARIABLES" => array()
					),
					"element" => array(
							"NAME" => "DETAIL PAGE",
							"DEFAULT" => "#ID#/",
							"VARIABLES" => array("ID")
					),
			),
	),
);