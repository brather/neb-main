<?php
CBitrixComponent::includeComponentClass('rest:rest.server');
CModule::IncludeModule('iblock');

/**
 * Class RestLibraryNebParty
 */
class RestExaleadBookPage extends RestServer
{
    /**
     * @var array
     */
    protected $_params
        = array(
            'book_id'     => null,
            'p'           => 1,
            'dataType'    => 'image',
            'width'       => DEFAULT_BOOK_PAGE_WIDTH,
            'search'      => null,
            'pageWidth'   => null,
            'isAvailable' => null,
        );


    /**
     * @return int
     * @throws Exception
     */
    public function getAction()
    {
        if (!CModule::IncludeModule('nota.exalead')) {
            throw new Exception('Module "nota.exalead" not installed', 500);
        }
        if (empty($this->_params['book_id'])) {
            throw new Exception('Parameter "book_id" is required');
        }
        $rslViewer = new \Nota\Exalead\RslViewer($this->_params['book_id']);
        switch ($this->_params['dataType']) {
            case 'image':
                header('Content-Type: image/jpeg;charset=utf-8');
                if ('open' === $this->_params['isAvailable']) {
                    if ((integer)$this->_params['pageWidth'] > 1000) {
                        $width = 1000;
                    } elseif ((integer)$this->_params['width'] > 0) {
                        $width = (integer)$this->_params['width'];
                    } else {
                        $width = 800;
                    }
                    if (empty($this->_params['search'])) {
                        echo $rslViewer->getDocumentPage(
                            $width,
                            $this->_params['p']
                        );
                    } else {
                        echo $rslViewer->getDocumentPageSearch(
                            $width,
                            $this->_params['p'],
                            urlencode(
                                str_replace(
                                    ' ', '|', trim($this->_params['search'])
                                )
                            )
                        );
                    }
                }
                die;
                break;
            case 'txt':
            case 'html':
                $this->arResult['content'] = $rslViewer->getDocumentPageData(
                    $this->_params['p'],
                    $this->_params['dataType']
                );
                break;
            default:
                throw new Exception('Unknown data type');
        }

        return 200;
    }
}