<?php
$MESS['LIB_PLAN_DIG_COMP_ALL_UPLOAD'] = 'Массовая загрузка ';
$MESS['LIB_PLAN_DIG_COMP_FOUNDS_FOR_DIGITIZATION'] = 'Фонды библиотеки для оцифровки';
$MESS['LIB_PLAN_DIG_COMP_DATA_ERROR'] = 'Неверные данные!';
$MESS['LIB_PLAN_DIG_COMP_LOADED'] = 'Загруженно';
$MESS['LIB_PLAN_DIG_COMP_DUPLICATION'] = 'Дубликаты';
$MESS['LIB_PLAN_DIG_COMP_NOT_FOUND_IN_NEL'] = 'Не найдены в НЭБ';
$MESS['LIB_PLAN_DIG_COMP_DETALIZATION'] = 'Детализация плана оцифровки.xls';
$MESS['LIB_PLAN_DIG_COMP_UNKNOWN_LIBRARY'] = 'Неизвестная библиотека!';
$MESS['LIB_PLAN_DIG_COMP_ERROR_LOAD_FILE'] = 'Ошибка загрузки файла!';
$MESS['LIB_PLAN_DIG_COMP_ERROR_LOAD_BOOK_INFO'] = 'Ошибка получения информации о изданиях!';