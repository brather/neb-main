<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?
$MESS['RIGHTHOLDER_STAT_BY_ALL_FIELDS'] = 'по всем полям';
$MESS['RIGHTHOLDER_STAT_BY_AUTHOR'] = 'по автору';
$MESS['RIGHTHOLDER_STAT_BY_FIND'] = 'Найти';
$MESS['RIGHTHOLDER_STAT_PERIOD_FROM'] = 'Всего в период с';
$MESS['RIGHTHOLDER_STAT_PERIOD_TO'] = 'по';
$MESS['RIGHTHOLDER_STAT_ON_PORTAL'] = 'на портале НЭБ размещено';
$MESS['RIGHTHOLDER_STAT_BOOK_5'] = 'ваших произведений';
$MESS['RIGHTHOLDER_STAT_BOOK_1'] = 'ваше произведение';
$MESS['RIGHTHOLDER_STAT_BOOK_2'] = 'ваших произведения';
$MESS['RIGHTHOLDER_STAT_NAME'] = 'Название';
$MESS['RIGHTHOLDER_STAT_AUTHOR'] = 'Автор';
$MESS['RIGHTHOLDER_STAT_BBK'] = 'ББК';
$MESS['RIGHTHOLDER_STAT_DATE'] = 'Дата добавления';
$MESS['RIGHTHOLDER_STAT_LINK'] = 'Ссылка<br> на pdf';
$MESS['RIGHTHOLDER_STAT_VIEWERS'] = 'Количество обращений /<br> дата последнего обращения';
$MESS['RIGHTHOLDER_STAT_READERS'] = 'Кол-во читателей';
$MESS['RIGHTHOLDER_STAT_VIEW'] = 'Посмотреть';