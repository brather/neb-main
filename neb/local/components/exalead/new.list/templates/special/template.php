<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if(empty($arResult['ITEMS'])) {
    return false;
}

?>

<div class="b-bookinfo_new iblock">
    <div class="b-bookinfo_tit">
        <!--<h2><a href="/special/new/" title="новые поступления">новые поступления</a></h2> <span class="b-searchpage_lnk">Май: добавлено <strong>1405 книги</strong> </span>-->
        <h2>новые поступления</h2> <span class="b-searchpage_lnk"><?=$arResult['MONTH']?>: добавлено <strong><?=$arResult['COUNT']?></strong> </span>
    </div>
    <ul class="b-bookboard_list">
        <?foreach($arResult['ITEMS'] as $arItem):?>
            <li>
                <div class="b_popular_descr">
                    <h4><a href="<?=$arItem['LINK']?>" class="popup_opener ajax_opener coverlay"
                           data-width="955" title="<?=$arItem['TITLE']?>"><?=$arItem['TITLE']?></a></h4>
                    <span class="b-autor">Автор: <?=$arItem['AUTHOR']?></span>
                    <div class="b-result_sorce_info"><em>Источник:</em> <?=$arItem['LIBRARY']?></div>
                </div>
            </li>
        <?endforeach;?>
    </ul>
</div>
