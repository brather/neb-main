<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 11:47
 */

namespace LbcBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class LbcMaindivisionintroductionimagesRepository extends EntityRepository
{
    public function findAllByMaindivision($maindivisionId)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT mii FROM LbcBundle:LbcMaindivisionintroductionimages mii
            JOIN mii.introduction mi
            WHERE mi.maindivisionid = :id'
            )->setParameter('id', $maindivisionId);

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}