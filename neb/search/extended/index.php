<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Расширенный поиск');
$APPLICATION->SetPageProperty('hide-title', true);
?>

<? $APPLICATION->IncludeComponent(
	"exalead:search.form",
	"redesign_extended",
	Array(
		"PAGE" => (''),
		"POPUP_VIEW" => 'Y',
		"ACTION_URL" => '/search/'
	)
); ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>