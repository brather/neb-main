<div class="b-addcomment">
	<form action="" class="b-form b-form_common b-formcomment" >
		<h3>Добавить комментарий</h3>
		<p class="meswarning">Внимание! Книга уже оцифрована. Хотите её оцифровать заново?</p>
		<p>Обязательно опишите почему вам нужна данная книга</p><p></p>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock ">

				<div class="field validate">
					<textarea   data-required="required" class="textarea" name="about" id="settings10" data-minlength="2" data-maxlength="800"></textarea>
					<em class="error required">Поле обязательно для заполнения</em>							
				</div>
			</div>
		</div>

		<p>Ваш комментарий может ускорить процесс оцифровки</p>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50">
				<div class="field validate">
					<input   data-required="required" class="input" name="about" id="settings10" data-minlength="2" data-maxlength="30"/>
					<em class="error required">Поле обязательно для заполнения</em>							
				</div>

			</div>
			<div class="fieldcell iblock w50">
			<div class="field validate">
				<div class="b-capcha"><img src="./pic/capha.jpg" alt=""></div>				
			</div>	
			</div>
		</div>
		<p>Введите код с картинки</p>
	
	<div class="clearfix">			
		<button type="submit" value="1" class="formbutton small">Добавить</button>
		<a class="formbutton gray btrefuse small" href="#">Отмена</a>	
	</div>


</form>

</div>  