<?php
/**
 * скрипт для агрегирования данных статистики по просмотру изданий библиотек
 * запускается раз в день и формирует пул данных за предыдущий день
 * Перенесен в \Neb\Main\Agents\StatsSaver::saveLibraryNebStatLog();
 */
if ('cli' !== php_sapi_name()) {
    die('Only CLI');
}
$documentRoot = realpath(dirname(__FILE__) . '/../');
$_SERVER['DOCUMENT_ROOT'] = $documentRoot;
require_once(__DIR__.'/functions.php');
require_once(__DIR__.'/config.php');
$_SERVER["DOCUMENT_ROOT"] = str_replace('_statistics', '', __DIR__);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
while (ob_get_level()) {
    ob_end_clean();
}
error_reporting(E_ERROR);
ini_set('display_errors', 1);
ini_set('memory_limit', '700M');
AddMessage2Log("Скрипт формирования статистики запущен");
// проверка было ли обновление 
$options = getopt('', array('from::', 'to::'));
$from = date("Y-m-d", time() - 86400);
$to = date("Y-m-d", time());
if (isset($options['from'])) {
    $from = $options['from'];
}
if (isset($options['to'])) {
    $to = $options['to'];
}
$strSql = "SELECT * FROM neb_stat_log WHERE DT >='$from' and DT < '$to'";
$rs = $DB->Query($strSql, false, $err_mess . __LINE__);

if ($rs->SelectedRowsCount() == 0) {
    // токен
    $array_post = array(
        'email'    => $Login,
        'password' => $Password
    );

    $json_res = curl(SITE_HOST . '/sso/auth/', $array_post);
    $result = json_decode($json_res);
    $token = $result->{'result'}->{'TOKEN'};

    //параметры для получения статистики
    $array_post2 = array(
        'from'  => $from,
        'to'    => $from,
        'token' => $token
    );
    $protocol = \COption::GetOptionString('nota.exalead', 'viewer_protocol');
    $ip       = \COption::GetOptionString("nota.exalead", "viewer_ip");
    $port     = \COption::GetOptionString("nota.exalead", "viewer_port");
    $key      = \COption::GetOptionString("nota.exalead", "viewer_key");

    $array_post2['offset'] = 0;
    $array_post2['len'] = 999;

    $alisLibrary = [];
    NebMainHelper::includeModule('nota.exalead');

    while (count($mass_stat) > 0 || $array_post2['offset'] == 0) {

        $json_res = curl($protocol . '://' . $ip . '/stat/', $array_post2, $port, $key);
        $mass_stat = json_decode($json_res, true);
        unset($json_res);

        /*  подключение к бд neb  */
        $link = mysql_connect($DBHost_bibl, $DBLogin_bibl, $DBPassword_bibl);

        if (!$link) {
            echo("Unable to connect to db");
            exit();
        }

        /* Выбор БД */
        if (!mysql_select_db($DBName_bibl, $link)) {
            echo("Cannot select DB");
            exit ();
        }

        mysql_set_charset("utf8");

        foreach ($mass_stat as $value) {

            $id_book = urldecode($value['document']);

            // определение библиотеки по id книги
            if (!isset($res[$id_book]['id_lib']) && !empty($id_book)) {

                $id_lib = 0;

                $bookData = \Nota\Exalead\LibraryBiblioCardTable::getByFullSymbolicId($id_book);
                $bookData = $bookData[0];

                if (!empty($bookData['ALIS'])) {
                    if (isset($alisLibrary[$bookData['ALIS']])) {
                        $id_lib = $alisLibrary[$bookData['ALIS']];
                    } else {
                        $alisLib = \Nota\Exalead\BiblioAlisTable::getById(
                            $bookData['ALIS']
                        )->fetch();
                        if (!empty($alisLib['Library'])) {
                            $id_lib = (integer)$alisLib['Library'];
                            $alisLibrary[$bookData['ALIS']] = $id_lib;
                        }
                    }
                }
                $res[$id_book]['id_lib'] = $id_lib;
            }

            // фиксирование количества просмотра книги
            if ($value['method'] == 'GetResource') {
                $res[$id_book]['read'] = ($res[$value['document']]['read'] > 0
                    ? $res[$value['document']]['read'] + 1 : 1);
                // фиксирование количества чтения книги
            } elseif ($value['method'] == 'RenderPageCustomSize'
                || $value['method'] == 'PageSearchRender'
            ) {
                $res[$id_book]['view'] = ($res[$value['document']]['view'] > 0
                    ? $res[$value['document']]['view'] + 1 : 1);
            }
        }

        $array_post2['offset'] = $array_post2['offset'] + 1000;
    }

    // запись статистики в таблицу
    foreach ($res as $id_book => $book) {
        if ($book['read'] > 0 || $book['view'] > 0) {
            $arFields = array(
                'DT'       => date('d.m.Y', strtotime($from)),
                'ID_BOOK'  => $id_book,
                'ID_LIB'   => $book['id_lib'],
                'CNT_READ' => $book['read'],
                'CNT_VIEW' => $book['view']
            );
            $arInsert = $DB->PrepareInsert("neb_stat_log", $arFields, "form");
            $strSql = "INSERT INTO neb_stat_log (" . $arInsert[0] . ") VALUES ("
                . $arInsert[1] . ")";
            $DB->Query($strSql, false, $err_mess . __LINE__);
        }

    }
    mysql_close($link);
    echo "Статистика успешно сформирована", PHP_EOL;
    AddMessage2Log("Статистика успешно сформирована");
} else {
    echo "Статистика была сформирована ранее", PHP_EOL;
    AddMessage2Log("Статистика была сформирована ранее");
}
?>