<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-filter_wrapper">
	<span class="sort_wrap sort_wrap_three">
		<a <?=SortingExalead("BOOK_AUTHOR")?>><?=GetMessage("FAVORITES_SORT_AUTHOR");?></a>
		<a <?=SortingExalead("BOOK_NAME")?>><?=GetMessage("FAVORITES_SORT_MAIN_TITLE");?></a>
		<a <?=SortingExalead("BOOK_PUBLISH_YEAR")?>><?=GetMessage("FAVORITES_SORT_DATE");?></a>
	</span>	
</div>
<ul class="b-filter_list_wrapper search-result__content-list clearfix">
	
		<?
			if(!empty($arResult['ITEMS'])){
				$i = 0;
				foreach($arResult['ITEMS'] as $arItem)
				{
					if(!empty($arResult['RESBOOKS'][$arItem['id']]))
						$arResBooks = $arResult['RESBOOKS'][$arItem['id']];
					else
						continue;

					$i++;
					$arItem['NUM_ITEM'] = ($arResult['iNumPage']-1)*($arParams['ITEM_COUNT']) + $i;
				?>
				<li class="search-result__content-list-kind clearfix" id="<?=$arItem['~id']?>" data-favorite-item>
					<button
						class="btn btn-primary button-read-listitem boockard-read-button"
						data-link="<?= $arItem['readBookId'] ?>"
						data-options="<?php echo htmlspecialchars(
							json_encode($arItem['viewerOptions']), ENT_QUOTES,
							'UTF-8'
						) ?>"
						data-server-error="<?=GetMessage("FAVORITES_SERVER_ERROR");?>"
						><?=GetMessage("FAVORITES_READ");?>
					</button>
					<button 
						class="btn btn-default button-remove" 
						data-id="<?=$arItem['~id']?>" 
						data-delete-faorite-edition 
						
						data-button-title="<?=GetMessage("FAVORITES_DELETE_YES");?>"
						data-cancel-title="<?=GetMessage("FAVORITES_DELETE_NO");?>"
						data-message-title="<?=GetMessage("FAVORITES_DELETE_TITLE");?>"
						data-message-text="<?=GetMessage("FAVORITES_DELETE_TEXT");?>"

						title="<?=GetMessage("FAVORITES_DELETE_CONFIRM");?>">&nbsp;
					</button>
					<div class="search-result__content-list-sidebar">
                        <span class="search-result__content-list-number"><?=$arItem['NUM_ITEM']?>.</span>

						<?php // иконка
						if ($arItem['fileExt'] && in_array('pdf',$arItem['fileExt']) && ($arItem['IS_READ'] == 'Y') ): ?>

							<?php if($arItem['isprotected'] == 2): ?>

								<span class="search-result__content-list-status-icon search-result__content-list-status-icon--close" title="<?=Loc::getMessage('COPYRIGHT_PUBLICATINON')?>"></span>
							<?php elseif($arItem['isprotected'] == 1 && !$arParams['IS_SHOW_PROTECTED']): ?>

								<span class="search-result__content-list-status-icon search-result__content-list-status-icon--close" title="<?=Loc::getMessage('PROTECTED_PUBLICATINON')?>"></span>
							<?php else: ?>

								<span class="search-result__content-list-status-icon search-result__content-list-status-icon--open" title="<?=Loc::getMessage('PUBLIC_PUBLICATION')?>"></span>
							<?php endif; ?>

						<?php elseif('10003' == $arItem['idlibrary']): ?>
							<span class="search-result__content-list-status-icon search-result__content-list-status-icon--el" title="<?=Loc::getMessage('EKBSON_CATALOG_RECORD')?>"></span>
						<?php else: ?>
							<span class="search-result__content-list-status-icon search-result__content-list-status-icon--read" title="<?=Loc::getMessage('CATALOG_RECORD')?>"></span>
						<?php endif; ?>

                    </div>

					<div class="search-result__content-main">

						<div class="search-result__content-main-links">
							<? if (!isset($arItem['readBookId'])) { ?>
								Книга <?=$arItem['id']?> не найдена
							<? } else { ?>
								<?php // название
								if ($arItem['fileExt'] && in_array('pdf',$arItem['fileExt']) && ($arItem['IS_READ'] == 'Y') ): ?>

									<?php if($arItem['isprotected'] == 2): ?>

										<a href="<?=$arItem['DETAIL_PAGE_URL']?>" data-width="955"
										   class="popup_opener ajax_opener coverlay
									   search-result__content-link-title search-result__content-link-title--close">
											<?=$arItem['title']?>
										</a>
									<?php elseif($arItem['isprotected'] == 1 && !$arParams['IS_SHOW_PROTECTED']): ?>

										<a href="<?=$arItem['DETAIL_PAGE_URL']?>" data-width="955"
										   class="popup_opener ajax_opener coverlay
									   search-result__content-link-title search-result__content-link-title--close">
											<?=$arItem['title']?>
										</a>
									<?php else: ?>

										<a href="<?=$arItem['DETAIL_PAGE_URL']?>" data-width="955"
										   class="popup_opener ajax_opener coverlay
									   search-result__content-link-title">
											<?=$arItem['title']?>
										</a>
									<?php endif; ?>

								<?php elseif('10003' == $arItem['idlibrary']): ?>
									<a href="<?=$arItem['DETAIL_PAGE_URL']?>" data-width="955"
									   class="popup_opener ajax_opener coverlay
								   search-result__content-link-title search-result__content-link-title--el">
										<?=$arItem['title']?>
									</a>
								<?php else: ?>
									<a href="<?=$arItem['DETAIL_PAGE_URL']?>" data-width="955"
									   class="popup_opener ajax_opener coverlay
								   search-result__content-link-title search-result__content-link-title--read">
										<?=$arItem['title']?>
									</a>
								<?php endif; ?>
							<?
							} ?>

							
						</div>
						<p class="search-result__content-link-info">
							<?
								if(!empty($arItem['authorbook']))
								{
								?><a title="<?=GetMessage("FAVORITES_AUTHOR");?>" href="/search/?f_field[authorbook]=<?=urlencode('f/authorbook/'.mb_strtolower(trim($arItem['authorbook'])))?>"><?=rtrim($arItem['authorbook'])?></a><?
								}
								if(!empty($arItem['year']))
								{
								?>, <a title="<?=GetMessage("FAVORITES_YEAR_PUBLICATION");?>" href="<?=(SITE_TEMPLATE_ID=="special")?"/special/search/":"/search/";?>?f_publishyear=<?=$arItem['year'].'&'.DeleteParam(array('f_authorbook', 'f_publishyear', 'f_publisher', 'dop_filter'))?>"><?=$arItem['year']?></a><?
								}
								if(intval($arItem['countpages']) > 0)
								{
								?>, <span title="<?=GetMessage("FAVORITES_NUMBER_OF_PAGES");?>"><?=$arItem['countpages']?></span><?
								}							
								if(!empty($arItem['publisher']))
								{
								?>, <a title="<?=GetMessage("FAVORITES_PUBLISHER");?>" href="<?=(SITE_TEMPLATE_ID=="special")?"/special/search/":"/search/";
								?>?f_field[publisher]=f/publisher/<?=urlencode(mb_strtolower(trim($arItem['publisher'])))
								.'&'.DeleteParam(['f_field', 'dop_filter'])?>"><?=$arItem['publisher']?></a><?
								}
							?>

						</p>
						
						<p><?=$arItem['text']?></p>	

						<div class="search-result__my-bar" data-list-widget>
							<a 
								class="search-result__my-favorites" 
								href="#" 
								data-list-toggler
								data-list-src="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>"
								data-edition-id="<?=urlencode($arItem['id'])?>"
							>
								<?=GetMessage("FAVORITES_MY_COLLECTIONS");?>
							</a>
							<span class="search-result__quote">х <?=(int)$arResBooks['UF_CNT_QUOTES']?></span>
							<span class="search-result__bmark">х <?=(int)$arResBooks['UF_CNT_BOOKMARKS']?></span>
							<span class="search-result__note">х <?=(int)$arResBooks['UF_CNT_NOTES']?></span>

                    		<div class="my-favorites-content" data-list-wrapper>
                				...
                    		</div>
						</div>			
					</div>
				</li><!-- /.b-result-docitem -->
				<?
				}
			}
		?>
</ul>
<?=$arResult['STR_NAV']?>