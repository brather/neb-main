<?php
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Loader;

/**
 * Created by PhpStorm.
 * User: achechel
 * Date: 23.01.2017
 * Time: 17:08
 */


class NotificationForm extends CBitrixComponent
{
    public $arResult = [];
    public $User = [];

    public function executeComponent()
    {
        global $USER;

        $this->arResult['URLS'] = $this->_getUrls();

        $this->_User(CUSer::GetByID( $USER->GetID() )->Fetch());

        $this->arResult['LIBRARIES'] = $this->_getLibraries($this->_rq()->get('query'));

        $this->includeComponentTemplate();
    }

    private function _getLibraries($libraryName = '')
    {
        Loader::includeModule('iblock');

        $libraryEmails = $this->_rq()->get('library_email');
        if (!is_array($libraryEmails)) {
            $libraryEmails = [$libraryEmails];
        }
        $libraryEmails = array_filter($libraryEmails);
        $libraryEmails = array_flip($libraryEmails);

        $filter = [
            'IBLOCK_ID'       => IBLOCK_ID_LIBRARY,
            '!PROPERTY_EMAIL' => false,
        ];
        if (!empty($libraryName)) {
            $filter['%NAME'] = $libraryName;
        }
        /** @todo Запилить модель для библиотек */
        $dbResult = (new CIBlockElement)->GetList(
            ['NAME' => 'ASC'],
            $filter,
            false,
            false,
            ['NAME', 'PROPERTY_EMAIL']
        );
        $libraries = [];
        while ($item = $dbResult->fetch()) {
            $libraries[] = [
                'name'     => $item['NAME'],
                'email'    => $item['PROPERTY_EMAIL_VALUE'],
                'selected' => isset($libraryEmails[$item['PROPERTY_EMAIL_VALUE']]),
            ];
        }
        unset($dbResult, $item, $filter, $libraryEmails);

        return $libraries;
    }

    private function _rq(){
        return \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
    }

    private function _getUrls()
    {
        return [
            'send'       => '/profile/notification/',
            'history'    => '/profile/notification/history/',
            'fileUpload' => '/api/files/upload/gallery/',
        ];
    }

    private function _User(array $user)
    {
        $arResGroups = [];

        $nebUser = new \nebUser($user['ID']);
        $arGroups = $nebUser->getUserGroups();
        foreach ($arGroups as $sCode)
            $arResGroups[] = 'ROLE_' . strtoupper($sCode);

        $this->User = $user;
        $this->User['EMAIL'] = $user['EMAIL'];
        $this->User['ROLES'] = $arResGroups;
        $this->User['TOKEN'] = $user['UF_TOKEN'];
        $this->User['NEB_USER'] = $nebUser;
    }
}