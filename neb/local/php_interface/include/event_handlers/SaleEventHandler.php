<?php

use \Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler('sale', 'OnSalePayOrder', ['SaleEventHandler', 'changeStatus']);

class SaleEventHandler
{
    public static function changeStatus($id, $val)
    {
        $obSaleOrder     = new \CSaleOrder();
        $obSaleBasket    = new \CSaleBasket();
        $obIBlockElement = new \CIBlockElement;

        $arOrder = $obSaleOrder->GetByID($id);
        $dbBasketItems = $obSaleBasket->GetList(
            array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
            array(
                "ORDER_ID" => $id
            ),
            false,
            false,
            array()// array("PRODUCT_ID")
        );
        while ($arItems = $dbBasketItems->Fetch()) {
            if ($val == "Y") {
                global $DB;
                $format = "YYYY-MM-DD";
                $new_format = "DD.MM.YYYY";
                $new_date = $DB->FormatDate($arOrder["PAY_VOUCHER_DATE"], $format, $new_format);
                $obIBlockElement->SetPropertyValues($arItems["PRODUCT_ID"], IBLOCK_PCC_ORDER, 21, "PAID");
                $obIBlockElement->SetPropertyValues($arItems["PRODUCT_ID"], IBLOCK_PCC_ORDER, $new_date, "PAID_DATE");

                //письмо об оплате продукта
                $rsUser = CUser::GetByID($arOrder["USER_ID"]);
                $arUser = $rsUser->Fetch();

                \CEvent::Send("WS_PSS_SALE_PAY", 's1', array("LOGIN" => $arUser['EMAIL'], "ID_SALE" => $arItems["PRODUCT_ID"]));
            } else {
                $obIBlockElement->SetPropertyValues($arItems["PRODUCT_ID"], IBLOCK_PCC_ORDER, "", "PAID");
                $obIBlockElement->SetPropertyValues($arItems["PRODUCT_ID"], IBLOCK_PCC_ORDER, "", "PAID_DATE");
            }
        }
    }
}