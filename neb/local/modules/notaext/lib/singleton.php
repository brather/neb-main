<?
namespace Bitrix\NotaExt;

/**
 * Абстрактный класс Singleton
 * @url http://ru.wikipedia.org/wiki/%D0%9E%D0%B4%D0%B8%D0%BD%D0%BE%D1%87%D0%BA%D0%B0_(%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)
 * "Шаблон проектирования, гарантирующий, что в однопоточном приложении будет единственный экземпляр класса с глобальной точкой доступа."
 */
abstract class Singleton
{
    private static $instances = array();

    public function __construct()
    {
        $class = get_called_class();
        if (array_key_exists($class, self::$instances))
            trigger_error("Tried to construct  a second instance of class \"$class\"", E_USER_WARNING);
    }

    /**
     * @return singleton
     */
    public static function getInstance()
    {
        $class = get_called_class();
        if (array_key_exists($class, self::$instances) === false)
            self::$instances[$class] = new $class();
        return self::$instances[$class];
    }
}