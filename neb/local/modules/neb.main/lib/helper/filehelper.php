<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 07.06.2017
 * Time: 15:42
 */

namespace Neb\Main\Helper;

use \Bitrix\Main\Config\Option;

/**
 * Class FileHelper - Утилиты для работы с файлами и изображениями
 *
 * @package Neb\Main\Helper
 */
class FileHelper
{
    /**
     * Превращает произвольную строку в название имя файла
     *
     * @param $sFileName
     * @return mixed|string
     */
    public static function str2FileName($sFileName) {

        $sFileName = trim($sFileName);
        $sFileName = str_replace(' ', '_', $sFileName);
        $sFileName = str_replace(['.', ',', ';', ':', '"', "'", '?', '/', '\\', '<', '>', '+', '=', '*', '|'], '', $sFileName);

        return $sFileName;
    }

    /**
     * Получает путь до файла по его Битрикс-массиву
     *
     * @param $arFile
     * @param bool $bServerAbs
     * @return string
     */
    public static function getFilePath($arFile, $bServerAbs = false) {
        return ($bServerAbs ? $_SERVER['DOCUMENT_ROOT'] : '')
            . '/' . Option::get('main','upload_dir','upload')
            . '/' . $arFile['SUBDIR'] . '/' . $arFile['FILE_NAME'];
    }

    /**
     * Автоматически преобразует mime-type изображения в расширение файла, используется в инфоблоках.
     * Карма: при расхождении расширения и mime-type у изображения IE блокирует его.
     *
     * @param $arFile
     * @return mixed
     */
    public static function convertTempImageFileExtension($arFile) {

        // нет файла
        if (!isset($arFile['tmp_name']) || empty($arFile['tmp_name'])) {
            return $arFile;
        }

        $arImageTypes = self::getImageFilesMap();

        // получение mime-type файла и его корректного расширения
        $sFileExt         = end(explode('.', $arFile['name']));
        $sCorrectMimeType = mime_content_type($arFile['tmp_name']);
        $sCorrectExt      = $arImageTypes[$sCorrectMimeType];

        // не картинка или не файл
        if (empty($sCorrectExt) || empty($sFileExt)) {
            return $arFile;
        }

        // замена расширения файла
        if ($sCorrectExt != $sFileExt) {
            $arFile['name'] = str_replace('.' . $sFileExt, '.' . $sCorrectExt, $arFile['name']);
        }

        // замена mime-типа файла
        if (isset($arFile['type']) && !empty($arFile['type']) && $arFile['type'] != $sCorrectMimeType) {
            $arFile['type'] = $sCorrectMimeType;
        }

        return $arFile;
    }

    public static function convertDbImageFileParams($arFile) {

        $arResult = [];

        $sFilePath    = self::getFilePath($arFile, true);
        $arImageTypes = self::getImageFilesMap();

        // получение mime-type файла и его корректного расширения
        $sFileExt         = end(explode('.', $sFilePath));
        $sCorrectMimeType = mime_content_type($sFilePath);
        $sCorrectExt      = $arImageTypes[$sCorrectMimeType];
        $sCorrectFileSize = !empty($sCorrectMimeType) ? intval(@filesize($sFilePath)) : 0;

        // не картинка или не файл или все хорошо
        if (empty($sFileExt) || empty($sCorrectMimeType) || empty($sCorrectExt) || $sCorrectFileSize == 0) {
            return $arResult;
        }

        // замена mime-типа файла
        if ($arFile['CONTENT_TYPE'] != $sCorrectMimeType) {
            $arResult['FILE']['CONTENT_TYPE'] = $sCorrectMimeType;
        }

        // замена расширения файла
        if ($sFileExt != $sCorrectExt) {
            $arResult['FILE']['FILE_NAME'] = str_replace('.' . $sFileExt, '.' . $sCorrectExt, $arFile['FILE_NAME']);
            $arResult['FILE']['ORIGINAL_NAME'] = str_replace('.' . $sFileExt, '.' . $sCorrectExt, $arFile['ORIGINAL_NAME']);
            $arResult['PATH'] = str_replace('.' . $sFileExt, '.' . $sCorrectExt, $sFilePath);
        }

        // замена размера файла
        if ($arFile['FILE_SIZE'] != $sCorrectFileSize) {
            $arResult['FILE']['FILE_SIZE'] = $sCorrectFileSize;
        }

        return $arResult;
    }

    /**
     * Оптимизация изображений по размеру без потери качества (кроме jpeg)
     * Для работы требуются линукс-утилиты: jpegtran, jpegoptim, optipng, gifsicle
     * https://developers.google.com/speed/docs/insights/OptimizeImages
     *
     * Прогрессивный JPEG: новый best practice: https://habrahabr.ru/post/165645/
     * Оптимизация изображений bash-скриптом: https://habrahabr.ru/post/154683/
     * Установка утилит: https://habrahabr.ru/sandbox/108538/ https://github.com/tjko/jpegoptim
     *
     * Также линукс-конманды для преобразования групп файлов
     * find . -type f \( -name "*.jpeg" -or -name "*.jpg" \) -exec jpegtran -verbose -copy none -optimize -progressive -outfile {} {} \;
     * find . -type f \( -name "*.jpeg" -or -name "*.jpg" \) -exec jpegoptim -m80 --all-progressive --strip-all {} {} \;
     * find . -type f -name "*.png" -exec optipng −strip all '{}' -o7 \;
     * find . -type f -name "*.gif" -exec gifsicle --batch -O2 {} > {} \;
     *
     * @param $sPath
     * @return string
     */
    public static function optimizeImage($sPath) {

        $sResult = $sCommand =  '';

        if (empty($sPath)) {
            return $sResult;
        }

        $sMimeType = mime_content_type($sPath);
        switch ($sMimeType) {
            case 'image/jpeg':  // сжатие с потерями (гугл хочет)
                //$sCommand = "/usr/bin/jpegtran -copy none -optimize -progressive -outfile '$sPath' '$sPath'";
                $sCommand = "/usr/bin/jpegoptim  -m80 --all-progressive --strip-all '$sPath' '$sPath'";
                break;
            case 'image/png':
                $sCommand = "/usr/bin/optipng −strip all '$sPath'"; // default speed, -o5 (slow), -o7 (very slow)
                break;
            case 'image/gif':
                $sCommand = "/usr/bin/gifsicle -O2 '$sPath' > '$sPath''";
                break;
        }
        $sUtilPath = explode(' ', $sCommand)[0];

        if (!empty($sCommand) && function_exists('exec') && file_exists($sUtilPath)) {
            exec($sCommand, $sResult);
        }

        return $sResult;
    }

    /**
     * Сохраняет внешнее изображение в кеш на диск и оптимизирует его размер
     *
     * @param $sPath
     * @param $sSubDir
     * @return bool|string
     */
    public static function saveTempImage($sPath, $sSubDir) {

        // формирование путей
        $sRootUploadDir =  '/' . Option::get('main','upload_dir','upload') . $sSubDir;
        $sFileSubDir    = $sRootUploadDir . '/' . substr(md5($sPath), 0, 3);
        $sFilePath      = $sFileSubDir . '/' . basename($sPath);

        if (file_exists($_SERVER["DOCUMENT_ROOT"] . $sFilePath)) {
            return $sFilePath;
        }

        // получение содержимого файла
        $bSourceFile = file_get_contents($sPath);
        if (empty($bSourceFile)) {
            return '';
        }

        // сохранение файла
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . $sRootUploadDir)) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . $sRootUploadDir, BX_DIR_PERMISSIONS);
        }
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . $sFileSubDir)) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . $sFileSubDir, BX_DIR_PERMISSIONS);
        }
        $iFileSize = file_put_contents($_SERVER["DOCUMENT_ROOT"] . $sFilePath, $bSourceFile);
        if ($iFileSize) {
            self::optimizeImage($_SERVER["DOCUMENT_ROOT"] . $sFilePath);
            return $sFilePath;
        }

        return '';
    }

    private static function getImageFilesMap() {
        return [
            'image/jpeg'  => 'jpg',
            'image/png'   => 'png',
            'image/gif'   => 'gif',
            'image/bmp'   => 'bmp',
            'image/tiff'  => 'tif'
        ];
    }
}