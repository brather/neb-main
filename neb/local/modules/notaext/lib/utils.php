<?
namespace Bitrix\NotaExt;

/**
 * Class Utils
 * @package Bitrix\NotaExt
 * Класс, в который необходимо размещать общие полезные функции.
 */
class Utils
{
    /**
     * функция отладки
     */
    public static function pre($var, $show = false)
    {
        if ($_REQUEST['sadmin'] == 'Y') {
            echo "<pre>";
            print_r($var);
            echo "</pre>";
        } else {
            if (!$show) echo "<!--PRE<pre>"; else echo "<pre>";
            print_r($var);
            if (!$show) echo "</pre>-->"; else echo "</pre>";
        }
    }

    /**
     * функция для формирования окончания
     */
    public static function GetEnding($number, $zero = 'ий', $n2_1 = 'ие', $n2_4 = 'ия')
    {
        $number = intval($number);
        $last_char = substr($number, strlen($number) - 1, 1);
        $last2char = substr($number, strlen($number) - 2, 2);

        $str = "неа";
        if (($last_char == 0) ||
            ($last_char >= 5 && $last_char <= 9) ||
            ($last_char >= 1 && $last_char <= 9 && ($last2char < 20 && $last2char > 10))
        )
            $str = $zero;
        else
            if ($last_char >= 2 && $last_char <= 4)
                $str = $n2_4;
            else
                $str = $n2_1;
        return $str;
    }

    /**
     * Функция обрезает строку на заданное число символов до слова
     * @param $string - обрезаисая строка
     * @param $length - до скольки символов обрезать строку
     * @return srtin - обрезаная строка
     */
    public static function trimming_line($string, $length = 70, $strEnd = ' …')
    {
        ++$length;
        $string = strip_tags($string);
        if ($length && mb_strlen($string) > $length) {
            $str = mb_substr($string, 0, $length);
            $pos = mb_strrpos($string, ' ');
            return mb_substr($str, 0, $pos) . $strEnd;
        }
        return $string;
    }
}