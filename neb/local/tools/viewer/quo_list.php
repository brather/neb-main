<?
/**
 * Получение списка цитат книги для открытого просмотровщика
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;

if (!$USER->IsAuthorized())
    exit();

$book_id = htmlspecialcharsbx(trim($_REQUEST['book_id']));
if (empty($book_id))
    exit();

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\UserData\Quotes,
    \Nota\UserData\Notes,
    \Nota\UserData\Bookmarks;

Loader::includeModule("nota.userdata");

$arQuo = Quotes::getListBook($book_id);

$arResJson = array();
if (!empty($arQuo)) {
    foreach ($arQuo as $quo) {
        $arResJson[] = array(
            'id' => $quo['ID'],
            'citearea' => $quo['UF_TEXT'],
            'citeimg' => $quo['UF_IMG_DATA'],
            'citepage' => $quo['UF_PAGE'],
            'citetop' => $quo['UF_TOP'],
            'citeleft' => $quo['UF_LEFT'],
            'citewidth' => $quo['UF_WIDTH'],
            'citeheight' => $quo['UF_HEIGHT'],
        );
    }
}

// Так же нам нужно добрать массивы заметок и закладок
$notesResJson = Notes::getNotesListJSON($book_id);
$bookmarksResJson = Bookmarks::getBookmarksListJSON($book_id);

// Инициализируем и наполняем результирующий массив
// Нужно так же учитывать особенность вёрстки, если один из массивов будет пуст - то с ним произойдёт ошибка
$arResultData = array(
    'cites' => array(),
    'notes' => array(),
    'bookmarks' => array(),
);
if ((bool)$arResJson && !empty($arResJson))
    $arResultData['cites'] = $arResJson;
if ((bool)$notesResJson && !empty($notesResJson))
    $arResultData['notes'] = $notesResJson;
if ((bool)$bookmarksResJson && !empty($bookmarksResJson))
    $arResultData['bookmarks'] = $bookmarksResJson;

MainHelper::showJson($arResultData);
