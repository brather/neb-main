<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);
$arRequest = $component->getRequest();

CJSCore::Init(array('date'));

?>
	<div class="b-readersearch clearfix">
		<h3><?=Loc::getMessage("MARKUP_LOG_TITLE");?></h3>
		<form class="form nrf">

			<input type="hidden" name="LOG[FILTER]" value="Y" />
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label for="e_author"><?= Loc::getMessage('MARKUP_LOG_AUTHOR') ?></label>
							<input type="text" name="LOG[AUTHOR]" id="e_author" value="<?=$arRequest['LOG']['AUTHOR']?>"
								   class="form-control" placeholder="<?= Loc::getMessage('MARKUP_LOG_AUTHOR') ?>" />
					</div>
					<div class="form-group">
						<label for="e_title"><?= Loc::getMessage('MARKUP_LOG_BOOK') ?></label>
							<input type="text" name="LOG[TITLE]" id="e_title" value="<?=$arRequest['LOG']['TITLE']?>"
								   class="form-control" placeholder="<?= Loc::getMessage('MARKUP_LOG_BOOK') ?>" />
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
						<label>Статус</label>
						<select class="form-control" name="LOG[STATUS]">
							<? foreach ($component->getStatuses() as $sCode): ?>
								<option value="<?=$sCode?>"<?= $arRequest['LOG']['STATUS'] == $sCode ? ' selected="selected"' : '';
								?>><?=Loc::getMessage('MARKUP_LOG_STATUS_' . strtoupper($sCode));?></option>
							<? endforeach; ?>
						</select>
					</div>

					<label>Статус добавлен/изменен</label>
					<div class="form-inline">
						<?=Loc::getMessage("MARKUP_LOG_FROM");?>
						<div class="form-group">
							<div class="input-group">
								<input type="text" name="LOG[DATE_FROM]" id="dateFromInput" size="10"
									   value="<?= $arRequest['LOG']['DATE_FROM'] ? : (new DateTime())->add("-60 days")?>"
									   class="datepicker-from form-control" data-masked="99.99.9999" />
							<span class="input-group-btn">
								<a class="btn btn-default" onclick="BX.calendar({node: 'dateFromInput', field: 'dateFromInput',  form: '', bTime: false, value: ''});">
									<span class="glyphicon glyphicon-calendar"></span>
								</a>
							</span>
							</div>
						</div>

						<?=Loc::getMessage("MARKUP_LOG_TO");?>
						<div class="form-group">
							<div class="input-group">
								<input type="text" name="LOG[DATE_TO]" id="dateToInput" size="10"
									   value="<?=$arRequest['LOG']['DATE_TO'] ? : (new DateTime())?>"
									   class="datepicker-current form-control" data-masked="99.99.9999" />
							<span class="input-group-btn">
								<a class="btn btn-default" onclick="BX.calendar({node: 'dateToInput', field: 'dateToInput',  form: '', bTime: false, value: ''});">
									<span class="glyphicon glyphicon-calendar"></span>
								</a>
							</span>
							</div>
						</div>
					</div>

				</div>
			</div>

			<input type="submit" value="<?= Loc::getMessage('FIND') ?>" class="btn btn-primary" />
			<input type="reset" value="<?= Loc::getMessage('MARKUP_LOG_RESET') ?>" class="btn btn-default" onClick="
				document.location.href = '<?=$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['template']?>';
			"/>
		</form>
	</div>

	<p><br /><?=Loc::getMessage("MARKUP_LOG_TOTAL");?>: <?=number_format($arResult['COUNT'], 0, '', ' ')?></p>

	<table class="table">
		<tr>
			<th><?=Loc::getMessage("MARKUP_LOG_AUTHOR");?></th>
			<th><?=Loc::getMessage("MARKUP_LOG_BOOK");?></th>
			<th><?=Loc::getMessage("MARKUP_LOG_WATCH");?></th>
			<th><?=Loc::getMessage("MARKUP_LOG_LOG");?></th>
		</tr>
		<? foreach($arResult['ITEMS'] as $arItem): ?>
			<tr>
				<td>
					<?=str_replace('#', ', ', $arItem['author']);?>
				</td>
				<td>
					<a href="/catalog/<?=$arItem['fullSymbolicIdBook']?>/" target="_blank"><?=$arItem['title'];?></a>
				</td>
				<td>
					<a href="/catalog/<?=$arItem['fullSymbolicIdBook']?>/viewer/"
					   target="_blank"><?=Loc::getMessage("MARKUP_LOG_WATCH");?></a>
				</td>
				<td>
					<a href="<?=$arParams['SEF_FOLDER'] . str_replace('#ID#', $arItem['fullSymbolicIdBook'],
						$arParams['SEF_URL_TEMPLATES']['detail'])?>"
					   title="_blank"><?=Loc::getMessage("MARKUP_LOG_LOG");?></a>
				</td>
			</tr>
		<? endforeach; ?>
	</table>

<?
$APPLICATION->IncludeComponent(
	"bitrix:main.pagenavigation",
	"",
	array(
		"NAV_OBJECT" => $arResult['NAV'],
		"SEF_MODE" => "Y",
	),
	false
);