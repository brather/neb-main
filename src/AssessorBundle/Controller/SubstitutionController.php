<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 14:43
 */

namespace AssessorBundle\Controller;


use AssessorBundle\Controller\Api\ApiAuthorRatingController;
use AssessorBundle\Controller\Api\ApiSubstitutionController;
use AssessorBundle\Controller\Api\RatingApiInterface;
use AssessorBundle\Traits\ControllerAssessorRatingTrait;
use BitrixBundle\Annotations\IncludeBitrix;
use JMS\DiExtraBundle\Annotation as DI;
use BitrixBundle\Annotations\Template as BitrixTemplate;

class SubstitutionController
{
    use ControllerAssessorRatingTrait;

    /**
     * @DI\Inject("assessor.api.substitution")
     * @var ApiSubstitutionController
     */
    public $api;

    /**
     * @BitrixTemplate("AssessorBundle:assessor:substitution.html.php")
     * @IncludeBitrix()
     *
     * @return array
     */
    public function listAction()
    {
        if ($query = $this->request->query->get('originalQuery')) {
            $qb = $this->_getListQueryBuilder($this->api->getEntityName());
            $qb->where($qb->expr()->like('t.originalQuery', ':query'));
            $qb->setParameter('query', "%$query%");
        }
        if(!$order = $this->request->query->get('order')) {
            $order = ['priority' => 'desc'];
        }
        $result = $this->getListData($this->api->getEntityName(), $order);

        return $result;
    }

}