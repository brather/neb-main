$(function() {
	//checkIsAvailablePdf();

	$( document ).on( "click", ".plan-digitalization .plus_ico", function() {
		var book_id = $(this).closest('.search-result').attr('id');
		//console.log(book_id);
		if($(this).closest('.plusico_wrap').hasClass('minus'))
		{
			//console.log('+');
			$.post("/local/tools/plan_digitization/add.php", { sessid: BX.message('bitrix_sessid'), BOOK_ID: book_id } );
		}
		else
		{
			//console.log('-');
			$.post("/local/tools/plan_digitization/remove.php", { sessid: BX.message('bitrix_sessid'), BOOK_ID: book_id } );
		}
	});
});

function popupVideoWindow(url, title) {
    var w = 640;
    var h = 480;
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

/*$(function() {
    $('.b-result-docitem .b-result-type_txt').click(function(){
        if(device.mobile()){
            messageOpen('.b-message');
        }else{
            $('.popup_autoreg_form').autoregPopup();
        }
        return false;
    });
});*/
