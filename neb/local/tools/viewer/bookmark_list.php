<?
/**
 * Список заметок для книги
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized())
	exit();
	
CModule::IncludeModule("nota.userdata");
use Nota\UserData\Bookmarks;	

$mark_id = intval($_REQUEST['mark_id']);

// удаление закладки
if ($mark_id > 0){

	Bookmarks::delete($mark_id);

} else {
	$book_id = htmlspecialcharsbx(trim($_REQUEST['book_id']));
	if(empty($book_id))
		exit();

	$page = intval($_REQUEST['page']);


	$arFields = array(
		'BOOK_ID' 	=> $book_id,
		'NUM_PAGE' 	=> $page,
		'PREVIEW' 	=> '',
	);
	$id = Bookmarks::addFromArray($arFields);

	echo $id;
}