<?namespace Intervolga\RemoteUserClient;

class Rights
{
	public static function canWrite()
	{
		global $USER;
		return $USER->IsAdmin();
	}

	public static function canRead()
	{
		global $USER;
		return $USER->IsAdmin();
	}
}