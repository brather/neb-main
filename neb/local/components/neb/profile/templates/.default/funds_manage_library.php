<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->IncludeComponent(
    "neb:library.funds.manage",
    "",
    Array(
        "LIBRARY_ID" => $arResult['LIBRARY']['ID'],
        "COUNTONPAGE" => 30
    ),
    false
);
