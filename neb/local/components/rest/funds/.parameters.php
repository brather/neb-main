<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'   => '/',
                'VARIABLES' => array(
                    'token',
                    'bookId',
                    'libraryId',
                    'reasonMsg',
                ),
                'TYPE'      => array('DELETE'),
            ),
        ),
    ),
); 