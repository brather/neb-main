<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;


$arResult['USER_VERIFIED'] = false;
$arResult['ESIA'] = false;
$arResult['FIELD_NAMES'] = array(
    'LAST_NAME'           => Loc::getMessage('MAIN_REGISTER_LASTNAME'),
    'NAME'                => Loc::getMessage('MAIN_REGISTER_FIRSTNAME'),
    'SECOND_NAME'         => Loc::getMessage('MAIN_REGISTER_SECNAME'),
    'PERSONAL_BIRTHDAY'   => Loc::getMessage('MAIN_REGISTER_BIRTHDAY'),
    'PERSONAL_GENDER'     => Loc::getMessage('MAIN_REGISTER_GENDER'),
    'UF_PASSPORT_NUMBER'  => Loc::getMessage('MAIN_REGISTER_PASSPORT_NUMBER'),
    'WORK_COMPANY'        => Loc::getMessage(
        'MAIN_REGISTER_PLACE_OF_EMPLOYMENT'
    ),
    'UF_BRANCH_KNOWLEDGE' => Loc::getMessage('MAIN_REGISTER_KNOWLEDGE'),
    'UF_EDUCATION'        => Loc::getMessage('MAIN_REGISTER_EDUCATION'),
    'UF_CITIZENSHIP'      => Loc::getMessage('MAIN_REGISTER_CITIZENSHIP'),
    'PERSONAL_ZIP'        => Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP'),
    'UF_REGION'           => Loc::getMessage('MAIN_REGISTER_REGION'),
    'UF_AREA'             => Loc::getMessage('MAIN_REGISTER_AREA'),
    'PERSONAL_CITY'       => Loc::getMessage('REGISTER_FIELD_PERSONAL_CITY'),
    'PERSONAL_STREET'     => Loc::getMessage('REGISTER_FIELD_PERSONAL_STREET'),
    'UF_FLAT'             => Loc::getMessage(
        'REGISTER_FIELD_PERSONAL_APARTMENT'
    ),
    'UF_PLACE_REGISTR'    => Loc::getMessage(
        'REGISTER_GROUP_ADDRESS_RESIDENCE'
    ),
    'WORK_ZIP'            => Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP'),
    'UF_REGION2'          => Loc::getMessage('MAIN_REGISTER_REGION'),
    'UF_AREA2'            => Loc::getMessage('MAIN_REGISTER_AREA'),
    'WORK_CITY'           => Loc::getMessage('REGISTER_FIELD_PERSONAL_CITY'),
    'WORK_STREET'         => Loc::getMessage('REGISTER_FIELD_PERSONAL_STREET'),
    'UF_HOUSE2'           => Loc::getMessage('REGISTER_FIELD_PERSONAL_HOUSE'),
    'UF_STRUCTURE2'       => Loc::getMessage(
        'REGISTER_FIELD_PERSONAL_BUILDING'
    ),
    'UF_FLAT2'            => Loc::getMessage(
        'REGISTER_FIELD_PERSONAL_APARTMENT'
    ),
);
$arResult['EMPTY_REQUIRED'] = array();
if ($arResult['DATA_SAVED'] == 'Y') {
    if (!is_array($arResult['ERRORS'])) {
        $arResult['ERRORS'] = array();
    }
    $error = false;
    $pushResult = array();
    $exceptionString = '';
    $nebUser = nebUser::forgeUser();
    if (isset($_REQUEST['scan1'])
        and isset($_REQUEST['scan2'])
        and !empty($_REQUEST['scan1'])
        and !empty($_REQUEST['scan2'])
    ) {
        $scan1 = CFile::MakeFileArray(
            $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['scan1']
        );
        $scan1['MODULE_ID'] = 'main';

        $scan2 = CFile::MakeFileArray(
            $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['scan2']
        );
        $scan2['MODULE_ID'] = 'main';

        $arFields = array(
            'UF_SCAN_PASSPORT1' => $scan1,
            'UF_SCAN_PASSPORT2' => $scan2
        );
        $user = new CUser;
        $user->Update($arResult['arUser']['ID'], $arFields);
        if ($user->LAST_ERROR) {
            $arResult['ERRORS'][] = Loc::getMessage('ERROR_USER_VERIFICATION');
            $arResult['DATA_SAVED'] = 'N';
        } else {
            if (!$arResult['arUser']['UF_SCAN_PASSPORT1']) {
                $arResult['arUser']['UF_SCAN_PASSPORT1'] = $scan1;
            }
            if (!$arResult['arUser']['UF_SCAN_PASSPORT2']) {
                $arResult['arUser']['UF_SCAN_PASSPORT2'] = $scan2;
            }
        }
    }
    if (nebUser::REGISTER_TYPE_ESIA == $arResult['arUser']['UF_REGISTER_TYPE']
    ) {
        $arResult['ESIA'] = true;
        $rgb = new \Nota\UserData\rgb();
        try {
            if ($nebUser->checkIntegrityGroup('full')) {
                $pushResult = $rgb->pushUserIntoRGB($nebUser);
            }
            if (isset($pushResult['id_request']) && $pushResult['id_request']) {
                $nebUser->setEICHB();
                $arResult['USER_VERIFIED'] = true;
            } else {
                $error = true;
            }
        } catch (Exception $e) {
            $error = true;
            $exceptionString = $e->__toString();
            AddMessage2Log($exceptionString);
        }
        if (true === $error) {
            $arResult['ERRORS'][] = Loc::getMessage('ERROR_USER_VERIFICATION');
            $arResult['DATA_SAVED'] = 'N';
        }
    } //меняем пользователю тип регистрации
    else {
        if ($arResult['arUser']['UF_SCAN_PASSPORT1']
            && $arResult['arUser']['UF_SCAN_PASSPORT2']
            && $arResult['arUser']['UF_PASSPORT_SERIES']
            && $arResult['arUser']['UF_PASSPORT_NUMBER']
        ) {
            try {
                global $USER;
                $USER->Update(
                    $USER->GetID(),
                    Array('UF_REGISTER_TYPE' => nebUser::REGISTER_TYPE_FULL)
                );
                if (!empty($arResult['arUser']['UF_RGB_USER_ID'])
                    || !empty($arResult['arUser']['UF_ID_REQUEST'])
                ) {
                    throw new Exception('ERROR_USER_VERIFICATION_SENT', 1);
                }
                $arResult['EMPTY_REQUIRED']
                    = $nebUser->getEmptyGroupFields('full');
                if (!empty($arResult['EMPTY_REQUIRED'])) {
                    foreach ($arResult['EMPTY_REQUIRED'] as $fieildName) {
                        $arResult['ERRORS'][] = sprintf(
                            Loc::getMessage(
                                'ERROR_USER_VERIFICATION_NO_REQUIRED_FIELD'
                            ),
                            isset($arResult['FIELD_NAMES'][$fieildName]) ?
                                $arResult['FIELD_NAMES'][$fieildName]
                                : $fieildName
                        );
                    }
                    throw new Exception(
                        'ERROR_USER_VERIFICATION_NO_REQUIRED', 1
                    );
                }

                $rgb = new \Nota\UserData\rgb();
                $pushResult = $rgb->pushUserIntoRGB(
                    $arResult['arUser']['ID']
                );
                if (!isset($pushResult['id_request'])
                    || empty($pushResult['id_request'])
                ) {
                    throw new Exception('ERROR_USER_VERIFICATION', 1);
                }
            } catch (Exception $e) {
                if (1 === $e->getCode()) {
                    array_unshift(
                        $arResult['ERRORS'],
                        Loc::getMessage($e->getMessage())
                    );
                    $arResult['DATA_SAVED'] = 'N';
                } else {
                    $arResult['ERRORS'][]
                        = Loc::getMessage('ERROR_USER_VERIFICATION');
                }

                $exceptionString = $e->__toString();
                AddMessage2Log($exceptionString);
            }
        } else {
            $arResult['ERRORS'][]
                = Loc::getMessage('ERROR_USER_VERIFICATION_PASSPORT');
        }
    }
    if (empty($pushResult)) {
        $log = new \CEventLog();
        $log->Log(
            'INFO',
            'user_profile_update',
            'user',
            $arResult['arUser']['ID'],
            serialize(
                array(
                    'error'            => $error,
                    'request'          => $_REQUEST,
                    'emptyGroupFields' => $nebUser->getEmptyGroupFields('full'),
                    'exception'        => $exceptionString,
                )
            )
        );
    }
}

if(!empty($arResult['ERRORS'])) {
    $arResult['DATA_SAVED'] = 'N';
}