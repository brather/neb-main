<?php
$MESS['COLLECTION_LIST_SORT_BY_COUNT'] = 'По количеству книг';
$MESS['COLLECTION_LIST_SORT_BY_TITLE'] = 'По названию';
$MESS['COLLECTION_LIST_SORT_BY_DATE'] = 'По дате';
$MESS['COLLECTION_LIST_ADD_TO_MY_COL'] = 'Добавить в мои подборки';
$MESS['COLLECTION_LIST_REMOVE_FROM_MY_COL'] = 'Удалить из моих подборок';

$MESS['COLLECTION_LIST_SHOW'] = 'Показать';

$MESS['COLLECTION_LIST_BOOK_5'] = 'изданий';
$MESS['COLLECTION_LIST_BOOK_1'] = 'издание';
$MESS['COLLECTION_LIST_BOOK_2'] = 'издания';
$MESS['COLLECTION_LIST_IN_COL'] = 'в подборке';

$MESS['COLLECTION_LIST_AUTHOR'] = 'Автор';
$MESS['COLLECTION_LIST_BY_LIB'] = 'по библиотеке';
$MESS['COLLECTION_LIST_MORE'] = 'следующие';