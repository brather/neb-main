<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);?>
<?if(empty($arResult['SECTIONS'])) {
	return false;
}?>
<?if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
	$APPLICATION->RestartBuffer();
}?>

<div class="search-result-sort clearfix res-sort-three">
	<a <?= SortingExalead("element_cnt") ?>><?= Loc::getMessage('COLLECTION_LIST_SORT_BY_COUNT'); ?></a>
	<a <?= SortingExalead("name") ?>><?= Loc::getMessage('COLLECTION_LIST_SORT_BY_TITLE'); ?></a>
	<a <?= SortingExalead("created") ?>><?= Loc::getMessage('COLLECTION_LIST_SORT_BY_DATE'); ?></a>
</div>
<ul class="collections-list">
	<?if($arParams['PAGEN'] > 1){
		$n = ($arParams['PAGEN'] - 1) * $arParams['SIZEN'];
	} else {
		$n = 0;
	}?>
	<?foreach($arResult['SECTIONS'] as $arSection) {

		/*if(empty($arResult['BOOKS_IN_SECTIONS'][$arSection['ID']]))
			continue;*/
		if($arSection['DEPTH_LEVEL'] == 1){?>
			<?$n++;?>
			<li class="collections-list-item cls">
				<div class="cls__wrapper">
					<!-- номер подборки --><span class="cls__num"><?= $n?>.</span>
					<?if(collectionUser::isAdd()){?>
						<div class="cls__add-books"
							 data-add-to-favorites-widget data-section-id="<?= $arSection['ID'] ?>"
							 data-add-href="<?= ADD_COLLECTION_URL ?>list.php?t=books_collection&id=<?= $arSection['ID'] ?>&action=checked"
							 data-add-toggler-text="<?= Loc::getMessage('COLLECTION_LIST_ADD_TO_MY_FAVORITES') ?>"
							 data-remove-href="<?= ADD_COLLECTION_URL ?>removeBook.php?t=books_collection&id=<?= $arSection['ID'] ?>"
							 data-remove-toggler-text="<?= Loc::getMessage('COLLECTION_LIST_REMOVE_FROM_MY_FAVORITES') ?>">
							<div class="cls_add-books-toggler-wrapper">
								<?php if(true === $arSection['IN_MY_COLLECTIONS']):?>
									<a href="<?= ADD_COLLECTION_URL ?>removeBook.php?t=books_collection&id=<?= $arSection['ID'] ?>"	data-remove-collection-from-favorites >
										<?= Loc::getMessage('COLLECTION_LIST_REMOVE_FROM_MY_FAVORITES') ?>
									</a>
								<?php else: ?>
									<a href="<?= ADD_COLLECTION_URL ?>list.php?t=books_collection&id=<?= $arSection['ID'] ?>&action=checked" data-add-collection-to-favorites>
										<?= Loc::getMessage('COLLECTION_LIST_ADD_TO_MY_FAVORITES') ?>
									</a>
								<?php endif; ?>
							</div>
						</div>
					<?}?>
					<!-- название подборки -->
					<h2><a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME'] ?></a></h2>
					<?if(!empty($arResult['LIBRARY'][$arSection['UF_LIBRARY']])) {?>
						<!-- автор библиотека -->
						<div class="cls__author"><em><?= Loc::getMessage('COLLECTION_LIST_AUTHOR'); ?>:</em>
							<a href="<?= $arResult['LIBRARY'][$arSection['UF_LIBRARY']]['DETAIL_PAGE_URL'] ?>" class="b-sorcelibrary">
								<?= $arResult['LIBRARY'][$arSection['UF_LIBRARY']]['NAME'] ?>
							</a>
						</div>
					<?}?>
					<div class="collections-list-item__preview clsp">
						<?$coverBookID = $arResult['SECTIONS_BOOKS']['COVER'][$arSection['ID']];?>
						<?if(!empty($arResult['BOOKS'][$coverBookID])) {
							$arItem = $arResult['BOOKS'][$coverBookID];?>
							<div class="clsp__cover">
								<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="clsp__image" data-width="955" >
									<img src="<?= $arItem['IMAGE_URL'] ?>&width=131&height=194"
										 alt="<?= $arItem['title'] ?>"
										 title="<?= $arItem['title'] ?>">
								</a>
								<div class="clsp__title">
									<?= $arItem['title'] ?>
								</div>
								<?if(!empty($arItem['authorbook'])) {?>
									<?foreach ($arItem['authorbook_for_link'] as $key=>$author) {?>
										<a href="/search/?f_field[authorbook]=f/authorbook/<?= urlencode(mb_strtolower(strip_tags(trim($author)))) ?>"
										   class="clsp__author">
											<?= $author?>
										</a>
										<?if ($key+1 < count($arItem['authorbook_for_link'])):?>, <?endif;?>
									<?}?>
								<?}?>
							</div>
						<?}?>
						<div class="clsp__details">
							<div class="clsp__editions-list"
								 data-section-id="<?= $arSection['ID'] ?>"
								 data-count-all="<?= $arSection['ELEMENT_CNT'] ?>"
								 data-count-in-slide="<?= $arParams["COUNT_BOOKS_IN_COLLECTION"] ?>"
								 data-iblock-id="<?= $arResult['IBLOCK_ID_COLLECTIONS'] ?>"
								 data-slide-editions="<?= $n ?>">
								<?if(!empty($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']])){
									$i = 0;
									$arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']] = array_slice($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']], 0, 20);
									foreach($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']] as $bookID) {
										$arItem = $arResult['BOOKS'][$bookID];
										if(empty($arItem)) {
											continue;
										}
										$i++;?>
										<div class="clsp__editions-list-item">
											<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="b_bookpopular_photo popup_opener ajax_opener coverlay" data-width="955" >
												<img data-placement="<?= $dPlacement ?>"
													 src="<?= $arItem['IMAGE_URL'] ?>&width=82&height=140"
													 alt="<?= $arItem['title'] ?>"
													 title="<?= $arItem['title'] ?>"
													 class="loadingimg">
											</a>
										</div>
										<?if(($i%4) == 0 and $i < count($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']])) {?>
											<!--/ul-->
										<?}?>
									<?}
								}?>
							</div>
							<!-- 3 книги в подборке -->
							<a href="<?= $arSection['SECTION_PAGE_URL'] ?>" class="btn btn-primary pull-right">
								<?= $arSection['ELEMENT_CNT'] ?> <?= GetEnding($arSection['ELEMENT_CNT'], Loc::getMessage('COLLECTION_LIST_BOOK_5'), Loc::getMessage('COLLECTION_LIST_BOOK_1'), Loc::getMessage('COLLECTION_LIST_BOOK_2')) ?> <span><?= Loc::getMessage('COLLECTION_LIST_IN_COL'); ?></span>
							</a>
							<button type="button" class="clsp__editions-list-prev"></button>
							<button type="button" class="clsp__editions-list-next"></button>
						</div>
					</div><!-- /.collections-list-item__preview -->
				</div><!-- /.cls__wrapper -->
			</li><!-- /.collections-list-item.cls -->
		<?}?>
	<?}?>
</ul>
<!-- /.b-collection_list -->
<?= $arResult['NAV_STRING'] ?>
<?if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
	exit();
}?>
<?if(!empty($arResult['LIBRARY'])) {
    CJSCore::Init();
    ?>
	<script type="text/javascript">
		$(function() {
			$(".b-sidenav input.checkbox").change(function() {
				var libraryId = 0;
				if($(this).attr('checked') == 'checked') {
					var libraryId = $(this).val();
				}
				$(".b-sidenav input.checkbox").not(this).attr('checked', false).closest('span.field').removeClass('checked');
				BX.showWait($(this).closest('li').attr('id'));
				$.get( "<?= $APPLICATION->GetCurPage() ?>", {library: libraryId}, function( data ) {
					BX.closeWait();
					$('.b-mainblock.left>*').remove();
					$('.b-mainblock.left').prepend(data);

					$('.js_slider_single_nodots').slick({
						dots: false,
						cssEase: 'ease',
						speed: 600
					});
					$('.iblock').cleanWS();
					/* $('.popup_opener').uipopup();
					$('.b-bookadd').favAdd();*/
				});
			});
			<?if(!empty($arParams["LIBRARY_ID"])) {?>
				/*var checkedDf = $(".b-sidenav input.checkbox[value='<?= $arParams["LIBRARY_ID"] ?>']").closest('ul').show().prev().addClass('open');*/
				var checkedDf = $(".b-sidenav input.checkbox[value='<?= $arParams["LIBRARY_ID"] ?>']").closest('div.b-sidenav').find('.b-sidenav_title').addClass('open');
				var checkedDf = $(".b-sidenav input.checkbox[value='<?= $arParams["LIBRARY_ID"] ?>']").closest('div.b-sidenav_cont').show();
			<?}?>
		});
	</script>
<?}?>

