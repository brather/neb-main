<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (false !== stripos($_SERVER['REQUEST_URI'], '/plans')): ?>

    <?$APPLICATION->IncludeComponent(
        "neb:library.plan_digital.stat.plans",
        ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
        )
    );?>

<? elseif (false !== stripos($_SERVER['REQUEST_URI'], '/publications')): ?>

    <?$APPLICATION->IncludeComponent(
        "neb:library.plan_digital.stat.publications",
        ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
        )
    );?>

<? else: ?>

    <a href="plans/">Статистика по планам оцифровки</a><br /><br />

    <a href="publications/">Статистика по оцифруемым изданиям</a><br /><br />

<? endif;