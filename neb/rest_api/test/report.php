<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	global $APPLICATION;
	$APPLICATION->SetTitle("Тест API");
	CJSCore::Init('jquery');
?>
<script type="text/javascript">
	$(function(){
		$('#api_dir').change(function(){
			$('form#ftest').attr('action', $(this).val());
		});
		
		$('#api_method').change(function(){
			$('form#ftest').attr('method', $(this).val());
		});
		
		$('form#ftest').submit(function(){
			$.ajax({
				url: 		$(this).attr('action'),
				//method: 	$(this).attr('method'),
				type: 		$(this).attr('method'),
				//dataType: 	'json',
				data: 		$(this).serialize()
			}).always(function(data){
				if(typeof(data.responseJSON) != 'undefined')
					$('#response').html(JSON.stringify(data.responseJSON, null, 2));
				else
					$('#response').html(JSON.stringify(data, null, 2));
			});
			
			return false;
		});
	});
</script>

<select name="api_dir" id="api_dir">
	<option value="">Выбор АПИ</option>
	<option value="/rest_api/library/">Библиотеки</option>
	<option value="/rest_api/users/">Пользователи</option>
</select>
<select id="api_method">
    <option value="">Выбор метода</option>
	<option value="GET">GET</option>
</select>

<form action="" method="" id="ftest">
	<label>Token: <input name="token" value="6c683b7249ba00be3891c01110f0dc42" /></label>
	<label>From: <input name="from" value="11.09.2014" /></label>
	<label>To: <input name="to" value="19.11.2014" /></label>

	<input type="submit" />
</form>
<br/>
<span>Ответ: <span id="response"></span></span>