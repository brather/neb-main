<?php

$MESS['FEEDBACK_REJECT_DATE_CREATE'] = 'Date create';
$MESS['FEEDBACK_REJECT_DATE_CLOSE']  = 'Date close';
$MESS['FEEDBACK_REJECT_STATUS']      = 'Status';
$MESS['FEEDBACK_REJECT_THEME']       = 'Theme';
$MESS['FEEDBACK_REJECT_BOOK']        = 'Book';
$MESS['FEEDBACK_REJECT_MESSAGE']     = 'Message';
$MESS['FEEDBACK_REJECT_ANSWER']      = 'Answer';
$MESS['FEEDBACK_REJECT_FILE']        = 'File';

$MESS['FEEDBACK_REJECT_TITLE']       = 'Reject order №';
$MESS['FEEDBACK_REJECT_WRITE_CAUSE'] = 'Reason for rejection';
$MESS['FEEDBACK_REJECT_SUBMIT']      = 'Reject order';