<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => Loc::getMessage('BOOKS_READING_LIST_DESCRIPTION_NAME'),
	"DESCRIPTION" => Loc::getMessage('BOOKS_READING_LIST_DESCRIPTION_DESCRIPTION'),
	"ICON" => '/images/icon.gif',
	"SORT" => 10,
	"PATH" => array(
		"ID" => 'NEB',
		"NAME" => '',
		"SORT" => 10,
		"CHILD" => array(
			"ID" => 'user_data',
			"NAME" => Loc::getMessage('BOOKS_READING_LIST_DESCRIPTION_GROUP'),
			"SORT" => 10
		)
	),
);
