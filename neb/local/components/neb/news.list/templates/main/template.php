<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>

<!-- Новости НАЧАЛО -->
<section class="main-news container">
    <div class="row">
        <div class="main-news__title clearfix">
            <div class="main-news__title-icon col-md-1">
                <img src="/local/templates/adaptive/img/news.png" alt="" />
            </div>
            <h3 class="col-md-11 main-news__title-header">
                <a href="/news/">
                    <?=Loc::getMessage("NEWS_BLOCK_TITLE")?>
                </a>
            </h3>
        </div>
        <div class="main-news__gallery main-news__gallery--general">
            <?php foreach($arResult['ITEMS'] as $key => $item): ?>
                <div class="col-md-4 col-sm-6 col-xs-12 main-news-item">
                    <a href="<?= $item['DETAIL_PAGE_URL']?>" class="main-news-item__image" <?if($item["PREVIEW_PICTURE"]['SRC']):?> style="background-image: url('<?php echo $item["PREVIEW_PICTURE"]['SRC'];  ?>');"<?endif;?>></a>
                    <a href="<?= $item['DETAIL_PAGE_URL']?>" class="main-news-item__title"><?= $item['NAME']?></a>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<!-- Новости КОНЕЦ -->

