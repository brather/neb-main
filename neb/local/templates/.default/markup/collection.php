
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>
						<div class="checkwrapper b-search_lib">
							<input class="checkbox js_btnsub" type="checkbox" name="" id="cb3"><label for="cb3" class="black fz_mid">Искать только в полнотекстовых изданиях</label>
						</div>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			
		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<input type="submit" class="formbutton" value="Принять">
				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix searchempty ">
	<div class="b-mainblock left">
		<nav class="b-commonnav">
			<a href="#" class="current">новые</a>
			<a href="#">популярные</a>
			<a href="#">рекомендованные</a>
		</nav>
		<div class="b-collection_descr">
			<div class="b-collection_act">
				<div class="b-addcollecion rel">
					<span>Добавить в мои подборки</span>
					<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
						<a class="b-bookadd" data-collection="ajax_favs.html" href="#"></a></div>
				</div>
				<div class="b-collupdate"><em>Обновлено:</em> 12.07. 2014</div>
			</div>
			<h1>Серебряный век: быт, нравы, моды, знаменитости</h1>
			<div class="b-result_sorce_info"><em>Автор:</em> <a class="b-sorcelibrary" href="#">Рязанская областная библиотека</a></div>
			<p>Хочу собрать книги, в которых пожилой герой (героиня) вспоминает свою молодость, или какие-то яркие события из жизни, или полностью рассказывает свой жизненный путь. Книги только художественные.</p>
			<p>Подборка коллективная, так что добавляйте — не стесняйтесь! Буду только благодарна за помощь!</p>
			<a class="b-lib_photoslink" href="#">Фотогалерея</a>
			<a href="#" class="set_opener iblock right">Настройки</a>
		</div>
		<div class="b-filter js_filter b-filternums">
			<div class="b-filter_wrapper">	
				<a href="#" class="sort sort_opener">Сортировать</a>
				<span class="sort_wrap">			
				<!--<a href="#" class="sort down">1..10</a>	-->
				<a href="#" class="sort up">По автору</a>
				<a href="#" class="sort">По названию</a>
				<a href="#" class="sort">По дате</a>
				</span>
				<span class="b-filter_act">
					<a href="#" title="Отобразить книги коллекции плиткой" class="b-filter_items"></a>
					<a href="#" title="Отобразить книги коллекции списком" class="b-filter_list current"></a>
					<span class="b-filter_show">Показать</span>
					<a href="#" class="b-filter_num b-filter_num_paging">10</a>
					<a href="#" class="b-filter_num b-filter_num_paging ">25</a>
					<a href="#" class="b-filter_num b-filter_num_paging">50</a>				
				</span>
			</div>
		</div><!-- /.b-filter -->
		<div class="b-filter_items_wrapper">
			<div class="b-result-doc b-result-docfavorite js_sortable ">
				<div class="b-result-docitem iblock removeitem">


					<div class="b-result-docinfo">
						<span class="num">1.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock popup_opener" href="#" ><img alt="" class="loadingimg" src="./pic/pic_4.jpg"></a>
							
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">2.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_27.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">3.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_29.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">футурологический конгресс</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">4.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_30.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">5.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_30.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						
					</div>

				</div><!-- /.b-result-docitem -->		
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">6.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_29.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">футурологический конгресс</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						
					</div>

				</div><!-- /.b-result-docitem -->	
				
			</div><!-- /.b-result-doc -->
			
			<div class="b-paging">
				<div class="b-paging_cnt">
					<a href="" class="b-paging_prev iblock"></a>
					<a href="" class="b-paging_num current iblock">1</a>
					<a href="" class="b-paging_num iblock">2</a>
					<a href="" class="b-paging_num iblock">3</a>
					<a href="" class="b-paging_num iblock">4</a>
					<a href="" class="b-paging_next iblock"></a>
				</div>
			</div><!-- /.b-paging -->

		</div><!-- /.b-filter_items_wrapper -->
		<div class="b-filter_list_wrapper">
			<div class="b-result-doc">
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php"><img alt="" class="loadingimg" src="./pic/pic_4.jpg"></a>
						
						<div class="b-result-doclabel">
							Требуется авторизация
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">1.</span>
						<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
						<a class="b-bookadd" data-collection="ajax_favs.html" href="#"></a></div>
						<h2><a data-width="955" href="ajax_bookview.php" class=" coverlay popup_opener ajax_opener">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					

					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_27.jpg"></a>

					</div>
					<div class="iblock b-result-docinfo">
					<span class="num">2.</span>
						<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
						<a class="b-bookadd" data-collection="ajax_favs.html" href="#"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Межпоселенческая централизованная библиотечная система Никольского муниципального района</a></div>
							
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_4.jpg"></a>

					</div>
					<div class="iblock b-result-docinfo">
					<span class="num">3.</span>
						<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
						<a class="b-bookadd" data-collection="ajax_favs.html" href="#"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_27.jpg"></a>
						<div class="b-result-doclabel able">
							Доступно в библиотеке
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">4.</span>
						<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
						<a class="b-bookadd" data-collection="ajax_favs.html" href="#"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#"  class="b-sorcelibrary">Рязанская областная библиотека</a></div>

						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_4.jpg"></a>

					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">5.</span>
						<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
						<a class="b-bookadd" data-collection="ajax_favs.html" href="#"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#"  class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_27.jpg"></a>

					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">6.</span>
						<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
						<a class="b-bookadd" data-collection="ajax_favs.html" href="#"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							
						</div>
					</div>

				</div>
				
				<div class="b-paging">
					<div class="b-paging_cnt">
						<a href="" class="b-paging_prev iblock"></a>
						<a href="" class="b-paging_num current iblock">1</a>
						<a href="" class="b-paging_num iblock">2</a>
						<a href="" class="b-paging_num iblock">3</a>
						<a href="" class="b-paging_num iblock">4</a>
						<a href="" class="b-paging_next iblock"></a>
					</div>
				</div><!-- /.b-paging -->
			</div><!-- /.b-result-doc -->
		</div><!-- /.b-filter_list_wrapper -->
	</div><!-- /.b-mainblock -->
	<div class="b-side right">
		<div class="b-favside mtop">
			<div class="b-favside_img js_flexbackground">
				<img src="./pic/pic_32.png"  data-bgposition="50% 0" class="js_flex_bgimage" alt=""><!-- pic_32.png  logo_c.jpg-->
				<img src="./pic/pic_47.jpg" class="real " alt="">
			</div>
			<a href="#" class="b-favside_books">15 книг в подборке</a>
			<hr>
			<div class="b-lib_counter">
				<div class="b-lib_counter_lb">Добавленно в избранное</div>
				<div class="icofavs">х 108</div>
			</div>
			<div class="b-lib_counter">
				<div class="b-lib_counter_lb">Количество просмотров</div>
				<div class="icoviews">х 15 678</div>
			</div>
		</div>
		<div class="b-sidenav">
			<a href="#" class="b-sidenav_title">Авторы</a>
			
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">Лем С.</div>
						<div class="checkwrapper type2 right">
							<label for="cb92" class="black">40</label><input class="checkbox" type="checkbox" name="" id="cb92">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Големский Т. В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb82" class="black">23</label><input class="checkbox" type="checkbox" name="" id="cb82">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лемин В.В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb81" class="black">2</label><input class="checkbox" type="checkbox" name="" id="cb81">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лиманн А.Н.</div>
						<div class="checkwrapper type2 right">
							<label for="cb78" class="black">3</label><input class="checkbox" type="checkbox" name="" id="cb78">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Голем Л. К.</div>
						<div class="checkwrapper type2 right">
							<label for="cb67" class="black">15</label><input class="checkbox" type="checkbox" name="" id="cb67">
						</div>				
					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">дата</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">2000 - 2050</div>
						<div class="checkwrapper type2 right">
							<label for="cb99" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb99">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1950 - 2000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1900 - 1950</div>
						<div class="checkwrapper type2 right">
							<label for="cb88" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb88">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1850 - 1900</div>
						<div class="checkwrapper type2 right">
							<label for="cb77" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb77">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1800 - 1850</div>
						<div class="checkwrapper type2 right">
							<label for="cb66" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb66">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1750 - 1800</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1700 - 1750</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1650 - 1700</div>
						<div class="checkwrapper type2 right">
							<label for="cb14" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb14">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1600 - 1650</div>
						<div class="checkwrapper type2 right">
							<label for="cb0" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb0">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1550 - 1600</div>
						<div class="checkwrapper type2 right">
							<label for="cb1" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb1">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1400 - 1550</div>
						<div class="checkwrapper type2 right">
							<label for="cb33" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb33">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1350 - 1400</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1300 - 1350</div>
						<div class="checkwrapper type2 right">
							<label for="cb34" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb34">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1350 - 1300</div>
						<div class="checkwrapper type2 right">
							<label for="cb92" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb92">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1200 - 1350</div>
						<div class="checkwrapper type2 right">
							<label for="cb20" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb20">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1150 - 1200</div>
						<div class="checkwrapper type2 right">
							<label for="cb9" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb9">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1100 - 1150</div>
						<div class="checkwrapper type2 right">
							<label for="cb8" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb8">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1050 - 1100</div>
						<div class="checkwrapper type2 right">
							<label for="cb7" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb7">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1000 - 1050</div>
						<div class="checkwrapper type2 right">
							<label for="cb6" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb6">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">950 - 1000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li><a href="#" class="b_sidenav_contmore js_moreopen">+ следующие</a></li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">Формат издания</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">pdf (оригинал)</div>
						<div class="checkwrapper type2 right">
							<label for="cb5" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb5">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">текст</div>
						<div class="checkwrapper type2 right">
							<label for="cb22" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb22">
						</div>
					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">Коллекции</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">

					<li class="clearfix">
						<div class="b-sidenav_value left">Коллекции</div>
						<div class="checkwrapper type2 right">
							<label for="cb4" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb4">
						</div>

					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">тематика</a>
			<div class="b-sidenav_cont">	
				
				<div class="themesearch">
					<input type="text" class="b-sidenav_tb" placeholder="Поиск по тематике">
					<a href="#" class="b-sidenav_srch">Расширеный список</a>
				</div>
				<ul class="b-sidenav_cont_list b-sidenav_cont_listmore">
					<li class="clearfix">					
						<div class="b-sidenav_value left">Горное дело </div>
						<div class="checkwrapper type2 right">
							<label for="cb72" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb72">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Пищевые производства</div>
						<div class="checkwrapper type2 right">
							<label for="cb62" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb62">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Радиоэлектроника </div>
						<div class="checkwrapper type2 right">
							<label for="cb52" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb52">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Строительство </div>
						<div class="checkwrapper type2 right">
							<label for="cb42" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb42">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Техника и технические 
							науки в целом </div>
							<div class="checkwrapper type2 right">
								<label for="cb32" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb32">
							</div>
						</li>

						<li><a href="ajax_fake_theme.html" class="b_sidenav_contmore js_ajax_theme" >+ следующие</a></li>
					</ul>
				</div>
				
				
				<a href="#" class="b-sidenav_title">язык</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">язык</div>
							<div class="checkwrapper type2 right">
								<label for="cb12" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb12">
							</div>

						</li>
					</ul>
				</div>
				
			</div> <!-- /.b-sidenav --> 

		</div><!-- /.b-side -->

	</section>

</div><!-- /.homepage -->

<div class="b-mylibrary">
	<div class="wrapper">
		<div class="b-mylibrarytit">Моя библиотека</div>
		<ul class="b-mylibrarylist">
			<li>
				<a href="#">Сейчас  читаю</a> <span class="b-num">2</span>
			</li>
			<li><a href="#">В закладках</a><span class="b-num">14</span></li>
			<li><a href="#">Буду читать</a><span class="b-num">47</span></li>
		</ul>
	</div>
</div><!-- /.b-mylibrary -->
<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu">мои подборки</a>
	

	<form class="b-selectionadd" action="">
		<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
		<span class="b-selectionaddtxt">Cоздать подборку</span>
		<input type="text" class="input hidden">
	</form>
</div><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>