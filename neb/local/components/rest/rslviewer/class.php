<?php
CBitrixComponent::includeComponentClass("rest:rest.server");
CModule::IncludeModule('nota.userdata');
CModule::IncludeModule('nota.exalead');
use Nota\Exalead\RslViewer as RslViewer;


/**
 * Компонент организует API для прокидываия функций сервиса выдачи
 *
 */

class RestViewer extends RestServer
{

    private $viewer;


    /**
     *
     * В сервисе выдачи реализовано извлечение текста из картинки по координатам.
     *
     *  пример использования API:
     *
     *   /000199_000009_004612677/document/pages/1/txt/crop?coordinates={"x1":"0.1",%20"x2":"0.9",%20"y1":"0.1",%20"y2":"0.9"}
     *
     *  Параметр coordinates - координаты прямоугольника, обрезающего текст. Координаты указываются в процентах от верхнего левого угла страницы. (0.9 = 90%)
     *
     */

    public function getTextByCoordinates(){
        $this->viewer = new RslViewer();

        try {
            if(empty($_REQUEST['book_id'])) throw new \Exception(GetMessage('NO_REQUIRED', array('#PARAM#' => 'book_id')));
            if(empty($_REQUEST['page'])) throw new \Exception(GetMessage('NO_REQUIRED', array('#PARAM#' => 'page')));
            if(empty($_REQUEST['coordinates'])) throw new \Exception(GetMessage('NO_REQUIRED', array('#PARAM#' => 'coordinates')));

            $book_id     = $_REQUEST['book_id'];
            $page        = $_REQUEST['page'];
            $coordinates = $_REQUEST['coordinates'];

            $this->arResult = $this->viewer->setBookId($book_id)->getTextByCoordinates($book_id, $page, $coordinates);

        } catch( \Exception $e){

            return $this->restError(404, $e->getMessage());
        }

        return 200;
    }

}