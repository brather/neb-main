$(function(){

	$('.boockard-read-button').click(function (event) {
        event.preventDefault();
        $('.boockard-error-msg').hide();
        var serverError = $(this).data('server-error');
        readBook(event, this);
        setTimeout(function () {
            if ($('.boockard-read-button').data('load') != 'load') {
                $('.boockard-error-msg').text(serverError).show();
            }
        }, 5000);
    });
   	$('form.extendedSearch [data-name="newcollection[]"]').attr('multiple','true');

});