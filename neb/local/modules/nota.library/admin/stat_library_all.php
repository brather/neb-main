<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CModule::IncludeModule('iblock');
// библиотеки в НЭБ
$arSelect = Array("ID", "NAME", "SHOW_COUNTER");
$arFilter = Array("IBLOCK_ID" => IBLOCK_ID_LIBRARY, "ACTIVE" => "Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

while ($ob = $res->GetNextElement()) {

    $arFields = $ob->GetFields();
    $arLibs[$arFields['ID']]['NAME'] = $arFields['NAME'];
    $arLibs[$arFields['ID']]['CNT'] = $arFields['SHOW_COUNTER'];

}

// количество библиотек
$countLibs = count($arLibs);


$sTableID = 'tbl_stat';
$lAdmin_tab1 = new CAdminList($sTableID, $oSort_tab1);


$arHeader = array(
    array(
        'id' => 'NAME',
        'content' => 'Библиотека',
        'sort' => 'NAME',
        'default' => true
    ),
    array(
        'id' => 'CNT',
        'content' => 'Количество просмотров',
        'default' => true,
        'sort' => 'CNT',
    ),

);

$lAdmin_tab1->AddHeaders($arHeader);
$arSelect = $lAdmin_tab1->GetVisibleHeaderColumns();

$rsData = new CAdminResult($arLibs, $sTableID);

$rsData->NavStart();
$lAdmin_tab1->NavText($rsData->GetNavPrint(''));

while ($arRes = $rsData->NavNext()) {
    $row = $lAdmin_tab1->AddRow($str_ID, $arRes);

}


$lAdmin_tab1->AddAdminContextMenu(array());
$lAdmin_tab1->CheckListMode();
$APPLICATION->SetTitle('Статистика посещений страницы библиотеки');
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<div class="adm-detail-content-wrap">
    <div class="adm-detail-content">
        <table border="0" cellspacing="0" cellpadding="0" width="100%" class="list-table">
            <tr class="heading">
                <td>Общее количество библиотек в НЭБ</td>
                <td class="bx-digit-cell"><?= $countLibs ?></td>
            </tr>
        </table>
    </div>
    <br>
</div>
<? $lAdmin_tab1->DisplayList(); ?>
<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>
