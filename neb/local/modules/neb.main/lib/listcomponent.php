<?php
/**
 * User: agolodkov
 * Date: 05.08.2015
 * Time: 10:32
 */

namespace Neb\Main;

use Bitrix\Iblock\ElementTable;
use Exception;
use \Bitrix\Main\Entity;

abstract class ListComponent extends \CBitrixComponent
{
    /**
     * @var array
     */
    protected $_defaultParams
        = array(
            'listParams' => array(),
            'nav'        => array(),
        );

    /**
     * @var array
     */
    protected $_defaultResult = array();

    /**
     * @var Navigation
     */
    protected $_navigation;

    /**
     * @param null $component
     */
    public function __construct($component = null)
    {
        parent::__construct($component);
        $this->arResult = array_merge_recursive(
            $this->arResult,
            $this->_defaultResult
        );
    }

    public function executeComponent()
    {
        try {
            if (!\CModule::IncludeModule('iblock')) {
                throw new Exception('Not forund module "iblock"');
            }
            $this
                ->prepareParams()
                ->loadList()
                ->buildNav();

            $this->IncludeComponentTemplate();

            return;
        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

    /**
     * @param $iblock
     *
     * @return $this
     */
    protected function _prepareIblockListParams($iblock)
    {
        $this->arParams['listParams'] = array_merge_recursive(
            $this->arParams['listParams'],
            array(
                'filter' => array(
                    'IBLOCK_ID' => $iblock,
                    'ACTIVE'    => 'Y'
                ),
            )
        );

        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareNavigation()
    {
        $this->_navigation = new Navigation();
        $nav = $this->_navigation->getParameters();
        if (isset($this->arParams['PAGE_SIZE'])) {
            $nav['pageElementCount'] = $this->arParams['PAGE_SIZE'];
        }
        if (isset($this->arParams['NAV_NUM'])) {
            $nav['navNum'] = $this->arParams['NAV_NUM'];
        }
        $this->arParams['nav'] = $nav;

        /** параметры списка */
        $params = &$this->arParams['listParams'];
        if (!isset($params['select'])) {
            $params['select'] = array('*');
        }
        array_unshift(
            $params['select'],
            new Entity\ExpressionField(
                'FOUND_ROWS', 'SQL_CALC_FOUND_ROWS ID'
            )
        );
        if ($pageSize = @$nav['pageElementCount']) {
            $params['limit'] = $pageSize;
            $params['offset'] = $pageSize
                * ($this->arParams['nav']['navPageNumber'] - 1);
        }

        return $this;
    }

    /**
     * @param $params
     *
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params = array_replace_recursive($this->_defaultParams, $params);

        return $params;
    }

    /**
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     */
    public function loadElemets()
    {
        $this->_loadTable(new ElementTable());

        return $this;
    }

    /**
     * @param Entity\DataManager $table
     * @param array              $params
     *
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     */
    protected function _loadTable(Entity\DataManager $table, $params = [])
    {
        if (isset($this->arParams['nav'])) {
            $entityName = get_class($table);
            $entityName = substr($entityName, 0, strpos($entityName, 'Table'));
            $entityName = str_replace('\\', '', $entityName);
            $entityName = preg_replace('/[A-Z]/', '_$0', $entityName);
            $entityName = strtolower($entityName);
            $entityName = substr($entityName, 1);
            /** для нативного класса битрикс по другому формирует имена */
            if (0 === strpos($entityName, 'bitrix')) {
                $entityName = substr($entityName, strlen('bitrix_'));
            }

            array_unshift(
                $this->arParams['listParams']['select'],
                new Entity\ExpressionField(
                    'FOUND_ROWS',
                    'SQL_CALC_FOUND_ROWS ' . $entityName . '.ID'
                )
            );

        }

        $this->arResult['items'] = array();
        $rsResult = $table->getList(
            empty($params) ? $this->arParams['listParams'] : $params
        );
        while ($item = $rsResult->fetch()) {
            $this->arResult['items'][] = $item;
        }

        if ($this->_navigation instanceof Navigation) {
            $this->arParams['nav']['totalCount'] = (integer)$table
                ->getEntity()
                ->getConnection()
                ->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function buildNav()
    {
        if ($this->_navigation instanceof Navigation) {
            $this->_navigation->setParameters($this->arParams['nav']);
            $this->arResult['nav'] = (string)$this->_navigation;
        }

        return $this;
    }

    /**
     * @return $this;
     */
    abstract function loadList();

    /**
     * @return $this
     */
    abstract function prepareParams();


}