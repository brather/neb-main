<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!CModule::IncludeModule('nota.collection')) {
    echo CAdminMessage::ShowMessage(Loc::getMessage('NOTA_COLLECTION_ACCESS_DENIED'));
    return false;
}

use Nota\Collection\LibraryTable;

$aTabs = array();
$aTabs[] = array(
    "DIV" => "edit1",
    'TAB' => Loc::getMessage('NOTA_LIB_EDIT'),
    'TITLE' => Loc::getMessage('NOTA_LIB_EDIT')

);

$tabControl = new CAdminTabControl('tabControl', $aTabs);

$arErrors = array();

if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && check_bitrix_sessid()) {
    $arFields = Array();
    $arFields["NAME"] = $NAME;
    $arFields["SIGNATURE"] = $SIGNATURE;
    $arFields["POSTAL_ADDRESS"] = $POSTAL_ADDRESS;
    $arFields["SCHEDULE"] = $SCHEDULE;
    $arFields["PHONE"] = $PHONE;
    $arFields["EMAIL"] = $EMAIL;
    $arFields["SKYPE"] = $SKYPE;
    $arFields["URL"] = $URL;
    $arFields["DESCRIPTION"] = $DESCRIPTION;
    $arFields["MAP"] = $UF_LIBS_MAP;
    $arFields["SORT"] = $SORT;
    $arFields["CODE"] = empty($CODE) ? Cutil::translit($arFields["NAME"], "ru") : $CODE;

    if (!empty($arFields["NAME"])) {
        if (!empty($_FILES['LOGO']['tmp_name']) or ${"LOGO_del"} == 'Y') {
            $arLOGO = $_FILES['LOGO'];
            $arLOGO["del"] = ${"LOGO_del"};
            $arLOGO["old_file"] = ${"LOGO_old_file"};
            $arLOGO["MODULE_ID"] = "nota.collection";

            $arFields['LOGO'] = (int)CFile::SaveFile($arLOGO, 'library_logo');
        }
    }

    if (strlen($ID) > 0) {
        $res = LibraryTable::update($ID, $arFields);
    } else {
        $res = LibraryTable::add($arFields);
        $ID = $res->getId();
    }

    if (!$res->isSuccess())
        $errors = $res->getErrorMessages();
    else {
        if (strlen($apply) <= 0)
            LocalRedirect('collection_library.php');
        else
            LocalRedirect($APPLICATION->GetCurPage() . "?lang=" . $lang . "&ID=" . UrlEncode($ID) . "&" . $tabControl->ActiveTabParam());
    }

}

if (strlen($ID) > 0)
    $APPLICATION->SetTitle(GetMessage("NOTA_LIBRARY_EDIT", array('#ID#' => $ID)));
else
    $APPLICATION->SetTitle(GetMessage("NOTA_LIBRARY_NEW"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

ClearVars("str_");

if (intval($ID) > 0) {
    $result = LibraryTable::getById($ID);
    if ($arRow = $result->fetch())
        extract($arRow, EXTR_PREFIX_ALL, 'str');
} else
    $ID = '';

$aMenu = array(
    array(
        'TEXT' => Loc::getMessage('NOTA_LIBRARY_LIST'),
        'TITLE' => Loc::getMessage('NOTA_LIBRARY_LIST'),
        'LINK' => 'collection_library.php' . '?lang=' . LANGUAGE_ID,
        'ICON' => 'btn_list',
    )
);

if (strlen($ID) > 0) {
    $aMenu[] = array("SEPARATOR" => "Y");
    $aMenu[] = array(
        "TEXT" => GetMessage("NOTA_LIBRARY_NEW"),
        "TITLE" => GetMessage("NOTA_LIBRARY_NEW_TITLE"),
        "LINK" => "collection_library_edit.php?lang=" . LANGUAGE_ID,
        "ICON" => "btn_new"
    );
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

if (!empty($errors))
    CAdminMessage::ShowMessage(implode("\n", $errors));

$map = LibraryTable::getMap();

?>
    <form method="POST" id="form" name="form" action="<?= $APPLICATION->GetCurUri() ?>" enctype="multipart/form-data">
        <?= bitrix_sessid_post() ?>
        <input type="hidden" name="Update" value="Y">
        <input type="hidden" name="ID" value="<?= $ID ?>">
        <?
        $tabControl->Begin();
        $tabControl->BeginNextTab();

        if (strlen($ID) > 0) {
            ?>
            <tr>
                <td><?= $map['ID']['title'] ?>:</td>
                <td><?= $str_ID ?></td>
            </tr>
            <?
        }
        ?>
        <tr>
            <td><?= $map['NAME']['title'] ?>:</td>
            <td><input type="text" name="NAME" size="60" maxlength="250" value="<?= htmlspecialcharsbx($str_NAME) ?>">
            </td>
        </tr>
        <tr>
            <td><?= $map['SIGNATURE']['title'] ?>:</td>
            <td><input type="text" name="SIGNATURE" size="60" maxlength="250"
                       value="<?= htmlspecialcharsbx($str_SIGNATURE) ?>"></td>
        </tr>
        <tr>
            <td><?= $map['POSTAL_ADDRESS']['title'] ?>:</td>
            <td><input type="text" name="POSTAL_ADDRESS" size="60" maxlength="250"
                       value="<?= htmlspecialcharsbx($str_POSTAL_ADDRESS) ?>"></td>
        </tr>
        <tr>
            <td><?= $map['SCHEDULE']['title'] ?>:</td>
            <td><input type="text" name="SCHEDULE" size="60" maxlength="250"
                       value="<?= htmlspecialcharsbx($str_SCHEDULE) ?>"></td>
        </tr>
        <tr>
            <td><?= $map['PHONE']['title'] ?>:</td>
            <td><input type="text" name="PHONE" size="60" maxlength="250" value="<?= htmlspecialcharsbx($str_PHONE) ?>">
            </td>
        </tr>
        <tr>
            <td><?= $map['EMAIL']['title'] ?>:</td>
            <td><input type="text" name="EMAIL" size="40" maxlength="250" value="<?= htmlspecialcharsbx($str_EMAIL) ?>">
            </td>
        </tr>
        <tr>
            <td><?= $map['SKYPE']['title'] ?>:</td>
            <td><input type="text" name="SKYPE" size="40" maxlength="250" value="<?= htmlspecialcharsbx($str_SKYPE) ?>">
            </td>
        </tr>
        <tr>
            <td><?= $map['URL']['title'] ?>:</td>
            <td><input type="text" name="URL" size="40" maxlength="250" value="<?= htmlspecialcharsbx($str_URL) ?>">
            </td>
        </tr>
        <tr>
            <td><?= $map['DESCRIPTION']['title'] ?>:</td>
            <td><textarea name="DESCRIPTION"
                          style="width: 400px; height: 200px;"><?= htmlspecialcharsbx($str_DESCRIPTION) ?></textarea>
            </td>
        </tr>
        <?
        global $USER_FIELD_MANAGER;
        $entity_id = 'LIBS_MAP';
        $arUserFields = $USER_FIELD_MANAGER->GetUserFields($entity_id, $ID, LANGUAGE_ID);
        if (isset($str_MAP) and strpos($str_MAP, ',') !== false)
            $arUserFields['UF_LIBS_MAP']['VALUE'] = $str_MAP;
        echo $USER_FIELD_MANAGER->GetEditFormHTML(false, $GLOBALS['UF_LIBS_MAP'], $arUserFields['UF_LIBS_MAP']);
        ?>
        </tr>
        <tr>
            <td><?= $map['SORT']['title'] ?>:</td>
            <td><input type="text" name="SORT" size="5" maxlength="250" value="<?= htmlspecialcharsbx($str_SORT) ?>">
            </td>
        </tr>
        <tr>
            <td><?= $map['CODE']['title'] ?>:</td>
            <td><input type="text" name="CODE" size="50" maxlength="250" value="<?= htmlspecialcharsbx($str_CODE) ?>">
            </td>
        </tr>
        <tr>
            <td><?= $map['LOGO']['title'] ?>:</td>
            <td>
                <?
                echo CFileInput::Show(
                    "LOGO",
                    isset($str_LOGO) ? $str_LOGO : 0,
                    array(
                        "IMAGE" => "Y",
                        "PATH" => "Y",
                        "FILE_SIZE" => "Y",
                        "DIMENSIONS" => "Y",
                        "IMAGE_POPUP" => "Y",
                        "MAX_SIZE" => array(
                            "W" => 200,
                            "H" => 200
                        )
                    ),
                    array(
                        'upload' => true,
                        'medialib' => false,
                        'file_dialog' => false,
                        'cloud' => false,
                        'del' => true,
                        'description' => false
                    )
                );

                if (intval($str_LOGO) > 0) {
                    ?>
                    <input type="hidden" name="LOGO_old_file" value="<?= $str_LOGO ?>"/>
                    <?
                }
                ?>

            </td>
        </tr>
        <?
        $tabControl->Buttons(array("disabled" => false));
        $tabControl->End();
        ?>
    </form>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>