
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		
		<h2 class="mode">регистрация на портале</h2>
		<form action="" class="b-form b-form_common b-regform">
			<hr/>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings01">Фамилия</label>
					<div class="field validate">
						<input type="text" data-validate="fio" data-required="required" value="" id="settings01" data-maxlength="30" data-minlength="2" name="sname" class="input" autocomplete="off">
						<em class="error required">Поле обязательно для заполнения</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>							
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings02">Имя</label>
					<div class="field validate">
						<input type="text" data-required="required" data-validate="fio" value="" id="settings02" data-maxlength="30" data-minlength="2" name="name" class="input"  autocomplete="off">	
						<em class="error required">Поле обязательно для заполнения</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>												
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings22">Отчество</label>
					<div class="field validate">
						<input type="text" data-validate="fio" value="" id="settings22" data-maxlength="30" data-minlength="2" name="name" class="input" autocomplete="off" >
						<em class="error required">Поле обязательно для заполнения</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>													
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings21">Номер читательского билета РГБ</label>
					<div class="field validate">
						<input type="text" data-validate="number" value="" id="settings21" data-required="required" data-maxlength="30" data-minlength="2" name="sname" class="input">	
						<em class="error required">Заполните</em>
					</div>
				</div>
			</div>
			
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings05">Пароль</label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="required" value="" id="settings05" data-maxlength="30" name="pass" class="pass_status input" autocomplete="off">
						<em class="error required">Поле обязательно для заполнения</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings55">Подтвердить пароль</label>
					<div class="field validate">
						<input data-identity="#settings05" type="password" data-required="required" value="" id="settings55" data-maxlength="30" name="pass" class="input" data-validate="password" autocomplete="off">
						<em class="error identity ">Пароль не совпадает с введенным ранее</em>
						<em class="error required">Поле обязательно для заполнения</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
				</div>
			</div>
			<hr>
			<div class="nookresult">
				<p class="error">Данные по номеру читательского билета не найдены в РГБ.</p>
				<p><a href="blind_reg_full.php" class="black" title="Пройдите процедуру полной регистрации">Пройдите процедуру полной регистрации</a></p>
			</div>
		</form>
	</div><!-- /.b-registration-->



</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>