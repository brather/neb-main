<?
define("ADMIN_SECTION", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header("Content-Type: text/xml");
CModule::IncludeModule("iblock");

if ((int)$ID>0)
{
    $ID = (int)$ID;
}
else
{
    $ID = trim($ID);
}
$LANG = trim($_REQUEST["LANG"]);
$TYPE = trim($TYPE);
$LIMIT = (int)$LIMIT;

function GetRSS($ID, $LANG, $TYPE, $LIMIT_NUM = false, $LIMIT_DAY = false)
{
    echo "<"."?xml version=\"1.0\" encoding=\"".LANG_CHARSET."\"?".">\n";
    echo "<rss version=\"2.0\"";
    echo ">\n";

    $dbr = CIBlockType::GetList(array(), array(
        "=ID" => $TYPE,
    ));
    $arType = $dbr->Fetch();
    if ($arType && ($arType["IN_RSS"] == "Y"))
    {
        $dbr = CIBlock::GetList(array(), array(
            "type" => $TYPE,
            "LID" => $LANG,
            "ACTIVE" => "Y",
            "ID" => $ID,
        ));
        $arIBlock = $dbr->Fetch();
        if ($arIBlock && ($arIBlock["RSS_ACTIVE"] == "Y"))
        {
            echo GetRSSText($arIBlock, $LIMIT_NUM, $LIMIT_DAY);
        }
    }

    echo "</rss>\n";
}

function ExtractProperties($str, &$arProps, &$arItem)
{
    reset($arProps);
    while (list($key, $val) = each($arProps))
        $str = str_replace("#".$key."#", $val["VALUE"], $str);
    reset($arItem);
    while (list($key, $val) = each($arItem))
        $str = str_replace("#".$key."#", $val, $str);
    return $str;
}

function GetRSSText($arIBLOCK, $LIMIT_NUM = false, $LIMIT_DAY = false)
{
    global $DB;

    $strRes = "";

    $serverName = "";

    if (isset($arIBLOCK["SERVER_NAME"]) && strlen($arIBLOCK["SERVER_NAME"]) > 0)
        $serverName = $arIBLOCK["SERVER_NAME"];

    if (strlen($serverName) <=0 && !isset($arIBLOCK["SERVER_NAME"]))
    {
        $dbSite = CSite::GetList(($b="sort"), ($o="asc"), array("LID" => $arIBLOCK["LID"]));
        if ($arSite = $dbSite->Fetch())
            $serverName = $arSite["SERVER_NAME"];
    }

    if (strlen($serverName) <=0)
    {
        if (defined("SITE_SERVER_NAME") && strlen(SITE_SERVER_NAME)>0)
            $serverName = SITE_SERVER_NAME;
        else
            $serverName = COption::GetOptionString("main", "server_name", "www.bitrixsoft.com");
    }

    $strRes .= "<channel>\n";
    $strRes .= "<title>".htmlspecialcharsbx($arIBLOCK["NAME"])."</title>\n";
    $strRes .= "<link>http://".$serverName."</link>\n";
    $strRes .= "<description>".htmlspecialcharsbx($arIBLOCK["DESCRIPTION"])."</description>\n";
    $strRes .= "<lastBuildDate>".date("r")."</lastBuildDate>\n";
    $strRes .= "<ttl>".$arIBLOCK["RSS_TTL"]."</ttl>\n";

    $db_img_arr = CFile::GetFileArray($arIBLOCK["PICTURE"]);
    if ($db_img_arr)
    {
        if(substr($db_img_arr["SRC"], 0, 1) == "/")
            $strImage = "http://".$serverName.$db_img_arr["SRC"];
        else
            $strImage = $db_img_arr["SRC"];


            $strRes .= "<image>\n";
            $strRes .= "<title>".htmlspecialcharsbx($arIBLOCK["NAME"])."</title>\n";
            $strRes .= "<url>".$strImage."</url>\n";
            $strRes .= "<link>http://".$serverName."</link>\n";
            $strRes .= "<width>".$db_img_arr["WIDTH"]."</width>\n";
            $strRes .= "<height>".$db_img_arr["HEIGHT"]."</height>\n";
            $strRes .= "</image>\n";

    }

    $arNodes = array();
    $db_res = $DB->Query("SELECT NODE, NODE_VALUE FROM b_iblock_rss WHERE IBLOCK_ID = ".IntVal($arIBLOCK["ID"]));
    while ($db_res_arr = $db_res->Fetch())
    {
        $arNodes[$db_res_arr["NODE"]] = $db_res_arr["NODE_VALUE"];
    }

    CTimeZone::Disable();

    $strSql =
        "SELECT DISTINCT BE.*, ".
        "	".$DB->DateToCharFunction("BE.TIMESTAMP_X")." as TIMESTAMP_X, ".
        "	".$DB->DateToCharFunction("BE.ACTIVE_FROM", "FULL")." as ACTIVE_FROM, ".
        "	".$DB->DateToCharFunction("BE.ACTIVE_TO", "FULL")." as ACTIVE_TO, ".
        "	L.DIR as LANG_DIR, B.DETAIL_PAGE_URL, B.LIST_PAGE_URL, B.LID, L.SERVER_NAME ".
        "FROM b_iblock_element BE, b_lang L, b_iblock B ".
        "	LEFT JOIN b_iblock_group IBG ON IBG.IBLOCK_ID=B.ID ".
        "WHERE BE.IBLOCK_ID = B.ID ".
        "	AND B.LID = L.LID ".
        "	AND IBG.GROUP_ID IN (2) ".
        "	AND BE.WF_STATUS_ID = 1 AND BE.WF_PARENT_ELEMENT_ID is null ".
        "	AND IBG.PERMISSION>='R' ".
        "	AND (IBG.PERMISSION='X' OR B.ACTIVE='Y') ".
        "	AND (BE.IBLOCK_ID = ".IntVal($arIBLOCK["ID"]).") ".
        "	AND ((BE.ACTIVE_TO >= ".$DB->GetNowFunction()." OR BE.ACTIVE_TO IS NULL) AND (BE.ACTIVE_FROM <= ".$DB->GetNowFunction()." OR BE.ACTIVE_FROM IS NULL)) ".
        "	AND (BE.ACTIVE = 'Y') ";
    if ($LIMIT_DAY!==false)
        $strSql .= " AND (BE.ACTIVE_FROM>=".$DB->CharToDateFunction($DB->ForSql(date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")), mktime(date("H"), date("i"), date("s"), date("m"), date("d")-IntVal($LIMIT_DAY), date("Y")))), "FULL")." OR BE.ACTIVE_FROM IS NULL) ";
    $strSql .= "ORDER BY BE.ACTIVE_FROM DESC, BE.SORT ASC ";

    $res = $DB->Query($strSql);

    CTimeZone::Enable();

    $items = new CIBlockResult($res->result);
    if ($LIMIT_NUM!==False && IntVal($LIMIT_NUM)>0)
        $items->NavStart($LIMIT_NUM);

    while ($arItem = $items->GetNext())
    {
        $props = CIBlockElement::GetProperty($arIBLOCK["ID"], $arItem["ID"], "sort", "asc", Array("ACTIVE"=>"Y", "NON_EMPTY"=>"Y"));
        $arProps = Array();
        while ($arProp = $props->Fetch())
        {
            if (strlen($arProp["CODE"])>0)
                $arProps[$arProp["CODE"]] = Array("NAME"=>htmlspecialcharsbx($arProp["NAME"]), "VALUE"=>htmlspecialcharsex($arProp["VALUE"]));
            else
                $arProps[$arProp["ID"]] = Array("NAME"=>htmlspecialcharsbx($arProp["NAME"]), "VALUE"=>htmlspecialcharsex($arProp["VALUE"]));
        }

        $arLinkProp = $arProps["DOC_LINK"];

        $strRes .= "<item>\n";
        if (strlen($arNodes["title"])>0)
        {
            $strRes .= "<title>".htmlspecialcharsbx(ExtractProperties($arNodes["title"], $arProps, $arItem))."</title>\n";
        }
        else
        {
            $strRes .= "<title>".htmlspecialcharsbx($arItem["~NAME"])."</title>\n";
        }
        if (strlen($arNodes["link"])>0)
        {
            $strRes .= "<link>".ExtractProperties($arNodes["link"], $arProps, $arItem)."</link>\n";
        }
        else
        {
            $strRes .= "<link>http://".$serverName.(($arLinkProp["VALUE"]) ? $arLinkProp["VALUE"] : $arItem["DETAIL_PAGE_URL"])."</link>\n";
        }
        if (strlen($arNodes["description"])>0)
        {
            $desc = preg_replace_callback(
            /* заменить все относительне пути к изображениям на абсолютные */
                '/(<img)(.+)?(src=)"(.+)"/',
                function ( $m ){
                    return $m[1] . $m[2] . $m[3] . '"http://' . $_SERVER['HTTP_HOST'] . $m[4] . '"';
                },
                ExtractProperties($arNodes["description"], $arProps, $arItem));

            $desc = preg_replace_callback(
                '/(<img)(.+)?(width="[\d]+")/',
                function ( $m ){
                    return $m[1] . $m[2];
                },
                $desc);

            $desc = preg_replace_callback(
                '/(<img)(.+)?(height="[\d]+")/',
                function ( $m ){
                    return $m[1] . $m[2];
                },
                $desc);

            $desc .= '<style>img {max-width: 100%;}</style>';

            $strRes .= "<description>".htmlspecialcharsbx($desc)."</description>\n";
        }
        else
        {
            $desc = preg_replace_callback(
                    /* заменить все относительне пути к изображениям на абсолютные */
                    '/(<img)(.+)?(src=)"(.+)"/',
                    function ( $m ){
                        return $m[1] . $m[2] . $m[3] . '"http://' . $_SERVER['HTTP_HOST'] . $m[4] . '"';
                    },
                    (($arItem["PREVIEW_TEXT"]) ? $arItem["PREVIEW_TEXT"] : $arItem["DETAIL_TEXT"]));

            $desc = preg_replace_callback(
                '/(<img)(.+)?(width="[\d]+")/',
                function ( $m ){
                    return $m[1] . $m[2];
                },
                $desc);

            $desc = preg_replace_callback(
                '/(<img)(.+)?(height="[\d]+")/',
                function ( $m ){
                    return $m[1] . $m[2];
                },
                $desc);

            $desc .= '<style>img {max-width: 100%;}</style>';

            $strRes .= "<description>". htmlspecialcharsbx($desc)."</description>\n";
        }
        if (strlen($arNodes["enclosure"])>0)
        {
            $strRes .= "<enclosure url=\"".htmlspecialcharsbx(ExtractProperties($arNodes["enclosure"], $arProps, $arItem))."\" length=\"".htmlspecialcharsbx(ExtractProperties($arNodes["enclosure_length"], $arProps, $arItem))."\" type=\"".htmlspecialcharsbx(ExtractProperties($arNodes["enclosure_type"], $arProps, $arItem))."\"/>\n";
        }
        else
        {
            $db_img_arr = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
            if ($db_img_arr)
            {
                if(substr($db_img_arr["SRC"], 0, 1) == "/")
                    $strImage = "http://".$serverName.$db_img_arr["SRC"];
                else
                    $strImage = $db_img_arr["SRC"];

                $strRes .= "<enclosure url=\"".$strImage."\" length=\"".$db_img_arr["FILE_SIZE"]."\" type=\"".$db_img_arr["CONTENT_TYPE"]."\" width=\"".$db_img_arr["WIDTH"]."\" height=\"".$db_img_arr["HEIGHT"]."\"/>\n";
            }
        }
        if (strlen($arNodes["category"])>0)
        {
            $strRes .= "<category>".htmlspecialcharsbx(ExtractProperties($arNodes["category"], $arProps, $arItem))."</category>\n";
        }
        else
        {
            $strPath = "";
            $nav = CIBlockSection::GetNavChain($arIBLOCK["ID"], $arItem["IBLOCK_SECTION_ID"]);
            while($ar_nav = $nav->GetNext())
            {
                $strPath .= $ar_nav["NAME"]."/";
            }
            if (strlen($strPath)>0)
            {
                $strRes .= "<category>".htmlspecialcharsbx($strPath)."</category>\n";
            }
        }
        if (strlen($arNodes["pubDate"])>0)
        {
            $strRes .= "<pubDate>".htmlspecialcharsbx(ExtractProperties($arNodes["pubDate"], $arProps, $arItem))."</pubDate>\n";
        }
        else
        {
            if (strlen($arItem["ACTIVE_FROM"])>0)
            {
                $strRes .= "<pubDate>".date("r", MkDateTime($DB->FormatDate($arItem["ACTIVE_FROM"], Clang::GetDateFormat("FULL"), "DD.MM.YYYY H:I:S"), "d.m.Y H:i:s"))."</pubDate>\n";
            }
            else
            {
                $strRes .= "<pubDate>".date("r")."</pubDate>\n";
            }
        }
        $strRes .= "</item>\n";
    }
    $strRes .= "</channel>\n";
    return $strRes;
}

GetRSS($ID, $LANG, $TYPE, $LIMIT, false);
