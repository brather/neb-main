<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

require_once('../../../functions.php');
require_once('../../PushAPI.inc');
require_once('../../example/blog-style/tick.inc');

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(E_ERROR);

\CModule::IncludeModule("main");
\CModule::IncludeModule("highloadblock");
\CModule::IncludeModule("nota.exalead");

use Nota\Exalead\BiblioCardTable;
use Nota\Exalead\BiblioAlisTable;
use Nota\Exalead\BiblioCardBxTable;

$exaleadOptions = Array(
    "KEY" => \COption::GetOptionString("nota.exalead", "book_add_key"),
    //"URL" => \COption::GetOptionString("nota.exalead", "book_add_url"),
    "URL" => 'http://neb.o.orestov.dev.notamedia.ru/_exalead_book_add/',
    //"IP" => \COption::GetOptionString("nota.exalead", "exalead_ip"),
    "IP" => '213.208.168.8',
    //"PORT" => \COption::GetOptionString("nota.exalead", "exalead_port"),
    "PORT" => 10002,
    "CONNECTOR" => 'test'
);

$LIB_ID = (int)$_REQUEST['LIB_ID'];
$LIB_NAME = htmlspecialcharsbx($_REQUEST['LIB_NAME']);
$BX_CARD_ID = (int)$_REQUEST['Id'];

if($_REQUEST["KEY"] != $exaleadOptions["KEY"]){
    CEventLog::Add(array(
        "SEVERITY" => "ERROR",
        "AUDIT_TYPE_ID" => "EXALEAD_ADD",
        "MODULE_ID" => "nota.exalead",
        "ITEM_ID" => $BX_CARD_ID,
        "DESCRIPTION" => "Указан не правильный ключ добавления книги",
    ));
    die("Указан не правильный ключ добавления книги");
}

if(!$LIB_ID || !$LIB_NAME || !$BX_CARD_ID){
    CEventLog::Add(array(
        "SEVERITY" => "ERROR",
        "AUDIT_TYPE_ID" => "EXALEAD_ADD",
        "MODULE_ID" => "nota.exalead",
        "ITEM_ID" => $BX_CARD_ID,
        "DESCRIPTION" => "Не указано значение библиотеки или книги при вызове скрипта добавления книги",
    ));
    die("Не указано значение библиотеки или книги при вызове скрипта добавления книги");
}

//библиотека существует?
$row = BiblioAlisTable::getRow(
    Array(
        'select' => Array('Id'),
        'filter' => array('Library' => $LIB_ID)
    )
);

//TODO: создать библиотеку
/*$ALIS_ID = false;
//создадим библиотеку, если она не существует
if($row){
    $ALIS_ID = $row["Id"];
} else {
    $result = BiblioAlisTable::add(
        Array(
            'Name' => $LIB_NAME,
            'Library' => $LIB_ID,
        )
    );

    if ($result->isSuccess())
    {
        $ALIS_ID = $result->getId();
    }
}

if(!$ALIS_ID){
    CEventLog::Add(array(
        "SEVERITY" => "ERROR",
        "AUDIT_TYPE_ID" => "EXALEAD_ADD",
        "MODULE_ID" => "nota.exalead",
        "ITEM_ID" => $BX_CARD_ID,
        "DESCRIPTION" => "Не указано значение ALIS ID при вызове скрипта добавления книги",
    ));
    die("Не указано значение ALIS ID при вызове скрипта добавления книги");
}*/

$arFields = $_REQUEST;
array_walk($arFields, 'htmlspecialcharbx');

//pdf существует?
if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $arFields["pdfLink"])){
    CEventLog::Add(array(
        "SEVERITY" => "ERROR",
        "AUDIT_TYPE_ID" => "EXALEAD_ADD",
        "MODULE_ID" => "nota.exalead",
        "ITEM_ID" => $BX_CARD_ID,
        "DESCRIPTION" => "PDF файл книги " . $arFields["pdfLink"] . " не существует",
    ));
    die("PDF файл книги " . $arFields["pdfLink"] . " не существует");
}

//TODO: таблица связей битрикс <-> экзалида. нужна ли она?

//получение текста из пдф
$filePathTxt = basename($arFields["pdfLink"], ".pdf") . ".txt";
$filePathTxt = $_SERVER['DOCUMENT_ROOT'] . '/_exalead_book_add/files/' . $filePathTxt;

exec('pdftotext -enc UTF-8 ' . $_SERVER['DOCUMENT_ROOT'] . $arFields["pdfLink"] . ' ' . $filePathTxt);

chmod($filePathTxt, 0775);
if(file_exists($filePathTxt)){
    $pdfContent = file_get_contents($filePathTxt);
    unlink($filePathTxt);
}

//$pdfContent = substr($pdfContent, 0, 5000);

//подготовка мета-данных экзалиды
$row = BiblioCardTable::getRow(
    Array(
        'filter' => array('IdFromALIS' => $arFields["IdFromALIS"])
    )
);

if(!$row){
    CEventLog::Add(array(
        "SEVERITY" => "ERROR",
        "AUDIT_TYPE_ID" => "EXALEAD_ADD",
        "MODULE_ID" => "nota.exalead",
        "ITEM_ID" => $BX_CARD_ID,
        "DESCRIPTION" => "Не удалось получить информацию о книге из БД",
    ));
    die("Не удалось получить информацию о книге из БД");
}

$arYearRange = Array(
    'century' => (int)substr($row['PublishYear'], 0, 2) + 1,
    'range' => floor((int)substr($row['PublishYear'], 2, 2) / 10) * 10 . '-' . ceil((int)substr($row['PublishYear'], 2, 2) / 10) * 10,
    'year' => $row['PublishYear']
);

$exaleadFields = Array(
    'authorbook' => $row['Author'],
    'title' => implode(' ', Array($row['Name'], $row['SubName'], $row['VolumeName'], $row['VolumeNumber'])),
    'publishyear' => $row['PublishYear'],
    'publishyearint' => $row['PublishYear'],
    'yearrange' => implode('/', $arYearRange),
    'publisher' => $row['Publisher'],
    'publishplace' => $row['PublishPlace'],
    'isbn' => $row['ISBN'],
    'lang' => $row['Language'],
    'series' => $row['Series'],
    'notes' => implode(' ', Array($row['ContentRemark'], $row['CommonRemark'])),
);

//$pdfContent = "тест2тест";

try {
    $papi = PushAPIFactory::createHttp($exaleadOptions["IP"], $exaleadOptions["PORT"], $exaleadOptions["CONNECTOR"]);
    $papi->ping();

    $part = new Part(
        array(
            'content' => $pdfContent,
            'directives' =>	array(
                'mimeHint' => 'text/html',
                'encoding' => 'utf-8'
            )
        )
    );

    $document = new Document(
        array(
            'uri' 	=> $exaleadStamp,
            'parts' => $part,
            'metas' => $exaleadFields,
            'stamp' => $arFields["IdFromALIS"]
        )
    );

    //pre($document, true);

    $papi->addDocument(array($document));
    $serial = $papi->setCheckpoint(0, true);

    $papi->enumerateCheckpointInfo();

    $papi->enumerateSyncedEntries('http://example.com/', PushAPI::RECURSIVE_DOCUMENTS);

    $papi->triggerIndexingJob();

    $papi->ping();
    $papi->close();

    //установим BX_IS_UPLOAD = 1
    $result = BiblioCardTable::update($BX_CARD_ID, array(
        'BX_IS_UPLOAD' => 1
    ));
    if(!$result->isSuccess()){
        CEventLog::Add(array(
            "SEVERITY" => "ERROR",
            "AUDIT_TYPE_ID" => "EXALEAD_ADD",
            "MODULE_ID" => "nota.exalead",
            "ITEM_ID" => $BX_CARD_ID,
            "DESCRIPTION" => "Ошибка при обновлении статуса книги " . $bookID,
        ));
    }

    CEventLog::Add(array(
        "SEVERITY" => "INFO",
        "AUDIT_TYPE_ID" => "EXALEAD_ADD",
        "MODULE_ID" => "nota.exalead",
        "ITEM_ID" => $BX_CARD_ID,
        "DESCRIPTION" => "Добавление книги " . $row['Name'] . " успешно завершено.",
    ));

} catch(PushAPIFactory $e)  {
    //echo "Error: " . $e->getMessage() . "\n";
    CEventLog::Add(array(
        "SEVERITY" => "ERROR",
        "AUDIT_TYPE_ID" => "EXALEAD_ADD",
        "MODULE_ID" => "nota.exalead",
        "ITEM_ID" => $BX_CARD_ID,
        "DESCRIPTION" => $e->getMessage(),
    ));
}

