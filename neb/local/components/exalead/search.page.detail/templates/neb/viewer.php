<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

global $APPLICATION, $USER;

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arResult['BOOK']['URL'] = urldecode($arResult['VARIABLES']['BOOK_ID']);

?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>" <? require $_SERVER["DOCUMENT_ROOT"].'/local/include_areas/open_graph_prefix.php';?>>
	<head>
		<title><? $APPLICATION->ShowTitle() ?> — просмотр онлайн — НЭБ.РФ</title>
		<meta charset="<?=SITE_CHARSET?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="userfront"
            <? if (in_array(UGROUP_MARKUP_APPROVER, $arResult['USER_GROUPS'])): ?>data-markup-approver<? endif ?>
            <? if (in_array(UGROUP_MARKUP_EDITOR, $arResult['USER_GROUPS'])): ?> data-markup-editor<? endif ?>
        />

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta name="msapplication-config" content="/browserconfig.xml" />
        <meta name="msapplication-TileImage" content="/local/templates/adaptive/favicon/ms-icon-144x144.png" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="theme-color" content="#ffffff" />
		
        <?if (!MINIFY_JS_CSS_MODE || MARKUP_DEBUG_MODE) { ?>
			<!--link href="<?=MARKUP?>css/style.css" type="text/css"  data-template-style="true"  rel="stylesheet" /-->
			<!-- он же -->
			<!--link rel="stylesheet" href="/local/templates/.default/markup/css/style.css"/-->

			<!-- Bootstrap -->
			<link href="/local/templates/adaptive/css/bootstrap.min.css" rel="stylesheet" />
			<link rel="stylesheet" href="/local/components/exalead/search.page.detail/templates/.default/styles.css" type="text/css" media="screen" />
			<!--link rel="stylesheet" href="/local/components/exalead/search.page.detail/templates/.default/js/jquery.arcticmodal-0.3.css" />
			<link rel="stylesheet" href="/local/components/exalead/search.page.detail/templates/.default/js/themes/simple.css" /-->

			<!-- Fonts -->
			<!--link href='//fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic,800,800italic,300,300italic&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'-->
			<!--link rel="stylesheet" href="/local/templates/adaptive/css/font-awesome.min.css" /-->
			<!-- jquery ui -->
			<!--link rel="stylesheet" href="/local/templates/adaptive/vendor/jquery-ui/jquery-ui.css" /-->
			<!-- Important Owl stylesheet -->
			<!-- <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
	        <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css"> -->

			<!--link rel="stylesheet" type="text/css" href="/local/templates/adaptive/vendor/custom-select/bootstrap-select.min.css" /-->

			<!--link href="/local/templates/adaptive/css/style.css" rel="stylesheet"-->

			<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			<!--[if IE]>
				<style>
					.page-book > .right {right:20px !important;}
				</style>
			<![endif]-->
			<script src="/local/templates/adaptive/js/jquery/prejquery.js" type="text/javascript"></script>
		<? } else { ?>
			<link href="/local/templates/adaptive/css/viewer.min.css" rel="stylesheet" />
			<!--[if lt IE 9]>
			<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js" type="text/javascript"></script>
			<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js" type="text/javascript"></script>
			<![endif]-->
			<!--[if IE]>
				<style>
					.page-book > .right {right:20px !important;}
				</style>
			<![endif]-->
			<script src="/local/templates/adaptive/js/jquery/prejquery.js" type="text/javascript"></script>
		<? };?>

		<meta name="book-library-id" content="<?= $arResult['BOOK']['LIBRARY_ID'] ?>" />

        <? $APPLICATION->ShowHead(); // требуется для popup-окон при авторизации через социалки?>

        <? require $_SERVER["DOCUMENT_ROOT"].'/local/include_areas/open_graph.php'; ?>
    </head>
	<body>
    <? $APPLICATION->ShowPanel() ?>
    <?/*<!--<div class="viewer-header">
		<img src="" alt="" class="viewer-header__logo">
		<div class="viewer-header__title">
			<p>Пушкин Александр Сергеевич</p>
			<h2>Сказка о золотом петушке</h2>
		</div>
		<div class="viewer-header__controls">
			<ul>
				<li><a href="#">Описание книги</a></li>
				<li><a href="#">Добавить в избранное</a></li>
				<li><a href="#">Сохранить книгу</a></li>
			</ul>
		</div>
	</div>-->

	<!--div class="blind-main-app" role="application">
		<a href="#blindpagetext" id="textlabel" aria-haspopup="true" aria-expanded="false">Текст страницы</a>
		<textarea id="blindpagetext" aria-labelledby="textlabel">вот текст</textarea>
	</div>
	<style>
		.blind-main-app {}
		#blindpagetext {display: none;}
	</style>
	<script>
		$(function(){
			$(document).on('click','#textlabel',function(e){
				$('#blindpagetext').show();
				$(this).attr('aria-expanded','true');
			});
		});
	</script-->*/?>

	<div class="page-book">
	<div class=""></div>
	<span class="turn-page">Свернуть экран</span>

		<!-- main -->
		<div class="main" style="padding-bottom:0;">
			<div class="view-content">
				<div class="book-title">
					<a href="/" class="book-title-logo" aria-hidden="true"></a>
					<h1 class="book-title-name" title="Название издания"><span class="sr-only">Название издания</span><?=$arResult['BOOK']['title']?></h1>
					<h2 class="book-title-author" title="Автор издания">
						<?if($arResult['BOOK']['authorbook']):?><?=$arResult['BOOK']['authorbook']?><?endif;?>
						<?/* if (in_array(UGROUP_MARKUP_APPROVER, $arResult['USER_GROUPS'])): ?>data-markup-approver<? endif ?><? if (in_array(UGROUP_MARKUP_EDITOR, $arResult['USER_GROUPS'])): ?> data-markup-editor<? endif */?>
					</h2>
				</div>
				<div class="view-page">



					<?/*details>
					    <summary><?=$arResult['USER_GROUPS']?></summary>
					    <pre><?php print_r($arResult);?></pre>
					</details*/?>

					<? if($arResult['BOOK']['pageCount'] > 1): ?>
						<button class="prev-page" title="Предыдущая страница"></button>
					<? endif; ?>
					<? if($arResult['BOOK']['pageCount'] > 1): ?>
						<button class="next-page" title="Следующая страница"></button>
					<? endif; ?>
					<a href="#" class="add-bookmark" title="Добавить страницу в закладки"></a>
					<div class="viewer">
						<div class='viewer_img'>
							<img id='image' class='zoomer-image' data-view-port="switched-on" data-view-name="simple-image">
						</div>
					</div>
					<div class="preloader"></div>
					<div class="quote-book">
						<div class="quote-icon"></div>
						<div class="quote-book-content"></div>
					</div>
					<div class="note-book">
						<div class="note-icon"></div>
						<div class="note-book-content"></div>
					</div>
					<div id='selectedrect' style='display:none'>
						<div id="selectedrect-wrapper" class="selectedrect-wrapper">
						</div>
						<div id='selected_rect_menu'>
							<a href='#' class="selected-increase">Увеличить фрагмент</a>
							<a href='#' class="selected-reduce selected-reduce--disabled">Уменьшить фрагмент</a>
							<a href='#' id='b-textLayer_quotes'>Добавить в цитаты</a>
							<a href='#' id='b-textLayer_notes'>Добавить в заметки</a>
						</div>
						<div class='layer_notes' style='display:none;'>
							<textarea id='text_note'></textarea>
							<input type='button' value='Добавить' id='add_note'>
						</div>
						<div class="crop-text-block">
							<p>Выделенный текст</p>
							<input type="text" class="crop-text">
							<div class="search_preloader"></div>
						</div>
					</div>
					
					<div class="text-view-wrapper" data-view-port data-view-name="display-text">
						<div class="text-view"></div>
					</div>		
				</div>
				<input type='hidden' id="page-number" value='<?=(intval($_REQUEST['page'])>0?intval($_REQUEST['page']):1)?>' />
				<input type='hidden' id="pagenumber" value='<?=(intval($_REQUEST['page'])>0?intval($_REQUEST['page']):1)?>' />
				<input type='hidden' id="book_id" value='<?=$arResult['BOOK']['URL']?>' />
				<input type='hidden' id="count_page" value='<?=$arResult['BOOK']['pageCount']?>' />
				<input type='hidden' id="positionpart" value='<?=$_REQUEST['positionpart']?>' />
			</div>
			<?if($arResult['BOOK']['pageCount'] > 1) {?>
				<div class="navigation" style="bottom: -165px;">
					<?/*
					<ul class="clearfix">
						<li><span class="first fa fa-angle-double-left" title="В начало" p=1></span></li>
						<li><span class="prev fa fa-angle-left" title="Назад"></span></li>
						<li><span class='page_1'></span></li>
						<li><span class='page_2'></span></li>
						<li><span class="page_3"></span></li>
						<li>...</li>
						<li><span p=<?=$arResult['BOOK']['pageCount']?>><?=$arResult['BOOK']['pageCount']?></span></li>
						<li><span class="next fa fa-angle-right" title="Вперёд"></span></li>
						<li><span class="last fa fa-angle-double-right" title="В конец" p=<?=$arResult['BOOK']['pageCount']?>></span></li>
					</ul>
					<span class="toggle-page-navigation"></span>
					*/?>
					<div class="flaviusmatis" aria-hidden="true"></div>
					<div class="page-navigation">
						<label>Переход к странице</label>
						<input type="text" class='page'>
						<a class="go" href="#"><span class="sr-only">Перейти к указанной странице</span></a>
					</div>
				</div>
				
			<?}?>
			
		</div>

		<!-- left -->
		<div class="left">

			<div class="left-tools">
				<ul class="ullefttools">
					<li><a href="#" class="bookmark-tools" title="Добавить страницу в закладки">В закладки</a></li>
					<li><a href="#" class="toggle-view image-now" title="Переключить вид">Показывать как текст</a></li>
					<li aria-hidden="true"><span class="superscale-page no-user-select" title="Смотреть страницу во весь экран" data-view-fullscreen>Во весь экран</span></li>
					<li aria-hidden="true" class="sub rotatetool">
						<span class="rotate" title="Поворот страницы">Повернуть</span>
						<ul class="rotateleftrightclass">
							<li><span class="rotate-left" title="Повернуть налево"></span></li>
							<li><span class="rotate-right" title="Повернуть направо"></span></li>
						</ul>
					</li>
					<li><a href="#" class="save" title="Сохранить изображение страницы">Сохранить страницу</a></li>
					<li><span class="question"><a href='/static/docs/neb_user_guide.pdf'>Руководство пользователя <span class="sr-only">формат PDF</span></a></span></li>
				</ul>
			</div>
		</div>
		<!-- end left -->
		
		<!-- right -->
		<div class="right">
			<div class="top-right-tools clearfix">
				<ul class="clearfix">
					<!--<li><span class="toggle-right-tools"></span></li>-->
					<li><a href="#editiondescription" aria-live="polite" class="properties-book" title="Описание книги" aria-haspopup="true" aria-expanded="false" id="editiondesctoggler">Описание книги</a></li>
					<!--<li><span class="search-book">Поиск по книге</span></li>-->
					<li><a href="#" class="favorite-book<?if(!empty($arResult['USER_BOOKS'])):?> active<?endif;?>" title="<?if(!empty($arResult['USER_BOOKS'])):?>В личном кабинете<?else:?>Добавить в личный кабинет<?endif;?>" data-collection="/local/tools/collections/list.php?t=books&id=<?=$arResult['BOOK']['URL']?>" data-url="/local/tools/collections/removeBook.php?id=<?=$arResult['BOOK']['URL']?>"><?if(!empty($arResult['USER_BOOKS'])):?>Удалить из избранного<?else:?>Добавить в избранное<?endif;?></a></li>
					<li>
						<a class="save-book-link allowed" data-save-edition title="Сохранить книгу" href='/local/tools/exalead/getFiles.php?book_id=<?=$arResult['BOOK']['URL']?>&name=<?=$arResult['BOOK']['title']?>&library_id=<?=$arResult['BOOK']['LIBRARY_ID']?>'>
							<span class="save-book" title="Сохранить книгу">Сохранить книгу</span>
						</a>
					</li>
				</ul>
			</div>
			<div class="hidden" data-markedup-edition-js-template>
		
				<div data-chapter-item data-status="" data-bitrix=''>
					<a title="">
						<span p="" class="title-anchor"></span>

						<? if (in_array(UGROUP_MARKUP_APPROVER, $arResult['USER_GROUPS'])): ?>
							<span class="approve" data-change-status-chapter class="data-change-status" title="Подтвердить произведение" data-confirm-title="Подтвердить"
							data-info='<?echo json_encode(array_merge($arFields, ['status_code' => 'confirm']))?>'></span>
						<? endif; ?>
						<? if (in_array(UGROUP_MARKUP_APPROVER, $arResult['USER_GROUPS'])): ?>
							<span class="decline" data-change-status-chapter class="data-change-status" title="Отклонить произведение" data-confirm-title="Отклонить"
							data-info='<?echo json_encode(array_merge($arFields, ['status_code' => 'decline']))?>'></span>
						<? endif; ?>

						<? if ( in_array(UGROUP_MARKUP_APPROVER, $arResult['USER_GROUPS']) || in_array(UGROUP_MARKUP_EDITOR, $arResult['USER_GROUPS']) ): ?>
							<span class='edit' data-edit-chapter title='Редактировать произведение' data-info=''></span>
						<? endif; ?>
						<? if ( in_array(UGROUP_MARKUP_APPROVER, $arResult['USER_GROUPS']) || in_array(UGROUP_MARKUP_EDITOR, $arResult['USER_GROUPS']) ): ?>
							<span class='change-status-btn' data-change-status-chapter title='Удалить произведение' data-confirm-title='Удалить' data-info=''></span>
						<? endif; ?>
					</a>
				</div>

				<? if (in_array(UGROUP_MARKUP_APPROVER, $arResult['USER_GROUPS'])): ?>
					<div class="right-tools-kind" data-confirmall-template>
						<span data-change-status-chapter class="right-tools-label change-status-btn all-approve-btn" title="Подтвердить все"
							  data-info='<?=json_encode([
								  'book_id'     => $arResult['BOOK']['id'],
								  'info'        => $arResult['BOOK']['title'],
								  'status_code' => 'confirm',
								  'change'      => 'подтвердить все произведения издания',
							  ])?>'>Подтвердить все</span>
					</div>

					<div class="right-tools-kind" data-declineall-template>
						<span data-change-status-chapter data-confirm-title="Отклонить" class="right-tools-label change-status-btn all-decline-btn" title="Отклонить все"
							  data-info='<?=json_encode([
								  'book_id'     => $arResult['BOOK']['id'],
								  'info'        => $arResult['BOOK']['title'],
								  'status_code' => 'decline',
								  'change'      => 'отклонить все произведения издания',
							  ])?>'>Отклонить все</span>
					</div>
				<? endif ?>

			</div>
			<div class="right-tools" data-right-tools-widget data-dl-accordion>
				<?/*!--<ul>
					<li><span class="miniatures"></span></li>
					<li><span class="bookmark"></span></li>
					<li><span class="quote"></span></li>
					<li><span class="note"></span></li>
				</ul>-->*/?>

				<div class="properties-book-content" aria-labelledby="editiondesctoggler" id="editiondescription" class="sr-only" aria-hidden="true" aria-live="polite">
					<h2>Описание книги</h2>
					<?if($arResult['BOOK']['authorbook']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR')?>:</h3> <span><?=$arResult['BOOK']['authorbook']?></span></div><?endif;?>
					<?if($arResult['BOOK']['title']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE')?>:</h3> <span><?=$arResult['BOOK']['title']?></span></div><?endif;?>
					<?if($arResult['BOOK']['subtitle']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SUBTITLE')?>:</h3> <span><?=$arResult['BOOK']['subtitle']?></span></div><?endif;?>
					<?if($arResult['BOOK']['responsibility']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_RESPONSIBILITY')?>:</h3> <span><?=$arResult['BOOK']['responsibility']?></span></div><?endif;?>
					<?if($arResult['BOOK']['publishplace']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>:</h3> <span><?=$arResult['BOOK']['publishplace']?></span></div><?endif;?>
					<?if($arResult['BOOK']['publisher']):?><div><h3><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH')?>:</h3> <span><?=$arResult['BOOK']['publisher']?></span></div><?endif;?>
					<?if($arResult['BOOK']['year']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR')?>:</h3> <span><?=$arResult['BOOK']['year']?></span></div><?endif;?>
					<?if($arResult['BOOK']['countpages']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES')?>:</h3> <span><?=$arResult['BOOK']['countpages']?></span></div><?endif;?>
					<?if($arResult['BOOK']['series']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE')?>:</h3> <span><?=$arResult['BOOK']['series']?></span></div><?endif;?>
					<?if($arResult['BOOK']['isbn']):?><div><h3>ISBN:</h3> <span><?=$arResult['BOOK']['isbn']?></span></div><?endif;?>
					<?if($arResult['BOOK']['issn']):?><div><h3>ISSN:</h3> <span><?=$arResult['BOOK']['issn']?></span></div><?endif;?>
					<?if($arResult['BOOK']['contentnotes']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT')?>:</h3> <span><?=$arResult['BOOK']['contentnotes']?></span></div><?endif;?>
					<?if($arResult['BOOK']['notes']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE')?>:</h3> <span><?=$arResult['BOOK']['notes']?></span></div><?endif;?>
					<?if($arResult['BOOK']['annotation']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ANNOTATION')?>:</h3> <span><?=$arResult['BOOK']['annotation']?></div><?endif;?>
                    <?if($arResult['BOOK']['bbk']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BBK')?>:</h3> <span><?=$arResult['BOOK']['bbk']?></span></div><?endif;?>
                    <?if($arResult['BOOK']['udk']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UDC')?>:</h3> <span><?=$arResult['BOOK']['udk']?></span></div><?endif;?>
					<?if($arResult['BOOK']['edition']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_EDITION')?>:</h3> <span><?=$arResult['BOOK']['edition']?></span></div><?endif;?>
					<?if($arResult['BOOK']['library']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB')?>:</h3> <span><?=$arResult['BOOK']['library']?></span></div><?endif;?>
                    <?if($arResult['BOOK']['lang']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LANGUAGE')?>:</h3> <span><?=$arResult['BOOK']['lang'];?></span></div><?endif;?>

                    <?if($arResult['BOOK']['fundnumber']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_FUND_NUMBER')?>:</h3> <span><?=$arResult['BOOK']['fundnumber'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['fundname']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_FUND_NAME')?>:</h3> <span><?=$arResult['BOOK']['fundname'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['inventorynumber']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_INVENTORY_NUMBER')?>:</h3> <span><?=$arResult['BOOK']['inventorynumber'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['inventoryname']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_INVENTORY_NAME')?>:</h3> <span><?=$arResult['BOOK']['inventoryname'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['unitnumber']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UNIT_NUMBER')?>:</h3> <span><?=$arResult['BOOK']['unitnumber'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['unitname']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UNIT_NAME')?>:</h3> <span><?=$arResult['BOOK']['unitname'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['dates']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_DATES')?>:</h3> <span><?=$arResult['BOOK']['dates'];?></span></div><?endif;?>

                    <?if($arResult['BOOK']['description']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_DESCRIPTION')?>:</h3> <span><?=$arResult['BOOK']['description'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['material']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_MATERIAL')?>:</h3> <span><?=$arResult['BOOK']['material'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['size']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SIZE')?>:</h3> <span><?=$arResult['BOOK']['size'];?></span></div><?endif;?>

                    <?if($arResult['BOOK']['mpk']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_MPK')?>:</h3> <span><?=$arResult['BOOK']['mpk'] ?></span></div><?endif;?>
                    <?if($arResult['BOOK']['declarant']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_DECLARANT')?>:</h3> <span><?=$arResult['BOOK']['declarant'] ?></span></div><?endif;?>
                    <?if($arResult['BOOK']['patentholder']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PATENTHOLDER')?>:</h3> <span><?=$arResult['BOOK']['patentholder'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['representative']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_REPRESENTATIVE')?>:</h3> <span><?=$arResult['BOOK']['representative'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['publish_date']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_DATE')?>:</h3> <span><?=$arResult['BOOK']['publish_date'];?></span></div><?endif;?>
                    <?if($arResult['BOOK']['patentcode']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PATENTCODE')?>:</h3> <span><?=$arResult['BOOK']['patentcode'];?></span></div><?endif;?>
                </div>

				<div class="right-tools-kind">
					<span class="right-tools-label note" data-define data-edition-notes title="Заметки">Заметки</span>
					<div class="note-content" data-describe><?/*foreach($arResult['BOOK']['NOTE'] as $note):?>
						<div>
							<a p="<?=$note["NUM_PAGE"]?>"><span><?=$note["NUM_PAGE"]?> страница</span>
							<span><?=$note["TEXT"]?></span>
							</a>
							<span class='delete' id='<?=$note["ID"]?>'></span>
						</div>
						<?endforeach;*/?></div>
				</div>

				<div class="right-tools-kind">
					<span class="right-tools-label bookmark" data-define data-right-bookmarks title="Закладки">Закладки</span>
					<div class="bookmark-content" data-describe><?/*foreach ($arResult['BOOK']['MARK'] as $mark):?>
							<div>
								<a p="<?=$mark['page']?>"><span><?=$mark['page']?></span> страница</a>
								<span class='delete' id='<?=$mark['id']?>'></span>
							</div>
						<?endforeach;*/?></div>
				</div>

				<div class="right-tools-kind">
					<span class="right-tools-label quote" data-define data-right-cites title="Цитаты">Цитаты</span>
					<div class="quote-content" data-describe><?/*foreach($arResult['BOOK']['QUOTES'] as $quotes):?>
							<div>
								<a p="<?=$quotes["UF_PAGE"]?>"><span><?=$quotes["UF_PAGE"]?> страница</span>
									<img src='<?=$quotes["UF_IMG_DATA"]?>' alt='' />
								</a>
								<span class='delete' id='<?=$quotes["ID"]?>'></span>
							</div>
						<?endforeach;*/?></div>
				</div>

				<div class="right-tools-kind">
					<span class="right-tools-label search" data-define title="Поиск по тексту">Поиск по тексту</span>
					<div class='search-book-content' data-describe>
						<div class='search_'>
							<div class="clearfix">
								<input name='search_text' id = 'search_text' type="text" />
								<button class="btn btn-primary search-submit-btn" id='search_submit' type='submit' value='Искать' title="Искать" />
									<span class="fa fa-search"></span>
								</button>
							</div>
							<div class="search_preloader"></div>
							<div class='search_result empty'></div>
						</div>
					</div>
				</div>

				<? if (!empty($arResult['BOOK']['markup'])): /* список произведений */ ?>
					<div class="right-tools-kind" data-dl>
						<span class="right-tools-label chapter" data-define title="Содержание">Содержание</span>
						<div class="chapter-content" data-describe data-chapter-list>
							<? foreach($arResult['BOOK']['markup'] as $arFields): ?>
								<div data-chapter-item data-status="<?= $arFields['status']?>" data-bitrix='<?echo json_encode($arFields)?>'>
									<a title="<?=$arFields["author"]?>">
										<span p="<?=$arFields["startPage"]?>" class="title-anchor"><?=$arFields["startPage"]?>. <?=$arFields["title"]?></span>

										<? if (in_array('approve', $arResult['BOOK']['actions']) && 'confirmed' != $arFields['status']): ?>
											<span class="approve" data-change-status-chapter class="data-change-status" title="Подтвердить произведение" data-confirm-title="Подтвердить"
											data-info='<?echo json_encode(array_merge($arFields, ['status_code' => 'confirm']))?>'></span>
										<? endif; ?>
										<? if (in_array('decline', $arResult['BOOK']['actions']) && 'confirmed' != $arFields['status']): ?>
											<span class="decline" data-change-status-chapter class="data-change-status" title="Отклонить произведение" data-confirm-title="Отклонить"
											data-info='<?echo json_encode(array_merge($arFields, ['status_code' => 'decline']))?>'></span>
										<? endif; ?>

										<? if (in_array('add', $arResult['BOOK']['actions'])): ?>
											<span class='edit' data-edit-chapter title='Редактировать произведение' data-info='<?echo
												json_encode(array_merge($arFields, ['action' => 'update']))?>'></span>
										<? endif; ?>
										<? if (in_array('delete', $arResult['BOOK']['actions'])): ?>
											<span class='change-status-btn' data-change-status-chapter title='Удалить произведение' data-confirm-title="Удалить" data-info='<?echo
												json_encode(array_merge($arFields, [
													'book_id'     => $arFields['fullSymbolicId'],
													'info'        => $arFields['startPage'] . '. ' . $arFields['title'],
													'status_code' => 'delete',
													'change'      => 'удалить произведение',
												]))?>'></span>
										<? endif; ?>
									</a>
								</div>
							<? endforeach; ?>
						</div>
					</div>
				<? endif; ?>

				<? if (in_array('approve', $arResult['BOOK']['actions'])): ?>
					<div class="right-tools-kind">
						<a class="right-tools-label edition-log" title="Лог издания"
						   href="/profile/markup/<?=$arResult['BOOK']['id']?>/">Лог издания
						</a>
					</div>
				<? endif; ?>

				<? if (in_array('add', $arResult['BOOK']['actions'])): ?>
					<div class="right-tools-kind">
						<span class="right-tools-label add-chapter" title="Добавить произведение"
							data-add-chapter
							data-define
							data-info='<?=json_encode(['book_id' => $arResult['BOOK']['id']])?>'>Добавить произведение</span>
						<div class="pre-add-chapter" data-describe>
							<form data-prefill data-info='<?=json_encode(['book_id' => $arResult['BOOK']['id']])?>'>
								<div class="addform-title">
									<div class="col-xs-12">Название произведения</div>
								</div>
								<div class="addform-row">
									<div class="col-xs-12">
										<input type="text" name="title" class="form-control input-sm">
									</div>
								</div>
								<div class="addform-title">
									<div class="col-xs-12">Страницы в издании</div>
								</div>
								<div class="addform-row">
									<div class="col-xs-1"><span class="flabel">с</span></div>
									<div class="col-xs-3">
										<input type="text" name="startPage" class="form-control input-sm">
									</div>
									<div class="col-xs-1"><span class="flabel">по</span></div>
									<div class="col-xs-3">
										<input type="text" name="finishPage" class="form-control input-sm">
									</div>
									<div class="col-xs-4 text-right">
										<button class="btn btn-default btn-sm">Далее</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				<? endif; ?>

				<? if (!empty($arResult['BOOK']['markup'])): ?>
					<? if (in_array('approve', $arResult['BOOK']['actions'])): ?>
						<div class="right-tools-kind">
							<span data-change-status-chapter class="right-tools-label change-status-btn all-approve-btn" title="Подтвердить все"
								  data-info='<?=json_encode([
									  'book_id'     => $arResult['BOOK']['id'],
									  'info'        => $arResult['BOOK']['title'],
									  'status_code' => 'confirm',
									  'change'      => 'подтвердить все произведения издания',
								  ])?>'>Подтвердить все</span>
						</div>
					<? endif; ?>

					<? if (in_array('decline', $arResult['BOOK']['actions'])): ?>
						<div class="right-tools-kind">
							<span data-change-status-chapter data-confirm-title="Отклонить" class="right-tools-label change-status-btn all-decline-btn" title="Отклонить все"
								  data-info='<?=json_encode([
									  'book_id'     => $arResult['BOOK']['id'],
									  'info'        => $arResult['BOOK']['title'],
									  'status_code' => 'decline',
									  'change'      => 'отклонить все произведения издания',
								  ])?>'>Отклонить все</span>
						</div>
					<? endif; ?>

					<? if (in_array('delete', $arResult['BOOK']['actions'])): ?>
						<div class="right-tools-kind" data-button-delete-all>
							<span data-change-status-chapter data-confirm-title="Удалить" class="right-tools-label delete-all-chapters" title="Удалить все произведения"
								  data-info='<?=json_encode([
									  'book_id'     => $arResult['BOOK']['id'],
									  'info'        => $arResult['BOOK']['title'],
									  'status_code' => 'delete',
									  'change'      => 'удалить все произведения издания',
								  ])?>'>Удалить все</span>
						</div>
					<? endif; ?>
				<? endif; ?>

			</div>
		</div>
		<!-- end right -->
		
		
	<div style="display: none;">
	    <div class="box-modal" id="exampleModal">
	        <div class="box-modal_close arcticmodal-close">закрыть</div>
			<p></p>
	    </div>
	</div>
		<!-- end main -->
		<!-- footer -->

		<!-- end footer -->
		<div id='big_image_holder' align="center" data-view-port data-view-name="fullscreen-image">

			<div class="fs-tool fs-contract no-user-select" data-contract-view>Свернуть экран</div>
			<div class="fs-tool fs-scale no-user-select" data-fs-scale-tool>Масштаб</div>
			<div class="fs-scale-submenu" data-fs-scale-select>
				<div class="fs-scale-normal no-user-select active" data-fs-normal>1:1</div>
				<div class="fs-scale-horizontal no-user-select" data-fs-horizontal></div>
				<div class="fs-scale-vertical no-user-select" data-fs-vertical></div>
			</div>

		      <div class="image-wrapper">
		          <img id="big_image" class='big_image original_image'>
	                <div class="prev-page"></div>
	                <div class="next-page"></div>
	            </div>
		</div>

	</div>
	<?
	$APPLICATION->IncludeComponent("bitrix:system.auth.form", "for_viewer", 
		Array(
			"FORGOT_PASSWORD_URL" => "/auth/",
			"SHOW_ERRORS" => "N",
			"PROFILE_URL" => "/catalog2/"
		),
		false
	);
	?>

	<? if (!empty($arResult['BOOK']['actions'])): ?>
		<? require_once 'markup_forms.php'; ?>
		<?/*$APPLICATION->IncludeFile($templateFolder . "/chapter.php", [], ['MODE' => 'php']);*/?>
	<? endif; ?>

<div class="modal fade" tabindex="-1" role="dialog" id="universal-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: relative; z-index: 1;"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="text-transform: none;"></h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<?if (!MINIFY_JS_CSS_MODE || MARKUP_DEBUG_MODE) { ?>
	<script type="text/javascript" src="/local/templates/.default/markup/js/libs/jquery.min.js"></script>
	<script type="text/javascript" src="/local/templates/adaptive/js/jquery/postjquery.js"></script>
	<script type="text/javascript" src="/local/templates/adaptive/js/spin.js"></script>
	<script type="text/javascript" src="/local/templates/adaptive/js/jquery/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/local/templates/adaptive/js/jquery/additional-methods.validate.min.js"></script>
	<script src="/local/components/exalead/search.page.detail/templates/.default/js/jquery.simplePagination.js"></script>
	<script type="text/javascript" src="/bitrix/js/jqRotate.js"></script>
	<script type="text/javascript" src="/bitrix/js/jquery.Jcrop.js"></script>
	<script type="text/javascript" src="/local/components/exalead/search.page.detail/templates/.default/js/jquery.mousewheel.min.js"></script>
	<script src="/local/templates/adaptive/js/bootstrap.min.js"></script>
	<!--script src="/local/components/exalead/search.page.detail/templates/.default/js/jquery.arcticmodal-0.3.min.js"></script-->
	<!--script type="text/javascript" src="/local/templates/adaptive/js/script.js"></script-->
	<!--script type="text/javascript" src="/local/components/exalead/search.page.detail/templates/special/script.js"></script-->
	<!--script type="text/javascript" src="/bitrix/js/main/core/core.min.js?143349360765494"></script-->
	<script type="text/javascript" src="/local/components/exalead/search.page.detail/templates/.default/script.js"></script>
<? } else { ?>
	<script type="text/javascript" src="/local/templates/adaptive/js/viewer.min.js"></script>
<? };?>

<? /* автопереход на нужную страницу #18534 */ ?>
<script type="text/javascript">
    $(function() {
        if($.trim($(".error_message").text())){
            $('.b_login_popup').show();
        }
        <? if ( isset($_GET['t_search']) && $_GET['t_search'] == 1 && isset($_COOKIE['__qSearch']) && mb_strlen($_COOKIE['__qSearch'])): ?>

            $('input#search_text').val('<?= base64_decode( $_COOKIE['__qSearch'] ); ?>');
            $('span.right-tools-label.search').trigger('click');

            <? if (!in_array($arResult['BOOK']['idlibrary'], [199, 200])): ?>
                window.triggerOwnerLibrary = true;
                $('button#search_submit').trigger('click');
            <? endif; ?>
        <? endif;?>
    });
</script>

<?$APPLICATION->IncludeFile("/local/include_areas/counters.php", Array(), Array("MODE" => "php", "NAME" => "Счетчики"));?>
		<div id="milk-shadow"></div>
	</body>
</html>