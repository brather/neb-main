<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 10.05.2017
 * Time: 12:35
 */

namespace Nota\Exalead\Agents;

use \Bitrix\Main\Loader,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient,
    \Neb\Main\Helper\LangHelper,
    \Neb\Main\SearchCollectionsTable,
    \Neb\Main\SearchLanguagesTable;

class Import
{
    /**
     * Агент производит импорт данных из статистики Exalead в Битрикс
     *
     * @return string
     */
    public static function main() {

        $sResult = "\\" . __METHOD__ . '();';

        $arStatistic = self::getStatistic();
        self::searchCollections($arStatistic['GROUPS']['COLLECTION_NEW']);
        self::searchLang($arStatistic['GROUPS']['LANG']);
        self::compareLibraries($arStatistic['GROUPS']['IDLIBRARY']);

        return $sResult;
    }

    /**
     * Получение статистических данных
     *
     * @return array
     */
    private static function getStatistic() {

        $query = new SearchQuery();
        $query->setQuery('#all');
        $query->setParam('sl', 'sl_statistic3');
        $query->setParam('hf', '0');
        $query->setTimeout(600);

        $client = new SearchClient();
        $result = $client->getResult($query);

        return $result;
    }

    /**
     * Сравнение поисковых коллекций в Exalead и Битрикс
     *
     * @param $arCollectionNew
     */
    private static function searchCollections(&$arCollectionNew) {

        if (empty($arCollectionNew)) {
            return;
        }

        $arExalead = $arBitrix = $arMess = [];

        // Шаг 1: Получение Коллекций из Exalead
        foreach($arCollectionNew as $arFields) {
            $arExalead[trim($arFields['title'])] = intval($arFields['count']);
        }

        // Шаг 2: получение коллекций Битрикса
        $rsCollections = SearchCollectionsTable::getList(['order' => ['NAME' => 'ASC']]);
        while ($arFields = $rsCollections->fetch()) {
            $arBitrix[$arFields['NAME']] = $arFields;
        }

        // Шаг 3: Импорт
        foreach ($arExalead as $sName => $iCount) {
            if (!isset($arBitrix[$sName])) { // добавление
                SearchCollectionsTable::add([
                    'NAME'    => $sName,
                    'NAME_EN' => LangHelper::yandexTranslateText($sName),
                    'COUNT'   => $iCount,
                    'SORT'    => 500,
                    'ACTIVE'  => 'Y',
                    'SELECT'  => 'Y'
                ]);
            }
            else { // изменение
                $arUpdate = [];
                if ($iCount != $arBitrix[$sName]['COUNT'])
                    $arUpdate['COUNT'] = $iCount;
                if ('Y' != $arBitrix[$sName]['ACTIVE'])
                    $arUpdate['ACTIVE'] = 'Y';
                if (!empty($arUpdate))
                    SearchLanguagesTable::update($arBitrix[$sName]['ID'], $arUpdate);
            }
        }
        if (!empty($arExalead) && !empty($arBitrix)) {
            foreach ($arBitrix as $sName => $arFields) {
                if (!isset($arExalead[$sName])) {
                    SearchCollectionsTable::update($arBitrix[$sName]['ID'], ['ACTIVE' => 'N']);
                }
            }
        }
    }

    /**
     * Импорт языков изданий
     *
     * @param $arLang
     */
    private static function searchLang(&$arLang) {

        if (empty($arLang)) {
            return;
        }

        $arExalead = $arBitrix = [];

        // Шаг 1: Получение Языков из Exalead
        foreach($arLang as $arFields) {
            $arExalead[trim($arFields['title'])] = intval($arFields['count']);
        }

        // Шаг 2: Получение языков Битрикса
        $rsLangs = SearchLanguagesTable::getList(['order' => ['NAME' => 'ASC']]);
        while ($arFields = $rsLangs->fetch()) {
            $arBitrix[$arFields['NAME']] = $arFields;
        }

        // Шаг 3: Импорт
        foreach ($arExalead as $sName => $iCount) {

            if (!isset($arBitrix[$sName])) { // добавление
                SearchLanguagesTable::add([
                    'NAME'    => $sName,
                    'NAME_EN' => LangHelper::yandexTranslateText($sName),
                    'COUNT'   => $iCount,
                    'SORT'    => 500
                ]);
            }
            else { // изменение
                $arUpdate = [];
                if ($iCount != $arBitrix[$sName]['COUNT'])
                    $arUpdate['COUNT'] = $iCount;
                if ('Y' != $arBitrix[$sName]['ACTIVE'])
                    $arUpdate['ACTIVE'] = 'Y';
                if (!empty($arUpdate))
                    SearchLanguagesTable::update($arBitrix[$sName]['ID'], $arUpdate);
            }
        }
        if (!empty($arExalead) && !empty($arBitrix)) {
            foreach ($arBitrix as $sName => $arFields) {
                if (!isset($arExalead[$sName])) {
                    SearchLanguagesTable::update($arBitrix[$sName]['ID'], ['ACTIVE' => 'N']);
                }
            }
        }
    }

    /**
     * Сравнивает список Библиотек в Exalead и в Битриксе, в случае расхождения высылает на почту
     * разработчика отдочное сообщение о необходимости добавить в Битрикс библиотеку.
     *
     * @param $arLibraries
     */
    private static function compareLibraries(&$arLibraries) {

        if (empty($arLibraries) || !Loader::includeModule('iblock')) {
            return;
        }

        $arExalead = $arBitrix = $arMess = [];

        // Шаг 1: Получение Библиотек из Exalead
        foreach ($arLibraries as $arLib) {
            $arExalead[$arLib['title']] = $arLib['count'];
        }

        // Шаг 2: Получение Библиотек Битрикса
        $rsLibraries = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, '!PROPERTY_LIBRARY_LINK' => false],
            false,
            false,
            ['ID', 'ACTIVE', 'PROPERTY_LIBRARY_LINK']
        );
        while ($arFields = $rsLibraries->Fetch()) {
            $arBitrix[$arFields['PROPERTY_LIBRARY_LINK_VALUE']] = $arFields;
        }
        unset($rsLibraries, $arFields);

        // Шаг 3: Сравнение Библиотек
        foreach ($arExalead as $iIdExalead => $iCountExalead) {
            if (empty($arBitrix[$iIdExalead])) {
                $arMess[] = 'В Битрикс отсутствует библиотека привязкой к NebLibs ID = ' . $iIdExalead;
            } elseif ($arBitrix[$iIdExalead]['ACTIVE'] == 'N') {
                $arMess[] = 'В Битрикс деактивирована библиотека ID = '
                    . $arBitrix[$iIdExalead]['ID'] . ' с привязкой к NebLibs ID = ' . $iIdExalead;
            }
        }

        // Шаг 4: Запись в лог ошибок синхронизации
        if (!empty($arMess)) {
            $arMess = "Ошибка синхронизации Библиотек в Exalead и Битрикс! \n\n" . implode(";\n\n", $arMess);
            $obEventLog = new \CEventLog();
            $obEventLog->Log('WARNING', 'Import', 'neb.main', __FUNCTION__, $arMess);
        }
    }
}