<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 14:43
 */

namespace AssessorBundle\Controller;


use AssessorBundle\Controller\Api\ApiAuthorRatingController;
use AssessorBundle\Controller\Api\ApiEditionRatingController;
use AssessorBundle\Controller\Api\RatingApiInterface;
use AssessorBundle\Traits\ControllerAssessorRatingTrait;
use BitrixBundle\Annotations\IncludeBitrix;
use JMS\DiExtraBundle\Annotation as DI;
use BitrixBundle\Annotations\Template as BitrixTemplate;

class EditionRatingController
{
    use ControllerAssessorRatingTrait;

    /**
     * @DI\Inject("assessor.api.edition_rating")
     * @var ApiEditionRatingController
     */
    public $api;

    /**
     * @BitrixTemplate("AssessorBundle:assessor:editions.html.php")
     * @IncludeBitrix()
     *
     * @return array
     */
    public function listAction()
    {
        $qb = $this->_getListQueryBuilder($this->api->getEntityName());
        if ($query = $this->request->query->get('author')) {
            if (is_array($query)) {
                foreach ($query as $key => $value) {
                    $qb->orWhere($qb->expr()->like('t.author', "?$key"));
                    $query[$key] = "%$value%";
                }
                $qb->setParameters($query);
            } else {
                $qb->andWhere($qb->expr()->like('t.author', ':author'));
                $qb->setParameter('author', "%$query%");
            }
        }
        if ($query = $this->request->query->get('title')) {
            if (is_array($query)) {
                foreach ($query as $key => $value) {
                    $qb->orWhere($qb->expr()->like('t.title', "?$key"));
                    $query[$key] = "%$value%";
                }
                $qb->setParameters($query);
            } else {
                $qb->andWhere($qb->expr()->like('t.title', ':title'));
                $qb->setParameter('title', "%$query%");
            }
        }
        if(!$order = $this->request->query->get('order')) {
            $order = ['rating' => 'desc'];
        }
        $result = $this->getListData($this->api->getEntityName(), $order);

        return $result;
    }

}