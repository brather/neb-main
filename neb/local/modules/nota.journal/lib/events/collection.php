<?php

namespace Nota\Journal\Events;

use Nota\Journal\J;
use Bitrix\Main\Type;

class Collection
{
    function onAdd(&$arFields){
        if($arFields['IBLOCK_CODE'] == IBLOCK_CODE_COLLECTIONS)
            J::add('collection', 'add', Array('COLLECTION_ID' => $arFields['IBLOCK_SECTION_ID']));
    }

    function onEdit(&$arFields){
        if($arFields['IBLOCK_CODE'] == IBLOCK_CODE_COLLECTIONS)
            J::add('collection', 'edit', Array('COLLECTION_ID' => $arFields['IBLOCK_SECTION_ID']));
    }

    function onDelete($arFields){
        if($arFields['IBLOCK_CODE'] == IBLOCK_CODE_COLLECTIONS)
            J::add('collection', 'delete', Array('COLLECTION_ID' => $arFields['IBLOCK_SECTION_ID']));
    }
}