<?

/**
 * Class collectionUser
 * Добавление в подборки
 */
class collectionUser
{
    private static $role = false;

    /**
     * Может литекущий пользователь добавить книгу в подборки
     * @return bool
     */
    public static function isAdd()
    {
        global $USER;
        if (!$USER->IsAuthorized())
            return false;

        if (self::$role) return 'user' == self::$role;

        $uobj = new nebUser();
        self::$role = $uobj->getRole();

        return 'user' == self::$role;
    }
}