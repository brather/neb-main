<?php
/**
 * User: agolodkov
 * Date: 17.08.2016
 * Time: 17:04
 */

namespace LbcBundle\Controller\Api;

use LbcBundle\Form\LbcSpecialdivisionsectionsType;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class LbcSpecialdivisionsectionsController
 *
 * @Route("/api/bbk")
 * @DI\Service("app.api.specialdivisionsections")
 */
class LbcSpecialdivisionsectionsController extends AbstractCategoryController
{
    /**
     * @return LbcSpecialdivisionsectionsType
     */
    public function getFormType()
    {
        return new LbcSpecialdivisionsectionsType();
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcSpecialdivisionsections';
    }

    /**
     * @return string
     */
    public function getCategoryOwnerFieldName()
    {
        return 'specialdivisionid';
    }
}