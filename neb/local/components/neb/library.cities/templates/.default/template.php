<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 */
?>
<section class="container hall-list">

    <div class="hall-list__main">
        <? foreach ($arResult['top'] as $city => $item) {
            if (false !== $item) { ?>
                <a href="/participants/?city_id=<?= $item ?>"
                   class="hall-list__main-link"><?= $city ?></a>
            <? } ?>
        <? } ?>
    </div>

    <ul class="hall-list__secondary">
        <? foreach ($arResult['items'] as $item) { ?>
            <li>
                <a href="/participants/?city_id=<?= $item['ID'] ?>"
                   class="hall-list__secondary-link"><?= $item['UF_CITY_NAME'] ?></a>
            </li>
        <? } ?>
    </ul>