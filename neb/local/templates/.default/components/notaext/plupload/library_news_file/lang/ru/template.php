<?php

$MESS['BROWSER_NOT_SUPPORTED'] = 'Ваш браузер не поддерживает загрузку файлов :-(';
$MESS['NO_FILE_SELECTED'] = 'Файл не выбран';
$MESS['SELECT_FILE'] = 'Выбрать файл';
$MESS['SELECT_FILES'] = 'Выбрать файлы';
$MESS['UPLOAD'] = 'Загрузить';
$MESS['PLUPLOAD_LIBRARY_NEWS_FILE_ATTACH'] = "Прикрепить файл к новости";
$MESS['PLUPLOAD_LIBRARY_NEWS_FILE_CHOOSE'] = "Выбрать файл";
$MESS['PLUPLOAD_LIBRARY_NEWS_FILE_FORMATS'] = "форматы";
$MESS['PLUPLOAD_LIBRARY_NEWS_FILE_DELETE'] = "Удалить";
$MESS['PLUPLOAD_LIBRARY_NEWS_FILE_LOADING'] = "Загружается";
$MESS['PLUPLOAD_LIBRARY_NEWS_FILE_NOT_SUPPORTED'] = "Файл данного формата не поддерживается";
