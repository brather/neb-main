$(function(){
    var $formEditSection = $('#collection-edit'),
        $inpDel = $formEditSection.find('input[name=delete_image]'),
        textConfirm = $inpDel.data('confirm');
    $formEditSection.on('submit', function(e){
        if($inpDel.is(':checked')) {
            var isDelete = confirm(textConfirm);
            if(!isDelete) e.preventDefault();
        }
    });
    $inpDel.on('click', function(e){
        var $blkImage = $('.b-scan_photo');
        if($inpDel.is(':checked')) $blkImage.addClass('delete-img');
        else $blkImage.removeClass('delete-img');
    });
    //region Скрытие возможности добавления элемента в слайдер при условии, того, что он является подразделом подборки
    var selParentSect = $('#parentSection'),
        blkSlideShow = $('.js-slider-show'),
        speedShow = 200;
    selParentSect.on('change', function(e){
        var $this = $(this),
            valSelect = $this.val();
        if(valSelect == '-') {
            if(!blkSlideShow.is(':visible')) {
                blkSlideShow.toggle(speedShow);
            }
        } else {
            if(blkSlideShow.is(':visible')) {
                blkSlideShow.toggle(speedShow);
            }
        }
    });
    //endregion
});