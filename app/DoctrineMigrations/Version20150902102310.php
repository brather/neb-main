<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150902102310 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
CREATE TABLE `neb_funds_delete_messages` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BOOK_ID`    VARCHAR(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE`    TEXT COLLATE utf8_unicode_ci,
  `TIMESTAMMP` TIMESTAMP NOT NULL      DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
            DROP TABLE `neb_funds_delete_messages`
SQL
        );

    }
}
