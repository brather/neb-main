<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!empty($arResult["ITEMS"])) {
    ?>
    <h2><?= Loc::getMessage('NEWS_LIST_LIBRARY_TITLE') ?></h2>
    <?
    foreach ($arResult["ITEMS"] as $arItem) {
        ?>
        <div class="library-news-item">
            <div class="library-news-item__date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></div>
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
            <p><?= $arItem["~PREVIEW_TEXT"] ?></p>
        </div>
        <?
    }
}