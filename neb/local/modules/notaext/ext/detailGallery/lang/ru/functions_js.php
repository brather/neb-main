<?
$MESS ['EDBG_DIALOG_TITLE'] = "Галерея для детального просмотра";
$MESS ['EDBG_BUTTON_NAME'] = "Галерея";
$MESS ['EDBG_CONTECST_EDIT_GALLERY'] = "Редактировать галерею";

/*search tab*/
$MESS ['EDBG_CHECKED_PHOTO'] = "Выбрать фото";

/* Edit tab	*/
$MESS ['EDBG_PHOTO_HIDE'] = "Скрыть";
$MESS ['EDBG_PHOTO_HIDE_TITLE'] = "Скрыть это фото";
$MESS ['EDBG_PHOTO_EDIT'] = "Редактировать";
$MESS ['EDBG_PHOTO_EDIT_TITLE'] = "Редактировать это фото";

/* Edit dialog	*/
$MESS ['EDBG_PHOTO_EDIT_TITLE_DIALOG'] = "Редактировать";
$MESS ['EDBG_PHOTO_EDIT_APPLY_CLOSE'] = "Применить и закрыть";
$MESS ['EDBG_PHOTO_EDIT_APPLY_NEXT'] = "Применить и перейти к следующей фотографии";
$MESS ['EDBG_PHOTO_EDIT_CANCEL'] = "Отменить и закрыть";
