<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient;

class AddBookFromReaderComponent extends CBitrixComponent {

    private $arRequest = array();
    private $userGroups = array();
    private $page = 'template';
    private $dClassPlan = '';
    private $dClassPlanList = '';
    private $arBlocks = array(HIBLOCK_CODE_DIGITIZATION_PLAN, HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
    private $userLib = '';
    private $usersLibAdmin = array();
    private $defaultArSort = array();
    private $defaultCodeSort = array();
    private $countItemsOnPage = '10';
    private $countItems = '';
    private $countPages = '';
    private $countDiff = 2;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        if(!empty($arParams["USER_GROUPS"])) {
            $this->userGroups = $arParams["USER_GROUPS"];
        }
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }

    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->_includeModule();                        // Подключение модулей
        $this->_getRequest();                           // Получение данных request

        $this->_getIdLibraryCurrent();                  // Возвращаем id библиотеки текущего администратора
        $this->_getInputHidden();                       // Возвращаем все скртытые input и устанавливаем передаваемые им значения

        $this->_getListBooks();                         // Возвращаем список книг по фильтру
        $this->_setArResultSort();                      // Возвращаем блок для сортировки книг
        $this->includeComponentTemplate($this->page);   // Подключаем шаблон и выводим данные
    }

    /**
     * Подключаем модули
     */
    private function _includeModule() {
        Loader::includeModule('highloadblock');
        Loader::includeModule('nota.exalead');
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public function _getRequest() {
        $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
        $this->arResult['REQUEST'] = $this->arRequest;
        return $this->arRequest;
    }

    /** Проверка request на наличие других параметров кроме $param
     * @param $param - параметр исключаемый из request для проверки
     * @return bool
     */
    private function _checkRequest($arParam) {
        $url = false;
        $i = 0;
        foreach($this->arRequest as $cReq => $vReq) {
            if(!in_array($cReq, $arParam)) {
                if($i == 0) {
                    $url = '?' . $cReq . '=' . $vReq;
                } else {
                    $url .= '&' . $cReq . '=' . $vReq;
                }
                $i++;
            }
        }
        return $url;
    }

    /** Получение id highload блока
     * @param $arCode - массив содержащий коды блоков
     * @return array - массив в котором ключ - код блока, значение - id блока
     */
    private function getIdBlock($arCode) {
        $arReturn = array();
        $condition = '';
        $i = 0;
        foreach($arCode as $cCode) {
            $pref = '';
            if($i != 0) {
                $pref = ' or';
            }
            $condition .= $pref . " NAME='" . $cCode . "'";
            $i++;
        }
        $connection = Application::getConnection();
        $sql = "SELECT ID,NAME FROM b_hlblock_entity WHERE " . $condition . ";";
        $recordset = $connection->query($sql);
        while($obRecord = $recordset->fetch()) {
            $arReturn[$obRecord['NAME']] = $obRecord['ID'];
        }
        return $arReturn;
    }

    /**
     * Возвращаем id библиотеки текущего администратора
     */
    private function _getIdLibraryCurrent() {
        if(UGROUP_LIB_CODE_ADMIN === nebUser::getCurrent()->getRole()) {
            $user = nebUser::getCurrent()->getUser();
            $this->arUser = $user;
            if(!empty($user["UF_LIBRARY"])) {
                $this->userLib = $user["UF_LIBRARY"];
            }
        }
    }

    /**
     * Получени идентификатора группы "Администратор библиотеки"
     * @return mixed
     */
    private function _getIdLibAdmin() {
        $arGroup = CGroup::GetList($by = "ID", $order = "asc", ["STRING_ID" => UGROUP_LIB_CODE_ADMIN, "Y"])->Fetch();
        return $arGroup["ID"];
    }

    /**
     * Возвращаем массив id пользователей группы "администраторы библиотек"
     */
    private function _getArUsers() {
        $idGroup = $this->_getIdLibAdmin();
        $this->usersLibAdmin = CGroup::GetGroupUser($idGroup);
    }

    /**
     * Получение html для форм скрытых значений для форм
     */
    private function _getInputHidden() {
        $inputHtml = '';
        foreach($this->arRequest as $cRequest => $vRequest) {
            $inputHtml .= '<input type="hidden" name="' . $cRequest . '" value="' . $vRequest . '" />';
        }
        $this->arResult['HTML_INPUT_HIDDEN'] = $inputHtml;
    }

    /** Возвращаем список изданий добавленных пользователями
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    private function _getListPlan() {

        // Вернем id HL блоков
        $arBlock = self::getIdBlock($this->arBlocks);
        $idPlan = $arBlock[HIBLOCK_CODE_DIGITIZATION_PLAN];
        $idPlanList = $arBlock[HIBLOCK_CODE_DIGITIZATION_PLAN_LIST];

        // Вернем значения объекта для работы с HL блоками dataClass
        $this->dClassPlanList = self::getDataManager($idPlanList);
        $this->dClassPlan = self::getDataManager($idPlan);
        $dClassPlan = $this->dClassPlan;
        $dClassPlanList = $this->dClassPlanList;

        // Вернем параметры сортировки
        $this->_setListSort();

        // Вернем массив свойств статуса
        $arValUFStatusPlan = self::getListValuesIdUF($idPlan);
        //$arValUFStatusPlanList = self::getListValuesIdUF($dClassPlanList);

        // Получим значения для кнопок
        $idStRemove = $idStAdd = $idStAgreed = 0;
        foreach($arValUFStatusPlan as $idStatus => $arStatus) {
            if($arStatus['VALUE'] == DIGITIZATION_STATUS_AGREED) {
                 $idStAgreed = $arStatus['ID'];
            } elseif($arStatus['VALUE'] == DIGITIZATION_STATUS_ON_AGREEMENT) {
                $idStAdd = $arStatus['ID'];
            } elseif($arStatus['VALUE'] == DIGITIZATION_STATUS_CANCELED) {
                // $idStRemove = $arStatus['ID'];
            }
        }
        $this->arResult['ID_STATUS_REMOVE'] = $idStRemove;
        $this->arResult['ID_STATUS_ADD'] = $idStAdd;

        $this->arResult["COUNT_ON_PAGE"] = $this->countItemsOnPage;
        if($this->arRequest["count_on_page"] != '') {
            $this->arResult["COUNT_ON_PAGE"] = $this->arRequest["count_on_page"];
        }

        if($this->arRequest["page"] != '') {
            $offset = ($this->arRequest["page"] - 1) * $this->arResult["COUNT_ON_PAGE"];
        } else {
            $offset = 0;
        }
        $idGroupAdmin = $this->_getIdLibAdmin();
        $usersAdmin = CGroup::GetGroupUser($idGroupAdmin);

        $arFilter = array(
            "UF_LIBRARY"        => $this->userLib,
            'UF_REQUEST_STATUS' => $idStAgreed,
            "!UF_NEB_USER"      => $usersAdmin,
        );

        $this->arResult['UF_STATUS_VALUES'] = $arValUFStatusPlan;
        $by = 'ID';
        $order = 'asc';
        if(!empty($this->arRequest['by'])) {
            $by = $this->arRequest['by'];
        }
        if(!empty($this->arRequest['order'])) {
            $order = $this->arRequest['order'];
        }

        $this->countItems = $dClassPlan::getCount($arFilter);
        $this->arResult['COUNT_ITEMS'] = $this->countItems;

        $rsData = $dClassPlan::getList(array(
            "select" => array('*'),
            "order" => array($by => $order),
            "filter" => $arFilter,
            "limit" => $this->arResult["COUNT_ON_PAGE"],
            "offset" => $offset,
        ));
        $iData = 0;
        $arInfoPlanList = array();
        while($arData = $rsData->fetch()) {
            $nameOfPlan = '';
            $this->arResult['LIST_ITEMS'][$iData] = $arData;
            //region Если статус издания "На согласовании"
            if($arValUFStatusPlan[$arData["UF_REQUEST_STATUS"]]["VALUE"] == DIGITIZATION_STATUS_ON_AGREEMENT) {
                //region Не совпадают id плана текущего и id плана у издания
                if($arData['UF_PLAN_ID'] != $this->arRequest['plan_id']) {
                    //region Вернем название плана в которое занесено издание
                    if(empty($arInfoPlanList[$arData['UF_PLAN_ID']])) {
                        $rsDataPlanList = $dClassPlanList::getList(array(
                            'filter' => array("ID" => $arData['UF_PLAN_ID']),
                            'select' => array("UF_PLAN_NAME")
                        ))->fetch();
                        $arInfoPlanList[$arData['UF_PLAN_ID']] = $rsDataPlanList['UF_PLAN_NAME'];
                    }
                    $nameOfPlan = $arInfoPlanList[$arData['UF_PLAN_ID']];
                    //endregion

                    $this->arResult['LIST_ITEMS'][$iData]["ARR_BTN"] = array(
                        "CLASS" => "",
                        "CHECKED" => '',
                        "ID_STATUS_ACTION_ADD" => '',
                        "ID_STATUS_ACTION_REMOVE" => '',
                        "MESS" => Loc::getMessage("LIB_ADD_PLAN_FROM_READER_BOOKS_DIGITIZED") . "<b>\"" . $nameOfPlan . "\"</b>"
                    );
                } 
                //endregion
                //region Издание находится на оцифровке в текущем плане
                else {
                    $this->arResult['LIST_ITEMS'][$iData]["ARR_BTN"] = array(
                        "CLASS" => "minus",
                        "CHECKED" => ' checked',
                        "ID_STATUS_ACTION_ADD" => $idStAdd,
                        "ID_STATUS_ACTION_REMOVE" => $idStRemove,
                        "MESS" => ""
                    );
                }
                //endregion
            } else {
                $this->arResult['LIST_ITEMS'][$iData]["ARR_BTN"] = array(
                    "CLASS" => "plus",
                    "CHECKED" => '',
                    "ID_STATUS_ACTION_ADD" => $idStAdd,
                    "ID_STATUS_ACTION_REMOVE" => $idStRemove,
                    "MESS" => ""
                );
            }
            $this->arResult['LIST_ITEMS'][$iData]["ARR_BTN"]['LABEL_ADD'] = Loc::getMessage('LIB_ADD_PLAN_FROM_READER_ADD');
            $this->arResult['LIST_ITEMS'][$iData]["ARR_BTN"]['LABEL_REMOVE'] = Loc::getMessage('LIB_ADD_PLAN_FROM_READER_REMOVE');
            //endregion
            $iData++;
        }
    }

    /**
     * Возвращаем список изданий которые используются в планах на оцифровку, но не имеют названия и автора
     */
    private function _getListBooks() {
        $this->_getListPlan();
        $arBooksExalead = array();
        foreach($this->arResult['LIST_ITEMS'] as $iData => $arData) {
            if($arData['UF_BOOK_NAME'] == '' || $arData['UF_BOOK_AUTHOR'] == '') {
                $arBooksExalead[$iData] = array(
                    'ID' => $arData['ID'],
                    'UF_EXALEAD_BOOK' => $arData['UF_EXALEAD_BOOK']
                );
            }
        }
        if(!empty($arBooksExalead)) {
            $this->arResult['LIST_BOOKS'] = array();
            $dClassPlan = $this->dClassPlan;
            foreach($arBooksExalead as $iData => $vData) {
                $query = new SearchQuery();
                $query->getById($vData['UF_EXALEAD_BOOK']);
                $client = new SearchClient();
                $result = $client->getResult($query);
                $idParent = '';
                if(isset($result['idparent2'])) {
                    $idParent = $result['idparent2'];
                }
                $this->arResult['LIST_BOOKS'][$iData]['UF_BOOK_AUTHOR'] = $result['authorbook'];
                $this->arResult['LIST_BOOKS'][$iData]['UF_BOOK_NAME'] = $result['title'];
                $this->arResult['LIST_BOOKS'][$iData]['UF_EXALEAD_PARENT'] = $idParent;
                $this->arResult['LIST_BOOKS'][$iData]['UF_BOOK_PUBLISHER'] = $result['publisher'];
                $this->arResult['LIST_BOOKS'][$iData]['UF_BOOK_YEAR_PUBLIC'] = $result['publishyear'];

                $dClassPlan::update($vData['ID'], array('UF_BOOK_AUTHOR' => $result['authorbook']));
                $dClassPlan::update($vData['ID'], array('UF_BOOK_NAME' => $result['title']));
                if($result['publishyear'] != '') {
                    $dClassPlan::update($vData['ID'], array('UF_BOOK_YEAR_PUBLIC' => $result['publishyear']));
                }
                if($idParent != '') {
                    $dClassPlan::update($vData['ID'], array('UF_EXALEAD_PARENT' => $idParent));
                }
            }
        }
        $this->_setNavBlock();
    }

    /** Возвращаем массив значений пользовательского свойства UF_REQUEST_STATUS
     * @return array
     */
    public function getListValuesIdUF($idHLBlock, $propertyCode = false) {
        if(!$propertyCode) {
            $propertyCode = 'UF_REQUEST_STATUS';
        }
        $arProp = CUserTypeEntity::GetList(
            array(),
            array('FIELD_NAME' => $propertyCode, 'ENTITY_ID' => 'HLBLOCK_' . $idHLBlock)
        )->Fetch();

        $arReturn = array();
        $cUserFieldEnum = new CUserFieldEnum();
        $rsUFValues = $cUserFieldEnum->GetList(array(), array('USER_FIELD_ID' => $arProp['ID']));
        while($obUFValues = $rsUFValues->Fetch()) {
            $arReturn[$obUFValues['ID']] = $obUFValues;
        }
        return $arReturn;
    }

    /** Возвращаем DataManager для работы с highload блоком
     * @param $idBlock - id hightload блока
     * @return \Bitrix\Main\Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    private function getDataManager($idBlock) {
        $hlblock   = HighloadBlockTable::getById($idBlock)->fetch();
        $resObject = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $resObject;
    }

    /**
     * Заполняем массив сортировки значениями по умолчанию
     */
    private function _setDefaultItemSort() {
        $this->defaultArSort = array(
            "ID" => array(
                "MESS" => Loc::getMessage("LIB_ADD_PLAN_FROM_READER_SORT_ID"),
                "ACTIVE" => "Y",
                "ORDER" => "asc",
            ),
            "UF_BOOK_NAME" => array(
                "MESS" => Loc::getMessage("LIB_ADD_PLAN_FROM_READER_TITLE_BOOK"),
                "ACTIVE" => "N",
                "ORDER" => "asc"
            ),
            "UF_BOOK_AUTHOR" => array(
                "MESS" => Loc::getMessage("LIB_ADD_PLAN_FROM_READER_AUTHOR_BOOK"),
                "ACTIVE" => "N",
                "ORDER" => "asc"
            ),
            "UF_DATE_ADD" => array(
                "MESS" => Loc::getMessage("LIB_ADD_PLAN_FROM_READER_SORT_DATE_CREATE"),
                "ACTIVE" => "N",
                "ORDER" => "asc",
            ),
            "UF_DATE_FINISH" => array(
                "MESS" => Loc::getMessage("LIB_ADD_PLAN_FROM_READER_SORT_DATE_EXECUTION"),
                "ACTIVE" => "N",
                "ORDER" => "asc",
            ),
        );
    }

    /** Возвращаем индекс акривного параметра сортировки
     * @return int|string
     */
    private function _getActiveItemSort($arrSort) {
        $sortItem = '';
        foreach($arrSort as $cSort => $arSubSort) {
            if($arSubSort['ACTIVE'] == 'Y') {
                $sortItem = $cSort;
                break;
            }
        }
        return $sortItem;
    }

    /**
     * Заполняем массив с параметрами сортировки
     */
    private function _setListSort() {
        $this->_setDefaultItemSort();
        $this->defaultCodeSort = $this->_getActiveItemSort($this->defaultArSort);
        $this->arParams["SORT"] = $this->defaultArSort;
        if(!empty($this->arRequest['by'])) {
            if($this->defaultCodeSort != $this->arRequest['by']) {
                $this->arParams["SORT"][$this->defaultCodeSort]["ACTIVE"] = "N";
                $this->arParams["SORT"][$this->arRequest['by']]["ACTIVE"] = "Y";
            }
            if($this->arParams["SORT"][$this->arRequest['by']]["ORDER"] != $this->arRequest['order']) {
                $this->arParams["SORT"][$this->arRequest['by']]["ORDER"] = $this->arRequest['order'];
            }
        }
    }

    /**
     * Формируем массив для вывода блока с сортировкой
     */
    private function _setArResultSort() {
        $prefUrl = $this->_checkRequest(array('by', 'order'));

        foreach($this->arParams["SORT"] as $cSort => $arSubSort) {
            $order = $arSubSort["ORDER"];
            $startSymb = '?';
            if($prefUrl != '') {
                $startSymb = '&';
            }
            if(($arSubSort["ACTIVE"] == "Y") && !empty($this->arRequest['order'])) {
                if($this->arRequest['order'] == 'asc') {
                    $order = 'desc';
                } else {
                    $order = 'asc';
                }
            }
            if($order == 'asc') {
                $arrowHtml = '<i class="fa fa-sort-up"></i>';
            } else {
                $arrowHtml = '<i class="fa fa-sort-desc"></i>';
            }
            $this->arResult['SORT'][] = array(
                "NAME" => $arSubSort["MESS"] . "&nbsp;" . $arrowHtml,
                "ACTIVE" => $arSubSort["ACTIVE"],
                "HREF" => $prefUrl . $startSymb . 'by=' . $cSort . '&order=' . $order,
                "COLUMN" => $arSubSort["COLUMN"]
            );
        }
    }

    /**
     * Формируем html постраничной навигации
     */
    private function _setNavBlock() {
        if($this->arResult["COUNT_ON_PAGE"] != 'all') {
            if($this->countItems % $this->arResult["COUNT_ON_PAGE"] == 0) {
                $this->countPages = $this->countItems / $this->arResult["COUNT_ON_PAGE"];
            } else {
                $this->countPages = floor($this->countItems / $this->arResult["COUNT_ON_PAGE"]) + 1;
            }
            $iPage = 1;
            $curPage = $iPage;
            if(!empty($this->arRequest["page"])) {
                $curPage = $this->arRequest["page"];
            }
            $prevPage = $curPage - 1;
            $nextPage = $curPage + 1;
            $prefUrl = $this->_checkRequest(array('page'));
            if($prefUrl) {
                $prefUrl = $this->arParams["SEF_FOLDER"] . $prefUrl . '&';
            } else {
                $prefUrl = $this->arParams["SEF_FOLDER"] . '?';
            }
            if($this->countPages > 1) {
                $sPage = 1;
                $ePage = $this->countPages;
                if($curPage != 1 && ($curPage - $this->countDiff) > 0) {
                    $sPage = $curPage - $this->countDiff;
                }
                if(($curPage + $this->countDiff) < $this->countPages) {
                    $ePage = $curPage + $this->countDiff;
                }
                $tmpHtml = '<section class="text-center"><ul class="pagination">';
                if($curPage - $this->countDiff > 1) {
                    $tmpHtml .= '
                    <li>
                        <a class="d-map-list-info" title="' . Loc::getMessage("LIB_ADD_PLAN_FROM_READER_NAV_FIRTS") . '" href="' . $prefUrl . 'page=' . 1 . '"><span class="fa fa-angle-double-left"></span></a>
                    </li>';
                }
                if($curPage > 1) {
                    $tmpHtml .= '
                    <li>
                        <a class="d-map-list-info" title="' . Loc::getMessage("LIB_ADD_PLAN_FROM_READER_NAV_PREV") . '" href="' . $prefUrl . 'page=' . $prevPage . '"><span class="fa fa-angle-left"></span></a>
                    </li>';
                }
                while($iPage <= $this->countPages) {
                    if(intval($curPage) == $iPage) {
                        $uriPage = '#';
                        $classPage = ' class="pagination__active"';
                        $actionPage = ' onclick="event.preventDefault();"';
                    } else {
                        $uriPage = $prefUrl . 'page=' . $iPage;
                        $classPage = '';
                        $actionPage = '';
                    }
                    if($iPage >= $sPage && $iPage <= $ePage) {
                        $tmpHtml .= '
                        <li>
                            <a href="' . $uriPage . '"' . $classPage . $actionPage .'>' . $iPage . '</a>
                        </li>';
                    }
                    $iPage++;
                }
                if($curPage < $this->countPages) {
                    $tmpHtml .= '
                    <li>
                        <a class="d-map-list-info" title="' . Loc::getMessage("LIB_ADD_PLAN_FROM_READER_NAV_NEXT") . '" href="' . $prefUrl . 'page=' . $nextPage . '"><span class="fa fa-angle-right"></span></a>
                    </li>';
                }
                if($curPage + $this->countDiff < $this->countPages) {
                    $tmpHtml .= '
                    <li>
                        <a class="d-map-list-info" title="' . Loc::getMessage("LIB_ADD_PLAN_FROM_READER_NAV_LAST") . '" href="' . $prefUrl . 'page=' . $this->countPages . '"><span class="fa fa-angle-double-right"></span></a>
                    </li>';
                }
                $tmpHtml .= '</ul></section>';
                $this->arResult["PAGINATION"] = $tmpHtml;
            } else {
                $this->arResult["PAGINATION"] = '';
            }
        }
    }
}