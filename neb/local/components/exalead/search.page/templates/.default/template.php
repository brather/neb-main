<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

?>
<!-- Контент НАЧАЛО-->
<main>
    <section class="search-result">

        <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/search_header.php', array('arResult' => $arResult, 'arParams' => $arParams )); ?>
        <?php if(!empty($arResult['ITEMS'])): ?>
            <main class="search-result__content">
                <div class="row reverse-row">

                    <?php if(!empty($arResult['GROUPS'])):?>
                        <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/filters.php', array('arParams' => $arParams,'arResult' => $arResult, 'index' => $index,'arItem' => $arItem )); ?>
                        <div class="col-md-9">
                    <?php else: ?>
                        <div class="col-md-9" style="width:100%;">
                    <?php endif; ?>

                        <div class="search-result-sort clearfix res-sort-four">
                            <a href="<?php echo preg_replace('#by=.*sort#','', $_SERVER['REQUEST_URI']);?>" ><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_RELEVANCE');?></a>
                            <a <?=SortingExalead("document_authorsort")?> ><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_AUTHOR');?></a>
                            <a <?=SortingExalead("document_titlesort")?> ><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_NAME');?></a>
                            <a <?=SortingExalead("document_publishyearsort")?> ><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_DATE');?></a>
                        </div>

                        <ul class="search-result__content-list clearfix">

                            <?php foreach($arResult['ITEMS'] as $index => $arItem): ?>
                                <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/search_row.php', array('arParams' => $arParams,'arResult' => $arResult, 'index' => $index,'arItem' => $arItem )); ?>
                            <?php endforeach; ?>

                        </ul>
                    </div>
            </main>
        <?php endif; ?>
    </section>

    <?php echo !empty($arResult['STR_NAV']) ? htmlspecialcharsBack($arResult['STR_NAV']) : ''; ?>

</main>
<!-- /Контент КОНЕЦ -->
<!-- Modal Blocks -->
<? foreach ( $arResult['ITEMS'] as $arItem ): ?>
    <? if ( $arItem['IS_MEDIA'] === "Y" && $arItem['IS_READ'] === "N" ): ?>
        <div class="modal fade" id="modal-media-player-<?= $arItem['NUM_ITEM']?>" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?= $arItem['title']?></h4>
                    </div>
                    <div class="modal-body">
                        <? if ( $arItem['IS_MEDIA_VIDEO'] === true ): ?>
                            <? if ( in_array( 'flv', $arItem['fileExt'] ) ):?>
                                <object id="media_player" type="application/x-shockwave-flash" data="/local/components/exalead/search.page/templates/.default/uppod.swf" width="567" height="320">
                                    <param name="bgcolor" value="#ffffff" />
                                    <param name="allowFullScreen" value="true" />
                                    <param name="allowScriptAccess" value="always" />
                                    <param name="wmode" value="window" />
                                    <param name="movie" value="/local/components/exalead/search.page/templates/.default/uppod.swf" />
                                    <param name="flashvars" value="comment=<?=$arItem['title']?>&amp;m=video&amp;file=<?=Option::get("nota.exalead", "viewer_protocol")?>://<?=Option::get("nota.exalead", "viewer_ip")?>:<?=Option::get("nota.exalead", "viewer_port")?>/<?=$arItem['id']?>/resources/<?= $arItem['fileExt'][0];?>" />
                                </object>
                            <? else :?>
                                <video controls width="567" height="320">
                                    <source src="<?=Option::get("nota.exalead", "viewer_protocol")?>://<?=Option::get("nota.exalead", "viewer_ip")?>:<?=Option::get("nota.exalead", "viewer_port")?>/<?=$arItem['id']?>/resources/<?= $arItem['fileExt'][0];?>" type="video/<?= $arItem['fileExt'][0];?>"/>
                                    Your browser does not support the video tag.
                                </video>
                            <? endif;?>
                        <? else :?>
                            <div class="text-center">
                                <audio src="<?=Option::get("nota.exalead", "viewer_protocol")?>://<?=Option::get("nota.exalead", "viewer_ip")?>:<?=Option::get("nota.exalead", "viewer_port")?>/<?=$arItem['id']?>/resources/<?= $arItem['fileExt'][0];?>" type="video/<?= $arItem['fileExt'][0];?>" controls></audio>
                            </div>
                        <? endif;?>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    <? endif;?>
<? endforeach;?>
<!-- /Modal Blocks -->