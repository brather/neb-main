<?


require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aTabs = array();
$aTabs[] = array(
    "DIV" => "edit1",
    'TAB'   => 'ЕБП и ГЗЧ РГБ',
    'TITLE' => 'Переконфигурирование  путей к ЕБП и ГЗЧ РГБ'

);

$tabControl = new CAdminTabControl('tabControl', $aTabs);

if($_SERVER['REQUEST_METHOD'] == 'POST' )
{
    COption::SetOptionString("nota.userdata","ebp_rgb", $_POST['ebp_rgb']);       // 1. ЕБП РГБ для функций работающих с данным сервером
    COption::SetOptionString("nota.userdata","gzchch_rgb", $_POST['gzchch_rgb']); // 2. ГЗЧЧ РГБ для функций работающих с данным сервером
    //COption::SetOptionString("nota.userdata","output_rgb", $_POST['output_rgb']); // 3. серверу выдачи РГБ для функций работающих с данным сервером
   // COption::SetOptionString("nota.userdata","syn_rgb", $_POST['syn_rgb']);       // 4. серверу синхронизации РГБ для функций работающих с данным сервером
    //COption::SetOptionString("nota.userdata","output_rnb", $_POST['output_rnb']); // 5. серверу выдачи РНБ для функций работающих с данным сервером
   // COption::SetOptionString("nota.userdata","syn_rnb", $_POST['syn_rnb']);       // 6. серверу синхронизации РНБ для функций работающих с данным сервером
   // COption::SetOptionString("nota.userdata","output_neb", $_POST['output_neb']); // 7. серверу выдачи НЭБ.РФ для ссылок на документы в результатах поиска Экзалида

}


$APPLICATION->SetTitle('Переконфигурирование путей к ЕБП и ГЗЧ РГБ');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>
<style scoped="">
    .adm-detail-content-cell-l{
        text-align: left;
    }
</style>

    <form method="POST" target="_self" id="form" name="form" action="<?=$APPLICATION->GetCurUri()?>" enctype="multipart/form-data">
        <?=bitrix_sessid_post()?>

        <table id="edit1_edit_table" class="adm-detail-content-table edit-table">
            <tbody>
            <tr>
                <td width="180" class="adm-detail-content-cell-l">
                    ЕБП РГБ
                </td>
                <td class="adm-detail-content-cell-r">
                    <input type="text" value="<?= COption::GetOptionString("nota.userdata", "ebp_rgb", "")?>" size="47" name="ebp_rgb" class="adm-input">
                </td>
            </tr>
            <tr>
                <td width="180" class="adm-detail-content-cell-l">
                    ГЗЧ РГБ
                </td>
                <td class="adm-detail-content-cell-r">
                    <input type="text" value="<?= COption::GetOptionString("nota.userdata", "gzchch_rgb", "")?>" size="47" name="gzchch_rgb" class="adm-input">
                </td>
            </tr>
            <!--
            <tr>
                <td width="180" class="adm-detail-content-cell-l">
                    серверу выдачи РГБ
                </td>
                <td class="adm-detail-content-cell-r">
                    <input type="text" value="<?= COption::GetOptionString("nota.userdata", "output_rgb", "")?>" size="47" name="output_rgb" class="adm-input">
                </td>
            </tr>
            <tr>
                <td width="180" class="adm-detail-content-cell-l">
                    серверу синхронизации РГБ
                </td>
                <td class="adm-detail-content-cell-r">
                    <input type="text" value="<?= COption::GetOptionString("nota.userdata", "syn_rgb", "")?>" size="47" name="syn_rgb" class="adm-input">
                </td>
            </tr>
            <tr>
                <td width="180" class="adm-detail-content-cell-l">
                    серверу выдачи РНБ
                </td>
                <td class="adm-detail-content-cell-r">
                    <input type="text" value="<?= COption::GetOptionString("nota.userdata", "output_rnb", "")?>" size="47" name="output_rnb" class="adm-input">
                </td>
            </tr>
            <tr>
                <td width="180" class="adm-detail-content-cell-l">
                    серверу синхронизации РНБ
                </td>
                <td class="adm-detail-content-cell-r">
                    <input type="text" value="<?= COption::GetOptionString("nota.userdata", "syn_rnb", "")?>" size="47" name="syn_rnb" class="adm-input">
                </td>
            </tr>
            <tr>
                <td width="180" class="adm-detail-content-cell-l">
                    серверу выдачи НЭБ.РФ
                </td>
                <td class="adm-detail-content-cell-r">
                    <input type="text" value="<?= COption::GetOptionString("nota.userdata", "output_neb", "")?>" size="47" name="output_neb" class="adm-input">
                </td>
            </tr>
-->
            </tbody>
        </table>



        <input type="submit" class="adm-btn-save" title="Сохранить" value="Сохранить" name="submit">

    </form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");

?>