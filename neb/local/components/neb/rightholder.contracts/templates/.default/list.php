<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>

<form method="get" action="?">
	Дата с
	<div class="form-group" style="width:200px;">
		<div class="input-group">
			<input
				type="text"
				name="dateFrom"
				id="dateFromInput"
				size="10"
				value="<?=htmlspecialchars($_REQUEST['dateFrom'])?>"
				class="datepicker-from form-control"
				data-masked="99.99.9999"
			>
			<span class="input-group-btn">
				<a class="btn btn-default" onclick="BX.calendar({node: 'dateFromInput', field: 'dateFromInput',  form: '', bTime: false, value: ''});">
					<span class="glyphicon glyphicon-calendar"></span>
				</a>
			</span>
		</div>
	</div>
	по
	<div class="form-group" style="width:200px;">
		<div class="input-group">
			<input
				type="text"
				name="dateTo"
				id="dateToInput"
				size="10"
				value="<?=htmlspecialchars($_REQUEST['dateTo'])?>"
				class="datepicker-current form-control"
				data-masked="99.99.9999"
			>
			<span class="input-group-btn">
				<a class="btn btn-default" onclick="BX.calendar({node: 'dateToInput', field: 'dateToInput',  form: '', bTime: false, value: ''});">
					<span class="glyphicon glyphicon-calendar"></span>
				</a>
			</span>
		</div>
	</div>

	<select name="status">
		<option value="">Все</option>
		<? foreach ($arResult['ENUMS']['UF_STATUS'] as $arFields):
			if (in_array('operator', $arResult['GROUPS']) && $arFields['XML_ID'] == 'draft')
				continue;
			?>
			<option <?=$_REQUEST['status'] == $arFields['ID'] ? 'selected="selected"' : ''?> value="<?=$arFields['ID']?>"><?=$arFields['VALUE']?></option>
		<? endforeach; ?>
	</select>
	<input type="submit" class="b-search_bth bbox" value="<?=GetMessage('RIGHTHOLDER_BOOKS_SEARCH');?>Найти">
</form>

<? if(!empty($arResult['CONTRACTS'])): ?>
	<div class="b-fondtable">
		<table cellspacing="0" cellpadding="4" border="1">
			<tr>
				<th class="id_cell"><a <?=SortingExalead("ID")?>>Id</a></th>
				<th class="title_cell"><a <?=SortingExalead("UF_NUMBER")?>>Номер</a></th>
				<th class="autor_cell"><a <?=SortingExalead("UF_DATE_FROM")?>>Дата заключения</a></th>
				<th class="bbk_cell"><a <?=SortingExalead("UF_DATE_TO")?>>Дата окончания</a></th>
				<th class="year_cell"><a <?=SortingExalead("UF_STATUS")?>>Статус</a></th>
				<th class="exs_cell">Просмотреть</th>
			</tr>
			<?foreach($arResult['CONTRACTS'] as $arContract):
				$sURL = str_replace('#ID#', $arContract['ID'],
    				$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['element']
				);
				?>
				<tr>
					<td><?= $arContract['ID']; ?></td>
					<td><?= $arContract['UF_NUMBER']?:'-'; ?></td>
					<td><?= $arContract['UF_DATE_FROM']; ?></td>
					<td><?= $arContract['UF_DATE_TO']; ?></td>
					<td><?= $arContract['STATUS']['VALUE']; ?></td>
					<td><a href="<?= $sURL; ?>">Посмотреть</a></td>
				</tr>
			<? endforeach; ?>
		</table>
	</div>
<? endif; ?>