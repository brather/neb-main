<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150909094354 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
ALTER TABLE neb_stat_library
ADD PUBLICATIONS_DIGITIZED INT,
ADD PUBLICATIONS_DIGITIZED_DD INT,
ADD PUBLICATIONS_DD INT;
SQL
        );
        $this->connection->executeQuery(
            <<<SQL
ALTER TABLE neb_stat_library
MODIFY COLUMN DATE TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP();
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
ALTER TABLE neb_stat_library
DROP COLUMN PUBLICATIONS_DIGITIZED,
DROP COLUMN PUBLICATIONS_DIGITIZED_DD,
DROP COLUMN PUBLICATIONS_DD;
SQL
        );
    }
}
