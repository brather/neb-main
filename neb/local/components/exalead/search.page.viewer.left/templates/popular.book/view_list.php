<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

#if(empty($arParams['ajax']) and (bool)$arParams['IS_SHOW_PROTECTED'] === false)
if(empty($arParams['ajax']))
	include_once($_SERVER['DOCUMENT_ROOT'].'/local/include_areas/books_closed.php');

?>
<div class="b-filter_list_wrapper">
	<div class="b-result-doc" id="list">
		<?
			if($arParams['ajax'])
				$APPLICATION->RestartBuffer();

			if(!empty($arResult['ITEMS']))
			{
                // Generate auth/register link according with user group
                global $USER;
                define('REGISTER_TYPE_SIMPLE', 37);
                define('REGISTER_TYPE_FULL', 38);

                if ($USER->IsAuthorized())
                {
					$arUser = CUser::GetList($by, $order,
						array("ID" => $USER->GetID()),
			            array("SELECT" => array("UF_REGISTER_TYPE"))
					)->Fetch();
                    if ($arUser['UF_REGISTER_TYPE'] == REGISTER_TYPE_SIMPLE ){
                        $path = '/profile/update_select/';
                    }
                } else {
                    $path = '/registration/';
                    $_SESSION['registration_recomended_mode'] = 'full';
                    // echo 'Anonimouse';
                }
				foreach($arResult['ITEMS'] as $index=>$arItem)
				{
				?>
				<div class="b-result-docitem search-result" id="<?=$arItem['~id']?>">

                    <div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="<?=$arItem['DETAIL_PAGE_URL']?>"><img alt="" src="<?=$arItem['IMAGE_URL']?>&width=80&height=125" class="loadingimg" data-autor="<?=$arItem['~authorbook']?>" data-title="<?=$arItem['~title']?>" ></a>
						<? 
							if($arItem['isprotected'] > 0 and !$arParams['IS_SHOW_PROTECTED']) 
							{ 
							?>
							<div class="b-result-doclabel"><a href="<?=$path?>"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_AUTH');?></a></div>
							<?
							}
						?>
					</div>

					<div class="iblock b-result-docinfo">
						<?
							/*if(!empty($arParams['IS_ADD_PLAN_DIGIT']))
							{
							?>
							<div class="rel plusico_wrap plan-digitalization <?=(!empty($arParams['BOOK_IN_PLAM']) and in_array(urldecode($arItem['~id']), $arParams['BOOK_IN_PLAM'])) ? 'minus':''?>">
							<div class="plus_ico plus_digital"></div>
							<div class="b-hint rel" data-plus="<?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_PLAN');?>" data-minus="<?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_PLAN');?>"><?
							if(!empty($arParams['BOOK_IN_PLAM']) and in_array(urldecode($arItem['~id']), $arParams['BOOK_IN_PLAM']))
							{
							?><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_PLAN');?><?
							}
							else
							{
							?><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_PLAN');?><?
							}
							?></div>
							</div>
							<?
							}
							else */
							if(collectionUser::isAdd())
							{
								if(!empty($arResult['USER_BOOKS'][urlencode($arItem['id'])]) or !empty($arResult['USER_BOOKS'][$arItem['id']]))
								{
								?>
								<div class="meta minus">
									<div class="b-hint rel"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_LIB');?></div>
									<a href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arItem['id'])?>" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>" class="b-bookadd fav"></a>
								</div>
								<?
								}
								else
								{
								?>
								<div class="meta">
									<div class="b-hint rel" data-plus="<?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_LIB');?>" data-minus="<?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_LIB');?>"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_LIB');?></div>
									<a href="#" data-normitem="Y" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arItem['id'])?>"  class="b-bookadd"></a>
								</div>
								<?
								}
							}
						?>

						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['title']?></a></h2>
						<ul class="b-resultbook-info">
							<?
								if(!empty($arItem['authorbook']))
								{
								?>
								<li><span><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_AUTHOR');?>:</span> <a href="/search/?f_field[authorbook]=f/authorbook/<?=urlencode(mb_strtolower(strip_tags(trim($arItem['authorbook'])))).'&'.DeleteParam(array('f_field', 'dop_filter'))?>"><?=$arItem['authorbook']?></a></li>
								<?
								}
								if(!empty($arItem['year']))
								{
								?>
								<li><span><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_YEAR');?>:</span> <a href="/search/?f_publishyear=<?=$arItem['year'].'&'.DeleteParam(array('f_authorbook', 'f_publishyear', 'f_publisher', 'dop_filter'))?>"><?=$arItem['year']?></a></li>
								<?
								}
								if(intval($arItem['countpages']) > 0)
								{
								?>
								<li><span><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_PAGES');?>:</span> <?=$arItem['countpages']?></li>
								<?
								}
								if(!empty($arItem['publisher']))
								{
								?>
								<li><span><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_PUBLISH');?>:</span> <a
										href="/search/?f_field[publisher]=f/publisher/<?=urlencode(mb_strtolower(trim($arItem['publisher'])))
										.'&'.DeleteParam(['f_field', 'dop_filter'])?>"><?=$arItem['publisher']?></a></li>
								<?
								}
							?>
						</ul>
						<p><?=$arItem['text']?></p>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SOURCE');?>:</em>
                                <?if($arItem['library_url']):?>
                                    <a href="<?=$arItem['library_url']?>" class="b-sorcelibrary"><?=$arItem['library']?></a>
                                <?else:?>
                                    <span><?=$arItem['library']?></span>
                                <?endif;?>
                            </div>

							<div class="b-result-type right">
								<?
									if ($arItem['source'] == 'VideoData')
									{
										/*?><span class="b-result-type_txt"><a class="popup_opener ajax_opener" target="_blank" href="<?=$arItem['VIDEO_URL']?>" data-width="640" data-height="480" data-posmy="left+120" data-posat="center right">mp4</a></span><?*/
									?><span class="b-result-type_txt"><a onclick="popupVideoWindow('<?=$arItem['VIDEO_URL']?>', '<?=$arItem['title']?>'); return false;" target="_blank" href="#">смотреть</a></span><?
									}
									elseif($arItem['isprotected'] > 0 and !empty($arItem['PROTECTED_URL']))
									{
										if((bool)$arParams['IS_SHOW_PROTECTED'] === true)
										{
										/*?><span class="b-result-type_txt"><a target="_blank" href="<?=$arItem['PROTECTED_URL']?>" onclick="messageOpen(this.href, '.b-message_warning'); return false;"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_READ');?></a></span><?*/
										?><span class="b-result-type_txt"><a target="_blank" href="<?=$arItem['PROTECTED_URL']?>"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_READ');?></a></span><?
										}
										else
										{
										
										?><span class="b-result-type_txt closed_book"><a target="_blank" data-link="<?=$arItem['PROTECTED_URL']?>" href="#"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_READ');?></a></span><?
										}
									}
									else
									{
										if(!empty($arItem['fileExt']))
										{
											foreach($arItem['fileExt'] as $ext)
											{
												if($ext != 'pdf')
													continue;

												if($ext == 'pdf' and $arItem['IS_READ'] == 'Y'){
												?>
												<span class="b-result-type_txt" data-ext="<?=$ext?>"><a target="_blank" href="<?=$arItem['VIEWER_URL']?>"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_READ');?></a></span>
												<?
												}else{
												/*?>
												<span class="b-result-type_txt" ><?=$ext?></span>
												<?*/
												}
											}
										}
									}
								?>
							</div>	

						</div>
						<span class="num"><?=$arItem['NUM_ITEM']?>.</span>
					</div>

				</div>
				<?
				}

				if(!$arParams['ajax'] and !empty($arResult['NEXT_URL_PARAMS']))
				{
				?>
				<div class="b-result-docmore rel" ><a class="js_ajax_loadresult" href="<?=$this->__component->__parent->__template->__folder.'/next_result.php?'.$arResult['NEXT_URL_PARAMS']?>"></a></div>
				<?
				}
			}

			if($arParams['ajax'])
			{
			?>
			<script type="text/javascript">
				<?
					if(!empty($arResult['NEXT_URL_PARAMS'])){
					?>
					$('a.js_ajax_loadresult').attr('href', '<?=$this->__component->__parent->__template->__folder.'/next_result.php?'.$arResult['NEXT_URL_PARAMS']?>');
					<?
					}else{
					?>
					$('div.b-result-docmore').remove();
					<?
					}
				?>
				$('.popup_opener').uipopup();
				checkIsAvailablePdf();
			</script>
			<?
				exit();
			}
		?>
	</div><!-- /.b-result-doc -->

	<?=!empty($arResult['STR_NAV']) ? htmlspecialcharsBack($arResult['STR_NAV']) : ''?>
</div><!-- /.b-filter_list_wrapper -->
