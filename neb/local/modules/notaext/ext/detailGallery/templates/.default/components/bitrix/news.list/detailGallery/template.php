<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?#$APPLICATION->AddHeadString('<link rel="stylesheet" href="'.$templateFolder.'/jcarousel.basic.css">',true)?>
<?
echo '<link rel="stylesheet" href="'.$templateFolder.'/jcarousel.basic.css">';
?>

<?#$APPLICATION->AddHeadString('<script>window.jQuery || document.write(\'<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">\x3C/script>\')</script>',true)?>
<?='<script>window.jQuery || document.write(\'<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">\x3C/script>\')</script>'?>
<?#$APPLICATION->AddHeadString('<script type="text/javascript" src="'.$templateFolder.'/jquery.jcarousel.min.js"></script>',true)?>
<?='<script type="text/javascript" src="'.$templateFolder.'/jquery.jcarousel.min.js"></script>'?>
<?#$APPLICATION->AddHeadString('<script type="text/javascript" src="'.$templateFolder.'/jcarousel.basic.js"></script>',true)?>
<?='<script type="text/javascript" src="'.$templateFolder.'/jcarousel.basic.js"></script>'?>
<?
$rand= randString(6);
?>
<style type="text/css">
	.wrapper<?=$rand?> {
		max-width: <?=$arParams['IMG_WIDTH']?>px;
		padding: 0 20px 40px 20px;
		margin: auto;
	}
</style>
<?
	#print_r($arResult);

	if(!empty($arResult['ITEMS']))
	{
	?>
	<div class="wrapper<?=$rand?>">
		<div class="jcarousel-wrapper">
			<div class="jcarousel">
				<ul><?
					foreach($arResult['ITEMS'] as $arItem){
						$file = CFile::ResizeImageGet($arItem['PROPERTIES']['REAL_PICTURE']['VALUE'], array('width'=>$arParams['IMG_WIDTH'], 'height'=>$arParams['IMG_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					?><li><img src="<?=$file['src']?>" alt="<?=$arItem['NAME']?>"/></li><?
					}
				?></ul>
			</div>

			<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
			<a href="#" class="jcarousel-control-next">&rsaquo;</a>

			<p class="jcarousel-pagination">

			</p>
		</div>
	</div>


	<?
	}
?>