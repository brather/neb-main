<?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CModule::IncludeModule('iblock');

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-" => " "));

// Получаем список доступных инфоблоков
$arrIBlock = array();
$iBlockRes = CIBlock::GetList(array('sort' => 'asc'));
while ($iBlockItem = $iBlockRes->Fetch())
    $arrIBlock[$iBlockItem['IBLOCK_TYPE_ID']][$iBlockItem['ID']] = '[' . $iBlockItem['ID'] . '] ' . $iBlockItem['NAME'];

/*
Устанавливаем табы и группы
*/
$arOptions = array(
    'arTabs' => array(
        array(
            'DIV' => 'edit1',
            'TAB' => Loc::getMessage('TAB_TITLE_SETTING'),
            'ICON' => '',
            'TITLE' => Loc::getMessage('TAB_TITLE_SETTING')
        )
    ),
    'arGroups' => array(
        'IB_SAVE' => array('TITLE' => Loc::getMessage('GROUP_SAVE_FOTO'), 'TAB' => '0', 'SORT' => 10),
        'TAGS' => array('TITLE' => Loc::getMessage('GROUP_TAGS'), 'TAB' => '0', 'SORT' => 20),
        'UPLOAD' => array('TITLE' => Loc::getMessage('GROUP_UPLOAD'), 'TAB' => '0', 'SORT' => 30),
        'OTHER' => array('TITLE' => Loc::getMessage('GROUP_OTHER'), 'TAB' => '0', 'SORT' => 40)

    ),
);

/*	IB_SAVE */
$arOptions['arOptions']['save_ib_type'] = array
(
    'GROUP' => 'IB_SAVE',
    'TITLE' => Loc::getMessage("IBLOCK_TYPE"),
    'TYPE' => 'SELECT',
    'VALUES' => \CExtOptions::prepareArrayForSelect($arTypesEx),
    'SORT' => 10,
    //'NOTES' 	=> GetMessage("IBLOCK_TYPE"),
    'REFRESH' => 'Y'
);

$iBTypeSave = \COption::GetOptionString('notaext', 'detailGallery_save_ib_type');
if (!empty($arrIBlock[$iBTypeSave])) {
    $arOptions['arOptions']['save_ib'] = array
    (
        'GROUP' => 'IB_SAVE',
        'TITLE' => Loc::getMessage("IBLOCK"),
        'TYPE' => 'SELECT',
        'VALUES' => \CExtOptions::prepareArrayForSelect($arrIBlock[$iBTypeSave]),
        'SORT' => 20,
        'REFRESH' => 'Y'
    );
}

$iBIdSave = \COption::GetOptionString('notaext', 'detailGallery_save_ib');
if (intval($iBIdSave) > 0) {

    $arOptions['arOptions']['save_sections'] = array
    (
        'GROUP' => 'IB_SAVE',
        'TITLE' => Loc::getMessage("SAVE_SECTIONS"),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'Y',
        'SORT' => 25,
    );


    $arFieldsForEdit = array(
        'NAME' => '[NAME] ' . Loc::getMessage('SHOW_FIELD_NAME'),
        'CODE' => '[CODE] ' . Loc::getMessage('SHOW_FIELD_CODE'),
        'PREVIEW_TEXT' => '[PREVIEW_TEXT] ' . Loc::getMessage('SHOW_FIELD_PREVIEW_TEXT')
    );

    $propsResult = CIBlockProperty::GetList(
        array('SORT' => 'ASC'),
        array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $iBIdSave,
            'PROPERTY_TYPE' => 'S'
        )
    );

    while ($iBlockPropItem = $propsResult->Fetch())
        $arFieldsForEdit['PROPERTY_' . $iBlockPropItem['CODE']] = '[PROPERTY_' . $iBlockPropItem['CODE'] . '] ' . $iBlockPropItem['NAME'];

    $propsResult = CIBlockProperty::GetList(
        array('SORT' => 'ASC'),
        array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $iBIdSave,
            'PROPERTY_TYPE' => 'L'
        )
    );

    while ($iBlockPropItem = $propsResult->Fetch())
        $arFieldsForEdit['PROPERTY_' . $iBlockPropItem['CODE']] = '[PROPERTY_' . $iBlockPropItem['CODE'] . '] ' . $iBlockPropItem['NAME'];

    $propsResult = CIBlockProperty::GetList(
        array('SORT' => 'ASC'),
        array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $iBIdSave,
            'PROPERTY_TYPE' => 'E'
        )
    );

    $arPropertyE = array();
    while ($iBlockPropItem = $propsResult->Fetch()) {
        $arFieldsForEdit['PROPERTY_' . $iBlockPropItem['CODE']] = '[PROPERTY_' . $iBlockPropItem['CODE'] . '] ' . $iBlockPropItem['NAME'];
        $arPropertyE['PROPERTY_' . $iBlockPropItem['CODE']] = '[PROPERTY_' . $iBlockPropItem['CODE'] . '] ' . $iBlockPropItem['NAME'];
    }

    $arOptions['arOptions']['save_fields'] = array
    (
        'GROUP' => 'IB_SAVE',
        'TITLE' => Loc::getMessage("SHOW_EDIT_FIELDS"),
        'TYPE' => 'MSELECT',
        'SIZE' => 3,
        'VALUES' => \CExtOptions::prepareArrayForSelect($arFieldsForEdit),
        'SORT' => 30,
    );

    $arOptions['arOptions']['save_fields_group'] = array
    (
        'GROUP' => 'IB_SAVE',
        'TITLE' => Loc::getMessage("SHOW_EDIT_GROUPS"),
        'TYPE' => 'MSELECT',
        'SIZE' => 3,
        'VALUES' => \CExtOptions::prepareArrayForSelect($arFieldsForEdit),
        'SORT' => 40,
    );

    $arOptions['arOptions']['save_field_tags'] = array
    (
        'GROUP' => 'IB_SAVE',
        'TITLE' => Loc::getMessage("FIELD_TAGS"),
        'TYPE' => 'SELECT',
        'VALUES' => \CExtOptions::prepareArrayForSelect(array_merge(array('TAGS' => 'TAGS'), $arPropertyE)),
        'SORT' => 50,
    );

}

/* Upload	*/

$arOptions['arOptions']['upload_ext'] = array
(
    'GROUP' => 'UPLOAD',
    'TITLE' => Loc::getMessage("UPLOAD_EXT"),
    'TYPE' => 'STRING',
    'SIZE' => 30,
    'DEFAULT' => 'jpg,jpeg',
    'SORT' => 10,
);
$arOptions['arOptions']['upload_max_size_file'] = array
(
    'GROUP' => 'UPLOAD',
    'TITLE' => Loc::getMessage("UPLOAD_MAX_SIZE_FILE"),
    'TYPE' => 'INT',
    'SIZE' => 10,
    'DEFAULT' => '30',
    'SORT' => 20,
);

$arOptions['arOptions']['upload_quality_file'] = array
(
    'GROUP' => 'UPLOAD',
    'TITLE' => Loc::getMessage("UPLOAD_QUALITY_FILE"),
    'TYPE' => 'INT',
    'SIZE' => 10,
    'DEFAULT' => '90',
    'SORT' => 30,
);

/* OTHER	*/

$arOptions['arOptions']['save_original'] = array
(
    'GROUP' => 'OTHER',
    'TITLE' => Loc::getMessage("SAVE_ORIGINAL"),
    'TYPE' => 'CHECKBOX',
    'SORT' => 10,
);

$arOptions['arOptions']['default_w'] = array
(
    'GROUP' => 'OTHER',
    'TITLE' => Loc::getMessage("DEFAULT_W"),
    'TYPE' => 'INT',
    'SIZE' => 7,
    'DEFAULT' => 2000,
    'SORT' => 20,
    'NOTES' => Loc::getMessage('DEF_W_NOTE')
);

$arOptions['arOptions']['default_h'] = array
(
    'GROUP' => 'OTHER',
    'TITLE' => Loc::getMessage("DEFAULT_H"),
    'TYPE' => 'INT',
    'SIZE' => 7,
    'DEFAULT' => 2000,
    'SORT' => 30,
    'NOTES' => Loc::getMessage('DEF_H_NOTE')
);

$arOptions['arOptions']['show_component'] = array
(
    'GROUP' => 'OTHER',
    'TITLE' => Loc::getMessage("SHOW_COMPONENT"),
    'TYPE' => 'STRING',
    'DEFAULT' => 'notaext:detail.gallery.inside',
    'SIZE' => 40,
    'SORT' => 40,
);
