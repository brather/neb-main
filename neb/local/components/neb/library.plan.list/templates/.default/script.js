$(function(){

    var speedAnimation = 300,
        btnPlanChange = $('.js-plan-change'),
        btnPlanDelete = $('.js-plan-delete');

    //region Изменение плана
    btnPlanChange.on('click', function(e){
        e.preventDefault();
        startLoader('body');
        var $this = $(this),
            thisForm = $this.closest('form'),
            collapseContain = thisForm.closest('.collapse'),
            titlePlan = collapseContain.closest('dl').find('[href=#' + collapseContain.attr('id') + ']').find('span'),
            titleDate = collapseContain.closest('dl').find('[href=#' + collapseContain.attr('id') + ']').find('small'),
            langDate = $this.data('lang-date-f-plan'),
            preffixPlan = $this.data('preffix'),
            newStatus = thisForm.find('[name=' + preffixPlan + 'DATE_EDIT_UF_REQUEST_STATUS]').val(), // delete..
            objData = {
                DATA_PLAN_UPDATE_AJAX   : 'Y',
                DATA_PLAN_ID            : $this.data('plan-id'),
                DATA_PLAN_NAME          : $this.data('plan-name'),
                DATA_UF_LIBRARY         : $this.data('plan-library'),
                DATA_PLAN_YEAR          : $this.data('plan-year'),
                DATA_PLAN_STATUS        : $this.data('plan-status'),
                DATA_PLAN_DATE_F        : thisForm.find('[name=' + preffixPlan + 'DATA_EDIT_UF_DATE_F_PLAN]').val(),
                DATA_BOOK_STATUS        : $('#' + preffixPlan + 'ps-' + newStatus).val(),
                SET_STATUS_BOOK         : 'Y'
            },
            blockLoadBooks = $('#load-book-in-' + objData.DATA_PLAN_ID),
            tableEditions = $('#tab-plan-' + objData.DATA_PLAN_ID),
            tdDateFinish = tableEditions.find('.plan-' + objData.DATA_PLAN_ID + '-date-finish'),
            dataUrlReload = '';//thisForm.find('[name=' + preffixPlan + 'DATE_EDIT_UF_REQUEST_STATUS]').find('option:selected').data('url-reload');
        //region Скрытие блока загрузки изображений при измененнии статуса
        if(newStatus != '') {
            blockLoadBooks.hide(speedAnimation);
            if(tableEditions.find('.js-delete-row').length > 0) {
                tableEditions.find('.js-delete-row').hide(speedAnimation);
            }
        } else {
            blockLoadBooks.show(speedAnimation);
            tableEditions.find('.js-delete-row').show(speedAnimation);
        }
        //endregion
        $.ajax({
            url : thisForm.data('ajax'),
            type : thisForm.attr('method'),
            data : objData,
            success : function(data) {
                titleDate.html('(' + langDate + data.DATA_PLAN_DATE_F + ')');
                tdDateFinish.html(data.DATA_PLAN_DATE_F);
                if(data.DATA_PLAN_NAME) {
                    titlePlan.html(data.DATA_PLAN_NAME);
                }
                if(dataUrlReload != '') {
                    window.location.replace(dataUrlReload);
                } else {
                    stopLoader();
                }
            }
        });
    });
    //endregion

    //region Удаление плана
    btnPlanDelete.on('click', function(e){
        e.preventDefault();

        var $this = $(this),
            toggler = $(e.target),
            message = {
                title: 'Удалить?',
                text: $this.data('mess-confirm'),
                confirmTitle: "Удалить"
            };

        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed) {
                startLoader('body');
                var thisForm = $this.closest('form'),
                    containerPlan = thisForm.closest('dl'),
                    objData = {
                        DATA_PLAN_DELETE_AJAX : 'Y',
                        DATA_PLAN_ID : $this.data('plan-id')
                    };
                $.ajax({
                    url : thisForm.data('ajax'),
                    type : thisForm.attr('method'),
                    data : objData,
                    dataType: 'json',
                    success : function(data) {
                        stopLoader();
                        if(data.ERROR) {
                            alert(data.ERROR);
                        } else {
                            containerPlan.hide('slow', function(){
                                containerPlan.remove();
                            });
                        }
                    }
                });
            }
        });
    });
    //endregion
    //region Прокрутка страницы до активного блока с планом
    var $tabActive = $('.js-plan-active').closest('.panel');
    if($tabActive.length > 0) {
        var tabOffset = $tabActive.offset().top;
        setTimeout(function(){
            $('body').scrollTo(tabOffset, speedAnimation);
        }, 200);
    }
    //endregion
});
//region Функция запуска лоадера
function startLoader(block) {
    stopLoader();
    var blkLoader = '<div id="blk-loader" class="blk-milk-shadow">' +
        '<i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i>' +
        '</div>';
    $(block).append(blkLoader);
}
//endregion
//region Функция остановки лоадера
function stopLoader() {
    $('#blk-loader').animate({ 'opacity' : 0 }, 300, function(){
        $(this).remove();
    });
}
//endregion