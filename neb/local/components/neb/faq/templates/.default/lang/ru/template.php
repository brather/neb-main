<?php
$MESS["FAQ_ADD_ELEMENT"] = "Добавить элемент";
$MESS["FAQ_EDIT_ELEMENT"] = "Редактировать элемент";
$MESS["FAQ_ADD_SECTION"] = "Добавить раздел";
$MESS["FAQ_EDIT_SECTION"] = "Редактировать раздел";
$MESS["FAQ_ADD_ITEM"] = "Добавить";
$MESS["FAQ_ADD"] = "Добавление";
$MESS["FAQ_EDIT"] = "Редактирование";