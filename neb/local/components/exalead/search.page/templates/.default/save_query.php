<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized())
    exit();

use \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\Main\Type\DateTime,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule("highloadblock");

$saveId = intval($_REQUEST['saveId']);

if ($_SERVER['REQUEST_METHOD'] == 'POST' and $saveId > 0) {

    $hlblock = HighloadBlockTable::getById(HIBLOCK_SEARCHSERS_USERS)->fetch();
    $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
    $rsData = $entity_data_class::getList(array(
        "select" => array("ID"),
        "filter" => array('ID' => $saveId, 'UF_UID' => $USER->GetID())
    ));

    if ($arData = $rsData->Fetch()) {
        $result = $entity_data_class::update($arData['ID'], array('UF_NAME' => htmlspecialcharsbx(trim($_REQUEST['name']))));
    }
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' and check_bitrix_sessid() and !empty($_REQUEST['url'])) {
    if (!saveQuery::isSave())
        exit();

    $hlblock = HighloadBlockTable::getById(HIBLOCK_SEARCHSERS_USERS)->fetch();
    $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

    $saveID = 0;

    $rsData = $entity_data_class::getList(array(
        "select" => array("ID"),
        "filter" => array('UF_MD5' => md5(htmlspecialcharsbx($_REQUEST['exalead_param'])), 'UF_UID' => $USER->GetID())
    ));

    if ($arData = $rsData->Fetch()) {
        $saveID = $arData['ID'];
    } else {
        $dt = new DateTime();

        $arFields = array(
            'UF_DATE_ADD' => $dt,
            'UF_FOUND' => (int)$_REQUEST['found'],
            'UF_QUERY' => htmlspecialcharsbx($_REQUEST['q']),
            'UF_MORE_OPTIONS' => htmlspecialcharsbx($_REQUEST['more_options']),
            'UF_URL' => htmlspecialcharsbx($_REQUEST['url']),
            'UF_UID' => $USER->GetID(),
            'UF_EXALEAD_PARAM' => htmlspecialcharsbx($_REQUEST['exalead_param']),
            'UF_MD5' => md5(htmlspecialcharsbx($_REQUEST['exalead_param'])),
        );

        $result = $entity_data_class::add($arFields);
        $saveID = $result->getId();
    }

    MainHelper::showJson(array('ID' => $saveID));
}