$(function () {

	// $('#requeststring').focus();
	
	// переключение по радиокнопкам разделов поиска
	drawExtFields( $('form.extendedSearch [type="radio"][name="librarytype"]:checked').val() );
	$(document).on('change','[type="radio"][name="librarytype"]',function(){
		drawExtFields( $(this).val() );
	});
	function drawExtFields(fLibrarytype) {
		if (!fLibrarytype) {
			fLibrarytype = $('form.extendedSearch [type="radio"][name="librarytype"]').val();
		}
		// переключение полей формы расширенного поиска
		var fAccess;
		$('.extendedSearch dt').each(function () {
			fAccess = false;
			if ($(this).hasClass('always') || $(this).hasClass(fLibrarytype))
				fAccess = true;

			$(this).css('display', fAccess ? 'block' : 'none')
				.next("dd").css('display', fAccess ? 'block' : 'none');

			$.each( $(this).next("dd").find('.form-control'), function() {
				$(this).attr('name', fAccess ? $(this).attr('data-name') : '');

				var phFormControl =  $(this).attr('data-placeholder-' + fLibrarytype);
				$(this).attr('placeholder', phFormControl != undefined ? phFormControl : '');
			});
		});
	}

	// селектор коллекций
	var collections = $('select#collection'),
		title = collections.attr('langcollection') || 'Выберите коллекцию';
	if ($.fn.selectpicker) {
        collections.selectpicker({
        	title:title,
        	selectAllCheckbox:true
        });
    }

    $(document).on('click','[type="reset"]',function(e){
    	var sels = $(this).closest('form').find('.bs-select-hidden');
    	setTimeout(function(){
    		sels.selectpicker('refresh');
    	},100);
    	if (document.location.search) {
    		document.location.href = '/search/extended/';
    	}
    });

	// иерархические чекбоксы ББК

	// генерация хидден-инпутов из адресной строки взамен отсутствующих чекбоксов из раздела "рубрики"
	var search = location.search.substring(1);
	if (search){
		var getStringParamsObj = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"').replace(/\+/g, ' ').replace(/%2F/g, '/') + '"}'),
			bbkParams = {}, // 
			bbkIdx = {},
			searchFormExtended = $('form[data-three-form]'),
			hiddenEl = '';
		for (var param in getStringParamsObj) {
			if ( param.indexOf('bbkfull') != -1 ) {
				bbkParams[param] = {
					"id": param.substring(8).slice(0,-1),
					"title":getStringParamsObj[param],
				} 
				bbkIdx[param.substring(8).slice(0,-1)] = {
					"title":getStringParamsObj[param],
					"name": getStringParamsObj[param]
				}
			}
	    }
	    for (var fieldKey in bbkParams) {
	    	hiddenEl = $('<input type="hidden" name="' + 'bbkfull[' + bbkParams[fieldKey]['id'] + ']' + '" value="' + bbkParams[fieldKey]['title'] + '" class="generated-instead-of-bbk-checkboxes"/>');
	    	searchFormExtended.append(hiddenEl);
	    }
	}

	$(document).on('click','#accordion [data-toggle]',function(){
		if ( $(this)[0].hasAttribute('data-once-opened') ) {
			return
		} else {
			$(this).attr('data-once-opened','true');
		}
		var toggler = $(this),
			controlsContainer = $(toggler).closest('#accordion').find('#collapseOne > .panel-body'),
			controls = $('[type="checkbox"]',controlsContainer),
			queue;
		if (controls.length) {
			queue = controls.slice();
		}

		queue = filterQueue(queue);
		$(queue.first()).each(function(){
			convertHidden(this)
		});

		function convertHidden(toActiveCheckbox) {
			var $this = $(toActiveCheckbox),
				value = $this.data('id'),
				treenum = $this.data('treenum') + 1;			
			$this.prop('checked',true);

			$.ajax({
				url: location.origin + '/local/components/exalead/search.form/bbk.php',
				data:{id: value},
				dataType: 'json',
				beforeSend: function () {
					FRONT.spinner.execute( $('#sc' + value).closest('div').get(0) , FRONT.spinner.mini );
				},
				success: function (res) {
					$('#sc' + value).closest('div').find('.spinner').detach();
					for (var key in res) {
						var val = res[key].replace(/(.+)\//, '');
						var html = '<div>'+
							'<label class="for" for="sc' + key + '">'+
								'<input value="' + res[key] + '"'+
								' data-id="' + key + '"'+
								' id="sc' + key + '"'+
								' type="checkbox" class="form-control"'+
								' name="bbkfull[' + key + ']"'+
								' data-name="bbkfull"'+
								' aria-labelledby="sc' + key + 'label"'+
								' data-treenum="' + treenum + '" />'+
								'<span class="lbl">'+
									'<span  aria-hidden="true">' + val + '</span>'+
									'<span class="screen-hide" id="sc' + key + 'label">' + val + '</span>'+
								'</span>'+
							'</label>'+
							'<div class="treenum" style="margin-left: 30px"></div>'+
						'</div>';
						// console.log('сначала убираем хиден инпут',key);
						$('[name="bbkfull[' + key + ']"]',searchFormExtended).detach();
						// console.log(key,'потом добавляем чекбокс');
						$('#sc' + value).parent().siblings('div.treenum').append(html);
						$.merge(queue, filterQueue( $('#sc' + value).parent().siblings('div.treenum').find('[type="checkbox"]') ) );
					}
					queue = $(queue).not($this);
					if (queue.length) {
						$(queue.first()).each(function(){
							convertHidden(this)
						})
					}
				}
			});
			// $this.trigger('change.unserviced');
		}
		function filterQueue(queue) {
			var queue = $(queue);
			var obj = $();
			queue.each(function(i,item){
				for (var filter in bbkParams) {
					if ( ('sc'+ bbkParams[filter]['id'].toString()) == $(this).attr('id') ) {
						$.merge(obj,$(item));
					}
				}
			});
			return obj
		}
	});

	$(document).on('change.unserviced', 'input[id^=sc]', function(event){
		if (event.namespace == 'unserviced') {
			return
		}
		var $this = $(this),
			value = $this.data('id'),
			treenum = $this.data('treenum') + 1;

		if (this.checked === true){
			$.ajax({
				url: location.origin + '/local/components/exalead/search.form/bbk.php',
				data:{id: value},
				dataType: 'json',
				beforeSend: function () {
					// $('#sc' + value).parent().siblings('div.treenum').html("<img src='/local/components/exalead/search.form/templates/redesign_extended/img/loader.gif' />");
					FRONT.spinner.execute( $('#sc' + value).closest('div').get(0) , FRONT.spinner.mini );
				},
				success: function (res) {
					// $('#sc' + value).parent().siblings('div.treenum').html("");
					$('#sc' + value).closest('div').find('.spinner').detach();
					for (var key in res) {
						var val = res[key].replace(/(.+)\//, '');
						var html = '<div><label class="for" for="sc' + key + '"><input value="' + res[key] + '" data-id="' + key + '" id="sc' + key + '" type="checkbox" class="form-control" name="bbkfull[' + key + ']" data-name="bbkfull" aria-labelledby="sc' + key + 'label" data-treenum="' + treenum + '" /><span class="lbl"><span  aria-hidden="true">' + val + '</span><span class="screen-hide" id="sc' + key + 'label">' + val + '</span></span></label><div class="treenum" style="margin-left: 30px"></div></div>';
						$('#sc' + value).parent().siblings('div.treenum').append(html);
					}
				}
			});
		} else {
			$('#sc' + value).parent().siblings('div.treenum').html("");
		}
	});

	// стилизация select для языков
	$('#lang').selectpicker({liveSearch: true, size:10, dropupAuto: false});

	$('[data-placeholder]').each(function(){
		$(this).attr('placeholder', $(this).attr('data-placeholder-library') );
	});
});