<?php
$MESS['FEEDBACK_STAT_TITLE']      = 'Статистика по количеству поступивших, отработанных и находящихся в работе заявок';
$MESS['FEEDBACK_STAT_DATE_FROM']  = 'Начало периода';
$MESS['FEEDBACK_STAT_DATE_TO']    = 'Конец периода';
$MESS['FEEDBACK_STAT_PERIOD']     = 'Статистика за период';
$MESS['FEEDBACK_STAT_REGISTERED'] = 'Подано заявок';
$MESS['FEEDBACK_STAT_CLOSED']     = 'Закрыто заявок';
$MESS['FEEDBACK_STAT_PROCESSED']  = 'Заявок в работе';
$MESS['FEEDBACK_STAT_HISTORY']    = '* история статусов "в работе" ведется с 22.08.2016';
$MESS['FEEDBACK_STAT_EXCEL']      = 'Выгрузить в .xls';
$MESS['FEEDBACK_STAT_SHOW']       = 'Показать статистику';
