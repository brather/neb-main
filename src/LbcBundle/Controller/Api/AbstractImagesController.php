<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 13:37
 */
namespace LbcBundle\Controller\Api;

use Doctrine\ORM\EntityRepository;
use LbcBundle\Controller\Traits\DoctrineInjectTrait;
use LbcBundle\Controller\Traits\ServiceContainerInjectTrait;
use LbcBundle\Entity\AbstractLbcImages;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use JMS\DiExtraBundle\Annotation as DI;


abstract class AbstractImagesController
{
    use DoctrineInjectTrait, ServiceContainerInjectTrait;

    /**
     *
     * @param int $id
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     *
     * @return array
     */
    public function imagesAction($id)
    {
        $repository = $this->getRepository();
        $id = (integer)$id;
        if ($id < 1) {
            throw new BadRequestHttpException('Empty ' . $this->getOwnFieldName());
        }
        $condition = [];
        $condition[$this->getOwnFieldName()] = $id;
        $entities = $repository->findBy($condition);
        $this->prepareImagePath($entities);

        return [
            'entities' => $entities,
        ];
    }

    /**
     * @param $entities
     */
    protected function prepareImagePath($entities)
    {
        $uploadDir = $this->container->getParameter('neb.upload');
        $uploadDir .= 'bbk/';
        /** @var AbstractLbcImages $entity */
        foreach ($entities as $entity) {
            $imagePath = $entity->getImagepath();
            $imagePath = str_replace('\\', '/', $imagePath);
            if (0 === strpos($imagePath, '/')) {
                $imagePath = substr($imagePath, 1);
            }
            $rootName = mb_substr($imagePath, 0, mb_strpos($imagePath, '/'));
            $imagePath = rawurlencode($rootName) . mb_substr($imagePath, mb_strpos($imagePath, '/'));
            $imagePath = str_replace('.JPG', '.jpg', $imagePath);
            $entity->setImagepath($uploadDir . $imagePath);
        }
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->doctrine->getManager()->getRepository($this->getEntityName());
    }

    /**
     * @return string
     */
    abstract public function getEntityName();

    /**
     * @return string
     */
    abstract public function getOwnFieldName();
}