<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CModule::IncludeModule('iblock');
CModule::IncludeModule('nota.library');

use Nota\Library;

$sTableID = 'tbl_library_users';

$oSort = new CAdminSorting($sTableID, "NAME", "desc");
$arOrder = (strtoupper($by) === "ID"? array($by => $order): array($by => $order, "ID" => "ASC"));
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = Array(
    "find",
    "find_type",
    "find_id",
    "find_timestamp_1",
    "find_timestamp_2",
    "find_last_login_1",
    "find_last_login_2",
    "find_active",
    "find_login",
    "find_name",
    "find_email",
    "find_keywords",
    "find_group_id"
);

$lAdmin->InitFilter($arFilterFields);

    $arFilter = Array(
        "ID" => $find_id,
        "TIMESTAMP_1" => $find_timestamp_1,
        "TIMESTAMP_2" => $find_timestamp_2,
        "LAST_LOGIN_1" => $find_last_login_1,
        "LAST_LOGIN_2" => $find_last_login_2,
        "ACTIVE" => $find_active,
        "LOGIN" => ($find!='' && $find_type == "login"? $find: $find_login),
        "NAME" => ($find!='' && $find_type == "name"? $find: $find_name),
        "EMAIL" => ($find!='' && $find_type == "email"? $find: $find_email),
        "KEYWORDS" => $find_keywords,
        "GROUPS_ID" => $find_group_id,
        "UF_LIBRARY" => CNotaLibrary::getLibraryID()
    );

TrimArr($arFilter);



$arHeader = array(
    array(
        'id'      => 'ID',
        'content' => "ID",
        'sort'    => 'ID',
        'default' => true
    ),
    array(
        'id'      => 'LOGIN',
        'content' => "LOGIN",
        'sort'    => 'LOGIN',
        'default' => true
    ),
    array(
        'id'      => 'EMAIL',
        'content' => "EMAIL",
        'sort'    => 'EMAIL',
        'default' => true
    ),
    array(
        'id'      => 'NAME',
        'content' => "Имя",
        'sort'    => 'NAME',
        'default' => true
    ),
    array(
        'id'      => 'LAST_NAME',
        'content' => "Фамилия",
        'sort'    => 'LAST_NAME',
        'default' => true
    ),
);

if($lAdmin->EditAction())
{
    $editableFields = array(
        "ACTIVE"=>1, "LOGIN"=>1, "TITLE"=>1, "NAME"=>1, "LAST_NAME"=>1, "SECOND_NAME"=>1, "EMAIL"=>1, "PERSONAL_PROFESSION"=>1,
        "PERSONAL_WWW"=>1, "PERSONAL_ICQ"=>1, "PERSONAL_GENDER"=>1, "PERSONAL_PHONE"=>1, "PERSONAL_MOBILE"=>1,
        "PERSONAL_CITY"=>1, "PERSONAL_STREET"=>1, "WORK_COMPANY"=>1, "WORK_DEPARTMENT"=>1, "WORK_POSITION"=>1,
        "WORK_WWW"=>1, "WORK_PHONE"=>1, "WORK_CITY"=>1, "XML_ID"=>1,
    );

    foreach($_POST["FIELDS"] as $ID => $arFields)
    {
        $ID = intval($ID);

        if(!$lAdmin->IsUpdated($ID))
            continue;

        foreach($arFields as $key => $field)
        {
            if(!isset($editableFields[$key]) && strpos($key, "UF_") !== 0)
            {
                unset($arFields[$key]);
            }
        }


        $DB->StartTransaction();

        $ob = new CUser;
        if(!$ob->Update($ID, $arFields))
        {
            $lAdmin->AddUpdateError(GetMessage("SAVE_ERROR").$ID.": ".$ob->LAST_ERROR, $ID);
            $DB->Rollback();
        }

        $DB->Commit();
    }
}

if(($arID = $lAdmin->GroupAction()) && CNotaLibrary::isAdmin())
{
    if($_REQUEST['action_target']=='selected')
    {
        $arID = Array();
        $rsData = CUser::GetList($by, $order, $arFilter);
        while($arRes = $rsData->Fetch())
            $arID[] = $arRes['ID'];
    }

    $gr_id = intval($_REQUEST['groups']);
    $struct_id = intval($_REQUEST['UF_DEPARTMENT']);

    foreach($arID as $ID)
    {
        $ID = intval($ID);
        if($ID <= 1)
            continue;

        $arGroups = array();
        $res = CUser::GetUserGroupList($ID);
        while($res_arr = $res->Fetch())
            $arGroups[intval($res_arr["GROUP_ID"])] = array("GROUP_ID"=>$res_arr["GROUP_ID"], "DATE_ACTIVE_FROM"=>$res_arr["DATE_ACTIVE_FROM"], "DATE_ACTIVE_TO"=>$res_arr["DATE_ACTIVE_TO"]);

        if(isset($arGroups[1]) && !$USER->CanDoOperation('edit_php')) // not admin can't edit admins
            continue;

        if(!$USER->CanDoOperation('edit_all_users') && $USER->CanDoOperation('edit_subordinate_users') && count(array_diff(array_keys($arGroups), $arUserSubordinateGroups))>0)
            continue;

        switch($_REQUEST['action'])
        {
            case "delete":
                @set_time_limit(0);
                $DB->StartTransaction();
                if(!CUser::Delete($ID))
                {
                    $DB->Rollback();
                    $err = '';
                    if($ex = $APPLICATION->GetException())
                        $err = '<br>'.$ex->GetString();
                    $lAdmin->AddGroupError(GetMessage("DELETE_ERROR").$err, $ID);
                }
                $DB->Commit();
                break;
            case "activate":
            case "deactivate":
                $ob = new CUser();
                $arFields = Array("ACTIVE"=>($_REQUEST['action']=="activate"?"Y":"N"));
                if(!$ob->Update($ID, $arFields))
                    $lAdmin->AddGroupError(GetMessage("MAIN_EDIT_ERROR").$ob->LAST_ERROR, $ID);
                break;
        }
    }
}


$lAdmin->AddHeaders($arHeader);

$arSelect = $lAdmin->GetVisibleHeaderColumns();
if(!in_array('ID', $arSelect))
    $arSelect[] = 'ID';

$rsData = CUser::GetList($by, $order, $arFilter, array(
    "SELECT" => $lAdmin->GetVisibleHeaderColumns(),
    "NAV_PARAMS"=> array("nPageSize"=>CAdminResult::GetNavSize($sTableID)),
));

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(''));

while ($arRes = $rsData->NavNext())
{
    $row = $lAdmin->AddRow($arRes['ID'], $arRes);

    $row->AddInputField('NAME', array('size' => 50));
    $row->AddInputField('LAST_NAME', array('size' => 50));
    $row->AddInputField('LOGIN', array('size' => 50));
    $row->AddInputField('EMAIL', array('size' => 50));

    $row->AddViewField('ID', '<a href="nota_library_user_edit.php?ID='. $arRes['ID'] .'&lang='. LANGUAGE_ID .'">'. $arRes['ID'] .'</a>');

    $row->AddActions(
        array(
            array(
                'ICON'   => 'edit',
                'TEXT'   => 'Редактировать',
                'ACTION' => $lAdmin->ActionRedirect('nota_library_user_edit.php?ID='.$arRes['ID'].'&lang='.LANGUAGE_ID),
                "DEFAULT"=>true
            ),
            array(
                'ICON'   => 'delete',
                'TEXT'   => 'Удалить',
                "ACTION" =>"if(confirm('".GetMessageJS("NOTA_COLLECTION_DELETE_CONFIRM")."')) ".$lAdmin->ActionDoGroup($arRes['ID'], "delete"),
            )
        )
    );
}


$lAdmin->AddAdminContextMenu(
    array(
        array(
            'TEXT'  => 'Добавить пользователя',
            'LINK'  => 'nota_library_user_edit.php?lang=' . LANGUAGE_ID,
            'TITLE' => 'Добавить пользователя',
            'ICON'  => 'btn_new',
        )
    )
);

$lAdmin->AddGroupActionTable(array('delete' => 'удалить'));

$lAdmin->CheckListMode();
$APPLICATION->SetTitle('Пользователи библиотеки');

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';
?>
<form method="GET" action="" name="find_form">
    <input type="hidden" name="filter" value="Y">
    <?
    $oFilter = new CAdminFilter(
        $sTableID."_filter",
        array(
            "ID",
            "NAME"
        )
    );

    $oFilter->Begin();
    ?>
    <tr>
        <td><b>Имя</b></td>
        <td><input type="text" name="find_name" value="<?=htmlspecialcharsbx($find_name)?>" size="40"></td>
    </tr>
    <tr>
        <td>ID:</td>
        <td><input type="text" name="find_id" value="<?=htmlspecialcharsbx($find_id)?>" size="15"></td>
    </tr>
    <?
    $oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPageParam(), "form"=>"find_form"));
    $oFilter->End();
    ?>
</form>
<?
$lAdmin->DisplayList();
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
?>
