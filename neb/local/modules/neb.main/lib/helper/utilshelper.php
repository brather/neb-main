<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 27.03.2017
 * Time: 13:03
 */

namespace Neb\Main\Helper;

use \Bitrix\Main\Application;
use \Bitrix\Main\Data\Cache;
use \Bitrix\Main\Page\Asset;

/**
 * Class UtilsHelper - Набор полезных утилит для НЭБа
 *
 * @package Neb\Main\Helper
 */
class UtilsHelper
{
    /**
     * Определяет, выпущен ли текущий браузер Microsoft
     *
     * @param bool $bCheckEdge - проверять на новый браузер Edge
     * @return bool
     */
    public static function isMicrosoftBrowser($bCheckEdge = false)
    {
        $sUserAgent = strtolower($_SERVER['HTTP_USER_AGENT']);

        $sMsAgents = 'msie|internet explorer|trident' . ($bCheckEdge ? '|edge' : ''); // IE and Edge user agents

        $bIE = preg_match('#(' . $sMsAgents . ')#', $sUserAgent, $version) && strpos($sUserAgent, "opera") == false;

        return $bIE;
    }

    /**
     * Преобразование xml (-> SimpleXMLElement -> json) и json в массив
     *
     * @param $sStr
     * @return mixed
     */
    public static function convertXmlJsonToArray($sStr) {

        // конвертирует xml -> SimpleXMLElement -> json
        if (false !== stripos($sStr, 'xml') && function_exists('simplexml_load_string')) {
            $sStr = simplexml_load_string($sStr, "SimpleXMLElement", LIBXML_NOCDATA);
            $sStr = json_encode($sStr);
        }

        // конвертирует json -> array
        $sStr = json_decode($sStr, true);

        return $sStr;
    }

    /**
     * Устанавливает канонический URL
     *
     * @param $sUrl
     */
    public static function setCanonicalUrl($sUrl) {

        if (empty($sUrl) || !defined('URL_SCHEME') || !defined('SITE_HOST'))
            return;

        Asset::getInstance()->addString(
            '<link rel="canonical" href="' . URL_SCHEME . '://' . SITE_HOST . $sUrl . '" />', true
        );
    }

    /**
     * Защищает e-mail от сканирования спам-роботами
     * 
     * @param $sEmail
     * @return string
     */
    public static function getPropetctedEmail($sEmail) {

        $arEmail = explode('@', $sEmail);
        $mail    = "mail_" . randString(2);

        $sResult = '<script type="text/javascript">
            var '.$mail.' = "'.$arEmail['0'].'";
            '.$mail.' = '.$mail.' + "@";
            '.$mail.' = '.$mail.' + "'.$arEmail['1'].'";
            document.write("<a href=" + "mail" + "to:" + '.$mail.' + ">" + '.$mail.' + "</a>");
        </script>';

        return $sResult;
    }

    /**
     * Библиотека паттернов регулярных выражений, возможны неточности. Источик: https://habrahabr.ru/post/123845/
     *
     * @param string $sName
     * @param bool $bAddSlash
     * @return string
     */
    public static function getRegExPattern($sName, $bAddSlash = true) {
        $arPatterns = [
            'CREDIT_CARD'          => '[0-9]{13,16}',
            'ICQ'                  => '([1-9])+(?:-?\d){4,}',
            'WORDS_NUMBERS'        => '[^a-zA-Z0-9]+$',
              'WORDS_NUMBERS_HYPHEN' => '[^a-zA-Z0-9\-]+$', // используется
            'TOTAL_NUMBERS_WORDS'  => '[^а-яА-ЯёЁa-zA-Z0-9]+$',
            'DOMAIN'               => '^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$',
            'IPV4'                 => '((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)',
            'IPV6'                 => '((^|:)([0-9a-fA-F]{0,4})){1,8}$',
            'USER_NAME'            => '^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$', //(с ограничением 2-20 символов, которыми могут быть буквы и цифры, первый символ обязательно буква)
            'PASSWORD'             => '^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$', //Строчные и прописные латинские буквы, цифры
            'BIG_PASSWORD'         => '(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$', //(Строчные и прописные латинские буквы, цифры, спецсимволы. Минимум 8 символов)
            'DATE_YYYY-MM-DD'      => '(19|20)\d\d-((0[1-9]|1[012])-(0[1-9]|[12]\d)|(0[13-9]|1[012])-30|(0[13578]|1[02])-31)',
            'DATE_DD/MM/YYYY'      => '(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d',
            'TIME_HH:MM:SS'        => '^([0-1]\d|2[0-3])(:[0-5]\d){2}$',
            'NUMBERS'              => '\-?\d+(\.\d{0,})?', //Целые числа и числа с плавающей точкой (разделитель точка)
            'UUID'                 => '^[0-9A-Fa-f]{8}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{12}$',
            'LATITUDE_LONGITUDE'   => '-?\d{1,3}\.\d+', //Широта или долгота
            'EMAIL'                => '^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$', //E-mail
            'MAC_ADDRESS'          => '([0-9a-fA-F]{2}([:-]|$)){6}$|([0-9a-fA-F]{4}([.]|$)){3}', //Mac-адрес
            'URL'                 => '~^(?:(?:https?|ftp|telnet)://(?:[а-яёa-z0-9_-]{1,32}(?::[а-яёa-z0-9_-]{1,32})?@)?)?(?:(?:[а-яёa-z0-9-]{1,128}\.)+(?:рф|xn--p1ai|ru|su|com|net|org|mil|edu|arpa|gov|biz|info|aero|inc|name|[a-z]{2})|(?!0)(?:(?!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:/[а-яёa-z0-9.,_@%&?+=\~/-]*)?(?:#[^ \'\"&]*)?$~i', //URL лат + рф.
        ];

        $sResult = strval($arPatterns[$sName]);
        if (!empty($sResult) && $bAddSlash) {
            $sResult = '/' . $sResult .  '/';
        }

        return $sResult;
    }

    /**
     * Получает из cookie настройки версии для слабовидащих и помещает в классы тега body
     *
     * @return string
     */
    public static function getBlindSettings() {

        $sResult = '';
        $sRegExPattern = self::getRegExPattern('WORDS_NUMBERS_HYPHEN');
        $arParams = ['blind-font-size', 'blind-colors', 'blind-font', 'blind-spacing'];
        foreach ($arParams as $sParam) {
            $sValue = preg_replace($sRegExPattern,'', $_COOKIE[$sParam]);
            if (!empty($sValue)) {
                $sResult .= ' ' . $sValue;
            }
        }

        return $sResult;
    }

    /**
     * Определяет является ли текущий пользователь поисковым роботом
     *
     * @return bool
     */
    public static function checkSearcherBot() {

        $arTables = $arResult = [];
        $cacheTime = 86400;
        $cacheDir  = CACHE_DIRECTORY . '/' . __CLASS__ . '/'. __FUNCTION__;
        $cacheId   = md5( $cacheDir );

        $obCache = Cache::createInstance();
        if ($obCache->initCache($cacheTime, $cacheId, $cacheDir)) {

            $arResult = $obCache->getVars();

        } elseif ($obCache->startDataCache()) {

            // проверка на существование таблицы
            $rsRecords = Application::getConnection()->query("SHOW TABLES;");
            while ($arFields = $rsRecords->fetch()) {
                $arFields = end($arFields);
                $arTables[$arFields] = $arFields;
            }

            if (!empty($arTables['b_stat_searcher'])) {
                $sSql = "SELECT USER_AGENT FROM b_stat_searcher
                    WHERE ACTIVE = 'Y' AND USER_AGENT IS NOT NULL AND TOTAL_HITS > 1000 ORDER BY TOTAL_HITS DESC";
                $rsRecords = Application::getConnection()->query($sSql);
                while ($arFields = $rsRecords->fetch()) {
                    if (false === stripos($arFields['USER_AGENT'], 'bot') || $arFields['USER_AGENT'] == 'bot')
                        $arResult[] = $arFields['USER_AGENT'];
                }
            }

            $obCache->endDataCache($arResult);
        }

        $bResult = false;
        foreach($arResult as $sSearcher) {
            if (false !== stripos($_SERVER['HTTP_USER_AGENT'], $sSearcher)) {
                $bResult = true;
                break;
            }
        }

        return $bResult;
    }
}