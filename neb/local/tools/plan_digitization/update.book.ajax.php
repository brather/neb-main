<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('highloadblock');
Loc::loadLanguageFile(__FILE__);

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
$arReturn = array();
if(!empty($arRequest['idBook'])) {
    //region Вернем id HL блока списка книг на оцифровку
    $connection = Application::getConnection();
    $sql = "SELECT ID FROM b_hlblock_entity WHERE NAME='" . HIBLOCK_CODE_DIGITIZATION_PLAN . "';";
    $recordset = $connection->query($sql)->fetch();
    $idHLBlock = $recordset['ID'];
    //endregion

    //region Вернем объект для работы с данными HL блока
    $hlblock = HighloadBlockTable::getById($idHLBlock)->fetch();
    $dataClass = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
    //endregion

    if($dataClass::update($arRequest['idBook'], array('UF_REQUEST_STATUS' => $arRequest['idStatus']))) {
        $message['SUCCESS'] = Loc::getMessage('REMOVE_BOOK_AJAX_UPDATE_SUCCESS');
    } else {
        $message['ERROR'] = Loc::getMessage('REMOVE_BOOK_AJAX_UPDATE_ERROR');
    }

    MainHelper::showJson($message);
}
