<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

if (!Loader::includeModule('iblock')) {
    die();
}

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE" => "Y"));
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr["ID"]] = "[" . $arr["ID"] . "] " . $arr["NAME"];
}

try {
    $arComponentParameters = array(
        'GROUPS' => array(),
        'PARAMETERS' => array(
            "IBLOCK_TYPE" => array(
                "PARENT" => "BASE",
                "NAME" => 'IBLOCK_TYPE',
                "TYPE" => "LIST",
                "VALUES" => $arIBlockType,
                "REFRESH" => "Y",
            ),
            "IBLOCK_ID" => array(
                "PARENT" => "BASE",
                "NAME" => 'IBLOCK_ID',
                "TYPE" => "LIST",
                "VALUES" => $arIBlock,
                "REFRESH" => "Y",
                "ADDITIONAL_VALUES" => "Y",
            ),
            "PAGE_URL" => array(
                "PARENT" => "BASE",
                "NAME" => Loc::getMessage('CATALOG_PROMO_PARAMETERS_PAGE_URL'),
                "TYPE" => "STRING",
                "DEFAULT" => '',
            ),
            'CACHE_TIME' => array(
                'DEFAULT' => 3600
            )
        )
    );
} catch (Main\LoaderException $e) {
    ShowError($e->getMessage());
}
