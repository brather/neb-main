<?

namespace Nota\UserData;

use \Bitrix\Main\Web\HttpClient,
    \Bitrix\Main\Config\Option,
    \Neb\Main\Helper\MainHelper;

/**
 * Работа с пользователем RGB
 * @package Nota\UserData
 *
 * От РГБ (19.05.2016): Группа записи читателей и API Единой базы пользователей:
 *   •  добавление пользователя: http://gzch.rsl.ru/external_users_api/add_neb_user
 *   •  проверка пользователя: http://gzch.rsl.ru/external_users_api/verification_neb_user
 *   •  API ЕБП: http://sub.rsl.ru/xmlrpc
 */
class rgb
{
    static $url_add_user = 'https://relar.rsl.ru/sub/xmlrpc';
    static $secretKey = 'faladsf0213954fqaoptierhgoslq';

    static $CONSUMER_KEY = "elar";
    static $CONSUMER_SECRET = "pN22Wv1wNGTT";
    static $RgbUrl = 'http://sub.rsl.ru/xmlrpc'; //'http://test.sub.rsl.ru/xmlrpc';
    static $gzchchUrl = 'https://relar.rsl.ru/external_users_api/verification_neb_user';

    static $SITIZENSHIP_RUSSIA = 134; // Россия

    public function __construct(){

        self::$url_add_user = Option::get("nota.userdata", "ebp_rgb");
        if (!self::$url_add_user)
            throw new \Exception('Укажите в админке путь к ЕБП РГБ! ');

        self::$gzchchUrl = Option::get("nota.userdata", "gzchch_rgb");
        if (!self::$gzchchUrl)
            throw new \Exception('Укажите в админке путь к ГЗЧ РГБ! ');
    }

    /**
     * @method add - Добавление пользователя в RGB
     * @param 	 $arFields
     * @return bool|mixed
     *
     * Для регистрации нужно отправить POST-запрос со следующими параметрами:
     *  secretKey - ключ доступа (sdaf;hl43521=asdf)
     *  uid - идентификатор пользователя, если пользователь уже регистрировался в РГБ. Если не
     * регистрировался, то передавать данный параметр не нужно.
     *  lastname - фамилия
     *  firstname - имя
     *  middlename - отчество
     *  birthday - дата рождения (формат: dd/mm/yyyy)
     *  citizenship - гражданство, номер из справочника
     *  residence - проживание, номер из справочника
     *  branch_of_knowledge - отрасль знаний, номер из справочника
     *  reader_category - образование, номер из справочника
     *  passport_number - номер паспорта. Пример: 1234 123456
     *  email - электронная почта
     *  password - пароль пользователя. Минимум 6 символов. Если передается uid, то
     * спрашивать у пользователя и передавать пароль не нужно.
     *  mobile_phone - мобильный телефон. Формат: +7(123)1234567
     *  registration_address - адрес регистрации. Строка
     *  home_address - адрес проживания. Строка
     *  job_study_place - место работы или учебы. Строка
     *  gender - пол (male или female)
     * Ответ от сервера в формате json:
     * {
     * result: true/false (результат регистрации)
     * errors: [] (массив с ошибками, если result = false
     * }
     */
    public static function add($arFields)
    {
        if (empty($arFields) || !is_array($arFields))
            return false;

        $arFieldsVar = array(
            'uid',
            'lastname',
            'firstname',
            'middlename',
            'birthday',
            'branch_of_knowledge',
            'reader_category',
            'passport_number',
            'email',
            'password',
            'mobile_phone',
            'registration_address',
            'home_address',
            'job_study_place',
            'gender',
        );

        TrimArr($arFields);

        $arPostFields = array(
            'secretKey' => self::$secretKey,
            'citizenship' => 134,
            'residence' => 1,
            'gender' => 'male',
        );

        foreach ($arFieldsVar as $key) {
            if (!empty($arFields[$key]))
                $arPostFields[$key] = trim($arFields[$key]);
        }

        $http = new HttpClient();
        $strQueryText = $http->post(self::$gzchchUrl, $arPostFields);

        if (empty($strQueryText))
            return false;

        return json_decode($strQueryText, true);
    }

    /**
     * Добавление пользователя в RGB по ID Битрикса
     *
     * @method addUserBx
     * @param $UID int
     * @return bool|mixed
     */
    public static function addUserBx($UID)
    {
        $UID = intval($UID);
        if ($UID <= 0)
            return false;

        $nebUser = new \nebUser($UID);
        $arRes = $nebUser->getUser();

        $branch_of_knowledge = '';
        if (!empty($arRes['UF_BRANCH_KNOWLEDGE'])) {
            $arBranch_of_knowledge = $nebUser->getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE', 'ID' => $arRes['UF_BRANCH_KNOWLEDGE']));
            if (!empty($arBranch_of_knowledge[0]['XML_ID']))
                $branch_of_knowledge = $arBranch_of_knowledge[0]['XML_ID'];
        }
        $reader_category = '';
        if (!empty($arRes['UF_BRANCH_KNOWLEDGE'])) {
            $arReader_category = $nebUser->getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION', 'ID' => $arRes['UF_EDUCATION']));
            if (!empty($arReader_category[0]['XML_ID']))
                $reader_category = $arReader_category[0]['XML_ID'];
        }

        $arFields = array(
            'lastname' => $arRes['LAST_NAME'],
            'firstname' => $arRes['NAME'],
            'middlename' => $arRes['SECOND_NAME'],
            'birthday' => ConvertDateTime($arRes['PERSONAL_BIRTHDAY'], 'DD/MM/YYYY'),
            'branch_of_knowledge' => $branch_of_knowledge,
            'reader_category' => $reader_category,
            'passport_number' => $arRes['UF_PASSPORT_NUMBER'] . ' ' . $arRes['UF_PASSPORT_SERIES'],
            'email' => $arRes['EMAIL'],
            'password' => $arRes['PERSONAL_NOTES'],
            'mobile_phone' => $arRes['PERSONAL_MOBILE'],
            'registration_address' => $arRes['PERSONAL_ZIP'] . ' ' . $arRes['PERSONAL_CITY'] . ' ' . $arRes['PERSONAL_STREET']
                . ' ' . $arRes['UF_CORPUS'] . ' ' . $arRes['UF_STRUCTURE'] . ' ' . $arRes['UF_FLAT'],
            'home_address' => $arRes['WORK_ZIP'] . ' ' . $arRes['WORK_CITY'] . ' ' . $arRes['WORK_STREET']
                . ' ' . $arRes['UF_HOUSE2'] . ' ' . $arRes['UF_STRUCTURE2'] . ' ' . $arRes['UF_FLAT2'],
            'job_study_place' => $arRes['WORK_COMPANY'],
        );

        $res = self::add($arFields);

        if (empty($res))
            return false;

        if ((boolean)$res['result'] === false)
            return $res;

        if ((boolean)$res['result'] === true) {

            //Если пользватель успешно добавлен в РГБ, попробуем узнать и записать его АЙДИ
            $response = self::getUserByCredential($arRes['EMAIL'], $arRes['PERSONAL_NOTES']);
            if (intval($response['uid']) > 0)
                $nebUser->Update($arRes['ID'], array('UF_RGB_USER_ID' => $response['uid'], 'UF_LIBRARY' => RGB_LIB_ID));

            return $response;
        }
    }

    /**
     * @method getUserByCredential - Получает информацию о пользователе по логину и паролю
     * @param $credential - значение поля
     * @param $password
     * @param string $credential_field - тип поля. Возможные значения: email, login, reader_number
     * @return bool|mixed
     *
     * 200000432234/123456
     *
     * Если пользователь не найден, возвращается ошибка server fault:
     * 101 – неверное название поля credential_field
     * 404 – пользователь не найден
     * 500 - системная ошибка
     */
    public static function getUserByCredential($credential, $password, $credential_field = 'email')
    {
        return self::sendRgbReguest('sub.getUserByCredential', array($credential, $password, $credential_field));
    }

    /**
     * Метод еще не реализован в РГБ
     *
     * @param $uidRgb
     * @return bool|mixed
     */
    public static function getUserByUid($uidRgb)
    {
        return self::sendRgbReguest('sub.getUserByUid', array($uidRgb . ''));
    }

    public static function isUserActive($uidRgb)
    {
        return self::sendRgbReguest('sub.isUserActive', array($uidRgb . ''));
    }

    /**
     * @param $field
     * @param $fieldValue
     * @return bool|mixed
     *
     *
     * usersFind – Поиск пользователей по полям
     * Входные данные:
     * Filters – поля для поиска
     * Page – номер страницы
     * Per_page – элементов на странице (максимум 20 элементов)
     * Выходные данные:
     * struct – 'users': найденные пользователи (сортировка происходит по идентификатору пользователя), 'count': количество найденных пользователей
     *
     * Коды ошибок:
     * 101 – номера страницы или количество результатов не положительное число, либо превышен лимит в 20 записей на странице.
     * 403 – система не имеет доступа для поиска по заданным полям
     * 500 – системная ошибка
     */
    public function usersFind($field, $fieldValue)
    {
        return self::sendRgbReguest('sub.usersFind', array(array($field => $fieldValue), 1, 10));
    }

    /**
     * Отправляем\получаем данные из РГБ согласно указанному методу
     *
     * @param $method
     * @param $arParams
     * @return bool|mixed
     */
    private function sendRgbReguest($method, $arParams)
    {
        $log = new \CEventLog();

        if (empty($method) or empty($arParams))
            return false;

        require_once(__DIR__ . '/../extlibs/OAuth.php');

        //Initialize OAuth Consumer
        $consumer = new \OAuthConsumer(self::$CONSUMER_KEY, self::$CONSUMER_SECRET);

        //Prepare the request

        $request = \OAuthRequest::from_consumer_and_token($consumer, NULL, 'POST', self::$RgbUrl, NULL);
        $request->sign_request(new \OAuthSignatureMethod_HMAC_SHA1(), $consumer, NULL);
        $auth_header = $request->to_header('api');

        $request = xmlrpc_encode_request(
            $method, $arParams, array(
                'escaping' => 'markup',
                'encoding' => 'UTF-8'
            )
        );

        //Отправляем запрос
        $ch = curl_init(static::$url_add_user);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, MainHelper::checkSslVerification(static::$url_add_user));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($auth_header, 'Content-Type: text/xml', 'User-Agent: xmlrpclibPHP.py/1.0.1'));

        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        //Fetch the event
        $response = curl_exec($ch);

        //$info = curl_getinfo($ch);
        //$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (empty($response)) {
            $log->Log(5, 'API RGB', 'API RGB', 'rgb', ' ОШИБКА АПИ РГБ' . curl_error($ch));
            return false;
        }

        curl_close($ch);

        $response = xmlrpc_decode($response);
        return $response;
    }

    private function getUserData($user)
    {
        if (is_object($user))
            $nebUser = $user;
        else
            $nebUser = new \nebUser($user);

        $arRes = $nebUser->getUser();
        if (!isset($arRes['ID']) || empty($arRes['ID'])) {
            return false;
        }

        $branch_of_knowledge = '';
        if (!empty($arRes['UF_BRANCH_KNOWLEDGE'])) {
            $arBranch_of_knowledge = $nebUser->getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE', 'ID' => $arRes['UF_BRANCH_KNOWLEDGE']));
            if (!empty($arBranch_of_knowledge[0]['XML_ID']))
                $branch_of_knowledge = $arBranch_of_knowledge[0]['XML_ID'];
        }
        $reader_category = '';
        if (!empty($arRes['UF_BRANCH_KNOWLEDGE'])) {
            $arReader_category = $nebUser->getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION', 'ID' => $arRes['UF_EDUCATION']));
            if (!empty($arReader_category[0]['XML_ID']))
                $reader_category = $arReader_category[0]['XML_ID'];
        }

        $citizenship = self::$SITIZENSHIP_RUSSIA;

        if (!empty($arRes['UF_CITIZENSHIP'])) {
            $arReader_category = $nebUser->getFieldEnum(array('USER_FIELD_NAME' => 'UF_CITIZENSHIP', 'ID' => $arRes['UF_CITIZENSHIP']));
            if (!empty($arReader_category[0]['XML_ID']))
                $citizenship = $arReader_category[0]['XML_ID'];
        }

        $arFields = array(
            'lastname' => $arRes['LAST_NAME'],
            'firstname' => $arRes['NAME'],
            'middlename' => $arRes['SECOND_NAME'],
            'birthday' => ConvertDateTime($arRes['PERSONAL_BIRTHDAY'], 'DD/MM/YYYY'),
            'email' => $arRes['EMAIL'],
            'gender' => 'F' === $arRes['PERSONAL_GENDER'] ? 'female' : 'male',
            'mobile_phone' => is_null($arRes['PERSONAL_MOBILE']) ? '' : $arRes['PERSONAL_MOBILE'],
            'citizenship' => $citizenship,
            'branch_of_knowledge' => $branch_of_knowledge,
            'reader_category' => $reader_category,
            'passport_number' => $arRes['UF_PASSPORT_SERIES'] . ' ' . $arRes['UF_PASSPORT_NUMBER'],
            'registration_address' => $arRes['PERSONAL_ZIP'] . ' ' . $arRes['PERSONAL_CITY']
                . ' ' . $arRes['PERSONAL_STREET'] . ' ' . $arRes['UF_CORPUS'] . ' ' . $arRes['UF_STRUCTURE'] . ' ' . $arRes['UF_FLAT'],
            'home_address' => $arRes['WORK_ZIP'] . ' ' . $arRes['WORK_CITY'] . ' ' . $arRes['WORK_STREET']
                            . ' ' . $arRes['UF_HOUSE2'] . ' ' . $arRes['UF_STRUCTURE2'] . ' ' . $arRes['UF_FLAT2'],
            'job_study_place' => is_null($arRes['WORK_COMPANY']) ? '' : $arRes['WORK_COMPANY'],

            //2. Поля про студентов опциональные, передавайте пустые значения
            'userStudentPage' => '',
            'userStudentPageExt' => '',

        );
        if ($arRes['UF_PLACE_REGISTR']) {
            $arFields['home_address'] = $arFields['registration_address'];
        }

        /*1. В РГБ по умолчанию, пароль дата рождения в формате ДДММГГГГ.
          Сразу после записи читателя будем высылать ему письмо с просьбой сменить его.*/
        if (!$arRes['UF_RGB_USER_ID']) {
            $arFields['password'] = ConvertDateTime($arRes['PERSONAL_BIRTHDAY'], 'DDMMYYYY');
        }

        return $arFields;
    }

    /**
     * Посылает данные пользователя в РГБ для присвоения статуса.
     * Используется ТОЛЬКО В ПОЛНОЙ РЕГИСТРАЦИИ для всего остального пользовать АПИ.
     *
     * @param $user
     * @return mixed
     */
    public function pushUserIntoRGB($user)
    {

        if (is_object($user)):
            $nebUser = $user;
        else:
            $nebUser = new \nebUser($user);
        endif;

        /**
         * По другому не работает, хз почему
         */
        $arFields = $this->getUserData($nebUser);
        $arFields['fields'] = $arFields;

        $arRes = $nebUser->getUser();

        /*
        photo[userPassportFirstPage] - base64 закодированная первая страница паспорта
        photo[userPassportFirstPageExt] - расширение файла первой страницы паспорта (png, jpeg...)
        photo[userPassportSecondPage]] - base64 закодированная вторая страница паспорта
        photo[userPassportSecondPageExt] - расширение файла второй страницы паспорта (png, jpeg...)
        */
        $arFields['photo'] = array(

            'userPassportFirstPage' => base64_encode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($arRes["UF_SCAN_PASSPORT1"]))),
            'userPassportFirstPageExt' => pathinfo(\CFile::GetPath($arRes["UF_SCAN_PASSPORT1"]), PATHINFO_EXTENSION),

            'userPassportSecondPage' => base64_encode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($arRes["UF_SCAN_PASSPORT2"]))),
            'userPassportSecondPageExt' => pathinfo(\CFile::GetPath($arRes["UF_SCAN_PASSPORT2"]), PATHINFO_EXTENSION),
        );

        $arFields['secretKey'] = self::$secretKey;

        $http = new HttpClient();
        $result = json_decode($http->post(self::$gzchchUrl, $arFields), true);

        if (isset($result['id_request']) && ($result['id_request'])) {
            $fields = Array(
                "UF_ID_REQUEST" => $result['id_request'],
            );
            $nebUser = new \nebUser($nebUser->USER_ID);

            $nebUser->Update($nebUser->USER_ID, $fields);
        } else {
            AddMessage2Log(
                'Error pushUserIntoRGB: ' . PHP_EOL . print_r(
                    $result, true
                )
            );
        }

        $log = new \CEventLog();
        $arFields['photo']['userPassportFirstPage'] = strlen($arFields['photo']['userPassportFirstPage']);
        $arFields['photo']['userPassportSecondPage'] = strlen($arFields['photo']['userPassportSecondPage']);
        $log->Log(
            'INFO',
            'pushUserIntoRGB',
            'nota.userdata',
            $nebUser->GetID(),
            serialize(
                array(
                    'user_id'    => $nebUser->GetID(),
                    'pushResult' => $result,
                    'fields'     => $arFields,
                )
            )
        );

        return $result;
    }

    /**
     * @param array $data - пример  array('uid'=>'1000', 'email' =>'test@test.ru');
     * @return bool|mixed
     * @throws \Exception
     *
     * userUpdate – редактирование пользователя Входные данные:
     * Struct – поля пользователя (uid должен обязательно присутствовать) Выходные данные:
     * Struct – {result – флаг редактирования (true/false), uid – идентификатор отредактированного пользователя,
     * errors – struct с описанием ошибок валидации полей}
     * Коды ошибок: 101 – отсутствует uid, либо обязательное для заполнения поле указано пустым.
     * 403 – присутствует поле, которое не разрешено редактировать системе
     *   501 – не удалось создать пользователя. Внутренняя ошибка.
     *   500 – системная ошибка
     */
    public function userUpdate(array $data)
    {
        if (!$data)
            throw new \Exception("Данные однако тоже обязательны!");

        if (!array_key_exists('uid', $data))
            throw new \Exception("ID'шник однако обязателен!");

        return self::sendRgbReguest('sub.userUpdate', array($data));
    }

    /**
     * @param $user
     *
     * @return bool|mixed
     * @throws Exception
     * @throws \Exception
     */
    public function userCreate($user)
    {
        if(false === ($arFields = $this->getUserData($user))) {
            throw new Exception('User no found', Exception::USER_NOT_FOUND);
        }
        $fields = array(
            'lastname'     => '',
            'firstname'    => '',
            'middlename'   => '',
            'email'        => '',
            'mobile_phone' => '',
            'password'     => '',
            'is_block'     => false,
        );
        $fields = array_merge(
            $fields, array_intersect_key($arFields, $fields)
        );
        $result = self::sendRgbReguest('sub.userCreate', array($fields));
        if($error = $this->_checkRgbResponseError($result)) {
            throw new Exception($error, Exception::RGB_USER_CREATE_ERROR);
        }
        $arFields['uid'] = $result['uid'];

        // костыль: убирем лишние поля
        unset($arFields['userStudentPage'], $arFields['userStudentPageExt']);
        
        $updateResult = $this->userUpdate($arFields);
        if($error = $this->_checkRgbResponseError($updateResult)) {
            throw new Exception($error, Exception::RGB_USER_CREATE_ERROR);
        }

        return $result;
    }

    private function _checkRgbResponseError(&$response)
    {
        if (isset($response['errors'])) {
            return $response['errors'];
        }
        if (isset($response['faultString'])) {
            return $response['faultString'];
        }
        if (false === (boolean)@$response['result']) {
            return 'unknown error';
        }

        return false;
    }

    /**
     * @param int|\CUser $user
     *
     * @return array|bool
     * @throws \Exception
     */
    public function userCreateBx($user)
    {
        $result = $this->userCreate($user);
        if (false === (boolean)$result['result']) {
            throw new Exception('Cannot create user in rgb', Exception::RGB_USER_CREATE_ERROR);
        }
        if ($user instanceof \CUser) {
            $userId = $user->GetID();
        } else {
            $userId = intval($user);
            $user = new \nebUser($userId);
            $userData = $user->getUser();
            $userId = @$userData['ID'];
        }
        if (!$user->Update($userId, array('UF_RGB_USER_ID' => $result['uid'], 'UF_LIBRARY' => RGB_LIB_ID))) {
            throw new Exception($user->LAST_ERROR, Exception::RGB_USER_CREATE_ERROR);
        }

        return true;
    }
}