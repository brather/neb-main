$(function(){

    $('ul.lk-table__row .unlock').on('click', function(){

        var user_id = $(this).attr('data-id');
        var user_login = $(this).attr('data-user');

        if (confirm("Вы точно хотете разблокировать пользователя "+user_login+"?")) {

            $(this).html('Заблокировать');
            $(this).addClass("block");
            $(this).removeClass("unlock");

            // запрос на активацию пользователя
            $.ajax({
                type: "PUT",
                url: "/rest_api/users/status/",
                data: "user_id=" + user_id + "&active=true&token=" + getUserToken(),
                success: function (data) {
                    if(true === data.success) {
                    }

                }
            });
        }

    })

    $('ul.lk-table__row .block').on('click' ,function(){

        var user_id = $(this).attr('data-id');
        var user_login = $(this).attr('data-user');

        if (confirm("Вы точно хотете заблокировать пользователя "+user_login+"?")) {

            $(this).html('Разблокировать');
            $(this).removeClass("block");
            $(this).addClass("unlock");


            // запрос на активацию пользователя
            $.ajax({
                type: "PUT",
                url: "/rest_api/users/status/",
                data: "user_id=" + user_id + "&active=false&token=" + getUserToken(),
                success: function (data) {
                    if(true === data.success) {
                    }

                }
            });
        }

    })

})