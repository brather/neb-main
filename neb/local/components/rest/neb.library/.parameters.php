<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'       => '/',
                'VARIABLES'     => array(
                    'token',
                    'limit',
                    'offset',
                    'UF_LOCALITY',
                ),
                'TYPE'          => array('GET'),
                'DOCUMENTATION' => array(
                    'GROUP'       => 'CONTROL',
                    'DESCRIPTION' => '',
                    'PARAMETERS'  => array()
                ),
            ),
        ),
    ),
); 