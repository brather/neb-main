<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\Main\UI\PageNavigation,
    \Bitrix\Main\Application,
    \Bitrix\Iblock\ElementTable,
    \Bitrix\Iblock\IblockTable;

/**
 * Class DigitizationPlanListComponent
 */
class DigitizationPlanListComponent extends \CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $arRequest = array();
    private $sort = '';
    private $order = '';
    private $showDuplication = false;
    private $arPSPlan = array();
    private $arPSBook = array();
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->arRequest = self::getRequest();
        $this->_setSortParam();
        $this->includeModule();
        $this->getSortUrl();
        $this->getPlan();
        $this->_getListLibrary();
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Подключение модулей
    /** Подключение модулей
     * @throws \Bitrix\Main\LoaderException
     */
    private function includeModule() {
        Loader::includeModule('highloadblock');
    }
    //endregion
    //region Установка переменных для сортировки
    /**
     * Установка переменных для сортировки
     */
    private function _setSortParam() {
        $this->sort = $this->arParams['BY_SORT_DEFAULT'];
        $this->order = $this->arParams['BY_ORDER_DEFAULT'];
        if(!empty($this->arRequest['sort'])) {
            $this->sort = $this->arRequest['sort'];
        }
        if(!empty($this->arRequest['order'])) {
            $this->order = $this->arRequest['order'];
        }
    }
    //endregion
    //region Возвращаем список планов
    /** Возвращаем список планов
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    private function getPlan() {
        $obDigitizationPlan = self::getDataClassHL(HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
        $obDigitizationBooks = self::getDataClassHL(HIBLOCK_CODE_DIGITIZATION_PLAN);

        $this->arPSPlan = self::getListPropertyStatus(HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
        $this->arPSBook = self::getListPropertyStatus(HIBLOCK_CODE_DIGITIZATION_PLAN);

        $arFilterPlan = [
            '!UF_REQUEST_STATUS' => false
        ];
        if ($iLib = intval($_REQUEST['LIBRARY']))
            $arFilterPlan['UF_LIBRARY'] = $iLib;
        if ($iStatus = intval($_REQUEST['STATUS']))
            $arFilterPlan['UF_REQUEST_STATUS'] = $iStatus;
        if (!empty($_REQUEST['DATE_FROM']))
            $arFilterPlan['>=UF_DATE_F_PLAN'] = $_REQUEST['DATE_FROM'];
        if (!empty($_REQUEST['DATE_TO']))
            $arFilterPlan['<=UF_DATE_F_PLAN'] = $_REQUEST['DATE_TO'];

        $arResultTemp = self::getListPlan($arFilterPlan, $obDigitizationPlan, $obDigitizationBooks);
        foreach($arResultTemp as $key => $arVal) {
            $this->arResult[$key] = $arVal;
        }
        //region Если необходимо показать только лишь те планы в которых есть дублирующиеся книги
        if($this->arRequest['show-duplication'] == 'on') {
            $arFilterPlan = array(
                'ID'                 => $this->arResult['DUPLICATION_PLANS'],
                '!UF_REQUEST_STATUS' => false,
            );
            $arResultTemp = self::getListPlan($arFilterPlan, $obDigitizationPlan, $obDigitizationBooks);
            foreach($arResultTemp as $key => $arVal) {
                $this->arResult[$key] = $arVal;
            }
        }

        $arTmpStatus = array();
        foreach($this->arPSPlan as $idStatus => $arStatus) {
            $arTmpStatus[$arStatus['XML_ID']] = $arStatus;
        }

        foreach($this->arResult['ITEMS'] as $idPlan => $arPlan) {
            $sortStatus = $this->arPSPlan[$arPlan['UF_REQUEST_STATUS']]['SORT'];
            if($sortStatus <= $arTmpStatus['AGREED']['SORT'] || $sortStatus == $arTmpStatus['CANCELED']['SORT']) {
                $this->arResult['ITEMS'][$idPlan]['LIST_STATUS'] = array(
                    'ACTIVE_SELECT' => 'Y',
                    'OPTIONS' => array(
                        $arTmpStatus['IN_AGREEING_NEL_OPERATOR']['ID'],
                        $arTmpStatus['AGREED']['ID'],
                        $arTmpStatus['CANCELED']['ID']
                    )
                );
            } elseif($sortStatus == $arTmpStatus['DIGITIZED']['SORT']) {
                $this->arResult['ITEMS'][$idPlan]['LIST_STATUS'] = array(
                    'ACTIVE_SELECT' => 'Y',
                    'OPTIONS' => array(
                        $arTmpStatus['DIGITIZED']['ID'],
                        $arTmpStatus['DONE']['ID']
                    )
                );
            } else {
                $this->arResult['ITEMS'][$idPlan]['LIST_STATUS'] = array(
                    'ACTIVE_SELECT' => 'N',
                    'OPTIONS' => array($arTmpStatus['DONE']['ID'])
                );
            }
        }
        $this->arResult['PROPERTY_STATUS_PLAN'] = $this->arPSPlan;
        $this->arResult['PROPERTY_STATUS_BOOK'] = $this->arPSBook;
        //endregion
    }
    //endregion
    //region Выборка планов по фильтру
    /** Выборка планов по фильтру
     * @param array $arFilterPlan
     * @param $obDigitizationPlan
     * @param $obDigitizationBooks
     * @return array
     */
    private function getListPlan($arFilterPlan = array(), $obDigitizationPlan, $obDigitizationBooks) {
        //region Общее количество возвращаемых планов на оцифровку
        $iTotalCount = $obDigitizationPlan::getList(array(
            'filter' => $arFilterPlan,
            'select' => ['ID']))->getSelectedRowsCount();
        //endregion
        //region Количество планов на странице
        $iPageLimit = $this->arParams['COUNT_ON_PAGE'];
        //endregion
        //region Возвращаем объект постраничной навигации
        $obPageNavigation = new PageNavigation('plan');
        $obPageNavigation->allowAllRecords(true)->initFromUri();
        //region Если "показать все" то снимаем лимит на показ количества на странице
        if($this->arRequest['plan'] == 'page-all')  {
            $iPageLimit = '';
        }
        //endregion
        $obPageNavigation->getLimit();
        $obPageNavigation->setRecordCount($iTotalCount)->setPageSize($iPageLimit);
        //endregion
        //region Номер текущей страницы
        $iCurrentPage = $obPageNavigation->getCurrentPage() - 1;
        //endregion
        //region Начальный индекс плана с которого будет осуществлен вывод
        $startIndex = $iCurrentPage * $iPageLimit;
        //endregion
        //region Определение параметров сортировки
        //region Параметры для сортировки по количеству книг в плане
        $pageLimitForSortCount = $iPageLimit;
        $startIndexForSortCount = $startIndex;
        //endregion
        if($this->sort == 'UF_COUNT_BOOKS') {
            $this->sort = 'ID';
            $iPageLimit = '';
        }
        $sort = $this->sort;
        $order = $this->order;
        //endregion
        //region Возвращаем список планов
        $libraryData = $obDigitizationPlan::getList(array(
            "filter" => $arFilterPlan,
            "order"  => array($sort => $order),
            'offset' => $startIndex,
            'limit'  => $iPageLimit,
        ));
        $arIdPlans = array();
        $arResult = array();
        $arResultTmp = array();
        while ($arItem = $libraryData->fetch()) {
            $arIdPlans[] = $arItem['ID'];
            $arFilterBooks = array('UF_PLAN_ID' => $arItem['ID']);
            $startIndex++;
            $arResultTmp[$arItem['ID']] = $arItem;
            $arResultTmp[$arItem['ID']]['INDEX'] = $startIndex;
            $arResultTmp[$arItem['ID']]['COUNT_BOOKS'] = $obDigitizationBooks::getList(array('filter' => $arFilterBooks))->getSelectedRowsCount();
        }
        //endregion
        //region Если установили значение сортировки по количеству книг
        if($this->arRequest['sort'] == 'UF_COUNT_BOOKS') {
            usort($arResultTmp, function($a, $b){
                global $order;
                if($order == 'asc') {
                    return ($a['COUNT_BOOKS'] - $b['COUNT_BOOKS']);
                } else {
                    return ($b['COUNT_BOOKS'] - $a['COUNT_BOOKS']);
                }
            });
            $i = 0;
            $stIDefault = $startIndexForSortCount;
            foreach($arResultTmp as $idBook => $arBook) {
                if($i >= $startIndexForSortCount) {
                    $startIndexForSortCount++;
                    $this->arResult['ITEMS'][$idBook] = $arBook;
                    $this->arResult['ITEMS'][$idBook]['INDEX'] = $startIndexForSortCount;
                }
                $i++;
                if($i - $stIDefault == $pageLimitForSortCount) {
                    break;
                }
            }
        }
        //endregion
        //region Если установили значение сортировки не по количеству книг
        else {
            $arResult['ITEMS'] = $arResultTmp;
        }
        //endregion
        //region Возвращаем параметры постраничной навигации и массивы со свойством "Статуса" для издания и плана
        $arResult['NAV'] = $obPageNavigation;
        //endregion
        //region Определим массив с книгами-дубликатами
        $idStatusBookCanceled = '';
        foreach($arResult['PROPERTY_STATUS_BOOK'] as $idStatus => $arStatus) {
            if($arStatus['VALUE'] == DIGITIZATION_STATUS_CANCELED) {
                $idStatusBookCanceled = $idStatus;
                break;
            }
        }
        $arFilterBooks = array('!UF_PLAN_ID' => false, '!UF_REQUEST_STATUS' => $idStatusBookCanceled);
        $arSelectBooks = array('ID', 'UF_PLAN_ID', 'UF_EXALEAD_BOOK');
        $rsBooks = $obDigitizationBooks::getList(array(
            'filter' => $arFilterBooks,
            'select' => $arSelectBooks
        ));
        $arDuplicationPlans = array();                                          // Массив содержащий id планов с дублирующимися книгами
        $arFullSymbolicId = array();                                            // Массив с используемыми fullsymbolicId
        $arListBooks = array();                                                 // массив содержащий все книги (кроме имеющих статус "Отменено")
        while($obBooks = $rsBooks->fetch()) {
            $arListBooks[$obBooks['ID']] = $obBooks;
            $arFullSymbolicId[] = $obBooks['UF_EXALEAD_BOOK'];
        }
        $arCountValuesFullSymbolicId = array_count_values($arFullSymbolicId);   // массив в котором [fullSymbolicId = Количество раз встречающийся в массиве]
        $arFullSymbolicIdDuplication = array();                                 // массив в который заносятся все дублируемые fullSymbolicId
        foreach($arCountValuesFullSymbolicId as $fsi => $count) {
            if($count > 1) {
                $arFullSymbolicIdDuplication[] = $fsi;
            }
        }
        foreach ($arListBooks as $idBook => $arBook) {
            if(in_array($arBook['UF_EXALEAD_BOOK'], $arFullSymbolicIdDuplication)) {
                if(!in_array($arBook['UF_PLAN_ID'], $arDuplicationPlans)) {
                    $arDuplicationPlans[] = $arBook['UF_PLAN_ID'];
                }
            }
        }
        if(!empty($arDuplicationPlans)) {
            $this->showDuplication = true;
            $this->_setUriDuplication();
            $arResult['DUPLICATION_PLANS'] = $arDuplicationPlans;
        }
        //endregion
        return $arResult;
    }
    //endregion
    //region Функция возвращает id HL блока по его коду
    /** Функция возвращает id HL блока по его коду
     * @param $codeHL - код HL блока
     * @return mixed
     */
    public function getIdBlock($codeHL) {
        $digitizationPlanHLBlock = HighloadBlockTable::getList(['filter' => [
            'NAME' => $codeHL
        ]])->fetchRaw();
        return $digitizationPlanHLBlock['ID'];
    }
    //endregion
    //region Функция возвращает сущность HL блока
    /** Функция возвращает сущность HL блока
     * @param $codeHL - код HL блока
     * @return \Bitrix\Main\Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    public function getDataClassHL($codeHL) {
        $iblockId  = self::getIdBlock($codeHL);
        $hlblock   = HighloadBlockTable::getById($iblockId)->fetch();
        $dataClass = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $dataClass;
    }
    //endregion
    //region Получение массива $_REQUEST (обертка D7)
    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }
    //endregion
    //region Функция возвращает массив ссылок на сортировку
    /** Функция возвращает ссылку на сортировку по параметру $sort
     * @param $sort - параметр сортировки
     * @param $order - порядок сортировки
     */
    private function getSortUrl() {
        $sort = $this->sort;
        $order = $this->order;
        $arSort = array();
        //region Получим все get параметры без значений сортировки
        $rString = '';
        $iString = 0;
        foreach($this->arRequest as $cReq => $vReq) {
            if($cReq != 'sort' && $cReq != 'order') {
                if($iString == 0) {
                    $rString .= '?';
                } else {
                    $rString .= '&';
                }
                $rString .= $cReq . '=' . $vReq;
                $iString++;
            }
        }
        //endregion
        foreach($this->arParams['BY_SORT_LIST'] as $vSort) {
            $arSort[$vSort]['URI'] = $this->arParams['SEF_FOLDER'] . $rString;
            if($rString != '') {
                $arSort[$vSort]['URI'] .= '&';
            } else {
                $arSort[$vSort]['URI'] .= '?';
            }
            if($vSort == $sort) {
                if($order == 'asc') {
                    $nOrder = 'desc';
                    $arSort[$vSort]['CLASS'] = 'sort up';
                } else {
                    $nOrder = 'asc';
                    $arSort[$vSort]['CLASS'] = 'sort down';
                }
            } else {
                $nOrder = 'asc';
            }
            $arSort[$vSort]['URI'] .= 'sort=' . $vSort . '&order=' . $nOrder;
        }
        $this->arResult['SORT_LIST'] = $arSort;
    }
    //endregion
    //region Вернем список библиотек
    /** Вернем список библиотек
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getListLibrary() {

        $arLibs = [];

        // получение названия таблицы планов изданий
        $arTable = Application::getConnection()->query(
            "SELECT TABLE_NAME FROM b_hlblock_entity WHERE NAME = 'PlanDigitizationList'"
        )->fetchRaw();

        if (empty($arTable['TABLE_NAME']))
            return $arLibs;

        // получение библиотек из планов изданий
        $rsLibs = Application::getConnection()->query(
            "SELECT UF_LIBRARY FROM " . $arTable['TABLE_NAME'] . " GROUP BY UF_LIBRARY"
        );
        while ($arItem = $rsLibs->fetch())
            $arLibs[] = $arItem['UF_LIBRARY'];

        if (empty($arLibs))
            return $arLibs;

        $rsLibrary = ElementTable::getList([
            'filter' => ['ID' => $arLibs],
            'select' => ['ID', 'NAME'],
            'order'  => ['NAME' => 'ASC']
        ]);
        while($arItem = $rsLibrary->fetch()) {
            $this->arResult['LIST_LIBRARY'][$arItem['ID']] = $arItem;
        }
    }
    //endregion
    //region Получим значения списка свойства типа список для HL блока (если не задан код свойства, то по умолчанию ищем статус)
    /** Получим значения списка свойства типа список для HL блока (если не задан код свойства, то по умолчанию ищем статус)
     * @param $codeHLBlock - символьный код HL блока
     * @param bool $propertyCode - символьный код свойства
     * @return array
     */
    public function getListPropertyStatus($codeHLBlock, $propertyCode = false) {
        if(!$propertyCode) {
            $propertyCode = 'UF_REQUEST_STATUS';
        }
        $idBlockPlan = self::getIdBlock($codeHLBlock);
        $resProp = CUserTypeEntity::GetList(
            array(),
            array('FIELD_NAME' => $propertyCode, 'ENTITY_ID' => 'HLBLOCK_' . $idBlockPlan)
        )->Fetch();
        $cUserFieldEnum = new CUserFieldEnum();
        $rsUFValues = $cUserFieldEnum->GetList(array(), array('USER_FIELD_ID' => $resProp['ID']));
        $arReturn = array();
        while($obUFValues = $rsUFValues->Fetch()) {
            $arReturn[$obUFValues['ID']] = $obUFValues;
        }
        return $arReturn;
    }
    //endregion
    //region Вернем ссылку на показ книг содержащихся в других планах на оцифровку
    /**
     * Вернем ссылку на показ книг содержащихся в других планах на оцифровку
     */
    private function _setUriDuplication() {
        if($this->showDuplication) {
            $iReq = 0;
            $newRequest = '';
            $vDuplication = 'on';
            foreach($this->arRequest as $cCode => $vCode) {
                if($iReq == 0) {
                    $newRequest .= '?';
                } else {
                    $newRequest .= '&';
                }
                if($cCode != 'show-duplication') {
                    $newRequest .= $cCode . '=' . $vCode;
                } else {
                    if($vCode == 'on') {
                        $vDuplication = 'off';
                    }
                }
                $iReq++;
            }
            $this->arResult['DUPLICATION_URI'] = $this->arParams['SEF_FOLDER'];
            if($newRequest != '') {
                $this->arResult['DUPLICATION_URI'] .= $newRequest . '&';
            } else {
                $this->arResult['DUPLICATION_URI'] .= '?';
            }
            $this->arResult['DUPLICATION_URI'] .=  'show-duplication=' . $vDuplication;
        }
    }
    //endregion
}