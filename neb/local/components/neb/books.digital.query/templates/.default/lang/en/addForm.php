<?
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_COMMENT"] = "Add a comment";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_DESRIBE"] = "Describe why you need this book";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_REQ"] = "Required field";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_FASTER"] = "Your comment may speed up the process of digitizing";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_ADD"] = "Add";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_CANCEL"] = "Cancel";
?>
