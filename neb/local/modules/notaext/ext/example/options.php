<?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var $arOptions array настройки расширения
 */
$arOptions = array(
    'arTabs' => array(
        array(
            'DIV' => 'edit1',
            'TAB' => 'Настройки ширины',
            'ICON' => '',
            'TITLE' => 'Настройки ширины'
        ),
        array(
            'DIV' => 'edit2',
            'TAB' => 'Настройки высоты',
            'ICON' => '',
            'TITLE' => 'Настройки высоты'
        )
    ),
    'arGroups' => array(
        'MAIN' => array('TITLE' => 'Ширина', 'TAB' => '0', 'SORT' => 3)
    ),
    'arOptions' => array(
        'wWidth' => array(
            'GROUP' => 'MAIN',
            //'TAB' => '1',
            'TITLE' => 'Ширина',
            'TYPE' => 'INT',
            'DEFAULT' => 900,
            'SORT' => 2,
            'NOTES' => 'Подсказка для ширины'
        ),
        'wHeight' => array(
            //'GROUP' => 'MAIN',
            'TAB' => '1',
            'TITLE' => 'Высота',
            'TYPE' => 'INT',
            'DEFAULT' => 900,
            'SORT' => 3,
            'NOTES' => 'Подсказка для высоты'
        )
    )
);