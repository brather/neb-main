<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->SetTitle( Loc::getMessage('CITY_SELECT') );
/**
 * @var CMain $APPLICATION
 */
$APPLICATION->IncludeComponent(
    'neb:library.cities', '', Array()
);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");