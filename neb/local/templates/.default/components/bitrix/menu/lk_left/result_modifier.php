<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$obNUser = new nebUser();
$obNUser->getRole();

$arParams['IS_LIBRARY'] = ($obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR or $obNUser->role == UGROUP_LIB_CODE_CONTROLLER);

foreach($arResult as $menuItem) {
    if($menuItem['SELECTED']) {
        $APPLICATION->SetTitle($menuItem['TEXT']);
    }
}