<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 */
?>
<br>

<div class="lk-table">
    <div class="lk-table__column" ></div>
    <div class="lk-table__column" ></div>
    <div class="lk-table__column" ></div>
    <div class="lk-table__column" ></div>
    <div class="lk-table__column" ></div>
    <div class="lk-table__column" ></div>
    <div class="lk-table__column" ></div>
    <div class="lk-table__column" ></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('AUTHOR_TITLE') ?>
        </li>
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('NAME_TITLE') ?>
        </li>
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('PUBLISH_YEAR_TITLE') ?>
        </li>
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('ISBN_TITLE') ?>
        </li>
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('PDF_TYTLE') ?>
        </li>
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('VIEWS') ?>
        </li>
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('ACTION') ?>
        </li>
        <li class="lk-table__header-kind">
        </li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['items'] as $book) { ?>
            <ul class="lk-table__row">
                <li class="lk-table__col">
                    <?= $book['Author'] ?>
                </li>
                <li class="lk-table__col">
                    <? if (true !== $book['new']) { ?>
                        <a target="_blank" href="<?= $book['DETAIL_URL'] ?>">
                            <?= $book['Name'] ?>
                        </a>
                    <? } else { ?>
                        <?= $book['Name'] ?>
                    <? } ?>
                </li>
                <li class="lk-table__col">
                    <?= $book['PublishYear'] ?>
                </li>
                <li class="lk-table__col">
                    <?= $book['ISBN'] ?>
                </li>
                <li class="lk-table__col">
                    <? if (isset($book['pdfLink'])) { ?>
                        <a target="_blank" href="<?= $book['pdfLink'] ?>">
                            <?= Loc::getMessage('PDF_TYTLE') ?>
                        </a>
                    <? } ?>
                </li>
                <li class="lk-table__col">
                    <?= $book['views'] ?>
                </li>
                <li class="lk-table__col">
                    <? if ('confirmed' === $book['status']) {
                        echo Loc::getMessage('CONFIRMED');
                    } elseif ('refused' === $book['status']) {
                        echo Loc::getMessage('REFUSED');
                    } else { ?>
                        <a
                            href="#"
                            class="rest-action confirm"
                            data-method="PUT"
                            data-result-msg="<?= Loc::getMessage('CONFIRMED') ?>"
                            data-confirm-msg="<?= Loc::getMessage('CONFIRM') ?>?"
                            data-url="<?= $book['confirmUrl'] ?>">
                            <?= Loc::getMessage('CONFIRM') ?>
                        </a>
                        <br>
                        <a
                            href="#"
                            class="rest-action confirm"
                            data-method="PUT"
                            data-result-msg="<?= Loc::getMessage('REFUSED') ?>"
                            data-confirm-msg="<?= Loc::getMessage('REFUSE') ?>?"
                            data-url="<?= $book['refuseUrl'] ?>">
                            <?= Loc::getMessage('REFUSE') ?>
                        </a>
                    <? } ?>
                </li>
                <li class="lk-table__col">
                    <?= true === $book['new'] ? Loc::getMessage('IS_NEW') : '' ?>
                </li>
            </ul>
        <? } ?>
    </section>
    <?= $arResult['nav'] ?>
</div>