<?php

namespace LbcBundle;

use LbcBundle\DependencyInjection\Compiler\CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class LbcBundle extends Bundle
{
    const BBK_EDITOR_ROLE = 'ROLE_OPERATOR';
    const BBK_OPERATOR_BBK_ROLE = 'ROLE_OPERATOR_BBK';

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new CompilerPass());
    }
}
