<?php
/**
 * User: agolodkov
 * Date: 01.07.2016
 * Time: 10:38
 */
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ArchiveStatsGuestList extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stats:archive:guest-list')
            ->setDescription('Archive statistics - guest list')
            ->addOption(
                'period',
                null,
                InputOption::VALUE_REQUIRED,
                'time to archive in seconds'
            )
            ->addOption(
                'path',
                null,
                InputOption::VALUE_REQUIRED,
                'backups root'
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);

        $this->getContainer()
            ->get('bitrix')
            ->requireProlog(
                $this->getContainer()
                    ->get('kernel')
                    ->getRootDir()
            );

        $fileName = $input->getOption('path');
        if (!is_dir($fileName)) {
            throw new \Exception("Directory $fileName not found");
        }

        $labels = [
            'ID'           => 'Посетитель',
            'LAST_USER_ID' => 'Пользователь',
            'LAST_IP'      => 'Ip адрес',
        ];

        \NebMainHelper::includeModule('statistic');
        $recordsLimit = \COption::GetOptionString('statistic', 'RECORDS_LIMIT');
        \COption::SetOptionString('statistic', 'RECORDS_LIMIT', 0);
        $guest = new \CGuest();
        $rsGuests = $guest->GetList(
            $by = [],
            $order = [],
            [
                'LAST_DATE1' => ConvertTimeStamp(
                    time() - (integer)$input->getOption('period')
                ),
            ]
        );
        \COption::SetOptionString('statistic', 'RECORDS_LIMIT', $recordsLimit);

        $user = new \CUser();
        $fileName .= '/guest_list_' . date('Y-m-d', time()) . '.csv';
        $fp = fopen($fileName, 'w');
        foreach ($labels as $key => $value) {
            $labels[$key] = iconv('UTF-8', 'cp1251', $value);
        }
        fputcsv($fp, $labels, ';');
        while ($guest = $rsGuests->Fetch()) {
            $fields = array_merge(
                $labels,
                array_intersect_key($guest, $labels)
            );
            $userData = [];
            if (!empty($fields['LAST_USER_ID'])) {
                $userData = $user->GetByID($fields['LAST_USER_ID'])->Fetch();
            }
            if (empty($userData)) {
                $fields['LAST_USER_ID'] = iconv('UTF-8', 'cp1251', 'не зарегистрированный');
            } else {
                $fields['LAST_USER_ID'] = "[{$userData['ID']}]({$userData['EMAIL']})";
            }
            fputcsv($fp, $fields, ';');
        }
        fclose($fp);

        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}