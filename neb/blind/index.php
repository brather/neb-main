<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Раздел для слепых");
?>
<div class="container" lang="ru-RU">
    <div class="header-logo"></div>
    <div class="v-spacing-lg text-center" lang="ru-RU">Режим для слепых</div>
    <?$APPLICATION->IncludeComponent(
        "bitrix:menu",
        "blind",
        array(
            "ROOT_MENU_TYPE" => "top_" . LANGUAGE_ID,
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "N",
            "MENU_CACHE_GET_VARS" => array(),
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "left_" . LANGUAGE_ID,
            "USE_EXT" => "N",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N"
        ),
        false
    );?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>