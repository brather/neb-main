<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->SetTitle(Loc::getMessage('WORKPLACES'));
/**
 * @var CMain $APPLICATION
 */
echo '<p>С помощью карты или формы поиска найдите наиболее подходящий вам по расположению электронный читальный зал - специально оборудованное помещение, в котором организован доступ к фондам НЭБ</p>';
$APPLICATION->IncludeComponent(
    'neb:workplaces.cities', '', Array()
);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");