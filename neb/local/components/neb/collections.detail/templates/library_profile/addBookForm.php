<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;
use \Bitrix\Main\Page\Asset,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable;

CJSCore::Init();

?>
<!doctype html>
<html class="inmodalhtml" lang="ru">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<script src="<?=MARKUP?>js/libs/modernizr.min.js"></script>
		<meta name="viewport" content="width=device-width"/>

		<title><?$APPLICATION->ShowTitle()?></title>
		<?$APPLICATION->ShowHead();?>
		<link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />

		<?Asset::getInstance()->addJs(MARKUP.'js/libs/jquery.min.js');?>
		<?Asset::getInstance()->addJs('/local/templates/.default/js/script.js');?>
		<?Asset::getInstance()->addJs(MARKUP.'js/slick.min.js');?>
		<?Asset::getInstance()->addJs(MARKUP.'js/plugins.js');?>
		<?Asset::getInstance()->addJs(MARKUP.'js/jquery.knob.js');?>
		<?Asset::getInstance()->addJs(MARKUP.'js/script.js');?>

		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<!--script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js" data-start></script-->
		<!--script src="/local/templates/adaptive/js/jquery/jquery-ui-1.10.4.custom.min.js"></script-->

		<?if($APPLICATION->GetCurPage() == '/'){?>
			<script type="text/javascript">
				$(function() {
					$('html').addClass('mainhtml'); /* это для морды */
				}); /* DOM loaded */
			</script>
		<?}?>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
		<script type="text/javascript">VK.init({apiId: 4525156, onlyWidgets: true});</script>
	</head>

	<body class="inmodal">
		<script type="text/javascript">/*console.log('addbook from view collection');*/</script>
		<base target="_self">
		<div class="modal-container modal-results">
			<?global $navParent;
			$navParent = '_self';
			$requestParams = array();
			if(!empty($_REQUEST['COLLECTION_ID'])) {
				$requestParams['COLLECTION_ID'] = (int)$_REQUEST['COLLECTION_ID'];
			}?>
			<?$APPLICATION->IncludeComponent(
				"exalead:search.form",
				"",
				Array(
					"PAGE" => (''),
					"POPUP_VIEW" => 'Y',
					"ACTION_URL" => $APPLICATION->GetCurPage(),
					"REQUEST_PARAMS" => $requestParams
				));?>

			<?$nebUser = new nebUser();
			$lib = $nebUser->getLibrary();
			$arData = array();
			if(!empty($lib['PROPERTY_LIBRARY_LINK_VALUE']))	{
                Loader::includeModule("highloadblock");
				$hlblock = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
                $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
                $arData = $entity_data_class::getById($lib['PROPERTY_LIBRARY_LINK_VALUE'])->fetch();
			}?>

			<?$arParamsSearchPage = array(
				'ID_LIBRARY' => $arData['UF_ID'],
				'FORM_ACTION' => '/profile/collection/'
			);
			if(!empty($requestParams['COLLECTION_ID'])) {
				$arParamsSearchPage['COLLECTION_ID'] = $requestParams['COLLECTION_ID'];
				$arParamsSearchPage['FORM_ACTION'] = '/profile/collection/' . $requestParams['COLLECTION_ID'] . '/';
			}
			if(isset($_REQUEST['q'])) {?>
				<?$APPLICATION->IncludeComponent(
					"exalead:search.page",
					"popup_library_collection",
					$arParamsSearchPage,
					false
					);?>
			<?}?>
		</div>

		<link href="/local/templates/adaptive/css/bootstrap.min.css" rel="stylesheet">
		<link href="/local/templates/adaptive/vendor/custom-select/bootstrap-select.min.css" rel="stylesheet">
		<link href="/local/templates/adaptive/css/font-awesome.min.css" rel="stylesheet">
		<link href="/local/templates/adaptive/vendor/jquery-ui/jquery-ui.css" rel="stylesheet">
		<link href="/local/templates/adaptive/css/style.css" rel="stylesheet">

		<!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript">defer$()</script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script-->
		<!--script src="/local/templates/adaptive/js/neb/jq-select-replace.js"></script-->
		<script src="/local/templates/adaptive/vendor/custom-select/bootstrap-select.min.js"></script>
		<script src="/local/templates/adaptive/js/bootstrap.min.js"></script>
		<script src="/local/templates/adaptive/js/spin.min.js"></script>
		<script src="/local/templates/adaptive/js/script.js"></script>
		<script type="text/javascript">
			$('#main_search_form').attr('action',''); /*форма та же что и на сайте, открывается страница в ифрейме*/
		</script>
	</body>
</html>