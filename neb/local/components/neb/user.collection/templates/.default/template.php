<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="b-filter_wrapper">
	<span class="sort_wrap sort_wrap_three">
		<a <?=SortingExalead("BOOK_AUTHOR")?>><?=GetMessage("FAVORITES_SORT_AUTHOR");?></a>
		<a <?=SortingExalead("BOOK_NAME")?>><?=GetMessage("FAVORITES_SORT_MAIN_TITLE");?></a>
		<a <?=SortingExalead("PUBLISH_DATE")?>><?=GetMessage("FAVORITES_SORT_DATE");?></a>
	</span>	
</div>
<input type="hidden" id="idCollection" value="<?=$arResult['idCollection']?>">
<ul class="b-filter_list_wrapper search-result__content-list clearfix">
	
		<?php
            foreach($arResult['ITEMS'] as $key => $arItem):
                if('quotes' == $arItem['UF_TYPE']):
                    $APPLICATION->IncludeFile($this->__folder . '/quote.php', array('arItem' => $arItem, 'key' => $key, 'arResult' => $arResult));

                elseif('books' == $arItem['UF_TYPE']):
                    $APPLICATION->IncludeFile($this->__folder . '/book.php',  array('arItem' => $arItem, 'key' => $key, 'arResult' => $arResult));

                elseif('bookmarks' == $arItem['UF_TYPE']):
                    $APPLICATION->IncludeFile($this->__folder . '/bookmark.php',  array('arItem' => $arItem, 'key' => $key, 'arResult' => $arResult));

                elseif('notes' == $arItem['UF_TYPE']):
                    $APPLICATION->IncludeFile($this->__folder . '/note.php',  array('arItem' => $arItem, 'key' => $key, 'arResult' => $arResult));

                else:
                    continue;
                endif;

            endforeach;
		?>
</ul>
<?=$arResult['STR_NAV']?>