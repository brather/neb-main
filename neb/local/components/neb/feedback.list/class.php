<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 19.09.2016
 * Time: 14:00
 */

CBitrixComponent::includeComponentClass('neb:feedback');

use \Bitrix\Main\Application,
    \Bitrix\Main\UI\PageNavigation;

/**
 * Class FeedbackListComponent
 */
class FeedbackListComponent extends FeedbackComponent
{
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent() {
        $this->includeModules(['form']);
        $this->getRequest();
        $this->getUser();
        $this->getForm(['SID' => 'SIMPLE_FORM_1']);
        $this->getList();
        $this->includeComponentTemplate();
    }

    /**
     * Получение списка заявок
     */
    private function getList() {

        $arStatuses = $arThemes = $arResult = $arUsers = [];

        if (empty($this->arResult['ERROR']))
            $arThemes = $this->getThemes();

        // получение всех заявок
        $arTickets = $this->getTickets($this->arForm['ID']);

        // получение всех исполнителей
        $arExecutors = $this->getExecutors();

        // получение объекта постраничной навигации
        $iPageCount = intval(COption::GetOptionString("form", "RESULTS_PAGEN")) ? : 10;
        $obPageNavigation = $this->getPageNavigation(count($arTickets), $iPageCount);
        $iCurrentPage = $obPageNavigation->getCurrentPage() - 1;

        // фомрирование результирующего массива
        
        // если заявок только на одну страницу или нажат показ всех заявок
        if ($iPageCount >= count($arTickets) || strpos($_SERVER['REQUEST_URI'], 'page-all') !== false) {
            foreach ($arTickets as $arItem) {
                $arItem['ANSWER'] = $this->getResultAnswer($this->arForm['ID'], $arItem['ID']);
                $arUsers[$arItem['USER_ID']] = $arItem['USER_ID'];
                $arResult[$arItem['ID']] = $arItem;
            }
        }
        // при количестве заявок больше одной страницы
        else {
            for ($i = $iCurrentPage * $iPageCount; $i <= $iCurrentPage * $iPageCount + $iPageCount - 1; $i++) {
                if (!$arTickets[$i]['ID'])
                    continue;
                $arTickets[$i]['ANSWER'] = $this->getResultAnswer($this->arForm['ID'], $arTickets[$i]['ID']);
                $arUsers[$arTickets[$i]['USER_ID']] = $arTickets[$i]['USER_ID'];
                $arResult[$arTickets[$i]['ID']] = $arTickets[$i];
            }
        }

        // получение пользователей-авторов из заявок
        $arUsers = $this->getUsers(array_values($arUsers));
        foreach ($arResult as &$arItem)
            if (!empty($arUsers[$arItem['USER_ID']]))
                $arItem['USER'] = $arUsers[$arItem['USER_ID']];

        // статусы
        $arStatuses = [];
        foreach ($this->arForm['STATUSES'] as $arFields)
            $arStatuses[$arFields['ID']] = $arFields;

        $this->arResult['STATUSES']  = $arStatuses;
        $this->arResult['THEMES']    = $arThemes;
        $this->arResult['EXECUTORS'] = $arExecutors;
        $this->arResult['ITEMS']     = $arResult;
        $this->arResult['NAV']       = $obPageNavigation;
    }

    /**
     * Получение ответов для результа формы
     *
     * @param $iForm - идентификатор формы
     * @param $iResult - идентификатор результата
     * @return array
     */
    private static function getResultAnswer($iForm, $iResult) {

        $obForm = new CForm();
        $obForm->GetResultAnswerArray($iForm, $arrColumns, $arrAnswers, $arResult, ["RESULT_ID" => $iResult]);

        reset($arResult);
        $arResult = current($arResult);
        foreach ($arResult as $sCode => &$arItem)
            $arItem = current($arItem);

        return $arResult;
    }

    /**
     * Получение сформированного фильтра
     *
     * @return mixed
     */
    private function getFilter() {

        $arRequest = $arFields = $arFilter = [];

        if ($this->arRequest['FEEDBACK']['ACTION'] == 'LIST')
            foreach ($this->arRequest['FEEDBACK'] as $sCode => $sVal)
                $arRequest[$sCode] = $sVal;

        // по идентификатору ID
        if ($arRequest['ID'] > 0)
            $arFilter['ID'] = $arRequest['ID'];

        // по идентификатору читателя
        if ($arRequest['USER_ID'] > 0)
            $arFilter['USER_ID'] = $arRequest['USER_ID'];

        // по дате регистрации
        if (!empty($arRequest['REGISTER_DATE_FROM']))
            $arFilter['DATE_CREATE_1'] = $arRequest['REGISTER_DATE_FROM'];
        if (!empty($arRequest['REGISTER_DATE_TO']))
            $arFilter['DATE_CREATE_2'] = $arRequest['REGISTER_DATE_TO'];

        // по дате окончания
        if (!empty($arRequest['CLOSE_DATE_FROM']))
            $arFilter['TIMESTAMP_1'] = $arRequest['CLOSE_DATE_FROM'];
        if (!empty($arRequest['CLOSE_DATE_TO']))
            $arFilter['TIMESTAMP_2'] = $arRequest['CLOSE_DATE_TO'];

        // по статусу
        if ($arRequest['STATUS'] > 0)
            $arFilter['STATUS_ID'] = $arRequest['STATUS'];

        // по теме
        if (!empty($arRequest['THEME']))
            $arFields[] = ["CODE" => "FB_THEME", 'PARAMETER_NAME' => 'ANSWER_VALUE', "VALUE" => $arRequest['THEME']];

        // по исполнителю
        if ($arRequest['EXECUTOR'] > 0)
            $arFields[] = ["CODE" => "FB_USER_ID", "VALUE" => $arRequest['EXECUTOR']];

        // по содержанию
        if (!empty($arRequest['TEXT']))
            $arFields[] = ["CODE" => "FB_TEXT", "VALUE" => $arRequest['TEXT']];

        // по собственной библиотеке
        if ($this->iUserLibrary)
            $arFields[] = ["CODE" => "FB_LIBRARY_ID", "VALUE" => $this->iUserLibrary];

        if (!empty($arFields))
            $arFilter["FIELDS"] = $arFields;

        return $arFilter;
    }

    /**
     * Получение всех заявок
     *
     * @param $iFormId
     * @return array
     */
    private function getTickets($iFormId) {

        $arResult = [];

        $arFilter = $this->getFilter();

        $by = 's_id'; $order = 'desc';
        $arSort = ['id', 'date_create', 'timestamp', 'user_id', 'status'];
        $arOrder = ['asc', 'desc'];

        if (in_array($this->arRequest['by'], $arSort))
            $by = 's_' . $this->arRequest['by'];
        if (in_array($this->arRequest['order'], $arOrder))
            $order = $this->arRequest['order'];

        // выборка всех заявок (да, Битриксы - мудачье, не знающее про OFFSET)
        $obFormResult = new CFormResult();
        $rsResults = $obFormResult->GetList($iFormId, $by, $order,  $arFilter, $is_filtered, "N");
        while ($arFields = $rsResults->Fetch())
            $arResult[] = $arFields;
        unset($rsResults, $arFields);

        return $arResult;
    }

    /**
     * Метод возвращает объект навигационной цепочки
     *
     * @param $iTotalCount
     * @param int $iPageCount
     * @return PageNavigation
     */
    private static function getPageNavigation($iTotalCount, $iPageCount = 10) {

        $obPageNavigation = new PageNavigation('tickets');
        $obPageNavigation->allowAllRecords(true)->initFromUri();
        $obPageNavigation->setRecordCount($iTotalCount)->setPageSize($iPageCount);

        return $obPageNavigation;
    }

    /**
     * Получение списка тем сообщений
     *
     * @return array
     */
    private static function getThemes() {

        $arResult = [];

        $obCFormField = new CFormField();
        $arTheme = $obCFormField->GetBySID('FB_THEME')->Fetch();
        if ($arTheme['ID'] < 1)
            return $arResult;

        $obFormAnswer = new CFormAnswer();
        $rsAnswers = $obFormAnswer->GetList($arTheme['ID'], $by="s_sort", $order="asc", [], $is_filtered);
        while ($arItem = $rsAnswers->Fetch())
            $arResult[$arItem['ID']] = $arItem;

        return $arResult;
    }

    /**
     * Получение списка всех исполнителей
     *
     * @return array
     */
    private function getExecutors() {

        $arResult = $arUsers = [];
        $sJoin = $sWhere = '';

        if ($this->arForm['FIELDS']['FB_USER_ID']['ID'] > 0)
            $sWhere .= " AND t1.FIELD_ID = " . $this->arForm['FIELDS']['FB_USER_ID']['ID'];

        if ($this->iUserLibrary) {
            $sJoin  = "INNER JOIN b_form_result_answer as t2 ON t1.RESULT_ID = t2.RESULT_ID";
            $sWhere .= " AND t2.FIELD_ID = " . $this->arForm['FIELDS']['FB_LIBRARY_ID']['ID']
                                . " AND t2.USER_TEXT = $this->iUserLibrary ";
        }

        $sQuery = "
            SELECT DISTINCT
              t1.USER_TEXT as USER_ID
            FROM
              b_form_result_answer as t1
              $sJoin
            WHERE
              t1.USER_TEXT != ''
              $sWhere
            ORDER BY
              t1.USER_TEXT
        ";

        $rsRecords = Application::getConnection()->query($sQuery);
        while ($arItem = $rsRecords->fetch())
            $arUsers[] = $arItem['USER_ID'];

        if (!empty($arUsers))
            $arResult = $this->getUsers($arUsers);

        return $arResult;
    }
}