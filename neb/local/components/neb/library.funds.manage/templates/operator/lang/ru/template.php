<?
$MESS['LIBRARY_FUNDS_MANAGE_ADD_BOOK'] = "Добавить книгу";
$MESS['LIBRARY_FUNDS_MANAGE_SEARCH'] = "Найти";
$MESS['LIBRARY_FUNDS_MANAGE_AUTHOR'] = "Автор";
$MESS['LIBRARY_FUNDS_MANAGE_TITLE'] = "Название";
$MESS['LIBRARY_FUNDS_MANAGE_DATE_ADDED'] = "Дата <br> добавления";
$MESS['LIBRARY_FUNDS_MANAGE_REQUEST_DEL'] = 'Запрос <br> <span class="nowrap">на удаление</span>';
$MESS['LIBRARY_FUNDS_MANAGE_DO_REQUEST_DEL'] = "<span>Удаление (дубликатов и экземпляров)</span>";
$MESS['LIBRARY_FUNDS_DELETE_CONFIRM'] = "Вы уверены?";
$MESS['LIBRARY_FUNDS_DELETED'] = "Удалено";
