<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Статистика НЭБ');
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
require(dirname(__FILE__) . '/includes/header.php');
$APPLICATION->IncludeComponent(
    'neb:statistics',
    '',
    array_intersect_key(
        $_REQUEST,
        array(
            'dateFrom'        => true,
            'dateTo'          => true,
            'dateReportFrom' => true,
            'dateReportTo'   => true,
            'LIBRARY_ID'      => true,
        )
    ),
    $component
);
require(dirname(__FILE__) . '/includes/footer.php');
