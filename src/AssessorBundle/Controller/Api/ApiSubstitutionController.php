<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 13:23
 */

namespace AssessorBundle\Controller\Api;

use AssessorBundle\Form\NebQuerySubstitutionType;
use AssessorBundle\Traits\AssessorRatingTrait;
use Doctrine\ORM\QueryBuilder;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ApiSubstitutionController
 *
 * @DI\Service("assessor.api.substitution", scope="request")
 */
class ApiSubstitutionController implements RatingApiInterface
{
    use AssessorRatingTrait;

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'AssessorBundle:NebQuerySubstitution';
    }

    /**
     * @return NebQuerySubstitutionType
     */
    public function getFormType()
    {
        return new NebQuerySubstitutionType();
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return void
     */
    public function handleAutoComplete($qb)
    {
        $query = $this->request->query->get('query');
        $qb->where('t.originalQuery LIKE :query');
        $qb->setParameter('query', $query . '%');
    }
}