<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;

?>
<section class="innersection innerwrapper clearfix " id="search_page_block">
<?$APPLICATION->IncludeComponent(
    'neb:library.plan.list',
    '',
    array(
        "HL_CODE"                  => HIBLOCK_CODE_DIGITIZATION_PLAN_LIST,
        "COUNT_ON_PAGE"            => "15",
        "SEF_FOLDER"               => $arParams['SEF_FOLDER'],
        "DETAIL_PAGE_URL"          => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['detail'],
        "ADD_PAGE_URL"             => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['add'],
        "ADD_PAGE_URL_FROM_READER" => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['reader'],
        "SORT"                     => array(
            "by"    => "UF_DATE_F_PLAN",
            "order" => "ASC"
        )
    ),
    false
);?>
</section>