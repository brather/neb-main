 
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="rel innersection innerwrapper clearfix">
	<h1>Библиотеки</h1>
	<form method="get" action="." class="b-map_search_filtr">
			<div class="b-map_search">
				<div class="wrapper">
					<div class="b-cytyinfo">
						<span class="b-map_search_txt">Найти ближайшую библиотеку в </span>
					</div>
					<input type="text" class="b-map_tb ui-autocomplete-input searchonmap" name="q" id="" autocomplete="off" placeholder="г. Москва">		
					<input type="submit" class="button_b" value="Найти" name="submit">
				</div>
			</div><!-- /.b-map_search -->
		</form>
	<div class="b-elar_usrs">
		<ol class="b-elar_usrs_list">
			<li>
				<span class="num">1.</span>
				<div class="b-elar_name iblock">
					<span class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</span>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, ул. Череповецкая, 17</div>
				</div>
			</li>
			<li>	
				<span class="num">2.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Президентская библиотека им. Б. Н. Ельцина</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, ул. Маршала Бирюзова, 32</div>
				</div>
			</li>
			<li>
				<span class="num">3.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, ул. Голубинская, 28</div>
				</div>
			</li>
			<li>
				<span class="num">4.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Президентская библиотека им. Б. Н. Ельцина</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, бул. Осенний, 12, корп.1</div>
				</div>
			</li>
			<li>
				<span class="num">5.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, пл. Киевского Вокзала, 2</div>
				</div>
			</li>
			<li>
				<span class="num">6.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Президентская библиотека им. Б. Н. Ельцина</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, ул. Раменки, 3</div>
				</div>
			</li>
			<li>
				<span class="num">7.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, Протопоповский пер., 3</div>
				</div>
			</li>
			<li>
				<span class="num">8.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Президентская библиотека им. Б. Н. Ельцина</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, Столярный пер., 3, корп.6</div>
				</div>
			</li>

		</ol>
	</div><!-- /.b-elar_usrs -->


</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>


</body>
</html>