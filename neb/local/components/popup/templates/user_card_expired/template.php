<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section class="innerwrapper clearfix">
    <div class="b-registration b-registration-popup rel">
        <form id="regform"  enctype="application/x-www-form-urlencoded" name="regform" class="b-form b-form_common b-regform" action="/popup/" method="post">

            <p class="note" style="margin: 0 auto;">
                Уважаемый читатель! Срок действия вашего читательского билета истек. Желаете ли Вы получить новый читательский билет?
            </p>

            <p class="note" style="margin: 0 auto;display: none;color:red;" id="error_setveryfied" >

            </p>

            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell " style="margin: 0 auto;">
                    <div class="field clearfix">
                        <button class="formbutton" value="1" type="button" onclick="document.location.href = '/profile/update_select/'">Да</button>
                        <button class="formbutton" value="1" type="button" onclick="document.location.href = '/profile';">Нет</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.b-registration-->
</section>
