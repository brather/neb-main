<?

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

// Constants & Main module
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/constants.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/main.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/notaext/autoloader.php');

// Helpers
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/functions.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/user/nebUsr.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/user/nebReader.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/PlanDigitalization.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/TheDistance.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/nebLibrary.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/saveQuery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/collectionUser.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/NebWorkplaces.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/NebMainHelper.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/CIBlockRSSHttps.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/Rightholder.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/NebMigration.php');
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/NebLinkLitres.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/TblEntities.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/helpers/TblReport.php');

// Event handlers
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/HighloadBlockHandler.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/IBlockSectionHandler.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/IBlockElementHandler.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/SetTypeUserRegister.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/SocialServicesAuth.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/SocServEventHandler.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/MainEventHandler.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/UserEventHandler.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/MailEventHandler.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/SaleEventHandler.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/WebFormsEventHandler.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event_handlers/WebFormsValidatorDateExNow.php');