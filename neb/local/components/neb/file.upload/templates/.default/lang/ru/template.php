<?php
$MESS['FILE_UPLOAD_T_TITLE'] = ' Загрузить файл';
$MESS['FILE_UPLOAD_T_SET_FILE'] = 'Выбрать файл';
$MESS['FILE_UPLOAD_T_TITLE_SHORT'] = 'Загрузить';
$MESS['FILE_UPLOAD_T_DELETE_FILE'] = ' Удалить';
$MESS['FILE_UPLOAD_T_CONFIRM_TITLE'] = 'Удалить?';
$MESS['FILE_UPLOAD_T_CONFIRM_YES'] = 'Удалить';
$MESS['FILE_UPLOAD_T_CONFIRM_TEXT'] = 'Вы уверены, что хотите удалить данный pdf?';
