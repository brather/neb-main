<?php

require_once(__DIR__ . '/../../../php_interface/include/PHPExcel.php');
CBitrixComponent::includeComponentClass("rest:rest.server");

/**
 * Class RestLibraryStatistic
 */
class RestUsersStatus extends RestServer
{
    /**
     * @var array
     */
    protected $_methodAllowGroups
        = array(
            'putAction' => array(
                6  => true,
                13 => true,
                14 => true,
            )
        );

    protected $_params
        = array(
            'active'    => null,
            'wchzLock'  => null,
            'verify'    => null,
            'user_id'   => null,
            'preVerify' => null,
            'libraryId' => null,
        );

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams()
    {
        $boolParams = [
            'active',
            'wchzLock',
            'verify',
            'preVerify',
        ];
        foreach ($boolParams as $paramName) {
            if (isset($this->_params[$paramName])) {
                $this->_params[$paramName] = filter_var(
                    $this->_params[$paramName],
                    FILTER_VALIDATE_BOOLEAN
                );
            }
        }
        if (isset($this->_params['user_id'])) {
            $this->_params['user_id'] = (integer)$this->_params['user_id'];
        }
        if (isset($this->_params['libraryId'])) {
            $this->_params['libraryId'] = (integer)$this->_params['libraryId'];
        }

        return $this;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function putAction()
    {
        if (empty($this->_params['user_id'])) {
            throw new Exception('"user_id" cannot be empty');
        }
        $this->arResult = [
            'success' => true,
        ];
        if (isset($this->_params['active'])) {
            if (!nebUser::getCurrent()->Update(
                $this->_params['user_id'],
                ['ACTIVE' => true === $this->_params['active'] ? 'Y' : 'N']
            )
            ) {
                throw new Exception(nebUser::getCurrent()->LAST_ERROR);
            }
        }
        if (isset($this->_params['verify'])) {

            // ============================= Верификация правообладателей =============================

            // получение ID группы "Правообладатели"
            $obRightHolder = new \RightHolder();
            $iRighrHolderID = $obRightHolder->getUserGroupRoleID();

            if ($iRighrHolderID > 0) {

                // проверка пользователя на принадлежность группе "Правообладатели"
                $rsUserGroup = \Bitrix\Main\UserGroupTable::getList(array(
                    'filter' => array(
                        'USER_ID'  => $this->_params['user_id'],
                        'GROUP_ID' => $iRighrHolderID
                    ),
                    'select' => array('USER_ID')
                ));
                if ($arUserGroup = $rsUserGroup->fetch()) {

                    // выборка текущего статуса пользователя
                    $arUser = \Bitrix\Main\UserTable::getList(array(
                        'filter' => array('ID'  => $this->_params['user_id']),
                        'select' => array('UF_STATUS')
                    ))->fetch();

                    // определение нового статуса пользователя
                    $iNewStatus = $arUser['UF_STATUS'] == nebUser::USER_STATUS_VERIFIED
                        ? nebUser::USER_STATUS_REGISTERED
                        : nebUser::USER_STATUS_VERIFIED;

                    // обновление статуса пользователя
                    $obUser = new \CUser;
                    $obUser->Update($this->_params['user_id'], array('UF_STATUS' => $iNewStatus));

                    // оповещение пользователя о успешной верификации
                    if ($iNewStatus == nebUser::USER_STATUS_VERIFIED) {
                        $obUser->SendUserInfo(
                            $this->_params['user_id'],
                            SITE_ID,
                            "Ваша заявка на регистрацию утверждена. Вы можете заключить договора на предоставление
                            доступа к произведениям в НЭБ и анализировать статистику просмотра изданий",
                            true
                        );
                    }

                    return 200;
                }
            }
            unset($obRightHolder, $iRighrHolderID, $rsUserGroup, $arUserGroup);


            // ================================= Верификация читателей РГБ ==========================================

            try {
                $nebUser = new nebUser($this->_params['user_id']);
                if (true === $this->_params['verify']) {
                    $nebUser->verify();
                    $params = [
                        'UF_STATUS' => nebUser::USER_STATUS_VERIFIED,
                    ];
                } else {
                    $params = [
                        'UF_STATUS' => nebUser::USER_STATUS_REGISTERED,
                    ];
                }
                if (true === $this->_params['preVerify']) {
                    $params['UF_PREVERIFY_TIME'] = ConvertTimeStamp(
                        time(), 'FULL'
                    );
                }
                if (0 < (integer)$this->_params['libraryId']) {
                    $params['UF_LIBRARY'] = $this->_params['libraryId'];
                }
                if (
                    nebUser::getCurrent()->Update(
                        $this->_params['user_id'],
                        $params
                    )
                    && true === $this->_params['verify']
                ) {
                    $obUser = new \CUser;
                    $obUser->SendUserInfo(
                        $this->_params['user_id'],
                        SITE_ID,
                        "Приветствуем Вас как нового пользователя НЭБ!",
                        true
                    );
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
                if (nebUser::ERROR_NOT_FULL_REGISTER === $e->getCode()) {
                    $message = 'Читатель не прошел полную регистрацию';
                }
                throw new Exception($message, $e->getCode(), $e);
            }
        }

        return 200;
    }
}