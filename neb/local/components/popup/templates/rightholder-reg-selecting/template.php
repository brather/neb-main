<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;

?>
<section class="innerwrapper clearfix">
    <div class="b-registration rel">
        <form id="regform" enctype="application/x-www-form-urlencoded"
              name="regform" class="b-form b-form_common b-regform"
              method="post">

            <p class="note" style="margin: 0 auto;">
                <?= Loc::getMessage('ESIA_ACCESS_QUESTION') ?>
            </p>

            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell ">
                    <div class="field clearfix divdisable">
                        <button class="formbutton" value="1" type="button"
                                onclick="document.location.href = '/registration/rh-individual/';return false;">
                            <?= Loc::getMessage('RH_INDIVIDUAL') ?>
                        </button>
                        <br>
                        <br>
                        <button class="formbutton" value="1" type="button"
                                onclick="document.location.href = '/registration/rh-entity/';return false;">
                            <?= Loc::getMessage('RH_ENTITY') ?>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- /.b-registration-->
</section>