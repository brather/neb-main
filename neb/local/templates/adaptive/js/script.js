/* расхлоп */
(function(){
    $("[data-list-expandable]").on('click', 'dt > span', function(){
        $(this).closest('dt').siblings('dt').toggleClass('additional-expandable__--expanded', false);
        $(this).closest('dt').toggleClass('additional-expandable__--expanded');
    });
    $("[data-style-expand]").on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('opened');
    });
    if ($.fn.popover) {
        $('[data-toggle="popover"]').popover({trigger:'click'});
        $('[data-hover-toggle="popover"]').popover({trigger:'hover'});
    }

    $(document).on('click', '[data-target-toggle]', function(e){
        e.preventDefault();
        var toggler = $(this),
            targetSelector = toggler.attr('[data-target-toggle]'),
            target = $(''+targetSelector);
        target.toggle();
    });
    $(document).on('click', '[data-target-toggle-class]', function(e){
        e.preventDefault();
        var toggler = $(this),
            targetSelector = toggler.attr('data-target-toggle-class'),
            toggletarget = $(''+targetSelector);
        toggletarget.toggleClass('show', true);

        $(document).off('click.menuout').on('click.menuout',function(e){//когда дропдаун открыт...
            var clickTarget = $(e.target),
                allChilds = $(toggletarget).find('*');
            if ( $(e.target).is(toggler) ) {//...клик по кнопке его закрывает
                $(document).off('click.menuout');
                toggletarget.toggleClass('show', false);
            }
            else if ( $(e.target).is(toggletarget) || $(e.target).is(allChilds) ) {/*клик по выпадашке*/
            }            
            else {// ...кликаутсайд его закрывает
                $(document).off('click.menuout');
                toggletarget.toggleClass('show', false);
            }
        });

    });

    
    /* аккордион в факе */
    $('[data-faq-list-expandable] dd').hide();
    $('[data-info-tabs]').show();
    $('[data-info-tabs] > div').not('.active').hide();
    var hash = window.location.hash,
        linkhash = hash.substr(1);
    $('[data-faq-list-expandable] dt > a, [data-faq-list-expandable] a[href^="#"]').on('click', function(e){
        e.preventDefault();
        if ($(e.target).attr("href")) {
            linkhash = $(e.target).attr("href").substr(1);
        }
        if(history.pushState) {
            history.pushState(null, null, '#'+linkhash);
        }
        else {
            window.location.hash = linkhash;
        }
        $('[data-faq-list-expandable] dd').not( $(this).closest('dt').next('dd') ).slideUp();        
        $("[data-faq-list-expandable]").find('dt[id="' + linkhash + '"]').next('dd').slideToggle();
    });

    


    if (hash) {
        $('[data-faq-list-expandable] dt[id="' + linkhash + '"] > a').click();        
    }

    /* Ссылка назад */    
    if (window.history.length > 1) {
        $('[data-link-back]').on('click', function(e){
            e.preventDefault();
            window.history.go(-1);
        });
    } else {
        $('.linkback-overall').hide();
    }

    $('[data-masked]').each(function(){
        var el = $(this),
            data = el.data('masked') || '+7 (999) 999-99-99';
        if (typeof $.mask == 'object') {
            el.mask(data);
        }
        else if (console) {
            //console.log('http://digitalbush.com/projects/masked-input-plugin');
        }
    });

    $(document).on('change', '[data-checkbox-for]', function(e){
        var toggler = $(this);
        FRONT.assets.bitrixCheckboxReverser(e, toggler);
    });
    $(document).on('click','[data-add-book-to-collection]', function(e){
        var toggler = $(this);
        FRONT.assets.addBookToCollection(e, toggler);
    });
    $(document).on('click', '[data-slide-toggler]', function(e){
        e.preventDefault();
        var button = $(this),
            container = button.closest('[data-slide-togglable]');
        container.toggleClass('expanded');
    });
    $(document).on('click', '[data-nex-row-toggle]', function(e){
        e.preventDefault();
        $(this).closest('tr').next().toggle();
    });
    $(document).on('click', '[data-stop-propagation]', function(e){
        e.stopPropagation();
    });
    $(document).on('click', '[data-prevent-default]', function(e){
        e.preventDefault();
    });

    $(document).on('click','[data-extsearch-toggle]',function(e){
        var toggler = $(this).get(0);
        FRONT.utils.toggleWidget(toggler,e);
    });

    //ie8
    $(function(){
        //$.cookie('name', 'value', { expires: 7, path: '/' });
        var html = $('html'),
            messageContainer = $('.lte8 .lte8mes');
        messageContainer.html('<nobr>Вы используете устаревшую версию браузера.</nobr> <nobr>Сайт может отображаться некорректно.</nobr> <nobr>Пожалуйста, обновите Ваш браузер.</nobr>');
        if ( html.hasClass('bx-ie8') ) {
            $(document).on('click','.lte8',function(e){
                $.cookie('ie8showed', 'true', { expires: 1, path: '/' });
                $(this).find('.lte8mes').removeClass('unshowedmes');
            });
            if ( $.cookie('ie8showed') != 'true' ) {
                messageContainer.addClass('unshowedmes').parent().css('z-index','1');
            }
        }

    });
})();



/** Rest requests */
(function () {
    $.fn.sendRestRequest = function (options) {
        var url = $(this).data('url');
        var requestMethod = $(this).data('method');
        var userToken = getUserToken();
        var that = this;

        if ('function' === typeof options) {
            options = {success: options};
        }
        if ('function' === typeof options.success) {
            var successCallback = options.success;
            options.success = function () {
                var args = [];
                args.push.apply(args, arguments);
                args.push(this);
                successCallback.apply(that, args);
            }
        }
        if ('function' === typeof options.error) {
            var errorCallback = options.error;
            options.success = function () {
                var args = [];
                args.push.apply(args, arguments);
                args.push(this);
                errorCallback.apply(that, args);
            }
        }

        options = $.extend({
            url: url,
            type: requestMethod,
            parameters: {}
        }, options);

        options.url += (0 > options.url.indexOf('?')) ? '?' : '&';
        options.url += 'token=' + userToken;
        for (var key in options.parameters) {
            if (options.parameters.hasOwnProperty(key)) {
                options.url += '&' + key + '=' + encodeURIComponent(options.parameters[key]);
            }
        }
        $.ajax(options);
    };
    $.fn.activeDeleteRequest = function (options) {
        $(this).click(function(e) {
            e.preventDefault();
            var confirmMsg = $(this).data('confirm-msg');
            if (confirmMsg && confirm(confirmMsg)) {
                $(this).sendRestRequest(options);
            }
        });
    };
})(jQuery);
/**************
AUTOCOMPLETE
**************/
(function() {
    $.fn.autocompliteFunc = function(options) {

        var settings = $.extend({
            submit: true
        }, options );        

        var word, wordStart, wordEnd, targetWord;
        this.each(function() {

            //Активируем переход из подсказки в поиск по enter
            if (settings.submit) {
                $(this).keydown(function(event){
                    if(event.keyCode == 13 ||  event.keyCode== 108){
                        event.preventDefault();
                        //$('.b-search_ex').hide();
                        //$('.main-search__submit-btn').click();
                        $(this).closest('form').submit();
                    }
                });
            }

            var sInput = $(this),
                inputMapping = {
                    title: "Названия книг",
                    authorbook: "Автор"
                },
                listType = $(this).attr('data-type');


            if((typeof(sInput.attr('rel')) == 'undefined') || (sInput.attr('rel') != '0') ){
                showUri = true;
            }else{
                showUri = false;
            }

            $.widget( "custom.catcomplete", $.ui.autocomplete, {
                _create: function() {
                    this._super();
                    this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
                },
                _renderMenu: function(ul, items) {
                    var that = this,
                    currentCategory = "",
                    categotyTitle = "";
                    $(ul).addClass(listType);
                    $.each( items, function( index, item ) {
                        
                        var addFilterSelected; 
                        if ( $(sInput).closest('[data-add-search-widget]').hasClass('main-search__additional-fields') ) {
                            addFilterSelected = $(sInput).closest('[data-add-search-widget]').find('select[name^="theme"]').val();
                        }
                        if ( $(sInput).attr('data-filter') ) {
                            addFilterSelected = $(sInput).attr('data-filter');
                        }

                        if ( (listType == 'additional-condition' ) && (typeof inputMapping[addFilterSelected] !== undefined)  ) {
                            if ( item["category"] != inputMapping[addFilterSelected] ) { 
                                // если значение item.category не автор и не название то автокомплита не будет
                                return
                            }
                        }

                        var li;                        
                        if ( item.category != currentCategory ) {// первое значение становится заголовочным, значения идут подряд
                            ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                            currentCategory = item.category;
                        }
                        li = that._renderItemData( ul, item );

                        if (item.category) {
                            if(item.category == 'Названия книг') {categotyTitle = 'title'}
                            if(item.category == 'Автор'){categotyTitle = 'authorbook'}
                            li.attr( "aria-label", item.category + " : " + item.label );
                            li.attr( "data-category", categotyTitle);
                            li.attr( "data-request", $(li).text());
                        }

                        $(li).mouseover(function() {
                            $('.ui-autocomplete .ui-menu-item').mouseover(function(){
                                $('.ui-autocomplete li').removeClass('list-hover');
                                $(this).addClass('list-hover');
                            });
                            sInput.attr('data-category', $(li).data('category'));
                            sInput.attr('data-request', $(li).text());
                        });
                        $('.ui-autocomplete li').removeClass('list-hover');
                        if($('.ui-autocomplete a').hasClass('ui-state-focus')){
                            $('.ui-state-focus').parent().addClass('list-hover');
                        }

                        //Выделение слова при поиске
                        targetWord = that.term.toLowerCase().replace(/\s+/g," ").trim();
                        word = li[0].innerHTML;
                        if(word.toLowerCase().indexOf(targetWord)==0){
                            wordStart = word.slice(0, targetWord.length).bold();
                            wordEnd = word.slice(targetWord.length);
                            li[0].innerHTML = wordStart + wordEnd;
                        }

                        // console.log('where show?, settings.submit = '+settings.submit);

                        if (settings.submit) {
                            $(li).click(function(event){
                                event.preventDefault();
                                item = $(this);
                                //$('.main-search__general-field-input').val(.text());
                                if('authorbook' === item.attr('data-category')){
                                    // $('#main_search_form input[name^="text"]').val('');
                                    // hiddenInput = $('<input>').prop('type','hidden').attr('name', 'f_field[authorbook][]').attr('data-tag-store','').val('f/authorbook/' + item.text());
                                    // $('#main_search_form').append(hiddenInput);
                                    // $('.main-search__general-field-input').val('');

                                    $(sInput).val( item.text() );

                                } else if ('title' === item.attr('data-category')){
                                    // $('.main-search__general-field-input').val('');
                                    // $('#main_search_form input[name^="text"]').val('');
                                    $(sInput).val( item.text() );
                                    // $("input[name='text[0]").val(item.text());
                                    // $("input[name='logic[0]'] option[value='AND']").prop('selected', true);
                                    // $("select[name='theme[0]'] option[value='title']").prop('selected', true);

                                }

                                // $('.main-search__submit-btn-text').click();
                                // console.log('show must go on');
                                // $('#main_search_form').submit(); 
                                sInput.closest('form').submit();

                            });
                        }

                       /*if(showUri){
                            $(li).find('a').attr('href','/search/?q='+item.label);
                            $(li).find('a').attr('target','_parent');
                        }*/

                    });
                },
                select: function(e, ui) {},
                response: function(e, ui) {}
            });


/*            if(sInput.attr('id') === 'asearch'){
                //Перевод из поисковых подсказок сразу в соответствующую категорию
                sInput.attr('data-category', $($('.list-hover')).data('category'));
                sInput.attr('data-request', $('.list-hover').text());
                if($('.list-hover').data('category') !== undefined || $('.list-hover').data('request') !== undefined){
                    $('#theme-input-0').val($('.list-hover').data('category'));
                    $('#text-input-0').val($('.list-hover').data('request'));
                }

                //Отправляем на сервер данные о поисковой форме при выводе автокомплита
                //Подменяется sInput.data('src'), добавляются соответствующие параметры
                var trueSrc = sInput.data('src');
                sInput.keydown(function(){
                    var newSrc = '';
                    var row = $('.visiblerow');

                    newSrc += '&';
                    newSrc += $('.b_search_set input:checked').attr('name') + '=' + $('.b_search_set input:checked').val();

                    $('select', row).each(function(){
                        newSrc += '&';
                        newSrc += $(this).attr('name') + '=' + $(this).val();
                    });
                    $('input:checked', row).each(function(){
                        newSrc += '&';
                        newSrc += $(this).attr('name') + '=' + $(this).val();
                    });
                    $('input[type=text]', row).each(function(){
                        if($(this).val() !== ''){
                            newSrc += '&';
                            newSrc += $(this).attr('name') + '=' + $(this).val();
                        }
                    });

                    newSrc += '&';
                    newSrc += $('#js_searchdate_prev').attr('name') + '=' + $('#js_searchdate_prev').val();

                    newSrc += '&';
                    newSrc += $('#js_searchdate_next').attr('name') + '=' + $('#js_searchdate_next').val();

                    $('.b_search_row .fln input:checked').each(function(){
                        newSrc += '&';
                        newSrc += $(this).attr('name') + '=' + $(this).val();
                    });

                    sInput.data('src', (trueSrc+newSrc).toString());
                });
            }*/

            

            if(sInput.data('src')){
                var affectedURL = sInput.data('src');
                $('#main_search_form input:checked').each(function(){
                    affectedURL += '&' + $(this).attr('name') + '=' + $(this).attr('value')
                });
                if ( $('#main_search_form input.main-search__type-input[name="librarytype"]').length ) {
                    affectedURL += '&librarytype=' + $('#main_search_form input.main-search__type-input[name="librarytype"]').val();
                }
                sInput.catcomplete({
                    delay: 0,
                    minLength: 3,
                    source: function(request, response){  
                        var librarytype = sInput.data('library'),
                            afUrl = affectedURL;
                        if (librarytype){
                            afUrl += '&librarytype='+librarytype;
                        }
                        $('#collection option').each(function(index, value){
                            if (true == $(value).prop('selected'))
                                afUrl += '&coll[]=' + encodeURIComponent($(value).val());
                        });

                        $.ajax({
                            url: afUrl,
                            dataType: 'json',
                            data: {
                                term: sInput.val().toString()
                            },
                            success: function(data){
                                response(data);
                            }
                        });
                    },
                    autofocus: true
                });
            }
        });

    }
    //end of closure
})(jQuery);

//Кнопка очистки поиска
(function() { 
    $.fn.searchClear = function() { 
        $(this).each(function(){
            var searchInput = $(this);
            var clearBtn = searchInput.next();
            //При загрузке страницы
            if(searchInput.val().length > 0){
                $(clearBtn).show();
            }
            else {
                $(clearBtn).hide();
            }

            //При вводе данных
            searchInput.keyup(function(event) {
                if(searchInput.val().length > 0) {
                    $(clearBtn).show();
                }
                else {
                    $(clearBtn).hide();
                }
            });

            //Клик по кнопке очистки
            $(clearBtn).click(function(event){
                event.preventDefault();
                searchInput.val('').focus();
                $(this).hide();
            });
        });
    };
    //end of closure
})(jQuery);

(function() { //create closure
    $.fn.kolenkaAccordion = function() { // показывать скрытые блоки
        this.each(function() {
            $(this).children().children('a').on('click', function(e){
                e.preventDefault();
                var toggler = e.target,
                    target = $(toggler).next('ul');
                target.stop().slideToggle(function(){
                    $(toggler).toggleClass('active-link', $(target).is(':visible') );
                });
            });
        });
    };
    //end of closure
})(jQuery);


//Прокрутка фильтров в поиске /* УПРАЗДНЕНА 04.12.2015 */
(function() { 
    $.fn.searchFilterInit = function() {
        $(this).each(function(){
            var cont = $(this);
            //Список фильтров
            var filterList = cont.children('.main-search__exsearch-filter-list');
            //Кнопки пролистывания
            var next = cont.children('.main-search__exsearch-filter-slide-btn--next');
            var prev = cont.children('.main-search__exsearch-filter-slide-btn--prev');

            //Установки
            var translateCount = 0; // обнуляем счетчик
            var translateStep = 272.5; // указываем шаг перемотки
            var colItem = 6; //количество элементов в одном столбце
            var colVisible = 4; //Видимых столбцов на странице
            var items = filterList.children().length; // количество ссылок в списке

            //Кнопки листания в фильтре поиска

            var translateLim = (items / colItem)-colVisible; // количество возможных прокруток
            var translateLimCount = 0; //Счетчик прокруток

            next.click(function(event){
                event.preventDefault();
                if(translateLimCount<translateLim){
                    translateLimCount++;
                    translateCount = translateCount-translateStep;
                    filterList.css({'transform':'translateX('+translateCount+'px)'});
                }
            });

            prev.click(function(event){
                event.preventDefault();
                if(translateLimCount>0){
                    translateLimCount--;
                    translateCount = translateCount+translateStep;
                    filterList.css({'transform':'translateX('+translateCount+'px)'});
                }
            });

        });
    };
    //end of closure
})(jQuery);


(function() { //create closure
    $.fn.openHideBlock = function() { // показывать скрытые блоки

        this.each(function() {
            var lnk =$(this);
            if(!lnk.hasClass('sort')){
                lnk.on('click',function(e) {
                    e.preventDefault();
                    if($('.b-sidenav .close').length == 0)
                        $('<a href="#" class="close"></a>').prependTo($('.b-sidenav'));
                    $('.b-sidenav').fadeIn();
                    $('.b-sidenav .close').on('click', function(e) {
                        e.preventDefault();
                        $(this).parent().fadeOut();
                        $(this).remove();
                    });
                });
            }else {
                var lock = true;
                lnk.on('click',function(e) {
                    e.preventDefault();
                    if(lock){
                        $('.sort_wrap').css({'display':'block'});
                        if($('.sort_wrap div').length == 0){
                            $('.sort_wrap').find('a').wrapAll('<div></div>');
                        }
                        lock=false;
                    }else{
                        $('.sort_wrap').css({'display':'none'});
                        lock=true;
                    }
                });
            }
        });
    }
    //end of closure
})(jQuery);


(function(cash) { //create closure аккордеон справа
    $.fn.sideAccord = function(options) {
        this.each(function() {
            var cont = $(this), title = $('.b-sidenav_title', cont), searchEx = $('.b-sidenav_srch', cont);
            title.click(function(e){
                var bt = $(this), accblock = bt.next('.b-sidenav_cont');

                if('undefined' !== typeof accblock[0]){
                    e.preventDefault();
                    bt.toggleClass('open');
                    accblock.slideToggle();
                }else{
                }

            });
            searchEx.click(function(e){
                var par = $(this).closest('.b-sidenav_cont'), ul = $('.b-sidenav_cont_list', par);
                ul.slideToggle();
            });

        });
    };
    //end of closure
})(jQuery);

(function() {
    $.fn.opened = function(options) {
        this.each(function() {
            var a = $(this), par =a.closest('ul');
            a.click(function(e){
                e.preventDefault();
                if (5 > par.find('li.hidden:lt(5)').removeClass('hidden').size()){
                    a.parent().addClass('hidden');
                }
            });

        });
    }
    //end of closure
})(jQuery);


// сообщение об ошибке на странице книги
(function() {
    // открытие окна для ввода ссообщения
    $('.book-error-button').click(function (e) {
        e.preventDefault();

        var formHtml = '<form role="form" action="/rest_api/book/error/" method="post" class="book-error">'
            +'<p class="error-info" style="color:red;"></p>';

        if ($(this).attr('data-is-authorized') == '0'){
            formHtml += '<div class="form-group"><label > Email *'
                +'<input required class="form-control" type="email" name="user_email" placeholder="email@example.com" /></label></div>';
        }

        formHtml += '<p class="text-info">Введите информацию об ошибке:</p>'
            +'<input type="hidden" name="book_id" value="' + $(this).attr('data-book-id') + '" />'
            +'<textarea class="form-control" name="comment" rows="5"></textarea><br />'
            +'<button type="button" class="btn btn-primary bth-error-submit">Отправить</button> '
            +'<button type="button" class="btn btn-default bth-error-close">Отмена</button>'
            +'</form>';

        FRONT.infomodal({
            options: {
                buttonBar: false
            },
            title: 'Сообщить об ошибке',
            obj: $(formHtml),
            callback: function(modalContent, modal) {
                $('.bth-error-submit', $(modalContent)).on('click',function(e){
                    e.preventDefault();
                    $('.book-error').submit();
                });
                $('.bth-error-close', $(modalContent)).on('click',function(e){
                    e.preventDefault();
                    $('.modal-dialog .close').click();
                });
            }
        });
    });

    // отправка сообщения
    $(document).on('submit', '.book-error',function(e){
        e.preventDefault();
        $.ajax({
            url:  $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            cache: false,
            success: function(data) {
                $('.bth-error-close').click();
            },
            error:  function(xhr, str){
                $('.error-info').html(xhr.responseJSON.errors.message);
            }
        });
    });
})(jQuery);



$(function(){
    /* Закладки в поиске */
    var $autocompleteInput = $('#main_search_form input[name=q]'); // Кешируем объект
    $('.nav-tabs.secondary-tabs li').click(function(event){
        event.preventDefault();
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        $autocompleteInput.data( 'library', $(this).children('a').data('type') );
    });


    //Чекбоксы в фильтре поиска
    $(".b-sidenav input.checkbox").change(function() {
        var val = $(this).val();
        var strDopParam = $( "form#dop_filter" ).serialize();
        // BX.showWait($(this).closest('li').attr('id'));
        $.get( "/search/?q=%D0%BF%D1%83%D1%88%D0%BA%D0%B8%D0%BD&is_full_search=on&logic%5B0%5D=AND&theme%5B0%5D=ALL&text%5B0%5D=&isprotected%5B0%5D=0&publishyear_prev=900&publishyear_next=2015", strDopParam, function( data ) {
            $('#search_page_block').remove();
            $('section.mainsection.innerpage').after(data);
            // BX.closeWait();
        });
    });

    $('.js_moreopen').opened();
    $('.b-sidenav').sideAccord(); // аккордеон менюшки сайдбара

    /* Слайдер */
    if (typeof $.fn.slick == 'function') {
        $('.main-slider__wrapper').slick({
            lazyLoad: 'ondemand',
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            swipeToSlide: true,
            responsive: [
                {
                  breakpoint: 1200,
                  settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 700,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 408,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                }
            ]
        });
    }
    /* слайдер в коллекциях */
    $('[data-slide-editions]').each(function(){
        var realSlidesCount = $(this).find('.clsp__editions-list-item').length;
        var self = this;
        var slidesAmount = 3;
        var slidesMove = 3;
        var centermode = true;
        if ( $(self).closest('.collections-list-item__preview').find(".clsp__cover").length == 0 ) {
            slidesAmount = slidesAmount + 1;
            slidesMove = slidesMove +1;
            centermode = false;
        } else {
            slidesAmount = slidesAmount;
        }
        if (realSlidesCount > slidesAmount) {
            var next = $(this).siblings('.clsp__editions-list-next');
            var prev = $(this).siblings('.clsp__editions-list-prev');
            if (typeof $.fn.slick == 'function') {
                $(this).slick({
                    slidesToShow: slidesAmount,
                    slidesToScroll: slidesMove,
                    arrows: true,
                    prevArrow: prev,
                    nextArrow: next,
                    responsive: [
                        {
                          breakpoint: 1200,
                          settings: {
                            slidesToShow: slidesAmount,
                            slidesToScroll: slidesMove
                          }
                        },
                        {
                          breakpoint: 992,
                          settings: {
                            slidesToShow: slidesAmount-1,
                            slidesToScroll: slidesMove-1
                          }
                        }
                    ]
                });
            }
        }

    });


    /* меню в мобильной версии */
    $('.main-navigation__hamburger-btn').click(function(){
        $('.main-navigation__general').toggleClass('main-navigation__general--open');
    });

    /* Открываем/прячем расширенное меню */
    $('.main-search__exsearch-btn').click(function(event){
        event.preventDefault();
        $('.main-search__exsearch').slideToggle();
        $('.main-search__exsearch-trigger').toggleClass('main-search__exsearch-trigger--hide');
    });
    $('.main-search__exsearch-close').click(function(event){
        event.preventDefault();
        $('.main-search__exsearch').slideToggle();
        $('.ad-search-control').toggleClass('hidden');
        // $('.main-search__exsearch-trigger').toggleClass('main-search__exsearch-trigger--hide');
        // $('.main-search__exsearch-trigger').prev('.btn').toggle();
    });

    $(document).on('click', '.main-search__general-clean', function(e){
        e.preventDefault();
        $('.main-search__general [type="text"]').val('').trigger('keydown');
    });

    /* Слайдер расширенного поиска */
    var exsearchMin = 900,
        totoday = new Date(),
        exsearchMax = totoday.getFullYear(),
        exsearchFrom = $('.main-search__exsearch-date-from-input'),
        exsearchTo = $('.main-search__exsearch-date-to-input'),
        cont = $(".main-search__exsearch-date-range"),

        exsearchFromVal = (exsearchFrom.val()<1800) ? (1200+exsearchFrom.val()/3) : exsearchFrom.val(),
        exsearchToVal = (exsearchTo.val()<1800) ? (1200+exsearchTo.val()/3) : exsearchTo.val();

    if ($.fn.slider) {
        $(".main-search__exsearch-date-range").slider({
            range: true,
            min: 1500,
            max: exsearchMax,
            values: [ exsearchFromVal , exsearchToVal ],
            slide: function( event, ui ) {
                var from = (ui.values[0]<1800) ? (ui.values[0]*3-3600) : ui.values[0];
                var to = (ui.values[1]<1800) ? (ui.values[1]*3-3600) : ui.values[1];
                exsearchFrom.val(from);
                exsearchTo.val(to);
            }
        });
    }

    exsearchFrom.keyup(function(e){
        if(exsearchFrom.val()>=exsearchMin && exsearchFrom.val()<=exsearchMax) {
            var value = (exsearchFrom.val()<1800) ? (1200+(exsearchFrom.val()/3)) : exsearchFrom.val();
            cont.slider( "values", 0, value);

        }
    });

    exsearchTo.keyup(function(e){
        if(exsearchTo.val()>=exsearchMin && exsearchTo.val()<=exsearchMax) {
            var value = (exsearchTo.val()<1800) ? (1200+(exsearchTo.val()/3)) : exsearchTo.val();
            cont.slider( "values", 1, value);
        }
    });

    //Обработчик выбора коллекций в расширенном поиске
    var langCollection = $('.main-search__exsearch-collection-select').attr('langcollection');
    if ($.fn.selectpicker) {
        $('.main-search__exsearch-collection-select').selectpicker();
        $('.feedback-select').selectpicker();
    }

    if(!$('.main-search__exsearch-collection-select').val()){
        $('.main-search__exsearch-collection-select .filter-option').text(langCollection).attr('title', langCollection);
    }
    $('.main-search__exsearch-collection-select').change(function(){
        if(!$('.main-search__exsearch-collection-select').val()){
            $('.main-search__exsearch-collection-select .filter-option').text(langCollection).attr('title', langCollection);
        }
    });

    //Адаптивное меню в личном кабинете
    $('.lk-sidebar__menu-link--active').click(function(event){
        var winLocPath = window.location.pathname,
            link = $(this),
            linkHref = link.attr('href');
        if (~linkHref.indexOf(winLocPath)) {
            //event.preventDefault();
            link.css('cursor','default');
        }
    });

    //Вкладки фильтров в расширенном поиске
    $('.main-search__exsearch-filter-link').click(function(){
        $('.main-search__exsearch-filter-link').removeClass('active');
        $(this).addClass('active');
        var filter = $(this).data('filter');
        $('.main-search__exsearch-filter-wrap').hide();
        $('.'+filter+'').show();
    });
    $('.main-search__exsearch-filter-tabs li:first-child>a').click();

    //Показать рубрики в карточке издания поисковой выдачи
    $('.search-result__content-main-link-category').click(function(event){
        event.preventDefault();
        var link = $(this);
        link.toggleClass('search-result__content-main-link-category--open');
        link.next().toggleClass('search-result__content-main-category--show');
        if(link.hasClass('search-result__content-main-link-category--open')){
            link.text(link.attr('data-close'));
        }
        else {
            link.text(link.attr('data-open'));
        }
    });

    //Подключаем датапикеры
    var currentDate = new Date();
    if ($.fn.datepicker) {
        $( '.datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            maxDate: '+4y',
            // showOn: "button",
            // buttonImage: "/bitrix/images/icons/calendar.gif",
            // buttonImageOnly: true,
            // buttonText: "Выберите дату"
        });
    }
    if(!$('.datepicker-current').val()){
        $('.datepicker-current').val(getOptionalDate(currentDate));
    }
    if(!$('.datepicker-from').val()){
        $('.datepicker-from').val(getOptionalDate(new Date(currentDate.getUTCFullYear(), currentDate.getUTCMonth()-1, currentDate.getUTCDate())));
    }

    //Аккордеоны
    var accType = $('ul.uleditstat').data('type');
    if ( typeof $.dcAccordion == 'function') {
        $('ul.uleditstat').dcAccordion({
            // autoClose: true,
            classArrow: 'icon',
            classParent: 'active-parent',
            classActive  : 'active-link',    // Class of active parent link
            eventType: 'click',
            autoClose: false,
            saveState: true,
            disableLink: true,
            speed: 'fast',
            cookie: accType
        });
    } else {
        $('ul.uleditstat').kolenkaAccordion();
    }

    //Кнопка очистки поиска
    $('.main-search__general-field-input').searchClear();

    /** <REST_REQUESTS> */
    $('.rest-action.delete-row').activeDeleteRequest(function (data) {
        if (data.hasOwnProperty('deletedFunds') && data.deletedFunds > 0) {
            $(this).parent().prepend($(this).data('delete-msg'));
            $(this).remove();
        }
    });
    $('.rest-action.fund-delete').click(function (e) {
        e.preventDefault();
        var message = prompt($(this).data('prompt-msg'));
        $(this).sendRestRequest({
            success: function (data) {
                if (data.hasOwnProperty('deletedFunds') && data.deletedFunds > 0) {
                    $(this).parent().prepend($(this).data('delete-msg'));
                    $(this).remove();
                }
            },
            parameters: {
                reasonMsg: message
            }
        });
    });

    $('.rest-action.confirm').click(function (e) {
        e.preventDefault();
        var confirmMsg = $(this).data('confirm-msg');
        if (confirmMsg && confirm(confirmMsg)) {
            $(this).sendRestRequest({
                success: function (data) {
                    if (data.hasOwnProperty('success') && true === data.success) {
                        $(this).parent().text($(this).data('result-msg'));
                    }
                }
            });
        }
    });
    /** </REST_REQUESTS> */


    // Подключаем автокомплит
    $( '.main-search__general-field-input, [data-autocomplete]' ).autocompliteFunc();

    //Переключение вкладок поиска
    $('.main-search__label').click(function(){
        var val = $(this).data('type');

        $('.main-search__type-input').val(val);
        if($('[name="q"]').val()){
            $('#main_search_form').submit();
        }
    });


    $('.main-search__exsearch-trigger').click(function(event){
        event.preventDefault();
        // $(this).toggleClass('main-search__exsearch-trigger--hide');
        // $(this).prev('.btn').toggle();
        $('.ad-search-control').toggleClass('hidden');
        $('.main-search__exsearch').slideToggle();
    });

    //Костыль для радио-кнопок в поисковой форме
    $('#cb1').click(function(){
        $('#cb1').prop('checked',true).val('on');
        $('#cb38').prop('checked',false).val('');
    });
    $('#cb38').click(function(){
        $('#cb38').prop('checked',true).val('on');
        $('#cb1').prop('checked',false).val('');
    });

    // Кнопка "читать"
    $(document).on('click','.boockard-read-button',function (event) {
        event.preventDefault();
        $('.boockard-error-msg').hide();
        var serverError = $(this).data('server-error');
        readBook(event, this);
        setTimeout(function () {
            if ($('.boockard-read-button').data('load') != 'load') {
                $('.boockard-error-msg').text(serverError).show();
            }
        }, 5000);
    });

});


//Получаем значение даты в строковом формате для вывода в инпуты
function getOptionalDate(date){
    var day = date.getUTCDate().toString();
    if(day.length<2) day = '0' + day;
    var month = (date.getUTCMonth() + 1).toString();
    if(month.length<2) month = '0' + month;
    var year = date.getUTCFullYear();
    var resultDate = day + '.' + month + '.' + year;
    return resultDate;
}

// Проверка на существование в массиве элемента (аналог php функции)
function in_array(val, arr) {
    for (var i = 0, l = arr.length; i < l; i++)
        if (arr[i] == val)
            return true;

    return false;
}

/**
 * Return user token
 * @returns {string}
 */
function getUserToken() {
    return $('meta[name=user-token]').attr('content');
}

/**
 * Проверяет строку на валидный JSON
 * @param str
 * @returns {boolean}
 */
function isJSON(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

/**
 *
 * @param {string} path
 * @param {{}} object
 * @returns {*}
 */
var getPropertyByPath = function (path, object) {
    path = path.split('.');
    for (var i = 0; i < path.length; i++) {
        if (object.hasOwnProperty(path[i])) {
            object = object[path[i]];
        } else {
            object = null;
            break;
        }
    }
    return object;
};

// переключалка версии для слепых
function setBlind(){

    if(!$.cookie('blind_version')){
        $.cookie('blind_version', '1', { expires: 30, path: '/' });
    }else{
        $.cookie('blind_version', null, { expires: 30, path: '/' });
    }

    window.location.reload();

    return false;
}

/* допполя расширенного поиска */
/* слизано со страницы некоего валидатора
http://formvalidation.io/examples/adding-dynamic-field/ */
(function(){
    $.fn.searchAdFields = function(options) {
        this.each(function(){

            $('.main-search__additional-fields:not(.ms-ad-template)')
                .each(function(){
                    $(this).find('.add-next-row').toggleClass('hidden',true);
                })
                .last().find('.add-next-row').toggleClass('hidden',false);

            var $form = $(this),
                $addInputs = $('.main-search__additional-fields:not(.ms-ad-template) input[type="text"]', $form),
                inputIndex = $addInputs.length - 1,
                $template = $('.ms-ad-template');
                $placehodler = $('#additional-placeholder');

            $addInputs.each( function(index,value){/*максимальный индекс*/
                var name = $(this).attr('name');
                var sub = parseInt( name.substr(5), 10);
                if ( !isNaN(sub) ) {
                    inputIndex = Math.max( inputIndex, sub );
                }
            });

            $form
                .on('click', '.add-next-row', function(){
                    inputIndex++;
                    var $clone = $template.clone()
                                    .removeClass('ms-ad-template')
                                    .insertBefore($placehodler),
                        textInput = $clone.find('[name="text"]'),
                        textInputEl = textInput.get(0);
                    $clone
                        .find('[name="logic"]').attr('name', 'logic[' + inputIndex + ']').selectpicker().end()
                        .find('[name="theme"]').attr('name', 'theme[' + inputIndex + ']').selectpicker().end()
                        .find('[name="text"]').attr('name', 'text[' + inputIndex + ']');
                    $(this).toggleClass('hidden',true);
                    textInput.autocompliteFunc({submit:true});
                    if (typeof VKI_attach == 'function') {
                        VKI_attach( textInputEl );
                    }
                })
                .on('click', '.del-this-row', function(){
                    var $row = $(this).closest('.main-search__additional-fields');
                    $row.remove();
                });
        });
    }
})(jQuery);
$('.main-search__form').searchAdFields();

if (!String.prototype.trim) {
  (function() {
    String.prototype.trim = function() {
      return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
    };
  })();
}

if (!Function.prototype.bind) {
  Function.prototype.bind = function(oThis) {
    if (typeof this !== 'function') {
      // ближайший аналог внутренней функции
      // IsCallable в ECMAScript 5
      throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
    }

    var aArgs = Array.prototype.slice.call(arguments, 1),
        fToBind = this,
        fNOP    = function() {},
        fBound  = function() {
          return fToBind.apply(this instanceof fNOP && oThis
                 ? this
                 : oThis,
                 aArgs.concat(Array.prototype.slice.call(arguments)));
        };

    fNOP.prototype = this.prototype;
    fBound.prototype = new fNOP();

    return fBound;
  };
}

/* кандидат на впихивание всего из глобальной области видимости */
var FRONT = FRONT || {};
FRONT.utils = FRONT.utils || {};
FRONT.utils.toggleWidget = function(toggler,e){
    var toggler = $(toggler),
        data = toggler.data(),
        widget = toggler.closest('[data-togglable-widget]');
    widget.toggleClass('expanded');
    //console.log('dismiss extended');
};
/**
 *
 * @param url
 * @param parameter
 * @returns {*}
 * @see http://stackoverflow.com/questions/1634748/how-can-i-delete-a-query-string-parameter-in-javascript
 */
FRONT.utils.removeQueryParameter = function(url, parameter) {
    var urlparts= url.split('?');
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        for (var i= pars.length; i-- > 0;) {
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        return url;
    } else {
        return url;
    }
};
FRONT.utils.addUrlTokem = function (url) {
    return url + (-1 === url.indexOf('?') ? '?' : '&') + 'token=' + FRONT.getUserToken()
};
FRONT.getUserToken = function() {
    return $('meta[name=user-token]').attr('content');
}
FRONT.getAutoCompleteSource = function() {
    return $('meta[name=search-auto-complete-source]').attr('content');
}
FRONT.getSessId = function() {
    return $('meta[name=bitrix-sessid]').attr('content');
}

FRONT.utils.stringCheckboxes = function(widget){
    var widget = $(widget),
        moreButton = $('[data-more-button]',widget),
        items = $('[data-tcs-item]',widget),
        titleEl = $('.tcs-title',widget),
        otherContainer = $('[data-other-container]', widget),
        otherItemsWidget = $('[data-other-widget]', widget),
        formControl = $('select', widget),
        controller = this;

    items.on('click',function(e){
        e.preventDefault();
        controller.markSelected( $(this) );
    });
    items.on('click.checkwidget',function(e){
        if ( $(this).parent().hasClass('dropdown-menu') ) {
            e.stopPropagation(); // avoid close menu
        } else {
            $(this).off('click.checkwidget'); // close menu when link is outside of it
        }
    });

    var itemsWidth = function(){
        var sum = 0;
        widget.children('[data-tcs-item]').each(function(){
            sum += $(this).outerWidth(); //plus space
        });
        return sum
    }

    this.markSelected = function(item){
        if (item) {
            var dataVal = $(item).data('value');
            formControl.find('option').each(function(){
                var option = $(this),
                    val = option.val(),
                    isSelected;

                if (val == dataVal) {
                    isSelected = option.prop('selected');
                    option.prop('selected', !isSelected);
                    item.attr('data-selected', !isSelected);
                }
            });
        } else {
            formControl.find('option').each(function(){
                var val = $(this).val();
                items.filter('[data-value="'+ val +'"]').attr('data-selected', $(this).prop('selected') );
            });
        }
        var flag = false;
        formControl.find('option').each(function(){
            var option = $(this),
                val = option.val(),
                mirrorItem = items.filter('[data-value="'+ val +'"]');
            if ( $(this).prop('selected') && mirrorItem.parent().get(0).hasAttribute('data-other-container') ) { 
                flag = true 
            }
        });
        moreButton.attr('data-selected',flag);
    }

    this.init = function(){
        // console.log('closure init');
        core();
        controller.markSelected();
        widget.addClass('inited');

        function core(){
            var wWidth = widget.width();
            // console.log('core executed');
            items.appendTo(otherContainer);

            items.each(function(index,item){
                $(this).insertBefore(otherItemsWidget);
                var contentWidth = titleEl.outerWidth() + itemsWidth() + ((widget.children('.tcs-item').length+1)*10) + moreButton.outerWidth();
                
                if ( wWidth < contentWidth ) {
                    $(this).prependTo(otherContainer);
                    return false
                } else {
                    // console.log( $(this).text() )
                }
            });
        }
        
        return function(){
            // console.log('closure execute');
            core();
        }
    }()

    return {
        refresh: function(){
            controller.init();
        }
    }
}
FRONT.utils.searchByAuthor = function(){
    // FRONT.autocomplete()
}


FRONT.spinner = {};
FRONT.spinner.opts = {
      lines: 17 // The number of lines to draw
    , length: 18 // The length of each line
    , width: 7 // The line thickness
    , radius: 19 // The radius of the inner circle
    , scale: 0.55 // Scales overall size of the spinner
    , corners: 0 // Corner roundness (0..1)
    , color: '#00708c' // #rgb or #rrggbb or array of colors
    , opacity: 0 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 99 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 12 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '50%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'absolute' // Element positioning
};
FRONT.spinner.btnOpts = {
      lines: 9 // The number of lines to draw
    , length: 3 // The length of each line
    , width: 2 // The line thickness
    , radius: 3 // The radius of the inner circle
    , scale: 1 // Scales overall size of the spinner
    , corners: 1 // Corner roundness (0..1)
    , color: '#fff' // #rgb or #rrggbb or array of colors
    , opacity: 0.25 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 60 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '-.7ex' // Top position relative to parent
    , left: '0' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'relative' // Element positioning
};
FRONT.spinner.mini = {
      lines: 7 // The number of lines to draw
    , length: 2 // The length of each line
    , width: 1 // The line thickness
    , radius: 1 // The radius of the inner circle
    , scale: 1 // Scales overall size of the spinner
    , corners: 1 // Corner roundness (0..1)
    , color: '#fff' // #rgb or #rrggbb or array of colors
    , opacity: 0.25 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1.6 // Rounds per second
    , trail: 60 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '-.7ex' // Top position relative to parent
    , left: '0' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'relative' // Element positioning
};

FRONT.spinner.graphOpts = {
    lines: 17 // The number of lines to draw
    , length: 30 // The length of each line
    , width: 7 // The line thickness
    , radius: 50 // The radius of the inner circle
    , scale: 0.55 // Scales overall size of the spinner
    , corners: 0 // Corner roundness (0..1)
    , color: '#00708c' // #rgb or #rrggbb or array of colors
    , opacity: 0 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 99 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 12 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '45%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'relative' // Element positioning
};
FRONT.spinner.execute = function(target,options){
    var opts = {};
    $.extend(opts,this.opts);
    if (options) {
        $.extend(opts,options);
    }
    // var options = options || this.opts;
    return new Spinner(opts).spin(target);
};
FRONT.init = function(){
    $(function(){
        $('[data-select-picker]').each(function(){
            var select = $(this),
                cty = $('option',select).length;
            if (cty > 10) {
                select.selectpicker({liveSearch: true,size:10});
            } else {
                select.selectpicker();
            }
        });
        // console.log('wtf');
        if (typeof $.fn.select2 == 'function') {
            $('[data-select-two][multiple]').select2();
        }

        // var stringCheckboxes = FRONT.utils.stringCheckboxes('[data-string-checkbox-widget]');
        $('[data-string-checkbox-widget]').each(function(){
            var el = $(this),
                widget = new FRONT.utils.stringCheckboxes(el);
            $(window).on('resize',function(){
                widget.refresh();
            })
        });
    });
}();
// FRONT.initObj = ({
//     init: $(function(){
//         // $('[data-select-two]').select2();
//     })
// }.init());
FRONT.formutils = {}
FRONT.formutils.addTagWidget = (function(tagList){
    var form = $('<form/>').attr('action','/local/tools/collections/add.php'),
        inputGroup = $('<div/>').addClass('input-group'),
        inputText = $('<input/>').addClass('form-control').attr({
            type: 'text',
            placeholder: 'Добавить подборку',
            maxlength: 200
        }),
        submitWrapper = $('<span>').addClass('input-group-btn'),
        submitButton = $('<button>').addClass('btn').addClass('btn-default').text('+'),

        submitButtonMarkup = $(submitWrapper).append(submitButton),
        widgetControl = $(form).append( $(inputGroup).append(inputText).append(submitButtonMarkup) );
    // widgetControl = $(form).append(inputGroup).append(inputText).append(submitButtonMarkup);
    $(form).on('submit', function(e){
        e.preventDefault();
        var newCollectionName = $(inputText).val();
        if (newCollectionName == '') {
            return true
        }
        $.ajax({
            method: 'GET',
            url: $(form).attr('action'),
            data: { name: newCollectionName },
            dataType: 'json',
            beforeSend: function(){
                $(inputText).attr('disabled','disabled');
                $(submitButton).attr('disabled','disabled');
            },
            success: function(data){
                $(inputText).val('');
                //добавление пункта в выпадающие списки
                var itemLi = $('<li>').addClass('tag-list__item'),
                    itemLabel = $('<label>'),
                    itemCheckBox = $('<input>').attr('type','checkbox').attr('name','collections[]').attr('value', data.ID).attr('data-toggle-tag',''),
                    itemTitle = $('<span>').addClass('lbl').text(newCollectionName);
                itemLabel = itemLabel.append(itemCheckBox).append(itemTitle);
                itemLi = itemLi.append(itemLabel);
                $(document).find('[data-tag-list]').append(itemLi);

                FRONT.reloadSideTags();

            }
        }).always(function(){
            $(inputText).removeAttr('disabled');
            $(submitButton).removeAttr('disabled');
        });

    });
    return widgetControl;
});
FRONT.reloadSideTags = (function(){
    $.ajax({
        url: window.location.href,// + '?reloadRightCollection=Y', ///profile/note/ выдаёт html страницы целиком если вотнуть через гет
        method: 'GET',
        data: {
            reloadRightCollection : 'Y'
        },
        success: function(html){
            $('[data-tag-side-widget]').html(html).trigger('refreshed');
        }
    });
});
FRONT.autocomplete = function (items, options) {
    //console.log('FRONT.autocomplete');
    items.each(function () {
        applyAutoComplete($(this));
    });

    options = options || {};
    if ('function' !== typeof  options.success) {
        options.success = function (data, entityField, queries, entityList) {
            if (data.entities) {
                for (var key in data.entities) {
                    if (data.entities[key][entityField]) {
                        queries.push(data.entities[key][entityField]);
                        entityList[data.entities[key][entityField]] = data.entities[key].id;
                    }
                }
            }
        }
    }

    function applyAutoComplete(input) {
        var sourceUrl = input.data('src'),
            entityField = input.data('entity-field'),
            entityList = {}
            ;
        input.autocomplete({
            serviceUrl: sourceUrl,
            delay: 500,
            minLength: 1,
            source: function (request, response) {
                $.ajax({
                    url: sourceUrl,
                    dataType: 'json',
                    data: {
                        query: input.val().toString(),
                        entityField: entityField
                    },
                    success: function (data) {
                        var queries = [];
                        entityList = {};
                        options.success(data, entityField, queries, entityList);
                        response(queries);
                    }
                });
            },
            select: function (e, object) {
                var code = object.item.value;
                if (entityList[code]) {
                    input.parent().find('input[name="parent"]').val(entityList[code]);
                }
            },
            autofocus: true
        })
    }

};
FRONT.infomodal = function(message) {
    var message = message || {},
        options = message.options || {
            buttonBar: true
        },
        btnPrimaryTitle = message.button || "Ок";
    var btnGroup = $('<div/>').addClass('btn-bar'),
        btnAccept = $('<button/>').addClass('btn').addClass('btn-primary').attr('data-dismiss','modal').text(btnPrimaryTitle);
    if (options.buttonBar) {
        btnGroup.append(btnAccept);
    }
    // if (!message.title) {message.title = 'Внимание';}
    if (!message.textContent) {
        message.obj = $(message.obj) || 'Ошибка в объекте';
    }


    var modalMarkup = $('#universal-modal').clone(true).insertAfter('#universal-modal');
        modalMarkup.removeAttr('id');

    $(modalMarkup)
    .one('show.bs.modal', function(){
        var modal = $(this),
            title = message["title"],
            modalContent;
        if (message.textContent) {
            // modalContent = '<p>'+message.textContent+'</p>';
            modalContent = $('<p/>').html(message.textContent);
            modal.find('.modal-dialog').addClass('modal-sm');
        } else {
            modalContent = message.obj;
        }
        modalContent = modal.find('.modal-body').append(modalContent);

        modal.find('.modal-title').text(title).end()
            // .find('.modal-body').append(modalContent).end()
            .find('.modal-body').append(btnGroup).end()
            .find('.modal-footer').hide();
        if (message.callback) {
            message.callback(modalContent,modal,message.context);
        }
    })
    .one('hidden.bs.modal', function(e){
        $(this).detach();
    })
    .off('keydown')
    .on('keydown', function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            $(btnAccept).click();
        }
        if(event.keyCode == 27) {
            event.preventDefault();
            $(btnAccept).click();
        }
    })
    .modal('show');
}
FRONT.confirm = {}; //  конфирм с бутстрабовским модальным окном
FRONT.confirm.handle = function (message,toggler) {
    return this.modalDialog(message,toggler)
};
FRONT.confirm.modalDialog = function (message,toggler) {
    var dfd = new jQuery.Deferred();
    var result = false,
        message = message || {},
        toggler = toggler || {},
        modalSizeClass = message.modalSize || 'modal-sm',
        btnGroup = $('<div/>').addClass('btn-bar'),
        btnPrimaryTitle = message.confirmTitle || $(toggler).data('button-title') || 'Да',
        btnDefaultText = message.cancelTitle || $(toggler).data('cancel-title') || 'Отмена',
        btnAccept = $('<button/>').addClass('btn').addClass('btn-primary').attr('data-dismiss','modal').text(btnPrimaryTitle),
        btnDefault = $('<button/>').addClass('btn').addClass('btn-default').attr('data-dismiss','modal').text(btnDefaultText);

    btnGroup.append(btnAccept).append(btnDefault);
    $(btnAccept).on('click',function(){
        result = true;
    });

    var modalMarkup = $('#universal-modal').clone(true).insertAfter('#universal-modal');            
        modalMarkup.removeAttr('id');

    $(modalMarkup)
    .one('show.bs.modal', function(e){
        var modal = $(this),
            button = $(e.relatedTarget),
            title = message.title || 'Внимание',
            caption;
        if (message.text) {
            caption = '<p>'+message.text+'</p>';
        } else {
            caption = message.obj;
        }

        modal.find('.modal-dialog').addClass(modalSizeClass).end()
            .find('.modal-title').text(title);

        if (message.text || message.obj || message.callback) {
            modal.find('.modal-body').html(caption);
            modal.find('.modal-footer').html(btnGroup);
        } else {
            modal.find('.modal-body').append(btnGroup);
            modal.find('.modal-footer').hide();
        }

        if (message.callback) {
            message.callback(modal,message.context);
        }
    })
    .one('hidden.bs.modal', function(e){
        $(this).detach();
        dfd.resolve(result);
    })
    .off('keydown')
    .on('keydown', function(event){
        if(event.keyCode == 13) {
            event.preventDefault();            
            $(btnAccept).click();
        }
        if(event.keyCode == 27) {
            event.preventDefault();            
            $(btnDefault).click();
        }
    })
    .modal('show', toggler);
    
    return dfd.promise();
};
FRONT.assets = FRONT.assets || {};
FRONT.assets.parseURLGetQuery = function(loc) {
    var data = {},
        loc = loc || {};
    if(loc.search) {
        var pair = (loc.search.substr(1)).split('&');
        for(var i = 0; i < pair.length; i ++) {
            var param = pair[i].split('=');
            data[param[0]] = param[1];
        }
    }
    return data;
};
FRONT.assets.bitrixCheckboxReverser = function(e, toggler){ 
    var toggler = $(toggler),
        toggled = e.target,
        checkboxTarget = $('[name="'+ toggler.data('checkbox-for') +'"]'),
        checked = toggler.prop('checked'),
        bitrixVal = checked == true ? '1' : '0';
    checkboxTarget
        .val( bitrixVal )
        .attr('value', bitrixVal);
};
FRONT.assets.addBookToCollection = function(e, toggler, opts){
    e.preventDefault();
    var toggler = $(toggler),
        toggled = e.target,
        collectionId = toggler.data('addBookToCollection');



    // console.log(collectionId);


    var modalMarkup = $('#universal-modal').clone(true).insertAfter('#universal-modal');
    modalMarkup.removeAttr('id');

    $(modalMarkup)
    .one('show.bs.modal', function(e){
        var modal = $(this),
            button = $(e.relatedTarget),
            iframeSrc = button.attr('href'),
            iframe = $('<iframe/>').attr('src',iframeSrc).css({
                'height':'100%',
                'width':'100%',
                'border':'none',
                // 'overflow':'hidden'
            });
        // console.log(  );

        modal
            .find('.modal-title').text('Добавить книгу')
            .end().find('.modal-dialog').addClass('fixed-modal modal-lg no-footer-bar')
            .end().find('.modal-footer').remove()
            .end().find('.modal-body').css('overflow','hidden').append(iframe);
    })
    .one('hidden.bs.modal', function(e){
        var modal = $(this);
        $(modalMarkup).detach();
    })
    .off('keydown')
    .on('keydown', function(event){
        if(event.keyCode == 13) {// enter on modal
            // event.preventDefault();            
            // $(btnAccept).click();
        }
        if(event.keyCode == 27) {//escape on modal
            // event.preventDefault();            
            // $(btnDefault).click();
        }
    })
    .modal('show', toggler);


};
FRONT.events = (function(){
  var topics = {};

  return {
    subscribe: function(topic, listener) {
      // создаем объект topic, если еще не создан
      if(!topics[topic]) topics[topic] = { queue: [] };

      // добавляем listener в очередь
      var index = topics[topic].queue.push(listener) -1;

      // предоставляем возможность удаления темы
      return {
        remove: function() {
          delete topics[topic].queue[index];
        }
      };
    },
    publish: function(topic, info) {
      // если темы не существует или нет подписчиков, не делаем ничего
      if(!topics[topic] || !topics[topic].queue.length) return;

      // проходим по очереди и вызываем подписки
      var items = topics[topic].queue;
      items.forEach(function(item) {
          item(info || {});
      });
    }
  };
})();

/* удаление сущности страницы */
$(function(){
    $(document).on('click','[data-page-item-delete]',function(e){
        var toggler = $(e.target),
            pageItem = toggler.closest('[data-page-item]'),
            pageItemId = pageItem.data('page-item'),
            message = {
                title: toggler.data('modal-title') || toggler.attr('title') || undefined,
                text: toggler.data('confirm-text') || undefined,
                confirmTitle: toggler.data('button-title') || "Удалить"
            };
        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed){
                $.ajax({
                    url: window.location.href,
                    data: {
                        action: 'remove',
                        id: pageItemId
                    },
                    method: 'GET',
                    beforeSend: function(){
                        pageItem.addClass('deletion-progress');
                    },
                    success: function(data){
                        $(pageItem).slideUp(function(){
                            this.remove();
                        });
                    }
                });
            }
        });
    });
});

//выпадашка подблорок издания
$(function() {
    //checkIsAvailablePdf();

    $(document).on('click','[data-list-toggler]', function(e){
        e.preventDefault();
        
        var toggler = $(this),
            wrapper = $(toggler).closest('[data-list-widget]').find('[data-list-wrapper]'),
            editionId = toggler.data('edition-id'),
            constURL = toggler.data('list-src'),
            wrapperFilled = wrapper.hasClass('filled'),
            tagList;

        $(wrapper).attr('data-list-src',constURL);
            
        if ( !toggler.hasClass('opened') ) {            
            $.ajax({
                url: toggler.data('list-src'),
                method: 'GET',
                success: function(html){
                    wrapper.html('');
                    wrapper.find('[data-list-wrapper] input, [data-list-wrapper] button').remove(); // удаляем элементы со слушалками событий
                    wrapper.find('[data-tag-list]').remove();
                    wrapper.prepend(html);
                    tagList = $(wrapper).find('[data-tag-list]');
                    $(tagList).attr('data-tag-list-edition',editionId);

                    var addTag = FRONT.formutils.addTagWidget(tagList);
                    $(wrapper).append(addTag);

                    wrapper.toggleClass('filled',true);

                }
            });
        }

        toggler.toggleClass('opened');
        $(document).off('click.mytagouter'+editionId).on('click.mytagouter'+editionId,function(e){//когда дропдаун открыт...
            var clickTarget = $(e.target),
                allChilds = $(wrapper).find('*');
            if ( $(e.target).is(toggler) ) {//...клик по кнопке его закрывает
                $(document).off('click.mytagouter'+editionId);
                toggler.toggleClass('opened',false);
            }
            else if ( $(e.target).is(wrapper) || $(e.target).is(allChilds) ) {/*клик по выпадашке*/
            }            
            else {// ...кликаутсайд его закрывает
                $(document).off('click.mytagouter'+editionId);
                toggler.toggleClass('opened',false);
            }
        });
    });

    $(document).on('click', '[data-tag-list-edition] [data-toggle-tag]', function(e){        
        e.preventDefault();
        var checkBox = $(this),
            checkBoxChangeToChecked = $(checkBox).prop('checked'),
            tagList = $(checkBox).closest('[data-tag-list]'),
            editionId = $(tagList).attr('data-tag-list-edition'),
            tagListWrapper = $(tagList).closest('[data-list-wrapper]'),
            tagId = $(this).val(),
            getData = {
                idCollection: tagId,
                // t: 'books',
                // id: editionId
            },
            URL = $(tagListWrapper).attr('data-list-src'); //'/local/tools/collections/list.php'; //             

        if (checkBoxChangeToChecked) {
            getData.action = 'checked'
        }

        if (tagId && editionId) {
            $.ajax({
                url: URL,
                data: getData,
                method: 'POST',
                success: function(data){
                    if (data == '') {// при успехе возвращается пустая строка
                        $(checkBox).prop('checked',checkBoxChangeToChecked);

                        FRONT.reloadSideTags();

                    }
                }
            });
        }            
    });

    $(document).on('click', '[data-add-to-favorites-widget] [data-add-collection-to-favorites]', function(e){
        e.preventDefault();
        if(!confirm('Все издания выставки будут добавлены в Вашу библиотеку, доступную в личном кабинете')) return false;
        var toggler = $(this),
            widgetWrapper = toggler.closest('[data-add-to-favorites-widget]'),
            removeTogglerText = $(widgetWrapper).data('remove-toggler-text') || 'Удалить',
            sectionId = widgetWrapper.data('section-id'),
            url = widgetWrapper.data('add-href');
        $.ajax({
            method: 'GET',
            url: url,
            success: function(data){
                toggler
                    .removeAttr('data-add-collection-to-favorites')
                    .removeAttr('href')
                    .attr('data-remove-collection-from-favorites','')
                    .text( removeTogglerText );
            },
            fail: function(){
            }
        });
    });
    
    $(document).on('click', '[data-add-to-favorites-widget] [data-remove-collection-from-favorites]', function(e){
        e.preventDefault();
        if(!confirm('Удалить издания выставки из Вашей личной библиотеки?')) return false;
        var toggler = $(this),
            widgetWrapper = toggler.closest('[data-add-to-favorites-widget]'),
            addTogglerText = $(widgetWrapper).data('add-toggler-text') || 'Сохранить',
            url = widgetWrapper.data('remove-href');
        $.ajax({
            method: 'GET',
            url: url,
            success: function(data){
                toggler
                    .removeAttr('data-remove-collection-from-favorites','')
                    .text( addTogglerText )
                    .attr('data-add-collection-to-favorites','');
            },
            fail: function(){
            }
        });
    });

});

/* Yandex.Metrika goals */
$(function() {
    $(document).on('submit','.three-simple-search', function(e){
        yaCounter27774831.reachGoal('simpleSearch');
    });
    $(document).on('submit','.three-middle-search', function(e){
        yaCounter27774831.reachGoal('doubleSearch');
    });
    $(document).on('submit','.searches-redesign-extended', function(e){
        yaCounter27774831.reachGoal('extendedSearch');
    });
    $(document).on('click', '[class*="-read-btn"], [class*="-read-button"]', function(e){
        yaCounter27774831.reachGoal('readBook');
    });
    $(document).on('click', '.categories-block a', function(e){
        yaCounter27774831.reachGoal('categoriesBlock');
    });
});

/* Добавление и удаление тегов (фильтров поиска) */
tagControl = {
    displayedTags: [{}],
    tagsLayout: {},
    controlTags: [{}],
    extForm: {},
    hiddenInputs: [{}]
}
tagControl.markExist = function(inputs) {
    var main = this,
        clearEl = $('<div>')
            .addClass('filter-tags__item')
            .attr('data-tag-clear', '')
            .addClass('filter-tags__item-clear')
            .text('Снять все фильтры');

    if (inputs.length || $(tagControl.tagsLayout).find('.filter-tags__item').length ) {
        $(tagControl.tagsLayout).prepend(clearEl);
    }
    $(inputs).each(function(){
        var targetDataName = $(this).attr('name'),
            targetDataValue = $(this).val();
        $('[data-tag-name="'+targetDataName+'"][data-tag-value="'+targetDataValue+'"]').each(function(){
            $(this).toggleClass('tag-added', true);
            if ( $(this).hasClass('b-sidenav_title') ) {
                $(this).closest('.b-sidenav_cont').prev().click();                
            } else if ( $(this).find('.b-sidenav_value').length ) {                
                $(this).parent().parent().parent().parent().prev(':not(.open)').click();
                $(this).parent().parent().prev(':not(.open)').click();
            }
        });
    });
};
tagControl.tagToggle = function(obj,canSubmit){
    var tagItem, 
        hiddenInput, 
        canSubmitFlag = canSubmit || false;
    if (obj) {
        tagItem = $('<div>')
            .addClass('filter-tags__item')
            .attr('data-tag-name', obj.attrName)
            .attr('data-tag-value', obj.attrValue)
            .text(obj.attrText)
            .append('<a class="filter-tags__remove-link" href="#"><img src="/local/templates/adaptive/img/filter-close.png" alt="Убрать фильтр"></a>');
        hiddenInput = $('<input>').prop('type','hidden').attr('name', obj.attrName).attr('data-tag-store','').val(obj.attrValue);
    }

    // чтобы не терялся параметр full_query_search при выборе фильтра
    var arrayVar = (window.location.search.substr(1)).split('&');
    var valueAndKey = [];
    for (i = 0; i < arrayVar.length; i ++) {
        valueAndKey = arrayVar[i].split('=');
        if (valueAndKey[0] == 'full_query_search' && valueAndKey[1] !== null && valueAndKey[1] !== 0) {
            $(this.extForm).append(
                $('<input>').prop('type', 'hidden').attr('name', 'full_query_search').val(valueAndKey[1])
            );
        }
    }

    if (!obj.attrName && !obj.attrValue) {
        if ( $(obj.el).data('checkbox-id') ) {
            $( '#'+ $(obj.el).data('checkbox-id') ).prop('checked',false);
            $(obj.el).closest('.filter-tags__item').remove();
            // canSubmitFlag = true;
            // this.sendForm(true, "if (!obj.attrName && !obj.attrValue) => if ( $(obj.el).data('checkbox-id') )");
        } 
        else if ( $(obj.el).data('collection-name') ) {
            $('#search-collections').find('option[value="' + $(obj.el).data('collection-name') + '"]').prop('selected',false);
            $('#collection').find('option[value="' + $(obj.el).data('collection-name') + '"]').prop('selected',false);            
            $(obj.el).closest('.filter-tags__item').remove();
            // canSubmitFlag = true;
            // this.sendForm(true, "if (!obj.attrName && !obj.attrValue) => else if ( $(obj.el).data('collection-name') )");
        }
        else if ( $(obj.el).data('range') ) {
            $( '#'+ $(obj.el).data('range') ).val( $(obj.el).data('range-init') );
            $(obj.el).closest('.filter-tags__item').remove();
            // canSubmitFlag = true;
            // this.sendForm(true, "if (!obj.attrName && !obj.attrValue) => else if ( $(obj.el).data('range') )");
        }
    }
    else {
        obj.attrValue = obj.attrValue.replace(/"/g, "&quot;");

        if ( $('[name="'+obj.attrName+'"][value="'+obj.attrValue+'"]', this.extForm).length ) {
            // remove tag
            $('[data-tag-add-submit][data-tag-name="'+obj.attrName+'"][data-tag-value="'+obj.attrValue+'"]').toggleClass('tag-added', false);
            $(this.tagsLayout).find('.filter-tags__item').each(function(){
                if ( $(this).data('tagName') == obj.attrName && $(this).data('tagValue') == obj.attrValue ) {
                    $(this).remove();
                }
            });
            $(this.extForm).find('[name="'+obj.attrName+'"][value="'+obj.attrValue+'"]').remove().end();
            // canSubmitFlag = true;
            // this.sendForm(true, " else if ( $('[name=\"'+obj.attrName+'\"][value=\"'+obj.attrValue+'\"]', this.extForm).length ) ");
        } else {
            // add tag
            $('[data-tag-add-submit][data-tag-name="'+obj.attrName+'"][data-tag-value="'+obj.attrValue+'"]').toggleClass('tag-added', true);
            $(this.tagsLayout).append(tagItem);
            $(this.extForm).append(hiddenInput);
            // canSubmitFlag = true;
            // this.sendForm(true, "else");
        }
    }
    if (canSubmitFlag) {
        this.sendForm(true, "canSubmitFlag");
    }
};
tagControl.sendForm = function(submit,from) {
    //return false;
    // console.log('from ' + from);
    var el;
    if (submit) {
        tagControl.extForm.submit();
        if ($('#milk-shadow').length == 0) {
            el = $('<div/>').attr('id','milk-shadow');
            $('body').append(el);
        }
        $('#milk-shadow').toggleClass('draw', true);
        FRONT.spinner.execute( $('#milk-shadow')[0], {scale:.95, className: 'spinner-neb-logo', nebLogo: true} );
        $('body').toggleClass('tag-change', true);
        $('.BackToTop').remove();
    } else {
        //console.dir( tagControl.extForm.serializeArray() );
    }
}
tagControl.init = function() {
    this.displayedTags = $('.filter-tags__item');
    this.controlTags = $('[data-tag-add-submit]');
    // this.extForm = $('.main-search__form');
    this.extForm = $('[data-three-form]').first();
    this.hiddenInputs = $('[type="hidden"]:not([class])',this.extForm);
    this.tagsLayout = $('.filter-tags');

    this.markExist(this.hiddenInputs);

    $(this.extForm).on('tagtoggle', function(e,obj){
        var canSubmit = false;
        if (obj.operation == 'toggle') {
            canSubmit = true;
        }
        tagControl.tagToggle(obj, canSubmit);
    });

    $(document).on('click', '[data-tag-add-submit]', function(e) {
        e.preventDefault();
        var obj = {
                attrName: $(this).data('tag-name'),
                attrValue: $(this).data('tag-value'),
                attrText: $(this).data('tag-text'),
                el: e.target,
                operation: 'toggle'
            };
        if (obj.name != '') {
            $(tagControl.extForm).trigger('tagtoggle', obj);
        }
    });

    $(document).on('click.all', '.filter-tags__remove-link', function(e) {
        // console.log('type = ' + e.type + ', namespace = ' + e.namespace);
        e.preventDefault();
        var isToggle = 'all' == e.namespace ? 'auto' : 'toggle',
            obj = {
                attrName: $(this).parent().data('tag-name'),
                attrValue: $(this).parent().data('tag-value'),
                attrText: '',
                el: e.currentTarget,
                operation: isToggle
            };
        $(tagControl.extForm).trigger('tagtoggle', obj);
    });

    $(document).on('click', '[data-tag-clear]', function(){
        $('[type="hidden"]:not([class])',this.extForm).remove();
        $('.filter-tags__item.tag-added',this.tagsLayout).remove();
        $('.filter-tags__item:not(.tag-added) .filter-tags__remove-link',this.tagsLayout).each( function(){ $(this).trigger('click.all') } );
        
        $('.tag-added').removeClass('tag-added');
        tagControl.sendForm(true, "data-tag-clear");
    });

}
tagControl.init();

(function () {
    var Neb = function () {
    };
    Neb.prototype = $.extend(Neb.prototype, {
        /**
         * Достать что то из объекта без проверок
         * @param {{}} object
         * @param      path
         */
        objectPathSearch: function (object, path) {
            path = path.split('.');
            for (var key in path) {
                if (!object || !object.hasOwnProperty(path[key])) {
                    return false;
                }
                object = object[path[key]];
            }
            return object;
        }
    });
    window.neb = new Neb();
})();

(function () {

    var _builPopup = function (popup) {
        popup.popup = $(
            '<div class="modal fade" id="access-closed-popup" >' +
            '</div>'
        );
        popup.popupDialog = $('<div class="modal-dialog" role="document"></div>');
        popup.popupContent = $('<div class="modal-content"></div>');
        popup.popupBody = $('<div class="modal-body"></div>');

        popup.popup.append(popup.popupDialog);
        popup.popupDialog.append(popup.popupContent);
        popup.popupContent.append(popup.popupBody);
    };

    var _openPopup = function (popup, popupMethod) {
        $('body').append(popup.popup);
        if ('function' === typeof popupMethod) {
            popupMethod.apply(popup);
        } else if ('function' === typeof popup[popupMethod]) {
            popup[popupMethod]();
        }
        popup.popup.modal();
    };

    var _setHeader = function (popup, hiddenClose) {
        var hiddenCloseBtn = (hiddenClose === true) ? " hidden": "";

        popup.popupHeader = $('<div class="modal-header"></div>');
        popup.popupHeader.append(
            $('<button type="button" class="close'+hiddenCloseBtn+'" data-dismiss="modal" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                '<br>'
            )
        );
        popup.popupContent.prepend(popup.popupHeader);
    };
    var _setFooter = function (popup) {
        popup.popupFooter = $('<div class="modal-footer"></div>');
        popup.popupContent.append(popup.popupFooter);
    };

    var _createPopup = function (popupMethod) {
        return new SimplePopup(popupMethod);
    };

    function SimplePopup(popupMethod) {
        _builPopup(this);
        _openPopup(this, popupMethod);
    }

    SimplePopup.prototype = $.extend(SimplePopup.prototype, {
        popup: null,
        popupDialog: null,
        popupContent: null,
        popupHeader: null,
        popupBody: null,
        popupFooter: null,
        applyHeader: function (hidden) {
            _setHeader(this, hidden);
        },
        applyFooter: function () {
            _setFooter(this);
        }
    });

    FRONT.simplePopup = _createPopup;
})();

FRONT.search = FRONT.search || {};
$(function(){
    FRONT.search.autocompleteSimple('[data-autocomplete-item]');
});
FRONT.search.autocompleteSimple = function(target) {
    if (! $(target).length ) {
        return
    }
    var passedObj = $(target), // passed objects
        items, // items to attach
        successes, // set of functions to parse response
        fullSearchApiSrc = '/local/components/exalead/search.form/autocomplete.php';

    if ( passedObj.length > 1 ) {
        items = passedObj.filter('[data-autocomplete-item]');
    } else if ( passedObj.length == 1 && passedObj.get(0).hasAttribute('data-autocomplete-item') ) {
        items = passedObj;
    } else if ( passedObj.length == 1 && !(passedObj.get(0).hasAttribute('data-autocomplete-item')) ) {
        items = passedObj.find('[data-autocomplete-item]');
    }
    if (items.length == 0) {
        return
    }

    successes = {//парсинг ответа
        default: function(data, entityField, response) {
            var queries = [];
            if (data.entities) {
                for (var key in data.entities) {
                    if(data.entities[key][entityField]) {
                        queries.push(data.entities[key][entityField]);
                    }
                }
            }
            response(queries);
        },
        search: function(data, entityField, response) {
            var queries = [];
            if (data) {
                for (var i = 0; i < data.length; i += 1) {
                    if(typeof data[i] == 'object') {
                        queries.push(data[i]['label']);
                    }
                }
            }
            response(queries);
        }
    };

    items.each(function(){
        var item = $(this),
            // search = typeof undefined !== typeof item.attr('data-search-auto-complete'),
            options = {
                //success: search ? successes.search : successes.default
                success: successes.search
            };
        if ( item[0].hasAttribute('data-filtered-by') ) {
            options.filteredBy = $(item).attr('data-filtered-by');
        }
        if ( item[0].hasAttribute('data-entity-field') ) {
            options.success = successes.default /* ентфилд - поле в ответе которое нужно парсить? */
        }
        applyAutoComplete( item , options );
    });

    function applyAutoComplete (input, options) {
        var _success = options.success || successes.default,
            entityField = input.data('entity-field');

        input.autocomplete({
            delay: 500,
            minLength: 1,
            source: function (request, response) {
                /*###################*/
                var inputEl = input.get(0),
                    apiSource,
                    sendData = {},
                    term,
                    filteredBy,
                    sessionId = input.closest('form').data('sessionId');

                term = input.val();
                sendData['term'] = term;
                apiSource = fullSearchApiSrc;

                if (sessionId) {
                    sendData['sessid'] = sessionId;
                }

                if ( inputEl.hasAttribute('data-entity-field') ) {
                    sendData['field'] = input.attr('data-query-field');
                }

                if ( inputEl.hasAttribute('data-autocomplete-full-api') ) {
                    sendData['s_in_closed'] = 'on';
                    sendData['is_full_search'] = 'on';
                    sendData['librarytype'] = 'library';
                } else {
                    sendData['type'] = 'double';
                    sendData['field'] = inputEl.getAttribute('data-query-field');
                }
                /*#####################*/

                var filterName,
                    filterField,
                    filterFieldVal,
                    sendDataFilterKey,
                    contextFieldset = $(input).closest('form');

                if (!contextFieldset.length) {
                    contextFieldset = $(input).closest('fieldset').length != 0 ? $(input).closest('fieldset') : $(input).closest('[data-widget]');
                }

                if (input[0].hasAttribute('data-filtered-by')) {
                    filterName = input.data('filtered-by');
                    filterField = contextFieldset.find('[data-query-field="'+filterName+'"]');
                    filterFieldVal = filterField.val();
                    sendDataFilterKey = 'filter[' + filterName + ']';
                    if (filterFieldVal) {
                        sendData[sendDataFilterKey] = filterFieldVal;
                    }
                }

                $.ajax({
                    url: apiSource,
                    dataType: 'json',
                    data: sendData,
                    success: function (data) {
                        _success(data, entityField, response);
                    }
                });
            },
            select: function( event, ui ) {
                var url = window.location.href,
                    search; 
                if ( url.indexOf('authors') != -1 ) {
                } else if ( url.indexOf('editions') != -1 || url.indexOf('substitustion') != -1 ) {
                } else {
                    search = '?query=' + ui.item["value"];
                    // window.location.search = search;
                }
            },
            autofocus: true
        })
    }
}