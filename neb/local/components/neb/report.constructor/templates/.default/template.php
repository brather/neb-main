<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

$bProcedure = false;
foreach ($arResult['report']['entities'] as $iEntity) {
    if ('procedure' == $arResult['entities'][$iEntity]['Type'])
        $bProcedure = true;
}

?>
<link rel="stylesheet" type="text/css" href="/local/templates/adaptive/js/chosen/chosen.min.css" />
<?if ($_REQUEST['id'] > 0):?>
    Редактирование отчета - <?=$arResult['report']['data']["Name"]?><br /><br />
<?endif;?>

<section class="innersection innerwrapper clearfix">
    <div class="step-selector">
        <div class="step-selector__item step-selector__item_active<?if ($_REQUEST['id'] > 0):?> step-selector__item_link<?endif;?>" data-step="1">Данные</div>
        <div class="step-selector__item<?if ($arResult['report']['id']):?> step-selector__item_link<?endif;?>" data-step="2">Фильтрация</div>
        <div class="step-selector__item<?if ($arResult['report']['id'] && !$bProcedure):?> step-selector__item_link<?endif;?>" data-step="3">Группировка</div>
        <div class="step-selector__item<?if ($arResult['report']['id'] && !$bProcedure):?> step-selector__item_link<?endif;?>" data-step="4">Сортировка</div>
    </div>
    <div class="steps">
        <form action='' method='post'>
            <input type='hidden' name='<?if ($arResult['report']['id']):?>edit<?else:?>add<?endif;?>_report' value='Y'>
			<input type='hidden' class="report_name" name='report_name' value='<?=$_REQUEST['report_name']?>'>
			<?if ($_REQUEST['id'] > 0):?><input type='hidden' class="report_id" name='report_id' value='<?=$_REQUEST['id']?>'><?endif;?>
            <div class="steps__wrapper steps__wrapper_start<?if ($arResult['report']['id']):?> _loaded<?endif;?>" data-id="1">
                <h3>Объекты учета</h3>
                <select data-placeholder="Выберите объекты" class="chosen-select" multiple="" tabindex="3"
                        name='entities[]'>
                    <option value=""></option>
                    <? foreach ($arResult['entities'] as $id => $item): ?>
                        <option value="<?= $id ?>" <?if (in_array($id, $arResult['report']['entities'])):?>selected<?endif;?> type="<?= $item['Type'] ?>"><?= $item['Description'] ?></option>
                    <? endforeach; ?>
                </select>
                <div class="steps__loader">
                    <span class="spinner"></span>
                    Загружаем данные
                </div>
                <div class="steps__error">
                    Произошла ошибка
                </div>
                <div class="steps__content steps__content_step-one<?if ($arResult['report']['id']):?> steps__content_visible<?endif;?>">
				<?if ($arResult['report']['id']):?>
				
                    <? if (count($arResult['report']['entities']) > 1){ ?>
                        <div class="filter-list">
                        <h4 class="filter-list__title">Связи</h4>
                            <?foreach ($arResult['report']['relations']['join'] as $relation):?>
                                <label for="join<?=$relation['Id']?>">
                                    <input type='hidden' name='entity_1_<?=$relation['Id']?>' value='<?=$relation['tblid1']?>'>
                                    <input type='hidden' name='entity_2_<?=$relation['Id']?>' value='<?=$relation['tblid2']?>'>
                                    <input class="relation-checkbox" type="checkbox" id="join<?=$relation['Id']?>" name="join[]" value="<?=$relation['Id']?>" <?if (in_array($relation['Id'], $arResult['report']['joins'])):?>checked<?endif;?>>
                                    <span class="lbl"><?=$relation['Description']?></span>
                                </label>
                            <?endforeach;?>
                        </div>
                    <? } ?>
                    <div class="filter-wrap">
                        <? foreach ($arResult['report']['entities'] as $item):?>
                            <div class="filter-list">
                            <h4 class="filter-list__title">
                            <?=$arResult['entities'][$item]['Description'];?>
                            </h4>
                            <?foreach ($arResult['report']['entityFields'][$item] as $field):?>
                                <div class="steps__row">
                                    <label for="fields<?=$field['Id']?>">
                                        <input type="checkbox" id="fields<?=$field['Id']?>" name="fields[]" value="<?=$field['Id']?>" <?if (in_array($field['Id'], $arResult['report']['fields'])):?>checked<?endif;?>>
                                        <span class="lbl"><?=$field['Description']?></span>
                                    </label>
                                </div>
                            <?endforeach;?>
                            </div>
                        <? endforeach;?>
                    </div>
				
				<?endif;?>
                </div>

                <div class="steps__footer<?if ($arResult['report']['id']):?> steps__footer_visible<?endif;?>">
                    <div class="text-right">
                        <a class="btn btn-primary next-step" target="_blank" href="#">
                            Далее
                        </a>
                    </div>
                </div>

            </div>
            <div class="steps__wrapper<?if ($arResult['report']['id']):?> _loaded<?endif;?>" data-ajax="/local/tools/report_constructor/filters.php" data-id="2">
                <h3>Поля для фильтрации</h3>
				
                <div class="steps__content<?if ($arResult['report']['id']):?> steps__content_visible<?endif;?>">
				<?if ($arResult['report']['id']):?>
						<div class="filters-wrap">
						<?if (count($arResult['report']['where']['data']) > 0):?>
						<?foreach ($arResult['report']['where']['data'] as $where):?>
						
							<div class="row steps__row clearfix">
								<div class="form-group col-sm-3">
									<select id="<?=$where['idfield']?>" class="js-select filter-select form-control" name="filter[]" data-select-picker  data-input-name="value_compare[]">
										<?foreach ($arResult['report']['entities'] as $item):
                                            $res = TblEntities::getEntities($item);
                                            $arFields = TblEntities::getEntityFields($item);?>
                                            <?foreach ($arFields as $field):?>
                                                <option value='<?=$field['Id']?>' type="<?=$field['FieldType']?>" <?if ($field['Id'] == $where['idfield']):
                                                ?>selected<?endif;?>><?=$res[$item];?> - <?=$field['Description']?></option>
                                            <?endforeach;?>
										<?endforeach;?>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<select id="" class="js-select form-control" name="compare[]" data-select-picker>
										<?foreach ($arResult['compare'] as $id=>$name):?>
										    <option value=<?=$id?> <?if ($id == $where['idcompare']):?>selected<?endif;?>><?=$name?></option>
										<?endforeach;?>
									</select>
								</div>

								<div class="form-group col-sm-3 input-cont">
								<?if ($where['FieldType'] == 'date') $where['Value'] = $DB->FormatDate($where['Value'], "YYYY-MM-DD", "DD.MM.YYYY");?>
									<input type="text" class="init-input" value="<?=$where['Value']?>" />
								</div>

								<div class="form-group col-sm-1">
									<a class="btn btn-default add-next-row">+</a>
									<a class="btn btn-default del-this-row">−</a>
								</div>
							</div>
						
						<?endforeach;?>
						<?else:?>
							<div class="row steps__row clearfix">
								<div class="form-group col-sm-3">
									<select  class="js-select filter-select form-control" name="filter[]" data-select-picker  data-input-name="value_compare[]">
										
										<?foreach ($arResult['report']['entities'] as $item):
										$res = TblEntities::getEntities($item);
										$arFields = TblEntities::getEntityFields($item);?>
										<?foreach ($arFields as $field):?>
										<option value='<?=$field['Id']?>' type="<?=$field['FieldType']?>"><?=$res[$item];?> - <?=$field['Description']?></option>
										<?endforeach;?>
										<?endforeach;?>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<select id="" class="js-select form-control" name="compare[]" data-select-picker>
										<?foreach ($arResult['compare'] as $id=>$name):?>
										<option value=<?=$id?> ><?=$name?></option>
										<?endforeach;?>
									</select>
								</div>

								<div class="form-group col-sm-3 input-cont">
									<input type="text" class="init-input" value="">
								</div>

								<div class="form-group col-sm-1">
									<a class="btn btn-default add-next-row">+</a>
									<a class="btn btn-default del-this-row">−</a>
								</div>
							</div>
						
						<?endif;?>
						</div>
				
				<?endif;?>
                </div>

                <div class="steps__loader">
                    <span class="spinner"></span>
                    &nbsp;
                    Загружаем данные
                </div>
                <div class="steps__error">
                    Произошла ошибка
                </div>

                <div class="steps__footer<?if (!$bProcedure):?> steps__footer_visible<?endif;?>">
                    <div class="text-right">
                        <a class="btn btn-primary next-step" target="_blank" href="#">
                            Далее
                        </a>
                    </div>
                </div>

                <div class="steps__submit<?if ($bProcedure):?> steps__footer_visible<?endif;?>">
                    <div class="text-right">
                        <a class="btn btn-primary report-form-submit">
                            Cформировать отчет
                        </a>
                    </div>
                </div>
            </div>
            <div class="steps__wrapper<?if ($arResult['report']['id']):?> _loaded<?endif;?>" data-ajax="/local/tools/report_constructor/groups.php" data-id="3">
                <div class="steps__content<?if ($arResult['report']['id']):?> steps__content_visible<?endif;?>">
					<?if ($arResult['report']['id']):?>
						<div class="steps__block_group">
						<?foreach ($arResult['report']['entities'] as $item):?>
						<?foreach ($arResult['report']['entityFields'][$item] as $field):?>
							<div class="steps__row clearfix row">
								<div class="form-group col-sm-3">
									<label for="group<?=$field['Id']?>">
										<input type="checkbox" id="group<?=$field['Id']?>" name="group[]" value="<?=$field['Id']?>" class="checkbox-depend-toggler" <?if (in_array($field['Id'], $arResult['report']['agrigate']['id'])):?>checked<?endif;?>>
										<span class="lbl"><?=$arResult['entities'][$item]['Description'];?> - <?=$field['Description']?></span>
									</label>
								</div>

								<div class="form-group col-sm-2">
									<select id="" class="js-select form-control" name="agr<?=$field['Id']?>" data-select-picker>
										<?foreach ($arResult['functions'] as $id=>$name):?>
										<option value=<?=$id?> <?if (in_array($field['Id'], $arResult['report']['agrigate']['id']) && $id == $arResult['report']['agrigate']['data'][$field['Id']]['IdReportAgregateType']):?>selected<?endif;?>><?=$name?></option>
										<?endforeach;?>
									</select>
								</div>

								<div class="form-group col-sm-2  <?if (!in_array($field['Id'], $arResult['report']['agrigate']['id'])):?>checkbox-depend<?endif;?>">
									<select id="" class="js-select form-control" name="having_compare<?=$field['Id']?>" data-select-picker>
										<?foreach ($arResult['compare'] as $id=>$name):?>
										<option value=<?=$id?> <?if (in_array($field['Id'], $arResult['report']['agrigate']['id']) && $id == $arResult['report']['agrigate']['data'][$field['Id']]['IdReportCompareType']):?>selected<?endif;?>><?=$name?></option>
										<?endforeach;?>
									</select>
								</div>

								<div class="form-group col-sm-2 <?if (!in_array($field['Id'], $arResult['report']['agrigate']['id'])):?>checkbox-depend<?endif;?>" style='width:130px;'>
									<input type="text" placeholder="Значение" name='value_agr<?=$field['Id']?>'  <?if (in_array($field['Id'], $arResult['report']['agrigate']['id'])):?>value="<?=$arResult['report']['agrigate']['data'][$field['Id']]['Value']?>"<?endif;?> style='width:100px;'>
								</div>
							</div>
						<?endforeach;?>
						<?endforeach;?>
						</div>
						<h4 class="filter-list__title">Группировать по:</h4>
						<div>
							<?foreach ($arResult['report']['group']['data'] as $field):?>
							<div class="steps__row clearfix row">
								<div class="form-group col-sm-2" style="width: 45px;padding-right: 0;">
									<a href="" class="sort-arrow sort-up">↑</a>
									<a href="" class="sort-arrow sort-down">↓</a>
								</div>
								<div class="form-group col-sm-3">
									<label for="groupby<?=$field['Id']?>">
										<input type="checkbox" id="groupby<?=$field['Id']?>" name="groupby[]" value="<?=$field['Id']?>" checked>
										<span class="lbl"><?=$arResult['entities'][$field['eid']]['Description'];?> - <?=$field['Description']?></span>
									</label>
								</div>
							</div>
							<?endforeach;?>
							<?foreach ($arResult['report']['entities'] as $item):?>
							<?foreach ($arResult['report']['entityFields'][$item] as $field):?>
							<?if (!in_array($field['Id'], $arResult['report']['group']['id'])):?>
								<div class="steps__row clearfix row">
									<div class="form-group col-sm-2" style="width: 45px;padding-right: 0;">
										<a href="" class="sort-arrow sort-up">↑</a>
										<a href="" class="sort-arrow sort-down">↓</a>
									</div>
									<div class="form-group col-sm-3">
										<label for="groupby<?=$field['Id']?>">
											<input type="checkbox" id="groupby<?=$field['Id']?>" name="groupby[]" value="<?=$field['Id']?>" >
											<span class="lbl"><?=$arResult['entities'][$item]['Description'];?> - <?=$field['Description']?></span>
										</label>
									</div>
								</div>
								<?endif;?>
								<?endforeach;?>
								<?endforeach;?>						
						</div>
					<?endif;?>
                </div>


                <div class="steps__loader">
                    <span class="spinner"></span>
                    &nbsp;
                    Загружаем данные
                </div>
                <div class="steps__error">
                    Произошла ошибка
                </div>


                <div class="steps__footer">
                    <div class="text-right">
                        <a class="btn btn-primary next-step" target="_blank" href="#">
                            Далее
                        </a>
                    </div>
                </div>
            </div>
            <div class="steps__wrapper<?if ($arResult['report']['id']):?> _loaded<?endif;?>" data-ajax="/local/tools/report_constructor/sort.php" data-id="4">
                <h3>Поля для сортировки</h3>

                <div class="steps__content<?if ($arResult['report']['id']):?> steps__content_visible<?endif;?>">
					<?if ($arResult['report']['id']):?>
					
						<?foreach ($arResult['report']['order']['data'] as $field):?>
							<div class="steps__row row clearfix">
								<div class="form-group col-sm-1" style="width: 45px;padding-right: 0;">
									<a href="" class="sort-arrow sort-up">↑</a>
									<a href="" class="sort-arrow sort-down">↓</a>
								</div>
								<?if ($field['agrType']):?>
								<div class="form-group col-sm-3" style="padding-left: 0;">
									<label for="sort<?=$field['Id']?>">
										<input type="checkbox" id="sort<?=$field['Id']?>" name="sort[]" value="agr<?=$field['Id']?>" checked>
										<span class="lbl"><?=$arResult['functions'][$field['agrType']]?>(<?=$arResult['entities'][$field['eid']]['Description'];?> - <?=$field['Description']?>)</span>
									</label>
								</div>
								<div class="form-group col-sm-2">
									<select id="" class="js-select form-control" name="order_agr<?=$field['Id']?>" data-select-picker>
										<?foreach ($arResult['order'] as $id=>$name):?>
										<option value=<?=$id?> <?if ($id == $field['OrderType']):?>selected<?endif;?>><?=$name?></option>
										<?endforeach;?>
									</select>
								</div>
								<?else:?>
								<div class="form-group col-sm-3" style="padding-left: 0;">
									<label for="sort<?=$field['Id']?>">
										<input type="checkbox" id="sort<?=$field['Id']?>" name="sort[]" value="<?=$field['Id']?>" checked>
										<span class="lbl"><?=$arResult['entities'][$field['eid']]['Description'];?> - <?=$field['Description']?></span>
									</label>
								</div>
								<div class="form-group col-sm-2">
									<select id="" class="js-select form-control" name="order<?=$field['Id']?>" data-select-picker>
										<?foreach ($arResult['order'] as $id=>$name):?>
										<option value=<?=$id?> <?if ($id == $field['OrderType']):?>selected<?endif;?>><?=$name?></option>
										<?endforeach;?>
									</select>
								</div>
								<?endif;?>
							</div>
						<?endforeach;?>
					
						<?foreach ($arResult['report']['entities'] as $item):?>
						<?foreach ($arResult['report']['entityFields'][$item] as $field):?>
						
						<?if (!in_array($field['Id'], $arResult['report']['order']['id'])):?>
							<div class="steps__row row clearfix">
								<div class="form-group col-sm-1" style="width: 45px;padding-right: 0;">
									<a href="" class="sort-arrow sort-up">↑</a>
									<a href="" class="sort-arrow sort-down">↓</a>
								</div>
								<div class="form-group col-sm-3" style="padding-left: 0;">
									<label for="sort<?=$field['Id']?>">
										<input type="checkbox" id="sort<?=$field['Id']?>" name="sort[]" value="<?=$field['Id']?>">
										<span class="lbl"><?=$arResult['entities'][$item]['Description'];?> - <?=$field['Description']?></span>
									</label>
								</div>
								<div class="form-group col-sm-2">
									<select id="" class="js-select form-control" name="order<?=$field['Id']?>" data-select-picker>
										<?foreach ($arResult['order'] as $id=>$name):?>
										<option value=<?=$id?>><?=$name?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
						<?endif;?>
						<?endforeach;?>
						<?endforeach;?>
						<?foreach ($arResult['report']['agrigate']['data'] as $field):?>
							<?if (!in_array($field['Id'], $arResult['report']['order']['agr_id'])):?>
							<?$fieldData = TblEntities::getField($field['Id']);?>
							<div class="steps__row row clearfix">
								<div class="form-group col-sm-1" style="width: 45px;padding-right: 0;">
									<a href="" class="sort-arrow sort-up">↑</a>
									<a href="" class="sort-arrow sort-down">↓</a>
								</div>
								<div class="form-group col-sm-3" style="padding-left: 0;">
									<label for="sort<?=$fieldData['Id']?>">
										<input type="checkbox" id="sort<?=$fieldData['Id']?>" name="sort[]" value="agr<?=$fieldData['Id']?>">
										<span class="lbl"><?=$arResult['functions'][$field['IdReportAgregateType']]?>(<?=$fieldData['EntityName'];?> - <?=$fieldData['Description']?>)</span>
									</label>
								</div>
								<div class="form-group col-sm-2">
									<select id="" class="js-select form-control" name="order_agr<?=$fieldData['Id']?>" data-select-picker>
										<?foreach ($arResult['order'] as $id=>$name):?>
										<option value=<?=$id?>><?=$name?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
							<?endif;?>
						<?endforeach;?>
				<?endif;?>
                </div>

                <div class="steps__loader">
                    <span class="spinner"></span>
                    &nbsp;
                    Загружаем данные
                </div>
                <div class="steps__error">
                    Произошла ошибка
                </div>


                <div class="steps__footer">
                    <div class="text-right">
                        <a class="btn btn-primary report-form-submit">
                            Cформировать отчет
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

