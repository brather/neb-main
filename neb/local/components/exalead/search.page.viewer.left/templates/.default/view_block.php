<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(empty($arParams['ajax']))
    include_once($_SERVER['DOCUMENT_ROOT'].'/local/include_areas/books_closed.php');

?>
<div class="b-filter_items_wrapper" style="display: block;">
	<div class="b-result-doc b-result-docfavorite">
		<?
			if($arParams['ajax'])
				$APPLICATION->RestartBuffer();

			if(!empty($arResult['ITEMS']))
			{
				foreach($arResult['ITEMS'] as $arItem)
				{
				?>
				<div class="b-result-docitem iblock removeitem search-result" id="<?=$arItem['id']?>">
					<div class="b-result-docinfo">
						<div class="b-result-docphoto">
							<a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/<?=$arItem['~id']?>/"><img src="/local/tools/exalead/thumbnail.php?url=<?=$arItem['URL']?>&source=<?=$arItem['source']?>&width=80&height=125" class="loadingimg" data-autor="<?=$arItem['~authorbook']?>" data-title="<?=$arItem['~title']?>" ></a>
						</div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/<?=$arItem['~id']?>/" title="<?=$arItem['title']?>"><?=trimming_line($arItem['title'], 45)?></a></h2>
						<?
							if(!empty($arItem['authorbook']))
							{
							?>
							<a href="/search/?f_field[authorbook]=f/authorbook/<?=urlencode(mb_strtolower(strip_tags(trim($arItem['authorbook']))))
							.'&'.DeleteParam(['f_field', 'dop_filter'])?>" class="b-book_autor"><?=$arItem['authorbook']?></a>
							<?
							}
						?>
						<div class="b-result_sorce clearfix">
							<?
								if ($arItem['source'] == 'VideoData')
								{
								?><span class="b-result-type_txt"><a onclick="popupVideoWindow('<?=$arItem['VIDEO_URL']?>', '<?=$arItem['title']?>'); return false;" target="_blank" href="#">смотреть</a></span><?
								}
								elseif($arItem['isprotected'] > 0 and !empty($arItem['PROTECTED_URL']))
								{
									if((bool)$arParams['IS_SHOW_PROTECTED'] === true)
									{
										/*?><span class="b-result-type_txt"><a target="_blank" href="<?=$arItem['PROTECTED_URL']?>" onclick="messageOpen(this.href, '.b-message_warning'); return false;">читать</a></span><?*/
									?><span class="b-result-type_txt"><a target="_blank" href="<?=$arItem['PROTECTED_URL']?>">читать</a></span><?
									}
									else
									{
									?><span class="b-result-type_txt closed_book"><a target="_blank" data-link="<?=$arItem['PROTECTED_URL']?>" href="#">читать</a></span><?
									}
								}
								elseif(!empty($arItem['fileExt']))
								{
								?>
								<div class="b-result-type">
									<?
										foreach($arItem['fileExt'] as $ext)
										{
											if($ext != 'pdf')
												continue;

											if($ext == 'pdf' and $arItem['IS_READ'] == 'Y'){
											?>
											<span class="b-result-type_txt"><a target="_blank" href="<?=$arItem['VIEWER_URL']?>">читать</a></span>
											<?
											}else{
												/*?>
												<span class="b-result-type_txt" ><?=$ext?></span>
												<?*/
											}
										}
									?>
								</div>	
								<?
								}
							?>	
						</div><!-- /.b-result_sorce -->
						<span class="num"><?=$arItem['NUM_ITEM']?>.</span>
					</div>

				</div><!-- /.b-result-docitem -->
				<?
				}

				if(!$arParams['ajax'] and !empty($arResult['NEXT_URL_PARAMS']))
				{
				?>
				<div class="b-result-docmore rel" ><a class="js_ajax_loadresult" href="<?=$this->__component->__parent->__template->__folder.'/next_result.php?'.$arResult['NEXT_URL_PARAMS']?>"></a></div>
				<?
				}
			}

			if($arParams['ajax'])
			{
			?>
			<script type="text/javascript">
				<?
					if(!empty($arResult['NEXT_URL_PARAMS'])){
					?>
					$('a.js_ajax_loadresult').attr('href', '<?=$this->__component->__parent->__template->__folder.'/next_result.php?'.$arResult['NEXT_URL_PARAMS']?>');
					<?
					}else{
					?>
					$('div.b-result-docmore').remove();
					<?
					}
				?>
				$('.popup_opener').uipopup();
				checkIsAvailablePdf();
			</script>
			<?
				exit();
			}
		?>
	</div><!-- /.b-result-doc -->
	<?=htmlspecialcharsBack($arResult['STR_NAV'])?>
</div>