<?
namespace Bitrix\NotaExt\Ext\typograph;

use Bitrix\NotaExt\NPHPCacheExt;

require_once(__DIR__ . '/emt.php');

/*
Получаем опции сохраненные для типографа
*/

class EmtOptions
{
    public function getOptions()
    {
        $arOptions = array();

        $obCacheExt = new NPHPCacheExt();
        $arCacheParams = array();
        $cacheTag = "typograph";

        if (!$obCacheExt->InitCache(__function__, $arCacheParams, $cacheTag)) {
            $typograph = new \EMTypograph();

            $tret_list = $typograph->get_trets_list();
            foreach ($tret_list as $tret) {
                $tret_obj = $typograph->get_tret($tret);
                foreach ($tret_obj->rules as $rulename => $rule) {
                    $valOption = \COption::GetOptionString('notaext', 'typograph_' . $rulename);
                    if (!empty($valOption)) {
                        $topt = explode('_', $tret);
                        $arOptions[$topt[2] . '.' . $rulename] = $valOption == 'Y' ? 'on' : 'off';
                    }

                }
            }

            $obCacheExt->StartDataCache($arOptions);
        } else {
            $arOptions = $obCacheExt->GetVars();
        }

        return $arOptions;

    }
}
