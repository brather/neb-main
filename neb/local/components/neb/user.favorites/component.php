<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;

CModule::IncludeModule("nota.userdata");
use Nota\UserData\Books;
use Nota\UserData\Quotes;

CModule::IncludeModule('iblock');

$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_BOOKS_DATA_USERS)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

// highload quo
$hlblock_quo = HL\HighloadBlockTable::getById(HIBLOCK_QUO_DATA_USERS)->fetch();
$entity_quo = HL\HighloadBlockTable::compileEntity($hlblock_quo);
$entity_data_class_quo = $entity_quo->getDataClass();

// highload note
$hlblock_note = HL\HighloadBlockTable::getById(HIBLOCK_NOTES_DATA_USERS)->fetch(
);
$entity_note = HL\HighloadBlockTable::compileEntity($hlblock_note);
$entity_data_class_note = $entity_note->getDataClass();

// highload bookmark
$hlblock_bookmark = HL\HighloadBlockTable::getById(HIBLOCK_BOOKMARKS_DATA_USERS)
    ->fetch();
$entity_bookmark = HL\HighloadBlockTable::compileEntity($hlblock_bookmark);
$entity_data_class_bookmark = $entity_bookmark->getDataClass();
if ($_REQUEST['action'] == 'remove' and !empty($_REQUEST['id'])
    and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'
) {
    $APPLICATION->RestartBuffer();
    $bookId = urldecode($_REQUEST['id']);
    $books = new Books();
    $books->deleteFromCollection($bookId);
    $books->delete($bookId);
    exit();
}

$arParamsGetList = array(
    "select" => array('UF_BOOK_ID', 'UF_DATE_ADD'),
    "filter" => array('UF_UID' => $USER->GetID()),
    "order"  => array('UF_DATE_ADD' => 'DESC')
);
error_reporting(E_ERROR);
ini_set('display_errors', 1);
$by = trim(htmlspecialcharsEx($_REQUEST['by']));
$order = trim(htmlspecialcharsEx($_REQUEST['order']));
$arParamsGetList['runtime'] = [];
if (!empty($by) and !empty($order)) {
    $bookDataClass = \NebMainHelper::getHIblockDataClassByName('BooksData');
    $arParamsGetList['runtime']['book_data'] = [
        'data_type' => $bookDataClass,
        'reference' => ['=this.UF_BOOK_ID' => 'ref.UF_BOOK_ID'],
    ];

    /** @todo найти как генерится алиас и выпилить users_books_data_book_data */
    $arParamsGetList['runtime']['DATA_IS_NULL'] = new Entity\ExpressionField(
        'DATA_IS_NULL', "(ISNULL(`users_books_data_book_data`.UF_BOOK_ID))"
    );
    $arParamsGetList['order'] = [
        'DATA_IS_NULL'          => 'asc',
        ('book_data.UF_' . $by) => $order . '',
    ];
}


if (!empty($arParams['UF_READING'])) {
    $arParamsGetList['filter']['UF_READING'] = $arParams['UF_READING'];
}

if ((int)$arParams['COLLECTION_ID'] > 0) {

    $hlblockLinks = HL\HighloadBlockTable::getById(
        HIBLOCK_COLLECTIONS_LINKS_USERS
    )->fetch();
    $entitykLinks = HL\HighloadBlockTable::compileEntity($hlblockLinks);
    $entity_data_classkLinks = $entitykLinks->getDataClass();

    $arParamsGetList['runtime']['link'] = [
        "data_type" => $entity_data_classkLinks,
        'reference' => ['=this.ID' => 'ref.UF_OBJECT_ID'],
    ];

    $arParamsGetList['filter']['=link.UF_COLLECTION_ID']
        = (int)$arParams['COLLECTION_ID'];
    $arParamsGetList['filter']['=link.UF_TYPE'] = 'books';
}
$pageSize = 10;
$arResult['iNumPage'] = is_set($_GET['PAGEN_1']) ? (int)$_GET['PAGEN_1'] : 1;
$arParamsGetList['count_total'] = true;
$arParamsGetList['offset'] = $pageSize * ($arResult['iNumPage'] - 1);
$arParamsGetList['limit'] = $pageSize;
$rsData = $entity_data_class::getList($arParamsGetList);
$arResult['STR_NAV'] = (string)new \Neb\Main\Navigation(
    [
        'nav' => [
            'totalCount'       => $rsData->getCount(),
            'navPageNumber'    => $arResult['iNumPage'],
            'pageElementCount' => $pageSize,
        ]
    ]
);

$arResult['RESBOOKS'] = array();
while ($arData = $rsData->Fetch()) {
    $arData['id'] = $arData['UF_BOOK_ID'];
    $arData['UF_DATE_ADD'] = $arData['UF_DATE_ADD']->toString();
    $arResult['RESBOOKS'][$arData['UF_BOOK_ID']] = $arData;
}
$arBooksID = array();
foreach ($arResult['RESBOOKS'] as &$arData) {
    $arParams = array(
        "select" => array("*"),
        "filter" => array('UF_UID'     => $USER->GetID(),
                          'UF_BOOK_ID' => $arData['UF_BOOK_ID']),
        "order"  => array('UF_DATE_ADD' => 'DESC')
    );

    // UF_CNT_QUOTES
    $rData_quo = $entity_data_class_quo::getList($arParams);
    $rData_quo = new CDBResult($rData_quo, $hlblock_quo["TABLE_NAME"]);
    $arData['UF_CNT_QUOTES'] = $rData_quo->SelectedRowsCount();

    // UF_CNT_NOTES
    $rData_note = $entity_data_class_note::getList($arParams);
    $rData_note = new CDBResult($rData_note, $hlblock_note["TABLE_NAME"]);
    $arData['UF_CNT_NOTES'] = $rData_note->SelectedRowsCount();

    // UF_CNT_BOOKMARKS
    $rData_bookmark = $entity_data_class_bookmark::getList($arParams);
    $rData_bookmark = new CDBResult(
        $rData_bookmark, $hlblock_bookmark["TABLE_NAME"]
    );
    $arData['UF_CNT_BOOKMARKS'] = $rData_bookmark->SelectedRowsCount();

    $arBooksID[] = $arData['UF_BOOK_ID'];
}
unset($arData);

if (empty($arBooksID)) {
    return false;
}

/*
Получаем данные из экзалиды
*/
$arParams['ITEM_COUNT'] = empty($_REQUEST['pagen']) ? 15
    : (int)$_REQUEST['pagen'];
if (!CModule::IncludeModule('nota.exalead')) {
    return false;
}

use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;

$query = new SearchQuery();
$query->getByArIds($arBooksID);


/* Уточнение */
if (!empty($_REQUEST['f_field'])) {
    $f_field = $_REQUEST['f_field'];
    if (!is_array($f_field)) {
        $f_field = array($f_field);
    }

    foreach ($f_field as $k => $v) {
        $v = urldecode($v);
        $v = trim($v);
        $query->setParam('r', '+' . $v, true);
    }
}

$query->setPageLimit(10000);

$client = new SearchClient();
$arResult = array_merge($arResult, $client->getResult($query));

if (intval($arResult['COUNT']) > 0 and !empty($arResult['ITEMS'])) {

    $items = $arResult['ITEMS'];
    $arResult['ITEMS'] = $arResult['RESBOOKS'];
    foreach ($items as $item) {
        if (isset($arResult['ITEMS'][$item['id']])) {
            $arResult['ITEMS'][$item['id']] = $item;
        }
    }
} else {
    $arResult['ITEMS'] = $arResult['RESBOOKS'];
}

$this->IncludeComponentTemplate();

$APPLICATION->SetTitle(Loc::getMessage('PAGE_TITLE'));

return array('arParams' => $arParams, 'arResult' => $arResult);