<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<div class="b-map rel mappage">
    <form method="get" action="." class="b-map_search_filtr">
        <div class="col-md-6 b-cytyinfo">
            <span class="b-map_search_txt"><?=Loc::getMessage('FIND_CLOSEST_LIBRARY')?></span>
            <? if ($arResult['CITY']):?>
                <a class="b-citycheck setLinkPoint"
                   data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>"
                   data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>"
                   href="#"><?= $arResult['CITY']["UF_CITY_NAME"] ?></a>
            <?endif?>
            <a href="/participants/cities/"> <?=Loc::getMessage('CHANGE')?></a>
        </div>
        <div class="col-md-6 b-citysearch">
            <input type="text" class="b-map_tb ui-autocomplete-input searchonmap" name="mapq" id="qcity" autocomplete="off" placeholder="<?=Loc::getMessage('ADDRESS_OR_NAME')?>"
                   value="<?php echo isset($_REQUEST['mapq']) ? htmlentities($_REQUEST['mapq'])
                       : '' ?>">
            <button type="submit" class="button_b" value="<?=Loc::getMessage('SEARCH_BTN')?>" name="submit"><span class="fa fa-search"></span></button>
        </div>
    </form>
        <br>
        <br>
    <div class="ymap libraries-map" id="ymap"
         data-path="/local/tools/map/participants.php<?if ($_REQUEST['city_id']):?>?city_id=<?=$_REQUEST['city_id']?><?endif;?>"
         data-lat="<? if ($arResult['CITY']):?><?= $arResult["CITY"]["UF_POS"][1] ?><?else:?>55.753676<?endif;?>"
         data-lng="<? if ($arResult['CITY']):?><?= $arResult["CITY"]["UF_POS"][0] ?><?else:?>37.619899<?endif;?>"
         data-zoom="<? if ($arResult['CITY']):?>10<?else:?>5<?endif;?>"
         data-show-nearly="<?php echo (
             isset($_REQUEST['show-nearly'])
             && 'Y' === $_REQUEST['show-nearly']
         ) ? 1 : 0 ?>"
         data-find-location="<?= $arResult["FIND_LOCATION"] ?>"
        ></div>
</div><!-- /.b-map -->

<section class="rel innerwrapper clearfix" id="library-list">
    <div class="b-elar_usrs">
        <ol class="b-elar_usrs_list">
           <?$i = ($_REQUEST['PAGEN_1'] > 0? $arParams["NEWS_COUNT"]*($_REQUEST['PAGEN_1']-1) : 0);?>

            <?foreach($arResult["ITEMS"] as $arItem):
                $pos = explode(' ', $arItem['POS']);
                $i++;?>
                <li class="clearfix" itemtype="http://schema.org/Organization" itemscope="">
                    <span class="num"><?=$i?>.</span>
                    <div class="b-elar_name">
                        <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
                            <?if($arItem["DETAIL_PAGE_URL"]):?>
                                <a class='b-elar_name_txt' href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a><br /><br />
                            <?else:?>
                                <b><?=$arItem["NAME"]?></b><br />
                            <?endif?>
                            <?//=Loc::getMessage('MEMBER_OF_NEL')?>
                            <?/*if ($arItem['WCHZ'] == true):?><br /><?=Loc::getMessage('VRR')?><?endif*/?>
                        <?endif?>
                    </div>
                    <div class="b-elar_address">
                        <div class="b-elar_address_txt" itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address">
                            <div class="icon">
                                <div class="b-hint">
                                    <?=Loc::getMessage('ADDRESS')?>									</div>
                            </div>
                            <?=$arItem["ADRESS"]?><br><a href="#" class="b-elar_address_link" data-lat="<?=$pos[1]?>" data-lng="<?=$pos[0]?>"><?=Loc::getMessage('SHOW_ON_THE_MAP')?></a></div>


                    </div>
                </li>
            <?endforeach?>

        </ol>
        <section class="text-center"></section>
    </div>
</section>
<script src="//api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif?>
