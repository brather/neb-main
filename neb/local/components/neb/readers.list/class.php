<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 07.10.2016
 * Time: 16:00
 */

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\GroupTable,
    \Bitrix\Main\UserTable,
    \Bitrix\Iblock\ElementTable,
    \Bitrix\Highloadblock\HighloadBlockTable;

/**
 * Class ReadersList
 */
class ReadersList extends CBitrixComponent
{
    private $arFilter = [], $arWords = [], $arTickets = [];

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        if (empty($arParams['WORKPLACE_ID']))
            return false;

        $arRequest = static::getRequest();
        $arParams['READERS'] = [];
        foreach ($arRequest['READERS'] as $k => $v)
            $arParams['READERS'][trim(htmlspecialcharsbx($k))] = trim(htmlspecialcharsbx($v));

        $arParams['BY']    = trim(htmlspecialcharsEx($_REQUEST['by']));
        $arParams['ORDER'] = trim(htmlspecialcharsEx($_REQUEST['order']));
        if (empty($arParams['BY']) || empty($arParams['ORDER'])) {
            $arParams['BY']    = 'ID';
            $arParams['ORDER'] = 'DESC';
            $arParams['SCOPE'] = 'Y';
        }

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->_getLibs();
        $this->_getUserTickets();
        $this->_getFilter();
        $this->_getUsers();

        $this->includeComponentTemplate();
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public static function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Получает фильтр для пользователей.
     */
    private function _getFilter() {

        // Шаг 1: Собираем массив из слов
        $arWords = explode(' ', $this->arParams['READERS']['FIO']);
        foreach ($arWords as $key => $word) {
            $arWords[$key] = trim($word);
            if (mb_strlen($arWords[$key]) < 1)
                unset($arWords[$key]);
        }

        // Шаг 2: Получаем все возможные комбинации полей поиска
        $arSearch = [];
        $iCount = count($arWords);
        if ($iCount) {
            $searchFields = ['LAST_NAME', 'NAME', 'SECOND_NAME'];
            foreach ($searchFields as $v1) {
                if ($iCount < 2) {
                    $arSearch[] = [$v1];
                    continue;
                }
                foreach ($searchFields as $v2) {
                    if ($iCount < 3 && $v1 !== $v2) {
                        $arSearch[] = [$v1, $v2];
                        continue;
                    }
                    foreach ($searchFields as $v3)
                        if ($v1 !== $v2 && $v2 !== $v3 && $v1 !== $v3)
                            $arSearch[] = [$v1, $v2, $v3];
                }
            }
        }

        // Шаг 3: Формируем массив поиска по полям ФИО
        $logicSearch = ['LOGIC' => 'OR'];
        foreach ($arSearch as $arFields) {
            $orLogic = ['LOGIC' => 'AND',];
            foreach ($arFields as $iNumber => $sFields) {
                $orLogic[] = [[
                    'LOGIC'        => 'AND',
                    $sFields       => '%' . $arWords[$iNumber] . '%',
                    '!' . $sFields => null,
                ],];
            }
            $logicSearch[] = $orLogic;
        }

        // формирование самого фильтра
        $arFilter = ['ACTIVE' => 'Y'];
        $arFilter[] = ['LOGIC' => 'AND', $logicSearch];

        if (!empty($this->arParams['READERS']['ECHB']))
            $arFilter[] = [['UF_NUM_ECHB' => $this->arParams['READERS']['ECHB']  . '%']];

        if (!empty($this->arParams['READERS']['EMAIL']))
            $arFilter[] = [['EMAIL' => $this->arParams['READERS']['EMAIL'] . '%']];

        // фильтрация по статусам пользователей
        if ($this->arParams['READERS']['STATUS'] == 'Y') {
            $arFilter['ID'] = array_keys($this->arTickets);
        }
        elseif ($this->arParams['READERS']['STATUS'] == 'N') {
            $arFilter['!ID'] = array_merge(array_keys($this->arTickets), static::_getExcludedUsers());
        }
        else {
            $arFilter['!ID'] = static::_getExcludedUsers();
        }

        $this->arWords = $arWords;
        $this->arFilter = $arFilter;
    }

    /**
     * Получение массива исключаемых пользователей из определенных групп
     *
     * @return array
     */
    private static function _getExcludedUsers() {

        $arResult = [];

        $arGroups = [UGROUP_LIB_CODE_ADMIN, UGROUP_LIB_CODE_EDITOR, UGROUP_LIB_CODE_CONTROLLER, UGROUP_RIGHTHOLDER_CODE];

        $rsUserGroups = GroupTable::getList([
            'filter' => ['ACTIVE' => 'Y', 'STRING_ID' => $arGroups],
            'select' => ['ID'],
            'order'  => ['ID' => 'ASC']
        ]);
        while ($arFields = $rsUserGroups->fetch())
            $arExcludeGroups[] = $arFields['ID'];

        $rsUsers = UserTable::getList([
            'filter' => ["Bitrix\Main\UserGroupTable:USER.GROUP_ID" => $arExcludeGroups, 'ACTIVE' => 'Y'],
            'select' => ['ID'],
            'order'  => ['ID' => 'ASC'],
        ]);
        while ($arUser = $rsUsers->fetch())
            $arResult[] = $arUser['ID'];

        return $arResult;
    }

    private function _getLibs() {

        $arResult = [];

        Loader::includeModule("iblock");

        $rsElements = ElementTable::getList([
            'filter' => ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ACTIVE' => 'Y'],
            'order'  => ['NAME' => 'ASC'],
            'select' => ['ID', 'NAME']
        ]);
        while ($arFields = $rsElements->fetch())
            $arResult[$arFields['ID']] = $arFields;

        $this->arParams['LIBS'] = $arResult;
    }

    /**
     * Получает релевантность пользователя
     *
     * @param $arUser
     * @return float|int|mixed
     */
    private function _getUserScope($arUser) {

        // приоритет совпадений запроса с полями пользователя
        $scopeParams = ['LAST_NAME' => 4, 'NAME' => 2, 'SECOND_NAME' => 1];

        $scope = 0;
        if ($this->arParams['SCOPE'] == 'Y' && !empty($this->arWords)) {
            foreach ($this->arWords as $sWord) {
                foreach ($scopeParams as $paramName => $scopeValue) {

                    if (!isset($arUser[$paramName]) || false === strpos($arUser[$paramName], $sWord))
                        continue;

                    $scope += $scopeValue;
                    if ($arUser[$paramName] === $sWord)
                        $scope += $scopeValue / 2;
                }
            }
        }

        return $scope;
    }

    /**
     * Получение билетов пользователей
     */
    private function _getUserTickets() {

        $arResult = [];

        if ($this->arParams['WORKPLACE_ID'] <= 0 || !Loader::includeModule('highloadblock'))
            return $arResult;

        $arHIblock      = HighloadBlockTable::getList(['filter' => ['NAME' => 'UsersTickets']])->fetchRaw();
        $obUsersTickets = HighloadBlockTable::compileEntity($arHIblock)->getDataClass();

        $rsTickets = $obUsersTickets::getList([
            "order"  => ["UF_USER_ID" => "ASC"],
            "filter" => ['>=UF_DATE' => new DateTime(), 'UF_WCHZ_ID' => $this->arParams['WORKPLACE_ID']],
            'select' => ['ID', 'UF_USER_ID']
        ]);
        while ($arFields = $rsTickets->fetch())
            $arResult[$arFields['UF_USER_ID']] = $arFields['ID'];

        $this->arTickets = $arResult;
    }

    /**
     * Выборка пользователей и их сортировка
     */
    private function _getUsers() {

        $arResult = [];

        // получение списка пользователей
        $rsUsers = UserTable::getList([
            'filter' => $this->arFilter,
            'select' => ['ID', 'LAST_NAME', 'NAME', 'SECOND_NAME', 'DATE_REGISTER', 'UF_NUM_ECHB', 'UF_STATUS'],
            'order'  => [$this->arParams['BY'] => $this->arParams['ORDER']]
        ]);
        while ($arUser = $rsUsers->fetch()) {

            $arUser['DATE_REGISTER'] = strval($arUser['DATE_REGISTER']);

            if ($this->arTickets[$arUser['ID']])
                $arUser['UF_STATUS'] = 'Верифицированный';
            else
                $arUser['UF_STATUS'] = 'Не&nbsp;верифицированный';

            $arUser['SCOPE']     = $this->_getUserScope($arUser);
            $arResult['USERS'][] = $arUser;
        }

        // сортировка результата по релевантности
        if ($this->arParams['SCOPE'] == 'Y') {
            usort($arResult['USERS'], function ($a, $b) {
                return -($a['SCOPE'] - $b['SCOPE']);
            });
        }

        $this->arResult = $arResult;
    }
}