// var FRONT = FRONT || {};
// FRONT.assets = {};

$(function(){
    $(document).on('click', '[data-delete-order]', function(e){
        e.preventDefault();

        var toggler = $(this),
            orderId = toggler.data('delete-order'),
            url = window.location.href + '?id='+orderId+'&action=delete',
            message = {
                title: 'Удалить заказ?',
                text: 'Подтвердите удаление заказа',
                confirmTitle: "Удалить"
            },
            orderRow = toggler.closest('.lk-table__row');

        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed){
                $.ajax({
                    url: url,
                    data: {
                        action: 'delete',
                        id: orderId
                    },
                    method: 'GET',
                    beforeSend: function(){},
                    success: function(data){
                        //console.log('done');
                        $(orderRow).slideUp(function(){
                            this.remove();
                        });
                    }
                });
            }
        });
    });
    $(document).on('click','[data-show-scan-order-pay]',function(e){
        e.preventDefault();
        var modalMarkup = $('#universal-modal').clone(true).insertAfter('#universal-modal'),
            toggler = $(this);
        modalMarkup.removeAttr('id');

        $(modalMarkup)
        .one('show.bs.modal', function(e){
            var modal = $(this),
                button = $(e.relatedTarget),
                iframeSrc = button.data('href') || button.attr('href'),
                iframe = $('<iframe/>').attr('src',iframeSrc).css({
                    'height':'100%',
                    'width':'100%',
                    'border':'none',
                    // 'overflow':'hidden'
                });
            // console.log(  );

            modal
                .find('.modal-title').text('Оплата заказа')
                .end().find('.modal-dialog').addClass('fixed-modal modal-lg no-footer-bar')
                .end().find('.modal-footer').remove()
                .end().find('.modal-body').css('overflow','hidden').append(iframe);
        })
        .one('hidden.bs.modal', function(e){
            var modal = $(this);
            $(modalMarkup).detach();
        })
        .off('keydown')
        .on('keydown', function(event){
            if(event.keyCode == 13) {// enter on modal
                // event.preventDefault();            
                // $(btnAccept).click();
            }
            if(event.keyCode == 27) {//escape on modal
                // event.preventDefault();            
                // $(btnDefault).click();
            }
        })
        .modal('show', toggler);
    });
    $(document).on('click','[data-show-scan-order-details]',function(e){
        e.preventDefault();
        var modalMarkup = $('#universal-modal').clone(true).insertAfter('#universal-modal'),
            toggler = $(this);
        modalMarkup.removeAttr('id');

        $(modalMarkup)
        .one('show.bs.modal', function(e){
            var modal = $(this),
                button = $(e.relatedTarget),
                iframeSrc = button.data('href'),
                iframe = $('<iframe/>').attr('src',iframeSrc).css({
                    'height':'100%',
                    'width':'100%',
                    'border':'none',
                    // 'overflow':'hidden'
                });
            // console.log(  );

            modal
                .find('.modal-title').text('Просмотр заказа')
                .end().find('.modal-dialog').addClass('fixed-modal modal-lg no-footer-bar')
                .end().find('.modal-footer').remove()
                .end().find('.modal-body').css('overflow','hidden').append(iframe);
        })
        .one('hidden.bs.modal', function(e){
            var modal = $(this);
            $(modalMarkup).detach();
        })
        .off('keydown')
        .on('keydown', function(event){
            if(event.keyCode == 13) {// enter on modal
                // event.preventDefault();            
                // $(btnAccept).click();
            }
            if(event.keyCode == 27) {//escape on modal
                // event.preventDefault();            
                // $(btnDefault).click();
            }
        })
        .modal('show', toggler);

    });
});

function WidgetDialog(target){                                    
    this.target = target;
    this.Reload = false;
    this.VisibleSave = false;
    this.Execute = function(link){
                        var aSTR = '<div id="WD-ui-widget-overlay" class="ui-widget-overlay ui-front dark" onClick="visibleDialog(false,'+this.Reload+');"></div>'+
                                                '<div id="WD-ui-dialog" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front no-close ajaxpopup" tabindex="-1" role="dialog" aria-describedby="ui-id-10" aria-labelledby="ui-id-11" style="height: auto; width: 100%; top: 0px; left: 0px; display: block; background: rgb(255, 255, 255);">'+
                                                    '<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">'+
                                                        '<span id="ui-id-11" class="ui-dialog-title">&nbsp;</span>'+
                                                        '<button onClick="visibleDialog(false,'+this.Reload+');" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title="close">'+
                                                            '<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>'+
                                                            '<span class="ui-button-text">close</span>'+
                                                        '</button>'+
                                                    '</div>'+       
                                                    '<iframe src="'+link+'" id="ifr_popup" scrolling="yes" frameborder="0" style="width:100%;" onload="iframeLoad(this);"></iframe>';
                        if (this.VisibleSave){
                            aSTR = aSTR+'<div style="width:100%;height:50px;padding: 2px 8px;"><button onClick="visibleDialog(false,'+this.Reload+');" type="button" class="button_mode bottompopupclose btn btn-primary" role="button" href="/" style="position: absolute;bottom:10px;right:35px;">Сохранить</button></div>';  
                        };
                        aSTR = aSTR+'</div>';
                        $('#'+this.target).html(aSTR);
                        visibleDialog(true, this.Reload);
                    }
};
function visibleDialog(value, reload){
  if (value == true){
    $('#WD-ui-widget-overlay').show();
    $('#WD-ui-dialog').show(); 
  } else{
    $('#WD-ui-widget-overlay').hide();
    $('#WD-ui-dialog').hide();
    if (reload){location.reload()};
  };  
};
function iframeLoad(frame){
    (function($){
        $.fn.ZIAutoHeight = function() {
            var t = this, 
                doc = 'contentDocument' in t[0]? t[0].contentDocument : t[0].contentWindow.document;
            t.css('height', doc.body.scrollHeight+'px');
        }
    })(jQuery);
    $(frame).ZIAutoHeight();
};

function PreviewClick(athis){
    var aWD = new WidgetDialog('ui-WD');
    aWD.Execute($('#'+athis).attr('ahref'));
};

/* pay */

// $('body').on('click', 'a.payorderlinksubmit', function(){
//     $(this).closest('form').submit();
// });
// $('body').on('click', 'a.payorderlinkcancel', function(){
//     console.log('cancel');
//     $(this).closest('#panel-formYandexMoney').remove();
// });
// $('a.payorderlink').click(function(){
//     var href = $(this).attr("href");
//     var params = href.substr(href.indexOf("?")+1,href.length);
//     BX.showWait();
//     $.post(window.location.href, params, function(data){
//         if($('#panel-formYandexMoney').length > 0)
//         {
//             $('panel-formYandexMoney').replaceWith(data);
//         }
//         else
//         {
//             $('body').prepend(data);
//         }
//         BX.closeWait();
//     }, "html");
//     return false;
// });
