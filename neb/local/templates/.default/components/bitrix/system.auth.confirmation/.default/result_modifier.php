<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $USER, $APPLICATION;

use \Bitrix\Main\Loader,
    \Nota\UserData\rgb;

$iUser = intval($arResult['USER']['ID']);

if ($iUser > 0)
{
    $nebUser = new nebUser($iUser);

    /**
     * Удаление метки у пользвателя, добавленного оператором ЭЧЗ, подтвердившего свой e-mail
     */
    if ($arResult['USER']['XML_ID'] == 'workplace') {
        $nebUser->Update($iUser, ['XML_ID' => '']);
    }

    /**
     * Если кейс: "SUCCESSFUL_CONFIRMATION" - E06 - отправляем пользователю приветственное письмо
     * Повторный переход по этой ссылке приведёт к "READY_CONFIRM" - E03, так что повторной отправки опасаться не стоит
     */
    if ($arResult['MESSAGE_CODE'] == 'E06') {

        if (!empty($arResult['USER']['UF_PASSPORT_NUMBER']) || !empty($arResult['USER']['UF_RGB_USER_ID'])) {

            Loader::IncludeModule('nota.userdata');

            // Добавляем пользователя в РГБ
            $res = rgb::addUserBx($iUser);

            // добавить комментарий
            $role = $nebUser->getRole();
            if ($role == 'user')
                $nebUser->setEICHB();

            //LocalRedirect("/auth/ok.php");
        }

        // Отправим пользователю приветствие
        $nebUser->SendUserInfo($iUser, SITE_ID, "Приветствуем Вас как нового пользователя НЭБ!");

        // Авторизуем пользователя
        $nebUser->Authorize($iUser);

        $APPLICATION->RestartBuffer();

        LocalRedirect('/profile/confirm/');
        die();
    }
}