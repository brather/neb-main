<?php

namespace Neb\Main\Component;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Nota\Exalead\BooksAdd;

/**
 * Class CreateBooksComponent
 *
 * @package Neb\Main\Component
 */
abstract class CreateBooksComponent extends \CBitrixComponent
{
    /**
     * @var array
     */
    protected $_defaultParams = array();

    public function executeComponent()
    {
        try {
            $this->_createBooksRequest();
            if (isset($this->arParams['RESULT_URI'])) {
                LocalRedirect($this->arParams['RESULT_URI']);
            }

            return;
        } catch (\Exception $e) {
            ShowError($e->getMessage());
        }
    }

    /**
     * @param $params
     *
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params = array_filter($params);
        $params = array_merge($this->_defaultParams, $params);
        $this->_prepareRequestParams($params);

        return $params;
    }

    /**
     * @param $params
     */
    abstract protected function _prepareRequestParams(&$params);

    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * @throws \Exception
     */
    protected function _createBooksRequest()
    {
        if(!Loader::includeModule('nota.exalead')) {
            throw new \Exception('Module "nota.exalead" not installed!');
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (empty($this->arParams['books'])) {
                throw new \Exception(
                    Loc::getMessage(
                        'ERROR_PARAMETER_BOOKS'
                    )
                );
            }
            foreach ($this->arParams['books'] as $book) {
                $book['request_type'] = $this->arParams['REQUEST_TYPE'];
                $result = BooksAdd::applyAddBookRequest(
                    $book,
                    array_intersect_key($this->arParams, array(
                        'LIBRARY_ID' => true,
                        'LIBRARY_NAME' => true,
                    ))
                );
                $errors = $result->getErrorMessages();
                if (!empty($errors)) {
                    throw new \Exception(implode(PHP_EOL, $errors));
                }
            }
        } else {
            LocalRedirect($this->arParams['RESULT_URI']);
        }
    }

}