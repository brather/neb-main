
<?
$pagename='';
?>
<? include("header_main_auth.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>						
						<!--<select name="searchopt" id=""class="js_select b_searchopt">
							<option value="1">по всем полям</option>
							<option value="2">по дате</option>

						</select>-->						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox js_btnsub" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			
		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">
		</div> <!-- /.b-search -->

		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<div class="b-book_bg">
	<section class="innersection innerwrapper clearfix">

		<div class="b-bookpopup b-bookpage">
			<div class="b-bookpopup_in bbox">
				<div class="rel clearfix"><a href="#" class="bookclose right">Закрыть окно</a></div> <!--closepage  -->

				<div class="b-onebookinfo iblock">
					<div class="b-bookhover">
						<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
						<span class="b-autor"><a href="#" class="lite">Станислав Лем</a></span>
						<div class="b-bookhover_tit black">Сумма технологии</div>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>
					</div>		
					<div class="clearfix">
						<button type="submit" value="1" class="formbutton left">Читать</button>	
						<div class="b-result-type right">
							<span class="b-result-type_txt">pdf</span>
							<span class="b-result-type_txt">текст</span>
						</div>	
					</div>		
					 <script type="text/javascript">
     
				
			$(function() {
				$('.b-onebookinfo .formbutton').click(function(){
					$('.popup_autoreg_form').autoregPopup();
					return false;
				});
			});

			(function() { /*create closure*/
				$.fn.autoregPopup = function() { /* попап регистрации */

					this.each(function() {
						var popup = $(this).clone(true);
							popup.removeClass('hidden');
							popup.dialog({
								closeOnEscape: true,	                   
								modal: true,
								draggable: false,
								resizable: false,             
								width: ($(window).width() > 480)? 560:445,
								dialogClass: 'autoreg_popup',
								position: "center",
								open: function(){
									$('.ui-widget-overlay').addClass('black');
									popup.find('.closepopup').click(function() {				
																	/*Close the dialog*/
																	popup.dialog("close").remove();
																	$('.ui-widget-overlay').remove();
																	return false;
																});  
								},
								close: function() {
									popup.dialog("close").remove();
								}
							});

							$('.ui-widget-overlay').click(function() {				
								/*Close the dialog*/
								popup.dialog("close");
							});   

					});
				}
				/*end of closure*/
			})(jQuery);

    </script>
					<div class="rel">
						<ul class="b-resultbook-info">
							<li><span>Количество страниц: </span> 427 </li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<div class="b-infobox b-descrinfo">						
							<div class="b-infoboxitem">
								<span class="tit iblock">Автор: </span>
								<span class="iblock val">Горшков, Евгений Андреевич</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Место издания: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">ISBN:</span>
								<span class="iblock val">2-266-11156-6</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Язык: </span>
								<span class="iblock val">русский</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тираж: </span>
								<span class="iblock val">427</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Номер тома: </span>
								<span class="iblock val">1</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Серия: </span>
								<span class="iblock val">123</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие тома: </span>
								<span class="iblock val">Сумма технологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Неконтролируемое имя: </span>
								<span class="iblock val">Институт психологии Российской академии наук</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие: </span>
								<span class="iblock val">Становление социальной психологии США : автореферат дис. ... кандидата психологических наук : 19.00.01</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Выходные данные: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Физическое описание: </span>
								<span class="iblock val">23 с.</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">Общая психология, психология личности, история психологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">История психологии -- История психологии в Соединенных Штатах Америки (США) -- История психологии в новейшее время -- Психологические направления</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Ключевые слова: </span>
								<span class="iblock val">социальная психология</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Хранение:</span>
								<span class="iblock val">9 11-3/12;</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Электронный адрес:</span>
								<span class="iblock val">Электронный ресурс</span>
							</div>							
						</div><!-- /b-infobox -->
					</div>
					<div class="b-line_social">
						<h5>Поделиться с друзьями</h5>
						<a href="#" class="b-login_slnk vklink iblock">вконтакте</a>
						<a href="#" class="b-login_slnk odnlink iblock">одноклассники</a>
						<a href="#" class="b-login_slnk fblink iblock">facebook</a>
					</div><!-- /.b-login_social -->
					<div class="b-lineinfo">
						<h5>заявка на переоцифровку некачественной книги</h5>
						<a href="ajax_addcomment.php" data-posat="left top" data-posmy="left+288 top-270" data-width="375"  class="formbtn grad popup_opener ajax_opener noclose">Заявка на оцифровку</a>
					</div>
				</div>
				
				
				<div class="b-addbook_popuptit clearfix">					
					<h2>Входит в подборки</h2>
				</div>
				<div class="js_threeslide selected_slider">
					<div class="slide">
						<div class="b-favside_img js_flexbackground">
							<img src="./pic/pic_32.png"  data-bgposition="50% 0" class="js_flex_bgimage" alt="">
							<img src="./pic/pic_47.jpg" class="real " alt="">
						</div>
						<h2>Серебряный век: быт, нравы, моды, знаменитости</h2>
						<a href="#">15 книг в подборке </a>
					</div>
					<div class="slide">
						<div class="b-favside_img js_flexbackground">
							<img src="./pic/pic_32.png"  data-bgposition="50% 0" class="js_flex_bgimage" alt="">
							<img src="./pic/pic_47.jpg" class="real " alt="">
						</div>
						<h2>Серебряный век: быт, нравы, моды, знаменитости</h2>
						<a href="#">15 книг в подборке </a>
					</div>
					
				</div>
				<div class="b-addbook_popuptit clearfix">					
					<h2>похожие издания</h2>
				
				</div>
				<table class="b-usertable tsize">
						<tr>
							<th><span>Автор</span></th>
							<th><span>Название</span></th>
							<th><span>Дата публикации</span></th>
							<th><span>Библиотека</span></th>
							<th><span>Читать</span></th>
						</tr>
						<tr>
							<td class="pl15">Станислав Лем</td>
							<td class="pl15"><a href="#">Сумма технологии</a></td>
							<td class="pl15">2011</td>
							<td>Рязанская областная библиотека</td>
							<td><button type="submit" value="1" class="formbutton">Читать</button>	</td>
						</tr>
						<tr>
							<td class="pl15">Станислав Лем</td>
							<td class="pl15"><a class="popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Сумма технологии</a></td>
							<td class="pl15">2011</td>
							<td>Рязанская областная библиотека</td>
							<td><button type="submit" value="1" class="formbutton">Читать</button>	</td>
						</tr>
						<tr>
							<td class="pl15">Станислав Лем</td>
							<td class="pl15"><a class="popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Сумма технологии</a></td>
							<td class="pl15">2011</td>
							<td>Рязанская областная библиотека</td>
							<td><button type="submit" value="1" class="formbutton">Читать</button>	</td>
						</tr>
					</table>
					<div class="b-paging">
					<div class="b-paging_cnt"><a class="b-paging_prev iblock" onclick="return false;" href=""></a><a class="b-paging_num current iblock" onclick="return false;" href="#" target="_parent">1</a><a href="/profile/plan_digitization/?by1=UF_DATE_ADD&amp;order1=desc&amp;PAGEN_1=2" class="b-paging_num iblock" target="_parent">2</a><a href="/profile/plan_digitization/?by1=UF_DATE_ADD&amp;order1=desc&amp;PAGEN_1=2" class="b-paging_next iblock" target="_parent"></a></div>
				</div>
				<!--<div class="book_slider">
					<div  class="slide">
						<a href="#"><img alt="" class="loadingimg" src="./pic/pic_35.jpg"></a>
						<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
					</div>
						<div class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_36.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
						</div>
						<div class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_37.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
						</div>
						<div  class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_38.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
						</div>
						<div class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_35.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
						</div>
						<div class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_40.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
						</div>
				</div>-->
				<div class="b-addbook_popuptit clearfix">					
					<h2>Экземпляры</h2>
				</div>
					<table class="b-usertable tsize">
						<tr>
							<th><span>Автор</span></th>
							<th><span>Название</span></th>
							<th><span>Дата публикации</span></th>
							<th><span>Библиотека</span></th>
							<th><span>Читать</span></th>
						</tr>
						<tr>
							<td class="pl15">Станислав Лем</td>
							<td class="pl15"><a href="#">Сумма технологии</a></td>
							<td class="pl15">2011</td>
							<td>Рязанская областная библиотека</td>
							<td><button type="submit" value="1" class="formbutton">Читать</button>	</td>
						</tr>
						<tr>
							<td class="pl15">Станислав Лем</td>
							<td class="pl15"><a class="popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Сумма технологии</a></td>
							<td class="pl15">2011</td>
							<td>Рязанская областная библиотека</td>
							<td><button type="submit" value="1" class="formbutton">Читать</button>	</td>
						</tr>
					</table>
			</div>

		</div> 	<!-- /.bookpopup -->

		

	</section>
</div>
</div><!-- /.homepage -->

<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu">мои подборки</a>
	

	<form class="b-selectionadd" action="">
		<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
		<span class="b-selectionaddtxt">Cоздать подборку</span>
		<input type="text" class="input hidden">
	</form>
</div><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
<!--popup Удалить из подборки--><div class="b-removepopup hidden"><p>Удалить книгу из Моей библиотеки?</p><a href="#" class="formbutton btremove">Удалить</a><a href="#" class="formbutton gray">Оставить</a></div><!--/popup Удалить из подборки-->
<!--popup доступ к закрытому изданию-->
<div class="popup_autoreg_form hidden">
	<a href="#" class="closepopup">Закрыть</a>
	
	<div class="ok tcenter" style="display:none;">
		<p>Вы успешно зарегистрированы как читатель РГБ</p>
		<p><button type="submit" value="1" class="formbutton">Читать</button></p>
	</div>
	
	<form action="" class="b-form b-formautoreg">
	<p>Доступ к закрытому изданию возможен только для читателей РГБ. Если Вы являетесь читателем РГБ, то введите логин и пароль РГБ. Иначе пройдите полную регистрацию.</p>
									<div class="fieldrow nowrap">
										<div class="fieldcell iblock">
											<label for="settings11">Логин</label>
											<div class="field validate">
												<input type="text"  value="" id="settings11"  name="login" class="input">										
											</div>
										</div>
											<a href="#" class="formbutton iblock">Помощь</a>	

									</div>
									<div class="fieldrow nowrap">

										<div class="fieldcell iblock">
											<label for="settings10">Пароль</label>
											<div class="field validate">
												<input type="password"  value="" id="settings10" name="pass" class="input">									
											</div>
										</div>
											<a href="#" class="formbutton iblock">Зарегистрироваться</a>	
										
									</div>
									<em class="error">Неверный логин или пароль</em>
									<div class="fieldrow nowrap fieldrowaction">
										<div class="fieldcell ">
											<div class="field clearfix">
												<button class="formbutton left" value="1" type="submit">Войти</button>
											</div>
										</div>
									</div>
</form>
</div><!-- /.popup_autoreg_form -->
<!--/popup доступ к закрытому изданию-->
</body>
</html>