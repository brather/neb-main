<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER;
if (!$USER->IsAuthorized())
    $APPLICATION->AuthForm("У вас нет права доступа к данному файлу.");

$arResult['name'] = htmlspecialcharsbx($_REQUEST['TITLE']);
$arResult['data'] = array();

// TODO: добавить проверку книги на принадлежность к своим активным договорам

CModule::IncludeModule("nota.exalead");

if($_REQUEST['ACTION'] == 'VIEW')
    $table = 'neb_stat_book_view'; // количество обращений
else
    $table = 'neb_stat_book_read'; // количество читателей

$sQuery = "SELECT YEAR(X_TIMESTAMP) as YID, MONTH(X_TIMESTAMP) as MID, COUNT(ID) as CNT FROM " . $table
    . " WHERE BOOK_ID='".htmlspecialcharsbx($_REQUEST['BOOK_ID'])."' GROUP BY YEAR(X_TIMESTAMP), MONTH(X_TIMESTAMP)";

$rsBook = \Bitrix\Main\Application::getConnection()->query($sQuery);
while($arItem = $rsBook->Fetch()) {

    if (empty($arResult['data'][$arItem['YID']]))
        $arResult['data'][$arItem['YID']] = array_fill(0, 12, 0);

    $arResult['data'][$arItem['YID']][$arItem['MID']] = intval($arItem['CNT']);
}

\Neb\Main\Helper\MainHelper::showJson($arResult);