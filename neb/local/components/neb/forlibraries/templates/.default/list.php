<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

global $APPLICATION;
$APPLICATION->SetTitle('Информация для участников НЭБ');

?>

<? if (count($arResult['SECTIONS']) > 1): ?>
	<div>
		<? foreach ($arResult['SECTIONS'] as $arItem): ?>
			<a title="<?=$arItem['NAME']?>" href="<?=$arItem['SECTION_PAGE_URL']?>"><?=$arItem['NAME']?></a>&nbsp;&nbsp;&nbsp;
		<? endforeach; ?>
	</div>
<? endif; ?>

<? if(!empty($arResult['ELEMENTS'])): ?>
	<br />
	<section class="main-news">
		<div class="row">
			<div class="main-news__gallery main-news__gallery--general">
				<? $i=0; foreach($arResult['ELEMENTS'] as $arItem): ?>
					<div class="col-md-4 col-sm-6 col-xs-6 main-news__gallery-item" <? if($i % 3 == 0) :?>style="clear:both;"<?endif;?>>
                        <a href="<?= $arItem['DETAIL_PAGE_URL']?>"><?= $arItem['NAME']?></a>
                        <a href="<?= $arItem['DETAIL_PAGE_URL']?>" >
                            <img src="<?= $arItem["PREVIEW_PICTURE"]['PATH'];  ?>" alt="<?= $arItem['NAME']?>" class="main-news__gallery-image">
						</a>
						<div class="main-news-item__section-name"><?=$arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['NAME']?></div>

                        <p><?= $arItem['PREVIEW_TEXT']?></p>
					</div>
				<? $i++;endforeach;?>
			</div>
		</div>
	</section>
<? endif; ?>

<?
$APPLICATION->IncludeComponent(
	"bitrix:main.pagenavigation",
	"",
	array(
		"NAV_OBJECT" => $arResult['NAV'],
		"SEF_MODE" => "Y",
	),
	false
);