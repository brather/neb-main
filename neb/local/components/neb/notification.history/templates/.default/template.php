<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>

<? if (empty($arResult['LIBRARY'])): ?>
    <p>
        <a href="/profile/notification/">Отправка</a> | <span data-href="/profile/notification/history/">История</span>
    </p>
<? endif; ?>

<form data-togglable-widget class="expanded search-with-ext" action="/profile/notification/history/" method="get">

    <div class="main-search-string" data-dt="search">
        <div class="main-search-string-row" data-main-string>
            <div class="string-field">
                <input placeholder="Поиск по заявкам" class="form-control input-lg"
                       name="query" value="<?=$_REQUEST['query']?>" />
                <a class="ext-search-expand" title="Расширенныей поиск" data-extsearch-toggle></a>
            </div>
            <div class="string-submit">
                <input type="submit" value="Найти" class="btn btn-lg btn-primary"/>
            </div>
        </div>
    </div>

    <div class="ext-search" data-dd="search">
        <button type="button" class="close" data-extsearch-toggle aria-label="Close"><span aria-hidden="true">×</span></button>
        <dl class="form-dl">

            <dd class="fullwidth">
                <? if (!empty($arResult['LIBRARIES'])): ?>
                    <select name="library[]" class="form-control" multiple="true" title="Все библиотеки" id="libraries">
                        <? foreach ($arResult['LIBRARIES'] as $arItem): ?>
                            <option value="<?=$arItem['EMAIL']?>" <? if (in_array($arItem['EMAIL'], $_REQUEST['library'])): ?>
                                selected="selected"<? endif; ?>><?=$arItem['NAME']?></option>
                        <? endforeach; ?>
                    </select>
                <? endif; ?>
            </dd>

            <dt>
                <label for="posting_id">Номер заявки</label>
            </dt>
            <dd>
                <input type="text" class="form-control" id="posting_id" name="number" value="<?=$_REQUEST['number']?>" />
            </dd>
            
            <dt>
                <label for="date_send_from">Дата отправки от</label>
            </dt>
            <dd>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="date_send_from" size="10" value="<?=$_REQUEST['date_send_from']?>"
                               id="date_send_from" class="form-control">
                        <span class="input-group-btn">
                            <a id="calendarlabel" class="btn btn-default"
                               onclick="BX.calendar({node: 'date_send_from', field: 'date_send_from', form: '', bTime: false, value: ''});">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </a>
                        </span>
                    </div>
                </div>
            </dd>


            <dt>
                <label for="date_send_to"> до </label>
            </dt>
            <dd>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="date_send_to" size="10" value="<?=$_REQUEST['date_send_to']?>"
                               id="date_send_to" class="form-control">
                        <span class="input-group-btn">
                            <a id="calendarlabel" class="btn btn-default"
                               onclick="BX.calendar({node: 'date_send_to', field: 'date_send_to', form: '', bTime: false, value: ''});">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </a>
                        </span>
                    </div>
                </div>
            </dd>

            <dt>
                <label for="sender_name">ФИО отправителя</label>
            </dt>
            <dd>
                <select name="sender" class="form-control" title="Все отправители" id="senders">
                    <option value="">Все отправители</option>
                    <? foreach ($arResult['SENDERS'] as $sEmail => $sName): ?>
                        <option value="<?=$sEmail?>" <? if ($sEmail == $_REQUEST['sender']): ?>
                            selected="selected"<? endif; ?>><?=$sName?></option>
                    <? endforeach; ?>
                </select>
            </dd>
        </dl>
        <div class="ext-buttons">
            <input type="submit" value="Найти" class="btn btn-lg btn-primary"/>
        </div>
    </div>
</form>

<table class="table table-striped table-hover">
    <tr>
        <th>Id</th>
        <th>Дата отправки</th>
        <th>Письмо</th>
        <th>Отправитель</th>
        <? if (empty($arResult['LIBRARY'])): ?>
            <th>Получатели</th>
        <? endif; ?>
    </tr>
    <? foreach ($arResult['ITEMS'] as $arItem): ?>
        <tr>
            <td><?=$arItem['ID']?></td>
            <td>
                <? if (!empty($arItem['DATE_SENT'])): ?>
                    <?=$arItem['DATE_SENT']?>
                <? else: ?>
                    <input value="Отправить" class="btn btn-primary send-subscribe" data-id="<?=$arItem['ID']?>" />
                <? endif; ?>
            </td>
            <td>
                <?=strip_tags(html_entity_decode($arItem['BODY']))?>
                <? if (!empty($arItem['FILES'])): ?>
                    <i style="font-size: 12px"><br />
                        <? foreach ($arItem['FILES'] as $iFile => $arFile): ?>
                            <a href="<?=$arFile['SRC']?>"><?=$arFile['FILE_NAME']?></a>&nbsp;(<?=CFile::FormatSize($arFile['FILE_SIZE'])?>) <br />
                        <? endforeach; ?>
                    </i>
                <? endif; ?>
            </td>
            <td><?=$arResult['SENDERS'][$arItem['FROM_FIELD']]?:$arItem['FROM_FIELD']?></td>
            <? if (empty($arResult['LIBRARY'])): ?>
                <td>
                    <? foreach ($arItem['EMAIL'] as $i => $sEmail):
                        if ($i > 0) echo ','; ?>
                        <a href="mailto:<?=$sEmail?>"><?=$sEmail?></a><?
                    endforeach; ?>
                </td>
            <? endif; ?>
        </tr>
    <? endforeach; ?>
</table>

<a href="?export=Y&<?=DeleteParam(array("export"))?>">
    <img src="/local/templates/adaptive/img/file-icons/excel.png" />
</a>

<?
$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $arResult['NAV'],
        "SEF_MODE" => "Y",
    ),
    false
);