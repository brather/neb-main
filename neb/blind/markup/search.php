<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? $APPLICATION->SetTitle(""); ?>

<div class="container v-padding">
    <div class="button-bar" role="toolbar">
    	<a href="<?=$APPLICATION->GetCurDir()?>" class="btn btn-default"  tabindex="6">Вернуться на главную</a>
    </div>
    <form>
	    <dl class="form-dl">
	    	<dt><label class="for-lg" for="searchrub" aria-hidden="true" id="searchrublabel">Раздел для поиска</label></dt>
	    	<dd>
	    		<select class="form-control input-lg" id="searchrub" tabindex="2" aria-labelledby="searchrublabel">
	    			<option>Библиотеки</option>
	    			<option>Музеи</option>
	    			<option>Архивы</option>
	    		</select>
	    	</dd>
	    	<dt><label class="for-lg" for="searchtype" aria-hidden="true" id="searchtypelabel">Вид поиска</label></dt>
	    	<dd>
	    		<select class="form-control input-lg" id="searchtype" tabindex="3" aria-labelledby="searchtypelabel">
	    			<option>Одной строкой</option>
	    			<option>Расширенный поиск</option>
	    		</select>
	    	</dd>
	    	<dt><label for="requeststring" class="for-lg" aria-hidden="true" id="requeststringlabel">Введите запрос</label></dt>
	    	<dd>
	    		<input type="text" class="form-control input-lg" id="requeststring" tabindex="1" title="" value="" aria-labelledby="requeststringlabel"/>
	    	</dd>
	    </dl>
	    <div class="button-bar" role="toolbar">
	    	<button class="btn btn-primary" tabindex="4">Найти</button><br/>
	    	<button class="btn btn-default" type="reset"  tabindex="5">Очистить поиск</button>
	    </div>
	</form>
</div>
<script>
	$(function(){
		$('#requeststring').focus();
	});
</script>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>