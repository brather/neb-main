<?php
/**
 * User: agolodkov
 * Date: 02.09.2016
 * Time: 18:33
 */

namespace LbcBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('templating.engine.php');
        if($container->getParameter('kernel.debug')) {
            $definition->setClass('LbcBundle\DependencyInjection\Templating\DebugPhpEngine');
        } else {
            $definition->setClass('LbcBundle\DependencyInjection\Templating\LbcPhpEngine');
        }
    }

}