$(function(){
    //region Задание основных переменных
    var animateSpeed = 200,
        cls = {
            infoTooltip     : '.info-ps',
            statusSelect    : '.js-plan-status-list',
            duplicationRow  : 'pl-duplication',
            showButton      : '.js-toggle-show',
            active          : 'active',
            listPlan        : '.table-plan-list'
        };
    //endregion
    //region Запуск всплывающие подсказок
    $(cls.infoTooltip).tooltip();
    //endregion
    //region Переключение между показом дублей и всех планов
    $(cls.showButton).on('click', function(e){
        var $this = $(this),
            containBlock = $this.closest('div'),
            show = $this.data('show');
        if(!$this.hasClass(cls.active)) {
            containBlock.find('.' + cls.active).removeClass(cls.active);
            $this.addClass(cls.active);
            startLoader('body');
            window.location.replace($this.data('href'));
        }
    });
    //endregion
    //region Отправка ajax запроса при изменении статуса плана
    $(cls.statusSelect).on('change', function(e){
        e.preventDefault();
        var $this = $(this),
            send = {
                DATA_PLAN_UPDATE_AJAX   : 'Y',
                DATA_PLAN_ID            : $this.data('plan-id'),
                DATA_PLAN_STATUS        : $this.val(),
                DATA_BOOK_STATUS        : $('#ps-' + $this.val()).val(),
                SET_STATUS_BOOK         : 'Y'
            },
            toggler = $(e.target),
            message = {
                title           : $this.data('confirm-title'),
                text            : $this.data('confirm-question'),
                confirmTitle    : $this.data('confirm-request')
            };
        if($this.closest('tr').hasClass(cls.duplicationRow)) {
            $.when(FRONT.confirm.handle(message, toggler)).then(function(confirmed) {
                if(confirmed) {
                    setUpdateStatusPlan();
                }
            });
        } else {
            setUpdateStatusPlan();
        }

        function setUpdateStatusPlan() {
            startLoader('body');
            $.ajax({
                type : 'post',
                data : send,
                url  : $this.data('ajax-url'),
                success : function() {
                    stopLoader();
                }
            });
        }
    });
    //endregion
});
//region Функция запуска лоадера
function startLoader(block) {
    stopLoader();
    var blkLoader = '<div id="blk-loader" class="blk-milk-shadow"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
    $(block).append(blkLoader);
}
//endregion
//region Функция остановки лоадера
function stopLoader() {
    $('#blk-loader').animate({ 'opacity' : 0 }, 300, function(){
        $(this).remove();
    });
}
//endregion