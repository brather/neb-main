/* удаление закладки */
$(function(){
    $(document).on('click','[data-delete-faorite-edition]',function(e){
        var toggler = $(e.target),
            editionItem = toggler.closest('[data-favorite-item]'),
            editionId = toggler.data('id'),
            message = {
                title: toggler.data('message-title') || 'Удалить издание?',
                text: toggler.data('message-text') || "Вы действительно хотите удалить издание из списка избранных изданий?"
            };
        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed){
                $.ajax({
                    url: window.location.href,
                    data: {
                        action: 'remove',
                        id: editionId
                    },
                    method: 'GET',
                    beforeSend: function(){
                        editionItem.addClass('deletion-progress');
                    },
                    success: function(data){
                        $(editionItem).slideUp(function(){
                            this.remove();
                        });
                    }
                });
            }
        });
    });
});