<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__DIR__.'/template.php');
?>

<?//$APPLICATION->SetAdditionalCSS("/local/templates/rgb/css/styles.css");?> 
<?//$APPLICATION->AddHeadScript('/local/templates/rgb/js/jquery-1.11.1.min.js');?>
<?//$APPLICATION->AddHeadScript('/local/templates/rgb/js/jquery-migrate-1.2.1.min.js');?>
<?//$APPLICATION->AddHeadScript('/local/templates/rgb/js/jquery.bxslider.min.js');?>

<section class="innersection innerwrapper searchempty clearfix qwqwqwq">
    <div data-template="<?=$APPLICATION->ShowProperty('view_template_index');?>"></div>
    <div data-theme="<?=$APPLICATION->ShowProperty('view_theme_index');?>"></div>
    <div class="menu__wrap cf">
        <div class="menu__logo"></div>
        <div class="prim-nav">
            <ul>
                <li><a href="#">РГБ</a></li>
                <li><a href="#">О библиотеке</a></li>
                <li><a href="#">Фонды</a></li>
                <li><a href="#">Коллекции</a></li>
            </ul>
        </div>
    </div>
    <div class="slider-container">
        <div class="slider-wrapper">
            <div class="slider-outer">
                <div class="slider cf js-slider">
                    <div class="s__item">
                        <div class="s__item__text">Современная библиотека <br/>
                            в центре Москвы
                        </div>
                        <img src="/local/templates/rgb/uploads/slider/1.jpg" alt=""/>
                    </div>
                    <div class="s__item">
                        <div class="s__item__text">Современная библиотека <br/>
                            в центре Москвы
                        </div>
                        <img src="/local/templates/rgb/uploads/slider/1.jpg" alt=""/>
                    </div>
                    <div class="s__item">
                        <div class="s__item__text">Современная библиотека <br/>
                            в центре Москвы
                        </div>
                        <img src="/local/templates/rgb/uploads/slider/1.jpg" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="description">
        <div class="description__text">
            Российская государственная библиотека предлагает
            читателям самый большой выбор беллетристики и научной литературы.
            <br>В фонде: <strong >50202</strong> <u>изданий</u> и
            <strong>217</strong>
            <u>коллекций</u>
        </div>
    </div>
    <div class="contacts cf">
        <div class="contacts__left-wrap">
            <div class="contacts__left">
                <span class="contacts__text contacts__text_where">Не знаете, как  пройти в библиотеку?</span>
                <a class="contacts__button" href="#">Посмотреть на карте</a>
                <a class="contacts__button" href="#">Заказать доставку книги</a>
            </div>
        </div>
        <div class="contacts__right-wrap">
            <div class="contacts__right">
                <span class="contacts__text contacts__text_what">Не знаете, что  выбрать почитать?</span>
                <a class="contacts__button" href="#">Посмотреть каталог</a>
                <a class="contacts__button" href="#">Уточнить у специалиста</a>
            </div>
        </div>
    </div>
    <div class="content cf">
        <h1 class="catalog-title">Каталог библиотеки</h1>
        <div class="prim-content">
            <div class="catalog">
                <div class="catalog__item">
                    <a class="catalog__item__link" href="#">
                        <div class="catalog__item__image">
                            <img src="/local/templates/rgb/uploads/catalog-photo-1.jpg">
                        </div>
                        <span class="">Jamie's Comfort Food</span>
                    </a>
                <span class="catalog__item-description">Автор:
                    <a class="catalog__item-author" href="#">Джейми Оливер</a>
                    <span class="catalog__item-views">x
                        <span class="catalog__item-count">15 000</span>
                    </span>
                </span>
                </div>
                <div class="catalog__item">
                    <a class="catalog__item__link" href="#">
                        <div class="catalog__item__image">
                            <img src="/local/templates/rgb/uploads/catalog-photo-2.jpg">
                        </div>
                        <span class="">Шелкопряд</span>
                    </a>
                <span class="catalog__item-description">Автор:
                    <a class="catalog__item-author" href="#">Джейми Оливер</a>
                    <span class="catalog__item-views">x
                        <span class="catalog__item-count">15 000</span>
                    </span>
                </span>
                </div>
                <div class="catalog__item">
                    <a class="catalog__item__link" href="#">
                        <div class="catalog__item__image">
                            <img src="/local/templates/rgb/uploads/catalog-photo-3.jpg">
                        </div>
                        <span class="">Гибель Богов-2. Первая дилогия</span>
                    </a>
                <span class="catalog__item-description">Автор:
                    <a class="catalog__item-author" href="#">Джейми Оливер</a>
                    <span class="catalog__item-views">x
                        <span class="catalog__item-count">15 000</span>
                    </span>
                </span>
                </div>
                <div class="catalog__item">
                    <a class="catalog__item__link" href="#">
                        <div class="catalog__item__image">
                            <img src="/local/templates/rgb/uploads/catalog-photo-1.jpg">
                        </div>
                        <span class="">Jamie's Comfort Food</span>
                    </a>
                <span class="catalog__item-description">Автор:
                    <a class="catalog__item-author" href="#">Джейми Оливер</a>
                    <span class="catalog__item-views">x
                        <span class="catalog__item-count">15 000</span>
                    </span>
                </span>
                </div>
                <div class="catalog__item">
                    <a class="catalog__item__link" href="#">
                        <div class="catalog__item__image">
                            <img src="/local/templates/rgb/uploads/catalog-photo-2.jpg">
                        </div>
                        <span class="">Шелкопряд</span>
                    </a>
                <span class="catalog__item-description">Автор:
                    <a class="catalog__item-author" href="#">Джейми Оливер</a>
                    <span class="catalog__item-views">x
                        <span class="catalog__item-count">15 000</span>
                    </span>
                </span>
                </div>
                <div class="catalog__item">
                    <a class="catalog__item__link" href="#">
                        <div class="catalog__item__image">
                            <img src="/local/templates/rgb/uploads/catalog-photo-3.jpg">
                        </div>
                        <span class="">Гибель Богов-2. Первая дилогия</span>
                    </a>
                <span class="catalog__item-description">Автор:
                    <a class="catalog__item-author" href="#">Джейми Оливер</a>
                    <span class="catalog__item-views">x
                        <span class="catalog__item-count">15 000</span>
                    </span>
                </span>
                </div>
                <div class="catalog__item">
                    <a class="catalog__item__link" href="#">
                        <div class="catalog__item__image">
                            <img src="/local/templates/rgb/uploads/catalog-photo-1.jpg">
                        </div>
                        <span class="">Jamie's Comfort Food</span>
                    </a>
                <span class="catalog__item-description">Автор:
                    <a class="catalog__item-author" href="#">Джейми Оливер</a>
                    <span class="catalog__item-views">x
                        <span class="catalog__item-count">15 000</span>
                    </span>
                </span>
                </div>
                <div class="catalog__item">
                    <a class="catalog__item__link" href="#">
                        <div class="catalog__item__image">
                            <img src="/local/templates/rgb/uploads/catalog-photo-2.jpg">
                        </div>
                        <span class="">Шелкопряд</span>
                    </a>
                <span class="catalog__item-description">Автор:
                    <a class="catalog__item-author" href="#">Джейми Оливер</a>
                    <span class="catalog__item-views">x
                        <span class="catalog__item-count">15 000</span>
                    </span>
                </span>
                </div>
                <div class="catalog__item">
                    <a class="catalog__item__link" href="#">
                        <div class="catalog__item__image">
                            <img src="/local/templates/rgb/uploads/catalog-photo-3.jpg">
                        </div>
                        <span class="">Гибель Богов-2. Первая дилогия</span>
                    </a>
                <span class="catalog__item-description">Автор:
                    <a class="catalog__item-author" href="#">Джейми Оливер</a>
                    <span class="catalog__item-views">x
                        <span class="catalog__item-count">15 000</span>
                    </span>
                </span>
                </div>
                <div class="pagination">
                    <a class="pagination-item pagination-item_prev" href="#"></a>
                    <a class="pagination-item pagination-item_active" href="#">1</a>
                    <a class="pagination-item" href="#">2</a>
                    <a class="pagination-item" href="#">3</a>
                    <a class="pagination-item" href="#">4</a>
                    <a class="pagination-item" href="#">5</a>
                    <a class="pagination-item pagination-item_next" href="#"></a>
                </div>
            </div>
        </div>
        <div class="sec-content">
            <div class="news">
                <span class="news__title">Новости библиотеки</span>
                <ul class="news__list">
                    <li class="news__item">
                        <a class="news__item-link" href="#">2015 год - объявлен годом литературы в России
                            <br>
                            <span class="news__date">17.06.2014</span>
                        </a>
                    </li>
                    <li class="news__item">
                        <a class="news__item-link" href="#">Голубом выставочном зале РГБ открыта выставка «Первая мировая
                            <br>
                            <span class="news__date">17.06.2014</span>
                        </a>
                    </li>
                    <li class="news__item">
                        <a class="news__item-link" href="#">2015 год - объявлен годом литературы в России
                            <br>
                            <span class="news__date">17.06.2014</span>
                        </a>
                    </li>
                    <li class="news__item">
                        <a class="news__item-link" href="#">Голубом выставочном зале РГБ открыта выставка «Первая мировая
                            <br>
                            <span class="news__date">17.06.2014</span>
                        </a>
                    </li>
                    <li class="news__item">
                        <a class="news__item-link" href="#">2015 год - объявлен годом литературы в России
                            <br>
                            <span class="news__date">17.06.2014</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="address">
        <div class="address__content">
            <div class="address__row">
                <div class="address__title">Адрес:
                    <div class="address__value">Москва, ул. Воздвиженка, 3/5</div>
                </div>
                <div class="address__title">Электронная почта:
                    <a href="mailto:info@rsl.ru" class="address__email">info@rsl.ru</a>
                </div>
            </div>
            <div class="address__row">
                <div class="address__title">График работы:
                    <div class="address__value">Ежедневно с 10:00 до 18:00</div>
                </div>
                <div class="address__title">Телефоны:
                    <a class="address__value address_phone" href="tel:+78001005790">+7 (800) 100-57-90</a>
                    <a class="address__value address_phone" href="tel:+74995570470">+7 (499) 557-04-70</a>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/local/templates/rgb/js/common.js"></script>
       
    <link rel="stylesheet" type="text/css" href="/local/templates/rgb/css/style_rgb.css"/>
    <?//<script type="text/javascript" src="/local/templates/rgb/js/jquery-1.11.1.min.js"></script>?>
    <?//<script type="text/javascript" src="/local/templates/rgb/js/jquery-migrate-1.2.1.min.js"></script>?>
    <script type="text/javascript" src="/local/templates/rgb/js/jquery.bxslider.min.js"></script>
    <script type="text/javascript">
        document.documentElement.className += " js";
        document.documentElement.className = document.documentElement.className.replace( /(?:^|\s)no-js(?!\S)/ , '' );
    </script>
        
    <?/*
     * 
     * this is an original template
     * and we are going to comment it
     * fuck it for now
     * 
	<div class="b-mainblock left">

		<?$APPLICATION->ShowViewContent('lib_menu')?>

		<div class="b-lib_descr">
			<div class="iblock b-lib_fulldescr">
				<div class="b-libstatus"><?=$arResult['ITEM']['PROPERTY_STATUS_VALUE']?></div>
				<h2><?=$arResult['ITEM']['NAME']?></h2>
				<a href="<?=$arResult['ITEM']['PROPERTY_URL_VALUE']?>" class="b-libsite"
                    target="_blank"><?=$component->getLinkView($arResult['ITEM']['PROPERTY_URL_VALUE'])?></a>
				<div class="b-rolledtext" data-height="300">
					<div class="b-rolled_in ani_all_500_in">
						<p><?=htmlspecialcharsBack(TxtToHTML($arResult['ITEM']['~PREVIEW_TEXT']))?></p>
					</div>
					<a href="#" class="slidetext">Читать далее</a>
				</div>
			</div>

			<!--<a href="#" class="b-lib_photoslink">Фотогалерея</a>-->

		</div>

		<?
		$APPLICATION->IncludeComponent(
			"neb:books.popular",
			"",
			Array(
				"LIBRARY_ID" => $arResult['ITEM']["ID"],
				"ITEM_COUNT" => 12,
				"CACHE_TIME" => $arParams["CACHE_TIME"],
			),
			$component
		);
		?>

	</div><!-- /.b-mainblock -->
	<div class="b-side right">
		<?
		$APPLICATION->IncludeComponent(
			"neb:library.right_counter",
			"",
			Array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"LIBRARY_ID" => $arResult['ITEM']["ID"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"COLLECTION_URL" => $arParams['COLLECTION_URL'],
			),
			$component
		);
		?>
		<?$APPLICATION->IncludeComponent("bitrix:voting.current", "right", Array(
				"CHANNEL_SID" => "LIBRARY_".$arResult['ITEM']['ID'],	// Группа опросов
				"VOTE_ID" => "",	// ID опроса
				"VOTE_ALL_RESULTS" => "N",	// Показывать варианты ответов для полей типа Text и Textarea
				"AJAX_MODE" => "Y",	// Включить режим AJAX
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
			),
			false
		);?>

		<ul class="iblock b-lib_contact">
			<? if(!empty($arResult['ITEM']['PROPERTY_ADDRESS_VALUE'])){ ?>
				<li>
					<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_ADDRESS');?>:</span>
					<span class="value"><?=$arResult['ITEM']['PROPERTY_ADDRESS_VALUE']?></span>
				</li>
			<? } ?>
			<? if(!empty($arResult['ITEM']['PROPERTY_SCHEDULE_VALUE'])) { ?>
				<li>
					<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_WORK');?>:</span>
					<span class="value"><?=$arResult['ITEM']['PROPERTY_SCHEDULE_VALUE']?></span>
				</li>
			<? } ?>
			<? if(!empty($arResult['ITEM']['PROPERTY_PHONE_VALUE'])) { ?>
				<li>
					<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_PHONE');?></span>
					<span class="value">
<? foreach ($arResult['ITEM']['PROPERTY_PHONE_VALUE'] as $sPhone): ?>
                                <span><?=trim($sPhone)?></span><br />
                            <? endforeach; ?></span>
				</li>
			<? } ?>
			<? if(!empty($arResult['ITEM']['PROPERTY_EMAIL_VALUE'])) { ?>
				<li>
					<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_EMAIL');?></span>
					<span class="value"><a href="mailto:<?=$arResult['ITEM']['PROPERTY_EMAIL_VALUE']?>"><?=$arResult['ITEM']['PROPERTY_EMAIL_VALUE']?></a></span>
				</li>
			<? } ?>
			<? if(!empty($arResult['ITEM']['PROPERTY_SKYPE_VALUE'])) { ?>
				<li>
					<span class="title">Skype</span>
					<span class="value"><?=$arResult['ITEM']['PROPERTY_SKYPE_VALUE']?></span>
				</li>
			<? } ?>
			<? if(!empty($arResult['ITEM']['PROPERTY_CONTACTS_VALUE'])) { ?>
				<li>
					<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_CONTACT');?></span>
					<span class="value"><?=implode('<br />', $arResult['ITEM']['PROPERTY_CONTACTS_VALUE'])?></span>
				</li>
			<? } ?>
		</ul><!-- /.b-lib_contact -->
	</div>
	<?
	global $arrFilterLibraryNews;
	$arrFilterLibraryNews = array('PROPERTY_LIBRARY' => $arResult['ITEM']['ID'], '!PROPERTY_LIBRARY' => false);

	$APPLICATION->IncludeComponent("bitrix:news.list", "library_news", Array(
			"IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
			"IBLOCK_ID" => "3",	// Код информационного блока
			"NEWS_COUNT" => "20",	// Количество новостей на странице
			"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
			"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
			"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
			"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
			"FILTER_NAME" => "arrFilterLibraryNews",	// Фильтр
			"FIELD_CODE" => array(	// Поля
				0 => "NAME",
				1 => "PREVIEW_TEXT",
			),
			"PROPERTY_CODE" => array(	// Свойства
			),
			"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
			"DETAIL_URL" => str_replace('#CODE#', $arResult['ITEM']["CODE"], $arParams['NEWS_DETAIL_URL']),	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
			"AJAX_MODE" => "N",	// Включить режим AJAX
			"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
			"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
			"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			"CACHE_TYPE" => "A",	// Тип кеширования
			"CACHE_TIME" => $arParams["CACHE_TIME"],	// Время кеширования (сек.)
			"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
			"CACHE_GROUPS" => "N",	// Учитывать права доступа
			"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
			"ACTIVE_DATE_FORMAT" => "j F",	// Формат показа даты
			"SET_TITLE" => "N",	// Устанавливать заголовок страницы
			"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
			"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
			"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
			"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
			"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
			"PARENT_SECTION" => "",	// ID раздела
			"PARENT_SECTION_CODE" => "",	// Код раздела
			"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
			"DISPLAY_DATE" => "Y",	// Выводить дату элемента
			"DISPLAY_NAME" => "Y",	// Выводить название элемента
			"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
			"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
			"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
			"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
			"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
			"PAGER_TITLE" => "Новости",	// Название категорий
			"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
			"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
			"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
			"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		),
		$component
	);?>
     * 
     * original template is
     * commented up to here
     * */?>
</section>

<?/*
<div class="fmap" id="fmap" data-lat="<?=$arResult['ITEM']['MAP'][0]?>" data-lng="<?=$arResult['ITEM']['MAP'][1]?>" data-zoom="16"></div>
<span class="marker_lib hidden">
	<a href="#" onclick="return false;" class="b-elar_name_txt"><?=$arResult['ITEM']['NAME']?></a><br />
	<span class="b-elar_status"><?=$arResult['ITEM']['PROPERTY_STATUS_VALUE']?></span><br />
	<span class="b-map_elar_info">
		<?
		if(!empty($arResult['ITEM']['PROPERTY_ADDRESS_VALUE']))
		{
			?>
			<span class="b-map_elar_infoitem"><span><?=Loc::getMessage('LIBRARY_DETAIL_ADDRESS_MAP');?>:</span><?=$arResult['ITEM']['PROPERTY_ADDRESS_VALUE']?></span><br />
		<?
		}
		if(!empty($arResult['ITEM']['PROPERTY_SCHEDULE_VALUE']))
		{
			?>
			<span class="b-map_elar_infoitem"><span><?=Loc::getMessage('LIBRARY_DETAIL_WORK');?>:</span><?=$arResult['ITEM']['PROPERTY_SCHEDULE_VALUE']?></span>
		<?
		}
		?>
	</span>
	<span class="b-mapcard_act clearfix">
		<span class="right neb b-mapcard_status"><?=Loc::getMessage('LIBRARY_DETAIL_USER');?></span>
	</span>
</span>

<script src="//api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
*/?>