<?php

/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 31.10.2016
 * Time: 16:14
 */
class NebLinkLitres
{
    /**
     * @var string
     */
    private $url        = "https://www.litres.ru/static/or4/view/or.html?baseurl=/nebread/";

    /**
     * @var string
     */
    private $partner_id = "270993";

    /**
     * @var string
     */
    private $secret_key = "qjomhhoj137s4t9s64ey2180fye01banb8ytacknm6ygw7ws6t";

    /**
     * @var int
     */
    private $timestamp;

    /**
     * @var
     */
    private $EPubID;

    /**
     * @var
     */
    private $userID;

    /**
     * NebLinkLitres constructor.
     */
    public function __construct()
    {
        $this->timestamp = time();
    }

    /**
     * @param $EPubID
     */
    public function setEPubID ( $EPubID )
    {
        $this->EPubID = trim($EPubID);
    }

    /**
     * @param $userID
     */
    public function setUserID ( $userID )
    {
        $this->userID = $userID;
    }

    /**
     * @return string
     * @throws Exception
     * */
    private function makeSHA256 ()
    {
        if ( !isset( $this->EPubID ) || !isset( $this->userID ) )
            throw new Exception("Empty book_id or not set user_id !");

        return mb_strtoupper(
            hash(
                'sha256',
                $this->timestamp    . ":" .
                $this->EPubID       . ":" .
                $this->partner_id   . ":" .
                $this->userID       . ":" .
                $this->secret_key
            )
        );
    }

    /** <b>Example:</b> <i>https://www.litres.ru/static/or4/view/or.html?baseurl=/nebread/partner/book_id/timestamp/sha/user_id/</i>
     *
     * @return string|Exception
     * @throws Exception
     */
    public function generateLink ( )
    {
        try {
            $sha256 = $this->makeSHA256();
        } catch (Exception $err) {
            return $err;
        }

        return $this->url . $this->partner_id . "/" . $this->EPubID . "/" . $this->timestamp . "/" . $sha256 . "/" . $this->userID . "/";
    }
}