<?
namespace Bitrix\NotaExt\Ext\DetailGallery;

use Bitrix\NotaExt\Ext\DetailGallery\Params as DGParams;

/*
Выводим галерею
*/

#AddEventHandler("main", "OnEndBufferContent", Array("setDetailGallery", "OnEndBufferContent"));

class setDetailGallery
{
    function OnEndBufferContent(&$content)
    {
        self::OnEndBufferContent_v1($content);
        self::OnEndBufferContent_v2($content);
    }

    function OnEndBufferContent_v2(&$content)
    {
        if (!defined('ADMIN_SECTION')) {
            $arResult = array();
            $szSearchPattern = '~<img[^>]*class="gallery-detail-object-v2"[^>]*>~';
            $arPics = array();
            preg_match_all($szSearchPattern, $content, $arPics);

            if (count($arPics) > 0) {
                foreach ($arPics[0] as $pic) {
                    preg_match_all('/(img|rel)=(")[^">]+/i', $pic, $media);
                    $rel = preg_replace('/(img|rel)("|\'|="|=\')(.*)/i', "$3", $media[0]);
                    unset($media);

                    preg_match_all('/(img|alt)=("|\')[^"\'>]+/i', $pic, $media);
                    $alt = preg_replace('/(img|alt)("|\'|="|=\')(.*)/i', "$3", $media[0]);
                    unset($media);

                    if (empty($alt[0]))
                        continue;

                    if (empty($rel[0]))
                        continue;

                    $param = DGParams::decode($rel[0]);

                    if (!is_array($param))
                        continue;

                    $show_component = DGParams::getParam('show_component');

                    $strComponentHtml = '';

                    ob_start();

                    $GLOBALS['APPLICATION']->IncludeComponent(
                        $show_component,
                        $param['template'],
                        array(
                            'PARAMS_GALLERY' => $param,
                            'PARAMS_ITEMS' => $alt[0],
                        ),
                        false,
                        array("HIDE_ICONS" => "Y")
                    );
                    $strComponentHtml = @ob_get_contents();
                    ob_get_clean();

                    $content = str_replace($pic, $strComponentHtml, $content);
                }
            }
            unset($arPics);
        }
    }


    function OnEndBufferContent_v1(&$content)
    {

        if (!defined('ADMIN_SECTION')) {
            $arResult = array();
            $szSearchPattern = '~<img[^>]*class="gallery-detail-object"[^>]*>~';
            $arPics = array();
            preg_match_all($szSearchPattern, $content, $arPics);

            if (count($arPics) > 0) {

                foreach ($arPics[0] as $pic) {
                    preg_match_all('/(img|rel)=(")[^">]+/i', $pic, $media);
                    $rel = preg_replace('/(img|rel)("|\'|="|=\')(.*)/i', "$3", $media[0]);
                    unset($media);

                    preg_match_all('/(img|alt)=("|\')[^"\'>]+/i', $pic, $media);
                    $alt = preg_replace('/(img|alt)("|\'|="|=\')(.*)/i', "$3", $media[0]);
                    unset($media);

                    $arItemsTemp = \cUtil::JsObjectToPhp($alt[0]);
                    $arGParams = \cUtil::JsObjectToPhp($rel[0]);

                    if (!empty($arItemsTemp)) {
                        $arItems = array();
                        foreach ($arItemsTemp as $k => $v)
                            $arItems[] = $k;

                        $res = \CIBlock::GetByID($arGParams['IBLOCK_GALLERY_ID']);
                        $ar_res = $res->GetNext();

                        ob_start();

                        global $detailGalleryFilter;

                        $detailGalleryFilter = array(
                            'ID' => $arItems
                        );

                        $GLOBALS['APPLICATION']->IncludeComponent("bitrix:news.list", "detailGallery", array(
                            "IBLOCK_TYPE" => $ar_res['IBLOCK_TYPE_ID'],
                            "IBLOCK_ID" => $arGParams['IBLOCK_GALLERY_ID'],
                            "NEWS_COUNT" => "100",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "detailGalleryFilter",
                            "FIELD_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "PROPERTY_CODE" => array(
                                0 => "",
                                1 => "REAL_PICTURE",
                            ),
                            "CHECK_DATES" => "N",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "N",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "INCLUDE_SUBSECTIONS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "IMG_WIDTH" => $arGParams['width'],
                            "IMG_HEIGHT" => $arGParams['height'],
                        ),
                            false,
                            array("HIDE_ICONS" => "Y")
                        );
                        $strComponentHtml = @ob_get_contents();
                        ob_get_clean();

                        $content = str_replace($pic, $strComponentHtml, $content);
                    }

                }
            }

            unset($arPics);
        }
    }

    /*
    Корректируем текст, расставляем абзацы
    */
    function PrepareText($text)
    {

        $strPath = __DIR__;
        $strPath = substr($strPath, 0, strlen($strPath) - strlen("detailGallery/lib"));

        $text = str_replace('background-color:', 'background_color:', $text);

        include_once($strPath . 'typograph/lib/emt.php');
        if (class_exists('EMTypograph')) {
            $typograph = new \EMTypograph();

            /* отключаем все опции кроме простановке параграфов */
            #$option_list = $typograph->get_options_list();
            #$options = array();
            #foreach($option_list['all'] as $k => $v)
            #	$options[$k] = 'off';

            /*$options['Text.paragraphs'] = 'on';
            $options['OptAlign.oa_oquote'] = 'off';
            $options['OptAlign.oa_oquote_extra'] = 'off';
            $options['OptAlign.oa_obracket_coma'] = 'off';
            $typograph->setup($options);*/

            $arTypographOptions = \Bitrix\NotaExt\Ext\typograph\EmtOptions::getOptions();
            $typograph->setup($arTypographOptions);


            $typograph->set_text($text);
            $text = $typograph->apply();
            unset($typograph);

            # фиксим
            # Убираем дублирование Р тега
            $text = str_replace(array('<br />', '<p></p>', "\r", "\n"), '', $text);
            $text = str_replace(array('<p><p', '</p></p>'), array('<p', '</p>'), $text);

            # убраем обрамление тега с галреей тегом Р
            $szSearchPattern = '~<p>[^>]*<img[^>]*class="gallery-detail-object"[^>]*/>[^>]*</p>~';
            $arPics = array();
            preg_match_all($szSearchPattern, $text, $arPics);
            if (!empty($arPics[0])) {
                foreach ($arPics[0] as $Pic) {
                    $PicNoP = str_replace(array('<p>', '</p>'), '', $Pic);
                    $text = str_replace($Pic, $PicNoP, $text);
                }
            }

            return $text;
        }
        return $text;
    }
}
