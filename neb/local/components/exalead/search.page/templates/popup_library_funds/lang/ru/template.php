<?
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO'] = 'Найдено 0 документов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND'] = 'К сожалению, ничего не найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SUGGEST'] = 'Возможно вы искали';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'Найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'Найден';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'Найдено';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'] = 'результатов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'] = 'результата';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'] = 'результат';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY'] = 'Сохранить поисковый запрос';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED'] = 'Поисковый запрос сохранен';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE'] = 'Добавить название';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SPECIFY_SEARCH'] = 'уточнить поиск';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE'] = 'Вы искали на портале';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE'] = 'Искать только в полнотекстовых изданиях';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY'] = 'По каталогу печатных изданий';

$MESS['LIB_SEARCH_PAGE_POPUP_CHOOSE_BOOK'] = 'Перед добавлением издания необходимо выбрать книгу из списка.';
$MESS['LIB_SEARCH_PAGE_TEMLATE_BOOK_AUTHOR'] = "Автор";
$MESS['LIB_SEARCH_PAGE_TEMLATE_BOOK_YEAR'] = "Год публикации";
$MESS['LIB_SEARCH_PAGE_TEMLATE_BOOK_PAGES'] = "Количество страниц";
$MESS['LIB_SEARCH_PAGE_TEMLATE_BOOK_PUBLISHER'] = "Издательство";
$MESS['LIB_SEARCH_PAGE_TEMLATE_BOOK_SOURCE'] = "Источник";
$MESS['LIB_SEARCH_PAGE_TEMLATE_CANCEL_PUBLICATION'] = "Отказаться";
$MESS['LIB_SEARCH_PAGE_TEMLATE_POST_PUBLICATION'] = "Разместить произведение";
