<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\RslViewer;

// входные параметры

$book_id = trim($_REQUEST['book_id']);
$docType = trim($_REQUEST['doc_type']);
$docType = strtolower($docType);
if (empty($docType)) {
    $docType = 'pdf';
}

Loader::includeModule('nota.exalead');

$obRslViewer = new RslViewer($book_id);

/*// проверка доступа
if ($obRslViewer->isAvailableType() != 'open') {
    exit('Доступ к изданию запрещен');
}

// трекер переведен на агент \Neb\Main\Agents\StatsService::saveBookDownloads();
$trackParams = [
    'book_id'    => $book_id,
    'session_id' => session_id(),
    'user_id'    => intval(nebUser::getCurrent()->GetID()),
];
if (isset($_REQUEST['library_id']) && !empty($_REQUEST['library_id'])) {
    $trackParams['library_id'] = intval($_REQUEST['library_id']);
}
\Neb\Main\Stat\Tracker::track('neb_book_downloads', $trackParams);*/

// отдача файла
MainHelper::clearBuffer();
$obRslViewer->getFile($docType);