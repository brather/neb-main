<?
#if (!CModule::IncludeModule('nota.exalead')) return false;
#use Nota\Exalead\SearchQuery;
#use Nota\Exalead\SearchClient;

class UserDataExaleadBook extends CUserTypeString
{
    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => "exalead_book",
            "CLASS_NAME" => __CLASS__,
            "DESCRIPTION" => "Привязка к книге в Exalead",
            "BASE_TYPE" => "string",
        );
    }

    function GetIBlockPropertyDescription()
    {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "exalead_book",
            "DESCRIPTION" => "Привязка к книге в Exalead",
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
        );
    }

    function getEditHTML($name, $value, $is_ajax = false)
    {

        CUtil::InitJSCore(array("jquery"));
        ob_start();
        ?>
        <script type="text/javascript">

            $(function () {
                function getExaleadBook(id) {
                    $.ajax({
                        type: "GET",
                        url: "/local/modules/nota.collection/admin/collection_search_ajax.php?id=" + id,
                        success: function (data) {
                            $('.appends').html(data);
                        }
                    });
                }

                getExaleadBook('<?=$value?>');

                $("#<?=htmlspecialcharsbx($name)?>").change(function () {
                    getExaleadBook($(this).val());
                });
            });
        </script>
        <?
        $content = ob_get_contents();
        ob_end_clean();

        $strResult = $content . '<input style="width: 150px;" type="text" name="' . htmlspecialcharsbx($name) . '" id="' . htmlspecialcharsbx($name) . '" value="' . $value . '">' .
            '<input type="button" value="..." onClick="jsUtils.OpenWindow(\'/local/modules/nota.collection/admin/collection_search.php?code=' . $name . '&lang=' . LANGUAGE_ID . '\', 800, 500);">';
        $strResult .= '<div class="appends"></div>';
        return $strResult;
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
    }

    // редактирование свойства в форме и списке (инфоблок)
    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        return $strHTMLControlName['MODE'] == 'FORM_FILL'
            ? self::getEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false)
            : self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

    function getViewHTML()
    {

    }
}

//AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("UserDataExaleadBook", "GetIBlockPropertyDescription"));
//AddEventHandler("main", "OnUserTypeBuildList", array("UserDataExaleadBook", "GetUserTypeDescription"));
?>