<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CModule::IncludeModule('iblock');

// библиотеки в НЭБ
$arSelect = Array("ID", "NAME", "PROPERTY_LIBRARY_LINK");
$arFilter = Array("IBLOCK_ID" => IBLOCK_ID_LIBRARY, "ACTIVE" => "Y", "?NAME" => $find_name);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arLibs[$arFields['PROPERTY_LIBRARY_LINK_VALUE']]['NAME'] = $arFields['NAME'];
    $arLibs[$arFields['PROPERTY_LIBRARY_LINK_VALUE']]['ID'] = $arFields['ID'];
}

// количество библиотек
$countLibs = count($arLibs);

$sTableID = 'tbl_stat_library';
$oSort = new CAdminSorting($sTableID, "NAME", "desc");
$arOrder = (strtoupper($by) === "ID" ? array($by => $order) : array($by => $order, "ID" => "ASC"));
$lAdmin = new CAdminList($sTableID, $oSort);
$arFilterFields = Array(
    "find_date",
    "find_date_to",
    "find_name",
);

$lAdmin->InitFilter($arFilterFields);

$arHeader = array(
    array(
        'id' => 'ID',
        'content' => 'ID',
        'sort' => 'ID',
        'default' => true
    ),
    array(
        'id' => 'NAME',
        'content' => 'Библиотека',
        'default' => true,
        'sort' => 'NAME',
    ),
    array(
        'id' => 'COUNT_EDITION',
        'content' => 'Общее количество изданий',
        'default' => true,
        'sort' => 'COUNT_EDITION',
    ),
    array(
        'id' => 'COUNT_EDITION_DIG',
        'content' => 'Общее количество оцифрованных изданий',
        'default' => true,
        'sort' => 'COUNT_EDITION_DIG',
    ),
    array(
        'id' => 'COUNT_VIEW',
        'content' => 'Количество прочитанных изданий',
        'default' => true,
        'sort' => 'COUNT_EDITION',
    ),
    array(
        'id' => 'COUNT_READ',
        'content' => 'Количество скачанных изданий',
        'default' => true,
        'sort' => 'COUNT_EDITION_DIG',
    ),
);

$lAdmin->AddHeaders($arHeader);
$arSelect = $lAdmin->GetVisibleHeaderColumns();

if (!$find_date && !$find_date_to) {
    $find_date_search = date('Y-m-d') . " 00:00:00";
    $find_date_search_to = date('Y-m-d') . " 23:59:59";

} else {
    $find_date_search = (!$find_date ? date('Y-m-d', strtotime($find_date_to)) : date('Y-m-d', strtotime($find_date))) . " 00:00:00";
    $find_date_search_to = (!$find_date_to ? date('Y-m-d', strtotime($find_date)) : date('Y-m-d', strtotime($find_date_to))) . " 23:59:59";
}


$query = "
		SELECT
		  DATE_FORMAT(`nebstat`.`DATE_STAT`, '%d-%m-%Y')  `date`,
		  `nebstat`.`COUNT_EDITION`                       `COUNT_EDITION`,
		  `nebstat`.`COUNT_EDITION_DIG`                   `COUNT_EDITION_DIG`,
		  `nebstat`.`ID_EXALED` `id`
		FROM `neb_stat_edition` `nebstat`
		WHERE DATE_STAT BETWEEN  '" . $find_date_search . "' AND '" . $find_date_search_to . "'
		" . ($find_name != '' ? " AND TITLE_LIB LIKE '%$find_name%'" : '') . "
	";

$res = $DB->Query($query, false, $err_mess . __LINE__);

while ($libraryStatItem = $res->Fetch()) {
    if ($arLibs[$libraryStatItem['id']]['ID']) {
        $arStat[$libraryStatItem['id']]['ID'] = $arLibs[$libraryStatItem['id']]['ID'];
        $arStat[$libraryStatItem['id']]['COUNT_EDITION'] = $libraryStatItem['COUNT_EDITION'];
        $arStat[$libraryStatItem['id']]['COUNT_EDITION_DIG'] = $libraryStatItem['COUNT_EDITION_DIG'];
        $arStat[$libraryStatItem['id']]['DATE'] = $libraryStatItem['date'];
        $arStat[$libraryStatItem['id']]['NAME'] = $arLibs[$libraryStatItem['id']]['NAME'];
    }
}

// статистика по просмотренным книгам
$query = "
		SELECT
			ID_LIB,
			count(`CNT_READ`) `reads`,
			count(`CNT_VIEW`) `views`
		FROM neb_stat_log
		WHERE DT BETWEEN  '" . date(
        'Y-m-d H:i:s', strtotime($find_date_search) - 86400
    ) . "' AND '" . date(
        'Y-m-d H:i:s', strtotime($find_date_search_to) - 86400
    ) . "'
		group by  ID_LIB
	";

$res = $DB->Query($query, false, $err_mess . __LINE__);

while ($bookStatItem = $res->Fetch()) {
    if ($arLibs[$bookStatItem['ID_LIB']]['ID']) {
        $arStat[$bookStatItem['ID_LIB']]['ID'] = $arLibs[$bookStatItem['ID_LIB']]['ID'];
        $arStat[$bookStatItem['ID_LIB']]['COUNT_READ'] = ($bookStatItem['reads'] > 0 ? $bookStatItem['reads'] : '');
        $arStat[$bookStatItem['ID_LIB']]['COUNT_VIEW'] = ($bookStatItem['views'] > 0 ? $bookStatItem['views'] : '');
        $arStat[$bookStatItem['ID_LIB']]['NAME'] = $arLibs[$bookStatItem['ID_LIB']]['NAME'];
    }
}
$rsData = new CAdminResult($arStat, $sTableID);

$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(''));

while ($arRes = $rsData->NavNext()) {
    $row = $lAdmin->AddRow($str_ID, $arRes);

}

$lAdmin->AddAdminContextMenu(array());

$lAdmin->CheckListMode();
$APPLICATION->SetTitle('Статистика по библиотекам');

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';

?>
    <form method="GET" action="" name="find_form">
        <input type="hidden" name="filter" value="Y">
        <?
        $oFilter = new CAdminFilter(
            $sTableID . "_filter",
            array("ID",)
        );
        $oFilter->Begin();
        ?>
        <tr>
            <td><b>Библиотека</b></td>
            <td><input type="text" name="find_name" value="<? echo htmlspecialcharsbx($find_name) ?>" size="40"></td>
        </tr>
        <tr>
            <td>Дата:</td>
            <td><?= CalendarDate("find_date", $find_date, "find_form", "15", "class=\"my_input\"") ?><span
                        class="adm-calendar-separate"
                        style="display: inline-block;"></span><?= CalendarDate("find_date_to", $find_date_to, "find_form", "15", "class=\"my_input\"") ?>
            </td>
        </tr>
        <?
        $oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPageParam(), "form" => "find_form"));
        $oFilter->End();
        ?>
    </form>
<?
$lAdmin->DisplayList();
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
?>