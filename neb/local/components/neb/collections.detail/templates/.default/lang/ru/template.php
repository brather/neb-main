<?php
$MESS['COLLECTION_DETAIL_ADD_TO_MY_COL'] = 'Сохранить издания подборки в мою библиотеку, доступную в личном кабинете';
$MESS['COLLECTION_DETAIL_REMOVE_FROM_MY_COL'] = 'Удалить издания подборки в моей библиотеке, доступной в личном кабинете';
$MESS['COLLECTION_DETAIL_UPDATED'] = 'Обновлено';
$MESS['COLLECTION_DETAIL_AUTHOR'] = 'Автор';
$MESS['COLLECTION_DETAIL_BOOK_5'] = 'изданий';
$MESS['COLLECTION_DETAIL_BOOK_1'] = 'издание';
$MESS['COLLECTION_DETAIL_BOOK_2'] = 'издания';
$MESS['COLLECTION_DETAIL_IN_COL'] = 'в подборке';
$MESS['COLLECTION_DETAIL_ADDED_TO_FAV'] = 'Добавленно в избранное';
$MESS['COLLECTION_DETAIL_VIEWS'] = 'Количество просмотров';
$MESS['COLLECTION_DETAIL_SETTINGS'] = 'Настройки';
$MESS['COLLECTION_DETAIL_LIST_SUB_COLLECTION_1'] = "Для удобства изучения и практического использования коллекции ";
$MESS['COLLECTION_DETAIL_LIST_SUB_COLLECTION_2'] = " все входящие в нее издания классифицированы по темам:";