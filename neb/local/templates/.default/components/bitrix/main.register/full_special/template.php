<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?
	if ( (int)$arResult['VALUES']['USER_ID'] > 0 && $arResult["USE_EMAIL_CONFIRMATION"] === "Y" )
	{ 
	?>
	<section class="innersection innerwrapper clearfix">
		<div class="b-formsubmit">
			<span class="b-successlink"><?=GetMessage("REGISTER_SUCCESSFUL")?></span>
			<p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
		</div>
	</section>
	<?
		return;
	}
?>


<script type="text/javascript">
	function setlogin(){
		$('input#LOGIN').val($('input#settings04').val());
	}
</script>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode">регистрация на портале</h2>
		<?
			if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && (int)$arResult['VALUES']['USER_ID'] > 0 ){
			?>
			<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
			<?
			}
		?>
		<form method="post" action="<?=POST_FORM_ACTION_URI?>" class="b-form b-form_common b-regform" name="regform" enctype="multipart/form-data" onsubmit="setlogin()" id="regform">
			<?
			if (count($arResult["ERRORS"]) > 0){
				foreach ($arResult["ERRORS"] as $key => $error){
					if (intval($key) == 0 && $key !== 0) {
						$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
					}
				}

				//ShowError(implode("<br />", $arResult["ERRORS"]).'<br /><br />');
				?>
				<p class="error_message"><?=implode("<br />", $arResult["ERRORS"]).'<br /><br />'?></p>
			<?

			}
			?>
			<?
				if($arResult["BACKURL"] <> ''){
				?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?
				}
			?>
			<input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value="" />
			<input type="hidden" name="REGISTER[PERSONAL_NOTES]" id="PERSONAL_NOTES" value="" />

			<p class="note">Просим обратить особое внимание на заполнение анкеты корректными данными. <br/>В случае выявления несоответствий Ваш доступ может быть заблокирован.</p>

				<div class="b-form_header"><span class="iblock">Паспортные данные</span></div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings01"><?=Loc::getMessage('MAIN_REGISTER_LASTNAME');?></label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
							<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
							<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>	
							<input type="text" data-validate="fio" data-required="required" value="<?=$arResult["VALUES"]['LAST_NAME']?>"  id="settings01" data-maxlength="30" data-minlength="2" name="REGISTER[LAST_NAME]" class="input">										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings02"><?=Loc::getMessage('MAIN_REGISTER_FIRSTNAME');?></label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
							<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
							<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>		
							<input type="text" data-required="required" data-validate="fio" value="<?=$arResult["VALUES"]['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="REGISTER[NAME]" class="input" >										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings22"><?=Loc::getMessage('MAIN_REGISTER_MIDNAME');?></label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
							<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
							<input type="text" data-validate="fio" value="<?=$arResult["VALUES"]['SECOND_NAME']?>" id="settings22" data-maxlength="30" data-minlength="2"data-required="false"  name="REGISTER[SECOND_NAME]" class="input" >										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings06"><?=Loc::getMessage('MAIN_REGISTER_BIRTHDAY');?></label>
						<div class="field validate iblock seldate" data-yearsrestrict="12">
							<em class="error dateformat"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT');?></em>
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
							<em class="error yearsrestrict"><?=Loc::getMessage('MAIN_REGISTER_AGE');?></em>
							<input type="hidden" class="realseldate" data-required="true" value="<?=$arResult["VALUES"]['PERSONAL_BIRTHDAY']?>" id="settings06" name="REGISTER[PERSONAL_BIRTHDAY]">
							<select name="birthday" id="PERSONAL_BIRTHDAY_D" class="js_select w102 sel_day"  data-required="true">
								<option value="-1"><?=Loc::getMessage('MAIN_REGISTER_DAY');?></option>
								<?
									for($i=1;$i<=31;$i++)
									{
									?>
									<option value="<?=$i?>"><?=$i?></option>
									<?
									}
								?>
							</select>

							<select name="birthmonth" id="PERSONAL_BIRTHDAY_M" class="js_select w165 sel_month"  data-required="true">
								<option value="-1"><?=Loc::getMessage('MAIN_REGISTER_MONTH');?></option>
								<?
									for($i=1;$i<=12;$i++)
									{
									?>
									<option value="<?=$i?>"><?=FormatDate('f', MakeTimeStamp('01.'.$i.'.'.date('Y')))?></option>
									<?
									}
								?>
							</select>

							<select name="birthyear" id="PERSONAL_BIRTHDAY_Y" class="js_select w102 sel_year"  data-required="true">
								<option value="-1"><?=Loc::getMessage('MAIN_REGISTER_YEAR');?></option>
								<?
									for($i=(date('Y')-14);$i>=(date('Y')-95);$i--)
									{
									?>
									<option value="<?=$i?>"><?=$i?></option>
									<?
									}
								?>
							</select>
						</div>									

					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings011">Серия и номер паспорта</label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<div class="field iblock"><input type="text"  data-required="required" value="<?=$arResult["VALUES"]['UF_PASSPORT_SERIES']?>" id="settings011" name="UF_PASSPORT_SERIES" class="input w110"></div>					  
							<div class="field iblock"><input type="text"  data-required="required" value="<?=$arResult["VALUES"]['UF_PASSPORT_NUMBER']?>" id="settings022" name="UF_PASSPORT_NUMBER" class="input w190"></div>						  
						</div>
					</div>
				</div>
				
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings08">Гражданство</label>
					<div class="field validate iblock">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<select name="UF_CITIZENSHIP" id="UF_CITIZENSHIP"  class="js_select w370"  data-required="true">
							<?
							$arUF_CITIZENSHIP = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'),array("ID"=>"ASC"));
							if(!empty($arUF_CITIZENSHIP))
							{
								foreach($arUF_CITIZENSHIP as $arItem)
								{
									if($arItem['DEF']=="Y")
									{
										$ruCitizenship = $arItem['ID'];
									}
									?>
									<option value="<?=$arItem['ID']?>"<?=($arItem['DEF']=="Y")?' selected="selected"':'';?>><?=$arItem['VALUE']?></option>
									<?php
								}
							}
							?>
						</select>
						<script type="text/javascript">
							$(function(){
								var copyEducation = $('select[name="UF_EDUCATION"]').clone().removeClass("custom");
								function correctEducationField()
								{
									var cit = $('select[name="UF_CITIZENSHIP"] option:selected').val();
									var parentEducation = $('select[name="UF_EDUCATION"]').closest("div.field");
									parentEducation.children("span").remove();
									parentEducation.append(copyEducation.clone());
									$('#UF_EDUCATIONopts').remove();
									if(cit == <?php echo $ruCitizenship;?>)
									{
										$('select[name="UF_EDUCATION"] option').filter('[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
									}
									else
									{
										$('select[name="UF_EDUCATION"] option').not('[VALUE=""],[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
									}
									$('select[name="UF_EDUCATION"]').selectReplace();
								}
								correctEducationField();
								$('select[name="UF_CITIZENSHIP"]').change(function(){
									correctEducationField();
								});
							});
						</script>
					</div>
				</div>

				<div class="b-form_header"><span class="iblock">Сфера специализации</span></div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings10">Место работы/учебы</label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<input type="text" data-validate="alpha" value="<?=$arResult["VALUES"]['WORK_COMPANY']?>" id="settings10" data-minlength="2" data-required="true" name="REGISTER[WORK_COMPANY]" class="input" >
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings08">Отрасль знаний</label>
						<div class="field validate iblock">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<select name="UF_BRANCH_KNOWLEDGE" id="settings08"  class="js_select w370"  data-required="true">
								<option value="">выберите</option>
								<?
									$arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE'));
									if(!empty($arUF_BRANCH_KNOWLEDGE))
									{
										foreach($arUF_BRANCH_KNOWLEDGE as $arItem)
										{
										?>
										<option value="<?=$arItem['ID']?>"><?=$arItem['VALUE']?></option>
										<?
										}
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings09">Образование. учёная степень</label>
						<div class="field validate iblock">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<select name="UF_EDUCATION" id="settings09"  class="js_select w370"  data-required="true">
								<option value="">выберите</option>
								<?
									$arUF_EDUCATION = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION'));
									if(!empty($arUF_EDUCATION))
									{
										foreach($arUF_EDUCATION as $arItem)
										{
										?>
										<option <?=$arResult["VALUES"]['UF_EDUCATION'] == $arItem['ID'] ? 'selected="selected"':''?> value="<?=$arItem['ID']?>"><?=$arItem['VALUE']?></option>
										<?
										}
									}
								?>
							</select>
						</div>
					</div>
				</div>


			<div class="b-form_header">Вход на сайт</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings04"><?=Loc::getMessage('MAIN_REGISTER_EMAIL');?></label>
					<div class="field validate">
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_EMAIL_FORMAT');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<input type="text" class="input" name="REGISTER[EMAIL]" data-minlength="2" data-maxlength="50" id="settings04" data-validate="email" value="<?=$arResult["VALUES"]['EMAIL']?>" data-required="true">
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<!--<div class="passecurity">
						<div class="passecurity_lb">Защищенность пароля</div>

					</div>-->
					<label for="settings05"><span>не менее 6 символов</span>Пароль</label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="true" value="<?=$arResult["VALUES"]['PASSWORD']?>" id="settings05" data-maxlength="30" data-minlength="6" name="REGISTER[PASSWORD]" class="pass_status input">										
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings55">Подтвердить пароль</label>
					<div class="field validate">
						<em class="error identity "><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_MISMATCH');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
						<input data-identity="#settings05" type="password" data-required="true" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" id="settings55" data-maxlength="30" data-minlength="2" name="REGISTER[CONFIRM_PASSWORD]" class="input" data-validate="password">								
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings11">Мобильный телефон</label>
					<div class="field validate">
						<input type="text" class="input maskphone" name="REGISTER[PERSONAL_MOBILE]"  id="settings11"  value="<?=$arResult["VALUES"]['PERSONAL_MOBILE']?>">	
						<!--<span class="phone_note">для смс-уведомлений о статусе Ваших заявок  на сайте РГБ</span>-->
					</div>
				</div>
			</div>




				<div class="b-form_header"><span class="iblock">Адрес регистрации</span></div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings03">Индекс <span class="minscreen">регистрации</span></label>
						<div class="field validate w140">
							<em class="error required">Заполните</em>
							<em class="error number">Некорректный почтовый индекс</em>
							<input type="text" data-validate="number" data-required="required" value="<?=$arResult["VALUES"]['PERSONAL_ZIP']?>" id="settings03" name="REGISTER[PERSONAL_ZIP]" class="input ">			  
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings033">Область / Регион <span class="minscreen">регистрации</span></label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<input type="text" data-required="required" value="<?=$arResult["VALUES"]['UF_REGION']?>" id="settings033" data-maxlength="30" data-minlength="2" name="UF_REGION" class="input">
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings013">Район <span class="minscreen">регистрации</span></label>
						<div class="field validate">
							<input type="text" value="<?=$arResult["VALUES"]['UF_AREA']?>" id="settings013" data-maxlength="30" data-minlength="2" name="UF_AREA" class="input">
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings055">Город <span class="minscreen">регистрации</span></label>
						<div class="field validate">
							<em class="error required">Заполните</em>
							<input type="text" data-required="required" value="<?=$arResult["VALUES"]['PERSONAL_CITY']?>" id="settings055" data-maxlength="30" data-minlength="2" name="REGISTER[PERSONAL_CITY]" class="input" >										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings07">Улица <span class="minscreen">регистрации</span></label>
						<div class="field validate">
							<input type="text" value="<?=$arResult["VALUES"]['PERSONAL_STREET']?>" id="settings07" data-maxlength="30" data-minlength="2" data-required="true"  name="REGISTER[PERSONAL_STREET]" class="input">					
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<div class="field iblock fieldthree ">
							<label for="settings09">Дом:</label>
							<div class="field validate">
								<input type="text" class="input" name="UF_CORPUS" data-maxlength="20" id="settings09" value="<?=$arResult["VALUES"]['UF_CORPUS']?>"  data-required="true"/>
								<em class="error required">Заполните</em>
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings10">Строение: </label>
							<div class="field">
								<input type="text" class="input" name="UF_STRUCTURE" data-maxlength="20" maxlength="4" id="settings10" value="<?=$arResult["VALUES"]['UF_STRUCTURE']?>" />
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings11">Квартира: </label>
							<div class="field">
								<input type="text" class="input" name="UF_FLAT" data-maxlength="20" maxlength="4" id="settings11" value="<?=$arResult["VALUES"]['UF_FLAT']?>" />
							</div>
						</div>
					</div>
				</div>
				<div class="b-form_header">
					<span class="iblock">Адрес проживания</span>
					<div class="checkwrapper">
						<input class="checkbox addrindently" type="addr" name="UF_PLACE_REGISTR" id="cb3r"><label for="cb3r" class="black">по месту регистрации</label>
						<?
						if(!empty($arResult["VALUES"]['UF_PLACE_REGISTR']))
						{
							?>
							<script type="text/javascript">
								$(function(){
									$('input[name="UF_PLACE_REGISTR"]').click();
								});
							</script>
						<?

						}
						?>
							<script type="text/javascript">
								$(function(){
									$('#cb3r').change(function(){
										$(this).attr('value', +$(this)[0].checked);
										$(".hidden-fieldgroup").each(function(){
											$(this).attr('value', $("input[name='" + $(this).attr('name') + "'][type != 'hidden']").attr('value'));
										})
									});		
								});
							</script>	
					</div>
				</div>
				<input type="hidden" class="hidden-fieldgroup" data-validate="number" value="" name="REGISTER[WORK_ZIP]">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings044">Индекс <span class="minscreen">проживания</span></label>
						<div class="field validate w140">
							<input type="text" data-validate="number" value="<?=$arResult["VALUES"]['WORK_ZIP']?>" id="settings044" name="REGISTER[WORK_ZIP]" class="input">			
						</div>
					</div>
				</div>
				<input type="hidden" class="hidden-fieldgroup" value="" name="UF_REGION2">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings033r">Область / Регион <span class="minscreen">проживания</span></label>
						<div class="field validate">
							<input type="text" value="<?=$arResult["VALUES"]['UF_REGION2']?>" id="settings033r" data-maxlength="30" data-minlength="2" name="UF_REGION2" class="input">
						</div>
					</div>
				</div>
				<input type="hidden" class="hidden-fieldgroup" value="" name="UF_AREA2">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings013r">Район <span class="minscreen">проживания</span></label>
						<div class="field validate">
							<input type="text" value="<?=$arResult["VALUES"]['UF_AREA2']?>" id="settings013r" data-maxlength="30" data-minlength="2" name="UF_AREA2" class="input">
						</div>
					</div>
				</div>
				<input type="hidden" class="hidden-fieldgroup" value="" name="REGISTER[WORK_CITY]">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings066">Город <span class="minscreen">проживания</span></label>
						<div class="field validate">
							<input type="text" value="<?=$arResult["VALUES"]['WORK_CITY']?>" id="settings066" data-maxlength="30" data-minlength="2" name="REGISTER[WORK_CITY]" class="input" >							
						</div>
					</div> 
				</div>
				<input type="hidden" class="hidden-fieldgroup" value="" name="REGISTER[WORK_STREET]">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings08r">Улица <span class="minscreen">проживания</span></label>
						<div class="field validate">
							<input type="text" value="<?=$arResult["VALUES"]['WORK_STREET']?>" id="settings08r" data-maxlength="30" data-minlength="2" name="REGISTER[WORK_STREET]" class="input">					
						</div>
					</div>
				</div>
				<input type="hidden" class="hidden-fieldgroup" value="" name="UF_HOUSE2">
				<input type="hidden" class="hidden-fieldgroup" value="" name="UF_STRUCTURE2">
				<input type="hidden" class="hidden-fieldgroup" value="" name="UF_FLAT2">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<div class="field iblock fieldthree ">
							<label for="settings09r">Дом:</label>
							<div class="field validate">
								<input type="text" class="input" name="UF_HOUSE2" data-maxlength="20" id="settings09r" value="<?=$arResult["VALUES"]['UF_HOUSE2']?>"/>
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings10">Строение: </label>
							<div class="field">
								<input type="text" class="input" name="UF_STRUCTURE2" data-maxlength="20" maxlength="4" id="settings10" value="<?=$arResult["VALUES"]['UF_STRUCTURE2']?>" />
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings11">Квартира: </label>
							<div class="field">
								<input type="text" class="input" name="UF_FLAT2" data-maxlength="20" maxlength="4" id="settings11" value="<?=$arResult["VALUES"]['UF_FLAT2']?>" />
							</div>
						</div>
					</div>
				</div>

			<hr>

			<?$APPLICATION->IncludeComponent(
				"notaext:plupload",
				"scan_passport1_reg",
				array(
					"MAX_FILE_SIZE" => "10",
					"FILE_TYPES" => "jpg,jpeg,png",
					"DIR" => "tmp_register",
					"FILES_FIELD_NAME" => "profile_file",
					"MULTI_SELECTION" => "N",
					"CLEANUP_DIR" => "Y",
					"UPLOAD_AUTO_START" => "Y",
					"RESIZE_IMAGES" => "Y",
					"RESIZE_WIDTH" => "4000",
					"RESIZE_HEIGHT" => "4000",
					"RESIZE_CROP" => "Y",
					"RESIZE_QUALITY" => "98",
					"UNIQUE_NAMES" => "Y",
				),
				false
			);?>


			<?$APPLICATION->IncludeComponent(
				"notaext:plupload",
				"scan_passport2_reg",
				array(
					"MAX_FILE_SIZE" => "10",
					"FILE_TYPES" => "jpg,jpeg,png",
					"DIR" => "tmp_register",
					"FILES_FIELD_NAME" => "profile_file",
					"MULTI_SELECTION" => "N",
					"CLEANUP_DIR" => "Y",
					"UPLOAD_AUTO_START" => "Y",
					"RESIZE_IMAGES" => "Y",
					"RESIZE_WIDTH" => "4000",
					"RESIZE_HEIGHT" => "4000",
					"RESIZE_CROP" => "Y",
					"RESIZE_QUALITY" => "98",
					"UNIQUE_NAMES" => "Y",
				),
				false
			);?>

			<hr>
			<div class="checkwrapper">
				<label class="defautl_cursor">Прежде чем зарегистрироваться, необходимо ознакомиться с </label> <a href="/local/components/neb/registration/templates/.default/ajax_agreement.php" target="_blank" class="popup_opener ajax_opener closein" data-width="955" data-height="auto">правилами использования</a>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix divdisable">
						<button disabled="disabled" type="submit" value="1" class="formbutton btdisable">Зарегистрироваться</button>
						<div class="b-hint">Прочтите правила использования</div>
						<input type="hidden" name="register_submit_button" value="Регистрация" />
					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->
</section>
