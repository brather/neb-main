<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader;
use \Nota\UserData\Books;

/**
 * Class SearchPageViewerLeftComponent
 */
class SearchPageViewerLeftComponent extends CBitrixComponent
{
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $this->arResult = $arParams['RESULT'];
        $arParams = $arParams['PARAMS'];

        $arParams['TITLE_MODE_BLOCK'] = empty($arParams['TITLE_MODE_BLOCK']) ? GetMessage('TITLE_MODE_BLOCK') : $arParams['TITLE_MODE_BLOCK'];
        $arParams['TITLE_MODE_LIST'] = empty($arParams['TITLE_MODE_LIST']) ? GetMessage('TITLE_MODE_LIST') : $arParams['TITLE_MODE_LIST'];

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        Loader::includeModule('nota.userdata');

        // обработка книг
        if (!empty($this->arResult['ITEMS'])) {
            foreach ($this->arResult['ITEMS'] as &$arItem) {
                $arItem['~authorbook'] = substr(str_replace(['"', '\''], '', strip_tags($arItem['authorbook'])), 0, 35);
                $arItem['~title'] = substr(str_replace(['"', '\''], '', strip_tags($arItem['title'])), 0, 35);
                $tmpLibrary = Bitrix\NotaExt\Iblock\Element::getList(
                    ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'PROPERTY_LIBRARY_LINK' => $arItem['idlibrary']], 1, ['DETAIL_PAGE_URL']
                );
                $arItem['library_url'] = $tmpLibrary['ITEM']['DETAIL_PAGE_URL'];
            }
        }

        // обработка пользователя
        $obNUser = new nebUser();
        if ($obNUser->IsAuthorized()) {
            $this->arResult['USER_BOOKS'] = Books::getListCurrentUser();
        }
        $this->arResult['isInWorkplace'] = $obNUser->isInWorkplace() && $obNUser->IsAuthorized();
        $this->arResult['nebUser'] = $obNUser->getUser();

        $this->includeComponentTemplate();
    }
}
