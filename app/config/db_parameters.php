<?php
if (file_exists(__DIR__ . '/../../neb/bitrix/.settings_extra.php')) {
    $config = require(__DIR__ . '/../../neb/bitrix/.settings_extra.php');
} else {
    throw new Exception('Database error');
}
$container->setParameter(
    'database_host', $config['connections']['value']['default']['host']
);
$container->setParameter(
    'database_port',
    null
);
$container->setParameter(
    'database_name',
    $config['connections']['value']['default']['database']
);
$container->setParameter(
    'database_user',
    $config['connections']['value']['default']['login']
);
$container->setParameter(
    'database_password',
    $config['connections']['value']['default']['password']
);
$container->setParameter(
    'database_unix_socket',
    $config['connections']['value']['default']['unixSocket']
);
