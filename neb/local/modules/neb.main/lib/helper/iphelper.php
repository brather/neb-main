<?php
/**
 * User: D-Efremov
 * Date: 04.05.2016
 * Time: 10:08
 */

namespace Neb\Main\Helper;

class IpHelper
{
    /**
     * Проверяет соответсвтие IP адреса стандарту IPv4
     * @param $ip string IP
     * @return bool
     */
    public static function validateIp($ip)
    {
        return $ip === filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }

    /**
     * Проверяет принадлежность IP адреса диапазону адресов локальной сети, а также зарезервированным адресам
     * @param $ip string IP
     * @return bool
     */
    public static function validatePrivateIp($ip)
    {
        return $ip === filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE);
    }

    /**
     * Проверяет диапазон IP адресов на принадлежность одной не-локальной подсети
     * @param $ipRange string IP range
     * @return bool
     */
    public static function validateIpRange($ipRange)
    {
        $parts   = explode('-', $ipRange);
        $leftIp  = trim($parts[0]);
        $rightIp = trim($parts[1]);

        preg_match('/^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})\.([0-9]{1,3})$/', $leftIp,  $tmpLeft);
        preg_match('/^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})\.([0-9]{1,3})$/', $rightIp, $tmpRight);

        // колчество - строго два
        if (count($parts) != 2)
            return false;

        // проверка на принадлежность к локальным адресам
        if (static::validatePrivateIp($leftIp) == false || static::validatePrivateIp($rightIp) == false)
            return false;

        // проверка на принадлежность одной подсети и правильности диапазона
        if ($tmpLeft[1] != $tmpRight[1] || $tmpLeft[2] > $tmpRight[2])
            return false;

        return true;
    }

    /**
     * Проверяет принадлежность IP адреса к диапазону IP адресов
     * @param string $userIp string IP пользователя
     * @param string $sRangeIp IP или диапазон IP адресов
     * @return bool
     */
    public static function validateIpInRange($userIp, $sRangeIp)
    {
        $userIp = ip2long($userIp);

        // Диапазон
        if (strpos($sRangeIp, '-') !== false)
        {
            $parts = explode('-', $sRangeIp);
            if (false === static::validateIp($parts[0]) || false === static::validateIp($parts[1]))
                return false;

            $left = ip2long(trim($parts[0]));
            $right = ip2long(trim($parts[1]));

            if (($userIp >= $left) && ($userIp <= $right))
                return true;

            return false;
        }

        if (false === static::validateIp($sRangeIp))
            return false;

        $sRangeIp = ip2long($sRangeIp);

        return $sRangeIp === $userIp;
    }

    /**
     * Возвращает IP пользователя
     * https://habrahabr.ru/post/177113/
     *
     * @return string
     */
    public static function getRemoteIp()
    {
        $sResult = '';

        $arSequence = [
            'HTTP_X_REAL_IP',
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'REMOTE_ADDR'
        ];

        foreach ($arSequence as $sIp) {
            if (!empty($_SERVER[$sIp]) && filter_var($_SERVER[$sIp], FILTER_VALIDATE_IP)){
                $sResult = $_SERVER[$sIp];
                break;
            }
        }

        return $sResult;
    }

    /**
     * Получает доменное имя узла, соответствующее переданному IP-адресу
     *
     * @param $sIp
     * @return string
     */
    public static function getHostByIp($sIp) {
        return @gethostbyaddr($sIp);
    }
}