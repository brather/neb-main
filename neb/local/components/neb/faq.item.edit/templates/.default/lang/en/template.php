<?php
$MESS["NAME_ITEM"] = "Name of element: ";
$MESS["INPUT_NAME_ITEM"] = "input name of element...";
$MESS["ADD_ITEM"] = "Add";
$MESS["UPDATE_ITEM"] = "Update";
$MESS["DELETE_ITEM"] = "Delete item: ";
$MESS["BACK_TO_LIST"] = "To list items";
$MESS["DETAIL_TEXT_ITEM"] = "Detail text for element: ";
$MESS["CHOISE_SECTION_FOR_ITEM"] = "Choise section for element: ";
$MESS["ACTIVE_ITEM"] = "Active element";
$MESS["FAQ_QUESTION_DELETE_ITEM"] = "Delete the item?";
$MESS["SORT_ITEM"] = "Sort index: ";
$MESS["SORT_ITEM_DEFAULT"] = "500";
$MESS["ITEM_EDIT_PAGE"] = "Edit item";
$MESS['REQUIRED_FIELD'] = "* - Required field.";