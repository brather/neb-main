
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<div class="popup_esia">
		<div class="simple-cover authn-cover">
			   <h2 class="page-title">Необходима авторизация</h2>

                <div class="content-styles mt10">
                    <p class="big">
                        Для доступа к&nbsp;системам и&nbsp;сервисам Электронного правительства вам нужно пройти авторизацию.
                    </p>
					<p><a href="https://esia.gosuslugi.ru/registration/" title="Регистрация" class="goto-reg">Регистрация</a></p>
                </div>
                <h2 class="page-title"> Вход </h2>
                <form id="authnFrm" method="post" action="/idp/authn/UsernamePasswordLogin">
                    <div class="data-form data-form-authn">

                        <dl class="login">
                            <dt>
								<label class="active" data="phone">Телефон</label>
							</dt>
							<dd>
								<input id="phone" class="ui-inputfield">
								  <div class="field-error">
										<div class="ui-message-error">
											<span class="ui-message-error-detail"></span>
										</div>
								</div>
							</dd>
							<dt>
								<label class="active" data="email">E-mail</label>
							</dt>
							<dd>
								<input id="email" class="ui-inputfield">
								<div  class="field-error">
										<div class="ui-message-error">
											<span class="ui-message-error-detail"></span>
										</div>
								</div>
							</dd>
							<dt>
								<label class="active" data="snils">СНИЛС</label>
							</dt>
							<dd>
								<input id="snils" class="ui-inputfield">
								<div style="display: none;" class="field-error">
										<div class="ui-message-error">
											<span class="ui-message-error-detail"></span>
										</div>
								</div>
							</dd>
						</dl>

                        <dl class="password">
                            <dt>
                                <label>Пароль</label>
                            </dt>
                            <dd>
                                <input id="password" name="password" class="ui-inputfield ui-inputtext" type="password">
								<div style="display: none;" class="field-error">
										<div class="ui-message-error">
											<span class="ui-message-error-detail"></span>
										</div>
								</div>
                                <input id="username" name="username" type="hidden">
                                <input id="idType" name="idType" type="hidden">
                            </dd>
                        </dl>

                        
                        <ul class="check-list">
                            <li>
                                <input id="saveId" name="saveId" type="checkbox"><label for="saveId">Запомнить меня</label>
                            </li>
                        </ul>
                        <dl class="button-authn">
                            <button id="authnBtn" name="authnBtn" class="ui-button ui-button-text-only button-big" type="button">
                                <span class="ui-button-text">Войти</span>
                            </button>
                        </dl>

                      <a href="https://esia.gosuslugi.ru/recovery" title="Не удается войти?" class="fail-login mt10">Не удается войти?</a>

                    </div>
                </form>

        </div>

		</div>
	</div><!-- /.b-registration-->



</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>