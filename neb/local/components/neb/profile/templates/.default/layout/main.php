<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;

/**
 * @var array $arResult
 */
?>
<div class="container lk-container">
    <div class="row">
        <div class="col-md-3 col-sm-3 lk-sidebar">
            <? require $arResult['leftTemplatePath']; ?>
            <br>
        </div>
        <div class="col-md-9 col-sm-9 lk-content">
            <h2 class="lk-content__title">
                <? $APPLICATION->ShowTitle(); ?>
            </h2>
            <? require $arResult['contentTemplatePath']; ?>
        </div>
        <?if($arResult['LIBRARY']['NAME']){?>
            <script>
                $(function(){
                    $('.lk-title').text($('.lk-title').text() + ' "' + '<?=$arResult['LIBRARY']['NAME']?>' + '"');
                });
            </script>
        <?}?>
    </div>
</div>

