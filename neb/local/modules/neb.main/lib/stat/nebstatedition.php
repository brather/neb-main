<?php
/**
 * User: agolodkov
 * Date: 13.08.2015
 * Time: 10:45
 */

namespace Neb\Main\Stat;


use Bitrix\Main\Entity\DataManager;

/**
 * Class NebStatEditionTable
 *
 * @package Neb\Main\Stat
 */
class NebStatEditionTable extends DataManager
{

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'neb_stat_edition';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID'                => array(
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
            ),
            'ID_BITRIXDB'       => array(
                'data_type' => 'integer',
            ),
            'ID_EXALED'         => array(
                'data_type' => 'integer',
            ),
            'TITLE_LIB'         => array(
                'data_type' => 'text'
            ),
            'COUNT_EDITION'     => array(
                'data_type' => 'integer',
            ),
            'COUNT_EDITION_DIG' => array(
                'data_type' => 'integer',
            ),
            'DATE_STAT'         => array(
                'data_type' => 'datetime'
            ),
            'ACTIVE_IN_BITRIX'  => array(
                'data_type' => 'boolean',
                'values'    => array('N', 'Y')
            ),
        );
    }

}