<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Required fields";
$MESS ['FORM_APPLY'] = "Apply";
$MESS ['FORM_ADD'] = "Add";
$MESS ['FORM_ACCESS_DENIED'] = "Web-form access denied.";
$MESS ['FORM_DATA_SAVED1'] = "Thank you. Your application form #";
$MESS ['FORM_DATA_SAVED2'] = " was received.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Web-form module is not installed.";
$MESS ['FORM_NOT_FOUND'] = "Web-form is not found.";
$MESS ['FORM_FEEDBACK_TITLE'] = "Feedback";
$MESS ['FORM_FIELD_EMAIL'] = "E-mail";
$MESS ['FORM_FIELD_SUBJECT'] = "Subject";
$MESS ['FORM_FIELD_MESSAGE'] = "Your message";
$MESS ['FORM_FIELD_CAPTCHA_CODE'] = "Captcha code";
$MESS ['FORM_BUTTON_SUBMIT'] = "Sent!";
?>
