<?
$MESS ['BOOKS_CLOSED_AUTHORIZATION_REQUIRED'] = 'Access to the locked publication is available only for NEB-users. If you are already a NEB-user, enter your Email and password for your personal account. Or you can go through the registration.';
$MESS ['BOOKS_CLOSED_FORM_PASS'] = 'Password';
$MESS ['BOOKS_CLOSED_FORM_ENTER'] = 'Sign in';
$MESS ['BOOKS_CLOSED_FORM_REG'] = 'Registration';
$MESS ['BOOKS_CLOSED_WRONG_PASS'] = 'Incorrect login or password';
$MESS ['BOOKS_CLOSED_ACCESS_DENY'] = 'You do not have access to private publications';
$MESS ['BOOKS_CLOSED_READ'] = 'Read';
$MESS ['BOOKS_CLOSED_SUCCESS_REG'] = 'You have been successfully registered as NEB reader';
$MESS ['BOOKS_CLOSED_GET_FULL_ACCESS'] = 'Get full access';
$MESS ['WARNING_PROGRAM_VIEWING']
    = 'Caution To read the works protected by copyright, must be installed on your computer';
$MESS ['WARNING_PROGRAM_VIEWING2'] = 'viewer';