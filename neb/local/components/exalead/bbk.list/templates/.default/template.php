<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (empty($arResult['ITEMS']) || 'ru' != LANGUAGE_ID)
    return;

$iColumnCount = intval(count($arResult['ITEMS']) / $arParams['COLUMNS']);

?>
<section class="categories-block container">
    <div class="row">
        <ul class="categories-block__list col-md-4">
            <? foreach($arResult['ITEMS'] as $i => $arItem): ?>
                <? if ($i % $iColumnCount == 0 && $i > 0 && $i < $iColumnCount * $arParams['COLUMNS']):?>
                    </ul><ul class="categories-block__list col-md-4">
                <? endif; ?>
                <li class="categories-block__list-kind clearfix">
                    <a class="categories-block__list-link" href="/category/?category_id=<?= $arItem['id'];
                    ?>&category_name=<?= trim($arItem['fullname']); ?>"><?= trim($arItem['fullname']); ?></a>
                </li>
            <? endforeach; ?>
        </ul>
    </div>
</section>