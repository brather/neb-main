<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
/**
 * Created by PhpStorm.
 * User: achechel
 * Date: 24.01.2017
 * Time: 13:11
 */
use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Main\HttpRequest;
use Bitrix\Posting\EmailTable;
use Bitrix\Posting\FileTable;
use Bitrix\Posting\ListRubricTable;

$rq = Application::getInstance()->getContext()->getRequest();
if (!$rq->isAjaxRequest()) die();

Loader::includeModule('subscribe');

class BPosting
{
    static public function create(HttpRequest $request)
    {
        global $USER;

        $token = CUser::GetByID( $USER->GetID() )->Fetch()['UF_TOKEN'];

        if ($token !== $request->get("token"))
            return json_encode ( array('error' => 'Token is not match!') );

        $bPosting = $request->getPost("bposting");

        $arNotification = ListRubricTable::getList([
            'select'  => ['ID'],
            'filter'  => ['CODE' => 'library_notification'],
        ])->fetchRaw();

        $cP = new CPosting();

        $arFields = Array(
            "FROM_FIELD" => $bPosting['fromField'],
            "SUBJECT" => "НЭБ: " . $bPosting['subject'],
            "BODY_TYPE" => "html",
            "STATUS" => "P",
            "BODY" => $bPosting['body'],
            "DIRECT_SEND" => "Y",
            "CHARSET" => "Windows-1251",
            "SUBSCR_FORMAT" => false,
            "RUB_ID" => array((int)$arNotification['ID']), // todo Получить ID рубкири из таблицы b_list_rubric
        );

        if (($postingID = $cP->Add($arFields)) === false)
            return json_encode ( array('error' => $cP->LAST_ERROR) );

        if (is_array($bPosting['postingEmails']) && count($bPosting['postingEmails']) > 0 )
        {
            foreach ($bPosting['postingEmails'] as $email)
            {
                $emailPosting = EmailTable::add(array(
                    "POSTING_ID" => $postingID,
                    "STATUS" => "D",
                    "EMAIL" => $email
                ));

                if (!$emailPosting->isSuccess())
                    return json_encode ( array('error' => $emailPosting->getErrorMessages()) );
            }
        }

        if (is_array($bPosting['postingFiles']) && count($bPosting['postingFiles']))
        {
            foreach ($bPosting['postingFiles'] as $file)
            {
                if ($fileID = CFile::SaveFile(
                    CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . $file),
                    'library-posting-attachments/'
                ))
                {
                    unlink($_SERVER['DOCUMENT_ROOT'] . $file);
                    $filePosting = FileTable::add(array(
                        "POSTING_ID" => $postingID,
                        "FILE_ID" => $fileID
                    ));

                    if (!$filePosting->isSuccess())
                        return json_encode ( array('error' => $filePosting->getErrorMessages()) );
                }
            }
        }

        $res = array(
            "success" => true,
            "posting_id" => $postingID
        );

        return json_encode($res);
    }

    static public function send($postingId)
    {
        $cPosting = new CPosting;
        $rsPosting = $cPosting->GetByID($postingId);
        $arPosting = $rsPosting->Fetch();

        if($arPosting)
        {
            /* Статус "Черновик" или "Остановлен", сменим статус на "В процессе" */
            if ( $arPosting["STATUS"] == "D" || $arPosting["STATUS"] == "W" )
            {
                $cPosting->ChangeStatus( $postingId, "P" );
                $arPosting["STATUS"] = "P";
            }

            /* Когда статус "В процессе" начнем отправку */
            if ($arPosting["STATUS"] == "P")
            {
                $limit = COption::GetOptionString("subscribe", "subscribe_max_emails_per_hit");

                if( ($statusSend = $cPosting->SendMessage($postingId, COption::GetOptionString("subscribe", "posting_interval"), (int)$limit)) === false )
                    return json_encode ( array('error' => $cPosting->LAST_ERROR) );

                /* Успешная отправка */
                if ($statusSend === true)
                    return json_encode ( array('success' => true, "msg" => "Posting send successful.") );

                /* Частичная отправка */
                if ($statusSend === "CONTINUE")
                    return json_encode ( array('success' => true, "remaining" => true) );
            }

            /* Рассылка уже производилась или завершилась с ошибкой, повторная отправка не предусмотрена */
            return json_encode ( array('error' => "Posting [ID:{$postingId}] send with error, create new posting.") );
        }
        else
            return json_encode ( array('error' => "No posting with ID {$postingId}") );
    }
}

switch ($rq->getPost('action'))
{
    case 'create':
        $res = BPosting::create($rq);
        break;
    case 'send':
        $res = BPosting::send($rq->getPost('id'));
        break;
    default:
        $res = "{error: true}";
}

die($res);