<?
global $GLOBAL;
$GLOBAL['PAGE'] = 'MAIN';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Специальная версия сайта НЭБ");
?>
    <section class="b-bookinfo">
        <div class="wrapper">
            <?
/*
            $APPLICATION->IncludeComponent(
                "collection:main.slider",
                "special",
                array(
                    "PAGE_URL" => "/catalog/#BOOK_ID#/",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "IBLOCK_TYPE" => "collections",
                    "IBLOCK_ID" => "6"
                ),
                false
            );
*/
            ?>
			<?$APPLICATION->IncludeComponent("bitrix:news.list", "main_news", array(
				"IBLOCK_TYPE" => "news",
				"IBLOCK_ID" => "9",
				"NEWS_COUNT" => "20",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "",
				"SORT_ORDER2" => "",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array(
					0 => "PREVIEW_TEXT",
					1 => "PREVIEW_PICTURE", 
					2 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "LINK_HEAD",
					1 => "",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"PAGER_TEMPLATE" => "",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
				),
				false
			);?>
            <div class="b-bookinfo_wrapper">

                <?$APPLICATION->IncludeComponent(
                    "exalead:new.list",
                    "special",
                    array(
                        "DATE" => "30",
                        "COUNT" => "3",
                        "PAGE_URL" => "/catalog/#BOOK_ID#/",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3601"
                    ),
                    false
                );?>

                <?$APPLICATION->IncludeComponent(
                    "exalead:popular.list",
                    "special",
                    array(
                        "COUNT" => "3",
                        "PAGE_URL" => "/catalog/#BOOK_ID#/",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600"
                    ),
                    false
                );?>
            </div> <!-- /.b-bookinfo_wrapper -->
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "benefits_special", array(
                    "IBLOCK_TYPE" => "benefits",
                    "IBLOCK_ID" => "1",
                    "NEWS_COUNT" => "20",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "",
                    "SORT_ORDER2" => "",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array(
                        0 => "PREVIEW_TEXT",
                        1 => "PREVIEW_PICTURE",
                        2 => "",
                    ),
                    "PROPERTY_CODE" => array(
                        0 => "LINK",
                        1 => "",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "PAGER_TEMPLATE" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Преимущества",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
                ),
                false
            );?>

        </div><!-- /.wrapper -->
    </section>
    </div><!-- /.homepage -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>