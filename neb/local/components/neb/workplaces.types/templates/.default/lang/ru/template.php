<?php
$MESS['WORKPLACES_TYPES_ID']     = 'ID';
$MESS['WORKPLACES_TYPES_XML_ID'] = 'Символьный код';
$MESS['WORKPLACES_TYPES_VALUE']  = 'Название';
$MESS['WORKPLACES_TYPES_ADD']    = 'Добавить';
$MESS['WORKPLACES_TYPES_SORT']   = 'Сортировка';
$MESS['WORKPLACES_TYPES_COUNT']  = 'Количество ЭЧЗ';
$MESS['WORKPLACES_TYPES_EDIT']   = 'Редактировать';