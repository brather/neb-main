<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Iblock\SectionTable,
    \Bitrix\Main\Application;

class FaqSectionAdd extends CBitrixComponent {
    //region Задание свойств компонента
    private $page = 'template';
    private $iblockId = '';
    private $minCountSymbol = 5;
    private $nameOfSection = '';
    private $sectionId = '';
    private $sortIndex = '500';
    private $arRequest = array();
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        if(!empty($arParams["IBLOCK_ID"])) {
            $this->iblockId = $arParams["IBLOCK_ID"];
        }
        if(!empty($arParams["MIN_COUNT_SYMBOL"])) {
            $this->minCountSymbol = $arParams["MIN_COUNT_SYMBOL"];
        }
        if(!empty($arParams["URL_PAGE"])) {
            $this->arResult["FORM_URL"] = $arParams["URL_PAGE"];
        }
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->_getRequest();
        $this->_getRequestNameSection();
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Проверка формы на ввод данных
    /**
     * Проверка формы на ввод данных
     */
    private function _getRequestNameSection() {
        if($this->arRequest["CONTROL_ADD_SECTION"] == "Y") {
            if(!empty($this->arRequest["NAME_SECTION"])) {
                if(!empty($this->arRequest['SORT_SECTION'])) {
                    $this->sortIndex = $this->arRequest['SORT_SECTION'];
                }
                $this->nameOfSection = $this->_checkString($this->arRequest["NAME_SECTION"]);
                if((int)strlen($this->nameOfSection) > (int)$this->minCountSymbol) {
                    $this->_addSectionToIblock();
                } else {
                    $this->arResult['ERROR'] = Loc::getMessage("ERROR_MIN_COUNT_SYMBOL");
                }
            } else {
                $this->arResult['ERROR'] = Loc::getMessage("ERROR_EMPTY_STRING");
            }
        }
    }
    //endregion
    //region Проверка строки на коррктность
    /** Проверка строки на коррктность
     * @param $string - проверяемая строка
     * @return string
     */
    private function _checkString($string) {
        $string = trim($string);
        $string = stripslashes($string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        return $string;
    }
    //endregion
    //region Функция добавления раздела
    /**
     * Функция добавления раздела
     */
    private function _addSectionToIblock() {
        if($this->_checkExistSection()) {
            $this->arResult['ERROR'] = Loc::getMessage("ERROR_SECTION_EXIST");
        } else {
            $section = new CIBlockSection;
            $paramsTranslite = array("replace_space"=>"-", "replace_other"=>"-");
            $arFields = Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $this->iblockId,
                "NAME" => $this->nameOfSection,
                "CODE" => Cutil::translit($this->nameOfSection, "ru", $paramsTranslite),
                "SORT" => $this->sortIndex
            );
            if($this->sectionId = $section->Add($arFields)) {
                $this->arResult['SUCCESS'] = Loc::getMessage("ADD_SECTION_SUCCESS_1") . $this->sectionId . Loc::getMessage("ADD_SECTION_SUCCESS_2");
            } else {
                $this->arResult['ERROR'] = Loc::getMessage("ADD_SECTION_ERROR")  . ' '  . $section->LAST_ERROR;
            }
        }
    }
    //endregion
    //region Проверка на существование раздела
    /** Проверка на существование раздела
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _checkExistSection() {
        $resSect = SectionTable::GetList(array(
            "filter" => array("IBLOCK_ID" => $this->iblockId, "NAME" => $this->nameOfSection),
            "select" => array("ID")
        ));
        if($obSect = $resSect->Fetch()) {
            return true;
        }
        else {
            return false;
        }
    }
    //endregion
    //region Получение массива $_REQUEST (обертка D7)
    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public function _getRequest() {
        return $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }
    //endregion
}