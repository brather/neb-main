<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160523191400 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "INSERT INTO b_group
       (ID, TIMESTAMP_X, ACTIVE, C_SORT,  ANONYMOUS, NAME,             STRING_ID)
VALUES (15, NOW(),      'Y',     500,    'N',       'Вход в админку', 'admin_panel');"
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql(
            "DELETE FROM b_group WHERE ID = 15;"
        );
    }
}
