<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-headernav_login right">
	<? 
		if($arResult["FORM_TYPE"] == "login")
		{
		?>
			<div class="b-headernav_login right">
				<a title="Регистрация" class="b-headernav_login_lnk" href="/special/registration/" title="Регистрация"><?=Loc::getMessage('AUTH_REISTER_BUTTON');?></a>
				<a title="Войти" class="b-headernav_login_lnk" href="/special/login/" title="Войти"><?=Loc::getMessage('AUTH_LOGIN_BUTTON');?></a>
			</div>
		<?
		} 
		else 
		{
		?>
			<a href="/special/profile/" class="b-headernav_login_lnk <?=strpos($APPLICATION->GetCurPage(), '/special/profile') !== false ? 'current':''?>" title="Личный кабинет"><?=Loc::getMessage('AUTH_LINK_PERSONAL');?></a>
			<a href="/special/?logout=yes" id="auth_login" class="b-headernav_login_lnk <?=strpos($APPLICATION->GetCurPage(), '/special/profile') === false ? 'current':''?>" title="Выйти"><?=Loc::getMessage('AUTH_EXIT');?></a>
		<?
		}
	?>
</div>
