<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LbcLocationplansectionimages
 *
 * @ORM\Table(name="lbc_locationplansectionimages")
 * @ORM\Entity
 */
class LbcLocationplansectionimages extends AbstractLbcImages
{

    /**
     * @var integer
     *
     * @ORM\Column(name="LocationPlanSectionId", type="integer", nullable=false)
     */
    private $locationplansectionid;

    /**
     * Set locationplansectionid
     *
     * @param integer $locationplansectionid
     *
     * @return LbcLocationplansectionimages
     */
    public function setLocationplansectionid($locationplansectionid)
    {
        $this->locationplansectionid = $locationplansectionid;

        return $this;
    }

    /**
     * Get locationplansectionid
     *
     * @return integer
     */
    public function getLocationplansectionid()
    {
        return $this->locationplansectionid;
    }

}
