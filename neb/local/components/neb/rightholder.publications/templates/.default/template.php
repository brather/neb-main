<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>

    <div class="b-addsearch clearfix">
        <form method="get">
            <div class="left">
                C <input type="text" id="date-from" name="date-from" size="10"
                         value="<?= $arResult['dateFrom'] ?>"
                         class="b-text datepicker datepicker-from">
                по <input type="text" name="date-to" id="date-to" size="10"
                          value="<?= $arResult['dateTo'] ?>"
                          class="b-text datepicker datepicker-current">
            </div>
            <br>

            <div class="b-search_field right">


                <input type="text" name="qbook" class="b-search_fieldtb b-text"
                       id="asearch"
                       value="<?= htmlspecialcharsbx($_REQUEST['qbook']) ?>"
                       autocomplete="off"
                       placeholder="Поиск">
                <select name="qbooktype"
                        class="selectpicker">
                    <option <?= $_REQUEST['qbooktype'] == 'all'
                        ? 'selected="selected"' : '' ?>
                        value="all"><?= GetMessage(
                            'RIGHTHOLDER_BOOKS_SEARCH_ALL'
                        ); ?></option>
                    <option <?= $_REQUEST['qbooktype'] == 'Name'
                        ? 'selected="selected"' : '' ?>
                        value="Name"><?= GetMessage(
                            'RIGHTHOLDER_BOOKS_SEARCH_TITLE'
                        ); ?></option>
                    <option <?= $_REQUEST['qbooktype'] == 'Author'
                        ? 'selected="selected"' : '' ?>
                        value="Author"><?= GetMessage(
                            'RIGHTHOLDER_BOOKS_SEARCH_AUTHOR'
                        ); ?></option>
                </select>
                <input type="submit" class="btn btn-primary"
                       value="<?= GetMessage('RIGHTHOLDER_BOOKS_SEARCH'); ?>">
            </div>
            <!-- /.b-search_field-->
        </form>
    </div>
<? if (!empty($arResult['BOOKS'])) { ?>
    <br>
    <input type="submit" class="btn btn-primary" value="Экспорт в excel"
           style="width: 220px;margin-top: 10px;" name="export">
    <br>
    <br>
    <div class="lk-table">
        <div class="lk-table__column" style="width:40%"></div>
        <div class="lk-table__column" style="width:25%"></div>
        <div class="lk-table__column" style="width:15%"></div>
        <div class="lk-table__column" style="width:20%"></div>
        <div class="lk-table__column" style="width:10%"></div>
        <ul class="lk-table__header">
            <li class="lk-table__header-kind"><a <?= SortingExalead(
                    "Name"
                ) ?>><?= GetMessage('RIGHTHOLDER_BOOKS_TITLE'); ?></a></li>
            <li class="lk-table__header-kind"><a <?= SortingExalead(
                    "Author"
                ) ?>><?= GetMessage('RIGHTHOLDER_BOOKS_AUTHOR'); ?></a></li>
            <li class="lk-table__header-kind"><a <?= SortingExalead(
                    "ISBN"
                ) ?>>ISBN</a></li>
            <li class="lk-table__header-kind"><a <?= SortingExalead(
                    "PublishYear"
                ) ?>><?= GetMessage('RIGHTHOLDER_BOOKS_PUBLISH_YEAR'); ?></a>
            </li>
            <li class="lk-table__header-kind"><a <?= SortingExalead(
                    "BX_TIMESTAMP"
                ) ?>><?= GetMessage('RIGHTHOLDER_BOOKS_DATE_ADD'); ?> </a></li>
            <li class="lk-table__header-kind"><a><?= GetMessage(
                        'RIGHTHOLDER_BOOKS_VIEWS'
                    ); ?></a></li>
        </ul>
        <section class="lk-table__body">
            <?
            foreach ($arResult['BOOKS'] as $arBook) {
                ?>
                <ul class="lk-table__row">
                    <li class="lk-table__col"><?= $arBook['Name'] ?></li>
                    <li class="lk-table__col"><?= $arBook['Author'] ?></li>
                    <li class="lk-table__col"><?= $arBook['ISBN'] ?></li>
                    <li class="lk-table__col"><?= $arBook['PublishYear'] ?></li>
                    <li class="lk-table__col"><?= $arBook['BX_TIMESTAMP_'] ?></li>
                    <li class="lk-table__col"><?= $arBook['VIEWS'] ?></li>
                </ul>
            <? } ?>
        </section>
    </div>
<? } ?>