/**
 * Created by achechel on 23.01.2017.
 */
window.addEventListener("DOMContentLoaded", function () {


    window.FRONT = window.FRONT || {};

    var sendPosting = function (postingId) {
        $.ajax({
            /*url: '/api/library-notification/' + postingId + '/send/?token=' + getUserToken(),*/
            url: '/local/components/neb/notification.form/ajax.php?token=' + getUserToken(),
            dataType: 'json',
            method: 'POST',
            data: {
                action: 'send',
                id: postingId
            },
            success: function (data) {
                if (data['remaining'] == true) {
                    sendPosting(postingId);
                } else {
                    location.href = '/profile/notification/history/'
                }
            }
        });
    };

    $(function () {
        var postingForm = $('[data-notification-form]'),
            formInputs = postingForm.find('[type="text"], textarea'),
            submitButton = $('[type="submit"]', postingForm);

        tinymce.init({
            selector: '#postingText',
            hidden_input: false,
            setup: function (editor) {
                editor.on('keyup', function (e) {
                    tinymce.triggerSave();
                    postingForm.trigger('change');
                });
            }
        });

        $(document)
            .on('change', postingForm, function () {
                var canPost = true;
                formInputs.each(function () {
                    var inp = $(this);
                    if (inp.val() == '') {
                        canPost = false;
                    }
                });
                submitButton.prop('disabled', !canPost);
            })
            .on('keyup', formInputs, function () {
                postingForm.trigger('change');
            });

        $('[data-submit-notification]').click(function (e) {
            e.preventDefault();
            var postingData = {
                bposting: {
                    postingEmails: [],
                    postingFiles: [],
                    subject: '',
                    body: ''
                }
            };
            $('select[name="bposting[postingEmails][]"] option:selected').each(function () {
                postingData.bposting.postingEmails.push($(this).val());
            });
            $('input[name="posting_files[]"]').each(function () {
                if ($(this).val()) {
                    postingData.bposting.postingFiles.push($(this).val());
                }
            });
            postingData.bposting.body = tinyMCE.get('postingText').getContent();
            postingData.bposting.subject = $('[data-notification-form] input[name="subject"]').val();
            postingData.bposting.fromField = $('[data-notification-form] input[name="fromField"]').val();
            postingData.action = 'create';


            $.ajax({
                /*url: '/api/library-notification/create/?token=' + getUserToken(),*/
                url: '/local/components/neb/notification.form/ajax.php?token=' + getUserToken(),
                dataType: 'json',
                method: 'POST',
                data: postingData,
                success: function (data) {
                    if (data['success'] !== undefined && data['success'] == true) {
                        $.when(window.FRONT.confirm.handle({
                            confirmTitle: 'Сохранить и отправить',
                            cancelTitle: 'Сохранить',
                            text: 'Оповещение создано.',
                            modalSize: 'modal-md'
                        })).then(function (confirmed) {
                            if (confirmed) {
                                sendPosting(data['posting_id']);
                            }
                        });
                    } else {
                        //console.error(data['error']);
                    }
                }
            });
        });
    });

    var imp1 = document.createElement('script');
    var imp2 = document.createElement('script');
    var imp3 = document.createElement('script');
    var imp4 = document.createElement('script');

    imp1.src = '/local/templates/adaptive/js/file.uploader.js';
    document.body.appendChild(imp1);
    imp2.src = '/local/templates/adaptive/vendor/blueimp-file-upload/js/vendor/jquery.ui.widget.js';
    document.body.appendChild(imp2);
    imp3.src = '/local/templates/adaptive/vendor/blueimp-file-upload/js/jquery.iframe-transport.js';
    document.body.appendChild(imp3);
    imp4.src = '/local/templates/adaptive/vendor/blueimp-file-upload/js/jquery.fileupload.js';
    document.body.appendChild(imp4);
});