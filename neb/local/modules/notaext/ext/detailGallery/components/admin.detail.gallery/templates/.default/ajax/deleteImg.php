<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!check_bitrix_sessid())
    die('Error sessid');

global $USER;
if (!$USER->IsAuthorized())
    die('Error auth');

$ELEMENT_ID = intval($_REQUEST['ELEMENT_ID']);

if ($ELEMENT_ID <= 0)
    die('Error ID');

CModule::IncludeModule("iblock");

$DB->StartTransaction();

if (!CIBlockElement::Delete($ELEMENT_ID)) {
    $DB->Rollback();
    die('Error Delete');
}
