<?

$obNUser = new nebUser();

if (!$obNUser->IsAuthorized())
	return false;

$obNUser->getRole();
$obNUser->getUserGroups();

/*
 * if($obNUser->role != 'user')
 *      $arLibrary = $obNUser->getLibrary();
 */

if($obNUser->role == 'user')
{
	/* меню пользователя */
	$aMenuLinksExt = array(
		array('Profile', '/profile/index.php', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('Orders for scanning', '/profile/scanning/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('search requests', '/profile/searches/', array(),		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_search")),
        array('quotes', '/profile/quote/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_qt")),
		array('bookmarks', '/profile/bookmark/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_bm")),
		array('notes', '/profile/note/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_notes")),
	);
}
else if($obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR)
{
	/* меню библиотекаря */
	$aMenuLinksExt = [
		'profile'           => ['News',               '/profile/index.php',          [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'settings'          => ['Profile settings',   '/profile/edit/',              [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS" => ""]],
		'help'              => ['Help',               '/faq/en/',                    [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS" => "", 'MSGNUM' => 0]],
		//'profile'           => ['Profile',           '/profile/index.php',        [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		//'news'              => ['News',              '/profile/news/',            [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS" => ""]],
		//'exit'              => ['Exit',              '/profile/?logout=yes',      [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS" => "", 'STRONG' => true]],
		//'funds'             => ['Funds',             '/profile/funds/manage/',    [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		//'readers'           => ['Readers',           '/profile/readers/',         [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		//'collections'       => ['Collections',       '/profile/collection/',      [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		//'news'              => ['News',              '/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=3&type=news&lang=ru', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>""]],
		'statistics'        => ['Statistics',         '/profile/statistics/',        [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'collection'        => ['Collections',        '/profile/collection/',        [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'check_eechb'       => ['SECL',               '/profile/check_eechb/',       [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'plan_digitization' => ['Digitization plans', '/profile/plan_digitization/', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'digitization'      => ['Readers orders',     '/profile/digitization/',      [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'feedback'          => ['Feedback',           '/profile/feedback/',          [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'markup'            => ['Markup editions',    '/profile/markup/',           [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>""]],
	];
	// доступ к разметке изданий только при дополнительной группе пользователя "Администратор аналитической росписи"
	if (!in_array(UGROUP_MARKUP_APPROVER, $obNUser->arUserGroups)) {
		unset($aMenuLinksExt['markup']);
	}
}
else if($obNUser->role == UGROUP_LIB_CODE_CONTORLLER)
{
	/* библиотекарь-контролер */
	$aMenuLinksExt = array(
		array(' ', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
		array('Profile settings', '/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
		array('Help', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'MSGNUM' => 0)),
		//array('Выйти', '/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
		array('Profile', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('Statistics', '/profile/statistics/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('SECL', '/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
	);
}
else if($obNUser->role == UGROUP_RIGHTHOLDER_CODE)
{
	/* меню правообладателя	*/
	$aMenuLinksExt = array(
		array('Publications list', '/profile/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => "")),
		//array('My contracts', '/profile/contracts/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT'   => false, 'DEPTH_LEVEL' => 1, "CLASS" => "")),
		array('Contract conclusion', '/profile/conclusion/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT'   => false, 'DEPTH_LEVEL' => 1, "CLASS"  => "")),
		array('Feedback', '/profile/feedback/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT'   => false, 'DEPTH_LEVEL' => 1, "CLASS" => "")),
		array('Reference', '/profile/reference/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT'   => false, 'DEPTH_LEVEL' => 1, "CLASS" => "")),
	);
} elseif ($obNUser->role == UGROUP_OPERATOR_CODE) {
	/* меню оператора НЭБ */
	$sectionRights = $obNUser->getSectionsRights();

	$sectionItems = [
		'libraries'          => ['Libraries',        '/profile/libraries/',        [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"  => ""]],
		'users'              => ['Users',            '/profile/users/',            [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'workplaces'         => ['Workplaces',       '/profile/workplaces/',       [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'workplaces_types'   => ['Workplaces_types', '/profile/workplaces_types/', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'statistics'         => ['Statistics',       '/profile/statistics/',       [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'funds'              => ['Funds',            '/profile/funds/',            [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
		'digitization_list'  => ['Digitization list','/profile/digitization_list/',[], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
		'digitization'       => ['Digitization orders', '/profile/digitization/',  [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
		'digitization_stat'  => ['Digitization statistics', '/profile/digitization_stat/',       [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
		'news'               => ['News',             '/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=11&type=newsNEB&lang=ru&find_section_section=0', [], ['FROM_IBLOCK' => true, 'IS_PARENT'   => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
		'forum'              => ['Forun',            '/forum/',                    [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
		'polls'              => ['Vote users',       '/bitrix/admin/vote_user_votes.php?lang=ru', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
		'rightholders'       => ['Rightholders',     '/profile/rightholders/',     [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'feedback'           => ['Feedback',         '/profile/feedback/',         [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		'notification'       => ['Notification',     '/profile/notification/',     [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
		'markup'             => ['Markup editions',  '/profile/markup/',           [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>""]],
		'faq'			     => ['FAQ',              '/profile/faq/',	           [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
	];

	// доступ к разметке изданий только при дополнительной группе пользователя "Администратор аналитической росписи"
	if (!in_array(UGROUP_MARKUP_APPROVER, $obNUser->arUserGroups)) {
		unset($sectionItems['markup']);
	}

	// если глобавльный оператор - всегда давать доступ
	if (in_array(UGROUP_OPERATOR_CODE, $obNUser->arUserGroups)) {
		$aMenuLinksExt = $sectionItems;
	}
	// иначе - проверять доп.группы
	else {
		$aMenuLinksExt = array_values(array_intersect_key($sectionItems, array_flip($sectionRights)));
	}
}
elseif($obNUser->role == UGROUP_OPERATOR_WCHZ) {
	/* меню оператора ЭЧЗ */
	$aMenuLinksExt = [
		['Workplace',    '/profile/index.php',     [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		['Statistics',   '/profile/statistics/',   [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		['Verification', '/profile/verification/', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		//['Readers',    '/profile/readers/',      [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		//['Users',      '/profile/users/',        [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
		//['New users',  '/profile/users-new/',    [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
	];
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);