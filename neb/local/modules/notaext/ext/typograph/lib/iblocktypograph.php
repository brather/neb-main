<?
namespace Bitrix\NotaExt\Ext\typograph;
require_once(__DIR__ . '/emt.php');

/*
*
* У нас есть arFields, в котором так же есть PROPERTY_VALUES всё нужно проверять и типографировать.
*
*/

class IblockTypograph
{
    function OnBeforeIBlockElementAddHandler(&$arFields)
    {
        if (!self::isIBlockNeedTypograph($arFields['IBLOCK_ID']))
            return true;

        $propList = self::getPropertiesList($arFields['IBLOCK_ID']);

        if (!$propList || empty($propList))
            return true;

        if (in_array('PREVIEW_TEXT', $propList) && strlen($arFields['PREVIEW_TEXT']) > 0)
            $arFields['PREVIEW_TEXT'] = self::processTypograph($arFields['PREVIEW_TEXT']);
        if (in_array('DETAIL_TEXT', $propList) && strlen($arFields['DETAIL_TEXT']) > 0)
            $arFields['DETAIL_TEXT'] = self::processTypograph($arFields['DETAIL_TEXT']);

        foreach ($arFields['PROPERTY_VALUES'] as $propCode => &$propArray) {
            // Если код/id свойства находятся в массиве типографирования - проверяем
            if (in_array($propCode, $propList)) {
                if (!empty ($propArray)) {
                    // В $propArray может быть либо массив $propArray[0]['VALUE']['TEXT']
                    // либо $propArray['VALUE']['TEXT'] - сработает и бля мульти тоже ))

                    if (isset($propArray['VALUE'])) {
                        //Вариант, когда не массив значений, а одно значение. Проверяем на наличие текста и типографируем
                        if (strlen($propArray['VALUE']['TEXT']) > 0) {
                            $propArray['VALUE']['TEXT'] = self::processTypograph($propArray['VALUE']['TEXT']);
                        }
                    } else {
                        // Админка: Хоть единичный, хоть multi - структура одинаковая
                        foreach ($propArray as $propValueKey => &$propValueInfo) {
                            // Если в VALUE не пустой TEXT - типографируем
                            if (strlen($propValueInfo['VALUE']['TEXT']) > 0) {
                                $propValueInfo['VALUE']['TEXT'] = self::processTypograph($propValueInfo['VALUE']['TEXT']);
                            }
                        }
                        unset($propValueInfo);
                    }
                }
            }
        }
        unset($propArray);
    }

    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if (!self::isIBlockNeedTypograph($arFields['IBLOCK_ID']))
            return true;

        $propList = self::getPropertiesList($arFields['IBLOCK_ID']);

        if (!$propList || empty($propList))
            return true;

        if (in_array('PREVIEW_TEXT', $propList) && strlen($arFields['PREVIEW_TEXT']) > 0)
            $arFields['PREVIEW_TEXT'] = self::processTypograph($arFields['PREVIEW_TEXT']);
        if (in_array('DETAIL_TEXT', $propList) && strlen($arFields['DETAIL_TEXT']) > 0)
            $arFields['DETAIL_TEXT'] = self::processTypograph($arFields['DETAIL_TEXT']);

        foreach ($arFields['PROPERTY_VALUES'] as $propCode => &$propArray) {
            // Если код/id свойства находятся в массиве типографирования - проверяем
            if (in_array($propCode, $propList)) {
                if (!empty ($propArray)) {
                    // В $propArray может быть либо массив $propArray[0]['VALUE']['TEXT']
                    // либо $propArray['VALUE']['TEXT'] - сработает и бля мульти тоже ))

                    if (isset($propArray['VALUE'])) {
                        //Вариант, когда не массив значений, а одно значение. Проверяем на наличие текста и типографируем
                        if (strlen($propArray['VALUE']['TEXT']) > 0) {
                            $propArray['VALUE']['TEXT'] = self::processTypograph($propArray['VALUE']['TEXT']);
                        }
                    } else {
                        // Админка: Хоть единичный, хоть multi - структура одинаковая
                        foreach ($propArray as $propValueKey => &$propValueInfo) {
                            // Если в VALUE не пустой TEXT - типографируем
                            if (strlen($propValueInfo['VALUE']['TEXT']) > 0) {
                                $propValueInfo['VALUE']['TEXT'] = self::processTypograph($propValueInfo['VALUE']['TEXT']);
                            }
                        }
                        unset($propValueInfo);
                    }
                }
            }
        }
        unset($propArray);
    }

    /*
    * Узнаёт, нужно ли для указанного инфоблока применять типограф
    *
    * @param integer $iBlockID
    * @return boolean
    */
    function isIBlockNeedTypograph($iBlockID)
    {
        // Проверяем, выбран ли вообще такой инфоблок
        $iblockString = \COption::GetOptionString('notaext', 'typograph_iblock_list');

        // Если ещё никто ничего не сохранял - выходим
        if (strlen($iblockString) <= 0)
            return false;

        $iblockArray = unserialize($iblockString);

        // Если массив пуст - дальше нам так же делать нечего
        if (empty($iblockArray))
            return false;

        if (in_array($iBlockID, $iblockArray)) {
            //Проверяем, выбраны ли свойства для этого инфоблока
            $iBlockPropsString = \COption::GetOptionString('notaext', "typograph_iblock_{$iBlockID}_props");

            if (strlen($iBlockPropsString) <= 0)
                return false;

            $iBlockPropsArray = unserialize($iBlockPropsString);
            if (!empty($iBlockPropsString))
                return true;
            else
                return false;
        }

        return false;
    }

    /*
    * Получаем массив свойств, которые необходимо типографировать
    *
    * @param integer $iBlockID
    * @return array
    */
    function getPropertiesList($iBlockID)
    {
        $returnArray = array();
        $iBlockPropsString = \COption::GetOptionString('notaext', "typograph_iblock_{$iBlockID}_props");

        if (strlen($iBlockPropsString) <= 0)
            return false;

        $propsArray = unserialize($iBlockPropsString);
        if (empty($propsArray))
            return false;

        foreach ($propsArray as $propItem) {
            if (strpos($propItem, '|') !== false) {
                $propArray = explode('|', $propItem);

                // Идентификатор
                $returnArray[] = $propArray[0];
                //Символьный код
                $returnArray[] = $propArray[1];

                continue;
            }

            $returnArray[] = $propItem;
        }

        return $returnArray;
    }

    /*
    * Типографирует текст
    *
    * @param string $text
    * @return string  - если всё ок - вернёт форматированный текст. Если нет - вернёт прежний текст
    */
    function processTypograph($text)
    {
        $typograf = new \EMTypograph();
        $typograf->set_text($text);

        /*$typograf->setup(array(
            'Text.paragraphs' => 'on',
            'Text.breakline' => 'off',
            'OptAlign.oa_oquote' => 'off',
            'OptAlign.oa_oquote_extra' => 'off',
            'OptAlign.oa_obracket_coma' => 'off',
            'Nobr.spaces_nobr_in_surname_abbr' => 'off',
        ));*/

        $arTypographOptions = \Bitrix\NotaExt\Ext\typograph\EmtOptions::getOptions();
        $typograf->setup($arTypographOptions);
        $result = $typograf->apply();

        return $result ? $result : $text;
    }

    function processTypographCURL($text)
    {
        $array = array(
            'text' => $text
        );
        //Соираем cUrl
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, "http://mdash.ru/api.v1.php");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $array);
            $response = curl_exec($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            if ($code != 200)
                return $text;

            $result = json_decode($response, true);
            if ($result['status'] == 'error')
                return $text;
            else
                return $result['result'];
        }

        return $text;
    }
}
