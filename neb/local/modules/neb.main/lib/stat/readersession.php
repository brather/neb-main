<?php
/**
 * User: agolodkov
 * Date: 21.08.2015
 * Time: 11:34
 */

namespace Neb\Main\Stat;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Type\Date;

/**
 * Class ReaderSessionTable
 *
 * @package Neb\Main\Stat
 */
class ReaderSessionTable extends DataManager
{
    const CHECK_TIME = 604800;

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'neb_reader_session';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID'        => array(
                'data_type' => 'string',
                'primary'   => true,
            ),
            'TIMESTAMP' => array(
                'data_type' => 'datetime'
            ),
            'BOOK_ID'   => array(
                'data_type' => 'string',
                'primary'   => true,
            ),
        );
    }

    /**
     * @param string $session
     * @param string $bookId
     *
     * @throws \Exception
     */
    public static function apply($session, $bookId)
    {
        if (empty($session)) {
            throw new \Exception('Session is empty');
        }
        $result = static::getByPrimary(
            array(
                'ID'      => $session,
                'BOOK_ID' => $bookId,
            )
        );
        if ($result->getSelectedRowsCount()) {
            static::update(
                array(
                    'ID'      => $session,
                    'BOOK_ID' => $bookId,
                ),
                array('ID' => $session)
            );
        } else {
            static::add(array('ID' => $session, 'BOOK_ID' => $bookId));
        }
    }

    /**
     * @param string $session
     * @param string $bookId
     * @param null   $checkTime
     *
     * @return bool
     */
    public static function checkSession($session, $bookId, $checkTime = null)
    {
        if (null === $checkTime) {
            $checkTime = static::CHECK_TIME;
        }
        $result = static::getByPrimary(
            array(
                'ID'      => $session,
                'BOOK_ID' => $bookId,
            ),
            array(
                'filter' => array(
                    '>TIMESTAMP' => new Date(
                        ConvertTimeStamp(time() - $checkTime)
                    ),
                )
            )
        );

        return $result->getSelectedRowsCount() > 0 ? true : false;
    }

}