<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$arRequest = $component->getRequest();

CJSCore::Init(array('date'));

?>

<? foreach($arResult['ERROR'] as $sError): ?>
    <?ShowError($sError);?>
<? endforeach; ?>

<form action="<?=$arParams['FEEDBACK_STAT_URL']?>" method="get" class="form-inline nrf p" data-feedback-filter-form>
    <h3>Выберите период</h3>
    <input type="hidden" name="FEEDBACK[ACTION]" value="STAT" />

    С
    <!--<?=Loc::getMessage('FEEDBACK_STAT_DATE_FROM')?>-->
    <div class="form-group">
        <div class="input-group">
            <input
                size="10"
                type="text"
                value="<?=$arRequest['FEEDBACK']['DATE_FROM']?>"
                name="FEEDBACK[DATE_FROM]"
                id="article_pub_date1"
                class="form-control"
            />
            <span class="input-group-btn">
                <a class="btn btn-default" onclick="BX.calendar({node: article_pub_date1, field: article_pub_date1, form: history, bTime: false, value: ''});">
                    <span class="glyphicon glyphicon-calendar"></span>
                </a>
            </span>
        </div>
    </div>
    по
    <!-- <?=Loc::getMessage('FEEDBACK_STAT_DATE_TO')?> -->
    <div class="form-group">
        <div class="input-group">
            <input
                size="10"
                type="text"
                value="<?=$arRequest['FEEDBACK']['DATE_TO']?>"
                name="FEEDBACK[DATE_TO]"
                id="article_pub_date2"
                class="form-control"
            />
            <span class="input-group-btn">
                <a class="btn btn-default" onclick="BX.calendar({node: article_pub_date2, field: article_pub_date2, form: history, bTime: false, value: ''});">
                    <span class="glyphicon glyphicon-calendar"></span>
                </a>
            </span>
        </div>
    </div>
    <div class="form-group">
        <!-- input type="submit" value="<?=Loc::getMessage('FEEDBACK_STAT_SHOW')?>" / -->
        <button class="btn btn-primary"><?=Loc::getMessage('FEEDBACK_STAT_SHOW')?></button>
    </div>
</form>

<? if (!empty($arResult['ITEMS'])): ?>
    <p>
        <?=Loc::getMessage('FEEDBACK_STAT_PERIOD')?>: <?=$arRequest['FEEDBACK']['DATE_FROM']?> - <?=$arRequest['FEEDBACK']['DATE_TO']?><br />
        <?=Loc::getMessage('FEEDBACK_STAT_REGISTERED')?>: <?=$arResult['ITEMS']['REGISTERED']['COUNT']?><br />
        <?=Loc::getMessage('FEEDBACK_STAT_CLOSED')?>: <?=$arResult['ITEMS']['CLOSED']['COUNT']?><br />
        <?=Loc::getMessage('FEEDBACK_STAT_PROCESSED')?>*: <?=$arResult['ITEMS']['PROCESSED']['COUNT']?>
    </p>
    <p>
        <?=Loc::getMessage('FEEDBACK_STAT_HISTORY')?>
    </p>
    <div>
        <a target="_blank" href="#" class="btn btn-default" data-get-stat-report data-onclick="get_stat_report(); return false;">
            <?=Loc::getMessage('FEEDBACK_STAT_EXCEL')?>
        </a>
    </div>

    <script>
        $(function(){
            $(document).on('click','[data-get-stat-report]',function(e){
                e.preventDefault();
                get_stat_report();
            });
            function get_stat_report() {
                date_from = $('#article_pub_date1').val();
                date_to = $('#article_pub_date2').val();
                window.location.href = '<?=$arParams['FEEDBACK_STAT_URL']?>?FEEDBACK[ACTION]=STAT&FEEDBACK[EXPORT]=Y'
                    + '&FEEDBACK[DATE_FROM]=' + date_from + '&FEEDBACK[DATE_TO]=' + date_to;
                return false;
            }
        });
    </script>
<? endif; ?>