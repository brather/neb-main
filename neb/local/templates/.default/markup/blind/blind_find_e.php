<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
				<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		
		<div class="b-plaintext">
			<h2>Расширенный поиск</h2>

			<form action="" class="b-form b-form_common">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<input type="text" name="" class="b-search_fieldtb b-text" id="" value="Станислав Лем">
					</div>
				</div>
				<div class="checkwrapper mt10">
					<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
				</div>
				
				<div class="fieldrow nowrap mt10">
				<div class="fieldcell iblock">
					<label for="settings02">Автор</label>
					<div class="field validate">
						<input type="text" value="" id="settings02" data-minlength="2" name="name" class="input"  autocomplete="off">
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings03">Название</label>
					<div class="field validate">
						<input type="text" value="" id="settings03" data-minlength="2" name="name" class="input"  autocomplete="off">	
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings04">Издательство</label>
					<div class="field validate">
						<input type="text" value="" id="settings04" data-minlength="2" name="name" class="input"  autocomplete="off">	
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings05">Место издательства</label>
					<div class="field validate">
						<input type="text" value="" id="settings05" data-minlength="2" name="name" class="input"  autocomplete="off">	
					</div>
				</div>
			</div>
				<div class="fieldrow nowrap ptop">
					<div class="fieldcell iblock">
						<label for="fb_theme">Доступ</label>
						<div class="field">
							<select name="theme" id="fb_theme" class="js_select w370">
								<option value="1">Все</option>
								<option value="2">Открытый доступ</option>
								<option value="3">Закрытый доступ</option>
							</select>
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings06">Год публикации от</label>
							<div class="field validate">
								<input type="text" value="1700" id="settings06" data-minlength="2" name="name" class="input" >	
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings07">Год публикации до</label>
							<div class="field validate">
								<input type="text" value="2014" id="settings07" data-minlength="2" name="name" class="input">	
							</div>
						</div>
					</div>
				<div class="fieldrow nowrap fieldrowaction">
					<div class="fieldcell ">
						<div class="field clearfix">
							<input type="submit" class="b-search_bth bbox" value="Найти">

						</div>
					</div>
				</div>
			</form>

		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>