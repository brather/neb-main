<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section class="innerwrapper clearfix">
    <div class="b-registration rel">
        <form id="regform"  enctype="application/x-www-form-urlencoded" name="regform" class="b-form b-form_common b-regform" action="/popup/" method="post">

            <p class="note">
                Для завершения процедуры регистрации укажите, пожалуйста, адрес Вашей электронной почты.
            </p>
            <?php if(isset($arResult['ERROR'])) :?>
                <div class="nookresult">
                    <p class="error"><?= $arResult['ERROR']?></p>
                </div>

            <?php endif; ?>
            <div class="fieldrow nowrap"><div class="fieldcell iblock">
                    <em class="hint">*</em>
                    <label for="settings04">Электронная почта</label>
                    <div class="field validate">
                        <em class="error validate">Неверный формат электронной почты</em>
                        <em class="error required">Заполните</em>
                        <input type="text" data-required="true" value="" data-validate="email" id="settings04" data-maxlength="50" data-minlength="2" name="email" class="input">
                    </div>
                </div></div>

            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell ">
                    <div class="field clearfix divdisable">
                        <button class="formbutton" value="1" type="submit">Зарегистрироваться</button>
                        <input type="hidden" value="Регистрация" name="register_submit_button">
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.b-registration-->
</section>