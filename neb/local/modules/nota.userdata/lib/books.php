<?
namespace Nota\UserData;

use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Highloadblock\HighloadBlockTable;
use \Bitrix\NotaExt\Iblock\Element;
use \Nota\Exalead\SearchClient;
use \Nota\Exalead\SearchQuery;

Loader::includeModule('highloadblock');

/**
 * Class Books - работа с избранными изданиями
 * @package Nota\UserData
 */
class Books
{
    /**
     * Получение сущности h-блока
     *
     * @param $HIBLOCK
     * @return Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    private static function getEntity_data_class($HIBLOCK = HIBLOCK_BOOKS_DATA_USERS)
    {
        $hlblock           = HighloadBlockTable::getById($HIBLOCK)->fetch();
        $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

        return $entity_data_class;
    }

    public static function getListCurrentUser($objectId = false, $uid = false)
    {
        global $USER;
        if (!$uid && $USER->IsAuthorized())
            $uid = $USER->GetID();

        if (!(bool)$uid)
            return false;

        $entity_data_class = self::getEntity_data_class();

        $arFilter = array('UF_UID' => $uid);
        if (!empty($objectId))
            $arFilter['=UF_BOOK_ID'] = $objectId;

        $rsData = $entity_data_class::getList(array(
            "select" => array("ID", "UF_BOOK_ID"),
            "filter" => $arFilter,
        ));

        if ($rsData->getSelectedRowsCount() <= 0)
            return false;

        $arResult = array();

        if (!empty($objectId)) {
            $arResult = $rsData->fetch();
        } else {
            while ($arData = $rsData->fetch())
                $arResult[$arData['UF_BOOK_ID']] = $arData['ID'];
        }

        return $arResult;
    }

    /**
     * Добавление избранного издания
     *
     * @param $objectId
     * @param bool $uid
     * @return bool|int|mixed
     */
    public static function add($objectId, $uid = false)
    {
        global $USER;
        if (empty($objectId))
            return false;

        if (!$uid)
            $uid = $USER->GetID();

        $favoriteObject = self::getListCurrentUser($objectId, $uid);
        if ($favoriteObject !== false)
            return $favoriteObject['ID'];

        $entity_data_class = self::getEntity_data_class();

        $arFields = [
            'UF_UID'        => $uid,
            'UF_BOOK_ID'    => $objectId,
            'UF_DATE_ADD'   => new DateTime(),
        ];

        $result = $entity_data_class::add($arFields);
        if ($saveID = $result->getId()) {
            static::addBookData($objectId);
        }
        return $saveID;
    }

    public static function addBookData($bookId)
    {
        if (!Loader::includeModule('nota.exalead'))
            return false;

        $query = new SearchQuery();
        $query->getById($bookId);

        $client = new SearchClient();
        $book = $client->getResult($query);

        $bookDataClass = \NebMainHelper::getHIblockDataClassByName('BooksData');
        $bookSelect    = $bookDataClass::getList(['filter' => ['UF_BOOK_ID' => $bookId]]);
        if ($bookSelect->getSelectedRowsCount() < 1) {
            $addResult = $bookDataClass::add([
                'UF_BOOK_ID'           => $bookId,
                'UF_BOOK_NAME'         => $book['title'],
                'UF_BOOK_AUTHOR'       => $book['authorbook'],
                'UF_BOOK_PUBLISH_YEAR' => $book['publishyear'],
            ]);

            return $addResult->getId();
        }

        return false;
    }

    /**
     * Удаление издания из избранного
     *
     * @param $objectId
     * @return bool
     */
    public static function delete($objectId)
    {
        if (empty($objectId))
            return false;

        $arData = self::getListCurrentUser($objectId);
        if ($arData === false)
            return false;

        $entity_data_class = self::getEntity_data_class();
        $entity_data_class::delete($arData['ID']);
        Collections::removeLinkObject($arData['ID']);
    }

    /**
     * @param $idObject
     * @return bool
     */
    public static function deleteFromCollection($idObject)
    {
        if (!Loader::includeModule('iblock'))
            return false;

        $obIBlockElement = new \CIBlockElement();
        $rsBooks = $obIBlockElement->GetList(
            array(),
            array('PROPERTY_BOOK_ID' => $idObject),
            false,
            false,
            array('IBLOCK_SECTION_ID')
        );
        if ($arBook = $rsBooks->Fetch()) {
            $collection = UsersBooksCollectionDataTable::getList(
                array('filter' => array('UF_COLLECTION_ID' => $arBook['IBLOCK_SECTION_ID']))
            );
            if ($collection = $collection->fetch()) {

                UsersBooksCollectionDataTable::delete($collection['ID']);

                $arBooks = Element::getList(
                    array(
                        'IBLOCK_CODE'             => IBLOCK_CODE_COLLECTIONS,
                        'SECTION_ID'              => intval($collection['UF_COLLECTION_ID']),
                        '!PROPERTY_BOOK_ID_VALUE' => false,
                    ),
                    false,
                    array('PROPERTY_BOOK_ID', 'IBLOCK_ID', 'skip_other')
                );
                foreach ($arBooks['ITEMS'] as $arItem)
                    if ($arItem['PROPERTY_BOOK_ID_VALUE'] != $idObject)
                        Books::add($arItem['PROPERTY_BOOK_ID_VALUE']);
            }
        }
    }

    /**
     * Получение полного количества избаранных изданий у пользователя
     *
     * @return int
     */
    public static function getAllCnt()
    {
        global $USER;
        $entity_data_class = self::getEntity_data_class();
        $arData = $entity_data_class::getList(array(
            "runtime" => array('cnt' => array('expression' => array('COUNT(*)'), 'data_type' => 'integer')),
            "select"  => array('cnt'),
            "filter"  => array('UF_UID' => $USER->GetID()),
        ))->fetch();

        return intval($arData['cnt']);
    }

    /**
     * Получение количества читаемых изданий
     *
     * @return int
     */
    public static function getReadingCnt()
    {
        global $USER;
        $entity_data_class = self::getEntity_data_class();
        $arData = $entity_data_class::getList(array(
            "runtime" => array('cnt' => array('expression' => array('COUNT(*)'), 'data_type' => 'integer')),
            "select" => array('cnt'),
            "filter" => array('UF_UID' => $USER->GetID(), '=UF_READING' => 1),
        ))->fetch();

        return intval($arData['cnt']);
    }

    /**
     * Получение количества прочитанных изданий
     * @return int
     */
    public static function getReadCnt()
    {
        global $USER;
        $entity_data_class = self::getEntity_data_class();
        $arData = $entity_data_class::getList(array(
            "runtime" => array('cnt' => array('expression' => array('COUNT(*)'), 'data_type' => 'integer')),
            "select"  => array('cnt'),
            "filter"  => array('UF_UID' => $USER->GetID(), '=UF_READING' => 2),
        ))->fetch();

        return intval($arData['cnt']);
    }

    /**
     * Удаляет по паре UID + ID, заведомо проверив, реально ли эта запись принадлежит пользователю
     */
    public static function deleteWithTest($id, $uid)
    {
        $entity_data_class = self::getEntity_data_class();
        $toDelete = $entity_data_class::getList(array(
            "select" => array("ID"),
            "filter" => array('ID' => $id, 'UF_UID' => $uid),
        ))->fetch();

        if (!$toDelete)
            return false;

        $entity_data_class::delete($toDelete['ID']);
        Collections::removeLinkObject($toDelete['ID']);

        return true;
    }
}