<?php
namespace Neb\Main\Library;

use Bitrix\Main\Entity;

class NebLibsCityTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'neb_libs_city';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField(
                'ID', array(
                    'primary'      => true,
                    'autocomplete' => true
                )
            ),
            new Entity\IntegerField('UF_LIB_AREA'),
            new Entity\ReferenceField(
                'LIB_AREA',
                'Neb\Main\Library\NebLibsAreaTable',
                array('=this.UF_LIB_AREA' => 'ref.ID')
            ),
            new Entity\StringField('UF_CITY_NAME'),
            new Entity\IntegerField('UF_IN_FAVORITES'),
            new Entity\IntegerField('UF_IN_LIST'),
            new Entity\StringField('UF_POS'),
            new Entity\IntegerField('UF_SORT'),
        );
    }

    public static function getByWokplaces()
    {
        $queryString = "
            SELECT
              DISTINCT city.*
            FROM b_iblock_element bie
              JOIN b_iblock_element_prop_s7 bieps7 ON bieps7.IBLOCK_ELEMENT_ID = bie.ID
              JOIN neb_libs nl ON nl.ID = bieps7.PROPERTY_23
              JOIN neb_libs_city city ON city.ID = nl.UF_CITY
            WHERE
              bie.IBLOCK_ID = %d AND bie.ACTIVE = 'Y'
            ORDER BY
              city.UF_CITY_NAME";
        $queryString = sprintf($queryString, IBLOCK_ID_WORKPLACES);

        return static::getEntity()->getConnection()->query($queryString);
    }


    public static function getByCityLib()
    {
        $queryString = "
            SELECT
              DISTINCT city.ID, city.*
            FROM b_iblock_element bie
              JOIN b_iblock_element_prop_s4 bieps4 ON bieps4.IBLOCK_ELEMENT_ID =  bie.ID
              JOIN neb_libs nl ON nl.ID = bieps4.PROPERTY_25
              JOIN neb_libs_city city ON city.ID = nl.UF_CITY
            WHERE
              bie.IBLOCK_ID = %d AND bie.ACTIVE = 'Y'
            ORDER BY
              city.UF_CITY_NAME";

        $queryString = sprintf($queryString, IBLOCK_ID_LIBRARY);

        return static
            ::getEntity()
            ->getConnection()
            ->query($queryString);
    }
}
