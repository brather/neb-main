<?php
$MESS["YEAR_OF_PUBLICATION"] = 'года издания';
$MESS["RESET_FILTER"] = 'Убрать фильтр';
$MESS["FROM"] = 'От';
$MESS["TO"] = 'До';
$MESS["COLLECTION"] = 'Коллекция';
$MESS["SEARCH_EXACT_PHRASE"] = 'Искать по точной фразе';
$MESS["SEARCH_WITHIN_RESULTS"] = 'Искать в найденном';
$MESS["LIB_SEARCH_FORM_TEMPLATE_AUTHORSBOOK"] = 'Авторы';
$MESS["PUBLISHYEAR"] = 'Год публикации';
$MESS["SEARCH_CATEGORY"] = 'Рубрика';
$MESS[""] = '';