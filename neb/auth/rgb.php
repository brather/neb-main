<?php
/**
 * Refactoring by D-Efremov
 * Date: 25.10.2016
 * Time: 15:00
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once $_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/CAS-1.3.3/CAS.php';

use \Bitrix\Main\Loader,
    \Bitrix\Main\UserTable,
    \Nota\UserData\rgb;

class RgbWebService {

    private $readerNumber, $arRgbUser = [], $arNebUser = [];

    public function __construct()
    {
        Loader::includeModule('nota.userdata');
    }

    public function main() {
        $this->getReaderNumber();
        $this->getRgbUser();
        $this->getNebUser();
        $this->registerUser();
        $this->authUser();
        SocialServicesAuth::JSRedirect();
    }

    /**
     * Получение номера ЧБ
     */
    private function getReaderNumber() {
        phpCAS::setDebug();
        phpCAS::client(CAS_VERSION_2_0, 'passport.rsl.ru/auth', 443, '');
        phpCAS::setNoCasServerValidation();
        phpCAS::forceAuthentication();

        if(!phpCAS::hasAttribute('readerNumber') || !phpCAS::getAttribute('readerNumber')) {
            die('Ошибка получения данных!');
        }
        else {
            $this->readerNumber = phpCAS::getAttribute('readerNumber');
        }
    }

    /**
     * Получает пользователя РГБ по номечу ЕЭЧБ
     * @throws Exception
     */
    private function getRgbUser() {
        $obRgb = new rgb();
        $arRgbUser = $obRgb->usersFind('reader_number', $this->readerNumber);
        if (!empty($arRgbUser['users'][0])) {
            $this->arRgbUser = $arRgbUser['users'][0];
        } else {
            die('Пользователь с данным номером читательского билета не найден в РГБ!');
        }
    }

    /**
     * Получает пользователя из БД по номеру РГБ или email
     */
    private function getNebUser() {

        $arFilter = [];
        $arFilter['LOGIC'] = 'OR';
        $arFilter[] = ['=UF_RGB_USER_ID' => $this->arRgbUser['uid']];
        if (!empty($this->arRgbUser['email']))
            $arFilter[] = ['=EMAIL' => $this->arRgbUser['email']];

        $this->arNebUser = UserTable::getList([
            'filter' => [$arFilter],
            'select' => ['ID', 'EMAIL', 'UF_RGB_USER_ID', 'UF_NUM_ECHB']
        ])->fetch();
    }

    /**
     * Регистрация пользователя
     */
    private function registerUser() {

        // обновить
        if ($this->arNebUser['ID'] > 0 && empty($this->arNebUser['UF_RGB_USER_ID'])) {

            $nebUser = new nebUser($this->arNebUser['ID']);
            $arUser  = $nebUser->getUser();

            $sBirthday = DateTime::createFromFormat('H:i:s d/m/Y', $this->arRgbUser['birthday'])->format('d.m.Y');

            $arFields = [
                "LAST_NAME"         => $arUser['LAST_NAME']   ? : $this->arRgbUser['lastname'],
                "NAME"              => $arUser['NAME']        ? : $this->arRgbUser['firstname'],
                "SECOND_NAME"       => $arUser['SECOND_NAME'] ? : $this->arRgbUser['middlename'],
                'PERSONAL_BIRTHDAY' => $arUser['PERSONAL_BIRTHDAY'] ? : $sBirthday,
                "LID"               => SITE_ID,
                "UF_RGB_USER_ID"    => $this->arRgbUser['uid'],
                "UF_RGB_CARD_NUMBER"=> $this->arRgbUser['reader_number'],
                'UF_LIBRARY'        => !empty($arUser['UF_LIBRARY']) ? $arUser['UF_LIBRARY'] : RGB_LIB_ID,
                'UF_LIBRARIES'      => !empty($arUser['UF_LIBRARIES']) ? $arUser['UF_LIBRARIES'] : [RGB_LIB_ID]
            ];

            $bUpdate = $nebUser->Update($this->arNebUser['ID'], $arFields);

            if ($bUpdate) {
                $nebUser->setStatus(nebUser::USER_STATUS_VERIFIED);
                $this->authUser();
            } else{
                die($nebUser->LAST_ERROR);
            }
        }
        // зарегистрировать
        elseif(intval($this->arNebUser['ID']) <= 0) {

            $nebUser = new nebUser();

            $sBirthday = DateTime::createFromFormat('H:i:s d/m/Y', $this->arRgbUser['birthday'])->format('d.m.Y');
            $sPass = randString(8);

            $arFields = [
                "LAST_NAME"         => $this->arRgbUser['lastname'],
                "NAME"              => $this->arRgbUser['firstname'],
                "SECOND_NAME"       => $this->arRgbUser['middlename'],
                'PERSONAL_BIRTHDAY' => $sBirthday,
                "EMAIL"             => $this->arRgbUser['email'],
                "LOGIN"             => $this->arRgbUser['email'],
                "LID"               => SITE_ID,
                "ACTIVE"            => "Y",
                "PASSWORD"          => $sPass,
                "CONFIRM_PASSWORD"  => $sPass,
                "UF_REGISTER_TYPE"  => 39,
                "UF_RGB_USER_ID"    => $this->arRgbUser['uid'],
                "UF_RGB_CARD_NUMBER"=> $this->arRgbUser['reader_number'],
                'UF_LIBRARY'        => RGB_LIB_ID,
                'UF_LIBRARIES'      => [RGB_LIB_ID]
            ];

            // в случае отсутствия email перенаправляем на страницу для его заполнения
            if (empty($arFields['EMAIL'])) {

                $_SESSION['ESIA_REGISTER'] = $arFields;
                SocialServicesAuth::JSRedirect('/auth/reg.php');
            }
            // регистритуем пользователя
            else {
                if ($arFields['ID'] = $nebUser->Add($arFields)) {
                    $nebUser->setStatus(nebUser::USER_STATUS_VERIFIED);
                    $nebUser->SendUserInfo($arFields['ID'], SITE_ID, "Приветствуем Вас как нового пользователя НЭБ!");

                    $this->arNebUser = $arFields;
                    $this->authUser();
                } else {
                    die($nebUser->LAST_ERROR);
                }
            }
        }
    }

    /**
     * Авторизация пользователя
     */
    private function authUser() {

        if ($this->arNebUser['ID'] <= 0)
            return ;

        $obUser = new CUser();
        $obUser->Authorize($this->arNebUser['ID'], true);

        SocialServicesAuth::JSRedirect('/profile/');
    }
}

$obRgbWebService = new RgbWebService();
$obRgbWebService->main();