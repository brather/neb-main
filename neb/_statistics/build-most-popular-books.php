<?php
/**
 * Скрипт отбора популярных изданий
 * Перенесен в \Neb\Main\Agents\PopularBooks::save();
 */
if ('cli' !== php_sapi_name()) {
    die('Only CLI');
}
/**
 * User: agolodkov
 * Date: 07.05.2015
 * Time: 17:27
 */
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

$documentRoot = realpath(dirname(__FILE__) . '/..');
$_SERVER['DOCUMENT_ROOT'] = $documentRoot;
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');
while (ob_get_level()) {
    ob_end_clean();
}
/**
 * @var CDatabase $DB
 */
$startDate = date('Y-m-d H:i:s', time() - (86400 * 30 * 3));
$topCount = 1000;
$exaleadQueryBatch = 50;

$getBooksWithFiles = function ($bookIds) use ($exaleadQueryBatch) {
    NebMainHelper::includeModule('nota.exalead');
    $digitizedBooks = [];
    $offset = 0;
    do {
        $ids = array_slice($bookIds, $offset, $exaleadQueryBatch);
        if (!empty($ids)) {
            $query = new \Nota\Exalead\SearchQuery();
            $query->getByArIds($ids);
            $query->setQuery('filesize>0 AND (' . $query->getParameter('q') . ')');
            $query->setPageLimit($exaleadQueryBatch);
            $query->setNextPagePosition($offset);

            $client = new \Nota\Exalead\SearchClient();
            $result = $client->getResult($query);
            if (!empty($result['ITEMS'])) {
                foreach ($result['ITEMS'] as $item) {
                    if (!empty($item['id'])) {
                        $digitizedBooks[$item['id']] = true;
                    }
                }
            }
        }
        $offset += $exaleadQueryBatch;
    } while ($offset < count($bookIds));

    return $digitizedBooks;
};
$buildTopCountInsertList = function ($topCount, $libraryId = null) use ($getBooksWithFiles) {
    global $DB;
    if (null === $libraryId) {
        $query
            = "
SELECT
    `book_view`.`BOOK_ID`        AS `BOOK_ID`,
    COUNT(`book_view`.`BOOK_ID`) AS `VIEWS`
  FROM
    `neb_stat_book_view` `book_view`
  GROUP BY
    `book_view`.`BOOK_ID`
  ORDER BY
    `VIEWS` DESC
  LIMIT 0, $topCount;";
    } else {
        $query
            = "
SELECT
    `book_view`.`BOOK_ID`                                    AS `BOOK_ID`,
    `book_view`.`LIB_ID`                                     AS `LIB_ID`,
    COUNT(`book_view`.`BOOK_ID`)                             AS `VIEWS`
  FROM
    `neb_stat_book_view` `book_view`
  WHERE
    `book_view`.`LIB_ID` = $libraryId
  GROUP BY
    `book_view`.`BOOK_ID`
  ORDER BY
    `VIEWS` DESC
  LIMIT 0, $topCount;";
    }

    $rs = $DB->Query($query, true);
    $values = [];
    $items = [];
    while ($item = $rs->Fetch()) {
        $items[$item['BOOK_ID']] = $item;
    }
    $digitizedBooks = $getBooksWithFiles(array_keys($items));
    foreach ($items as $bookId => $item) {
        $item['BOOK_ID'] = '"' . $item['BOOK_ID'] . '"';
        $item['VIEWS'] = (integer)$item['VIEWS'];
        if (isset($item['LIB_ID'])) {
            $item['LIB_ID'] = (integer)$item['LIB_ID'];
        }
        if(!$libraryId) {
            unset($item['LIB_ID']);
        }
        $item['HAS_FILE'] = isset($digitizedBooks[$bookId])?1:0;

        $values[] = '(' . implode(',', $item) . ')';
    }

    return implode(",\n", $values);
};
$commonList = $buildTopCountInsertList($topCount);
$queries = array(
    'create table'              => <<<SQL
CREATE TABLE IF NOT EXISTS neb_stat_book_view_most_popular (
  `BOOK_ID`   VARCHAR(255)
              COLLATE utf8_unicode_ci NOT NULL,
  `LIB_ID`    INT(11),
  `VIEWS`     INT(11)                 NOT NULL,
  `HAS_FILE`  TINYINT(1),
  PRIMARY KEY (`LIB_ID`, `VIEWS`, `BOOK_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  COMMENT = 'Статистика просмотра книг(топ популярных)';
SQL
    ,
    'common transaction start'  => <<<SQL
START TRANSACTION;
SQL
    ,
    'common delete'             => <<<SQL
DELETE FROM neb_stat_book_view_most_popular
WHERE LIB_ID IS NULL;
SQL
    ,
    'common fill table'         => <<<SQL
REPLACE INTO neb_stat_book_view_most_popular(BOOK_ID, VIEWS, HAS_FILE)
  VALUES $commonList;
SQL
    ,
    'common transaction commit' => <<<SQL
COMMIT;
SQL
    ,
);

/** Заполняем просмотры по библиотекам */
CModule::IncludeModule('iblock');
$libraries = CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => 4, 'ACTIVE' => 'Y'),
    false,
    false,
    array('PROPERTY_LIBRARY_LINK')
);
$libIds = array();
while ($library = $libraries->Fetch()) {
    if (!empty($library['PROPERTY_LIBRARY_LINK_VALUE'])) {
        $libIds[] = $library['PROPERTY_LIBRARY_LINK_VALUE'];
    }
}
$nebLibResult = \Neb\Main\Library\LibsTable::getList(
    array('filter' => array('ID' => $libIds))
);
while ($library = $nebLibResult->Fetch()) {
    if (!$libraryId = $library['UF_ID']) {
        continue;
    }
    $queries['library ' . $libraryId . ' start transaction']
        = <<<SQL
START TRANSACTION;
SQL;
    $queries['library ' . $libraryId . ' delete']
        = <<<SQL
DELETE FROM neb_stat_book_view_most_popular
WHERE LIB_ID = $libraryId;
SQL;
    if($libraryList = $buildTopCountInsertList($topCount, $libraryId)) {
        $queries['library ' . $libraryId . ' fill table']
            = <<<SQL
REPLACE INTO neb_stat_book_view_most_popular (BOOK_ID, LIB_ID, VIEWS, HAS_FILE)
  VALUES $libraryList;
SQL;
    }

    $queries['library ' . $libraryId . ' transaction commit']
        = <<<SQL
COMMIT;
SQL;
}
$timeStart = microtime(true);
$affectedRows = 0;
foreach ($queries as $queryName => $query) {
    $start = microtime(true);
    $result = $DB->Query($query, true);
    if (!$result) {
        echo 'Error: ', $DB->GetErrorMessage(), PHP_EOL,
        'Query: ', $query, PHP_EOL;
        die;
    }
    echo 'Query "' . $queryName . '": ',
    PHP_EOL, 'AffectedRows - ', $result->AffectedRowsCount(),
    PHP_EOL, 'Time - ', microtime(true) - $start, ' seconds',
    PHP_EOL;
    $affectedRows += $result->AffectedRowsCount();
}
echo 'TOTAL Time: ', microtime(true) - $timeStart, ' seconds', PHP_EOL;
echo 'TOTAL AffectedRows: ', $affectedRows, PHP_EOL;
echo PHP_EOL;