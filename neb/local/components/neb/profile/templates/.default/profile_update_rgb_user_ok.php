<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var CMain $APPLICATION
 */
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('UPDATE_RGB_TITLE_BROWSER'));
?>

<section class="innersection innerwrapper clearfix">
    <div class="b-registration rel">
        <h2 class="mode"><?= Loc::getMessage('UPDATE_RGB_TITLE'); ?></h2>
    </div>
</section>