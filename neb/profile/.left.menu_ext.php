<?
if (!$USER->IsAuthorized())
	return false;

$obNUser = new nebUser();
$obNUser->getRole();

/*if($obNUser->role != 'user')
$arLibrary =	$obNUser->getLibrary();
*/

if($obNUser->role == 'user')
{
	/* массиы меню пользователя*/

	$aMenuLinksExt = array(
		array(' ', '/profile/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
		array('Заказы на сканирование', '/profile/scanning/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
		array('Настройки профиля', '/profile/edit/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
		array('Помощь', '/faq/', array(), 									array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
		//array('Выйти', '/profile/?logout=yes', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
		array('Личный кабинет', '/profile/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('моя библиотека', '/profile/my-library/', array(),			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_lb")),
		array('поисковые запросы', '/profile/searches/', array(),			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_search")),
		array('цитаты', '/profile/quote/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_qt")),
		array('закладки', '/profile/bookmark/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_bm")),
		array('заметки', '/profile/note/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_notes")),
	);
}
else if($obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR)
{
	/* массиы меню библиотекаря*/

	$aMenuLinksExt = array(
		array(' ', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
		array('Настройки профиля', '/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
		array('Помощь', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'MSGNUM' => 0)),
		array('Кабинет', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		//array('Новости', '/profile/news/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
		//array('Выйти', '/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
		array('Статистика', '/profile/statistics/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		//array('Фонды', '/profile/funds/manage/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		//array('Читатели', '/profile/readers/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		//array('Коллекции', '/profile/collection/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		//array('Новости', '/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=3&type=news&lang=ru', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('Выставки', '/profile/collection/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('Новости', '/profile/news/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('Оцифровка', '/profile/plan_digitization/', array(), 	array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('ЕЭЧБ', '/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
	);
}
else if($obNUser->role == UGROUP_LIB_CODE_CONTROLLER)
{
	/* массиы меню библиотекаря-контролера*/

	$aMenuLinksExt = array(
		array(' ', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
		array('Настройки профиля', '/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
		array('Помощь', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'MSGNUM' => 0)),
		//array('Выйти', '/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
		array('Кабинет', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('Статистика', '/profile/statistics/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('ЕЭЧБ', '/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
	);
}
else if($obNUser->role == UGROUP_RIGHTHOLDER_CODE)
{
	/* Массив меню правообладателя	*/
	$aMenuLinksExt = array(
		array(' ', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
		array('Настройки профиля', '/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
		array('Помощь', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
		//array('Выйти', '/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
		array('Личный кабинет', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('Работа с фондами', '/profile/work-fonds/', array(),	array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		array('Статистика', '/profile/statistics/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
	);
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);