<?
require(dirname(__FILE__)
    . '/../../../../template-parts/lang/ru/rh-common.php');

$MESS["REGISTER_HEADER_ENTITY_FIELDS"] = 'Наименование юридического лица';
$MESS["REGISTER_FIELD_ENTITY_ADDRESS"] = 'Адрес места нахождения';
$MESS["REGISTER_FIELD_ENTITY_ADDRESS_LEGAL"] = 'юридический';
$MESS["REGISTER_FIELD_ENTITY_ADDRESS_MAIL"] = 'почтовый';
$MESS["REGISTER_FIELD_ENTITY_INN"] = "ИНН юридического лица";
$MESS["REGISTER_FIELD_ENTITY_KPP"] = "КПП юридического лица";
$MESS["REGISTER_FIELD_ENTITY_OKPO"] = "ОКПО юридического лица";
$MESS["REGISTER_FIELD_ENTITY_OGRN"] = "ОГРН юридического лица";
$MESS["REGISTER_FIELD_ENTITY_OKATO"] = "ОКАТО (ОКТМО) юридического лица";
$MESS["REGISTER_FIELD_ENTITY_KBK"] = "КБК юридического лица";
$MESS["REGISTER_FIELD_ENTITY_POSITION_HEAD"] = 'Должность руководителя (полномочного представителя) ';
$MESS["REGISTER_FIELD_ENTITY_FIO_HEAD"] = 'ФИО руководителя (полномочного представителя)';
$MESS["REGISTER_FIELD_ENTITY_CONTACT_INFO"] = 'ФИО контактного лица, полномочия контактного лица (Устав, доверенность, пр.)';
$MESS["REGISTER_FIELD_ENTITY_CONTACT_PHONE"] = 'Телефон контактного лица';
$MESS["REGISTER_FIELD_ENTITY_CONTACT_MOBILE"] = 'Мобильный телефон контактного лица';
