<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('nota.exalead');
use Nota\Exalead\RslViewer;

$book_id = trim($_REQUEST['book_id']);
$search = urlencode(str_replace(" ", "|", trim(urldecode($_REQUEST['search_text']))));

$Viewer = new RslViewer($book_id);
$result = $Viewer->getPageSearch($search);

$data = json_decode($result, true);
if (!$data) $data = array();

$result = array();
foreach ($data as $d):
    $result = array_merge($result, $d['pages']);
endforeach;

$result = array_unique($result);
sort($result);

?>
<? if (count($result) > 0): ?>

    <? foreach ($result as $page): ?>
        <div><a p='<?= $page ?>'><span><?= $page ?></span> страница</a></div>
    <? endforeach; ?>

<? else: ?>
    <div>Не найдено ничего.</div>
<? endif; ?>

