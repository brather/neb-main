<?php
namespace BitrixBundle\Entity\Posting;


use Doctrine\ORM\Mapping as ORM;

/**
 * BPostingEmail
 *
 * @ORM\Table(name="b_posting_file", uniqueConstraints={@ORM\UniqueConstraint(name="UK_POSTING_POSTING_FILE", columns={"POSTING_ID", "FILE_ID"})})
 * @ORM\Entity
 */
class BPostingFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="POSTING_ID", type="integer", nullable=false)
     */
    private $postingId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FILE_ID", type="integer", nullable=true)
     * �������� ������� ID
     * @ORM\Id
     */
    private $fileId;

    /**
     * @return int
     */
    public function getPostingId()
    {
        return $this->postingId;
    }

    /**
     * @param int $postingId
     */
    public function setPostingId($postingId)
    {
        $this->postingId = $postingId;
    }

    /**
     * @return int
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * @param int $fileId
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;
    }
}
