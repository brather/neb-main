<?php
/**
 * User: agolodkov
 * Date: 25.09.2015
 * Time: 10:01
 */

/**
 * Class CreateBookRequestComponent
 */
class CreateBookRequestComponent
    extends \Neb\Main\Component\CreateBooksComponent
{


    /**
     * @param $params
     */
    protected function _prepareRequestParams(&$params)
    {
        $params['books'] = array();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['books'])) {
                $params['books'] = $_POST['books'];
            } else {
                $requestFields = array(
                    'ID'           => 'Id',
                    'AUTHOR'       => 'Author',
                    'NAME'         => 'Name',
                    'PUBLISH_YEAR' => 'PublishYear',
                    'BBK'          => 'BBKText',
                    'ISBN'         => 'ISBN',
                    'FILE_PDF'     => 'pdfLink',
                );
                $fields = array();
                foreach ($requestFields as $requestName => $fieldName) {
                    if (isset($_POST[$requestName])) {
                        $fields[$fieldName] = $_POST[$requestName];
                    }
                }
                $params['books'][] = $fields;
            }
        }
    }


}