
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right">
						<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix ">
	
	<div class="b-mainblock left">
		<div class="b-searchresult">
			<ul class="b-profile_nav">
				<li>
					<a href="#" class="b-profile_navlk js_profilemenu ">личный кабинет</a>
					<ul class="b-profile_subnav">
						<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
						<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
					</ul>
				</li>
				<li><a class="" href="#">статистика</a></li>
				<li><a href="#" class="current">фонды</a></li>
				<li><a href="#" class=" ">читатели</a></li>
				<li><a href="#" class="">коллекции</a></li>
				<li><a href="#" class="">новости</a></li>
			</ul>                 
		</div><!-- /.b-searchresult-->
		<div class="b-breadcrumb">
			<span>Разделы</span>
			<ul class="b-breadcrumblist">
				<li><span class="right iblock">Книг в плане на оцифровку: 777</span><a href="#">Фонды библиотеки для оцифровки</a></li>
				<li><a href="#" class="right warning">Внимание! У 12 произведений истекает дата оцифровки</a><a href="#" class="current">План оцифровки</a></li>
			</ul>
			
		</div><!-- /.b-breadcrumb-->
		<div class="b-search_field b-search_digital">
			<div class="clearfix">
				<input type="text" name="" class="b-search_fieldtb b-text" id="asearch" value="" autocomplete="off" data-src="search.php?session=MY_SESSION&moreparams=MORE_PARAMS">
				<select name="searchopt" id=""class="js_select b_searchopt">
					<option value="1">по всем полям</option>
					<option value="2">по дате</option>

				</select>
				<input type="submit" class="b-search_bth bbox" value="Найти">
			</div>	

		</div><!-- /.b-search_field-->
		<div class="b-add_digital js_digital">
			
			<table class="b-usertable">
				<tr>
					<th class="autor_cell"><a href="#" class="sort up">Автор</a></th>
					<th class="namedig_cell"><a href="#" class="sort up">Название / Описание / Есть ли в планах на оцифровку</a></th>
					<th class="plan_cell"><a href="#" class="">Включить <br>в план </a></th>

				</tr>
				<tr>
					<td>Горшков, Евгений Андреевич</td>
					<td class="pl15">Становление социальной психологии США
						<div class="b-digital_act">
							<a href="#" class="b-digital_desc">Описание</a>
						</div>
					</td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>					
				</tr>
				<tr class="scrolled">
					<td colspan="3">
						<div class="b-infobox rel b-infoboxdescr"  data-link="descr">
							<a href="#" class="close"></a>							
							<div class="b-infoboxitem">
								<span class="tit iblock">Автор: </span>
								<span class="iblock val">Горшков, Евгений Андреевич</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Неконтролируемое имя: </span>
								<span class="iblock val">Институт психологии Российской академии наук</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие: </span>
								<span class="iblock val">Становление социальной психологии США : автореферат дис. ... кандидата психологических наук : 19.00.01</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Выходные данные: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Физическое описание: </span>
								<span class="iblock val">23 с.</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">Общая психология, психология личности, история психологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">История психологии -- История психологии в Соединенных Штатах Америки (США) -- История психологии в новейшее время -- Психологические направления</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Ключевые слова: </span>
								<span class="iblock val">социальная психология</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Хранение:</span>
								<span class="iblock val">9 11-3/12;</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Электронный адрес:</span>
								<span class="iblock val">Электронный ресурс</span>
							</div>							
						</div><!-- /b-infobox -->
						<div class="b-infobox rel" data-link="digital">
							<a href="#" class="close"></a>
							<div class="b-infoboxitem b-infoboxmain">
								<span class="tit iblock">Оцифровывает</span>
								<span class="iblock val"><strong>Российская государственная библиотека</strong><span class="date">до 20.12.2016</span></span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Автор: </span>
								<span class="iblock val">Горшков, Евгений Андреевич</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Неконтролируемое имя: </span>
								<span class="iblock val">Институт психологии Российской академии наук</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие: </span>
								<span class="iblock val">Становление социальной психологии США : автореферат дис. ... кандидата психологических наук : 19.00.01</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Выходные данные: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Физическое описание: </span>
								<span class="iblock val">23 с.</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">Общая психология, психология личности, история психологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">История психологии -- История психологии в Соединенных Штатах Америки (США) -- История психологии в новейшее время -- Психологические направления</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Ключевые слова: </span>
								<span class="iblock val">социальная психология</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Хранение:</span>
								<span class="iblock val">9 11-3/12;</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Электронный адрес:</span>
								<span class="iblock val">Электронный ресурс</span>
							</div>							
						</div><!-- /b-infobox -->
					</td>
				</tr>
				<tr>
					<td>Доккинс Ричард</td>
					<td class="pl15" >Эгоистичный ген
						<div class="b-digital_act">
							<a href="#" class="b-digital_desc">Описание</a>
							<a href="#" class="b-digital_desc b-digital">На оцифровке</a>
						</div>
					</td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint del"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>					
				</tr>
				<tr class="scrolled">
					<td colspan="3">
						<div class="b-infobox rel b-infoboxdescr"  data-link="descr">
							<a href="#" class="close"></a>							
							<div class="b-infoboxitem">
								<span class="tit iblock">Автор: </span>
								<span class="iblock val">Горшков, Евгений Андреевич</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Неконтролируемое имя: </span>
								<span class="iblock val">Институт психологии Российской академии наук</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие: </span>
								<span class="iblock val">Становление социальной психологии США : автореферат дис. ... кандидата психологических наук : 19.00.01</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Выходные данные: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Физическое описание: </span>
								<span class="iblock val">23 с.</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">Общая психология, психология личности, история психологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">История психологии -- История психологии в Соединенных Штатах Америки (США) -- История психологии в новейшее время -- Психологические направления</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Ключевые слова: </span>
								<span class="iblock val">социальная психология</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Хранение:</span>
								<span class="iblock val">9 11-3/12;</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Электронный адрес:</span>
								<span class="iblock val">Электронный ресурс</span>
							</div>							
						</div><!-- /b-infobox -->
						<div class="b-infobox rel" data-link="digital">
							<a href="#" class="close"></a>
							<div class="b-infoboxitem b-infoboxmain">
								<span class="tit iblock">Оцифровывает</span>
								<span class="iblock val"><strong>Российская государственная библиотека</strong><span class="date">до 20.12.2016</span></span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Автор: </span>
								<span class="iblock val">Горшков, Евгений Андреевич</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Неконтролируемое имя: </span>
								<span class="iblock val">Институт психологии Российской академии наук</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие: </span>
								<span class="iblock val">Становление социальной психологии США : автореферат дис. ... кандидата психологических наук : 19.00.01</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Выходные данные: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Физическое описание: </span>
								<span class="iblock val">23 с.</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">Общая психология, психология личности, история психологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">История психологии -- История психологии в Соединенных Штатах Америки (США) -- История психологии в новейшее время -- Психологические направления</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Ключевые слова: </span>
								<span class="iblock val">социальная психология</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Хранение:</span>
								<span class="iblock val">9 11-3/12;</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Электронный адрес:</span>
								<span class="iblock val">Электронный ресурс</span>
							</div>							
						</div><!-- /b-infobox -->
					</td>
				</tr>
			</table>
		</div><!-- /.b-add_digital-->
		

		
		
	</div><!-- /.b-mainblock -->
	<div class="b-side right">
		<a class="b-btlibpage" href="#">Страница библитеки<br>на портале НЭБ</a>
		<div class="b-sidenav mt140">
			<a href="#" class="b-sidenav_title">Авторы</a>
			
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">Лем С.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">40</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Големский Т. В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">23</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лемин В.В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">2</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лиманн А.Н.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">3</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Голем Л. К.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">15</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>				
					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">дата</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">2000 - 2050</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1950 - 2000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1900 - 1950</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1850 - 1900</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1800 - 1850</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1750 - 1800</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1700 - 1750</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1650 - 1700</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1600 - 1650</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1550 - 1600</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1400 - 1550</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1350 - 1400</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1300 - 1350</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1350 - 1300</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1200 - 1350</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1150 - 1200</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1100 - 1150</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1050 - 1100</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1000 - 1050</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">950 - 1000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li><a href="#" class="b_sidenav_contmore js_moreopen">+ следующие</a></li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">формат издания</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">

					<li class="clearfix">
						<div class="b-sidenav_value left">формат издания</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">Коллекции</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">

					<li class="clearfix">
						<div class="b-sidenav_value left">Коллекции</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">тематика</a>
			<div class="b-sidenav_cont">	
				
				<div class="themesearch">
					<input type="text" class="b-sidenav_tb" placeholder="Поиск по тематике">
					<a href="#" class="b-sidenav_srch">Расширеный список</a>
				</div>
				<ul class="b-sidenav_cont_list b-sidenav_cont_listmore">
					<li class="clearfix">					
						<div class="b-sidenav_value left">Горное дело </div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Пищевые производства</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Радиоэлектроника </div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Строительство </div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Техника и технические 
							науки в целом </div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>
						</li>

						<li><a href="ajax_fake_theme.html" class="b_sidenav_contmore js_ajax_theme" >+ следующие</a></li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">язык</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">язык</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>
						</li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">Библиотеки</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">Библиотеки</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">издательство</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">издательство</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">место издания</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">место издания</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">объем издания</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">объем издания</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
			</div> <!-- /.b-sidenav --> 

		</div><!-- /.b-side -->

	</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<!--<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu js_openmenu">мои подборки</a>
	
	<ul class="b-selectionlist">
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb8">	<label for="cb8" class="black">Любимые авторы</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb9">	<label for="cb9" class="black">Научно-популярная фантастика</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb10"><label for="cb10" class="black">Ракеты и люди</label>

		</li>	
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb14"><label for="cb14" class="lite">Отметить как прочитанное</label>

		</li>
		<li><a href="#" class="b-selection_add"><span>+</span>Cоздать подборку</a></li>
	</ul>
</div>--><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>