<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Iblock\ElementTable,
    \Bitrix\Iblock\SectionTable,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

class FaqItemEdit extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $iblockId = '';
    private $minCountSymbol = 5;
    private $nameOfItem = '';
    private $detailTextItem = '';
    private $sectionIdForItem = '';
    private $activeItem = '';
    private $itemId = '';
    private $indexSort = '500';
    private $arRequest = array();
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        if(!empty($arParams["IBLOCK_ID"])) {
            $this->iblockId = $arParams["IBLOCK_ID"];
        }
        if(!empty($arParams["MIN_COUNT_SYMBOL"])) {
            $this->minCountSymbol = $arParams["MIN_COUNT_SYMBOL"];
        }
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->_getRequest();
        $this->_setDataInForm();
        $this->_getListSections();
        $this->_getRequestNameItem();
        if($this->arRequest['DELETE_ITEM'] != 'on') $this->_setDataInForm();
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Проверка формы на ввод данных
    /**
     * Проверка формы на ввод данных
     */
    private function _getRequestNameItem() {
        //region Отправка формы произведена
        if($this->arRequest["CONTROL_UPDATE_ITEM"] == "Y") {
            //region Отправлен запрос на удаление элемента
            if($this->arRequest['DELETE_ITEM'] == 'on') {
                $this->_deleteItemFromIblock();
            }
            //endregion
            //region Отправка формы произведена и отправлен запрос на обновление элемента
            elseif(
                !empty($this->arRequest["NAME_ITEM"])) {
                $this->nameOfItem = $this->_checkString($this->arRequest["NAME_ITEM"]);
                $this->detailTextItem = $this->arRequest["DETAIL_TEXT_ITEM"];
                $this->sectionIdForItem = $this->_checkString($this->arRequest["SECTION_NAME_FOR_ITEM"]);
                if($this->arRequest["ACTIVE_ITEM"] == 'on') {
                    $this->activeItem = 'Y';
                }
                if(!empty($this->arRequest["SORT_ITEM"])) {
                    $this->indexSort = $this->arRequest["SORT_ITEM"];
                }
                $detailText = trim($this->_checkString($this->detailTextItem));
                if(empty($detailText)) {
                    $this->arResult['ERROR'] = Loc::getMessage("ERROR_DETAIL_TEXT_EMPTY");
                } else {
                    //region Длина названия больше либо равно минимально допустимому значению
                    if(strlen($this->nameOfItem) >= intval($this->minCountSymbol)) {
                        $this->_updateItemToIblock();
                    }
                    //endregion
                    //region Длина названия меньше минимально допустимого значения
                    else {
                        $this->arResult['ERROR'] = Loc::getMessage("ERROR_MIN_COUNT_SYMBOL");
                    }
                    //endregion
                }
            }
            //endregion
            //region Отправка формы произведена но поля не заполнены
            else {
                $this->arResult['ERROR'] = Loc::getMessage("ERROR_EMPTY_STRING");
            }
            //endregion
        }
        //endregion
    }
    //endregion
    //region Возвращаем список разделов в которые нужно добавить элемент
    /** Возвращаем список разделов в которые нужно добавить элемент
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getListSections() {
        $resSections = SectionTable::GetList(array(
            "filter" => array("IBLOCK_ID" => $this->iblockId, "ACTIVE" => "Y"),
            "select" => array("ID", "NAME")
        ));
        while($obSection = $resSections->Fetch()) {
            $this->arResult["SECTIONS"][$obSection["ID"]] = $obSection["NAME"];
        }
    }
    //endregion
    //region Проверка строки на коррeктность
    /** Проверка строки на коррeктность
     * @param $string - проверяемая строка
     * @return string
     */
    private function _checkString($string) {
        $string = trim($string);
        $string = stripslashes($string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        return $string;
    }
    //endregion
    //region Функция обновления свойств элемента
    /**
     * Функция обновления свойств элемента
     */
    private function _updateItemToIblock() {
        $element = new CIBlockElement;
        $paramsTranslite = array("replace_space"=>"-", "replace_other"=>"-");
        $arFields = Array(
            "ACTIVE" => $this->activeItem,
            "IBLOCK_ID" => $this->iblockId,
            "NAME" => $this->nameOfItem,
            "CODE" => Cutil::translit($this->nameOfItem, "ru", $paramsTranslite),
            "IBLOCK_SECTION_ID" => $this->sectionIdForItem,
            "DETAIL_TEXT" => $this->detailTextItem,
            "SORT" => $this->indexSort
        );
        if($element->Update($this->itemId, $arFields)) {
            $this->arResult['SUCCESS'] = Loc::getMessage("EDIT_SUCCESS_1") . $this->itemId . Loc::getMessage("EDIT_SUCCESS_2");
        } else {
            $this->arResult['ERROR'] = Loc::getMessage("ERROR")  . ' '  . $element->LAST_ERROR;
        }
    }
    //endregion
    //region Удаление элемента
    /**
     * Удаление элемента
     */
    private function _deleteItemFromIblock() {
        if(CIBlockElement::Delete($this->itemId)) {
            $this->arResult['SUCCESS'] = Loc::getMessage("ITEM_DELETE_SUCCESS_1") . $this->itemId . Loc::getMessage("ITEM_DELETE_SUCCESS_2");
            $this->_redirectToRootList();
        }
        else {
            $this->arResult['ERROR'] = Loc::getMessage("ITEM_DELETE_ERROR_1") . $this->itemId . Loc::getMessage("ITEM_DELETE_ERROR_2");
        }
    }
    //endregion
    //region Возвращаем данные элемента для обновления
    /** Возвращаем данные элемента для обновления
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _setDataInForm() {
        $this->itemId = $this->arRequest["id"];
        if(!empty($this->arParams["URL_PAGE"])){
            $this->arResult['FORM_URL'] = $this->arParams['SEF_FOLDER'] . str_replace('#ID#', $this->arRequest['id'], $this->arParams["URL_PAGE"]);
        }
        $resItem = ElementTable::GetList(array(
            'filter' => array('IBLOCK_ID' => $this->iblockId, 'ID' => $this->itemId),
            'select' => array('ACTIVE', 'ID', 'NAME', 'DETAIL_TEXT', 'IBLOCK_SECTION_ID', 'SORT')
        ));
        if($obItem = $resItem->Fetch()) {
            $this->arResult['ITEM'] = $obItem;
        } else {
            $this->arResult['ERROR'] = Loc::getMessage('ITEM_NOT_FOUND_1') . $this->itemId . Loc::getMessage('ITEM_NOT_FOUND_2');
        }
    }
    //endregion
    //region Перенаправление на список элементов при успешном удалении
    /**
     * Перенаправление на список элементов при успешном удалении
     */
    private function _redirectToRootList() {
        LocalRedirect($this->arParams['SEF_FOLDER']);
    }
    //endregion
    //region Получение массива $_REQUEST (обертка D7)
    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public function _getRequest() {
        return $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }
    //endregion
}