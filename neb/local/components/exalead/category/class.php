<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Nota\Exalead\SearchQuery;
use \Nota\Exalead\SearchClient;

Loc::loadMessages(__FILE__);

class CategoryComponent extends CBitrixComponent
{
    const ACCESS_FIELDS = ['bbk_id', 'bbk_fullname', 'bbk_name'];

    /**
     * Подготовка входных параметров компонента
     *
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arCat = explode('/', $_REQUEST['category_id']);
        foreach($arCat as $iCat) {
            if ($iCat = intval($iCat))
                $arParams['CATEGORY_ID'][] = $iCat;
        }

        if (!isset($_REQUEST['PAGEN_1'])) {
            $_REQUEST['PAGEN_1'] = 1;
        }

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->includeModules(['nota.exalead']);
        $this->getCategory();
        $this->getSubCategory();
        $this->getBooks();

        $this->includeComponentTemplate();
    }

    /**
     * Подключение модулей
     *
     * @param $arModules
     */
    private static function includeModules($arModules) {
        foreach ($arModules as $sModule) {
            if (!Loader::includeModule($sModule)) {
                die("Module '$sModule' not included!");
            }
        }
    }

    /**
     * Получение информации по текущему разделу ББК
     */
    private function getCategory() {

        $sCat = '';
        foreach ($this->arParams['CATEGORY_ID'] as $iCat) {
            $sCat .= (!empty($sCat) ? ' OR ' : '') . 'bbk_id:' . $iCat;
        }
        if (empty($sCat)) {
            return;
        }

        $query = new SearchQuery($sCat);
        $query->setParam('st', 'st_bbk');
        $query->setParam('sl', 'sl_bbk');
        $query->setParam('of', 'json');
        $query->setQueryType('json');
        $query->setPageLimit('100');

        $client = new SearchClient();
        $data = $client->getResult($query);

        $sBbkFullId = '';
        foreach($data['hits'] as $key => $item) {
            if (!empty($item['metas'])) {
                foreach ($item['metas'] as $value) {
                    if (in_array($value['name'], self::ACCESS_FIELDS)) {
                        if ($value['name'] == 'bbk_id') {
                            $sBbkFullId .= (!empty($sBbkFullId) ? '/' : '') . $value['value'];
                        }
                        $this->arResult['CATEGORY'][$key][$value['name']] = $value['value'];
                    }
                }
                $this->arResult['CATEGORY'][$key]['bbk_fullid'] = $sBbkFullId;
            }
        }
    }

    /**
     * Список подкатегорий
     */
    private function getSubCategory() {

        if (empty($this->arParams['CATEGORY_ID'])) {
            $query = new SearchQuery('bbk_level:1');
        } else {
            $query = new SearchQuery('bbk_parentid:' . end($this->arParams['CATEGORY_ID']));
        }

        $query->setParam('st', 'st_bbk');
        $query->setParam('sl', 'sl_bbk');
        $query->setParam('of', 'json');
        $query->setQueryType('json');
        $query->setPageLimit('100');

        $client = new SearchClient();
        $data = $client->getResult($query);

        foreach($data['hits'] as $key => $item) {
            foreach ($item['metas'] as $value) {
                if (in_array($value['name'], self::ACCESS_FIELDS)) {
                    $this->arResult['res'][$key][$value['name']] = $value['value'];
                }
            }
        }
    }

    /**
     * Получение списка книг
     */
    private function getBooks() {

        CPageOption::SetOptionString("main", "nav_page_in_session", "N");

        $sCategories = $sQueryCategories = '';
        foreach ($this->arResult['CATEGORY'] as $arItem) {
            $sCategories      .= (!empty($sCategories) ? ' ' : '') . $arItem['bbk_name'];
            $sQueryCategories .= (!empty($sQueryCategories) ? ' AND ' : '') . 'bbkfull2:"' . $arItem['bbk_name'] . '"';
        }

        if (!empty($sCategories)) {
            $query = new SearchQuery('#all');
            $bbk = '('
                . '(' . $sQueryCategories . ' AND filesize>0){b =60000000}'
                . ' OR (bbkfull:"' . $sCategories . '" AND filesize>0){b =50000000}'
                . ' OR (bbkfull:"' . $sCategories . '" AND NOT filesize>0){b =40000000}'
                . ' OR ("' . $sCategories . '" AND filesize>0){b =30000000}'
                . ' OR ' . $sCategories . ')';
            $query->addParamToQuery($bbk, 'AND');
        } else {
            $query = new SearchQuery();
            $query->setParam('q', 'filesize>0');
        }
        $query->setParam('sl', 'sl_bbkbook');
        $query->setNavParams();
        $query->setQueryType('search');

        $client = new SearchClient();
        $this->arResult['books'] = $client->getResult($query);

        if (intval($this->arResult['books']['COUNT']) > 0) {
            $client->getDBResult($this->arResult['books']);
            $this->arResult['STR_NAV'] = $client->strNav;
            $this->arResult['NavNum']  = $client->arNavParam['NavNum'];
        }

        CPageOption::SetOptionString("main", "nav_page_in_session", "Y");
    }
}