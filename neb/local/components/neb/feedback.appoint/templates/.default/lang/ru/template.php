<?php
$MESS['FEEDBACK_APPOINT_TITLE']  = 'Назначение ответственного на заявку №';
$MESS['FEEDBACK_APPOINT_SELECT'] = '-- Выберите библиотеку -- ';
$MESS['FEEDBACK_APPOINT_NAME']   = 'ФИО';
$MESS['FEEDBACK_APPOINT_EMAIL']  = 'E-mail';
$MESS['FEEDBACK_APPOINT_PHONE']  = 'Телефон';