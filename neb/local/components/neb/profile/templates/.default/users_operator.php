<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
$APPLICATION->SetTitle('Активные пользователи');
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
require(dirname(__FILE__) . '/includes/header.php');
$APPLICATION->IncludeComponent(
    'neb:user.list', '',
    Array(
        'FIO'               => isset($_REQUEST['FIO'])
            ? $_REQUEST['FIO']
            : null,
        'ORDER'             => isset($_REQUEST['order'])
            ? $_REQUEST['order']
            : null,
        'BY'                => isset($_REQUEST['by'])
            ? $_REQUEST['by']
            : null,
        'SUBORDINATE_GROUP' => UGROUP_OPERATOR_CODE,
    ),
    $component
);
require(dirname(__FILE__) . '/includes/footer.php');
