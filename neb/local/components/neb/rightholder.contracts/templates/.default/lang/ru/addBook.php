<?
$MESS["RIGHTHOLDER_BOOKS_ADD_NEW"] = "Добавление новой книги";
$MESS["RIGHTHOLDER_BOOKS_ADD_EDIT"] = "Редактирование книги";
$MESS["RIGHTHOLDER_BOOKS_ADD_FULL_TITLE"] = "Полное название произведения";
$MESS["RIGHTHOLDER_BOOKS_ADD_REQ_FIELD"] = "Поле обязательно для заполнения";
$MESS["RIGHTHOLDER_BOOKS_ADD_INCORRECT_FIELD"] = "Поле заполнено неверно";
$MESS["RIGHTHOLDER_BOOKS_ADD_AUTHOR"] = "Автор";
$MESS["RIGHTHOLDER_BOOKS_ADD_CODE_BBK"] = "Код ББК";
$MESS["RIGHTHOLDER_BOOKS_ADD_BBK_EXAMPLE"] = "Пример ББК: 81.411.2-4";
$MESS["RIGHTHOLDER_BOOKS_ADD_PUB_YEAR"] = "Год издания";
$MESS["RIGHTHOLDER_BOOKS_ADD_DATE_ADD"] = "Дата добавления";
$MESS["RIGHTHOLDER_BOOKS_ADD_UPDATE"] = "Обновить произведение";
$MESS["RIGHTHOLDER_BOOKS_ADD_POST"] = "Разместить произведение";
$MESS["RIGHTHOLDER_BOOKS_ADD_REFUSE"] = "Отказаться";
?>
