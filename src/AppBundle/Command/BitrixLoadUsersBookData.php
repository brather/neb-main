<?php
/**
 * User: agolodkov
 * Date: 22.06.2016
 * Time: 13:08
 */

namespace AppBundle\Command;


use Nota\Exalead\SearchClient;
use Nota\Exalead\SearchQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BitrixLoadUsersBookData extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('bitrix:load:users-book-data')
            ->setDescription('Load users books data')
            ->addArgument(
                'run-time',
                InputArgument::OPTIONAL,
                'run time seconds'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);
        $batchSize = 50;

        $this->getContainer()
            ->get('bitrix')
            ->requireProlog(
                $this->getContainer()
                    ->get('kernel')
                    ->getRootDir()
            );

        while (ob_get_level()) {
            ob_end_clean();
        }

        error_reporting(E_ERROR);
        ini_set('display_errors', 1);

        \NebMainHelper::includeModule('nota.exalead');

        global $DB;
        $result = $DB->Query("SELECT DISTINCT nubd.UF_BOOK_ID UF_BOOK_ID
FROM neb_users_books_data nubd
  LEFT JOIN neb_books_data nbd ON nbd.UF_BOOK_ID = nubd.UF_BOOK_ID
WHERE nbd.UF_BOOK_ID IS NULL");
        $bookDataClass = \NebMainHelper::getHIblockDataClassByName('BooksData');
        $bookIds = [];
        $i = 0;
        $insertBatch = function ($bookIds) use ($bookDataClass, $batchSize) {
            $success = 0;
            $query = new SearchQuery();
            $query->getByArIds($bookIds);
            $query->setPageLimit($batchSize);
            $client = new SearchClient();
            $exaleadResult = $client->getResult($query);
            foreach($exaleadResult['ITEMS'] as $exaleadItem) {
                $addResult = $bookDataClass::add(
                    [
                        'UF_BOOK_ID'           => $exaleadItem['id'],
                        'UF_BOOK_NAME'         => $exaleadItem['title'],
                        'UF_BOOK_AUTHOR'       => $exaleadItem['authorbook'],
                        'UF_BOOK_PUBLISH_YEAR' => $exaleadItem['publishyear'],
                    ]
                );
                if($addResult->getId()) {
                    $success++;
                }
            }
            return $success;
        };
        while ($item = $result->Fetch()) {
            $bookIds[] = $item['UF_BOOK_ID'];
            if(count($bookIds) >= $batchSize) {
                $i += $insertBatch($bookIds);
                echo "Success add $i items\r";
                $bookIds = [];
            }
        }
        if(!empty($bookIds)) {
            $i += $insertBatch($bookIds);
            echo "Success add $i items\r";
        }
        echo "\n";

        $output->writeln(
            'Total items inserted: ' . $i
        );
        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}