<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Регистрация правообладателя (физическое лицо)");

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('REGISTRATION_RIGHTHOLDER_PAGE_TITLE'));
$APPLICATION->IncludeComponent(
    'neb:main.register',
    'rightholder_individual',
    array(
        'USER_PROPERTY_NAME' => '',
        'SHOW_FIELDS'        => array(
            'EMAIL',
            'NAME',
            'SECOND_NAME',
            'LAST_NAME',
            'PERSONAL_GENDER',
            'PERSONAL_BIRTHDAY',
            'PERSONAL_PHOTO',
            'PERSONAL_ZIP',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
            'PERSONAL_GENDER',
            'WORK_ZIP',
            'WORK_CITY',
            'WORK_STREET',
            'PERSONAL_MOBILE',
        ),
        'REQUIRED_FIELDS'    => array(
            'EMAIL',
            'NAME',
            'LAST_NAME',
            'PERSONAL_GENDER',
            'PERSONAL_ZIP',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
        ),
        'USER_PROPERTY'      => array(
            'UF_LIBRARY',
            'UF_STATUS',
            'UF_PASSPORT_NUMBER',
            'UF_ISSUED',
            'UF_CORPUS',
            'UF_STRUCTURE',
            'UF_FLAT',
            'UF_PASSPORT_SERIES',
            'UF_EDUCATION',
            'UF_HOUSE2',
            'UF_STRUCTURE2',
            'UF_FLAT2',
            'UF_PLACE_REGISTR',
            'UF_SCAN_PASSPORT1',
            'UF_SCAN_PASSPORT2',
            'UF_CITIZENSHIP',
            'UF_STATUS',
            'UF_BANK_NAME',
            'UF_BANK_INN',
            'UF_BANK_KPP',
            'UF_BANK_BIC',
            'UF_COR_ACCOUNT',
            'UF_PERSONAL_ACCOUNT',
            'UF_FOUNDATION',
        ),
        'AUTH'               => 'Y',
        'USE_BACKURL'        => 'Y',
        'SUCCESS_PAGE'       => '',
        'SET_TITLE'          => 'N',
        'NEED_CONFIRM'       => 'Y',
        'MAIL_EVENTS'        => array(
            'NEW_USER'
        ),
    ),
    false
); ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>