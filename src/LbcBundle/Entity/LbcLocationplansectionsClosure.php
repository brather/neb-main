<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 16:33
 */

namespace LbcBundle\Entity;

use Gedmo\Tree\Entity\MappedSuperclass\AbstractClosure;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="lbc_locationplansections_closure")
 * @ORM\Entity
 */
class LbcLocationplansectionsClosure extends AbstractClosure
{

}