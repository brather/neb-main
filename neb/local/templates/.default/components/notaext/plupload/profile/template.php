<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
	\CUtil::InitJSCore(array("jquery"));
	$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);
?>
<div class="inside-profile-preview spp">
	<div class="spp-wrapper">
		<div class="spp-photo">
			<img width="110" src="<?=!empty($arParams['PERSONAL_PHOTO'])? $arParams['PERSONAL_PHOTO'] : MARKUP.'i/user_photo.png'?>" alt="<?=$arParams['FULL_NAME']?>" id="user_photo">
		</div>
		<div class="spp-name"><?=$arParams['FULL_NAME']?></div>
        <?if (!empty($arParams['UF_RGB_CARD_NUMBER']))
		{?>
			<div class="spp-rgb-cardnumber"><?=$arParams['UF_RGB_CARD_NUMBER']?></div>
		<?}
		if (strpos($arParams['EMAIL'], $arParams['UF_RGB_CARD_NUMBER']) === false)
		{?>
			<div class="spp-email"><?=$arParams['EMAIL']?></div>
		<?}?>
		<div class="spp-registered"><?=GetMessage("PROFILE_CARD_REGISTERED");?>: <?=ConvertTimeStamp(MakeTimeStamp($arParams['DATE_REGISTER']))?></div>
		<ul class="spp-info" id="container_<?=$arParams['RAND_STR']?>">
			<li><a href="#" id="setphoto_profile"><?=GetMessage("PROFILE_CARD_CHANGE_PHOTO");?></a></li>
		</ul>
		<?
		//упрощенная регистрация?
		if((($arParams['UF_REGISTER_TYPE'] == 37) || empty($arParams['UF_REGISTER_TYPE'])) && ($arParams['FOR_LIBRARY_USER'] != "Y")
		&& 'user' === $arParams['USER_ROLE']){
			?>
			<a href="<?=(SITE_TEMPLATE_ID=="special")?"/special":""?>/profile/update_select/" class="btn btn-primary"><?=GetMessage("PROFILE_CARD_GET_FULL_ACCESS");?></a>
		<?
		}
		?>
	</div>
</div><!-- /.b-side -->
<?#pre($this->__folder,1)?>
<div class="pl_button"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>

<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'setphoto_profile',
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'), 
				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
					unique_names: true,
					<?
					}
				?>

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				<?
					if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
					{
					?>
					resize: {
						width: <?=intval($arParams['RESIZE_WIDTH'])?>,
						height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
						<?
							if($arParams['RESIZE_CROP'] == 'Y')
							{
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=intval($arParams['RESIZE_QUALITY'])?>
					},
					<?
					}
				?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '1mb',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'
				},
				multi_selection: false,
				init: {

					FilesAdded: function(up, files) {

						uploader_<?=$arParams['RAND_STR']?>.start();
					},
					FileUploaded: function(up, file, response) {
						var result = response.response;
						if (result) {
							var obResponse = JSON.parse(result);
							$('#user_photo').attr('src', obResponse.file);

							$.post( "<?=$this->__folder?>/set_photo.php", { file: obResponse.file, sessid: '<?=bitrix_sessid()?>', USER_ID: '<?=(isset($arParams["USER_ID"]) && intval($arParams["USER_ID"])>0)?intval($arParams["USER_ID"]):0;?>' });
						}
					},
					Error: function(up, err) {
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
					}
				}
			}
		);

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>
