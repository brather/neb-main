$(function(){
    // Запуск всплывающих подсказок
    $('.info-tooltip').tooltip();

    // Запуск модального окна для добавления издания в план
    var btnShowModal = $('.js-modal-search-books'),
        modalBooks = $('#universal-modal').clone(true).insertAfter('#universal-modal');
    btnShowModal.on('click', function(e){
        e.preventDefault();

        var $this = $(this),
            plan = {
                id : $this.data('plan-id'),
                name : $this.data('plan-name'),
                datef : $this.data('plan-f'),
                url : $this.data('url-books')
            },
            textAddBook = $this.data('text'),
            modalUrl = plan.url + '?date=' + plan.datef + '&plan_id=' + plan.id;
        modalBooks.one('show.bs.modal', function(e){
            var modal = $(this),
                iframe = $('<iframe/>').attr('src', modalUrl).css({
                    'height':'100%',
                    'width':'100%',
                    'border':'none'
                });
            modal.find('.modal-title').text(textAddBook)
                .end().find('.modal-dialog').addClass('fixed-modal modal-lg no-footer-bar')
                .end().find('.modal-footer').remove()
                .end().find('.modal-body').css('overflow','hidden').append(iframe);
        }).one('hidden.bs.modal', function(e){
            $(modalBooks).detach();
            startLoader('body');
            window.location.replace(location.pathname);
        }).modal('show', $this);
    });

    // Показ/скрытие строки в таблице с описанием издания
    var btnShowDesc = $('.js-show-info'),
        toggleSpeed = 100,
        clsIShow = 'fa-caret-up',
        clsIHide = 'fa-caret-down',
        clsHidden = 'hidden';
    btnShowDesc.on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            containTable = $('#' + $this.data('parent')),
            iCaret = $this.find('.fa'),
            rowShow = $($this.attr('href')),
            spanSpin = $this.closest('td').find('span');

        if(iCaret.hasClass(clsIHide)) {
            var activeICaret = containTable.find('.' + clsIShow),
                activeSpan = activeICaret.closest('td').find('span'),
                activeRow = $(activeICaret.closest('a').attr('href'));
            activeSpan.removeClass(clsHidden);
            activeRow.toggle(toggleSpeed, function(){
                activeICaret.removeClass(clsIShow).addClass(clsIHide);
                activeSpan.addClass(clsHidden);
            });
            spanSpin.removeClass(clsHidden);
            rowShow.toggle(toggleSpeed, function(){
                iCaret.removeClass(clsIHide).addClass(clsIShow);
                spanSpin.addClass(clsHidden);
            });
        } else if(iCaret.hasClass(clsIShow)) {
            spanSpin.removeClass(clsHidden);
            rowShow.toggle(toggleSpeed, function(){
                iCaret.removeClass(clsIShow).addClass(clsIHide);
                spanSpin.addClass(clsHidden);
            });
        }
    });

    // Удаление издания из плана
    var btnDeleteEdition = $('.js-delete-row');
    btnDeleteEdition.on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            dataSend = {
                STATUS : $this.data('status'),
                BOOK_ID : {
                    0 : $this.data('book')
                },
                sessid : $this.data('ssid'),
                plan_id : $this.data('plan-id'),
                date : $this.data('plan-date')
            },
            toggler = $(e.target),
            message = {
                title: $this.data('text-title'),
                text: $this.data('text-confirm-text'),
                confirmTitle: $this.data('text-confirm-title')
            };
        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed) {
                startLoader('body');
                $.ajax({
                    url : $this.attr('href'),
                    type : 'post',
                    data : dataSend,
                    success : function(data) {
                        stopLoader();
                        $this.closest('tr').hide(toggleSpeed).remove();
                        $('#book-' + dataSend.BOOK_ID[0]).hide(toggleSpeed).remove();
                    }
                });
            }
        });
    });

    // Загрузка плана на оцифровку
    var $formAddExcel = $('.digit-plan-load__add-excel-form');
    $formAddExcel.fileupload({
        dataType: 'json',
        submit: function(e, data) {
            var $this = $(this),
                rowContain = $this.closest('.row'),
                $progressBarAddExcel = rowContain.find('.digit-plan-load__progress');
            $this.hide();
            $progressBarAddExcel.show();
        },
        done : function(e, data) {
            var $this = $(this),
                preffix = $this.data('preffix'),
                plan_id = preffix.replace(/\D+/g,""),
                blkLang = $this.find('.blk-lang'),
                rowContain = $this.closest('.row'),
                objText = {
                    count       : blkLang.data('resp-count'),
                    added       : blkLang.data('resp-added'),
                    error       : blkLang.data('resp-error'),
                    error_try   : blkLang.data('resp-error-try'),
                    already     : blkLang.data('resp-already'),
                    notfound    : blkLang.data('resp-notfound')
                },
                $progressBarAddExcel = $this.next('.digit-plan-load__progress'),
                $formGetExcel = $('#' + preffix + 'get_excel'),
                $btnSubmitGetExcel = $formGetExcel.find('.digit-plan-load__get-excel-btn'),
                $inputResponseGetExcel = $formGetExcel.find('.digit-plan-load__get-excel-input'),
                $blockMessAddExcel = rowContain.find('.digit-plan-load__message'),
                clsWrapper = '#digitization_plan_list-' + plan_id; //.b-add_digital-wrapper.' + preffix + 'digital-wrapper';

            $progressBarAddExcel.hide();
            $this.show();
            $btnSubmitGetExcel.show().removeAttr('disabled');
            var response = data._response.result;
            $inputResponseGetExcel.val(JSON.stringify(response));
            if(!response.error) {
                var loaded = '',
                    err = '',
                    already = '',
                    notfound = '';
                (response.added) ? loaded = '<li>' + objText.added + '<b>' + response.added.length + '</b>' + objText.count + '</li>' : loaded = '';
                (response.error) ? err = '<li>' + objText.error + '<b>' + response.error.length + '</b>' + objText.count + '</li>' : err = '';
                (response.already) ? already = '<li>' + objText.already + '<b>'+response.already.length+'</b>' + objText.count + '</li>' : already = '';
                (response.notfound) ? notfound  = '<li><b>' + response.notfound.length + '</b>' + objText.notfound + '</li>' : notfound = '';
                $blockMessAddExcel.html('<ul>' + loaded + err + already + notfound + '</ul>');
            } else {
                $blockMessAddExcel.html(response.error_message);
            }

            BX.showWait(document.body.querySelector(clsWrapper));
            $.ajax({
                url: $formAddExcel.attr('action') + '?digitization_plan_update=' + plan_id,
                data: {},
                type: "GET",
                beforeSend: function(xhr){xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');},
                success: function(data) {
                    $(clsWrapper).html('').prepend(data);
                    BX.closeWait(document.body.querySelector(clsWrapper));
                }
            });
        },
        fail : function(e, data) {
            var $this = $(this),
                rowContain = $this.closest('.row'),
                $progressBarAddExcel = rowContain.find('.digit-plan-load__progress'),
                $blockMessAddExcel = rowContain.find('.digit-plan-load__message'),
                blkLang = $this.find('.blk-lang'),
                objText = {
                    count       : blkLang.data('resp-count'),
                    added       : blkLang.data('resp-added'),
                    error       : blkLang.data('resp-error'),
                    error_try   : blkLang.data('resp-error-try'),
                    already     : blkLang.data('resp-already'),
                    notfound    : blkLang.data('resp-notfound')
                };
            $this.show();
            $progressBarAddExcel.hide();
            $blockMessAddExcel.html('<p>' + objText.error_try + '</p>');
        }
    });

    // Перемещение списка файлов в книги в блок книги
    $('.books-in-plan').find('.book-in-plan').each(function(i,y){
        var idPlan = $(y).data('id-plan'),
            idBook = $(y).data('id-book'),
            $blockFiles = $('#upload-files-' + idPlan + '-' + idBook),
            $blockTo = $('#container-plan-' + idPlan + '-book-' + idBook);
        if($blockFiles.length > 0) {
            $blockTo.append($blockFiles);
        }
    });
});

// Функция запуска лоадера
function startLoader(block) {
    stopLoader();
    var blkLoader = '<div id="blk-loader" class="blk-milk-shadow"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
    $(block).append(blkLoader);
}

// Функция остановки лоадера
function stopLoader() {
    $('#blk-loader').animate({ 'opacity' : 0 }, 300, function(){
        $(this).remove();
    });
}