<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * @todo перенести все это в REST
 */
use \Bitrix\Main\Loader,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

global $APPLICATION, $USER;
$APPLICATION->RestartBuffer();

$jsonResult = [];

$iWorkplace = intval($arResult['USER']['UF_WCHZ']);
$iUser      = intval($arResult['VARIABLES']['USER_ID']);

if ($USER->IsAuthorized() && $iWorkplace && $iUser)
{
    if (!Loader::includeModule('highloadblock')) {

        $jsonResult = [
            'error' => 3,
            'message' => 'Module "highloadblock" not installed!',
        ];
    } else {

        $arHIblock = HighloadBlockTable::getList(['filter' => ['NAME' => 'UsersTickets']])->fetchRaw();
        $obUsersTickets = HighloadBlockTable::compileEntity($arHIblock)->getDataClass();

        $arResult = $obUsersTickets::getList([
            'order' => ['UF_USER_ID' => 'ASC'],
            'filter' => ['>=UF_DATE' => new DateTime(), 'UF_WCHZ_ID' => $iWorkplace, 'UF_USER_ID' => $iUser],
            'select' => ['ID']
        ])->fetchRaw();

        if ($arResult['ID'] > 0) {
            $res = $obUsersTickets::delete($arResult['ID']);
            if ($res->isSuccess()) {
                $jsonResult = [
                    'error' => 0,
                    'message' => 'Вы успешно удалили читателя из библиотеки',
                ];
            } else {
                $jsonResult = [
                    'error' => 2,
                    'message' => 'Не удалось удалить читателя из библиотеки. Обратитесь, пожалуйста, за консультацией к администратору сайта.',
                ];
            }
        }
    }
}

if (empty($jsonResult)) {
    $jsonResult = [
        'error' => 1,
        'message' => 'Неверные параметры запроса.',
    ];
}

MainHelper::showJson($jsonResult);