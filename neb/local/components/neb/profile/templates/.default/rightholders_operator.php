<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Список правообладателей');
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
require(dirname(__FILE__) . '/includes/header.php');

$APPLICATION->IncludeComponent(
    'neb:user.list',
    'rightholders',
    Array(
        'FIO'          => @$_REQUEST['FIO'],
        'WORK_COMPANY' => @$_REQUEST['WORK_COMPANY'],
        'STATUS'       => @$_REQUEST['STATUS'],
        'GROUP_ID'     => 9,
        'ORDER'        => $_REQUEST['order']?:null,
        'BY'           => $_REQUEST['by']?:null,
    ),
    $component
);
require(dirname(__FILE__) . '/includes/footer.php');
