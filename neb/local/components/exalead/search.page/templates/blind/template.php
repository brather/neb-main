<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

// для возврата к форме поиска
$uriString = Application::getInstance()->getContext()->getRequest()->getRequestUri();
$uriString = explode('?', $uriString);
if (count($uriString) == 2 && !empty($uriString[1]))
    $_SESSION['BLIND_QUERY'] = $uriString[1];

$sDirPath = $component->getPath() . '/templates/' . $component->getTemplateName();

reset($arResult['ITEMS']);
$ar = current($arResult['ITEMS']);

//debugPre($arResult['ITEMS']);
//debugPre($arResult['COUNT']);

?>

<div class="container v-padding">
    <div class="button-bar" role="navigation">
        <a href="../" class="btn btn-default">Вернуться на главную</a><br />
        <a href="../search/?<?=$_SESSION['BLIND_QUERY']?>" class="btn btn-default">Изменить параметры поиска</a>
        <a href="/" class="btn btn-default" tabindex="7" style="float:right;" aria-hidden="true">Переход в режим для зрячих</a>
    </div>
    <div role="main">
        <p class="h4">Найдено результатов: <span><?=intval($arResult['COUNT'])?></span></p>

        <? if ($arResult['COUNT']): ?>
            <p>Сортировать по:</p>

            <select class="form-control input-lg results-sorting">
                <option value="<?= preg_replace('#by=.*sort#', '', $_SERVER['REQUEST_URI']);?>">Релевантности</option>
                <option <?= str_replace('href', 'value', SortingExalead('document_authorsort'))?><?
                    if ($_REQUEST['by'] == 'document_authorsort'):?> selected="selected"<?endif;?>>По автору</option>
                <option <?= str_replace('href', 'value', SortingExalead('document_titlesort'))?><?
                    if ($_REQUEST['by'] == 'document_titlesort'):?> selected="selected"<?endif;?>>По названию</option>
            </select>

            <div class="search-results" style="counter-reset: searchresults <?=$ar['NUM_ITEM']-1?>;" data-explain="last-value-was-4-so-starts-from-position-minus-1">

                <? foreach($arResult['ITEMS'] as $index => $arItem): ?>
                    <? $APPLICATION->IncludeFile($sDirPath . '/search_row.php', array(
                        'arParams' => $arParams,'arResult' => $arResult, 'index' => $index,'arItem' => $arItem
                    )); ?>
                <? endforeach; ?>

                <?
                $iStr = intval($_REQUEST['PAGEN_1']) > 1 ? intval($_REQUEST['PAGEN_1']) : 1;
                $nav = !empty($arResult['STR_NAV']) ? htmlspecialcharsback($arResult['STR_NAV']) : '';
                $nav = str_replace('class="text-center"', '', $nav);
                $nav = str_replace(
                    '<ul',
                    '<p style="margin-left: 2em;">Навигация по страницам (Страница: ' . $iStr . ')</p><ul',
                    $nav
                );
                $nav = str_replace(
                    '<span aria-hidden="true" class="fa fa-angle-left"></span>',
                    '<span>Назад</span>',
                    $nav
                );
                $nav = str_replace(
                    '<span aria-hidden="true" class="fa fa-angle-right"></span>',
                    '<span>Вперед</span>',
                    $nav
                );

                echo $nav;
                ?>

            </div>
        <? endif; ?>
    </div>
</div>