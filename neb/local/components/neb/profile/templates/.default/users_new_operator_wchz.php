<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
$APPLICATION->SetTitle('Новые пользователи');
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Список новых пользователей');
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */

$APPLICATION->IncludeComponent(
    'neb:user.list', 'new-wchz',
    [
        'FIO'               => $_REQUEST['FIO'],
        'SUBORDINATE_GROUP' => UGROUP_OPERATOR_CODE,
        'listParams'        => [
            'order' => [
                'DATE_REGISTER' => 'DESC',
            ]
        ]
    ],
    $component
);

