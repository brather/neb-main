<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
\CUtil::InitJSCore(array("jquery"));
$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);

?>

<div class="fieldcell nowrap photorow">
	<label for="settings05"><?=GetMessage('PLUPLOAD_LIBRARY_COLLECTION_ADD_IMG')?></label>
    <div class="field validate">
        <div class="setscan iblock" id="container_<?=$arParams['RAND_STR']?>">
            <input type="hidden" class="photofile" name="background" value="<?=$arParams['FILE_PATH']?>" data-required="true">
            <a href="#" id="setphoto_background"><?=GetMessage('PLUPLOAD_LIBRARY_COLLECTION_ADD_LOAD')?></a>
            <div class="setphoto_lb"><?=GetMessage('PLUPLOAD_LIBRARY_COLLECTION_ADD_DRAG')?></div>
            <div class="progressbar" id="progressbar1" style="display: none;">
                <div class="progress">
                    <span style="width:0px;"></span>
                </div>
                <div class="text"><?=GetMessage('PLUPLOAD_LIBRARY_COLLECTION_ADD_LOADING')?> <span class="num">0</span>%</div>
            </div>
        </div>
        <div class="b-scan_photo iblock">
            <div class="b-favside_img js_flexbackground">
                <img src="<?=$arParams['FILE_PATH'] ? $arParams['FILE_PATH'] : '/bitrix/images/1.gif'?>" alt=""  id="background_photo" height="161">
            </div>
        </div>
    </div>
    <em class="error errsize" style="display:none;"><?=GetMessage('PLUPLOAD_LIBRARY_COLLECTION_ADD_SMALL')?></em>
</div>


<div style="display: none;" class="pl_button"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>

<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'setphoto_background',
				drop_element: "container_<?=$arParams['RAND_STR']?>",
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'), 
				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
					unique_names: true,
					<?
					}
				?>

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				<?
					if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
					{
					?>
					resize: {
						width: <?=intval($arParams['RESIZE_WIDTH'])?>,
						height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
						<?
							if($arParams['RESIZE_CROP'] == 'Y')
							{
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=intval($arParams['RESIZE_QUALITY'])?>
					},
					<?
					}
				?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '100kb',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'

				},
				multi_selection: false,
				init: {

					FilesAdded: function(up, files) {
						up.start();
					},
					FileUploaded: function(up, file, response) {
						var result = response.response;
						if (result) {
							var obResponse = JSON.parse(result);
							$('#background_photo').attr('src', obResponse.file);
							$('input[name="background"]').val(obResponse.file);
						}
					},
					UploadProgress: function(up, file) {
						$('#progressbar1').show();
						$('#progressbar1 .progress span').css('width' , file.percent+"%");
						$('#progressbar1 .text .num').text(file.percent);
					},
					Error: function(up, err) {
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
					}
				}
			}
		);

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>
