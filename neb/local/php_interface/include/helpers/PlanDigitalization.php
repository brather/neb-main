<?

/**
 * План на оцифровку
 * Class PlanDigitalization
 */
class PlanDigitalization
{
    const STATUS_ON_AGREEMENT = 1;
    const STATUS_AGREED = 2;
    const STATUS_IN_PROCESS = 3;
    const STATUS_CANCELED = 4;
    const STATUS_DONE = 5;
    const STATUS_CLOSED = 6;

    const CREATE_EMAIL_TEMPLATE_ID = 136;
    const CHANGE_STATUS_EMAIL_TEMPLATE_ID = 137;

    protected static $_statusEnums;
    protected static $_enumsStatusMap;

    /**
     * Может литекущий пользователь добавлять в план на оцифровку книги
     * @return bool
     */
    public static function isAdd()
    {
        global $USER;
        if (!$USER->IsAuthorized())
            return false;

        $uobj = new nebUser();
        $role = $uobj->getRole();

        if ($role == UGROUP_LIB_CODE_ADMIN || $role == UGROUP_LIB_CODE_EDITOR || $role == UGROUP_LIB_CODE_CONTROLLER)
            return true;
        else
            return false;
    }


    public static function getBookInPlan()
    {
        $arResult = array();

        if (!self::isAdd())
            return false;

        $obUser = new nebUser();
        if (!$obUser->isLibrary())
            return false;

        $Library = $obUser->getLibrary();
        if (empty($Library['ID']))
            return false;

        if (!CModule::IncludeModule("highloadblock"))
            return false;

        $hlblock           = Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_PLAN_DIGIT)->fetch();
        $entity_data_class = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        $rsData = $entity_data_class::getList([
            "select" => ["ID", 'UF_EXALEAD_BOOK'],
            "filter" => ['UF_LIBRARY' => $Library['ID']]
        ]);
        while ($arData = $rsData->Fetch())
            $arResult[] = $arData['UF_EXALEAD_BOOK'];

        return $arResult;
    }

    /**
     * @param int $type
     *
     * @return string|null
     */
    public static function getStatusName($type)
    {
        static::getStatusEnums();
        $type = (integer)$type;
        if (0 === $type)
            $type = 1;

        return isset(static::$_statusEnums[$type]['VALUE'])
            ? static::$_statusEnums[$type]['VALUE']
            : null;
    }

    /**
     * @param int $valueId
     *
     * @return null|string
     */
    public static function getStatusNameByValueId($valueId)
    {
        return static::getStatusName(static::getStatusByValueId($valueId));
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        $enums = static::getStatusEnums();
        $list = [];
        foreach ($enums as $enum)
            $list[(integer)$enum['XML_ID']] = $enum['VALUE'];

        return $list;
    }

    /**
     * @param int $status
     *
     * @return null
     */
    public static function getStatusValueId($status)
    {
        static::getStatusEnums();

        return isset(static::$_statusEnums[$status]['ID'])
            ? static::$_statusEnums[$status]['ID']
            : null;
    }


    /**
     * @param int $valueId
     *
     * @return null
     */
    public static function getStatusByValueId($valueId)
    {
        static::getStatusEnums();

        return isset(static::$_enumsStatusMap[$valueId])
            ? static::$_enumsStatusMap[$valueId]
            : null;
    }


    /**
     * @return array
     */
    public static function getStatusEnums()
    {
        if (null === static::$_statusEnums) {
            $userEnum = new CUserFieldEnum();
            $rsEnum = $userEnum->GetList(
                [], ['USER_FIELD_NAME' => 'UF_REQUEST_STATUS']
            );
            while ($statusEnum = $rsEnum->Fetch()) {
                static::$_statusEnums[(integer)$statusEnum['XML_ID']] = $statusEnum;
                static::$_enumsStatusMap[$statusEnum['ID']]           = (integer)$statusEnum['XML_ID'];
            }
        }

        return static::$_statusEnums;
    }

    public static function getAllwedChangeStatus($orderId, $userId)
    {
        $tableClass = NebMainHelper::getHIblockDataClassName(HIBLOCK_PLAN_DIGIT);
        $order = $tableClass::getById($orderId)->fetch();
        $user = new nebUser($userId);
        if ((integer)$order['ID'] !== (integer)$orderId)
            throw new Exception('Заявка не найдена!');

        $statusId = PlanDigitalization::getStatusByValueId( (integer)$order['UF_REQUEST_STATUS'] );
        if (!$statusId)
            $statusId = static::STATUS_ON_AGREEMENT;

        return static::getAllowedChangeStatusRS($user->getRole(), $statusId);
    }

    public static function getAllowedChangeStatusRS($role, $status)
    {
        $result = [];
        switch ($role) {
            case 'user':
                switch ($status) {
                    case static::STATUS_ON_AGREEMENT:
                        $result = [
                            static::STATUS_CANCELED,
                        ];
                        break;
                }
                break;
            case 'operator':
                switch ($status) {
                    case static::STATUS_ON_AGREEMENT:
                        $result = [
                            static::STATUS_CANCELED,
                            static::STATUS_AGREED,
                        ];
                        break;
                    case static::STATUS_DONE:
                        $result = [
                            static::STATUS_IN_PROCESS,
                            static::STATUS_CLOSED,
                        ];
                        break;
                }
                break;
            case 'library_admin':
                switch ($status) {
                    case static::STATUS_AGREED:
                        $result = [
                            static::STATUS_ON_AGREEMENT,
                            static::STATUS_IN_PROCESS,
                        ];
                        break;
                    case static::STATUS_IN_PROCESS:
                        $result = [
                            static::STATUS_ON_AGREEMENT,
                            static::STATUS_DONE,
                        ];
                        break;
                }
                break;
        }

        return $result;
    }

    /**
     * Какие статусы можно показывать пользователю с переданной ролью
     *
     * @param string $role
     *
     * @return array
     */
    public static function getAllowedRoleStatuses($role)
    {
        $actionStatuses = [
            static::STATUS_CANCELED     => true,
            static::STATUS_AGREED       => true,
            static::STATUS_CLOSED       => true,
            static::STATUS_DONE         => true,
            static::STATUS_IN_PROCESS   => true,
            static::STATUS_ON_AGREEMENT => true,
        ];
        switch ($role) {
            case UGROUP_LIB_CODE_ADMIN:
                unset($actionStatuses[static::STATUS_ON_AGREEMENT]);
                break;
        }

        return $actionStatuses;
    }

    public static function getAllowedActionsRS($role, $status)
    {
        $status = (integer)$status;
        if (!$status) {
            $status = static::STATUS_ON_AGREEMENT;
        }
        $actionStatuses = [
            static::STATUS_CANCELED     => 'cancel',
            static::STATUS_AGREED       => 'agree',
            static::STATUS_CLOSED       => 'close',
            static::STATUS_DONE         => 'done',
            static::STATUS_IN_PROCESS   => 'in_progress',
            static::STATUS_ON_AGREEMENT => 'refuse',
        ];
        $result = array_intersect_key(
            $actionStatuses,
            array_flip(static::getAllowedChangeStatusRS($role, $status))
        );
        switch ($role) {
            case 'operator':
                if (static::STATUS_DONE === $status)
                    $result[static::STATUS_IN_PROCESS] = 'back_in_progress';
                break;
        }

        return $result;
    }

    /**
     * @param string $parentId
     * @param string $bookId
     * @param array  $parameters
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getRelatedOrders(
        $bookId,
        $parentId = null,
        $parameters = []
    ) {
        $fitler = [];
        if (empty($parentId)) {
            $fitler['UF_EXALEAD_BOOK'] = $bookId;
        } else {
            $fitler[] = [
                'LOGIC' => 'OR',
                ['UF_EXALEAD_BOOK' => $bookId],
                ['UF_EXALEAD_PARENT' => $parentId],
            ];
        }
        $parameters = array_replace_recursive(
            [
                'filter' => $fitler
            ], $parameters
        );
        $tableClass = NebMainHelper::getHIblockDataClassName(
            HIBLOCK_PLAN_DIGIT
        );

        return $tableClass::getList($parameters)->fetchAll();
    }

    /**
     * @param int $orderId
     *
     * @return array
     */
    public static function getRelatedOrdersById($orderId)
    {
        $tableClass = NebMainHelper::getHIblockDataClassName(
            HIBLOCK_PLAN_DIGIT
        );
        if ($order = $tableClass::getById($orderId)->fetch()) {
            return static::getRelatedOrders(
                $order['UF_EXALEAD_BOOK'],
                $order['UF_EXALEAD_PARENT']
            );
        }

        return [];
    }

    /**
     * @param array $ordersIds
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getRelatedOrdersByIdsList($ordersIds)
    {
        $result = [];
        $ordersIds = array_filter($ordersIds);
        if (!empty($ordersIds)) {
            $tableClass = NebMainHelper::getHIblockDataClassName(
                HIBLOCK_PLAN_DIGIT
            );
            $rsOrders = $tableClass::getList(
                [
                    'filter' => [
                        'ID' => $ordersIds,
                    ],
                ]
            );
            $parameters = [
                'filter' => [
                    [
                        'LOGIC' => 'OR'
                    ]
                ]
            ];
            $filterOrLogic = [];
            while ($orderItem = $rsOrders->fetch()) {
                if (empty($orderItem['UF_EXALEAD_PARENT'])) {
                    $filterOrLogic[]['UF_EXALEAD_BOOK']
                        = $orderItem['UF_EXALEAD_BOOK'];
                } else {
                    $filterOrLogic[] = [
                        'LOGIC' => 'OR',
                        ['UF_EXALEAD_BOOK' => $orderItem['UF_EXALEAD_BOOK']],
                        ['UF_EXALEAD_PARENT' => $orderItem['UF_EXALEAD_PARENT']],
                    ];
                }
            }
            if(!empty($filterOrLogic)) {
                $parameters['filter'][0] = array_merge($parameters['filter'][0], $filterOrLogic);
                $rsOrders = $tableClass::getList($parameters);
                while($orderItem = $rsOrders->fetch()) {
                    $result[] = $orderItem;
                }
            }
        }

        return $result;
    }
}