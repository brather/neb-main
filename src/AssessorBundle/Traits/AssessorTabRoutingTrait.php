<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 16:06
 */

namespace AssessorBundle\Traits;


use AssessorBundle\Controller\Api\RatingApiInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use JMS\DiExtraBundle\Annotation as DI;


trait AssessorTabRoutingTrait
{
    /**
     * @DI\Inject("router")
     * @var Router
     */
    public $router;

    /**
     * @return array
     */
    public function getAssessorTabsUrls()
    {
        $urls = [];
        foreach ($this->_getAssessorRoutes() as $name => $route) {
            $urls[$name] = $this->router->generate($route);
        }

        return $urls;
    }

    /**
     * @param string $url
     *
     * @return null|string
     */
    public function getCurrentName($url)
    {
        try {
            $currentRoute = $this->router->match($url);
            $currentRoute = $currentRoute['_route'];
        } catch (\Exception $e) {
            $currentRoute = RatingApiInterface::DEFAULT_ROUTE;
        }

        return $this->getRouteCode($currentRoute);
    }

    /**
     * @param string $route
     *
     * @return null
     */
    public function getRouteCode($route)
    {
        $routes = $this->_getAssessorRoutes();
        $routes = array_flip($routes);

        return isset($routes[$route]) ? $routes[$route] : null;
    }

    /**
     * @return array
     */
    protected function _getAssessorRoutes()
    {
        return [
            'queries'      => 'assessor_queries',
            'authors'      => 'assessor_authors',
            'editions'     => 'assessor_editions',
            'substitution' => 'assessor_substitution',
            'searchQuery'  => 'assessor_search_query',
        ];
    }


}