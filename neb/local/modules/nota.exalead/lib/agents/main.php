<?
/**
 * Основные агенты модуля nota.exalead
 */
namespace Nota\Exalead\Agents;

use \Bitrix\Main\Config\Option,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient;

class Main
{
    /**
     * Агент получает количество изданий из Exalead и записывает его в Битрикс
     *
     * @return string
     */
    public static function setCountBook()
    {
		// 1. Количество карточек изданий
        self::setParamCount('#all', 'book_count_catalog');

        // 2. Количество объекто с электронной копией полное (издания + разметка)
        self::setParamCount('filesize>0', 'book_count_total');

        // 3. Количество изданий с электронной копией с авторским правом
        self::setParamCount('filesize>0 AND isprotected:1 AND NOT ispart:1', 'book_count_closed');

        // 4. Количество изданий с электронной копией без авторского права
        self::setParamCount('filesize>0 AND isprotected:0 AND NOT ispart:1', 'book_count_open');

        // 5. Количество изданий с электронной копией заблокированных
        self::setParamCount('filesize>0 AND isprotected:2 AND NOT ispart:1', 'book_count_copyright');

        // 6. Количество произведений
        self::setParamCount('filesize>0 AND ispart:1', 'book_count_markup');

        return __METHOD__ . '();';
    }

    /**
     * Метод обращается к Exalead, получает из него количество изданий и записывает из в настройки модуля nota.exalead
     *
     * @param $q
     * @param $param
     */
    private static function setParamCount($q, $param) {

        $query = new SearchQuery($q);
        $query->setParam('hf', 0);
        $query->setParam('sl', 'sl_statistic3');
        $query->setTimeout(600);
        //$qp = $query->getParameters(); debugPre($qp);

        $client = new SearchClient();
        $result = $client->getResult($query);

        if (intval($result['COUNT']) > 0) {
            Option::set('nota.exalead', $param, intval($result['COUNT']));
        }
    }
}