<?php
$MESS["ERROR_MIN_COUNT_SYMBOL"] = "Ошибка! Вы ввели слишком мало символов.";
$MESS["ERROR_EMPTY_STRING"] = "Ошибка! Вы не ввели название раздела.";
$MESS["ERROR"] = "Ошибка!";
$MESS["ERROR_SECTION_EXIST"] = "Ошибка! Раздел с таким именем уже существует. Пожалуйста введите другое название раздела.";
$MESS["ADD_SECTION_SUCCESS_1"] = "Добавление раздела с id=<b>";
$MESS["ADD_SECTION_SUCCESS_2"] = "</b> произведено успешно!";
$MESS["ADD_SECTION_ERROR"] = "Ошибка добавления раздела!";