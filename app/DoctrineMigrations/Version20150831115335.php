<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150831115335 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery(<<<SQL
ALTER TABLE neb_stat_library
ADD ACTIVE_USERS INT
AFTER VIEWS,
ADD DELETED_USERS INT
AFTER ACTIVE_USERS,
ADD DOWNLOADS INT
AFTER DELETED_USERS,
ADD SEARCH_COUNT INT
AFTER DOWNLOADS,
ADD FEEDBACK_COUNT INT
AFTER SEARCH_COUNT,
ADD VIEWS_BOOK INT
AFTER FEEDBACK_COUNT;
SQL
);
        $this->connection->executeQuery(<<<SQL
CREATE UNIQUE INDEX library_stat ON neb_stat_library (LIBRARY_ID, DATE);
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->executeQuery(<<<SQL
ALTER TABLE neb_stat_library
DROP COLUMN ACTIVE_USERS,
DROP COLUMN DELETED_USERS,
DROP COLUMN DOWNLOADS,
DROP COLUMN SEARCH_COUNT,
DROP COLUMN FEEDBACK_COUNT,
DROP COLUMN VIEWS_BOOK;
SQL
        );
        $this->connection->executeQuery(<<<SQL
DROP INDEX library_stat ON neb_stat_library;
SQL
        );
    }
}
