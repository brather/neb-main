<?php
$MESS["ERROR_MIN_COUNT_SYMBOL"] = "Error! You have entered too few characters.";
$MESS["ERROR_EMPTY_STRING"] = "Error! You did not fill one of the fields.";
$MESS["ERROR"] = "Error!";
$MESS["ERROR_ITEM_EXIST"] = "Error! This element exist. Please, enter another element name.";
$MESS["ADD_ITEM_SUCCESS_1"] = "Add item with id=<b>";
$MESS["ADD_ITEM_SUCCESS_2"] = "</b> was success!";
$MESS["ADD_ITEM_ERROR"] = "Error! Add item was not success.";
$MESS["ERROR_SECTION_NAME_EMPTY"] = "Error! Empty section name.";
$MESS["ERROR_DETAIL_TEXT_EMPTY"] = "Error! Please, enter detail text.";