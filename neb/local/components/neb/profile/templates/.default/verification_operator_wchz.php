<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
global $APPLICATION;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Верификация пользователей');

global $APPLICATION;

$APPLICATION->IncludeComponent(
    'neb:user.verification',
    '.default',
    array(
        'COMPONENT_TEMPLATE' => '.default',
        'SEF_MODE' => 'Y',
        'SEF_FOLDER' => $arParams['SEF_FOLDER'] . str_replace('(.*)', '', $arParams['SEF_URL_TEMPLATES']['verification']),
        'SEF_URL_TEMPLATES' => array(
            'list'          => '/',
            'user_add'      => 'user/add/',
            'user_edit'     => 'user/edit/#ID#/',
            'user_deact'    => 'user/deact/#ID#/',
            'ticket_add'    => 'ticket/add/',
            'ticket_edit'   => 'ticket/edit/#ID#/',
            'ticket_delete' => 'ticket/delete/#ID#/',
        )
    ),
    false
);