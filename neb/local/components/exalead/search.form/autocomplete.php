<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

/**
 * Автозаполнение полей поиска
 */

require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once ($_SERVER["DOCUMENT_ROOT"] . '/local/tools/suggest/ReflectionTypeHint.php');
require_once ($_SERVER["DOCUMENT_ROOT"] . '/local/tools/suggest/Text/LangCorrect.php');
require_once ($_SERVER["DOCUMENT_ROOT"] . '/local/tools/suggest/UTF8.php');

use \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchQueryLibrary,
    \Nota\Exalead\SearchQueryMuseum,
    \Nota\Exalead\SearchQueryArchive,
    \Nota\Exalead\SearchClient,
    \Bitrix\NotaExt\Utils,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class LibSearchFormAutoComplete {

    const LIMIT = 10; // ограничение на количество результатов автокомплита

    private $sQuery = '', $arResult = [];

    public function getResult() {

        $this->prepareQuery();
        $this->prepareModule();
        $this->switchAction();
        $this->showJson();
    }

    /**
     * Подготовка запроса
     *
     * @return bool|string
     */
    private function prepareQuery() {

        $corrector = new Text_LangCorrect();

        $q = trim(htmlspecialcharsEx($_REQUEST['term']));
        if (empty($q))
            exit();

        $q = $corrector->parse($q, $corrector::KEYBOARD_LAYOUT);

        $this->sQuery = $q;
    }

    /**
     * Подготовка модулей
     */
    private function prepareModule() {

        if (!Loader::includeModule('nota.exalead'))
            die();

        if (!check_bitrix_sessid())
            exit(0);
    }

    private function switchAction() {

        if ($_REQUEST['type'] == 'double') {
            $this->getDoubleSuggestion();
        } else {
            $this->getSimpleSuggestion();
        }
    }

    /**
     * Получение подсказок для двойного поиска
     */
    private function getDoubleSuggestion() {

        $queryUrl = '';
        $arEntity = [];

        //SearchQuery
        if ($_REQUEST['field'] == 'authorbook') {

            /* ?term=Пушкин&sessid=39e9486e9cbe1fec56f81da8479fafc0&field=authorbook
             * /search-api/search?hf=15&sl=sl_suggests2&q=authorbook:(Пушкин*) */
            /*$query = new SearchQuery();
            $query->setParam('sl', 'sl_suggests2');
            $query->setParam('hf', '15');
            $query->setQuery('authorbook:(*'.htmlspecialchars($_REQUEST['term']).')');

            $client = new SearchClient();
            $result = $client->getResult($query);
            $queryUrl = $query->getUrl() . '?' . $client->prepParameters($query->getParameters());

            foreach ($result['GROUPS']['AUTHORBOOK'] as $arItem) {
                $arEntity[] = [
                    'title' => $arItem['title'],
                    'count' => $arItem['count'],
                    'id'    => $arItem['id']
                ];
            }*/

            $queryAuthor = new SearchQueryLibrary();
            $queryAuthor->getSuggestAuthor($this->sQuery);

            $client = new SearchClient();
            $result = $client->getResult($queryAuthor);

            foreach ($result as $sAuthor) {
                $arEntity[] = ['title' => $sAuthor];
            }
        }
        elseif ($_REQUEST['field'] == 'title') {

            /* ?term=Золотая рыбка&sessid=39e9486e9cbe1fec56f81da8479fafc0&field=title&filter[authorbook]=Пушкин
             * /search-api/search?hf=15&sl=sl_suggests2&q=title:(Золотая рыбка*) AND authorbook:(Пушкин*) */

            $query = new SearchQuery();
            $query->setParam('sl', 'sl_suggests2');
            $query->setParam('hf', '15');
            $query->setQuery('title:(' . htmlspecialchars($_REQUEST['term'])
                . '*) AND authorbook:(' . htmlspecialchars($_REQUEST['filter']['authorbook']) . '*)');

            $client = new SearchClient();
            $result = $client->getResult($query);
            $queryUrl = $query->getUrl() . '?' . $client->prepParameters($query->getParameters());

            foreach ($result['ITEMS'] as $arItem) {
                $arEntity[] = ['title' => $arItem['name']];
            }
        }

        $this->arResult = [
            'query' => $queryUrl,
            'field' => $_REQUEST['field'],
            'entities' => $arEntity
        ];

    }

    /**
     * Обычный и расширенный тип поиска
     */
    private function getSimpleSuggestion() {

        $bDisser = $bPatent = false;
        foreach ($_REQUEST['coll'] as $sColl) {
            if (false !== mb_stripos($sColl, 'Автореферат')) {
                $bDisser = true;
            } elseif (false !== mb_stripos($sColl, 'Патент')) {
                $bPatent = true;
            }
        }

        switch ($_REQUEST['librarytype']) {
            case 'museum':
                $queryTitle = new SearchQueryMuseum();
                $queryAuthor = new SearchQueryMuseum();
                break;
            case 'archive':
                $queryTitle = new SearchQueryArchive();
                $queryAuthor = new SearchQueryArchive();
                break;
            case 'library':
            default:
                $queryTitle = new SearchQueryLibrary();
                $queryAuthor = new SearchQueryLibrary();
        }

        if ($bDisser && $bPatent) {

            $queryTitle->getSuggestTitleDisserPatent($this->sQuery);
            $queryAuthor->getSuggestAuthorDisserPatent($this->sQuery);

        } elseif ($bDisser && !$bPatent) {

            $queryTitle->getSuggestTitleDisser($this->sQuery);
            $queryAuthor->getSuggestAuthorDisser($this->sQuery);

        } elseif (!$bDisser && $bPatent) {

            $queryTitle->getSuggestTitlePatent($this->sQuery);
            $queryAuthor->getSuggestAuthorPatent($this->sQuery);

        } else {

            $queryTitle->getSuggestTitle($this->sQuery);
            $queryAuthor->getSuggestAuthor($this->sQuery);
        }

        $arResult = [];
        $client = new SearchClient();
        $result = $client->getResult($queryTitle);

        if (!empty($result)) {
            foreach ($result as $v) {
                $_v = Utils::trimming_line($v, 60, '');
                $arResult[] = array('label' => $_v, 'original' => $v, 'category' => Loc::getMessage('LIB_SEARCH_FORM_AUTOCOMPLETE_BOOK'), 'field' => 'title');
            }
        }

        $result = $client->getResult($queryAuthor);

        $arResult = array_slice($arResult, 0, self::LIMIT - (count($result) < 5 ? count($result) : 5), true);
        if (!empty($result)) {
            foreach ($result as $v) {
                $_v = Utils::trimming_line($v, 60, '');
                $arResult[] = array('label' => $_v, 'original' => $v, 'category' => Loc::getMessage('LIB_SEARCH_FORM_AUTOCOMPLETE_AUTHOR'), 'field' => 'author');
            }
        }

        // вывод не более 10 элементов
        $arResult = array_slice($arResult, 0, self::LIMIT, true);

        $this->arResult =  $arResult;
    }

    /**
     * Вывод результата в виде JSON-массива
     */
    private function showJson() {
        MainHelper::showJson($this->arResult);
    }
}

$obAutoComplete = new LibSearchFormAutoComplete();
$obAutoComplete->getResult();