<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
require(dirname(__FILE__) . '/includes/header.php');
$APPLICATION->IncludeComponent(
    'neb:create.book.request',
    '',
    array(
        'REQUEST_TYPE' => 'rightholder',
        'LIBRARY_ID'   => LIB_ID_RIGHTHOLDER,
        'LIBRARY_NAME' => 'Библиотека правообладателей',
        'RESULT_URI'   => '/profile/',
    ),
    $component
);
require(dirname(__FILE__) . '/includes/footer.php');
