<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?
\CUtil::InitJSCore(array("jquery"));
use \Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs('/lib/plupload/js/plupload.full.min.js');
Asset::getInstance()->addJs(
    $component->__path . '/lib/plupload/js/i18n/' . LANGUAGE_ID . '.js'
);
?>
<div class="fieldcell nowrap photorow hidden" id="drag_ducuments">
    <em class="hint">*</em>
    <label><?= GetMessage("UPLOAD_RIGHTHOLDER_DOCUMENTS"); ?>:</label>

    <div class="field validate">
        <div class="setscan iblock"
             id="container_<?= $arParams['RAND_STR'] ?>">
            <input type="hidden" class="photofile"
                   name="rightholder_document" value="">
            <a href="#" id="upload_ducument"><?= GetMessage(
                    "UPLOAD_IMAGE"
                ); ?></a>

            <div class="setphoto_lb"><?= GetMessage(
                    "DRAG_INTO_FIELD"
                ); ?></div>
            <div class="progressbar" id="ducments_progressbar"
                 style="display: none;">
                <div class="progress">
                    <span style="width:0px;"></span>
                </div>
                <div class="text"><?= GetMessage("UPLOADING"); ?> <span
                        class="num">0</span>%
                </div>
            </div>
        </div>
        <div class="b-scan_photo iblock">
            <img src="/bitrix/images/1.gif" alt="" id="document_thumbnail"
                 height="161">
        </div>
        <em class="error required"><?= GetMessage(
                "ERROR_UPLOAD_SCAN"
            ); ?></em>
    </div>
</div>
<div id="rightholder-documents">
    <?php
    $document = 1;
    while (isset($_REQUEST['rightholder_document' . $document])) {
        ?>
        <div class="fieldcell nowrap photorow" id="drag_ducuments<?=$document?>">
            <em class="hint">*</em>
            <label><?= GetMessage("UPLOAD_RIGHTHOLDER_DOCUMENTS"); ?>:</label>

            <div class="field validate">
                <div class="setscan iblock"
                     id="container_<?= $arParams['RAND_STR'] ?><?=$document?>">
                    <input type="hidden" class="photofile"
                           name="rightholder_document<?=$document?>"
                           value="<?=$_REQUEST['rightholder_document' . $document]?>">
                    <a href="#" id="upload_ducument<?=$document?>"><?= GetMessage(
                            "UPLOAD_IMAGE"
                        ); ?></a>

                    <div class="setphoto_lb"><?= GetMessage(
                            "DRAG_INTO_FIELD"
                        ); ?></div>
                    <div class="progressbar" id="ducments_progressbar<?=$document?>"
                         style="display: none;">
                        <div class="progress">
                            <span style="width:0px;"></span>
                        </div>
                        <div class="text"><?= GetMessage("UPLOADING"); ?> <span
                                class="num">0</span>%
                        </div>
                    </div>
                </div>
                <div class="b-scan_photo iblock">
                    <img src="<?=$_REQUEST['rightholder_document' . $document]?>" alt=""
                         id="document_thumbnail<?=$document?>"
                         height="161">
                </div>
                <em class="error required"><?= GetMessage(
                        "ERROR_UPLOAD_SCAN"
                    ); ?></em>
            </div>
        </div>
        <?php
        $document++;
    }
    ?>
</div>
<span id="plus-one-file" class="plus-one-file">+</span>

<div style="display: none;" class="pl_button"></div>
<pre style="display: none;"
     id="plupload_console_<?= $arParams['RAND_STR'] ?>"></pre>
<style>
    .plus-one-file {
        cursor: pointer;
        border: 1px solid #c4cdd0;
        width: 1em;
        height: 1em;
        text-align: center;
        color: #9ea2a8;
        display: inline-block;
        vertical-align: middle;
        -webkit-border-radius: 0.5em;
        -moz-border-radius: 0.5em;
        border-radius: 0.5em;
        font-size: 4em;
        line-height: 0.9em;
    }
</style>
<script type="text/javascript">
    $(function () {
        var params = {
            browseButton: 'upload_ducument',
            dropElement: 'drag_ducuments',
            container: 'container_<?=$arParams['RAND_STR']?>',
            thumbnailBlock: 'document_thumbnail',
            uploadProgressbar: 'ducments_progressbar'
        };
        var fileInputName = 'rightholder_document';
        var blocksIndex = 1;

        var initPlupLoad = function () {
            var i = (0 === blocksIndex ? '' : blocksIndex.toString());
            var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
                {
                    runtimes: 'html5,flash,html4',
                    browse_button: params.browseButton + i,
                    drop_element: params.dropElement + i,
                    container: document.getElementById(params.container + i),
                    <?
                        if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
                        {
                        ?>
                    unique_names: true,
                    <?
                    }
                ?>

                    url: '<?=$component->__path?>/ajax.php',
                    filters: {
                        max_file_size: '<?=$arParams['MAX_FILE_SIZE']?>mb',
                        prevent_duplicates: true
                        <?
                            if(!empty($arParams['FILE_TYPES'])){
                            ?>
                        ,
                        mime_types: [
                            {
                                title: "Mine files",
                                extensions: '<?=$arParams['FILE_TYPES']?>'
                            }
                        ]
                        <?
                        }
                    ?>
                    },
                    <?
                        if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
                        {
                        ?>
                    resize: {
                        width: <?=intval($arParams['RESIZE_WIDTH'])?>,
                        height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
                        <?
                            if($arParams['RESIZE_CROP'] == 'Y')
                            {
                            ?>
                        crop: true,
                        <?
                        }
                    ?>
                        quality: <?=intval($arParams['RESIZE_QUALITY'])?>
                    },
                    <?
                    }
                ?>
                    max_file_size: '<?=$arParams['MAX_FILE_SIZE']?>mb',
                    chunk_size: '100kb',
                    flash_swf_url: '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

                    multipart_params: {
                        plupload_ajax: 'Y',
                        sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
                        aFILE_TYPES: '<?=$arParams['FILE_TYPES']?>',
                        aDIR: '<?=$arParams['DIR']?>',
                        aMAX_FILE_SIZE: '<?=$arParams['MAX_FILE_SIZE']?>',
                        aMAX_FILE_AGE: '<?=$arParams['MAX_FILE_AGE']?>',
                        aFILES_FIELD_NAME: '<?=$arParams['FILES_FIELD_NAME']?>',
                        aMULTI_SELECTION: '<?=$arParams['MULTI_SELECTION']?>',
                        aCLEANUP_DIR: '<?=$arParams['CLEANUP_DIR']?>',
                        aRAND_STR: '<?=$arParams['RAND_STR']?>',
                        aTHUMBNAIL_HEIGHT: 161,
                        aTHUMBNAIL_WIDTH: 226
                    },
                    multi_selection: false,
                    init: {

                        FilesAdded: function (up) {
                            up.start();
                        },
                        FileUploaded: function (up, file, response) {
                            var thumbnail = null;
                            if(up.files.length > 0) {
                                var fileName = up.files[0].target_name;
                                switch(fileName.split('.').pop()) {
                                    case 'doc':
                                    case 'docx':
                                        thumbnail = '/local/templates/.default/markup/i/word-icon.jpg';
                                        break;
                                    case 'pdf':
                                        thumbnail = '/local/templates/.default/markup/i/pdf-icon.png';
                                        break;
                                }
                            }
                            var result = response.response;
                            if (result) {
                                var obResponse = JSON.parse(result);
                                if(null === thumbnail) {
                                    thumbnail = obResponse.thumbnail;
                                }
                                $('#' + params.thumbnailBlock + i).
                                    attr('src', thumbnail);
                                $('input[name="' + fileInputName + i + '"]').val(obResponse.file);
                            }
                        },
                        UploadProgress: function (up, file) {
                            $('#' + params.uploadProgressbar + i).show();
                            $('#' + params.uploadProgressbar + i + ' .progress span').css('width', file.percent + "%");
                            $('#' + params.uploadProgressbar + i + ' .text .num').text(file.percent);
                        },
                        Error: function (up, err) {
                            document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
                        }
                    }
                }
            );

            uploader_<?=$arParams['RAND_STR']?>.init();
        };

        var clonePluploadBlock = function () {
            var i = (0 === blocksIndex ? '' : blocksIndex.toString());
            var blocksCotainer = $('#rightholder-documents');
            var block = $('#' + params.dropElement);
            block = block.clone();
            block.attr('id', params.dropElement + i);
            for (var key in params) {
                if (!params.hasOwnProperty(key)) {
                    continue;
                }
                var element = block.find('#' + params[key]);
                if (element.length < 1) {
                    continue;
                }
                element.attr('id', params[key] + i);
            }
            var input = block.find('input[name="' + fileInputName + '"]');
            if (input.length > 0) {
                input.attr('name', fileInputName + i);
            }
            blocksCotainer.append(block);
            block.removeClass('hidden');
        };

        if($('#rightholder-documents').find('.fieldcell').length < 1) {
            clonePluploadBlock();
        }
        initPlupLoad();

        $('#plus-one-file').click(function () {
            blocksIndex++;
            clonePluploadBlock();
            initPlupLoad();
        });
    });
</script>