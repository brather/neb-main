<?php

namespace AssessorBundle\Entity;

use AssessorBundle\Traits\AssessorEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;


/**
 * TblQueryRaiting
 *
 * @ORM\Table(name="tbl_query_raiting", indexes={@ORM\Index(name="IDX_tbl_query_raiting_IdQuery", columns={"IdQuery"})})
 * @ORM\Entity(repositoryClass="AssessorBundle\Entity\Repository\CommonRepository")
 */
class TblQueryRating implements AssessorRatingEntityInterface
{
    use AssessorEntityTrait;
    /**
     * @var integer
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serialization\Groups({"list", "details"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdQuery", type="integer", nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    private $idquery;

    /**
     * @var string
     *
     * @ORM\Column(name="FullSymbolicId", type="string", length=100, nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    private $fullsymbolicid;

    /**
     * @var integer
     *
     * @ORM\Column(name="Raiting", type="integer", nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    private $rating;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdquery()
    {
        return $this->idquery;
    }

    /**
     * @param int $idquery
     */
    public function setIdquery($idquery)
    {
        $this->idquery = $idquery;
    }

    /**
     * @return string
     */
    public function getFullsymbolicid()
    {
        return $this->fullsymbolicid;
    }

    /**
     * @param string $fullsymbolicid
     */
    public function setFullsymbolicid($fullsymbolicid)
    {
        $this->fullsymbolicid = $fullsymbolicid;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }
}
