<?php

use \Neb\Main\Helper\IpHelper;

/**
 * Сервис импорта виртуальных читальных залов НЭБ
 */
class ImportWorkspaces
{
    public static function import($sFileAddress, $bClearLog = false)
    {
        self::importModules();
        self::checkLogTable();

        $arImportData = self::getDataFromExcel($sFileAddress);

        self::setWorkplaces($arImportData, $bClearLog);
    }

    private static function setWorkplaces($arImportData, $bClearLog = false) {

        $arWorkplacesAdd = $arWorkplacesUpdate = $arWorkplacesDelete = array();

        $arTemp = $arImportData; $arImportData = array();
        foreach ($arTemp as $arItem)
            $arImportData[$arItem['id']] = $arItem;

        $arWorkplaces = self::getWorkplaces();

        foreach ($arImportData as $arItem) {
            if ($arWorkplaces[$arItem['id']]['ID'])
                $arWorkplacesUpdate[$arWorkplaces[$arItem['id']]['ID']] = $arItem;
            else
                $arWorkplacesAdd[] = $arItem;
        }
        // поиск ВЧЗ на удаление
        foreach ($arWorkplaces as $iRgbId => $arItem)
            if (empty($arImportData[$iRgbId]))
                $arWorkplacesDelete[] = $arItem['ID'];

        // очистка таблицы логов
        if ($bClearLog)
            self::clearLogTable();

        self::addWorkplaces($arWorkplacesAdd);    // добавление отсутствующих
        self::updateWorkplaces($arWorkplacesUpdate); // обновление имеющихся
        self::deleteWorkplaces($arWorkplacesDelete); // удаление только инфоблочных
    }

    /**
     * Добавление ВЧЗ в инфоблок из массива
     *
     * @param $arWorkplaces
     * @return bool
     */
    private static function addWorkplaces($arWorkplaces) {

        if (empty($arWorkplaces))
            return false;

        $obUser          = new \CUser;
        $obIBlockElement = new \CIBlockElement;

        // Получение всех библиотек и установка их ключей - RGB ID
        $arNetLibsTemp = ImportLibs::getNebLibs();
        foreach ($arNetLibsTemp as $arItem)
            if ($arItem['UF_RGB_ID'])
                $arNetLibs[$arItem['UF_RGB_ID']] = $arItem;
        unset($arNetLibsTemp);

        // обработка данных
        foreach ($arWorkplaces as $arItem) {

            $sStatus = $sInfo = null;

            $arItem['ip_list'] = self::getIpArray($arItem['ip']);

            // ошибка ip-адреса => запись в лог
            if (empty($arItem['ip_list'])) {
                $sStatus = 'ERROR';
                $sInfo   = 'Неправильный IP: ' . $arItem['ip'];
            }

            // ошибка нахождения библиотеки => запись в лог
            if (empty($arNetLibs[$arItem['lib_id']])) {
                $sStatus = 'ERROR';
                $sInfo .= "\n" . 'Библиотека не найдена!';
            }

            // добавление
            if (empty($sStatus)) {

                $arAddFields = Array(
                    "IBLOCK_ID"      => IBLOCK_ID_WORKPLACES,
                    "IBLOCK_SECTION" => false,
                    "NAME"           => $arItem['short_name'],
                    "ACTIVE"         => $arItem['active'] == 1 ? "Y" : "N",
                    'SORT'           => 1,
                    "PROPERTY_VALUES" => array(
                        'RGB_ID' => $arItem['id'],
                        'FULL_NAME' => $arItem['long_name'],
                        'LIBRARY'   => $arNetLibs[$arItem['lib_id']]['ID'],
                        'IP'        => $arItem['ip_list'],
                    ),
                    "CREATED_BY"  => $obUser->GetID(),
                    "MODIFIED_BY" => $obUser->GetID()
                );
                if ($iWorkPlace = $obIBlockElement->Add($arAddFields)) {
                    $sStatus = 'SUCCESS';
                    $sInfo   = $iWorkPlace;
                } else {
                    $sStatus = 'ERROR';
                    $sInfo   = $obIBlockElement->LAST_ERROR;
                }
            }

            // запись результата в лог
            if (!empty($sStatus))
                self::writeLog($arItem['id'], $arItem['lib_id'], $arItem['long_name'], $sStatus, $sInfo);
        }

        return true;
    }

    /**
     * Обновление ВЧЗ в инфоблоке из массива
     *
     * @param $arWorkplaces
     * @return bool
     */
    private static function updateWorkplaces($arWorkplacesUpdate) {

        if (empty($arWorkplacesUpdate))
            return false;

        $arWorkplacesDb = self::getWorkplaces();
        //debugPre($arWorkplacesDb);

        $obIBlockElement = new \CIBlockElement;

        // обработка данных
        foreach ($arWorkplacesUpdate as $arItem) {

            //debugPre($arItem);

            $sStatus  = $sInfo = null;
            $arUpdate = $arUpdateProp = array();

            if ($arItem['short_name'] != $arWorkplacesDb[$arItem['id']]['NAME'])
                $arUpdate['NAME'] = $arItem['short_name'];

            if ($arItem['long_name'] != $arWorkplacesDb[$arItem['id']]['PROPERTY_FULL_NAME_VALUE'])
                $arUpdateProp['FULL_NAME'] = $arItem['long_name'];

            // библиотеку воткнуть на обновление - PROPERTY_LIBRARY_VALUE

            // IP
            $arIP   = self::getIpArray($arItem['ip']);
            $arIpDb = $arWorkplacesDb[$arItem['id']]['PROPERTY_IP_VALUE'];
            sort($arIP); sort($arIpDb);
            if (!empty($arIP) && md5(implode(',', $arIP)) != md5(implode(',', $arIpDb)))
                $arUpdateProp['IP'] = $arIP;

            // ошибка ip-адреса => запись в лог
            if (!empty($arItem['ip']) && empty($arIP)) {
                $sStatus = 'ERROR';
                $sInfo   = 'Неправильный IP: ' . $arItem['ip'];
            }

            // добавление
            if (empty($sStatus)) {

                if (!empty($arUpdate)) {
                    if ($obIBlockElement->Update($arWorkplacesDb[$arItem['id']]['ID'], $arUpdate)) {
                        $sStatus = 'SUCCESS';
                        $sInfo = 'Обновлено: ' . $arWorkplacesDb[$arItem['id']]['ID'];
                    } else {
                        $sStatus = 'ERROR';
                        $sInfo = $obIBlockElement->LAST_ERROR;
                    }
                }

                if (!empty($arUpdateProp) && $sStatus != 'ERROR') {

                    foreach ($arUpdateProp as $k => $v)
                        $obIBlockElement->SetPropertyValues($arWorkplacesDb[$arItem['id']]['ID'], IBLOCK_ID_WORKPLACES, $v, $k);

                    if (empty($sStatus)) {
                        $sStatus = 'SUCCESS';
                        $sInfo = 'Обновлено: ' . $arWorkplacesDb[$arItem['id']]['ID'];
                    }
                }
            }

            // запись результата в лог
            if (!empty($sStatus))
                self::writeLog($arItem['id'], $arItem['lib_id'], $arItem['long_name'], $sStatus, $sInfo);
        }
    }

    /**
     * Удаление ВЧЗ в инфоблоке из массива
     *
     * @param $arWorkplaces
     * @return bool
     */
    private static function deleteWorkplaces($arWorkplaces) {

        if (empty($arWorkplaces))
            return false;

        $obIBlockElement = new \CIBlockElement;

        if (is_integer($arWorkplaces)) {

            $obIBlockElement->Delete($arWorkplaces);

        } elseif (is_array($arWorkplaces)) {

            foreach ($arWorkplaces as $iWorkplace)
                $obIBlockElement->Delete($iWorkplace);
        }
    }

    // Получает список
    public static function getWorkplaces() {

        $obIBlockElement = new \CIBlockElement;

        // выборка активных ВЧЗ
        $rsWorkPlaces = $obIBlockElement->GetList(
            array(),
            array(
                'IBLOCK_ID'        => IBLOCK_ID_WORKPLACES,
                'ACTIVE'           => 'Y',
                '!PROPERTY_RGB_ID' => false
            ),
            false,
            false,
            array(
                "ID",
                "IBLOCK_ID",
                "NAME",
                "SORT",
                'PROPERTY_FULL_NAME',
                'PROPERTY_LIBRARY',
                'PROPERTY_IP',
                'PROPERTY_RGB_ID'
            )
        );
        while($arWorkPlace = $rsWorkPlaces->Fetch())
            $arResult[$arWorkPlace['PROPERTY_RGB_ID_VALUE']] = $arWorkPlace;
        unset($rsWorkPlaces, $arWorkPlace);

        return $arResult;
    }

    /**
     * Запись в лог
     *
     * @param $sRgbId
     * @param $sLibId
     * @param $sName
     * @param $sStatus
     * @param $sInfo
     */
    private static function writeLog($sRgbId, $sLibId, $sName, $sStatus, $sInfo) {

        $arStatLog = array(
            'ID'     => 0,
            'DATE'   => 'now()',
            'RGB_ID' => $sRgbId,
            'LIB_ID' => $sLibId,
            'NAME'   => $sName,
            'STATUS' => $sStatus,
            'INFO'   => $sInfo
        );
        $strSql = self::buildInsertQuery('neb_log_import_workplaces', $arStatLog);
        \Bitrix\Main\Application::getConnection()->query($strSql);
    }

    // проверяет наличие таблицы логов
    private static function checkLogTable() {

        //\Bitrix\Main\Application::getConnection()->query('DROP TABLE IF EXISTS neb_log_import_workplaces;');
        $sQuery = '
            CREATE TABLE IF NOT EXISTS neb_log_import_workplaces (
              ID int NOT NULL AUTO_INCREMENT,
              DATE timestamp NOT NULL,
              RGB_ID int NOT NULL,
              LIB_ID int NOT NULL,
              NAME varchar(5000) NOT NULL,
              STATUS varchar(10) DEFAULT NULL,
              INFO  varchar(1000) DEFAULT NULL,
              PRIMARY KEY (ID)
            )
            ENGINE = INNODB;
        ';
        \Bitrix\Main\Application::getConnection()->query($sQuery);
    }

    // очистка старых логов
    private static function clearLogTable($iMaxTime = 30) {
        $sQuery = 'DELETE FROM neb_log_import_workplaces';
        if ($iMaxTime)
            $sQuery .= ' WHERE DATE <=' . (strtotime('now') - $iMaxTime * 60);
        \Bitrix\Main\Application::getConnection()->query($sQuery);
    }

    // собирает insert-запрос
    private static function buildInsertQuery($sTable, $arFields) {

        $sQuery = "INSERT INTO `".$sTable."` (";
        foreach ($arFields as $k => $v)
            if ($k == 'ID')
                $sQuery .= $k;
            else
                $sQuery .= ', '.$k;
        $sQuery .= ") VALUES (";
        foreach ($arFields as $k => $v)
            if ($k == 'ID')
                $sQuery .= "'0'";
            elseif ($k == 'DATE')
                $sQuery .= ", ".$v;
            else
                $sQuery .= ", '".$v."'";
        $sQuery .= ")";

        return $sQuery;
    }

    private static function getIpArray($sIpImport) {

        $bError   = false;
        $arResult = array();

        $arIp = explode('/', $sIpImport);
        foreach ($arIp as $sIp) {
            // диапазон адресов
            if (strpos($sIp, '-') !== false) {
                if (IpHelper::validateIpRange($sIp)) {
                    $arRange    = explode('-', $sIp);
                    $arResult[] = self::prepareIp($arRange[0]) . '-' . self::prepareIp($arRange[1]);
                } else
                    $bError = true;
            }
            // одиночный адрес
            else {
                if (IpHelper::validatePrivateIp(self::prepareIp($sIp)))
                    $arResult[] = $sIp;
                else
                    $bError = true;
            }
        }

        if ($bError)
            $arResult = array();

        return $arResult;
    }

    // подключаем модули
    private static function importModules()
    {
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel.php");

        \Bitrix\Main\Loader::IncludeModule("iblock");
        \Bitrix\Main\Loader::IncludeModule("highloadblock");
    }

    // подготовка результата Excel-файла
    public static function getDataFromExcel($filename)
    {
        $arExcel = self::readExcel($filename);

        $arResult = array();

        $arMapName = array(
            'ID'                               => 'id',
            'ID Библиотеки'                    => 'lib_id',
            'Название полное'                  => 'long_name',
            'Название на сайт'                 => 'short_name',
            'Активность'                       => 'active',
            'IP_адреса_или_диапазоны_адресов'  => 'ip',
            'комментарий'                      => 'commnet',
            'связанны ранее'                   => 'previously_linked'
        );

        foreach ($arExcel as $k => $arItem) {
            if (!$k) {
                foreach ($arItem as $key => $val)
                    if ($arMapName[$val])
                        $arNewMapName[$key] = $arMapName[$val];
            } else {
                $arRes = array();
                foreach ($arItem as $key => $val)
                    if (!empty($arNewMapName[$key]))
                        $arRes[$arNewMapName[$key]] = self::trimSpaces($val);
                    else
                        $arRes[$key] = self::trimSpaces($val);

                $arResult[$k - 1] = $arRes;
            }
        }

        return $arResult;
    }

    // подготовка IP-адреса к проверке
    private static function prepareIp($str) {
        $str = preg_replace('/\s/', '', $str);
        if (substr($str, strlen($str)-1, 1) == '.')
            $str = substr($str, 0, strlen($str)-1);
        return $str;
    }

    // множественный explode
    private static function multiexplode($delimiters, $string) {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }

    // удаление переносов строк, удаление лишних пробелов, обрезка по краям
    private static function trimSpaces($str) {
        return trim(preg_replace('/\s{2,}/', ' ', str_replace("\n", '', $str)));
    }

    // ================================================= EXCEL ======================================================

    // чтение Excel-файла
    private static function readExcel($filename)
    {
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $filename))
            die('File does not exist '. $_SERVER['DOCUMENT_ROOT'] . $filename);

        // получаем тип файла (xls, xlsx), чтобы правильно его обработать
        $file_type = PHPExcel_IOFactory::identify( $_SERVER['DOCUMENT_ROOT'] . $filename );

        // создаем объект для чтения
        $objReader = PHPExcel_IOFactory::createReader( $file_type );

        // загружаем данные файла в объект
        $objPHPExcel = $objReader->load( $_SERVER['DOCUMENT_ROOT'] . $filename );

        // выгружаем данные из объекта в массив
        $arResult = $objPHPExcel->getActiveSheet()->toArray();

        return $arResult;
    }

    // запись Excel-файла
    private static function writeExcel($arResult, $filename = '/saveLibs.xlsx')
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $filename))
            unlink($_SERVER['DOCUMENT_ROOT'] . $filename);

        if (empty($filename))
            die('Empty file name!');

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getActiveSheet()->fromArray($arResult, NULL, 'A1'); // A1 - ячейка с которой начинаем вставку

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save($_SERVER['DOCUMENT_ROOT'] . $filename);

        unset($objWriter);
    }
}