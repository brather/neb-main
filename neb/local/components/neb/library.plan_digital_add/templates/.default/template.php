<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru">
<![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="ru">
<![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="ru">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="ru">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <script src="<?=MARKUP?>js/libs/modernizr.min.js"></script>
    <meta name="viewport" content="width=device-width"/>
    <title><?$APPLICATION->ShowTitle()?></title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <?$APPLICATION->ShowHead();?>
    <link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />
    <?//$APPLICATION->SetAdditionalCSS(MARKUP.'css/style.css');?>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic,800,800italic,300,300italic&amp;subset=latin,cyrillic-ext,cyrillic'
          rel='stylesheet' type='text/css' />
    <link href="/local/templates/adaptive/css/bootstrap.min.css" rel="stylesheet">
    <link href="/local/templates/adaptive/vendor/custom-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="/local/templates/adaptive/css/font-awesome.min.css" rel="stylesheet">
    <link href="/local/templates/adaptive/vendor/jquery-ui/jquery-ui.css" rel="stylesheet">
    <link href="/local/templates/adaptive/css/style.css" rel="stylesheet">

    <?/*$APPLICATION->AddHeadScript(MARKUP.'js/libs/jquery.min.js');
        $APPLICATION->AddHeadScript('/local/templates/.default/js/script.js');
        $APPLICATION->AddHeadScript(MARKUP.'js/slick.min.js');
        $APPLICATION->AddHeadScript(MARKUP.'js/plugins.js');
        $APPLICATION->AddHeadScript(MARKUP.'js/jquery.knob.js');
        $APPLICATION->AddHeadScript(MARKUP.'js/script.js'); */?>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!--script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js" data-start></script-->
    <!--script src="/local/templates/adaptive/js/jquery/jquery-ui-1.10.4.custom.min.js"></script-->

    <script type="text/javascript">
        $(function() {
            $( document ).on( "click", "[data-toggle-edition-in-plan]", function() {
                var $this = $(this),
                    state = $this.attr('data-toggle-edition-in-plan'),
                    edition = $this.closest('[data-edition-item]'),
                    labelContainer = $this.siblings('.lbl'),
                    book_id = edition.attr('id'),
                    send = {
                        sessid  : '<?= bitrix_sessid() ?>',
                        BOOK_ID : book_id,
                        STATUS  : '',
                        plan_id : parseInt('<?= $_REQUEST["plan_id"] ?>'),
                        date    : '<?= $_REQUEST["date"] ?>'
                    },
                    url = {
                        plus : '/local/tools/plan_digitization/add.php',
                        minus : '/local/tools/plan_digitization/remove.php'
                    },
                    unstate = {
                        plus    : 'minus',
                        minus   : 'plus'
                    };

                $.post(url[state], send).done(function(){
                    $this.attr('data-toggle-edition-in-plan', unstate[state])
                        .data('', unstate[state]);

                    if (state == 'plus') {
                        labelContainer.html('Удалить');
                        $('.comment-form-' + book_id).show(300);
                    } else {
                        labelContainer.html('Добавить');
                        $('.comment-form-' + book_id)
                            .hide(300)
                            .children('.commentBook').val('');
                    }
                });
            });

            /* добавление / редактирование комментария */
            $('.addCommentForm').submit(function(e){
                e.preventDefault();
                $.ajax({
                    url:  $(this).attr('action'),
                    data: $(this).serialize(),
                    success: function(result){

                    }
                });
            });

            $( document ).on( "click", ".b-digitizing_mass_action a", function() {
                var aLink = this;
                if($(aLink).hasClass('minus'))
                {
                    $(aLink).removeClass('minus');
                    $(aLink).find('.wAdd').text('Добавить');
                    $(aLink).find('.wAdd2').text('в');

                    $('.search-result .plusico_wrap.plan-digitalization.minus').each(function() {
                        $(this).find('.plus_ico').click();
                    });

                }
                else
                {
                    $(aLink).addClass('minus');
                    $(aLink).find('.wAdd').text('Удалить');
                    $(aLink).find('.wAdd2').text('из');
                    $('.search-result .plusico_wrap.plan-digitalization').each(function() {
                        if(!$(this).hasClass('minus'))
                        {
                            $(this).find('.plus_ico').click();
                        }
                    });
                }

                return false;
            })

        });
    </script>

</head>
<body class="inmodal">
    <div class="modal-container modal-results">
        <?
        /*$APPLICATION->IncludeComponent(
            "exalead:search.form",
            "popup_plan_digit_add_library",
            Array(
                "PAGE" => (''),
                "POPUP_VIEW" => 'Y',
                "ACTION_URL" => $APPLICATION->GetCurPage(),
                "SHOW_URLS" => 0,
            )
        );*/
        $requestParams = array(
            "PLAN_ID" => $_REQUEST["plan_id"],
            "date" => $_REQUEST["date"]
        );

        /*$APPLICATION->IncludeComponent(
            "exalead:search.form",
            "",
            Array(
                "PAGE" => (''),
                "POPUP_VIEW" => 'Y',
                "ACTION_URL" => $APPLICATION->GetCurPage(),
                "SHOW_URLS" => 0,
                "REQUEST_PARAMS" => $requestParams
            )
        );*/
        $APPLICATION->IncludeComponent(
            "exalead:search.form",
            "redesign_extended",
            Array(
                "PAGE" => (''),
                "POPUP_VIEW" => 'Y',
                "PLAN_DIGITIZATION" => 'Y',
                "ACTION_URL" => $APPLICATION->GetCurPage(),
                "SHOW_URLS" => 0,
                "REQUEST_PARAMS" => $requestParams
            )
        );
        ?>
        <div class="container b-plan-add_digital" style="padding: 0;">
            <?
            $APPLICATION->IncludeComponent(
                "exalead:search.page",
                "popup_plan_digit_add_library",
                array(
                    'ID_LIBRARY' => $arParams['LIBRARY_ID'],
                    'NAV_PARENT' => '_self',
                    'UF_LIBRARY' => $arParams['UF_LIBRARY'],
                )
          	);
        	?>
        </div>
    </div>
    <script src="/local/templates/adaptive/vendor/custom-select/bootstrap-select.min.js"></script>
    <script src="/local/templates/adaptive/js/bootstrap.min.js"></script>
    <script src="/local/templates/adaptive/js/spin.min.js"></script>
    <script src="/local/templates/adaptive/js/script.js"></script>
</div>
</body>
</html>