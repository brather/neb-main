<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Iblock\SectionTable,
    \Bitrix\Iblock\ElementTable,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

class FaqItemAdd extends CBitrixComponent {
    //region Задание свойств для класса
    private $page = 'template';
    private $iblockId = '';
    private $minCountSymbol = 5;
    private $nameOfItem = '';
    private $detailTextItem = '';
    private $sectionIdForItem = '';
    private $itemId = '';
    private $indexSort = '500';
    private $arRequest = array();
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        if(!empty($arParams["IBLOCK_ID"])) {
            $this->iblockId = $arParams["IBLOCK_ID"];
        }
        if(!empty($arParams["MIN_COUNT_SYMBOL"])) {
            $this->minCountSymbol = $arParams["MIN_COUNT_SYMBOL"];
        }
        if(!empty($arParams["URL_PAGE"])) {
            $this->arResult["FORM_URL"] = $arParams["URL_PAGE"];
        }
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Исполнение компонента
    /**
     * Исполнение компонента
     */
    public function executeComponent() {
        $this->_getRequest();
        $this->_getListSections();
        $this->_getRequestNameItem();
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Проверка формы на ввод данных
    /**
     * Проверка формы на ввод данных
     */
    private function _getRequestNameItem() {
        if($this->arRequest["CONTROL_ADD_ITEM"] == "Y") {
            if(!empty($this->arRequest["NAME_ITEM"])) {
                $this->nameOfItem = $this->_checkString($this->arRequest["NAME_ITEM"]);
                $this->detailTextItem = $this->arRequest["DETAIL_TEXT_ITEM"];
                $detailTextWithoutTags = trim($this->_checkString($this->detailTextItem));
                $this->sectionIdForItem = $this->_checkString($this->arRequest["SECTION_NAME_FOR_ITEM"]);
                if($this->sectionIdForItem == '-') {
                    $this->arResult['ERROR'] = Loc::getMessage("ERROR_SECTION_NAME_EMPTY");
                } elseif(empty($detailTextWithoutTags)){
                    $this->arResult['ERROR'] = Loc::getMessage("ERROR_DETAIL_TEXT_EMPTY");
                } else {
                    if(!empty($this->arRequest["SORT_ITEM"])) {
                        $this->indexSort = $this->arRequest["SORT_ITEM"];
                    }
                    if((int)strlen($this->nameOfItem) > (int)$this->minCountSymbol) {
                        $this->_addItemToIblock();
                    } else {
                        $this->arResult['ERROR'] = Loc::getMessage("ERROR_MIN_COUNT_SYMBOL");
                    }
                }
            } else {
                $this->arResult['ERROR'] = Loc::getMessage("ERROR_EMPTY_STRING");
            }
        }
    }
    //endregion
    //region Возвращаем список разделов в которые нужно добавить элемент
    /** Возвращаем список разделов в которые нужно добавить элемент
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getListSections() {
        $resSections = SectionTable::GetList(array(
            "filter" => array("IBLOCK_ID" => $this->iblockId, "ACTIVE" => "Y"),
            "select" => array("ID", "NAME")
        ));
        while($obSection = $resSections->Fetch()) {
            $this->arResult["SECTIONS"][$obSection["ID"]] = $obSection["NAME"];
        }
    }
    //endregion
    //region Проверка строки на корректность
    /** Проверка строки на корректность
     * @param $string - проверяемая строка
     * @return string
     */
    private function _checkString($string) {
        $string = trim($string);
        $string = stripslashes($string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        return $string;
    }
    //endregion
    //region Добавление элемента
    /**
     * Добавление элемента
     */
    private function _addItemToIblock() {
        if($this->_checkExistItem()) {
            $this->arResult['ERROR'] = Loc::getMessage("ERROR_ITEM_EXIST");
        } else {
            $element = new CIBlockElement;
            $paramsTranslite = array("replace_space"=>"-", "replace_other"=>"-");
            $arFields = Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $this->iblockId,
                "NAME" => $this->nameOfItem,
                "CODE" => Cutil::translit($this->nameOfItem, "ru", $paramsTranslite),
                "IBLOCK_SECTION_ID" => $this->sectionIdForItem,
                "DETAIL_TEXT" => $this->detailTextItem,
                "SORT" => $this->indexSort
            );
            if($this->itemId = $element->Add($arFields)) {
                $this->arResult['SUCCESS'] = Loc::getMessage("ADD_ITEM_SUCCESS_1") . $this->itemId . Loc::getMessage("ADD_ITEM_SUCCESS_2");
            } else {
                $this->arResult['ERROR'] = Loc::getMessage("ADD_ITEM_ERROR")  . ' '  . $element->LAST_ERROR;
            }
        }
    }
    //endregion
    //region Проверка на существование элемента с таким же названием
    /** Проверка на существование элемента с таким же названием
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _checkExistItem() {
        $resItem = ElementTable::GetList(array(
            "filter" => array("IBLOCK_ID" => $this->iblockId, "NAME" => $this->nameOfItem),
            "select" => array("NAME", "ID")
        ));
        if($obItem = $resItem->Fetch()) return true;
        else return false;
    }
    //endregion
    //region Получение массива $_REQUEST (обертка D7)
    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public function _getRequest() {
        return $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }
    //endregion
}