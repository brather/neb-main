<?php

namespace AssessorBundle\Entity;

use AssessorBundle\Traits\AssessorEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * TblQueryFrequency
 *
 * @ORM\Table(name="tbl_query_frequency", uniqueConstraints={@ORM\UniqueConstraint(name="UK_tbl_query_frequency_Query", columns={"Query"})})
 * @ORM\Entity(repositoryClass="AssessorBundle\Entity\Repository\CommonRepository")
 * @UniqueEntity(fields="query", message="Такой запрос уже существует")
 */
class TblQueryFrequency implements AssessorRatingEntityInterface
{
    use AssessorEntityTrait;
    /**
     * @var integer
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serialization\Groups({"list", "details"})
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Query", type="string", length=255, nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    public $query;

    /**
     * @var integer
     *
     * @ORM\Column(name="Frequency", type="integer", nullable=true)
     * @Serialization\Groups({"list", "details"})
     */
    public $frequency;

    /**
     * @var integer
     *
     * @ORM\Column(name="Md5", type="string", length=32, nullable=true)
     */
    public $md5;

    /**
     * @var bool
     */
    public $hasSorting = false;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
        $this->setMd5(md5($query));
    }

    /**
     * @return int
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * @param int $frequency
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    }

    /**
     * @return int
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * @param int $md5
     */
    public function setMd5($md5)
    {
        $this->md5 = $md5;
    }

    /**
     * @return boolean
     */
    public function hasSorting()
    {
        return $this->hasSorting;
    }

    /**
     * @param boolean $hasSorting
     */
    public function setHasSorting($hasSorting)
    {
        $this->hasSorting = $hasSorting;
    }
}
