<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Bitrix\Main\Application,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\RslViewer;

Loader::includeModule('nota.exalead');

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

$sBookId  = trim($arRequest['book_id']);
$iPage    = intval($arRequest['p']);
$iWidth   = intval($arRequest['width']) > 0 ? intval($arRequest['width']) : 800;
$sSearch  = urlencode(str_replace(" ", "|", trim($arRequest['search'])));

$obRslViewer = new RslViewer($sBookId);
$obNUser     = new nebUser();

/*if ($obRslViewer->isAvailableType() != 'open') {
    exit('Доступ к изданию запрещен');
}*/

/* трекер переведен на агент \Neb\Main\Agents\StatsService::savePageView();
$arParams = [
    'book_id'    => $sBookId,
    'session_id' => session_id(),
    'page_num'   => $iPage,
    'user_id'    => intval($obNUser->GetID()),
];
if ($iLibraryId = intval($arRequest['library_id'])) {
    $arParams['library_id'] = $iLibraryId;
}
\Neb\Main\Stat\Tracker::track('neb_book_page_view', $arParams);*/

// для авторизации
if (!$obNUser->IsAuthorized()) {
    $_SESSION['AUTH_REFERER'] = '/catalog/' . $sBookId . '/viewer/?page=' . $iPage;
}

if (!$sSearch) {
    $jpg = $obRslViewer->getDocumentPage(($iWidth > 0 ? $iWidth : 1000), $iPage);
} else {
    $jpg = $obRslViewer->getDocumentPageSearch(($iWidth > 0 ? $iWidth : 1000), $iPage, $sSearch);
}

MainHelper::clearBuffer();
header('Content-Type: image/jpeg;charset=utf-8');
echo $jpg;