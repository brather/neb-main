<?php
/**
 * Сервис миграции данных между статистическими системами напрямую, без использования сервиса выдачи и Yii
 *
 * User: D-Efremov
 * Date: 12.04.2017
 * Time: 12:20
 */

namespace Neb\Main\Agents;

use \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\Loader,
    \Neb\Main\Helper\DbHelper;

/**
 * Class StatsService
 *
 * @package Neb\Main\Agents
 */
class StatsService
{
    /**
     * Производит ежедневную запись статистики просмотров изданий в сервис выдачи
     *
     * @return string
     */
    public static function saveTblAccessBookLogs()
    {
        $sResult = "\\" . __METHOD__ . '();';

        try {

            if (!Service::addCheckDate($sResult)) {
                return $sResult;
            }

            $sLastDay = (new DateTime())->add('-1 day')->format('Y-m-d');
            //$sLastDay = '2016.09.23'; /// test

            $libraryConnection = DbHelper::getLibraryConnection();
            $sSqlInsert = <<<SQL
        INSERT INTO tbl_access_book_logs
(
    EventDate,
    EventDateOnly,
    SessionId,
    Email,
    FullSymbolicId,
    AccessType,
    Os,
    LibraryType,
    Language,
    NewCollection,
    FileExtension,
    is_tenpercent,
    LibraryScale,
    BBKFull,
    LibraryName,
    IdLibrary,
    IdVchz,
    ViewCount,
    ViewDuration,
    ExternalIp
)
SELECT
    MIN(EventDate) AS `EventDate`,
    `al`.`EventDateOnly` AS `EventDateOnly`,
    `al`.`SessionId` AS `SessionId`,
    `al`.`Email` AS `Email`,
    `al`.`FullSymbolicId` AS `FullSymbolicId`,
    `al`.`AccessType` AS `AccessType`,
    `al`.`Os` AS `Os`,
    `al`.`LibraryType` AS `LibraryType`,
    `al`.`Language` AS `Language`,
    `al`.`NewCollection` AS `NewCollection`,
    `al`.`FileExtension` AS `FileExtension`,
    `al`.`is_tenpercent` AS `is_tenpercent`,
    `l`.`LibraryScale` AS `LibraryScale`,
    `al`.`BBKFull` AS `bbkfull`,
    `l`.`Name` AS `LibraryName`,
    `al`.`IdLibrary` AS `IdLibrary`,
    `al`.`IdVchz` AS `IdVchz`,
    COUNT(*) AS `ViewCount`,
    TIMEDIFF(MAX(EventDate) + INTERVAL 15 SECOND, MIN(EventDate)) AS ViewDuration,
    `al`.`ExternalIp` AS `ExternalIp`
FROM 
  tbl_access_logs al
JOIN
  tbl_libraries l ON l.Id = al.IdLibrary
WHERE 
    `al`.`StatusCode` = 200
    AND `al`.`Operation` = 'pageview'
    AND al.EventDateOnly = '$sLastDay'
GROUP BY
    `al`.`EventDateOnly`,
    `al`.`SessionId`,
    `al`.`FullSymbolicId`;

SQL;
            $libraryConnection->query($sSqlInsert);

        } catch (\Exception $e) {
            $log = new \CEventLog();
            $log->Log('ERROR', 'stats_service', 'neb.main', __FUNCTION__, strval($e));
        }

        return $sResult;
    }

    /**
     * Запись статистики скачивания книг. Замена для /neb/local/tools/exalead/getFiles.php
     *
     * @return string
     */
    public static function saveBookDownloads()
    {
        $sResult = "\\" . __METHOD__ . '();';

        try {

            if (!Service::addCheckDate($sResult))
                return $sResult;
            self::saveBookAction('resourcedownload', 'neb_book_downloads');

        } catch (\Exception $e) {
            $log = new \CEventLog();
            $log->Log('ERROR', 'stats_service', 'neb.main', __FUNCTION__, strval($e));
        }

        return $sResult;
    }

    /**
     * Запись статистики просмотра страниц книг. Замена для /neb/local/tools/exalead/getImages.php
     *
     * @return string
     */
    public static function savePageView()
    {
        $sResult = "\\" . __METHOD__ . '();';

        try {

            if (!Service::addCheckDate($sResult))
                return $sResult;
            self::saveBookAction('pageview', 'neb_book_page_view');

        } catch (\Exception $e) {
            $log = new \CEventLog();
            $log->Log('ERROR', 'stats_service', 'neb.main', __FUNCTION__, strval($e));
        }

        return $sResult;
    }

    /**
     * Перемещение статистики открытия изданий из сервиса выдачи в Yii
     *
     * @return string
     */
    public static function saveBookView()
    {
        $sResult = "\\" . __METHOD__ . '();';

        $sAction    = 'neb_book_view';
        $iLimit     = 25000;

        try {

            Service::setMemoryLimit('512M'); // с запасом

            // создание подключений
            $libraryConnection = DbHelper::getLibraryConnection();
            $statsConnection   = DbHelper::getStatsConnection();

            $arUsers    = self::getUsers();            // получение пользователей
            $arLibs     = self::getLibs();             // получение библиотек
            self::deleteAggregation($sAction, explode(' ', self::getLastDate($sAction))[0]); // удаление агрегации

            $iRowsCount = 1;
            while ($iRowsCount > 0 && Service::addCheckDate($sResult)) {

                $sEventDate = self::getLastDate($sAction); // получение последней даты загрузки данных

                $sSqlQuery = <<<SQL
    SELECT
      Id, EventDate, SessionId, Email, FullSymbolicId, AccessType, IdLibrary, IdVchz, ExternalIp
    FROM
      tbl_access_book_logs
    WHERE
      EventDate > '$sEventDate'
    ORDER BY
      Id ASC
    LIMIT $iLimit;
SQL;
                $rsDownloads = $libraryConnection->query($sSqlQuery);
                $iRowsCount  = $rsDownloads->getSelectedRowsCount();
                while ($arFields = $rsDownloads->fetch()) {

                    $sEventDate = (new DateTime($arFields['EventDate']))->format('Y-m-d H:i:s');
                    $iIp        = intval(ip2long($arFields['ExternalIp']));
                    $iUser      = intval($arUsers[$arFields['Email']]);
                    $iWchz      = intval($arFields['IdVchz']);

                    $sBookId    = trim($arFields['FullSymbolicId']);
                    $sSessionId = trim($arFields['SessionId']);
                    $iAccess    = intval($arFields['AccessType']);
                    $iNebLib    = intval($arFields['IdLibrary']);
                    $iLibrary   = intval($arLibs[$iNebLib]);

                    // первая вставка
                    $sInsertActionSQL = <<<SQL
    INSERT INTO
      neb_action
      (id, timestamp, action, ip, user_id, wchz_id)
    VALUES
      (0, '$sEventDate', '$sAction', $iIp, $iUser, $iWchz)
    ;
SQL;
                    $rsAction  = $statsConnection->query($sInsertActionSQL);
                    $iActionId = $statsConnection->getInsertedId();

                    // вторая вставка
                    $sInsertBookActionSQL = <<<SQL
    INSERT INTO
      neb_book_action
      (id, action_id, book_id, page_num, session_id, closed, library_id, neb_library_id)
    VALUES
      (0, $iActionId, '$sBookId', 0, '$sSessionId', $iAccess, $iLibrary, $iNebLib)
    ;
SQL;
                    $rsBookAction  = $statsConnection->query($sInsertBookActionSQL);
                    $iBookActionId = $statsConnection->getInsertedId();
                }
            }

        } catch (\Exception $e) {
            $log = new \CEventLog();
            $log->Log('ERROR', 'stats_service', 'neb.main', __FUNCTION__, strval($e));
        }

        return $sResult;
    }


    /*******************************************************************************************************************
     ******************************                  private - методы               ************************************
     ******************************************************************************************************************/

    /**
     * Трансфер статистики
     *
     * @param $sActionFrom
     * @param $sActionTo
     * @return array
     */
    private static function saveBookAction($sActionFrom, $sActionTo)
    {
        $arUsers    = self::getUsers(); // получение пользователей
        $arLibs     = self::getLibs();  // получение библиотек
        $sEventDate = self::getLastDate($sActionTo); // получение последней даты загрузки данных
        self::deleteAggregation($sActionTo, (explode(' ', $sEventDate)[0])); // удаление агрегации

        if (empty($arUsers) || empty($arLibs) || empty($sEventDate)) {
            return [];
        }

        // создание подключений
        $libraryConnection = DbHelper::getLibraryConnection();
        $statsConnection   = DbHelper::getStatsConnection();

        // получение статистики
        $sSqlQuery = <<<SQL
    SELECT
      EventDate, ExternalIp, Email, IdVchz, FullSymbolicId, Pagenum, SessionId, AccessType, IdLibrary
    FROM
      tbl_access_logs
    WHERE
      Operation = '$sActionFrom' AND EventDate > '$sEventDate'
    ORDER BY
      Id;
SQL;
        $rsDownloads = $libraryConnection->query($sSqlQuery);
        while ($arFields = $rsDownloads->fetch()) {

            $sEventDate = (new DateTime($arFields['EventDate']))->format('Y-m-d H:i:s');
            $iIp        = intval(ip2long($arFields['ExternalIp']));
            $iUser      = intval($arUsers[$arFields['Email']]);
            $iWchz      = intval($arFields['IdVchz']);

            $sBookId    = trim($arFields['FullSymbolicId']);
            $iPageNum   = intval($arFields['Pagenum']);
            $sSessionId = trim($arFields['SessionId']);
            $iAccess    = intval($arFields['AccessType']);
            $iNebLib    = intval($arFields['IdLibrary']);
            $iLibrary   = intval($arLibs[$iNebLib]);

            // первая вставка
            $sInsertActionSQL = <<<SQL
    INSERT INTO
      neb_action
      (id, timestamp, action, ip, user_id, wchz_id)
    VALUES
      (0, '$sEventDate', '$sActionTo', $iIp, $iUser, $iWchz)
    ;
SQL;
            $rsAction  = $statsConnection->query($sInsertActionSQL);
            $iActionId = $statsConnection->getInsertedId();

            // вторая вставка
            $sInsertBookActionSQL = <<<SQL
    INSERT INTO
      neb_book_action
      (id, action_id, book_id, page_num, session_id, closed, library_id, neb_library_id)
    VALUES
      (0, $iActionId, '$sBookId', $iPageNum, '$sSessionId', $iAccess, $iLibrary, $iNebLib)
    ;
SQL;
            $rsBookAction  = $statsConnection->query($sInsertBookActionSQL);
            $iBookActionId = $statsConnection->getInsertedId();
        }
    }

    /**
     * Получение пользователей
     *
     * @return array
     */
    private static function getUsers() {

        $arResult = [];

        $bitrixConnection = DbHelper::getBitrixConnection();

        $sTablesSQL = "SELECT ID, EMAIL FROM b_user";
        $rsUsers = $bitrixConnection->query($sTablesSQL);
        while ($arFields = $rsUsers->fetch()) {
            if (empty($arFields['EMAIL']))
                continue;
            $arResult[$arFields['EMAIL']] = $arFields['ID'];
        }

        return $arResult;
    }

    /**
     * Получение библиотек
     */
    private static function getLibs() {

        if (!Loader::includeModule('iblock')) {
            die('module iblock not installed');
        }

        $arResult = [];
        $rsLibs = \CIBlockElement::GetList(
            ['ID' => 'ASC'],
            ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, '!PROPERTY_LIBRARY_LINK' => false],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'PROPERTY_LIBRARY_LINK']
        );
        while ($arFields = $rsLibs->Fetch()) {
            $arResult[$arFields['PROPERTY_LIBRARY_LINK_VALUE']] = $arFields['ID'];
        }

        return $arResult;
    }

    /**
     *  Получение последней даты загрузки данных
     *
     * @param string $sAction
     * @return datetime
     */
    private static function getLastDate($sAction) {

        $statsConnection = DbHelper::getStatsConnection();

        $sLastDateSQL = <<<SQL
SELECT
  timestamp
FROM
  neb_action
WHERE
  action = '$sAction'
ORDER BY
  timestamp DESC
LIMIT 1;
SQL;
        $arMaxDate = $statsConnection->query($sLastDateSQL)->fetchRaw();

        return !empty($arMaxDate['timestamp']) ? $arMaxDate['timestamp'] : '1970-01-01 00:00:00';
    }

    /**
     * Удаление аггрегации
     *
     * @param $sAction
     * @param $sDate
     */
    private static function deleteAggregation($sAction, $sDate) {

        $statsConnection = DbHelper::getStatsConnection();

        $sDeleteAggSQL = <<<SQL
DELETE
FROM
  neb_aggregated_daily_report_data
WHERE
  report_code = '$sAction' AND date >= '$sDate';
;
SQL;
        $statsConnection->query($sDeleteAggSQL);
    }
}