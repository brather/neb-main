<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel/IOFactory.php");

CModule::IncludeModule('nota.exalead');
use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;

global $DB, $APPLICATION;

$APPLICATION->restartBuffer();
@ob_end_clean();

$strSql = "
        SELECT `BOOK_ID`, `SEARCH_QUERY` ,  COUNT(`BOOK_ID`) as `ccc`
        FROM `neb_stat_book_open`
        WHERE `TIMESTAMP` BETWEEN STR_TO_DATE('{$_REQUEST['date_from']} 00:00:00' , '%d.%m.%Y %H:%i:%s') AND STR_TO_DATE('{$_REQUEST['date_to']} 23:59:59' , '%d.%m.%Y %H:%i:%s')
        GROUP BY `BOOK_ID`, `SEARCH_QUERY`
        ORDER BY `SEARCH_QUERY`
        ";

$res = $DB->Query($strSql, false);

$objPHPExcel = new \PHPExcel();

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Статистика по отчётам');

$oldQuery = '';
$row      = 1;

$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "Запрос")->getStyle('A'.$row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, "ID")->getStyle('B'.$row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, "Название")->getStyle('C'.$row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, "Количество")->getStyle('D'.$row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, "Автор")->getStyle('E'.$row)->getFont()->setBold(true);

$row++;
$client = new SearchClient();
$query  = new SearchQuery();

while ($data = $res->Fetch()):
    if($oldQuery !== $data['SEARCH_QUERY']):
        $row++;
        if(!empty($data['SEARCH_QUERY'])):
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $data['SEARCH_QUERY'])->getStyle('A'.$row)->getFont()->setBold(true);
        else:
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Не определено')->getStyle('A'.$row)->getFont()->setBold(true);
        endif;

        $oldQuery = $data['SEARCH_QUERY'];
    endif;


    $books  = $query->getById($data['BOOK_ID']);
    $query->setParam('hf', 2);


    $eBook = $client->getResult($query);

    $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $data['BOOK_ID']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $eBook['title']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $data['ccc']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $eBook['authorbook']);

    $row++;
endwhile;

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);

$objPHPExcel->setActiveSheetIndex(0);


$strSql = "
        SELECT `BOOK_ID`, `SEARCH_QUERY`,  COUNT(`SEARCH_QUERY`) as `ccc`
        FROM `neb_stat_book_open`
        WHERE `TIMESTAMP` BETWEEN STR_TO_DATE('{$_REQUEST['date_from']} 00:00:00' , '%d.%m.%Y %H:%i:%s') AND STR_TO_DATE('{$_REQUEST['date_to']} 23:59:59' , '%d.%m.%Y %H:%i:%s')
        GROUP BY `BOOK_ID`, `SEARCH_QUERY`
        ORDER BY `BOOK_ID`
        ";

$res = $DB->Query($strSql, false);

$objPHPExcel->createSheet(1);
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle('Статистика по изданиям');


$oldQuery = '';
$row      = 1;


$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, "ID")->getStyle('A'.$row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, "Запрос")->getStyle('B'.$row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, "Количество")->getStyle('C'.$row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, "Название")->getStyle('D'.$row)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, "Автор")->getStyle('E'.$row)->getFont()->setBold(true);

$row++;
$client = new SearchClient();
$query  = new SearchQuery();

while ($data = $res->Fetch()):
    if($oldQuery !== $data['BOOK_ID']):
        $row++;
        if(!empty($data['BOOK_ID'])):
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $data['BOOK_ID'])->getStyle('A'.$row)->getFont()->setBold(true);
        else:
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, '')->getStyle('A'.$row)->getFont()->setBold(true);
        endif;

        $oldQuery = $data['BOOK_ID'];
    endif;


    $books  = $query->getById($data['BOOK_ID']);
    $query->setParam('hf', 2);


    $eBook = $client->getResult($query);



    if(!empty($data['SEARCH_QUERY'])):
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $data['SEARCH_QUERY'])->getStyle('A'.$row)->getFont()->setBold(true);
    else:
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, '')->getStyle('A'.$row)->getFont()->setBold(true);
    endif;

    $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $data['ccc']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $eBook['title']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $eBook['authorbook']);

    $row++;
endwhile;


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);




$objPHPExcel->setActiveSheetIndex(0);

header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="Отчёт по запросам и изданиям.xls"');
header('Cache-Control: max-age=0');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
header ('Cache-Control: public, must-revalidate');

$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
$objWriter->save('php://output');
@flush();
@ob_end_flush();
exit;