<?php
$MESS["ERROR_MIN_COUNT_SYMBOL"] = "Error! You have entered too few characters.";
$MESS["ERROR_EMPTY_STRING"] = "Error! You did not enter a section title.";
$MESS["ERROR"] = "Error!";
$MESS["ERROR_SECTION_EXIST"] = "Error! This section exist. Please, enter another section name.";
$MESS["ADD_SECTION_SUCCESS_1"] = "Add section with id=<b>";
$MESS["ADD_SECTION_SUCCESS_2"] = "</b> was success!";
$MESS["ADD_SECTION_ERROR"] = "Error! Add section was not success.";