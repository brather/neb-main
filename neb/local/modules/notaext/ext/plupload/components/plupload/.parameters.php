<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$arComponentParameters = array(
    'PARAMETERS' => array(
        'MAX_FILE_SIZE' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('MAX_FILE_SIZE'),
            'TYPE' => 'STRING',
            'DEFAULT' => '1024'
        ),
        'FILE_TYPES' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('FILE_TYPES'),
            'TYPE' => 'STRING',
            'DEFAULT' => 'jpg,jpeg,doc,docx,xls,pdf,zip,rar'
        ),
        'DIR' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('DIR'),
            'TYPE' => 'STRING',
            'DEFAULT' => ''
        ),
        'FILES_FIELD_NAME' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('FILES_FIELD_NAME'),
            'TYPE' => 'STRING',
            'DEFAULT' => 'plupload_files'
        ),
        'MULTI_SELECTION' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('MULTI_SELECTION'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y'
        ),
        'CLEANUP_DIR' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('CLEANUP_DIR'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y',
            "REFRESH" => "Y"
        )
    )
);

if ($arCurrentValues["CLEANUP_DIR"] == "Y") {
    $arComponentParameters['PARAMETERS']['MAX_FILE_AGE'] = array(
        'PARENT' => 'BASE',
        'NAME' => GetMessage('MAX_FILE_AGE'),
        'TYPE' => 'STRING',
        'DEFAULT' => '3600'
    );
}


?>