<?php
namespace Neb\Main;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Entity\StringField;

class GroupSubordinateTable extends DataManager
{
    public static function getTableName()
    {
        return 'b_group_subordinate';
    }

    public static function getMap()
    {
        return array(
            'ID'             => array(
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
            ),
            'AR_SUBGROUP_ID' => new StringField('AR_SUBGROUP_ID', array()),
            'GROUP'          => new ReferenceField(
                'GROUP',
                'Bitrix\Main\GroupTable',
                array('=this.ID' => 'ref.ID'),
                array('join_type' => 'LEFT')
            ),
        );
    }
}