<?php
include dirname(__FILE__) . 'common.php';
$MESS['READER_BLOCK_NO_ALLOWED_STATUS'] = 'Cannot lock reader with current status.';
$MESS['READER_VERIFY_NO_ALLOWED_STATUS'] = 'Cannot verify reader with current status.';
$MESS['READER_BLOCK_SUCCESS'] = 'Verification successfully canceled.';
$MESS['READER_UNBLOCK_SUCCESS'] = 'Reader successfully verify.';
$MESS['READER_BLOCK_ERROR'] = 'Error lock reader.';
$MESS['READER_VERIFY_ERROR'] = 'Error reader verification.';
$MESS['READER_BLOCK_ACCESS_ERROR'] = 'Error lock reader, as it is not assigned to your library.';
$MESS['READER_ACCESS_ERROR'] = 'Reader s not assigned to your library.';
$MESS['READER_BLOCK_RGB_ERROR'] = 'Error add user in rgb';