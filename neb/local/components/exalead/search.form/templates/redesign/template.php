<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

global $APPLICATION, $USER;

?><!-- Поисковая форма -->
<?/*hr/>
<details>
    <summary>$arResult[]</summary>
    <pre><?php print_r($arResult);?></pre>
</details>
<hr/*/?>
<div class="container searches-block">
    <form
        class="three-simple-search tss"
        data-session-id="<?= bitrix_sessid()?>"
        action="<?=!empty($arParams['ACTION_URL'])? $arParams['ACTION_URL'] : '/search/'?>"
        method="get"
        data-three-form
    >
        <fieldset>
            <legend><?= Loc::getMessage('SEARCH_FORM_SIMPLE_SEARCH') ?> <a class="tssfaqlabel"
                href="/how-to-search/#simplesearch" target="_blank" title="<?= Loc::getMessage('SEARCH_FORM_FAQ_TITLE') ?>"
                ><?= Loc::getMessage('SEARCH_FORM_FAQ_ANCHOR') ?></a></legend>
            <div class="tss-row">
                <div class="tss-cell">
                    <input
                        type="text"
                        name="q"
                        class="form-control input-lg virtkb"
                        data-autocomplete
                        data-src="<?= $componentPath . '/autocomplete.php?' . bitrix_sessid_get() ?>"
                        data-type="main-search-autocomplete"
                        data-library="<?= !empty($_REQUEST['librarytype'])? $_REQUEST['librarytype'] : 'library'?>"
                        placeholder="<?= Loc::getMessage('SEARCH_FORM_MORE_SIMPLE_PLACEHOLDER') ?>"
                        data-virtual-keyboard
                        value="<?= htmlspecialcharsEx($_REQUEST['QUERY']); ?>"
                    />
                </div>
                <div class="tss-cell">
                    <button class="btn btn-lg btn-primary"><?= Loc::getMessage('SEARCH_FORM_BUTTON') ?></button>
                </div>
            </div>

            <div class="appendix-row">
                <div class="three-collections-string tcs" data-string-checkbox-widget>
                    <select
                        multiple="true"
                        name="newcollection[]"
                        langcollection="<?=Loc::getMessage('SEARCH_FORM_CHOISE_COLLECTION')?>"
                        id="collection"
                    >
                        <? foreach($arResult['newcollection'] as $arCollection):?>
                            <option value="<?= $arCollection['NAME']; ?>"<? if(
                                empty($_REQUEST['newcollection']) && 'Y' == $arCollection['SELECT']
                                || in_array($arCollection['NAME'], $_REQUEST['newcollection'])
                            ) echo ' selected="true"';
                            ?>><?= LANGUAGE_ID == 'en' ? $arCollection['NAME_EN'] : $arCollection['NAME'] ?></option>
                        <? endforeach;?>
                    </select>
                    <span class="tcs-title"><?= Loc::getMessage('SEARCH_FORM_COLLECTIONS') ?></span>

                    <div class="more-list" data-other-widget>
                        <a href="#" data-more-button data-toggle="dropdown" class="tcs-item more-toggler" data-more-mobile-title="<?= Loc::getMessage('SEARCH_FORM_MORE_MOBILE') ?>" data-selected="false"><?= Loc::getMessage('SEARCH_FORM_MORE') ?></a>
                        <ul class="dropdown-menu dropdown-menu-right" data-other-container>
                            <?foreach($arResult['newcollection'] as $arCollection):?>
                                <a href="#" class="tcs-item" data-tcs-item data-value="<?= $arCollection['NAME']; ?>"<? if(
                                    empty($_REQUEST['newcollection']) && 'Y' == $arCollection['SELECT']
                                    || in_array($arCollection['NAME'], $_REQUEST['newcollection'])
                                ) echo ' data-selected="true"';
                                ?>><?= LANGUAGE_ID == 'en' ? $arCollection['NAME_EN'] : $arCollection['NAME'] ?></a>
                            <? endforeach;?>
                        </ul>
                    </div>
                </div>
                <div class="tss-extended-toggler">
                    <a href="/search/extended/" class="btn btn-link btn-lg"><?= Loc::getMessage('SEARCH_FORM_EXTENDED_SEARCH') ?></a>
                </div>
            </div>
            <?if ($USER->IsAdmin() || nebUser::checkElar() || EXALEAD_DEBUG_MODE): ?>
                <label for="debug_simple">
                    <input type="checkbox" id="debug_simple" name="debug" value="1" <?php
                    if(EXALEAD_DEBUG_MODE):?> checked="checked" <?php endif;?> />
                    <span class="lbl"><?=Loc::getMessage('SEARCH_FORM_DEBUG')?></span>
                </label>
            <? endif;?>
        </fieldset>
    </form>

    <form
        class="three-middle-search tms"
        data-session-id="<?= bitrix_sessid()?>"
        action="<?=!empty($arParams['ACTION_URL'])? $arParams['ACTION_URL'] : '/search/'?>"
        method="get"
        data-three-form
    >
        <fieldset>
            <legend><?= Loc::getMessage('SEARCH_FORM_DOUBLE_SEARCH') ?> <a class="tmsfaqlabel" target="_blank"
                href="/how-to-search/#titleauthorsearch" title="<?= Loc::getMessage('SEARCH_FORM_FAQ_TITLE') ?>"
                ><?= Loc::getMessage('SEARCH_FORM_FAQ_ANCHOR') ?></a></legend>
            <div class="tms-row">
                <div class="tms-cell">
                    <label><?= Loc::getMessage('SEARCH_FORM_AUTHOR') ?></label>
                </div>
                <div class="tms-cell">
                    <input
                        type="text"
                        name="q_author"
                        class="form-control input-lg"
                        data-autocomplete-item
                        data-query-field="authorbook"
                        data-entity-field="title"
                        placeholder="<?= Loc::getMessage('SEARCH_FORM_AUTHOR_PLACEHOLDER') ?>"
                        data-virtual-keyboard
                        <?php if (isset($_REQUEST['q_author'])): ?>
                            value="<?= htmlspecialcharsEx($_REQUEST['q_author']) ?>"
                        <?php endif; ?>
                    />
                </div>
                <div class="tms-cell">
                    <button class="btn btn-lg btn-primary"><?= Loc::getMessage('SEARCH_FORM_BUTTON') ?></button>
                </div>
            </div>
            <div class="tms-row">
                <div class="tms-cell">
                    <label><?= Loc::getMessage('SEARCH_FORM_TITLE') ?></label>
                </div>
                <div class="tms-cell">
                    <input
                        type="text"
                        name="q_name"
                        class="form-control input-lg"
                        data-autocomplete-item
                        data-query-field="title"
                        data-entity-field="title"
                        data-filtered-by="authorbook"
                        placeholder="<?= Loc::getMessage('SEARCH_FORM_TITLE_PLACEHOLDER') ?>"
                        data-virtual-keyboard
                        <? if (isset($_REQUEST['q_name'])): ?>
                            value="<?= htmlspecialcharsEx($_REQUEST['q_name']) ?>"
                        <? endif; ?>
                    />
                </div>
                <div class="tms-cell">
                    <a class="btn btn-lg btn-link" href="/search/extended/"><?= Loc::getMessage('SEARCH_FORM_EXTENDED_SEARCH') ?></a>
                </div>
            </div>

            <?if ($USER->IsAdmin() || nebUser::checkElar() || EXALEAD_DEBUG_MODE): ?>
                <label for="debug_double">
                    <input type="checkbox" id="debug_double" name="debug" value="1" <?php
                    if(EXALEAD_DEBUG_MODE):?> checked="checked" <?php endif;?> />
                    <span class="lbl"><?=Loc::getMessage('SEARCH_FORM_DEBUG')?></span>
                </label>
            <? endif;?>
        </fieldset>

        <?php if (!empty($_REQUEST['category_id'])): ?>
            <input type="hidden" name="category_id" value="<?=htmlspecialcharsbx($_REQUEST['category_id'])?>" />
        <?php endif; ?>
        <?php if (!empty($_REQUEST['category_name'])): ?>
            <input type="hidden" name="category_name" value="<?=htmlspecialcharsbx($_REQUEST['category_name'])?>" />
        <?php endif; ?>
    </form>
</div>
<!-- /Поисковая форма КОНЕЦ-->

<?php $APPLICATION->IncludeFile('/local/components/exalead/search.form/templates/.default/search_tags.php',
                                array('arParams' => $arParams, 'arResult' => $arResult, 'component' => $component)); ?>