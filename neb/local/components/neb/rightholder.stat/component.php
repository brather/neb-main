<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $USER;
if (!$USER->IsAuthorized())
    $APPLICATION->AuthForm("У вас нет права доступа к данному файлу.");

CModule::IncludeModule("nota.exalead");
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Application;
use Nota\Exalead\BiblioCardTable;
use Nota\Exalead\BooksAdd;
use Nota\Exalead\DateConverter;

// переменные из Request
$obRequest = Application::getInstance()->getContext()->getRequest();

$sBy       = htmlspecialcharsBX(strtoupper($obRequest->getQuery("by")));
$sOrder    = htmlspecialcharsBX(strtoupper($obRequest->getQuery("order")));

/** ================================== ПОЛУЧЕНИЕ ДОГОВОРОВ И КНИГ ПРАВООБЛАДАТЕЛЯ ================================== */

$obRightHolder = new \RightHolder();
$arContracts   = $obRightHolder->getContractsActive();
$arEntity      = $obRightHolder->getContractsEntities($arContracts);
$arBooks       = $obRightHolder->getBooksFromExalead($arEntity['BOOKS']);
// TODO: добавить сортировку книг по полям
$userId        = $USER->GetID();

/** ======================================= ПОЛУЧЕНИЕ СТАТИСТИКИ ПО КНИГАМ ========================================= */

$firstView = $lastView = new Date();
$arStat = [];

if (!empty($arBooks)) {

    $arView = $arRead = [];

    foreach ($arBooks as $k => $v)
        $arBooksQuery[] = "'". $k . "'";

    $sQuery = "SELECT BOOK_ID, COUNT(ID) as CNT FROM neb_stat_book_view "
        . " WHERE BOOK_ID IN (".implode(',', $arBooksQuery).") GROUP BY BOOK_ID";
    $rsBook = \Bitrix\Main\Application::getConnection()->query($sQuery);
    while($arItem = $rsBook->Fetch())
        $arView[$arItem['BOOK_ID']] = $arItem['CNT'];

    $sQuery = "SELECT BOOK_ID, COUNT(ID) as CNT FROM neb_stat_book_read "
        . " WHERE BOOK_ID IN (".implode(',', $arBooksQuery).") GROUP BY BOOK_ID";
    $rsBook = \Bitrix\Main\Application::getConnection()->query($sQuery);
    while($arItem = $rsBook->Fetch())
        $arRead[$arItem['BOOK_ID']] = $arItem['CNT'];
}

$arResult = array(
    'FIRST_BOOK_DATE' => $firstView,
    'LAST_BOOK_DATE'  => $lastView,
    'CONTRACTS'       => $arContracts,
    'BOOKS'           => $arBooks,
    'STATISTICS'      => $arStat,
    'VIEW'            => $arView,
    'READ'            => $arRead
);

$this->IncludeComponentTemplate();

$APPLICATION->SetTitle("Статистика");