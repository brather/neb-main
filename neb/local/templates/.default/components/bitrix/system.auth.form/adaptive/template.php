<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<header>
    <div class="container">
        <div class="row">
            <div class="neb-logo-mid">
                <a href="/" title="<?=Loc::getMessage('NATIONAL_ELECTRONIC_LIBRARY')?>">
                    <img src="/local/templates/adaptive/img/logo-mid.png" alt="<?=Loc::getMessage('NATIONAL_ELECTRONIC_LIBRARY')?>" class="">
                </a>
            </div>
        </div>
    </div>
</header>
<main>
    <div class="container">
        <h2 class="col-md-12 login-form__title"><?=Loc::getMessage('AUTHORIZATION')?></h2>
        <div class="login-form">

            <div class="row">
                <div class="social-auth">
                    <h3 class="text-centered"><?=Loc::getMessage('SIGN_WITH_IN')?></h3>
                    <ul class="social-auth__list clearfix">
                        <? if (!empty($arResult['AUTH_SERVICES']['Esia'])): ?>
                            <li class="social-auth__list-kind">
                                <a href="#"
                                   class="social-auth__list-link social-auth__list-link--gu socservice-auth-with-alert"
                                   data-service-popup="/WebServices/Esia.php?act=do_login"
                                   title="<?= Loc::getMessage('AUTH_GOSUSLUGI') ?>">
                                </a>
                            </li>
                        <? endif; ?>
                        <li class="social-auth__list-kind">
                            <a href="#"
                               class="social-auth__list-link social-auth__list-link--rgb socservice-auth-with-alert"
                               data-service-popup="/auth/rgb.php?redirect=1&show_errors=1"
                               title="<?= Loc::getMessage('AUTH_RGB') ?>">
                            </a>
                        </li>
                        <? if (!empty($arResult['AUTH_SERVICES']['VKontakte'])): ?>
                            <li class="social-auth__list-kind">
                                <a href="#"
                                   onclick="<?=$arResult['AUTH_SERVICES']['VKontakte']['ONCLICK']?>"
                                   class="social-auth__list-link social-auth__list-link--vk"
                                   title="<?=Loc::getMessage('AUTH_VKONTAKTE')?>">
                                </a>
                            </li>
                        <? endif; ?>
                        <? if (!empty($arResult['AUTH_SERVICES']['Odnoklassniki'])): ?>
                            <li class="social-auth__list-kind">
                                <a href="#"
                                   onclick="<?=$arResult['AUTH_SERVICES']['Odnoklassniki']['ONCLICK']?>"
                                   class="social-auth__list-link social-auth__list-link--ok"
                                   title="<?=Loc::getMessage('AUTH_ODNOKLASSINIKI')?>">
                                </a>
                            </li>
                        <? endif; ?>
                        <? if (!empty($arResult['AUTH_SERVICES']['Facebook'])): ?>
                            <li class="social-auth__list-kind">
                                <a href="#"
                                   onclick="<?=$arResult['AUTH_SERVICES']['Facebook']['ONCLICK']?>"
                                   class="social-auth__list-link social-auth__list-link--fb"
                                   title="<?=Loc::getMessage('AUTH_FACEBOOK')?>">
                                </a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>

                <div class="error_message">
                    <?
                    if(!empty($arResult['ERROR_MESSAGE']['MESSAGE']) ||
                        (is_string($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE'])))
                    {
                        if(is_array($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']['MESSAGE']))
                        {
                            $arResult['ERROR_MESSAGE'] = $arResult['ERROR_MESSAGE']['MESSAGE'];
                        }
                        ?>
                        <?=$arResult['ERROR_MESSAGE'];?>
                        <?/*
                        <script type="text/javascript">
                            $(function() {
                                $('#login-link').click();
                                $('.b_login_popup .error_message').show();
                            });
                        </script>
                        */?>
                        <?
                    }
                    ?>
                </div>

                <form class="" action="<?=$arResult["AUTH_URL"]?>" method="post">

                    <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ? : '/'?>" />

                    <? foreach ($arResult["POST"] as $key => $value): ?>
                        <input type="hidden" name="<?= $key?>" value="<?= $value?>" />
                    <? endforeach; ?>

                    <input type="hidden" name="AUTH_FORM" value="Y" />
                    <input type="hidden" name="TYPE" value="AUTH" />
                    <input type="hidden" name="USER_REMEMBER" value="Y" />

                    <div class="form-group">
                        <label for="form-group-mail-input" class="login-form__label"><?=Loc::getMessage('AUTH_LOGIN')?></label>
                        <div class="relative">
                            <input type="text" class="form-control login-form__input" id="form-group-mail-input" value="<?=$_REQUEST['USER_LOGIN']?>" name="USER_LOGIN" data-virtual-keyboard>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-group-pass-input" class="login-form__label"><?=Loc::getMessage('AUTH_PASSWORD')?></label>
                        <div class="relative">
                            <input type="password" class="form-control login-form__input" id="form-group-pass-input" name="USER_PASSWORD" data-virtual-keyboard>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6">
                            <button type="submit" class="btn btn-primary btn-lg col-md-12 col-sm-12 col-xs-12 login-form-btn"><?=Loc::getMessage('AUTH_SIGN_IN')?></button>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6">
                            <button type="button" class="btn btn-default btn-lg col-md-12 col-sm-12 col-xs-12 login-form-btn" onclick="document.location.href = '<?=$arParams['LINK_REFERER']?>'"><?=Loc::getMessage('AUTH_CANCEL')?></button>
                        </div>
                        <a href="#" data-toggle="modal" data-target="#forget" class="col-md-12 col-sm-12 col-xs-12 login-form__link" title="Забыли пароль?"><?=Loc::getMessage('AUTH_FORGOT')?></a>
                    </div>

                </form>
                <div class="hr"></div>
                <div class="row login-form-forgot">
                    <a href="/auth/reg.php" class="col-md-12 col-sm-12 col-xs-12 login-form__link" title="Зарегистрироваться"><?=Loc::getMessage('REGISTER')?></a>
                </div>

                <div class="modal fade recovery-modal" tabindex="-1" role="dialog" id="forget">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="/auth/forgot_password.php?forgot_password=yes" method="post" class="b-passrecoveryform b-form">
                                <input type="hidden" name="AUTH_FORM" value="Y">
                                <input type="hidden" name="TYPE" value="SEND_PWD">
                                <input type="hidden" name="backurl" value="/">
                                <input type="hidden" name="send" value="1">
                                <input type="hidden" name="IS_AJAX" value="Y">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 class="modal-title"><?=Loc::getMessage('AUTH_PASS_RECOVERY_TITLE')?></h4>
                            </div>
                            <div class="modal-body" style="text-align: center;">
                                <div class="icloser">
                                    <div class="js-fieldrow">
                                        <div class="b-warning hidden"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND')?></div>
                                        <label for="settings11"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC')?></label>
                                        <div class="field validate">
                                            <input type="text" class="form-control input-lg" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="50" data-minlength="2" name="USER_EMAIL" class="input">
                                        </div>
                                    </div>
                                    <p class="b-passconfirm hidden"><?=Loc::getMessage('AUTH_PASS_RECOVERY_SUCCESS')?></p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary js-recovery-submit btn-lg" value="1" type="submit"><?=Loc::getMessage('AUTH_PASS_RECOVERY_RECOVER')?></button>
                                <button type="button" class="btn btn-default btn-lg" data-dismiss="modal"><?=Loc::getMessage('AUTH_CLOSE')?></button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <script>
                    $(function(){
                        $('#forget').on('shown.bs.modal',function(){
                            $('#settings11').focus();
                        })
                    });
                </script>
            </div>
        </div>
</main>
