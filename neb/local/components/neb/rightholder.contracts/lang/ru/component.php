<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$MESS['ERROR_PERM_DENIED']      = 'У вас нет права доступа к данному файлу!';
$MESS['ERROR_CONTRACTS_IBLOCK'] = 'Инфоблок RightHoldersContracts не найден!';
$MESS['ERROR_HISTORY_IBLOCK']   = 'Инфоблок RightHoldersContractsHistory не найден!';

$MESS['TITLE_MY_CONTRACTS']           = 'Мои договоры';
$MESS['TITLE_RIGHTHOLDERS_CONTRACTS'] = 'Договоры правообладателей';
$MESS['TITLE_INFO_CONTRACT']          = 'Договор #ID# от #DATE_FROM#';