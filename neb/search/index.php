<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('SEARCH_PAGE_TITLE'));
$APPLICATION->SetPageProperty('hide-title', true);
?>
<?$APPLICATION->IncludeComponent(
	"exalead:search.page",
	"",
Array(),
false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>