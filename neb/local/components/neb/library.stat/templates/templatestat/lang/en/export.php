<?php
$MESS['LIBRARY_STAT_NUMBER'] = "Number of publications";
$MESS['LIBRARY_STAT_NUMBER_COPY'] = "Number of publications with an electronic copy";
$MESS['LIBRARY_STAT_NUMBER_DOWNLOADED'] = "Number of downloaded publications";
$MESS['LIBRARY_STAT_NUMBER_READ'] = "Number of read publications";
$MESS['LIBRARY_STAT_DATE'] = "Date";