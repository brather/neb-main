<?php
/**
 * User: agolodkov
 * Date: 20.10.2016
 * Time: 16:16
 */

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serialization;

class Workplace
{
    /**
     * @var integer
     *
     * @Serialization\Groups({"list", "details"})
     */
    public $id;

    /**
     * @var string
     *
     * @Serialization\Groups({"list", "details"})
     */
    public $name;

    /**
     * @var array
     *
     * @Serialization\Groups({"list", "details"})
     */
    public $ips;

    /**
     * @var integer
     *
     * @Serialization\Groups({"list", "details"})
     */
    public $active;

    /**
     * @var integer
     *
     * @Serialization\Groups({"list", "details"})
     */
    public $library_id;

    /**
     * @var integer
     *
     * @Serialization\Groups({"list", "details"})
     */
    public $is_check_ip;

    /**
     * @var integer
     *
     * @Serialization\Groups({"list", "details"})
     */
    public $is_check_fingerprint;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getIps()
    {
        return $this->ips;
    }

    /**
     * @param array $ips
     */
    public function setIps($ips)
    {
        $this->ips = $ips;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive( $active )
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getLibraryId()
    {
        return $this->library_id;
    }

    /**
     * @param int|string $library_id
     */
    public function setLibraryId( $library_id )
    {
        $this->library_id = (int)$library_id;
    }

    /**
     * @return int
     */
    public function getIsCheckFingerprint()
    {
        return $this->is_check_fingerprint;
    }

    /**
     * @param int $is_check_fingerprint
     */
    public function setIsCheckFingerprint( $is_check_fingerprint )
    {
        $this->is_check_fingerprint = (int)$is_check_fingerprint;
    }

    /**
     * @return int
     */
    public function getIsCheckIp()
    {
        return $this->is_check_ip;
    }

    /**
     * @param int $is_check_ip
     */
    public function setIsCheckIp( $is_check_ip )
    {
        $this->is_check_ip = (int)$is_check_ip;
    }

}