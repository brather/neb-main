<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;
Loc::loadMessages(__FILE__);

CJSCore::Init();

$context = Application::getInstance()->getContext();
$request = $context->getRequest();
$getRequest = $context->getRequest();
$arRequest = $request->toArray();
$sefFolder = $request->getRequestedPageDirectory();
$valueInput = '';
$inputNameBookTitle = 'book_title';
if(!empty($arRequest[$inputNameBookTitle])) {
    $valueInput = ' value="' . $arRequest[$inputNameBookTitle] . '"';
}
?>
<div class="container-fluid">
    <form action="<?= $sefFolder ?>/" class="form-horizontal">
        <?if(!empty($getRequest)) {
            foreach($getRequest as $nInput => $vInput) {
                if($nInput != $inputNameBookTitle) {?>
                    <input type="hidden" name="<?= $nInput ?>" value="<?= $vInput ?>">
                <?}
            }
        }?>
        <div class="form-group">
            <label for="title" class="col-xs-2 control-label"><?= Loc::getMessage('COLLECTION_BOOK_SEARCH') ?></label>
            <div class="col-xs-10">
                <div class="input-group">
                    <input type="text"
                           name="<?= $inputNameBookTitle ?>"
                           class="form-control"
                           placeholder="<?= Loc::getMessage('COLLECTION_BOOK_SEARCH_PLACEHOLDER') ?>"
                        <?= $valueInput ?>/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="b-collection_booklist">
    <div class="b-collection-item">
        <a href="/collections/<?=$arResult['SECTION']['ID']?>_<?=$arResult['SECTION']['CODE']?>/">
            <b><?=Loc::getMessage('COLLECTION_DETAIL_LIB_COL_PAGE'); ?></b>
        </a>
        <p class="total">
            <a class="button_blue btadd closein add_books_to_fond"
               data-add-book-to-collection="<?=$arResult['SECTION']['ID']?>"
               href="<?=$this->__folder . '/addBookForm.php?'.bitrix_sessid_get().'&COLLECTION_ID=' . $arResult['SECTION']['ID']?>">
                <?= Loc::getMessage('COLLECTION_DETAIL_LIB_ADD_BOOK') ?>
            </a>
            <span class="right">
                <?= Loc::getMessage('COLLECTION_DETAIL_LIB_TOTAL') ?><?= $arResult['SECTION']['CNT'] ?>
                <?= GetEnding($arResult['SECTION']['CNT'], Loc::getMessage('COLLECTION_DETAIL_LIB_BOOK_5'), Loc::getMessage('COLLECTION_DETAIL_LIB_BOOK_1'), Loc::getMessage('COLLECTION_DETAIL_LIB_BOOK_2')) ?>
                <?= Loc::getMessage('COLLECTION_DETAIL_LIB_IN_COL') ?>
            </span>
        </p>
    </div>
</div>

<!--<a href="--><?//=$arResult['LIBRARY']['DETAIL_PAGE_URL']?><!--" class="b-btlibpage">--><?//= Loc::getMessage('COLLECTION_DETAIL_LIB_LIB_PAGE') ?><!--</a>-->

<a name="nav_start"></a>
<div class="search-result-sort clearfix res-sort-three">
    <span class="sort_wrap">
        <a <?=SortingExalead("document_authorsort")?>><?= Loc::getMessage('COLLECTION_DETAIL_LIB_BY_AUTHOR') ?></a>
        <a <?=SortingExalead("document_titlesort")?>><?= Loc::getMessage('COLLECTION_DETAIL_LIB_BY_NAME') ?></a>
        <a <?=SortingExalead("document_publishyearsort")?>><?= Loc::getMessage('COLLECTION_DETAIL_LIB_BY_DATE') ?></a>
    </span>
</div>
<?
$dataAjaxPath = $this->__folder . '/sortBooksAjax.php';?>
<div class="b-result-doc b-collection_booklist js_sortable"
     id="accordion-books"
     data-page-items-count="<?= $arParams['ITEM_COUNT'] ?>"
     data-ajax-path="<?= $dataAjaxPath ?>">
    <?if($arRequest['PAGEN_1'] > 1) {
        $stIndex = $arParams['ITEM_COUNT'] * ($arRequest['PAGEN_1'] - 1) + 1;
    } else {
        $stIndex = 1;
    }
    $arParamGet = array('pagen', 'by', 'order', 'book_title');
    $strForGet = '';
    $htmlForGet = '';
    foreach($arParamGet as $cParam) {
        if($arRequest[$cParam]) {
            $strForGet .= '&' . $cParam . '=' . $arRequest[$cParam];
            $htmlForGet .= '<input type="hidden" name="' . $cParam . '" value="' . $arRequest[$cParam] . '">';
        }
    }
    if($arRequest['PAGEN_1']) {
        $arParamGet['pagen'] = '&PAGEN_1=' . $arRequest['PAGEN_1'];
    }
    if(!empty($arRequest['by']) && !empty($arRequest['order'])) {
        $arParamGet['sort'] = '&by=' . $arRequest['by'] . '&order=' . $arRequest['order'];
    }?>
    <?foreach($arResult['ITEMS'] as $iBook => $arBook) {
        $hrefOnDeleteBook = '';
        $hrefOnDeleteBook = $APPLICATION->GetCurPage()
            . '?' . bitrix_sessid_get() .'&COLLECTION_ID=' . $arResult['SECTION']['ID']
            . '&BOOK_ID=' . $arBook['BOOK_IBLOCK_ID'] . '&action=removeBook'
             . $strForGet . '&anchor=nav_start';
        $messConfirm = Loc::getMessage('COLLECTION_BOOK_DELETE_1') . '&laquo;' . trim($arBook['title']) . '&raquo;'
            . Loc::getMessage('COLLECTION_BOOK_DELETE_2') . $arResult['SECTION']['NAME']
            . Loc::getMessage('COLLECTION_BOOK_DELETE_3');
        $confirDelBook = "return confirm('" . $messConfirm . ")";
        $hrefOnToggleBook = $APPLICATION->GetCurPage()
            . '?' . bitrix_sessid_get() . '&BOOK_ID=' . $arBook['BOOK_IBLOCK_ID']
            . '&IS_COVER=' . $arBook['BITRIX']['IS_COVER'] . '&action=toggleBookCover'
             . $strForGet . '&anchor=' . $arBook['BITRIX']['ID'];
        ?>
        <div class="b-result-docitem container-fluid" data-id="<?= $arBook['BITRIX']['ID'] ?>">
            <div class="row"><a name="<?= $arBook['BITRIX']['ID'] ?>"></a></div>
            <div class="row" id="<?= $arBook['id'] ?>">
                <div class="col-xs-1">
                    <div class="row"><h2><?= $stIndex ?>.</h2></div>
                </div>
                <div class="col-xs-2 iblock b-result-docphoto">
                    <a class="b_bookpopular_photo iblock" href="<?= $arBook['DETAIL_PAGE_URL'] ?>">
                        <img alt="<?= $arBook['title'] ?>" class="loadingimg" src="<?=$arBook['IMAGE_URL']?>">
                    </a>
                </div>
                <div class="col-xs-9 iblock b-result-docinfo">
                    <h2>
                        <a data-width="955" href="/catalog/<?= $arBook['~id'] ?>/" class=" coverlay popup_opener ajax_opener">
                            <?= $arBook['title'] ?>
                        </a>
                    </h2>
                    <?if ($arBook['authorbook_for_link']) {?>
                        <ul class="b-resultbook-info">
                            <li><span><?= Loc::getMessage('COLLECTION_DETAIL_LIB_AUTHOR') ?></span>
                                <?foreach ($arBook['authorbook_for_link'] as $key => $author){?>
                                    <a href="/search/?f_field[authorbook]=f/authorbook/<?= urlencode(mb_strtolower(strip_tags(trim($author))))
                                    . '&' . DeleteParam(['f_field', 'dop_filter'])?>">
                                        <?= $author ?>
                                    </a>
                                    <?if ($key + 1 < count($arBook['authorbook_for_link'])){?>, <?}?>
                                <?}?>
                            </li>
                        </ul>
                    <?}?>
                    <div class="b-collstat">
                        <?if(!empty($arBook['authorbook'])){?>
                            <div class="b-lib_counter">
                                <span class="b-lib_counter_lb"><?= Loc::getMessage('COLLECTION_DETAIL_LIB_AUTHOR') ?></span>
                                <span class="icouser"><?= $arBook['authorbook'] ?></span>
                            </div>
                        <?}?>
                        <div class="b-lib_counter">
                            <span class="b-lib_counter_lb"><?= Loc::getMessage('COLLECTION_DETAIL_LIB_READ'); ?></span>
                            <span class="icouser">х <?= number_format($arBook['STAT']['READ_CNT'], 0, '', ' ') ?></span>
                        </div>
                        <div class="b-lib_counter">
                            <span class="b-lib_counter_lb"><?= Loc::getMessage('COLLECTION_DETAIL_LIB_VIEW'); ?></span>
                            <span class="icoviews">х <?= number_format($arBook['STAT']['VIEW_CNT'], 0, '', ' ') ?></span>
                        </div>
                        <div class="b-lib_counter">
                            <span class="b-lib_counter_lb"><?= Loc::getMessage('COLLECTION_DETAIL_LIB_FAV') ?></span>
                            <span class="icofav">х <?= number_format($arBook['STAT']['FAV_CNT'], 0, '', ' ') ?></span>
                        </div>
                    </div>
                    <!--<p><?= $arBook['text']?></p>-->
                    <div class="b-result_sorce clearfix">
                        <div class="b-result_sorce_info left">
                            <em><?= Loc::getMessage('COLLECTION_DETAIL_LIB_SOURCE') ?>:</em>
                            <span class="b-sorcelibrary"><?= $arBook['library'] ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row"><br></div>
                    <div class="b-sort_book">
                        <form action="<?= $APPLICATION->GetCurPage() ?>"
                              method="post"
                              data-ajax-path="<?= $dataAjaxPath ?>">
                            <?= $htmlForGet ?>
                            <input type="hidden" name="BOOK_UPDATE_SORT" value="Y">
                            <input type="hidden" name="BOOK_UPDATE_AJAX" value="N">
                            <input type="hidden" name="BOOK_ID" value="<?= $arBook['BOOK_IBLOCK_ID'] ?>">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-10">
                                        <div class="row">
                                            <div class="col-xs-5">
                                                <div class="row">
                                                    <label for="BOOK_VALUE_SORT"><?= Loc::getMessage("COLLECTION_BOOK_SORT_LABEL") ?></label>
                                                </div>
                                            </div>
                                            <div class="col-xs-7">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" value="<?= $arBook['UPDATE_SORT']['SORT'] ?>" name="BOOK_VALUE_SORT">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-info"><?= Loc::getMessage("COLLECTION_BOOK_SORT_CHANGE") ?></button>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">

                            </div>
                        </form>
                        <?
                        $messSuccess = $arBook['UPDATE_SORT']['MESS']['SUCCESS'];
                        $messError = $arBook['UPDATE_SORT']['MESS']['ERROR'];
                        $clsMess = '';
                        $mess = '';
                        if(!empty($messError) || !empty($messSuccess)) {
                            if(!empty($messError)){
                                $clsMess = 'danger';
                                $mess = $messError;
                            } elseif(!empty($messSuccess)) {
                                $clsMess = 'success';
                                $mess = $messSuccess;
                            }?>
                            <div class="block-mess bg-<?= $clsMess ?>">
                                <h4 class="text-<?= $clsMess ?>"><br/>&nbsp;<?= $mess ?>&nbsp;<br/><br/></h4>
                            </div>
                            <div class="row"><hr></div>
                        <?}?>
                    </div>
                </div>
                <div class="col-xs-12 b-collaction">
                    <a href="#" class="item_up"></a>
                    <a href="#" class="item_down"></a>
                    <a data-lang-question="<?= Loc::getMessage('COLLECTION_BOOK_REMOVE_BOOK_QUESTION') ?>"
                       data-lang-full-question="<?= $messConfirm ?>"
                       data-lang-title="<?= Loc::getMessage('COLLECTION_BOOK_REMOVE_BOOK_TITLE') ?>"
                       href="<?= $hrefOnDeleteBook ?>"
                       class="right button_red btn btn-danger js-delete-book"
                       data-width="330">
                        <?= Loc::getMessage('COLLECTION_DETAIL_LIB_REMOVE') ?>
                    </a>
                    <?if($arBook['BITRIX']['IS_COVER']){
                        $clsToggle = ' btn-danger button_red';
                        $messToggle = Loc::getMessage('COLLECTION_DETAIL_LIB_UNSET_COVER');
                    } else {
                        $clsToggle = ' btn-default';
                        $messToggle = Loc::getMessage('COLLECTION_DETAIL_LIB_SET_COVER');
                    }?>
                    <a href="<?= $hrefOnToggleBook ?>" class="btn<?= $clsToggle ?>"><?= $messToggle ?></a>
                    <hr>
                </div>
            </div>
        </div>
        <?$stIndex++;?>
    <?}?>
</div>
<?=$arResult['STR_NAV']?>