<?php
/**
 * User: agolodkov
 * Date: 29.09.2015
 * Time: 17:13
 */
use Bitrix\Main\DB\MysqlResult;

class NebMysqlResult extends MysqlResult
{

    /**
     * FUCK BITRIX strtoupper() !!! parent line:47
     *
     * @return \Bitrix\Main\Entity\ScalarField[]
     */
    public function getFields()
    {
        if ($this->resultFields == null) {
            $this->resultFields = array();
            if (is_resource($this->resource)) {
                $numFields = mysql_num_fields($this->resource);
                if ($numFields > 0 && $this->connection) {
                    $helper = $this->connection->getSqlHelper();
                    for ($i = 0; $i < $numFields; $i++) {
                        $name = mysql_field_name($this->resource, $i);
                        $type = mysql_field_type($this->resource, $i);

                        $this->resultFields[$name]
                            = $helper->getFieldByColumnType($name, $type);
                    }
                }
            }
        }

        return $this->resultFields;
    }
}