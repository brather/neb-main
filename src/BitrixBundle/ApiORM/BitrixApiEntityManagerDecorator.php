<?php
/**
 * User: agolodkov
 * Date: 11.09.2016
 * Time: 18:18
 */

namespace BitrixBundle\ApiORM;


use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\Decorator\EntityManagerDecorator;
use Doctrine\ORM\EntityManagerInterface;

class BitrixApiEntityManagerDecorator extends EntityManagerDecorator
{
    public function getRepository($entityName)
    {
        $metadata = $this->getClassMetadata($entityName);
        return parent::getRepository($entityName);
    }
}