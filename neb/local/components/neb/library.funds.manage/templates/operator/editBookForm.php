<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//var_dump($arResult);
?>
<div class="b-addbook b-addfondbook">

<?if(!empty($arResult['FORM']['Id'])) {?>
    <h2>Редактирование книги</h2>
<?} else {?>
    <h2>Добавление новой книги</h2>
<?}?>
<form method="post" action="" class="b-form b-form_common b-addbookform">
<?=bitrix_sessid_post()?>
<input type="hidden" name="action" value="editBook"/>
<input type="hidden" name="bookId" value="<?=$arResult['FORM']['Id']?>"/>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock w50c">
        <label for="settings03">Автор (сведения об интеллектуальной ответственности)</label>
        <div class="field validate">
            <input type="text"  data-validate="fio" data-required="required" value="<?=$arResult['FORM']['Author']?>" id="settings03" data-maxlength="30" data-minlength="2" name="Author" class="input w50p" >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <label for="settings02">Полное заглавие произведения</label>
        <div class="field validate">
            <input type="text" data-required="required" value="<?=$arResult['FORM']['Name']?>" id="settings02" data-maxlength="30" data-minlength="2" name="Name" class="input" >
            <em class="error required">Поле обязательно для заполнения</em>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <label for="settings22">Сведения, относящиеся к заглавию</label>
        <div class="field validate">
            <input type="text" data-required="required" value="<?=$arResult['FORM']['SubName']?>" id="settings22" data-maxlength="30" data-minlength="2" name="SubName" class="input" >
            <em class="error required">Поле обязательно для заполнения</em>
        </div>
    </div>
</div>
<?/*<div class="fieldrow nowrap">
        <div class="fieldcell iblock w20pc">
            <label for="settings23">Номер подчасти</label>
            <div class="field validate">
                <input type="text" data-validate="number"  data-required="required" value="" id="settings23" data-maxlength="30" data-minlength="2" name="name" class="input b-yeartb" >
                <em class="error required">Поле обязательно для заполнения</em>
                <em class="error validate">Поле заполнено неверно</em>
            </div>
        </div>
    </div>
	<div class="fieldrow nowrap">
        <div class="fieldcell iblock w76">
            <label for="settings21">Название подчасти</label>
            <div class="field validate">
                <input type="text" data-validate="alpha"  data-required="required" value="" id="settings21" data-maxlength="30" data-minlength="2" name="name" class="input" >
                <em class="error required">Поле обязательно для заполнения</em>
                <em class="error validate">Поле заполнено неверно</em>
            </div>
        </div>
    </div>*/?>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock mt0 w20pc">
        <label for="settings25">Год издания</label>
        <div class="field validate">
            <input type="text"  data-validate="number" data-required="required" value="<?=$arResult['FORM']['PublishYear']?>" id="settings25" data-maxlength="30" data-minlength="2" name="PublishYear" class="input b-yeartb" >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <label for="settings20">Издательство или издающая организация (точное название)</label>
        <div class="field validate">
            <input type="text" data-required="required" value="<?=$arResult['FORM']['Publisher']?>" id="settings20" data-maxlength="30" data-minlength="2" name="Publisher" class="input" >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock w50c">
        <label for="settings19">Место издания</label>
        <div class="field validate">
            <input type="text" data-required="required" value="<?=$arResult['FORM']['PublishPlace']?>" id="settings19" data-maxlength="30" data-minlength="2" name="PublishPlace" class="input w50p" >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock mt10 w50c">
        <label for="settings18">Международные стандартные книжные номера (ISBN)</label>
        <div class="field validate">
            <input type="text" data-validate="number"  data-required="required" value="<?=$arResult['FORM']['ISBN']?>" id="settings18" data-maxlength="30" data-minlength="2" name="ISBN" class="input w50p" >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>
<?/*<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <label for="settings17">Язык</label>
        <div class="field validate">
            <select name="language" id="" class="js_select w288"  data-required="true">
                <option value="1">русский</option>
                <option value="2">нерусский</option>
            </select>
        </div>
    </div>
</div>*/?>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock w20pc">
        <label for="settings16">Тираж</label>
        <div class="field validate">
            <input type="text" data-validate="number"  data-required="required" value="<?=$arResult['FORM']['Printing']?>" id="settings16" data-maxlength="30" data-minlength="2" name="Printing" class="input b-yeartb" >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock w50c">
        <label for="settings15">Серия</label>
        <div class="field validate">
            <input type="text" data-required="required" value="<?=$arResult['FORM']['Series']?>" id="settings15" data-maxlength="30" data-minlength="2" name="Series" class="input w50p" >
            <em class="error required">Поле обязательно для заполнения</em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock w20pc">
        <label for="settings14">Номер тома</label>
        <div class="field validate">
            <input type="text" data-validate="number"  data-required="required" value="<?=$arResult['FORM']['VolumeNumber']?>" id="settings14" data-maxlength="30" data-minlength="2" name="VolumeNumber" class="input b-yeartb" >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock w76">
        <label for="settings13">Заглавие тома</label>
        <div class="field validate">
            <input type="text" data-validate="alpha"  data-required="required" value="<?=$arResult['FORM']['VolumeName']?>" id="settings13" data-maxlength="30" data-minlength="2" name="VolumeName" class="input " >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock mt10">
        <label for="settings12">Примечание о содержании</label>
        <div class="field validate">
            <input type="text" data-required="required" value="<?=$arResult['FORM']['ContentRemark']?>" id="settings12" data-maxlength="30" data-minlength="2" name="ContentRemark" class="input" >
            <em class="error required">Поле обязательно для заполнения</em>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <label for="settings11">Примечание общего характера</label>
        <div class="field validate">
            <input type="text" data-required="required" value="<?=$arResult['FORM']['CommonRemark']?>" id="settings11" data-maxlength="30" data-minlength="2" name="CommonRemark" class="input" >
            <em class="error required">Поле обязательно для заполнения</em>
        </div>
    </div>
</div>


<div class="fieldrow nowrap">
    <div class="fieldcell iblock mt10">
        <label for="settings10">Аннотация/реферат/резюме</label>
        <div class="field validate">
            <textarea data-maxlength="800" data-minlength="2" id="settings10" name="Annotation" class="textarea"><?=$arResult['FORM']['Annotation']?></textarea>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock w50">
        <label for="settings07">Код ББК</label>
        <div class="field validate">
            <input type="text" data-validate="number"  data-required="required" value="<?=$arResult['FORM']['BBKText']?>" id="settings07" data-maxlength="30" data-minlength="2" name="BBKText" class="input " >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock w50">
        <label for="settings09">УДК</label>
        <div class="field validate">
            <input type="text" data-validate="number"  data-required="required" value="<?=$arResult['FORM']['UDKText']?>" id="settings09" data-maxlength="30" data-minlength="2" name="UDKText" class="input " >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>

<?/*<div class="fieldrow nowrap">
    <div class="fieldcell iblock w20pc">
        <label for="settings06">Международные стандартные серийные номера (ISSN) </label>
        <div class="field validate">
            <input type="text" data-validate="number" data-required="required" value="<?=$arResult['FORM']['issn']?>" id="settings06" data-maxlength="30" data-minlength="2" name="issn" class="input b-yeartb" >
            <em class="error required">Поле обязательно для заполнения</em>
            <em class="error validate">Поле заполнено неверно</em>
        </div>
    </div>
</div>*/?>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock b-pdfadd">

        <?$APPLICATION->IncludeComponent(
            "notaext:plupload",
            "fond_add_books",
            array(
                "MAX_FILE_SIZE" => "512",
                "FILE_TYPES" => "pdf",
                "DIR" => "tmp_books_pdf",
                "FILES_FIELD_NAME" => "books_pdf",
                "MULTI_SELECTION" => "N",
                "CLEANUP_DIR" => "Y",
                "UPLOAD_AUTO_START" => "Y",
                "RESIZE_IMAGES" => "N",
                "UNIQUE_NAMES" => "Y",
                "ALREADY_UPLOADED_FILE" => $arResult['FORM']['pdfLink']
            ),
            false
        );?>

        <div class="right">
            <p>Дата добавления </p>
            <div class="b-fieldeditable">
                <div class="b-fondtitle"><span class="b-fieldtext"><?=$arResult['FORM']['CreationDateTime']?></span><a class="b-fieldeedit" href="#"></a></div>
                <div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" name="CreationDateTime" class="txt txt_size2"></span><a class="b-editablsubmit iblock" href="#"></a></div>
            </div>
        </div>
    </div>
</div>
<div class="fieldrow nowrap fieldrowaction">
    <div class="fieldcell ">
        <div class="field clearfix">
            <a href="<?=$APPLICATION->GetCurPage()?>" class="formbutton gray right btrefuse">Отказаться</a>
            <button class="formbutton left" value="1" type="submit">Разместить произведение</button>
        </div>
    </div>
</div>
</form>
</div>

<?/*
<div class="b-addbook" id="drop_upload">
	<h2>Добавление новой книги</h2>
	<form action="" class="b-form b-form_common b-addbookform" method="post">
		<input type="hidden" name="add_book" value="Y"/>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings02">Полное название произведения</label>
				<div class="field validate">
					<input type="text" data-required="required" value="" id="settings02" data-maxlength="255" data-minlength="2" name="NAME" class="input" >
					<em class="error required">Поле обязательно для заполнения</em>
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50">
				<label for="settings03">Автор</label>
				<div class="field validate">
					<input type="text"  data-validate="fio" data-required="required" value="" id="settings03" data-maxlength="60" data-minlength="2" name="AUTHOR" class="input w50p" >
					<em class="error required">Поле обязательно для заполнения</em>
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50">
				<label for="settings04">Код ББК</label>
				<div class="field validate">
					<input type="text"  data-validate="number" data-required="required" value="" id="settings04" data-maxlength="30" data-minlength="2" name="BBK" class="input w50p" > <span class="example">Пример ББК: 81.411.2-4 </span>
					<em class="error required">Поле обязательно для заполнения</em>
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w20pc">
				<label for="settings05">Год издания</label>
				<div class="field validate">
					<input type="text"  data-validate="number" data-required="required" value="" id="settings5" data-maxlength="40" data-minlength="4" name="PUBLISH_YEAR" class="input b-yeartb" >
					<em class="error required">Поле обязательно для заполнения</em>
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock b-pdfadd">
				<?$APPLICATION->IncludeComponent(
					"notaext:plupload",
					"add_books",
					array(
						"MAX_FILE_SIZE" => "512",
						"FILE_TYPES" => "pdf",
						"DIR" => "tmp_books_pdf",
						"FILES_FIELD_NAME" => "books_pdf",
						"MULTI_SELECTION" => "N",
						"CLEANUP_DIR" => "Y",
						"UPLOAD_AUTO_START" => "Y",
						"RESIZE_IMAGES" => "N",
						"UNIQUE_NAMES" => "Y"
					),
					false
				);?>

				<div class="right">
					<p>Дата добавления </p>
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><span class="b-fieldtext"><?=date('d.m.Y')?></span>
						<!--<a class="b-fieldeedit" href="#"></a>-->
						</div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" value="<?=date('d.m.Y')?>" class="txt txt_size2" name="DATE_ADD"></span>
						<!--<a class="b-editablsubmit iblock" href="#"></a>-->
						</div>
					</div>
				</div>


			</div>
		</div>
		<div class="fieldrow nowrap fieldrowaction">
			<div class="fieldcell ">
				<div class="field clearfix">
					<a href="#" class="formbutton gray right btrefuse">Отказаться</a>
					<button class="formbutton left" value="1" type="submit">Разместить произведение</button>

				</div>
			</div>
		</div>
	</form>
</div>