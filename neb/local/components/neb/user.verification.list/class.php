<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\Application,
    \Bitrix\Main\UI\PageNavigation;

/**
 * Class UserVerificationListComponent
 * Написан на нативном SQL, т.к. нельзя было сделать прямую фильтрацию по активности пользователя без костылей
 */
class UserVerificationListComponent extends CBitrixComponent
{
    private $arUser = [];

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arRequest = static::getRequest();

        $arParams['READERS'] = [];
        foreach ($arRequest['READERS'] as $k => $v)
            $arParams['READERS'][trim(htmlspecialcharsbx($k))] = trim(htmlspecialcharsbx($v));

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->_getUser();
        $this->_getTicketsList();
        $this->includeComponentTemplate();
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public static function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Получение текущего пользователя
     */
    private function _getUser() {
        global $USER;
        $this->arUser = CUser::GetByID($USER->GetID())->Fetch();
    }

    /**
     * Запрос ЭЧЗ
     *
     * @param $iWorkplace
     * @return array|false
     * @throws \Bitrix\Main\ArgumentException
     */
    private static function _getWorkPlace($iWorkplace) {

        $iIBlock = IBLOCK_ID_WORKPLACES;

        $sQuery = <<<SQL
SELECT
  ID
FROM
  b_iblock_element
WHERE
  IBLOCK_ID = $iIBlock AND ID = $iWorkplace AND ACTIVE = 'Y'
SQL;
        $arWorkplace = Application::getConnection()->query($sQuery)->fetch();

        return $arWorkplace;
    }

    /**
     * Получает наименование таблицы ЭЧБ по коду сущности H-инфоблока
     *
     * @param $sEntity
     * @return mixed
     */
    private function _getTicketsTable($sEntity) {

        $sQuery = <<<SQL
SELECT
  TABLE_NAME
FROM
  b_hlblock_entity
WHERE
  NAME = '$sEntity'
SQL;
        $arTable = Application::getConnection()->query($sQuery)->fetch();

        return $arTable['TABLE_NAME'];
    }

    /**
     * Формирование SQL-фильтра по ФИО пользователя
     * @return string
     */
    private function _getFullNameSQLFilter() {

        $sFullName = '';

        // Шаг 1: Собираем массив из слов (ФИО)
        $arWords = explode(' ', $this->arParams['READERS']['FIO']);
        foreach ($arWords as $key => $word) {
            $arWords[$key] = trim($word);
            if (mb_strlen($arWords[$key]) < 1)
                unset($arWords[$key]);
        }

        // Шаг 2: Получаем все возможные комбинации полей поиска (ФИО)
        $arSearch = [];
        $iCount = count($arWords);
        if ($iCount) {
            $searchFields = ['LAST_NAME', 'NAME', 'SECOND_NAME'];
            foreach ($searchFields as $v1) {
                if ($iCount < 2) {
                    $arSearch[] = [$v1];
                    continue;
                }
                foreach ($searchFields as $v2) {
                    if ($iCount < 3 && $v1 !== $v2) {
                        $arSearch[] = [$v1, $v2];
                        continue;
                    }
                    foreach ($searchFields as $v3)
                        if ($v1 !== $v2 && $v2 !== $v3 && $v1 !== $v3)
                            $arSearch[] = [$v1, $v2, $v3];
                }
            }
        }

        // Шаг 3: Формируем массив поиска по полям ФИО
        foreach ($arSearch as $arFields) {
            $sStr = '';
            foreach ($arFields as $iNumber => $sFields) {
                if (!empty($sStr))
                    $sStr .= ' AND ';
                $sStr .= 'u.'.$sFields . ' LIKE ' . "'%" . $arWords[$iNumber] . "%'";
            }

            if (!empty($sFullName))
                $sFullName .= "\n OR ";
            $sFullName .= "($sStr)";
        }

        return $sFullName;
    }

    /**
     * Формирование полного SQL-фильтра
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getSQLFilter() {

        $sResult = '';

        // получить id ВЧЗ
        $iWorkplace  = intval($this->arUser['UF_WCHZ']);

        // запрос ВЧЗ
        $arWorkplace = static::_getWorkPlace($iWorkplace);

        if ($iWorkplace != $arWorkplace['ID'])
            return $sResult;

        $sResult .= "WHERE \n  t.UF_WCHZ_ID = " . $iWorkplace;

        if ($this->arParams['ID'] > 0) {
            $sResult .= "\n  AND t.ID = " . $this->arParams['ID'];
        }

        if ($this->arParams['READERS']['STATUS'] == 'N') {
            $sResult .= "\n AND  t.UF_DATE < '" . (new DateTime())->format('Y-m-d') . "'";
        } else {
            $sResult .= "\n  AND t.UF_DATE >= '" . (new DateTime())->format('Y-m-d') . "'";
        }

        if (!empty($this->arParams['READERS']['NUMBER'])) {
            $sResult .= "\n AND  t.UF_NUMBER = '" . $this->arParams['READERS']['NUMBER'] . "'";
        }

        $sFullName = $this->_getFullNameSQLFilter();
        if (!empty($sFullName)) {
            $sResult .= "\n  AND (\n$sFullName\n)";
        }

        if ($this->arParams['READERS']['ACTIVE'] != 'N') {
            $sResult .= "\n AND  u.ACTIVE = 'Y'";
        } else {
            $sResult .= "\n  AND  u.ACTIVE = 'N'";
        }

        if (!empty($this->arParams['READERS']['EMAIL'])) {
            $sResult .= "\n  AND u.EMAIL LIKE '".$this->arParams['READERS']['EMAIL'] . "%'";
        }

        if (!empty($this->arParams['READERS']['ECHB'])) {
            $sResult .= "\n  AND f.UF_NUM_ECHB LIKE '".$this->arParams['READERS']['ECHB'] . "%'";
        }

        return $sResult;
    }

    /**
     * Получение списка ЭЧБ
     */
    private function _getTicketsList() {

        $sFilter = $this->_getSQLFilter();
        $sTable  = $this->_getTicketsTable('UsersTickets');

        if (!empty($sFilter) && !empty($sTable)) {

            // формирование объекта постраничной навигации
            $nav = new PageNavigation("tickets");
            $nav->allowAllRecords(true)->setPageSize(20)->initFromUri();
            $iLimit = $nav->getLimit();
            $iOffset = $nav->getOffset();

            // запрос количества для навигационной цепочки
            $sQuery = <<<SQL
SELECT
  COUNT(*) AS rows
FROM
  $sTable t
INNER JOIN b_user as u
  ON t.UF_USER_ID = u.ID
INNER JOIN b_uts_user as f
  ON u.ID = f.VALUE_ID
$sFilter
SQL;
            $arRows = Application::getConnection()->query($sQuery)->fetch();
            $nav->setRecordCount($arRows['rows']);

            // основной запрос данных
            $sQuery = <<<SQL
SELECT
  t.ID,
  t.UF_USER_ID,
  t.UF_WCHZ_ID,
  t.UF_NUMBER,
  t.UF_DATE,
  u.LOGIN,
  u.ACTIVE,
  u.LAST_NAME,
  u.NAME,
  u.SECOND_NAME,
  f.UF_NUM_ECHB
FROM
  $sTable t
INNER JOIN b_user AS u
  ON t.UF_USER_ID = u.ID
LEFT JOIN b_uts_user AS f
  ON u.ID = f.VALUE_ID

$sFilter

ORDER BY
  t.ID DESC
SQL;
            if (strpos($_SERVER['REQUEST_URI'], 'page-all') === false) {
                $sQuery .= " LIMIT $iLimit OFFSET $iOffset";
            }

            $arResult = [];
            $rsRecords = Application::getConnection()->query($sQuery);
            while ($arFields = $rsRecords->fetch()) {
                $arResult['ITEMS'][$arFields['ID']] = $arFields;
            }

            // возвращает первый элемент при заданном ID
            if ($this->arParams['ID'] > 0) {
                reset($arResult['ITEMS']);
                $arResult = current($arResult['ITEMS']);
            }

            $arResult['NAV'] = $nav;
            $this->arResult = array_merge($this->arResult, $arResult);
        }
    }
}