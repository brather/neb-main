<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$countOnPage = 30;// выводим 10 элементов
if($arParams['COUNTONPAGE'])
    $countOnPage=$arParams['COUNTONPAGE'];
$page=1;
if(isset($_GET['PAGEN_1']))
    $page = intval($_GET['PAGEN_1']);
$startarr=$page*$countOnPage-$countOnPage;
//$stoparr=$page*$countOnPage-1;
$arSubRes=$arResult['USERS'];
$arResult['USERS'] = array_slice($arResult['USERS'], $startarr,  $countOnPage);

?>
<div class="b-addreaders">
	<div class="b-readersearch clearfix">
		<form>
            <div class="row">
                <div class="col-xs-4">
                    <input type="text" name="READERS[FIO]" class="form-control"
                           value="<?=$arParams['READERS']['FIO']?>" placeholder="ФИО" />
                </div>
                <div class="col-xs-4">
                    <input type="text" name="READERS[EMAIL]" class="form-control"
                           value="<?=$arParams['READERS']['EMAIL']?>" placeholder="E-mail" />
                </div>
                <div class="col-xs-2">
                    <input type="text" name="READERS[ECHB]" class="form-control"
                           value="<?=$arParams['READERS']['ECHB']?>" placeholder="Номер ЕЭЧБ" />
                </div>
                <div class="col-xs-2">
                    <input type="text" name="READERS[CHB]" class="form-control"
                           value="<?=$arParams['READERS']['CHB']?>" placeholder="Номер ЭЧБ" />
                </div>
                <div class="col-xs-4">
                    <select name="READERS[LIB]" style="width:200px;">
                        <option value="">Все библиотеки</option>
                        <? foreach ($arParams['LIBS'] as $arItem): ?>
                            <option value="<?=$arItem['ID']?>"<? if ($arParams['READERS']['LIB'] == $arItem['ID']):
                                ?> selected="selected"<? endif; ?>><?=$arItem['NAME']?></option>
                        <? endforeach; ?>
                    </select>
                </div>
                <div class="col-xs-4">
                    <select name="READERS[STATUS]" style="width:200px;">
                        <option value="">Все статусы</option>
                        <option value="Y"<? if ($arParams['READERS']['STATUS'] == 'Y'):
                            ?> selected="selected"<? endif; ?>>Верифицирован</option>
                        <option value="N"<? if ($arParams['READERS']['STATUS'] == 'N'):
                            ?> selected="selected"<? endif; ?>>Не верифицирован</option>
                    </select>
                </div>
                <div class="col-xs-2">
                    <input type="submit" value="<?=GetMessage('READERS_LIST_FIND');?>"
                           class="b-search_bth bbox btn btn-primary" />
                </div>
            </div>
		</form>
	</div>

    <div class="lk-table" data-list-layout data-session-id="<?=bitrix_sessid();?>">
        <div class="lk-table__column" style="width:10%"></div>
        <div class="lk-table__column" style="width:40%"></div>
        <div class="lk-table__column" style="width:10%"></div>
        <div class="lk-table__column" style="width:10%"></div>
        <div class="lk-table__column" style="width:10%"></div>
        <div class="lk-table__column" style="width:20%"></div>

        <ul class="lk-table__header">
            <li class="lk-table__header-kind">№</li>
            <li class="lk-table__header-kind"><?=GetMessage('READERS_LIST_FIO');?></li>
            <li class="lk-table__header-kind"><?=GetMessage('READERS_LIST_ELP_NUMBER');?></li>
            <li class="lk-table__header-kind"><?=GetMessage('READERS_LIST_REGISTRATION_DATE');?></li>
            <li class="lk-table__header-kind"><?=GetMessage('READERS_LIST_STATUS');?></li>
            <li class="lk-table__header-kind"><?=GetMessage('READERS_LIST_ACTION');?></li>
        </ul>
        <section class="lk-table__body">
            <? foreach($arResult['USERS'] as $arUser): ?>
                <ul class="lk-table__row" data-reader-block>
                    <li class="lk-table__col"><?=$arUser['ID']?></li>
                    <li class="lk-table__col"><?=$arUser['LAST_NAME']?> <?=$arUser['NAME']?> <?=$arUser['SECOND_NAME']?></li>
                    <li class="lk-table__col"><?=$arUser['UF_NUM_ECHB']?></li>
                    <li class="lk-table__col"><?=FormatDateFromDB($arUser['DATE_REGISTER'], 'SHORT')?></li>
                    <li class="lk-table__col"><?=$arUser['UF_STATUS']?></li>
                    <li class="lk-table__col">
                        <div class="status">
                            <a class="action-edit"
                            href="<?= str_replace('#USER_ID#', $arUser['ID'], $arParams['ACTION_EDIT_URL']); ?>">
                                <?= GetMessage('READERS_LIST_EDIT'); ?>
                            </a>
                            <br />
                            <a href="#" data-unbind data-reader-id="<?=$arUser['ID']?>"
                                data-url="<?= str_replace('#USER_ID#', $arUser['ID'], $arParams['ACTION_UNBIND_URL'] ); ?>">
                                <?= GetMessage('READERS_LIST_DELETE'); ?>
                            </a>
                            <? if (true === $arUser['CAN_LOCK']): ?>
                                <div class="checkbox-action checkbox">
                                    <label>
                                        <input 
                                        class="checkbox" 
                                        type="checkbox" 
                                        data-verify-toggler
                                        data-verified-text="<?= GetMessage('READERS_LIST_VERIFIED'); ?>"
                                        data-verify-text="<?= GetMessage('READERS_LIST_VERIFY'); ?>"
                                        data-url="/rest_api/users/status/"
                                        data-user-id="<?= $arUser['ID'] ?>"
                                        <?php echo false === $arUser['LOCK'] ? 'checked' : '' ?>
                                        />
                                        <span class="lbl">
                                            <?= GetMessage(false === $arUser['LOCK'] ? 'READERS_LIST_VERIFIED' : 'READERS_LIST_VERIFY'); ?>
                                        </span>
                                    </label>
                                </div>
                            <? endif; ?>
                        </div>
                    </li>
                </ul>
            <?endforeach;?>
        </section>
    </div>
</div>
<?

$elements=$arSubRes;
$elementsPage = array_slice($elements, $page * $countOnPage, $countOnPage);
//echo renderElementsPage($elementsPage);
$navResult = new CDBResult();
$navResult->NavPageCount = ceil(count($elements) / $countOnPage);
$navResult->NavPageNomer = $page;
$navResult->NavNum = 1;
$navResult->NavPageSize = $countOnPage;
$navResult->NavRecordCount = count($elements);

$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', '', array(
    'NAV_RESULT' => $navResult,
));
//htmlspecialcharsBack($arResult['STR_NAV']);