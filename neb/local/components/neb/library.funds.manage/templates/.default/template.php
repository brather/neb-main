<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<div class="b-addsearchmode clearfix">
    <!-- <a class="button_blue btadd left closein add_books_to_fond" data-loadtxt="загрузка..." href="<?=$this->__folder . '/addXLSForm.php?'.bitrix_sessid_get()?>"  data-width="785" ><?=GetMessage('LIBRARY_FUNDS_MANAGE_ADD_BOOKS_XLS');?></a> -->
    <div class="b-search_field right">
		<form method="get">
	        <input type="text" name="qbook" class="b-search_fieldtb b-text" id="asearch" value="<?=htmlspecialcharsbx($_REQUEST['qbook'])?>" autocomplete="off">
	        <input type="submit" class="b-search_bth bbox btn btn-primary" value="<?=GetMessage('LIBRARY_FUNDS_MANAGE_SEARCH');?>">
		</form>

    </div><!-- /.b-search_field-->
</div>


<div class="lk-table">
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:50%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <div class="lk-table__column" style="width:20%"></div>

    <ul class="lk-table__header">
        <li class="lk-table__header-kind"><?=GetMessage('LIBRARY_FUNDS_MANAGE_AUTHOR');?></li>
        <li class="lk-table__header-kind"><?=GetMessage('LIBRARY_FUNDS_MANAGE_TITLE');?></li>
        <li class="lk-table__header-kind"><?=GetMessage('LIBRARY_FUNDS_MANAGE_DATE_ADDED');?></li>
        <li class="lk-table__header-kind"></li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['ITEMS'] as $book): ?>
        <ul class="lk-table__row">
            <li class="lk-table__col"><?=$book['authorbook']?></li>
            <li class="lk-table__col"><?=$book['title']?></li>
            <li class="lk-table__col"><?=$DB->FormatDate($book['dateindex'], 'MM/DD/YYYY HH:MI:SS', 'DD.MM.YYYY')?></li>
            <li class="lk-table__col"><div class="rel plusico_wrap minus">
                    <? if (true === $book['isDeleted']) {
                        echo GetMessage(
                            'LIBRARY_FUNDS_DELETED'
                        );
                    } else { ?>
                        <a
                            href="#"
                            class="js-vedeniefondov-deletebook plus_ico lib rest-action fund-delete"
                            id="<?= $book['Id'] ?>"
                            data-method="DELETE"
                            data-delete-msg="<?= GetMessage(
                                'LIBRARY_FUNDS_DELETED'
                            ) ?>"
                            data-prompt-msg="<?= GetMessage(
                                'LIBRARY_FUNDS_DELETE_CONFIRM'
                            ); ?>"
                            data-url="<?= $book['deleteUrl'] ?>"></a>
                        <div class="b-hint rel"><?= GetMessage(
                                'LIBRARY_FUNDS_MANAGE_DO_REQUEST_DEL'
                            ); ?></div>
                    <? } ?>
                </div></li>
        </ul>
        <?endforeach;?>
    </section>

</div>

<?=htmlspecialcharsBack($arResult['STR_NAV'])?>

