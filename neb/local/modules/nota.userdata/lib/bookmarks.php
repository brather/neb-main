<?
namespace Nota\UserData;

use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock\HighloadBlockTable;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Type\DateTime;

use \Nota\Exalead\SearchQuery;
use \Nota\Exalead\SearchClient;
use \Nota\Journal\J;

Loader::includeModule("highloadblock");
Loader::includeModule('nota.exalead');
Loader::includeModule('nota.journal');

class Bookmarks
{
    /**
     * Получение класса-сущности закладок пользователя
     *
     * @param $HIBLOCK
     * @return Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    private static function getEntity_data_class($HIBLOCK = HIBLOCK_BOOKMARKS_DATA_USERS)
    {
        $hlblock = HighloadBlockTable::getById($HIBLOCK)->fetch();
        $entity  = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $entity;
    }

    /**
     * Получение списка закладок
     *
     * @param int $uid - идентификатор пользователя
     * @param string $bookID - идентификатор книги
     * @param string $type - тип выбираемых закладок (page, word)
     * @param int $numPage - номер страницы
     * @param int $wordPage - номер слова
     *
     * @return array|bool
     */
    public static function getBookmarkList($uid = 0, $bookID = '', $type = '', $numPage = 0, $wordPage = '')
    {
        $arResult = [];

        global $USER;
        if (!$uid)
            $uid = $USER->GetID();

        $entity_data_class = self::getEntity_data_class();
        $arFilter = ['UF_UID' => $uid];

        if (!empty($bookID))
            $arFilter['UF_BOOK_ID'] = $bookID;

        if (strtolower($type) == 'page')
            $arFilter['>UF_NUM_PAGE'] = 0;
        elseif (strtolower($type) == 'word')
            $arFilter['!UF_NUM_WORD'] = false;

        if (intval($numPage))
            $arFilter['UF_NUM_PAGE'] = $numPage;
        elseif (!empty($wordPage))
            $arFilter['UF_NUM_WORD'] = $wordPage;

        $rsData = $entity_data_class::getList([
            "select" => ['ID', 'UF_BOOK_ID', 'UF_NUM_PAGE', 'UF_NUM_WORD', 'UF_PREVIEW'],
            "filter" => $arFilter,
        ]);

        if ($rsData->getSelectedRowsCount() <= 0)
            return false;

        if (!empty($bookID) && (intval($numPage) > 0 || !empty($wordPage))) {
            $arData = $rsData->Fetch();
            $arResult = [
                'ID'       => $arData['ID'],
                'BOOK_ID'  => $arData['UF_BOOK_ID'],
                'NUM_PAGE' => intval($arData['UF_NUM_PAGE']),
                'NUM_WORD' => strval($arData['UF_NUM_WORD']),
                'PREVIEW'  => $arData['UF_PREVIEW']
            ];
        } else {
            while ($arData = $rsData->Fetch()) {
                $arResult[$arData['ID']] = [
                    'ID'       => $arData['ID'],
                    'BOOK_ID'  => $arData['UF_BOOK_ID'],
                    'NUM_PAGE' => intval($arData['UF_NUM_PAGE']),
                    'NUM_WORD' => strval($arData['UF_NUM_WORD']),
                    'PREVIEW'  => $arData['UF_PREVIEW']
                ];
            }
        }

        return $arResult;
    }

    /**
     * Получаем список заметок в книге для пользователя
     *
     * @param $bookID - идентификатор книги
     * @param bool $uid - идентификатор пользователя
     *
     * @return array|bool
     */
    public static function getBookmarksListJSON($bookID, $uid = false)
    {
        global $USER;

        if (!$uid)
            $uid = $USER->GetID();

        if (!(bool)$bookID || !(bool)$uid)
            return false;

        // Получаем список закладок
        $userBookmarksArray = self::getBookmarkList($uid, $bookID, '', 0, 0);
        if (empty($userBookmarksArray))
            return false;

        // Приводим ключики к нужному виду
        $jsonArray = array();
        foreach ($userBookmarksArray as $userBookmark) {
            $jsonArray[] = array(
                'id' => $userBookmark['ID'],
                'page' => $userBookmark['NUM_PAGE'],
                'preview' => $userBookmark['PREVIEW'],
            );
        }

        return $jsonArray;
    }

    /**
     * Добавление закладки
     *
     * @param $uid - идентификатор пользователя
     * @param $bookId - идентификатор книги
     * @param $numPage - номер страницы
     * @param $numWord - номер слова
     * @param $arDataFields - массив дополнительных параметов
     * @param $arDataFields['PREVIEW'] - выделенный текст
     *
     * @return int
     */
    public static function add($uid = false, $bookId, $numPage, $numWord, $arDataFields)
    {
        global $USER;
        if (!$uid)
            $uid = $USER->GetID();

        if (!$numPage && empty($numWord))
            die('Number param empty!');

        $bookmarkObject = self::getBookmarkList($uid, $bookId, '', $numPage, $numWord);
        if ($bookmarkObject !== false)
            return $bookmarkObject['ID'];

        $query = new SearchQuery();
        $query->getById($bookId);

        $client = new SearchClient();
        $resExalead = $client->getResult($query);

        $entity_data_class = self::getEntity_data_class();
        $dt = new DateTime();

        $arFields = [
            'UF_UID'         => $uid,
            'UF_BOOK_ID'     => $bookId,
            'UF_NUM_PAGE'    => $numPage ? : 0,
            'UF_NUM_WORD'    => $numWord ? : '',
            'UF_DATE_ADD'    => $dt,
            'UF_PREVIEW'     => $arDataFields['PREVIEW'],
            'UF_BOOK_NAME'   => $resExalead['title'],
            'UF_BOOK_AUTHOR' => $resExalead['authorbook'],
        ];

        J::add('bookmark', 'add', ['BOOK_ID' => $bookId, 'BOOK_LINK' => '/catalog/' . $bookId . '/', 'DATE' => $dt]);

        $saveID = $entity_data_class::add($arFields)->getId();

        return $saveID;
    }

    /**
     * Для api был добавлен метод add(), на тот момент список его параметров был актуален
     * Для добавления из массива добавлен метод addFromArray, который уже использует add()
     *
     * @param $arFields - Массив дополнительных параметов
     * @param $arFields ['BOOK_ID'] - ID книги
     * @param $arFields ['NUM_PAGE'] - Номер страницы
     * @param $arFields ['PREVIEW'] - превью
     * @param $arFields ['UID'] - ID пользователя
     *
     * @return int
     */
    public static function addFromArray($arFields)
    {
        return self::add($arFields['UID'], $arFields['BOOK_ID'], $arFields['NUM_PAGE'], 0, $arFields);
    }

    /**
     * Удаляет закладку, заведомо проверив принадлежность её пользователю
     *
     * @param $id - идентификатор закладки
     * @param $uid - идентификатор пользователя
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function deleteWithTest($id, $uid)
    {
        $entity_data_class = self::getEntity_data_class();
        $toDelete = $entity_data_class::getList([
            "select" => ["ID"],
            "filter" => ['ID' => $id, 'UF_UID' => $uid],
        ])->Fetch();

        if (!$toDelete)
            return false;

        self::delete($toDelete['ID']);

        return true;
    }

    /**
     * Удаление закладки по идентификатору
     *
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public static function delete($id)
    {
        $id = intval($id);
        if ($id <= 0)
            return false;

        $bookmark = self::getById($id);
        if (empty($bookmark))
            return false;

        J::add('bookmark', 'delete', Array('BOOK_ID' => $bookmark['UF_BOOK_ID'], 'BOOK_LINK' => '/catalog/' . $bookmark['UF_BOOK_ID'] . '/', 'DATE' => new DateTime()));

        $entity_data_class = self::getEntity_data_class();
        $entity_data_class::Delete($bookmark['ID']);

        Collections::removeLinkObject($bookmark['ID'], false, 'bookmarks');
    }

    /**
     * Получение закладки по идентификатору
     *
     * @param $ID
     * @return array|bool|false
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getById($ID)
    {
        if (empty($ID))
            return false;

        $entity_data_class = self::getEntity_data_class();
        $arResult = $entity_data_class::getList([
            "select" => ['*'],
            "filter" => ['=ID' => $ID]
        ])->Fetch();

        return $arResult;
    }

    /**
     * Получение закладки по номеру страницы текущего пользователя
     *
     * @param $book_id
     * @param $page
     * @return array|false
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getMark($book_id, $page)
    {
        global $USER;
        $uid = intval($USER->GetID());

        $entity_data_class = self::getEntity_data_class();
        $arResult = $entity_data_class::getList([
            "select" => ['*'],
            "filter" => [
                '=UF_UID'      => $uid,
                '=UF_BOOK_ID'  => $book_id,
                '=UF_NUM_PAGE' => $page
            ],
        ])->Fetch();

        return $arResult;
    }
}