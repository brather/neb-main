<?php
/**
 * User: agolodkov
 * Date: 31.07.2015
 * Time: 10:37
 */

/**
 * Class NebLibraryPartyComponent
 */
class LibraryPartyComponent extends \Neb\Main\ListComponent
{
    /**
     * @return $this
     */
    public function prepareParams()
    {
        $this->_prepareNavigation()
            ->_prepareIblockListParams(IBLOCK_ID_LIBRARY);

        if (mb_strlen($this->arParams['LIBRARY_NAME']) > 0)
            $this->arParams['listParams']['filter']['NAME'] = '%' . $this->arParams['LIBRARY_NAME'] . '%';

        return $this;
    }

    /**
     * @return $this
     */
    public function loadList()
    {
        $this->loadElemets();
        foreach ($this->arResult['items'] as &$item) {
            $item['DETAIL_URL'] = '/profile/statistics/?LIBRARY_ID='
                . $item['ID'];
        }
        unset($item);

        return $this;
    }
}