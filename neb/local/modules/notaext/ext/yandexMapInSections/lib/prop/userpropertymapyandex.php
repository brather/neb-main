<?
namespace Bitrix\NotaExt\Ext\yandexMapInSections\Prop;
/*
Пользовательское свойство "Привязка к ЯндексКарте"
*/

\Bitrix\Main\Loader::IncludeModule("fileman");

class UserPropertyMapYandex extends \CIBlockPropertyMapYandex
{
    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => "user_map_yandex",
            "CLASS_NAME" => __CLASS__,
            "DESCRIPTION" => 'Yandex Maps',
            "BASE_TYPE" => "string",
        );
    }

    function GetDBColumnType($arUserField)
    {
        global $DB;
        switch (strtolower($DB->type)) {
            case "mysql":
                return "text";
            case "oracle":
                return "varchar2(2000 char)";
            case "mssql":
                return "varchar(2000)";
        }
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        $arHtmlControl['MODE'] = 'FORM_FILL';
        $arHtmlControl['VALUE'] = $arHtmlControl['NAME'];

        ob_start();
        parent::GetPropertyFieldHtml($arUserField, $arUserField, $arHtmlControl);
        $strComponentHtml = @ob_get_contents();
        ob_get_clean();
        return $strComponentHtml;
    }
}

//AddEventHandler("main", "OnUserTypeBuildList", array("UserPropertyMapYandex", "GetUserTypeDescription"));