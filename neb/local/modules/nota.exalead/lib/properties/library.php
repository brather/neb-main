<?
namespace Nota\Exalead\Properties;

class Library extends \CUserTypeString
{
    function GetIBlockPropertyDescription()
    {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "exalead_library",
            "DESCRIPTION" => "Привязка к библиотеке в Exalead",
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
        );
    }

    function getEditHTML($name, $value, $is_ajax = false)
    {
        /**
         * Из-за ":" в селекторе jquery падает с ошибкой http://prntscr.com/4x06aj
         * Поэтому в идентификаторе заменяем на символ "_"
         */
        $jsId = str_replace(':', '_', $name);

        \CUtil::InitJSCore(array("jquery"));
        ob_start();
        ?>
        <script type="text/javascript">

            $(function () {
                function getExaleadLibrary(id) {
                    $.ajax({
                        type: "GET",
                        url: "/local/modules/nota.exalead/admin/library_search_ajax.php?id=" + id,
                        success: function (data) {
                            $('.appends').html(data);
                        }
                    });
                }

                getExaleadLibrary('<?=$value?>');

                $('#' + '<?=htmlspecialcharsbx($jsId)?>').change(function () {
                    getExaleadLibrary($(this).val());
                });
            });
        </script>
        <?
        $content = ob_get_contents();
        ob_end_clean();

        $strResult = $content . '<input style="width: 150px;" type="text" name="' . \htmlspecialcharsbx($name) . '" id="' . \htmlspecialcharsbx($jsId) . '" value="' . $value . '">' .
            '<input type="button" value="..." onClick="jsUtils.OpenWindow(\'/local/modules/nota.exalead/admin/library_search.php?code=' . $jsId . '&lang=' . LANGUAGE_ID . '\', 800, 500);">';
        $strResult .= '<div class="appends"></div>';
        return $strResult;
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
    }

    // редактирование свойства в форме и списке (инфоблок)
    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        return $strHTMLControlName['MODE'] == 'FORM_FILL'
            ? self::getEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false)
            : self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

    function getViewHTML()
    {

    }
}
