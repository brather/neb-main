<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;

$APPLICATION->IncludeComponent(
    "neb:feedback",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => $arParams['SEF_FOLDER'] . str_replace('(.*)', '/', $arParams['SEF_URL_TEMPLATES']['feedback']),
        "SEF_URL_TEMPLATES" => array(
            "list"             => "/",
            "appoint"          => "appoint/",
            "responsible"      => "responsible/",
            "reply"            => "reply/",
            "reject"           => "reject/",
            "history"          => "history/",
            "statistics"       => 'statistics/',
            "statistics_total" => 'statistics/total/',
            "statistics_time"  => 'statistics/time/'
        )
    ),
    false
);