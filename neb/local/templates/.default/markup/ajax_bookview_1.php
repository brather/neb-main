<div class="b-bookpopup popup" >
	<a href="#" class="closepopup">Закрыть окно</a>
	<div class="b-bookpopup_in bbox">
	
		<div class="b-onebookinfo iblock">
			<div class="b-bookhover">
				<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
				<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
				<span class="b-autor"><a href="#" class="lite">поэт Ку Кушкин</a></span>
				<div class="b-bookhover_tit black">Муха села на варенье</div>
				<p>Муха села на варенье... вот и всё стихо-творенье. Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>
			</div>	
			<div class="clearfix">
				<button type="submit" value="1" class="formbutton left">Читать</button>	
				<div class="b-result-type right">
					<span class="b-result-type_txt">pdf</span>
					<span class="b-result-type_txt">текст</span>
				</div>	
			</div>	
			
			<div class="rel js_description">
				<ul class="b-resultbook-info">
					<li><span>Количество страниц: </span> 25 </li>
					<li><span>Год публикации:</span> <a href="#">2015</a></li>
					<li><span>Издательство:</span> <a href="#">самиздат</a></li>
				</ul>
						<div class="b-infobox b-descrinfo">
							<div class="b-infoboxitem">
								<span class="tit iblock">Автор: </span>
								<span class="iblock val">Горшков, Евгений Андреевич</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Место издания: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">ISBN:</span>
								<span class="iblock val">2-266-11156-6</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Язык: </span>
								<span class="iblock val">русский</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тираж: </span>
								<span class="iblock val">427</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Номер тома: </span>
								<span class="iblock val">1</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Серия: </span>
								<span class="iblock val">123</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие тома: </span>
								<span class="iblock val">Сумма технологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Неконтролируемое имя: </span>
								<span class="iblock val">Институт психологии Российской академии наук</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие: </span>
								<span class="iblock val">Становление социальной психологии США : автореферат дис. ... кандидата психологических наук : 19.00.01</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Выходные данные: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Физическое описание: </span>
								<span class="iblock val">23 с.</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">Общая психология, психология личности, история психологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">История психологии -- История психологии в Соединенных Штатах Америки (США) -- История психологии в новейшее время -- Психологические направления</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Ключевые слова: </span>
								<span class="iblock val">социальная психология</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Хранение:</span>
								<span class="iblock val">9 11-3/12;</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Электронный адрес:</span>
								<span class="iblock val">Электронный ресурс</span>
							</div>							
						</div><!-- /b-infobox -->
			</div>
			<div class="b-line_social">
				<h5>поделиться с друзьями</h5>
				<a href="#" class="b-login_slnk vklink iblock">вконтакте</a>
				<a href="#" class="b-login_slnk odnlink iblock">одноклассники</a>
				<a href="#" class="b-login_slnk fblink iblock">facebook</a>
			</div><!-- /.b-login_social -->
			<div class="b-lineinfo">
				<h5>найти похожие книги по тематике</h5>
				<ul class="b-lineinfolist">
					<li><a href="#">Общая психология, психология личности, история психологии</a></li>
					<li><a href="#">История психологии</a></li>
					<li><a href="#">История психологии в Соединенных Штатах Америки (США)</a></li>
					<li><a href="#">История психологии в новейшее время</a></li>
					<li><a href="#">Психологические направления</a></li>
				</ul>
			</div>
		</div>
		
				<div class="b-addbook_popuptit clearfix">					
					<h2>Входит в подборки</h2>
				</div>
				<div class="js_threeslide selected_slider">
					<div class="slide">
						<div class="b-favside_img js_flexbackground">
							<img src="./pic/pic_32.png"  data-bgposition="50% 0" class="js_flex_bgimage" alt="">
							<img src="./pic/pic_47.jpg" class="real " alt="">
						</div>
						<h2>Серебряный век: быт, нравы, моды, знаменитости</h2>
						<a href="#">15 книг в подборке </a>
					</div>
					<div class="slide">
						<div class="b-favside_img js_flexbackground">
							<img src="./pic/pic_32.png"  data-bgposition="50% 0" class="js_flex_bgimage" alt="">
							<img src="./pic/pic_47.jpg" class="real " alt="">
						</div>
						<h2>Серебряный век: быт, нравы, моды, знаменитости</h2>
						<a href="#">15 книг в подборке </a>
					</div>
					<div class="slide">
						<div class="b-favside_img js_flexbackground">
							<img src="./pic/pic_32.png"  data-bgposition="50% 0" class="js_flex_bgimage" alt="">
							<img src="./pic/pic_47.jpg" class="real " alt="">
						</div>
						<h2>Серебряный век: быт, нравы, моды, знаменитости</h2>
						<a href="#">15 книг в подборке </a>
					</div>
					<div class="slide">
						<div class="b-favside_img js_flexbackground">
							<img src="./pic/pic_32.png"  data-bgposition="50% 0" class="js_flex_bgimage" alt="">
							<img src="./pic/pic_47.jpg" class="real " alt="">
						</div>
						<h2>Серебряный век: быт, нравы, моды, знаменитости</h2>
						<a href="#">15 книг в подборке </a>
					</div>
					<div class="slide">
						<div class="b-favside_img js_flexbackground">
							<img src="./pic/pic_32.png"  data-bgposition="50% 0" class="js_flex_bgimage" alt="">
							<img src="./pic/pic_47.jpg" class="real " alt="">
						</div>
						<h2>Серебряный век: быт, нравы, моды, знаменитости</h2>
						<a href="#">15 книг в подборке </a>
					</div>
				</div>
				<div class="b-addbook_popuptit clearfix">					
					<h2>похожие издания</h2>

				</div>
				<div class="book_slider">
					<div  class="slide">
						<a href="#"><img alt="" class="loadingimg" src="./pic/pic_35.jpg"></a>
						<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php?id=111111111">Н. Огнев</a></span>
									</div>
					</div>
						<div class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_36.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php?id=222222222">Н. Огнев</a></span>
									</div>
						</div>
						<div class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_37.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php?id=333333333">Н. Огнев</a></span>
									</div>
						</div>
						<div  class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_38.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php?id=4444444">Н. Огнев</a></span>
									</div>
						</div>
						<div class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_35.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php?id=555555555">Н. Огнев</a></span>
									</div>
						</div>
						<div class="slide">
							<a href="#"><img alt="" class="loadingimg" src="./pic/pic_40.jpg"></a>
							<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php?id=777777777">Н. Огнев</a></span>
									</div>
						</div>
				</div>
			
	</div>
	
</div> 	<!-- /.bookpopup -->