/**
 * Из старенького
 */
// maps page

$(function(){
    $(document).find('.ymap.libraries-map').css('height', $(window).height()*.8 );
    $('.b-map').css('height', $('.libs-map-widget').height() + 50 ); 
    if( $('.libs-map-widget').length == 0 ) {
        $('.b-map').css('height', 'auto' ); 
    }
});

var mapSideOffset = 0;
$(function(){
    if ( $('[data-lsr-list]').length > 0 && $(document).width() > 800 ) {
        mapSideOffset = 180;
    }
});

(function ($) { //create closur
    var myPosition = null;
    var findMyPosition = function (callback) {
        if (null !== myPosition) {
            callback(myPosition);
            return this;
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                callback(position);
            }.bind(this));
        }
        return this;
    };

    var LibraryMap = function (options) {
        options = options || {};
        this.points = options.points || [];
        if (!options.map) {
            throw new Error('Not set yandex map!');
        }
        var params = ['map', 'cluster', 'startBounds'];
        for (var i = 0; i < params.length; i++) {
            if (!options.hasOwnProperty('cluster')) {
                continue;
            }
            this[params[i]] = options[params[i]];
        }
        this.options = options;
    };
    LibraryMap.prototype = $.extend(LibraryMap.prototype, {
        map: null,
        cluster: null,
        points: {},
        myPosition: null,
        searchPoint: null,
        startBounds: [],
        options: {},
        showNearest: false,
        findInPoints: function (query) {
            var points = [];
            for (var key in this.points) {
                if (!this.points.hasOwnProperty(key) || !this.points[key].hasOwnProperty('libraryPoint')) {
                    continue;
                }
                var point = this.points[key].libraryPoint;
                if (point.name.toLowerCase().indexOf(query.toLowerCase()) != -1 // find library by name
                    || point.address.toLowerCase().indexOf(query.toLowerCase()) != -1) // find library by address
                {
                    points.push(this.points[key]);
                }
            }
            return points;
        },
        findNearlyPoint: function (point, pointsList) {
            pointsList = pointsList || this.points;
            var coordSystem = this.map.options.get('projection').getCoordSystem();
            var sourcePoint = point.geometry.getCoordinates();
            if (!pointsList[0] || !pointsList[0].hasOwnProperty('geometry')) {
                return null;
            }
            var minDist = coordSystem.getDistance(sourcePoint, pointsList[0].geometry.getCoordinates());
            if (pointsList[0].hasOwnProperty('libraryPoint')) {
                pointsList[0].libraryPoint.distance = minDist;
            }
            var closestPointIdx = 0;

            var nearlyPoint = pointsList[0];
            for (var i = 1, l = pointsList.length; i < l; ++i) {
                if (!pointsList[i].hasOwnProperty('geometry')) {
                    continue;
                }
                var dist = coordSystem.getDistance(sourcePoint, pointsList[i].geometry.getCoordinates());
                if (pointsList[i].hasOwnProperty('libraryPoint')) {
                    pointsList[i].libraryPoint.distance = dist;
                }
                if (minDist > dist) {
                    minDist = dist;
                    closestPointIdx = i;
                    nearlyPoint = pointsList[i];
                }
            }
            nearlyPoint.distance = minDist;
            return nearlyPoint;
        },
        showNearlyPoint: function (points) {
            points = points || this.points;
            this.findMyPosition(function (myPosition) {
                var nearlyPoint = this.findNearlyPoint(myPosition, points);
                this.showPoint(nearlyPoint, {
                    callback: function () {
                        nearlyPoint.events.once('overlaychange', function () {
                            nearlyPoint.balloon.open();
                        });
                    }
                });

            }.bind(this));
            return this;
        },
        /**
         * @todo add refresh method
         * @param points
         * @param flag
         * @returns {LibraryMap}
         */
        visiblePoints: function (points, flag) {
            points = points || this.points;
            function setVisibleFlag(points, flag) {
                for (var i = 0; i < points.length; i++) {
                    points[i].visible = flag;
                }
            }
            if (true === flag) {
                this.cluster.add(points);
                setVisibleFlag(points, true);
            } else {
                this.cluster.remove(points);
                setVisibleFlag(points, false);
            }
            return this;
        },
        findByAddress: function (query) {
            ymaps.geocode(query, {
                boundedBy: this.startBounds,
                results: 1,
                strictBounds: true
            }).then(
                function (res) {
                    this.searchPoint = res.geoObjects.get(0); //map empty test
                    if (this.searchPoint) {
                        this.map.geoObjects.add(this.searchPoint);
                        this.showPoint(this.searchPoint);
                    } else if (this.options.hasOwnProperty('mapContainer')) {
                        this.options.mapContainer.trigger('empty-search-result');
                    }
                }.bind(this),
                function (err) {
                }
            );
            return this;
        },
        clearAddressPoint: function () {
            if (null !== this.searchPoint) {
                this.map.geoObjects.remove(this.searchPoint);
            }
            return this;
        },
        showPoint: function (point, options) {
            this.applyMapBounds(this.buildBounds(point), options);
        },
        buildBounds: function (point) {
            var coordinates = point.geometry.getCoordinates();
            var bounds = [
                coordinates.slice(0),
                coordinates.slice(0)
            ];
            if (point.hasOwnProperty('libraryPoint')) {
                return bounds;
            }

            var shieldValue = 2;
            var nearlyPoint = this.findNearlyPoint(point);
            if (null !== nearlyPoint && nearlyPoint.hasOwnProperty('geometry')) {
                var boundsNearly = nearlyPoint.geometry.getBounds();

                if (shieldValue > Math.abs(bounds[1][0] - boundsNearly[1][0])
                    && shieldValue > Math.abs(bounds[1][1] - boundsNearly[1][1])
                    && shieldValue > Math.abs(bounds[0][0] - boundsNearly[0][0])
                    && shieldValue > Math.abs(bounds[0][1] - boundsNearly[0][1])
                ) {
                    for (var key in boundsNearly) {
                        if (!boundsNearly.hasOwnProperty(key)) {
                            continue;
                        }
                        if ('string' === typeof  boundsNearly[key][0]) {
                            boundsNearly[key][0] = parseFloat(boundsNearly[key][0]);
                        }
                        if ('string' === typeof  boundsNearly[key][1]) {
                            boundsNearly[key][1] = parseFloat(boundsNearly[key][1]);
                        }
                    }
                    if (bounds[0][0] > boundsNearly[0][0]) {
                        bounds[0][0] = boundsNearly[0][0];
                    }
                    if (bounds[0][1] > boundsNearly[0][1]) {
                        bounds[0][1] = boundsNearly[0][1];
                    }

                    if (bounds[1][0] < boundsNearly[1][0]) {
                        bounds[1][0] = boundsNearly[1][0];
                    }
                    if (bounds[1][1] < boundsNearly[1][1]) {
                        bounds[1][1] = boundsNearly[1][1];
                    }
                }
            }
            return bounds;
        },
        applyMapBounds: function (bounds, options) {
            options = options || {};
            var callback = options.callback || function () { };
            this.map.setBounds(bounds, {
                checkZoomRange: true, // проверяем наличие тайлов на данном масштабе.
                callback: function () {
                    var mapBounds = this.map.getBounds();
                    var mapLatValue = mapBounds[1][0] - mapBounds[0][0];
                    var topMarginValue = mapLatValue / 4;
                    var boundIndex = 0;
                    if (bounds[0][0] < bounds[1][0]) {
                        boundIndex = 1;
                    }
                    var topMargin = mapBounds[1][0] - bounds[boundIndex][0];
                    if (topMargin < topMarginValue) {
                        bounds[boundIndex][0] += topMarginValue;
                        this.map.setBounds(bounds, {checkZoomRange: true});
                        callback();
                    } else {
                        callback();
                    }
                }.bind(this)
            });

            // позиционирование центра карты после нажатия крестика в поиске
            if (options['lat'] && options['lng'] && options['zoom']) {
                this.map.setCenter([options['lat'], options['lng']], options['zoom'], {
                    checkZoomRange: true
                });
            }
            return this;
        },
        findMyPosition: function (callback) {
            if (null !== this.myPosition) {
                callback(this.myPosition);
                return this;
            }
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    this.myPosition = new ymaps.GeoObject({
                            geometry: {
                                type: "Point",
                                coordinates: [position.coords.latitude, position.coords.longitude]
                            }
                        },
                        {
                            iconImageHref: '/local/templates/.default/markup/i/geolocation.png',
                            iconImageSize: [24, 24],
                            iconImageOffset: [-12, -12]
                        });
                    this.map.geoObjects.add(this.myPosition);
                    callback(this.myPosition);
                }.bind(this));
            }
            return this;
        },
        getSubsQuery: function (query) {
            query = query.trim().toLowerCase();
            var subsQueries = {};
            subsQueries['ргб'] = 'российская государственная библиотека (ргб)';
            subsQueries['российская государственная библиотека'] = 'российская государственная библиотека (ргб)';
            if (subsQueries.hasOwnProperty(query)) {
                return subsQueries[query];
            }
            return query;
        },
        getInSightItems: function () {
            var inSightPoints = [];
            var mapBounds = this.map.getBounds();
            for (var key in this.points) {
                if (!this.points.hasOwnProperty(key) && !this.points[key].hasOwnProperty('libraryPoint')) {
                    continue;
                }
                if (false === this.points[key].visible || false === this.points[key].options.get('visible')) {
                    continue;
                }
                var coordinates = this.points[key].libraryPoint.coordinates;
                if ('string' === typeof  coordinates[0]) {
                    coordinates[0] = parseFloat(coordinates[0]);
                }
                if ('string' === typeof  coordinates[1]) {
                    coordinates[1] = parseFloat(coordinates[1]);
                }
                if (mapBounds[0][0] > coordinates[0]
                    || mapBounds[0][1] > coordinates[1]
                    || mapBounds[1][0] < coordinates[0]
                    || mapBounds[1][1] < coordinates[1]
                ) {
                    continue;
                }
                inSightPoints.push(this.points[key].libraryPoint);
            }

            return inSightPoints;
        }
    });
    var LibraryList = function (libraries, options) {
        this.libraries = libraries;
        this.options = $.extend(this.options, (options || {}));
        if (this.options.hasOwnProperty('events')) {
            this.events = this.options.events;
        }
    };
    LibraryList.prototype = $.extend(LibraryList.prototype, {
        libraries: {},
        options: {
            sortNearly: false
        },
        events: {},
        render: function () {
            var listSection = $('#library-list'),
                list = listSection.find('[data-result-list]'),
                virtList = $('<ol>').addClass('b-elar_usrs_list').attr('data-lsr-list','');
            list.empty();
            listSection.find('.b-paging_cnt').hide();
            if (true === this.options.sortNearly) {
                this.libraries.sort(function (a, b) {
                    return a.distance - b.distance;
                });
            }
            for (var key in this.libraries) {
                if (!this.libraries.hasOwnProperty(key)) {
                    continue;
                }
                var item = this.libraries[key];
                var template = '<li ' +
                    'class="clearfix"' +
                    'data-map-link ' +
                    'data-lat="' + item.coordinates[0] + '"' +
                    'data-lng="' + item.coordinates[1] + '"' +
                    '>' +
                    '<span class="num">' + (++key) + '.</span>' +
                    '<div class="b-elar_name">' +
                        (
                            item.link
                                ? '<a href="' + item.link + '" class="b-elar_name_txt">' + item.name + '</a>'
                                : '<span class="b-elar_name_txt">' + item.name + '</span>'
                        ) +
                        (
                            true === item.parten
                                ? (
                                '<div class="b-elar_status neb"><div>' +
                                (item.status ? item.status + '<br>' : '') +
                                'Участник проекта НЭБ' +
                                (true === item.wchz ? '<br>ЭЧЗ' : '' ) +
                                '</div></div>'
                            )
                                : ''
                        ) +
                    '</div>' +
                    '<div class="b-elar_address">' +
                        '<div class="b-elar_address_txt">' +
                            '<div class="icon">' +
                            '<div class="b-hint">Адрес</div>' +
                            '</div>' +
                        item.address +
                        ' </div>' +
                    '</div>' +
                '</li>';
                //list.append(template);
                virtList.append(template);
            }
            //list.replaceWith(virtList);            
            for (var i in this.events) {
                if (!this.events.hasOwnProperty(i)) {
                    continue;
                }
                switch (i) {
                    case 'clickShowMapLink':
                        list.find('.b-elar_address_link').click(this.events[i]);
                        break;
                }
            }
            //list.quickPagerLibraries();
            virtList.quickPagerLibraries();
        }
    });
    $.fn.mappage = function (options) {
        var libraryMap;
        var boundChangeTimeOut = null;
        var buildListTimeOut = 500;

        var clickShowMapLink = function (link) {
            var that = $(this),
                link = $(link),
                _lat = that.data('lat') || link.data('lat'),
                _lng = that.data('lng') || link.data('lng');
            if ( !_lat || !_lng ) {
                return false
            }
            
            map.setCenter([_lat, _lng], 16);
            var centerposition = map.getGlobalPixelCenter();
            map.setGlobalPixelCenter([ centerposition[0] + mapSideOffset, centerposition[1] ]);

            libraryMap
                .visiblePoints(null, false)
                .visiblePoints(null, true);
            setTimeout(function () {
                var arr = libraryMap.points;
                for (var m = 0; m < arr.length; m++) {
                    var coords = arr[m].geometry.getCoordinates();
                    if (coords[0] == _lat && coords[1] == _lng) {
                        map.geoObjects.add(arr[m]);
                        arr[m].balloon.open();
                        // arr[m].balloon.open( map.getCenter() );
                        // map.setCenter( arr[m].geometry.getCoordinates() ); //###
                    }
                }
            }.bind(this), 600);
        };

        this.each(function () {
            var autoCompleteItems = [];
            var mapContainer = $(this);
            var defaults = {};
            var startBounds = [];
            var errors = 0,
                msg = '',
                o;
            if (typeof options != 'string') {
                o = $.extend(defaults, options);
            } else {
                o = defaults;
            }
            var cont = $(this);
            var maproot = null;
            if (cont.hasClass('ymap')) {
                maproot = cont;
            } else {
                maproot = cont.find('.ymap');
            }
            var form = cont.find('.b-map_search_filtr'), exmarker = cont.find('.marker_lib.hidden');

            var mapPosition = [(maproot.data('lat')) ? maproot.data('lat') : 55.76, (maproot.data('lng')) ? maproot.data('lng') : 37.64];


            ymaps.ready(function () {
                if (maproot.data('find-location')) {
                    findMyPosition(function (myPosition) {
                        mapPosition = [myPosition.coords.latitude, myPosition.coords.longitude];
                        var myGeocoder = ymaps.geocode(mapPosition);
                        myGeocoder.then(
                            function (res) {
                                var cityName = getPropertyByPath(
                                    'GeocoderMetaData'
                                    + '.AddressDetails'
                                    + '.Country'
                                    + '.AdministrativeArea'
                                    + '.SubAdministrativeArea'
                                    + '.Locality'
                                    + '.LocalityName',
                                    res.geoObjects.get(0).properties.get('metaDataProperty')
                                );
                                $.getJSON('/rest_api/neb-library/?token=' + getUserToken()
                                    + '&UF_LOCALITY=' + cityName
                                    + '&limit=1',
                                    function (json) {
                                        cityId = null;
                                        if (json.hasOwnProperty('data')
                                            && json.data.hasOwnProperty(0)) {
                                            var cityId = json.data[0].UF_CITY;
                                        }
                                        if (cityId) {
                                            location.href = '/library/?city_id=' + cityId;
                                        }
                                    });
                            },
                            function (err) {
                            }
                        );

                    }.bind(this));
                }
                init();
            });

            function init() {                
                map = new ymaps.Map("ymap", {
                    center: mapPosition,
                    zoom: (maproot.data('zoom')) ? maproot.data('zoom') : 10
                });

                var centerposition = map.getGlobalPixelCenter();
                map.setGlobalPixelCenter([ centerposition[0] + mapSideOffset, centerposition[1] ]);

                map.controls.add('zoomControl', {
                    left: 20,
                    top: maproot.height() / 2 - 30
                });
                map.events.add('click', function (e) {
                    if (map.balloon.isOpen()) {
                        map.balloon.close();
                    }
                });
                startBounds = map.getBounds();
                var renderLibraryList = function () {
                    if (null === libraryMap) {
                        return;
                    }
                    (new LibraryList(
                        libraryMap.getInSightItems(),
                        {
                            events: {clickShowMapLink: clickShowMapLink},
                            sortNearly: (null !== libraryMap.myPosition)
                        }
                    ))
                        .render();
                };
                map.events.add('boundschange', function () {
                    if (null !== boundChangeTimeOut) {
                        window.clearTimeout(boundChangeTimeOut);
                    }
                    boundChangeTimeOut = window.setTimeout(
                        renderLibraryList.bind(this),
                        buildListTimeOut
                    );
                }.bind(this));

                var clusterIcon = [{
                        href: '/local/templates/.default/markup/i/clustericon.png',
                        size: [55, 55],
                        offset: [-20, -20]
                    }],
                    clusterNumbers = [10, 40],
                    clusterer = new ymaps.Clusterer({
                        clusterIcons: clusterIcon,
                        clusterNumbers: clusterNumbers
                    });
                clusterer.options.set({gridSize: 128});
                // map.behaviors.enable(['scrollZoom']);
                map.behaviors.disable('scrollZoom');
                // map.behaviors.disable(['drag']);
                map.behaviors.disable('multiTouch');
                var allowZoom = 0;
                function customToggleZoomScroll(flag) {
                    if (flag) {
                        allowZoom = flag
                    }
                    if (!allowZoom) {
                        // console.dir(map);
                        // map.behaviors.enable(['drag']);
                        map.behaviors.enable('scrollZoom');
                        allowZoom = 1;
                        return true
                    } else {
                        // console.log(map);
                        map.behaviors.disable('scrollZoom');
                        // map.behaviors.disable('drag');
                        allowZoom = 0;
                        return false
                    }
                }
                // map.events.add('click', function onMapClick(e) {map.behaviors.enable(['scrollZoom'])});
                map.events.add('click', function onMapClick(e) { customToggleZoomScroll() });
                if (!maproot.data('path')) {
                    alert('Не указан путь к файлу с точками');
                    return false;
                }
                var arr = [];
                var coordsEqual = {};
                var geoObjectsArr = [];

                function resetPlacemarks(libs) {
                    for (var i = 0; i < libs.length; i++) {

                        var id = libs[i].id;
                        libs[i].pmset = new ymaps.GeoObjectCollection({}, {
                            iconImageHref: libs[i].placemarkicon,
                            iconImageSize: libs[i].placemarksize,
                            iconImageOffset: [-libs[i].placemarksize[0] / 2, -libs[i].placemarksize[1] / 2]
                        });

                        for (var k = 0; k < libs[i].regions.length; k++) { // добавить в нее точки
                            var city = libs[i].regions[k].city,
                                coordinates = libs[i].regions[k].coordinates,
                                metro = libs[i].regions[k].metro; // metro array (minimum, one item)
                            for (var m = 0; m < metro.length; m++) {
                                var station = metro[m].station;
                                for (var p = 0; p < metro[m].points.length; p++) {
                                    var point = metro[m].points[p];
                                    var buildContent = function (point) {
                                        return (
                                            '<span class="marker_lib ml1">' +
                                            (
                                                point.link
                                                    ? '<a href="' + point.link + '" class="b-elar_name_txt">' + point.name + '</a><br />'
                                                    : '<span class="b-elar_name_txt">' + point.name + '</span><br />'
                                            )
                                            + (
                                                point.status
                                                    ? '<span class="b-elar_status">' + point.status + '</span><br />'
                                                    : ''
                                            )
                                            + '<span class="b-map_elar_info">'
                                            + '<span class="b-map_elar_infoitem">'
                                            + '<span>'
                                            + exmarker.find('.b-map_elar_info .addr span:first-child').text()
                                            + '</span>'
                                            + ((station) ? ' м. ' + station + ', ' : '') + point.address
                                            + '</span><br />' +
                                            (
                                                point.available
                                                    ? '<span class="b-map_elar_infoitem">'
                                                + '<span>'
                                                + exmarker.find('.b-map_elar_info .graf  span:first-child').text()
                                                + '</span>'
                                                + point.available
                                                + '</span>' : ''
                                            )
                                            + '</span>'
                                            + '<span class="b-mapcard_act clearfix">'
                                            + (
                                                point.parten
                                                    ? '<span class="right neb b-mapcard_status">'
                                                + exmarker.find('.b-mapcard_act .b-mapcard_status').text()
                                                + '</span>'
                                                    : ''
                                            ) +
                                            (
                                                point.link
                                                    ? '<a href="' + point.link + '" class="button_mode">'
                                                + exmarker.find('.b-mapcard_act .button_mode').text()
                                                + '</a>'
                                                    : ''
                                            )
                                            + '</span>'
                                            + '</span>'
                                        );
                                    };

                                    /*################################*/
                                    var BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
                                        buildContent(point)
                                        // '<span class="marker_lib ml2">' +
                                        //     (
                                        //         point.link
                                        //             ? '<a href="' + point.link + '" class="b-elar_name_txt">' + point.name + '</a>'
                                        //             : '<span class="b-elar_name_txt">' + point.name + '</span>'
                                        //     )
                                        //     + (
                                        //         point.status
                                        //             ? '<span class="b-elar_status">' + point.status + '</span><br />'
                                        //             : ''
                                        //     )
                                        //     + '<span class="b-map_elar_info">'
                                        //     + '<span class="b-map_elar_infoitem">'
                                        //     + '<span>'
                                        //     + exmarker.find('.b-map_elar_info .addr span:first-child').text()
                                        //     + '</span>'
                                        //     + ((station) ? ' м. ' + station + ', ' : '') + point.address
                                        //     + '</span><br />' +
                                        //     (
                                        //         point.available
                                        //             ? '<span class="b-map_elar_infoitem">'
                                        //         + '<span>'
                                        //         + exmarker.find('.b-map_elar_info .graf  span:first-child').text()
                                        //         + '</span>'
                                        //         + point.available
                                        //         + '</span>' : ''
                                        //     )
                                        //     + '</span>'
                                        //     + '<span class="b-mapcard_act clearfix">'
                                        //     + (
                                        //         point.parten
                                        //             ? '<span class="right neb b-mapcard_status">'
                                        //                 + exmarker.find('.b-mapcard_act .b-mapcard_status').text()
                                        //                 + '</span>'
                                        //             : ''
                                        //     ) +
                                        //     (
                                        //         point.link
                                        //             ? '<a href="' + point.link + '" class="button_mode">'
                                        //         + exmarker.find('.b-mapcard_act .button_mode').text()
                                        //         + '</a>'
                                        //             : ''
                                        //     )
                                        //     + '</span>'
                                        // + '</span>'
                                        ,
                                        {

                                        build: function () {
                                            BalloonContentLayout.superclass.build.call(this);
                                        },
                                        clear: function () {
                                            BalloonContentLayout.superclass.clear.call(this);
                                        },
                                    });

                                    // placemark = new ymaps.Placemark(map.getCenter(), {
                                    //     balloonContent: '<span class="marker_lib">' + html + '</span>'
                                    // }, {
                                    //     balloonCloseButton: true,
                                    //     balloonOffset: (cont.width() < 410) ? [175, 180] : [207, 180],
                                    //     balloonMaxWidth: (cont.width() < 410) ? 265 : 410,
                                    //     balloonMinWidth: (cont.width() < 410) ? 265 : 410,
                                    //     balloonAutoPan: true,
                                    //     hideIconOnBalloonOpen: false,
                                    //     iconLayout: 'default#image',
                                    //     iconImageHref: '/local/templates/.default/markup/i/pin.png', //i/pin.png
                                    //     iconImageSize: [34, 48],
                                    //     iconImageOffset: [-17, -24]
                                    // });

                                    placemark = new ymaps.Placemark([point.coordinates[0], point.coordinates[1]], {
                                        name: 'Считаем'
                                    }, {
                                        balloonContentLayout: BalloonContentLayout,
                                        balloonCloseButton: true,
                                        balloonMinHeight: 70,
                                        balloonAutoPan: true,
                                        balloonOffset: [0, -5],
                                        // balloonOffset: [0, 120],
                                        hideIconOnBalloonOpen: false,
                                        iconLayout: 'default#image',
                                        iconImageHref: '/local/templates/.default/markup/i/pin.png', //i/pin.png
                                        iconImageSize: [34, 48],
                                        iconImageOffset: [-17, -24]
                                    });
                                    /*#########################################*/
                                    var coordinatesKey = point.coordinates[0] + '-' + point.coordinates[1];
                                    if (coordsEqual.hasOwnProperty(coordinatesKey)) {
                                        var balloonContent = coordsEqual[coordinatesKey].properties.get('balloonContent');
                                        coordsEqual[coordinatesKey].properties.set('balloonContent',
                                            coordsEqual[coordinatesKey].properties.get('balloonContent')
                                            + buildContent(point)
                                        );
                                    } else {
                                        coordsEqual[coordinatesKey] = placemark;
                                        placemark.libraryPoint = point;
                                        libs[i].pmset.add(placemark);
                                        clusterer.add(placemark);
                                        arr.push(placemark);
                                        // geoObjectsArr[p] = placemark;
                                        // console.log(p);
                                    }
                                }
                            }
                        }
                    }
                    // console.dir(geoObjectsArr);
                    // clusterer.add(geoObjectsArr);
                    map.geoObjects.add(clusterer);
                }

                maproot.on('libsloaded', function (e, libs) {
                    resetPlacemarks(libs);
                    for (var index = 0; index < arr.length; index++) {
                        if (!arr[index].hasOwnProperty('libraryPoint')) {
                            continue;
                        }
                        autoCompleteItems.push(arr[index].libraryPoint.name);
                        autoCompleteItems.push(arr[index].libraryPoint.address);
                    }
                    if (1 === maproot.data('show-nearly')) {
                        libraryMap.showNearlyPoint();
                        libraryMap.showNearest = true;
                    }
                    if (field.val()) {
                        findLibrary();
                    }
                    renderLibraryList();
                });

                $.getJSON(maproot.data('path'), function (json) {
                    $.each(json, function (key, val) {
                        maproot.trigger('libsloaded', [val]);
                    });
                });

                /* Модальное окно, которое открывается при отсутствии результатов поиска, у кого спросить зачем оно? */
                // mapContainer.on('empty-search-result', function () {
                //     form.val('');
                //     var emptyPopup = $('<div class="b_popup">' +
                //         '<p>Ничего не найдено</p>' +
                //         '<button class="find-nearly">Ближайшая библиотека</button>' +
                //         '<br><button class="continue">Продолжить</button>' +
                //         '</div>');
                //     emptyPopup.find('.continue').click(function () {
                //         emptyPopup.dialog('close');
                //     });
                //     emptyPopup.find('.find-nearly').click(function () {
                //         field.val('');
                //         form.submit();
                //         emptyPopup.dialog('close');
                //     });
                //     emptyPopup.dialog({
                //         closeOnEscape: true,
                //         modal: true,
                //         draggable: false,
                //         resizable: false,
                //         width: 300,
                //         height: 150,
                //         open: function () {
                //             $(this).parents('.ui-dialog').parent().find('.ui-widget-overlay').addClass('dark');
                //         }
                //     });
                // });

                /**
                 *  Сообщение об отсутствии результатов поиска
                 *  */
                 mapContainer.on('empty-search-result', function () {
                     $('#library-list [data-options-holder]').text('По данному запросу ничего не найдено');
                 });

                libraryMap = new LibraryMap({
                    points: arr,
                    map: map,
                    mapContainer: mapContainer,
                    cluster: clusterer,
                    startBounds: startBounds
                });

                var findLibrary = function () {
                    libraryMap.clearAddressPoint();
                    var foundPoints = libraryMap.findInPoints(libraryMap.getSubsQuery(field.val()));
                    if (foundPoints.length > 0) {

                        // переход на положение "по умолчанию" при нажатии крестика в поиске
                        var options = {};
                        if (field.val().length < 3)
                            options = { lat:  maproot.data('lat'), lng:  maproot.data('lng'), zoom: maproot.data('zoom') };

                        libraryMap
                            //.showNearlyPoint(foundPoints)
                            .visiblePoints(null, false) // снятие всех точек
                            .visiblePoints(foundPoints, true) // нанесение новых точек
                            .applyMapBounds(libraryMap.cluster.getBounds(), options) ; // позиционирование
                            //libraryMap.map.setBounds(libraryMap.cluster.getBounds(), {checkZoomRange: true});
                    } else {
                        libraryMap
                            .visiblePoints(null, false)
                            //.visiblePoints(null, true)
                            .findByAddress(field.val());
                    }

                };

                form.on('submit', function (e) {
                    e.preventDefault();
                    if (true === libraryMap.showNearest || field.val().length < 3) {
                        return;
                    }
                    findLibrary();
                    window.setTimeout(
                        renderLibraryList(),
                        buildListTimeOut
                    );
                });

                $(document).on("click", ".setLinkPoint", function (e) {
                    e.preventDefault();
                    if ($(this).hasClass("setPointAndReload")) {
                        //при клике на "Да, верно" ставим город в куку и обновляем страницу
                        setCityAndReload($(this).data("city-id"));
                    } else {
                        //при клике на название города ставим его на карте
                        setLinkPoint($(this).data("lat"), $(this).data("lng"));
                    }

                });
                function setLinkPoint(link_lat, link_lng) {
                    ymaps
                        .geocode(link_lat + ", " + link_lng, {
                            boundedBy: startBounds,
                            results: 1,
                            strictBounds: true
                        })
                        .then(function (res) {
                            var firstGeoObject = res.geoObjects.get(0),
                                coords = firstGeoObject.geometry.getCoordinates(),
                                bounds = firstGeoObject.properties.get('boundedBy');
                            //                              bounds = firstGeoObject.properties.get('boundedBy');
                            map.setBounds(bounds, {
                                //checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                            });
                            map.setZoom(10);

                            var centerposition = map.getGlobalPixelCenter();
                            map.setGlobalPixelCenter([ centerposition[0] + mapSideOffset, centerposition[1] ]);
                        });
                }

                var availabletext = [],
                    boundedBy = [],
                    field = $(".searchonmap"),
                    deleteIcon = $(".button_d");

                field.autocomplete({
                    source: autoCompleteItems,
                    minLength: 3,
                    appendTo: field.parents('.b-map_search_filtr'),
                    select: function () {
                        /** @todo remove timeout */
                        setTimeout(function() {
                            field.parent().find('button.button_b').click();
                        }, 200);
                    }

                }).on('keypress', function(e) {
                    if (!e) e = window.event;
                    if (e.keyCode == '13'){
                        field.autocomplete('close');
                    }
                })
                .on('keyup', function(e) {
                    if ($(this).val().length > 0) {
                        $('.button_d').show();
                    } else {
                        $('.button_d').hide();
                    }
                });;

                deleteIcon.on('click', function(e) {
                    field.val(''); // очищаем строку поиска
                    $(this).hide(); // скрываем крестик
                    findLibrary(); // ставим метки и позиционируем карту
                    window.setTimeout(
                        renderLibraryList(),
                        buildListTimeOut
                    );
                });

                // $(document).on('click','.b-elar_address_link',function(e){
                //     e.preventDefault();
                //     clickShowMapLink(this);
                // });
                $(document).on('click','[data-map-link]',function(e){
                    e.preventDefault();
                    if ( $(e.target).prop('tagName') == 'A' && $(e.target).hasClass('b-lear_name_txt') ) {
                        e.stopPropagation();
                    }
                    clickShowMapLink(this);
                });
            }
        });
    };
    //end of closure    

})(jQuery);

/**
 * @link https://code.google.com/a/eclipselabs.org/p/muathuoc/source/browse/trunk/Scripts/quickpager.jquery.js?r=196
 */
(function ($) {
    $.fn.quickPagerLibraries = function (options) {

        var defaults = {
            pageSize: 10,
            currentPage: 1,
            showCount: 5,
            holder: null, // контейнер для сообщения "ничего не найдено"
            pagerLocation: 'after'
        };

        options = $.extend(defaults, options);
        return this.each(function () {
            var selector = $(this),
                pageCounter = 1,
                maxPage = 1,
                widgetContainer = $('[data-libs-sort-result]'),
                displayList = widgetContainer.find('[data-lsr-list]');

            options.holder = widgetContainer.find('[data-options-holder]');
            selector.children().each(function (i) {
                if (i < pageCounter * options.pageSize && i >= (pageCounter - 1) * options.pageSize) {
                    $(this).addClass('simplePagerPage' + pageCounter);
                }
                else {
                    $(this).addClass('simplePagerPage' + (pageCounter + 1));
                    pageCounter++;
                }
            });
            maxPage = pageCounter;

            selector.children().hide();
            selector.children('.simplePagerPage' + options.currentPage).show();
            displayList.replaceWith(selector);

            if (pageCounter <= 1) {
                options.holder.empty();
                return;
            }
            if (pageCounter > options.showCount) {
                pageCounter = options.showCount;
            }

            var buildPageNavigation = function () {
                options.holder.empty();
                var startPage = 1;
                var pagerSide = Math.floor(pageCounter / 2);
                
                if (pagerSide < options.currentPage && pageCounter != 3) {
                    startPage = options.currentPage - pagerSide;
                }
                
                var showMaxPage = pageCounter + startPage - 1;
                if (showMaxPage > maxPage) {
                    showMaxPage = maxPage;
                }

                if ( (showMaxPage - options.currentPage) < 1  && options.currentPage > 4) {
                    startPage = startPage - 2;
                }
                else if ( (showMaxPage - options.currentPage) < 2  && options.currentPage > 3 ) {
                    startPage = startPage - 1;
                }

                var pageNav = $('<ul/>').addClass('pagination'),
                    prevAttr = options.currentPage;
                if (options.currentPage > 1) {
                    prevAttr = options.currentPage - 1;
                }

                pageNav.append('<li><a rel="'
                    + prevAttr + '" href="#" class="b-paging_prev iblock simplePageNav'
                    + prevAttr + '"><span class="fa fa-angle-left" aria-hidden="true"></span></a></li>');
                for (var i = startPage; i <= showMaxPage; i++) {
                    if (i == options.currentPage) {
                        pageNav.append('<li><a rel="' + i + '" class="b-paging_num pagination__active iblock simplePageNav' + i + '" onlcik="return false;">' + i + '</a></li>');
                    }
                    else {
                        pageNav.append('<li><a rel="' + i + '" class="b-paging_num iblock simplePageNav' + i + '" href="#">' + i + '</a></li>');
                    }
                }

                pageNav.append('<li><a href="#" rel="' + (options.currentPage + 1) + '" class="b-paging_next iblock simplePageNav' + (options.currentPage + 1) + '"><span class="fa fa-angle-right" aria-hidden="true"></span></a></li>');

                $(options.holder).append(pageNav);
                
                if (1 >= options.currentPage) {
                    $(options.holder).find('a.b-paging_prev').addClass('current pagination__active');
                    $(options.holder).find('a.b-paging_prev').click(function (e) {
                        e.preventDefault();
                    });
                }
                if (maxPage <= options.currentPage) {
                    $(options.holder).find('a.b-paging_next').addClass('current pagination__active');
                    $(options.holder).find('a.b-paging_next').click(function (e) {
                        e.preventDefault();
                    });
                }

                options.holder.find('a').click(function () {
                    //console.log('page nav item click');
                    if ($(this).hasClass('current')) {
                        return false;
                    }
                    var clickedLink = $(this).attr('rel');
                    options.currentPage = parseInt(clickedLink);
                    selector.children().hide();
                    selector.find('.simplePagerPage' + clickedLink).show();
                    buildPageNavigation();

                    $('[data-lsr-list]').scrollTop(0);
                    return false;
                });
            };
            buildPageNavigation();
        });
    }
})(jQuery);

// maps page
(function ($) { //create closure
    $.fn.mapones = function (options) {
        this.each(function () {
            var defaults = {};
            var errors = 0,
                msg = '',
                o;
            if (typeof options != 'string') {
                o = $.extend(defaults, options);
            } else {
                o = defaults;
            }
            var cont = $(this);
            var map, placemark, bal = cont.parent().find('.marker_lib.hidden');
            var html = bal.html(), maproot = cont;
            ymaps.ready(init); // on MAP READY
            function init() {
                map = new ymaps.Map("fmap", {
                    center: [(cont.data('lat')) ? cont.data('lat') : 55.76, (cont.data('lng')) ? cont.data('lng') : 37.64],
                    zoom: (cont.data('zoom')) ? cont.data('zoom') : 10,
                    type: 'yandex#publicMap'
                });                

                map.controls.add('zoomControl', {
                    left: 20,
                    top: maproot.height() / 2 + 10
                });
                
                map.events.add('click', function (e) {
                    if (map.balloon.isOpen()) {
                        map.balloon.close();
                    }

                });
                map.behaviors.enable(['scrollZoom']);
                placemark = new ymaps.Placemark(map.getCenter(), {
                    balloonContent: '<span class="marker_lib">' + html + '</span>'
                }, {
                    balloonCloseButton: true,
                    balloonOffset: (cont.width() < 410) ? [175, 180] : [207, 180],
                    balloonMaxWidth: (cont.width() < 410) ? 265 : 410,
                    balloonMinWidth: (cont.width() < 410) ? 265 : 410,
                    balloonAutoPan: true,
                    hideIconOnBalloonOpen: false,
                    iconLayout: 'default#image',
                    iconImageHref: '/local/templates/.default/markup/i/pin.png', //i/pin.png
                    iconImageSize: [34, 48],
                    iconImageOffset: [-17, -24]
                });
                map.geoObjects.add(placemark);
                placemark.balloon.open();

            }
        });
    };
    //end of closure
})(jQuery);

$(function(){
    $('.mappage').mappage();
    $('.fmap').mapones();

    $('body').on('click.changefontsize', function(event){
        if (event.namespace == 'changefontsize') {
            //console.log('try');
            $('#ymap ymaps.ymaps-events-pane').first().trigger('click');
        }
    });

    /* мобильный расхлоп сайдбара карты */
    function toggleSidebar(flag) {
        if (flag !== 'undefined') {
            $('.lmw-bar').toggleClass('expanded',flag);
        } else {
            $('.lmw-bar').toggleClass('expanded');
        }
        
    }
    $(document)
    .on('click','.libs-list-title',toggleSidebar)
    .on('click','[data-map-link]',function(){ toggleSidebar(false); });

});