<?php
$MESS["NAME_ITEM"] = "Name of element: ";
$MESS["INPUT_NAME_ITEM"] = "input name of element...";
$MESS["ADD_ITEM"] = "Add";
$MESS["DETAIL_TEXT_ITEM"] = "Detail text for element: ";
$MESS["CHOISE_SECTION_FOR_ITEM"] = "Choise section for element: ";
$MESS["SORT_ITEM_DEFAULT"] = "500";
$MESS["SORT_ITEM"] = "Sort index: ";
$MESS['REQUIRED_FIELD'] = "* - Required field.";