<?namespace Intervolga\RemoteUserClient\EventHandlers;
use Intervolga\RemoteUserClient\File;
use Intervolga\RemoteUserClient\User;

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/log.txt");

/**
 * Class Main
 *
 * @package Intervolga\RemoteUserClient\EventHandlers
 */
class Main
{
	/**
	 * User records from @see onBeforeUserAdd() are saved.
	 *
	 * @var array
	 */
	protected static $arAddUsers = array();

	/**
	 * User records from @see onBeforeUserUpdate() are saved.
	 *
	 * @var array
	 */
	protected static $arUpdateUsers = array();

	/**
	 * Adds file fields to user array.
	 *
	 * File fields can be found in POST.
	 *
	 * @param array $arUser user array
	 *
	 * @return mixed
	 */
	protected static function getFileFieldsFromPost($arUser)
	{
		// Get files from POST on register pages
		$arNamesForFileFields = array(
			"PERSONAL_PHOTO" => "PERSONAL_PHOTO",
			"scan1" => "UF_SCAN_PASSPORT1",
			"scan2" => "UF_SCAN_PASSPORT2",
		);
		foreach ($arNamesForFileFields as $sVarName => $sFieldName)
		{
			if ($_POST[$sVarName] && parse_url($_POST[$sVarName]) && File::exists($_POST[$sVarName]))
			{
				if (substr_count($_POST[$sVarName], "/upload/main/") == 0 && substr_count($_POST[$sVarName], "/upload/uf/") == 0)
				{
					$arUser[$sFieldName . "_url"] = $_POST[$sVarName];
				}
			}
		}

		// Get files from POST on profile pages
		$arProfilePages = array(
			"/components/notaext/plupload/profile/set_photo.php" => "PERSONAL_PHOTO",
			"/components/notaext/plupload/scan_passport1/set_photo.php" => "UF_SCAN_PASSPORT1",
			"/components/notaext/plupload/scan_passport2/set_photo.php" => "UF_SCAN_PASSPORT2",
		);
		foreach ($arProfilePages as $sProfilePage => $sFieldName)
		{
			if (substr_count($_SERVER["REQUEST_URI"], $sProfilePage) > 0 && $_POST["file"] && parse_url($_POST["file"]))
			{
				if (parse_url($_POST["file"]) && File::exists($_POST["file"]))
				{
					if (substr_count($_POST["file"], "/upload/main/") == 0 && substr_count($_POST["file"], "/upload/uf/") == 0)
					{
						$arUser[$sFieldName . "_url"] = $_POST["file"];
					}
				}
			}
		}

		if (count($arUser["PERSONAL_PHOTO"]) == 1 && $arUser["PERSONAL_PHOTO"]["MODULE_ID"])
		{
			unset($arUser["PERSONAL_PHOTO"]);
		}
		if (count($arUser["UF_SCAN_PASSPORT1"]) == 1 && $arUser["UF_SCAN_PASSPORT1"]["MODULE_ID"])
		{
			unset($arUser["UF_SCAN_PASSPORT1"]);
		}
		if (count($arUser["UF_SCAN_PASSPORT2"]) == 1 && $arUser["UF_SCAN_PASSPORT2"]["MODULE_ID"])
		{
			unset($arUser["UF_SCAN_PASSPORT2"]);
		}

		return $arUser;
	}

	/**
	 * Checks fields and UF's and removes protected fields from add user array.
	 *
	 * @param array $arUser
	 *
	 * @return bool
	 */
	public static function onBeforeUserAdd(&$arUser)
	{
		if (!defined("ADMIN_SECTION"))
		{
			global $APPLICATION;

			static::$arAddUsers[] = $arUser;
			// If can't be added on remote site
			if ($arErrors = User::checkAdd($arUser))
			{
				// Prevent adding locally
				$APPLICATION->ThrowException(implode("<br>", $arErrors));
				return FALSE;
			}
			else
			{
				// Remove protected fields and UF's
				$arUser = User::getFieldsForThisSite($arUser);
				$arUser["UF_IV_UPLOAD_DATE"] = date("d.m.Y H:i:s");
				$arUser["UF_IV_UPLOAD_PREVENT"] = 1;
			}
		}
		return TRUE;
	}

	/**
	 * Adds user on remote site, if added locally.
	 *
	 * @param array $arUser
	 */
	public static function onAfterUserAdd($arUser)
	{
		if (!defined("ADMIN_SECTION"))
		{
			$arFullUser = array_pop(static::$arAddUsers);
			if ($arUser["ID"])
			{
				$arFullUser["ID"] = $arUser["ID"];
				User::add($arFullUser);
			}
		}
	}

	/**
	 * Checks fields and UF's and removes protected fields from update user array.
	 *
	 * @param $arUser
	 *
	 * @return bool
	 */
	public static function onBeforeUserUpdate(&$arUser)
	{
		if (!defined("ADMIN_SECTION"))
		{
			global $APPLICATION;

			static::$arUpdateUsers[] = $arUser;
			// If can't be added on remote site
			if ($arErrors = User::checkUpdate($arUser))
			{
				// Prevent local update
				$APPLICATION->ThrowException(implode("<br>", $arErrors));
				return FALSE;
			}
			else
			{
				// Remove protected fields and UF's
				$arUser = User::getFieldsForThisSite($arUser);
			}
		}
		return TRUE;
	}

	/**
	 * Updates user on remote site.
	 *
	 * @param array $arUser user array
	 */
	public static function onAfterUserUpdate($arUser)
	{
		if (!defined("ADMIN_SECTION"))
		{
			$arFullUser = array_pop(static::$arUpdateUsers);
			if ($arUser["ID"] && $arUser["RESULT"])
			{
				// Grab remote server file urls from post data and append them to update array
				$arFullUser = static::getFileFieldsFromPost($arFullUser);

				if (count($arFullUser) == 1 && $arFullUser["ID"])
				{
					// Skip ID updating
					return;
				}
				User::update($arFullUser["ID"], $arFullUser);
			}
		}
	}

	public static function onBeforeUserDelete()
	{
		// Nothing yet
	}

	/**
	 * Deletes user from remote server.
	 *
	 * @param int $iUser
	 */
	public static function onUserDelete($iUser)
	{
		User::delete($iUser);
	}
}