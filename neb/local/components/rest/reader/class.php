<?php

CBitrixComponent::includeComponentClass("rest:rest.server");

use \Bitrix\Main\UserTable,
    \Neb\Main\Helper\IpHelper;

/**
 * Class RestReader
 */
class RestReader extends RestServer
{
	protected $token, $action, $q, $ip, $fingerprint;

	public function processRequest($token = null, $action = null, $q = null, $ip = null, $fingerprint = null)
	{
        $this->token       = !empty($token)       ? trim($token)       : false;
        $this->action      = !empty($action)      ? trim($action)      : false;
        $this->q           = !empty($q)           ? trim($q)           : false;
        $this->ip          = !empty($ip)          ? trim($ip)          : false;
        $this->fingerprint = !empty($fingerprint) ? trim($fingerprint) : false;
        
        switch ($this->action) {
            case 'isanonymousaccess':
                $this->checkAnonymousAccess();
                break;
            case 'getUserStatus':
                $this->checkUserToken();
                $this->GETUserStatus();
                break;
            case 'getIPStatus':
                $this->checkUserToken();
                $this->GETIPStatus();
                break;
            default:
                return $this->restError(403, 'Parameter "action" incorrect!');
        }
	}

    /**
     * Проверка возможности анонимного доступа. Возвращает либо false либо токен анонимного доступа.
     */
    protected function checkAnonymousAccess() {

        if (nebUser::checkAuthAccessMode()) {
            $this->arResult['result'] = false;
        } else {
            $arUser = nebUser::getAnonymousUser();
            $this->arResult['result'] = true;
            $this->arResult['token']  = $arUser['UF_TOKEN'];
        }
    }

    /**
     * Проверка токена пользователя
     *
     * @return bool
     */
    protected function checkUserToken() {

        if (!$this->token || strlen($this->token) <= 0)
            return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));

        $arUser = UserTable::getList([
            'filter' => ['ACTIVE' => 'Y', 'UF_TOKEN' => $this->token],
            'select' => ['ID']
        ])->fetch();

        if (empty($arUser))
            return $this->restError(404, GetMessage('USER_NOT_FOUND'));
    }

    /**
     * Получение статуса пользователя
     *
     * @return int
     */
    protected function GETUserStatus(){

        $arResult = [];

        $arUser = nebUser::UserFields($this->q);

        //22 == свойсто статус пользователя
        $obUserFieldEnum = new CUserFieldEnum();
        $rsStatus = $obUserFieldEnum->GetList(['SORT' => 'ASC'], ['USER_FIELD_ID' => '22']);
        while ($arStatus = $rsStatus->GetNext())
            $arResult[$arStatus['ID']] = $arStatus['XML_ID'];

        $this->arResult = $arResult[$arUser['UF_STATUS']];

        return 200;
    }

    /**
     * Получение статуса доступа с IP адреса (#20473)
     * 
     * @return int
     */
    protected function GETIPStatus()
    {
        if (empty($this->ip)) {
            $this->ip = $_SERVER['REMOTE_ADDR'];
        }

        // проверка на локальные IP
        if (
            IpHelper::validateIpInRange($this->ip, '172.16.0.0-172.31.255.255')
            || IpHelper::validateIpInRange($this->ip, '192.168.0.0-192.168.255.255')
        ) {
            $this->arResult = false;
            return 200;
        }

        // 1. Получение ЭЧЗ по IP адресу пользщователя

        $arWorkPlace = NebWorkplaces::getWorkplacesByIp($this->ip);
        reset($arWorkPlace); $arWorkPlace = current($arWorkPlace);

        if (empty($this->fingerprint)) {
            $this->arResult = !empty($arWorkPlace);
            return 200;
        }

        // 2. Получение fingerprint

        $obIBlockElement = new CIBlockElement();
        $arMachine = $obIBlockElement->GetList(
            [],
            [
                'IBLOCK_CODE'          => IBLOCK_CODE_TRUSTED_MACHINES,
                'PROPERTY_FINGERPRINT' => $this->fingerprint,
                'ACTIVE'               => 'Y'
            ],
            false,
            false,
            [
                'ID',
                'IBLOCK_ID',
                'ACTIVE',
                'PROPERTY_WORKPLACE_ID',
                'PROPERTY_WORKPLACE_ID.PROPERTY_LIBRARY',
            ]
        )->Fetch();

        // 3. Определение идентификатора
        $iWPLib = intval($arWorkPlace['PROPERTY_LIBRARY_VALUE']);
        $iFPLib = intval($arMachine['PROPERTY_WORKPLACE_ID_PROPERTY_LIBRARY_VALUE']);
        $iLibId = $iWPLib ? : $iFPLib;

        if (($iWPLib > 0 && $iFPLib > 0 && $iWPLib != $iFPLib) || ($iWPLib == 0 && $iFPLib == 0))
        {
            $this->arResult = false;
            return 200;
        }

        // 4. Получение библиотеки
        if ($iLibId) {
            $arLibrary = $obIBlockElement->GetList(
                [],
                [
                    'IBLOCK_ID'        => IBLOCK_ID_WORKPLACES,
                    'PROPERTY_LIBRARY' => $iLibId,
                    'ACTIVE'           => 'Y'
                ],
                false,
                false,
                [
                    'ID',
                    'IBLOCK_ID',
                    'PROPERTY_LIBRARY',
                    'PROPERTY_IS_CHECK_IP',
                    'PROPERTY_IS_CHECK_FINGERPRINT',
                ]
            )->Fetch();

            if (!empty($arLibrary)) {
                $arLibrary = [
                    'ID'                   => $arLibrary['PROPERTY_LIBRARY_VALUE'],
                    'IS_CHECK_IP'          => $arLibrary['PROPERTY_IS_CHECK_IP_VALUE'] == 'Y',
                    'IS_CHECK_FINGERPRINT' => $arLibrary['PROPERTY_IS_CHECK_FINGERPRINT_VALUE'] == 'Y'
                ];
            }
        }

        if (empty($arLibrary) || !($arLibrary['IS_CHECK_IP'] || $arLibrary['IS_CHECK_FINGERPRINT'])) {
            $this->arResult = false;
            return 200;
        }
        if ($arLibrary['IS_CHECK_IP'] && empty($arWorkPlace)) {
            $this->arResult = false;
            return 200;
        }
        if ($arLibrary['IS_CHECK_FINGERPRINT'] && empty($arMachine)) {
            $this->arResult = false;
            return 200;
        }

        $this->arResult = true;
        return 200;
    }
}