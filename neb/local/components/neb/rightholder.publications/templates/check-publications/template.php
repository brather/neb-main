<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>

<div class="fieldrow nowrap" id="publications-list">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label>Выберите публикации:</label>

        <?
        if (!empty($arResult['BOOKS'])) {
            //print_r($arResult['BOOKS']);
            foreach ($arResult['BOOKS'] as $key => $arBook) {
                ?>
                <div class="field validate ">
                    <div class="b-radio">
                        <input type="checkbox"
                               value="<?= $arBook['Id'] ?>" class="checkbox"
                               id="book<?= $key ?>"
                            <?= (
                            isset($arResult['CHECKED_BOOKS'][$arBook['Id']])
                                ? 'checked' : ''
                            ) ?>>
                        <label style="width: 200px;" for="book<?= $key ?>">
                            <?= $arBook['Name'] ?>
                        </label>
                    </div>
                </div>
            <?
            }
        }
        ?>

    </div>
</div>
<script>
    $(function () {
        var publicationsList = $('#publications-list');
        publicationsList.find('input.checkbox').change(function () {
            var booksValues = [];
            publicationsList.find('input.checkbox').each(function () {
                if ($(this).prop('checked')) {
                    booksValues.push($(this).val());
                }
            });
            booksValues = booksValues.length > 0 ? ('[' + booksValues.join(',') + ']') : '';
            $('#field_PUBLICATIONS_LIST').find('input').val(booksValues);
        });
    });
</script>
<style>
    .fieldcell span.radio {
        float: left;
    }
</style>