<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$APPLICATION->SetTitle("Статистика"); ?>
<? //$APPLICATION->AddHeadScript('http://code.highcharts.com/highcharts.js');?>
<div class="iblock b-lib_fulldescr">
<div class='b-breadcrumb' style='margin-bottom:15px;'>Количество посещений страницы библиотеки в НЭБ - <?=intval($arResult['ITEM']['SHOW_COUNTER'])?></div>

<!--<div class="b-breadcrumb bot">
	<span>Разделы</span>
    <ul class="b-breadcrumblist">
        <li><a href="?form=read&from=<? //=$arParams['DATE_FROM']?>&to=<? //=$arParams['DATE_TO']?>&PAGEN_1=1">Количество прочитанных книг библиотеки</a></li>
        <li><a href="?form=view&from=<? //=$arParams['DATE_FROM']?>&to=<? //=$arParams['DATE_TO']?>&PAGEN_1=1">Количество просмотренных книг библиотеки</a></li>
                    
    </ul>
</div>-->

<div class='b-add_digital js_digital'>
<form action="<?=$APPLICATION->GetCurPage()?>?<?if ($arParams['FORMAT'] == 'view'):?>form=view<?endif;?>" method="GET" name="form1">
с <?echo CalendarDate("from", $arParams['DATE_FROM'], "form1", "10", "class=\"b-text\"")?>
по <?echo CalendarDate("to", $arParams['DATE_TO'], "form1", "10", "class=\"b-text\"")?>
<input type='hidden' value='<?=$arParams['FORMAT']?>' name='form'>
<input type="submit" class="b-search_bth bbox" value="Обновить" style='float:none;'>
</form>
<br>
<div><?if ($arParams['FORMAT'] != 'view'):?>Количество прочитанных книг библиотеки<?else:?>Количество просмотренных книг библиотеки<?endif;?></div>
<br>

<table class='b-usertable tsize'>
	<tbody>
		<tr><th class='autor_cell'><a>Дата</a></th><th class='namedig_cell'><a>Количество просмотров</a></th></tr>
		<?foreach ($arResult['ITEMS'] as $date=>$value):?>
			<tr>
            	<td class='pl15'><?=$date?></td>
                <td class='pl15'><?if ($arParams['FORMAT'] != 'view'):?><?=intval($value['RD'])?><?else:?><?=intval($value['VW'])?><?endif;?></td>
            </tr>
		<?endforeach;?>
	</tbody>
</table>
<div id="container" style="width:100%; height:400px;"></div>
<script>
$(function () {
    $('#container').highcharts({
        title: {
            text: 'Monthly Average Temperature',
            x: -20 /*center*/
        },
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Tokyo',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
            name: 'New York',
            data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
        }, {
            name: 'Berlin',
            data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
        }, {
            name: 'London',
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        }]
    });
});
</script>
<?=htmlspecialcharsBack($arResult['STR_NAV'])?>
</div>
<br><br>
</div>

