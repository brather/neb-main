<?
namespace Bitrix\NotaExt\Ext\plupload\Prop;

class PropertyPlupload
{
    public function GetUserTypeDescription()
    {
        return array(
            'PROPERTY_TYPE' => 'F',
            'USER_TYPE' => 'PLUPLOAD',
            'DESCRIPTION' => '[plupload] Загрузка больших файлов',
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetPropertyFieldHtmlMulty' => array(__CLASS__, 'GetPropertyFieldHtmlMulty'),
            'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
            'ConvertToDB' => array(__CLASS__, 'ConvertToDB'),
            'PrepareSettings' => array(__CLASS__, 'PrepareSettings')
        );
    }

    public function GetPropertyFieldHtmlMulty($arProperty, $arValues, $strHTMLControlName)
    {
        return static::GetElementForm($arProperty, $arValues, $strHTMLControlName, 'Y');
    }

    public function GetPropertyFieldHtml($arProperty, $arValues, $strHTMLControlName)
    {
        return static::GetElementForm($arProperty, $arValues, $strHTMLControlName);
    }

    protected function GetElementForm($arProperty, $arValues, $strHTMLControlName, $multi = 'N')
    {
        global $APPLICATION;
        #echo "<pre>";	print_r($arProperty); echo "</pre>";
        #echo "<pre>";	print_r($strHTMLControlName); echo "</pre>";
        #echo "<pre>";	print_r($arValues); echo "</pre>";

        $arParamsComponent = array(
            'FILE_TYPES' => $arProperty['FILE_TYPE'],
            'MAX_FILE_SIZE' => $arProperty['USER_TYPE_SETTINGS']['MAX_FILE_SIZE'],
            'FILES_FIELD_NAME' => $strHTMLControlName['VALUE'],
            'MULTI_SELECTION' => $multi,
            'CLEANUP_DIR' => true,
            'FIELD_DESCRIPTION_LEN' => $arProperty['USER_TYPE_SETTINGS']['DESCRIPTION_LEN'],
            'IMG_WIDTH' => $arProperty['USER_TYPE_SETTINGS']['WIDTH'],
            'IMG_HEIGHT' => $arProperty['USER_TYPE_SETTINGS']['HEIGHT'],
            'IMG_QUALITY' => $arProperty['USER_TYPE_SETTINGS']['QUALITY'],
            'IMG_CROP' => $arProperty['USER_TYPE_SETTINGS']['CROP'],
            'OLD_FILES' => $arValues
        );

        if ($arProperty['WITH_DESCRIPTION'] == 'Y') {
            if ($arProperty['MULTIPLE'] == 'N')
                $arParamsComponent['FILES_FIELD_DESCRIPTION'] = $strHTMLControlName['DESCRIPTION'];
            else
                $arParamsComponent['FILES_FIELD_DESCRIPTION'] = $strHTMLControlName['VALUE'];
        }

        $APPLICATION->IncludeComponent('notaext:plupload', 'admin', $arParamsComponent);
    }

    public function ConvertToDB($arProperty, $arValue)
    {
        if (!empty($arValue['VALUE']) and !empty($arValue['VALUE']['tmp_name'])) {
            $dirToSave = !empty($arProperty['USER_TYPE_SETTINGS']['DIR']) ? $arProperty['USER_TYPE_SETTINGS']['DIR'] : 'plupload_files';
            $fid = \CFile::SaveFile($arValue['VALUE'], $dirToSave);
            if ($fid > 0) {
                @unlink($arValue['VALUE']['tmp_name']);
                $arValue['VALUE'] = $fid;
            }
        }
        return $arValue;
    }

    function PrepareSettings($arUserField)
    {
        return array(
            'DIR' => $arUserField['USER_TYPE_SETTINGS']['DIR'],
            'MAX_FILE_SIZE' => $arUserField['USER_TYPE_SETTINGS']['MAX_FILE_SIZE'],
            'DESCRIPTION_LEN' => $arUserField['USER_TYPE_SETTINGS']['DESCRIPTION_LEN'],
            'WIDTH' => $arUserField['USER_TYPE_SETTINGS']['WIDTH'],
            'HEIGHT' => $arUserField['USER_TYPE_SETTINGS']['HEIGHT'],
            'QUALITY' => $arUserField['USER_TYPE_SETTINGS']['QUALITY'],
            'CROP' => $arUserField['USER_TYPE_SETTINGS']['CROP'],
        );
    }

    function CheckSettings($arSettings)
    {
        $arSettings['MAX_FILE_SIZE'] = intval($arSettings['MAX_FILE_SIZE']);
        $arSettings['DESCRIPTION_LEN'] = intval($arSettings['DESCRIPTION_LEN']);
        $arSettings['WIDTH'] = intval($arSettings['WIDTH']);
        $arSettings['HEIGHT'] = intval($arSettings['HEIGHT']);
        $arSettings['QUALITY'] = intval($arSettings['QUALITY']);

        $arSettings['DIR'] = htmlspecialcharsEx($arSettings['DIR']);

        if ($arSettings['MAX_FILE_SIZE'] <= 0)
            $arSettings['MAX_FILE_SIZE'] = 500;

        if ($arSettings['DESCRIPTION_LEN'] <= 0)
            $arSettings['DESCRIPTION_LEN'] = 30;

        if ($arSettings['QUALITY'] <= 0)
            $arSettings['QUALITY'] = 90;

        return $arSettings;
    }

    public function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array("COL_COUNT"),
        );

        $arSettings = self::CheckSettings($arProperty['USER_TYPE_SETTINGS']);

        ob_start();
        ?>

        <tr>
            <td>Директория:</td>
            <td><input type="text" size="50" name="<?= $strHTMLControlName["NAME"] ?>[DIR]"
                       value="<?= $arSettings['DIR'] ?>"></td>
        </tr>
        <tr>
            <td>Максимальный размер файлов (в Мб):</td>
            <td><input type="text" size="7" name="<?= $strHTMLControlName["NAME"] ?>[MAX_FILE_SIZE]"
                       value="<?= $arSettings["MAX_FILE_SIZE"] ?>"></td>
        </tr>
        <tr>
            <td>Размер поля для описания:</td>
            <td><input type="text" size="7" name="<?= $strHTMLControlName["NAME"] ?>[DESCRIPTION_LEN]"
                       value="<?= $arSettings["DESCRIPTION_LEN"] ?>"></td>
        </tr>
        <tr>
            <td>Изменять размер изображений:</td>
            <td><input type="text" size="7" placeholder="Ширина" name="<?= $strHTMLControlName["NAME"] ?>[WIDTH]"
                       value="<?= $arSettings["WIDTH"] ?>"> X <input type="text" size="7" placeholder="Высота"
                                                                     name="<?= $strHTMLControlName["NAME"] ?>[HEIGHT]"
                                                                     value="<?= $arSettings["HEIGHT"] ?>"></td>
        </tr>
        <tr>
            <td>Качество изображения:</td>
            <td><input type="text" size="7" name="<?= $strHTMLControlName["NAME"] ?>[QUALITY]"
                       value="<?= $arSettings["QUALITY"] ?>">%
            </td>
        </tr>

        <tr>
            <td>Обрезать изображение?:</td>
            <td><?= InputType('checkbox', $strHTMLControlName["NAME"] . '[CROP]', 'N', htmlspecialcharsbx($arSettings["CROP"])) ?></td>
        </tr>

        <?
        $returnHTML = ob_get_contents();
        ob_end_clean();

        return $returnHTML;
    }
}
