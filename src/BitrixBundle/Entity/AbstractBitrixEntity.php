<?php
/**
 * User: agolodkov
 * Date: 11.09.2016
 * Time: 19:15
 */

namespace BitrixBundle\Entity;


abstract class AbstractBitrixEntity
{
    /**
     * @return string
     */
    abstract function getRepository();
}