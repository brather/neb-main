<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<!-- Приложения НЭБ -->
<section class="applications">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 applications__desc">
                <p class="col-md-12 col-sm-12 col-xs-12"><?=getMessage('APPLICATIONS_DESC')?></p>
            </div>
            <div class="col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-12 applications__list">
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <a href="https://play.google.com/store/apps/details?id=ru.elar.neb.viewer" class="applications__links applications__links--android" title="<?=getMessage('APPLICATIONS_ANDROID')?>">
                            Android
                        </a>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <a href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=944682831&mt=8" class="applications__links applications__links--ios" title="<?=getMessage('APPLICATIONS_IOS')?>">
                            iPhone
                        </a>
                    </div>
                    <?/*div class="col-md-2 col-sm-2 col-xs-2">
                               <a href="http://www.windowsphone.com/ru-ru/store/app/%D0%BD%D1%8D%D0%B1-%D1%80%D1%84-%D0%BD%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BD%D0%BD%D0%B0%D1%8F-%D0%B1%D0%B8%D0%B1%D0%BB%D0%B8%D0%BE%D1%82%D0%B5%D0%BA%D0%B0/d58559b7-2471-4a8f-a523-11cc51f6b23f" class="applications__links applications__links--winphone" title="<?=getMessage('APPLICATIONS_WINPHONE')?>">
                                   Windows Phone
                               </a>
                         </div*/?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Приложения конец -->