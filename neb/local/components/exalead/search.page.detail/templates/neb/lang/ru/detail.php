<?

$MESS['LIB_SEARCH_PAGE_DETAIL_AUTHOR'] = 'Автор';
$MESS['LIB_SEARCH_PAGE_DETAIL_TITLE'] = 'Заглавие';
$MESS['LIB_SEARCH_PAGE_DETAIL_SUBTITLE'] = 'Подзаглавие';
$MESS['LIB_SEARCH_PAGE_DETAIL_RESPONSIBILITY'] = 'Ответственность';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE'] = 'Место издания';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH'] = 'Издательство';
$MESS['LIB_SEARCH_PAGE_DETAIL_YEAR'] = 'Год издания';
$MESS['LIB_SEARCH_PAGE_DETAIL_PAGES'] = 'Количество страниц';
$MESS['LIB_SEARCH_PAGE_DETAIL_SERIE'] = 'Серия';
$MESS['LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT'] = 'Примечание содержания';
$MESS['LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE'] = 'Общее примечание';
$MESS['LIB_SEARCH_PAGE_DETAIL_ANNOTATION'] = 'Аннотация';
$MESS['LIB_SEARCH_PAGE_DETAIL_BBK'] = 'ББК';
$MESS['LIB_SEARCH_PAGE_DETAIL_UDC'] = 'УДК';
$MESS['LIB_SEARCH_PAGE_DETAIL_UDCFULL'] = 'УДК рубрики';
$MESS['LIB_SEARCH_PAGE_DETAIL_EDITION'] = 'Редакция';
$MESS['LIB_SEARCH_PAGE_DETAIL_LIB'] = 'Библиотека';
$MESS['LIB_SEARCH_PAGE_DETAIL_LANGUAGE'] = 'Язык';
$MESS['LIB_SEARCH_PAGE_DETAIL_INCLUDED_IN'] = 'Входит в состав';

$MESS['LIB_SEARCH_PAGE_DETAIL_FUND_NUMBER'] = 'Номер фонда';
$MESS['LIB_SEARCH_PAGE_DETAIL_FUND_NAME'] = 'Название фонда';
$MESS['LIB_SEARCH_PAGE_DETAIL_INVENTORY_NUMBER'] = 'Номер описи';
$MESS['LIB_SEARCH_PAGE_DETAIL_INVENTORY_NAME'] = 'Заголовок описи';
$MESS['LIB_SEARCH_PAGE_DETAIL_UNIT_NUMBER'] = 'Номер дела';
$MESS['LIB_SEARCH_PAGE_DETAIL_UNIT_NAME'] = 'Заголовок дела';
$MESS['LIB_SEARCH_PAGE_DETAIL_DATES'] = 'Крайние даты';

$MESS['LIB_SEARCH_PAGE_DETAIL_DESCRIPTION'] = 'Описание предмета';
$MESS['LIB_SEARCH_PAGE_DETAIL_MATERIAL'] = 'Материал, техника исполнения';
$MESS['LIB_SEARCH_PAGE_DETAIL_SIZE'] = 'Размеры';

$MESS['LIB_SEARCH_PAGE_DETAIL_MPK'] = 'МПК';
$MESS['LIB_SEARCH_PAGE_DETAIL_DECLARANT'] = 'Заявитель';
$MESS['LIB_SEARCH_PAGE_DETAIL_PATENTHOLDER'] = 'Патентообладатель';
$MESS['LIB_SEARCH_PAGE_DETAIL_REPRESENTATIVE'] = 'Представитель';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH_DATE'] = 'Дата публикации';
$MESS['LIB_SEARCH_PAGE_DETAIL_PATENTCODE'] = 'Код вида документа';

$MESS['LIB_SEARCH_PAGE_DETAIL_CATEGORIES'] = 'Рубрики';

// ---- libs
$MESS['LIB_SEARCH_PAGE_DETAIL_ADDRESS'] = 'Адрес';
$MESS['LIB_SEARCH_PAGE_DETAIL_WORK'] = 'График работы';
$MESS['LIB_SEARCH_PAGE_DETAIL_USER'] = 'Участник';
$MESS['LIB_SEARCH_PAGE_DETAIL_URL'] = 'перейти в библиотеку';

// ----
$MESS['LIB_SEARCH_PAGE_DETAIL_ABOUT']     = 'О произведении';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLIC']    = 'Открытое издание';
$MESS['LIB_SEARCH_PAGE_DETAIL_PROTECTED'] = 'Этот объект охраняется авторским правом';
$MESS['LIB_SEARCH_PAGE_DETAIL_COPYRIGHT'] = 'Издание закрыто по требованию правообладателя';

$MESS['LIB_SEARCH_PAGE_DETAIL_EKBSON'] = 'Запись каталога ЭКБСОН';
$MESS['LIB_SEARCH_PAGE_DETAIL_CATALOG'] = 'Запись каталога';

$MESS['LIB_SEARCH_PAGE_DETAIL_NEAREST_BIBLIO_PAPER'] = 'Ближайшая библиотека с бумажным экземпляром издания';
$MESS['LIB_SEARCH_PAGE_DETAIL_NEAREST_BIBLIO_I'] = 'Ближайший электронный читальный зал';
$MESS['LIB_SEARCH_PAGE_DETAIL_SERVER_ERROR'] = 'Ошибка сервера. Пожалуйста, повторите запрос';
$MESS['LIB_SEARCH_PAGE_DETAIL_RELEVANT_BOOKS'] = 'C этим изданием также искали';
$MESS['LIB_SEARCH_PAGE_DETAIL_PAPER_TO_EBOOK'] = 'Вы можете оформить заявку на перевод данного произведения в электронный вид';

// ---- действия
$MESS['LIB_SEARCH_PAGE_DETAIL_BACK'] = 'Вернуться';
$MESS['LIB_SEARCH_PAGE_DETAIL_READ'] = 'Читать';
$MESS['LIB_SEARCH_PAGE_DETAIL_VIEW'] = 'Смотреть';
$MESS['LIB_SEARCH_PAGE_DETAIL_LISTEN'] = 'Слушать';
$MESS['LIB_SEARCH_PAGE_DETAIL_REPORT_AN_ERROR'] = 'Сообщить об ошибке';
$MESS['LIB_SEARCH_PAGE_DETAIL_OPEN_THIS_BOOK'] = 'Открыть эту книгу';
$MESS['LIB_SEARCH_PAGE_DETAIL_CLOSE_THIS_BOOK'] = 'Закрыть эту книгу';
$MESS['LIB_SEARCH_PAGE_DETAIL_ORDER'] = 'Оформить заявку';

$MESS[''] = '';