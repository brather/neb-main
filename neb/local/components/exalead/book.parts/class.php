<?php

/**
 * Class ExaleadBookPartsComponent
 */
class ExaleadBookPartsComponent extends CBitrixComponent
{
    /**
     * @var \Nota\Exalead\SearchClient
     */
    protected $_partsClient;

    /**
     * @param $arParams
     *
     * @return mixed
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams['PARENT_ID'] = trim($arParams['BOOK_ID']);

        return $arParams;
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function _loadParts()
    {
        NebMainHelper::includeModule('nota.exalead');
        $query = new \Nota\Exalead\SearchQuery();
        $query->setQuery('idbook:"' . $this->arParams['BOOK_ID'] . '"');
        $query->setParam('s', 'asc(document_startpageparts)');
        $query->setNavParams();
        $this->_partsClient = new \Nota\Exalead\SearchClient();
        $this->arResult['parts'] = $this->_partsClient->getResult($query);
        if (isset($this->arResult['parts']['ITEMS'])
            && is_array($this->arResult['parts']['ITEMS'])
        ) {
            usort(
                $this->arResult['parts']['ITEMS'],
                function ($a, $b) {
                    return ((integer)$a['startpageparts']
                        < (integer)$b['startpageparts'])
                        ? -1 : 1;
                }
            );
        }
        if (!is_array($this->arResult['parts'])) {
            $this->arResult['parts'] = array();
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _buildNav()
    {
        $this->_partsClient->getDBResult($this->arResult['parts']);
        $this->arResult['navStr'] = $this->_partsClient->strNav;

        return $this;
    }

    public function executeComponent()
    {
        $this->arResult['allowReadClosedBooks']
            = nebUser::allowReadClosedBooks();

        $this->_loadParts()
            ->_buildNav();

        $this->includeComponentTemplate();
    }
}