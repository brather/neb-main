<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);?>

<?$APPLICATION->IncludeComponent(
    "neb:faq.list.edit.url",
    "",
    array(
        "IBLOCK_ID" => $arResult['IBLOCK_ID'],
        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
        "SEF_FOLDER" => $arParams['SEF_FOLDER'],
        "URL_PAGE_SECTION" => $arParams['SEF_URL_TEMPLATES']['section'],
        "URL_PAGE_ITEM" => $arParams['SEF_URL_TEMPLATES']['item'],
    )
);?>

<!-- <?= Loc::getMessage("FAQ_ADD") ?> -->
<p>

    <a class="btn btn-default" href="<?= $arParams['SEF_FOLDER'] ?><?= $arParams['SEF_URL_TEMPLATES']['add_item'] ?>">
        <?= Loc::getMessage("FAQ_ADD_ELEMENT") ?>
    </a>

    <a class="btn btn-default" href="<?= $arParams['SEF_FOLDER'] ?><?= $arParams['SEF_URL_TEMPLATES']['add_section'] ?>">
        <?= Loc::getMessage("FAQ_ADD_SECTION") ?>
    </a>
</p>

<!-- <?= Loc::getMessage("FAQ_EDIT") ?> -->