<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
	$this->SetViewTarget('lib_menu', $pos);
	include_once('menu.php');
	$this->EndViewTarget();
	
	$APPLICATION->IncludeComponent(
		"neb:library.news.detail",
		"spacial",
		Array(
			"IBLOCK_ID" => 3,
			"ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
			"CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
			"LIBRARY_CODE" => $arResult["VARIABLES"]["CODE"],
			"LIST_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]['list'],
			"NEWS_DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]['news_detail'],
			"COLLECTION_URL" => $arMenu['collections'],
		),
		$component
	);
?>