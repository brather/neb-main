<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

?>
<? ShowError($arResult["strProfileError"]); ?>
<?
if ($arResult['DATA_SAVED'] == 'Y') {
    LocalRedirect($arParams['STEP_2_PAGE']);
    exit();
}

?>
<form class="b-form b-form_common b-regform" method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>"
      enctype="multipart/form-data">

    <?= $arResult["BX_SESSION_CHECK"] ?>
    <input type="hidden" name="lang" value="<?= LANG ?>"/>
    <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
    <input type="hidden" name="EMAIL" value="<?= $arResult["arUser"]["EMAIL"] ?>"/>
    <input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>"/>


    <div class="b-form_header"><span class="iblock">Личная информация</span><span
                class="iblock">Сфера специализации</span></div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings01">Фамилия</label>
            <div class="field validate">
                <input type="text" data-validate="fio" data-required="required"
                       value="<?= $arResult["arUser"]["LAST_NAME"] ?>" id="settings01" data-maxlength="30"
                       data-minlength="2" name="LAST_NAME" class="input">
                <em class="error required">Поле обязательно для заполнения</em>
                <em class="error validate">Неверный формат</em>
                <em class="error maxlength">Более 30 символов</em>
            </div>
        </div>
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings08">Отрасль знаний</label>
            <div class="field validate iblock">
                <em class="error required">Заполните поле</em>
                <select name="UF_BRANCH_KNOWLEDGE" id="settings08" class="js_select w370" data-required="true">
                    <option value="">выберете</option>
                    <?
                    $arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE'));
                    if (!empty($arUF_BRANCH_KNOWLEDGE)) {
                        foreach ($arUF_BRANCH_KNOWLEDGE as $arItem) {
                            ?>
                            <option <?= $arResult['arUser']['UF_BRANCH_KNOWLEDGE'] == $arItem['ID'] ? 'selected="selected"' : '' ?>
                                    value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
                            <?
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings02">Имя</label>
            <div class="field validate">
                <input type="text" data-required="required" data-validate="fio"
                       value="<?= $arResult["arUser"]["NAME"] ?>" id="settings02" data-maxlength="30" data-minlength="2"
                       name="NAME" class="input">
                <em class="error required">Поле обязательно для заполнения</em>
                <em class="error validate">Неверный формат</em>
                <em class="error maxlength">Более 30 символов</em>
            </div>
        </div>
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings09">Образование</label>
            <div class="field validate iblock">
                <em class="error required">Заполните поле</em>
                <select name="UF_EDUCATION" id="settings09" class="js_select w370" data-required="true">
                    <option value="">выберете</option>
                    <?
                    $arUF_EDUCATION = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION'));
                    if (!empty($arUF_EDUCATION)) {
                        foreach ($arUF_EDUCATION as $arItem) {
                            ?>
                            <option <?= $arResult['arUser']['UF_EDUCATION'] == $arItem['ID'] ? 'selected="selected"' : '' ?>
                                    value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
                            <?
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings22">Отчество</label>
            <div class="field validate">
                <input type="text" data-validate="fio" value="<?= $arResult["arUser"]["SECOND_NAME"] ?>" id="settings22"
                       data-maxlength="30" data-minlength="2" data-required="true" name="SECOND_NAME" class="input">
                <em class="error required">Поле обязательно для заполнения</em>
                <em class="error validate">Неверный формат</em>
                <em class="error maxlength">Более 30 символов</em>
            </div>
        </div>
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings10">Место работы/учебы</label>
            <div class="field validate">
                <em class="error required">Заполните поле</em>
                <input type="text" data-validate="alpha" value="<?= $arResult["arUser"]["WORK_COMPANY"] ?>"
                       id="settings10" data-maxlength="30" data-minlength="2" data-required="true" name="WORK_COMPANY"
                       class="input">
            </div>
        </div>
    </div>

    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <?
            # переводим дату в другой формат, что бы сработал js
            if (!empty($arResult['arUser']['PERSONAL_BIRTHDAY']))
                $arResult['arUser']['PERSONAL_BIRTHDAY'] = FormatDate('j.n.Y', MakeTimeStamp($arResult['arUser']['PERSONAL_BIRTHDAY']))
            ?>
            <label for="settings06">Дата рождения</label>
            <div class="field validate iblock seldate">
                <em class="error dateformat">Неверный формат даты</em>
                <em class="error required">Заполните</em>
                <em class="error yearsrestrict">Вам должно быть<br>не меньше 12 лет</em>
                <input type="hidden" class="realseldate" data-required="true"
                       value="<?= $arResult["arUser"]["PERSONAL_BIRTHDAY"] ?>" id="settings06"
                       name="REGISTER[PERSONAL_BIRTHDAY]">
                <select name="birthday" class="js_select w102 sel_day" data-required="true">
                    <option value="">день</option>
                    <?
                    for ($i = 1; $i <= 31; $i++) {
                        ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                        <?
                    }
                    ?>
                </select>

                <select name="birthmonth" class="js_select w165 sel_month" data-required="true">
                    <option value="">месяц</option>
                    <?
                    for ($i = 1; $i <= 12; $i++) {
                        ?>
                        <option value="<?= $i ?>"><?= FormatDate('f', MakeTimeStamp('01.' . $i . '.' . date('Y'))) ?></option>
                        <?
                    }
                    ?>
                </select>

                <select name="birthyear" class="js_select w102 sel_year" data-required="true">
                    <option value="">год</option>
                    <?
                    for ($i = (date('Y') - 14); $i >= (date('Y') - 95); $i--) {
                        ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                        <?
                    }
                    ?>
                </select>
            </div>

        </div>
    </div>
    <div class="fieldrow nowrap ptop">
        <div class="fieldcell iblock">
            <label for="settings07">Пол</label>
            <div class="field validate">
                <select name="PERSONAL_GENDER" id="settings07" class="js_select w270">
                    <option value="M"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
                    <option value="F"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_FEMALE") ?></option>
                </select>
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <label for="settings11">Мобильный телефон</label>
            <div class="field validate">
                <input type="text" class="input maskphone" name="PERSONAL_MOBILE" id="settings11"
                       value="<?= $arResult["arUser"]["PERSONAL_MOBILE"] ?>">
            </div>
        </div>
    </div>
    <?
    $sImage = (intval($arResult['arUser']['PERSONAL_PHOTO']) > 0) ? CFile::GetPath($arResult['arUser']['PERSONAL_PHOTO']) : '';
    if ($arResult['arUser']['PERSONAL_PHOTO']['SRC']) {
        $sImage = $arResult['arUser']['PERSONAL_PHOTO']['SRC'];
    }
    ?>
    <? $APPLICATION->IncludeComponent(
        "notaext:plupload",
        "full_access_1",
        array(
            "MAX_FILE_SIZE" => "10",
            "FILE_TYPES" => "jpg,jpeg,png",
            "DIR" => "tmp_register",
            "FILES_FIELD_NAME" => "profile_file",
            "MULTI_SELECTION" => "N",
            "CLEANUP_DIR" => "Y",
            "UPLOAD_AUTO_START" => "Y",
            "RESIZE_IMAGES" => "Y",
            "RESIZE_WIDTH" => "110",
            "RESIZE_HEIGHT" => "110",
            "RESIZE_CROP" => "Y",
            "RESIZE_QUALITY" => "98",
            "UNIQUE_NAMES" => "Y",
            "PERSONAL_PHOTO" => $sImage,
        ),
        false
    ); ?>
    <hr>
    <div class="checkwrapper ">
        <input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb3"><label for="cb3" class="black">Согласен
            на обработку персональных данных, ознакомлен c</label><a href="/user-agreement/" target="_blank">правилами
            использования</a>
    </div>
    <div class="fieldrow nowrap fieldrowaction">
        <div class="fieldcell ">
            <div class="field clearfix">
                <button class="formbtn btdisable" name="save" value="1" type="submit" disabled="disabled">Перейти к шагу
                    2
                </button>

            </div>
        </div>
    </div>
</form>
