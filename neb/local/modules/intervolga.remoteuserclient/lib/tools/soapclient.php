<?namespace Intervolga\RemoteUserClient\Tools;

\Bitrix\Main\Loader::includeModule("webservice");

/**
 * Class SoapClient
 *
 * @package Intervolga\RemoteUserClient\Tools
 */
class SoapClient extends \CSOAPClient
{
	protected $errorNumber;
	protected $errorString;
	protected $ErrorString;

	public function send(\CSOAPRequest $request)
	{
		if ( $this->Timeout != 0 )
		{
			$fp = fsockopen( ($this->Port == 443 ? "ssl://" : "") . $this->Server,
				$this->Port,
				$this->errorNumber,
				$this->errorString,
				$this->Timeout );
		}
		else
		{
			$fp = fsockopen( ($this->Port == 443 ? "ssl://" : "") . $this->Server,
				$this->Port,
				$this->errorNumber,
				$this->errorString );
		}

		if ( $fp == 0 )
		{
			$this->ErrorString = '<b>Error:</b> ' . __METHOD__ . ' : Unable to open connection to ' . $this->Server . '.';
			return 0;
		}

		$payload = $request->payload();

		$authentification = "";
		if ( ( $this->login() != "" ) )
		{
			$authentification = "Authorization: Basic " . base64_encode( $this->login() . ":" . $this->password() ) . "\r\n" ;
		}

		$name = $request->name();
		$namespace = $request->get_namespace();
		if ($namespace[strlen($namespace)-1] != "/")
			$namespace .= "/";

		$HTTPRequest = "POST " . $this->Path . " HTTP/1.0\r\n" .
			"User-Agent: BITRIX SOAP Client\r\n" .
			"Host: " . $this->Server . "\r\n" .
			$authentification .
			"Content-Type: text/xml; charset=utf-8\r\n" .
			"SOAPAction: \"" . $request->get_namespace() . $request->name() . "\"\r\n" .
			"Content-Length: " . (defined('BX_UTF') && BX_UTF == 1 && function_exists('mb_strlen') ? mb_strlen($payload, 'latin1') : strlen($payload))  . "\r\n\r\n" .
			$payload;

		$this->SOAPRawRequest = $HTTPRequest;
		if ( !fwrite( $fp, $HTTPRequest /*, strlen( $HTTPRequest )*/ ) )
		{
			$this->ErrorString = "<b>Error:</b> could not send the SOAP request. Could not write to the socket.";
			$response = 0;
			return $response;
		}

		$rawResponse = "";
		// fetch the SOAP response
		while ( $data = fread( $fp, 32768 ) )
		{
			$rawResponse .= $data;
		}

		// close the socket
		fclose( $fp );

		$this->SOAPRawResponse = $rawResponse;
		$response = new \CSOAPResponse();
		$response->decodeStream($request, $rawResponse);

		return $response;
	}
}
