<?
use Bitrix\NotaExt\ImgConvert;

$arTemplateParam = array();
$arComponentTemplates = CComponentUtil::GetTemplatesList($this->__component->__name);
foreach ($arComponentTemplates as $template) {
    if ($template['NAME'] == $this->__name)
        $arTemplateParam = \Cutil::JsObjectToPhp($template['DESCRIPTION']);
}

if (empty($arTemplateParam))
    return false;

$arItems = array();
foreach ($arParams['PARAMS_ITEMS'] as $item) {
    $arCropParams = array();
    if (!empty($item['Ar_PARAM']['Data'])) {
        unset($item['Ar_PARAM']['Data']['id']);
        unset($item['Ar_PARAM']['Data']['sessid']);
        $arCropParams = $item['Ar_PARAM']['Data'];
    }

    $arCropParams['wResize'] = $arTemplateParam['width'];
    $arCropParams['hResize'] = $arTemplateParam['height'];

    $img = ImgConvert::Crop($arResult['ITEMS'][$item['ID']]['PICTURE'], $arCropParams, '/upload/resize_cache_dg/');

    $arItem = array(
        'PREVIEW_TEXT' => empty($item['PREVIEW_TEXT']) ? $arResult['ITEMS'][$item['ID']]['TEXT'] : $item['PREVIEW_TEXT'],
        'PICTURE' => $img
    );
    $arItems[] = $arItem;
}

$arResult['ITEMS'] = $arItems;
