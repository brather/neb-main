<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('nota.userdata');

use Nota\UserData\rgb;

$rgb = new rgb();

$data = $_REQUEST;
$result = array();

try{

    if(!isset($data['secret_key']) || ('lFDf2243alWf1Bg' !== $data['secret_key']))
        throw new Exception('Неверный секретный ключ!');

    if(!isset($data['status']) || (!in_array($data['status'], array('rejected', 'confirm'))))
        throw new Exception('Неверный статус!');

    if(!isset($data['id_request']) || (!is_numeric($data['id_request'])))
        throw new Exception('Неверный id_request!');

    $nebUser  = new nebUser();
    $by    = 'id';
    $order = 'asc';

    $arParams["SELECT"] = array("UF_ID_REQUEST", 'UF_RGB_CARD_NUMBER', 'UF_RGB_USER_ID', 'UF_STATUS', 'UF_LIBRARY' , 'UF_LIBRARIES');

    $userData  = $nebUser->GetList($b, $o, array("=UF_ID_REQUEST" => $data['id_request']), $arParams)->Fetch();
    if(!$userData)
        $userData  = $nebUser->GetList($b, $o, array("=EMAIl" => $data['email']), $arParams)->Fetch();

    if(!$userData)
        throw new Exception('Нет такого пользователя!');

    if('confirm' == $data['status']):
        $fields = array(
            "UF_ID_REQUEST"      => $data['id_request'],
            "UF_RGB_CARD_NUMBER" => $data['reader_number'],
            "UF_RGB_USER_ID"     => $data['uid'],
            "UF_STATUS"          => nebUser::USER_STATUS_VERIFIED,
            "UF_PREVERIFY_TIME"  => '',
        );
        
        if(!$userData['UF_LIBRARY']):
            $fields['UF_LIBRARY'] = RGB_LIB_ID;
        endif;

        if(!$userData['UF_LIBRARIES']) {
            $fields['UF_LIBRARIES'] = array( RGB_LIB_ID );
        } elseif ( !in_array(RGB_LIB_ID, $userData['UF_LIBRARIES'] ) ) {
            $fields['UF_LIBRARIES'] = $userData['UF_LIBRARIES'];
            $fields['UF_LIBRARIES'][] = RGB_LIB_ID;
        }

        $res = $nebUser->Update($userData['ID'], $fields);
        //Отправка почты.

        //Пользователю
        $event = new CEvent;
        //$event->SendImmediate('USER_REGISTRATION', SITE_ID, $userData); //Почему-то не работает
        $event->Send('USER_REGISTRATION', SITE_ID, $userData); // Послать письмо пользователю, которого зарегистрировали.
        
        //<Библиотеке
        $res = CIBlockElement::GetProperty(4, $userData['UF_LIBRARY'], array(),array('CODE'=>'EMAIL')); //Пробуем найти email библиотеки
        while ($ar_res = $res->GetNext()) {
            $email = $ar_res['VALUE'];
//            $email = 'slapin@elar.ru'; //Дебаг
            if (trim($email) != ''){ //Если email указан отправляем инфо пользователя
                $libData = $userData;
                $libData['EMAIL'] = $email;
                $event->Send('USER_INFO', SITE_ID, $userData); // Письмо с реквизитами пользователя в адрес бибилиотеки.       
            }
        }

    elseif (!empty($userData['UF_ID_REQUEST'])
        && $nebUser->canVerify($userData['ID'])
        && 'rejected' === $data['status']
    ):
        $nebUser->Update($userData['ID'], array('UF_ID_REQUEST' => ''));
    endif;

    $result['result']  = true;
    $result['error']   = false;
    $result['message'] = '';

}catch (\Exception $e){
    $result['result']  = false;
    $result['error']   = true;
    $result['message'] = $e->getMessage();
}

$log = new \CEventLog();
$log->Log(
    'INFO',
    'verification_neb_user',
    'user',
    'user',
    serialize(
        array(
            'result'  => $result,
            'request' => $_REQUEST,
        )
    )
);

echo json_encode($result);
flush();