<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 14:02
 */

namespace LbcBundle\Controller\Traits;


use Symfony\Component\DependencyInjection\ContainerInterface;
use JMS\DiExtraBundle\Annotation as DI;

trait ServiceContainerInjectTrait
{
    /**
     * @DI\Inject("service_container")
     * @var ContainerInterface
     */
    public $container;
}