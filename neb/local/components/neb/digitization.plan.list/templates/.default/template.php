<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

$context = Application::getInstance()->getContext();
$arServer = $context->getServer();
$arRequest = $context->getRequest()->toArray();
$showDuplication = false;
if($arRequest['show-duplication'] == 'off' || empty($arRequest['show-duplication'])){
    $clsShowAll = ' active';
    $clsShowDuble = '';
} elseif($arRequest['show-duplication'] == 'on') {
    $showDuplication = true;
    $clsShowAll = '';
    $clsShowDuble = ' active';
}

?>
<?if(!empty($arResult['DUPLICATION_URI'])){?>
    <div class="container-fluid">
        <div class="row">
            <div class="btn-group" data-toggle="buttons">
                <label data-show="all" data-href="<?= $arResult['DUPLICATION_URI'] ?>" class="btn btn-info js-toggle-show<?= $clsShowAll ?>">
                    <input type="radio" name="options" id="show-all"><?= Loc::getMessage('DIG_PLAN_EDIT_T_SHOW_ALL_PLANS') ?>
                </label>
                <label data-show="duplication" data-href="<?= $arResult['DUPLICATION_URI'] ?>" class="btn btn-default js-toggle-show<?= $clsShowDuble ?>">
                    <input type="radio" name="options" id="show-duplication"><?= Loc::getMessage('DIG_PLAN_EDIT_T_SHOW_ONLY_DUPLICATION') ?>
                </label>
            </div>
        </div>
        <div class="row"><hr></div>
    </div>
<?}?>


    <div class="b-readersearch clearfix">
        <h3><?=Loc::getMessage("MARKUP_LOG_TITLE");?></h3>
        <form class="form nrf" method="get" action="?">

            <input type="hidden" name="FILTER" value="Y" />

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Библиотека</label>
                        <select class="form-control" name="LIBRARY">
                            <option value="">Все</option>
                            <? foreach ($arResult['LIST_LIBRARY'] as $arLib): ?>
                                <option value="<?=$arLib['ID']?>"<?= $arRequest['LIBRARY'] == $arLib['ID']
                                    ? ' selected="selected"' : ''; ?>><?=$arLib['NAME']?></option>
                            <? endforeach;?>
                        </select>
                    </div>

                    <label>Плановая дата выполнения</label>
                    <div class="form-inline">
                        <?=Loc::getMessage("MARKUP_LOG_FROM");?>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="DATE_FROM" id="dateFromInput" size="10"
                                       value="<?= $arRequest['DATE_FROM'] ? : (new DateTime())->add("-60 days")?>"
                                       class="datepicker-from form-control" data-masked="99.99.9999" />
							<span class="input-group-btn">
								<a class="btn btn-default" onclick="BX.calendar({node: 'dateFromInput', field: 'dateFromInput',  form: '', bTime: false, value: ''});">
									<span class="glyphicon glyphicon-calendar"></span>
								</a>
							</span>
                            </div>
                        </div>

                        <?=Loc::getMessage("MARKUP_LOG_TO");?>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="DATE_TO" id="dateToInput" size="10"
                                       value="<?=$arRequest['DATE_TO'] ? : (new DateTime())?>"
                                       class="datepicker-current form-control" data-masked="99.99.9999" />
							<span class="input-group-btn">
								<a class="btn btn-default" onclick="BX.calendar({node: 'dateToInput', field: 'dateToInput',  form: '', bTime: false, value: ''});">
									<span class="glyphicon glyphicon-calendar"></span>
								</a>
							</span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Статус</label>
                        <select class="form-control" name="STATUS">
                            <option value="">Все</option>
                            <? foreach ($arResult['PROPERTY_STATUS_PLAN'] as $arStatus): ?>
                                <option value="<?=$arStatus['ID']?>"<?= $arRequest['STATUS'] == $arStatus['ID']
                                    ? ' selected="selected"' : '';?>><?=$arStatus['VALUE']?></option>
                            <? endforeach;?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>&nbsp;</label><br />
                        <input type="submit" value="<?= Loc::getMessage('FIND') ?>" class="btn btn-primary" />
                        <input type="reset" value="Сбросить" class="btn btn-default" onClick="
                            document.location.href = '<?=$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['template']?>';
                        "/>
                    </div>
                </div>
            </div>
        </form>
    </div>



<table class="table table-bordered table-striped table-hover table-plan-list">
    <thead>
    <tr>
        <th><?= Loc::getMessage('DIGITIZATION_PLAN_T_NUMBER') ?></th>
        <?foreach($arResult['SORT_LIST'] as $cSort => $arSort): ?>
            <th>
                <a href="<?= $arSort['URI'] ?>" class="<?= $arSort['CLASS'] ?>">
                    <?= Loc::getMessage('DIGITIZATION_PLAN_T_SORT_' . $cSort) ?>
                    <i class="caret"></i>
                </a>
            </th>
        <?endforeach;?>
    </tr>
    </thead>
    <?$iRow = 1;?>
    <?foreach ($arResult['ITEMS'] as $arItem):?>
        <?$icon = '';
        $clsDouble = '';
        $titleRow = '';
        $clsTd = '';
        if(in_array($arItem['ID'], $arResult['DUPLICATION_PLANS'])){
            $icon = '<i class="fa fa-exclamation-circle"></i> ';
            $clsDouble = ' class="pl-duplication"';
            $clsTd = ' class="info-ps"';
            $titleRow = ' title="' . Loc::getMessage('DIG_PLAN_EDIT_T_IN_CURRENT_PLAN_DUPLICATION_BOOKS') . '"';
        }?>
        <tr<?= $clsDouble ?>>
            <td><?= $arItem['INDEX'] ?></td>
            <td>
                <p<?= $clsTd ?><?= $titleRow ?>>
                    <?= $icon ?><?= $arResult['LIST_LIBRARY'][$arItem['UF_LIBRARY']]['NAME'] ?>
                </p>
            </td>
            <td>
                <a<?= $clsTd ?><?= $titleRow ?> href="<?= $arParams['SEF_FOLDER'] ?>?idplan=<?= $arItem['ID'] ?>">
                    <?= $arItem['UF_PLAN_NAME'] ?>
                </a>
            </td>
            <td><?= $arItem['COUNT_BOOKS'] ?></td>
            <td><?= $arItem['UF_DATE_F_PLAN'] ?></td>
            <td>
                <?if(empty($arItem['UF_REQUEST_STATUS'])){
                    $titleSelect = Loc::getMessage('DIGITIZATION_PLAN_T_EMPTY_STATUS');
                } else {
                    $titleSelect = $arResult['PROPERTY_STATUS_PLAN'][$arItem['UF_REQUEST_STATUS']]['VALUE'];
                }?>
                <?if($arItem['LIST_STATUS']['ACTIVE_SELECT'] == 'Y') {?>
                    <?foreach ($arResult['PROPERTY_STATUS_PLAN'] as $idStatus => $arStatus) {
                        foreach($arResult['PROPERTY_STATUS_BOOK'] as $idStatusBook => $arStatusBook) {
                            if($arStatusBook['VALUE'] == $arStatus['VALUE']) {?>
                                <input id="ps-<?= $idStatus ?>" type="hidden" name="plan-status-<?= $idStatus ?>" value="<?= $idStatusBook ?>" />
                                <?break;
                            }
                        }
                    }?>
                    <select name="plan-status"
                            id="ps-<?= $arItem['ID'] ?>"
                            data-plan-id="<?= $arItem['ID'] ?>"
                            data-ajax-url="<?= $arParams['AJAX_PLAN_EDIT'] ?>"
                            data-confirm-title="<?= Loc::getMessage('DIG_PLAN_LIST_T_CONFIRM_TITLE') ?>"
                            data-confirm-question="<?= Loc::getMessage('DIG_PLAN_LIST_T_CONFIRM_QUESTION') ?>"
                            data-confirm-request="<?= Loc::getMessage('DIG_PLAN_LIST_T_CONFIRM_REQUEST') ?>"
                            class="form-control js-plan-status-list info-ps"
                            title="<?= $titleSelect ?>">
                        <?foreach($arItem['LIST_STATUS']['OPTIONS'] as $idStatus) {?>
                            <?$selectPlan = '';
                            if($idStatus == $arItem['UF_REQUEST_STATUS']) {
                                $selectPlan = ' selected';
                            }?>
                            <option value="<?= $idStatus ?>"<?= $selectPlan ?>>
                                <?= $arResult['PROPERTY_STATUS_PLAN'][$idStatus]['VALUE'] ?>
                            </option>
                        <?}?>
                    </select>
                <?} else {?>
                    <p><?= $arResult['PROPERTY_STATUS_PLAN'][$arItem['LIST_STATUS']['OPTIONS'][0]]['VALUE'] ?></p>
                <?}?>
            </td>
            <td><?= $arItem['UF_DATE_AGREEMENT'] ?></td>
            <td><?= $arItem['UF_DATE_F_ACTUAL'] ?></td>
        </tr>
        <?$iRow++;?>
    <?endforeach;?>
</table>

<?
$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $arResult['NAV'],
        "SEF_MODE" => "N",
    ),
    false
);