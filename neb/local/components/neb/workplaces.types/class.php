<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 07.09.2016
 * Time: 16:00
 */

use \Bitrix\Main\Application;
use \Bitrix\Iblock\PropertyTable;
use \Bitrix\Iblock\PropertyEnumerationTable;

/**
 * Class WorkplacesTypesComponent
 */
class WorkplacesTypesComponent extends CBitrixComponent
{
    private $page      = 'template';
    private $iProperty = 0;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        if (isset($arParams['ID']))
            $this->page = 'edit';

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->_includeModules();
        $this->_getProperty();
        $this->_getData();
        $this->_workplacesCount();
        $this->_updateData();

        $this->includeComponentTemplate($this->page);
    }

    /**
     * Подключает модули
     * @throws \Bitrix\Main\LoaderException
     */
    private function _includeModules() {
        \Bitrix\Main\Loader::IncludeModule('iblock');
    }

    /**
     * Получает идентификатор свойства 'Тип ЭЧЗ'
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getProperty() {

        // получение ID свойства
        $arProperty = PropertyTable::getList([
            'filter' => ['IBLOCK_ID' => IBLOCK_ID_WORKPLACES, 'CODE' => 'TYPE', 'ACTIVE' => 'Y'],
            'select' => ['ID'],
        ])->fetchRaw();
        if ($arProperty['ID'])
            $this->iProperty = intval($arProperty['ID']);
        else
            $this->arResult['ERRORS'][] = 'Невозможно найти свойство "TYPE" в инфоблоке №' . IBLOCK_ID_WORKPLACES;
    }

    /**
     * Метод получает список значений свойства 'Тип ЭЧЗ'
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getData() {

        if ($this->iProperty < 1)
            return;

        $arResult = [];

        // получение списка значений свойства
        $rsElements = PropertyEnumerationTable::getList([
            'filter' => ['PROPERTY_ID' => $this->iProperty],
            'order'  => ['SORT' => 'ASC'],
            'select' => ['PROPERTY_ID', 'ID', 'XML_ID', 'VALUE', 'SORT', 'DEF'],
        ]);
        while ($arFields = $rsElements->fetch())
            $arResult[$arFields['ID']] = ['IBLOCK_ID' => IBLOCK_ID_WORKPLACES] + $arFields;

        $this->arResult['ITEMS'] = $arResult;
    }

    /**
     * Получение группировки свойств по ЭЧЗ
     */
    private function _workplacesCount() {

        $obIBlockElement = new \CIBlockElement();
        $res = $obIBlockElement->GetList(
            [],
            ['IBLOCK_ID' => IBLOCK_ID_WORKPLACES],
            ['PROPERTY_TYPE']
        );
        while ($arFields = $res->Fetch())
            if (!empty($this->arResult['ITEMS'][$arFields['PROPERTY_TYPE_ENUM_ID']]))
                $this->arResult['ITEMS'][$arFields['PROPERTY_TYPE_ENUM_ID']]['COUNT'] = $arFields['CNT'];
    }

    /**
     * Добавляет/изменяет/удаляет значение свойства "Тип ЭЧЗ"
     */
    private function _updateData() {

        $arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
        $arRequest = $arRequest['TYPE'];

        if ($this->iProperty < 1 || empty($arRequest))
            return;

        $arError = [];

        if (empty($arRequest['XML_ID']))
            $arError[] = 'Поле "Символьный код" обязательно для заполнения!';
        if (empty($arRequest['VALUE']))
            $arError[] = 'Поле "Название" обязательно для заполнения!';
        if (intval($arRequest['SORT']) < 1)
            $arError[] = 'Поле "Сортировка" обязательно для заполнения!';

        if (empty($arError)) {

            // пропускаем только эти поля
            $arUpdate = [];
            foreach (['XML_ID', 'VALUE', 'SORT'] as $sProp)
                $arUpdate[$sProp] = $arRequest[$sProp];

            // удаление
            if ($arRequest['ACTION'] == 'delete') { // TODO: впилить защиту при имеющихся ВЧЗ с данным свойством
                $result = PropertyEnumerationTable::delete(['PROPERTY_ID' => $this->iProperty, 'ID' => $arRequest['ID']]);
            }
            // обновление
            elseif ($arRequest['ID'] > 0) {
                $result = PropertyEnumerationTable::update(
                    ['PROPERTY_ID' => $this->iProperty, 'ID' => $arRequest['ID']],
                    $arUpdate
                );
            }
            // добавление
            else {
                $result = PropertyEnumerationTable::add(['PROPERTY_ID' => $this->iProperty] + $arUpdate);
            }

            if ($result->isSuccess())
                LocalRedirect($this->arParams['LIST_URL']);
            else
                $arError[] = implode(', ',$result->getErrorMessages());
        }

        $this->arResult['ERRORS'] = $arError;
    }
}