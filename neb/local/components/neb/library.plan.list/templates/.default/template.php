<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

use \Bitrix\Main\Application;

$context = Application::getInstance()->getContext();
$arServer = $context->getServer()->toArray();
$arRequest = $context->getRequest()->toArray();

foreach ($arResult['LIST_STATUS'] as $arItem)
    $arStatus[$arItem['ID']] = $arItem['VALUE'];

?>
<?$APPLICATION->IncludeComponent(
    'neb:library.plan.add',
    '',
    array(
        "HL_ID" => $arResult["HL_ID"],
        "HL_CODE" => HIBLOCK_CODE_DIGITIZATION_PLAN_LIST,
        "SEF_FOLDER" => "/profile/plan_digitization/",
        "UF_LIBRARY" => $arResult["UF_LIBRARY"],
    ),
    false
);?>

<? if (!empty($arResult["ITEMS_PLAN"])): ?>

    <div id="context" class="contexmenufix">
        <dl class="responsive-table-layout biblio-digitization">

            <dt>
                <span style="width: 120px;">План</span>
                <span>Дата создания</span>
                <span>Дата согласования</span>
                <span>Плановая дата выполнения</span>
                <span>Фактическая дата выполнения</span>
                <span>Статус</span>
            </dt>

            <?foreach($arResult["ITEMS_PLAN"] as $iPlan => $arPlan): ?>
                <dd>
                    <span><a href="/profile/plan_digitization/info/<?= $arPlan["ID"] ?>/"><?=$arPlan['UF_PLAN_NAME']?></a></span>
                    <span><?=$arPlan['UF_DATE_CREATE']?></span>
                    <span><?=$arPlan['UF_DATE_AGREEMENT']?></span>
                    <span><?=$arPlan['UF_DATE_F_PLAN']?></span>
                    <span><?=$arPlan['UF_DATE_F_ACTUAL']?></span>
                    <span><?=$arStatus[$arPlan['UF_REQUEST_STATUS']]?></span>
                </dd>
            <? endforeach; ?>

        </dl>
    </div>
<? endif; ?>

<?
$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $arResult['NAV'],
        "SEF_MODE" => "Y",
    ),
    false
);