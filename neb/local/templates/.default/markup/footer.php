<footer>
	<div class="wrapper">
		<div class="b-footer_row clearfix">
			<a href="#" class="b-footerlogo"><img src="./i/logo_2.png" alt=""></a>
			<ul class="b-footer_nav">
				<li><a href="#">Участники</a></li>
				<li><a href="#">Коллекции</a></li>
			</ul>
			<ul class="b-footer_nav">
				<li><a href="#">О проекте</a></li>
				<li><a href="#">Для частных лиц</a></li>
				<li><a href="#">Для библиотек</a></li>
				<li><a href="#">Правовая информация</a></li>
				<li><a href="#">Пользовательское соглашение</a></li>
			</ul>
			<ul class="b-footer_nav">
				<!--<li><a href="#">Форум</a></li>-->
				<li><a href="#">Часто задаваемые вопросы</a></li>
				<li><a href="#">Регистрация</a></li>
				<li><a href="mailto:">Обратная связь</a></li>
			</ul>
		</div>
		<div class="b-footer_row clearfix">					
			<div class="b-copy">Все права защищены. Полное или частичное копирование материалов запрещено,<br> при согласованном использовании материалов необходима ссылка на ресурс. <br>
				Полное или частичное копирование произведений запрещено, согласование <br> использования произведений производится с их авторами.</div>					
				<div class="b-footer_support">
					<div class="b-footer_support_tit">При поддержке</div>
					<a href="http://mkrf.ru/"><img src="./i/ministr.png" alt=""></a>
				</div>
			</div>
		</div><!-- /.wrapper -->
	</footer>