<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'       => '/',
                'VARIABLES'     => array(
                    'token',
                    'request_id',
                    'confirm',
                    'refuse',
                    'view_price',
                    'request_type',
                    'book_id',
                    'limit',
                    'offset',
                ),
                'TYPE'          => array('PUT', 'GET'),
                'DOCUMENTATION' => array(
                    'GROUP'       => 'CONTROL',
                    'DESCRIPTION' => '',
                    'PARAMETERS'  => array()
                ),
            ),
        ),
    ),
); 