<?
$MESS['NOTES_SORT'] = 'Сортировать';
$MESS['NOTES_SORT_AUTHOR'] = 'По автору';
$MESS['NOTES_SORT_NAME'] = 'По названию';
$MESS['NOTES_SORT_DATE'] = 'По дате';
$MESS['NOTES_SHOW'] = 'Показать';
$MESS['NOTES_SETTINGS'] = 'Настройки';
$MESS['NOTES_REMOVE'] = 'Удалить';
$MESS['NOTES_FROM_NOTES'] = 'из Моих заметок';
$MESS['NOTES_AUTHOR'] = 'Автор';
$MESS['NOTES_BOOK'] = 'Книга';
$MESS['NOTES_TEXT'] = 'Заметка на странице';
$MESS['NOTES_MY_COLLECTIONS'] = 'Мои подборки';
$MESS['NOTES_CREATE_COLLECTION'] = 'cоздать подборку';
$MESS['PROFILE_MU_NUMBER_LC'] = 'Номер ЕЭЧБ';
?>