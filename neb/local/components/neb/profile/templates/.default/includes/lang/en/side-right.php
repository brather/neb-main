<?php
$MESS['PROFILE_MU_REGISTER'] = 'Registered';
$MESS['PROFILE_MU_ACCESS_R'] = 'limited access';
$MESS['PROFILE_MU_ACCESS_F'] = 'full access';
$MESS['PROFILE_MU_PROFILE'] = 'profile settings';
$MESS['PROFILE_MU_ACCESS_GET'] = 'get full access';
$MESS['PROFILE_MU_VIEW_ALL_MESSAGES'] = 'My messages';