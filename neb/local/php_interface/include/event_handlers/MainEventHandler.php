<?

use \Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler('main', 'OnEndBufferContent', ['MainEventHandler', 'minifyHtmlCode']);
EventManager::getInstance()->addEventHandler('main', 'OnBuildGlobalMenu',  ['MainEventHandler', 'buildAdminMenu']);

class MainEventHandler
{
    /**
     * Минификация html-кода (требование Google https://developers.google.com/speed/pagespeed/insights/ )
     * Не подключается:
     *  1. В административной части Битрикса
     *  2. Во всех режимах отладки НЭБ (для pre() требуется развернутый вид)
     * Для его корректной работы требуется полное отсутствие однострочных комментариев между тегами <script></script>
     * Утилита поиска однострочных комментариев в проекте \Neb\Main\Agents\Main::searchSingleLineComments();
     *
     * @param $content
     */
    public static function minifyHtmlCode (&$content) {
        if (MINIFY_HTML_MODE === true && ADMIN_SECTION !== true
                && EXALEAD_DEBUG_MODE !== true && EXALEAD_FULL_DEBUG_MODE !== true && MARKUP_DEBUG_MODE !== true) {
            // && !nebUser::checkDemo() && !nebUser::checkLocal()
            $search  = ['/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s'];
            $replace = ['>', '<', '\\1'];
            $content = preg_replace($search, $replace, $content);
        }
    }

    /**
     * Сборка меню в администратративной части сайта
     *
     * @param $adminMenu
     * @param $aModuleMenu
     */
    public static function buildAdminMenu(&$adminMenu, &$aModuleMenu)
    {
        $obUser = new \CUser;
        if ($obUser->IsAdmin()) {
            // Меню НЭБ
            $adminMenu['global_menu_neb'] = array(
                "parent_menu" => "global_menu_neb",
                "menu_id" => 'neb',
                "section" => "neb",
                "sort" => 400,
                "url" => "",
                "text" => 'НЭБ',
                "items" => array(
                    array(
                        "sort" => 10,
                        "url" => "/bitrix/admin/user_statistics.php",
                        "text" => 'Статистика пользователей',
                        "icon" => "form_menu_icon",
                        "page_icon" => "form_page_icon",
                    ),
                    array(
                        "sort" => 20,
                        "url" => "/bitrix/admin/stat_library.php",
                        "text" => 'Общая статистика по библиотекам',
                        "icon" => "form_menu_icon",
                        "page_icon" => "form_page_icon",
                    ),
                    array(
                        "sort" => 30,
                        "url" => "/bitrix/admin/stat_library_all.php",
                        "text" => 'Статистика посещений страницы библиотеки',
                        "icon" => "form_menu_icon",
                        "page_icon" => "form_page_icon",
                    ),
                    array(
                        "sort" => 40,
                        "url" => "/bitrix/admin/log_import_libs.php",
                        "text" => 'Лог импорта библиотек',
                        "icon" => "form_menu_icon",
                        "page_icon" => "form_page_icon",
                    ),
                    array(
                        "sort" => 50,
                        "url" => "/bitrix/admin/log_import_libs_geocode.php",
                        "text" => 'Лог импорта координат библиотек',
                        "icon" => "form_menu_icon",
                        "page_icon" => "form_page_icon",
                    ),
                    array(
                        "sort" => 60,
                        "url" => "/bitrix/admin/log_import_workplaces.php",
                        "text" => 'Лог импорта ЭЧЗ',
                        "icon" => "form_menu_icon",
                        "page_icon" => "form_page_icon",
                    ),
                ),
            );
        }
        $unsetSections = [
            'forum' => true,
            'MAIN' => true,
        ];
        if(UGROUP_OPERATOR_WCHZ === nebUser::getCurrent()->getRole()) {
            unset($adminMenu['global_menu_desktop']);
            unset($adminMenu['global_menu_services']);
            foreach($aModuleMenu as $key => $menuItem) {
                if(isset($unsetSections[$menuItem['section']])) {
                    unset($aModuleMenu[$key]);
                }
            }
        }
    }
}