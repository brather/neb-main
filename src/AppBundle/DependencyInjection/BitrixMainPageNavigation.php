<?php
/**
 * User: agolodkov
 * Date: 29.09.2016
 * Time: 19:00
 */

namespace AppBundle\DependencyInjection;


use Bitrix\Main\UI\PageNavigation;
use BitrixBundle\DependencyInjection\BitrixHelper;

/**
 * @todo    ��� ������ ����������� ��������, ������� ������ �� mustache � ������������ ��� � ��������� � � �������
 * Class BitrixMainPageNavigation
 *
 * @package AppBundle\DependencyInjection
 */
class BitrixMainPageNavigation
{
    protected $_pageNavigation = [];

    /**
     * @var BitrixHelper
     */
    protected $_bitrix;

    /**
     * @var int
     */
    protected $_pageSize;

    /**
     * @param BitrixHelper $bitrix
     * @param int          $pageSize
     */
    public function __construct(BitrixHelper $bitrix, $pageSize)
    {
        $this->_bitrix = $bitrix;
        $this->_pageSize = $pageSize;
    }

    /**
     * @param $name
     *
     * @return PageNavigation
     */
    public function getPageNavigation($name)
    {
        if (!isset($this->_pageNavigation[$name])) {
            $this->_pageNavigation[$name] = new PageNavigation($name);
        }

        return $this->_pageNavigation[$name];
    }


    /**
     * @param string $name
     * @param int    $total
     * @param int    $size
     * @param int    $pageNum
     */
    public function initPageNavigation($name, $total = null, $size = null, $pageNum = null)
    {
        if ($size < 1) {
            $size = $this->_pageSize;
        }
        $obPageNavigation = $this->getPageNavigation($name);
        $obPageNavigation->allowAllRecords(false)->initFromUri();
        $obPageNavigation->setPageSize($size);
        if ($pageNum > 0) {
            $obPageNavigation->setCurrentPage($pageNum);
        }
        if ($total > 0) {
            $obPageNavigation->setRecordCount($total);
        }
    }

    /**
     *
     * @param string $name
     *
     * @return string
     */
    public function buildBitrixMainPageNavigation($name)
    {
        $obPageNavigation = $this->getPageNavigation($name);
        ob_start();
        $this->_bitrix->getApplication()->IncludeComponent(
            'bitrix:main.pagenavigation',
            '',
            array(
                'NAV_OBJECT' => $obPageNavigation,
            ),
            false
        );

        return ob_get_clean();
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->_pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->_pageSize = $pageSize;
    }
}