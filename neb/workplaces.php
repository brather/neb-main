<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
?>
<section class="bbox b-bookboardlg">
<?
	$APPLICATION->IncludeComponent(
	    'neb:workplaces', 'list', Array(),
	    $component
	);
?>

</section>
<script>
$(function() {
	$('ul.ul-city-list').dcAccordion({
		classParent: 'active-parent',
		classArrow: 'icon',
		eventType: 'click',
		autoClose: false,
		saveState: false,
		disableLink: true,
		showCount: false,
		speed: 'fast'
	});
});
</script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
