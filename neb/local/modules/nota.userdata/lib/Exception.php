<?php
/**
 * User: agolodkov
 * Date: 13.05.2015
 * Time: 16:08
 */

namespace Nota\UserData;


/**
 * Class Exception
 *
 * @package Nota\UserData
 */
class Exception extends \Exception
{
    const USER_NOT_FOUND = 1;
    const RGB_USER_CREATE_ERROR = 20;

    /**
     * @var array
     */
    protected $_errors = array();

    /**
     * @param string    $message
     * @param int       $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 0,
        Exception $previous = null
    ) {
        if (is_array($message)) {
            $this->_errors = $message;
            $message = implode(';' . PHP_EOL, $message);
        }

        parent::__construct($message, $code, $previous);
    }

    public function getErrors()
    {
        return $this->_errors;
    }
}