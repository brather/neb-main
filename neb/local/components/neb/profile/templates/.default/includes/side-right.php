<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<?php
/** @todo remove from template */
$arResult['FULL_ACCESS'] = false;
$arResult['CAN_GET_FULL_ACCESS'] = false;
if (USER_STATUS_VERIFIED === (integer)$arResult['USER']['UF_STATUS']
    || USER_STATUS_VIP === (integer)$arResult['USER']['UF_STATUS']
) {
    $arResult['FULL_ACCESS'] = true;
}
if (false === $arResult['FULL_ACCESS']) {
    $arResult['CAN_GET_FULL_ACCESS'] = true;
}
?>
<div class="side-profile<?if ($arResult['componentPage'] == 'profile_edit_user') {?> hidden<?}?>">
    <div class="side-profile__photo">
        <img alt="user photo" width="110"
             src="<?= (intval($arResult['USER']['PERSONAL_PHOTO']) > 0)
                 ? CFile::GetPath($arResult['USER']['PERSONAL_PHOTO'])
                 : MARKUP . 'i/user_photo.png' ?>">
    </div>

    <?/*details style="position:relative; width: 1000px; z-index: 1; text-align: left;">
        <summary><?= $arResult['USER']['LAST_NAME'] ?></summary>
        <pre><?php print_r($arResult);?>
        </pre>
    </details*/?>

    <div class="side-profile__name"><?= $arResult['USER']['NAME'] . ' '
        . $arResult['USER']['LAST_NAME'] ?></div>
    <? if (!empty($arResult['USER']['UF_RGB_CARD_NUMBER'])) {
        ?>
        <div
            class="side-profile__id"><?= $arResult['USER']['UF_RGB_CARD_NUMBER'] ?></div>
    <?
    }
    if (strpos(
            $arResult['USER']['EMAIL'], $arResult['USER']['UF_RGB_CARD_NUMBER']
        ) === false
    ) {
        ?>
        <div class="side-profile__email"><?= $arResult['USER']['EMAIL'] ?></div>
    <? } ?>
    <div class="side-profile__reg"><?= Loc::getMessage('PROFILE_MU_REGISTER'); ?>
        : <?= ConvertTimeStamp(
            MakeTimeStamp($arResult['USER']['DATE_REGISTER'])
        ) ?>
    </div>    
    <? if ($arResult['USER']['UF_NUM_ECHB']): ?>
        <div class="side-profile__num"><?= Loc::getMessage(
            'PROFILE_MU_NUMBER_LC'
        ); ?>: <?= $arResult['USER']['UF_NUM_ECHB'] ?></div>
    <? endif; ?>
    <div class="side-profile__linkedit"><a href="<?= $arResult['FOLDER']
        . $arResult['URL_TEMPLATES']['profile_edit'] ?>"><?= Loc::getMessage(
                'PROFILE_MU_PROFILE'
            ); ?></a></div>

    <div class="side-profile__my-messages"><a href="<?= str_replace(
        "#USER_ID#", $arResult["USER"]["ID"],
        $arParams["FORUM_USER_ALL_MESSAGES_URL"]
    ) ?>"><?= Loc::getMessage(
            'PROFILE_MU_VIEW_ALL_MESSAGES'
        ); ?></a></div>

</div>