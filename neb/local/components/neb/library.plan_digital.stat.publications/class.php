<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 14.02.2016
 * Time: 15:00
 */

use \Bitrix\Main\Loader;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\UI\PageNavigation;
use \Bitrix\Highloadblock\HighloadBlockTable;
use \Bitrix\Iblock\ElementTable;

/**
 * Class StatisticsPublicationsComponent
 */
class StatisticsPublicationsComponent extends CBitrixComponent
{
    private $arLibraries;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent() {

        $this->includeModules(['iblock', 'highloadblock']);

        $this->getStatuses();
        $this->getDigitizationBooks();
        $this->getLibraries();

        $this->includeComponentTemplate();
    }

    /**
     * Подключение модулей
     *
     * @param $arModules - массив модулей
     * @throws \Bitrix\Main\LoaderException
     */
    private static function includeModules($arModules) {
        foreach ($arModules as $sModule) {
            if (!Loader::includeModule($sModule))
                die('Module ' . $sModule . 'not installed!');
        }
    }

    /**
     * Получение списка статусов
     */
    private function getStatuses() {
        $iHIblock                   = self::getIdBlock(HIBLOCK_CODE_DIGITIZATION_PLAN);
        $this->arResult['STATUSES'] = self::getUserFieldsEnum('HLBLOCK_' . $iHIblock, 'UF_REQUEST_STATUS');
    }

    /**
     * Получение количества книг по планам оцифровкки
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    private function getDigitizationBooks() {

        $arFilter = [];

        if (!empty($_REQUEST['DIGITIZATION']['STATUS']))
            $arFilter['UF_REQUEST_STATUS'] = intval($_REQUEST['DIGITIZATION']['STATUS']);

        if (!empty($_REQUEST['DIGITIZATION']['DATE_FROM']))
            $arFilter['>=UF_DATE_COORDINATION'] = (new DateTime($_REQUEST['DIGITIZATION']['DATE_FROM']));

        if (!empty($_REQUEST['DIGITIZATION']['DATE_TO']))
            $arFilter['<=UF_DATE_COORDINATION'] = (new DateTime($_REQUEST['DIGITIZATION']['DATE_TO']));

        $arQueryParams = [
            'filter' => $arFilter,
            'order'  => ['ID' => 'DESC'],
            'select' => ['ID', 'UF_PLAN_ID', 'UF_LIBRARY', 'UF_REQUEST_STATUS',
                'UF_EXALEAD_BOOK', 'UF_BOOK_AUTHOR', 'UF_BOOK_NAME']
        ];

        $iHIblock         = self::getIdBlock(HIBLOCK_CODE_DIGITIZATION_PLAN);
        $digitizationPlan = self::getDataClassHL($iHIblock);

        // выборка полного количества элементов
        $iCount = $digitizationPlan::getList($arQueryParams)->getSelectedRowsCount();

        // иницирование навигационной цепочки
        $iPageLimit = 100;
        $obPageNavigation = new PageNavigation('books');
        $obPageNavigation->allowAllRecords(true)->initFromUri();
        $obPageNavigation->setRecordCount($iCount)->setPageSize($iPageLimit);
        $this->arResult['NAV'] = $obPageNavigation;

        if (strpos($_SERVER['REQUEST_URI'], 'page-all') === false) {
            $arQueryParams['limit']  = $obPageNavigation->getLimit();
            $arQueryParams['offset'] = $obPageNavigation->getOffset();
        }

        // выборка списка книг
        $digitizationPlanData = $digitizationPlan::getList($arQueryParams);
        while ($arFields = $digitizationPlanData->fetch()) {
            $this->arLibraries[$arFields['UF_LIBRARY']] = $arFields['UF_LIBRARY'];
            $this->arResult['ITEMS'][] = $arFields;
        }
    }

    /**
     * Получение библиотек
     *
     * @throws \Bitrix\Main\ArgumentException
     */
    private function getLibraries() {

        if (empty($this->arLibraries))
            return [];

        $rsElements = ElementTable::getList([
            'order'  => ['NAME' => 'ASC'],
            'select' => ['ID', 'NAME'],
            'filter' => ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ID' => array_values($this->arLibraries), 'ACTIVE' => 'Y'],
        ]);
        while ($arFields = $rsElements->fetch()) {
            $this->arResult['LIBRARIES'][$arFields['ID']] = $arFields['NAME'];
        }
    }


    /** ============================================ СЛУЖЕБНЫЕ МЕТОДЫ =============================================== */

    /**
     * Функция возвращает id HL блока по его коду
     *
     * @param $codeHL - код HL блока
     * @return mixed
     */
    private function getIdBlock($codeHL) {
        $digitizationPlanHLBlock = HighloadBlockTable::getList(['filter' => ['NAME' => $codeHL]])->fetchRaw();
        return $digitizationPlanHLBlock['ID'];
    }

    /**
     * Функция возвращает сущность HL блока
     *
     * @param $iblockId - id HL блока
     * @return \Bitrix\Main\Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    private function getDataClassHL($iblockId) {
        $hlblock   = HighloadBlockTable::getById($iblockId)->fetch();
        $dataClass = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $dataClass;
    }

    /**
     * Получениет значения пользовательского свойства типа 'список'
     *
     * @param $sEntity - сущность
     * @param $sCode - символьный код свойства
     * @return array - массив значений списка
     */
    private static function getUserFieldsEnum($sEntity, $sCode)
    {
        $arResult = [];

        // поиск ID свойства типа 'список'
        $iField = \CUserTypeEntity::GetList(
            ['ID' => 'ASC'],
            ['ENTITY_ID' => $sEntity, 'FIELD_NAME' => $sCode, 'USER_TYPE_ID' => 'enumeration']
        )->Fetch();

        if ($iField <= 0)
            return $arResult;

        // выборка значений свойства типа 'список'
        $obUserFieldEnum  = new \CUserFieldEnum();
        $rsUserFieldEnum = $obUserFieldEnum->GetList(
            ['SORT' => 'ASC'],
            ['USER_FIELD_ID' => $iField]
        );
        while ($arFields = $rsUserFieldEnum->Fetch())
            $arResult[$arFields['ID']] = $arFields;

        return $arResult;
    }
}