<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

global $APPLICATION;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle( Loc::getMessage( "NOTIFICATION_OPERATOR_FORM_SEND" ));

$APPLICATION->IncludeComponent("neb:notification.form", ".default", array());
