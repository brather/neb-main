
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb bbox b-text" id="" value="Станислав Лем">							
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>	
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			
		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-searchresult noborder">
		<ul class="b-profile_nav">
			<li>
				<a href="#" class="b-profile_navlk js_profilemenu">личный кабинет</a>
				<ul class="b-profile_subnav">
					<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
					<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
				</ul>
			</li>
			<li><a class="b-profile_nav_lb" href="#">моя библиотека</a></li>
			<li><a href="#" class="b-profile_nav_qt current">цитаты</a></li>
			<li><a href="#" class="b-profile_nav_bm ">закладки</a></li>
			<li><a href="#" class="b-profile_nav_notes">заметки</a></li>
			
		</ul>                 
	</div><!-- /.b-searchresult-->
	<div class="b-razdel">
			<select class="js_select">
				<option value="-1">Выберите раздел</option>
				<option value="user_profile_new.php">Личный кабинет</option>
				<option value="user_profile_favorites.php" >Моя библиотека</option>
				<option value="user_profile_lk_quo.php"  selected="selected">Цитаты</option>
				<option value="user_profile_lk_bookmark.php">Закладки</option>
				<option value="user_profile_lk_notes.php">Заметки</option>
				<option value="user_profile_lk_searchresult.php">Поисковые запросы</option>
			</select>
		</div>
	<div class="b-mainblock left">

		
		<div class="b-filter">
			<div class="b-filter_wrapper">	
				<a href="#" class="sort sort_opener">Сортировать</a>
				<span class="sort_wrap">
				<a href="#" class="sort up">По автору</a>
				<a href="#" class="sort">По названию</a>
				<a href="#" class="sort">По дате</a>
				</span>
				<span class="b-filter_act">
					
					<span class="b-filter_show">Показать</span>
					<span class="b-filter_num current">10</span>
					<span class="b-filter_num">25</span>
					<span class="b-filter_num">50</span>
				</span>
			</div>
		</div><!-- /.b-filter -->
		<a href="#" class="set_opener iblock right">Настройки</a>
		<div class="b-quote_list" data-action="ajax_action.php">
			<div class="b-quote_item rel removeitem textshow">
				<div class="meta minus">
					<div class="b-hint rel"><span>Удалить</span> из Моих Цитат</div>
					<a class="b-bookadd fav" data-remove="removeonly" href="#"></a>
				</div> <!-- meta -->
				<div class="b-quote">
					<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_28.jpg" alt=""></a>
					<div class="iblock b-quote_txt">
						<div class="b-quotein">
							<img class="hidden b-quoteimg" src="./pic/pic_48.jpg" alt="">
							<p>Театры, концерты, книги – я почти утратил вкус ко всем этим буржуазным привычкам. Они не были в духе времен. Политика была сама по себе в достаточной мере театром, ежевечерняя стрельба заменяла концерты, а огромная книга людской нужды убеждала больше целых библиотек.</p>
							<textarea name="" id="" class="input hidden">Театры, концерты, книги – я почти утратил вкус ко всем этим буржуазным привычкам. Они не были в духе времен. Политика была сама по себе в достаточной мере театром, ежевечерняя стрельба заменяла концерты, а огромная книга людской нужды убеждала больше целых библиотек.</textarea>
						</div>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Эрих Мария Ремарк</a></li>
							<li><span>Книга:</span> <a href="#">Три товарища</a></li>

						</ul>
					</div>
					
				</div><!-- /.b-quote -->
				<div class="clearfix rel b-quote_act">
						<!--<div class="b-quote_copy iblock">
						<a href="#" class="b-quote_copy_label">Скопировать</a>
						<div class="iblock ">
							<a href="#" class="b-openermenu js_openmenu">подпись ГОСТ</a>
							<ul class="b-gostlist"  data-ok-text="Подпись скопирована в буфер обмена" >
								<li data-clipboard-text="Исаев И.А. История государства и права России : учеб. пособие для студ. вузов / И.А. Исаев. - М. : Проспект, 2009. - 336 с."><span>Исаев И.А. История государства и права России : учеб. пособие для студ. вузов / И.А. Исаев. - М. : Проспект, 2009. - 336 с.</span></li>
								<li data-clipboard-text="Писахов С.Г. Сказки Сени Малины : сказки / С. Г. Писахов. - Архангельск : ИПП "Правда Севера", 2009. - 272 с. ил."><span>Писахов С.Г. Сказки Сени Малины : сказки / С. Г. Писахов. - Архангельск : ИПП "Правда Севера", 2009. - 272 с. ил.</span></li>
								<li data-clipboard-text="Холодная война в Арктике / сост. М. Н. Супрун. - Архангельск : [б. и.], 2009. - 379 с."><span>Холодная война в Арктике / сост. М. Н. Супрун. - Архангельск : [б. и.], 2009. - 379 с.</span></li>		
							</ul>
						</div>

					</div> <!-- /.b-quote_copy -->
					<div class="iblock rel b-myselection_list">
						<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs_nomark.html">мои подборки</a>
						<div class="b-favs">										
							<form class="b-selectionadd" action="">
								<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs_nomark.html" value="+">
								<span class="b-selectionaddtxt">cоздать подборку</span>
								<input type="text" class="input hidden">
							</form>
						</div><!-- /.b-favs  -->
						
					</div> <!-- /.b-myselection_list-->
					
					
				</div>
			</div><!-- /.b-quote_item rel -->
			<div class="b-quote_item rel removeitem">
				<div class="meta minus">
					<div class="b-hint rel"><span>Удалить</span> из Моих Цитат</div>
					<a class="b-bookadd fav" data-remove="removeonly" href="#"></a>
				</div> <!-- meta -->
				<div class="b-quote">

					<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_28.jpg" alt=""></a>
					<div class="iblock b-quote_txt">
						<div class="b-quotein">
							<img class="b-quoteimg" width="891" src="./pic/pic_48.jpg" alt="">
							<p class="hidden">Театры, концерты, книги – я почти утратил вкус ко всем этим буржуазным привычкам. Они не были в духе времен. Политика была сама по себе в достаточной мере театром, ежевечерняя стрельба заменяла концерты, а огромная книга людской нужды убеждала больше целых библиотек.</p>
							<textarea name="" id="" class="hidden input">Театры, концерты, книги – я почти утратил вкус ко всем этим буржуазным привычкам. Политика была сама по себе в достаточной мере театром, огромная книга людской нужды убеждала больше целых библиотек.</textarea>
						</div>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Эрих Мария Ремарк</a></li>
							<li><span>Книга:</span> <a href="#">Три товарища</a></li>

						</ul>
					</div>

				</div><!-- /.b-quote -->
				<div class="clearfix rel b-quote_act">

					<div class="iblock rel b-myselection_list">
						<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs_nomark.html">мои подборки</a>
						<div class="b-favs">										
							<form class="b-selectionadd" action="">
								<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs_nomark.html" value="+">
								<span class="b-selectionaddtxt">cоздать подборку</span>
								<input type="text" class="input hidden">
							</form>
						</div><!-- /.b-favs  -->
					</div> <!-- /.b-myselection_list-->


				</div>
			</div><!-- /.b-quote_item rel -->
			<div class="b-quote_item rel removeitem">
				<div class="meta minus">
					<div class="b-hint rel"><span>Удалить</span> из Моих Цитат</div>
					<a class="b-bookadd fav" data-remove="removeonly" href="#"></a>
				</div> <!-- meta -->
				<div class="b-quote">

					<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_28.jpg" alt=""></a>
					<div class="iblock b-quote_txt">
						<div class="b-quotein">
							<img class="hidden b-quoteimg" src="./pic/pic_48.jpg" alt="">
							<p class="hidden">Театры, концерты, книги – я почти утратил вкус ко всем этим буржуазным привычкам. Они не были в духе времен. Политика была сама по себе в достаточной мере театром, ежевечерняя стрельба заменяла концерты, а огромная книга людской нужды убеждала больше целых библиотек.</p>
							<textarea name="" id="" class="input">Театры, концерты, книги – я почти утратил вкус ко всем этим буржуазным привычкам. Политика была сама по себе в достаточной мере театром, огромная книга людской нужды убеждала больше целых библиотек.</textarea>
						</div>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Эрих Мария Ремарк</a></li>
							<li><span>Книга:</span> <a href="#">Три товарища</a></li>

						</ul>
					</div>

				</div><!-- /.b-quote -->
				<div class="clearfix rel b-quote_act">

					<div class="iblock rel b-myselection_list">
						<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs_nomark.html">мои подборки</a>
						<div class="b-favs">										
							<form class="b-selectionadd" action="">
								<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs_nomark.html" value="+">
								<span class="b-selectionaddtxt">cоздать подборку</span>
								<input type="text" class="input hidden">
							</form>
						</div><!-- /.b-favs  -->
					</div> <!-- /.b-myselection_list-->


				</div>
			</div><!-- /.b-quote_item rel -->


		</div><!-- /.b-quote_list -->
		<div class="b-paging">
			<div class="b-paging_cnt">
				<a href="" class="b-paging_prev iblock"></a>
				<a href="" class="b-paging_num current iblock">1</a>
				<a href="" class="b-paging_num iblock">2</a>
				<a href="" class="b-paging_num iblock">3</a>
				<a href="" class="b-paging_num iblock">4</a>
				<a href="" class="b-paging_next iblock"></a>
			</div>
		</div><!-- /.b-paging -->
	</div><!-- /.b-mainblock -->
	<div class="b-side right">
		<div class="b-libfilter">
			<h4>Моя библиотека</h4>
			<!--<div class="b-libfilter_item current"><span class="b-libfilter_num">53</span><a href="#" class="b-libfilter_name">Все книги</a></div>
			<div class="b-libfilter_item "><span class="b-libfilter_num">1</span><a href="#" class="b-libfilter_name">Сейчас читаю</a></div>
			<div class="b-libfilter_item "><span class="b-libfilter_num">21</span><a href="#" class="b-libfilter_name">Прочитал</a></div>-->
			<div class="js_sortable">
				<div class="b-libfilter_item b-libfilter_useritem">
					<span class="b-libfilter_num">1</span>
					<div class="checkwrapper">
						<a href="#" class="black b-libfilter_name">Любимые авторы</a><input class="checkbox" type="checkbox" name="" id="cb11">
					</div>			
				</div>
				<div class="b-libfilter_item current b-libfilter_useritem b-libfilterrow">
					<span class="b-libfilter_num">2</span>
					<div class="checkwrapper">
						<a href="#" class="black b-libfilter_name ">Научно-популярная фантастика</a><input class="checkbox" type="checkbox" name="" id="cb21">
					</div>			
				</div>
				<div class="b-libfilter_item  b-libfilter_useritem">
					<span class="b-libfilter_num">1</span>
					<div class="checkwrapper">
						<a href="#" class="black b-libfilter_name">Ракеты и люди</a><input class="checkbox" type="checkbox" name="" id="cb31">
					</div>			
				</div>
			</div>
			<div class="b-libfilter_action clearfix">
				<a href="#" class="b-libfilter_remove"></a>
				<form action="" class="b-selectionadd">
					<input type="submit" value="+" data-collection="ajax_favs_nomark.html" class="b-selectionaddsign">
					<span class="b-selectionaddtxt ">cоздать подборку</span>
					<input type="text" class="input hidden">
				</form>
			</div>
			<div class="b-removepopup" ><p>Вы действительно хотите удалить коллекцию?</p><a class="formbutton btremove" href="#">Удалить</a><a class="formbutton gray" href="#">Оставить</a></div>

		</div><!-- /.b-side_libfilter -->

		<div class="b-sidenav">
			<a href="#" class="b-sidenav_title">Авторы</a>

			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">Лем С.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">40</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Големский Т. В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">23</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лемин В.В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">2</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лиманн А.Н.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">3</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Голем Л. К.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">15</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>				
					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">тематика</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">1950 - 2000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1900 - 1950</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li><a href="#" class="b_sidenav_contmore">+ следующие</a></li>
				</ul>
			</div>
		</div> <!-- /.b-sidenav -->

	</div><!-- /.b-side -->

</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>
<!--popup Удалить из подборки--><div class="b-removepopup hidden"><p>Удалить цитату из Моих цитат?</p><a href="#" class="formbutton btremove">Удалить</a><a href="#" class="formbutton gray">Оставить</a></div><!--/popup Удалить из подборки-->

</body>
</html>