<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<h3><?=GetMessage('LIBRARY_CHECK_EECHB_GIVEN_DATA');?></h3>
<form action="<?=$APPLICATION->GetCurPage();?>" class="" method="post">
	<input type="hidden" name="sessid" value="<?=bitrix_sessid();?>"/>
	<label for="settings01"><?=GetMessage('LIBRARY_CHECK_EECHB_ELP_NUMBER');?></label>
	<br>
	<input type="text" class="input" name="eechb" data-minlength="4" data-maxlength="30" data-validate="string" data-required="required" id="settings01" value="<?=$arParams['eechb']?>">
    <!-- <em class="error required"><?=GetMessage('LIBRARY_CHECK_EECHB_REQ');?></em>
    <em class="error validate"><?=GetMessage('LIBRARY_CHECK_EECHB_INCORRECT');?></em> -->
    <button class="formbutton btn btn-primary" value="1" type="submit" ><?=GetMessage('LIBRARY_CHECK_EECHB_CHECK');?></button>
</form>

<?
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		if(empty($arResult['ITEMS'])){
		?>
		<div class="b-checkuserfail">
			<h3><?=GetMessage('LIBRARY_CHECK_EECHB_404');?>.</h3>
			<p><?=GetMessage('LIBRARY_CHECK_EECHB_CHECK_CORRECT');?> <a href="/profile/readers/new/"><?=GetMessage('LIBRARY_CHECK_EECHB_CREATE_NEW');?></a>.</p>
		</div>
		<?
		}
		else
		{
		?>
		<h3>Список пользователей</h3>
		<div class="lk-table">
		    <div class="lk-table__column" style="width:5%"></div>
		    <div class="lk-table__column" style="width:35%"></div>
		    <div class="lk-table__column" style="width:20%"></div>
		    <div class="lk-table__column" style="width:20%"></div>
		    <div class="lk-table__column" style="width:10%"></div>
		    <ul class="lk-table__header">
		        <li class="lk-table__header-kind">
		            <a href="#" onclick="return false;" class="sort ">№</a>
		        </li>
		        <li class="lk-table__header-kind">
		            <a href="#" onclick="return false;" class="sort "><?=GetMessage('LIBRARY_CHECK_EECHB_FIO');?></a>
		        </li>
		        <li class="lk-table__header-kind">
		        	<a href="#" onclick="return false;" class="sort "><?=GetMessage('LIBRARY_CHECK_EECHB_ELP_NUMBER_SHORT');?></a>
		        </li>
		        <li class="lk-table__header-kind">
		        	<a href="#" onclick="return false;" class="sort "><?=GetMessage('LIBRARY_CHECK_EECHB_REG_DATE');?></a>
		        </li>
		        <li class="lk-table__header-kind">
		        	<a href="#" onclick="return false;" class="sort "><?=GetMessage('LIBRARY_CHECK_EECHB_STATUS');?></a></a>
		        </li>
		    </ul>
		    <section class="lk-table__body">
				<?
					$i=0;
					foreach($arResult['ITEMS'] as $arItem)
					{
						$i++;
					?>
		            <ul class="lk-table__row">
		                <li class="lk-table__col">
		                    <?=$i?>
		                </li>
		                <li class="lk-table__col">
		                    <?=$arItem['LAST_NAME']?> <?=$arItem['NAME']?> <?=$arItem['SECOND_NAME']?>
		                </li>
		                <li class="lk-table__col">
		                    <?=$arItem['UF_NUM_ECHB']?>
		                </li>
		                <li class="lk-table__col">
		                    <span class="date"><?=ConvertDateTime($arItem['DATE_REGISTER'], "DD.MM.YYY"); ?></span>
		                </li>
		                <li class="lk-table__col">
		                    <span class="status"><?=!empty($arItem['UF_STATUS_NAME']) ? $arItem['UF_STATUS_NAME'] : ''?></span>
		                </li>
		            </ul>
					<?
					}
				?>
		    </section>
		</div>
		<?
		}
	}
?>