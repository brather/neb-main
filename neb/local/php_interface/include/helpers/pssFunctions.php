<?

class pssFunctions{

    /**
     * Метод для удаления старых неоплаченных заказов (убираем активность)
     * @return string
     */
    public static function delete(){

        // удаление старого заказа
        $arFilter = array(
            'IBLOCK_ID' => IBLOCK_PCC_ORDER,
            '<=PROPERTY_FINISHDATE' => date("Y-m-d"),
            '!PROPERTY_FILES_' => false,
            '!PROPERTY_PAID' => 21,
            'ACTIVE' => 'Y'
        );
        
        $arScan_del = \Bitrix\NotaExt\Iblock\Element::getList(
            $arFilter,
            false,
            array(
                'ID',
                'PROPERTY_STARTDATE',
                'PROPERTY_FINISHDATE',
                'PROPERTY_IMAGECOUNT',
                'PROPERTY_FILES_',
                'PROPERTY_PRICE',
                'PROPERTY_PAID',
                'PROPERTY_USERRFID',
                'PROPERTY_PAID_DATE',
                'skip_other'
            ),
            $arOrder
        );

        foreach($arScan_del['ITEMS'] as $scan):

            $el = new CIBlockElement;
            $el->Update($scan["ID"], array("ACTIVE" => "N"));

            $rsUsers = CUser::GetList(($by="id"), ($order="desc"), array('ID' => $scan["PROPERTY_USERRFID_VALUE"]), array('FIELDS'=> array('EMAIL'), array()));
            if ($arUser = $rsUsers->Fetch()){

                // отправка письма
                CEvent::Send("WS_PSS_SALE_DELETE", 's1', array("LOGIN" => $arUser['EMAIL']));
            }
        endforeach;

        // дата окончания заказа заканчивается через 3дня
        $arFilter = array(
            'IBLOCK_ID' => IBLOCK_PCC_ORDER,
            '<=PROPERTY_FINISHDATE' => date("Y-m-d", time() +  3*86400),
            '!PROPERTY_FILES_' => false,
            '!PROPERTY_PAID' => 21,
            'ACTIVE' => 'Y'
        );

        $arScan = Bitrix\NotaExt\Iblock\Element::getList(
            $arFilter,
            false,
            array(
                'ID',
                'PROPERTY_STARTDATE',
                'PROPERTY_FINISHDATE',
                'PROPERTY_IMAGECOUNT',
                'PROPERTY_FILES_',
                'PROPERTY_PRICE',
                'PROPERTY_PAID',
                'PROPERTY_USERRFID',
                'PROPERTY_PAID_DATE',
                'skip_other'
            ),
            $arOrder
        );

        foreach($arScan['ITEMS'] as $scan):

            if ($arUser = $rsUsers->Fetch()){

                // отправка письма
                CEvent::Send("WS_PSS_NOTICE", 's1', array("LOGIN" => $arUser['EMAIL']));
            }
        endforeach;

        return 'pssFunctions::delete();';
    }
}