<?php
$arComponentParameters = array(
    "PARAMETERS" => array(
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => Array("DEFAULT" => 3600),

        "SERVICE_NAME" => array(
            "NAME" => GetMessage("SERVICE_NAME"),
            "TYPE" => "TEXT",
            "DEFAULT" => GetMessage('DEFAULT_NAME'),
        ),
    ),
);