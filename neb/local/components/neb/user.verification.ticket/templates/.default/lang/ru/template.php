<?php
$MESS['USER_VERIFICATION_LOGIN']  = 'E-mail пользователя';
$MESS['USER_VERIFICATION_NUMBER'] = 'Номер читательского билета';
$MESS['USER_VERIFICATION_DATE']   = 'Дата окончания действия читательского билета';
$MESS['USER_VERIFICATION_SAVE']   = 'Сохранить';
