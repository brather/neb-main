<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "PARAMETERS" => array(
        "PARAMS_GALLERY" => Array(
            "NAME" => 'Str parameters gallery',
            "PARENT" => "BASE",
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "PARAMS_ITEMS" => Array(
            "NAME" => 'Str parameters items',
            "PARENT" => "BASE",
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
    )
);
