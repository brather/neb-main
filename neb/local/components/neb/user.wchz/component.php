<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// получить ip ВЧЗ
$arUser = CUser::GetByID($USER->GetID())->Fetch();
if ($arUser['UF_WCHZ'] > 0) {

    // получить ip адреса ВЧЗ
    $db_props = CIBlockElement::GetProperty(IBLOCK_ID_WORKPLACES, $arUser['UF_WCHZ'], array("sort" => "asc"), Array("CODE" => "IP"));

    while ($ar_props = $db_props->Fetch()) {

        if (strpos($ar_props['VALUE'], '-') !== false) {
            $parts = explode('-', $ar_props['VALUE']);
            $left = trim($parts[0]);
            $right = trim($parts[1]);

            $res = preg_match('/(\d{1,3}\.\d{1,3}\.\d{1,3})\.(\d{1,3})/', $right, $tmp);
            $res_1 = preg_match('/\d{1,3}\.\d{1,3}\.\d{1,3}\.(\d{1,3})/', $left, $tmp_1);

            if ($res && $res_1) {
                $end = (int)$tmp[2];
                $start = (int)$tmp_1[1];

                for ($i = $start; $i <= $end; $i++) {
                    $ip[] = $tmp[1] . "." . $i;
                }
            }
        } else {

            $ip[] = $ar_props['VALUE'];
        }
    }

}

// список сессий с полученным ip за последнии 3 часа
$arFilter = array(
    "IP" => implode("|", $ip),
    "DATE_START_1" => FormatDate('d.m.Y H:i:s', time() - 3 * 60 * 60),
);

// получим список записей
$rs = CSession::GetList(
    ($by = "s_date_first"),
    ($order = "asc"),
    $arFilter,
    $is_filtered
);

// выведем все записи
while ($ar = $rs->Fetch()) {
    if ($ar['USER_ID'] > 0) {
        $massUsers[] = $ar['USER_ID'];
    }
}

if (count($massUsers) > 0):
    $massUsers = array_unique($massUsers);

    // пользователи по id
    $filter = Array
    (
        "ACTIVE" => "Y",
        "ID" => implode("|", $massUsers),
    );
endif;

if ($arParams['STATUS']) {


    $filter = Array
    (
        "ACTIVE" => "N",
        "UF_WCHZ" => $arUser['UF_WCHZ'],
    );


}

if ($arParams['FIO']) {
    $words = trim($arParams['FIO']);
    $words = str_replace(' ', "&", $words);
    $filter['NAME'] = $words;
    // если поиск по ФИО то фильтр по всем пользователям
    if ($arParams['STATUS']) unset($filter['UF_WCHZ']);
}
if ($filter):

    $rsUsers = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter, array("SELECT" => array("UF_*")));

    while ($arUser = $rsUsers->Fetch()) {
        if (!in_array(14, CUser::GetUserGroup($arUser['ID']))) {

            $user = new nebUser($arUser['ID']);
            $arUser['ROLE'] = $user->getRole();

            $arResult["USERS"][] = $arUser;
        }
    }
endif;

$this->IncludeComponentTemplate();