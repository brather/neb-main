<?php
/**
 * User: agolodkov
 * Date: 03.06.2015
 * Time: 17:32
 */
/**
 * @var CBitrixComponentTemplate $this
 */
$fieldId = $this->__component->getParent()->arResult['arAnswers']['PUBLICATIONS_LIST'][0]['ID'];
$books = $this->__component->getParent()->arResult['arrVALUES']['form_hidden_'.$fieldId];
if($books) {
    $books = json_decode($books);
} else {
    $books = array();
}
$arResult['CHECKED_BOOKS'] = array_flip($books);