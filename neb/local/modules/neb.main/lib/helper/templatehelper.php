<?php
/**
 * User: agolodkov
 * Date: 22.09.2016
 * Time: 14:02
 */

namespace Neb\Main\Helper;


final class TemplateHelper
{
    /**
     * @var TemplateHelper
     */
    private static $_instance;

    /**
     * @var array
     */
    private $_items = [];

    /**
     * @var array
     */
    private $_itemsMap = [];

    /**
     * @return TemplateHelper
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @param int   $key
     * @param array $maps
     */
    private function _addItemMaps($key, $maps)
    {
        foreach ($maps as $map) {
            if (!isset($this->_itemsMap[$map])) {
                $this->_itemsMap[$map] = [];
            }
            $this->_itemsMap[$map][] = $key;
        }
    }

    /**
     * @param       $path
     * @param array $params
     */
    public function addJs($path, $params = [])
    {
        $this->_items[] = $path;
        $key = count($this->_items) - 1;
        $maps = [
            'js'
        ];
        foreach ($params as $value) {
            if (is_string($value)) {
                $maps[] = $value;
            }
        }
        $this->_addItemMaps($key, $maps);
    }

    /**
     * @param $maps
     *
     * @return mixed
     */
    public function getMappedItemKeys($maps)
    {
        $mapKeys = [];
        foreach ($maps as $map) {
            if (isset($this->_itemsMap[$map])) {
                $mapKeys[] = $this->_itemsMap[$map];
            }
        }
        array_unshift($mapKeys, array_keys($this->_items));

        return call_user_func_array('array_intersect', $mapKeys);
    }

    /**
     * @param array $maps
     *
     * @return string
     */
    public function buildJs($maps = [])
    {
        $maps[] = 'js';
        $itemKeys = $this->getMappedItemKeys($maps);
        $items = [];
        foreach ($itemKeys as $key) {
            if (isset($this->_items[$key])) {
                $item = $this->_items[$key];
                $items[$key] = "<script type=\"text/javascript\" src=\"$item\"></script>";
                unset($this->_items[$key]);
            }
        }

        return implode(PHP_EOL, $items);
    }


}