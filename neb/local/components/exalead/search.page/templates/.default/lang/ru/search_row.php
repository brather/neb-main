<?php

$MESS['PUBLIC_PUBLICATION']     = 'Открытое издание';
$MESS['PROTECTED_PUBLICATINON'] = 'Этот объект охраняется авторским правом';
$MESS['COPYRIGHT_PUBLICATINON'] = 'Издание закрыто по требованию правообладателя';

$MESS['EKBSON_CATALOG_RECORD'] = 'Запись каталога ЭКБСОН';
$MESS['CATALOG_RECORD'] = 'Запись каталога';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_READ'] = 'Читать';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_VIEW'] = 'Смотреть';
$MESS['VIDEO_VIEW'] = 'Смотреть';
$MESS['INCLUDED_IN'] = 'Входит в состав';
$MESS['AUDIO_LISTEN'] = 'Слушать';
$MESS['CATEGORIES'] = 'Рубрики';
$MESS['CATEGORIES_CLOSE'] = 'Скрыть рубрики';
$MESS['KEYWORDS'] = 'Ключевые слова';
$MESS['KEYWORDS_CLOSE'] = 'Скрыть ключевые слова';
$MESS[''] = '';