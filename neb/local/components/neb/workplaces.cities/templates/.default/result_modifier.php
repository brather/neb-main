<?php
/**
 * @var array $arResult
 */
$arResult['top'] = array('г. Москва' => false, 'г. Санкт-Петербург' => false, 'г. Севастополь' => false);

foreach ($arResult['items'] as $key => $item) {
    if (isset($arResult['top'][$item['UF_CITY_NAME']])) {
        $arResult['top'][$item['UF_CITY_NAME']] = $item['ID'];
        unset($arResult['items'][$key]);
    }
}