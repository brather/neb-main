<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Вывод всех билетов библиотеки
 */
$APPLICATION->IncludeComponent(
    'neb:user.verification.list',
    "",
    array(
        "LIST_URL"      => $arParams['SEF_FOLDER'],
        "TICKET_ADD"    => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['ticket_add'],
        "TICKET_EDIT"   => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['ticket_edit'],
        "TICKET_DELETE" => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['ticket_delete'],
        "USER_ADD"      => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['user_add'],
        "USER_DEACT"    => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['user_deact']
    ),
    $component
);