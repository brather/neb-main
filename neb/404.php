<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

global $APPLICATION;
$APPLICATION->SetTitle("Страница не найдена");

?>

<style>
    .p404 {
        height: 200px;
        background: url("<?=MARKUP?>i/page404.jpg") no-repeat center center;
        background-size: cover;
    }
</style>
<section class="innersection innerwrapper clearfix page404">
    <p>Перейдите <a href="/">на главную страницу</a> или воспользуйтесь поиском</p>
    <div class="p404"></div>
</section>

<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");?>