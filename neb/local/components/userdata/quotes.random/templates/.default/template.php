<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<div class="b-random_quote">
	<h2><?=Loc::getMessage('QUOTES_RANDOM_TEMPLATE_RANDOM'); ?></h2>
	<div class="b-quote_item">
	<div class="b-quote">
		<a href="#" class="b-bookboardphoto iblock"><img src="<?=MARKUP?>pic/pic_28.jpg" alt=""></a>
		<div class="iblock b-quote_txt"><p>Театры, концерты, книги – я почти утратил вкус ко всем этим буржуазным привычкам. Они не были в духе времен. Политика была сама по себе в достаточной мере театром, ежевечерняя стрельба заменяла концерты, а огромная книга людской нужды убеждала больше целых библиотек.</p>
		</div>
	</div><!-- /.b-quote -->
	<div class="clearfix rel b-quote_act">
		<div class="b-quote_copy ">
			<a href="#" class="b-quote_copy_label"><?=Loc::getMessage('QUOTES_RANDOM_TEMPLATE_COPY'); ?></a>
			<div class="iblock ">
				<a href="#" class="b-openermenu js_openmenu"><?=Loc::getMessage('QUOTES_RANDOM_TEMPLATE_GOST'); ?></a>
				<ul class="b-gostlist">
					<li><span>Исаев И.А. История государства и права России : учеб. пособие для студ. вузов / И.А. Исаев. - М. : Проспект, 2009. - 336 с.</span></li>
					<li><span>Писахов С.Г. Сказки Сени Малины : сказки / С. Г. Писахов. - Архангельск : ИПП "Правда Севера", 2009. - 272 с. ил.</span></li>
					<li class="current"><span>Холодная война в Арктике / сост. М. Н. Супрун. - Архангельск : [б. и.], 2009. - 379 с.</span></li>		
				</ul>
			</div>
			
		</div>
		<ul class="b-resultbook-info">
			<li><span><?=Loc::getMessage('QUOTES_RANDOM_TEMPLATE_AUTHOR'); ?></span> <a href="#">Эрих Мария Ремарк</a></li>
			<li><span><?=Loc::getMessage('QUOTES_RANDOM_TEMPLATE_BOOK'); ?></span> <a href="#">Три товарища</a></li>

		</ul>
	</div>
	</div>
	<a href="#" class="button_mode"><?=Loc::getMessage('QUOTES_RANDOM_TEMPLATE_GOTO'); ?></a>
</div><!-- /.b-random_quote -->