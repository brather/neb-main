<?php
/**
 * User: agolodkov
 * Date: 12.08.2015
 * Time: 10:01
 */

use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;

/**
 * Class NebLibraryPartyComponent
 */
class StatisticsComponent extends \CBitrixComponent
{

    /**
     * @var array
     */
    protected $_defaultParams
        = array(
            'LIBRARY_ID' => 0,
        );
    /**
     * @var array
     */
    protected $_defaultResult
        = array(
            'grphicData' => array(),
        );


    /**
     * @param $params
     *
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params = array_filter($params);
        $params = array_merge($this->_defaultParams, $params);
        $this->arResult = array_merge($this->_defaultResult, $this->arResult);

        if (!isset($params['SHOW_DAYS'])) {
            $params['SHOW_DAYS'] = \Nota\Collection\LibraryStatTable::DEFAULT_STAT_DAYS;
        }
        if (!isset($params['dateFrom'])) {
            $params['dateFrom'] = ConvertTimeStamp(time() - 86400 * $params['SHOW_DAYS']);
        }
        if (!isset($params['dateTo'])) {
            $params['dateTo'] = ConvertTimeStamp(time());
        }
        if (!isset($params['dateReportFrom'])) {
            $params['dateReportFrom'] = $params['dateFrom'];
        }
        if (!isset($params['dateReportTo'])) {
            $params['dateReportTo'] = $params['dateTo'];
        }

        $yearSeconds = (85400 * 365);
        if ((strtotime($params['dateTo']) - strtotime($params['dateFrom'])) > $yearSeconds) {
            $params['dateFrom'] = ConvertTimeStamp(
                strtotime($params['dateTo']) - ($yearSeconds - 86400)
            );
            /** @todo Убрать ShowErrpr, передавать ошибку в шаблон */
            ShowError('Диапазон не должен быть больше 365 календарных дней');
        }

        $params['LIBRARY_ID'] = (integer)$params['LIBRARY_ID'];

        $params['userToken'] = nebUser::getCurrent()->getToken();
        $params['userToken'] = $params['userToken']['UF_TOKEN'];

        return $params;
    }

    public function executeComponent()
    {
        try {
            if (!Loader::includeModule('iblock')) {
                throw new Exception('Not found module "iblock"');
            }

            $this->loadLibraryStatstics()
                ->loadLibrary()
                ->buildChartsData()
                ->includeComponentTemplate();

            return;

        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

    public function loadLibrary()
    {
        global $APPLICATION;
        $APPLICATION->SetTitle('Статистика НЭБ');
        if ($this->arParams['LIBRARY_ID'] > 0) {
            $arLibrary = CIBlockElement::GetByID($this->arParams['LIBRARY_ID'])->Fetch();
            if (isset($arLibrary['NAME'])) {
                $APPLICATION->SetTitle('Статистика: ' . $arLibrary['NAME']);
            }
        }

        return $this;
    }

    public function buildChartsData()
    {
        $graphs = [
            'neb_feedback_count'         => [
                'title'       => 'Количество обращений',
                'description' => 'Обращения пользователей в обратную связь',
                'library'     => false,
                'date'        => [],
            ],
            'neb_user_registers'         => [
                'title'       => 'Количество регистраций',
                'description' => 'Регистрации пользователей на портале',
                'library'     => false,
                'date'        => [],
            ],
            'neb_user_active'            => [
                'title'       => 'Количество активных пользователей',
                'description' => 'Количество авторизовавшихся пользователей на портале',
                'library'     => false,
                'date'        => [],
            ],
            'neb_book_page_view'         => [
                'title'       => 'Просмотрено страниц',
                'description' => 'Количество просмотров страниц книг',
                'library'     => false,
                'date'        => [],
            ],
            'neb_user_deletions'         => [
                'title'       => 'Удалено читателей',
                'description' => '',
                'library'     => false,
                'date'        => [],
            ],
            'neb_user_active_library'    => [
                'title'       => 'Количество активных читателей',
                'description' => 'Количество авторизовавшихся пользователей привязынных к библиотеке',
                'library'     => true,
                'date'        => [],
            ],
            'neb_library_user_registers' => [
                'title'       => 'Количество зарегистрированных читателей в библиотеке',
                'description' => '',
                'library'     => true,
                'date'        => [],
            ],

            'neb_book_library_page_view' => [
                'title'       => 'Просмотрено страниц',
                'description' => 'Количество просмотров страниц книг пренадлежащих библиотеке',
                'library'     => true,
                'date'        => [],
            ],
            'neb_search_queries'         => [
                'title'       => 'Количество поисковых запросов',
                'description' => 'Поисковые запросы на портале НЭБ',
                'library'     => false,
                'date'        => [],
            ],
            'neb_library_search_queries' => [
                'title'       => 'Количество поисковых запросов по фондам конкретной библиотеки',
                'description' => '',
                'library'     => true,
                'date'        => [],
            ],
            'neb_books_count'            => [
                'title'       => 'Количество изданий',
                'description' => 'Количество изданий в базе данных НЭБ',
                'library'     => false,
                'date'        => [],
            ],
            'neb_library_books_count'    => [
                'title'       => 'Количество изданий',
                'description' => 'Количество изданий принадлежащих библиотеке в базе данных НЭБ',
                'library'     => true,
                'date'        => [],
            ],
            'neb_book_downloads'         => [
                'title'       => 'Количество скачиваний',
                'description' => 'Скачивания книг с портала НЭБ',
                'library'     => null,
                'date'        => [],
            ],
            'neb_book_document_view'     => [
                'title'       => 'Количество просмотров книг',
                'description' => 'Количество просмотренных книг',
                'library'     => null,
                'date'        => [],
            ],
            'neb_library_user_deletions' => [
                'title'       => 'Удалено пользователей',
                'description' => '',
                'library'     => true,
                'date'        => [],
            ],
        ];
        foreach ($graphs as $code => &$graph) {
            $params = [
                'report'   => $code,
                'dateFrom' => date(
                    'Y-m-d', strtotime($this->arParams['dateFrom'])
                ),
                'dateTo'   => date(
                    'Y-m-d', strtotime($this->arParams['dateTo'])
                ),
            ];
            if ($this->arParams['LIBRARY_ID'] > 0) {
                if (false === $graph['library']) {
                    unset($graphs[$code]);
                    continue;
                }
                $params['library_id'] = $this->arParams['LIBRARY_ID'];
            } elseif (true === $graph['library']) {
                unset($graphs[$code]);
                continue;
            }
            $query = http_build_query($params);
            $token = COption::GetOptionString('neb.main', 'stats_reader_token');
            $host = str_replace(
                ['http:', 'http2:'], '',
                COption::GetOptionString('neb.main', 'stats_host')
            );

            $graph['url'] = $host . 'reports?'
                . 'access-token=' . $token . '&' . $query;
            $graph['exportUrl'] = $host . 'exports?'
                . 'access-token=' . $token . '&' . $query . '&periodItem=day&name=' . $graph['title'];
        }

        $this->arResult['charts'] = $graphs;

        return $this;
    }

    /**
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     */
    public function loadLibraryStatstics()
    {
        $rsStatistics = \Nota\Collection\LibraryStatTable::getList(
            array(
                'filter' => array(
                    '>DATE'      => new \Bitrix\Main\Type\Date(
                        $this->arParams['dateFrom']
                    ),
                    '<DATE'      => new \Bitrix\Main\Type\Date(
                        $this->arParams['dateTo']
                    ),
                    'LIBRARY_ID' => $this->arParams['LIBRARY_ID'],
                ),
                'order'  => array(
                    'DATE' => 'ASC'
                ),
            )
        );
        $statFiedls = array(
            'USERS',
            'ACTIVE_USERS',
            'PUBLICATIONS',
            'PUBLICATIONS_DD',
            'SEARCH_COUNT',
            'VIEWS',
            'DOWNLOADS',
            'VIEWS_BOOK',
            'PUBLICATIONS_DIGITIZED',
            'PUBLICATIONS_DIGITIZED_DD',
        );
        if (0 == $this->arParams['LIBRARY_ID']) {
            $statFiedls[] = 'FEEDBACK_COUNT';
        }
        $graphics = array();
        foreach ($statFiedls as $fieldName) {
            $queryParams = array(
                'token'      => $this->arParams['userToken'],
                'exportType' => 'excel',
            );
            if (isset($this->arParams['dateFrom'])) {
                $queryParams['dateFrom'] = $this->arParams['dateFrom'];
            }
            if (isset($this->arParams['dateTo'])) {
                $queryParams['dateTo'] = $this->arParams['dateTo'];
            }
            $graphics[$fieldName] = array(
                'data'  => array(),
                'title' => \Bitrix\Main\Localization\Loc::getMessage(
                    'GRAPHIC_' . $fieldName . '_TITLE'
                ),
            );
            if ('FEEDBACK_COUNT' !== $fieldName) {
                $graphics[$fieldName]['exportUrl']
                    = '/rest_api/library-statistic/export/'
                    . $fieldName . '/'
                    . '?' . http_build_query($queryParams);
            }
        }
        while ($statRow = $rsStatistics->fetch()) {
            /** @var \Bitrix\Main\Type\DateTime $date */
            $date = $statRow['DATE'];
            if (!$date instanceof \Bitrix\Main\Type\DateTime) {
                continue;
            }
            $statRow['DATE'] = date(
                $date->convertFormatToPhp(FORMAT_DATE),
                $date->getTimestamp()
            );
            foreach ($statRow as $fieldName => $value) {
                if (isset($graphics[$fieldName])) {
                    $graphics[$fieldName]['data'][$statRow['DATE']]
                        = (integer)$statRow[$fieldName];
                }
            }
        }

        $widgetParams = array(
            'date'              => date(
                    'Y-m-d', MakeTimeStamp($this->arParams['dateFrom'])
                ) . ','
                . date('Y-m-d', MakeTimeStamp($this->arParams['dateTo'])),
            'moduleToWidgetize' => 'NebStatistics',
        );
        $statParams = array(
            'USERS'                     => array(
                'idGoal'            => 4,
                'actionToWidgetize' => 'getGoals',
                'columns'           => 'nb_conversions',
            ),
            'ACTIVE_USERS'              => array(
                'idGoal'            => 5,
                'actionToWidgetize' => 'getGoals',
                'columns'           => 'nb_conversions',
            ),
            'PUBLICATIONS'              => array(
                'actionType'        => 'books_count',
                'actionToWidgetize' => 'getActionsEvolutionGraph',
                'columns'           => array('neb_books_count'),
            ),
            'PUBLICATIONS_DD'           => array(
                'actionType'        => 'books_dd_count',
                'actionToWidgetize' => 'getActionsEvolutionGraph',
                'columns'           => array('neb_books_count'),
            ),
            'SEARCH_COUNT'              => array(
                'moduleToWidgetize' => 'VisitsSummary',
                'actionToWidgetize' => 'getEvolutionGraph',
                'columns'           => 'nb_visits',
            ),
            'VIEWS'                     => array(
                'idGoal'            => 2,
                'actionToWidgetize' => 'getGoals',
                'columns'           => 'nb_conversions',
            ),
            'DOWNLOADS'                 => array(
                'idGoal'            => 3,
                'actionToWidgetize' => 'getGoals',
                'columns'           => 'nb_conversions',
            ),
            'VIEWS_BOOK'                => array(
                'idGoal'            => 1,
                'actionToWidgetize' => 'getGoals',
                'columns'           => 'nb_conversions',
            ),
            'PUBLICATIONS_DIGITIZED'    => array(
                'actionType'        => 'books_digit_count',
                'actionToWidgetize' => 'getActionsEvolutionGraph',
                'columns'           => array('neb_books_count'),
            ),
            'PUBLICATIONS_DIGITIZED_DD' => array(
                'actionType'        => 'books_digit_dd_count',
                'actionToWidgetize' => 'getActionsEvolutionGraph',
                'columns'           => array('neb_books_count'),
            ),
            'FEEDBACK_COUNT'            => array(
                'idGoal'            => 6,
                'actionToWidgetize' => 'getGoals',
                'columns'           => 'nb_conversions',
            ),
        );
        foreach ($graphics as $graphName => $graphic) {
            if (!isset($statParams[$graphName])) {
                continue;
            }
            $this->arResult['grphicData'][] = $graphic;
        }

        $strSql = "
          SELECT DATE_FORMAT(min(`TIMESTAMP`), '%d.%m.%Y') as `date_begin_stat`
          FROM `neb_stat_book_open`
        ";
        $arDateBegin = Application::getConnection()->query($strSql)->fetch();

        $this->arResult['date_begin_stat'] = $arDateBegin['date_begin_stat'];

        //debugPre($this->arResult);

        return $this;
    }
}