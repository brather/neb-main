<?	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if(empty($arResult['ITEMS']))
    return false;

?>	
<div class="b-bookinfo_popular iblock">
	<div class="b-bookinfo_tit"><h2><?=Loc::getMessage('POPULAR_LIST_TEMPLATE_TITLE'); ?></h2></div>
	<ul class="b_bookpopular">
		<?foreach($arResult['ITEMS'] as $arElement):?>

			<li>
				<div itemscope itemtype="http://schema.org/Book" class="b_popular_descr">
					<h4><a itemprop="name" href="<?=$arElement['LINK']?>" class="popup_opener ajax_opener coverlay" data-width="955"><?=$arElement['TITLE']?></a></h4>

					<?if($arElement['AUTHOR']):?>
						<span class="b-autor"><?=Loc::getMessage('POPULAR_LIST_TEMPLATE_AUTHOR'); ?>:
                            <?foreach ($arElement['AUTHOR_FOR_LINK'] as $key=>$author):?>
                                <a itemprop="author" class="lite" href="/search/?f_field[authorbook]=<?=urlencode($arElement['AUTH_LINK'][str_replace(array(' ','<strong>','</strong>'), '',$author)])?>"><?=$author?></a>
                                <?if ($key+1 < count($arElement['AUTHOR_FOR_LINK'])):?>, <?endif;?>
                            <?endforeach;?>
                        </span>
					<?endif;?>
					<?if($arElement['LIBRARY']):?>
						<div class="b-result_sorce_info"><em><?=Loc::getMessage('POPULAR_LIST_TEMPLATE_SOURCE'); ?>:</em> <a href="<?=$arElement['LIB_LINK']?>" class="b-sorcelibrary"><?=$arElement['LIBRARY']?></a></div>
					<?endif;?>

				</div>
			</li>
		<?endforeach;?>
	</ul>
<?//echo '<pre>'; print_r($arResult); echo '</pre>'?>

</div> <!-- /.b-bookinfo_popular -->