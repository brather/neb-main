<?php
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO'] = 'Найдено 0 документов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND'] = 'Nothing found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SUGGEST'] = 'Возможно вы искали';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'Documents';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'Document';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'Documents';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'] = 'found';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY'] = 'Save the search request';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED'] = 'Search request saved';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE'] = 'Add a title';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SPECIFY_SEARCH'] = 'Specify the search';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE'] = 'Search on the website';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE'] = 'Website';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY'] = 'Libraries';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_EMPTY'] = 'Enter the search expression';
$MESS['LIB_SEARCH_PAGE_SETTINGS'] = 'Settings';

$MESS["RM_BOOK_DIGITIZED"] = "Book digitized";
$MESS["RM_BOOK_DELETE"] = "Remove from plan digitization";
$MESS["RM_BOOK_IN_PLAN"] = "The book is digitized according to plan ";
$MESS["RM_BOOK_ADD_TO_PLAN"] = "Add to plan of digitization";

$MESS["T_LIB_BOOKS_NOT_FOUND"] = "Sorry, nothing found";
$MESS["T_LIB_CHANGE_PARAM_OF_SEARCH"] = "Try changing search criteria.";
$MESS["T_LIB_BOOKS_ON_DIGITIZED"] = "On the digitization";
$MESS["T_LIB_AUTHOR"] = "Author";
$MESS["T_LIB_TITLE_DESCRIPTION"] = "Title / Description";
$MESS["T_LIB_DESCRIPTION"] = "Description";
$MESS["T_LIB_DIGITIZING"] = "Digitizes";
$MESS["T_LIB_TIME_OF_DIGITIZED"] = "  Term of digitizing:";
$MESS["T_LIB_DESC_AUTHOR"] = "Author: ";
$MESS["T_LIB_DESC_TITLE"] = "Title: ";
$MESS["T_LIB_DESC_DATE_OF_PUBLICATION"] = "Date of publication: ";
$MESS["T_LIB_DESC_LIBRARY"] = "Library: ";
$MESS["T_LIB_DESC_PAGES"] = "Pages: ";