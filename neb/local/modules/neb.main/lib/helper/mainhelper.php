<?php
/**
 * User: agolodkov
 * Date: 18.07.2016
 * Time: 19:55
 */

namespace Neb\Main\Helper;

use \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable;

class MainHelper
{
    /**
     * @param $name
     *
     * @throws \Exception
     * @throws \Bitrix\Main\LoaderException
     */
    public static function includeModule($name)
    {
        if (!Loader::includeModule($name)) {
            throw new \Exception('Module "' . $name . '" not installed!');
        }
    }

    /**
     * @param int $hiblockId
     *
     * @return \Bitrix\Main\Entity\DataManager
     * @throws \Exception
     * @throws \Bitrix\Main\SystemException
     */
    public static function getHIblockDataClassName($hiblockId)
    {
        static::includeModule('highloadblock');

        $arHBlock   = HighloadBlockTable::getById($hiblockId)->fetch();
        $data_class = HighloadBlockTable::compileEntity($arHBlock)->getDataClass();

        return $data_class;
    }

    /**
     * @param string $name
     *
     * @return \Bitrix\Main\Entity\DataManager
     * @throws \Exception
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getHIblockDataClassByName($name)
    {
        static::includeModule('highloadblock');

        $arHBlock   = HighloadBlockTable::getList(['filter' => ['NAME' => $name]])->fetch();
        $data_class = HighloadBlockTable::compileEntity($arHBlock)->getDataClass();

        return $data_class;
    }

    /**
     * Автоматически закрывает индексацию сайта поисковыми роботами на любых компьютерах, кроме production и localhost
     * Исполняется не на агентах, а на каждом хите.
     *
     * @return string
     */
    public static function checkRobotsTxt() {

        $sResult = "\\" . __METHOD__ . '();';

        // текущий компьютер - не production и не windows-localhost
        if (defined('SITE_HOST') && isset($_SERVER['SERVER_NAME'], $_SERVER["DOCUMENT_ROOT"])
            && false === stripos($_SERVER['SERVER_NAME'], SITE_HOST)
            && false === stripos($_SERVER['SERVER_NAME'], 'нэб.рф')
            && $_SERVER['SERVER_NAME'] !== filter_var($_SERVER['SERVER_NAME'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)
            && false === stripos($_SERVER["DOCUMENT_ROOT"], '/home/bitrix/www')
            && false === stripos($_SERVER["DOCUMENT_ROOT"], ':')
            && false === stripos($_SERVER["SCRIPT_URI"], 'localhost')
        ) {
            $sFile = $_SERVER["DOCUMENT_ROOT"] . '/robots.txt';
            $sNew  = "User-Agent: *\nDisallow: /";

            $sRobots = file_get_contents($sFile);
            if ($sNew != $sRobots) {

                file_put_contents($sFile, $sNew);

                // с демо-серверов и windows-локалхоста не отправлем сообщения
                if (false === stripos($_SERVER["SERVER_NAME"], 'neb.elar.ru')
                    && false === stripos($_SERVER["DOCUMENT_ROOT"], ':')
                ) {
                    Debug::sendMail(
                        'Сайт заблокирован для индексирования в файле robots.txt',
                        'Адрес сайта: http://' . $_SERVER['SERVER_NAME'] . '/robots.txt'
                        . "\nОшибка сгенерирована в файле:" . __FILE__
                        . "\nИнформация о сервере:" . print_r($_SERVER, 1)
                    );
                }
            }
        }

        return $sResult;
    }

    /**
     * Очищает буфер вывода
     *
     * @param bool $bDisableBuffer - отключение дальнейшей буферизации вывода
     * @param bool $bSendToBrowser - отправка содержимого буффера
     */
    public static function clearBuffer($bSendToBrowser = false, $bDisableBuffer = true) {
        if (!$bSendToBrowser) {
            while (@ob_get_level()) { // Очистка всех уровней буфферов
                if ($bDisableBuffer) {
                    @ob_end_clean(); // Очищает (стирает) буфер вывода и отключает буферизацию вывода
                } else {
                    @ob_clean();     // Очищает (стирает) буфер вывода
                }
            }
        } else {
            if ($bDisableBuffer) {
                @ob_end_flush(); // Сброс (отправка) буфера вывода и отключение буферизации вывода
            } else {
                @ob_flush();     // Сброс (отправка) буфера вывода
            }
        }
    }

    /**
     * Вывод результата в виде JSON-массива
     *
     * @param $arResult
     * @param int $code
     */
    public static function showJson($arResult, $code = 200) {
        self::clearBuffer();
        \CHTTP::SetStatus($code);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($arResult);
        exit;
    }

    /**
     * Определяет необходимость проверки ssl-сертификата при обращении к удаленному сайту
     *
     * * * CURLOPT_SSL_VERIFYHOST — будет ли производиться проверка имени удалённого сервера, указанного в сертификате.
     *     Если установить значение «2», то будет произведена ещё и проверка соответствия имени хоста. (если честно, я так и не понял что делает этот флаг)
     * * * CURLOPT_SSL_VERIFYPEER — если поставить его в 0, то удалённый сервер не будет проверять наш сертификат. В противном случае необходимо этот самый сертификат послать.
     * * * CURLOPT_CAINFO — указывать файл сертификата, если CURLOPT_SSL_VERIFYPEER установлен в 1.
     *
     * @param $sUrl==
     * @return bool
     */
    public static function checkSslVerification($sUrl) {
        return true === SECURITY_SSL_VERIFICATION && false !== mb_stripos($sUrl, 'https://');
    }
}