<?
require(dirname(__FILE__)
    . '/../../../../template-parts/lang/en/rh-common.php');

$MESS['REGISTER_HEADER_ENTITY_FIELDS'] = 'Name of Entity';
$MESS['REGISTER_FIELD_ENTITY_ADDRESS'] = 'Location address';
$MESS['REGISTER_FIELD_ENTITY_ADDRESS_LEGAL'] = 'Legal address';
$MESS['REGISTER_FIELD_ENTITY_ADDRESS_MAIL'] = 'Mail address';
$MESS['REGISTER_FIELD_ENTITY_INN'] = 'INN number of the legal entity';
$MESS['REGISTER_FIELD_ENTITY_KPP'] = 'KPP number of the legal entity';
$MESS['REGISTER_FIELD_ENTITY_OKPO'] = 'OKPO legal entity';
$MESS['REGISTER_FIELD_ENTITY_OGRN'] = 'OGRN legal entity';
$MESS['REGISTER_FIELD_ENTITY_OKATO'] = 'OKATO (OKTMO) legal entity';
$MESS['REGISTER_FIELD_ENTITY_KBK'] = 'KBK legal entity';
$MESS['REGISTER_FIELD_ENTITY_POSITION_HEAD'] = 'Position of the head (the authorized representative)';
$MESS['REGISTER_FIELD_ENTITY_FIO_HEAD'] = 'Name of the head (the authorized representative)';
$MESS['REGISTER_FIELD_ENTITY_CONTACT_INFO'] = 'Name of contact person, the powers of the contact person (Constitution, the power of attorney, etc.).';
$MESS['REGISTER_FIELD_ENTITY_CONTACT_PHONE'] = 'Contact phone';
$MESS['REGISTER_FIELD_ENTITY_CONTACT_MOBILE'] = 'Contact mobile phone';
