<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'       => '/',
                    'VARIABLES' => array(
                        'token',
                        'book_id',
                    ),
                'TYPE'          => array('POST'),
            ),
        ),
    ),
); 