<?php

CBitrixComponent::includeComponentClass('rest:rest.server');

use \Bitrix\Main\Application,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\Loader,
    \Bitrix\Main\UserFieldTable,
    \Bitrix\Highloadblock\HighloadBlockTable;

/**
 * Компонент, возвращающий информацию о библиотеках-участницах НЭБ
 *
 * Class RestLibrary
 */
class RestLibraries extends RestServer
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams()
    {
        return $this;
    }

    /**
     * @return int
     */
    public function getAction()
    {
        $arNebLibs    = self::getNebLibs();
        $arWorkplaces = self::getWorkPlaces($arNebLibs);
        $arUFValues   = self::getUserFieldsValues();

        foreach ($arNebLibs['ITEMS'] as $iLib => $arLib) {
            $arNebLibs['ITEMS'][$iLib]['TYPE']  = $arUFValues['UF_LIBRARY_TYPE'][$arLib['TYPE']];
            $arNebLibs['ITEMS'][$iLib]['SCALE'] = $arUFValues['UF_LIBRARY_SCALE'][$arLib['SCALE']];
            if (!empty($arWorkplaces[$iLib])) {
                $arNebLibs['ITEMS'][$iLib]['WORKPLACES'] = $arWorkplaces[$iLib];
            }
        }

        $arNebLibs['ITEMS'] = array_values($arNebLibs['ITEMS']);

        $this->arResult = $arNebLibs;

        return 200;
    }

    /**
     * Шаг 1: Получение всех NEB_LIB_ID из ЭЧЗ
     *
     * @return array
     */
    private static function getPrevWorkplaces() {

        $arResult = [];

        if (!Loader::includeModule('iblock'))
            return $arResult;

        $rsLibs = CIBlockElement::GetList(
            [],
            ["IBLOCK_ID" => IBLOCK_ID_WORKPLACES, "ACTIVE" => "Y", '!PROPERTY_LIBRARY' => false],
            ['PROPERTY_LIBRARY']
        );
        while ($arItem = $rsLibs->Fetch()) {
            $arResult[] = $arItem['PROPERTY_LIBRARY_VALUE'];
        }

        sort($arResult);

        return $arResult;
    }

    /**
     * Шаг 2: Получение NebLibs
     *
     * @return array
     */
    private static function getNebLibs() {

        $arResult = $arLibs = [];

        $arRequest = static::getRequest();

        if (!Loader::includeModule('highloadblock'))
            return $arResult;

        $arLibs = self::getPrevWorkplaces();

        $arFilter = ['ID' => $arLibs];

        if (!empty($arRequest['date_start'])) {
            $arFilter['>=UF_DATE_MODIFY'] = $arRequest['date_start'];
        }
        if (!empty($arRequest['date_end'])) {
            $arFilter['<=UF_DATE_MODIFY'] = $arRequest['date_end'];
        }

        $libraryHLBlock  = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity   = HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        $arResult['TOTAL'] = $libraryEntity::getList([
            'select' => ['ID'],
            'filter' => $arFilter,
        ])->getSelectedRowsCount();

        $libraryData = $libraryEntity::getList([
            'order'  => ['ID' => 'ASC'],
            'select' => [
                'ID',
                'UF_NAME',
                'UF_FULL_NAME',
                'UF_LIBRARY_TYPE',
                'UF_LIBRARY_SCALE',
                'UF_ADRESS',
                'UF_PHONE',
                'UF_CONTRACT',
                'UF_RGB_ID',
                'UF_POS',
                'UF_RSL_CODE',
                'UF_DATE_MODIFY'
            ],
            'filter' => $arFilter,
            'limit'  => intval($arRequest['limit'])  ? : '',
            'offset' => intval($arRequest['offset']) ? : '',
        ]);
        while ($arItem = $libraryData->fetch()) {
            $arResult['ITEMS'][$arItem['ID']] = [
                'ID'          => $arItem['ID'],
                'NAME'        => trim($arItem['UF_NAME']),
                'FULL_NAME'   => trim($arItem['UF_FULL_NAME']),
                'TYPE'        => $arItem['UF_LIBRARY_TYPE'],
                'SCALE'       => $arItem['UF_LIBRARY_SCALE'],
                'ADDRESS'     => trim($arItem['UF_ADRESS']),
                'PHONE'       => trim($arItem['UF_PHONE']),
                'CONTRACT'    => trim($arItem['UF_CONTRACT']),
                'RGB_ID'      => trim($arItem['UF_RGB_ID']),
                'COORDINATES' => trim($arItem['UF_POS']),
                'RSL_CODE'    => trim($arItem['UF_RSL_CODE']),
                'DATE_MODIFY' => (new DateTime($arItem['UF_DATE_MODIFY']))->toString(),
                'WORKPLACES'  => []
            ];
        }

        return $arResult;
    }

    /**
     * Шаг 3: получение ЭЧЗ по списку Библиотек
     *
     * @param $arNebLibs
     * @return array
     */
    private static function getWorkPlaces($arNebLibs) {

        $arResult = $arWorkplaces = [];

        $rsWorkplaces = CIBlockElement::GetList(
            ['ID' => 'ASC'],
            [
                "IBLOCK_ID"        => IBLOCK_ID_WORKPLACES,
                "ACTIVE"           => "Y",
                'PROPERTY_LIBRARY' => array_keys($arNebLibs['ITEMS'])
            ],
            false,
            false,
            [
                'ID',
                'IBLOCK_ID',
                'NAME',
                'TIMESTAMP_X',
                'PROPERTY_LIBRARY',
                'PROPERTY_FULL_NAME',
                'PROPERTY_IP',
                'PROPERTY_TYPE',
                'PROPERTY_AGREEMENT_NUMBER',
                'PROPERTY_RGB_ID',
                'PROPERTY_IS_CHECK_IP',
                'PROPERTY_IS_CHECK_FINGERPRINT',
            ]
        );
        while ($arItem = $rsWorkplaces->Fetch()) {
            $arWorkplaces[$arItem['ID']] = [
                'ID'                   => $arItem['ID'],
                'NAME'                 => trim($arItem['NAME']),
                'FULL_NAME'            => trim($arItem['PROPERTY_FULL_NAME_VALUE']),
                'NEB_LIB'              => $arItem['PROPERTY_LIBRARY_VALUE'],
                'DATE_MODIFY'          => $arItem['TIMESTAMP_X'],
                'IP'                   => $arItem['PROPERTY_IP_VALUE'],
                'TYPE'                 => $arItem['PROPERTY_TYPE_VALUE'] ? : '',
                'AGREEMENT_NUMBER'     => $arItem['PROPERTY_AGREEMENT_NUMBER_VALUE'],
                'RGB_ID'               => $arItem['PROPERTY_RGB_ID_VALUE'],
                'IS_CHECK_IP'          => $arItem['PROPERTY_IS_CHECK_IP_VALUE'] == 'Y',
                'IS_CHECK_FINGERPRINT' => $arItem['PROPERTY_IS_CHECK_FINGERPRINT_VALUE'] == 'Y',
            ];
        }
        unset($rsWorkplaces, $arItem);

        // получение массива Fingerprint-ов
        $arFingerPrint = self::getFingerprints($arWorkplaces);

        // формирование результата
        foreach ($arWorkplaces as $arItem) {
            $arItem['FINGERPRINTS'] = $arFingerPrint[$arItem['ID']] ? : [];
            $arResult[$arItem['NEB_LIB']][] = $arItem;
        }

        return $arResult;
    }

    /**
     * Шаг 4: получение списка доверенных машин по ЭЧЗ
     *
     * @param $arWorkplaces
     * @return array
     */
    private static function getFingerprints($arWorkplaces) {

        $arResult = [];

        if (empty($arWorkplaces))
            return $arResult;

        $rsFingerprint = CIBlockElement::GetList(
            ['ID' => 'ASC'],
            [
                "IBLOCK_CODE"           => IBLOCK_CODE_TRUSTED_MACHINES,
                "ACTIVE"                => "Y",
                "PROPERTY_WORKPLACE_ID" => array_keys($arWorkplaces)
            ],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'PROPERTY_WORKPLACE_ID', 'PROPERTY_FINGERPRINT']
        );
        while ($arItem = $rsFingerprint->Fetch()) {
            $arResult[$arItem['PROPERTY_WORKPLACE_ID_VALUE']][] = $arItem['PROPERTY_FINGERPRINT_VALUE'];
        }
        unset($rsFingerprint, $arItem);

        return $arResult;
    }

    /**
     * Шаг 5: Получение значений списочных свойств
     *
     * @return array
     */
    private static function getUserFieldsValues() {

        $arResult = $arLibsProps = [];

        $rsFields = UserFieldTable::getList([
            'filter'  => [
                'ENTITY_ID'  => 'HLBLOCK_' . HIBLOCK_LIBRARY,
                'FIELD_NAME' => ["UF_LIBRARY_TYPE", "UF_LIBRARY_SCALE"]
            ],
            'select' => ['ID', 'FIELD_NAME']
        ]);
        while ($arItem = $rsFields->fetch()) {
            $arLibsProps[$arItem['ID']] = $arItem['FIELD_NAME'];
        }

        if (empty($arLibsProps)) {
            return $arLibsProps;
        }

        $obCUserFieldEnum = new CUserFieldEnum();
        $rsValues = $obCUserFieldEnum->GetList(
            ['USER_FIELD_ID' => 'ASC'],
            ['USER_FIELD_ID' => array_keys($arLibsProps)]
        );
        while ($arItem = $rsValues->Fetch()) {
            $arResult[$arLibsProps[$arItem['USER_FIELD_ID']]][$arItem['ID']] = trim($arItem['VALUE']);
        }

        return $arResult;
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    protected static function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }
}