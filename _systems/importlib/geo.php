<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);

	$_SERVER["DOCUMENT_ROOT"] = __DIR__.'../../../neb';
	$_SERVER["DOCUMENT_ROOT"] = '/srv/sites/neb/a.eremichev/neb';

	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	$YKey = 'AOv1PFQBAAAAKk2RdwYATNoqy9yq5kyyJexEqPVIvrsqZmkAAAAAAAAAAAAXAIsBYBVMdoXRsSPIq-KT9bc4ZA==';

	#http://geocode-maps.yandex.ru/1.x/?format=json&key=AOv1PFQBAAAAKk2RdwYATNoqy9yq5kyyJexEqPVIvrsqZmkAAAAAAAAAAAAXAIsBYBVMdoXRsSPIq-KT9bc4ZA==&geocode=694530,%20%D0%A1%D0%B0%D1%85%D0%B0%D0%BB%D0%B8%D0%BD%D1%81%D0%BA%D0%B0%D1%8F%20%D0%BE%D0%B1%D0%BB.,%20%D0%9A%D1%83%D1%80%D0%B8%D0%BB%D1%8C%D1%81%D0%BA%D0%B8%D0%B9%20%D1%80-%D0%BD,%20%D0%B3.%20%D0%9A%D1%83%D1%80%D0%B8%D0%BB%D1%8C%D1%81%D0%BA,%20%D1%83%D0%BB.%20%D0%9D%D0%B0%D0%B1%D0%B5%D1%80%D0%B5%D0%B6%D0%BD%D0%B0%D1%8F,%202

	$url = 'http://geocode-maps.yandex.ru/1.x/?format=json&key='.$YKey.'&geocode=';

	\CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 

	$hlblock = HL\HighloadBlockTable::getById(6)->fetch();
	$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
	$entity_data_class = $entity->getDataClass();

	$rsData = $entity_data_class::getList(array(
		"select" => array("ID", "UF_ADRESS"),
		"filter" => array('=UF_POS' => false, '!UF_ADRESS' => false)
	));

	while($arData = $rsData->Fetch())
	{
		#pre($arData,1);

		$content = file_get_contents($url.urlencode($arData['UF_ADRESS']));
		$arCont = json_decode($content, true);
		$pos = $arCont['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']; 
		$AdministrativeAreaName = $arCont['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['AdministrativeAreaName']; 
		$SubAdministrativeAreaName = $arCont['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['SubAdministrativeAreaName']; 
		$LocalityName = $arCont['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName']; 

		#pre($pos."\n",1);
		#pre($AdministrativeAreaName."\n",1);
		#pre($SubAdministrativeAreaName."\n",1);
		#pre($LocalityName."\n",1);

		$content = file_get_contents($url.urlencode($AdministrativeAreaName));
		$arCont = json_decode($content, true);

		$AdministrativeAreaName2 = $arCont['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['AdministrativeAreaName'];


		#pre($AdministrativeAreaName2."\n",1);

		$arFields = array(
			'UF_ADMINISTRATIVEARE'		=> $AdministrativeAreaName2, 
			'UF_LOCALITY'				=> $LocalityName,
			'UF_AREA'					=> $AdministrativeAreaName,
			'UF_AREA2'					=> $SubAdministrativeAreaName,
			'UF_POS'					=> $pos,

		);

		$entity_data_class::update($arData['ID'], $arFields);


	}

?>