<?php
$MESS['FIND_CLOSEST_LIBRARY'] = 'Selected city '; //Найти ближайшую библиотеку в
$MESS['CHANGE'] = '(change)'; //(изменить)
$MESS['ADDRESS_OR_NAME'] = 'Enter the address or the name of the library'; //Введите адрес или название библиотеки
$MESS['SEARCH_BTN'] = 'Search'; //Найти
$MESS['MEMBER_OF_NEL'] = 'Member of NEB'; //Участник проекта НЭБ<br>
$MESS['VRR'] = 'VRR'; //ВЧЗ
$MESS['ADDRESS'] = 'Address'; //Адрес
$MESS['SHOW_ON_THE_MAP'] = 'Show on the map'; //Показать на карте

