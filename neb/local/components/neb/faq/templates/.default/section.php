<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?= $arResult['BACK_TO_LIST'] ?>
<?$APPLICATION->IncludeComponent(
    "neb:faq.section.edit",
    "",
    array(
        "IBLOCK_ID" => $arResult['IBLOCK_ID'],
        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
        "MIN_COUNT_SYMBOL" => $arParams['MIN_COUNT_SYMBOL'],
        "SEF_FOLDER" => $arParams['SEF_FOLDER'],
        "URL_PAGE" => $arParams['SEF_URL_TEMPLATES']['section']
    ));
?>