<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//var_dump($arResult);

use \Bitrix\Highloadblock\HighloadBlockTable,
	\Bitrix\Main\Page\Asset,
	\Bitrix\Main\Application,
    \Bitrix\Main\Loader;
?>
<!doctype html>
<html class="inmodalhtml" lang="ru">
	<head>
		<meta charset="<?= SITE_CHARSET ?>" />
		<script src="<?= MARKUP ?>js/libs/modernizr.min.js"></script>
		<meta name="viewport" content="width=device-width" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title><? $APPLICATION->ShowTitle() ?></title>
		<script src="/local/templates/adaptive/js/jquery/prejquery.js" type="text/javascript"></script>
		<? $APPLICATION->ShowHead() ?>

		<link rel="icon" href="<?= MARKUP ?>favicon.ico" type="image/x-icon" />
		<?//$APPLICATION->SetAdditionalCSS(MARKUP.'css/style.css');?>
		<?/*Asset::getInstance()->addJs(MARKUP . 'js/libs/jquery.min.js');*/?>
		<?Asset::getInstance()->addJs('/local/templates/.default/js/script.js');?>
		<?/*Asset::getInstance()->addJs(MARKUP . 'js/slick.min.js');*/?>
		<?/*Asset::getInstance()->addJs(MARKUP . 'js/plugins.js');?>
		<?Asset::getInstance()->addJs(MARKUP . 'js/jquery.knob.js');?>
		<?Asset::getInstance()->addJs(MARKUP . 'js/script.js');*/?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<!--script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js" data-start></script-->
		<!--script src="/local/templates/adaptive/js/jquery/jquery-ui-1.10.4.custom.min.js"></script-->
		<? if($APPLICATION->GetCurPage() == '/'): ?>
			<script>
				$(function() {
					$('html').addClass('mainhtml'); /* это для морды */
				}); /* DOM loaded */
			</script>
		<? endif; ?>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
		<script type="text/javascript">VK.init({apiId: 4525156, onlyWidgets: true});</script>
	</head>

	<body class="inmodal">
		<base target="_self">
			<div class="modal-container modal-results">
				<?
				$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
				// Вернем список книг уже находящихся в подборке

				// Подключим классы
				use \Bitrix\Iblock\IblockTable,
					\Bitrix\Iblock\SectionTable,
					\Bitrix\Iblock\ElementTable;

				// Вернем id инфоблока с подборками
				$idBlockCollection = IblockTable::getList(array(
					'filter' => array("CODE" => IBLOCK_CODE_COLLECTIONS),
					'select' => array('ID')
				))->fetch();

				// Вернем подразделы у подборки (если имеются)
				$rsSubSection = SectionTable::getList(array(
					'filter' => array("IBLOCK_ID" => $idBlockCollection['ID'], "IBLOCK_SECTION_ID" => $arRequest['COLLECTION_ID']),
					'select' => array('ID')
				));
				$arSections = array($arRequest['COLLECTION_ID']);
				while($obSubSect = $rsSubSection->fetch()) {
					$arSections[] = $obSubSect['ID'];
				}

				$rsBooksCollection = ElementTable::getList(array(
					'filter' => array("IBLOCK_ID" => $idBlockCollection['ID'], "IBLOCK_SECTION_ID" => $arSections),
					'select' => array('ID')
				));
				$arBooksExalead = array();
				while($obBooks = $rsBooksCollection->fetch()) {
					$rsExalead = CIBlockElement::GetProperty($idBlockCollection['ID'], $obBooks["ID"], array('sort' => 'asc'), array("CODE" => "BOOK_ID"));
					while($obExalead = $rsExalead->GetNext()) {
						$arBooksExalead[] = $obExalead["VALUE"];
					}
				}

				global $navParent;
				$navParent = '_self';
				$requestParams['COLLECTION_ID'] = $arRequest['COLLECTION_ID'];
				$requestParams['ARR_EXALEAD_BOOKS'] = $arBooksExalead;?>
				<?$APPLICATION->IncludeComponent(
					"exalead:search.form",
					"",
					Array(
						"PAGE" => (''),
						"POPUP_VIEW" => 'Y',
						"ACTION_URL" => $APPLICATION->GetCurPage(),
						"REQUEST_PARAMS" => $requestParams,
					)
				);?>
				<?$nebUser = new nebUser();
				$lib = $nebUser->getLibrary();?>
				<?if(!empty($lib['PROPERTY_LIBRARY_LINK_VALUE'])) {
                    Loader::includeModule("highloadblock");
					$hlblock = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
                    $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
                    $arData = $entity_data_class::getById($lib['PROPERTY_LIBRARY_LINK_VALUE'])->fetch();
				}?>
				<?$APPLICATION->IncludeComponent(
					"exalead:search.page",
					"popup_library_collection",
					Array(
						'ID_LIBRARY' => $arData['UF_ID'],
						'COLLECTION_ID' => $arRequest['COLLECTION_ID'],
						'ARR_EXALEAD_BOOKS' => $requestParams['ARR_EXALEAD_BOOKS'],
						'FORM_ACTION' => '/profile/collection/'
					),
					false
				);?>
			</div>
			<link
				href='//fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic,800,800italic,300,300italic&amp;subset=latin,cyrillic-ext,cyrillic'
				rel='stylesheet' type='text/css' />
			<link href="/local/templates/adaptive/css/bootstrap.min.css" rel="stylesheet" />
			<link href="/local/templates/adaptive/vendor/custom-select/bootstrap-select.min.css" rel="stylesheet" />
			<link href="/local/templates/adaptive/css/font-awesome.min.css" rel="stylesheet" />
			<link href="/local/templates/adaptive/vendor/jquery-ui/jquery-ui.css" rel="stylesheet" />
			<link href="/local/templates/adaptive/css/style.css" rel="stylesheet" />

			<!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script>defer$()</script>
			<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script-->
			<!--script src="/local/templates/adaptive/js/neb/jq-select-replace.js"></script-->
			<script src="/local/templates/adaptive/vendor/custom-select/bootstrap-select.min.js"></script>
			<script src="/local/templates/adaptive/js/bootstrap.min.js"></script>
			<script src="/local/templates/adaptive/js/spin.min.js"></script>
			<script src="/local/templates/adaptive/js/script.js"></script>
			<script>
				$('#main_search_form').attr('action',''); /*форма та же что и на сайте, открывается страница в ифрейме*/
			</script>
		</base>
	</body>
</html>