<?php

namespace AssessorBundle;

use AssessorBundle\Command\UpdateQueryFrequency;
use Symfony\Component\Console\Application;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AssessorBundle extends Bundle
{
    /**
     * {@inheritDoc}
     */
    public function registerCommands(Application $application)
    {
        parent::registerCommands($application);
        $application->add(new UpdateQueryFrequency());
    }
}
