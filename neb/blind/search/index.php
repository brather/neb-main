<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
global $APPLICATION;
$APPLICATION->SetTitle('Форма поиска');
?>
<? $APPLICATION->IncludeComponent(
	"exalead:search.form",
	"blind",
	Array(
		"PAGE" => (''),
		"POPUP_VIEW" => 'Y',
		"ACTION_URL" => '/search/',
		"REQUEST_PARAMS" => $requestParams
	)
); ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>