<?php
/**
 * User: agolodkov
 * Date: 04.09.2015
 * Time: 11:18
 */

namespace Neb\Main;


use Bitrix\Main\Entity\DataManager;
use Nota\Exalead\SearchClient;
use Nota\Exalead\SearchQuery;

/**
 * Class UserDigitizationTable
 *
 * @package Neb\Main
 */
class UserDigitizationTable extends DataManager
{
    const DEFAULT_NOTIFY_BATCH = 5;

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'neb_user_digitization';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID'       => array(
                'data_type' => 'integer',
                'primary'   => true,
            ),
            'USER_ID'  => array(
                'data_type' => 'integer',
            ),
            'IS_READY' => array(
                'data_type' => 'integer',
            ),
        );
    }

    public static function notifyUsers()
    {
        if (!\CModule::IncludeModule('nota.exalead')) {
            throw new \Exception('Not installed module "nota.exalead"');
        }
        $queryString
            = <<<SQL
SELECT
  pd.ID              AS ID,
  pd.UF_EXALEAD_BOOK AS BOOK_ID,
  nud.USER_ID        AS USER_ID
FROM neb_user_digitization nud
  JOIN plan_digitization pd ON pd.ID = nud.ID
WHERE nud.IS_READY = 0
LIMIT %d, %d
SQL;
        $offset = 0;
        $connection = static::getEntity()->getConnection();
        $dbResult = $connection->query(
            sprintf(
                $queryString,
                $offset,
                self::DEFAULT_NOTIFY_BATCH
            )
        );
        $readyBooks = array();
        while ($dbResult->getSelectedRowsCount() > 0) {
            while ($item = $dbResult->fetch()) {
                if (empty($item['BOOK_ID'])) {
                    continue;
                }
                $query = new SearchQuery();
                $query->setQuery('id:"' . $item['BOOK_ID'] . '"');
                $client = new SearchClient();
                $book = $client->getResult($query);
                if (!isset($book['ITEMS'][0]['filesize'])
                    || $book['ITEMS'][0]['filesize'] < 1
                ) {
                    continue;
                }
                if (!$readyBooks[$item['USER_ID']]) {
                    $readyBooks[$item['USER_ID']] = array();
                }
                $bookItem = array_intersect_key(
                    $book['ITEMS'][0],
                    array(
                        'id'    => true,
                        'title' => true,
                    )
                );
                $bookItem['planId'] = $item['ID'];
                $readyBooks[$item['USER_ID']][] = $bookItem;
            }
            $offset += self::DEFAULT_NOTIFY_BATCH;
            $dbResult = $connection->query(
                sprintf(
                    $queryString,
                    $offset,
                    self::DEFAULT_NOTIFY_BATCH
                )
            );
        }
        foreach ($readyBooks as $userId => $books) {
            $user = new \nebUser($userId);
            $planIds = array();
            $serverName = \COption::GetOptionString(
                'main', 'server_name', $_SERVER['HTTP_HOST']
            );
            foreach ($books as &$book) {
                $planIds[] = $book['planId'];
                $book
                    = "<a href=\"http://{$serverName}/catalog/{$book['id']}/\">{$book['title']}</a>";
            }
            unset($book);
            $books = implode('<br>', $books);
            $eventFields = array(
                'EMAIL_TO' => $user->GetEmail(),
                'BOOKS'    => $books,
            );
            $messId = \CEvent::Send(
                'BOOK_DIGITIZATION_READY',
                's1',
                $eventFields,
                'Y',
                130
            );
            if ($messId > 0 && count($planIds) > 0) {
                $planIds = array_filter($planIds);
                $connection->query(
                    sprintf(
                        <<<SQL
UPDATE neb_user_digitization
SET IS_READY = 1
WHERE ID IN (%s);
SQL
                        ,
                        implode(',', $planIds)
                    )
                );
            }
        }
        return 'Neb\Main\UserDigitizationTable::notifyUsers();';
    }
}