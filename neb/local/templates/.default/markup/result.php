<?
$pagename='';
?>
<? include("header_main_search.php"); ?>

<section class="innersection innerwrapper clearfix">

	<div class="b-mainblock left">
		
		
		
		<script type="text/javascript">
     
				
			$(function() {
				$('.b-result-docitem .b-result-type_txt').click(function(){
					if(device.mobile()){
						messageOpen('.b-message');
						}else{
					$('.popup_autoreg_form').autoregPopup();
						}
					return false;
				});
			});

			(function() { /*create closure*/
				$.fn.autoregPopup = function() { /*попап регистрации*/

					this.each(function() {
						var popup = $(this).clone(true);
							popup.removeClass('hidden');
							popup.dialog({
								closeOnEscape: true,	                   
								modal: true,
								draggable: false,
								resizable: false,             
								width: 550,
								dialogClass: 'autoreg_popup',
								position: "center",
								open: function(){
									$('.ui-widget-overlay').addClass('black');
									popup.find('.closepopup').click(function() {				
                                        /*Close the dialog*/
                                        popup.dialog("close").remove();
                                        $('.ui-widget-overlay').remove();
                                        return false;
                                    });
								},
								close: function() {
									popup.dialog("close").remove();
								}
							});

							$('.ui-widget-overlay').click(function() {				
								/*Close the dialog*/
								popup.dialog("close");
							});
					});
				}
				/*end of closure*/
			})(jQuery);

		</script>
		<div class="b-filter_items_wrapper">
			<div class="b-result-doc b-result-docfavorite js_sortable ">
				<div class="b-result-docitem iblock removeitem">


					<div class="b-result-docinfo">
						<span class="num">1.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock popup_opener" href="#" ><img class="loadingimg" data-title="Сумма технологии" data-autor="Станислав Лем" alt="" src="./pic/pic_4.jpg"></a>
							
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							
							<div class="b-result-type ">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">2.</span>	
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg"  data-title='Сумма технологии имени А. Ю. Б<strong>Лермонтова</strong>' data-autor="Станислав Лем" alt="" src="./pic/pic_27.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							
							<div class="b-result-type ">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">3.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg"  data-title="Сумма технологии" data-autor="Станислав Лем" src="./pic/pic_29.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">футурологический конгресс</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							
							<div class="b-result-type ">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">4.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" data-title="Сумма технологии" data-autor="Станислав Лем" alt="" src="./pic/pic_30.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							
							<div class="b-result-type ">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">5.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" data-title="Сумма технологии" data-autor="Станислав Лем" alt="" src="./pic/pic_30.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							
							<div class="b-result-type ">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->		
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">6.</span>
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay"  data-width="945" href="ajax_bookview.php"><img class="loadingimg" data-title="Сумма технологии" data-autor="Станислав Лем" alt="" src="./pic/pic_29.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">футурологический конгресс</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							
							
							<div class="b-result-type ">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->	
				<div class="b-result-docmore rel" ><a class="js_ajax_loadresult" href="ajax_fake_result_mode.html"></a></div>
			</div><!-- /.b-result-doc -->
			
			<div class="b-paging">
				<div class="b-paging_cnt">
					<a href="" class="b-paging_prev iblock"></a>
					<a href="" class="b-paging_num current iblock">1</a>
					<a href="" class="b-paging_num iblock">2</a>
					<a href="" class="b-paging_num iblock">3</a>
					<a href="" class="b-paging_num iblock">4</a>
					<a href="" class="b-paging_next iblock"></a>
				</div>
			</div><!-- /.b-paging -->

		</div><!-- /.b-filter_items_wrapper -->
		<div class="b-filter_list_wrapper">
			<div class="b-result-doc">
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay"  data-width="945" href="ajax_bookview.php?id=000000"><img class="loadingimg" data-title="Сумма технологии" data-autor="Станислав Лем"  alt="" src="./pic/pic_4.jpg"></a>
						
						<div class="b-result-doclabel">
							Требуется авторизация
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">1.</span>
						<div class="meta"><div class="b-hint rel" data-plus="Добавить в Мою библиотеку" data-minus="Удалить из Моей библиотеки">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
						<div class="rel plusico_wrap minus">
							<div class="plus_ico plus_digital"></div>
							<div class="b-hint rel" data-plus="Добавить в План оцифровки" data-minus="Удалить из Плана оцифровки">Удалить из Плана оцифровки</div>
						</div>

						<h2><a data-width="955" href="ajax_bookview.php" class=" coverlay popup_opener ajax_opener">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							<div class="b-result-type right">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay"  data-width="945" href="ajax_bookview.php"><img class="loadingimg"  data-title="Сумма технологии" data-autor="Станислав Лем"  src="./pic/pic_27.jpg"></a>

					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">2.</span>
						<div class="meta"><div class="b-hint rel" data-plus="Добавить в Мою библиотеку" data-minus="Удалить из Моей библиотеки">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Межпоселенческая централизованная библиотечная система Никольского муниципального района</a></div>
							<div class="b-result-type right">
								<span class="b-result-type_txt"><!--<a href="spd:http://213.208.168.8:8080/?id=45646545646" onclick="messageOpen(this.href, '.b-message_warning'); return false;">-->Читать<!--</a>--></span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg"  data-title="Сумма технологии" data-autor="Станислав Лем"  src="./pic/pic_4.jpg"></a>

					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">3.</span>
						<div class="meta"><div class="b-hint rel" data-plus="Добавить в Мою библиотеку" data-minus="Удалить из Моей библиотеки">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							<div class="b-result-type right">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" data-title="Сумма технологии" data-autor="Станислав Лем"  src="./pic/pic_27.jpg"></a>
						<div class="b-result-doclabel able">
							Доступно в библиотеке
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">4.</span>
						<div class="meta"><div class="b-hint rel" data-plus="Добавить в Мою библиотеку" data-minus="Удалить из Моей библиотеки">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#"  class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							<div class="b-result-type right">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" data-title="Сумма технологии" data-autor="Станислав Лем"  alt="" src="./pic/pic_4.jpg"></a>

					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">5.</span>
						<div class="meta"><div class="b-hint rel" data-plus="Добавить в Мою библиотеку" data-minus="Удалить из Моей библиотеки">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#"  class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							<div class="b-result-type right">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay"  data-width="945" href="ajax_bookview.php"><img class="loadingimg" data-title="Сумма технологии" data-autor="Станислав Лем"  alt="" src="./pic/pic_27.jpg"></a>

					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">6.</span>
						<div class="meta"><div class="b-hint rel" data-plus="Добавить в Мою библиотеку" data-minus="Удалить из Моей библиотеки">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							<div class="b-result-type right">
								<span class="b-result-type_txt">Читать</span>
								<!--<span class="b-result-type_txt">текст</span>-->
							</div>	
						</div>
					</div>

				</div>
				<div class="b-result-docmore rel" ><a class="js_ajax_loadresult" href="ajax_fake_result.html"></a></div>
				<div class="b-paging">
					<div class="b-paging_cnt">
						<a href="" class="b-paging_prev iblock"></a>
						<a href="" class="b-paging_num current iblock">1</a>
						<a href="" class="b-paging_num iblock">2</a>
						<a href="" class="b-paging_num iblock">3</a>
						<a href="" class="b-paging_num iblock">4</a>
						<a href="" class="b-paging_next iblock"></a>
					</div>
				</div><!-- /.b-paging -->
			</div><!-- /.b-result-doc -->
		</div><!-- /.b-filter_list_wrapper -->
	</div><!-- /.b-mainblock -->

	<div class="b-side right">
		
		<div class="b-sidenav">
			<a href="#" class="b-sidenav_title">Авторы</a>
			
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">Лем С.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">40</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Големский Т. В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">23</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лемин В.В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">2</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лиманн А.Н.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">3</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Голем Л. К.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">15</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>				
					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">дата</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">2000 - 2050</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1950 - 2000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1900 - 1950</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1850 - 1900</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1800 - 1850</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1750 - 1800</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1700 - 1750</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1650 - 1700</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1600 - 1650</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1550 - 1600</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1400 - 1550</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1350 - 1400</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1300 - 1350</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1350 - 1300</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1200 - 1350</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1150 - 1200</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1100 - 1150</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1050 - 1100</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1000 - 1050</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">950 - 1000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li><a href="#" class="b_sidenav_contmore js_moreopen">+ следующие</a></li>
				</ul>
			</div>
			<!--<a href="#" class="b-sidenav_title">Коллекции</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">

					<li class="clearfix">
						<div class="b-sidenav_value left">Коллекции</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">тематика</a>
			<div class="b-sidenav_cont">	
				
				<div class="themesearch">
					<input type="text" class="b-sidenav_tb" placeholder="Поиск по тематике">
					<a href="#" class="b-sidenav_srch">Расширеный список</a>
				</div>
				<ul class="b-sidenav_cont_list b-sidenav_cont_listmore">
					<li class="clearfix">					
						<div class="b-sidenav_value left">Горное дело </div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Пищевые производства</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Радиоэлектроника </div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Строительство </div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Техника и технические 
							науки в целом </div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>
						</li>

						<li><a href="ajax_fake_theme.html" class="b_sidenav_contmore js_ajax_theme" >+ следующие</a></li>
					</ul>
				</div>-->
				<a href="#" class="b-sidenav_title">язык</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">язык</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>
						</li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">Библиотеки</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">Библиотеки</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">издательство</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">издательство</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">место издания</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">место издания</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
				<!--<a href="#" class="b-sidenav_title">объем издания</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">объем издания</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>-->
			</div> <!-- /.b-sidenav --> 

		</div><!-- /.b-side -->

	</section>

</div><!-- /.homepage -->

<div class="b-mylibrary">
	<div class="wrapper">
		<div class="b-mylibrarytit">Моя библиотека</div>
		<ul class="b-mylibrarylist">
			<li>
				<a href="#">Сейчас  читаю</a> <span class="b-num">2</span>
			</li>
			<li><a href="#">В закладках</a><span class="b-num">14</span></li>
			<li><a href="#">Буду читать</a><span class="b-num">47</span></li>
		</ul>
	</div>
</div><!-- /.b-mylibrary -->
<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<div class="selectionBlock">
	<a href="#" class="close"></a>
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu">мои подборки</a>
	

	<form class="b-selectionadd" action="">
		<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
		<span class="b-selectionaddtxt">Cоздать подборку</span>
		<input type="text" class="input hidden">
	</form>
</div><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
<!--popup Удалить из подборки--><div class="b-removepopup hidden"><p>Удалить книгу из Моей библиотеки?</p><a href="#" class="formbutton btremove">Удалить</a><a href="#" class="formbutton gray">Оставить</a></div><!--/popup Удалить из подборки-->
<!--popup доступ к закрытому изданию-->
<div class="popup_autoreg_form hidden">
	
	<a href="#" class="closepopup">Закрыть окно</a>
	<div class="ok tcenter" style="display:none;">
		<p>Вы успешно зарегистрированы как читатель РГБ</p>
		<p><button type="submit" value="1" class="formbutton">Читать</button></p>
	</div>
	
	<form action="" class="b-form b-formautoreg">
	<p>Доступ к закрытому изданию возможен только для читателей РГБ. Если Вы являетесь читателем РГБ, то введите логин и пароль РГБ. Иначе пройдите полную регистрацию.</p>
									<div class="fieldrow nowrap">
										<div class="fieldcell iblock">
											<label for="settings11">Логин</label>
											<div class="field validate">
												<input type="text"  value="" id="settings11"  name="login" class="input">										
											</div>
										</div>

									</div>
									<div class="fieldrow nowrap">

										<div class="fieldcell iblock">
											<label for="settings10">Пароль</label>
											<div class="field validate">
												<input type="password"  value="" id="settings10" name="pass" class="input">									
											</div>
										</div>
											<a href="#" class="formbutton iblock">Зарегистрироваться</a>	
										
									</div>
									<em class="error">Неверный логин или пароль</em>
									<div class="fieldrow nowrap fieldrowaction">
										<div class="fieldcell ">
											<div class="field clearfix">
												<button class="formbutton left" value="1" type="submit">Войти</button>
											</div>
										</div>
									</div>
</form>
	<p class="attention"> Внимание! Для чтения изданий, ограниченных авторским правом, необходимо установить на компьютере <a href="#">программу просмотра.</a></p>
</div><!-- /.popup_autoreg_form -->
<!--/popup доступ к закрытому изданию-->
<!--предупреждение о необходимости доп программы.-->
<div class="b-message_warning hidden">
	<a href="#" class="closepopup">Закрыть окно</a>
	<p>Программа, нужная для просмотра, не установлена.</p>
	<p>Скачайте её здесь:
	<br/>Для Windows: <a target="_blank" href="juniper:open" download="http://нэб.рф/distribs/viewer_windows.rar">http://нэб.рф/distribs/viewer_windows.rar</a>
	<br/>Для IOS: <a target="_blank" href="juniper:open"  download="http://нэб.рф/distribs/viewer_IOS.rar">http://нэб.рф/distribs/viewer_IOS.rar</a>
	<br/>Для Android: <a target="_blank" href="juniper:open"  download="http://нэб.рф/distribs/viewer_Android.rar">http://нэб.рф/distribs/viewer_Android.rar</a>
	</p>
</div>
<!--/предупреждение о необходимости доп программы.-->
<!--сообщение.-->
<div class="b-message hidden">
	<a href="#" class="closepopup">Закрыть окно</a>
	<p>Уважаемый читатель. В связи с изменением регламента доступа к изданиям, защищенным авторским правом, их просмотр возможен только со стационарных компьютеров до момента выхода новой версии НЭБ для мобильных устройств. Приносим Вам свои извинения.</p>
</div>
<!--/!--сообщение.-->

</body>

</html>