<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title></title>
	
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width"/>
	<link rel="icon" href="favicon.ico" type="image/x-icon" />

	<script src="js/libs/modernizr.min.js"></script>

	<link rel="stylesheet" href="css/style.css">
	
	<script src="js/libs/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script>window.jQuery.ui || document.write('<script src="js/libs/jquery.ui.min.js">\x3C/script>')</script>	
	<script src="js/libs/jquery.ui.touch-punch.min.js"></script>

	<script src="js/plugins.js"></script>
		<script src="js/slick.min.js"></script>
	<script src="js/script.js"></script>
	<script>
	$(function() {
        $('html').addClass('mainhtml'); /* это для морды */
    }); /* DOM loaded*/
	</script>
	
</head>
<body class="" >
	<div class="oldie hidden">Вы пользуетесь устаревшей версией браузера. <br/>Не все функции будут работать в нем корректно. <br/>Смените на более новую версию</div>

	<div class="homepage">

		<header id="header" class="mainheader">		

			<div class="wrapper clearfix">
				
				
				<div class="b-headernav_login right">
					<a href="#" class="b-headernav_login_lnk current">Личный кабинет</a>
					<a href="#" class="b-headernav_login_lnk ">Выйти</a> <!-- data-width="550"  --> 
					<div class="b_login_popup popup" >
						<div class="b_login_popup_in bbox">
							<div class="b-login_txt hidden"><a href="#">вход для библиотек</a> <a href="#">для правообладателей</a></div>
							
							
							<div class="b-login_social iblock hidden">
								<h4>через социальные сети</h4>
								<a href="#" class="b-login_slnk vklink">вконтакте</a>
								<a href="#" class="b-login_slnk odnlink">одноклассники</a>
								<a href="#" class="b-login_slnk fblink">facebook</a>
							</div><!-- /.b-login_social -->

						</div>

					</div> 	<!-- /.b_login_popup -->
				</div>

				<div class="b-header_lang right hidden">
					<a href="#" class="en iblock">Eng</a>
					<a href="#" class="ru iblock"></a>
				</div>
				<!--<div class="b-headernav-apps right">
					<a href="#" class="b-headerapps_img"><span>3</span></a>						
					<a href="#" class="b-headerapps_lnk">Приложения для моб. устройств</a>
				</div>-->
				<nav class="b-headernav left">    
					<a href="#collections" class="b-headernav_lnk">Коллекции</a>
					<a href="#" class="b-headernav_lnk current">Библиотеки</a>
					<a href="#" class="b-headernav_lnk">Форум</a>
				</nav>
													<p class="b-passconfirm hidden">На почту выслано письмо с ссылкой на сброс пароля</p>

			</div>	<!-- /.wrapper -->			
			
		</header>