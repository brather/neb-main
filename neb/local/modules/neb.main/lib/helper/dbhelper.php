<?php
/**
 * User: agolodkov
 * Date: 20.09.2016
 * Time: 10:08
 */

namespace Neb\Main\Helper;

use \Bitrix\Main\Application;

class DbHelper
{
    /**
     * @param string $string
     *
     * @return string
     */
    public static function mysqlEscapeString($string)
    {
        if (empty($string)) {
            return $string;
        }

        if (defined('BX_USE_MYSQLI') && true === BX_USE_MYSQLI) {
            $resource = Application::getConnection()->getResource();
            $string = mysqli_real_escape_string($resource, $string);
        } else {
            $string = mysql_real_escape_string($string);
        }

        return $string;
    }

    public static function quotReplace($sStr) {
        return str_replace("'", "\'", $sStr);
    }

    /**
     * Подготовка INSERT-запроса
     *
     * @param $sTable
     * @param $arFields
     * @return string
     */
    public static function getInsertQuery($sTable, $arFields) {

        $sNames = $sValues = '';
        foreach ($arFields as $field => $value)
        {
            $sNames  .= (!empty($sNames) ? ', ' : '') . '`' . $field . '`';
            $sValues .= (!empty($sValues) ? ', ' : '') . (empty($value) ? "''" : "'" . self::quotReplace($value) . "'");
        }

        return "INSERT INTO $sTable ($sNames) VALUES ($sValues)";
    }

    public static function getUpdateQuery($sTable, $arFields, $arWhere) {

        $sSet = $sWhere = '';
        foreach ($arFields as $field => $value) {
            $sSet .= (!empty($sSet) ? ', ' : '') . ("`".$field."` = '".self::quotReplace($value)."'");
        }
        foreach ($arWhere as $field => $value) {
            $sWhere .= (!empty($sWhere) ? ' AND ' : ' WHERE ') . ($field . " = '" . $value."'");
        }

        return "UPDATE $sTable SET $sSet $sWhere";
    }

    /**
     * Получение подключения к БД Битрикса
     *
     * @return \Bitrix\Main\DB\Connection
     */
    public static function getBitrixConnection() {
        return Application::getConnection();
    }

    /**
     * Получение подключения к БД Библиотеки
     *
     * @param bool $bEncode
     * @param bool $bCollate
     * @return \Bitrix\Main\DB\Connection
     */
    public static function getLibraryConnection($bEncode = true, $bCollate = true) {

        $connection = Application::getConnection('library');
        if ($bEncode) {
            $connection->queryExecute('SET NAMES "utf8"');
        }
        if ($bCollate) {
            $connection->queryExecute('SET collation_connection = "utf8_general_ci"');
        }

        return $connection;
    }

    /**
     * Получение к БД Статистики
     *
     * @param bool $bEncode
     * @param bool $bCollate
     * @return \Bitrix\Main\DB\Connection
     */
    public static function getStatsConnection($bEncode = true, $bCollate = true) {

        $connection = Application::getConnection('stats');
        if ($bEncode) {
            $connection->queryExecute('SET NAMES "utf8"');
        }
        if ($bCollate) {
            $connection->queryExecute('SET collation_connection = "utf8_unicode_ci"');
        }

        return $connection;
    }
}