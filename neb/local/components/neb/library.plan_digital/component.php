<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) { die(); }

global $APPLICATION;

// Подключение классов
use \Bitrix\Main\Loader,
    \Bitrix\Main\Entity,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\NotaExt\Iblock\Element,
    \Nota\Exalead\RslViewer,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient,
    \Neb\Main\Helper\MainHelper;

// Подключение модулей
Loader::includeModule('nota.exalead');
Loader::includeModule("highloadblock");

// Зададим преффикс
$preffixInput = '';
if(!empty($arParams['DATA_PLAN']["ID"])) {
    $preffixInput = 'plan-' . $arParams['DATA_PLAN']["ID"] . '-';
}

$user = new \nebUser();
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

$obIBlockElement = new \CIBlockElement();

// Вернем id блока оцифровки по его коду
$idHLBlockDigitization = '';
$connection = Application::getConnection();
$sql = "SELECT ID FROM b_hlblock_entity WHERE NAME='" . HIBLOCK_CODE_DIGITIZATION_PLAN . "';";
$recordset = $connection->query($sql)->fetch();
$idHLBlockDigitization = $recordset['ID'];

// Вернем объект данных блока оцифровки
$hlblock = HighloadBlockTable::getById($idHLBlockDigitization)->fetch();
$entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

// Получим данные по свойству "Статус"
// Возвращаем id самого свойства
$arPropertyStatus = CUserTypeEntity::GetList(
    array("SORT" => "ASC"),
    array("ENTITY_ID" => "HLBLOCK_" . $idHLBlockDigitization, "FIELD_NAME" => "UF_REQUEST_STATUS")
)->Fetch();
$idPropStatus = $arPropertyStatus['ID'];

// Возвращаем массив значений свойства с id
$arPropStatus = array();
if($idPropStatus != '') {
    $obUserFieldEnum = new CUserFieldEnum();
    $rsStatus = $obUserFieldEnum->GetList(['SORT' => 'ASC'], ['USER_FIELD_ID' => $idPropStatus]);
    while ($arStatus = $rsStatus->Fetch()) {
        if($arStatus['VALUE'] == DIGITIZATION_STATUS_ON_AGREEMENT) {
            $idNewStatus = $arStatus['ID'];
        }
        $arPropStatus[$arStatus['ID']] = $arStatus;
    }
}

// Загрузка excel
// Экспорт протокола загрузки
if (isset($arRequest['digitizing-plan']) && $arRequest['digitizing-plan']):
    try {
        MainHelper::clearBuffer();

        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel.php");
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel/IOFactory.php");

        if (!$data = json_decode($arRequest['digitizing-plan'], true)) {
            throw new \Exception(Loc::getMessage('LIB_PLAN_DIG_COMP_DATA_ERROR'));
        }

        $objPHPExcel = new \PHPExcel();

        /* загруженно */
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle(Loc::getMessage('LIB_PLAN_DIG_COMP_LOADED'));

        $row = 1;
        foreach ($data['added'] as $rowcount => $value):
            $objPHPExcel->getActiveSheet()->setCellValue(
                'A' . $row, $value['idFromALIS']
            );
            $objPHPExcel->getActiveSheet()->setCellValue(
                'B' . $row, $value['UF_EXALEAD_NAME']
            );
            $objPHPExcel->getActiveSheet()->setCellValue(
                'C' . $row, $value['AUTHORBOOK']
            );
            $row++;
        endforeach;


        /* уже загружены */
        $objPHPExcel->createSheet(1);
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle(Loc::getMessage('LIB_PLAN_DIG_COMP_DUPLICATION'));

        $row = 1;
        foreach ($data['already'] as $rowcount => $value):
            $objPHPExcel->getActiveSheet()->setCellValue(
                'A' . $row, $value['idFromALIS']
            );
            $objPHPExcel->getActiveSheet()->setCellValue(
                'B' . $row, $value['UF_EXALEAD_NAME']
            );
            $objPHPExcel->getActiveSheet()->setCellValue(
                'C' . $row, $value['AUTHORBOOK']
            );
            $objPHPExcel->getActiveSheet()->setCellValue(
                'D' . $row, $value['LIBRARY']
            );
            $objPHPExcel->getActiveSheet()->setCellValue(
                'E' . $row, $value['UF_DATE_FINISH']
            );
            $row++;
        endforeach;


        /* не найдены */
        $objPHPExcel->createSheet(2);
        $objPHPExcel->setActiveSheetIndex(2);
        $objPHPExcel->getActiveSheet()->setTitle(Loc::getMessage('LIB_PLAN_DIG_COMP_NOT_FOUND_IN_NEL'));

        $row = 1;
        foreach ($data['notfound'] as $rowcount => $value):
            $objPHPExcel->getActiveSheet()->setCellValue(
                'A' . $row, $value['idFromALIS']
            );
            $row++;
        endforeach;

        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . Loc::getMessage('LIB_PLAN_DIG_COMP_DETALIZATION') . '"');
        header('Cache-Control: max-age=0');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: public, must-revalidate');

        $objWriter->save('php://output');
        exit;
    } catch (\Exception $e) {

    }
endif;

// Импорт плана оцифровки
if (isset($arRequest['excel']) || isset($_FILES['excel'])):// а в начале все было так просто.
    try {
        MainHelper::clearBuffer();
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel.php");
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel/IOFactory.php");

        $dt = new DateTime();
        if (!empty($arRequest['plan-date'])) {
            $dtPlus = new DateTime($arRequest['plan-date'], 'd.m.Y');
        } else {
            $dtPlus = new DateTime();
            $dtPlus->add('+' . PLAN_DIGIT_PERIOD_FINISH . ' days');
        }

        $result = array('error' => false, 'error_message' => '');
        $lib = $user->getLibrary();

        $exlaeadLibraryId = $lib['PROPERTY_LIBRARY_LINK_VALUE'];
        $rsAlisList = \Nota\Exalead\BiblioAlisTable::getList(
            ['filter' => ['Library' => $exlaeadLibraryId]]
        );
        $alisList = [];
        while ($alisItem = $rsAlisList->fetch()) {
            $alisList[] = $alisItem;
        }
        if (empty($alisList)) {
            throw new \Exception(Loc::getMessage('LIB_PLAN_DIG_COMP_UNKNOWN_LIBRARY'));
        }
        $viewer = new RslViewer();

        $tmp_name = $_FILES['excel']["tmp_name"];
        $name = $_FILES['excel']['name'];
        $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));
        $name = $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/' . md5($name) . '.' . $ext;
        $res = move_uploaded_file($tmp_name, $name);

        if (!$res) {
            throw new \Exception(Loc::getMessage('LIB_PLAN_DIG_COMP_ERROR_LOAD_FILE'));
        }

        $e = PHPExcel_IOFactory::load($name);

        $objWorksheet = $e->getActiveSheet();

        $i = 0;
        $fileData = array();
        foreach ($objWorksheet->getRowIterator() as $key => $row) {
            if (1 == $key) {
                continue;
            }
            foreach ($row->getCellIterator() as $cell) {
                $cellValue = trim($cell->getValue());
                if (empty($cellValue)) {
                    continue;
                }
                $fileData[$cellValue] = array('idFromALIS' => $cellValue,);
            }
        }

        $booksIds = array();                                                // массив с exalead id изданий
        $arItemsSymbolicId = array();
        foreach ($alisList as $alisItem) {
            foreach($fileData as &$fileDataItem) {
                $fileDataItem['ALIS'] = $alisItem['Id'];
            }
            unset($fileDataItem);
            $symbolicIds = $viewer->getSymbolicIds(array_values($fileData));
            foreach($symbolicIds as $item) {
                if(!empty($item['fullSymbolicId'])) {
                    $arItemsSymbolicId[$item['fullSymbolicId']] = $item;
                    $booksIds[] = $item['fullSymbolicId'];
                } else {
                    $result['notfound'][] = $item;
                }
            }
        }
        if (empty($booksIds)) {
            throw new \Exception(Loc::getMessage('LIB_PLAN_DIG_COMP_ERROR_LOAD_BOOK_INFO'));
        }
        $query = new SearchQuery();
        $books = $query->getByArIds($booksIds);
        $query->setParam('hf', 10000);
        $client = new SearchClient();
        $eBooks = $client->getResult($query);
        $books = array();
        foreach ($eBooks['ITEMS'] as $arExBook) {
            $books[$arExBook['id']] = $arExBook;
        }
        foreach ($booksIds as $r):
            $addData = array();
            $rsData = $entity_data_class::getList(
                array(
                    "select" => array('ID', 'UF_LIBRARY', 'UF_DATE_ADD', 'UF_DATE_FINISH'),
                    "filter" => array('=UF_EXALEAD_BOOK' => $r)
                )
            )->fetch();

            if ($rsData) {
                $key = 'already';
                //уже добавлена
                $addData['UF_DATE_FINISH'] = $rsData['UF_DATE_FINISH']->toString();
                $addData['UF_DATE_ADD'] = $rsData['UF_DATE_ADD']->toString();
                $addData['UF_LIBRARY'] = $rsData['UF_LIBRARY'];
            } else {
                $addData = array(
                    //'UF_LIBRARY' => $lib['PROPERTY_LIBRARY_LINK_VALUE'],
                    'UF_LIBRARY' => $lib['EXTERNAL_ID'],
                    'UF_EXALEAD_BOOK' => $r,
                    'UF_DATE_ADD' => $dt,
                    'UF_DATE_FINISH' => $dtPlus,
                    'UF_EXALEAD_PARENT' => $books[$r]['idparent2'],
                    'UF_COMMENT' => Loc::getMessage('LIB_PLAN_DIG_COMP_ALL_UPLOAD') . $dt->format('d/m/Y'),
                    'UF_NEB_USER' => $user->USER_ID,
                    'UF_BOOK_NAME' => $books[$r]['title'],
                    'UF_BOOK_AUTHOR' => $books[$r]['authorbook'],
                    'UF_ADD_TYPE' => 'library',
                    'UF_REQUEST_STATUS' => $idNewStatus,                            // статус плана на оцифровку
                    'UF_PLAN_ID' => $arRequest['plan-id'],                          // id плана на оцифровку
                    'UF_BOOK_YEAR_PUBLIC' => $books[$r]['publishyear'],             // год издания
                );
                $entity_data_class::add($addData);
                $key = 'added';
            }

            //$addData['UF_EXALEAD_BOOK'] = $r['fullSymbolicId'];
            $addData['UF_EXALEAD_BOOK'] = $r;
            //$addData['idFromALIS'] = $r['idFromALIS'];
            $addData['idFromALIS'] = $arItemsSymbolicId[$r]['idFromALIS'];
            //$addData['LIBRARY'] = $books[$r['fullSymbolicId']]['library'];
            $addData['LIBRARY'] = $lib['NAME'];
            //$addData['AUTHORBOOK'] = $books[$r['fullSymbolicId']]['authorbook'];
            $addData['AUTHORBOOK'] = $books[$r]['authorbook'];
            //$addData['UF_EXALEAD_NAME'] = isset($books[$r['fullSymbolicId']]) ? $books[$r['fullSymbolicId']]['title'] : '';
            $addData['UF_EXALEAD_NAME'] = $books[$r]['title'];
            $result[$key][] = $addData;
        endforeach;
    } catch (\Exception $e) {
        $result['error'] = true;
        $result['error_message'] = $e->getMessage();
    }
    @unlink($name);


    MainHelper::showJson($result);
endif;

// Задание данных массива arParams
// Подствляется в параметр постранички
$arParams['NavNum'] = 1;

// Количество на страницу
$arParams['listLimit'] = 10;

// Размер пачки книг запрашиваемой из экзолида
$arParams['exolidRequestBatch'] = 500;

// Параметры постранички
$arParams['pageNavParam'] = 'PAGEN_' . $arParams['NavNum'];
$arParams['pageNum'] = (integer)isset($arRequest[$arParams['pageNavParam']]) ? $arRequest[$arParams['pageNavParam']] : 1;
$arParams['listOffset'] = ($arParams['pageNum'] - 1) * $arParams['listLimit'];

// Даты
$arParams['dateFrom'] = strtotime($arParams['DATE_FROM']);
$arParams['dateTo'] = strtotime($arParams['DATE_TO']);

if (empty($arParams['DATE_TO'])) {
    if(!empty($arRequest[$preffixInput . 'to'])) {
        $arParams['dateTo'] = strtotime($arRequest[$preffixInput . 'to']);
    } else {
        $arParams['dateTo'] = time();
    }
}
if (empty($arParams['DATE_FROM'])) {
    if(!empty($arRequest[$preffixInput . 'from'])) {
        $arParams['dateFrom'] = strtotime($arRequest[$preffixInput . 'from']);
    }
    //$arParams['dateFrom'] = time() - 86400 * PLAN_DIGIT_PERIOD_FINISH;
}

$arParams['exportMode'] = false;
if ((isset($arRequest[$preffixInput . 'export']) && 'Y' === $arRequest[$preffixInput . 'export'])
    || (isset($arRequest[$preffixInput . 'print']) && 'Y' === $arRequest[$preffixInput . 'print'])) {
    $arParams['exportMode'] = true;
    $arParams['listLimit'] = 0;
    $arParams['listOffset'] = 0;
}


$arResult['dateTo'] = $arParams['dateTo'];
$arResult['dateFrom'] = $arParams['dateFrom'];
CPageOption::SetOptionString("main", "nav_page_in_session", "N");
$by = trim(htmlspecialcharsEx($arRequest['by']));
$order = trim(htmlspecialcharsEx($arRequest['order']));
$q = trim(htmlspecialcharsEx($arRequest['name_q']));

if (empty($by)) {
    global $by1, $order1;
    $by1 = trim(htmlspecialcharsEx($by1));
    $order1 = trim(htmlspecialcharsEx($order1));
}

$by1 = empty($by1) ? 'UF_DATE_ADD' : $by1;
$order1 = empty($order1) ? 'desc' : $order1;
$arOrder = array($by1 => $order1);

// Значение фильтра по статусу
// Получим id свойства статуса
$rsDataPropStatus = CUserTypeEntity::GetList(
    array("SORT" => "ASC"),
    array("ENTITY_ID" => "HLBLOCK_" . $idHLBlockDigitization, "FIELD_NAME" => "UF_REQUEST_STATUS")
);
$idPropStatus = '';
if($arResPropStatus = $rsDataPropStatus->Fetch()) {
    $idPropStatus = $arResPropStatus["ID"];
}

// Возвращаем массив значений свойства с id
$idStatusCanceled = ''; // id издания которое было отменено администратором
$arPropStatus = array();
if($idPropStatus != '') {
    $obUserFieldEnum = new CUserFieldEnum();
    $rsStatus = $obUserFieldEnum->GetList(['SORT' => 'ASC'], ['USER_FIELD_ID' => $idPropStatus]);
    while ($arStatus = $rsStatus->Fetch()) {
        if($arStatus['VALUE'] == DIGITIZATION_STATUS_CANCELED) {
            $idStatusCanceled = $arStatus['ID'];
        }
        $arPropStatus[$arStatus['ID']] = $arStatus;
    }
}
$arResult["PROP_LIST_STATUS"] = $arPropStatus;

// Устанавливаем значения самого фильтра
$filter = array('!UF_REQUEST_STATUS' => $idStatusCanceled);

// Фильтр по плану
if (!empty($arParams["DATA_PLAN"]["ID"])) {
    $filter["UF_PLAN_ID"] = $arParams["DATA_PLAN"]["ID"];
}

// фильтр по библиотеке
if ($arParams['LIBRARY_ID'] > 0) {
    $filter['UF_LIBRARY'] = $arParams['LIBRARY_ID'];
}

// Фильтр по пользователю
if(!empty($arRequest[$preffixInput . 'NEB_USER'])) {
    if($arRequest[$preffixInput . 'NEB_USER'] != 'ALL') {
        if($arRequest[$preffixInput . 'NEB_USER'] == 'ADMIN') {
            $filter['UF_NEB_USER'] = array(intval($user->USER_ID));
        } elseif($arRequest[$preffixInput . 'NEB_USER'] == 'READER') {
            $filter['!UF_NEB_USER'] = array(intval($user->USER_ID));
        }
    }
}

// Фильтр по диапазону дат
if (false !== $arParams['dateFrom']) {
    $filter['>=UF_DATE_ADD'] = date('d.m.Y', $arParams['dateFrom']);
}
if (false !== $arParams['dateTo']) {
    $filter['<=UF_DATE_ADD'] = date('d.m.Y', $arParams['dateTo'] + 86400);
}

// Количество записей для постранички
$cache = new CPHPCache();
$cacheTime = 3600;
$cacheId = md5(serialize($filter));
$cachePath = '/neb/library.plan_digital/';

$totalCount = $entity_data_class::getList(
    array(
        'select'  => array('count'),
        'runtime' => array(new Entity\ExpressionField('count', 'COUNT(*)')),
        'filter'  => $filter,
    )
)->fetch();

if (isset($totalCount['count'])) {
    $totalCount = (integer)$totalCount['count'];
    $cache->StartDataCache($cacheTime, $cacheId, $cachePath);
    $cache->EndDataCache(array('count' => $totalCount));
} else {
    $totalCount = 0;
}

$rsData = $entity_data_class::getList(
    array(
        "runtime" => array(
            "other"   => array(
                "data_type" => $entity_data_class,
                'reference' => array('=this.UF_EXALEAD_BOOK' => 'ref.UF_EXALEAD_BOOK'),
                #, '!=this.UF_LIBRARY' => 'ref.UF_LIBRARY'
            ),
            'library' => array(
                "data_type" => "Bitrix\Iblock\ElementTable",
                'reference' => array('=this.PLAN_DIGITIZATION_other_UF_LIBRARY' => 'ref.ID'),
            )
        ),
        "select"  => array(
            "ID",
            'UF_EXALEAD_BOOK',
            'UF_DATE_ADD',
            'UF_REQUEST_STATUS',
            'UF_EXALEAD_PARENT',
            'UF_DATE_FINISH',
            'UF_LIBRARY',
            'UF_COMMENT',
            'UF_BOOK_YEAR_PUBLIC',
            'UF_BOOK_NAME',
            'UF_BOOK_AUTHOR',
            'other.UF_LIBRARY',
            'other.UF_EXALEAD_BOOK',
            'other.UF_DATE_FINISH',
            'library.ID',
            'library.NAME',
            'other.UF_COMMENT'),
        "order"   => $arOrder,
        "filter"  => $filter,
        "limit"   => $arParams['listLimit'],
        "offset"  => $arParams['listOffset'],
    )
);

$arBooks = array();
$arBooksOther = array();
$arBooksID = array();
while ($arData = $rsData->Fetch()) {
    if ($arData['UF_LIBRARY'] != $arData['PLAN_DIGITIZATION_other_UF_LIBRARY']) {
        $date = '';
        if (!is_object($arData['PLAN_DIGITIZATION_other_UF_DATE_FINISH'])) {
            $date = $arData['PLAN_DIGITIZATION_other_UF_DATE_FINISH'];
        } else {
            $date = $arData['PLAN_DIGITIZATION_other_UF_DATE_FINISH']->toString();
        }
        $arBooksOther[$arData['UF_EXALEAD_BOOK']][]
            = array('NAME' => $arData['PLAN_DIGITIZATION_library_NAME'],
                    'DATE' => $date);
    }
    if(empty($arData['UF_BOOK_NAME'])) {
        $query = new SearchQuery('id:' . $arData['UF_EXALEAD_BOOK']);
        //$query->getById($arData['UF_EXALEAD_BOOK']);
        $client = new SearchClient();
        $result = $client->getResult($query);
        if(!empty($result['title'])) {
            $arData['UF_BOOK_NAME'] = $result['title'];
        }
    }
    if(!is_object($arData['UF_DATE_FINISH'])) {
        $ufDateFinish = $arData['UF_DATE_FINISH'];
    } else {
        $ufDateFinish = $arData['UF_DATE_FINISH']->toString();
    }
    $arBook = [
        'ID'                    => $arData['ID'],
        'LIBRARY_ID'            => $arData['UF_LIBRARY'],
        'BOOK_ID'               => $arData['UF_EXALEAD_BOOK'],
        'DATE_ADD'              => $arData['UF_DATE_ADD']->format('d.m.Y'),
        'DATE_FINISH'           => $ufDateFinish,
        'UF_REQUEST_STATUS'     => $arData['UF_REQUEST_STATUS'],
        'UF_EXALEAD_PARENT'     => $arData['UF_EXALEAD_PARENT'],
        'UF_BOOK_YEAR_PUBLIC'   => $arData['UF_BOOK_YEAR_PUBLIC'],
        'UF_BOOK_NAME'          => $arData['UF_BOOK_NAME'],
        'UF_BOOK_AUTHOR'        => $arData['UF_BOOK_AUTHOR'],
        'COMMENT'               => htmlspecialchars($arData['UF_COMMENT']),
    ];

    $arRess[] = $arBook;
}

// заполняется массив для постраничной навигации
$arRess = array_pad($arRess, -(count($arRess) + $arParams['listOffset']), true);
$arRess = array_pad($arRess, $totalCount, true);

// постраничный вывод
$rs = new CDBResult();
$rs->InitFromArray($arRess);
if (false === $arParams['exportMode']) {
    $rs->NavNum = $arParams['NavNum'];
    $rs->NavStart($arParams['listLimit']);
    $arResult["STR_NAV"] = $rs->GetPageNavStringEx($navComponentObject, "", "");
}


$arResult['NAV_COUNT'] = $totalCount;
$minDateAdd = 0;
while ($arRes = $rs->Fetch()) {
    if($minDateAdd == 0 || $minDateAdd > MakeTimeStamp($arRes["DATE_ADD"])) {
        $minDateAdd = MakeTimeStamp($arRes["DATE_ADD"]);
    }
    //$arResult['ITEMS'][] = $arRes;
    $arBooksID[] = $arRes['BOOK_ID'];
    $arResult['BOOKS'][$arRes['BOOK_ID']] = $arRes;
}

if(!empty($arRequest[$preffixInput . 'from'])) {
    $arResult['dateFrom'] = $arRequest[$preffixInput . 'from'];
} else {
    $arResult['dateFrom'] = ConvertTimeStamp($minDateAdd, "SHORT", "ru");
}

if (!Loader::includeModule('nota.exalead')) {
    return false;
}


// Запрашиваем книги пачками
$booksCount = count($arBooksID);
for (
    $offset = 0; $offset < $totalCount;
    $offset += $arParams['exolidRequestBatch']
) {
    $books = array_slice($arBooksID, $offset, $arParams['exolidRequestBatch']);
    if (!empty($books)) {
        $query = new SearchQuery();

        $query->getByArIds($books);
        if (!empty($q)) {
            $qp = $query->getParameter('q');
            $query->setParam('q', '(' . $qp . ') AND ("' . $q . '")');
        }

        # сортировка
        if (!empty($by) and !empty($order)) {
            $query->setParam('s', $order . '(' . $by . ')');
            unset($by1, $order1);
        }

        // Уточнение
        if (!empty($arRequest['f_field'])) {
            $f_field = $arRequest['f_field'];
            if (!is_array($f_field)) {
                $f_field = array($f_field);
            }

            foreach ($f_field as $k => $v) {
                $v = urldecode($v);
                $v = trim($v);
                $query->setParam('r', '+' . $v, true);
            }
        }

        $query->setPageLimit(10000);

        $client = new SearchClient();
        $resEx = $client->getResult($query);

        foreach ($resEx['ITEMS'] as $item) {

            $arResult['ITEMS'][$item['id']] = $item;
            if(!empty($arResult['BOOKS'][$item['id']])) {
                if(empty($arResult['BOOKS'][$item['id']]['UF_BOOK_NAME']) && !empty($item['title'])) {
                    $arResult['BOOKS'][$item['id']]['UF_BOOK_NAME'] = $item['title'];
                    $entity_data_class::update($arResult['BOOKS'][$item['id']]['ID'], array('UF_BOOK_NAME' => $item['title']));
                }
                if(empty($arResult['BOOKS'][$item['id']]['UF_BOOK_AUTHOR']) && !empty($item['authorbook'])) {
                    $arResult['BOOKS'][$item['id']]['UF_BOOK_AUTHOR'] = $item['authorbook'];
                    $entity_data_class::update($arResult['BOOKS'][$item['id']]['ID'],  array('UF_BOOK_AUTHOR' => $item['authorbook']));
                }
            }
        }
    }
}

// сохранение стоимости скана
if ($arRequest) {
    $obIBlockElement->SetPropertyValues(
        $arParams['LIBRARY_ID'], IBLOCK_ID_LIBRARY,
        ($arRequest[$preffixInput . 'scan_cost_1'] > 0 ? $arRequest[$preffixInput . 'scan_cost_1'] : 0) . "."
        . ($arRequest[$preffixInput . 'scan_cost_2'] > 0 ? $arRequest[$preffixInput . 'scan_cost_2'] : 0), 'SCAN_COST'
    );
}

//получение стоимости одного скана
$db_props = $obIBlockElement->GetProperty(
    IBLOCK_ID_LIBRARY, $arParams['LIBRARY_ID'], "sort", "asc",
    Array("CODE" => "SCAN_COST")
);
if ($ob = $db_props->GetNext()) {

    $scan_cost = explode(".", $ob['VALUE']);
    $arResult['SCAN_COST_1'] = $scan_cost[0];
    $arResult['SCAN_COST_2'] = $scan_cost[1];
}


$arResult['BOOKS_OTHER'] = $arBooksOther;

if (!empty($arRequest['dop_filter']) && $arRequest['dop_filter'] == 'Y'
    && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && !$arRequest['digitization_plan_update']) {
    $APPLICATION->RestartBuffer();
}

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && empty($arRequest['dop_filter'])) {
    $arParams['ajax'] = true;
}

$template = '';
if (!empty($arRequest[$preffixInput . 'export'])) {
    $template = 'export';
} elseif (!empty($arRequest[$preffixInput . 'print'])) {
    $template = 'print';
}

$this->IncludeComponentTemplate($template);

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && !$arRequest['digitization_plan_update']) {
    exit();
}

$APPLICATION->SetTitle(Loc::getMessage('LIB_PLAN_DIG_COMP_FOUNDS_FOR_DIGITIZATION'));?>