<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 10.05.2017
 * Time: 15:00
 */

namespace Neb\Main;

use \Bitrix\Main\Entity\DataManager;
use \Bitrix\Main\Entity\Validator\Length;

/**
 * Class SearchLanguagesTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> NAME string(1024) mandatory
 * <li> COUNT int optional
 * <li> SORT int optional default 500
 * </ul>
 *
 * @package Bitrix\Search
 **/

class SearchLanguagesTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'neb_search_languages';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'NAME' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateName'),
            ),
            'NAME_EN' => array(
                'data_type' => 'string',
                'required' => false,
                'validation' => array(__CLASS__, 'validateName'),
            ),
            'COUNT' => array(
                'data_type' => 'integer',
            ),
            'ACTIVE' => array(
                'data_type' => 'boolean',
                'values' => array('N', 'Y'),
            ),
            'SORT' => array(
                'data_type' => 'integer',
            ),
        );
    }
    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return array(
            new Length(null, 1024),
        );
    }
}