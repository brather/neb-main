<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title></title>
	
	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />

	<script src="../js/libs/modernizr.min.js"></script>

	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/low_vision.css">

	
	<script src="../js/libs/jquery.min.js"></script>
	
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>	
	<script>window.jQuery.ui || document.write('<script src="../js/libs/jquery.ui.min.js">\x3C/script>')</script>
	<script src="../js/plugins.js"></script>
	<script src="../js/slick.min.js"></script>
	<script src="../js/jquery.cookie.js"></script>
	<script src="../js/blind.js"></script>
	<script src="../js/script.js"></script>
	<script>
		$(function() {
				$('html').addClass('mainhtml'); /* это для морды */
			}); /* DOM loaded */
	</script>
	
</head>
<body class="blind ">
	<div class="oldie hidden">Вы пользуетесь устаревшей версией браузера. <br/>Не все функции будут работать в нем корректно. <br/>Смените на более новую версию</div>
	<div class="homepage">
	<div class="blindmenu">
		<div class="access wrapper">
					
					<dl class="a-fontsize">
						<dt>Размер шрифта:</dt>
						<dd><a class="a-fontsize-small" rel="fontsize-small" href="#"></a></dd>
						<dd><a class="a-fontsize-normal" href="#" rel="fontsize-normal"></a></dd>
						<dd><a class="a-fontsize-big" rel="fontsize-big" href="#"></a></dd>
					</dl>
					<dl class="a-colors">
						<dt>Цвета сайта</dt>
						<dd><a class="a-color1" rel="color1" href="#"></a></dd>
						<dd><a class="a-color2" rel="color2" href="#"></a></dd>
						<dd><a class="a-color3" rel="color3" href="#"></a></dd>
					</dl>		
					<dl class="a-images">
						<dt>Изображения</dt>
						<dd><a class="a-imagesoff" href="#" rel="imagesoff"></a></dd>
					</dl>
					<p class="a-search"><a href="blind_find_e.php" title="поиск">Поиск</a></p>
					<p class="a-settings"><a href="#">Настройки</a></p>

					<div class="popped">
						<h2>Настройки шрифта:</h2>
						
						<p class="choose-font-family">Выберите шрифт <a href="#" rel="sans-serif" id="sans-serif" class="font-family">Arial</a> <a href="#" id="serif" rel="serif" class="font-family">Times New Roman</a></p>
						<p class="choose-letter-spacing">Интервал между буквами <span>(Кернинг)</span>: <a href="#" rel="spacing-small" id="spacing-small" class="letter-spacing">Маленький</a> <a href="#" id="spacing-normal" class="letter-spacing" rel="spacing-normal">Стандартный</a> <a href="#" id="spacing-big" class="letter-spacing" rel="spacing-big">Большой</a></p>
						
						<h2>Выбор цветовой схемы:</h2>
						<ul class="choose-colors">
							<li id="color1"><a href="#" rel="color1"><span>&mdash;</span>Черным по белому</a></li>
							<li id="color2"><a href="#" rel="color2"><span>&mdash;</span>Белым по черному</a></li>
							<li id="color3"><a href="#" rel="color3"><span>&mdash;</span>Темно-синим по голубому</a></li>
							<li id="color4"><a href="#" rel="color4"><span>&mdash;</span>Коричневым по бежевому</a></li>
							<li id="color5"><a href="#" rel="color5"><span>&mdash;</span>Зеленым по темно-коричневому</a></li>
						</ul>
						<p class="saveit"><a href="#" class="closepopped"><span>Закрыть панель</span></a> <a href="#" class="default"><span>Вернуть стандартные настройки</span></a></p>
					</div>

				</div>
	</div> 
		
		<header id="header" class="mainheader">		

			<div class="wrapper clearfix">

				
				<div class="b-headernav_login right">
					<a href="blind_reg_choice.php" class="b-headernav_login_lnk" title="Регистрация">Регистрация</a>
					<a href="blind_singin.php" class="b-headernav_login_lnk" title="Войти">Войти</a> 
				</div>
				<div class="b-header_lang right"><a title="Eng" class="en iblock" href="/?setlang=en">Eng</a><a class="ru iblock" href="/special/"></a></div>	
				<nav class="b-headernav left">    
					<a href="blind_libs.php" class="b-headernav_lnk current"  title="Библиотеки">Библиотеки</a>
					<a class="b-headernav_lnk" href="#">Библиотеки</a>
					<a class="b-headernav_lnk" href="#">Форум</a>
					<a class="b-headernav_lnk" href="#">О ПРОЕКТЕ</a>				
				</nav>
				<span class="testmode mBot">Портал работает в тестовом режиме. В случае обнаружения технических проблем или некорректных данных - <a href="http://нэб.рф/feedback/">сообщите нам</a>.</span>
				<span class="testmode no_PTop"> Внимание! Для чтения изданий, ограниченных авторским правом, необходимо установить на компьютере <a href="http://нэб.рф/distribs/NEB_0.0.14.msi" target="_blank">программу просмотра</a>.</span>
			</div>	<!-- /.wrapper -->			
			
		</header>