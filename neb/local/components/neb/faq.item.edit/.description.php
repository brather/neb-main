<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    "NAME" => Loc::getMessage('FAQ_ITEM_EDIT_NAME'),
    "DESCRIPTION" => Loc::getMessage('FAQ_ITEM_EDIT_DESC'),
    "ICON" => "/images/icon.gif",
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "NEB"
    ),
    "COMPLEX" => "N",
);