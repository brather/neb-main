<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? $APPLICATION->SetTitle(""); ?>

<div class="container v-padding">
    <div class="button-bar">
    	<a href="<?=$APPLICATION->GetCurDir()?>" class="btn btn-default">Вернуться на главную</a>
    	<br/><a data-toggle="collapse" href="#searchformcontainer" class="btn btn-default collapsed">Изменить параметры поиска</a>
    </div>
    <div id="searchformcontainer" class="collapse" aria-expanded="false">
	    <form>
		    <dl class="form-dl">
		    	<dt><label class="for-lg">Раздел для поиска</label></dt>
		    	<dd>
		    		<select class="form-control input-lg">
		    			<option>Библиотеки</option>
		    			<option>Музеи</option>
		    			<option>Архивы</option>
		    		</select>
		    	</dd>
		    	<dt><label class="for-lg">Вид поиска</label></dt>
		    	<dd>
		    		<select class="form-control input-lg">
		    			<option>Одной строкой</option>
		    			<option>Расширенный поиск</option>
		    		</select>
		    	</dd>
		    	<dt><label for="requeststring" class="for-lg">Введите запрос</label></dt>
		    	<dd>
		    		<input type="text" class="form-control input-lg" id="requeststring" placeholder="Пушкин, кабы я была царица"/>
		    	</dd>
		    </dl>
		    <div class="button-bar">
		    	<button class="btn btn-primary">Найти</button><br/>
		    	<button class="btn btn-default" type="reset">Очистить поиск</button>
		    </div>
		</form>
	</div>
	<p class="h4">Найдено результатов: <span>1698</span></p>
	<p>Сортировать по:</p>
	<select class="form-control input-lg">
		<option>Релевантсности</option>
		<option>Году издания</option>
	</select>

	<div class="search-results" style="counter-reset: searchresults 4;" data-explain="last-value-was-4-so-starts-from-position-minus-1">
		<div class="search-result-item sri">
			<h5>Пушкин запись номер 1</h5>
			<dl class="sri-characters">
				<dt>Автор издания</dt>
				<dd>Гослитиздат</dd>
				<dt>Год издания</dt>
				<dd>1950</dd>
				<dt>Количество страниц</dt>
				<dd>255</dd>
				<dt>Доступ</dt>
				<dd>Открытый доступ</dd>
			</dl>
			<div class="button-bar">
				<button class="btn btn-default">Карточка издания</button><br/>
				<button class="btn btn-primary">Читать</button>
			</div>
		</div>
		<div class="search-result-item sri">
			<h5>Пушкин запись номер 1</h5>
			<dl class="sri-characters">
				<dt>Автор издания</dt>
				<dd>Гослитиздат</dd>
				<dt>Год издания</dt>
				<dd>1950</dd>
				<dt>Количество страниц</dt>
				<dd>255</dd>
				<dt>Доступ</dt>
				<dd>Открытый доступ</dd>
			</dl>
			<div class="button-bar">
				<button class="btn btn-default">Карточка издания</button><br/>
				<button class="btn btn-primary">Читать</button>
			</div>
		</div>
		<div class="search-result-item sri">
			<h5>Пушкин запись номер 1</h5>
			<dl class="sri-characters">
				<dt>Автор издания</dt>
				<dd>Гослитиздат</dd>
				<dt>Год издания</dt>
				<dd>1950</dd>
				<dt>Количество страниц</dt>
				<dd>255</dd>
				<dt>Доступ</dt>
				<dd>Открытый доступ</dd>
			</dl>
			<div class="button-bar">
				<button class="btn btn-default">Карточка издания</button><br/>
				<button class="btn btn-primary">Читать</button>
			</div>
		</div>
		<section>
			<p style="margin-left: 2em;">Навигация по страницам (Страница: 1)</p>
			<ul class="pagination">
				<li class="bx-pag-prev"><span class="pagination__active">Назад</span></li>
				<li class="bx-active"><span class="pagination__active">1</span></li>
				<li class=""><a href="/profile/markup/books/page-2/"><span>2</span></a></li>
				<li class=""><a href="/profile/markup/books/page-3/"><span>3</span></a></li>
				<li class=""><a href="/profile/markup/books/page-4/"><span>4</span></a></li>
				<li class=""><a href="/profile/markup/books/page-269/"><span>269</span></a></li>
				<li class="bx-pag-next"><a href="/profile/markup/books/page-2/"><span>Вперед</span></a></li>
				<li class="bx-pag-all"><a href="/profile/markup/books/page-all/" rel="nofollow"><span>Все</span></a></li>
			</ul>
		</section>
	</div>

</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>