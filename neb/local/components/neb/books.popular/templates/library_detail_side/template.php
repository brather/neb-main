<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
if(!empty($arResult['POPULAR_BOOKS']))
{
	?>
	<div class="b-popularbooks">
		<h2>популярные издания</h2>
		<div class="b-popularslider" data-slides="one">
			<?
			$i = 0;
			foreach($arResult['POPULAR_BOOKS'] as $arItem)
			{
				$i++;
				?>

				<div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto">

							<?/*<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>*/?>

							<?/*?><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/<?=$arItem['~id']?>/" ><img alt="<?=$arItem['title']?>" src="/local/tools/exalead/thumbnail.php?url=<?=$arItem['URL']?>&source=<?=$arItem['source']?>&width=80&height=125" class="loadingimg"></a><?*/?>

							<?
							/*
							?>
							<div class="b-loadprogress">
							<div class="b-loadlabel"></div>
							<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>
							<?
							*/
							?>
						</div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/<?=$arItem['~id']?>/"><?=trimming_line($arItem['title'], 35)?></a></h2>
						<a href="/search/?f_field[authorbook]=f/authorbook/<?=urlencode(mb_strtolower(strip_tags(trim($arItem['authorbook']))))
						.'&'.DeleteParam(['f_field', 'dop_filter'])?>" class="b-book_autor"><?=str_replace(',', '<br />', $arItem['authorbook'])?></a>
						<?
						if(!empty($arResult['POPULAR_BOOKS_VIEWS'][$arItem['id']])){
							?>
							<div class="icoviews">х <?=number_format($arResult['POPULAR_BOOKS_VIEWS'][$arItem['id']], 0, '', ' ')?></div>
						<?
						}
						?>
						<?/*<div class="b-result_sorce clearfix">
							<?
							if(!empty($arItem['fileExt']))
							{
								?>
								<div class="b-result-type">
									<?
									foreach($arItem['fileExt'] as $ext)
									{
										if($ext != 'pdf')
											continue;

										if($ext == 'pdf'){
											?>
											<span class="b-result-type_txt" style="display: none;"><a target="_blank" href="/catalog/<?=$arItem['~id']?>/viewer/"><?=$ext?></a></span>
										<?
										}else{
											?>
											<span class="b-result-type_txt" ><?=$ext?></span>
										<?
										}
									}
									?>
								</div>
							<?
							}
							?>
						</div><!-- /.b-result_sorce -->*/?>
					</div>
				</div><!-- /.b-result-docitem -->
			<?
			}
			?>
		</div>
	</div><!-- /.b-popularbooks -->
<?
}
?>