<?
$MESS["LEARNING_TEMPLATE_COPYRIGHT"] = "&copy; 2013 Bitrix, Inc.";
$MESS["FOOTER_ADD_TO"] = "Add to";
$MESS["FOOTER_MY_COLLECTIONS"] = "My collections";
$MESS["FOOTER_CREATE_COLLECTION"] = "Make a collection";
$MESS["FOOTER_REMOVE_BOOK"] = "Remove the book from My Library";
$MESS["FOOTER_ACCEPT_REMOVE"] = "Remove";
$MESS["FOOTER_KEEP_BOOK"] = "Cancel";
$MESS["FOOTER_CLOSE_WINDOW"] = "Close window";
?>
