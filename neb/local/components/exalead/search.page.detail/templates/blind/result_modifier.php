<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 16.02.2017
 * Time: 18:01
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

if (!empty($arResult['BOOK'])) {

    if (empty($arResult['BOOK']['id'])) {
        $arResult['BOOK']['title'] = $arResult['BOOK']['META']['TITLE'] = 'Издание не найдено';
    }

    $APPLICATION->SetTitle($arResult['BOOK']['META']['TITLE']);
    $APPLICATION->SetPageProperty("description", $arResult['BOOK']['META']['DESCRIPTION']);
}
