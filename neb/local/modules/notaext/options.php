<?
require_once(__DIR__ . "/prolog.php");
$module_id = ADMIN_MODULE_NAME;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
Loc::loadMessages(__FILE__);

$MOD_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MOD_RIGHT >= "R") {
    $arAllOptions = array(
        'main' => array(
            Array('option_checkbox', 'option_checkbox', 'N', Array('checkbox')),
            Array("note" => 'note note'),
            Array('option_text', 'option_text', '10', Array('text')),
        )
    );

    $aTabs = array(
        #array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => "ad_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
        array("DIV" => "edit2", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "ICON" => "ad_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")),
    );

    $tabControl = new CAdminTabControl("tabControl", $aTabs);

    if ($REQUEST_METHOD == "GET" && $MOD_RIGHT == "W" && strlen($RestoreDefaults) > 0 && check_bitrix_sessid()) {
        COption::RemoveOption($module_id);
        $z = CGroup::GetList($v1 = "id", $v2 = "asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
        while ($zr = $z->Fetch())
            $APPLICATION->DelGroupRight($module_id, array($zr["ID"]));
    }

    if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply) > 0 && $MOD_RIGHT >= "W" && check_bitrix_sessid()) {
        foreach ($arAllOptions as $aOptGroup) {
            foreach ($aOptGroup as $option) {
                __AdmSettingsSaveOption($module_id, $option);
            }
        }

        $Update = $Update . $Apply;
        ob_start();
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
        ob_end_clean();

        if ($Apply == '' && $_REQUEST["back_url_settings"] <> '')
            LocalRedirect($_REQUEST["back_url_settings"]);
        else
            LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
    }
    ?>
    <?
    if ($e = $APPLICATION->GetException())
        $message = new CAdminMessage('', $e);

    if ($message)
        echo $message->Show();

    $tabControl->Begin();
    ?>
    <form method="POST" action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx($mid) ?>&lang=<?= LANG ?>"
          name="<?= $module_id ?>_settings">
        <input type="hidden" name="Update" value="Y">
        <input type="hidden" name="back_url_settings" value="<?= htmlspecialchars($_REQUEST["back_url_settings"]) ?>">
        <?= bitrix_sessid_post(); ?>

        <?#$tabControl->BeginNextTab();
        ?>

        <?#__AdmSettingsDrawList($module_id, $arAllOptions['main']);
        ?>

        <? $tabControl->BeginNextTab(); ?>
        <? require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/admin/group_rights.php'); ?>

        <? $tabControl->Buttons(); ?>
        <script language="JavaScript">
            function RestoreDefaults() {
                if (confirm('<?=AddSlashes(GetMessage('MAIN_HINT_RESTORE_DEFAULTS_WARNING'))?>'))
                    window.location = "<?=$APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?=LANG?>&mid=<?=urlencode($mid) . "&" . bitrix_sessid_get();?>";
            }
        </script>
        <?
        if (strlen($_REQUEST["back_url_settings"]) > 0) {
            ?>
            <input type="submit" name="Update" <?= $MOD_RIGHT < "W" ? " disabled" : "" ?>
                   value="<?= GetMessage('MAIN_SAVE') ?>">
            <?
        }
        ?>
        <input type="submit" name="Apply" value="<?= GetMessage("MAIN_OPT_APPLY") ?>"
               title="<?= GetMessage("MAIN_OPT_APPLY_TITLE") ?>"<?= $MOD_RIGHT < "W" ? " disabled" : "" ?>>

        <?
        if (strlen($_REQUEST["back_url_settings"]) > 0) {
            ?>
            <input type="button" name="Cancel" value="<?= GetMessage("MAIN_OPT_CANCEL") ?>"
                   title="<?= GetMessage("MAIN_OPT_CANCEL_TITLE") ?>"
                   onclick="window.location='<?= htmlspecialchars(CUtil::JSEscape($_REQUEST["back_url_settings"])) ?>'">
            <?
        }
        ?>
        <input type="button" title="<?= GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>" OnClick="RestoreDefaults();"
               value="<?= GetMessage("MAIN_RESTORE_DEFAULTS") ?>"<?= $MOD_RIGHT < "W" ? " disabled" : "" ?>>

        <? $tabControl->End(); ?>
    </form>
    <? $tabControl->ShowWarnings($module_id . "_settings", $message); ?>
    <?
}
