<?php
$MESS["T_IBLOCK_DESC_ASC"] = "По возрастанию";
$MESS["T_IBLOCK_DESC_DESC"] = "По убыванию";
$MESS["T_IBLOCK_DESC_LIST_ID"] = "Код информационного блока";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Тип информационного блока";

$MESS["TITLE_BLOCK_NAME_PROJECT"] = "Название проекта";
$MESS["TITLE_BLOCK_NAME_PROJECT_VALUE"] = "НЭБ";
$MESS["TITLE_BLOCK_NAME"] = "Заголовок функционала для пользователя";
$MESS["TITLE_BLOCK_NAME_VALUE"] = "Вопросы и ответы";
$MESS["SHOW_TITLE_BLOCK_NAME"] = "Показывать заголовок блока описания функционала для пользователя";
$MESS["TITLE_BLOCK_DESCRIPTION"] = "Описание функционала для пользователя";
$MESS["TITLE_BLOCK_DESCRIPTION_VALUE"] = "В этом разделе можно найти ответы на наиболее часто задаваемые вопросы пользователей.";
$MESS["TITLE_BLOCK_URL_FEEDBACK_PAGE"] = "Адрес страницы для обратной связи";
?>