/*
Получение кропнутого изображения
*/
function getCropImg(params, w, h, id)
{
	params.wResize = w;
	params.hResize = h;
	params.id = id;
	params.sessid = BX.bitrix_sessid();

	$.getJSON( "/local/tools/notaext/detailGallery/img_crop.php", params, function( json ) {
		$('.edit-items-list li#' + id + ' img').attr('src', json.img);
	});
}

/*
заполнение контента в табе редактирования
*/
function initEditTab(obItem)
{
	if(obItem.length <= 0)
	{
		//console.log('initEditTab: empty obItem');
		return false;
	}
	$( "#tabs-detail-gallery" ).tabs( "enable", "#tabs-edit" );
	$('a[href="#tabs-edit"]').click();

	$.each( obItem, function( key, item ) {

		if($('li.eid' + item.ID).length <= 0)
		{
			var itemHtml = 	''+
			'<li class="item eid'+item.ID+'" id="'+item.ID+'" rel="'+(item.PARAM ? item.PARAM : '')+'">'+	
			'<img src="'+item.PREVIEW_PICTURE+'" alt="" />'+
			'<div class="button">'+
			'<div class="edit" title="'+BX.message('EDBG_PHOTO_EDIT_TITLE')+'">'+BX.message('EDBG_PHOTO_EDIT')+'</div>'+
			'<div class="hide" title="'+BX.message('EDBG_PHOTO_HIDE_TITLE')+'">'+BX.message('EDBG_PHOTO_HIDE')+'</div>'+						
			'</div>'+
			'</li>';

			$('.edit-items-list').append(itemHtml);
		}
	});

	$( ".edit-items-list" ).sortable(
		{
			cursor: "move",
			scroll: false,
			scrollSensitivity: 10
		}
	);
}

/* Теги в табе загрузки */
function initSelect2Tags(obj)
{
	obj.select2({
		tags: true,
		language: "ru",
		//placeholder: "Search tags",
		minimumInputLength: 2,
		multiple:true,
		ajax: {
			delay: 250,
			url: "/local/tools/notaext/detailGallery/tags.php",
			dataType: 'json',
			data: function (term, page) {
				return term;
			},
			results: function (data, page) {
				return { results: data };
			}
		}
	});
}

$(function() {

	/*Search tab	*/
	$( "body" ).on('submit', '#tabs-search .search-form form', function(event){
		event.preventDefault();

		$( "#tabs-search .search-result ul" ).remove();

		if($('.search-form .field input').val().length <= 0)
			return false;

		BX.showWait();

		$.getJSON( $(this).attr('action'), {query : $('.search-form .field input').val()}, function( data ) {

			BX.closeWait();
			var items = [];

			$.each( data, function( key, val ) {
				items.push( "<li id='" + val.ID + "'><div class='img'><img src='" + val.PREVIEW_PICTURE + "'/><input title='"+BX.message('EDBG_CHECKED_PHOTO')+"' type='checkbox' name='search-select-items[]' value='" + val.ID + "'></div><div class='desc'>" + val.PREVIEW_TEXT + "</div></li>" );
			});
			$( "<ul/>", {
				//"class": "my-new-list",
				html: items.join( "" )
			}).appendTo( "#tabs-search .search-result" );
		});

		return false;
	});

	$( "body" ).on('click','#tabs-search .search-result .img input', function(e){
		var n = $( "#tabs-search .search-result .img input:checked" ).length;
		if(n <= 0)
			$( "#tabs-search .selection" ).addClass('hidden');
		else
		{
			$( "#tabs-search .selection" ).removeClass('hidden');
			var text = $( "#tabs-search .selection input" ).attr('rel');
			text = text.replace('#', n);
			$( "#tabs-search .selection input" ).val(text);
		}

	});

	$( "body" ).on('click', '#tabs-search .selection input', function(event){
		var n = $( "#tabs-search .search-result .img input:checked" ).length;
		if(n <= 0) 
			return false;

		var items = [];

		$( "#tabs-search .search-result .img input:checked" ).each(function() {
			//$(this).attr("checked", false);
			$(this).click();

			var parent = $(this).closest('li');
			var id = $(parent).attr('id'); 
			var img = $(parent).find('img').attr('src');
			items.push({ID: id, PREVIEW_PICTURE: img}); 
		});

		initEditTab(items);
	});	

	/* Edit tab */
	$( "body" ).off('click', '#tabs-edit li.item .hide');
	$( "body" ).on('click', '#tabs-edit li.item .hide', function(e){
		$(this).closest('li').remove();
	});

	$( "body" ).off('click', '#tabs-edit li.item .edit');
	$( "body" ).on('click', '#tabs-edit li.item .edit', function(e){
		var $root = $(this).closest('li');
		var idItem = $root.attr('id');

		var applyCloseBtn = new BX.CWindowButton({	
			title: BX.message('EDBG_PHOTO_EDIT_APPLY_CLOSE'),
			className: 'adm-btn-save',
			action: function()
			{
				var cropParams = window.frames['detailGallery-edit-frame'].getCropParams();
				cropParams = cropParams[0];

				getCropImg(cropParams.Data, 200, 140, cropParams.ID);

				var cropParamsB64 = btoa('('+JSON.stringify(cropParams)+')');
				$('.item.eid'+cropParams.ID).attr('rel', cropParamsB64);
				detailGalleryWndEdit.Close();
			}
		});
		var applyCloseNextBtn = new BX.CWindowButton({
			title: BX.message('EDBG_PHOTO_EDIT_APPLY_NEXT'),
			className: 'adm-btn-save',
			action: function()
			{
				var cropParams = window.frames['detailGallery-edit-frame'].getCropParams();
				cropParams = cropParams[0];

				getCropImg(cropParams.Data, 200, 140, cropParams.ID);

				var cropParamsB64 = btoa('('+JSON.stringify(cropParams)+')');
				$('.item.eid'+cropParams.ID).attr('rel', cropParamsB64);

				// reload frame next photo
				var next = $('ul.edit-items-list li.item.eid'+cropParams.ID).next('li.item');
				if(next.length <= 0)
					next = $('ul.edit-items-list li.item').eq(0);

				var url = $('iframe.detailGallery-edit-frame').attr('rel');
				url = url.replace('#IDITEM#', $(next).attr('id'));
				url = url.replace('#SESSID#', BX.bitrix_sessid());
				$('iframe.detailGallery-edit-frame').attr('src', url);
			}
		});
		var CloseBtn = new BX.CWindowButton({
			title: BX.message('EDBG_PHOTO_EDIT_CANCEL'),
			className: 'adm-btn-save',
			action: function()
			{
				detailGalleryWndEdit.Close();
			}
		});

		detailGalleryWndEdit = null;

		BX.showWait();
		BX.ajax.get(
			'/local/tools/notaext/detailGallery/editpic.php?sessid=' + BX.bitrix_sessid() + '&idItem=' + idItem,
			BX.delegate(function(res)
				{
					BX.closeWait();

					detailGalleryWndEdit = new BX.CDialog({
						content: res,
						resizable: false,
						draggable: false,
						resize_id: 'detail_gallery_edit',
						width: 1050,
						height: 600,
						title: BX.message('EDBG_PHOTO_EDIT_TITLE_DIALOG'),
						buttons: [
							applyCloseBtn,
							applyCloseNextBtn,
							CloseBtn,
						]
					});

					BX.addClass(detailGalleryWndEdit.DIV, 'WindowDetailGalleryEdit');

					BX.addCustomEvent(detailGalleryWndEdit, 'onWindowClose', function(obWnd) {
						obWnd.DIV.parentNode.removeChild(obWnd.DIV);
						obWnd.OVERLAY.parentNode.removeChild(obWnd.OVERLAY);
					});

					detailGalleryWndEdit.Show();
					BX.addClass(detailGalleryWndEdit.OVERLAY, 'WindowDetailGalleryEdit');
				}, this)
		);

	});

});

/* Добавление кнопки в виз редактор + диалог */

if(!EditorDGInit)
	var EditorDGInit = [];

;(function(){
	var detailGalleryHandler = function(editor, targetRel, targetAlt, idDg)
	{	
		BX.showWait();

		BX.ajax.post(
			'/local/tools/notaext/detailGallery/main.php?get=main&sessid=' + BX.bitrix_sessid()+'&lang='+BX.message('LANGUAGE_ID'),
			{targetRel: (targetRel? targetRel : ''), targetAlt: (targetAlt? targetAlt : ''), idDg: (idDg? idDg : ''), EDITOR_ID: editor.id},
			BX.delegate(function(res)
				{
					BX.closeWait();
					detailGalleryWnd = new BX.CDialog({
						content: res,
						resizable: false,
						draggable: false,
						resize_id: 'detail_gallery_res',
						width: 890,
						height: 550,
						title: BX.message('EDBG_DIALOG_TITLE'),
					});

					BX.addClass(detailGalleryWnd.DIV, 'WindowDetailGallery');

					BX.addCustomEvent(detailGalleryWnd, 'onWindowClose', function(obWnd) {
						obWnd.DIV.parentNode.removeChild(obWnd.DIV);
						obWnd.OVERLAY.parentNode.removeChild(obWnd.OVERLAY);
					});

					$('body').on('WindowDetailGalleryClose', function(e) {
						detailGalleryWnd.Close();
					});

					//detailGalleryHandler.apply(this, [editor]);
					detailGalleryWnd.Show();

					BX.addClass(detailGalleryWnd.OVERLAY, 'WindowDetailGallery');

				}, this)
		);
	};

	function applyForEditor(editor)
	{
		var _run = true;
		if(EditorDGInit.length > 0)
		{	
			$.each( EditorDGInit, function( key, value ) {
				if(editor.id == value)
					_run = false
			});
		}									

		if(_run)
		{
			EditorDGInit.push(editor.id);

			BX.addCustomEvent(editor, 'OnIframeContextMenu', applyContextEditor);
			editor.AddButton(
				{
					id : 'DetailGallery',
					src : '/local/tools/notaext/detailGallery/images/detailGallery.png',
					name : BX.message('EDBG_BUTTON_NAME'),
					codeEditorMode : true,
					compact : true,
					handler : function(){detailGalleryHandler(editor);},
					toolbarSort: 220
				}
			);
		}
	}

	var ContextTarget;

	function applyContextEditor(context)	
	{
		var isAddMenu = true;
		if(context.target.className != 'gallery-detail-object-v2')
			isAddMenu = false;
		else
			ContextTarget	= context.target;

		if (window.BXHtmlEditor && window.BXHtmlEditor.editors)
		{
			for (var id in window.BXHtmlEditor.editors)
			{
				if(isAddMenu && $(ContextTarget).attr('editor'))
					id = $(ContextTarget).attr('editor');

				if (window.BXHtmlEditor.editors.hasOwnProperty(id))
				{
					var isAddContext = true;

					$.each(window.BXHtmlEditor.Get(id).contextMenu.items.IMG, function( key, value ) {
						if(value.idContext && value.idContext == 'edit_gallery')
						{
							isAddContext = false;
							if(!isAddMenu)
								window.BXHtmlEditor.Get(id).contextMenu.items.IMG[key] = '';
						}
					});

					if(isAddContext && isAddMenu) 
					{
						window.BXHtmlEditor.Get(id).contextMenu.items.IMG.push({
							text: BX.message('EDBG_CONTECST_EDIT_GALLERY'), 
							idContext: 'edit_gallery', 
							bbMode: true, 
							onclick: function()
							{
								applyContextEditorTarget(window.BXHtmlEditor.Get(id));
								this.popupWindow.close();
							}
						});
					}

				}
			}
		}
	}

	function applyContextEditorTarget(editor)
	{
		detailGalleryHandler(editor, $(ContextTarget).attr('rel'), $(ContextTarget).attr('alt'), $(ContextTarget).attr('id'));
	}

	if (window.BXHtmlEditor && window.BXHtmlEditor.editors)
	{
		for (var id in window.BXHtmlEditor.editors)
		{
			if (window.BXHtmlEditor.editors.hasOwnProperty(id))
			{
				applyForEditor(window.BXHtmlEditor.Get(id))
			}
		}
	}

	BX.addCustomEvent("OnEditorInitedBefore", applyForEditor);
})();
