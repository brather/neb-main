<?	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if(empty($arResult['ITEMS']))
    return false;
	
?>	
	<div class="b-bookinfo_popular iblock">
		<div class="b-bookinfo_tit"><h2><?=Loc::getMessage('POPULAR_LIST_TEMPLATE_TITLE'); ?></h2></div>
		<ul class="b_bookpopular">
			<?foreach($arResult['ITEMS'] as $arElement){?>
			<li>
				<a href="<?=$arElement['LINK']?>" class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955"><img class="loadingimg" src="/local/tools/exalead/thumbnail.php?url=<?=$arElement['URL']?>&source=<?=$arElement['SOURCE']?>&width=80&height=125" alt="" data-autor="<?=$arElement['AUTHOR']?>" data-title="<?=$arElement['TITLE']?>"></a>
				<div class="b_popular_descr iblock">
					<?if($arElement['TITLE']){?>
						<h4><a href="<?=$arElement['LINK']?>" class="popup_opener ajax_opener coverlay" data-width="955"><?=$arElement['TITLE']?></a></h4>
					<?}?>
					<?if($arElement['AUTHOR']){?>
						<span class="b-autor"><?=Loc::getMessage('POPULAR_LIST_TEMPLATE_AUTHOR'); ?> <a href="<?=$arElement['AUTH_LINK']?>" class="lite"><?=$arElement['AUTHOR']?></a></span>
					<?}?>
					<?if($arElement['LIBRARY']){?>
						<div class="b-result_sorce_info"><em><?=Loc::getMessage('POPULAR_LIST_TEMPLATE_SORCE'); ?></em> <a href="<?=$arElement['LIB_LINK']?>" class="b-sorcelibrary"><?=$arElement['LIBRARY']?></a></div>
					<?}?>
				</div>
			</li>
			<?}?>
		</ul>
	</div> <!-- /.b-bookinfo_popular -->
	
