<?php
$MESS['MAIN_SETTINGS'] = 'Общие';
$MESS['MAIN_SETTINGS_USERS'] = 'Пользователи';
$MESS['TEMPORARY_VERIFICATION'] = 'Временная верификация(в часах)';
$MESS['TEMPORARY_USER_LOCK'] = 'Временная блокировка(в часах)';
$MESS['AUTH_ACCESS_MODE'] = 'Разрешить доступ к закрытым изданиям только зарегистрированным пользователям из ЭЧЗ';
$MESS['MAIN_SETTINGS_MINIFY'] = 'Оптимизация CSS, JS и HTML';
$MESS['MAIN_SETTINGS_MINIFY_CSS_JS'] = 'Минифицировать CSS и JS файлы';
$MESS['MAIN_SETTINGS_MINIFY_HTML'] = 'Минифицировать HTML код сайта';
$MESS['MAIN_SETTINGS_SECURITY'] = 'Безопасность';
$MESS['MAIN_SETTINGS_SECURITY_SSL'] = 'Проверять SSL-сертификат домена при внешних подключениях<br /><i>(не подходит для подмененных и самоподписанных сертификатов)</i>';

$MESS['MAIN_PAGE_SETTINGS'] = 'Главная страница';
$MESS['MAIN_PAGE_NEWS_SETTINGS'] = 'Настройки новостей';
$MESS['MAIN_PAGE_NEWS_COUNT'] = 'Количество новостей';
$MESS['MAIN_PAGE_NEWS_COUNT_YL'] = 'Количество новостей года литературы';

$MESS['MAIN_PAGE_MESSAGE_SETTINGS'] = 'Настройки предупреждения';
$MESS['MAIN_PAGE_MESSAGE']          = 'Выводить предупреждение';
$MESS['MAIN_PAGE_MESSAGE_TEXT']     = 'Текст предупреждения';

$MESS['STATS_SETTINGS'] = 'Статистика';
$MESS['STATS_HOST'] = 'Хост статистики';
$MESS['STATS_WRITER_TOKEN'] = 'Writer token';
$MESS['STATS_READER_TOKEN'] = 'Reader token';

$MESS['OTHER_SETTINGS'] = 'Прочие';
$MESS['DEBUG_EMAIL']    = 'E-mail для отладочных сообщений';
