$(function() { 
  $( ".tabs" ).tabs();
  $('.toolbarBottomProgress').slider({
	range: "min",
	value: 37,
	min: 1,
	max: 700,
	slide: function( event, ui ) {
	//$( "#amount" ).val( "$" + ui.value );
	}
	});

  $('.scaleBox').slider({
	orientation: "vertical",
	range: "min",
	min: 0,
	max: 100,
	value: 60,
	slide: function( event, ui ) {
	//$( "#amount" ).val( ui.value );
	}
	});
  $('.iblock').cleanWS();
  $('.toolbar').toolbarView();


	var viewer = document.getElementById('viewer');


	//viewer.addEventListener('vieweronafterdraw', createTableOfContents);

	viewer.addEventListener('vieweronafterdraw', createCite);

	viewer.addEventListener('vieweronafterdraw', createSearch); // поиск

	viewer.addEventListener('viewerscalechange', updateSelectionOnPage);

	viewer.addEventListener('viewernumpagechange', updateSelectionOnPage);

	viewer.addEventListener('viewerscrollintoview', function(e){
		scrollToThumbIfNeeded();
	});



});



function scrollToThumbIfNeeded() {
	var thumbnailView = $('#thumbnailView');
	if (!thumbnailView.hasClass('hidden')) {
		var scr = thumbnailView.parents('.js_scroll:eq(0)');
		var scrwrap = scr.find('.mCSB_container');
		var newpage = thumbnailView.find('.selected'), a = newpage.parent();
		var pos = a.position();
		var currscr = Math.abs(parseInt(scrwrap.css('left')));
		var scrW = scr.width();
		if (scr.length>0) {
			if (pos.left < currscr || pos.left+a.outerWidth(true) > currscr+scrW) {
				scr.mCustomScrollbar("scrollTo",pos.left);
	//				PDFViewerApplication.forceRendering();
			}
		}
		//PDFView.pdfThumbnailViewer._getVisibleThumbs();
		//PDFView.pdfThumbnailViewer.forceRendering();
	}
	var outline = $('#outlineView');
	if (!outline.hasClass('hidden')) {
		var page = PDFView.page;
		var scr = outline.parents('.js_scroll:eq(0)');
		var scrwrap = scr.find('.mCSB_container');
		scr.mCustomScrollbar("scrollTo",'.p_'+page+':eq(0)');
	}	
}



function createTableOfContents(){
	var thumb = $('#thumbnailView'), outline = $('#outlineView');
	if (outline.children().length<1) { // нет оглавления
		outline.addClass('hidden');
		thumb.removeClass('hidden');
		thumb.parents('.js_scroll:eq(0)').addClass('thumbviewer').mCustomScrollbar('update');
		var W = 0;
		var chld = thumb.children();
		for (var i=0; i<chld.length; i++) {
			W+=chld.outerWidth(true);
		}
		thumb.css('min-width',W);
	} else {
		outline.removeClass('hidden');
		thumb.addClass('hidden');
		thumb.parents('.js_scroll:eq(0)').removeClass('thumbviewer').addClass('outlineviewer');
	}
}




function updateSelectionOnPage() { // отрисовка цитат
		var scope = angular.element(document.getElementById('mainContainer')).scope();
		if (scope && PDFView && PDFView.page) {
				if (scope.bookmarks) {
					var toolbarBookmark = $('.toolbarBookmark');
					toolbarBookmark.removeClass('bmchecked');
					for (var i=0; i<scope.bookmarks.length; i++){
						if (scope.bookmarks[i].page==PDFView.page) {
							toolbarBookmark.addClass('bmchecked');
							break;
						}
					}
				}

			var page = $('#pageContainer'+PDFView.page+' .canvasWrapper');
			if (page.length>0){
				var scale = (PDFView && PDFView.currentScale)? PDFView.currentScale : 1;
				page.find('.storedcite').remove();
				if (scope.cites) {
					for (var i=0; i<scope.cites.length; i++) {
						if (scope.cites[i].citepage==PDFView.page) {
							var cite  = scope.cites[i];
							var newcite = $('<div class="storedcite"></div>').appendTo(page);
							newcite.css({top: cite.citetop*scale, left: cite.citeleft*scale, width: cite.citewidth*scale, height: cite.citeheight*scale});
						}
					}
				}
				var np = [];
				if (scope.notes) {
					for (var i=0; i<scope.notes.length; i++) {
						var note = scope.notes[i];
						if (note.notepage==PDFView.page) {
							var newnote = $('<div class="storedcite"></div>').appendTo(page);
							newnote.css({top: note.notetop*scale, left: note.noteleft*scale, width: note.notewidth*scale, height: note.noteheight*scale});
							note.noteframe = newnote;
						}
						if (np.indexOf(note.notepage) < 0) {
							np.push(note.notepage);
							// обновить заметки для страниц
							refreshNotesOnPage(note.notepage);
						}
					}
				}
			}
		scrollToThumbIfNeeded();
		}

		// 

}




debugmode = false;
	function createCite(e) {
			var viewer = $(e.target);
			var rect = {};
			var curs = {top:0,left:0};
			var sel = $('.selectedrect');
			var overlay = $('.ui-widget-overlay');
			var formblock = $('.formblockopen');
			var canvaswrapper, canvas;
			var rectarea = {left:0,top:0,right:0,bottom:0};
			var page; var way = false;
			// возвращает координаты курсора мыши
			function fixcoord(e){
				if (rect.selstart) {
					var pos = canvaswrapper.getBoundingClientRect()
					curs.left = e.pageX - pos.left;
					curs.top = e.pageY - pos.top;
				}
			}
			// убрать область выделения
			function removeSelectionLayer(e){
				var targ = $(e.target);
				if (targ.parents('.selectedrect').length<1 && targ.parents('.formblockopen').length<1) {
					$('.selectedrect, .formblockopen').remove();
					//document.removeEventListener('click', removeSelectionLayer);
				}
			}			



			var startX, startY;

			// отрисовка области выделения
			function createrect(e, isTouch) {
				//alert(way)
				if (!way) return;

				if (isTouch) {
					LT = getCoors(e);
					var page = $(e.target).parents('.page:eq(0)');
					var pos = page.offset();
				}
				var L = (isTouch)? -1 * pos.left + LT.x : curs.left;				
				var T = (isTouch)? -1 * pos.top + LT.y : curs.top;				

				if (!rect.movestart) {
					startX = rectarea.left = L; startY = rectarea.top = T;
					sel.remove();
					sel = $('<div class="selectedrect"></div>').appendTo(canvaswrapper); // слой рамки-выделения

					($('.selected_rect_menu').clone()).removeClass('hidden').appendTo(sel);

					rect.movestart = true;
				}
				var x,y,w,h;

				if (way == 'CANVAS') { // новый фрагмент
					x = Math.min(L,  rectarea.left);
					y = Math.min(T,  rectarea.top);
					w = Math.abs(L - rectarea.left);
					h = Math.abs(T - rectarea.top);
				}

				//console.log('way: '+way+' curs.left: '+ curs.left +' startX: '+startX+ ' curs.top: ' + curs.top + ' startY: '+startY)
				//} else 
				if (way=='tl') { // левый верхний угол
					rectarea.left = L;
					rectarea.top = T;
					x = L;
					y = T;
					w = Math.abs(startX-L);
					h = Math.abs(startY-T);
				} else if (way=='bl') { // левый нижний угол
					rectarea.left = L;
					rectarea.top = startY;
					x = L;
					y = startY;
					w = Math.abs(startX-L);
					h = Math.abs(T-startY);

				} else if (way=='tr') {  // правый верхний угол
					rectarea.left = startX;
					rectarea.top = T;
					x = rectarea.left;
					y = T;
					w = Math.abs(L-startX);
					h = Math.abs(T-((rectarea.bottom)? rectarea.bottom : startY));
				} else if (way=='br') {  // правый нижний угол
					x = rectarea.left;
					y = rectarea.top;
					w = L-rectarea.left;
					h = T-rectarea.top;
					//alert('x: '+x+' y: '+y+' w: '+w+' h: '+h)
				}
					if (L >= startX && T-3 >= startY) {
						way = 'br';
					} else if (L >= startX && T < startY) {
						way = 'tr';
					} else if (L < startX && T >= startY) {
						way='bl';
					} else if (L < startX && T < startY) {
						way='tl';
					} 

				if (!w || !h) {
					return;
				}

				// перерисовать область выделения
				sel.css({left:x,top:y,width:w,height:h});
				// сохранить параметры выделения
				//rectarea.top = y;
				//rectarea.left = x;
				rectarea.right = x+w;
				rectarea.bottom = y+h;
			}

			// нажатие мышки и возможное начало выделения
			function startSelection(e, isTouch){
				removeSelectionLayer(e);
				if ($(e.target).parents('.page').length>0){
					document.addEventListener('mousemove', fixcoord, false);
					if (e.target.tagName == 'CANVAS') {
						canvas = e.target;
						way = 'CANVAS';
						canvaswrapper = $(canvas).parents('.canvasWrapper:eq(0)');
						if (canvaswrapper.length>0) {
							rect.selstart = true;
							canvaswrapper = canvaswrapper.get(0);
						}
						document.addEventListener('mousemove', createrect, false);
						document.addEventListener('mouseup', fixcoord, false);
						document.addEventListener('mouseup', stopCrop, false);
					} else if ($(e.target).hasClass('rect_dot')) {
						var targ = $(e.target);
						if (targ.hasClass('rect_tl')) { 
							startX = rectarea.right;
							startY = rectarea.bottom;
							way = 'tl';
						} else if (targ.hasClass('rect_tr')) { 
							startX = rectarea.left;
							startY = rectarea.bottom;
							way = 'tr';
 						} else if (targ.hasClass('rect_bl')) { 
							startX = rectarea.right;
							startY = rectarea.top;
 							way = 'bl';
 						} else if (targ.hasClass('rect_br')) {
							startX = rectarea.left;
							startY = rectarea.top;
							way = 'br';
						} 
						
						rect.selstart = rect.movestart = true;
						document.addEventListener('mousemove', createrect, false);
						document.addEventListener('mouseup', fixcoord, false);
						document.addEventListener('mouseup', stopCrop, false);
					}
				}
			}





			function getCiteText(){ // достает текст из текстового слоя
				var text = '';
				var textdiv = $('.textLayer', page).children();
				var err = 0; var w = sel.width();
				var contpos = $(canvas).offset();
				for (var i=0; i<textdiv.length; i++) {
					var cont = textdiv.eq(i);
					var t = parseInt(cont.css('top')), l = parseInt(cont.css('left'));
					if (t>rectarea.top && t<rectarea.bottom && t+(cont.height()-cont.height()/2)<rectarea.bottom && l<rectarea.right) {
						var conttext = cont.text(), newtext = "";
						for (var k = conttext.length - 1; k >= 0; k--) {
							newtext = '<span class="tmp rel iblock">'+conttext[k]+'</span>'+newtext;
						};
						cont.html(newtext);

						cont.find('.tmp').each(function(){
							var letter = $(this), pos = letter.offset();
							pos.left = pos.left-contpos.left;
							if (pos.left>rectarea.left && pos.left+letter.width()/2<rectarea.right){
								text += letter.text();
							}
						});
						cont.text(conttext);
					}
				}
				return text;
			}


			function getCiteImage() { // достает изображение из canvas
				if (rectarea.right>rectarea.left && rectarea.bottom>rectarea.top && canvas.tagName == 'CANVAS') {

					if (!canvas.getContext) {
						if (debugmode) alert('Ошибка: canvas.getContext не существует!');
						canvas = null;
						return;
					}
					// Получаем 2D canvas context.
					var context = canvas.getContext('2d');
					if (!context) {
						if (debugmode) alert('Ошибка: getContext! не существует');
						canvas = null;
						return;
					}
					var imgdata = context.getImageData(rectarea.left, rectarea.top, rectarea.right-rectarea.left, rectarea.bottom-rectarea.top);
					var datacanv = document.createElement('CANVAS');
					datacanv.width = rectarea.right-rectarea.left;
					datacanv.height = rectarea.bottom-rectarea.top
					var ctx = datacanv.getContext("2d");
					ctx.putImageData(imgdata, 0, 0);
					return datacanv.toDataURL();
				} else {
					return false;
				}
			}


			// кнопка мышки отпущена, выделение закончено
			function stopCrop(e, isTouch){
				if (isTouch) {

				} else {
					document.removeEventListener('mousemove', fixcoord);
					document.removeEventListener('mousemove', createrect);
					document.removeEventListener('mouseup', fixcoord);
					document.removeEventListener('mouseup', stopCrop);
				}
				rect.selstart = rect.movestart = way = false;
				var sw = sel.width(), sh = sel.height();

				if (!sw || sw<10 || !sh || sh<10) {
					removeSelectionLayer(e);
					return;
				}
				if (sel.find('.rect_dot').length < 1) {
					$('<div class="rect_dot rect_tl" rel="tl"></div>'+
						'<div class="rect_dot rect_tr" rel="tr"></div>'+
						'<div class="rect_dot rect_bl" rel="bl"></div>'+
						'<div class="rect_dot rect_br" rel="br"></div>').appendTo(sel);
				}

				page = $(canvaswrapper).parent('.page');
				rectarea.left = parseInt(sel.css('left'));
				rectarea.top = parseInt(sel.css('top'));
				rectarea.right = rectarea.left + sw;
				rectarea.bottom = rectarea.top + sh;

				text = getCiteText();
				src = getCiteImage();
				//				if (text) {
					sel.addClass('rectready');
				//				}
				sel.find('.b-textLayer_quotes, .b-textLayer_notes').off('click touchstart');

				sel.find('.b-textLayer_quotes, .b-textLayer_notes').on('click touchstart', function(e){

					e.preventDefault();
					var A = $(this), cite = A.hasClass('b-textLayer_quotes'), note = A.hasClass('b-textLayer_notes');

					if ($('.formblockopen').length > 0) return;
					overlay = $('<div class="ui-widget-overlay" />').appendTo('body');
					// цитата
					if (cite) { formblockopen = ($('.selected_rect_cite_form').clone()).appendTo('body'); }
					// заметка
					if (note) {
						formblockopen = ($('.selected_rect_note_form').clone()).appendTo('body'); 
						//var notearea = formblockopen.find('.form_field_note');
						//notearea.focus();
					}
					
					formblockopen.addClass('formblockopen');

					sel.removeClass('rectready');
					
					formblockopen.removeClass('hidden').css({
								'position': 'fixed',
								'top': '50%',
								'left':'50%',
								'margin-top':'-'+formblockopen.height()/2+'px',
								'margin-left':'-'+formblockopen.width()/2+'px',
								'z-index':'1974'
					});

					var scale = (PDFView && PDFView.currentScale)? PDFView.currentScale : 1;

					var T = Math.round(rectarea.top/scale),
						L = Math.round(rectarea.left/scale),
						W = Math.round((rectarea.right - rectarea.left)/scale),
						H = Math.round((rectarea.bottom - rectarea.top)/scale);

					// fillout formfields
					var page = canvas.id.replace('page','');
					formblockopen.find('.form_field_area').val(text);
					formblockopen.find('.form_field_img').val(src);
					formblockopen.find('.form_field_page').val(page);
					formblockopen.find('.form_field_top').val(T);
					formblockopen.find('.form_field_left').val(L);
					formblockopen.find('.form_field_width').val(W);
					formblockopen.find('.form_field_height').val(H);

					if (note) {
						var notearea = formblockopen.find('.form_field_note');
						notearea.on('click touchstart', function(){
							notearea.focus();
						});
							notearea.focus();
					}

					formblockopen.find('.cancel_button').on('click touchstart', function(e){ // отмена выбора цитаты
						e.preventDefault();
						sel.addClass('rectready');
						formblockopen.remove();
						overlay.remove();
					});

					// сабмит формы
					formblockopen.find('.add_button').on('click touchstart', function(e){ // выбор цитаты
						e.preventDefault();
						var form = formblockopen.find('form');
						var formdata = form.serialize();


						$.post(form.attr('action'), formdata, function(data){
							sel.remove();
							formblockopen.remove();
							overlay.remove();
							var result = $(data).appendTo('body');
							result.removeClass('hidden');
							var w = result.width(), h = result.height();
							var wnd = $(window), ww = wnd.width(), wh = wnd.height();
							result.css({'z-index':1980,'left':'40%', 'top': '40%', 'opacity': 0})
									.animate({'opacity': 1, 'left':ww-w-40, 'top':wh-h-40}, 450, 'easeOutQuad')
									.on('click', function(){
										result.remove();
									});

							// отправить в модель приложения 
							var scope = angular.element(document.getElementById('mainContainer')).scope();
							if (cite) {							
								var newcite = {
										"id":result.data('id'),
										"citearea":text,
										"citeimg":src,
										"citepage":page,
										"citetop":T,
										"citeleft":L,
										"citewidth":W,
										"citeheight":H
									};
								scope.$apply(function(){
									scope.cites.push(newcite);
								});
							}

							if (note) {							
								var newnote = {
										"id":result.data('id'),
										"note":formblockopen.find('.form_field_note').val(),
										"notearea":text,
										"noteimg":src,
										"notepage":page,
										"notetop":T,
										"noteleft":L,
										"notewidth":W,
										"noteheight":H
									};
								scope.$apply(function(){
									scope.notes.push(newnote);
								});
								refreshNotesOnPage(page);
							}

							updateSelectionOnPage();


							window.setTimeout(function(){
								result.animate({opacity:0}, 450, 'easeOutQuad', function(){
									result.remove();
								});
							}, 2000);
						});
					});
				

				});
			}


			// ВЫДЕЛЕНИЕ НАЧИНАЕТСЯ ЗДЕСЬ
			document.addEventListener('mousedown', startSelection, false);

			var vcont = $('#viewerContainer');
			var bt = parseInt(vcont.css('border-top-width'));

			function getCoors(e) {
				bt = parseInt(vcont.css('border-top-width'));
				var coors = {};
					if (e.touches && e.touches.length) { 	// iPhone
					coors.x = e.touches[0].clientX;
					coors.y = e.touches[0].clientY;
				} else { 				// all others
					coors.x = e.clientX;
					coors.y = e.clientY;
				}
				//alert(coors.y + ' | ' +vcont.scrollTop()+' | '+bt)
				// coors.x += vcont.scrollLeft();
				// coors.y += vcont.scrollTop()-bt;
				return coors;
			}

			var onlongtouch; var touchtimer;
			var touchE, eX, eY, fX, fY;
			var touchduration = 750; //length of time we want the user to touch before we do something


			var startTouchRect = false;

			function touchstart(e) {
				var coors = getCoors(e);
				touchE = e;
				fX = eX = coors.x;
				fY = eY = coors.y;
				if (e.target.tagName == 'CANVAS') {
					startTouchRect = true;
					touchtimer = window.setTimeout(onlongtouch, touchduration);
				} else if ($(e.target).hasClass('rect_dot')) {
						startTouchRect = true;
						var targ = $(e.target);
						//alert(targ.attr('class'))
						if (targ.hasClass('rect_tl')) { 
							startX = rectarea.right;
							startY = rectarea.bottom;
							way = 'tl';
						} else if (targ.hasClass('rect_tr')) { 
							startX = rectarea.left;
							startY = rectarea.bottom;
							way = 'tr';
 						} else if (targ.hasClass('rect_bl')) { 
							startX = rectarea.right;
							startY = rectarea.top;
 							way = 'bl';
 						} else if (targ.hasClass('rect_br')) {
							startX = rectarea.left;
							startY = rectarea.top;
							way = 'br';
						} 
						
						rect.selstart = rect.movestart = true;
					}
			}


			function touchend(touchE) {
				//stops short touches from firing the event
				var coors = getCoors(touchE);
				fX = coors.x;
				fY = coors.y;
				if (touchtimer)
					clearTimeout(touchtimer); // clearTimeout, not cleartimeout..
				if (startTouchRect) {
					stopCrop(touchE, true);
				}
			}

			onlongtouch = function() { //do something 
				if (Math.abs(fX-eX) > 25 || Math.abs(fY-eY) > 25) return
				var page = $(touchE.target).parents('.page');
				if (page.length>0){
					sel.remove();
					var pos = page.offset();
					//alert(pos.top +' | '+ eY)
					x= -1 * pos.left + eX - 40, y= -1 * pos.top + eY - 40, w=80,h=80;

					if (touchE.target.tagName == 'CANVAS') {

						var scale = (PDFView && PDFView.currentScale)? PDFView.currentScale : 1;
						canvas = touchE.target;
						way = 'CANVAS';
						canvaswrapper = $(canvas).parents('.canvasWrapper:eq(0)');
						if (canvaswrapper.length>0) {
							rect.selstart = true;
							canvaswrapper = canvaswrapper.get(0);
						}
						sel = $('<div class="selectedrect"></div>').appendTo(canvaswrapper); // слой рамки-выделения
						($('.selected_rect_menu').clone()).removeClass('hidden').appendTo(sel);
						//sel.css({top: eY/scale, left: eX/scale, width: w, height: h});
						sel.css({top: y, left: x, width: w, height: h});
						stopCrop(touchE, true); // touchE - event, true - touch device
					}
				}
			};
			document.addEventListener('touchstart', touchstart);
			document.addEventListener('touchend', touchend);

			document.addEventListener('touchmove', function(e){
				if (touchtimer)
					clearTimeout(touchtimer); // clearTimeout, not cleartimeout..
				if ($(e.target).hasClass('rect_dot')) {
					e.preventDefault();
					createrect(e, true);
					return false;
				}
			})

			//document.addEventListener('mousedown', touchstart);
			//document.addEventListener('mouseup', touchend);




			updateSelectionOnPage();

}




function createSearch() {
	var scope = angular.element(document.getElementById('mainContainer')).scope();
	if (scope && PDFView && PDFView.page) {
		if (scope.search) {
			var searchbar = document.getElementById('findbar');
			var pc = PDFView.findController.pageContents;

				var nBefore = 50, nAfter = 50; // количество символов до и после фразы

				var charactersToNormalize = {
					'\u2018': '\'', // Left single quotation mark
					'\u2019': '\'', // Right single quotation mark
					'\u201A': '\'', // Single low-9 quotation mark
					'\u201B': '\'', // Single high-reversed-9 quotation mark
					'\u201C': '"', // Left double quotation mark
					'\u201D': '"', // Right double quotation mark
					'\u201E': '"', // Double low-9 quotation mark
					'\u201F': '"', // Double high-reversed-9 quotation mark
					'\u00BC': '1/4', // Vulgar fraction one quarter
					'\u00BD': '1/2', // Vulgar fraction one half
					'\u00BE': '3/4' // Vulgar fraction three quarters
				};
				// Compile the regular expression for text normalization once
				var replace = Object.keys(charactersToNormalize).join('');
				var normalizationRegex = new RegExp('[' + replace + ']', 'g');

				function normalize(text) {
					return text.replace(normalizationRegex, function (ch) {
						return charactersToNormalize[ch];
					});
				}			

			scope.search = {"q":'', "results":[],"count":0, "show":{'limit':3,'start':0,'finish':2}};

			var searchfield = document.getElementById('findInput');
			var defaultplaceholder = searchfield.getAttribute('data-readyplaceholder');
			defaultplaceholder = (defaultplaceholder)? defaultplaceholder : '';
			searchfield.setAttribute('placeholder', defaultplaceholder);
			searchfield.disabled = false;
			if (searchbar.style.display == 'block') {
				searchfield.focus();
			}


			scope.nextResults = function(){
				var newstart = scope.search.show.start + scope.search.show.limit;
				var newfinish = newstart + scope.search.show.limit - 1;

				//newfinish = (newfinish>scope.search.count)? scope.search.count : newfinish;
				if (newfinish >= scope.search.count) {
					newfinish = scope.search.count - 1;
					newstart = scope.search.count - scope.search.show.limit;
				}

				scope.search.show.start = newstart;
				scope.search.show.finish = newfinish;
			}


			scope.prevResults = function(){
				var newfinish = scope.search.show.finish - scope.search.show.limit;
				var newstart = newfinish - scope.search.show.limit + 1;
				//newstart = (newstart<0)? 0 : newstart;
				if (newstart < 0) {
					newstart = 0;
					newfinish = scope.search.show.limit - 1;
				}
				scope.search.show.start = newstart;
				scope.search.show.finish = newfinish;
			}

			function getSearchResults() {
				if (searchfield.disabled) return;
				var q = searchfield.value;
				var queryLen = q.length;
				scope.$apply(function(){
					scope.search.q = q;
					scope.search.count = 0;
					scope.search.results = [];
					scope.search.show = {'limit':3,'start':0,'finish':2};
				});
				var results = [];

				var numPages = PDFView.pdfDocument.numPages;

				if (queryLen === 0) {
					return; // Do nothing: the matches should be wiped out already.
				}

				var query = normalize(q);
				var query = q;
				query = query.toLowerCase();


				function fillResults(page, matches){
					var pc = PDFView.findController.pageContents[page];
 					for (var k = 0; k < matches.length; k++) {
 						var m = matches[k]; // смещение k-вхождения на странице i
 						if (!m) continue;
 						var strbefore = pc.substr(((m - nBefore > -1)? (m - nBefore) : 0), ((m - nBefore > 0)? nBefore : m));
 						var str = pc.substr(m, queryLen);
 						var strafter = pc.substr(m+queryLen, nAfter);
 						results.push({"page":page+1,"strbefore":strbefore,"str":str,"strafter":strafter});
					}
					scope.$apply(function(){
						scope.search.count = results.length;
						scope.search.results = results;
						//scope.search.show.start = 0;
						//scope.search.show.finish = scope.search.show.start + scope.search.show.limit - 1;
					});
				}



				function fillMatches(page){
					var pc = PDFView.findController.pageContents[page];
					pc = pc.toLowerCase();
					var matches = [];
					var matchIdx = -queryLen;
					while (true) {
						matchIdx = pc.indexOf(query, matchIdx + queryLen);
						if (matchIdx === -1) {
							break;
						}
						matches.push(matchIdx);
					}
					if (matches.length>0){
						fillResults(page, matches);
					}
				}

				for (var i=0; i<numPages; i++) {
					if (PDFView.findController.pageContents[i] && PDFView.findController.pageContents[i].length > 0) {
						fillMatches(i);
					} else {
						window.addEventListener('pagecontentsready_'+i, function(e, page){
							fillMatches(e.detail.page);
						});
					}
				}

			}

			document.getElementById('findbarButton').addEventListener('click', getSearchResults);
			searchfield.addEventListener('keydown', function(evt) {
	  			if (evt.keyCode==13) {// Enter
	  				getSearchResults();
				}
			});

		}

	}
}

function refreshNotesOnPage(page){
	var scope = angular.element(document.getElementById('mainContainer')).scope();
	if (!scope) return;
	var pagewrapper = $('#pageContainer'+page);
	var noteslayer = pagewrapper.children('.pagenoteslayer');
	if (noteslayer.length < 1) {
		noteslayer = $('<div class="pagenoteslayer"><div class="pagenoteslist"></div></div>').appendTo(pagewrapper);
		noteslayer.mCustomScrollbar();
	}
	var pagenoteslist = noteslayer.find('.pagenoteslist');
	pagenoteslist.html('');
	for (var i=0; i<scope.notes.length; i++) {
		if (scope.notes[i].notepage == page) {
			(function(){
				var scnote = scope.notes[i];
				var pagenote = $('<div class="pagenote"></div>').appendTo(pagenoteslist);
				pagenote.text(scnote.note);
				pagenote.on('mouseenter', function(){
					scnote.noteframe.addClass('hover');
				}).on('mouseleave', function(){
					scnote.noteframe.removeClass('hover');
				});
			}());
		}
	}
}


	var bookreader = angular.module('bookApp', []);
	bookreader.controller('bookCtrl', function($scope, $http) {
		this.initialize = function(cites){
			$http.get(cites).success(function(data) {
				$scope.cites = data.cites; // цитаты
				$scope.notes = data.notes; // заметки
				$scope.bookmarks = data.bookmarks; // закладки
				$scope.search = {}; // результаты поиска
			});
		};
		$scope.gotoCite = function(cite){
			var menu = $('.viewercontmenu');
				menu.data('status','close').fadeOut(250, 'easeOutQuad');
			if (PDFView) { 
				PDFView.page = cite.citepage;
				window.setTimeout(updateSelectionOnPage,1000);
			}
		};
		$scope.gotoNote = function(note){
			var menu = $('.viewercontmenu');
				menu.data('status','close').fadeOut(250, 'easeOutQuad');
			if (PDFView) { 
				PDFView.page = note.notepage;
				window.setTimeout(updateSelectionOnPage,1000);
			}
		};
		$scope.gotoPage = function(num){
			var menu = $('.viewercontmenu');
				menu.data('status','close').fadeOut(250, 'easeOutQuad');
			if (PDFView) { 
				PDFView.page = num;
				window.setTimeout(updateSelectionOnPage,1000);
			}
		};

		$scope.editNote = function(note){
			var overlay = $('<div class="ui-widget-overlay" />').appendTo('body');
			var formblockopen = ($('.selected_edit_note_form').clone()).appendTo('body');
			formblockopen.addClass('formblockopen');
			formblockopen.removeClass('hidden').css({
								'position': 'fixed',
								'top': '50%',
								'left':'50%',
								'margin-top':'-'+formblockopen.height()/2+'px',
								'margin-left':'-'+formblockopen.width()/2+'px',
								'z-index':'1974'
			});
			// fillout formfields
			formblockopen.find('.form_field_id').val(note.id);
			formblockopen.find('.form_field_note').val(note.note);

			formblockopen.find('.cancel_button').on('click', function(e){ // отмена выбора цитаты
				e.preventDefault();
				formblockopen.remove();
				overlay.remove();
			});

			// сабмит формы
			formblockopen.find('.add_button').on('click', function(e){ // выбор цитаты
				e.preventDefault();
				var form = formblockopen.find('form');
				var formdata = form.serialize();

				$.post(form.attr('action'), formdata, function(data){
					formblockopen.remove();
					overlay.remove();
					var result = $(data).appendTo('body');
					result.removeClass('hidden');
					var w = result.width(), h = result.height();
					var wnd = $(window), ww = wnd.width(), wh = wnd.height();
					result.css({'z-index':1980,'left':'40%', 'top': '40%', 'opacity': 0})
						.animate({'opacity': 1, 'left':ww-w-40, 'top':wh-h-40}, 450, 'easeOutQuad')
						.on('click', function(){
							result.remove();
						});

					$scope.$apply(function(){
						note.note = formblockopen.find('.form_field_note').val();
					});
					updateSelectionOnPage();
					refreshNotesOnPage(note.notepage);


					window.setTimeout(function(){
						result.animate({opacity:0}, 450, 'easeOutQuad', function(){
							result.remove();
						});
					}, 2000);
				});
			});

		};
	});















(function($) { //create closure
  $.fn.cleanWS = function(options) {
	this.each(function() {
	  var iblock = this,
	  par = iblock.parentNode,
	  prev = iblock.previousSibling,
	  next = iblock.nextSibling;
	  while (prev) {
		var newprev = prev.previousSibling;
		if (prev.nodeType == 3 && prev.nodeValue) {
		  for (var i = prev.nodeValue.length - 1; i > -1; i--) {
			var cc = prev.nodeValue.charCodeAt(i);
			if (cc == 9 || cc == 10 || cc == 32) {
			  prev.nodeValue = prev.nodeValue.slice(0, i);
			} else {
			  break;
			}
		  }
		}
		if (prev.nodeType == 8) par.removeChild(prev); // remove comment
		prev = newprev;
	  }
	  while (next) {
		var newnext = next.nextSibling;
		if (next.nodeType == 3 && next.nodeValue) {
		  while (next.nodeValue.length) {
			var cc = next.nodeValue.charCodeAt(0);
			if (cc == 9 || cc == 10 || cc == 32) {
			  next.nodeValue = next.nodeValue.slice(1);
			} else {
			  break;
			}
		  }
		}
		if (next.nodeType == 8) par.removeChild(next); // remove comment
		next = newnext;
	  }

	});
  }
  //end of closure
})(jQuery);



function closeContentsMenu() {
	$('.viewercontmenu').data('status','close').fadeOut(250, 'easeOutQuad');
}


(function($) { //create closure
	$.fn.toolbarView = function(options) {
		this.each(function() {
			var cont = $(this),
			inner = cont.find('.toolbarInner'),
			toolbarClosed = cont.find('.toolbarClosed'),
			menulink = cont.find('.b-text_contents'),
			menu = cont.find('.viewercontmenu');

			var searchbutton = $('#viewFindTrigger'), searchbar = $('#findbar');


			$('.js_scroll', menu).mCustomScrollbar({
				live:true,
				axis:"yx",
				advanced:{
					updateOnContentResize: true
				}
			});

			function openSearchBar(){
				searchbar.data('status','open').removeClass('hidden').fadeIn(250, 'easeOutQuad', function(){
					//$('.textLayer').css('z-index',3);
					document.getElementById('findInput').focus();
				});
			}

			function closeSearchBar(fast){
				searchbar.data('status','close').fadeOut(((fast)?50:250), 'easeOutQuad', function(){
					//$('.textLayer').css('z-index','');
				});
			}

			searchbutton.on('click', function(e){
				e.preventDefault();
				if (searchbar.data('status')=='open') {
					closeSearchBar();
				} else {
					closeMenuBar(true);
					openSearchBar();
				}
			});

			function openMenuBar(){
				menu.data('status','open').fadeIn(250, 'easeOutQuad', function(){
					createTableOfContents();
					scrollToThumbIfNeeded();
				});
			}

			function closeMenuBar(fast){
				menu.data('status','close').fadeOut(((fast)?50:250), 'easeOutQuad', function(){
				});
			}

			menulink.on('click', function(e){
				e.preventDefault();
				if (menu.data('status')=='open') {
					closeMenuBar();
				} else {
					openMenuBar();
					closeSearchBar(true);
				}
			});

		$(document).on('click', function(e) {
			var targ = $(e.target);
			if (!targ.hasClass('toolbar') 
				&& targ.parents('.toolbar').length<1 
				&& targ.parents('.b-addform').length<1 
				) {
				if (menu.css('display')=='block') menu.data('status','open').fadeOut(50, 'easeOutQuad');
				if (searchbar.css('display')=='block') searchbar.data('status','open').fadeOut(50, 'easeOutQuad', function(){/* $('.textLayer').css('z-index',''); */});
				cont.addClass('closed');
				inner.slideUp(200, 'easeInOutQuad', function(){
					toolbarClosed.show(100, 'easeOutQuad');
				});
			}
		});

		var toolbarBookmark = cont.find('.toolbarBookmark');
		toolbarBookmark.on('click', function(e){
			e.preventDefault()
			e.stopPropagation();
			var A = $(this);
			if (!PDFView) return;

			if (A.hasClass('bmchecked')) { // страница уже в закладках
				return false;
				A.removeClass('bmchecked');
			} else { // страница еще не в закладках, надо добавить
				var pagenum = PDFView.page;
				var pageitem = $('#pageContainer'+pagenum);
				var textLayer = pageitem.find('.textLayer');
				var textitems = $('div', textLayer);
				var numstr = 0, currH = 0, text = '';
				var start = 0;
				
				for (var i=0; i<textitems.length; i++) {
					var item = $(textitems.get(i));
					var top = parseInt(item.css('top'));
					if (top>start) {
						if (top>currH) { currH = top; numstr++; }
						text += item.text();
					}
						if (numstr > 5) break;
				}

				var bookmark = {
					"page":pagenum,
					"preview": text
				}

				$.post(A.attr('href'), bookmark, function(data){
					var result = $(data).appendTo('body');
					bookmark.id = result.data('id');
					// отправить в модель приложения 
					var scope = angular.element(document.getElementById('mainContainer')).scope();
					scope.$apply(function(){
						scope.bookmarks.push(bookmark);
					});
					result.removeClass('hidden');
					var w = result.width(), h = result.height();
					var wnd = $(window), ww = wnd.width(), wh = wnd.height();
					var Apos = A.offset();
					result.css({'z-index':1980,'left': Apos.left-w/2, 'top': Apos.top, 'opacity': 1});
					window.setTimeout(function(){
						result.animate({'opacity': 0, 'top':wh-h}, 500, 'easeInQuad', function(){
							result.remove();
						});
					}, 700);
					A.addClass('bmchecked');
				});
			}
		});
		cont.on('click', function(e){
			e.stopPropagation();
			toolbarClosed.hide(100, 'easeOutQuad');
			cont.removeClass('closed');
			inner.slideDown(200, 'easeInOutQuad', function(){
				if (menu.data('status')=='open') {
					menu.fadeIn(50, 'easeOutQuad');
				}
				if (searchbar.data('status')=='open') {
					openSearchBar()
				}
			});
		});

	});
  }
  //end of closure
})(jQuery);