<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-map rel mappage">
    <form method="get" action="." class="b-map_search_filtr row">
        <div class="col-md-6 b-cytyinfo">
            <span class="b-map_search_txt"><?=Loc::getMessage('FIND_CLOSEST_LIBRARY')?></span>
            <? if ($arResult['CITY']): ?>
                <a class="b-citycheck setLinkPoint"
                    data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>"
                    data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>"
                    href="#"><?= $arResult['CITY']["UF_CITY_NAME"] ?></a>
            <?endif?>
            <a href="/workplaces/cities/"> <?=Loc::getMessage('CHANGE')?></a>
        </div>
        <div class="col-md-6 b-citysearch">
            <input type="text"
                   class="b-map_tb ui-autocomplete-input searchonmap form-control" name="mapq"
                   id="qcity" autocomplete="off"
                   placeholder="<?=Loc::getMessage('ADDRESS_OR_NAME')?>"
                   value="<?= isset($_REQUEST['mapq']) ? htmlentities($_REQUEST['mapq']) : null ?>" />

            <span class="button_d" title="<?=Loc::getMessage('CLEAR_SEARCH');?>"><span class="fa fa-times"></span></span>
            <button type="submit" class="btn btn-default button_b" value="<?=Loc::getMessage('SEARCH_BTN');?>" name="submit"
                    title="<?=Loc::getMessage('SEARCH_START');?>"><span class="fa fa-search"></span></button>
        </div>
    </form>
    <div class="libs-map-widget lmw"> 
        <div class="lmw-map">       
            <div class="ymap libraries-map" id="ymap"
                 data-path="/local/tools/map/workplaces.php<?if ($_REQUEST['city_id']):?>?city_id=<?=$_REQUEST['city_id']?><?endif;?>"
                 data-lat="<?= ($arResult["CITY"]["UF_POS"][1]) ? : '55.753676'; ?>"
                 data-lng="<?= ($arResult["CITY"]["UF_POS"][0]) ? : '37.619899'; ?>"
                 data-zoom="<?= !empty($arResult['CITY']) ? '10' : '5'; ?>"
                 data-show-nearly="<?= (isset($_REQUEST['show-nearly']) && $_REQUEST['show-nearly'] === 'Y') ? 1 : 0 ?>"
                 data-find-location="<?= $arResult["FIND_LOCATION"] ?>"
            ></div>
        </div>
        <div class="lmw-bar" id="library-list">
            <div class="libs-list-title">Результаты поиска</div>
            <div data-libs-sort-result class="libs-list-view">
                <section class="pagenav-info" data-options-holder></section>
                <ol data-lsr-list class="b-elar_usrs_list">
                    <?$i = 1;?>
                    <? foreach ($arResult['items'] as $key=>$arLibrary) { ?>
                    <li class="clearfix" itemtype="http://schema.org/Organization" itemscope="" data-map-link data-lat="<?=$arLibrary['UF_POS'][1]?>" data-lng="<?=$arLibrary['UF_POS'][0]?>">
                        <span class="num"><?=$i?>.</span>
                        <?$i++?>
                        <div class="b-elar_name">
                            <?if ($arLibrary['link']):?>
                                <a class="b-elar_name_txt" href="<?= $arLibrary['link'] ?>"><?= $arLibrary['UF_NAME']?></a>
                            <?else:?>
                                <?= $arLibrary['UF_NAME']?>
                            <?endif?>
                            <br>
                            <?if ($arLibrary['link']):?><?=Loc::getMessage('MEMBER_OF_NEL')?><?endif;?>
                            <?=Loc::getMessage('VRR')?>
                        </div>
                        <div class="b-elar_address">
                            <div class="b-elar_address_txt" itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address">
                                <div class="icon">
                                    <div class="b-hint">
                                        <?=Loc::getMessage('ADDRESS')?>
                                    </div>
                                </div>
                                <?=$arLibrary['UF_ADRESS']?>
                                <br>
                                <a href="#" class="b-elar_address_link" data-lat="<?=$arLibrary['UF_POS'][1]?>" data-lng="<?=$arLibrary['UF_POS'][0]?>"><?=Loc::getMessage('SHOW_ON_THE_MAP')?></a>
                            </div>
                        </div>
                    </li>
                    <? } ?>
                </ol>
            </div>
        </div>
    </div>
</div><!-- /.b-map -->

<!--script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script-->
<script src="//api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
