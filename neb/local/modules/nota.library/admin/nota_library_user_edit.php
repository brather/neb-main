<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CModule::IncludeModule('iblock');
CModule::IncludeModule('nota.library');

use Nota\Library;


ClearVars();


$ID = intval($_REQUEST["ID"]);

$uid = $USER->GetID();

$PROPERTY_ID = "USER";
$message = null;
$strError = '';
$res = true;

$strRedirect_admin = BX_ROOT."/admin/nota_library_user_list.php?lang=".LANG;
$strRedirect = BX_ROOT."/admin/nota_library_user_edit.php?lang=".LANG;

if($COPY_ID<=0)
{
    $arUserGroups = CUser::GetUserGroup($ID);
}


$editable = $USER->IsAdmin() || CNotaLibrary::isAdmin();



$aTabs = array();
$aTabs[] = array("DIV" => "edit1", "TAB" => "Данные пользователя", "ICON"=>"main_user_edit", "TITLE"=>"Данные пользователя");

$tabControl = new CAdminForm("user_edit", $aTabs);

if(
    $_SERVER["REQUEST_METHOD"]=="POST"
    && (
        $_REQUEST["save"]<>''
        || $_REQUEST["apply"]<>''
        || $_REQUEST["Update"]=="Y"
        || $_REQUEST["save_and_add"]<>''
    )
    && $editable
    && check_bitrix_sessid()
)
{
    if(COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
    {
        //possible encrypted user password
        $sec = new CRsaSecurity();
        if(($arKeys = $sec->LoadKeys()))
        {
            $sec->SetKeys($arKeys);
            $errno = $sec->AcceptFromForm(array('NEW_PASSWORD', 'NEW_PASSWORD_CONFIRM'));
            if($errno == CRsaSecurity::ERROR_SESS_CHECK)
                $strError .= GetMessage("main_profile_sess_expired").'<br />';
            elseif($errno < 0)
                $strError .= GetMessage("main_profile_decode_err", array("#ERRCODE#"=>$errno)).'<br />';
        }
    }

    if($strError == '')
    {
        $user = new CUser;

        $arPERSONAL_PHOTO = $_FILES["PERSONAL_PHOTO"];
        $arWORK_LOGO = $_FILES["WORK_LOGO"];

        $arUser = false;
        if($ID>0)
        {
            $dbUser = CUser::GetById($ID);
            $arUser = $dbUser->Fetch();
        }

        if($arUser)
        {
            $arPERSONAL_PHOTO["old_file"] = $arUser["PERSONAL_PHOTO"];
            $arPERSONAL_PHOTO["del"] = $_POST["PERSONAL_PHOTO_del"];

            $arWORK_LOGO["old_file"] = $arUser["WORK_LOGO"];
            $arWORK_LOGO["del"] = $_POST["WORK_LOGO_del"];
        }

        $arFields = array(
            "TITLE" => $_POST["TITLE"],
            "NAME" => $_POST["NAME"],
            "LAST_NAME" => $_POST["LAST_NAME"],
            "SECOND_NAME" => $_POST["SECOND_NAME"],
            "EMAIL" => $_POST["EMAIL"],
            "LOGIN" => $_POST["LOGIN"],
            "PERSONAL_PROFESSION" => $_POST["PERSONAL_PROFESSION"],
            "PERSONAL_WWW" => $_POST["PERSONAL_WWW"],
            "PERSONAL_ICQ" => $_POST["PERSONAL_ICQ"],
            "PERSONAL_GENDER" => $_POST["PERSONAL_GENDER"],
            "PERSONAL_BIRTHDAY" => $_POST["PERSONAL_BIRTHDAY"],
            "PERSONAL_PHOTO" => $arPERSONAL_PHOTO,
            "PERSONAL_PHONE" => $_POST["PERSONAL_PHONE"],
            "PERSONAL_FAX" => $_POST["PERSONAL_FAX"],
            "PERSONAL_MOBILE" => $_POST["PERSONAL_MOBILE"],
            "PERSONAL_PAGER" => $_POST["PERSONAL_PAGER"],
            "PERSONAL_STREET" => $_POST["PERSONAL_STREET"],
            "PERSONAL_MAILBOX" => $_POST["PERSONAL_MAILBOX"],
            "PERSONAL_CITY" => $_POST["PERSONAL_CITY"],
            "PERSONAL_STATE" => $_POST["PERSONAL_STATE"],
            "PERSONAL_ZIP" => $_POST["PERSONAL_ZIP"],
            "PERSONAL_COUNTRY" => $_POST["PERSONAL_COUNTRY"],
            "PERSONAL_NOTES" => $_POST["PERSONAL_NOTES"],
            "WORK_COMPANY" => $_POST["WORK_COMPANY"],
            "WORK_DEPARTMENT" => $_POST["WORK_DEPARTMENT"],
            "WORK_POSITION" => $_POST["WORK_POSITION"],
            "WORK_WWW" => $_POST["WORK_WWW"],
            "WORK_PHONE" => $_POST["WORK_PHONE"],
            "WORK_FAX" => $_POST["WORK_FAX"],
            "WORK_PAGER" => $_POST["WORK_PAGER"],
            "WORK_STREET" => $_POST["WORK_STREET"],
            "WORK_MAILBOX" => $_POST["WORK_MAILBOX"],
            "WORK_CITY" => $_POST["WORK_CITY"],
            "WORK_STATE" => $_POST["WORK_STATE"],
            "WORK_ZIP" => $_POST["WORK_ZIP"],
            "WORK_COUNTRY" => $_POST["WORK_COUNTRY"],
            "WORK_PROFILE" => $_POST["WORK_PROFILE"],
            "WORK_LOGO" => $arWORK_LOGO,
            "WORK_NOTES" => $_POST["WORK_NOTES"],
            "AUTO_TIME_ZONE" => ($_POST["AUTO_TIME_ZONE"] == "Y" || $_POST["AUTO_TIME_ZONE"] == "N"? $_POST["AUTO_TIME_ZONE"] : ""),
            "XML_ID" => $_POST["XML_ID"],
            "UF_LIBRARY" => $_POST["UF_LIBRARY"],
        );

        if(isset($_POST["TIME_ZONE"]))
            $arFields["TIME_ZONE"] = $_POST["TIME_ZONE"];

        if($editable)
        {
            if($_POST["LID"] <> '')
                $arFields["LID"] = $_POST["LID"];

            if(is_set($_POST, 'EXTERNAL_AUTH_ID'))
                $arFields['EXTERNAL_AUTH_ID'] = $_POST["EXTERNAL_AUTH_ID"];

            if ($ID == 1 && $COPY_ID <= 0)
                $arFields["ACTIVE"] = "Y";
            else
                $arFields["ACTIVE"] = $_POST["ACTIVE"];

            if(isset($_REQUEST["GROUP_ID_NUMBER"]))
            {
                $GROUP_ID_NUMBER = intval($_REQUEST["GROUP_ID_NUMBER"]);
                $GROUP_ID = array();
                $ind = -1;
                for ($i = 0; $i <= $GROUP_ID_NUMBER; $i++)
                {
                    if (${"GROUP_ID_ACT_".$i} == "Y")
                    {
                        $gr_id = intval(${"GROUP_ID_".$i});

                        if($gr_id == 1 && !$USER->IsAdmin())
                            continue;

                        if ($USER->CanDoOperation('edit_subordinate_users') && !$USER->CanDoOperation('edit_all_users') && !in_array($gr_id, $arUserSubordinateGroups))
                            continue;

                        $ind++;
                        $GROUP_ID[$ind]["GROUP_ID"] = $gr_id;
                        $GROUP_ID[$ind]["DATE_ACTIVE_FROM"] = ${"GROUP_ID_FROM_".$i};
                        $GROUP_ID[$ind]["DATE_ACTIVE_TO"] = ${"GROUP_ID_TO_".$i};
                    }
                }

                if ($ID == "1" && $COPY_ID<=0)
                {
                    $ind++;
                    $GROUP_ID[$ind]["GROUP_ID"] = 1;
                    $GROUP_ID[$ind]["DATE_ACTIVE_FROM"] = false;
                    $GROUP_ID[$ind]["DATE_ACTIVE_TO"] = false;
                }

                $arFields["GROUP_ID"]=$GROUP_ID;
            }

            if (($editable && $ID!=$USER->GetID()) || $USER->IsAdmin())
                $arFields["ADMIN_NOTES"] = $_POST["ADMIN_NOTES"];
        }

        if($_POST["NEW_PASSWORD"] <> '')
        {
            $arFields["PASSWORD"] = $_POST["NEW_PASSWORD"];
            $arFields["CONFIRM_PASSWORD"] = $_POST["NEW_PASSWORD_CONFIRM"];
        }

        if($ID>0 && $COPY_ID<=0)
        {
            $res = $user->Update($ID, $arFields, true);
        }
        else
        {
            $ID = $user->Add($arFields);
            $res = ($ID > 0);
            if(COption::GetOptionString("main", "event_log_register", "N") === "Y" && $res)
            {
                $res_log["user"] = ($_POST["NAME"] != "" || $_POST["LAST_NAME"] != "") ? trim($_POST["NAME"]." ".$_POST["LAST_NAME"]) : $_POST["LOGIN"];
                CEventLog::Log("SECURITY", "USER_REGISTER", "main", $ID, serialize($res_log));
            }
            $new = "Y";
        }

        $strError .= $user->LAST_ERROR;
        if ($APPLICATION->GetException())
        {
            $err = $APPLICATION->GetException();
            $strError .= $err->GetString();
            $APPLICATION->ResetException();
        }
    }

    if($strError == '' && $ID>0)
    {
        if(is_array($_REQUEST["profile_module_id"]) && count($_REQUEST["profile_module_id"])>0)
        {
            $db_opt_res = CModule::GetList();
            while ($opt_res = $db_opt_res->Fetch())
            {
                if (in_array($opt_res["ID"], $_REQUEST["profile_module_id"]))
                {
                    $mdir = $opt_res["ID"];
                    if (file_exists($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/".$mdir) && is_dir($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/".$mdir))
                    {
                        $ofile = $_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/".$mdir."/options_user_settings_set.php";
                        if (file_exists($ofile))
                        {
                            $MODULE_RIGHT = $APPLICATION->GetGroupRight($mdir);
                            if ($MODULE_RIGHT>="R")
                            {
                                include($ofile);
                                $mname = str_replace(".", "_", $mdir);
                                if(!${$mname."_res"})
                                {
                                    $res = false;
                                    if($APPLICATION->GetException())
                                    {
                                        $err = $APPLICATION->GetException();
                                        $strError .= $err->GetString();
                                        $APPLICATION->ResetException();
                                    }
                                    else
                                    {
                                        $strError .= ${$mname."WarningTmp"};
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if($strError == '' && $res)
        {
            if($_POST["user_info_event"] == "Y")
            {
                $arMess = false;
                $res_site = CSite::GetByID($_POST["LID"]);
                if($res_site_arr = $res_site->Fetch())
                    $arMess = IncludeModuleLangFile(__FILE__, $res_site_arr["LANGUAGE_ID"], true);

                if($new == "Y")
                    CUser::SendUserInfo($ID, $_POST["LID"], "Администратор зарегистрировал вас на сайте", true);
                else
                    CUser::SendUserInfo($ID, $_POST["LID"], "Администратор сайта изменил ваши регистрационные данные.", true);
            }

            if($USER->CanDoOperation('edit_all_users') || $USER->CanDoOperation('edit_subordinate_users') || ($USER->CanDoOperation('edit_own_profile') && $ID==$uid))
            {
                if($_POST["save"] <> '')
                    LocalRedirect($strRedirect_admin);
                elseif($_POST["apply"] <> '')
                    LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
                elseif(strlen($save_and_add)>0)
                    LocalRedirect($strRedirect."&ID=0&".$tabControl->ActiveTabParam());
            }
            elseif($new=="Y")
                LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
        }
    }
}

$str_GROUP_ID = array();

$user = CUser::GetByID($ID);
if(!$user->ExtractFields("str_"))
{
    $ID = 0;
    $str_ACTIVE = "Y";
    $str_LID = CSite::GetDefSite();
}
else
{
    $dbUserGroup = CUser::GetUserGroupList($ID);
    while ($arUserGroup = $dbUserGroup->Fetch())
    {
        $str_GROUP_ID[intval($arUserGroup["GROUP_ID"])]["DATE_ACTIVE_FROM"] = $arUserGroup["DATE_ACTIVE_FROM"];
        $str_GROUP_ID[intval($arUserGroup["GROUP_ID"])]["DATE_ACTIVE_TO"] = $arUserGroup["DATE_ACTIVE_TO"];
    }
}

if($strError <> '' || !$res)
{
    $save_PERSONAL_PHOTO = $str_PERSONAL_PHOTO;
    $save_WORK_LOGO = $str_WORK_LOGO;

    $DB->InitTableVarsForEdit("b_user", "", "str_");

    $str_PERSONAL_PHOTO = $save_PERSONAL_PHOTO;
    $str_WORK_LOGO = $save_WORK_LOGO;

    $GROUP_ID_NUMBER = intval($_REQUEST["GROUP_ID_NUMBER"]);
    $str_GROUP_ID = array();
    for ($i = 0; $i <= $GROUP_ID_NUMBER; $i++)
    {
        if (${"GROUP_ID_ACT_".$i} == "Y")
        {
            $str_GROUP_ID[intval(${"GROUP_ID_".$i})]["DATE_ACTIVE_FROM"] = ${"GROUP_ID_FROM_".$i};
            $str_GROUP_ID[intval(${"GROUP_ID_".$i})]["DATE_ACTIVE_TO"] = ${"GROUP_ID_TO_".$i};
        }
    }
}

if($ID>0 && $COPY_ID<=0)
    $APPLICATION->SetTitle("Редактирование пользователя", array("#ID#"=>$ID));
else
    $APPLICATION->SetTitle("Добавление пользователя");

require_once ($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");

if ($e = $APPLICATION->GetException())
    $message = new CAdminMessage("Ошибка сохранения", $e);
if($message)
    echo $message->Show();
if($strError <> '')
{
    $e = new CAdminException(array(array('text' => $strError)));
    $message = new CAdminMessage("Ошибка сохранения", $e);
    echo $message->Show();
    //echo CAdminMessage::ShowMessage(Array("MESSAGE"=>$strError, "HTML"=>true, "TYPE"=>"ERROR"));
}

//We have to explicitly call calendar and editor functions because
//first output may be discarded by form settings
$tabControl->BeginPrologContent();

$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();
?>
<?=bitrix_sessid_post()?>
<input type="hidden" name="Update" value="Y">
<input type="hidden" name="UF_LIBRARY" value=<?=CNotaLibrary::getLibraryID()?>>
<?if(!$ID):?>
    <input type="checkbox" name="user_info_event" value="Y" id="bx_user_info_event" class="adm-designed-checkbox">
<?endif;?>
<?
$tabControl->EndEpilogContent();


$tabControl->Begin(array(
    "FORM_ACTION" => $APPLICATION->GetCurPage()."?ID=".intval($ID)."&lang=".LANG,
));

$tabControl->BeginNextFormTab();


if($ID!='1' && $ID!=$USER->GetID()):
    $tabControl->BeginCustomField("ACTIVE", "Активен:");
    ?>
    <tr>
        <td><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td>
            <?if($canSelfEdit):?>
                <input type="checkbox" name="ACTIVE" value="Y"<?if($str_ACTIVE=="Y") echo " checked"?>>
            <?else:?>
                <input type="checkbox" <?if($str_ACTIVE=="Y") echo " checked"?> disabled>
                <input type="hidden" name="ACTIVE" value="<?=$str_ACTIVE;?>">
            <?endif;?>
    </tr>
    <?
    $tabControl->EndCustomField("ACTIVE", '<input type="hidden" name="ACTIVE" value="'.$str_ACTIVE.'">');
else:
    $tabControl->HideField('ACTIVE');
endif;

$emailRequired = (COption::GetOptionString("main", "new_user_email_required", "Y") <> "N");

$tabControl->AddEditField("NAME", "Имя:", false, array("size"=>30), $str_NAME);
$tabControl->AddEditField("LAST_NAME", "Фамилия:", false, array("size"=>30), $str_LAST_NAME);
$tabControl->AddEditField("SECOND_NAME", "Отчество:", false, array("size"=>30), $str_SECOND_NAME);
$tabControl->AddEditField("EMAIL", "E-Mail:", $emailRequired, array("size"=>30), $str_EMAIL);
$tabControl->AddEditField("LOGIN", "Логин (мин. 3 символа):", true, array("size"=>30), $str_LOGIN);
$tabControl->BeginCustomField("PASSWORD", "Новый пароль (мин. 6 символов):", true);

$bSecure = false;
if(!CMain::IsHTTPS() && COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
{
    $sec = new CRsaSecurity();
    if(($arKeys = $sec->LoadKeys()))
    {
        $sec->SetKeys($arKeys);
        $sec->AddToForm('user_edit_form', array('NEW_PASSWORD', 'NEW_PASSWORD_CONFIRM'));
        $bSecure = true;
    }
}
?>
    <tr id="bx_pass_row" style="display:<?=($str_EXTERNAL_AUTH_ID <> ''? 'none':'')?>;"<?if($ID<=0 || $COPY_ID>0):?> class="adm-detail-required-field"<?endif?>>
        <td>Новый пароль:<sup><span class="required">1</span></sup></td>
        <td><input type="password" name="NEW_PASSWORD" size="30" maxlength="50" value="<? echo htmlspecialcharsbx($NEW_PASSWORD) ?>" autocomplete="off" style="vertical-align:middle;">
            <?if($bSecure):?>
                <span class="bx-auth-secure" id="bx_auth_secure" title="Перед отправкой формы пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде." style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
                <noscript>
				<span class="bx-auth-secure" title="Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                </noscript>
                <script type="text/javascript">
                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                </script>
            <?endif?>
        </td>
    </tr>
    <tr id="bx_pass_confirm_row" style="display:<?=($str_EXTERNAL_AUTH_ID <> ''? 'none':'')?>;"<?if($ID<=0 || $COPY_ID>0):?> class="adm-detail-required-field"<?endif?>>
        <td>Подтверждение нового пароля:</td>
        <td><input type="password" name="NEW_PASSWORD_CONFIRM" size="30" maxlength="50" value="<? echo htmlspecialcharsbx($NEW_PASSWORD_CONFIRM) ?>" autocomplete="off"></td>
    </tr>
<?
$tabControl->EndCustomField("PASSWORD");

if(!$ID):
    $tabControl->BeginCustomField("LID", "Сайт по умолчанию для уведомлений:");
    ?>
    <tr>
        <td><?echo $tabControl->GetCustomLabelHTML()?></td>
        <?if(!$editable) $dis = " disabled"?>
        <td><?=CSite::SelectBox("LID", $str_LID, "", "", "style=\"width:220px\"".$dis);?></td>
    </tr>
    <?
    $tabControl->EndCustomField("LID", '<input type="hidden" name="LID" value="'.$str_LID.'">');

    $params = array('id="bx_user_info_event"');
    if(!$editable || $str_EXTERNAL_AUTH_ID <> '')
    {
        $params[] = "disabled";
    }
    $tabControl->AddCheckBoxField("user_info_event", "Оповестить пользователя:", false, "Y", ($_REQUEST["user_info_event"]=="Y"), $params);
endif;
?>



<?
    $allowedGroups = CNotaLibrary::getGroupsArrayByCode(array(UGROUP_LIB_CODE_ADMIN, UGROUP_LIB_CODE_CONTROLLER, UGROUP_LIB_CODE_EDITOR));
    $tabControl->BeginCustomField("GROUP_ID", "Группы пользователя");
    ?>
    <tr>
        <td colspan="2" align="center">
                <?
                $ind = -1;
                $dbGroups = CGroup::GetList(($b = "c_sort"), ($o = "asc"), array("ANONYMOUS" => "N"));
                while ($arGroups = $dbGroups->Fetch())
                {
                    if (!$USER->CanDoOperation('edit_all_users') && $USER->CanDoOperation('edit_subordinate_users') && !in_array($arGroups["ID"], $arUserSubordinateGroups) || $arGroups["ID"] == 2)
                        continue;
                    if($arGroups["ID"]==1 && !$USER->IsAdmin())
                        continue;
                    $ind++;
                    ?>

                    <?if(in_array($arGroups["ID"], $allowedGroups)):?>
                        <tr>
                            <td>
                                <input type="hidden" name="GROUP_ID_<?=$ind?>" value="<?=$arGroups["ID"]?>" /><input type="checkbox" name="GROUP_ID_ACT_<?=$ind?>" id="GROUP_ID_ACT_ID_<?=$ind?>" value="Y"<?
                                if (array_key_exists($arGroups["ID"], $str_GROUP_ID))
                                    echo " checked=\"checked\"";
                                ?> />
                            </td>
                            <td class="align-left">
                                <label for="GROUP_ID_ACT_ID_<?= $ind ?>"><?=htmlspecialcharsbx($arGroups["NAME"])?> [<a href="/bitrix/admin/group_edit.php?ID=<?=$arGroups["ID"]?>&lang=<?=LANGUAGE_ID?>" title="Просмотреть параметры группы пользователей"><?echo intval($arGroups["ID"])?></a>]</label>
                            </td>
                        </tr>
                    <?else:?>
                        <input type="hidden" name="GROUP_ID_<?=$ind?>" value="<?=$arGroups["ID"]?>" />
                        <?if(array_key_exists($arGroups["ID"], $str_GROUP_ID)):?>
                            <input type="hidden" name="GROUP_ID_ACT_<?=$ind?>" id="GROUP_ID_ACT_ID_<?=$ind?>" value="Y"/>
                        <?endif;?>
                    <?endif;?>
                <?
                }
                ?>
            <input type="hidden" name="GROUP_ID_NUMBER" value="<?= $ind ?>">
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("GROUP_ID");
?>
<?

$tabControl->Buttons(array(
    "disabled" => !$editable,
    "btnSaveAndAdd" => false,
    "back_url" => "user_admin.php?lang=".LANGUAGE_ID,
));

$tabControl->Show();

$tabControl->ShowWarnings($tabControl->GetName(), $message);
?>

<?if(!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
    <?echo BeginNote();?>
    <span class="required">1</span> <?$GROUP_POLICY = CUser::GetGroupPolicy($ID);echo $GROUP_POLICY["PASSWORD_REQUIREMENTS"];?><br>
    <?echo EndNote();?>
<?endif;?>

<?
require_once ($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");

