<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150921132427 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
CREATE TABLE `neb_stat_book_open` (
  `ID`           INT(11) UNSIGNED        NOT NULL AUTO_INCREMENT,
  `BOOK_ID`      VARCHAR(255)
                 COLLATE utf8_unicode_ci NOT NULL,
  `REFERER`      VARCHAR(1024)
                 COLLATE utf8_unicode_ci,
  `SEARCH_QUERY` VARCHAR(1024)
                 COLLATE utf8_unicode_ci,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `BOOK_ID` (`BOOK_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
DROP TABLE neb_stat_book_open;
SQL
        );
    }
}
