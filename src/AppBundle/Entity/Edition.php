<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 11:36
 */

namespace AppBundle\Entity;

use AppBundle\Traits\Entity\ExaleadEntityTrait;
use JMS\Serializer\Annotation as Serialization;

class Edition implements SearchResultEntityInterface
{
    use ExaleadEntityTrait;

    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $fileSize;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $ispart;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $idbook;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $namebook;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $startpageparts;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $finishpageparts;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $rangepageparts;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $subtitle;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $dateindex;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $year;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $idlibrary;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $publishyear;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $librarytype;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $library;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $publisher;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $idparent;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $idparent2;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $responsibility;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $publishplace;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $countpages;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $isprotected;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $filename;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $viewurl;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $fileName;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $id;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $idAbis;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $isSkbr;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $idSkbr;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $series;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $annotation;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $publicurl;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $bbk;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $contentnotes;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $edition;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $isbn;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $issn;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $notes;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $placepublish;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $udk;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $isschool;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $dates;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $fundnumber;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $fundname;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $inventorynumber;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $inventoryname;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $unitnumber;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $unitname;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $description;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $material;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $size;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $positionpart;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $text;

    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $title;

    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $titleSource;

    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $titleHtml;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $bbkBbkfull;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $bbkfull;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $udkfull;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */

    public $authorbook;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $authorbookSource;
    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $authorbookHtml;

    /**
     * @var
     * @Serialization\Groups({"list", "details"})
     */
    public $collection;

    /**
     * @param $value
     */
    public function setAuthorbook($value)
    {
        $this->applySegmentsAttribute('authorbook', $value);
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->applySegmentsAttribute('title', $value);
    }
}