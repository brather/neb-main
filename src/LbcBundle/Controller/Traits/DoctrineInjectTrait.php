<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 13:24
 */

namespace LbcBundle\Controller\Traits;

use Doctrine\Bundle\DoctrineBundle\Registry;
use JMS\DiExtraBundle\Annotation as DI;

trait DoctrineInjectTrait
{
    /**
     * @DI\Inject("doctrine")
     * @var Registry
     */
    public $doctrine;
}