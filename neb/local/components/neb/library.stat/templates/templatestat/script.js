// JavaScript Document
/*$(function(){

	$('ul.uleditstat').dcAccordion({
		classParent  : 'active-parent',
		classArrow: 'icon',
		eventType: 'click',
		autoClose: true,
		saveState: true,
		disableLink: true,
		showCount: false,
		speed: 'fast',
		cookie  : 'uleditstat',
	});

});*/
//classParent � ??����� ������������� ������ ����
//classActive � ����� ������ ��������� ������������� ������ ����
//classArrow � ����� ���� span (��� ������, ��������� ���� ���� �������� ������)
//classCount � ����� ���� span (��� ������ ���������� ����������)
//eventType � ������� ��� ������� ����� ����������� ��������� (click ��� hover)
//hoverDelay � �������� ��� ������� hover
//menuClose � ���� true � ������� hover, �� ��� ��������� �������, ��� ���� ���������
//autoClose � ��� �������� true, ������ ���� ����� ����� ���������
//speed � �������� ��������
//saveState � ��� �������� true, ��������� �������� ����� � Cookie
//disableLink � ��������� ��� ������ ������������ �������
//showCount � ���������� ��� ��� ���������� ����������
//cookie � ��� ����, ���� �������� ���������� �������


(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as anonymous module.
		define(['jquery'], factory);
	} else {
		// Browser globals.
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
		} catch(e) {
			return;
		}

		try {
			// If we can't parse the cookie, ignore it, it's unusable.
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write
		if (value !== undefined && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) !== undefined) {
			// Must not alter options, thus extending a fresh object...
			$.cookie(key, '', $.extend({}, options, { expires: -1 }));
			return true;
		}
		return false;
	};

}));


// (function($){$.fn.dcAccordion=function(options){var defaults={classParent:'dcjq-parent',classActive:'active',classArrow:'dcjq-icon',classCount:'dcjq-count',classExpand:'dcjq-current-parent',eventType:'click',hoverDelay:300,menuClose:true,autoClose:true,autoExpand:false,speed:'slow',saveState:true,disableLink:true,showCount:false,cookie:'dcjq-accordion'};var options=$.extend(defaults,options);this.each(function(options){var obj=this;setUpAccordion();if(defaults.saveState==true){checkCookie(defaults.cookie,obj)}if(defaults.autoExpand==true){$('li.'+defaults.classExpand+' > a').addClass(defaults.classActive)}resetAccordion();if(defaults.eventType=='hover'){var config={sensitivity:2,interval:defaults.hoverDelay,over:linkOver,timeout:defaults.hoverDelay,out:linkOut};$('li a',obj).hoverIntent(config);var configMenu={sensitivity:2,interval:1000,over:menuOver,timeout:1000,out:menuOut};$(obj).hoverIntent(configMenu);if(defaults.disableLink==true){$('li a',obj).click(function(e){if($(this).siblings('ul').length>0){e.preventDefault()}})}}else{$('li a',obj).click(function(e){$activeLi=$(this).parent('li');$parentsLi=$activeLi.parents('li');$parentsUl=$activeLi.parents('ul');if(defaults.disableLink==true){if($(this).siblings('ul').length>0){e.preventDefault()}}if(defaults.autoClose==true){autoCloseAccordion($parentsLi,$parentsUl)}if($('> ul',$activeLi).is(':visible')){$('ul',$activeLi).slideUp(defaults.speed);$('a',$activeLi).removeClass(defaults.classActive)}else{$(this).siblings('ul').slideToggle(defaults.speed);$('> a',$activeLi).addClass(defaults.classActive)}if(defaults.saveState==true){createCookie(defaults.cookie,obj)}})}function setUpAccordion(){$arrow='<span class="'+defaults.classArrow+'"></span>';var classParentLi=defaults.classParent+'-li';$('> ul',obj).show();$('li',obj).each(function(){if($('> ul',this).length>0){$(this).addClass(classParentLi);$('> a',this).addClass(defaults.classParent).append($arrow)}});$('> ul',obj).hide();if(defaults.showCount==true){$('li.'+classParentLi,obj).each(function(){if(defaults.disableLink==true){var getCount=parseInt($('ul a:not(.'+defaults.classParent+')',this).length)}else{var getCount=parseInt($('ul a',this).length)}$('> a',this).append(' <span class="'+defaults.classCount+'">('+getCount+')</span>')})}}function linkOver(){$activeLi=$(this).parent('li');$parentsLi=$activeLi.parents('li');$parentsUl=$activeLi.parents('ul');if(defaults.autoClose==true){autoCloseAccordion($parentsLi,$parentsUl)}if($('> ul',$activeLi).is(':visible')){$('ul',$activeLi).slideUp(defaults.speed);$('a',$activeLi).removeClass(defaults.classActive)}else{$(this).siblings('ul').slideToggle(defaults.speed);$('> a',$activeLi).addClass(defaults.classActive)}if(defaults.saveState==true){createCookie(defaults.cookie,obj)}}function linkOut(){}function menuOver(){}function menuOut(){if(defaults.menuClose==true){$('ul',obj).slideUp(defaults.speed);$('a',obj).removeClass(defaults.classActive);createCookie(defaults.cookie,obj)}}function autoCloseAccordion($parentsLi,$parentsUl){$('ul',obj).not($parentsUl).slideUp(defaults.speed);$('a',obj).removeClass(defaults.classActive);$('> a',$parentsLi).addClass(defaults.classActive)}function resetAccordion(){$('ul',obj).hide();$allActiveLi=$('a.'+defaults.classActive,obj);$allActiveLi.siblings('ul').show()}});function checkCookie(cookieId,obj){var cookieVal=$.cookie(cookieId);if(cookieVal!=null){var activeArray=cookieVal.split(',');$.each(activeArray,function(index,value){var $cookieLi=$('li:eq('+value+')',obj);$('> a',$cookieLi).addClass(defaults.classActive);var $parentsLi=$cookieLi.parents('li');$('> a',$parentsLi).addClass(defaults.classActive)})}}function createCookie(cookieId,obj){var activeIndex=[];$('li a.'+defaults.classActive,obj).each(function(i){var $arrayItem=$(this).parent('li');var itemIndex=$('li',obj).index($arrayItem);activeIndex.push(itemIndex)});$.cookie(cookieId,activeIndex,{path:'/'})}}})(jQuery);

// (function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);

