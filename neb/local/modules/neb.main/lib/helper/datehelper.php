<?php

namespace Neb\Main\Helper;

/**
 * User: agolodkov
 * Date: 14.07.2016
 * Time: 9:05
 */
class DateHelper
{
    /**
     * @param        $date
     * @param string $format
     *
     * @return bool
     */
    public static function validateFormat($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);

        return $d && $d->format($format) == $date;
    }
}