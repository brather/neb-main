<?php
$MESS['FILE_UPLOAD_C_ERROR_FOLDER_NOT_FOUND'] = 'Error! Folder for files not found.';
$MESS['FILE_UPLOAD_C_DELETE_FILE'] = ' Delete';
$MESS['FILE_UPLOAD_C_EMPTY_FOLDER'] = 'Currently not uploaded pdf files';
$MESS['FILE_UPLOAD_C_SIZE_B'] = 'byte';
$MESS['FILE_UPLOAD_C_SIZE_KB'] = 'Kb';
$MESS['FILE_UPLOAD_C_SIZE_MB'] = 'Mb';
$MESS['FILE_UPLOAD_C_SIZE_GB'] = 'Gb';