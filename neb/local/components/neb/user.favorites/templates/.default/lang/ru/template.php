<?
$MESS['FAVORITES_SORT'] = 'Сортировать';
$MESS['FAVORITES_SORT_AUTHOR'] = 'По автору';
$MESS['FAVORITES_SORT_MAIN_TITLE'] = 'По названию';
$MESS['FAVORITES_SORT_DATE'] = 'По дате';
$MESS['FAVORITES_SHOW'] = 'Показать';
$MESS['FAVORITES_SETTINGS'] = 'Настройки';
$MESS['FAVORITES_REMOVE'] = 'Удалить';
$MESS['FAVORITES_FROM_MY_LIBRARY'] = 'из Моей библиотеки';
$MESS['FAVORITES_AUTHOR'] = 'Автор';
$MESS['FAVORITES_YEAR_PUBLICATION'] = 'Год публикации';
$MESS['FAVORITES_NUMBER_OF_PAGES'] = 'Количество страниц';
$MESS['FAVORITES_PUBLISHER'] = 'Издательство';
$MESS['FAVORITES_MY_COLLECTIONS'] = 'Мои подборки';
$MESS['FAVORITES_CREATE_COLLECTION'] = 'cоздать подборку';
$MESS['FAVORITES_READ'] = 'Читать';
$MESS['FAVORITES_SERVER_ERROR'] = 'Ошибка сервера. Пожалуйста, повторите запрос';
$MESS['FAVORITES_DELETE_CONFIRM'] = 'Удалить издание из избранного?';
$MESS['FAVORITES_DELETE_YES'] = 'Удалить';
$MESS['FAVORITES_DELETE_NO'] = 'Отмена';
$MESS['FAVORITES_DELETE_TITLE'] = 'Удалить издание?';
$MESS['FAVORITES_DELETE_TEXT'] = 'Вы действительно хотите удалить издание из списка избранных изданий?';
?>