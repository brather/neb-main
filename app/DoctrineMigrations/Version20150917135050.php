<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150917135050 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
CREATE TABLE `neb_users_books_collection_data` (
  `ID`               INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `UF_UID`           INT(18)                   DEFAULT NULL,
  `UF_COLLECTION_ID` INT(18)                   DEFAULT NULL,
  `UF_DATE_ADD`      TIMESTAMP                 DEFAULT NOW(),
  PRIMARY KEY (`ID`)
)
  AUTO_INCREMENT = 1
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE UNIQUE INDEX user_collection ON neb_users_books_collection_data (UF_UID, UF_COLLECTION_ID);
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
DROP TABLE neb_users_books_collection_data;
SQL
        );
    }
}
