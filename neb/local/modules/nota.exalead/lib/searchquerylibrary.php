<?php
namespace Nota\Exalead;

/**
 * Подсказки в поисковой строке по типу "Библиотеки"
 */
class SearchQueryLibrary extends SearchQuery {

    /**
     * Подсказки в поисковой строке
     *
     * @param $query
     * @return $this|bool
     */
    public function getSuggestAuthor($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author';
        if (empty($query))
            return false;
        $this->setQuery($query);

        return $this;
    }

    /**
     * Подсказки в поисковой строке
     *
     * @param $query
     * @return $this|bool
     */
    public function getSuggestTitle($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title';
        if (empty($query))
            return false;
        $this->setQuery($query);

        return $this;
    }

    public function getSuggestTitleAll($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title_all';

        $this->setQuery($query);

        return $this;
    }

    public function getSuggestAuthorAll($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author_all';

        $this->setQuery($query);
        return $this;
    }
}