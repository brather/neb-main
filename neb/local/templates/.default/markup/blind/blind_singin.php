 
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		
		<h2 class="mode">Вход</h2>
		<div class="b-form b-form_common b-regform">
				<hr/>
				<div class="fieldrow nowrap radiolist">
						<div class="fieldcell iblock mt10">
							<div class="field validate">
								<div class="b-radio">
									<a href="blind_singuser.php" title="Вход пользователя портала">Вход пользователя портала</a>
								</div>	
								<div class="b-radio">
									<a href="blind_singsoc.php" title="Вход через социальные сети">Вход через социальные сети</a>
								</div>	
								<div class="b-radio">
									<a href="blind_reg.php" title="Регистрация на портале">Регистрация на портале</a>
								</div>	
							</div>
						</div>
					</div>
					<hr/>
		</div>
	</div><!-- /.b-registration-->



</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>