<?php
$MESS['LIBRARY_RIGHT_COUNTER_FUND'] = 'In the fund of the library';
$MESS['LIBRARY_RIGHT_COUNTER_FUND_DIGITIZED'] = 'Digitized';

$MESS['LIBRARY_RIGHT_COUNTER_PUB_5'] = 'publications';
$MESS['LIBRARY_RIGHT_COUNTER_PUB_1'] = 'publication';
$MESS['LIBRARY_RIGHT_COUNTER_PUB_2'] = 'publications';

$MESS['LIBRARY_RIGHT_COUNTER_INC'] = 'The library includes';

$MESS['LIBRARY_RIGHT_COUNTER_COL_5'] = 'collections';
$MESS['LIBRARY_RIGHT_COUNTER_COL_1'] = 'collection';
$MESS['LIBRARY_RIGHT_COUNTER_COL_2'] = 'collections';

$MESS['LIBRARY_RIGHT_COUNTER_VIEW_ALL'] = 'View all';
$MESS['LIBRARY_RIGHT_COUNTER_READER'] = 'Number of readers';
$MESS['LIBRARY_RIGHT_COUNTER_VIEWERS'] = 'Number of views';