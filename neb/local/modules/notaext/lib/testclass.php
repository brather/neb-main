<?
namespace Bitrix\NotaExt;

use Bitrix\Main\Entity;
use Bitrix\NotaExt\Utils;

Utils::pre('**++**', 1);

class TestClass extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }
}