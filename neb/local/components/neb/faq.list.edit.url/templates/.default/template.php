<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);?>

        <div class="" id="accordion">
            <?foreach($arResult["SECTION_ITEMS"] as $idSection => $arSection){?>
                <dl class="panel advanced-list-expandable advanced-list-decorated">
                    <dt>
                        <a data-toggle="collapse" data-parent="#accordion" href="#s<?= $idSection ?>"><?= $arSection["NAME"] ?></a>

                        <a href="<?= $arSection["URL"] ?>" data-created-date="<?= $arSection['DATE_CREATE'] ?>" title="<?= Loc::getMessage("SECTION_EDIT") ?>"></a>
                    </dt>
                    <div id="s<?= $idSection ?>" class="collapse">
                        <dd>
                            <?foreach($arSection["ITEMS"] as $idItem => $arItem){?>
                                <div>
                                    <?= $arItem["NAME"] ?>
                                    <a data-edit-item class="ald-edit-item" data-created-date="<?= $arItem['DATE_CREATE'] ?>" href="<?= $arItem["URL"] ?>">
                                    </a>
                                </div>
                            <?}?>
                        </dd>
                    </div>
                </dl>
            <?}?>
        </div>
    