<?php

$MESS['LIB_SEARCH_FORM_TEMPLATE_ALL'] = 'all items';
$MESS['LIB_SEARCH_FORM_TEMPLATE_NAME'] = 'main title';
$MESS['LIB_SEARCH_FORM_TEMPLATE_MPUBLISHPLACE'] = 'place of publication';
$MESS['LIB_SEARCH_FORM_TEMPLATE_MPUBLISHER'] = 'publisher';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AUTHOR'] = 'author';
$MESS['LIB_SEARCH_FORM_TEMPLATE_ISBN'] = 'ISBN';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TEXT'] = 'library';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SEARCH'] = 'Advanced search';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PORTAL'] = 'Search only in scanned editions';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TEXT'] = 'Libraries';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FORACCESS'] = 'access';
$MESS['LIB_SEARCH_FORM_TEMPLATE_OPEN_ACCESS'] = 'Free access';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CLOSE_ACCESS'] = 'Limited access';
$MESS['LIB_SEARCH_FORM_TEMPLATE_EXAMPLES'] = 'Example: Gagarin’s biography';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SEARCH_AUTHOR'] = 'By author';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SEARCH_NAME'] = 'By title';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FIND'] = 'Find';

$MESS['LIB_SEARCH_FORM_TEMPLATE_OR'] = 'or';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AND'] = 'and';
$MESS['LIB_SEARCH_FORM_TEMPLATE_NO'] = 'not';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AUTHORBOOK'] = 'author';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TITLE'] = 'main title';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PUBLISHER'] = 'publishers';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PUBLISHPLACE'] = 'place of publication';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CATEGORY'] = 'by category';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE'] = 'date';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE_PUBLIC'] = 'Date of publication';

$MESS['LIB_SEARCH_FORM_TEMPLATE_ADD'] = 'add a search condition';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DEL'] = 'remove condition';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE_SUBMIT'] = 'accept';

$MESS['LIB_SEARCH_FORM_TEMPLATE_ABOUT'] = 'About the Project';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FULLTEXT_SEARCH_ONLY'] = 'Search in full-text publications only';

$MESS['LIB_SEARCH_FORM_TEMPLATE_FIND_IN'] = 'Search in results';

$MESS['LIB_SEARCH_FORM_TEMPLATE_STRICT_SEARCH'] = 'Exact phrase';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FOR_FULL_TEXT'] = 'Search on full text of publications';
$MESS['LIB_SEARCH_FORM_TEMPLATE_IN_DISERTATIONS'] = 'Search in theses and dissertations';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CLEAN'] = 'Clean search';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SCANNED_BOOKS'] = 'Scanned books Russian libraries';
$MESS['LIB_SEARCH_FORM_TEMPLATE_BETA'] = 'beta-version';

$MESS["TAB_LIBRARY"] = "Libraries";
$MESS["TAB_MUSEUM"] = "Museums";
$MESS["TAB_ARCHIVE"] = "Archives";
$MESS["BUTTON_SEARCH"] = "Search";
$MESS["SEARCH_PLACEHOLDER"] = "By all fields";

//Лида, начинать отсюда :)
$MESS["EXTENDED_SEARCH"] = "Advanced search"; // Расширенный поиск
$MESS["PUBLISHED"] = "Date of publication"; // Дата публикации
$MESS["PUBLISHED_FROM"] = 'from';//"от";
$MESS["PUBLISHED_TO"] = 'to';//"до";
$MESS["PUBLISHED_YEAR"] = 'year'; // года
$MESS["COLLECTIONS_SEARCH"] = "Material type"; //Искать по коллекциям;
$MESS["SEARCH_CHOISE_COLLECTION"] = 'choise collection';

$MESS["SEARCH_LOGIC_AND"] = 'and'; // и
$MESS["SEARCH_LOGIC_OR"] = 'or'; // или
$MESS["SEARCH_LOGIC_NOT"] = 'not'; // не

$MESS["SEARCH_ALL"] = 'Anywhere';// по всем полям;
$MESS["SEARCH_authorbook"] = 'Authorbook'; // по автору
$MESS["SEARCH_title"] = 'Title'; // по названию
$MESS["SEARCH_publisher"] = 'Publisher'; // по издательствам
$MESS["SEARCH_publishplace"] = 'Place of publication'; //по месту издательства

$MESS["SEARCH_IN_CATALOG_OF_PUBLICATIONS"] = 'Search in catalogue cards'; //Искать в каталогах печатных изданий
$MESS["SEARCH_BY_NEB_TEXTS"] = 'Search in the electronic copies of publications';// Искать по текстам изданий в НЭБ'
$MESS["SEARCH_EXACT_PHRASE"] = 'Search by an exact phrase'; // Искать по точной фразе
$MESS["SEARCH_WITHIN_RESULTS"] = 'Search in result'; // Искать в найденном
$MESS["SEARCH_DEBUG"] = 'Debug mode'; // Режим отладки

$MESS["SEARCH_IN_CLOSED"] = 'Search in publications with copyright';

$MESS["SEARCH_SIMPLE_SEARCH"] = 'Simple search';
$MESS["SEARCH_AUTHOR_AND_OR_TITLE"] = 'Search books by author and / or title';
$MESS["SEARCH_AUTHOR_UPPER_TITLE"] = 'Author';
$MESS["SEARCH_NAME_UPPER_TITLE"] = 'Title';