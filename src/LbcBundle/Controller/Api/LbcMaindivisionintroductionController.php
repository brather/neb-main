<?php
/**
 * User: agolodkov
 * Date: 12.08.2016
 * Time: 17:13
 */

namespace LbcBundle\Controller\Api;

use Doctrine\Bundle\DoctrineBundle\Registry;
use LbcBundle\Entity\LbcMaindivisionintroduction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class LbcMaindivisionintroductionController
 *
 * @Route("/api/bbk")
 */
class LbcMaindivisionintroductionController implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @DI\Inject("doctrine")
     * @var Registry
     */
    public $doctrine;

    /**
     * MainDivision introduction text.
     *
     * @param int $id
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     *
     * @return array
     */
    public function introductionAction($id)
    {
        $repository = $this->doctrine->getManager()->getRepository('LbcBundle:LbcMaindivisionintroduction');
        $id = (integer)$id;
        if ($id < 1) {
            throw new BadRequestHttpException('Empty maindivision id');
        }
        $condition = [];
        $condition['maindivisionid'] = $id;
        /** @var LbcMaindivisionintroduction[] $entities */
        $entities = $repository->findBy($condition);
        if (empty($entities)) {
            throw new NotFoundHttpException('Unable to find LbcBundle:LbcMaindivisionintroduction entity.');
        }
        $uploadDir = $this->container->getParameter('dir.upload');
        $uploadDir = realpath($uploadDir);
        $uploadDir .= '/bbk/';
        $content = '';
        foreach ($entities as $entity) {
            $textPath = $entity->getTextpath();
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                $textPath = iconv('utf-8', 'cp1251', $textPath);
            }
            $file = $uploadDir . $textPath;
            $file = str_replace('\\', '/', $file);
            if (file_exists($file)) {
                $fileData = file_get_contents($file);
                $fileData = str_replace(['<br>', '<br/>', '<br />'], '', $fileData);
                $charset = null;
                /** @todo выпилить силище кастылище */
                if (false !== strpos($fileData, 'content="text/html; charset=UTF-8"')) {
                    $content .= $fileData;
                } else {
                    $content .= iconv('cp1251', 'utf-8', $fileData);
                }
                continue;
            }
        }

        return [
            'content'  => $content,
            'entities' => $entities,
        ];
    }
}