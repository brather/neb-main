<?php
/**
 * Получение предыдущей версиии произведений издания при попытке отклонения всех добавленных/измененных произведений
 *
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 09.02.2017
 * Time: 15:57
 */

define('STOP_STATISTICS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\RslViewer;

global $USER;
if (!$USER->IsAuthorized() || !Loader::includeModule('nota.exalead'))
    exit();

$obUser       = new nebUser($USER->GetID());
$arToken      = $obUser->getToken();
$arUserGroups = $obUser->getUserGroups();

$sBookId  = trim($_REQUEST['book_id']);
$arParams = ['token' => $arToken['UF_TOKEN']];

if (empty($sBookId) || empty($arParams['token']) || !in_array(UGROUP_MARKUP_APPROVER, $arUserGroups))
    exit();

$obRslViewer = new RslViewer($sBookId);
$arResult    = $obRslViewer->prevArtsMarkup($arParams);

// результатом всегда должен быть массив
if (empty($arResult)) {
    $arResult = [];
}

MainHelper::showJson($arResult);