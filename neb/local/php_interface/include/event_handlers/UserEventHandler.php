<?php
/**
 * User: agolodkov
 * Date: 03.05.2015
 * Time: 12:00
 */

use \Bitrix\Main\EventManager,
    \Bitrix\Main\Loader;

EventManager::getInstance()->addEventHandler('main', 'OnBeforeUserUpdate', ['UserEventHandler', 'applyLibrariesBeforeUpdate']);
EventManager::getInstance()->addEventHandler('main', 'OnBeforeUserAdd',    ['UserEventHandler', 'applyLibrariesBeforeAdd']);
EventManager::getInstance()->addEventHandler('main', 'OnBeforeUserAdd',    ['UserEventHandler', 'removeEsiaExternalAuthId']);
EventManager::getInstance()->addEventHandler('main', 'OnAfterUserAdd',     ['UserEventHandler', 'countUserAdding']);
EventManager::getInstance()->addEventHandler('main', 'OnAfterUserAdd',     ['UserEventHandler', 'createEICHB']);
EventManager::getInstance()->addEventHandler('main', 'OnAfterUserUpdate',  ['UserEventHandler', 'countUserVerification']);
EventManager::getInstance()->addEventHandler('main', 'OnAfterUserUpdate',  ['UserEventHandler', 'countUserBindLibrary']);
EventManager::getInstance()->addEventHandler('main', 'OnBeforeUserDelete', ['UserEventHandler', 'countUserDeletions']);
EventManager::getInstance()->addEventHandler('main', 'OnBeforeUserLogin',  ['UserEventHandler', 'checkUserVerification']);
EventManager::getInstance()->addEventHandler('main', 'OnBeforeUserLogin',  ['UserEventHandler', 'checkLoginFields']);
EventManager::getInstance()->addEventHandler('main', 'OnAfterUserAuthorize', ['UserEventHandler', 'countUserAuth']);
EventManager::getInstance()->addEventHandler("main", "OnAfterUserLogin",     ["UserEventHandler", "regenToken"]);
EventManager::getInstance()->addEventHandler("main", "OnAfterUserAuthorize", ["UserEventHandler", "regenToken"]);
EventManager::getInstance()->addEventHandler("main", "OnAfterUserLogin",     ["UserEventHandler", "checkRGBStatus"]);
EventManager::getInstance()->addEventHandler("main", "OnAfterUserAuthorize", ["UserEventHandler", "checkRGBStatus"]);
EventManager::getInstance()->addEventHandler("main", "OnAfterUserAuthorize", ["UserEventHandler", "abortGroupsCache"]);
EventManager::getInstance()->addEventHandler("main", "OnAfterUserLogin",     ["UserEventHandler", "redirectToProfile"]);
EventManager::getInstance()->addEventHandler('main', 'OnBeforeUserRegister', ['UserEventHandler', 'checkScans']);
EventManager::getInstance()->addEventHandler('main', 'OnAfterUserRegister',  ['UserEventHandler', 'checkUser']);
EventManager::getInstance()->addEventHandler('main', 'OnAfterUserRegister',  ['UserEventHandler', 'createEICHB']);
EventManager::getInstance()->addEventHandler('main', 'OnBeforeUserChangePassword', ['UserEventHandler', 'changePasswordIncorrentCheckWordMessage']);

class UserEventHandler
{
    /**
     * Привязка пользователя к библиотекам
     *
     * @param $arFields
     */
    public static function applyLibrariesBeforeUpdate(&$arFields)
    {
        if (!isset($arFields['ID']) || empty($arFields['ID'])
            || (!isset($arFields['UF_LIBRARY']) && !isset($arFields['UF_LIBRARIES']))
        ) {
            return;
        }
        $nebUser = new NebUser($arFields['ID']);
        $arUser = $nebUser->getUser();

        /**
         * Если не передан список библиотек то берем текущий
         */
        if (!isset($arFields['UF_LIBRARIES'])) {
            $arFields['UF_LIBRARIES'] = $arUser['UF_LIBRARIES'];
        }
        if (!is_array($arFields['UF_LIBRARIES'])) {
            $arFields['UF_LIBRARIES'] = array();
        }
        $arFields['UF_LIBRARIES'] = array_filter($arFields['UF_LIBRARIES']);

        /**
         * Если удаляем основную библиотеку то из списка тоже
         */
        if (isset($arFields['UF_LIBRARY']) && empty($arFields['UF_LIBRARY']) && !empty($arUser['UF_LIBRARY'])
            && false !== ($libKey = array_search($arUser['UF_LIBRARY'], $arFields['UF_LIBRARIES']))
        ) {
            unset($arFields['UF_LIBRARIES'][$libKey]);
        } /**
         *  Если в списке библиотек нету основной - то исправляем это
         */
        elseif (isset($arFields['UF_LIBRARY']) && false === array_search($arFields['UF_LIBRARY'], $arFields['UF_LIBRARIES'])
        ) {
            $arFields['UF_LIBRARIES'][] = $arFields['UF_LIBRARY'];
        }

        /**
         * Если список пуст то удаляем и основную библиотеку
         */
        if (empty($arFields['UF_LIBRARIES'])) {
            $arFields['UF_LIBRARY'] = '';

            return;
        } /**
         * Если основной нету то берем первую из списка
         */
        elseif (empty($arFields['UF_LIBRARY'])) {
            $arFields['UF_LIBRARY'] = current($arFields['UF_LIBRARIES']);
        }
    }

    /**
     * Если задана только основная библиотека то добаляем ее в список
     * Если только список библиотек то выбираем из него основную
     *
     * @param $arFields
     */
    public static function applyLibrariesBeforeAdd(&$arFields)
    {
        if (!isset($arFields['UF_LIBRARY']) && !isset($arFields['UF_LIBRARIES'])
        ) {
            return;
        }

        if (!isset($arFields['UF_LIBRARIES']) && isset($arFields['UF_LIBRARY'])
        ) {
            $arFields['UF_LIBRARIES'] = array($arFields['UF_LIBRARY']);
        }

        if (!isset($arFields['UF_LIBRARY']) && isset($arFields['UF_LIBRARIES'])
        ) {
            $arFields['UF_LIBRARY'] = current($arFields['UF_LIBRARIES']);
        }
    }

    /**
     * @param array &$arFields
     */
    public static function removeEsiaExternalAuthId(&$arFields)
    {
        if ($arFields['EXTERNAL_AUTH_ID'] === 'socservices' && $arFields['UF_REGISTER_TYPE'] === '40'
        ) {
            unset($arFields['EXTERNAL_AUTH_ID']);
        }
    }

    /**
     * @param array &$arFields
     */
    public static function countUserAdding(&$arFields)
    {
        if (!empty($arFields['ID'])) {
            \Neb\Main\Stat\Tracker::track('neb_user_registers', ['user_id' => $arFields['ID']]);
        }
    }

    /**
     * @param array $arFields
     */
    public static function countUserAuth($arFields)
    {
        if (!empty($arFields['user_fields']['ID'])) {
            $arUser = CUser::GetByID($arFields['user_fields']['ID'])->Fetch();

            \Neb\Main\Stat\Tracker::track(
                'neb_user_auth',
                [
                    'user_id'    => $arFields['user_fields']['ID'],
                    'library_id' => $arUser['UF_LIBRARY'],
                ]
            );
        }
    }

    public static function regenToken(){
        $obUser = new nebUser();
        $obUser->regenToken();

        return true;
    }

    /**
     * При каждой авторизации проверяем емейл по нашей базе и базе РГБ.
     * Если в РГБ его статус просрочен - предлагаем пройти регистрацию со сканами паспорта,
     * если заблокирован - уведомляем об этом, статус меняем на упрощенного, пока не закончится срок блокировки
     */
    public static function checkRGBStatus(){

        $obUser = new \nebUser();
        $arUser = $obUser->getUser();

        if ($arUser['UF_RGB_USER_ID'] && $arUser['UF_RGB_CARD_NUMBER']) {
            
            Loader::includeModule('nota.userdata');

            $obRgb  = new \Nota\UserData\rgb();
            $arStatus = $obRgb->isUserActive($arUser['UF_RGB_USER_ID']);

            if ($arStatus['is_card_expired']) {

                $obUser->Update($arUser['ID'], ["UF_STATUS" => $obUser::USER_STATUS_REGISTERED]);
                LocalRedirect('/popup/?action=user_card_expired', false);

            } elseif($arStatus['is_block']) {

                $obUser->Update($arUser['ID'], ["UF_STATUS" => $obUser::USER_STATUS_BLOCKED]);
                LocalRedirect('/popup/?action=user_blocked', false);

            } elseif($arUser["UF_STATUS"] == $obUser::USER_STATUS_BLOCKED) {

                $obUser->Update($arUser['ID'], ["UF_STATUS" => $obUser::USER_STATUS_VERIFIED]);
            }
        }

        return true;
    }

    /**
     * Очищает кеш групп пользователя
     * @param $arFields
     */
    public static function abortGroupsCache($arFields) {
        if ($arFields['user_fields']['ID'] > 0) {
            \nebUser::getUserGroupsInfo($arFields['user_fields']['ID'], true);
        }
    }

    /**
     * Для всех пользователей при логине через публичную часть сайта, редиректим на страницу скоторой пришел
     */
    public static function redirectToProfile(&$arFields){

        if (defined('ADMIN_SECTION') && ADMIN_SECTION === true)
            return true;

        // для всех пользователей редирект на страницу с которой перешел на авторизацию
        if ($arFields['USER_ID'] > 0)
            LocalRedirect($_SESSION['AUTH_REFERER']);
    }

    public static function checkScans(&$arFields){
        /*
        if (empty($_REQUEST['scan1']) || empty($_REQUEST['scan2'])):
            $GLOBALS['APPLICATION']->ThrowException('Приложите сканы паспорта!');
            return false;
        endif;
        */
        return true;
    }

    public static function checkUser(&$arFields) {

        $obUser = new \CUser();

        $arUser = $obUser->GetByID($arFields['USER_ID'])->Fetch();

        if (!empty($arFields['USER_ID'])) {
            $obUser->Update($arFields['USER_ID'], array('ACTIVE' => 'Y'));
        }

        // проверка типа регистрации
        // ели пользователь регистрируется по упрощенному типу то выполнять проверку на ip адреса ВЧЗ
        if ($arUser['UF_REGISTER_TYPE'] == 37) {

            Loader::includeModule('nota.userdata'); // нужен ли здесь???

            $arWorkPlaces = NebWorkplaces::getWorkplacesByIp($arFields['USER_IP']);

            // если найдены ЭЧЗ с таким IP то обновлаем данные для пользователя
            if (!empty($arWorkPlaces)) {
                reset($arWorkPlaces); $arWorkPlaces = current($arWorkPlaces);
                $obUser->Update($arFields['USER_ID'], ["UF_WCHZ" => $arWorkPlaces['ID']]);
            }
        }
    }

    /**
     * После добавления или регистрации пользователя ему автоматически присваивается ЕЭЧБ
     * @param $arFields
     */
    public static function createEICHB(&$arFields) {
        if ($arFields['ID'] > 0) {
            $obNebUser = new \nebUser($arFields['ID']);
            $obNebUser->setEICHB();
        }
    }

    /**
     * @param array &$arFields
     */
    public static function countUserVerification(&$arFields)
    {
        if ($arFields['RESULT'] && nebUser::USER_STATUS_VERIFIED === intval($arFields['UF_STATUS'])) {
            \Neb\Main\Stat\Tracker::track('neb_user_verifications', ['user_id' => $arFields['ID']]);
        }
    }

    /**
     * @param array &$arFields
     */
    public static function countUserBindLibrary(&$arFields)
    {
        if ($arFields['RESULT'] && isset($arFields['UF_LIBRARY']) && !empty($arFields['UF_LIBRARY'])
        ) {
            \Neb\Main\Stat\Tracker::track(
                'neb_user_bindings',
                [
                    'user_id'    => $arFields['ID'],
                    'library_id' => $arFields['UF_LIBRARY'],
                ]
            );
        }
    }

    /**
     * @param $userId
     */
    public static function countUserDeletions($userId)
    {
        $trackParams = [
            'user_id' => $userId,
        ];
        $arUser = CUser::GetByID($userId)->Fetch();
        if (!empty($arUser['UF_LIBRARY'])) {
            $trackParams['library_id'] = $arUser['UF_LIBRARY'];
        }

        \Neb\Main\Stat\Tracker::track('neb_user_deletions', $trackParams);
    }

    public static function checkUserVerification(&$params)
    {
        if (!empty($params['LOGIN'])) {
            $arUser = CUser::GetByLogin($params['LOGIN'])->Fetch();
            if (isset($arUser['ID'])) {
                nebUser::checkTemporaryVerification($arUser['ID']);
            }
        }
    }

    public static function changePasswordIncorrentCheckWordMessage()
    {
        global $MESS;
        $MESS['CHECKWORD_INCORRECT'] = "Ссылка на сброс пароля больше не действительна.";
    }

    /**
     * @param $params
     *
     * @return bool
     */
    public static function checkLoginFields($params)
    {
        if ('Y' === $params['PASSWORD_ORIGINAL']) {
            return static::_checkCommonFields($params);
        }

        return true;
    }

    /**
     * @param array $params
     *
     * @return bool
     */
    public static function checkAddFields($params)
    {
        return static::_checkCommonFields($params);
    }

    /**
     * @param array $arFields
     *
     * @return bool
     */
    private static function _checkCommonFields(&$arFields)
    {
        global $APPLICATION;
        $result = true;
        if (isset($arFields['PASSWORD']) && mb_strlen($arFields['PASSWORD']) > 1024) {
            $APPLICATION->ThrowException('Длина пароля не должна превышать 1024 символов');
            $result = false;
        }

        return $result;
    }
}