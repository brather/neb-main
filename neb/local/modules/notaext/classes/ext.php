<?
use Bitrix\NotaExt\NotaExtTable as NET;
use Bitrix\NotaExt\Utils as Ut;
use Bitrix\Main\Config\Option as ConfigOption;

class CExt
{
    var $EXT_FOLDER;
    var $MODULE_FOLDER;
    var $MODULE_HOLDER;

    var $arExtFolders = array(
        'admin',
        'components',
        'db',
        'lang',
        'lib',
        'tools',
        'wizards',
        'templates',
    );

    public function __construct()
    {
        $this->MODULE_FOLDER = substr(__DIR__, 0, strrpos(__DIR__, '/'));
        $this->EXT_FOLDER = $this->MODULE_FOLDER . '/ext';
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local'))
            $this->MODULE_HOLDER = 'local';
        else
            $this->MODULE_HOLDER = 'bitrix';
    }

    public function getOptions()
    {
        if (file_exists($this->EXT_FOLDER . '/' . $this->EXT_ID . '/options.php')) {
            include_once($this->EXT_FOLDER . '/' . $this->EXT_ID . '/options.php');
            if (is_array($arOptions) and !empty($arOptions))
                return $arOptions;
        }
        return false;
    }

    public function IsInstalled()
    {
        $isInstalled = false;
        $ext = self::getExt();
        if ($ext !== false and $ext['INSTALL'] == 'Y')
            $isInstalled = true;

        return $isInstalled;
    }

    public function Install()
    {
        static::onBeforeInstall();
        self::InstallSymLinks('Y');
        self::setExtInstall('Y');
        static::onAfterInstall();
    }

    public function Uninstall()
    {
        static::onBeforeUninstall();
        self::UnInstallSymLinks();
        self::setExtInstall('N');
        self::UnInstallOptions();
        static::onAfterUninstall();
    }

    private function UnInstallOptions()
    {
        $con = Bitrix\Main\Application::getConnection();
        $sqlHelper = $con->getSqlHelper();

        $result = $con->query('
				SELECT `NAME` FROM `b_option`
				WHERE 
				`MODULE_ID` = \'' . $sqlHelper->forSql('notaext') . '\'
				AND
				`NAME` LIKE \'' . $sqlHelper->forSql("{$this->EXT_ID}_%") . '\'
			');

        if ($result->getSelectedRowsCount() > 0) {
            while ($optItem = $result->Fetch()) {
                ConfigOption::delete('notaext', array('name' => $optItem['NAME']));
            }
        }
    }

    private function InstallSymLinks()
    {
        global $DB;

        $dirExt = $this->EXT_FOLDER . '/' . $this->EXT_ID;
        $dirList = self::getDir($dirExt);
        natsort($dirList);
        foreach ($dirList as $dir) {
            if (!in_array($dir, $this->arExtFolders))
                continue;

            $arFile = self::getDir($dirExt . '/' . $dir, true);
            $arDir = self::getDir($dirExt . '/' . $dir);

            switch ($dir) {
                case 'admin':
                    /* Создаем файл в папке /bitrix/admin/ */
                    if (!empty($arFile)) {
                        $isMenu = false;
                        foreach ($arFile as $file) {
                            if ($file == 'menu.php')
                                continue;

                            $absFile = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $file;
                            $absFileRequire = str_replace($_SERVER["DOCUMENT_ROOT"], '', $dirExt) . '/admin/' . $file;
                            if (!file_exists($absFile))
                                RewriteFile($absFile, '<?require_once($_SERVER["DOCUMENT_ROOT"]."' . $absFileRequire . '");?>');
                        }
                    }
                    break;

                case 'lang':

                    CheckDirPath($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/modules/notaext/lang/ru/ext/');
                    CheckDirPath($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/modules/notaext/lang/en/ext/');
                    /* симлинки на языковые файлы	*/
                    if (file_exists($dirExt . '/' . $dir . '/ru'))
                        symlink('../../../ext/' . $this->EXT_ID . '/lang/ru', $this->MODULE_FOLDER . '/lang/ru/ext/' . $this->EXT_ID);

                    if (file_exists($dirExt . '/' . $dir . '/en'))
                        symlink('../../../ext/' . $this->EXT_ID . '/lang/en', $this->MODULE_FOLDER . '/lang/en/ext/' . $this->EXT_ID);
                    break;

                case 'components':
                    /* симлинки на компоненты */
                    if (!empty($arDir)) {
                        CheckDirPath($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/components/notaext/');
                        foreach ($arDir as $dirComp)
                            @symlink('../../modules/notaext/ext/' . $this->EXT_ID . '/components/' . $dirComp, $_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/components/notaext/' . $dirComp);
                    }
                    break;
                case 'db':
                    /* исполняем sql файл*/
                    if (file_exists($dirExt . '/' . $dir . '/install.sql'))
                        $DB->RunSQLBatch($dirExt . '/' . $dir . '/install.sql');
                    break;

                case 'lib':

                    CheckDirPath($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/modules/notaext/lib/ext/');
                    /* симлинк на папку с либами и регистрация свойств	*/
                    if (!empty($arFile) or !empty($arDir))
                        @symlink('../../ext/' . $this->EXT_ID . '/lib', $this->MODULE_FOLDER . '/lib/ext/' . strtolower($this->EXT_ID));

                    if (file_exists($dirExt . '/' . $dir . '/prop')) {
                        $arFile = self::getDir($dirExt . '/' . $dir . '/prop', true);
                        foreach ($arFile as $file) {
                            $nameSpace = self::getNameSpace($dirExt . '/' . $dir . '/prop/' . $file);
                            $class = self::getClass($dirExt . '/' . $dir . '/prop/' . $file);
                            if (!empty($nameSpace) and !empty($class)) {
                                $classPatch = $nameSpace . '\\' . $class;
                                if (substr($class, 0, 4) == 'User')
                                    RegisterModuleDependences("main", "OnUserTypeBuildList", 'notaext', $classPatch, "GetUserTypeDescription", 1000);
                                else
                                    RegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", 'notaext', $classPatch, "GetUserTypeDescription", 1000);
                            }
                        }
                    }
                    break;

                case 'tools':
                    /* симлинк в tools */
                    if (!empty($arDir) or !empty($arFile)) {
                        CheckDirPath($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/tools/notaext/');
                        symlink('../../modules/notaext/ext/' . $this->EXT_ID . '/tools', $_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/tools/notaext/' . $this->EXT_ID);
                    }
                    break;

                case 'wizards':
                    /* симлинк в визардс */
                    if (!empty($arDir)) {
                        CheckDirPath($_SERVER['DOCUMENT_ROOT'] . '/bitrix/wizards/notaext/');
                        foreach ($arDir as $dirWizard) {

                            #pre($_SERVER['DOCUMENT_ROOT'].'/'.$this->MODULE_HOLDER.'/modules/notaext/ext/'.$this->EXT_ID.'/wizards/'.$dirWizard,1);
                            #pre($_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/notaext/'.$dirWizard,1);
                            #exit();

                            CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/modules/notaext/ext/' . $this->EXT_ID . '/wizards/' . $dirWizard, $_SERVER['DOCUMENT_ROOT'] . '/bitrix/wizards/notaext/' . $dirWizard);
                        }

                        #symlink('../../../local/modules/notaext/ext/'.$this->EXT_ID.'/wizards/'.$dirWizard, $_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/notaext/'.$dirWizard);
                    }
                    break;

                case 'templates':
                    /*симлинк на шаблоны компонентов*/
                    if (!empty($arDir)) {
                        foreach ($arDir as $tdir)
                            self::getComponentTemplate($dirExt . '/' . $dir);
                    }

                    break;
            }
        }
    }

    /*
    Находим папки шаблонов и ставим на них симлинки
    */
    private function getComponentTemplate($patch, $rm = false)
    {
        if (!file_exists($patch))
            return false;

        $arFile = self::getDir($patch, true);
        if (!empty($arFile)) {
            if (in_array('template.php', $arFile)) {

                $strExtFolder = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/modules/notaext/ext/' . $this->EXT_ID;
                $strTemplatePatch = str_replace($strExtFolder, '', $patch);

                $strTemplateCompPatch = substr($strTemplatePatch, 0, strrpos($strTemplatePatch, "/"));

                if (!$rm) {
                    CheckDirPath($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . $strTemplateCompPatch . '/');
                    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . $strTemplatePatch))
                        symlink('../../../../../modules/notaext/ext/' . $this->EXT_ID . $strTemplatePatch, $_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . $strTemplatePatch);
                } else {
                    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . $strTemplatePatch) and is_link($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . $strTemplatePatch))
                        unlink($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . $strTemplatePatch);
                }
            }
        }

        $arDir = self::getDir($patch);
        if (!empty($arDir)) {
            foreach ($arDir as $dir) {
                self::getComponentTemplate($patch . '/' . $dir, $rm);
            }
        }
    }

    private function UnInstallSymLinks()
    {
        global $DB;
        $dirExt = $this->EXT_FOLDER . '/' . $this->EXT_ID;
        $dirList = self::getDir($dirExt);
        natsort($dirList);
        foreach ($dirList as $dir) {
            if (!in_array($dir, $this->arExtFolders))
                continue;

            $arFile = self::getDir($dirExt . '/' . $dir, true);
            $arDir = self::getDir($dirExt . '/' . $dir);

            switch ($dir) {
                case 'admin':
                    /* Создаем файл в папке /bitrix/admin/ */
                    if (!empty($arFile)) {
                        $isMenu = false;
                        foreach ($arFile as $file) {
                            if ($file == 'menu.php')
                                continue;

                            $absFile = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $file;
                            $absFileRequire = str_replace($_SERVER["DOCUMENT_ROOT"], '', $dirExt) . '/admin/' . $file;
                            if (file_exists($absFile))
                                unlink($absFile);
                        }
                    }
                    break;

                case 'lang':
                    /* симлинки на языковые файлы	*/
                    if (file_exists($dirExt . '/' . $dir . '/ru') and file_exists($this->MODULE_FOLDER . '/lang/ru/ext/' . $this->EXT_ID))
                        unlink($this->MODULE_FOLDER . '/lang/ru/ext/' . $this->EXT_ID);

                    if (file_exists($dirExt . '/' . $dir . '/en') and file_exists($this->MODULE_FOLDER . '/lang/en/ext/' . $this->EXT_ID))
                        unlink($this->MODULE_FOLDER . '/lang/en/ext/' . $this->EXT_ID);
                    break;

                case 'components':
                    /* симлинки на компоненты */
                    if (!empty($arDir)) {
                        foreach ($arDir as $dirComp)
                            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/components/notaext/' . $dirComp))
                                unlink($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/components/notaext/' . $dirComp);
                    }
                    break;
                case 'db':
                    /* исполняем sql файл*/
                    if (file_exists($dirExt . '/' . $dir . '/uninstall.sql'))
                        $DB->RunSQLBatch($dirExt . '/' . $dir . '/uninstall.sql');
                    break;

                case 'lib':
                    /* симлинк на папку с либами и регистрация свойств	*/
                    if (file_exists($this->MODULE_FOLDER . '/lib/ext/' . strtolower($this->EXT_ID)))
                        unlink($this->MODULE_FOLDER . '/lib/ext/' . strtolower($this->EXT_ID));

                    if (file_exists($dirExt . '/' . $dir . '/prop')) {
                        $arFile = self::getDir($dirExt . '/' . $dir . '/prop', true);
                        foreach ($arFile as $file) {
                            $nameSpace = self::getNameSpace($dirExt . '/' . $dir . '/prop/' . $file);
                            $class = self::getClass($dirExt . '/' . $dir . '/prop/' . $file);
                            if (!empty($nameSpace) and !empty($class)) {
                                $classPatch = $nameSpace . '\\' . $class;
                                if (substr($class, 0, 4) == 'User') {
                                    UnRegisterModuleDependences("main", "OnUserTypeBuildList", 'notaext', $classPatch, "GetUserTypeDescription");
                                } else
                                    UnRegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", 'notaext', $classPatch, "GetUserTypeDescription");
                            }
                        }
                    }
                    break;

                case 'tools':
                    /* симлинк в tools */
                    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/tools/notaext/' . $this->EXT_ID))
                        unlink($_SERVER['DOCUMENT_ROOT'] . '/' . $this->MODULE_HOLDER . '/tools/notaext/' . $this->EXT_ID);
                    break;

                case 'wizards':
                    /* симлинк в визардс */
                    if (!empty($arDir)) {
                        foreach ($arDir as $dirWizard)
                            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/bitrix/wizards/notaext/' . $dirWizard))
                                DeleteDirFilesEx('/bitrix/wizards/notaext/' . $dirWizard);
                        #unlink($_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards/notaext/'.$dirWizard);
                    }
                    break;

                case 'templates':
                    /*симлинк на шаблоны компонентов*/
                    if (!empty($arDir)) {
                        foreach ($arDir as $tdir)
                            self::getComponentTemplate($dirExt . '/' . $dir, true);
                    }

                    break;
            }
        }
    }

    private function getNameSpace($file)
    {
        $content = file_get_contents($file);
        $szSearchPattern = "'[^>]*namespace (.*?);[^>]*'si";
        $ar = array();
        preg_match_all($szSearchPattern, $content, $ar);
        if (!empty($ar[1][0]))
            return $ar[1][0];

        return false;
    }

    private function getClass($file)
    {
        $content = file_get_contents($file);
        $szSearchPattern = "'[^>]*class (.*?) [^>]*'si";
        $ar = array();
        preg_match_all($szSearchPattern, $content, $ar);

        if (!empty($ar[1][0])) {
            /* Fix */
            if (strpos($ar[1][0], '{') !== false) {
                $artmp = explode('{', $ar[1][0]);
                $ar[1][0] = $artmp[0];
            }

            $ar[1][0] = trim($ar[1][0]);
            $ar[1][0] = str_replace(array("\r", "\n"), '', $ar[1][0]);

            return $ar[1][0];
        }

        return false;
    }

    public function getDir($dirExt, $getFile = false)
    {
        $arResult = array();
        if (file_exists($dirExt) and is_dir($dirExt)) {
            $handle = @opendir($dirExt);
            if ($handle) {
                while (false !== ($dir = readdir($handle))) {
                    if (!$getFile) {
                        if (is_dir($dirExt . "/" . $dir) and $dir != "." and $dir != "..")
                            $arResult[] = $dir;
                    } else {
                        if (is_file($dirExt . "/" . $dir) and $dir != "." and $dir != "..") {
                            $info = pathinfo($dirExt . "/" . $dir);
                            if ($info['extension'] == 'php')
                                $arResult[] = $dir;
                        }

                    }
                }
                closedir($handle);
            }
        }
        return $arResult;
    }

    private function getExt()
    {
        $extRes = NET::getList(array('filter' => array('=EXT_ID' => $this->EXT_ID)));
        if ($extRes->getSelectedRowsCount() > 0)
            return $extRes->fetch();
        return false;
    }

    private function setExtInstall($install = 'Y')
    {
        $ext = self::getExt();
        if ($ext === false)
            NET::add(array('EXT_ID' => $this->EXT_ID, 'INSTALL' => 'Y'));
        else
            NET::update($ext['ID'], array('INSTALL' => $install));
    }
}

?>