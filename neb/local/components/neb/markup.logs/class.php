<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\UI\PageNavigation,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\Loader,

    \Nota\Exalead\RslViewer,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient;

Loc::loadMessages(__FILE__);

class MarkupApprove  extends CBitrixComponent
{
    private $sUserToken = '';
    private $arStatus = [];

    public function onPrepareComponentParams($arParams)
    {
        $this->arStatus   = ['added', 'consideration', 'confirmed', 'deleted'];
        $this->sUserToken = $this->getUserToken();

        return static::getUriParams($arParams);
    }

    public function executeComponent()
    {
        // детальная страница
        if ($this->arParams['COMPONENT_PAGE'] == 'detail') {
            $this->getMarkupList();
        }
        // страница списка
        else {
            $this->getMarkupBooks();
        }

        $this->includeComponentTemplate( $this->arParams['COMPONENT_PAGE'] );
    }

    // ================================================================================================================

    /**
     * Возвращает коды статусов для фильтра (используется в шаблоне компонента)
     * @return array
     */
    public function getStatuses() {
        return $this->arStatus;
    }

    /**
     * Возвращает массив $_REQUEST (используется в шаблоне компонента)
     * @return array
     */
    public static function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Получает массив из ЧПУ
     */
    private static function getUriParams($arParams) {

        $arDefaultUrlTemplates404 = array(
            "template" => "",
            "detail"   => "log/#ID#/",
        );

        $arDefaultVariableAliases404 = array();

        $arDefaultVariableAliases = array();

        $arComponentVariables = array("ID");

        $SEF_FOLDER = "";
        $arUrlTemplates = array();

        if ($arParams["SEF_MODE"] == "Y")
        {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

            $componentPage = CComponentEngine::parseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

            if (StrLen($componentPage) <= 0)
                $componentPage = "template";

            CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

            $SEF_FOLDER = $arParams["SEF_FOLDER"];

        } else {

            $arVariables = array();

            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
            CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

            $componentPage = "";
            if (intval($arVariables["ELEMENT_ID"]) > 0) {
                $componentPage = "detail";
            } else {
                $componentPage = "template";
            }
        }

        $arParams['VARIABLES'] = $arVariables;
        $arParams['COMPONENT_PAGE'] = $componentPage;

        return $arParams;
    }

    /**
     * Получнеие токена пользователя
     * @return mixed
     */
    private static function getUserToken() {

        $obUser  = new nebUser();
        $arToken = $obUser->getToken();

        return $arToken['UF_TOKEN'];
    }

    /**
     * Получение и обработка данных фильтра
     * @return array
     */
    private static function getFilter(){

        $arResult = [];

        $arRequest = static::getRequest();

        if ($arRequest['LOG']['FILTER'] != 'Y')
            return $arResult;

        foreach ($arRequest['LOG'] as $sCode => $sVal)
            $arResult[htmlspecialchars($sCode)] = trim(htmlspecialchars($sVal));

        return $arResult;
    }

    /**
     * Страница списка: получение списка всех изданий
     *
     * @param string $sStatus
     * @param int $iPageLimit
     */
    private function getMarkupBooks($sStatus = 'added', $iPageLimit = 20) {

        if (!Loader::includeModule('nota.exalead'))
            exit();

        // получение данных фильтра
        $arFilter = $this->getFilter();

        // инициирование запроса к сервису выдачи
        $arQueryParams = [
            'token'  => $this->sUserToken,
            'status' => $arFilter['STATUS'] ? : 'added'
        ];
        // фильтр: дата начала
        if (!empty($arFilter['DATE_FROM']))
            $arQueryParams['date_from'] = (new DateTime($arFilter['DATE_FROM']))->format("Y-m-d");
        // фильтр: дата окончания
        if (!empty($arFilter['DATE_TO']))
            $arQueryParams['date_to'] = (new DateTime($arFilter['DATE_TO']))->format("Y-m-d");
        // фильтр: автор
        if (!empty($arFilter['AUTHOR']))
            $arQueryParams['author'] = $arFilter['AUTHOR'];
        // фильтр: издание
        if (!empty($arFilter['TITLE']))
            $arQueryParams['title'] = $arFilter['TITLE'];

        // иницирование навигационной цепочки
        $obPageNavigation = new PageNavigation('books');
        $obPageNavigation->allowAllRecords(true)->initFromUri();

        if (strpos($_SERVER['REQUEST_URI'], 'page-all') === false) {
            $arQueryParams['limit']  = $obPageNavigation->getLimit();
            $arQueryParams['offset'] = $obPageNavigation->getOffset();
        }

        // запрос к Exalead
        $obRslViewer = new RslViewer();
        $arResult = $obRslViewer->getMarkupBooks($arQueryParams);

        // задание в навигационной цепочке ограничений
        $obPageNavigation->setRecordCount($arResult['count'])->setPageSize($iPageLimit);

        $this->arResult['ITEMS'] = $arResult['biblioCardId'];
        $this->arResult['COUNT'] = $arResult['count'];
        $this->arResult['NAV']   = $obPageNavigation;
    }

    /**
     * Детальная страница: получение информации по изданию и его произведениям
     */
    private function getMarkupList() {

        $sBookId = trim($this->arParams['VARIABLES']['ID']);

        if (empty($sBookId) || !Loader::includeModule('nota.exalead'))
            exit();

        // запрос издания из Exalead
        $query = new SearchQuery();
        $query->getById($sBookId);
        $client = new SearchClient();
        $this->arResult['BOOK'] = $client->getResult($query);

        if (empty($this->arResult['BOOK']['id']))
            exit();

        // запрос лога произведений
        $arQueryParams = ['token' => $this->sUserToken];
        $obRslViewer = new RslViewer($sBookId);
        $this->arResult['ITEMS'] = $obRslViewer->getMarkupLog($arQueryParams);
    }
}