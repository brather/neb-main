function checkIsAvailablePdf(){
	$( "div.search-result" ).each(function() {
		var book_item = this;

		if(!$(book_item).data('CheckIsAvailable'))
		{
			$(this).find('.b-result-type_txt').each(function() {
				if($(this).data('ext') == 'pdf'){
					var obPdf = this;
					$.getJSON( "/local/tools/viewer/isavailable.php", { book_id: book_item.id }, function( data ) {
						if(data.isAvailable){
							$(obPdf).show();
						}
						else
						{
							//$(obPdf).remove();
						}

						$(book_item).data('CheckIsAvailable', true);
					});
				}
			});
		}
	});
}

function readBook(event, element) {
    $('.boockard-error-msg').hide();
    $('.read-button-text').hide();
    $('.read-button-preloader').show();
    var data = {};
    var options = $(element).attr('data-options');
    try {
        options = $.parseJSON(options);
    } catch (e) {
        options = {};
    }
    options.id = $(element).attr('data-link');
    options.token = '';
    options.appUrl = '';

    var getAction = function () {
        var existsApp = (device.ios() || device.android());
        var action = null;
        if ('error' == data.result) {
            action = 'error';
        } else if (!existsApp && (device.mobile() || device.tablet())) {
            action = 'notExistsMobileApp';
        } else if (!existsApp && 'open' === data.result) {
            action = 'webView';
        } else if (('open' !== data.result && false === data.isAuthorized) && device.ios()) {
            action = 'iosNotAuthorized';
        } else if (('open' !== data.result && false === data.isAuthorized) && device.android()) {
            action = 'andriodNotAuthorized';
        } else if ('open' !== data.result && false === data.isAuthorized) {
            action = 'notAuthorized';
        } else if (data.hasOwnProperty('redirectUrl') && 'open' !== data.result) {
            action = 'redirectUrl';
        } else if ('verified' !== data.status && 'open' !== data.result) {
            action = 'notVerified';
        } else if (device.ios()) {
            action = 'iosApp';
        } else if (device.android()) {
            action = 'andriodApp';
        } else if ('verified' === data.status && 'open' !== data.result) {
            action = 'desktopApp';
        } else {
            action = 'error';
        }
        return action;
    };


    var action = function () {
        var autoRegForm =  $('.popup_autoreg_form');
        var closeLink = autoRegForm.find('.closed-link');
        var action = getAction();
        var queryParams = {};
        var hideAutoregViewerLink = function() {
            $('.popup_autoreg_form p.get-viewer').hide();
        };
        if (options.hasOwnProperty('page')) {
            queryParams.page = options.page;
        }
        switch (action) {
            case 'webView':
                queryParams = $.param(queryParams);
                if (queryParams) {
                    queryParams = '?' + queryParams;
                }
                window.open('/catalog/' + options.id + '/viewer/' + queryParams);
                break;
            case 'notExistsMobileApp':
                messageOpen('.b-message');
                break;
            case 'notAuthorized':
                closeLink.click(function (e) {
                    e.preventDefault();
                    readBook(event, element);
                });
                autoRegForm.autoregPopup();
                break;
            case 'notVerified':
                $('.popup_book_access_deny').simplePopup();
                break;
            case 'iosNotAuthorized':
                closeLink.click(function (e) {
                    e.preventDefault();
                    readBook(event, element);
                });
                autoRegForm.autoregPopup({open: hideAutoregViewerLink});
                break;
            case 'andriodNotAuthorized':
                closeLink.click(function (e) {
                    e.preventDefault();
                    readBook(event, element);
                });
                autoRegForm.autoregPopup({open: hideAutoregViewerLink});
                break;
            case 'iosApp':
                options.appUrl = 'http://itunes.apple.com/app/id944682831';
                startViewerApp(options.id, options.token, options);
                break;
            case 'andriodApp':
                options.appUrl = 'https://play.google.com/store/apps/details?id=ru.elar.neb.viewer';
                startViewerApp(options.id, options.token, options);
                break;
            case 'redirectUrl':
                if (data.hasOwnProperty('redirectUrl')) {
                    window.open(data.redirectUrl);
                }
                break;
            case 'desktopApp':
                if (data.hasOwnProperty('redirectUrl')) {
                    window.open(data.redirectUrl);
                    break;
                }
                queryParams = $.param(queryParams);
                if (queryParams) {
                    queryParams = '&' + queryParams;
                }
                window.open('spd:https://выдача.нэб.рф/?id=' + options.id + '&token=' + options.token + queryParams, '_self');
                break;
            default:
                alert('Книга временно недоступна.');
                break;
        }
    };

    $.ajax({
        url: '/local/tools/access_closed_books.php',
        async: false,
        dataType: 'json',
        data: {book_id: options.id},
        success: function (result) {
            data = result;

            if (data.hasOwnProperty('token')) {
                options.token = data.token;
            }
            options.isOpen = 'open' === data.result;
            options.status = 'verified' === data.status ? 1 : 0;

            action();

            $('.boockard-read-button').attr('data-load','load');
            $('.read-button-preloader').hide();
            $('.read-button-text').show();
        },
        error: function () {
            $('.boockard-read-button').attr('data-load','load');
            $('.read-button-preloader').hide();
            $('.read-button-text').show();
            $('.boockard-error-msg').text('Произошла ошибка, повторите попытку').show();
        }
    });
}

/**
 * @param id
 * @param token
 * @param options
 */
function startViewerApp(id, token, options) {
    options = options || {};
    if (!token) {
        token = '';
    }
    var params = [
        'book_id=' + id,
        'token=' + token,
    ];
    if (options.hasOwnProperty('author')) {
        params.push('author=' + options.author);
    }
    if (options.hasOwnProperty('title')) {
        params.push('title=' + options.title);
    }
    if (options.hasOwnProperty('isOpen')) {
        params.push('is_open=' + (options.isOpen ? '1' : '0'));
    }
    if (options.hasOwnProperty('status')) {
        params.push('status=' + options.status);
    }
    var time = (new Date()).getTime();
    if (device.android() && navigator.userAgent.match(/Chrome/)) {
        window.open('intent://?' + params.join('&') + '#Intent;scheme=viewer;package=ru.elar.neb.viewer;end');
    } else {
        var to = setTimeout(function () {
            if (confirm('Приложение не установлено на вашем устройстве, вы хотите установить его?')) {
                document.location = options.appUrl;
            }
        }, 3000);
        $(window).one('blur', function() {
            clearTimeout(to);
        });
        location.href = 'viewer://?' + params.join('&');
    }
}
