<?
$MESS['RIGHTHOLDER_BOOKS_ADD_BOOK'] = "Добавить книгу";
$MESS['RIGHTHOLDER_BOOKS_SEARCH_ALL'] = "по всем полям";
$MESS['RIGHTHOLDER_BOOKS_SEARCH_TITLE'] = "по названию";
$MESS['RIGHTHOLDER_BOOKS_SEARCH_AUTHOR'] = "по автору";
$MESS['RIGHTHOLDER_BOOKS_SEARCH'] = "Найти";
$MESS['RIGHTHOLDER_BOOKS_TITLE'] = "Название";
$MESS['RIGHTHOLDER_BOOKS_AUTHOR'] = "Автор";
$MESS['RIGHTHOLDER_BOOKS_BBK'] = "ББК";
$MESS['RIGHTHOLDER_BOOKS_PUBLISH_YEAR'] = "Год издания";
$MESS['RIGHTHOLDER_BOOKS_DATE_ADD'] = "Дата добавления";
$MESS['RIGHTHOLDER_BOOKS_PDF_LINK'] = "Ссылка<br> на pdf";
$MESS['RIGHTHOLDER_BOOKS_DESCRIPTION'] = "Описание";
$MESS['RIGHTHOLDER_BOOKS_DEL_REQUEST'] = 'Запрос <br> <span class="nowrap">на удаление</span>';
$MESS['RIGHTHOLDER_BOOKS_VIEW'] = "Посмотреть";
$MESS['RIGHTHOLDER_BOOKS_EDIT'] = "Редактировать";
$MESS['RIGHTHOLDER_BOOKS_REQUEST_TO_DEL'] = "Запросить удаление";
$MESS['RIGHTHOLDER_BOOKS_CANCEL_DEL'] = "Отменить<br>запрос";
$MESS['RIGHTHOLDER_BOOKS_WITHDRAW'] = "Отозвать запрос на удаление";
?>
