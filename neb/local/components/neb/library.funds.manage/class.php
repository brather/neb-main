<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule("nota.exalead");

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;
use Nota\Exalead\BooksAdd;
use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;

class LibraryFundsManageComponent extends \CBitrixComponent
{
    /**
     * @var nebUser
     */
    protected $user;
    protected $userLibrary;

    public function onIncludeComponentLang()
    {
        $this -> includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    public function onPrepareComponentParams($params)
    {
        $result = array(
            'LIBRARY_ID' => intval($params['LIBRARY_ID']),
        );
        return $result;
    }

    /**
     * Проверка на корректность обязательных параметров компонента
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        $this->user = new nebUser();

        if ($this -> arParams['LIBRARY_ID'] <= 0 && $this->user->getRole() != 'operator')
            throw new Main\ArgumentNullException('LIBRARY_ID');
    }

    /**
     * Проверка прав доступа
     */
    protected function checkAccess()
    {
        $this->user = new nebUser();
        $this->userLibrary = $this->user->getLibrary();

        global $APPLICATION;
        if ((!is_array($this->userLibrary) || $this->userLibrary['ID'] != $this->arParams['LIBRARY_ID']) && $this->user->getRole() != 'operator')
            $APPLICATION->AuthForm("У вас нет права доступа к данному файлу.");
    }

    /**
     * Обработка действий
     */
    protected function checkActions()
    {
        $action = (isset($_REQUEST['action']) ? trim($_REQUEST['action']) : '');

        if (!empty($action) && check_bitrix_sessid())
        {
            switch($action)
            {
                case 'addBook':
                    $this->addBook($_REQUEST);
                    break;
                case 'addBooksXLS':
                    $this->addBooksXLS($_REQUEST['FILE_PDF']);
                    break;
                case 'editBook':
                    $this->editBook($_REQUEST);
                    break;
                case 'deleteBook':
                    $this->deleteBook($_REQUEST['bookId']);
                    break;
                case 'toDel':
                    $this->deleteBook($_REQUEST);
                    break;
                case 'undotoDel':
                    $this->deleteBook($_REQUEST);
                    break;
                case 'editBookForm':
                    $this->editBookForm($_REQUEST);
                    break;
            }
        }
    }



    /**
     * Добавление
     * @param $request
     */
    protected function addBook($request)
    {
        if (isset($request['BOOK_NUM_ID'])) {

            if (is_array($request['BOOK_NUM_ID'])) {
                $books = array_filter($request['BOOK_NUM_ID']);
            } else {
                $books = array((integer)$request['BOOK_NUM_ID']);
            }
            $book = new BooksAdd();

            foreach ($books as $bookNum) {
                $bookID = htmlspecialcharsEx($request['BOOK_ID_' . $bookNum]);
                $libID = null;
                if (true === $this->user->isLibrary()) {
                    $libID = str_pad(
                        intval(
                            $this->userLibrary['PROPERTY_LIBRARY_LINK_VALUE']
                        ), 6, '0', STR_PAD_LEFT
                    );
                }
                $pdfFile = null;
                if (isset($request['FILE_PDF_' . $bookNum])) {
                    $pdfFile = htmlspecialcharsEx(
                        $request['FILE_PDF_' . $bookNum]
                    );
                }
                if (!$pdfFile) {
                    continue;
                }

                $fields = array(
                    'Id'       => $bookID,
                    'PDF_LINK' => $pdfFile,
                    'LIB_ID'   => $libID
                );
                $book->add($fields);
            }

            ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Файлы добавлены"));
        } else {
            ShowError(Loc::getMessage('LIBRARY_FUNDS_POPUP_CHOOSE_BOOK'));
        }
    }


    /**
     * Добавление/изменение карточек (загрузка из файла)
     * @param $file
     */
    protected function addBooksXLS($file)
    {
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel.php");
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel/IOFactory.php");
        $arFields = array(
            'IdFromALIS',
            'Author',
            'Name',
            'SubName',
            'PublishYear',
            'PublishPlace',
            'Publisher',
            'ISBN',
            'LanguageText',
            'CountPages',
            'Printing',
            'Format',
            'Series',
            'VolumeNumber',
            'VolumeName',
            'AccessType',
            'Responsibility',
            'Annotation',
            'ContentRemark',
            'CommonRemark',
            'PublicationInformation',
            'BBKText',
            'UDKText',
        );
        // идентификатор библиотеки
        $libID = str_pad(intval($this->userLibrary['PROPERTY_LIBRARY_LINK_VALUE']), 6, '0', STR_PAD_LEFT);
        //найти ALIS библиотеки

        $res = \Nota\Exalead\BiblioAlisTable::getList(array(
            'filter' => array(
                'Library' => intval($libID)
            ),
            'select' => array(
                'Id'
            )
        ));



        if ($massALIS = $res->fetchAll()) $ALIS = $massALIS[0]['Id'];

        $e = PHPExcel_IOFactory::load($_SERVER['DOCUMENT_ROOT'].$file);
        $objWorksheet = $e->getActiveSheet();
        $i = 0;
        foreach ($objWorksheet->getRowIterator() as $key => $row):
            $fields = array();
            if (1 == $key) continue;

            foreach ($row->getCellIterator() as $k=>$cell):
                $fields[$arFields[$k]] = trim($cell->getValue());

                if(empty($cellValue)):
                    continue;
                endif;

            endforeach;

            //загрузка карточки
            if (count($fields) > 1 ) {
                $fields['ALIS'] = $ALIS;

                $fields['Name'] 	= implode(' ', Array($fields['Name'], $fields['SubName'], $fields['VolumeName'], $fields['VolumeNumber']));

                if (preg_match('/(\S*)\s(\S*)\s(\S*)/', $fields['Author'], $tmp) == true )
                    $fields['Author2'] = $tmp[1]." ".substr($tmp[2],0,2).".".substr($tmp[3],0,2).".";
                else
                    $fields['Author2'] = $fields['Author'];

                $fields['Responsibility2'] = $fields['Author'];

                $fields['PublishPlace'] = str_replace(array('М.','Л.','Спб'),array('Москва','Ленинград','Санкт-Петербург'),$fields['PublishPlace']);
                //var_dump($fields['IdFromALIS']);
                // проверить есть ли книга по IdFromALIS

                $res = \Nota\Exalead\LibraryBiblioCardTable::getList(array(
                    'filter' => array(
                        'IdFromALIS' => $fields['IdFromALIS'],
                        'ALIS' =>  $fields['ALIS']
                    ),
                    'select' => array(
                        'Id'
                    )
                ));

                $book = $res->fetchAll();
                $arrFields = array();
                foreach ($fields as $name=>$value){

                    $arrFields[] = "`".$name."` = '".htmlspecialchars($value)."'";
                }

                if ($book[0]['Id']){

                    //обновление карточки
                    $result = \Nota\Exalead\LibraryBiblioCardTable::getEntity()->getConnection()->query("
					UPDATE
						`tbl_common_biblio_card`
					SET ".implode(",", $arrFields)." ,
						`UpdatingDateTime` = now()
					WHERE `Id` = ".$book[0]['Id']." ");


                }else{
                    //добавление карточки
                    $result = \Nota\Exalead\LibraryBiblioCardTable::getEntity()->getConnection()->query("
					INSERT
						`tbl_common_biblio_card`
					SET ".implode(",", $arrFields)." ,
						`CreationDateTime` = now(),
						`UpdatingDateTime` = now()
				 ");

                }
                $i++;
            }

        endforeach;



        global $APPLICATION;
        ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Загруженно ".$i." записей"));
        //LocalRedirect($APPLICATION->GetCurPage());
    }

    /**
     * Добавление / редактирование книги
     * @param $request
     */
    protected function editBook($request)
    {
        global $APPLICATION;
        $libId = BooksAdd::setLibrary($this->userLibrary['PROPERTY_LIBRARY_LINK_VALUE'], $this->userLibrary['NAME']);

        $fields = array(
            'ALIS' => $libId,
            'Author' => htmlspecialcharsEx($request['Author']),
            'Name' => htmlspecialcharsEx($request['Name']),
            'SubName' => htmlspecialcharsEx($request['SubName']),
            'PublishYear' => htmlspecialcharsEx($request['PublishYear']),
            'PublishPlace' => htmlspecialcharsEx($request['PublishPlace']),
            'Publisher' => htmlspecialcharsEx($request['Publisher']),
            'ISBN' => htmlspecialcharsEx($request['ISBN']),
            //'Language' => $request['Language'],
            'CountPages' => htmlspecialcharsEx($request['CountPages']),
            'Printing' => htmlspecialcharsEx($request['Printing']),
            'Format' => htmlspecialcharsEx($request['Format']),
            'Series' => htmlspecialcharsEx($request['Series']),
            'VolumeNumber' => htmlspecialcharsEx($request['VolumeNumber']),
            'VolumeName' => htmlspecialcharsEx($request['VolumeName']),
            'Collection' => htmlspecialcharsEx($request['Collection']),
            'AccessType' => htmlspecialcharsEx($request['AccessType']),
            'isLicenseAgreement' => htmlspecialcharsEx($request['isLicenseAgreement']),
            'Responsibility' => htmlspecialcharsEx($request['Responsibility']),
            'Annotation' => htmlspecialcharsEx($request['Annotation']),
            'ContentRemark' => htmlspecialcharsEx($request['ContentRemark']),
            'CommonRemark' => htmlspecialcharsEx($request['CommonRemark']),
            'PublicationInformation' => htmlspecialcharsEx($request['PublicationInformation']),
            'pdfLink' => htmlspecialcharsEx($request['pdfLink']),
            'BBKText' => htmlspecialcharsEx($request['BBKText']),
            'UDKText' => htmlspecialcharsEx($request['UDKText']),
            //'UDKText' => htmlspecialcharsEx($request['UDKText']),
            'CreationDateTime' => htmlspecialcharsEx(ConvertDateTime($request['CreationDateTime'], 'Y-m-d 00:00:00')),
            'pdfLink' => htmlspecialcharsEx($request['FILE_PDF']),
            //PDF
            //'LanguageText' => $request['LanguageText'],
            'BX_IS_UPLOAD' => null,
            'BX_LIB_ID' => $this->userLibrary['ID'],
            'toDel' => htmlspecialcharsEx($request['toDel'])
        );

        $bookId = intval($request['bookId']);

        if ($bookId <= 0)
        {
            // Добавление новой книги
            BooksAdd::add($fields, $this->userLibrary['PROPERTY_LIBRARY_LINK_VALUE']);
        }
        else
        {
            // Редактирование книги
            BooksAdd::edit($bookId, $fields);
        }

        LocalRedirect($APPLICATION->GetCurPage());
    }

    /**
     * Форма добавления/редактирования книги
     * @param $request
     */
    protected function editBookForm($request)
    {
        $bookId = intval($request['bookId']);
        if ($bookId > 0)
        {
            /**
             * Редактирование книги. Подгружаем сюда данные из существующей книги
             */
            $filter = array(
                'and Id="'.$bookId.'"'
            );
            $select = array();
            $books = BooksAdd::getListBooksLibrary($this->arParams['LIBRARY_ID'], $filter, $select, 'Id', 'asc');
            if (!empty($books[0]))
                $this->arResult['FORM'] = $this->prepareBook($books[0]);
        }
        else
        {
            /**
             * Добавление книги, выставляем значения по-умолчанию
             */
            $fields = array(
                'CreationDateTime' => FormatDate('d.m.Y', time())
            );
            $this->arResult['FORM'] = $fields;
        }

        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        $this->IncludeComponentTemplate('editBookForm');
        die;
    }

    /**
     * Удаление книги
     * @param $id
     */
    protected function deleteBook($id)
    {
        BooksAdd::remove($id);

        global $APPLICATION;
        LocalRedirect($APPLICATION->GetCurPage());
    }

    /**
     * Подготовка полей книги для вывода на сайте / форме
     * @param $bookFields
     * @return mixed
     */
    public function prepareBook($bookFields)
    {
        $bookFields['CreationDateTime'] = FormatDate('d.m.Y', strtotime($bookFields['CreationDateTime']));
        return $bookFields;
    }

    protected function getResult()
    {
        global $APPLICATION;
        $arFilter = array();

        $q = htmlspecialcharsbx($_REQUEST['qbook']);
        $by = htmlspecialcharsbx($_REQUEST['by']);
        $order = htmlspecialcharsbx($_REQUEST['order']);

        $lib_id = $this->arParams['LIBRARY_ID'];
        $obNUser = new nebUser();
        $arLibrary = $obNUser->getLibrary();
        $idLibrary = $arLibrary["PROPERTY_LIBRARY_LINK_VALUE"];

        CPageOption::SetOptionString("main", "nav_page_in_session", "N");
        $arParams['ITEM_COUNT'] = intval($_REQUEST['pagen']) > 0 ? intval($_REQUEST['pagen']) : 15;
        if($arParams['ITEM_COUNT'] > 60) $arParams['ITEM_COUNT'] = 60;
        $arParams['NEXT_ITEM'] = intval($_REQUEST['next']);
        $arParams['MODE'] = empty($_REQUEST['mode']) ? 'list' : trim($_REQUEST['mode']);

        $arParams['IS_SHOW_PROTECTED'] = nebUser::isShowProtectedContent();

        $by = trim(htmlspecialcharsEx($_REQUEST['by']));
        $order = trim(htmlspecialcharsEx($_REQUEST['order']));

        $arParams['q'] = ($q == ''?'#all' : 'simple:('.$q.')').($idLibrary > 0 ? ' AND idlibrary:'.$idLibrary."":"");

        $query = new SearchQuery($arParams['q']);
        $query->setPageLimit($arParams['ITEM_COUNT']);
        $query->setNavParams();

        if(!empty($by) and !empty($order)){
            $query->setParam('s', $order.'('.$by.')');
        }

        $client = new SearchClient();
        $arSearchResult = $client->getResult($query);

        if(intval($arSearchResult['COUNT']) > 0)
        {
            $client->getDBResult($arSearchResult);
            $this->arResult['STR_NAV'] = $client->strNav;

            $this->arResult += $arSearchResult;

        }

        $bookIds = array();
        $deleteParams = array();
        if (true === $this->user->isLibrary()) {
            $deleteParams['libraryId'] = str_pad(
                intval(
                    $this->userLibrary['PROPERTY_LIBRARY_LINK_VALUE']
                ), 6, '0', STR_PAD_LEFT
            );
        }
        foreach ($this->arResult['ITEMS'] as $key => &$item) {
            $item['isDeleted'] = false;
            $deleteParams['bookId'] = $item['id'];

            $item['deleteUrl'] = '/rest_api/funds/?'
                . http_build_query($deleteParams);
            $bookIds[$item['id']] = $key;
        }
        unset($item);
        if (!empty($bookIds)) {
            $data = \Nota\Exalead\LibraryBiblioCardTable::getByFullSymbolicId(
                array_keys($bookIds)
            );

            foreach ($data as $item) {
                if ('1' === $item['is_delete']
                    && isset($this->arResult['ITEMS'][$bookIds[$item['FullSymbolicId']]])
                ) {
                    $this->arResult['ITEMS'][$bookIds[$item['FullSymbolicId']]]['isDeleted']
                        = true;
                }
            }
        }

        $APPLICATION->SetTitle(Loc::getMessage('LIBRARY_FUNDS_TITLE'));
        return $arResult;
    }

    public function executeEpilog()
    {

    }

    public function executeComponent()
    {
        try
        {
            $this->checkParams();
            $this->checkAccess();
            $this->checkActions();

            $this->getResult();

            $this->IncludeComponentTemplate();

            return $this -> executeEpilog();
        }
        catch (Exception $e)
        {
            ShowError($e -> getMessage());
        }
    }

    protected function toDel($request){
        $arFields = array(
            'toDel' 			=> htmlspecialcharsEx(1)
        );
        $bookId = (int)$request['bookId'];
        if (intval($bookId) <= 0 || empty($arFields)) return;

        global $DB;
        foreach($arFields as &$Field)
        {
            $Field = '"'.$DB->ForSql($Field).'"';
        }
        $DB->Update('tbl_common_biblio_card', $arFields, "WHERE ID='".$bookId."'", $err_mess.__LINE__);
        print_r($err_mess);
        //LocalRedirect($APPLICATION->GetCurPage());
    }

    protected function undotoDel($request){
        $arFields = array(
            'toDel' 			=> htmlspecialcharsEx(0)
        );
        $bookId = (int)$request['bookId'];
        if (intval($bookId) <= 0 || empty($arFields)) return;

        global $DB;
        foreach($arFields as &$Field)
        {
            $Field = '"'.$DB->ForSql($Field).'"';
        }
        $DB->Update('tbl_common_biblio_card', $arFields, "WHERE ID='".$bookId."'", $err_mess.__LINE__);
        LocalRedirect($APPLICATION->GetCurPage());
    }
}