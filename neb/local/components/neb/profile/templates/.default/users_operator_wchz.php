<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Список активных пользователей');
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */

$APPLICATION->IncludeComponent(
    'neb:user.list.wchz', '',
    Array(
        'FIO'               => $_REQUEST['FIO'],
        'SUBORDINATE_GROUP' => UGROUP_OPERATOR_WCHZ,
    ),
    $component
);

