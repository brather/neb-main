<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161004072912 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID)
VALUES (40, NOW(), 'Y', 500, 'N', 'Ассессор', NULL, NULL, 'accessor');
"
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
