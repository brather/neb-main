<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('popup'));

if((int)$arResult['VALUES']['USER_ID'] > 0 ) {
    $USER->Authorize($arResult['VALUES']['USER_ID']);
    header("Location: /profile/");
    exit();
}

?>
<header>
    <div class="container">
        <div class="row">
            <div class="neb-logo-mid">
                <a href="/" title="<?=Loc::getMessage('NATIONAL_ELECTRONIC_LIBRARY')?>">
                    <img src="/local/templates/adaptive/img/logo-mid.png" alt="<?=Loc::getMessage('NATIONAL_ELECTRONIC_LIBRARY')?>" class="">
                </a>
            </div>
        </div>
    </div>
</header>

<main>
    <div class="container">
        <div class="login-form">
            <h2 class="login-form__title"><?=Loc::getMessage('AUTH_REGISTER');?></h2>
            <p class="text-center" style="display: none;"><?=Loc::getMessage('FULL_ACCESS_REGISTER')?></p>
            <div class="social-auth">
                <h3 class="text-centered"><?=Loc::getMessage('SOCIAL_REGISTER')?></h3>
                <ul class="social-auth__list clearfix">
                    <li class="social-auth__list-kind">
                        <a href="#" onclick="BX.util.popup('/WebServices/Esia.php?act=do_login', 800, 600)" class="social-auth__list-link social-auth__list-link--gu" title="<?=Loc::getMessage('AUTH_GOSUSLUGI')?>">
                        </a>
                    </li>
                    <li class="social-auth__list-kind">
                        <a href="#"  onclick="BX.util.popup('/auth/rgb.php?redirect=1&show_errors=1', 800, 600)"  class="social-auth__list-link social-auth__list-link--rgb" title="<?=Loc::getMessage('AUTH_RGB')?>">
                        </a>
                    </li>
                    <li class="social-auth__list-kind">
                        <a href="#" onclick="BX.util.popup('https://oauth.vk.com/authorize?client_id=4602101&redirect_uri=http%3A%2F%2Fxn--90ax2c.xn--p1ai%3A80%2F%3Fauth_service_id%3DVKontakte&scope=friends,offline&response_type=code&state=site_id%3Ds1%26backurl%3D%252F%253Fcheck_key%253D2a087c3763e9c2ef481a7cbb1ba4c3bd%26redirect_url%3D%252F', 730, 580)" class="social-auth__list-link social-auth__list-link--vk" title="<?=Loc::getMessage('AUTH_VKONTAKTE')?>">
                        </a>
                    </li>
                    <li class="social-auth__list-kind">
                        <a href="#" onclick="BX.util.popup('http://www.odnoklassniki.ru/oauth/authorize?client_id=1129337088&redirect_uri=http%3A%2F%2Fxn--90ax2c.xn--p1ai%2F%3F&response_type=code&state=site_id%3Ds1%26backurl%3D%252F%253Fcheck_key%253D2a087c3763e9c2ef481a7cbb1ba4c3bd%26redirect_url%3D%252F%26mode%3Dopener', 580, 400)" class="social-auth__list-link social-auth__list-link--ok" title="<?=Loc::getMessage('AUTH_ODNOKLASSINIKI')?>">
                        </a>
                    </li>
                    <li class="social-auth__list-kind">
                        <a href="#" onclick="BX.util.popup('https://www.facebook.com/dialog/oauth?client_id=1521410158074781&redirect_uri=http%3A%2F%2Fxn--90ax2c.xn--p1ai%3A80%2F%3Fauth_service_id%3DFacebook%26check_key%3D2a087c3763e9c2ef481a7cbb1ba4c3bd%26backurl%3D%252F&scope=email,publish_actions&display=popup', 580, 400)"  class="social-auth__list-link social-auth__list-link--fb" title="<?=Loc::getMessage('AUTH_FACEBOOK')?>">
                        </a>
                    </li>
                </ul>
            </div>

            <div class="error_container">
				<?
					if (count($arResult["ERRORS"]) > 0){
						foreach ($arResult["ERRORS"] as $key => $error){
							if (intval($key) == 0 && $key !== 0) {
								$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
							}
						}

						ShowError(implode("<br />", $arResult["ERRORS"]).'<br /><br />');

					}elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && (int)$arResult['VALUES']['USER_ID'] > 0 ){
					?>
					<p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
					<?
					}
				?>
			</div>

            <form id="shortregform" action="<?=POST_FORM_ACTION_URI?>" method="post"  enctype="application/x-www-form-urlencoded">
                <?
                if($arResult["BACKURL"] <> ''){
                    ?>
                    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                    <?
                }
                ?>
                <input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value="" />

                <div class="form-group">
                    <label for="email" class="login-form__label">E-mail</label>
                    <input type="email" class="form-control login-form__input" id="email" name="REGISTER[EMAIL]" data-minlength="2" data-maxlength="50" required >
                </div>
                <div class="form-group">
                    <label for="enterpassword" class="login-form__label"><?=Loc::getMessage('PASSWORD')?></label>
                    <input type="password" class="form-control login-form__input" id="enterpassword"  data-required="true" value="<?=$arResult["VALUES"]['PASSWORD']?>" name="REGISTER[PASSWORD]" required> <!-- pattern="[A-Za-z0-9]{6,}" -->
                </div>
                <div class="form-group">
                    <label for="confirmpassword" class="login-form__label"><?=Loc::getMessage('CONFIRM_PASSWORD')?></label>
                    <input type="password" class="form-control login-form__input" id="confirmpassword" data-required="true" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" name="REGISTER[CONFIRM_PASSWORD]" required>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-12">
                        <button type="submit" class="btn btn-primary btn-lg col-md-12 col-sm-12 col-xs-12 login-form-btn" name="register_submit_button" value="register"><?=Loc::getMessage('MAIN_REGISTER_REGISTER')?></button>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <button type="button" class="btn btn-default btn-lg col-md-12 col-sm-12 col-xs-12 login-form-btn" onclick="document.location.href='/'"><?=Loc::getMessage('AUTH_CANCEL')?></button>
                    </div>
                </div>
            </form>
            <script>
            	$(function(){
					jQuery.validator.addMethod("latin", function(value, element) {
					    return this.optional(element) || /^[A-Za-z0-9\w]{6,20}/.test(value);
					}, "Используйте только латинские буквы и цифры");
					
					$('#shortregform').validate({
					    rules: {
					        "REGISTER[EMAIL]": {
					            required: true,
					            email: true
					        },
					        "REGISTER[PASSWORD]": {
					            required: true,
					            minlength: 6,
					            latin: true
					        },
					        "REGISTER[CONFIRM_PASSWORD]": {
					            required: true,
					            minlength: 6,
					            latin: true,
					            equalTo: "#enterpassword"
					        }
					    },
					    messages: {
					        "REGISTER[EMAIL]": {
					            required: "Введите адрес электронной почты",
					            email: "В адресе электронной почты обнаружена ошибка"            
					        },
					        "REGISTER[PASSWORD]": {
					            required: "Введите пароль",
					            minlength: jQuery.validator.format("не менее {0} символов")
					        },
					        "REGISTER[CONFIRM_PASSWORD]": {
					            required: "Повторите пароль",
					            minlength: jQuery.validator.format("не менее {0} символов"),
					            equalTo: "Пароль должен совпасть с паролем, введённым выше"

					        }
					    },
					    errorPlacement: function(error, element) {
					        error.appendTo(element.parent());
					        $(element).closest('.form-group').toggleClass('has-error', true);
					    },
					    /*specifying a submitHandler prevents the default submit, good for the demo
					    submitHandler: function() {
					        alert("submitted!");
					    },
					    set this class to error-labels to indicate valid fields*/
					    success: function(label) {
					        /* set &nbsp; as text for IE */
					        label.html("&nbsp;").addClass("resolved");
					    },
					    highlight: function(element, errorClass, validClass) {        
					        $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
					        $(element).parent().find("." + errorClass).removeClass("resolved");
					    },
					    unhighlight: function(element, errorClass, validClass) {
					    	$(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
					    }
					});


            	});
            </script>
    	</div>
    </div>
</main>

<?php if (false):?>
<!--

<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode"><?=Loc::getMessage('MAIN_REGISTER_TITLE');?></h2>
		<?
			if (count($arResult["ERRORS"]) > 0){
				foreach ($arResult["ERRORS"] as $key => $error){
					if (intval($key) == 0 && $key !== 0) {
						$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
					}
				}

				ShowError(implode("<br />", $arResult["ERRORS"]).'<br /><br />');

			}elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && (int)$arResult['VALUES']['USER_ID'] > 0 ){
			?>
			<p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
			<?
			}
		?>
		<form action="<?=POST_FORM_ACTION_URI?>" class="b-form b-form_common b-regform" method="post" name="regform" enctype="multipart/form-data" onsubmit="setlogin()" id="regform">
			<?
				if($arResult["BACKURL"] <> ''){
				?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?
				}
			?>
			<input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value="" />

			<p class="note"><?=Loc::getMessage('MAIN_REGISTER_IMPORTANT');?></p>
			<div class="b-form_header"><span class="iblock"><?=Loc::getMessage('MAIN_REGISTER_PASSPORT_DATA');?></span></div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings01"><?=Loc::getMessage('MAIN_REGISTER_LASTNAME');?></label>
					<div class="field validate">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>	
						<input type="text" data-validate="fio" data-required="required" value="<?=$arResult["VALUES"]['LAST_NAME']?>" id="settings01" data-maxlength="30" data-minlength="2" name="REGISTER[LAST_NAME]" class="input">										
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings02"><?=Loc::getMessage('MAIN_REGISTER_FIRSTNAME');?></label>
					<div class="field validate">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>		
						<input type="text" data-required="required" data-validate="fio" value="<?=$arResult["VALUES"]['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="REGISTER[NAME]" class="input" >										
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings22"><?=Loc::getMessage('MAIN_REGISTER_MIDNAME');?></label>
					<div class="field validate">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
						<input type="text" data-validate="fio" value="<?=$arResult["VALUES"]['SECOND_NAME']?>" id="settings22" data-maxlength="30" data-minlength="2" data-required="false"  name="REGISTER[SECOND_NAME]" class="input" >										
					</div>
				</div>
			</div>

			<div class="b-form_header"><?=Loc::getMessage('MAIN_REGISTER_ENTER');?></div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings04"><?=Loc::getMessage('MAIN_REGISTER_EMAIL');?></label>
					<div class="field validate">
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_EMAIL_FORMAT');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<input type="text" class="input" name="REGISTER[EMAIL]" data-minlength="2" data-maxlength="50" id="settings04" data-validate="email" value="<?=$arResult["VALUES"]['EMAIL']?>" data-required="true">
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings05"><span><?=Loc::getMessage('MAIN_REGISTER_MIN_6S');?></span><?=Loc::getMessage('REGISTER_FIELD_PASSWORD');?></label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="true" value="<?=$arResult["VALUES"]['PASSWORD']?>" id="settings05" data-maxlength="30" data-minlength="6" name="REGISTER[PASSWORD]" class="pass_status input">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings55"><?=Loc::getMessage('MAIN_REGISTER_CONFIRM_PASSWORD');?></label>
					<div class="field validate">
						<em class="error identity "><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_MISMATCH');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
						<input data-identity="#settings05" type="password" data-required="true" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" id="settings55" data-maxlength="30" data-minlength="2" name="REGISTER[CONFIRM_PASSWORD]" class="input" data-validate="password">								
					</div>
				</div>
			</div>
			<hr>
			<!--div class="checkwrapper ">
				<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb33"><label for="cb33" class="black"><?=Loc::getMessage('MAIN_REGISTER_AGRE1');?> </label><a href="/user-agreement/" target="_blank"><?=Loc::getMessage('MAIN_REGISTER_AGRE2');?></a> <label for="cb3" class="black"><?=Loc::getMessage('MAIN_REGISTER_AGRE3');?></label>
			</div>
			<div class="checkwrapper">
				<label class="defautl_cursor"><?=Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP');?> </label> <a href="/local/components/neb/registration/templates/.default/ajax_agreement.php" target="_blank" class="popup_opener ajax_opener closein" data-width="955" data-height="auto"><?=Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP_TERMS');?></a>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix divdisable">
						<button disabled="disabled" type="submit" value="1" class="formbutton btdisable"><?=Loc::getMessage('MAIN_REGISTER_REGISTER');?></button>
						<div class="b-hint bhint_right"><?=Loc::getMessage('MAIN_REGISTER_READ_TERMS');?></div>
						<input type="hidden" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration>
</section>
-->
<?php endif; ?>