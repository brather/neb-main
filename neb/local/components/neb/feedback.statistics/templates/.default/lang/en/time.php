<?php

$MESS['FEEDBACK_STAT_TITLE']      = 'Statistics by reply time';
$MESS['FEEDBACK_STAT_DATE_FROM']  = 'Date from';
$MESS['FEEDBACK_STAT_DATE_TO']    = 'Date to';
$MESS['FEEDBACK_STAT_PERIOD']     = 'Statistics by period';
$MESS['FEEDBACK_STAT_SHOW']       = 'Show statistics';

$MESS['FEEDBACK_STAT_ID']         = 'ID';
$MESS['FEEDBACK_STAT_USER']       = 'User';
$MESS['FEEDBACK_STAT_TIME']       = 'Work time';
$MESS['FEEDBACK_STAT_AVERAGE']    = 'Average work time';

$MESS['FEEDBACK_STAT_DAYS']       = 'd';
$MESS['FEEDBACK_STAT_HOURS']      = 'h';
$MESS['FEEDBACK_STAT_MINUTES']    = 'min';
$MESS['FEEDBACK_STAT_SECONDS']    = 'sec';

$MESS['FEEDBACK_STAT_EXCEL']      = 'Export to .xls';