<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
	\CUtil::InitJSCore(array("jquery"));
	$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);
$inputName = 'FILE_PDF';
if(isset($arParams['INPUT_NAME'])) {
	$inputName = $arParams['INPUT_NAME'];
}
?>
<div class="left">
	<p><?=GetMessage('PLUPLOAD_ADD_BOOKS_PDF_LINK');?></p>
	<div class="b-pdfaddwrapper" id='add-file-block<?=$inputName?>'>
		<a href="#" class="tdu" id="add_file<?=$inputName?>"><?=empty($arParams['ALREADY_UPLOADED_FILE']) ? GetMessage('PLUPLOAD_ADD_BOOKS_ADD') : GetMessage('PLUPLOAD_ADD_BOOKS_UPDATE')?> pdf</a> <em class="errorformat file hidden" id="error-format<?=$inputName?>"><?=GetMessage('PLUPLOAD_ADD_BOOKS_LOAD_FILE');?></em>
	</div> 

	<div class="field validate addpdfield" style="top:11px;">
		<input type="text" value="<?=$arParams['ALREADY_UPLOADED_FILE']?>" id="<?=$inputName?>" data-maxlength="300" data-minlength="2" name="<?=$inputName?>" class="input hidden" />
	</div>
	<div class="b-pdfwrapper hidden" id="pdfwrapper<?=$inputName?>">
		<div class="b-fondtitle b-filename"><span class="b-fieldtext">file.pdf</span><a class="b-fieldeedit" href="#" id="fieldeedit<?=$inputName?>"></a></div>
		<div class="b-loaded"><?=GetMessage('PLUPLOAD_ADD_BOOKS_LOADED');?>: 100%</div>
		<em class="errorformat file hidden"><?=GetMessage('PLUPLOAD_ADD_BOOKS_ERROR_FORMAT');?></em>
	</div>

	<div class="progressbar hidden" id="progressbar<?=$inputName?>"><div class="progress"><span id="file-procent<?=$inputName?>" style="width:50px;"></span></div><div class="text"><?=GetMessage('PLUPLOAD_ADD_BOOKS_LOADED');?>: <span class="num">33</span>%</div></div>
</div>
<div id="container_<?=$arParams['RAND_STR']?>" class="pl_button" style="display: none;"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>
<style type="text/css">
	.b-pdfwrapper .errorformat{	margin-left: -118px;}
</style>
<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'add_file<?=$inputName?>',
				drop_element: "drop_upload",
				container: document.getElementById('add-file-block<?=$inputName?>'), 
				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
					unique_names: true,
					<?
					}
				?>

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				<?
					if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
					{
					?>
					resize: {
						width: <?=intval($arParams['RESIZE_WIDTH'])?>,
						height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
						<?
							if($arParams['RESIZE_CROP'] == 'Y')
							{
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=intval($arParams['RESIZE_QUALITY'])?>
					},
					<?
					}
				?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '1mb',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'
				},
				multi_selection: false,
				/*autostart: true,*/
				init: {

					FilesAdded: function(up, files) {
						up.start();

						$('#pdfwrapper<?=$inputName?> .b-fieldtext').text(files[0].name);

						$('.errorformat').addClass('hidden');
						$('.addpdfield .error').hide();
						$('.addpdfield').removeClass('error');
						$('#pdfwrapper<?=$inputName?>').addClass('hidden');
						$('#progressbar<?=$inputName?>').addClass('hidden');

					},
					FileUploaded: function(up, file, response) {
						$('#progressbar<?=$inputName?>').addClass('hidden');
						$('#pdfwrapper<?=$inputName?>').removeClass('hidden');
						$('.b-loaded').removeClass('hidden');

						var result = response.response;

						if (result) 
						{
							var obResponse = JSON.parse(result);
							$('#<?=$inputName?>').val(obResponse.file);
						}
						if(up.files[0])
							up.removeFile(up.files[0].id);
						up.setOption('browse_button', 'fieldeedit<?=$inputName?>');
						up.refresh();							
					},
					UploadProgress: function(up, file) {
						$('#add-file-block<?=$inputName?>').addClass('hidden');
						$('#progressbar<?=$inputName?>').removeClass('hidden');
						$('#progressbar #file-procent<?=$inputName?>').css('width', file.percent+ '%');
						$('#progressbar .num').text(file.percent);
					},

					Error: function(up, err) {
						$('.addpdfield .error').hide();
						$('.addpdfield').removeClass('error');
						/*console.log(err);*/
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
						if(err.code == -601)
						{
							$('.errorformat').removeClass('hidden');
							$('.b-loaded').addClass('hidden');
						}
					}
				}
			}
		);

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>
