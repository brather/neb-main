<? use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$arComponentDescription = array(
        "NAME" => Loc::getMessage( "NF_DESCRIPTION_COM_NAME" ),
		"DESCRIPTION" => "",
		"ICON" => "/images/icon.gif",
		"SORT" => 10,
		"CACHE_PATH" => "Y",
		"PATH" => array(
			"ID" => "profile", 
		),
		"COMPLEX" => "N",
	);

?>