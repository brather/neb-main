<?
	define('STATIC_PAGE', true);

	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("About the Project");
?> 		 
    <h3>The main aim of the NEB</h3>

    <p>To provide free access of the citizens of the Russian Federation to the electronic funds of federal, regional and municipal libraries as well as to the funds of academic and educational institutions via the Internet.</p>
    <br />
    <h3>The strategy of organizing the NEB</h3>
    <ul class="b-commonlist">
        <li>The single portal of remote access, the integrated process of searching and indexation, the same set of services, the single library pass.</li>
        <li>The distributed electronic fund of the libraries of different levels and departmental affiliation with the integrated technological process and standards of digitalization.</li>
    </ul>

    <h3>The key tasks</h3>
    <ul class="b-commonlist">
        <li>To create the single portal of the NEB – the one access point to the funds of the library;</li>
        <li>To integrate the portal into the system of electronic government, to create the single electronic library pass;</li>
        <li>To provide coordinated filling of the distributed electronic fund;</li>
        <li>The informative development of small libraries on the basis of the NEB.</li>
    </ul>
    <br />
    <p>Since 2004 The Project of the National Electronic Library (NEB) has been developed by the chief Russian libraries under the auspices of Ministry of Culture of the Russian Federation. At present the project of the NEB is supported by three members: the Russian State Library, the Russian National Library and the State Public Scientific and Technical Library of Russia. Nowadays about 30 libraries – partners participate in the project. The total volume of electronic documents of the NEB constitutes 15 million pages in electronic form and the library is being enlarged in accordance with the plan. At this moment the NEB includes documents that are not under the law of copyright or the right of its usage is transferred to the rights holder under contact. In 2013 it is planned to replenish the fund of the NEB by editions and collections from the funds of 15 libraries with total volume of 5 300 000 pages.</p>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>