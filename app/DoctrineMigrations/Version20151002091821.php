<?php

namespace Application\Migrations;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151002091821 extends AbstractMigration
{

    /**
     * @return \Doctrine\DBAL\Connection
     * @throws \Doctrine\DBAL\DBALException
     */
    private function _getExaleadConnection()
    {
        $bitrixConfig = include __DIR__ . '/../../neb/bitrix/.settings.php';
        if (file_exists(__DIR__ . '/../../neb/local/.settings.php')) {
            $bitrixConfig = array_replace_recursive(
                $bitrixConfig,
                include(__DIR__ . '/../../neb/local/.settings.php')
            );
        }
        $exaleadConnectionConfig
            = $bitrixConfig['connections']['value']['library'];

        return DriverManager::getConnection(
            array(
                'dbname'   => $exaleadConnectionConfig['database'],
                'user'     => $exaleadConnectionConfig['login'],
                'password' => $exaleadConnectionConfig['password'],
                'host'     => $exaleadConnectionConfig['host'],
                'driver'   => 'pdo_mysql',
            ), new Configuration()
        );
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
            ALTER TABLE tbl_common_biblio_card
ADD COLUMN status VARCHAR(20) DEFAULT NULL;
SQL
        );

        $exaleadConnection = $this->_getExaleadConnection();
        $stmt = $exaleadConnection->executeQuery(
            <<<SQL
SELECT MAX(Id) AS Id
FROM tbl_common_dictionary_value;
SQL
        );
        $id = $stmt->fetch();
        $id = (integer)$id['Id'];
        $id++;
        $exaleadConnection->executeUpdate(
            <<<SQL
INSERT INTO tbl_common_dictionary_value (Id, Name, Description, Type)
  VALUE ($id, 'Rightholder', 'Rightholder', 5);
SQL
        );
        $exaleadConnection->executeUpdate(
            <<<SQL
INSERT INTO tbl_libraries_alis (Name, Type, Library, PathAndFileName)
  VALUES ('Rightholder', $id, 199, 'rightholder');
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
            ALTER TABLE tbl_common_biblio_card
DROP COLUMN status;
SQL
        );

        $exaleadConnection = $this->_getExaleadConnection();
        $stmt = $exaleadConnection->executeQuery(
            <<<SQL
SELECT
  Id,
  Type
FROM tbl_libraries_alis
WHERE PathAndFileName = 'rightholder';
SQL
        );
        $recurd = $stmt->fetch();
        $id = $recurd['Id'];
        $type = $recurd['Type'];
        $exaleadConnection->executeUpdate(
            <<<SQL
DELETE FROM tbl_libraries_alis
WHERE Id = $id;
SQL
        );
        $exaleadConnection->executeUpdate(
            <<<SQL
DELETE FROM tbl_common_dictionary_value
WHERE Id = $type;
SQL
        );
    }
}
