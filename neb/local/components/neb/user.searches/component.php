<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;

use \Bitrix\Main;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock\HighloadBlockTable;
use \Nota\UserData\Searches;

Loc::loadMessages(__FILE__);

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

Loader::includeModule('highloadblock');
Loader::includeModule('nota.userdat');

$arResult['ERRORS'] = [];

try {
    $hlblock           = HighloadBlockTable::getById(HIBLOCK_SEARCHSERS_USERS)->fetch();
    $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

    if ($_REQUEST['action'] == 'remove' and (int)$_REQUEST['id'] > 0
        and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'
    ) {
        $APPLICATION->RestartBuffer();
        Searches::delete(intval($_REQUEST['id']));
        exit();
    }

    $arParams['ITEM_COUNT'] = empty($_REQUEST['pagen']) ? 15 : intval($_REQUEST['pagen']);

    if (!empty($_REQUEST['by']) and !empty($_REQUEST['order'])) {
        $arOrder = array(strtoupper($_REQUEST['by']) => $_REQUEST['order']);
    } else {
        $arOrder = array("UF_DATE_ADD" => "DESC");
    }

    // pagination
    $limit = array(
        'nPageSize' => $arParams['ITEM_COUNT'],
        'iNumPage'  => is_set($_GET['PAGEN_1']) ? $_GET['PAGEN_1'] : 1,
        'bShowAll'  => false
    );

    $arResult['iNumPage'] = $limit['iNumPage'];

    $arNavigation = CDBResult::GetNavParams($limit);

    if (isset($_GET['dateFrom'])) {
        $arParams['dateFrom'] = $_GET['dateFrom'];
    } else {
        $arParams['dateFrom'] = ConvertTimeStamp(time() - 86400 * 30);
    }

    if (isset($_GET['dateTo'])) {
        $arParams['dateTo'] = $_GET['dateTo'];
    } else {
        $arParams['dateTo'] = ConvertTimeStamp(time());
    }

    if (!strtotime($arParams['dateFrom'])) {
        throw new \Neb\Main\Exception\ValidateException('Некорректная дата "с"');
    }
    if (!strtotime($arParams['dateTo'])) {
        throw new \Neb\Main\Exception\ValidateException('Некорректная дата "по"');
    }

    $arParamsGetList = array(
        "select" => array("*"),
        "filter" => array(
            'UF_UID'        => $USER->GetID(),
            '>=UF_DATE_ADD' => $arParams['dateFrom'],
            '<=UF_DATE_ADD' => $arParams['dateTo'] . ' 23:59:59',
        ),
        "order"  => $arOrder,
        "limit"  => $limit['nPageSize'],
        "offset" => (($limit['iNumPage'] - 1) * $limit['nPageSize']),
    );

    if (intval($arParams['COLLECTION_ID']) > 0) {
        $hlblockLinks            = HighloadBlockTable::getById(HIBLOCK_COLLECTIONS_LINKS_USERS)->fetch();
        $entity_data_classkLinks = HighloadBlockTable::compileEntity($hlblockLinks)->getDataClass();

        $arParamsGetList['runtime'] = array(
            'link' => array(
                "data_type" => $entity_data_classkLinks,
                'reference' => array('=this.ID' => 'ref.UF_OBJECT_ID'),
            ),
        );
        $arParamsGetList['filter']['=link.UF_COLLECTION_ID'] = intval($arParams['COLLECTION_ID']);
        $arParamsGetList['filter']['=link.UF_TYPE'] = 'searches';
    }
    $rsData = $entity_data_class::getList($arParamsGetList);

    //подсчет для постранички, костылим
    $arParamsGetListCnt = $arParamsGetList;
    unset($arParamsGetListCnt['limit'], $arParamsGetListCnt['offset'], $arParamsGetListCnt['order']);
    $arParamsGetListCnt['runtime']['CNT'] = array('expression' => array('COUNT(*)'),
                                                  'data_type'  => 'integer');
    $arParamsGetListCnt['select'] = array('CNT');
    
    $rsDataCnt = $entity_data_class::getList($arParamsGetListCnt); #количество всех элементов
    $arDataCnt = $rsDataCnt->fetch();
    $resultCnt = $arDataCnt['CNT'];

    $rsData = new CDBResult($rsData);
    $rsData->NavStart($arNavigation['SIZEN'], $limit['bShowAll']);
    $rsData->NavRecordCount = $resultCnt;
    $rsData->NavPageSize = $limit['nPageSize'];
    $rsData->bShowAll = $limit['bShowAll'];
    $rsData->NavPageCount = ceil($rsData->NavRecordCount / $rsData->NavPageSize);
    $rsData->NavPageNomer = $limit['iNumPage'];

    $arResult["NAV_STRING"] = $rsData->GetPageNavStringEx($navComponentObject, '', '', $limit['bShowAll']);

    while ($arData = $rsData->Fetch()) {
        $arData['UF_DATE_ADD'] = $arData['UF_DATE_ADD']->toString();
        $arData['UF_DATE_ADD'] = FormatDate(
            'd.m.Y H:i', MakeTimeStamp($arData["UF_DATE_ADD"]), time()
        );

        if (!empty($arData['UF_MORE_OPTIONS'])) {
            $arData['UF_MORE_OPTIONS'] = unserialize(
                htmlspecialcharsBack($arData['UF_MORE_OPTIONS'])
            );
        }

        if (!empty($arData['UF_EXALEAD_PARAM'])) {
            $arData['UF_EXALEAD_PARAM'] = unserialize(
                htmlspecialcharsBack($arData['UF_EXALEAD_PARAM'])
            );
        }

        $urlQuery = [];
        parse_str(
            parse_url(
                urldecode(
                    htmlspecialchars_decode($arData['UF_URL'])
                ),
                PHP_URL_QUERY
            ),
            $urlQuery
        );
        $urlQuery = array_merge(
            [
                'q'                => true,
                'f_publishyear'    => 0,
                'publishyear_prev' => true,
                'publishyear_next' => true,
                'newcollection'    => true,
                'logic'            => true,
                'theme'            => true,
                'text'             => true,
                'is_full_search'   => true,
                's_strict'         => true,
                'librarytype'      => true,
                'by'               => true,
                'order'            => true,
                'notransform'      => true,
                'f_type'           => true,
                's_tc'             => true,
                'slparam'          => true,
                'f_field'          => true,
                'f_ft'             => true,
                's_in_results'     => true,
                'dop_filter'       => true,
            ], $urlQuery
        );

        $arData['SEARCH_PARAMS'] = [];
        $logicParamNames = [
            'ALL'          => 'по всем полям',
            'authorbook'   => 'по автору',
            'title'        => 'по названию',
            'publisher'    => 'по издательству',
            'publishplace' => 'по месту издания',
        ];
        $paramsNames = [
            'filters'        => 'Фильтры',
            'years'          => 'Год публикации',
            'collection_new' => 'Коллекции',
            'terms'          => 'Термины',
            'publishplace'   => 'Место издания',
            'publisher'      => 'Издательство',
            'authorbook'     => 'Авторы',
            'yearrange'      => 'Дата',
            'f_publishyear'  => 'Год публикации',
        ];
        $searchParams = [
            'filters'        => [],
            'years'          => [],
            'collection_new' => [],
            'terms'          => [],
            'publishplace'   => [],
        ];

        switch (empty($urlQuery['librarytype'])
            ? 'library'
            : $urlQuery['librarytype']) {
            case 'library':
                if ('on' === $urlQuery['s_tc']) {
                    $searchParams['filters'][] = 'в каталогах печатных изданий';
                }
                $searchParams['filters'][] = 'в библиотеках';
                $paramsNames['library'] = 'Библиотеки';
                break;
            case 'archive':
                if ('on' === $urlQuery['s_tc']) {
                    $searchParams['filters'][] = 'в каталогах';
                }
                $searchParams['filters'][] = 'в архивах';
                $paramsNames['library'] = 'Архивы';
                break;
            case 'museum':
                if ('on' === $urlQuery['s_tc']) {
                    $searchParams['filters'][] = 'в каталогах';
                }
                $searchParams['filters'][] = 'в музеях';
                $paramsNames['library'] = 'Музеи';
                break;
        }
        $logics = [];


        if ('on' === $urlQuery['s_strict']) {
            $searchParams['filters'][] = 'по точной фразе';
        }
        if ('on' === $urlQuery['is_full_search']) {
            $searchParams['filters'][] = 'по электронным копиям';
        }
        if ('on' === $urlQuery['s_in_results']) {
            $searchParams['filters'][] = 'искать в найденном';
        }
        foreach (
            [
                'collection_new',
                'terms',
                'publishplace',
                'publisher',
                'authorbook',
                'yearrange',
                'library',
            ] as $fFieldParam
        ) {
            if (isset($urlQuery['f_field'][$fFieldParam])) {
                foreach ($urlQuery['f_field'][$fFieldParam] as $collection) {
                    $collection = substr(
                        $collection,
                        strlen('f/' . $fFieldParam . '/')
                    );
                    $searchParams[$fFieldParam][] = $collection;
                }
            }
        }
        if (is_array($urlQuery['newcollection'])) {
            foreach ($urlQuery['newcollection'] as $collection) {
                if (false === array_search(
                        $collection,
                        $searchParams['collection_new']
                    )
                ) {
                    $searchParams['collection_new'][] = $collection;
                }
            }
        }


        if (empty($urlQuery['f_publishyear'])) {
            $searchParams['years']
                = $urlQuery['publishyear_prev']
                . ' - '
                . $urlQuery['publishyear_next'];
        } else {
            $searchParams['f_publishyear'] = $urlQuery['f_publishyear'];
        }

        foreach ($searchParams as $paramsName => $params) {
            if (empty($params) or !isset($paramsNames[$paramsName])) {
                continue;
            }

            $arData['SEARCH_PARAMS'][$paramsNames[$paramsName]]
                = is_array($params) ? implode(', ', $params) : $params;
        }
        if (is_array($urlQuery['logic'])) {
            foreach ($urlQuery['logic'] as $key => $logic) {
                if (!isset($urlQuery['theme'][$key])) {
                    continue;
                }
                if (!isset($urlQuery['text'][$key])) {
                    continue;
                }
                $logicValue = $urlQuery['text'][$key];
                $logicField = $urlQuery['theme'][$key];
                if (empty($logicValue)) {
                    continue;
                }
                if (!isset($logicParamNames[$logicField])) {
                    continue;
                }
                $logicKey = (
                'AND' === $logic
                    ? 'и'
                    : ('OR' === $logic ? 'или'
                    : ('NOT' === $logic ? 'не' : null)
                ));
                if (null === $logicKey) {
                    continue;
                }
                $logicKey .= ' - ' . $logicParamNames[$logicField];
                $arData['SEARCH_PARAMS'][$logicKey] = $logicValue;
            }
        }


        $arResult['ITEMS'][] = $arData;
    }

} catch (\Neb\Main\Exception\ValidateException $e) {
    $arResult['ERRORS'][] = $e->getMessage();
}
$this->IncludeComponentTemplate();
$APPLICATION->SetTitle(Loc::getMessage('PAGE_TITLE'));

$APPLICATION->SetPageProperty(
    "ALERT_REMOVE_MESSAGE", "Удалить запрос из Моих поисковых запросов?"
);