$(function(){
    var btnShowDescr = $('.b-digital_desc'),
        clsAct = 'row-active',
        speedToggle = 300;
    btnShowDescr.on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            table = $this.closest('table'),
            rowNext = $this.closest('tr').next();
        if(rowNext.hasClass(clsAct)) {
            rowNext.toggle(speedToggle, function(){
                rowNext.removeClass(clsAct);
            })
        } else {
            if(table.find('.' + clsAct).length > 0) {
                table.find('.' + clsAct).toggle(speedToggle, function(){
                    table.find('.' + clsAct).removeClass(clsAct);
                    rowNext.toggle(speedToggle).addClass(clsAct);
                });
            } else {
                rowNext.toggle(speedToggle).addClass(clsAct);
            }
        }
    });

    $(document).on( "click", ".plusico_wrap .plus_ico",function(e) {
        var lnk = $(this),
            par = lnk.closest('.plusico_wrap'),
            hint = par.find('.b-hint'),
            html_remove,
            html_add;

        if (lnk.hasClass('plus_digital') ) {
            e.preventDefault();
            html_remove = hint.data('minus'),
                html_add = hint.data('plus');
            par.toggleClass('minus');
            if ( par.hasClass('minus')) {
                hint.html(html_remove);
            } else {
                hint.html(html_add);
            }
        }

        if (lnk.hasClass('lib') ) {
            html_remove = hint.data('minus'),
                html_add = hint.data('plus');
            /*удаление*/
        }
    });

    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $.ajax({
            type: 'GET',
            url: $(this).attr('href') + '&PLAN_DIGITIZATION_PAGER=Y',
            dataType: 'html',
            cache: false
        })
        .done(function( data ){
            $('.b-plan-add_digital').html(data);
        });
    });
});