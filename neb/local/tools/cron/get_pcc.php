<?
/*
ПСС
http://213.208.168.5:7880/paidService/PaidScanService.asmx
*/
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

$_SERVER["DOCUMENT_ROOT"] = str_replace('/local/tools/cron', '', __DIR__);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

ini_set('soap.wsdl_cache_enabled', 0);

$wsdlurl = 'http://213.208.168.5:80/paidService/PaidScanService.asmx?WSDL';
// $wsdlurl = 'http://195.74.82.189:30012/paidService/PaidScanService.asmx?WSDL';

$client = new SoapClient(
    $wsdlurl,
    array(
        "trace" => 1,
        "exceptions" => 1,
        'encoding' => 'UTF-8',
        'proxy_host' => '213.208.168.5',
        'proxy_port' => '80',
        // 'proxy_host'	=> '195.74.82.189',
        // 'proxy_port'	=> '30012',
    )
);


$res = $client->GetPagePrice();
$pagePrice = $res->GetPagePriceResult;

/*
Получаем список заказов
*/
$stmp = AddToTimeStamp(array('HH' => -4));
$params = array('startDate' => date("c", $stmp), 'finishDate' => date("c"));
#$params = array('startDate' => date("Y-12-04\T12:10:00+03:00", $stmp), 'finishDate' => date("c")); # отладка
#pre($params,1);
$something = $client->GetOrderList($params);
if (empty($something->GetOrderListResult->OrderInfo))
    die('empty order list');

#pre($something->GetOrderListResult->OrderInfo,1); exit();

CModule::IncludeModule('iblock');

//var_dump($something->GetOrderListResult->OrderInfo);
$OrderInfo = $something->GetOrderListResult->OrderInfo;
if (is_object($OrderInfo))
    $OrderInfo = (array)$OrderInfo;

$arOrders = array();
if (isset($OrderInfo['OrderId']))
    $OrderInfo = array($OrderInfo);

foreach ($OrderInfo as $order_) {
    $order = (object)$order_;

    $arSelect = Array("ID", "IBLOCK_ID");
    $arFilter = Array("IBLOCK_ID" => IBLOCK_PCC_ORDER, "ACTIVE" => "Y", "PROPERTY_ORDERID" => (string)$order->OrderId);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount" => 1), $arSelect);
    if ($ob = $res->GetNextElement())
        continue;

    #if(!empty($arOrders)) continue; # грузим по одному заказу

    $arOrder = array(
        'OrderId' => (string)$order->OrderId,
        'UserRFID' => (string)$order->UserRFID,
        'StartDate' => ConvertTimeStamp(strtotime($order->StartDate), 'FULL'),
        'FinishDate' => ConvertTimeStamp(strtotime($order->FinishDate), 'FULL'),
        'ImageCount' => (int)$order->ImageCount,
        'Price' => ((int)$order->ImageCount * $pagePrice),
    );

    /*
    Получаем список файлов в заказе
    */
    $resFileList = $client->GetFileListByOrderId(array('orderId' => $arOrder['OrderId']));
    $arFile = array();
    if (!empty($resFileList->GetFileListByOrderIdResult))
        $arFile = explode(',', $resFileList->GetFileListByOrderIdResult);

    if (empty($arFile))
        continue;

    foreach ($arFile as $file) {
        /*
        Получаем содержимое файла
        */
        $resFile = $client->GetFile(array('orderId' => $arOrder['OrderId'], 'fileName' => $file));
        if (!empty($resFile->GetFileResult)) {
            $fileName = "/upload/pcc/" . md5($arOrder['OrderId']) . "/" . md5($file) . '.' . pathinfo($file, PATHINFO_EXTENSION);
            RewriteFile($_SERVER["DOCUMENT_ROOT"] . $fileName, $resFile->GetFileResult);
            $arOrder['FILES'][] = array('VALUE' => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $fileName), 'DESCRIPTION' => $file);
        }
    }

    if (empty($arOrder['FILES']))
        continue;

    $arOrders[] = $arOrder;
}

if (empty($arOrders))
    die('empty orders');

$el = new CIBlockElement;

CModule::IncludeModule('catalog');
/*
Добавляем заказы в ИБ
*/
global $CACHE_MANAGER;

foreach ($arOrders as $order) {
    $PROP = array();
    $PROP['ORDERID'] = $order['OrderId'];
    $PROP['USERRFID'] = $order['UserRFID'];
    $PROP['STARTDATE'] = $order['StartDate'];
    $PROP['FINISHDATE'] = $order['FinishDate'];
    $PROP['IMAGECOUNT'] = $order['ImageCount'];
    $PROP['PRICE'] = $order['Price'];
    $PROP['FILES'] = $order['FILES'];

    $arLoadProductArray = Array(
        "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
        "IBLOCK_ID" => IBLOCK_PCC_ORDER,
        "PROPERTY_VALUES" => $PROP,
        "NAME" => $order['OrderId'],
        "ACTIVE" => "Y",            // активен
    );

    if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
        echo "New order ID: " . $PRODUCT_ID . "\n<br />";

        /*
        Добавляем цену в торговый кталог
        */
        $PRICE_TYPE_ID = 1;
        $CURRENCY = 'RUB';

        $arFields = array(
            "ID" => $PRODUCT_ID,
            "QUANTITY" => 1,
            "PURCHASING_PRICE" => $order['Price'],
            "PURCHASING_CURRENCY" => $CURRENCY,

        );
        if (CCatalogProduct::Add($arFields)) {
            $arFields = array(
                "PRODUCT_ID" => $PRODUCT_ID,
                "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                "PRICE" => $order['Price'],
                "CURRENCY" => $CURRENCY,
            );

            $obPrice = new CPrice();
            $obPrice->Add($arFields);
        }
    } else
        echo "Error: " . $el->LAST_ERROR . "\n<br />";;

    $CACHE_MANAGER->ClearByTag("iblock_id_" . IBLOCK_PCC_ORDER);

    /*
    Удаляем файлы
    */
    DeleteDirFilesEx("/upload/pcc/" . md5($order['OrderId']));
}