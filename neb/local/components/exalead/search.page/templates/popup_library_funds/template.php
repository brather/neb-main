<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<form action="/profile/funds/manage/" target="_top" method="post" class="b-form b-form_common b-addbookform">
	<input type="hidden" name="action" value="addBook">
	<?=bitrix_sessid_post()?>
	<div class="fieldrow">
		<table class="b-result-doc">
            <?$i = 0;?>
			<?foreach($arResult["ITEMS"] as $item):?>
			<tr class="b-result-docitem">

				
				<td>
					<h2><span class="num"><?=$item['NUM_ITEM']?>.</span><a href="<?=$item['DETAIL_PAGE_URL']?>" target="_blank"><?=$item['title']?></a></h2>
					<ul class="b-resultbook-info">
						<?if(!empty($item['authorbook'])):?><li><span><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_AUTHOR');?>:</span> <?=$item['authorbook']?></li><?endif;?>
						<?if(!empty($item['publishyear'])):?><li><span><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_YEAR');?>:</span> <?=$item['publishyear']?></li><?endif;?>
						<?if(!empty($item['countpages'])):?><li><span><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_PAGES');?>:</span> <?=$item['countpages']?></li><?endif;?>
						<?if(!empty($item['publisher'])):?><li><span><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_PUBLISHER');?>:</span> <?=$item['publisher']?></li><?endif;?>
						<?if(!empty($item['library'])):?><li><span>	<em><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_SOURCE');?>:</em> <?=$item['library']?></li><?endif;?>
					</ul>
					<?$APPLICATION->IncludeComponent(
						'notaext:plupload',
						'add_books',
						array(
							'MAX_FILE_SIZE' => '512',
							'FILE_TYPES' => 'pdf',
							'DIR' => 'tmp_books_pdf',
							'FILES_FIELD_NAME' => 'books_pdf',
							'MULTI_SELECTION' => 'N',
							'CLEANUP_DIR' => 'Y',
							'UPLOAD_AUTO_START' => 'Y',
							'RESIZE_IMAGES' => 'N',
							'UNIQUE_NAMES' => 'Y',
							'INPUT_NAME' => 'FILE_PDF_'.$item['NUM_ITEM'],
						),
						false
					);?>
				</td>
				<td>
					<div class="b-radio">
						<input type="checkbox" name="BOOK_NUM_ID[]" value="<?=$item['NUM_ITEM']?>" class="checkbox" id="libraryFundsBook" <?if($i++ == 0):?>checked="checked" <?endif;?>>
						<input type="hidden" name="BOOK_ID_<?=$item['NUM_ITEM']?>" value="<?=$item['id']?>">
						<input type="hidden" name="BOOK_TITLE_<?=$item['NUM_ITEM']?>" value="<?=strip_tags($item['title'])?>">
						<input type="hidden" name="BOOK_AUTHOR_<?=$item['NUM_ITEM']?>" value="<?=strip_tags($item['authorbook'])?>">
						<input type="hidden" name="BOOK_YEAR_<?=$item['NUM_ITEM']?>" value="<?=$item['publishyear']?>">
					</div>
				</td>

			</tr>
			<?endforeach;?>
		</table><!-- /.b-result-doc -->
	</div>
	<?=$arResult['STR_NAV']?>
	<div class="fieldrow nowrap fieldrowaction">
		<div class="fieldcell ">
			<div class="field clearfix">
				<a href="#" class="formbutton gray right btrefuse"><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_CANCEL_PUBLICATION');?></a>
				<button class="formbutton left" value="1" type="submit"><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_POST_PUBLICATION');?></button>
			</div>
		</div>
	</div>
</form>
