<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\RslViewer;

Loader::includeModule('nota.exalead');

$bookId = trim($_REQUEST['book_id']);
$extLabels = array(
    'txt' => 'txt(автораспознавание)',
);

$_excludeExt = array('meta', 'col', 'txt', 'mrc', 'open', 'iso');

$obRslViewer = new RslViewer($bookId);
$arResources = $obRslViewer->getResources();

$result = array();
foreach ($arResources as $ext) {

    if (in_array($ext, $_excludeExt))
        continue;

    $item = array('ext' => $ext);
    if (isset($extLabels[$ext])) {
        $item['label'] = $extLabels[$ext];
    }
    $result[] = $item;
}

MainHelper::showJson($result);