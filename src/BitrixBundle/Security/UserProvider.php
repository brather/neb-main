<?php
/**
 * User: agolodkov
 * Date: 16.08.2016
 * Time: 16:50
 */

namespace BitrixBundle\Security;


use BitrixBundle\DependencyInjection\BitrixHelper;
use BitrixBundle\Exception\AuthenticationNotUnique;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class UserProvider
 *
 * @todo    Все что касается НЭБ перенести в AppBundle\Security\UserProvider
 * @package BitrixBundle\Security
 */
class UserProvider implements UserProviderInterface
{

    /**
     * @var BitrixHelper
     */
    protected $_bitrix;

    public function __construct(BitrixHelper $bitrix)
    {
        $this->_bitrix = $bitrix;
    }

    /**
     * @inheritdoc
     */
    public function loadUserByUsername($username)
    {
        return $this->_loadUserByFilter(['EMAIL' => $username]);
    }

    /**
     * @param string $token
     *
     * @return User
     * @throws \Exception
     */
    public function loadUserByToken($token)
    {
        return $this->_loadUserByFilter(['UF_TOKEN' => $token]);
    }

    /**
     * @return User
     * @throws \Exception
     */
    public function getCurrentUser()
    {
        $this->_bitrix->checkIncludeThrowException();
        if ($user = \nebUser::getCurrent()->getUser()) {
            return $this->_forgeBitrixUser(\nebUser::getCurrent()->getUser());
        }

        return $this->forgeUser();
    }

    /**
     * @param array $filter
     *
     * @return User
     * @throws \Exception
     */
    protected function _loadUserByFilter($filter)
    {
        $this->_bitrix->checkIncludeThrowException();
        $oUser = new \CUser();
        $user = $oUser->GetList(
            $by = [],
            $order = [],
            $filter
        );
        if ($user->SelectedRowsCount() > 1) {
            throw new AuthenticationNotUnique();
        }
        $user = $user->Fetch();
        if (!$user) {
            throw new AuthenticationCredentialsNotFoundException('User not found');
        }

        return $this->_forgeBitrixUser($user);
    }

    protected function _forgeBitrixUser(array $user)
    {
        $symfonyUser = $this->forgeUser();
        $symfonyUser->setUsername($user['LOGIN']);
        $symfonyUser->setEmail($user['EMAIL']);
        $symfonyUser->setUserRawData($user);

        return $symfonyUser;
    }

    /**
     * @inheritdoc
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @inheritdoc
     */
    public function supportsClass($class)
    {
        return $class === 'BitrixBundle\Security\User';
    }

    /**
     * @return User
     */
    public function forgeUser()
    {
        return new User();
    }

}