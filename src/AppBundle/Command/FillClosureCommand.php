<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 16:46
 */

namespace AppBundle\Command;


use Gedmo\Tree\Entity\Repository\ClosureTreeRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FillClosureCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('lbc:fill:closure');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);

        $this->getContainer()
            ->get('bitrix')
            ->requireProlog(
                $this->getContainer()
                    ->get('kernel')
                    ->getRootDir()
            );

        while (ob_get_level()) {
            ob_end_clean();
        }
        error_reporting(E_ERROR);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '2024M');

        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var ClosureTreeRepository[] $repositories */
        $repositories = [];
        $repositories[] = $em->getRepository('LbcBundle:LbcMaindivision');
        $repositories[] = $em->getRepository('LbcBundle:LbcLocationplans');
        $repositories[] = $em->getRepository('LbcBundle:LbcLocationplansections');

        try {
            foreach($repositories as $repository) {
                $repository->recover();
            }

        } catch (\Exception $e) {
            echo $e->__toString();
        }

        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}