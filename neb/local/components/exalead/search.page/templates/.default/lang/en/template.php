<?
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO'] = 'Found 0 documents';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND'] = 'Nothing found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SUGGEST'] = 'Maybe you are searching for';

$MESS['LIB_SEARCH_SIMPLE_QUERY'] = 'Search carried out for bibliographic information: ';
$MESS['LIB_SEARCH_DO_FULL_SEARCH'] = 'Search for text books';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'Documents';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'Document';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'Documents';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'] = 'found';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY'] = 'Save the search request';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED'] = 'Search request saved';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE'] = 'Add a title';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SPECIFY_SEARCH'] = 'Specify the search';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE'] = 'Search on the website';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE'] = 'Search only in scanned editions';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY'] = 'Libraries';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_EMPTY'] = 'Enter the search expression';
$MESS['LIB_SEARCH_PAGE_SETTINGS'] = 'Settings';
$MESS['LIB_SEARCH_PAGE_FOUND_MORE_300000_RESULT'] = 'Found more than 300 000 results';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_COPIES'] = 'copies of publications';
$MESS['LIB_SEARCH_PAGE_INCLUDING'] = "including";
$MESS['LIB_SEARCH_PAGE_DUPLICATED'] = "duplicates";
// ---------        
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_RELEVANCE'] = 'By relevance'; //По релевантности
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_AUTHOR'] = 'By author'; //По автору
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_NAME'] = 'By title'; // По названию
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_DATE'] = 'By date'; // По дате
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_SHOW'] = 'Show'; //Показать
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_AUTHORS'] = 'Authors'; //Авторы
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_DATE'] = 'Date'; //Дата
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_PUBLISH'] = 'Publisher'; //Издательство
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS'] = 'Library'; //Библиотеки
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS_library'] = 'Library'; //Библиотеки
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS_archive'] = 'Archives'; //Архивы
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS_museum'] = 'Museums'; //Музеи
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_PLACE'] = 'place of publication';//Место издания
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_COLLECTION'] = 'Collections'; //Коллекции
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_TERMS'] = 'Terms'; //Термины
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'] = 'more';//следующие