<?php

$settings = array (
    // If 'strict' is True, then the PHP Toolkit will reject unsigned
    // or unencrypted messages if it expects them signed or encrypted
    // Also will reject the messages if not strictly follow the SAML
    // standard: Destination, NameId, Conditions ... are validated too.
    'strict' => false,

    // Enable debug mode (to print errors)
    'debug' => true,

    'security' => array(
        'authnRequestsSigned' => true,
        'logoutResponseSigned' => true,
        'logoutRequestSigned' => true
    ),

    // Service Provider Data that we are deploying
    'sp' => array (
        // Identifier of the SP entity  (must be a URI)
        'entityId' => 'https://xn--90ax2c.xn--p1ai',
        // Specifies info about where and how the <AuthnResponse> message MUST be
        // returned to the requester, in this case our SP.
        'assertionConsumerService' => array (
            // URL Location where the <Response> from the IdP will be returned
            //'url' => 'https://demo.neb.elar.ru/local/tools/esia/psaml/demo1/index.php?acs1',
            'url' => 'https://xn--90ax2c.xn--p1ai/WebServices/Esia.php?act=login',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ),
        // Specifies info about where and how the <Logout Response> message MUST be
        // returned to the requester, in this case our SP.
        'singleLogoutService' => array (
            // URL Location where the <Response> from the IdP will be returned
            'url' => 'https://xn--90ax2c.xn--p1ai/WebServices/Esia.php?act=logout',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Specifies constraints on the name identifier to be used to
        // represent the requested subject.
        // Take a look on lib/Saml2/Constants.php to see the NameIdFormat supported
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',

        // Usually x509cert and privateKey of the SP are provided by files placed at
        // the certs folder. But we can also provide them with the following parameters
        //'x509cert' => '',
        //'privateKey' > '',
    ),

    // Identity Provider Data that we want connect with our SP
    'idp' => array (
        // Identifier of the IdP entity  (must be a URI)
        'entityId' => 'https://esia.gosuslugi.ru/idp/shibboleth',
        // SSO endpoint info of the IdP. (Authentication Request protocol)
        'singleSignOnService' => array (
            // URL Target of the IdP where the SP will send the Authentication Request Message
            'url' => 'https://esia.gosuslugi.ru/idp/profile/SAML2/Redirect/SSO',
            //'url' => 'https://esia-portal1.test.gosuslugi.ru/',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-POST binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ),
        // SLO endpoint info of the IdP.
        'singleLogoutService' => array (
            // URL Location of the IdP where the SP will send the SLO Request
            'url' => 'https://esia.gosuslugi.ru/idp/profile/SAML2/Redirect/SLO',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Public x509 certificate of the IdP
        'x509cert' => 'MIICnzCCAggCCQCfqN/IN2j+CDANBgkqhkiG9w0BAQUFADCBkzELMAkGA1UEBhMC
UlUxFzAVBgNVBAgMDkNlbnRyYWwgcmVnaW9uMQ8wDQYDVQQHDAZNb3Njb3cxDDAK
BgNVBAoMA1JTTDEQMA4GA1UECwwHbGlicmFyeTEeMBwGA1UEAwwVw5DCvcORwo3D
kMKxLsORwoDDkcKEMRowGAYJKoZIhvcNAQkBFgtwb3N0QHJzbC5ydTAeFw0xNTAy
MDkwOTAwMTdaFw0yNTAyMDYwOTAwMTdaMIGTMQswCQYDVQQGEwJSVTEXMBUGA1UE
CAwOQ2VudHJhbCByZWdpb24xDzANBgNVBAcMBk1vc2NvdzEMMAoGA1UECgwDUlNM
MRAwDgYDVQQLDAdsaWJyYXJ5MR4wHAYDVQQDDBXDkMK9w5HCjcOQwrEuw5HCgMOR
woQxGjAYBgkqhkiG9w0BCQEWC3Bvc3RAcnNsLnJ1MIGfMA0GCSqGSIb3DQEBAQUA
A4GNADCBiQKBgQDXgOpUHAj2v0DsARBIbqchpjSd/3t8qf1i9YO0aq9RA6yOOEOH
CrP9jYSnujmvkkf/5rYAow4eBhbACUSbfqh9ToE5O0DJEi/89R2k3saiO1sQNnzC
C3tKCCEKnfX/9r0zaeHDXy/2Er9rsxpR4me4ZDpzUgg2KdiJBZr4LGgKEQIDAQAB
MA0GCSqGSIb3DQEBBQUAA4GBAGdbib9Bti6vnH5w1XSZdHBqhPtXxj+zzd/LfZzc
zxeg2fyV6CuooM3N/m7+vs8U5P0fdOziOOro/i8d37nsLcOOA9D/tEqXGTDtYaCh
CVZwNXQ8e0Ldz1E8U+TFhudSY8TpwJORjD2K5F+5KNVC2DJGajlyiDkBSXilLGjh
JVXS',
        /*
         *  Instead of use the whole x509cert you can use a fingerprint
         *  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it)
         */
        // 'certFingerprint' => '',
    ),
);
