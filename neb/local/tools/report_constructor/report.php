<?
/**
 * Непосредственно отчет
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
global $APPLICATION;

if (!TblEntities::checkAccess()) {
    die('Permissions denied!');
}

$arReport = TblReport::getReportById($_REQUEST['id']);
$data     = TblReport::getData($_REQUEST['id'], $_REQUEST['offset']);

if ($_REQUEST['offset'] == 'all') {

    \Neb\Main\Helper\MainHelper::clearBuffer();
    @ini_set("memory_limit", "512M");

    require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel/IOFactory.php");

    $objPHPExcel = new \PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getActiveSheet()->setTitle(TruncateText($arReport['Name'], 25));

    $objPHPExcel->getActiveSheet()->setCellValue('A2', $arReport['Name']);
    $objPHPExcel->getActiveSheet()->setCellValue('A3', date('d.m.Y'));
    // название столбцов
    $row = 5;
    foreach ($data['fields'] as $key=>$fields) {
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key, $row, $fields);
    }
    // данные таблицы
    if (count($data['data'])> 0) {
        foreach ($data['data'] as $fields) {
            if (count($fields) > 0) {
                $row++;
                $cell = 0;
                foreach ($fields as $field) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell, $row, $field);
                    $cell++;
                }
            }
        }
    }
    $row = $row + 3;
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $data['sql']);

    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header ('Content-Disposition: attachment; filename="'.$arReport['Name'].'.xlsx"');
    header ('Cache-Control: max-age=0');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: public, must-revalidate');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

    $objWriter->save('php://output');
    \Neb\Main\Helper\MainHelper::clearBuffer(true);
    exit;
}
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/css/style.css" />
    <style>
        body {
            margin-top: 30px;
            padding-left: 40px;       }
        table td {
            padding: 10px;
        }
        .button{
            background: #0086a6;
            border-color: #0086a6;
            color: #fff;
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            border: 1px solid transparent;
            border-radius: 4px;
        }
    </style>
    <title><?=$arReport['Name']?></title>
</head>
<body>

<h3><?=$arReport['Name']?></h3>

<p><?=date('d.m.Y');?></p>

<table border=1 cellpadding="10">
    <tr>
        <?foreach ($data['fields'] as $fields):?>
            <td><b><?=$fields;?></b></td>
        <?endforeach;?>
    </tr>

    <?if (count($data['data'])> 0):?>
        <?foreach ($data['data'] as $fields):?>
            <tr>
                <?if (count($fields)> 0):?>
                    <?foreach ($fields as $field):?>
                        <td><?=$field;?></td>
                    <?endforeach;?>
                <?endif;?>
            </tr>
        <?endforeach;?>
    <?endif;?>
</table>

<?if ($_REQUEST['offset'] - 200 >= 0):?>
    <a href='<?=$APPLICATION->GetCurPageParam("offset=".($_REQUEST['offset']-200), array("offset"));?>'>Предыдущие 200</a> /
<?endif;?>
<?if (/*$data['count']*/ count($data['data']) >= 200): // по просьбе Леши ?>
    <a href='<?=$APPLICATION->GetCurPageParam("offset=".($_REQUEST['offset']+200), array("offset"));?>'>Следующие 200</a>
<?endif;?>
<br>
<br>
Запрос: <?=$data['sql']?>
<br><br>
<a href='<?=$APPLICATION->GetCurPageParam("offset=all", array("offset"));?>'><input type='button' class='button' value='Экспорт в формат Excel'></a>
</body>
</html>