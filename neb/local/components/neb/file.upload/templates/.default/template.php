<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
?>
<div class="container-fluid">
    <?if(empty($arResult['ERROR'])){?>
        <?
        $styleForEmptyRow = ' style="display: none;"';
        $idForm = 'pdf-upload';
        $uploadBlock = 'upload-files';
        $progressbar = 'progressbar';
        $postfix = '';
        if(!empty($arParams['SUB_FOLDER'])) {
            $postfix .= '-' . str_replace(' ', '', trim($arParams['SUB_FOLDER']));
        }
        if(!empty($arParams['FILE_NAME'])){
            $postfix .= '-' . str_replace(' ', '', trim($arParams['FILE_NAME']));
        }
        $idForm .= checkStringForSetAttr($postfix);
        $uploadBlock .= checkStringForSetAttr($postfix);
        $progressbar .= checkStringForSetAttr($postfix);?>
        <?if(count($arParams['ADD_PARAMS_FOR_REQUEST']) > 0){
            $addParams = '';
            foreach($arParams['ADD_PARAMS_FOR_REQUEST'] as $codeData => $vData) {
                $addParams .= ' data-add-param-' . $codeData . '="' . $vData . '"';
            }
        }?>
        <form action="<?= $arParams['SEF_FOLDER'] ?>"
              id="<?= $idForm ?>"
              class="form-file-pdf"
              method="POST"
              enctype="multipart/form-data">
            <div class="form-group">
                <?if($arResult['FULL_DIRECTORY']){
                    $clsDisabled = ' load-disabled';
                }?>
                <div class="row<?= $clsDisabled ?>">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-default btn-file">
                            <span><i class="fa fa-upload"></i>&nbsp;<?= Loc::getMessage('FILE_UPLOAD_T_TITLE_SHORT') ?></span>
                            <input type="file"
                                   class="file-upload-input"
                                   name="file-upload-input"
                                   <?= $addParams ?>
                                   data-ie-confirm-title="<?= Loc::getMessage('FILE_UPLOAD_T_CONFIRM_TITLE') ?>"
                                   data-ie-confirm-text="<?= Loc::getMessage('FILE_UPLOAD_T_CONFIRM_TEXT') ?>"
                                   data-ie-confirm-yes="<?= Loc::getMessage('FILE_UPLOAD_T_CONFIRM_YES') ?>"
                                   data-url="<?= $arParams['AJAX_ADD_FILES'] ?>"
                                   data-folder="<?= $arParams['FOLDER_FOR_FILES'] ?>"
                                   data-sub-folder="<?= $arParams['SUB_FOLDER'] ?>"
                                   data-alias="<?= $arParams['FILE_ALIAS'] ?>"
                                   data-url-download="<?= $arParams['URL_DOWNLOAD'] ?>"
                                   data-file-name="<?= checkStringForSetAttr($arParams['FILE_NAME']) ?>"
                                   data-file-maxsize="<?= $arParams['MAX_FILE_SIZE'] ?>"
                                   data-max-files="<?= $arParams['COUNT_FILES_IN_DIRECTORY'] ?>"
                                   data-file-type="<?= $arParams['TYPE_FILE'] ?>"
                                   data-ajax-delete="<?= $arParams['AJAX_DEL_FILES'] ?>"/>
                        </span>
                        <div class="progress progress-striped active" id="<?= $progressbar ?>" style="display: none;">
                            <div class="progress-bar progress-bar-info"
                                 role="progressbar"
                                 aria-valuenow="0"
                                 aria-valuemin="0"
                                 aria-valuemax="100"
                                 style="width: 0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row log-<?= $arParams['SUB_FOLDER_NAME'] ?>"></div>
        <div class="upload-files"
             id="<?= checkStringForSetAttr($uploadBlock) ?>"
             data-max-files="<?= $arParams['COUNT_FILES_IN_DIRECTORY'] ?>"
             data-mess-empty="<?= $arParams['EMPTY_MESS'] ?>">
            <table class="table" role="presentation">
                <tbody class="files">
                    <?if(!empty($arResult['FILES'])) {?>
                        <?foreach($arResult['FILES'] as $iFile => $arFile){?>
                            <tr class="template-download fade in">
                                <td>
                                    <p class="name">
                                        <?if(!empty($arFile['ALIAS'])){
                                            $titleFile = $arFile['ALIAS'];
                                        } else {
                                            $titleFile = $arFile['NAME_SHORT'];
                                        }?>
                                        <?if($arParams['URL_DOWNLOAD']){?>
                                            <a class="file-fa"
                                               href="<?= $arFile['URL_DOWNLOAD'] ?>"
                                               title="<?= $arFile['NAME_FULL'] ?>"
                                               download="<?= $arFile['NAME_FULL'] ?>">
                                                <?= $titleFile ?>
                                            </a>
                                        <?} else {?>
                                            <span class="file-fa"><?= $titleFile ?></span>
                                        <?}?>
                                    </p>
                                </td>
                                <td>
                                    <span class="size"><?= $arFile['FILE_SIZE'] ?></span>
                                </td>
                                <td>
                                    <a class="btn btn-default js-download-book"
                                       target="_blank"
                                       href="/local/tools/plan_digitization/download.book.php?path=<?echo
                                            $arResult['UPLOAD_DIR']['PATH_RELATIVE'] ?>/<?= $arFile['NAME_FULL'] ?>">
                                        <i class="fa fa-download"></i> <span>Скачать файл</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="#"
                                       data-delete="<?= $arResult['UPLOAD_DIR']['PATH_RELATIVE'] ?>/<?= $arFile['NAME_FULL'] ?>"
                                       data-form="#<?= $idForm ?>"
                                       data-container="#<?= checkStringForSetAttr($uploadBlock) ?>"
                                       data-lang-confirm-title="<?= Loc::getMessage('FILE_UPLOAD_T_CONFIRM_TITLE') ?>"
                                       data-lang-confirm-text="<?= Loc::getMessage('FILE_UPLOAD_T_CONFIRM_TEXT') ?>"
                                       data-lang-confirm-yes="<?= Loc::getMessage('FILE_UPLOAD_T_CONFIRM_YES') ?>"
                                       data-ajax-delete="<?= $arParams['AJAX_DEL_FILES'] ?>"
                                        <?= $addParams ?>
                                       class="btn btn-danger btn-delete-file">
                                        <i class="glyphicon glyphicon-trash"></i><span><?= Loc::getMessage('FILE_UPLOAD_T_DELETE_FILE') ?></span>
                                    </a>
                                </td>
                            </tr>
                        <?}?>
                    <?} else {
                        $styleForEmptyRow = '';
                    }?>
                    <tr class="template-download fade in empty-files"<?= $styleForEmptyRow ?>>
                        <td class="text-right" colspan="3"><small><?= $arParams['EMPTY_MESS'] ?></small></td>
                    </tr>
                </tbody>
            </table>
        </div>
    <?} else {?>
        <p class="text-center text-danger"><?= $arResult['ERROR'] ?></p>
    <?}?>
</div>
<?= $arResult['SCRIPT'] ?>