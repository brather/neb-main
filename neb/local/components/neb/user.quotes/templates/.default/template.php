<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="b-filter_wrapper">
	<span class="sort_wrap">
		<a <?=SortingExalead("UF_BOOK_AUTHOR")?>><?=GetMessage("QUOTES_SORT_AUTHOR");?></a>
		<a <?=SortingExalead("UF_BOOK_NAME")?>><?=GetMessage("QUOTES_SORT_NAME");?></a>
		<a <?=SortingExalead("UF_DATE_ADD")?>><?=GetMessage("QUOTES_SORT_DATE");?></a>
	</span>	
</div>
<ul class="b-filter_list_wrapper search-result__content-list clearfix">
	<?
		if(!empty($arResult['ITEMS']))
		{
                    $i=0;
			foreach($arResult['ITEMS'] as $key => $arItem)
			{
				$ExaleadBook = $arResult['EXALEAD_BOOKS'][$arItem['UF_BOOK_ID']];
                                ++$i;
			?>
			<li class="search-result__content-list-kind clearfix" id="_<?=$arItem['ID']?>" data-page-item="<?=urlencode($arItem['ID'])?>">

				<button class="btn btn-default button-remove" data-page-item-delete title="Удалить цитату" data-modal-title="Удалить цитату?" data-button-title="Удалить">&nbsp;</button>

				<div class="search-result__content-list-sidebar">
                    <span class="search-result__content-list-number"><?php echo $i+(($arResult['iNumPage']-1)*$arParams['ITEM_COUNT']); ?>.</span>
                    <span class="search-result__content-list-status-icon search-result__content-list-status-icon--open"></span>
                </div>

                <div class="search-result__content-main">

				<?if(empty($ExaleadBook)) {?>
				Книга не найдена (ID: <?=$arItem['UF_BOOK_ID']?>)
				<?} else {?>
					<div class="search-result__content-main-links">
						<a href="<?=$ExaleadBook['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay search-result__content-link-title" data-width="955"><?=$arItem['UF_BOOK_NAME']?></a>
						<p class="search-result__content-link-info">
							<a href="<?=(SITE_TEMPLATE_ID=="special")?"/special":"";?>/search/?f_field[authorbook]=f/authorbook/<?=urlencode(mb_strtolower(strip_tags(trim($ExaleadBook['authorbook']))))?>"><?=$arItem['UF_BOOK_AUTHOR']?></a>
						</p>
					</div>
					
                    <a
                        target="_blank"
                        class="btn btn-primary search-result__btn-quotes"
                        href="javascript:void(0);"
                        onclick="readBook(event, this); return false;"
                        data-link="<?= $arItem['UF_BOOK_ID'] ?>"
                        data-options="<?php echo htmlspecialchars(
                            json_encode(
                                array(
                                    'page' => trim(
                                        $arItem['UF_PAGE']
                                    ),
                                )
                            ),
                            ENT_QUOTES,
                            'UTF-8'
                        )?>"><?= GetMessage("QUOTES_TEXT"); ?> <?= $arItem['UF_PAGE']; ?></a>

					<div class="search-result__cite-image">
						<img src="<?=$arItem['UF_IMG_DATA']?>" alt="">						
					</div>

					<div class="search-result__my-bar" data-list-widget>
						<a 
							href="#" 
							class="search-result__my-favorites" 
							data-list-toggler
							data-callback="reloadRightMenuBlock()" 
							data-list-src="<?=ADD_COLLECTION_URL?>list.php?t=quotes&id=<?=$arItem['ID']?>"
							data-edition-id="<?=$arItem['ID']?>"
						>
							<?=GetMessage("QUOTES_MY_COLLECTIONS");?>
						</a>

                		<div class="my-favorites-content" data-list-wrapper>
            				...
                		</div>
					</div>
				<?}?>
				</div>
			</li><!-- /.b-quote_item rel -->
			<?
			}
		}
	?>
</ul><!-- /.b-quote_list -->
<?=$arResult['NAV_STRING']?>