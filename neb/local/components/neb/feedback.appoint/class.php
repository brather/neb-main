<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 12.09.2016
 * Time: 11:00
 */

CBitrixComponent::includeComponentClass('neb:feedback');

use \Bitrix\Main\Loader;
use \Bitrix\Main\GroupTable;
use \Bitrix\Iblock\ElementTable;
use Nota\Exalead\SearchClient;
use Nota\Exalead\SearchQuery;

/**
 * Class FeedbackAppointComponent
 */
class FeedbackAppointComponent extends FeedbackComponent
{
    private $arRequestForm = [];

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->includeModules(['iblock', 'form', 'nota.exalead']);
        $this->getRequest();
        $this->getUser();
        $this->getTicket();
        $this->getForm();
        $this->getLibs();
        $this->getLibUsers();
        $this->getRequestForm();
        $this->setActions();

        $this->includeComponentTemplate();
    }

    /**
     * Получение данных формы
     */
    private function getRequestForm() {

        $arResult = [];

        if ($this->arRequest['FEEDBACK']['ACTION'] == 'APPOINT')
            foreach ($this->arRequest['FEEDBACK'] as $sCode => $sVal)
                $arResult[$sCode] = $sVal;

        $this->arRequestForm = $arResult;
    }

    /**
     * Получение библиотек-участниц НЭБ
     *
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException]
     */
    private function getLibs() {

        $arResult = [];

        if ($this->arTicket['ID'] < 1)
            return $arResult;

        $rsElements = ElementTable::getList([
            'filter' => ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ACTIVE' => 'Y'],
            'order'  => ['NAME' => 'ASC'],
            'select' => ['ID', 'NAME']
        ]);
        while ($arFields = $rsElements->fetch())
            $arResult[$arFields['ID']] = $arFields;

        $this->arResult['LIBS'] = $arResult;
    }

    /**
     * Возвращает массив групп пользователей
     *
     * @param array $arGroups
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    private function getGroups($arGroups = []) {

        $arResult = [];

        if (!empty($arGroups))
            $arFilter = ['ACTIVE' => 'Y', 'STRING_ID' => $arGroups];
        else
            $arFilter = ['ACTIVE' => 'Y', '!STRING_ID' => false];

        $rsUserGroups = GroupTable::getList(['filter' => $arFilter, 'select' => ['ID', 'STRING_ID']]);
        while ($arFields = $rsUserGroups->fetch())
            $arResult[$arFields['STRING_ID']] = $arFields['ID'];

        return $arResult;
    }

    /**
     * Получение всех пользователей библиотеки
     *
     * @return array
     */
    private function getLibUsers() {

        $arResult = [];

        $iLibId = intval($this->arRequest['lib']);
        if ($this->arTicket['ID'] < 1 || empty($this->arResult['LIBS'][$iLibId]))
            return $arResult;

        // получение групп пользователей: "Библиотека - Администратор" и "Оператор НЭБ - Библиотеки"
        $arGrUsers = ['library_admin', 'operator_lib'];
        $arGroups = $this->getGroups($arGrUsers);
        if (empty($arGroups))
            $this->arResult['ERROR'][] = 'Не найдено ни одной из групп пользоваталей: ' . implode(', ', $arGrUsers);

        if (empty($this->arResult['ERROR'])) {
            $rsUsers = CUser::GetList(
                ($by = 'id'),
                ($order = 'desc'),
                ['ACTIVE' => 'Y', 'UF_LIBRARY' => $iLibId, 'GROUPS_ID' => array_values($arGroups)],
                [
                    'FIELDS' => ['ID', 'EMAIL', 'NAME', 'SECOND_NAME', 'LAST_NAME', 'WORK_PHONE'],
                    'SELECT' => ['UF_LIBRARY']
                ]
            );
            while ($arFields = $rsUsers->fetch())
                $arResult[$arFields['ID']] = $arFields;
        }

        return $this->arResult['USERS'] = $arResult;
    }

    /**
     * Перевод заявки в статус "В работе"
     *
     * @throws Exception
     * @throws \Bitrix\Main\SystemException
     */
    private function setActions() {

        $arMail = [];

        $iLib     = intval($this->arRequest['lib']);
        $bAppoint = $this->arRequest['FEEDBACK']['ACTION'] == 'APPOINT';

        if ($this->arTicket['ID'] < 1 || $iLib < 1 || !$bAppoint || !empty($this->arError))
            return;

        if (empty($this->arResult['LIBS'][$iLib]))
            $this->arResult['ERROR'][] = 'Библиотека ID:' . $iLib . 'не найдена!';

        // установка идентификатора библиотеки
        if (empty($this->arResult['ERROR']) && !$this->setTicketField('FB_LIBRARY_ID', $iLib))
            $this->arResult['ERROR'][] = 'Не удалось установить библиотеку у заявки!';

        // установка статуса для заявки
        if (empty($this->arResult['ERROR']) && !$this->setTicketStatus('PROCESSED'))
            $this->arResult['ERROR'][] = 'Не удалось установить статус "В работе" для данной заявки!';

        // добавление записи в лог
        if (empty($this->arResult['ERROR']) && !$this->setHistoryItem('В работе', $this->arUser['ID']))
            $this->arResult['ERROR'][] = 'Не удалось записать лог-историю заявки!';


        // отправка сообщения на почту выбранным или всем пользователям библиотеки
        if (empty($this->arResult['ERROR'])) {

            $arUsers  = !empty($this->arRequest['FEEDBACK']['USER'])
                            ? $this->arRequest['FEEDBACK']['USER'] : array_keys($this->arResult['USERS']);

            foreach ($arUsers as $iUser) {
                if (!empty($this->arResult['USERS'][$iUser])) {

                    $this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']
                        = trim($this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']);

                    $arMail = [
                        "NUMBER" => $this->arTicket['ID'],
                        "EMAIL"  => $this->arResult['USERS'][$iUser]['EMAIL'],
                        "NAME"   => $this->arResult['USERS'][$iUser]['NAME'],
                        "THEME"  => $this->arTicket['FIELDS']['FB_THEME']['ANSWER_TEXT'],
                        "TEXT"   => $this->arTicket['FIELDS']['FB_TEXT']['USER_TEXT'],
                        "DATE_CREATE" => $this->arTicket['DATE_CREATE'],
                        "OPERATOR_NAME" => $this->arUser['NAME'],
                        "OPERATOR_EMAIL" => $this->arUser['EMAIL'],
                        "RESPONSE_EMAIL" => $this->arTicket['FIELDS']['FB_EMAIL']['USER_TEXT'],
                    ];

                    if (!empty($this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']))
                    {
                        Loader::includeModule('nota.exalead');

                        $query = new SearchQuery();
                        $query->getById($this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']);

                        $client = new SearchClient();
                        $res = $client->getResult($query);

                        unset ($query, $client);

                        $arMail['EX_BOOK_LINK'] = 'Издание: <a href="'.$_SERVER['HTTP_ORIGIN'].'/catalog/'.$this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT'].'/">' . $res['authorbook_original'] . ': ' . $res['name'] . '</a>';
                    }

                    $this->sendMail("NEB_FEEDBACK_APPOINT", $arMail);
                }
            }
        }

        $this->getTicket(); // для показа результата в JSON
        $this->arResult['TICKET'] = $this->arTicket;

        // выдача результата: в режиме аякса вывод в виде JSON, иначе - редирект
        if ($this->arRequest['ajax'] == 'Y') {
            $this->arResult['TICKET']['USER'] = '';
            $this->arResult['json'] = true;
        } elseif (empty($this->arResult['ERROR'])) {
            LocalRedirect($this->arParams['FEEDBACK_URL']);
        }
    }
}