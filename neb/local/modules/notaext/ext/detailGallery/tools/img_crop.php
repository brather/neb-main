<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/prolog.php");

use \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('notaext');
Loc::loadMessages(__FILE__);

if (!check_bitrix_sessid() or !Bitrix\NotaExt\Ext\DetailGallery\Access::check())
    die(GetMessage("ACCESS_DENIED"));

$idItem = intval($_REQUEST['id']);
if ($idItem <= 0)
    die(GetMessage("Empty ID"));

$arFilter = array(
    'IBLOCK_ID' => Bitrix\NotaExt\Ext\DetailGallery\Params::getParam('save_ib'),
    'ID' => $idItem,
    '!DETAIL_PICTURE' => false
);

$arSelect = array('ID', 'DETAIL_PICTURE', 'skip_other');
$arSort = array();

$resResult = Bitrix\NotaExt\Iblock\Element::getList($arFilter, 1, $arSelect, $arSort);

if (empty($resResult['ITEM']))
    die('Not found photo');

$arCropParams = array(
    'x' => intval($_REQUEST['x']),
    'y' => intval($_REQUEST['y']),
    'width' => intval($_REQUEST['width']),
    'height' => intval($_REQUEST['height']),
    'wResize' => intval($_REQUEST['wResize']),
    'hResize' => intval($_REQUEST['hResize']),
    'rotate' => intval($_REQUEST['rotate']),
    'exact_size' => (empty($_REQUEST['exact_size']) ? false : ($_REQUEST['exact_size'] == 'true')),
);

if (!empty($_REQUEST['prev']) and $_REQUEST['prev'] == 'y')
    $SavePatch = '/upload/resize_detailGalleryEditor';
else
    $SavePatch = '/upload/resize_cache_dg';

$img = Bitrix\NotaExt\ImgConvert::Crop($resResult['ITEM']['DETAIL_PICTURE'], $arCropParams, $SavePatch);

MainHelper::showJson(array('img' => $img));
