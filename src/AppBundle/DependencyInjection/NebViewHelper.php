<?php
/**
 * User: agolodkov
 * Date: 22.09.2016
 * Time: 14:25
 */

namespace AppBundle\DependencyInjection;


use BitrixBundle\DependencyInjection\BitrixHelper;
use BitrixBundle\DependencyInjection\BitrixViewHelper;
use Neb\Main\Helper\TemplateHelper;
use Symfony\Component\Templating\Helper\Helper;

/**
 * Class NebHelper
 *
 * @package AppBundle\DependencyInjection
 */
class NebViewHelper extends Helper
{
    /**
     * @var BitrixHelper
     */
    protected $_bitrix;
    /**
     * @var BitrixViewHelper
     */
    protected $_bitrixView;

    /**
     * @param BitrixHelper     $bitrix
     * @param BitrixViewHelper $bitrixView
     */
    public function __construct(BitrixHelper $bitrix, BitrixViewHelper $bitrixView)
    {
        $this->_bitrix = $bitrix;
        $this->_bitrixView = $bitrixView;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'nebViewHelper';
    }

    /**
     * @return TemplateHelper
     */
    public function templateHelper()
    {
        return TemplateHelper::getInstance();
    }
}