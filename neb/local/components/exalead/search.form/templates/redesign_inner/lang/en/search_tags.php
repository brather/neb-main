<?php
$MESS["YEAR_OF_PUBLICATION"] = 'year of publication'; //года издания
$MESS["RESET_FILTER"] = 'Reset filter'; //Убрать фильтр
$MESS["FROM"] = 'From'; // От
$MESS["TO"] = 'To'; // До
$MESS["COLLECTION"] = 'Collection'; // Коллекция
$MESS["SEARCH_EXACT_PHRASE"] = 'Search exact phrase'; // Искать по точной фразе
$MESS["SEARCH_WITHIN_RESULTS"] = 'Search within results'; // Искать в найденном
$MESS["LIB_SEARCH_FORM_TEMPLATE_AUTHORSBOOK"] = 'Authors'; // Авторы';
$MESS["PUBLISHYEAR"] = 'Publish year';
$MESS["SEARCH_CATEGORY"] = 'Category'; // Рубрика