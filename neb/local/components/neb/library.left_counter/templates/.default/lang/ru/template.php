<?php
$MESS['LIBRARY_RIGHT_COUNTER_FUND'] = 'Из фонда библиотеки в НЭБ представлено';

$MESS['LIBRARY_RIGHT_COUNTER_PUB_5'] = 'изданий';
$MESS['LIBRARY_RIGHT_COUNTER_PUB_1'] = 'издание';
$MESS['LIBRARY_RIGHT_COUNTER_PUB_2'] = 'издания';

$MESS['LIBRARY_RIGHT_COUNTER_INC'] = 'Библиотека собрала';

$MESS['LIBRARY_RIGHT_COUNTER_COL_5'] = 'подборок';
$MESS['LIBRARY_RIGHT_COUNTER_COL_1'] = 'подборку';
$MESS['LIBRARY_RIGHT_COUNTER_COL_2'] = 'подборки';

$MESS['LIBRARY_RIGHT_COUNTER_VIEW_ALL'] = 'посмотреть все';
$MESS['LIBRARY_RIGHT_COUNTER_READER'] = 'Количество читателей';
$MESS['LIBRARY_RIGHT_COUNTER_VIEWERS'] = 'Количество просмотров';