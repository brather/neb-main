<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetPageProperty('hide-title', true);
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<header>
    <div class="container">
        <div class="row">
            <div class="neb-logo-mid">
                <a href="/" title="<?=Loc::getMessage('NATIONAL_ELECTRONIC_LIBRARY')?>">
                    <img src="/local/templates/adaptive/img/logo-mid.png" alt="<?=Loc::getMessage('NATIONAL_ELECTRONIC_LIBRARY')?>" class="">
                </a>
            </div>
        </div>
    </div>
</header>

<section class="narrow-local">
	<div class="b-formsubmit" style='display: block!important'>
		<?if($_REQUEST['change_pwd']){?>
			<p><? ShowMessage($arParams["~AUTH_RESULT"]); ?></p>
		<?}?>
	</div>
	<div class="b-passform rel">
			<h2 class="mode"><?=GetMessage('RECOVERY'); ?></h2>
			<form name="bform" class="b-form b-form_common b-passchge" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
				<script>
					var rulesAndMessages = {
						rules: {},
						messages: {}
					};
				</script>
				<?if (strlen($arResult["BACKURL"]) > 0): ?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<? endif ?>
				<input type="hidden" name="AUTH_FORM" value="Y">
				<input type="hidden" name="TYPE" value="CHANGE_PWD">
				<input type="hidden" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" class="bx-auth-input" />
				<input type="hidden" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" class="bx-auth-input" />

				<div class="form-group">
					<label for="settings05"><?=GetMessage("AUTH_NEW_PASSWORD")?> <em>(<?=GetMessage("SIX")?>)</em></label>
					<input type="password" 
						value="" 
						id="settings05" 
						name="USER_PASSWORD" 
						class="form-control input-lg">										
					<script>
				        rulesAndMessages.rules["USER_PASSWORD"] = {
				            required: true,
				            minlength: 6,
				            maxlength: 30
				        };
				        rulesAndMessages.messages["USER_PASSWORD"] = {
				            required: '<?=GetMessage("REQUIRED")?>',
				            minlength: '<?=GetMessage("MIN_LEN")?>',
				            maxlength: '<?=GetMessage("MAX_LEN")?>'
				        };
				    </script>
				</div>

				<div class="form-group">
					<label for="settings55"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></label>
					<input 
						type="password" 
						value="" 
						id="settings55" 
						name="USER_CONFIRM_PASSWORD" 
						class="form-control input-lg" 
					>										
					<script>
				        rulesAndMessages.rules["USER_CONFIRM_PASSWORD"] = {
				            required: true,
				            minlength: 6,
				            maxlength: 30,
				            equalTo: '#settings05'
				        };
				        rulesAndMessages.messages["USER_CONFIRM_PASSWORD"] = {
				            required: '<?=GetMessage("REQUIRED")?>',
				            minlength: '<?=GetMessage("MIN_LEN")?>',
				            maxlength: '<?=GetMessage("MAX_LEN")?>',
				            equalTo: 'Пароль не совпадает с введенным ранее'
				        };
				    </script>
				</div>
				<div class="text-center" style="margin-top: 2em;">
					<button name="change_pwd" class="btn btn-primary btn-lg" value="1" type="submit"><?=GetMessage("SEND")?></button>
				</div>
			</form>
			<script>
				$(function(){

					$('form[name="bform"]').validate({
			            rules: rulesAndMessages.rules,
			            messages: rulesAndMessages.messages,            
			            errorPlacement: function(error, element) {
			                if ( element.attr('type') == "radio" ) {
			                    error.appendTo( $(element).closest('.form-group').find('label')[0] );
			                } else {
			                    error.appendTo(element.parent());
			                }
			                $(element).closest('.form-group').toggleClass('has-error', true);
			            },
			            /*specifying a submitHandler prevents the default submit, good for the demo
			            submitHandler: function(form) {
			                console.log("valid!");
			            },
			            set this class to error-labels to indicate valid fields*/
			            success: function(label) {
			                /* set &nbsp; as text for IE
			                label.html("&nbsp;").addClass("resolved");*/
			                label.remove()
			            },
			            highlight: function(element, errorClass, validClass) {        
			                $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
			                $(element).parent().find("." + errorClass).removeClass("resolved");
			            },
			            unhighlight: function(element, errorClass, validClass) {
			                $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
			            }
			        });

				});
			</script>
		</div><!-- /.b-registration-->
	</section>
</div><!-- /.homepage -->		
