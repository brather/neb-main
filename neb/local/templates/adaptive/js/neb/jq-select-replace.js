/**
 * Что то из старого
 */
(function ($) { //create closure
    $.fn.selectReplace = function (options) {
        this.each(function () {
            var field = $(this), selblock = $("<span class=\"inpselblock\"></span>"), seltxt = $("<span class=\"inpseltxt\"></span>"), w = field.outerWidth();
            if (field.hasClass('custom')) return false;
            if (field.hasClass('hidden')) {
                selblock.addClass('hidden');
            }
            selblock.parent().width(w);
            //selblock.width(w);
            if (field.hasClass('b_searchopt')) {
                selblock.addClass('searchopt');
            }

            field.wrap(selblock);
            seltxt.insertBefore(field);
            seltxt.text($("option:selected", field).text());
            //seltxt.css('border', '1px solid red');
            var selopener = $("<a class=\"selopener\" href=\"#\"></a>").insertBefore(seltxt);
            if (field.attr('disabled')) {
                selopener.addClass('disabled');
            }
            var optcontainer = $("<div class=\"optcontainer" + ((field.hasClass('round')) ? ' round' : '') + "\" id=\"" + field.attr("name") + "opts\"></div>").appendTo("body");
            optcontainer.css('min-width', w).hide(function () {
                $(this).addClass("hide");
            });

            if ($(this).data('class') !== undefined) {
                optcontainer.addClass($(this).data('class'));
            }
            if ($(this).data('seltext') !== undefined) {
                seltxt.addClass($(this).data('seltext'));
            }
            /*
             seltxt.eq(0).addClass('filter-field-first');
             seltxt.eq(1).addClass('filter-field-second');*/

            $("option", field).each(function (k) {
                var opt = $(this);
                var aopt = $("<a href=\"" + opt.val() + "\" title=\"" + opt.text() + "\">" + opt.text() + "</a>").appendTo(optcontainer);
                aopt.click(function () {
                    field.get(0).selectedIndex = k;
                    seltxt.text($("option:eq(" + k + ")", field).text());

                    optcontainer.find('.selected').removeClass('selected');
                    $(this).addClass('selected');
                    optcontainer.slideUp("fast", function () {
                        $(this).addClass("hide");
                        selopener.removeClass('open');
                    });
                    field.change();
                    return false;
                });
                if (opt.prop('selected')) aopt.addClass('selected');
            });
            selopener.click(function () {
                if ($(this).closest('.inpselblock').find('select').prop('disabled'))return false;
                if ($(this).hasClass('disabled')) return false;

                $('.inpselblock').removeClass('open');
                $("div.optcontainer").slideUp("fast", function () {
                    $(this).addClass("hide");
                });

                if (optcontainer.hasClass("hide")) {
                    var pos = seltxt.offset(), zi = 0;
                    optcontainer.removeClass("hide");

                    optcontainer.css({
                        left: pos.left + "px",
                        top: pos.top - 1 + "px",
                        'min-width': field.outerWidth(),
                        'max-width': field.width()
                    });
                    optcontainer.slideDown("fast", function () {
                        $(document).on("click", function (e) {
                            if (e.pageX < pos.left
                                || e.pageX > (pos.left + optcontainer.width())
                                || e.pageY < (pos.top)
                                || e.pageY > (pos.top + optcontainer.height())) {
                                optcontainer.slideUp("fast", function () {
                                    $(this).addClass("hide");
                                    selopener.removeClass('open');
                                });
                            }
                        });
                        selopener.addClass('open');
                    });

                    optcontainer.siblings().each(function () {
                        var nzi = parseInt($(this).css('z-index'));
                        zi = Math.max(zi, (!isNaN(nzi)) ? nzi : 0);
                    });
                    optcontainer.css({'z-index': zi + 90});


                } else {
                    optcontainer.slideUp("fast", function () {
                        $(this).addClass("hide");
                        selopener.removeClass('open');
                    });
                }


                return false;
            });

            field.addClass('custom');
        });
    }
})(jQuery);