<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LbcSpecialdivisionsectionimages
 *
 * @ORM\Table(name="lbc_specialdivisionsectionimages")
 * @ORM\Entity
 */
class LbcSpecialdivisionsectionimages extends AbstractLbcImages
{

    /**
     * @var integer
     *
     * @ORM\Column(name="SpecialDivisionSectionId", type="integer", nullable=false)
     */
    private $specialdivisionsectionid;


    /**
     * Set specialdivisionsectionid
     *
     * @param integer $specialdivisionsectionid
     *
     * @return LbcSpecialdivisionsectionimages
     */
    public function setSpecialdivisionsectionid($specialdivisionsectionid)
    {
        $this->specialdivisionsectionid = $specialdivisionsectionid;

        return $this;
    }

    /**
     * Get specialdivisionsectionid
     *
     * @return integer
     */
    public function getSpecialdivisionsectionid()
    {
        return $this->specialdivisionsectionid;
    }

}
