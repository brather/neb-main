<?php
/**
 * User: agolodkov
 * Date: 12.07.2016
 * Time: 17:20
 */

namespace Neb\Main\Agents;

use \Bitrix\Main\Application;
use \Bitrix\Main\Config\Configuration;
use \Bitrix\Main\DB\MysqlConnection;

use \Neb\Main\Helper\Debug;
use \Neb\Main\Stat\LibraryDailyTracker;

/**
 * Class StatsSaver
 *
 * @package Neb\Main\Agents
 */
class StatsSaver
{
    /**
     * @return string
     * @throws \Exception
     */
    public static function saveLibraryBooksCount()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'LibraryBooks');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public static function saveLibraryBooksDigitizedCount()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'LibraryBooksDigitized');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public static function saveLibraryUsersRegisters()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'LibraryUsersRegisters');
    }

    /**
     * @return string
     */
    public static function savePublicationNotProtected()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'PublicationProtected');
    }

    /**
     * @return string
     */
    public static function savePublicationProtected()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'PublicationProtected', $isProtected = 1);
    }

    /**
     * @return string
     */
    public static function savePublicationCopyright()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'PublicationProtected', $isProtected = 2);
    }

    /**
     * @return string
     */
    public static function savePublicationMarkup()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'PublicationMarkup');
    }

    /**
     * @return string
     */
    public static function savePublicationLanguage()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'PublicationLanguage');
    }

    /**
     * @return string
     */
    public static function saveCollectionOnLibrary()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'LibraryCollection');
    }

    /**
     * @return string
     */
    public static function saveFileExtension()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'FileExtension');
    }

    /**
     * @return string
     */
    public static function saveBBKFull()
    {
        return static::_libraryTrackerMethod(__FUNCTION__, 'BBKFull');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public static function saveLibraryNebStatLog()
    {
        $connectionnsConfig = Configuration::getValue('connections');
        $connectionPool = Application::getInstance()->getConnectionPool();
        $connectionPool->setConnectionParameters(
            'check_date', $connectionnsConfig['default']
        );
        $dateCheckConnection = $connectionPool->getConnection('check_date');
        try {
            $agent = Service::getAgentByExecute("\\" . __CLASS__ . '::' . __FUNCTION__ . '();');
            $nextExec = strtotime($agent['NEXT_EXEC']);
            $date = date('Y-m-d', $nextExec);
            $tracker = new LibraryDailyTracker(0, $date);
            $tracker->trackLibraryBookStatLog(
                [
                    'onTick' => function () use ($agent, $dateCheckConnection) {
                        /** @var MysqlConnection $dateCheckConnection */
                        if ($agentId = (integer)$agent['ID']) {
                            $dateCheckConnection->query(
"
UPDATE b_agent
SET DATE_CHECK = DATE_ADD(NOW(), INTERVAL 1200 SECOND)
WHERE ID = $agentId;
"
                            );
                        }
                    }
                ]
            );
            $dateCheckConnection->disconnect();
        } catch (\Exception $e) {
            Debug::error(strval($e));
            $log = new \CEventLog();
            $log->Log('ERROR', 'stats_saver', 'neb.main', __FUNCTION__, strval($e));
            $dateCheckConnection->disconnect();
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }

        return static::_libraryTrackerMethod(__FUNCTION__, 'LibraryStatLog');
    }

    /**
     * @param $saverMethod
     * @param $trackerMethod
     * @param null $param
     *
     * @return string
     * @throws \Exception
     */
    private static function _libraryTrackerMethod ( $saverMethod, $trackerMethod, $param = null )
    {
        $execute = "\\". __CLASS__ . '::' . $saverMethod . '();';
        try {
            $agent = Service::getAgentByExecute($execute);
            $nextExec = strtotime($agent['NEXT_EXEC']);
            $date = date('Y-m-d', $nextExec);

            LibraryDailyTracker::trackAllLibrariesStat(
                $trackerMethod,
                $date,
                [
                    'onTick' => function () use ($agent) {
                        \CAgent::Update($agent['ID'], ['DATE_CHECK' => ConvertTimeStamp(time() + (60 * 20), 'FULL')]);
                    }
                ],
                $param
            );

            \CAgent::Update($agent['ID'], ['NEXT_EXEC' => ConvertTimeStamp($nextExec, 'FULL')]);

        } catch (\Exception $e) {
            Debug::error(strval($e));
            $log = new \CEventLog();
            $log->Log('ERROR', 'stats_saver', 'neb.main', $saverMethod, strval($e));
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }

        return $execute;
    }
}