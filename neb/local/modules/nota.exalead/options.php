<?
global $APPLICATION, $USER;
if (!$USER->IsAdmin())
    return false;

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

$arExaleadOptions = array(
    array("exalead_ip", Loc::getMessage("EXALEAD_IP"), false, array("text", 20)),
    array("exalead_port", Loc::getMessage("EXALEAD_PORT"), false, array("text", 5)),
);

$arViewerOptions = array(
    array("viewer_ip", Loc::getMessage("VIEWER_IP"), false, array("text", 20)),
    array("viewer_port", Loc::getMessage("VIEWER_PORT"), false, array("text", 5)),    
    array("viewer_protocol", Loc::getMessage("VIEWER_PROTOCOL"), false, array('selectbox', array(
        "http" => "http",
        "https" => "https"
    ))),
    array("viewer_key", Loc::getMessage("VIEWER_KEY"), false, array("text", 40)),
    array("viewer_pdf_directory", Loc::getMessage("VIEWER_DIRECTORY_FOR_PDF_FILES"), false, array("text", 40)),
);

$arBooksAddOptions = array(
    array("book_add_file_path", Loc::getMessage("BOOK_ADD_FILE_PATH"), false, array("text", 50)),
    array("book_add_file_path_2", Loc::getMessage("BOOK_ADD_FILE_PATH_2"), false, array("text", 50)),
    array("book_add_url", Loc::getMessage("BOOK_ADD_URL"), false, array("text", 50)),
    array("book_add_exalead_ip", Loc::getMessage("BOOK_ADD_EXALEAD_IP"), false, array("text", 14)),
    array("book_add_login", Loc::getMessage("BOOK_ADD_LOGIN"), false, array("text", 20)),
    array("book_add_password", Loc::getMessage("BOOK_ADD_PASSWORD"), false, array("text", 20)),
    array("book_add_db", Loc::getMessage("BOOK_ADD_DB"), false, array("text", 20)),
);

$arViewerOptionsEx = Array(
    array("viewer_version", Loc::getMessage("VIEWER_VERSION"), false, array("text", 10)),
    array("viewer_link", Loc::getMessage("VIEWER_LINK"), false, array("text", 60)),
    array("viewer_link_mac", Loc::getMessage("VIEWER_LINK_MAC"), false, array("text", 60)),
    array("viewer_ex_warning_show", Loc::getMessage("VIEWER_WARNING_SHOW"), false, array("checkbox")),
    array("viewer_ex_warning_text", Loc::getMessage("VIEWER_WARNING_TEXT"), false, array('textarea', 10, 50)),
    array("viewer_ex_book_percent", Loc::getMessage("VIEWER_BOOK_PERCENT"), '10', array("text", 3)),
    array("viewer_ex_library_window_show", Loc::getMessage("VIEWER_LIBRARY_WINDOW_SHOW"), false, array("checkbox")),
    array("viewer_ex_library_window_text", Loc::getMessage("VIEWER_LIBRARY_WINDOW_TEXT"), false, array('textarea', 10, 50)),
    array("viewer_ex_library_window_period", Loc::getMessage("VIEWER_LIBRARY_WINDOW_PERIOD"), false, array('selectbox', array(
        "1" => "По времени",
        "2" => "По страницам"
    ))),
    array("viewer_ex_library_window_interval", Loc::getMessage("VIEWER_LIBRARY_WINDOW_INTERVAL"), false, array("text", 5)),
    array("viewer_ex_library_window_grow", Loc::getMessage("VIEWER_LIBRARY_WINDOW_GROW"), false, array("checkbox")),
    array("viewer_ex_library_window_grow_speed", Loc::getMessage("VIEWER_LIBRARY_WINDOW_GROW_SPEED"), false, array("text", 5)),
    array("viewer_ex_books_show", Loc::getMessage("VIEWER_BOOK_SHOW"), false, array("checkbox")),
    array("viewer_ex_message", Loc::getMessage("VIEWER_MESSAGE"), false, array('textarea', 10, 50)),
    
    Loc::getMessage("VIEWER_BLOCK_SETTINGS"),
    array("viewer_ex_analysis_period", Loc::getMessage("VIEWER_ANALYSIS_PERIOD"), '1', array("text", 4)),
    array("viewer_ex_max_open_page_count", Loc::getMessage("VIEWER_MAX_OPEN_PAGE_COUNT"), '', array("text", 4)),
    array("viewer_ex_identification", Loc::getMessage("VIEWER_IDENTIFICATION"), false, array('selectbox', array(
        "1" => "по токену",
        "2" => "по fingerprint",
        "3" => "по сессии",
    ))),
    array("viewer_ex_block_user", Loc::getMessage("VIEWER_BLOCK_USER"), false, array("checkbox")),
    array("viewer_ex_block_computer", Loc::getMessage("VIEWER_BLOCK_COMPUTER"), false, array("checkbox")),
    array("viewer_ex_block_workplace", Loc::getMessage("VIEWER_BLOCK_WORKPLACE"), false, array("checkbox")),

    Loc::getMessage("VIEWER_WATERMARK_SETTINGS"),
    array("viewer_ex_watermark_email", Loc::getMessage("VIEWER_WATERMARK_EMAIL"), '', array("text", 50)),
    array("viewer_ex_watermark_date", Loc::getMessage("VIEWER_WATERMARK_DATE"), '', array("text", 50)),
    array("viewer_ex_watermark_workplace", Loc::getMessage("VIEWER_WATERMARK_WORKPALCE"), '', array("text", 50)),
    array("viewer_ex_watermark_position", Loc::getMessage("VIEWER_WATERMARK_POSITION"), false, array('selectbox', array(
        "1" => "верхний правый угол",
        "2" => "нижний правый угол",
        "3" => "нижний левый угол",
        "4" => "верхний левый угол",
    ))),
    array("viewer_ex_watermark_color", Loc::getMessage("VIEWER_WATERMARK_COLOR"), '', array("text", 50)),
);

$arOtherOptions = array(
    array("search_query_translate", Loc::getMessage("SEARCH_QUERY_TRANSLATE"), false, array("checkbox")),
);

$aTabs = array(
    array(
        "DIV" => "exalead_settings",
        "TAB" => Loc::getMessage("EXALEAD_SETTINGS"),
        "TITLE" => Loc::getMessage("EXALEAD_SETTINGS"),
        "OPTIONS" => array(),
    ),
    array(
        "DIV" => "viewer_settings",
        "TAB" => Loc::getMessage("RSL_SETTINGS"),
        "TITLE" => Loc::getMessage("RSL_SETTINGS"),
        "OPTIONS" => array(),
    ),
    array(
        "DIV" => "books_add",
        "TAB" => Loc::getMessage("BOOKS_ADD_SETTINGS"),
        "TITLE" => Loc::getMessage("BOOKS_ADD_SETTINGS"),
        "OPTIONS" => array(),
    ),
    array(
        "DIV" => "viewer_settings_ex",
        "TAB" => Loc::getMessage("VIEWER_SETTINGS"),
        "TITLE" => Loc::getMessage("VIEWER_SETTINGS"),
        "OPTIONS" => array(),
    ),
    array(
        "DIV" => "other_settings",
        "TAB" => Loc::getMessage("OTHER_SETTINGS"),
        "TITLE" => Loc::getMessage("OTHER_SETTINGS"),
        "OPTIONS" => array(),
    ),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid()) {
    if (strlen($RestoreDefaults) > 0) {
        COption::RemoveOption($mid);
    } else {
        __AdmSettingsSaveOptions($mid, $arExaleadOptions);
        __AdmSettingsSaveOptions($mid, $arViewerOptions);
        __AdmSettingsSaveOptions($mid, $arBooksAddOptions);
        __AdmSettingsSaveOptions($mid, $arViewerOptionsEx);
        __AdmSettingsSaveOptions($mid, $arOtherOptions);
    }

    if (strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
        LocalRedirect($_REQUEST["back_url_settings"]);
    else
        LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID)
            . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
}

$tabControl->Begin();
?>

<form method="post" action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>">
    <?
    $tabControl->BeginNextTab();
    __AdmSettingsDrawList($mid, $arExaleadOptions);

    $tabControl->BeginNextTab();
    __AdmSettingsDrawList($mid, $arViewerOptions);

    $tabControl->BeginNextTab();
    __AdmSettingsDrawList($mid, $arBooksAddOptions);

    $tabControl->BeginNextTab();
    __AdmSettingsDrawList($mid, $arViewerOptionsEx);

    $tabControl->BeginNextTab();
    __AdmSettingsDrawList($mid, $arOtherOptions);

    $tabControl->Buttons(); ?>
    <input type="submit" name="Update" value="<?= Loc::getMessage("MAIN_SAVE") ?>"
           title="<?= Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>" class="adm-btn-save">
    <input type="submit" name="Apply" value="<?= Loc::getMessage("MAIN_OPT_APPLY") ?>"
           title="<?= Loc::getMessage("MAIN_OPT_APPLY_TITLE") ?>">
    <? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
        <input type="button" name="Cancel" value="<?= Loc::getMessage("MAIN_OPT_CANCEL") ?>"
               title="<?= Loc::getMessage("MAIN_OPT_CANCEL_TITLE") ?>"
               onclick="window.location='<? echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
        <input type="hidden" name="back_url_settings" value="<?= htmlspecialcharsbx($_REQUEST["back_url_settings"]) ?>">
    <? endif ?>
    <input type="submit" name="RestoreDefaults" title="<? echo Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           OnClick="return confirm('<? echo AddSlashes(Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
           value="<? echo Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>" />
    <?= bitrix_sessid_post(); ?>
    <? $tabControl->End(); ?>
</form>