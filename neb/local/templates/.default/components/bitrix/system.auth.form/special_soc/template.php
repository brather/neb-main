<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper clearfix">
    <div class="b-registration rel">

        <h2 class="mode">Вход через социальные сети</h2>
        <?
        if(!empty($arResult['ERROR_MESSAGE']['MESSAGE']) ||
            (is_string($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE'])))
        {
            if(is_array($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']['MESSAGE']))
            {
                $arResult['ERROR_MESSAGE'] = $arResult['ERROR_MESSAGE']['MESSAGE'];
            }
            ?>
            <em class="error_message"><?=$arResult['ERROR_MESSAGE'];?></em>
            <script type="text/javascript">
                $(function() {
                    $('#login-link').click();
                });
            </script>
        <?
        }
        ?>
        <div class="b-form b-form_common b-regform">
            <hr/>
            <div class="b-login_social b_login_popup_in">
                <?if(isset($USER) && is_object($USER) && $USER->IsAuthorized()):?>
                    Вы успешно авторизовались на сайте!
                <?else:?>
                    <?foreach ($arResult['AUTH_SERVICES'] as $authService):?>
                        <a href="javascript:void(0)" onclick="<?=$authService['AUTH_LINK']?>" class="b-login_slnk <?=$authService['CSS_CLASS']?>"><?=$authService['NAME']?></a>
                    <?endforeach;?>
                <?endif;?>
            </div><!-- /.b-login_social -->
            <hr/>
        </div>
    </div><!-- /.b-registration-->
</section>
