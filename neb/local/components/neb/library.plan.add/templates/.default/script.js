$(function(){
    var formAddPlan = $('#DATE_ADD_PLAN'),
        calendarDateFPlan = $('#DATA_UF_DATE_F_PLAN'),
        inputLang = formAddPlan.find('[name=ARR_LANG_INPUT]');

    function ruDate(dateString) {
        var d = parseInt(dateString[0] + dateString[1]),
            m = parseInt(dateString[3] + dateString[4]),
            y = parseInt(dateString[6] + dateString[7] + dateString[8] + dateString[9]),
            date = new Date(y, m - 1, d);
        return date;
    }

    jQuery.validator.addMethod(
        "inFuture", 
        function(value, element, params) {
            var form = $(element).closest('form');
            if (!/Invalid|NaN/.test( ruDate(value) )) {
                return ruDate(value) >= new Date();
            }
        },
        'Must be in future.'
    );

    (function(){
        formAddPlan.validate({
            rules: {
                "DATA_UF_DATE_F_PLAN": {
                    inFuture: true
                }
            },
            messages: {
                "DATA_UF_DATE_F_PLAN": {
                    inFuture: "Дата должна быть в будущем"
                }
            },
            errorPlacement: function(error, element) {
                error.appendTo( $(element).closest('.form-group') );
                $(element).closest('.form-group').toggleClass('has-error', true);
            },
            success: function(label) {
                label.remove();
            },
            highlight: function(element, errorClass, validClass) {        
                $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                $(element).parent().find("." + errorClass).removeClass("resolved");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
            },
            submitHandler: function(form) {
                console.log("valid!");

                var targetDateRaw = calendarDateFPlan.val();
                var ojbLang = {
                    T_PLAN_ADD_AJAX_DATE_CREATE : inputLang.data('lang-date-create'),
                    T_PLAN_ADD_AJAX_TITLE_DATE_F_PLAN : inputLang.data('lang-date-digitization'),
                    T_PLAN_ADD_AJAX_STATUS : inputLang.data('lang-status'),
                    T_PLAN_ADD_AJAX_CHANGE : inputLang.data('lang-change'),
                    T_PLAN_ADD_AJAX_DELETE : inputLang.data('lang-delete'),
                    T_PLAN_ADD_AJAX_DELETE_CONFIRM : inputLang.data('lang-delete-confirm'),
                    T_PLAN_ADD_AJAX_STATUS_AGREEMENT : inputLang.data('lang-agreement'),
                    T_PLAN_ADD_AJAX_RESTART_PAGE : inputLang.data('lang-restatr-page'),
                    T_PLAN_ADD_AJAX_ERROR : inputLang.data('lang-error')
                },
                objData = {
                    DATA_AJAX : true,
                    DATA_UF_LIBRARY : formAddPlan.find('input[name=DATA_UF_LIBRARY]').val(),
                    DATA_UF_DATE_F_PLAN : targetDateRaw,
                    DATA_SEF_FOLDER : formAddPlan.data('sef-folder'),
                    DATA_AR_LANG : ojbLang
                },
                container = $('#container-for-ajax').find('.row');
                calendarDateFPlan.removeClass('error');
                startLoader();
                $.ajax({
                    url : formAddPlan.data('ajax'),
                    type : formAddPlan.attr('method'),
                    data : objData,
                    dataType: 'json',
                    success : function(data) {
                        if(data.SUCCESS) {
                            window.location.replace(formAddPlan.prop('action') + 'info/' + data.SUCCESS.NEW_PLAN_ID + '/');
                        } else if(data.ERROR) {
                            container.find('.row').html(data.ERROR);
                            stopLoader();
                        }
                    }
                });

            }
        });

        formAddPlan.on('change, blur','[name="DATA_UF_DATE_F_PLAN"]',function(){
            formAddPlan.find('[name="DATA_UF_DATE_F_PLAN"]').valid();
        });

    })()

    function startLoader() {
        var blkSpin = $('#milk-shadow'),
            clsSpin = 'draw',
            clsBlur = 'tag-change',
            $body = $('body');

        if (blkSpin.length == 0) {
            var el = $('<div/>').attr('id','milk-shadow');
            $('body').append(el);
        }
        blkSpin.addClass(clsSpin);
        FRONT.spinner.execute( blkSpin[0] );
        $body.addClass(clsBlur);
    }
    //endregion
    //region Функция остановки лоадера
    function stopLoader() {
        var blkSpin = $('#milk-shadow'),
            clsSpin = 'draw',
            clsBlur = 'tag-change',
            $body = $('body');
        blkSpin.removeClass(clsSpin);
        $body.removeClass(clsBlur);
    }
    //endregion
});