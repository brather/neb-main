<?php
namespace Neb\Main\Library;

use Bitrix\Main\Entity;

class NebLibsAreaTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'neb_libs_area';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\StringField('UF_LIB_AREA_NAME'),
            new Entity\StringField('UF_LIB_AREA_NAME_S'),
            new Entity\IntegerField('UF_SORT'),
        );
    }
}
