<?php
/**
 * User: agolodkov
 * Date: 17.08.2016
 * Time: 17:02
 */

namespace LbcBundle\Controller\Api;


use LbcBundle\Form\LbcSpecialdivisionType;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("app.api.specialdivision")
 */
class LbcSpecialdivisionController extends AbstractCategoryController
{

    /**
     * @return LbcSpecialdivisionType
     */
    public function getFormType()
    {
        return new LbcSpecialdivisionType();
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcSpecialdivision';
    }

    /**
     * @return string
     */
    public function getCategoryOwnerFieldName()
    {
        return 'maindivisionid';
    }
}