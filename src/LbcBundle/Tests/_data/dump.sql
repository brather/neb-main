-- MySQL dump 10.13  Distrib 5.6.21, for Win32 (x86)
--
-- Host: localhost    Database: neb
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `b_admin_notify`
--

DROP TABLE IF EXISTS `b_admin_notify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_admin_notify` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `ENABLE_CLOSE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `PUBLIC_SECTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `IX_AD_TAG` (`TAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_admin_notify_lang`
--

DROP TABLE IF EXISTS `b_admin_notify_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_admin_notify_lang` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NOTIFY_ID` int(18) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_ADM_NTFY_LANG` (`NOTIFY_ID`,`LID`),
  KEY `IX_ADM_NTFY_LID` (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_agent`
--

DROP TABLE IF EXISTS `b_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_agent` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(18) NOT NULL DEFAULT '100',
  `NAME` text COLLATE utf8_unicode_ci,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `LAST_EXEC` datetime DEFAULT NULL,
  `NEXT_EXEC` datetime NOT NULL,
  `DATE_CHECK` datetime DEFAULT NULL,
  `AGENT_INTERVAL` int(18) DEFAULT '86400',
  `IS_PERIOD` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `USER_ID` int(18) DEFAULT NULL,
  `RUNNING` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `ix_act_next_exec` (`ACTIVE`,`NEXT_EXEC`),
  KEY `ix_agent_user_id` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=52048 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_app_password`
--

DROP TABLE IF EXISTS `b_app_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_app_password` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `APPLICATION_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DIGEST_PASSWORD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `DATE_LOGIN` datetime DEFAULT NULL,
  `LAST_IP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSCOMMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_app_password_user` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_bitrixcloud_option`
--

DROP TABLE IF EXISTS `b_bitrixcloud_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_bitrixcloud_option` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL,
  `PARAM_KEY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARAM_VALUE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_bitrixcloud_option_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_bp_workflow_state_identify`
--

DROP TABLE IF EXISTS `b_bp_workflow_state_identify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_bp_workflow_state_identify` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `WORKFLOW_ID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_bp_wsi_wf` (`WORKFLOW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_cache_tag`
--

DROP TABLE IF EXISTS `b_cache_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_cache_tag` (
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CACHE_SALT` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RELATIVE_PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAG` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `ix_b_cache_tag_0` (`SITE_ID`,`CACHE_SALT`,`RELATIVE_PATH`(50)),
  KEY `ix_b_cache_tag_1` (`TAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_captcha`
--

DROP TABLE IF EXISTS `b_captcha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_captcha` (
  `ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CODE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IP` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  UNIQUE KEY `UX_B_CAPTCHA` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_contractor`
--

DROP TABLE IF EXISTS `b_catalog_contractor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_contractor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSON_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `PERSON_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSON_LASTNAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSON_MIDDLENAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHONE` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POST_INDEX` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COUNTRY` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CITY` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMPANY` varchar(145) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INN` varchar(145) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KPP` varchar(145) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADDRESS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_MODIFY` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_currency`
--

DROP TABLE IF EXISTS `b_catalog_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_currency` (
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `AMOUNT_CNT` int(11) NOT NULL DEFAULT '1',
  `AMOUNT` decimal(18,4) DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DATE_UPDATE` datetime NOT NULL,
  `NUMCODE` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BASE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CREATED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `CURRENT_BASE_RATE` decimal(26,12) DEFAULT NULL,
  PRIMARY KEY (`CURRENCY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_currency_lang`
--

DROP TABLE IF EXISTS `b_catalog_currency_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_currency_lang` (
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_STRING` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FULL_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEC_POINT` varchar(5) COLLATE utf8_unicode_ci DEFAULT '.',
  `THOUSANDS_SEP` varchar(5) COLLATE utf8_unicode_ci DEFAULT ' ',
  `DECIMALS` tinyint(4) NOT NULL DEFAULT '2',
  `THOUSANDS_VARIANT` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HIDE_ZERO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CREATED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  PRIMARY KEY (`CURRENCY`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_currency_rate`
--

DROP TABLE IF EXISTS `b_catalog_currency_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_currency_rate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_RATE` date NOT NULL,
  `RATE_CNT` int(11) NOT NULL DEFAULT '1',
  `RATE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `CREATED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CURRENCY_RATE` (`CURRENCY`,`DATE_RATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_disc_save_group`
--

DROP TABLE IF EXISTS `b_catalog_disc_save_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_disc_save_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_DSG_DISCOUNT` (`DISCOUNT_ID`),
  KEY `IX_CAT_DSG_GROUP` (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_disc_save_range`
--

DROP TABLE IF EXISTS `b_catalog_disc_save_range`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_disc_save_range` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `RANGE_FROM` double NOT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `VALUE` double NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_DSR_DISCOUNT` (`DISCOUNT_ID`),
  KEY `IX_CAT_DSR_DISCOUNT2` (`DISCOUNT_ID`,`RANGE_FROM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_disc_save_user`
--

DROP TABLE IF EXISTS `b_catalog_disc_save_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_disc_save_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ACTIVE_FROM` datetime NOT NULL,
  `ACTIVE_TO` datetime NOT NULL,
  `RANGE_FROM` double NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_DSU_DISCOUNT` (`DISCOUNT_ID`),
  KEY `IX_CAT_DSU_USER` (`DISCOUNT_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_discount`
--

DROP TABLE IF EXISTS `b_catalog_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_discount` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL DEFAULT '0',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `RENEWAL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MAX_USES` int(11) NOT NULL DEFAULT '0',
  `COUNT_USES` int(11) NOT NULL DEFAULT '0',
  `COUPON` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `MAX_DISCOUNT` decimal(18,4) DEFAULT NULL,
  `VALUE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `VALUE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `MIN_ORDER_SUM` decimal(18,4) DEFAULT '0.0000',
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COUNT_PERIOD` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'U',
  `COUNT_SIZE` int(11) NOT NULL DEFAULT '0',
  `COUNT_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `COUNT_FROM` datetime DEFAULT NULL,
  `COUNT_TO` datetime DEFAULT NULL,
  `ACTION_SIZE` int(11) NOT NULL DEFAULT '0',
  `ACTION_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `PRIORITY` int(18) NOT NULL DEFAULT '1',
  `LAST_DISCOUNT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `NOTES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDITIONS` text COLLATE utf8_unicode_ci,
  `UNPACK` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_C_D_COUPON` (`COUPON`),
  KEY `IX_C_D_ACT` (`ACTIVE`,`ACTIVE_FROM`,`ACTIVE_TO`),
  KEY `IX_C_D_ACT_B` (`SITE_ID`,`RENEWAL`,`ACTIVE`,`ACTIVE_FROM`,`ACTIVE_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_discount2cat`
--

DROP TABLE IF EXISTS `b_catalog_discount2cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_discount2cat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `CATALOG_GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2C_CATDIS` (`CATALOG_GROUP_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2C_CATDIS_B` (`DISCOUNT_ID`,`CATALOG_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_discount2group`
--

DROP TABLE IF EXISTS `b_catalog_discount2group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_discount2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2G_GRDIS` (`GROUP_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2G_GRDIS_B` (`DISCOUNT_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_discount2iblock`
--

DROP TABLE IF EXISTS `b_catalog_discount2iblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_discount2iblock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `IBLOCK_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2I_IBDIS` (`IBLOCK_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2I_IBDIS_B` (`DISCOUNT_ID`,`IBLOCK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_discount2product`
--

DROP TABLE IF EXISTS `b_catalog_discount2product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_discount2product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2P_PRODIS` (`PRODUCT_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2P_PRODIS_B` (`DISCOUNT_ID`,`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_discount2section`
--

DROP TABLE IF EXISTS `b_catalog_discount2section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_discount2section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2S_SECDIS` (`SECTION_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2S_SECDIS_B` (`DISCOUNT_ID`,`SECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_discount_cond`
--

DROP TABLE IF EXISTS `b_catalog_discount_cond`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_discount_cond` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_GROUP_ID` int(11) NOT NULL DEFAULT '-1',
  `PRICE_TYPE_ID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_discount_coupon`
--

DROP TABLE IF EXISTS `b_catalog_discount_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_discount_coupon` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `COUPON` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_APPLY` datetime DEFAULT NULL,
  `ONE_TIME` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_cat_dc_index1` (`DISCOUNT_ID`,`COUPON`),
  KEY `ix_cat_dc_index2` (`COUPON`,`ACTIVE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_discount_module`
--

DROP TABLE IF EXISTS `b_catalog_discount_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_discount_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_DSC_MOD` (`DISCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_docs_barcode`
--

DROP TABLE IF EXISTS `b_catalog_docs_barcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_docs_barcode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOC_ELEMENT_ID` int(11) NOT NULL,
  `BARCODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_CATALOG_DOCS_BARCODE1` (`DOC_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_docs_element`
--

DROP TABLE IF EXISTS `b_catalog_docs_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_docs_element` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOC_ID` int(11) NOT NULL,
  `STORE_FROM` int(11) DEFAULT NULL,
  `STORE_TO` int(11) DEFAULT NULL,
  `ELEMENT_ID` int(11) DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `PURCHASING_PRICE` double DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_CATALOG_DOCS_ELEMENT1` (`DOC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_dsc_tmp`
--

DROP TABLE IF EXISTS `b_catalog_dsc_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_dsc_tmp` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_export`
--

DROP TABLE IF EXISTS `b_catalog_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_export` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FILE_NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `DEFAULT_PROFILE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_MENU` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_AGENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_CRON` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SETUP_VARS` text COLLATE utf8_unicode_ci,
  `LAST_USE` datetime DEFAULT NULL,
  `IS_EXPORT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NEED_EDIT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BCAT_EX_FILE_NAME` (`FILE_NAME`),
  KEY `IX_CAT_IS_EXPORT` (`IS_EXPORT`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_extra`
--

DROP TABLE IF EXISTS `b_catalog_extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_extra` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PERCENTAGE` decimal(18,2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_group`
--

DROP TABLE IF EXISTS `b_catalog_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `BASE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_group2group`
--

DROP TABLE IF EXISTS `b_catalog_group2group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_group2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATALOG_GROUP_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `BUY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CATG2G_UNI` (`CATALOG_GROUP_ID`,`GROUP_ID`,`BUY`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_group_lang`
--

DROP TABLE IF EXISTS `b_catalog_group_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_group_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATALOG_GROUP_ID` int(11) NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANG` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CATALOG_GROUP_ID` (`CATALOG_GROUP_ID`,`LANG`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_iblock`
--

DROP TABLE IF EXISTS `b_catalog_iblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_iblock` (
  `IBLOCK_ID` int(11) NOT NULL,
  `YANDEX_EXPORT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SUBSCRIPTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `VAT_ID` int(11) DEFAULT '0',
  `PRODUCT_IBLOCK_ID` int(11) NOT NULL DEFAULT '0',
  `SKU_PROPERTY_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IBLOCK_ID`),
  KEY `IXS_CAT_IB_PRODUCT` (`PRODUCT_IBLOCK_ID`),
  KEY `IXS_CAT_IB_SKU_PROP` (`SKU_PROPERTY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_load`
--

DROP TABLE IF EXISTS `b_catalog_load`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_load` (
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'I',
  `LAST_USED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`NAME`,`TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_measure`
--

DROP TABLE IF EXISTS `b_catalog_measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_measure` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` int(11) NOT NULL,
  `MEASURE_TITLE` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYMBOL_RUS` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYMBOL_INTL` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYMBOL_LETTER_INTL` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_DEFAULT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_CATALOG_MEASURE1` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_measure_ratio`
--

DROP TABLE IF EXISTS `b_catalog_measure_ratio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_measure_ratio` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `RATIO` double NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_CATALOG_MEASURE_RATIO` (`PRODUCT_ID`,`RATIO`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_price`
--

DROP TABLE IF EXISTS `b_catalog_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_price` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `EXTRA_ID` int(11) DEFAULT NULL,
  `CATALOG_GROUP_ID` int(11) NOT NULL,
  `PRICE` decimal(18,2) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `QUANTITY_FROM` int(11) DEFAULT NULL,
  `QUANTITY_TO` int(11) DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRICE_SCALE` decimal(26,12) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_CAT_PRICE_PID` (`PRODUCT_ID`,`CATALOG_GROUP_ID`),
  KEY `IXS_CAT_PRICE_GID` (`CATALOG_GROUP_ID`),
  KEY `IXS_CAT_PRICE_SCALE` (`PRICE_SCALE`)
) ENGINE=InnoDB AUTO_INCREMENT=347 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_product`
--

DROP TABLE IF EXISTS `b_catalog_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_product` (
  `ID` int(11) NOT NULL,
  `QUANTITY` double NOT NULL,
  `QUANTITY_TRACE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `WEIGHT` double NOT NULL DEFAULT '0',
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PRICE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `RECUR_SCHEME_LENGTH` int(11) DEFAULT NULL,
  `RECUR_SCHEME_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `TRIAL_PRICE_ID` int(11) DEFAULT NULL,
  `WITHOUT_ORDER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SELECT_BEST_PRICE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `VAT_ID` int(11) DEFAULT '0',
  `VAT_INCLUDED` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `CAN_BUY_ZERO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NEGATIVE_AMOUNT_TRACE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PURCHASING_PRICE` decimal(18,2) DEFAULT NULL,
  `PURCHASING_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BARCODE_MULTI` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `QUANTITY_RESERVED` double DEFAULT '0',
  `SUBSCRIBE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WIDTH` double DEFAULT NULL,
  `LENGTH` double DEFAULT NULL,
  `HEIGHT` double DEFAULT NULL,
  `MEASURE` int(11) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `AVAILABLE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUNDLE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_product2group`
--

DROP TABLE IF EXISTS `b_catalog_product2group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_product2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `ACCESS_LENGTH` int(11) NOT NULL,
  `ACCESS_LENGTH_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_P2G_PROD_GROUP` (`PRODUCT_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_product_sets`
--

DROP TABLE IF EXISTS `b_catalog_product_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_product_sets` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE` int(11) NOT NULL,
  `SET_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `OWNER_ID` int(11) NOT NULL,
  `ITEM_ID` int(11) NOT NULL,
  `QUANTITY` double DEFAULT NULL,
  `MEASURE` int(11) DEFAULT NULL,
  `DISCOUNT_PERCENT` double DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `CREATED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_PR_SET_TYPE` (`TYPE`),
  KEY `IX_CAT_PR_SET_OWNER_ID` (`OWNER_ID`),
  KEY `IX_CAT_PR_SET_SET_ID` (`SET_ID`),
  KEY `IX_CAT_PR_SET_ITEM_ID` (`ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_store`
--

DROP TABLE IF EXISTS `b_catalog_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_store` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ADDRESS` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `GPS_N` varchar(15) COLLATE utf8_unicode_ci DEFAULT '0',
  `GPS_S` varchar(15) COLLATE utf8_unicode_ci DEFAULT '0',
  `IMAGE_ID` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOCATION_ID` int(11) DEFAULT NULL,
  `DATE_MODIFY` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_CREATE` datetime DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  `PHONE` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHEDULE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISSUING_CENTER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SHIPPING_CENTER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_store_barcode`
--

DROP TABLE IF EXISTS `b_catalog_store_barcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_store_barcode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `BARCODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STORE_ID` int(11) DEFAULT NULL,
  `ORDER_ID` int(11) DEFAULT NULL,
  `DATE_MODIFY` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_CATALOG_STORE_BARCODE1` (`BARCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_store_docs`
--

DROP TABLE IF EXISTS `b_catalog_store_docs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_store_docs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOC_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONTRACTOR_ID` int(11) DEFAULT NULL,
  `DATE_MODIFY` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_STATUS` datetime DEFAULT NULL,
  `DATE_DOCUMENT` datetime DEFAULT NULL,
  `STATUS_BY` int(11) DEFAULT NULL,
  `TOTAL` double DEFAULT NULL,
  `COMMENTARY` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_store_product`
--

DROP TABLE IF EXISTS `b_catalog_store_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_store_product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `AMOUNT` double NOT NULL DEFAULT '0',
  `STORE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CATALOG_STORE_PRODUCT2` (`PRODUCT_ID`,`STORE_ID`),
  KEY `IX_CATALOG_STORE_PRODUCT1` (`STORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_vat`
--

DROP TABLE IF EXISTS `b_catalog_vat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_vat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `RATE` decimal(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_VAT_ACTIVE` (`ACTIVE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_catalog_viewed_product`
--

DROP TABLE IF EXISTS `b_catalog_viewed_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_catalog_viewed_product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FUSER_ID` int(11) NOT NULL,
  `DATE_VISIT` datetime NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `VIEW_COUNT` int(11) NOT NULL DEFAULT '1',
  `ELEMENT_ID` int(11) NOT NULL DEFAULT '0',
  `RECOMMENDATION` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_V_PR_FUSER_ID` (`FUSER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_checklist`
--

DROP TABLE IF EXISTS `b_checklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_checklist` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_CREATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TESTER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMPANY_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PICTURE` int(11) DEFAULT NULL,
  `TOTAL` int(11) DEFAULT NULL,
  `SUCCESS` int(11) DEFAULT NULL,
  `FAILED` int(11) DEFAULT NULL,
  `PENDING` int(11) DEFAULT NULL,
  `SKIP` int(11) DEFAULT NULL,
  `STATE` longtext COLLATE utf8_unicode_ci,
  `REPORT_COMMENT` text COLLATE utf8_unicode_ci,
  `REPORT` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `EMAIL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHONE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SENDED_TO_BITRIX` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `HIDDEN` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_clouds_file_bucket`
--

DROP TABLE IF EXISTS `b_clouds_file_bucket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_clouds_file_bucket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SORT` int(11) DEFAULT '500',
  `READ_ONLY` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `SERVICE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUCKET` varchar(63) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOCATION` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CNAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_COUNT` int(11) DEFAULT '0',
  `FILE_SIZE` float DEFAULT '0',
  `LAST_FILE_ID` int(11) DEFAULT NULL,
  `PREFIX` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  `FILE_RULES` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_clouds_file_resize`
--

DROP TABLE IF EXISTS `b_clouds_file_resize`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_clouds_file_resize` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ERROR_CODE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `FILE_ID` int(11) DEFAULT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `FROM_PATH` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_PATH` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_file_resize_ts` (`TIMESTAMP_X`),
  KEY `ix_b_file_resize_path` (`TO_PATH`(100)),
  KEY `ix_b_file_resize_file` (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_clouds_file_upload`
--

DROP TABLE IF EXISTS `b_clouds_file_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_clouds_file_upload` (
  `ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FILE_PATH` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `TMP_FILE` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUCKET_ID` int(11) NOT NULL,
  `PART_SIZE` int(11) NOT NULL,
  `PART_NO` int(11) NOT NULL,
  `PART_FAIL_COUNTER` int(11) NOT NULL,
  `NEXT_STEP` text COLLATE utf8_unicode_ci,
  `FILE_SIZE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_cluster_dbnode`
--

DROP TABLE IF EXISTS `b_cluster_dbnode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_cluster_dbnode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) NOT NULL DEFAULT 'Y',
  `GROUP_ID` int(11) DEFAULT NULL,
  `ROLE_ID` varchar(50) DEFAULT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `DB_HOST` varchar(250) DEFAULT NULL,
  `DB_NAME` varchar(250) DEFAULT NULL,
  `DB_LOGIN` varchar(50) DEFAULT NULL,
  `DB_PASSWORD` varchar(50) DEFAULT NULL,
  `MASTER_ID` int(11) DEFAULT NULL,
  `MASTER_HOST` varchar(50) DEFAULT NULL,
  `MASTER_PORT` int(11) DEFAULT NULL,
  `SERVER_ID` varchar(50) DEFAULT NULL,
  `STATUS` varchar(50) DEFAULT NULL,
  `UNIQID` varchar(50) DEFAULT NULL,
  `SELECTABLE` char(1) DEFAULT NULL,
  `WEIGHT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_cluster_group`
--

DROP TABLE IF EXISTS `b_cluster_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_cluster_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_cluster_queue`
--

DROP TABLE IF EXISTS `b_cluster_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_cluster_queue` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUP_ID` int(11) NOT NULL,
  `COMMAND` varchar(50) DEFAULT NULL,
  `PARAM1` varchar(255) DEFAULT NULL,
  `PARAM2` varchar(255) DEFAULT NULL,
  `PARAM3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_cluster_queue_time` (`TIMESTAMP_X`),
  KEY `ix_b_cluster_queue_group` (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_cluster_table`
--

DROP TABLE IF EXISTS `b_cluster_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_cluster_table` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MODULE_ID` varchar(50) DEFAULT NULL,
  `TABLE_NAME` varchar(50) DEFAULT NULL,
  `KEY_COLUMN` varchar(50) DEFAULT NULL,
  `FROM_NODE_ID` int(11) DEFAULT NULL,
  `TO_NODE_ID` int(11) DEFAULT NULL,
  `REC_COUNT` int(11) DEFAULT NULL,
  `LAST_ID` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_cluster_webnode`
--

DROP TABLE IF EXISTS `b_cluster_webnode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_cluster_webnode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) DEFAULT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `HOST` varchar(250) DEFAULT NULL,
  `PORT` int(11) DEFAULT NULL,
  `STATUS_URL` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_component_params`
--

DROP TABLE IF EXISTS `b_component_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_component_params` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `COMPONENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TEMPLATE_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REAL_PATH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SEF_MODE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SEF_FOLDER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `START_CHAR` int(11) NOT NULL,
  `END_CHAR` int(11) NOT NULL,
  `PARAMETERS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_comp_params_name` (`COMPONENT_NAME`),
  KEY `ix_comp_params_path` (`SITE_ID`,`REAL_PATH`),
  KEY `ix_comp_params_sname` (`SITE_ID`,`COMPONENT_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=814 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_counter_data`
--

DROP TABLE IF EXISTS `b_counter_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_counter_data` (
  `ID` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `DATA` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_culture`
--

DROP TABLE IF EXISTS `b_culture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_culture` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATETIME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEEK_START` int(1) DEFAULT '1',
  `CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIRECTION` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_event`
--

DROP TABLE IF EXISTS `b_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_event` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `EVENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE_ID` int(18) DEFAULT NULL,
  `LID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `C_FIELDS` longtext COLLATE utf8_unicode_ci,
  `DATE_INSERT` datetime DEFAULT NULL,
  `DATE_EXEC` datetime DEFAULT NULL,
  `SUCCESS_EXEC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DUPLICATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `ix_success` (`SUCCESS_EXEC`),
  KEY `ix_b_event_date_exec` (`DATE_EXEC`)
) ENGINE=InnoDB AUTO_INCREMENT=35495 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_event_attachment`
--

DROP TABLE IF EXISTS `b_event_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_event_attachment` (
  `EVENT_ID` int(18) NOT NULL,
  `FILE_ID` int(18) NOT NULL,
  PRIMARY KEY (`EVENT_ID`,`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_event_log`
--

DROP TABLE IF EXISTS `b_event_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_event_log` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SEVERITY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `AUDIT_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `REMOTE_ADDR` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_AGENT` text COLLATE utf8_unicode_ci,
  `REQUEST_URI` text COLLATE utf8_unicode_ci,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_ID` int(18) DEFAULT NULL,
  `GUEST_ID` int(18) DEFAULT NULL,
  `DESCRIPTION` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_event_log_time` (`TIMESTAMP_X`)
) ENGINE=InnoDB AUTO_INCREMENT=1926547 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_event_message`
--

DROP TABLE IF EXISTS `b_event_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_event_message` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EVENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EMAIL_FROM` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#EMAIL_FROM#',
  `EMAIL_TO` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#EMAIL_TO#',
  `SUBJECT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE` longtext COLLATE utf8_unicode_ci,
  `BODY_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `BCC` text COLLATE utf8_unicode_ci,
  `REPLY_TO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IN_REPLY_TO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRIORITY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD1_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD1_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD2_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD2_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE_PHP` longtext COLLATE utf8_unicode_ci,
  `SITE_TEMPLATE_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADDITIONAL_FIELD` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_event_message_name` (`EVENT_NAME`(50))
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_event_message_attachment`
--

DROP TABLE IF EXISTS `b_event_message_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_event_message_attachment` (
  `EVENT_MESSAGE_ID` int(18) NOT NULL,
  `FILE_ID` int(18) NOT NULL,
  PRIMARY KEY (`EVENT_MESSAGE_ID`,`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_event_message_site`
--

DROP TABLE IF EXISTS `b_event_message_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_event_message_site` (
  `EVENT_MESSAGE_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`EVENT_MESSAGE_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_event_type`
--

DROP TABLE IF EXISTS `b_event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_event_type` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `EVENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `SORT` int(18) NOT NULL DEFAULT '150',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_1` (`EVENT_NAME`,`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_favorite`
--

DROP TABLE IF EXISTS `b_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_favorite` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `CODE_ID` int(18) DEFAULT NULL,
  `COMMON` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `MENU_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_file`
--

DROP TABLE IF EXISTS `b_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_file` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HEIGHT` int(18) DEFAULT NULL,
  `WIDTH` int(18) DEFAULT NULL,
  `FILE_SIZE` bigint(20) DEFAULT NULL,
  `CONTENT_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'IMAGE',
  `SUBDIR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ORIGINAL_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HANDLER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXTERNAL_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FILE_EXTERNAL_ID` (`EXTERNAL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25766 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_file_search`
--

DROP TABLE IF EXISTS `b_file_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_file_search` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SESS_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `F_PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `B_DIR` int(11) NOT NULL DEFAULT '0',
  `F_SIZE` int(11) NOT NULL DEFAULT '0',
  `F_TIME` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_filters`
--

DROP TABLE IF EXISTS `b_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_filters` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) DEFAULT NULL,
  `FILTER_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FIELDS` text COLLATE utf8_unicode_ci NOT NULL,
  `COMMON` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRESET` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRESET_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(18) DEFAULT NULL,
  `SORT_FIELD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_finder_dest`
--

DROP TABLE IF EXISTS `b_finder_dest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_finder_dest` (
  `USER_ID` int(11) NOT NULL,
  `CODE` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `CODE_USER_ID` int(11) DEFAULT NULL,
  `CODE_TYPE` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONTEXT` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_USE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`USER_ID`,`CODE`,`CONTEXT`),
  KEY `IX_FINDER_DEST` (`CODE_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form`
--

DROP TABLE IF EXISTS `b_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `BUTTON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `C_SORT` int(18) DEFAULT '100',
  `FIRST_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGE_ID` int(18) DEFAULT NULL,
  `USE_CAPTCHA` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'html',
  `FORM_TEMPLATE` text COLLATE utf8_unicode_ci,
  `USE_DEFAULT_TEMPLATE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SHOW_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MAIL_EVENT_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SHOW_RESULT_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRINT_RESULT_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_RESULT_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILTER_RESULT_TEMPLATE` text COLLATE utf8_unicode_ci,
  `TABLE_RESULT_TEMPLATE` text COLLATE utf8_unicode_ci,
  `USE_RESTRICTIONS` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `RESTRICT_USER` int(5) DEFAULT '0',
  `RESTRICT_TIME` int(10) DEFAULT '0',
  `RESTRICT_STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STAT_EVENT1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STAT_EVENT2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STAT_EVENT3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SID` (`SID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_2_group`
--

DROP TABLE IF EXISTS `b_form_2_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_2_group` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `GROUP_ID` int(18) NOT NULL DEFAULT '0',
  `PERMISSION` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_2_mail_template`
--

DROP TABLE IF EXISTS `b_form_2_mail_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_2_mail_template` (
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `MAIL_TEMPLATE_ID` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FORM_ID`,`MAIL_TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_2_site`
--

DROP TABLE IF EXISTS `b_form_2_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_2_site` (
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`FORM_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_answer`
--

DROP TABLE IF EXISTS `b_form_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_answer` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FIELD_ID` int(18) NOT NULL DEFAULT '0',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `FIELD_WIDTH` int(18) DEFAULT NULL,
  `FIELD_HEIGHT` int(18) DEFAULT NULL,
  `FIELD_PARAM` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_FIELD_ID` (`FIELD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_crm`
--

DROP TABLE IF EXISTS `b_form_crm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_crm` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `AUTH_HASH` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_crm_field`
--

DROP TABLE IF EXISTS `b_form_crm_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_crm_field` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `LINK_ID` int(18) NOT NULL DEFAULT '0',
  `FIELD_ID` int(18) DEFAULT '0',
  `FIELD_ALT` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `CRM_FIELD` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `ix_b_form_crm_field_1` (`LINK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_crm_link`
--

DROP TABLE IF EXISTS `b_form_crm_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_crm_link` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `CRM_ID` int(18) NOT NULL DEFAULT '0',
  `LINK_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_form_crm_link_1` (`FORM_ID`,`CRM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_field`
--

DROP TABLE IF EXISTS `b_form_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_field` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TITLE` text COLLATE utf8_unicode_ci,
  `TITLE_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `SID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `ADDITIONAL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `REQUIRED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_FILTER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_RESULTS_TABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_EXCEL_TABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `FIELD_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGE_ID` int(18) DEFAULT NULL,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `FILTER_TITLE` text COLLATE utf8_unicode_ci,
  `RESULTS_TABLE_TITLE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`),
  KEY `IX_SID` (`SID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_field_filter`
--

DROP TABLE IF EXISTS `b_form_field_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_field_filter` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FIELD_ID` int(18) NOT NULL DEFAULT '0',
  `PARAMETER_NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FILTER_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FIELD_ID` (`FIELD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_field_validator`
--

DROP TABLE IF EXISTS `b_form_field_validator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_field_validator` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `FIELD_ID` int(18) NOT NULL DEFAULT '0',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'y',
  `C_SORT` int(18) DEFAULT '100',
  `VALIDATOR_SID` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `PARAMS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`),
  KEY `IX_FIELD_ID` (`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_menu`
--

DROP TABLE IF EXISTS `b_form_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_menu` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `MENU` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_result`
--

DROP TABLE IF EXISTS `b_form_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_result` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `STATUS_ID` int(18) NOT NULL DEFAULT '0',
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `USER_ID` int(18) DEFAULT NULL,
  `USER_AUTH` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `STAT_GUEST_ID` int(18) DEFAULT NULL,
  `STAT_SESSION_ID` int(18) DEFAULT NULL,
  `SENT_TO_CRM` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`),
  KEY `IX_STATUS_ID` (`STATUS_ID`),
  KEY `IX_SENT_TO_CRM` (`SENT_TO_CRM`)
) ENGINE=InnoDB AUTO_INCREMENT=1605 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_result_answer`
--

DROP TABLE IF EXISTS `b_form_result_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_result_answer` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `RESULT_ID` int(18) NOT NULL DEFAULT '0',
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `FIELD_ID` int(18) NOT NULL DEFAULT '0',
  `ANSWER_ID` int(18) DEFAULT NULL,
  `ANSWER_TEXT` text COLLATE utf8_unicode_ci,
  `ANSWER_TEXT_SEARCH` longtext COLLATE utf8_unicode_ci,
  `ANSWER_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANSWER_VALUE_SEARCH` longtext COLLATE utf8_unicode_ci,
  `USER_TEXT` longtext COLLATE utf8_unicode_ci,
  `USER_TEXT_SEARCH` longtext COLLATE utf8_unicode_ci,
  `USER_DATE` datetime DEFAULT NULL,
  `USER_FILE_ID` int(18) DEFAULT NULL,
  `USER_FILE_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_FILE_IS_IMAGE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_FILE_HASH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_FILE_SUFFIX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_FILE_SIZE` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_RESULT_ID` (`RESULT_ID`),
  KEY `IX_FIELD_ID` (`FIELD_ID`),
  KEY `IX_ANSWER_ID` (`ANSWER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4840 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_status`
--

DROP TABLE IF EXISTS `b_form_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_status` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DEFAULT_VALUE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CSS` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'statusgreen',
  `HANDLER_OUT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HANDLER_IN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MAIL_EVENT_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_status_2_group`
--

DROP TABLE IF EXISTS `b_form_status_2_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_status_2_group` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `STATUS_ID` int(18) NOT NULL DEFAULT '0',
  `GROUP_ID` int(18) NOT NULL DEFAULT '0',
  `PERMISSION` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_STATUS_GROUP` (`STATUS_ID`,`GROUP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_form_status_2_mail_template`
--

DROP TABLE IF EXISTS `b_form_status_2_mail_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_form_status_2_mail_template` (
  `STATUS_ID` int(18) NOT NULL DEFAULT '0',
  `MAIL_TEMPLATE_ID` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`STATUS_ID`,`MAIL_TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum`
--

DROP TABLE IF EXISTS `b_forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `FORUM_GROUP_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `SORT` int(10) NOT NULL DEFAULT '150',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_HTML` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_ANCHOR` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_BIU` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_IMG` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_VIDEO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_LIST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_QUOTE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_CODE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_FONT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_SMILES` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_UPLOAD` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_TABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_ALIGN` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_UPLOAD_EXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ALLOW_MOVE_TOPIC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_TOPIC_TITLED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_NL2BR` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_SIGNATURE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `PATH2FORUM_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ASK_GUEST_EMAIL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `USE_CAPTCHA` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `INDEXATION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `DEDUPLICATION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `MODERATION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ORDER_BY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `ORDER_DIRECTION` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DESC',
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ru',
  `TOPICS` int(11) NOT NULL DEFAULT '0',
  `POSTS` int(11) NOT NULL DEFAULT '0',
  `LAST_POSTER_ID` int(11) DEFAULT NULL,
  `LAST_POSTER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_POST_DATE` datetime DEFAULT NULL,
  `LAST_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `POSTS_UNAPPROVED` int(11) DEFAULT '0',
  `ABS_LAST_POSTER_ID` int(11) DEFAULT NULL,
  `ABS_LAST_POSTER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABS_LAST_POST_DATE` datetime DEFAULT NULL,
  `ABS_LAST_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `EVENT1` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'forum',
  `EVENT2` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'message',
  `EVENT3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HTML` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_SORT` (`SORT`),
  KEY `IX_FORUM_ACTIVE` (`ACTIVE`),
  KEY `IX_FORUM_GROUP_ID` (`FORUM_GROUP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum2site`
--

DROP TABLE IF EXISTS `b_forum2site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum2site` (
  `FORUM_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `PATH2FORUM_MESSAGE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`FORUM_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_dictionary`
--

DROP TABLE IF EXISTS `b_forum_dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_dictionary` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_email`
--

DROP TABLE IF EXISTS `b_forum_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_email` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL_FORUM_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `FORUM_ID` int(11) NOT NULL,
  `SOCNET_GROUP_ID` int(11) DEFAULT NULL,
  `MAIL_FILTER_ID` int(11) NOT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `USE_EMAIL` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL_GROUP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBJECT_SUF` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USE_SUBJECT` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL_TEMPLATES_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOT_MEMBER_POST` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_EMAIL_FORUM_SOC` (`FORUM_ID`,`SOCNET_GROUP_ID`),
  KEY `IX_B_FORUM_EMAIL_FILTER_ID` (`MAIL_FILTER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_file`
--

DROP TABLE IF EXISTS `b_forum_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_file` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORUM_ID` int(18) DEFAULT NULL,
  `TOPIC_ID` int(20) DEFAULT NULL,
  `MESSAGE_ID` int(20) DEFAULT NULL,
  `FILE_ID` int(18) NOT NULL,
  `USER_ID` int(18) DEFAULT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `HITS` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_FILE_FILE` (`FILE_ID`),
  KEY `IX_FORUM_FILE_FORUM` (`FORUM_ID`),
  KEY `IX_FORUM_FILE_TOPIC` (`TOPIC_ID`),
  KEY `IX_FORUM_FILE_MESSAGE` (`MESSAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_filter`
--

DROP TABLE IF EXISTS `b_forum_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_filter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DICTIONARY_ID` int(11) DEFAULT NULL,
  `WORDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATTERN` text COLLATE utf8_unicode_ci,
  `REPLACEMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `USE_IT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATTERN_CREATE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_FILTER_2` (`USE_IT`),
  KEY `IX_B_FORUM_FILTER_3` (`PATTERN_CREATE`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_group`
--

DROP TABLE IF EXISTS `b_forum_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SORT` int(11) NOT NULL DEFAULT '150',
  `PARENT_ID` int(11) DEFAULT NULL,
  `LEFT_MARGIN` int(11) DEFAULT NULL,
  `RIGHT_MARGIN` int(11) DEFAULT NULL,
  `DEPTH_LEVEL` int(11) DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_group_lang`
--

DROP TABLE IF EXISTS `b_forum_group_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_group_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORUM_GROUP_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_GROUP` (`FORUM_GROUP_ID`,`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_letter`
--

DROP TABLE IF EXISTS `b_forum_letter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_letter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DICTIONARY_ID` int(11) DEFAULT '0',
  `LETTER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REPLACEMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_message`
--

DROP TABLE IF EXISTS `b_forum_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_message` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FORUM_ID` int(10) NOT NULL,
  `TOPIC_ID` bigint(20) NOT NULL,
  `USE_SMILES` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NEW_TOPIC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `APPROVED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SOURCE_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'WEB',
  `POST_DATE` datetime NOT NULL,
  `POST_MESSAGE` text COLLATE utf8_unicode_ci,
  `POST_MESSAGE_HTML` text COLLATE utf8_unicode_ci,
  `POST_MESSAGE_FILTER` text COLLATE utf8_unicode_ci,
  `POST_MESSAGE_CHECK` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATTACH_IMG` int(11) DEFAULT NULL,
  `PARAM1` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARAM2` int(11) DEFAULT NULL,
  `AUTHOR_ID` int(10) DEFAULT NULL,
  `AUTHOR_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_IP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_REAL_IP` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GUEST_ID` int(10) DEFAULT NULL,
  `EDITOR_ID` int(10) DEFAULT NULL,
  `EDITOR_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDITOR_EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_REASON` text COLLATE utf8_unicode_ci,
  `EDIT_DATE` datetime DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HTML` text COLLATE utf8_unicode_ci,
  `MAIL_HEADER` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_MESSAGE_FORUM` (`FORUM_ID`,`APPROVED`),
  KEY `IX_FORUM_MESSAGE_TOPIC` (`TOPIC_ID`,`APPROVED`,`ID`),
  KEY `IX_FORUM_MESSAGE_AUTHOR` (`AUTHOR_ID`,`APPROVED`,`FORUM_ID`,`ID`),
  KEY `IX_FORUM_MESSAGE_APPROVED` (`APPROVED`),
  KEY `IX_FORUM_MESSAGE_PARAM2` (`PARAM2`),
  KEY `IX_FORUM_MESSAGE_XML_ID` (`XML_ID`),
  KEY `IX_FORUM_MESSAGE_DATE_AUTHOR_ID` (`POST_DATE`,`AUTHOR_ID`),
  KEY `IX_FORUM_MESSAGE_AUTHOR_TOPIC_ID` (`AUTHOR_ID`,`TOPIC_ID`,`ID`),
  KEY `IX_FORUM_MESSAGE_AUTHOR_FORUM_ID` (`AUTHOR_ID`,`FORUM_ID`,`ID`,`APPROVED`,`TOPIC_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1017 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_perms`
--

DROP TABLE IF EXISTS `b_forum_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_perms` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORUM_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `PERMISSION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_PERMS_FORUM` (`FORUM_ID`,`GROUP_ID`),
  KEY `IX_FORUM_PERMS_GROUP` (`GROUP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=831 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_pm_folder`
--

DROP TABLE IF EXISTS `b_forum_pm_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_pm_folder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `SORT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_PM_FOLDER_USER_IST` (`USER_ID`,`ID`,`SORT`,`TITLE`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_points`
--

DROP TABLE IF EXISTS `b_forum_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_points` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MIN_POINTS` int(11) NOT NULL,
  `CODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VOTES` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_P_MP` (`MIN_POINTS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_points2post`
--

DROP TABLE IF EXISTS `b_forum_points2post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_points2post` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MIN_NUM_POSTS` int(11) NOT NULL,
  `POINTS_PER_POST` decimal(18,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_P2P_MNP` (`MIN_NUM_POSTS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_points_lang`
--

DROP TABLE IF EXISTS `b_forum_points_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_points_lang` (
  `POINTS_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`POINTS_ID`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_private_message`
--

DROP TABLE IF EXISTS `b_forum_private_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_private_message` (
  `ID` bigint(10) NOT NULL AUTO_INCREMENT,
  `AUTHOR_ID` int(11) DEFAULT '0',
  `RECIPIENT_ID` int(11) DEFAULT '0',
  `POST_DATE` datetime DEFAULT NULL,
  `POST_SUBJ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POST_MESSAGE` text COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `FOLDER_ID` int(11) NOT NULL,
  `IS_READ` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REQUEST_IS_READ` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `USE_SMILES` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_PM_USER` (`USER_ID`),
  KEY `IX_B_FORUM_PM_AFR` (`AUTHOR_ID`,`FOLDER_ID`,`IS_READ`),
  KEY `IX_B_FORUM_PM_UFP` (`USER_ID`,`FOLDER_ID`,`POST_DATE`),
  KEY `IX_B_FORUM_PM_POST_DATE` (`POST_DATE`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_rank`
--

DROP TABLE IF EXISTS `b_forum_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_rank` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MIN_NUM_POSTS` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_rank_lang`
--

DROP TABLE IF EXISTS `b_forum_rank_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_rank_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RANK_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_RANK` (`RANK_ID`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_smile`
--

DROP TABLE IF EXISTS `b_forum_smile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_smile` (
  `ID` smallint(3) NOT NULL AUTO_INCREMENT,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `TYPING` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLICKABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(10) NOT NULL DEFAULT '150',
  `IMAGE_WIDTH` int(11) NOT NULL DEFAULT '0',
  `IMAGE_HEIGHT` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_smile_lang`
--

DROP TABLE IF EXISTS `b_forum_smile_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_smile_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SMILE_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_SMILE_K` (`SMILE_ID`,`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_stat`
--

DROP TABLE IF EXISTS `b_forum_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_stat` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) DEFAULT NULL,
  `IP_ADDRESS` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHPSESSID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_VISIT` datetime DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORUM_ID` smallint(5) NOT NULL DEFAULT '0',
  `TOPIC_ID` int(10) DEFAULT NULL,
  `SHOW_NAME` varchar(101) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_STAT_SITE_ID` (`SITE_ID`,`LAST_VISIT`),
  KEY `IX_B_FORUM_STAT_TOPIC_ID` (`TOPIC_ID`,`LAST_VISIT`),
  KEY `IX_B_FORUM_STAT_FORUM_ID` (`FORUM_ID`,`LAST_VISIT`),
  KEY `IX_B_FORUM_STAT_PHPSESSID` (`PHPSESSID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_subscribe`
--

DROP TABLE IF EXISTS `b_forum_subscribe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_subscribe` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) NOT NULL,
  `FORUM_ID` int(10) NOT NULL,
  `TOPIC_ID` int(10) DEFAULT NULL,
  `START_DATE` datetime NOT NULL,
  `LAST_SEND` int(10) DEFAULT NULL,
  `NEW_TOPIC_ONLY` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ru',
  `SOCNET_GROUP_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_SUBSCRIBE_USER` (`USER_ID`,`FORUM_ID`,`TOPIC_ID`,`SOCNET_GROUP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_topic`
--

DROP TABLE IF EXISTS `b_forum_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_topic` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FORUM_ID` int(10) NOT NULL,
  `TOPIC_ID` bigint(20) DEFAULT NULL,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TITLE_SEO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAGS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ICON_ID` tinyint(2) DEFAULT NULL,
  `STATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `APPROVED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(10) NOT NULL DEFAULT '150',
  `VIEWS` int(10) NOT NULL DEFAULT '0',
  `USER_START_ID` int(10) DEFAULT NULL,
  `USER_START_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `START_DATE` datetime NOT NULL,
  `POSTS` int(10) NOT NULL DEFAULT '0',
  `LAST_POSTER_ID` int(10) DEFAULT NULL,
  `LAST_POSTER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_POST_DATE` datetime NOT NULL,
  `LAST_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `POSTS_UNAPPROVED` int(11) DEFAULT '0',
  `ABS_LAST_POSTER_ID` int(10) DEFAULT NULL,
  `ABS_LAST_POSTER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABS_LAST_POST_DATE` datetime DEFAULT NULL,
  `ABS_LAST_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HTML` text COLLATE utf8_unicode_ci,
  `SOCNET_GROUP_ID` int(10) DEFAULT NULL,
  `OWNER_ID` int(10) DEFAULT NULL,
  `ICON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_TOPIC_FORUM` (`FORUM_ID`,`APPROVED`),
  KEY `IX_FORUM_TOPIC_APPROVED` (`APPROVED`),
  KEY `IX_FORUM_TOPIC_ABS_L_POST_DATE` (`ABS_LAST_POST_DATE`),
  KEY `IX_FORUM_TOPIC_LAST_POST_DATE` (`LAST_POST_DATE`),
  KEY `IX_FORUM_TOPIC_USER_START_ID` (`USER_START_ID`),
  KEY `IX_FORUM_TOPIC_DATE_USER_START_ID` (`START_DATE`,`USER_START_ID`),
  KEY `IX_FORUM_TOPIC_XML_ID` (`XML_ID`),
  KEY `IX_FORUM_TOPIC_TITLE_SEO` (`FORUM_ID`,`TITLE_SEO`),
  KEY `IX_FORUM_TOPIC_TITLE_SEO2` (`TITLE_SEO`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_user`
--

DROP TABLE IF EXISTS `b_forum_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_user` (
  `ID` bigint(10) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) NOT NULL,
  `ALIAS` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IP_ADDRESS` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AVATAR` int(10) DEFAULT NULL,
  `NUM_POSTS` int(10) DEFAULT '0',
  `INTERESTS` text COLLATE utf8_unicode_ci,
  `LAST_POST` int(10) DEFAULT NULL,
  `ALLOW_POST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `LAST_VISIT` datetime NOT NULL,
  `DATE_REG` date NOT NULL,
  `REAL_IP_ADDRESS` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SIGNATURE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SHOW_NAME` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `RANK_ID` int(11) DEFAULT NULL,
  `POINTS` int(11) NOT NULL DEFAULT '0',
  `HIDE_FROM_ONLINE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SUBSC_GROUP_MESSAGE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SUBSC_GET_MY_MESSAGE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_FORUM_USER_USER6` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6199 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_user_forum`
--

DROP TABLE IF EXISTS `b_forum_user_forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_user_forum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) DEFAULT NULL,
  `FORUM_ID` int(11) DEFAULT NULL,
  `LAST_VISIT` datetime DEFAULT NULL,
  `MAIN_LAST_VISIT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_USER_FORUM_ID1` (`USER_ID`,`FORUM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2936 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_user_points`
--

DROP TABLE IF EXISTS `b_forum_user_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_user_points` (
  `FROM_USER_ID` int(11) NOT NULL,
  `TO_USER_ID` int(11) NOT NULL,
  `POINTS` int(11) NOT NULL DEFAULT '0',
  `DATE_UPDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`FROM_USER_ID`,`TO_USER_ID`),
  KEY `IX_B_FORUM_USER_POINTS_TO_USER` (`TO_USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_forum_user_topic`
--

DROP TABLE IF EXISTS `b_forum_user_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_forum_user_topic` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TOPIC_ID` int(11) NOT NULL DEFAULT '0',
  `USER_ID` int(11) NOT NULL DEFAULT '0',
  `FORUM_ID` int(11) DEFAULT NULL,
  `LAST_VISIT` datetime DEFAULT NULL,
  PRIMARY KEY (`TOPIC_ID`,`USER_ID`),
  KEY `ID` (`ID`),
  KEY `IX_B_FORUM_USER_FORUM_ID2` (`USER_ID`,`FORUM_ID`,`TOPIC_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20477 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_group`
--

DROP TABLE IF EXISTS `b_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_group` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `ANONYMOUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECURITY_POLICY` text COLLATE utf8_unicode_ci,
  `STRING_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_group_collection_task`
--

DROP TABLE IF EXISTS `b_group_collection_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_group_collection_task` (
  `GROUP_ID` int(11) NOT NULL,
  `TASK_ID` int(11) NOT NULL,
  `COLLECTION_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`TASK_ID`,`COLLECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_group_subordinate`
--

DROP TABLE IF EXISTS `b_group_subordinate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_group_subordinate` (
  `ID` int(18) NOT NULL,
  `AR_SUBGROUP_ID` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_group_task`
--

DROP TABLE IF EXISTS `b_group_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_group_task` (
  `GROUP_ID` int(18) NOT NULL,
  `TASK_ID` int(18) NOT NULL,
  `EXTERNAL_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`GROUP_ID`,`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_hlblock_entity`
--

DROP TABLE IF EXISTS `b_hlblock_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_hlblock_entity` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TABLE_NAME` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_hot_keys`
--

DROP TABLE IF EXISTS `b_hot_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_hot_keys` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `KEYS_STRING` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CODE_ID` int(18) NOT NULL,
  `USER_ID` int(18) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_b_hot_keys_co_u` (`CODE_ID`,`USER_ID`),
  KEY `ix_hot_keys_code` (`CODE_ID`),
  KEY `ix_hot_keys_user` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18305 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_hot_keys_code`
--

DROP TABLE IF EXISTS `b_hot_keys_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_hot_keys_code` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CLASS_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMMENTS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITLE_OBJ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_CUSTOM` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `ix_hot_keys_code_cn` (`CLASS_NAME`),
  KEY `ix_hot_keys_code_url` (`URL`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock`
--

DROP TABLE IF EXISTS `b_iblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IBLOCK_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `LIST_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DETAIL_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTION_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PICTURE` int(18) DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `RSS_TTL` int(11) NOT NULL DEFAULT '24',
  `RSS_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `RSS_FILE_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RSS_FILE_LIMIT` int(11) DEFAULT NULL,
  `RSS_FILE_DAYS` int(11) DEFAULT NULL,
  `RSS_YANDEX_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INDEX_ELEMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `INDEX_SECTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `WORKFLOW` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `BIZPROC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SECTION_CHOOSER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIST_MODE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RIGHTS_MODE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTION_PROPERTY` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `LAST_CONV_ELEMENT` int(11) NOT NULL DEFAULT '0',
  `SOCNET_GROUP_ID` int(18) DEFAULT NULL,
  `EDIT_FILE_BEFORE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_FILE_AFTER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTIONS_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTION_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELEMENTS_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELEMENT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROPERTY_INDEX` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CANONICAL_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock` (`IBLOCK_TYPE_ID`,`LID`,`ACTIVE`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_cache`
--

DROP TABLE IF EXISTS `b_iblock_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_cache` (
  `CACHE_KEY` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `CACHE` longtext COLLATE utf8_unicode_ci NOT NULL,
  `CACHE_DATE` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`CACHE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element`
--

DROP TABLE IF EXISTS `b_iblock_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `IBLOCK_ID` int(11) NOT NULL DEFAULT '0',
  `IBLOCK_SECTION_ID` int(11) DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PREVIEW_PICTURE` int(18) DEFAULT NULL,
  `PREVIEW_TEXT` text COLLATE utf8_unicode_ci,
  `PREVIEW_TEXT_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `DETAIL_PICTURE` int(18) DEFAULT NULL,
  `DETAIL_TEXT` longtext COLLATE utf8_unicode_ci,
  `DETAIL_TEXT_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `SEARCHABLE_CONTENT` text COLLATE utf8_unicode_ci,
  `WF_STATUS_ID` int(18) DEFAULT '1',
  `WF_PARENT_ELEMENT_ID` int(11) DEFAULT NULL,
  `WF_NEW` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WF_LOCKED_BY` int(18) DEFAULT NULL,
  `WF_DATE_LOCK` datetime DEFAULT NULL,
  `WF_COMMENTS` text COLLATE utf8_unicode_ci,
  `IN_SECTIONS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAGS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WF_LAST_HISTORY_ID` int(11) DEFAULT NULL,
  `SHOW_COUNTER` int(18) DEFAULT NULL,
  `SHOW_COUNTER_START` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_element_1` (`IBLOCK_ID`,`IBLOCK_SECTION_ID`),
  KEY `ix_iblock_element_4` (`IBLOCK_ID`,`XML_ID`,`WF_PARENT_ELEMENT_ID`),
  KEY `ix_iblock_element_3` (`WF_PARENT_ELEMENT_ID`),
  KEY `ix_iblock_element_code` (`IBLOCK_ID`,`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=3490 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_iprop`
--

DROP TABLE IF EXISTS `b_iblock_element_iprop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_iprop` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `ELEMENT_ID` int(11) NOT NULL,
  `IPROP_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ELEMENT_ID`,`IPROP_ID`),
  KEY `ix_b_iblock_element_iprop_0` (`IPROP_ID`),
  KEY `ix_b_iblock_element_iprop_1` (`IBLOCK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_lock`
--

DROP TABLE IF EXISTS `b_iblock_element_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_lock` (
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `DATE_LOCK` datetime DEFAULT NULL,
  `LOCKED_BY` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_prop_m3`
--

DROP TABLE IF EXISTS `b_iblock_element_prop_m3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_prop_m3` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `IBLOCK_PROPERTY_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `VALUE_ENUM` int(11) DEFAULT NULL,
  `VALUE_NUM` decimal(18,4) DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_elem_prop_m3_1` (`IBLOCK_ELEMENT_ID`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_elem_prop_m3_2` (`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_elem_prop_m3_3` (`VALUE_ENUM`,`IBLOCK_PROPERTY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=729 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_prop_m4`
--

DROP TABLE IF EXISTS `b_iblock_element_prop_m4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_prop_m4` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `IBLOCK_PROPERTY_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `VALUE_ENUM` int(11) DEFAULT NULL,
  `VALUE_NUM` decimal(18,4) DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_elem_prop_m4_1` (`IBLOCK_ELEMENT_ID`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_elem_prop_m4_2` (`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_elem_prop_m4_3` (`VALUE_ENUM`,`IBLOCK_PROPERTY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_prop_m7`
--

DROP TABLE IF EXISTS `b_iblock_element_prop_m7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_prop_m7` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `IBLOCK_PROPERTY_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `VALUE_ENUM` int(11) DEFAULT NULL,
  `VALUE_NUM` decimal(18,4) DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_elem_prop_m7_1` (`IBLOCK_ELEMENT_ID`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_elem_prop_m7_2` (`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_elem_prop_m7_3` (`VALUE_ENUM`,`IBLOCK_PROPERTY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5253 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_prop_m8`
--

DROP TABLE IF EXISTS `b_iblock_element_prop_m8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_prop_m8` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `IBLOCK_PROPERTY_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `VALUE_ENUM` int(11) DEFAULT NULL,
  `VALUE_NUM` decimal(18,4) DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_elem_prop_m8_1` (`IBLOCK_ELEMENT_ID`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_elem_prop_m8_2` (`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_elem_prop_m8_3` (`VALUE_ENUM`,`IBLOCK_PROPERTY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11544 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_prop_s3`
--

DROP TABLE IF EXISTS `b_iblock_element_prop_s3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_prop_s3` (
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `PROPERTY_3` int(11) DEFAULT NULL,
  `PROPERTY_20` longtext COLLATE utf8_unicode_ci,
  `DESCRIPTION_20` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROPERTY_21` text COLLATE utf8_unicode_ci,
  `PROPERTY_22` int(11) DEFAULT NULL,
  `DESCRIPTION_22` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_prop_s4`
--

DROP TABLE IF EXISTS `b_iblock_element_prop_s4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_prop_s4` (
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `PROPERTY_4` int(11) DEFAULT NULL,
  `PROPERTY_5` text COLLATE utf8_unicode_ci,
  `PROPERTY_6` text COLLATE utf8_unicode_ci,
  `PROPERTY_7` longtext COLLATE utf8_unicode_ci,
  `PROPERTY_8` text COLLATE utf8_unicode_ci,
  `PROPERTY_9` text COLLATE utf8_unicode_ci,
  `PROPERTY_10` text COLLATE utf8_unicode_ci,
  `PROPERTY_11` text COLLATE utf8_unicode_ci,
  `PROPERTY_12` decimal(18,4) DEFAULT NULL,
  `PROPERTY_13` decimal(18,4) DEFAULT NULL,
  `PROPERTY_14` decimal(18,4) DEFAULT NULL,
  `PROPERTY_15` decimal(18,4) DEFAULT NULL,
  `PROPERTY_16` text COLLATE utf8_unicode_ci,
  `PROPERTY_19` longtext COLLATE utf8_unicode_ci,
  `PROPERTY_25` text COLLATE utf8_unicode_ci,
  `PROPERTY_26` text COLLATE utf8_unicode_ci,
  `PROPERTY_27` text COLLATE utf8_unicode_ci,
  `PROPERTY_43` text COLLATE utf8_unicode_ci,
  `PROPERTY_44` text COLLATE utf8_unicode_ci,
  `PROPERTY_45` text COLLATE utf8_unicode_ci,
  `PROPERTY_46` text COLLATE utf8_unicode_ci,
  `PROPERTY_47` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_prop_s7`
--

DROP TABLE IF EXISTS `b_iblock_element_prop_s7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_prop_s7` (
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `PROPERTY_23` text COLLATE utf8_unicode_ci,
  `PROPERTY_24` longtext COLLATE utf8_unicode_ci,
  `PROPERTY_48` int(11) DEFAULT NULL,
  `PROPERTY_71` text COLLATE utf8_unicode_ci,
  `PROPERTY_73` text COLLATE utf8_unicode_ci,
  `PROPERTY_74` decimal(18,4) DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_prop_s8`
--

DROP TABLE IF EXISTS `b_iblock_element_prop_s8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_prop_s8` (
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `PROPERTY_28` text COLLATE utf8_unicode_ci,
  `PROPERTY_29` text COLLATE utf8_unicode_ci,
  `PROPERTY_30` text COLLATE utf8_unicode_ci,
  `PROPERTY_31` text COLLATE utf8_unicode_ci,
  `PROPERTY_32` decimal(18,4) DEFAULT NULL,
  `PROPERTY_33` longtext COLLATE utf8_unicode_ci,
  `PROPERTY_34` text COLLATE utf8_unicode_ci,
  `PROPERTY_35` int(11) DEFAULT NULL,
  `PROPERTY_36` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_33` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROPERTY_41` text COLLATE utf8_unicode_ci,
  `PROPERTY_42` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_property`
--

DROP TABLE IF EXISTS `b_iblock_element_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_property` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_PROPERTY_ID` int(11) NOT NULL,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `VALUE_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `VALUE_ENUM` int(11) DEFAULT NULL,
  `VALUE_NUM` decimal(18,4) DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_element_property_1` (`IBLOCK_ELEMENT_ID`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_element_property_2` (`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_element_prop_enum` (`VALUE_ENUM`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_element_prop_num` (`VALUE_NUM`,`IBLOCK_PROPERTY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2200 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_element_right`
--

DROP TABLE IF EXISTS `b_iblock_element_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_element_right` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `ELEMENT_ID` int(11) NOT NULL,
  `RIGHT_ID` int(11) NOT NULL,
  `IS_INHERITED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RIGHT_ID`,`ELEMENT_ID`,`SECTION_ID`),
  KEY `ix_b_iblock_element_right_1` (`ELEMENT_ID`,`IBLOCK_ID`),
  KEY `ix_b_iblock_element_right_2` (`IBLOCK_ID`,`RIGHT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_fields`
--

DROP TABLE IF EXISTS `b_iblock_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_fields` (
  `IBLOCK_ID` int(18) NOT NULL,
  `FIELD_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `IS_REQUIRED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEFAULT_VALUE` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`IBLOCK_ID`,`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_group`
--

DROP TABLE IF EXISTS `b_iblock_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_group` (
  `IBLOCK_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `PERMISSION` char(1) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `ux_iblock_group_1` (`IBLOCK_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_iblock_iprop`
--

DROP TABLE IF EXISTS `b_iblock_iblock_iprop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_iblock_iprop` (
  `IBLOCK_ID` int(11) NOT NULL,
  `IPROP_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`IPROP_ID`),
  KEY `ix_b_iblock_iblock_iprop_0` (`IPROP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_iproperty`
--

DROP TABLE IF EXISTS `b_iblock_iproperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_iproperty` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `TEMPLATE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_iblock_iprop_0` (`IBLOCK_ID`,`ENTITY_TYPE`,`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_messages`
--

DROP TABLE IF EXISTS `b_iblock_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_messages` (
  `IBLOCK_ID` int(18) NOT NULL,
  `MESSAGE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`MESSAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_offers_tmp`
--

DROP TABLE IF EXISTS `b_iblock_offers_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_offers_tmp` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PRODUCT_IBLOCK_ID` int(11) unsigned NOT NULL,
  `OFFERS_IBLOCK_ID` int(11) unsigned NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_property`
--

DROP TABLE IF EXISTS `b_iblock_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_property` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IBLOCK_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEFAULT_VALUE` text COLLATE utf8_unicode_ci,
  `PROPERTY_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `ROW_COUNT` int(11) NOT NULL DEFAULT '1',
  `COL_COUNT` int(11) NOT NULL DEFAULT '30',
  `LIST_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'L',
  `MULTIPLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `XML_ID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_TYPE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MULTIPLE_CNT` int(11) DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINK_IBLOCK_ID` int(18) DEFAULT NULL,
  `WITH_DESCRIPTION` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEARCHABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `FILTRABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_REQUIRED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `USER_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_TYPE_SETTINGS` text COLLATE utf8_unicode_ci,
  `HINT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_property_1` (`IBLOCK_ID`),
  KEY `ix_iblock_property_3` (`LINK_IBLOCK_ID`),
  KEY `ix_iblock_property_2` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_property_enum`
--

DROP TABLE IF EXISTS `b_iblock_property_enum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_property_enum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROPERTY_ID` int(11) NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `XML_ID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_iblock_property_enum` (`PROPERTY_ID`,`XML_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_right`
--

DROP TABLE IF EXISTS `b_iblock_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_right` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `GROUP_CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `DO_INHERIT` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `TASK_ID` int(11) NOT NULL,
  `OP_SREAD` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `OP_EREAD` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `XML_ID` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_iblock_right_iblock_id` (`IBLOCK_ID`,`ENTITY_TYPE`,`ENTITY_ID`),
  KEY `ix_b_iblock_right_group_code` (`GROUP_CODE`,`IBLOCK_ID`),
  KEY `ix_b_iblock_right_entity` (`ENTITY_ID`,`ENTITY_TYPE`),
  KEY `ix_b_iblock_right_op_eread` (`ID`,`OP_EREAD`,`GROUP_CODE`),
  KEY `ix_b_iblock_right_op_sread` (`ID`,`OP_SREAD`,`GROUP_CODE`),
  KEY `ix_b_iblock_right_task_id` (`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_rss`
--

DROP TABLE IF EXISTS `b_iblock_rss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_rss` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `NODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NODE_VALUE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_section`
--

DROP TABLE IF EXISTS `b_iblock_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `IBLOCK_ID` int(11) NOT NULL,
  `IBLOCK_SECTION_ID` int(11) DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `GLOBAL_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PICTURE` int(18) DEFAULT NULL,
  `LEFT_MARGIN` int(18) DEFAULT NULL,
  `RIGHT_MARGIN` int(18) DEFAULT NULL,
  `DEPTH_LEVEL` int(18) DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `SEARCHABLE_CONTENT` text COLLATE utf8_unicode_ci,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DETAIL_PICTURE` int(18) DEFAULT NULL,
  `SOCNET_GROUP_ID` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_section_1` (`IBLOCK_ID`,`IBLOCK_SECTION_ID`),
  KEY `ix_iblock_section_depth_level` (`IBLOCK_ID`,`DEPTH_LEVEL`),
  KEY `ix_iblock_section_left_margin` (`IBLOCK_ID`,`LEFT_MARGIN`,`RIGHT_MARGIN`),
  KEY `ix_iblock_section_right_margin` (`IBLOCK_ID`,`RIGHT_MARGIN`,`LEFT_MARGIN`),
  KEY `ix_iblock_section_code` (`IBLOCK_ID`,`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_section_element`
--

DROP TABLE IF EXISTS `b_iblock_section_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_section_element` (
  `IBLOCK_SECTION_ID` int(11) NOT NULL,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `ADDITIONAL_PROPERTY_ID` int(18) DEFAULT NULL,
  UNIQUE KEY `ux_iblock_section_element` (`IBLOCK_SECTION_ID`,`IBLOCK_ELEMENT_ID`,`ADDITIONAL_PROPERTY_ID`),
  KEY `UX_IBLOCK_SECTION_ELEMENT2` (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_section_iprop`
--

DROP TABLE IF EXISTS `b_iblock_section_iprop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_section_iprop` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `IPROP_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`SECTION_ID`,`IPROP_ID`),
  KEY `ix_b_iblock_section_iprop_0` (`IPROP_ID`),
  KEY `ix_b_iblock_section_iprop_1` (`IBLOCK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_section_property`
--

DROP TABLE IF EXISTS `b_iblock_section_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_section_property` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `PROPERTY_ID` int(11) NOT NULL,
  `SMART_FILTER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISPLAY_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISPLAY_EXPANDED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILTER_HINT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`SECTION_ID`,`PROPERTY_ID`),
  KEY `ix_b_iblock_section_property_1` (`PROPERTY_ID`),
  KEY `ix_b_iblock_section_property_2` (`SECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_section_right`
--

DROP TABLE IF EXISTS `b_iblock_section_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_section_right` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `RIGHT_ID` int(11) NOT NULL,
  `IS_INHERITED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RIGHT_ID`,`SECTION_ID`),
  KEY `ix_b_iblock_section_right_1` (`SECTION_ID`,`IBLOCK_ID`),
  KEY `ix_b_iblock_section_right_2` (`IBLOCK_ID`,`RIGHT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_sequence`
--

DROP TABLE IF EXISTS `b_iblock_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_sequence` (
  `IBLOCK_ID` int(18) NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEQ_VALUE` int(11) DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_site`
--

DROP TABLE IF EXISTS `b_iblock_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_site` (
  `IBLOCK_ID` int(18) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_type`
--

DROP TABLE IF EXISTS `b_iblock_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_type` (
  `ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SECTIONS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EDIT_FILE_BEFORE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_FILE_AFTER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IN_RSS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(18) NOT NULL DEFAULT '500',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_iblock_type_lang`
--

DROP TABLE IF EXISTS `b_iblock_type_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_iblock_type_lang` (
  `IBLOCK_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SECTION_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELEMENT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_im_status`
--

DROP TABLE IF EXISTS `b_im_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_im_status` (
  `USER_ID` int(18) NOT NULL,
  `STATUS` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'online',
  `STATUS_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDLE` datetime DEFAULT NULL,
  `DESKTOP_LAST_DATE` datetime DEFAULT NULL,
  `MOBILE_LAST_DATE` datetime DEFAULT NULL,
  `EVENT_ID` int(18) DEFAULT NULL,
  `EVENT_UNTIL_DATE` datetime DEFAULT NULL,
  `COLOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  KEY `IX_IM_STATUS_EUD` (`EVENT_UNTIL_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_lang`
--

DROP TABLE IF EXISTS `b_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_lang` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(18) NOT NULL DEFAULT '100',
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DIR` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_DATE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATETIME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEEK_START` int(11) DEFAULT NULL,
  `CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `DOC_ROOT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOMAIN_LIMITED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SERVER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SITE_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CULTURE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_lang_domain`
--

DROP TABLE IF EXISTS `b_lang_domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_lang_domain` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `DOMAIN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`LID`,`DOMAIN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_language`
--

DROP TABLE IF EXISTS `b_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_language` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_DATE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATETIME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEEK_START` int(11) DEFAULT NULL,
  `CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIRECTION` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CULTURE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_list_rubric`
--

DROP TABLE IF EXISTS `b_list_rubric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_list_rubric` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `AUTO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DAYS_OF_MONTH` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DAYS_OF_WEEK` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMES_OF_DAY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEMPLATE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_EXECUTED` datetime DEFAULT NULL,
  `VISIBLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `FROM_FIELD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_medialib_collection`
--

DROP TABLE IF EXISTS `b_medialib_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_medialib_collection` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `DATE_UPDATE` datetime NOT NULL,
  `OWNER_ID` int(11) DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KEYWORDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ITEMS_COUNT` int(11) DEFAULT NULL,
  `ML_TYPE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_medialib_collection_item`
--

DROP TABLE IF EXISTS `b_medialib_collection_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_medialib_collection_item` (
  `COLLECTION_ID` int(11) NOT NULL,
  `ITEM_ID` int(11) NOT NULL,
  PRIMARY KEY (`ITEM_ID`,`COLLECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_medialib_item`
--

DROP TABLE IF EXISTS `b_medialib_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_medialib_item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_TYPE` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `SOURCE_ID` int(11) NOT NULL,
  `KEYWORDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEARCHABLE_CONTENT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_medialib_type`
--

DROP TABLE IF EXISTS `b_medialib_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_medialib_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EXT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SYSTEM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_module`
--

DROP TABLE IF EXISTS `b_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_module` (
  `ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_ACTIVE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_module_group`
--

DROP TABLE IF EXISTS `b_module_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_module_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `G_ACCESS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_GROUP_MODULE` (`MODULE_ID`,`GROUP_ID`,`SITE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_module_to_module`
--

DROP TABLE IF EXISTS `b_module_to_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_module_to_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SORT` int(18) NOT NULL DEFAULT '100',
  `FROM_MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TO_MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `TO_PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_CLASS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_METHOD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_METHOD_ARG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_module_to_module` (`FROM_MODULE_ID`(20),`MESSAGE_ID`(20),`TO_MODULE_ID`(20),`TO_CLASS`(20),`TO_METHOD`(20))
) ENGINE=InnoDB AUTO_INCREMENT=824 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_operation`
--

DROP TABLE IF EXISTS `b_operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_operation` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BINDING` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'module',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_option`
--

DROP TABLE IF EXISTS `b_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_option` (
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `ix_option` (`MODULE_ID`,`NAME`,`SITE_ID`),
  KEY `ix_option_name` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_cache`
--

DROP TABLE IF EXISTS `b_perf_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_cache` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `COMPONENT_ID` int(18) DEFAULT NULL,
  `NN` int(18) DEFAULT NULL,
  `CACHE_SIZE` float DEFAULT NULL,
  `OP_MODE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MODULE_NAME` text COLLATE utf8_unicode_ci,
  `COMPONENT_NAME` text COLLATE utf8_unicode_ci,
  `BASE_DIR` text COLLATE utf8_unicode_ci,
  `INIT_DIR` text COLLATE utf8_unicode_ci,
  `FILE_NAME` text COLLATE utf8_unicode_ci,
  `FILE_PATH` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_PERF_CACHE_0` (`HIT_ID`,`NN`),
  KEY `IX_B_PERF_CACHE_1` (`COMPONENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_cluster`
--

DROP TABLE IF EXISTS `b_perf_cluster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_cluster` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `THREADS` int(11) DEFAULT NULL,
  `HITS` int(11) DEFAULT NULL,
  `ERRORS` int(11) DEFAULT NULL,
  `PAGES_PER_SECOND` float DEFAULT NULL,
  `PAGE_EXEC_TIME` float DEFAULT NULL,
  `PAGE_RESP_TIME` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_component`
--

DROP TABLE IF EXISTS `b_perf_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_component` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `NN` int(18) DEFAULT NULL,
  `CACHE_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CACHE_SIZE` int(11) DEFAULT NULL,
  `CACHE_COUNT_R` int(11) DEFAULT NULL,
  `CACHE_COUNT_W` int(11) DEFAULT NULL,
  `CACHE_COUNT_C` int(11) DEFAULT NULL,
  `COMPONENT_TIME` float DEFAULT NULL,
  `QUERIES` int(11) DEFAULT NULL,
  `QUERIES_TIME` float DEFAULT NULL,
  `COMPONENT_NAME` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_PERF_COMPONENT_0` (`HIT_ID`,`NN`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_error`
--

DROP TABLE IF EXISTS `b_perf_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_error` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `ERRNO` int(18) DEFAULT NULL,
  `ERRSTR` text COLLATE utf8_unicode_ci,
  `ERRFILE` text COLLATE utf8_unicode_ci,
  `ERRLINE` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_PERF_ERROR_0` (`HIT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_history`
--

DROP TABLE IF EXISTS `b_perf_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_history` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TOTAL_MARK` float DEFAULT NULL,
  `ACCELERATOR_ENABLED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_hit`
--

DROP TABLE IF EXISTS `b_perf_hit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_hit` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_HIT` datetime DEFAULT NULL,
  `IS_ADMIN` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REQUEST_METHOD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERVER_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERVER_PORT` int(11) DEFAULT NULL,
  `SCRIPT_NAME` text COLLATE utf8_unicode_ci,
  `REQUEST_URI` text COLLATE utf8_unicode_ci,
  `INCLUDED_FILES` int(11) DEFAULT NULL,
  `MEMORY_PEAK_USAGE` int(11) DEFAULT NULL,
  `CACHE_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CACHE_SIZE` int(11) DEFAULT NULL,
  `CACHE_COUNT_R` int(11) DEFAULT NULL,
  `CACHE_COUNT_W` int(11) DEFAULT NULL,
  `CACHE_COUNT_C` int(11) DEFAULT NULL,
  `QUERIES` int(11) DEFAULT NULL,
  `QUERIES_TIME` float DEFAULT NULL,
  `COMPONENTS` int(11) DEFAULT NULL,
  `COMPONENTS_TIME` float DEFAULT NULL,
  `SQL_LOG` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAGE_TIME` float DEFAULT NULL,
  `PROLOG_TIME` float DEFAULT NULL,
  `PROLOG_BEFORE_TIME` float DEFAULT NULL,
  `AGENTS_TIME` float DEFAULT NULL,
  `PROLOG_AFTER_TIME` float DEFAULT NULL,
  `WORK_AREA_TIME` float DEFAULT NULL,
  `EPILOG_TIME` float DEFAULT NULL,
  `EPILOG_BEFORE_TIME` float DEFAULT NULL,
  `EVENTS_TIME` float DEFAULT NULL,
  `EPILOG_AFTER_TIME` float DEFAULT NULL,
  `MENU_RECALC` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_PERF_HIT_0` (`DATE_HIT`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_index_ban`
--

DROP TABLE IF EXISTS `b_perf_index_ban`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_index_ban` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BAN_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAMES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_index_complete`
--

DROP TABLE IF EXISTS `b_perf_index_complete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_index_complete` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BANNED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAMES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INDEX_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_perf_index_complete_0` (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_index_suggest`
--

DROP TABLE IF EXISTS `b_perf_index_suggest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_index_suggest` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SQL_MD5` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SQL_COUNT` int(11) DEFAULT NULL,
  `SQL_TIME` float DEFAULT NULL,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_ALIAS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAMES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SQL_TEXT` text COLLATE utf8_unicode_ci,
  `SQL_EXPLAIN` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_perf_index_suggest_0` (`SQL_MD5`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_index_suggest_sql`
--

DROP TABLE IF EXISTS `b_perf_index_suggest_sql`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_index_suggest_sql` (
  `SUGGEST_ID` int(11) NOT NULL DEFAULT '0',
  `SQL_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SUGGEST_ID`,`SQL_ID`),
  KEY `ix_b_perf_index_suggest_sql_0` (`SQL_ID`,`SUGGEST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_sql`
--

DROP TABLE IF EXISTS `b_perf_sql`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_sql` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `COMPONENT_ID` int(18) DEFAULT NULL,
  `NN` int(18) DEFAULT NULL,
  `QUERY_TIME` float DEFAULT NULL,
  `NODE_ID` int(18) DEFAULT NULL,
  `MODULE_NAME` text COLLATE utf8_unicode_ci,
  `COMPONENT_NAME` text COLLATE utf8_unicode_ci,
  `SQL_TEXT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_PERF_SQL_0` (`HIT_ID`,`NN`),
  KEY `IX_B_PERF_SQL_1` (`COMPONENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_sql_backtrace`
--

DROP TABLE IF EXISTS `b_perf_sql_backtrace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_sql_backtrace` (
  `SQL_ID` int(18) NOT NULL DEFAULT '0',
  `NN` int(18) NOT NULL DEFAULT '0',
  `FILE_NAME` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINE_NO` int(18) DEFAULT NULL,
  `CLASS_NAME` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FUNCTION_NAME` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`SQL_ID`,`NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_tab_column_stat`
--

DROP TABLE IF EXISTS `b_perf_tab_column_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_tab_column_stat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_ROWS` float DEFAULT NULL,
  `COLUMN_ROWS` float DEFAULT NULL,
  `VALUE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_perf_tab_column_stat` (`TABLE_NAME`,`COLUMN_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_tab_stat`
--

DROP TABLE IF EXISTS `b_perf_tab_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_tab_stat` (
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TABLE_SIZE` float DEFAULT NULL,
  `TABLE_ROWS` float DEFAULT NULL,
  PRIMARY KEY (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_perf_test`
--

DROP TABLE IF EXISTS `b_perf_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_perf_test` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `REFERENCE_ID` int(18) DEFAULT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_PERF_TEST_0` (`REFERENCE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=401 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_posting`
--

DROP TABLE IF EXISTS `b_posting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_posting` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `VERSION` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_SENT` datetime DEFAULT NULL,
  `SENT_BCC` mediumtext COLLATE utf8_unicode_ci,
  `FROM_FIELD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TO_FIELD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BCC_FIELD` mediumtext COLLATE utf8_unicode_ci,
  `EMAIL_FILTER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBJECT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BODY_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `BODY` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `DIRECT_SEND` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CHARSET` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSG_CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBSCR_FORMAT` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERROR_EMAIL` mediumtext COLLATE utf8_unicode_ci,
  `AUTO_SEND_TIME` datetime DEFAULT NULL,
  `BCC_TO_SEND` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_posting_email`
--

DROP TABLE IF EXISTS `b_posting_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_posting_email` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `POSTING_ID` int(11) NOT NULL,
  `STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SUBSCRIPTION_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_posting_email_status` (`POSTING_ID`,`STATUS`),
  KEY `ix_posting_email_email` (`POSTING_ID`,`EMAIL`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_posting_file`
--

DROP TABLE IF EXISTS `b_posting_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_posting_file` (
  `POSTING_ID` int(11) NOT NULL,
  `FILE_ID` int(11) NOT NULL,
  UNIQUE KEY `UK_POSTING_POSTING_FILE` (`POSTING_ID`,`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_posting_group`
--

DROP TABLE IF EXISTS `b_posting_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_posting_group` (
  `POSTING_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  UNIQUE KEY `UK_POSTING_POSTING_GROUP` (`POSTING_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_posting_rubric`
--

DROP TABLE IF EXISTS `b_posting_rubric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_posting_rubric` (
  `POSTING_ID` int(11) NOT NULL,
  `LIST_RUBRIC_ID` int(11) NOT NULL,
  UNIQUE KEY `UK_POSTING_POSTING_RUBRIC` (`POSTING_ID`,`LIST_RUBRIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating`
--

DROP TABLE IF EXISTS `b_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CALCULATION_METHOD` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SUM',
  `CREATED` datetime DEFAULT NULL,
  `LAST_MODIFIED` datetime DEFAULT NULL,
  `LAST_CALCULATED` datetime DEFAULT NULL,
  `POSITION` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `AUTHORITY` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `CALCULATED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CONFIGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_component`
--

DROP TABLE IF EXISTS `b_rating_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_component` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ENTITY_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `RATING_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `COMPLEX_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CALC_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EXCEPTION_METHOD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_MODIFIED` datetime DEFAULT NULL,
  `LAST_CALCULATED` datetime DEFAULT NULL,
  `NEXT_CALCULATION` datetime DEFAULT NULL,
  `REFRESH_INTERVAL` int(11) NOT NULL,
  `CONFIG` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_RATING_ID_1` (`RATING_ID`,`ACTIVE`,`NEXT_CALCULATION`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_component_results`
--

DROP TABLE IF EXISTS `b_rating_component_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_component_results` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `RATING_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `COMPLEX_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CURRENT_VALUE` decimal(18,4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ENTITY_TYPE_ID` (`ENTITY_TYPE_ID`),
  KEY `IX_COMPLEX_NAME` (`COMPLEX_NAME`),
  KEY `IX_RATING_ID_2` (`RATING_ID`,`COMPLEX_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_prepare`
--

DROP TABLE IF EXISTS `b_rating_prepare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_prepare` (
  `ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_results`
--

DROP TABLE IF EXISTS `b_rating_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_results` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `CURRENT_VALUE` decimal(18,4) DEFAULT NULL,
  `PREVIOUS_VALUE` decimal(18,4) DEFAULT NULL,
  `CURRENT_POSITION` int(11) DEFAULT '0',
  `PREVIOUS_POSITION` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_RATING_3` (`RATING_ID`,`ENTITY_TYPE_ID`,`ENTITY_ID`),
  KEY `IX_RATING_4` (`RATING_ID`,`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_rule`
--

DROP TABLE IF EXISTS `b_rating_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_rule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_MODULE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDITION_CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_CONFIG` text COLLATE utf8_unicode_ci NOT NULL,
  `ACTION_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ACTION_CONFIG` text COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVATE_CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVATE_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEACTIVATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DEACTIVATE_CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEACTIVATE_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED` datetime DEFAULT NULL,
  `LAST_MODIFIED` datetime DEFAULT NULL,
  `LAST_APPLIED` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_rule_vetting`
--

DROP TABLE IF EXISTS `b_rating_rule_vetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_rule_vetting` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RULE_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `ACTIVATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `APPLIED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `RULE_ID` (`RULE_ID`,`ENTITY_TYPE_ID`,`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_user`
--

DROP TABLE IF EXISTS `b_rating_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `BONUS` decimal(18,4) DEFAULT '0.0000',
  `VOTE_WEIGHT` decimal(18,4) DEFAULT '0.0000',
  `VOTE_COUNT` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RATING_ID` (`RATING_ID`,`ENTITY_ID`),
  KEY `IX_B_RAT_USER_2` (`ENTITY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=32575 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_vote`
--

DROP TABLE IF EXISTS `b_rating_vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_vote` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_VOTING_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `OWNER_ID` int(11) NOT NULL,
  `VALUE` decimal(18,4) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED` datetime NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `USER_IP` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_RAT_VOTE_ID` (`RATING_VOTING_ID`,`USER_ID`),
  KEY `IX_RAT_VOTE_ID_2` (`ENTITY_TYPE_ID`,`ENTITY_ID`,`USER_ID`),
  KEY `IX_RAT_VOTE_ID_3` (`OWNER_ID`,`CREATED`),
  KEY `IX_RAT_VOTE_ID_4` (`USER_ID`),
  KEY `IX_RAT_VOTE_ID_5` (`CREATED`,`VALUE`),
  KEY `IX_RAT_VOTE_ID_6` (`ACTIVE`),
  KEY `IX_RAT_VOTE_ID_7` (`RATING_VOTING_ID`,`CREATED`),
  KEY `IX_RAT_VOTE_ID_8` (`ENTITY_TYPE_ID`,`CREATED`),
  KEY `IX_RAT_VOTE_ID_9` (`CREATED`,`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_vote_group`
--

DROP TABLE IF EXISTS `b_rating_vote_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_vote_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) NOT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `RATING_ID` (`GROUP_ID`,`TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_voting`
--

DROP TABLE IF EXISTS `b_rating_voting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_voting` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `OWNER_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED` datetime DEFAULT NULL,
  `LAST_CALCULATED` datetime DEFAULT NULL,
  `TOTAL_VALUE` decimal(18,4) NOT NULL,
  `TOTAL_VOTES` int(11) NOT NULL,
  `TOTAL_POSITIVE_VOTES` int(11) NOT NULL,
  `TOTAL_NEGATIVE_VOTES` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ENTITY_TYPE_ID_2` (`ENTITY_TYPE_ID`,`ENTITY_ID`,`ACTIVE`),
  KEY `IX_ENTITY_TYPE_ID_4` (`TOTAL_VALUE`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_voting_prepare`
--

DROP TABLE IF EXISTS `b_rating_voting_prepare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_voting_prepare` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_VOTING_ID` int(11) NOT NULL,
  `TOTAL_VALUE` decimal(18,4) NOT NULL,
  `TOTAL_VOTES` int(11) NOT NULL,
  `TOTAL_POSITIVE_VOTES` int(11) NOT NULL,
  `TOTAL_NEGATIVE_VOTES` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_RATING_VOTING_ID` (`RATING_VOTING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_rating_weight`
--

DROP TABLE IF EXISTS `b_rating_weight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_rating_weight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_FROM` decimal(18,4) NOT NULL,
  `RATING_TO` decimal(18,4) NOT NULL,
  `WEIGHT` decimal(18,4) DEFAULT '0.0000',
  `COUNT` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_affiliate`
--

DROP TABLE IF EXISTS `b_sale_affiliate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_affiliate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `AFFILIATE_ID` int(11) DEFAULT NULL,
  `PLAN_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_CREATE` datetime NOT NULL,
  `PAID_SUM` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `APPROVED_SUM` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `PENDING_SUM` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `ITEMS_NUMBER` int(11) NOT NULL DEFAULT '0',
  `ITEMS_SUM` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `LAST_CALCULATE` datetime DEFAULT NULL,
  `AFF_SITE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AFF_DESCRIPTION` text COLLATE utf8_unicode_ci,
  `FIX_PLAN` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SAA_USER_ID` (`USER_ID`,`SITE_ID`),
  KEY `IX_SAA_AFFILIATE_ID` (`AFFILIATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_affiliate_plan`
--

DROP TABLE IF EXISTS `b_sale_affiliate_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_affiliate_plan` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `BASE_RATE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `BASE_RATE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `BASE_RATE_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MIN_PAY` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `MIN_PLAN_VALUE` decimal(18,4) DEFAULT NULL,
  `VALUE_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_affiliate_plan_section`
--

DROP TABLE IF EXISTS `b_sale_affiliate_plan_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_affiliate_plan_section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PLAN_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'catalog',
  `SECTION_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RATE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `RATE_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SAP_PLAN_ID` (`PLAN_ID`,`MODULE_ID`,`SECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_affiliate_tier`
--

DROP TABLE IF EXISTS `b_sale_affiliate_tier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_affiliate_tier` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `RATE1` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE2` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE3` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE4` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE5` decimal(18,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SAT_SITE_ID` (`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_affiliate_transact`
--

DROP TABLE IF EXISTS `b_sale_affiliate_transact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_affiliate_transact` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AFFILIATE_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TRANSACT_DATE` datetime NOT NULL,
  `AMOUNT` decimal(18,4) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DEBIT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DESCRIPTION` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `EMPLOYEE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SAT_AFFILIATE_ID` (`AFFILIATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_auxiliary`
--

DROP TABLE IF EXISTS `b_sale_auxiliary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_auxiliary` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ITEM` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_MD5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `DATE_INSERT` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_STT_USER_ITEM` (`USER_ID`,`ITEM_MD5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_basket`
--

DROP TABLE IF EXISTS `b_sale_basket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_basket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FUSER_ID` int(11) NOT NULL,
  `ORDER_ID` int(11) DEFAULT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `PRODUCT_PRICE_ID` int(11) DEFAULT NULL,
  `PRICE` decimal(18,4) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `WEIGHT` double(18,2) DEFAULT NULL,
  `QUANTITY` double(18,4) NOT NULL DEFAULT '0.0000',
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `DELAY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CAN_BUY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `MODULE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORDER_CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DETAIL_PAGE_URL` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_PRICE` decimal(18,4) NOT NULL,
  `CANCEL_CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAY_CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_PROVIDER_CLASS` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CATALOG_XML_ID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_XML_ID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_VALUE` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_COUPON` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VAT_RATE` decimal(18,4) DEFAULT '0.0000',
  `SUBSCRIBE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DEDUCTED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RESERVED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `BARCODE_MULTI` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RESERVE_QUANTITY` double DEFAULT NULL,
  `CUSTOM_PRICE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DIMENSIONS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `SET_PARENT_ID` int(11) DEFAULT NULL,
  `MEASURE_CODE` int(11) DEFAULT NULL,
  `MEASURE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RECOMMENDATION` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BASE_PRICE` decimal(18,4) DEFAULT NULL,
  `VAT_INCLUDED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `IXS_BASKET_LID` (`LID`),
  KEY `IXS_BASKET_USER_ID` (`FUSER_ID`),
  KEY `IXS_BASKET_ORDER_ID` (`ORDER_ID`),
  KEY `IXS_BASKET_PRODUCT_ID` (`PRODUCT_ID`),
  KEY `IXS_BASKET_PRODUCT_PRICE_ID` (`PRODUCT_PRICE_ID`),
  KEY `IXS_SBAS_XML_ID` (`PRODUCT_XML_ID`,`CATALOG_XML_ID`),
  KEY `IXS_BASKET_DATE_INSERT` (`DATE_INSERT`)
) ENGINE=InnoDB AUTO_INCREMENT=298 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_basket_props`
--

DROP TABLE IF EXISTS `b_sale_basket_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_basket_props` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BASKET_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `IXS_BASKET_PROPS_BASKET` (`BASKET_ID`),
  KEY `IXS_BASKET_PROPS_CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=599 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_bizval`
--

DROP TABLE IF EXISTS `b_sale_bizval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_bizval` (
  `CODE_KEY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CONSUMER_KEY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `PROVIDER_KEY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PROVIDER_VALUE` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`CODE_KEY`,`CONSUMER_KEY`,`PERSON_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_bizval_code_1c`
--

DROP TABLE IF EXISTS `b_sale_bizval_code_1c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_bizval_code_1c` (
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `CODE_INDEX` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PERSON_TYPE_ID`,`CODE_INDEX`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_bizval_old`
--

DROP TABLE IF EXISTS `b_sale_bizval_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_bizval_old` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE_ID` int(11) NOT NULL,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `ENTITY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BSB_SECONDARY` (`CODE_ID`,`PERSON_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_bizval_persondomain`
--

DROP TABLE IF EXISTS `b_sale_bizval_persondomain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_bizval_persondomain` (
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `DOMAIN` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PERSON_TYPE_ID`,`DOMAIN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_company`
--

DROP TABLE IF EXISTS `b_sale_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_company` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `LOCATION_ID` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `DATE_CREATE` datetime DEFAULT NULL,
  `DATE_MODIFY` datetime DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  `ADDRESS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_delivery2location`
--

DROP TABLE IF EXISTS `b_sale_delivery2location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_delivery2location` (
  `DELIVERY_ID` int(11) NOT NULL,
  `LOCATION_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'L',
  `LOCATION_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`DELIVERY_ID`,`LOCATION_CODE`,`LOCATION_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_delivery2paysystem`
--

DROP TABLE IF EXISTS `b_sale_delivery2paysystem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_delivery2paysystem` (
  `DELIVERY_ID` int(11) NOT NULL,
  `PAYSYSTEM_ID` int(11) NOT NULL,
  `LINK_DIRECTION` char(1) COLLATE utf8_unicode_ci NOT NULL,
  KEY `IX_DELIVERY` (`DELIVERY_ID`),
  KEY `IX_LINK_DIRECTION` (`LINK_DIRECTION`),
  KEY `IX_PAYSYSTEM` (`PAYSYSTEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_delivery_es`
--

DROP TABLE IF EXISTS `b_sale_delivery_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_delivery_es` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLASS_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `RIGHTS` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DELIVERY_ID` int(11) NOT NULL,
  `INIT_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `IX_BSD_ES_DELIVERY_ID` (`DELIVERY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_delivery_handler_old`
--

DROP TABLE IF EXISTS `b_sale_delivery_handler_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_delivery_handler_old` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT '',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `HID` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `HANDLER` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SETTINGS` text COLLATE utf8_unicode_ci,
  `PROFILES` text COLLATE utf8_unicode_ci,
  `TAX_RATE` double DEFAULT '0',
  `LOGOTIP` int(11) DEFAULT NULL,
  `BASE_CURRENCY` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONVERTED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `IX_HID` (`HID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_delivery_old`
--

DROP TABLE IF EXISTS `b_sale_delivery_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_delivery_old` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `PERIOD_FROM` int(11) DEFAULT NULL,
  `PERIOD_TO` int(11) DEFAULT NULL,
  `PERIOD_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEIGHT_FROM` int(11) DEFAULT NULL,
  `WEIGHT_TO` int(11) DEFAULT NULL,
  `ORDER_PRICE_FROM` decimal(18,2) DEFAULT NULL,
  `ORDER_PRICE_TO` decimal(18,2) DEFAULT NULL,
  `ORDER_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `PRICE` decimal(18,2) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `LOGOTIP` int(11) DEFAULT NULL,
  `STORE` text COLLATE utf8_unicode_ci,
  `CONVERTED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `IXS_DELIVERY_LID` (`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_delivery_srv`
--

DROP TABLE IF EXISTS `b_sale_delivery_srv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_delivery_srv` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `SORT` int(11) NOT NULL,
  `LOGOTIP` int(11) DEFAULT NULL,
  `CONFIG` longtext COLLATE utf8_unicode_ci,
  `CLASS_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `TRACKING_PARAMS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ALLOW_EDIT_SHIPMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IX_BSD_SRV_CODE` (`CODE`),
  KEY `IX_BSD_SRV_PARENT_ID` (`PARENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_discount`
--

DROP TABLE IF EXISTS `b_sale_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_discount` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRICE_FROM` decimal(18,2) DEFAULT NULL,
  `PRICE_TO` decimal(18,2) DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_VALUE` decimal(18,2) NOT NULL,
  `DISCOUNT_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `PRIORITY` int(18) NOT NULL DEFAULT '1',
  `LAST_DISCOUNT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `CONDITIONS` mediumtext COLLATE utf8_unicode_ci,
  `UNPACK` mediumtext COLLATE utf8_unicode_ci,
  `ACTIONS` mediumtext COLLATE utf8_unicode_ci,
  `APPLICATION` mediumtext COLLATE utf8_unicode_ci,
  `USE_COUPONS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `EXECUTE_MODULE` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'all',
  PRIMARY KEY (`ID`),
  KEY `IXS_DISCOUNT_LID` (`LID`),
  KEY `IX_SSD_ACTIVE_DATE` (`ACTIVE_FROM`,`ACTIVE_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_discount_coupon`
--

DROP TABLE IF EXISTS `b_sale_discount_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_discount_coupon` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `ACTIVE` char(1) NOT NULL DEFAULT 'Y',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `COUPON` varchar(32) NOT NULL,
  `TYPE` int(11) NOT NULL DEFAULT '0',
  `MAX_USE` int(11) NOT NULL DEFAULT '0',
  `USE_COUNT` int(11) NOT NULL DEFAULT '0',
  `USER_ID` int(11) NOT NULL DEFAULT '0',
  `DATE_APPLY` datetime DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `DESCRIPTION` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_discount_entities`
--

DROP TABLE IF EXISTS `b_sale_discount_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_discount_entities` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) NOT NULL,
  `ENTITY` varchar(255) NOT NULL,
  `FIELD_ENTITY` varchar(255) NOT NULL,
  `FIELD_TABLE` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SALE_DSC_ENT_DISCOUNT_ID` (`DISCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_discount_group`
--

DROP TABLE IF EXISTS `b_sale_discount_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_discount_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_S_DISGRP` (`DISCOUNT_ID`,`GROUP_ID`),
  UNIQUE KEY `IX_S_DISGRP_G` (`GROUP_ID`,`DISCOUNT_ID`),
  KEY `IX_S_DISGRP_D` (`DISCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_discount_module`
--

DROP TABLE IF EXISTS `b_sale_discount_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_discount_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SALE_DSC_MOD` (`DISCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_export`
--

DROP TABLE IF EXISTS `b_sale_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_export` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `VARS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_fuser`
--

DROP TABLE IF EXISTS `b_sale_fuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_fuser` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `CODE` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_USER_ID` (`USER_ID`),
  KEY `IX_CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_gift_related_data`
--

DROP TABLE IF EXISTS `b_sale_gift_related_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_gift_related_data` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `ELEMENT_ID` int(11) DEFAULT NULL,
  `SECTION_ID` int(11) DEFAULT NULL,
  `MAIN_PRODUCT_SECTION_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_S_GRD_O_1` (`DISCOUNT_ID`),
  KEY `IX_S_GRD_O_2` (`MAIN_PRODUCT_SECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_lang`
--

DROP TABLE IF EXISTS `b_sale_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_lang` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_loc_2site`
--

DROP TABLE IF EXISTS `b_sale_loc_2site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_loc_2site` (
  `LOCATION_ID` int(11) NOT NULL,
  `SITE_ID` char(2) NOT NULL,
  `LOCATION_TYPE` char(1) NOT NULL DEFAULT 'L',
  PRIMARY KEY (`SITE_ID`,`LOCATION_ID`,`LOCATION_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_loc_def2site`
--

DROP TABLE IF EXISTS `b_sale_loc_def2site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_loc_def2site` (
  `LOCATION_CODE` varchar(100) NOT NULL,
  `SITE_ID` char(2) NOT NULL,
  `SORT` int(11) DEFAULT '100',
  PRIMARY KEY (`LOCATION_CODE`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_loc_ext`
--

DROP TABLE IF EXISTS `b_sale_loc_ext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_loc_ext` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SERVICE_ID` int(11) NOT NULL,
  `LOCATION_ID` int(11) NOT NULL,
  `XML_ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_SALE_LOC_EXT_LID_SID` (`LOCATION_ID`,`SERVICE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8886 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_loc_ext_srv`
--

DROP TABLE IF EXISTS `b_sale_loc_ext_srv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_loc_ext_srv` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_loc_legacy`
--

DROP TABLE IF EXISTS `b_sale_loc_legacy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_loc_legacy` (
  `ID` int(11) DEFAULT NULL,
  `COUNTRY_ID` int(11) DEFAULT NULL,
  `REGION_ID` int(11) DEFAULT NULL,
  `CITY_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_loc_name`
--

DROP TABLE IF EXISTS `b_sale_loc_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_loc_name` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LANGUAGE_ID` char(2) NOT NULL,
  `LOCATION_ID` int(11) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `NAME_UPPER` varchar(100) NOT NULL,
  `SHORT_NAME` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_SALE_LOC_NAME_NAME_U` (`NAME_UPPER`),
  KEY `IX_B_SALE_LOC_NAME_LI_LI` (`LOCATION_ID`,`LANGUAGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2671 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_loc_type`
--

DROP TABLE IF EXISTS `b_sale_loc_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_loc_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(30) NOT NULL,
  `SORT` int(11) DEFAULT '100',
  `DISPLAY_SORT` int(11) DEFAULT '100',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_loc_type_name`
--

DROP TABLE IF EXISTS `b_sale_loc_type_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_loc_type_name` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LANGUAGE_ID` char(2) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `TYPE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_SALE_LOC_TYPE_NAME_TI_LI` (`TYPE_ID`,`LANGUAGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location`
--

DROP TABLE IF EXISTS `b_sale_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COUNTRY_ID` int(11) DEFAULT NULL,
  `REGION_ID` int(11) DEFAULT NULL,
  `CITY_ID` int(11) DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `LOC_DEFAULT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LEFT_MARGIN` int(11) DEFAULT NULL,
  `RIGHT_MARGIN` int(11) DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT '0',
  `DEPTH_LEVEL` int(11) DEFAULT '1',
  `TYPE_ID` int(11) DEFAULT NULL,
  `LATITUDE` decimal(8,6) DEFAULT NULL,
  `LONGITUDE` decimal(9,6) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_SALE_LOC_CODE` (`CODE`),
  KEY `IXS_LOCATION_COUNTRY_ID` (`COUNTRY_ID`),
  KEY `IXS_LOCATION_REGION_ID` (`REGION_ID`),
  KEY `IXS_LOCATION_CITY_ID` (`CITY_ID`),
  KEY `IX_B_SALE_LOC_MARGINS` (`LEFT_MARGIN`,`RIGHT_MARGIN`),
  KEY `IX_B_SALE_LOC_MARGINS_REV` (`RIGHT_MARGIN`,`LEFT_MARGIN`),
  KEY `IX_B_SALE_LOC_PARENT` (`PARENT_ID`),
  KEY `IX_B_SALE_LOC_DL` (`DEPTH_LEVEL`),
  KEY `IX_B_SALE_LOC_TYPE` (`TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1336 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location2location_group`
--

DROP TABLE IF EXISTS `b_sale_location2location_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location2location_group` (
  `LOCATION_ID` int(11) NOT NULL,
  `LOCATION_GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`LOCATION_ID`,`LOCATION_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location_city`
--

DROP TABLE IF EXISTS `b_sale_location_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location_city` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REGION_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_LOCAT_REGION_ID` (`REGION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1239 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location_city_lang`
--

DROP TABLE IF EXISTS `b_sale_location_city_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location_city_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CITY_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_LOCAT_CITY_LID` (`CITY_ID`,`LID`),
  KEY `IX_NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=2477 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location_country`
--

DROP TABLE IF EXISTS `b_sale_location_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location_country` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location_country_lang`
--

DROP TABLE IF EXISTS `b_sale_location_country_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location_country_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COUNTRY_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_LOCAT_CNTR_LID` (`COUNTRY_ID`,`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location_group`
--

DROP TABLE IF EXISTS `b_sale_location_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_SALE_LOC_GROUP_CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location_group_lang`
--

DROP TABLE IF EXISTS `b_sale_location_group_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location_group_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOCATION_GROUP_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_location_group_lid` (`LOCATION_GROUP_ID`,`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location_region`
--

DROP TABLE IF EXISTS `b_sale_location_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location_region` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location_region_lang`
--

DROP TABLE IF EXISTS `b_sale_location_region_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location_region_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REGION_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_LOCAT_REGION_LID` (`REGION_ID`,`LID`),
  KEY `IXS_NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_location_zip`
--

DROP TABLE IF EXISTS `b_sale_location_zip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_location_zip` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOCATION_ID` int(11) NOT NULL DEFAULT '0',
  `ZIP` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_LOCATION_ID` (`LOCATION_ID`),
  KEY `IX_ZIP` (`ZIP`)
) ENGINE=InnoDB AUTO_INCREMENT=8886 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order`
--

DROP TABLE IF EXISTS `b_sale_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `PAYED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_PAYED` datetime DEFAULT NULL,
  `EMP_PAYED_ID` int(11) DEFAULT NULL,
  `CANCELED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_CANCELED` datetime DEFAULT NULL,
  `EMP_CANCELED_ID` int(11) DEFAULT NULL,
  `REASON_CANCELED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS_ID` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_STATUS` datetime NOT NULL,
  `EMP_STATUS_ID` int(11) DEFAULT NULL,
  `PRICE_DELIVERY` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `ALLOW_DELIVERY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_ALLOW_DELIVERY` datetime DEFAULT NULL,
  `EMP_ALLOW_DELIVERY_ID` int(11) DEFAULT NULL,
  `DEDUCTED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_DEDUCTED` datetime DEFAULT NULL,
  `EMP_DEDUCTED_ID` int(11) DEFAULT NULL,
  `REASON_UNDO_DEDUCTED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MARKED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_MARKED` datetime DEFAULT NULL,
  `EMP_MARKED_ID` int(11) DEFAULT NULL,
  `REASON_MARKED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESERVED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PRICE` decimal(18,4) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT_VALUE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `USER_ID` int(11) NOT NULL,
  `PAY_SYSTEM_ID` int(11) DEFAULT NULL,
  `DELIVERY_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `USER_DESCRIPTION` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADDITIONAL_INFO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_CODE` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_MESSAGE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_SUM` decimal(18,2) DEFAULT NULL,
  `PS_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_RESPONSE_DATE` datetime DEFAULT NULL,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `TAX_VALUE` decimal(18,2) NOT NULL DEFAULT '0.00',
  `STAT_GID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUM_PAID` decimal(18,2) NOT NULL DEFAULT '0.00',
  `RECURRING_ID` int(11) DEFAULT NULL,
  `PAY_VOUCHER_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAY_VOUCHER_DATE` date DEFAULT NULL,
  `LOCKED_BY` int(11) DEFAULT NULL,
  `DATE_LOCK` datetime DEFAULT NULL,
  `RECOUNT_FLAG` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `AFFILIATE_ID` int(11) DEFAULT NULL,
  `DELIVERY_DOC_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DELIVERY_DOC_DATE` date DEFAULT NULL,
  `UPDATED_1C` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `STORE_ID` int(11) DEFAULT NULL,
  `ORDER_TOPIC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESPONSIBLE_ID` int(11) DEFAULT NULL,
  `DATE_PAY_BEFORE` datetime DEFAULT NULL,
  `DATE_BILL` datetime DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TRACKING_NUMBER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ID_1C` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION_1C` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(11) NOT NULL DEFAULT '0',
  `EXTERNAL_ORDER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `BX_USER_ID` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRICE_PAYMENT` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `CREATED_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_ACCOUNT_NUMBER` (`ACCOUNT_NUMBER`),
  KEY `IXS_ORDER_USER_ID` (`USER_ID`),
  KEY `IXS_ORDER_PERSON_TYPE_ID` (`PERSON_TYPE_ID`),
  KEY `IXS_ORDER_STATUS_ID` (`STATUS_ID`),
  KEY `IXS_ORDER_REC_ID` (`RECURRING_ID`),
  KEY `IX_SOO_AFFILIATE_ID` (`AFFILIATE_ID`),
  KEY `IXS_ORDER_UPDATED_1C` (`UPDATED_1C`),
  KEY `IXS_SALE_COUNT` (`USER_ID`,`LID`,`PAYED`,`CANCELED`),
  KEY `IXS_DATE_UPDATE` (`DATE_UPDATE`),
  KEY `IXS_XML_ID` (`XML_ID`),
  KEY `IXS_ID_1C` (`ID_1C`),
  KEY `IX_BSO_DATE_ALLOW_DELIVERY` (`DATE_ALLOW_DELIVERY`),
  KEY `IX_BSO_ALLOW_DELIVERY` (`ALLOW_DELIVERY`),
  KEY `IX_BSO_DATE_CANCELED` (`DATE_CANCELED`),
  KEY `IX_BSO_CANCELED` (`CANCELED`),
  KEY `IX_BSO_DATE_PAYED` (`DATE_PAYED`),
  KEY `IX_BSO_DATE_INSERT` (`DATE_INSERT`),
  KEY `IX_BSO_DATE_PAY_BEFORE` (`DATE_PAY_BEFORE`)
) ENGINE=InnoDB AUTO_INCREMENT=294 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_change`
--

DROP TABLE IF EXISTS `b_sale_order_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_change` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATA` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_MODIFY` datetime NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ENTITY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ENTITY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_ORDER_ID_CHANGE` (`ORDER_ID`),
  KEY `IXS_TYPE_CHANGE` (`TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=1483 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_coupons`
--

DROP TABLE IF EXISTS `b_sale_order_coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_coupons` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `ORDER_DISCOUNT_ID` int(11) NOT NULL,
  `COUPON` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `COUPON_ID` int(11) NOT NULL,
  `TYPE` int(11) NOT NULL,
  `DATA` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_SALE_ORDER_CPN_ORDER` (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_delivery`
--

DROP TABLE IF EXISTS `b_sale_order_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_delivery` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `ACCOUNT_NUMBER` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_REQUEST` datetime DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `DELIVERY_LOCATION` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `STATUS_ID` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `PRICE_DELIVERY` decimal(18,4) DEFAULT NULL,
  `CUSTOM_PRICE_DELIVERY` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BASE_PRICE_DELIVERY` decimal(18,4) DEFAULT NULL,
  `ALLOW_DELIVERY` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `DATE_ALLOW_DELIVERY` datetime DEFAULT NULL,
  `EMP_ALLOW_DELIVERY_ID` int(11) DEFAULT NULL,
  `DEDUCTED` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `DATE_DEDUCTED` datetime DEFAULT NULL,
  `EMP_DEDUCTED_ID` int(11) DEFAULT NULL,
  `REASON_UNDO_DEDUCTED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESERVED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DELIVERY_ID` int(11) NOT NULL,
  `DELIVERY_DOC_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DELIVERY_DOC_DATE` datetime DEFAULT NULL,
  `TRACKING_NUMBER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DELIVERY_NAME` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CANCELED` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `DATE_CANCELED` datetime DEFAULT NULL,
  `EMP_CANCELED_ID` int(11) DEFAULT NULL,
  `REASON_CANCELED` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `MARKED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_MARKED` datetime DEFAULT NULL,
  `EMP_MARKED_ID` int(11) DEFAULT NULL,
  `REASON_MARKED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSTEM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RESPONSIBLE_ID` int(11) DEFAULT NULL,
  `EMP_RESPONSIBLE_ID` int(11) DEFAULT NULL,
  `DATE_RESPONSIBLE_ID` datetime DEFAULT NULL,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `DISCOUNT_PRICE` decimal(18,4) DEFAULT NULL,
  `COMPANY_ID` int(11) DEFAULT NULL,
  `TRACKING_STATUS` int(11) DEFAULT NULL,
  `TRACKING_DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TRACKING_LAST_CHECK` datetime DEFAULT NULL,
  `TRACKING_LAST_CHANGE` datetime DEFAULT NULL,
  `ID_1C` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION_1C` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXTERNAL_DELIVERY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `UPDATED_1C` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_DLV_ACCOUNT_NUMBER` (`ACCOUNT_NUMBER`),
  KEY `IX_BSOD_ORDER_ID` (`ORDER_ID`),
  KEY `IX_BSOD_DATE_ALLOW_DELIVERY` (`DATE_ALLOW_DELIVERY`),
  KEY `IX_BSOD_ALLOW_DELIVERY` (`ALLOW_DELIVERY`),
  KEY `IX_BSOD_DATE_CANCELED` (`DATE_CANCELED`),
  KEY `IX_BSOD_CANCELED` (`CANCELED`)
) ENGINE=InnoDB AUTO_INCREMENT=284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_delivery_es`
--

DROP TABLE IF EXISTS `b_sale_order_delivery_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_delivery_es` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SHIPMENT_ID` int(11) NOT NULL,
  `EXTRA_SERVICE_ID` int(11) NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BSOD_ES_SHIPMENT_ID` (`SHIPMENT_ID`),
  KEY `IX_BSOD_ES_EXTRA_SERVICE_ID` (`EXTRA_SERVICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_delivery_req`
--

DROP TABLE IF EXISTS `b_sale_order_delivery_req`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_delivery_req` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `DATE_REQUEST` datetime DEFAULT NULL,
  `DELIVERY_LOCATION` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `PARAMS` text COLLATE utf8_unicode_ci,
  `SHIPMENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ORDER_ID` (`ORDER_ID`),
  KEY `IX_SHIPMENT_ID` (`SHIPMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_discount`
--

DROP TABLE IF EXISTS `b_sale_order_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_discount` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT_HASH` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITIONS` mediumtext COLLATE utf8_unicode_ci,
  `UNPACK` mediumtext COLLATE utf8_unicode_ci,
  `ACTIONS` mediumtext COLLATE utf8_unicode_ci,
  `APPLICATION` mediumtext COLLATE utf8_unicode_ci,
  `USE_COUPONS` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `LAST_DISCOUNT` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIONS_DESCR` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_SALE_ORDER_DSC_HASH` (`DISCOUNT_HASH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_discount_data`
--

DROP TABLE IF EXISTS `b_sale_order_discount_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_discount_data` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `ENTITY_TYPE` int(11) NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `ENTITY_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ENTITY_DATA` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SALE_DSC_DATA_CMX` (`ORDER_ID`,`ENTITY_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=511 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_dlv_basket`
--

DROP TABLE IF EXISTS `b_sale_order_dlv_basket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_dlv_basket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_DELIVERY_ID` int(11) NOT NULL,
  `BASKET_ID` int(11) NOT NULL,
  `DATE_INSERT` datetime NOT NULL,
  `QUANTITY` decimal(18,4) NOT NULL,
  `RESERVED_QUANTITY` decimal(18,4) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BSODB_ORDER_DELIVERY_ID` (`ORDER_DELIVERY_ID`),
  KEY `IX_S_O_DB_BASKET_ID` (`BASKET_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_flags2group`
--

DROP TABLE IF EXISTS `b_sale_order_flags2group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_flags2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) NOT NULL,
  `ORDER_FLAG` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_sale_ordfla2group` (`GROUP_ID`,`ORDER_FLAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_history`
--

DROP TABLE IF EXISTS `b_sale_order_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_history` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `H_USER_ID` int(11) unsigned NOT NULL,
  `H_DATE_INSERT` datetime NOT NULL,
  `H_ORDER_ID` int(11) unsigned NOT NULL,
  `H_CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `PERSON_TYPE_ID` int(11) unsigned DEFAULT NULL,
  `PAYED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_PAYED` datetime DEFAULT NULL,
  `EMP_PAYED_ID` int(11) unsigned DEFAULT NULL,
  `CANCELED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_CANCELED` datetime DEFAULT NULL,
  `REASON_CANCELED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS_ID` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_STATUS` datetime DEFAULT NULL,
  `PRICE_DELIVERY` decimal(18,2) DEFAULT NULL,
  `ALLOW_DELIVERY` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_ALLOW_DELIVERY` datetime DEFAULT NULL,
  `RESERVED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEDUCTED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_DEDUCTED` datetime DEFAULT NULL,
  `REASON_UNDO_DEDUCTED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MARKED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_MARKED` datetime DEFAULT NULL,
  `REASON_MARKED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRICE` decimal(18,2) DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_VALUE` decimal(18,2) DEFAULT NULL,
  `USER_ID` int(11) unsigned DEFAULT NULL,
  `PAY_SYSTEM_ID` int(11) unsigned DEFAULT NULL,
  `DELIVERY_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_CODE` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_MESSAGE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_SUM` decimal(18,2) DEFAULT NULL,
  `PS_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_RESPONSE_DATE` datetime DEFAULT NULL,
  `TAX_VALUE` decimal(18,2) DEFAULT NULL,
  `STAT_GID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUM_PAID` decimal(18,2) DEFAULT NULL,
  `PAY_VOUCHER_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAY_VOUCHER_DATE` date DEFAULT NULL,
  `AFFILIATE_ID` int(11) unsigned DEFAULT NULL,
  `DELIVERY_DOC_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DELIVERY_DOC_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ixH_ORDER_ID` (`H_ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_modules`
--

DROP TABLE IF EXISTS `b_sale_order_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_modules` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_DISCOUNT_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SALE_ORDER_MDL_DSC` (`ORDER_DISCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_payment`
--

DROP TABLE IF EXISTS `b_sale_order_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_payment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `ACCOUNT_NUMBER` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAID` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_PAID` datetime DEFAULT NULL,
  `EMP_PAID_ID` int(11) DEFAULT NULL,
  `PAY_SYSTEM_ID` int(11) NOT NULL,
  `PS_STATUS` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_MESSAGE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_SUM` decimal(18,4) DEFAULT NULL,
  `PS_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_RESPONSE_DATE` datetime DEFAULT NULL,
  `PAY_VOUCHER_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAY_VOUCHER_DATE` date DEFAULT NULL,
  `DATE_PAY_BEFORE` datetime DEFAULT NULL,
  `DATE_BILL` datetime DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUM` decimal(18,4) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `PAY_SYSTEM_NAME` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `RESPONSIBLE_ID` int(11) DEFAULT NULL,
  `DATE_RESPONSIBLE_ID` datetime DEFAULT NULL,
  `EMP_RESPONSIBLE_ID` int(11) DEFAULT NULL,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `COMPANY_ID` int(11) DEFAULT NULL,
  `PAY_RETURN_DATE` date DEFAULT NULL,
  `EMP_RETURN_ID` int(11) DEFAULT NULL,
  `PAY_RETURN_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAY_RETURN_COMMENT` text COLLATE utf8_unicode_ci,
  `IS_RETURN` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PS_INVOICE_ID` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRICE_COD` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `ID_1C` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION_1C` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXTERNAL_PAYMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `UPDATED_1C` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_PAY_ACCOUNT_NUMBER` (`ACCOUNT_NUMBER`),
  KEY `IX_BSOP_ORDER_ID` (`ORDER_ID`),
  KEY `IX_BSOP_DATE_PAID` (`DATE_PAID`),
  KEY `IX_BSOP_PAID` (`PAID`)
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_payment_es`
--

DROP TABLE IF EXISTS `b_sale_order_payment_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_payment_es` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PAYMENT_ID` int(11) NOT NULL,
  `EXTRA_SERVICE_ID` int(11) NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BSOP_ES_PAYMENT_ID` (`PAYMENT_ID`),
  KEY `IX_BSOP_ES_EXTRA_SERVICE_ID` (`EXTRA_SERVICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_processing`
--

DROP TABLE IF EXISTS `b_sale_order_processing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_processing` (
  `ORDER_ID` int(11) DEFAULT '0',
  `PRODUCTS_ADDED` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `PRODUCTS_REMOVED` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  KEY `IX_ORDER_ID` (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_props`
--

DROP TABLE IF EXISTS `b_sale_order_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_props` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `REQUIRED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DEFAULT_VALUE` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `USER_PROPS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_LOCATION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PROPS_GROUP_ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_EMAIL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_PROFILE_NAME` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_PAYER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_LOCATION4TAX` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_FILTERED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_ZIP` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_PHONE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVE` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `UTIL` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `INPUT_FIELD_LOCATION` int(11) NOT NULL DEFAULT '0',
  `MULTIPLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_ADDRESS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SETTINGS` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_ORDER_PROPS_PERSON_TYPE_ID` (`PERSON_TYPE_ID`),
  KEY `IXS_CODE_OPP` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_props_group`
--

DROP TABLE IF EXISTS `b_sale_order_props_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_props_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `IXS_ORDER_PROPS_GROUP_PERSON_TYPE_ID` (`PERSON_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_props_rel_old`
--

DROP TABLE IF EXISTS `b_sale_order_props_rel_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_props_rel_old` (
  `PROPERTY_ID` int(11) NOT NULL,
  `ENTITY_ID` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PROPERTY_ID`,`ENTITY_ID`,`ENTITY_TYPE`),
  KEY `IX_PROPERTY` (`PROPERTY_ID`),
  KEY `IX_ENTITY_ID` (`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_props_relation`
--

DROP TABLE IF EXISTS `b_sale_order_props_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_props_relation` (
  `PROPERTY_ID` int(11) NOT NULL,
  `ENTITY_ID` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PROPERTY_ID`,`ENTITY_ID`,`ENTITY_TYPE`),
  KEY `IX_PROPERTY` (`PROPERTY_ID`),
  KEY `IX_ENTITY_ID` (`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_props_value`
--

DROP TABLE IF EXISTS `b_sale_order_props_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_props_value` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `ORDER_PROPS_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SOPV_ORD_PROP_UNI` (`ORDER_ID`,`ORDER_PROPS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_props_variant`
--

DROP TABLE IF EXISTS `b_sale_order_props_variant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_props_variant` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_PROPS_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_ORDER_PROPS_VARIANT_ORDER_PROPS_ID` (`ORDER_PROPS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_rules`
--

DROP TABLE IF EXISTS `b_sale_order_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_rules` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ORDER_DISCOUNT_ID` int(11) NOT NULL,
  `ORDER_ID` int(11) NOT NULL,
  `ENTITY_TYPE` int(11) NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `ENTITY_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COUPON_ID` int(11) NOT NULL,
  `APPLY` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `ACTION_BLOCK_LIST` text COLLATE utf8_unicode_ci,
  `APPLY_BLOCK_COUNTER` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_SALE_ORDER_RULES_ORD` (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_rules_descr`
--

DROP TABLE IF EXISTS `b_sale_order_rules_descr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_rules_descr` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ORDER_DISCOUNT_ID` int(11) NOT NULL,
  `ORDER_ID` int(11) NOT NULL,
  `RULE_ID` int(11) NOT NULL,
  `DESCR` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SALE_ORDER_RULES_DS_ORD` (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_order_tax`
--

DROP TABLE IF EXISTS `b_sale_order_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_order_tax` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `TAX_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` decimal(18,4) DEFAULT NULL,
  `VALUE_MONEY` decimal(18,4) NOT NULL,
  `APPLY_ORDER` int(11) NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_PERCENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IS_IN_PRICE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `ixs_sot_order_id` (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_pay_system`
--

DROP TABLE IF EXISTS `b_sale_pay_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_pay_system` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DESCRIPTION` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ALLOW_EDIT_PAYMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IXS_PAY_SYSTEM_LID` (`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_pay_system_action`
--

DROP TABLE IF EXISTS `b_sale_pay_system_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_pay_system_action` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PAY_SYSTEM_ID` int(11) DEFAULT NULL,
  `PERSON_TYPE_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTION_FILE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESULT_FILE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NEW_WINDOW` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `PARAMS` text COLLATE utf8_unicode_ci,
  `TARIF` text COLLATE utf8_unicode_ci,
  `HAVE_PAYMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HAVE_ACTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HAVE_RESULT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HAVE_PREPAY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HAVE_RESULT_RECEIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ENCODING` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOGOTIP` int(11) DEFAULT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PSA_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DESCRIPTION` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAVE_PRICE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_EDIT_PAYMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IS_CASH` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PS_MODE` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTO_CHANGE_1C` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_pay_system_err_log`
--

DROP TABLE IF EXISTS `b_sale_pay_system_err_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_pay_system_err_log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MESSAGE` text COLLATE utf8_unicode_ci NOT NULL,
  `ACTION` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_INSERT` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_pay_system_es`
--

DROP TABLE IF EXISTS `b_sale_pay_system_es`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_pay_system_es` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLASS_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `SHOW_MODE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAY_SYSTEM_ID` int(11) NOT NULL,
  `DEFAULT_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `IX_BSPS_ES_PAY_SYSTEM_ID` (`PAY_SYSTEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_pay_system_map`
--

DROP TABLE IF EXISTS `b_sale_pay_system_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_pay_system_map` (
  `PS_ID_OLD` int(11) DEFAULT NULL,
  `PS_ID` int(11) DEFAULT NULL,
  `PT_ID` int(11) DEFAULT NULL,
  `NEW_PS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_person_type`
--

DROP TABLE IF EXISTS `b_sale_person_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_person_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '150',
  `ACTIVE` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IXS_PERSON_TYPE_LID` (`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_person_type_site`
--

DROP TABLE IF EXISTS `b_sale_person_type_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_person_type_site` (
  `PERSON_TYPE_ID` int(18) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`PERSON_TYPE_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_product2product`
--

DROP TABLE IF EXISTS `b_sale_product2product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_product2product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `PARENT_PRODUCT_ID` int(11) NOT NULL,
  `CNT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_PRODUCT2PRODUCT_PRODUCT_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_recurring`
--

DROP TABLE IF EXISTS `b_sale_recurring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_recurring` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MODULE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_ID` int(11) DEFAULT NULL,
  `PRODUCT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_PRICE_ID` int(11) DEFAULT NULL,
  `PRICE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'R',
  `RECUR_SCHEME_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  `RECUR_SCHEME_LENGTH` int(11) NOT NULL DEFAULT '0',
  `WITHOUT_ORDER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PRICE` decimal(10,0) NOT NULL DEFAULT '0',
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CANCELED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_CANCELED` datetime DEFAULT NULL,
  `PRIOR_DATE` datetime DEFAULT NULL,
  `NEXT_DATE` datetime NOT NULL,
  `CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_PROVIDER_CLASS` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CANCELED_REASON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORDER_ID` int(11) NOT NULL,
  `REMAINING_ATTEMPTS` int(11) NOT NULL DEFAULT '0',
  `SUCCESS_PAYMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IX_S_R_USER_ID` (`USER_ID`),
  KEY `IX_S_R_NEXT_DATE` (`NEXT_DATE`,`CANCELED`,`REMAINING_ATTEMPTS`),
  KEY `IX_S_R_PRODUCT_ID` (`MODULE`,`PRODUCT_ID`,`PRODUCT_PRICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_service_rstr`
--

DROP TABLE IF EXISTS `b_sale_service_rstr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_service_rstr` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SERVICE_ID` int(11) NOT NULL,
  `SORT` int(11) DEFAULT '100',
  `CLASS_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `SERVICE_TYPE` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BSSR_SERVICE_ID` (`SERVICE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_site2group`
--

DROP TABLE IF EXISTS `b_sale_site2group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_site2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_sale_site2group` (`GROUP_ID`,`SITE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_status`
--

DROP TABLE IF EXISTS `b_sale_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_status` (
  `ID` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'O',
  `NOTIFY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_status2group`
--

DROP TABLE IF EXISTS `b_sale_status2group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_status2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) NOT NULL,
  `STATUS_ID` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `PERM_VIEW` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_CANCEL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_MARK` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_DELIVERY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_DEDUCTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_PAYMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_UPDATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_DELETE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_STATUS_FROM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_sale_s2g_ix1` (`GROUP_ID`,`STATUS_ID`),
  KEY `ix_sale_s2g_1` (`STATUS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_status_group_task`
--

DROP TABLE IF EXISTS `b_sale_status_group_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_status_group_task` (
  `STATUS_ID` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `GROUP_ID` int(18) NOT NULL,
  `TASK_ID` int(18) NOT NULL,
  PRIMARY KEY (`STATUS_ID`,`GROUP_ID`,`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_status_lang`
--

DROP TABLE IF EXISTS `b_sale_status_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_status_lang` (
  `STATUS_ID` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`STATUS_ID`,`LID`),
  UNIQUE KEY `ixs_status_lang_status_id` (`STATUS_ID`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_store_barcode`
--

DROP TABLE IF EXISTS `b_sale_store_barcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_store_barcode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BASKET_ID` int(11) NOT NULL,
  `BARCODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STORE_ID` int(11) DEFAULT NULL,
  `QUANTITY` double NOT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `DATE_MODIFY` datetime DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  `ORDER_DELIVERY_BASKET_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_BSSB_O_DLV_BASKET_ID` (`ORDER_DELIVERY_BASKET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tax`
--

DROP TABLE IF EXISTS `b_sale_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tax` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP_X` datetime NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `itax_lid` (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tax2location`
--

DROP TABLE IF EXISTS `b_sale_tax2location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tax2location` (
  `TAX_RATE_ID` int(11) NOT NULL,
  `LOCATION_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'L',
  `LOCATION_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`TAX_RATE_ID`,`LOCATION_CODE`,`LOCATION_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tax_exempt2group`
--

DROP TABLE IF EXISTS `b_sale_tax_exempt2group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tax_exempt2group` (
  `GROUP_ID` int(11) NOT NULL,
  `TAX_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`TAX_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tax_rate`
--

DROP TABLE IF EXISTS `b_sale_tax_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tax_rate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TAX_ID` int(11) NOT NULL,
  `PERSON_TYPE_ID` int(11) DEFAULT NULL,
  `VALUE` decimal(18,4) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_PERCENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IS_IN_PRICE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `APPLY_ORDER` int(11) NOT NULL DEFAULT '100',
  `TIMESTAMP_X` datetime NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `itax_pers_type` (`PERSON_TYPE_ID`),
  KEY `itax_lid` (`TAX_ID`),
  KEY `itax_inprice` (`IS_IN_PRICE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tp`
--

DROP TABLE IF EXISTS `b_sale_tp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tp` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  `CATALOG_SECTION_TAB_CLASS_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLASS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CODE` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tp_ebay_cat`
--

DROP TABLE IF EXISTS `b_sale_tp_ebay_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tp_ebay_cat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `PARENT_ID` int(11) NOT NULL,
  `LEVEL` int(11) NOT NULL,
  `CONDITION_ID_VALUES` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_ID_DEFINITION_URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_SPECIFIC_ENABLED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `VARIATIONS_ENABLED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `PRODUCT_CREATION_ENABLED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_UPDATE` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tp_ebay_cat_var`
--

DROP TABLE IF EXISTS `b_sale_tp_ebay_cat_var`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tp_ebay_cat_var` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORY_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `REQUIRED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `MIN_VALUES` int(11) NOT NULL,
  `MAX_VALUES` int(11) NOT NULL,
  `SELECTION_MODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ALLOWED_AS_VARIATION` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `DEPENDENCY_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEPENDENCY_VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `HELP_URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tp_ebay_fq`
--

DROP TABLE IF EXISTS `b_sale_tp_ebay_fq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tp_ebay_fq` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FEED_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DATA` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tp_ebay_fr`
--

DROP TABLE IF EXISTS `b_sale_tp_ebay_fr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tp_ebay_fr` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FILENAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FEED_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UPLOAD_TIME` datetime NOT NULL,
  `PROCESSING_REQUEST_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROCESSING_RESULT` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESULTS` text COLLATE utf8_unicode_ci,
  `IS_SUCCESS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tp_map`
--

DROP TABLE IF EXISTS `b_sale_tp_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tp_map` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_ID` int(11) NOT NULL,
  `VALUE_EXTERNAL` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE_INTERNAL` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BSTPM_E_V_V` (`ENTITY_ID`,`VALUE_EXTERNAL`,`VALUE_INTERNAL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tp_map_entity`
--

DROP TABLE IF EXISTS `b_sale_tp_map_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tp_map_entity` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TRADING_PLATFORM_ID` int(11) NOT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CODE_TRADING_PLATFORM_ID` (`TRADING_PLATFORM_ID`,`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_tp_order`
--

DROP TABLE IF EXISTS `b_sale_tp_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_tp_order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `TRADING_PLATFORM_ID` int(11) NOT NULL,
  `EXTERNAL_ORDER_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_UNIQ_NUMBERS` (`ORDER_ID`,`TRADING_PLATFORM_ID`,`EXTERNAL_ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_user_account`
--

DROP TABLE IF EXISTS `b_sale_user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_user_account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CURRENT_BUDGET` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `LOCKED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_LOCKED` datetime DEFAULT NULL,
  `NOTES` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_S_U_USER_ID` (`USER_ID`,`CURRENCY`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_user_cards`
--

DROP TABLE IF EXISTS `b_sale_user_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_user_cards` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PAY_SYSTEM_ACTION_ID` int(11) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CARD_TYPE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CARD_NUM` text COLLATE utf8_unicode_ci NOT NULL,
  `CARD_CODE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CARD_EXP_MONTH` int(11) NOT NULL,
  `CARD_EXP_YEAR` int(11) NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUM_MIN` decimal(18,4) DEFAULT NULL,
  `SUM_MAX` decimal(18,4) DEFAULT NULL,
  `SUM_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_STATUS` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_STATUS_CODE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_STATUS_DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_STATUS_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_SUM` decimal(18,4) DEFAULT NULL,
  `LAST_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_S_U_C_USER_ID` (`USER_ID`,`ACTIVE`,`CURRENCY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_user_props`
--

DROP TABLE IF EXISTS `b_sale_user_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_user_props` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `XML_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION_1C` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_USER_PROPS_USER_ID` (`USER_ID`),
  KEY `IXS_USER_PROPS_PERSON_TYPE_ID` (`PERSON_TYPE_ID`),
  KEY `IXS_USER_PROPS_XML_ID` (`XML_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_user_props_value`
--

DROP TABLE IF EXISTS `b_sale_user_props_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_user_props_value` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_PROPS_ID` int(11) NOT NULL,
  `ORDER_PROPS_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_USER_PROPS_VALUE_USER_PROPS_ID` (`USER_PROPS_ID`),
  KEY `IXS_USER_PROPS_VALUE_ORDER_PROPS_ID` (`ORDER_PROPS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_user_transact`
--

DROP TABLE IF EXISTS `b_sale_user_transact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_user_transact` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TRANSACT_DATE` datetime NOT NULL,
  `AMOUNT` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DEBIT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ORDER_ID` int(11) DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NOTES` text COLLATE utf8_unicode_ci,
  `EMPLOYEE_ID` int(11) DEFAULT NULL,
  `CURRENT_BUDGET` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `PAYMENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_S_U_T_USER_ID` (`USER_ID`),
  KEY `IX_S_U_T_USER_ID_CURRENCY` (`USER_ID`,`CURRENCY`),
  KEY `IX_S_U_T_ORDER_ID` (`ORDER_ID`),
  KEY `IX_S_U_T_PAYMENT_ID` (`PAYMENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sale_viewed_product`
--

DROP TABLE IF EXISTS `b_sale_viewed_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sale_viewed_product` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FUSER_ID` int(11) unsigned NOT NULL DEFAULT '0',
  `DATE_VISIT` datetime NOT NULL,
  `PRODUCT_ID` int(11) unsigned NOT NULL DEFAULT '0',
  `MODULE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRICE` decimal(18,2) NOT NULL DEFAULT '0.00',
  `NOTES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREVIEW_PICTURE` int(11) DEFAULT NULL,
  `DETAIL_PICTURE` int(11) DEFAULT NULL,
  `CALLBACK_FUNC` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_PROVIDER_CLASS` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ixLID` (`FUSER_ID`,`LID`),
  KEY `ixPRODUCT_ID` (`PRODUCT_ID`),
  KEY `ixDATE_VISIT` (`DATE_VISIT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_content`
--

DROP TABLE IF EXISTS `b_search_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_content` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_CHANGE` datetime NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CUSTOM_RANK` int(11) NOT NULL DEFAULT '0',
  `USER_ID` int(11) DEFAULT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ENTITY_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  `TITLE` text COLLATE utf8_unicode_ci,
  `BODY` longtext COLLATE utf8_unicode_ci,
  `TAGS` text COLLATE utf8_unicode_ci,
  `PARAM1` text COLLATE utf8_unicode_ci,
  `PARAM2` text COLLATE utf8_unicode_ci,
  `UPD` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_FROM` datetime DEFAULT NULL,
  `DATE_TO` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_B_SEARCH_CONTENT` (`MODULE_ID`,`ITEM_ID`),
  KEY `IX_B_SEARCH_CONTENT_1` (`MODULE_ID`,`PARAM1`(50),`PARAM2`(50)),
  KEY `IX_B_SEARCH_CONTENT_2` (`ENTITY_ID`(50),`ENTITY_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3806 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_content_freq`
--

DROP TABLE IF EXISTS `b_search_content_freq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_content_freq` (
  `STEM` int(11) NOT NULL DEFAULT '0',
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FREQ` float DEFAULT NULL,
  `TF` float DEFAULT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_FREQ` (`STEM`,`LANGUAGE_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_content_param`
--

DROP TABLE IF EXISTS `b_search_content_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_content_param` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `PARAM_NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PARAM_VALUE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  KEY `IX_B_SEARCH_CONTENT_PARAM` (`SEARCH_CONTENT_ID`,`PARAM_NAME`),
  KEY `IX_B_SEARCH_CONTENT_PARAM_1` (`PARAM_NAME`,`PARAM_VALUE`(50),`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_content_right`
--

DROP TABLE IF EXISTS `b_search_content_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_content_right` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `GROUP_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_RIGHT` (`SEARCH_CONTENT_ID`,`GROUP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_content_site`
--

DROP TABLE IF EXISTS `b_search_content_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_content_site` (
  `SEARCH_CONTENT_ID` int(18) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`SEARCH_CONTENT_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_content_stem`
--

DROP TABLE IF EXISTS `b_search_content_stem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_content_stem` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `STEM` int(11) NOT NULL,
  `TF` float NOT NULL,
  `PS` float NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_STEM` (`STEM`,`LANGUAGE_ID`,`TF`,`PS`,`SEARCH_CONTENT_ID`),
  KEY `IND_B_SEARCH_CONTENT_STEM` (`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_content_text`
--

DROP TABLE IF EXISTS `b_search_content_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_content_text` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `SEARCH_CONTENT_MD5` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `SEARCHABLE_CONTENT` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_content_title`
--

DROP TABLE IF EXISTS `b_search_content_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_content_title` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `POS` int(11) NOT NULL,
  `WORD` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_TITLE` (`SITE_ID`,`WORD`,`SEARCH_CONTENT_ID`,`POS`),
  KEY `IND_B_SEARCH_CONTENT_TITLE` (`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_custom_rank`
--

DROP TABLE IF EXISTS `b_search_custom_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_custom_rank` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `APPLIED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RANK` int(11) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PARAM1` text COLLATE utf8_unicode_ci,
  `PARAM2` text COLLATE utf8_unicode_ci,
  `ITEM_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IND_B_SEARCH_CUSTOM_RANK` (`SITE_ID`,`MODULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_phrase`
--

DROP TABLE IF EXISTS `b_search_phrase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_phrase` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `RESULT_COUNT` int(11) NOT NULL,
  `PAGES` int(11) NOT NULL,
  `SESSION_ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `PHRASE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAGS` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL_TO` text COLLATE utf8_unicode_ci,
  `URL_TO_404` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL_TO_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STAT_SESS_ID` int(18) DEFAULT NULL,
  `EVENT1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IND_PK_B_SEARCH_PHRASE_SESS_PH` (`SESSION_ID`,`PHRASE`(50)),
  KEY `IND_PK_B_SEARCH_PHRASE_SESS_TG` (`SESSION_ID`,`TAGS`(50)),
  KEY `IND_PK_B_SEARCH_PHRASE_TIME` (`TIMESTAMP_X`)
) ENGINE=InnoDB AUTO_INCREMENT=387 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_stem`
--

DROP TABLE IF EXISTS `b_search_stem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_stem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `STEM` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_B_SEARCH_STEM` (`STEM`)
) ENGINE=InnoDB AUTO_INCREMENT=23619 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_suggest`
--

DROP TABLE IF EXISTS `b_search_suggest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_suggest` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `FILTER_MD5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `PHRASE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `RATE` float NOT NULL,
  `TIMESTAMP_X` datetime NOT NULL,
  `RESULT_COUNT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IND_B_SEARCH_SUGGEST` (`FILTER_MD5`,`PHRASE`(50),`RATE`),
  KEY `IND_B_SEARCH_SUGGEST_PHRASE` (`PHRASE`(50),`RATE`),
  KEY `IND_B_SEARCH_SUGGEST_TIME` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_tags`
--

DROP TABLE IF EXISTS `b_search_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_tags` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`SEARCH_CONTENT_ID`,`SITE_ID`,`NAME`),
  KEY `IX_B_SEARCH_TAGS_0` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_search_user_right`
--

DROP TABLE IF EXISTS `b_search_user_right`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_search_user_right` (
  `USER_ID` int(11) NOT NULL,
  `GROUP_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_USER_RIGHT` (`USER_ID`,`GROUP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_filter_mask`
--

DROP TABLE IF EXISTS `b_sec_filter_mask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_filter_mask` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SORT` int(11) NOT NULL DEFAULT '10',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILTER_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIKE_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREG_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_frame_mask`
--

DROP TABLE IF EXISTS `b_sec_frame_mask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_frame_mask` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SORT` int(11) NOT NULL DEFAULT '10',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FRAME_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIKE_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREG_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_iprule`
--

DROP TABLE IF EXISTS `b_sec_iprule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_iprule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RULE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ADMIN_SECTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_FROM_TIMESTAMP` int(11) DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `ACTIVE_TO_TIMESTAMP` int(11) DEFAULT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_sec_iprule_active_to` (`ACTIVE_TO`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_iprule_excl_ip`
--

DROP TABLE IF EXISTS `b_sec_iprule_excl_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_iprule_excl_ip` (
  `IPRULE_ID` int(11) NOT NULL,
  `RULE_IP` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `IP_START` bigint(18) DEFAULT NULL,
  `IP_END` bigint(18) DEFAULT NULL,
  PRIMARY KEY (`IPRULE_ID`,`RULE_IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_iprule_excl_mask`
--

DROP TABLE IF EXISTS `b_sec_iprule_excl_mask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_iprule_excl_mask` (
  `IPRULE_ID` int(11) NOT NULL,
  `RULE_MASK` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `LIKE_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREG_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IPRULE_ID`,`RULE_MASK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_iprule_incl_ip`
--

DROP TABLE IF EXISTS `b_sec_iprule_incl_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_iprule_incl_ip` (
  `IPRULE_ID` int(11) NOT NULL,
  `RULE_IP` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `IP_START` bigint(18) DEFAULT NULL,
  `IP_END` bigint(18) DEFAULT NULL,
  PRIMARY KEY (`IPRULE_ID`,`RULE_IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_iprule_incl_mask`
--

DROP TABLE IF EXISTS `b_sec_iprule_incl_mask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_iprule_incl_mask` (
  `IPRULE_ID` int(11) NOT NULL,
  `RULE_MASK` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `LIKE_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREG_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IPRULE_ID`,`RULE_MASK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_recovery_codes`
--

DROP TABLE IF EXISTS `b_sec_recovery_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_recovery_codes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `USED` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `USING_DATE` datetime DEFAULT NULL,
  `USING_IP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_sec_recovery_codes_user_id` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_redirect_url`
--

DROP TABLE IF EXISTS `b_sec_redirect_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_redirect_url` (
  `IS_SYSTEM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `URL` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMETER_NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_session`
--

DROP TABLE IF EXISTS `b_sec_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_session` (
  `SESSION_ID` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SESSION_DATA` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`SESSION_ID`),
  KEY `ix_b_sec_session_time` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_user`
--

DROP TABLE IF EXISTS `b_sec_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_user` (
  `USER_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SECRET` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYPE` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `ATTEMPTS` int(18) DEFAULT NULL,
  `INITIAL_DATE` datetime DEFAULT NULL,
  `SKIP_MANDATORY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DEACTIVATE_UNTIL` datetime DEFAULT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_virus`
--

DROP TABLE IF EXISTS `b_sec_virus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_virus` (
  `ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` datetime NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `INFO` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sec_white_list`
--

DROP TABLE IF EXISTS `b_sec_white_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sec_white_list` (
  `ID` int(11) NOT NULL,
  `WHITE_SUBSTR` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_security_sitecheck`
--

DROP TABLE IF EXISTS `b_security_sitecheck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_security_sitecheck` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEST_DATE` datetime DEFAULT NULL,
  `RESULTS` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_adv_autolog`
--

DROP TABLE IF EXISTS `b_seo_adv_autolog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_adv_autolog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CAMPAIGN_ID` int(11) NOT NULL,
  `CAMPAIGN_XML_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BANNER_ID` int(11) NOT NULL,
  `BANNER_XML_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CAUSE_CODE` int(11) DEFAULT '0',
  `SUCCESS` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_adv_autolog1` (`ENGINE_ID`),
  KEY `ix_b_seo_adv_autolog2` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_adv_banner`
--

DROP TABLE IF EXISTS `b_seo_adv_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_adv_banner` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `OWNER_ID` varchar(255) NOT NULL,
  `OWNER_NAME` varchar(255) NOT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `XML_ID` varchar(255) NOT NULL,
  `LAST_UPDATE` timestamp NULL DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `SETTINGS` text,
  `CAMPAIGN_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) DEFAULT NULL,
  `AUTO_QUANTITY_OFF` char(1) DEFAULT 'N',
  `AUTO_QUANTITY_ON` char(1) DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_adv_banner` (`ENGINE_ID`,`XML_ID`),
  KEY `ix_b_seo_adv_banner1` (`CAMPAIGN_ID`),
  KEY `ix_b_seo_adv_banner2` (`AUTO_QUANTITY_OFF`,`AUTO_QUANTITY_ON`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_adv_campaign`
--

DROP TABLE IF EXISTS `b_seo_adv_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_adv_campaign` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `ACTIVE` char(1) NOT NULL DEFAULT 'Y',
  `OWNER_ID` varchar(255) NOT NULL,
  `OWNER_NAME` varchar(255) NOT NULL,
  `XML_ID` varchar(255) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `LAST_UPDATE` timestamp NULL DEFAULT NULL,
  `SETTINGS` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_adv_campaign` (`ENGINE_ID`,`XML_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_adv_group`
--

DROP TABLE IF EXISTS `b_seo_adv_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_adv_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `OWNER_ID` varchar(255) NOT NULL,
  `OWNER_NAME` varchar(255) NOT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `XML_ID` varchar(255) NOT NULL,
  `LAST_UPDATE` timestamp NULL DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `SETTINGS` text,
  `CAMPAIGN_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_adv_group` (`ENGINE_ID`,`XML_ID`),
  KEY `ix_b_seo_adv_group1` (`CAMPAIGN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_adv_link`
--

DROP TABLE IF EXISTS `b_seo_adv_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_adv_link` (
  `LINK_TYPE` char(1) NOT NULL,
  `LINK_ID` int(18) NOT NULL,
  `BANNER_ID` int(11) NOT NULL,
  PRIMARY KEY (`LINK_TYPE`,`LINK_ID`,`BANNER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_adv_log`
--

DROP TABLE IF EXISTS `b_seo_adv_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_adv_log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `REQUEST_URI` varchar(100) NOT NULL,
  `REQUEST_DATA` text,
  `RESPONSE_TIME` float NOT NULL,
  `RESPONSE_STATUS` int(5) DEFAULT NULL,
  `RESPONSE_DATA` text,
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_adv_log1` (`ENGINE_ID`),
  KEY `ix_b_seo_adv_log2` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_adv_order`
--

DROP TABLE IF EXISTS `b_seo_adv_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_adv_order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CAMPAIGN_ID` int(11) NOT NULL,
  `BANNER_ID` int(11) NOT NULL,
  `ORDER_ID` int(11) NOT NULL,
  `SUM` float DEFAULT '0',
  `PROCESSED` char(1) DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_adv_order` (`ENGINE_ID`,`CAMPAIGN_ID`,`BANNER_ID`,`ORDER_ID`),
  KEY `ix_b_seo_adv_order1` (`ORDER_ID`,`PROCESSED`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_adv_region`
--

DROP TABLE IF EXISTS `b_seo_adv_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_adv_region` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `OWNER_ID` varchar(255) NOT NULL,
  `OWNER_NAME` varchar(255) NOT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `XML_ID` varchar(255) NOT NULL,
  `LAST_UPDATE` timestamp NULL DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `SETTINGS` text,
  `PARENT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_adv_region` (`ENGINE_ID`,`XML_ID`),
  KEY `ix_b_seo_adv_region1` (`PARENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_keywords`
--

DROP TABLE IF EXISTS `b_seo_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_keywords` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KEYWORDS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_keywords_url` (`URL`,`SITE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_search_engine`
--

DROP TABLE IF EXISTS `b_seo_search_engine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_search_engine` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SORT` int(5) DEFAULT '100',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CLIENT_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLIENT_SECRET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REDIRECT_URI` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_search_engine_code` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_sitemap`
--

DROP TABLE IF EXISTS `b_seo_sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_sitemap` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `DATE_RUN` datetime DEFAULT NULL,
  `SETTINGS` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_sitemap_entity`
--

DROP TABLE IF EXISTS `b_seo_sitemap_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_sitemap_entity` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `SITEMAP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_sitemap_entity_1` (`ENTITY_TYPE`,`ENTITY_ID`),
  KEY `ix_b_seo_sitemap_entity_2` (`SITEMAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_sitemap_iblock`
--

DROP TABLE IF EXISTS `b_seo_sitemap_iblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_sitemap_iblock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `SITEMAP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_sitemap_iblock_1` (`IBLOCK_ID`),
  KEY `ix_b_seo_sitemap_iblock_2` (`SITEMAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_sitemap_runtime`
--

DROP TABLE IF EXISTS `b_seo_sitemap_runtime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_sitemap_runtime` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PID` int(11) NOT NULL,
  `PROCESSED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ITEM_PATH` varchar(700) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ITEM_ID` int(11) DEFAULT NULL,
  `ITEM_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `ACTIVE_ELEMENT` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `ix_seo_sitemap_runtime1` (`PID`,`PROCESSED`,`ITEM_TYPE`,`ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_seo_yandex_direct_stat`
--

DROP TABLE IF EXISTS `b_seo_yandex_direct_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_seo_yandex_direct_stat` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CAMPAIGN_ID` int(11) NOT NULL,
  `BANNER_ID` int(11) NOT NULL,
  `DATE_DAY` date NOT NULL,
  `CURRENCY` char(3) DEFAULT NULL,
  `SUM` float DEFAULT '0',
  `SUM_SEARCH` float DEFAULT '0',
  `SUM_CONTEXT` float DEFAULT '0',
  `CLICKS` int(7) DEFAULT '0',
  `CLICKS_SEARCH` int(7) DEFAULT '0',
  `CLICKS_CONTEXT` int(7) DEFAULT '0',
  `SHOWS` int(7) DEFAULT '0',
  `SHOWS_SEARCH` int(7) DEFAULT '0',
  `SHOWS_CONTEXT` int(7) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_seo_yandex_direct_stat` (`BANNER_ID`,`DATE_DAY`),
  KEY `ix_seo_yandex_direct_stat1` (`CAMPAIGN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_short_uri`
--

DROP TABLE IF EXISTS `b_short_uri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_short_uri` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `URI` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `URI_CRC` int(18) NOT NULL,
  `SHORT_URI` varbinary(250) NOT NULL,
  `SHORT_URI_CRC` int(18) NOT NULL,
  `STATUS` int(18) NOT NULL DEFAULT '301',
  `MODIFIED` datetime NOT NULL,
  `LAST_USED` datetime DEFAULT NULL,
  `NUMBER_USED` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ux_b_short_uri_1` (`SHORT_URI_CRC`),
  KEY `ux_b_short_uri_2` (`URI_CRC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_site_template`
--

DROP TABLE IF EXISTS `b_site_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_site_template` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `TEMPLATE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_B_SITE_TEMPLATE` (`SITE_ID`,`CONDITION`,`TEMPLATE`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_smile`
--

DROP TABLE IF EXISTS `b_smile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_smile` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `SET_ID` int(18) NOT NULL DEFAULT '0',
  `SORT` int(10) NOT NULL DEFAULT '150',
  `TYPING` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLICKABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IMAGE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE_WIDTH` int(11) NOT NULL DEFAULT '0',
  `IMAGE_HEIGHT` int(11) NOT NULL DEFAULT '0',
  `HIDDEN` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IMAGE_DEFINITION` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SD',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_smile_lang`
--

DROP TABLE IF EXISTS `b_smile_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_smile_lang` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `SID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_SMILE_SL` (`TYPE`,`SID`,`LID`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_smile_set`
--

DROP TABLE IF EXISTS `b_smile_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_smile_set` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `STRING_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(10) NOT NULL DEFAULT '150',
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'G',
  `PARENT_ID` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_socialservices_contact`
--

DROP TABLE IF EXISTS `b_socialservices_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_socialservices_contact` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `USER_ID` int(11) NOT NULL,
  `CONTACT_USER_ID` int(11) DEFAULT NULL,
  `CONTACT_XML_ID` int(11) DEFAULT NULL,
  `CONTACT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONTACT_LAST_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONTACT_PHOTO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIFY` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `LAST_AUTHORIZE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_socialservices_contact1` (`USER_ID`),
  KEY `ix_b_socialservices_contact2` (`CONTACT_USER_ID`),
  KEY `ix_b_socialservices_contact3` (`TIMESTAMP_X`),
  KEY `ix_b_socialservices_contact4` (`LAST_AUTHORIZE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_socialservices_contact_connect`
--

DROP TABLE IF EXISTS `b_socialservices_contact_connect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_socialservices_contact_connect` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `CONTACT_ID` int(11) DEFAULT NULL,
  `LINK_ID` int(11) DEFAULT NULL,
  `CONTACT_PROFILE_ID` int(11) NOT NULL,
  `CONTACT_PORTAL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONNECT_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT 'P',
  `LAST_AUTHORIZE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_socialservices_contact_connect1` (`CONTACT_ID`),
  KEY `ix_b_socialservices_contact_connect2` (`LINK_ID`),
  KEY `ix_b_socialservices_contact_connect3` (`LAST_AUTHORIZE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_socialservices_message`
--

DROP TABLE IF EXISTS `b_socialservices_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_socialservices_message` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `SOCSERV_USER_ID` int(11) NOT NULL,
  `PROVIDER` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INSERT_DATE` datetime DEFAULT NULL,
  `SUCCES_SENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_socialservices_user`
--

DROP TABLE IF EXISTS `b_socialservices_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_socialservices_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PHOTO` int(11) DEFAULT NULL,
  `EXTERNAL_AUTH_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `XML_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CAN_DELETE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `PERSONAL_WWW` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERMISSIONS` varchar(555) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OATOKEN` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OATOKEN_EXPIRES` int(11) DEFAULT NULL,
  `OASECRET` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REFRESH_TOKEN` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEND_ACTIVITY` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SITE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INITIALIZED` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_SOCIALSERVICES_USER` (`XML_ID`,`EXTERNAL_AUTH_ID`),
  KEY `IX_B_SOCIALSERVICES_US_1` (`USER_ID`),
  KEY `IX_B_SOCIALSERVICES_US_3` (`LOGIN`)
) ENGINE=InnoDB AUTO_INCREMENT=2338 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_socialservices_user_link`
--

DROP TABLE IF EXISTS `b_socialservices_user_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_socialservices_user_link` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `SOCSERV_USER_ID` int(11) NOT NULL,
  `LINK_USER_ID` int(11) DEFAULT NULL,
  `LINK_UID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LINK_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINK_LAST_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINK_PICTURE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINK_EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP_X` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_socialservices_user_link_5` (`SOCSERV_USER_ID`),
  KEY `ix_b_socialservices_user_link_6` (`LINK_USER_ID`,`TIMESTAMP_X`),
  KEY `ix_b_socialservices_user_link_7` (`LINK_UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_adv`
--

DROP TABLE IF EXISTS `b_stat_adv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_adv` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `REFERER1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REFERER2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COST` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `REVENUE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `EVENTS_VIEW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GUESTS` int(18) NOT NULL DEFAULT '0',
  `NEW_GUESTS` int(18) NOT NULL DEFAULT '0',
  `FAVORITES` int(18) NOT NULL DEFAULT '0',
  `C_HOSTS` int(18) NOT NULL DEFAULT '0',
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `HITS` int(18) NOT NULL DEFAULT '0',
  `DATE_FIRST` datetime DEFAULT NULL,
  `DATE_LAST` datetime DEFAULT NULL,
  `GUESTS_BACK` int(18) NOT NULL DEFAULT '0',
  `FAVORITES_BACK` int(18) NOT NULL DEFAULT '0',
  `HOSTS_BACK` int(18) NOT NULL DEFAULT '0',
  `SESSIONS_BACK` int(18) NOT NULL DEFAULT '0',
  `HITS_BACK` int(18) NOT NULL DEFAULT '0',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `PRIORITY` int(18) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `IX_REFERER1` (`REFERER1`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_adv_day`
--

DROP TABLE IF EXISTS `b_stat_adv_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_adv_day` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ADV_ID` int(18) NOT NULL DEFAULT '0',
  `DATE_STAT` date DEFAULT NULL,
  `GUESTS` int(18) NOT NULL DEFAULT '0',
  `GUESTS_DAY` int(18) NOT NULL DEFAULT '0',
  `NEW_GUESTS` int(18) NOT NULL DEFAULT '0',
  `FAVORITES` int(18) NOT NULL DEFAULT '0',
  `C_HOSTS` int(18) NOT NULL DEFAULT '0',
  `C_HOSTS_DAY` int(18) NOT NULL DEFAULT '0',
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `HITS` int(18) NOT NULL DEFAULT '0',
  `GUESTS_BACK` int(18) NOT NULL DEFAULT '0',
  `GUESTS_DAY_BACK` int(18) NOT NULL DEFAULT '0',
  `FAVORITES_BACK` int(18) NOT NULL DEFAULT '0',
  `HOSTS_BACK` int(18) NOT NULL DEFAULT '0',
  `HOSTS_DAY_BACK` int(18) NOT NULL DEFAULT '0',
  `SESSIONS_BACK` int(18) NOT NULL DEFAULT '0',
  `HITS_BACK` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_ADV_ID_DATE_STAT` (`ADV_ID`,`DATE_STAT`),
  KEY `IX_DATE_STAT` (`DATE_STAT`)
) ENGINE=InnoDB AUTO_INCREMENT=1684 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_adv_event`
--

DROP TABLE IF EXISTS `b_stat_adv_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_adv_event` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ADV_ID` int(18) DEFAULT '0',
  `EVENT_ID` int(18) DEFAULT '0',
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `COUNTER_BACK` int(18) NOT NULL DEFAULT '0',
  `MONEY` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `MONEY_BACK` decimal(18,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`ID`),
  KEY `IX_ADV_EVENT_ID` (`ADV_ID`,`EVENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_adv_event_day`
--

DROP TABLE IF EXISTS `b_stat_adv_event_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_adv_event_day` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ADV_ID` int(18) DEFAULT '0',
  `EVENT_ID` int(18) DEFAULT '0',
  `DATE_STAT` date DEFAULT NULL,
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `COUNTER_BACK` int(18) NOT NULL DEFAULT '0',
  `MONEY` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `MONEY_BACK` decimal(18,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`ID`),
  KEY `IX_ADV_ID_EVENT_ID_DATE_STAT` (`ADV_ID`,`EVENT_ID`,`DATE_STAT`),
  KEY `IX_DATE_STAT` (`DATE_STAT`)
) ENGINE=InnoDB AUTO_INCREMENT=695 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_adv_guest`
--

DROP TABLE IF EXISTS `b_stat_adv_guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_adv_guest` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADV_ID` int(11) NOT NULL DEFAULT '0',
  `BACK` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `GUEST_ID` int(11) NOT NULL DEFAULT '0',
  `DATE_GUEST_HIT` datetime DEFAULT NULL,
  `DATE_HOST_HIT` datetime DEFAULT NULL,
  `SESSION_ID` int(11) NOT NULL DEFAULT '0',
  `IP` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IP_NUMBER` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ADV_ID_GUEST` (`ADV_ID`,`GUEST_ID`),
  KEY `IX_ADV_ID_IP_NUMBER` (`ADV_ID`,`IP_NUMBER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_adv_page`
--

DROP TABLE IF EXISTS `b_stat_adv_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_adv_page` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ADV_ID` int(18) NOT NULL DEFAULT '0',
  `PAGE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `C_TYPE` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'TO',
  PRIMARY KEY (`ID`),
  KEY `IX_ADV_ID_TYPE` (`ADV_ID`,`C_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_adv_searcher`
--

DROP TABLE IF EXISTS `b_stat_adv_searcher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_adv_searcher` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ADV_ID` int(18) NOT NULL,
  `SEARCHER_ID` int(18) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_browser`
--

DROP TABLE IF EXISTS `b_stat_browser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_browser` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_AGENT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_city`
--

DROP TABLE IF EXISTS `b_stat_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_city` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `COUNTRY_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `REGION` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `NEW_GUESTS` int(18) NOT NULL DEFAULT '0',
  `HITS` int(18) NOT NULL DEFAULT '0',
  `C_EVENTS` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `UX_B_STAT_CITY` (`COUNTRY_ID`,`REGION`(50),`NAME`(50)),
  KEY `IX_B_STAT_CITY_XML_ID` (`XML_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_city_day`
--

DROP TABLE IF EXISTS `b_stat_city_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_city_day` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CITY_ID` int(18) NOT NULL,
  `DATE_STAT` date NOT NULL,
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `NEW_GUESTS` int(18) NOT NULL DEFAULT '0',
  `HITS` int(18) NOT NULL DEFAULT '0',
  `C_EVENTS` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_B_STAT_CITY_DAY_1` (`CITY_ID`,`DATE_STAT`),
  KEY `IX_B_STAT_CITY_DAY_2` (`DATE_STAT`)
) ENGINE=InnoDB AUTO_INCREMENT=22957 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_city_ip`
--

DROP TABLE IF EXISTS `b_stat_city_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_city_ip` (
  `START_IP` bigint(18) NOT NULL,
  `END_IP` bigint(18) NOT NULL,
  `COUNTRY_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CITY_ID` int(18) NOT NULL,
  PRIMARY KEY (`START_IP`),
  KEY `IX_B_STAT_CITY_IP_END_IP` (`END_IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_country`
--

DROP TABLE IF EXISTS `b_stat_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_country` (
  `ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `NEW_GUESTS` int(18) NOT NULL DEFAULT '0',
  `HITS` int(18) NOT NULL DEFAULT '0',
  `C_EVENTS` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_country_day`
--

DROP TABLE IF EXISTS `b_stat_country_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_country_day` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `COUNTRY_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_STAT` date DEFAULT NULL,
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `NEW_GUESTS` int(18) NOT NULL DEFAULT '0',
  `HITS` int(18) NOT NULL DEFAULT '0',
  `C_EVENTS` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_COUNTRY_ID_DATE_STAT` (`COUNTRY_ID`,`DATE_STAT`)
) ENGINE=InnoDB AUTO_INCREMENT=22945 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_day`
--

DROP TABLE IF EXISTS `b_stat_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_day` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_STAT` date DEFAULT NULL,
  `HITS` int(18) NOT NULL DEFAULT '0',
  `C_HOSTS` int(18) NOT NULL DEFAULT '0',
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `C_EVENTS` int(18) NOT NULL DEFAULT '0',
  `GUESTS` int(18) NOT NULL DEFAULT '0',
  `NEW_GUESTS` int(18) NOT NULL DEFAULT '0',
  `FAVORITES` int(18) NOT NULL DEFAULT '0',
  `TOTAL_HOSTS` int(18) NOT NULL DEFAULT '0',
  `AM_AVERAGE_TIME` decimal(18,2) NOT NULL DEFAULT '0.00',
  `AM_1` int(18) NOT NULL DEFAULT '0',
  `AM_1_3` int(18) NOT NULL DEFAULT '0',
  `AM_3_6` int(18) NOT NULL DEFAULT '0',
  `AM_6_9` int(18) NOT NULL DEFAULT '0',
  `AM_9_12` int(18) NOT NULL DEFAULT '0',
  `AM_12_15` int(18) NOT NULL DEFAULT '0',
  `AM_15_18` int(18) NOT NULL DEFAULT '0',
  `AM_18_21` int(18) NOT NULL DEFAULT '0',
  `AM_21_24` int(18) NOT NULL DEFAULT '0',
  `AM_24` int(18) NOT NULL DEFAULT '0',
  `AH_AVERAGE_HITS` decimal(18,2) NOT NULL DEFAULT '0.00',
  `AH_1` int(18) NOT NULL DEFAULT '0',
  `AH_2_5` int(18) NOT NULL DEFAULT '0',
  `AH_6_9` int(18) NOT NULL DEFAULT '0',
  `AH_10_13` int(18) NOT NULL DEFAULT '0',
  `AH_14_17` int(18) NOT NULL DEFAULT '0',
  `AH_18_21` int(18) NOT NULL DEFAULT '0',
  `AH_22_25` int(18) NOT NULL DEFAULT '0',
  `AH_26_29` int(18) NOT NULL DEFAULT '0',
  `AH_30_33` int(18) NOT NULL DEFAULT '0',
  `AH_34` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_23` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_12` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_DATE_STAT` (`DATE_STAT`)
) ENGINE=InnoDB AUTO_INCREMENT=562 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_day_site`
--

DROP TABLE IF EXISTS `b_stat_day_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_day_site` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_STAT` date DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `HITS` int(18) NOT NULL DEFAULT '0',
  `C_HOSTS` int(18) NOT NULL DEFAULT '0',
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `C_EVENTS` int(18) NOT NULL DEFAULT '0',
  `GUESTS` int(18) NOT NULL DEFAULT '0',
  `NEW_GUESTS` int(18) NOT NULL DEFAULT '0',
  `FAVORITES` int(18) NOT NULL DEFAULT '0',
  `TOTAL_HOSTS` int(18) NOT NULL DEFAULT '0',
  `AM_AVERAGE_TIME` decimal(18,2) NOT NULL DEFAULT '0.00',
  `AM_1` int(18) NOT NULL DEFAULT '0',
  `AM_1_3` int(18) NOT NULL DEFAULT '0',
  `AM_3_6` int(18) NOT NULL DEFAULT '0',
  `AM_6_9` int(18) NOT NULL DEFAULT '0',
  `AM_9_12` int(18) NOT NULL DEFAULT '0',
  `AM_12_15` int(18) NOT NULL DEFAULT '0',
  `AM_15_18` int(18) NOT NULL DEFAULT '0',
  `AM_18_21` int(18) NOT NULL DEFAULT '0',
  `AM_21_24` int(18) NOT NULL DEFAULT '0',
  `AM_24` int(18) NOT NULL DEFAULT '0',
  `AH_AVERAGE_HITS` decimal(18,2) NOT NULL DEFAULT '0.00',
  `AH_1` int(18) NOT NULL DEFAULT '0',
  `AH_2_5` int(18) NOT NULL DEFAULT '0',
  `AH_6_9` int(18) NOT NULL DEFAULT '0',
  `AH_10_13` int(18) NOT NULL DEFAULT '0',
  `AH_14_17` int(18) NOT NULL DEFAULT '0',
  `AH_18_21` int(18) NOT NULL DEFAULT '0',
  `AH_22_25` int(18) NOT NULL DEFAULT '0',
  `AH_26_29` int(18) NOT NULL DEFAULT '0',
  `AH_30_33` int(18) NOT NULL DEFAULT '0',
  `AH_34` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_HOST_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_GUEST_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_NEW_GUEST_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_SESSION_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_HIT_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_EVENT_23` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_0` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_1` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_2` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_3` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_4` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_5` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_6` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_7` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_8` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_9` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_10` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_11` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_12` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_13` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_14` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_15` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_16` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_17` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_18` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_19` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_20` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_21` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_22` int(18) NOT NULL DEFAULT '0',
  `HOUR_FAVORITE_23` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HOST_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_NEW_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_SESSION_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_HIT_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_EVENT_6` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_0` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_1` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_2` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_3` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_4` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_5` int(18) NOT NULL DEFAULT '0',
  `WEEKDAY_FAVORITE_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_HOST_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_GUEST_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_NEW_GUEST_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_SESSION_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_HIT_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_EVENT_12` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_1` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_2` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_3` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_4` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_5` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_6` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_7` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_8` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_9` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_10` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_11` int(18) NOT NULL DEFAULT '0',
  `MONTH_FAVORITE_12` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SITE_ID_DATE_STAT` (`SITE_ID`,`DATE_STAT`)
) ENGINE=InnoDB AUTO_INCREMENT=1112 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_ddl`
--

DROP TABLE IF EXISTS `b_stat_ddl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_ddl` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `SQL_TEXT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_event`
--

DROP TABLE IF EXISTS `b_stat_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_event` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `EVENT1` varchar(166) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EVENT2` varchar(166) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MONEY` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `DATE_ENTER` datetime DEFAULT NULL,
  `DATE_CLEANUP` datetime DEFAULT NULL,
  `C_SORT` int(18) DEFAULT '100',
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `ADV_VISIBLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `KEEP_DAYS` int(18) DEFAULT NULL,
  `DYNAMIC_KEEP_DAYS` int(18) DEFAULT NULL,
  `DIAGRAM_DEFAULT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IX_EVENT1_EVENT2` (`EVENT1`,`EVENT2`),
  KEY `IX_B_STAT_EVENT_2` (`KEEP_DAYS`,`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_event_day`
--

DROP TABLE IF EXISTS `b_stat_event_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_event_day` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_STAT` date DEFAULT NULL,
  `DATE_LAST` datetime DEFAULT NULL,
  `EVENT_ID` int(18) NOT NULL DEFAULT '0',
  `MONEY` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_EVENT_ID_DATE_STAT` (`EVENT_ID`,`DATE_STAT`)
) ENGINE=InnoDB AUTO_INCREMENT=830 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_event_list`
--

DROP TABLE IF EXISTS `b_stat_event_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_event_list` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `EVENT_ID` int(18) NOT NULL DEFAULT '0',
  `EVENT3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MONEY` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `DATE_ENTER` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `REFERER_URL` text COLLATE utf8_unicode_ci,
  `URL` text COLLATE utf8_unicode_ci,
  `REDIRECT_URL` text COLLATE utf8_unicode_ci,
  `SESSION_ID` int(18) DEFAULT NULL,
  `GUEST_ID` int(18) DEFAULT NULL,
  `ADV_ID` int(18) DEFAULT NULL,
  `ADV_BACK` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HIT_ID` int(18) DEFAULT NULL,
  `COUNTRY_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KEEP_DAYS` int(18) DEFAULT NULL,
  `CHARGEBACK` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REFERER_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_GUEST_ID` (`GUEST_ID`),
  KEY `IX_B_STAT_EVENT_LIST_2` (`EVENT_ID`,`DATE_ENTER`),
  KEY `IX_B_STAT_EVENT_LIST_3` (`KEEP_DAYS`,`DATE_ENTER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_guest`
--

DROP TABLE IF EXISTS `b_stat_guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_guest` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FAVORITES` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `C_EVENTS` int(18) NOT NULL DEFAULT '0',
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `HITS` int(18) NOT NULL DEFAULT '0',
  `REPAIR` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `FIRST_SESSION_ID` int(18) DEFAULT NULL,
  `FIRST_DATE` datetime DEFAULT NULL,
  `FIRST_URL_FROM` text COLLATE utf8_unicode_ci,
  `FIRST_URL_TO` text COLLATE utf8_unicode_ci,
  `FIRST_URL_TO_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `FIRST_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIRST_ADV_ID` int(18) DEFAULT NULL,
  `FIRST_REFERER1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIRST_REFERER2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIRST_REFERER3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_SESSION_ID` int(18) DEFAULT NULL,
  `LAST_DATE` datetime DEFAULT NULL,
  `LAST_USER_ID` int(18) DEFAULT NULL,
  `LAST_USER_AUTH` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_URL_LAST` text COLLATE utf8_unicode_ci,
  `LAST_URL_LAST_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `LAST_USER_AGENT` text COLLATE utf8_unicode_ci,
  `LAST_IP` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_COOKIE` text COLLATE utf8_unicode_ci,
  `LAST_LANGUAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_ADV_ID` int(18) DEFAULT NULL,
  `LAST_ADV_BACK` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `LAST_REFERER1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_REFERER2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_REFERER3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_COUNTRY_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_CITY_ID` int(18) DEFAULT NULL,
  `LAST_CITY_INFO` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_LAST_DATE` (`LAST_DATE`)
) ENGINE=InnoDB AUTO_INCREMENT=1561878 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_hit`
--

DROP TABLE IF EXISTS `b_stat_hit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_hit` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `SESSION_ID` int(18) NOT NULL DEFAULT '0',
  `DATE_HIT` datetime DEFAULT NULL,
  `GUEST_ID` int(18) DEFAULT NULL,
  `NEW_GUEST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `USER_ID` int(18) DEFAULT NULL,
  `USER_AUTH` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  `URL_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `URL_FROM` text COLLATE utf8_unicode_ci,
  `IP` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `METHOD` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COOKIES` text COLLATE utf8_unicode_ci,
  `USER_AGENT` text COLLATE utf8_unicode_ci,
  `STOP_LIST_ID` int(18) DEFAULT NULL,
  `COUNTRY_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CITY_ID` int(18) DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_DATE_HIT` (`DATE_HIT`)
) ENGINE=InnoDB AUTO_INCREMENT=46623480 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_page`
--

DROP TABLE IF EXISTS `b_stat_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_page` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_STAT` date NOT NULL DEFAULT '0000-00-00',
  `DIR` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `URL` text COLLATE utf8_unicode_ci NOT NULL,
  `URL_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `URL_HASH` int(32) DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COUNTER` int(11) NOT NULL DEFAULT '0',
  `ENTER_COUNTER` int(18) NOT NULL DEFAULT '0',
  `EXIT_COUNTER` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_DATE_STAT` (`DATE_STAT`),
  KEY `IX_URL_HASH` (`URL_HASH`)
) ENGINE=InnoDB AUTO_INCREMENT=4455153 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_page_adv`
--

DROP TABLE IF EXISTS `b_stat_page_adv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_page_adv` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_STAT` date DEFAULT NULL,
  `PAGE_ID` int(18) NOT NULL DEFAULT '0',
  `ADV_ID` int(18) NOT NULL DEFAULT '0',
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `ENTER_COUNTER` int(18) NOT NULL DEFAULT '0',
  `EXIT_COUNTER` int(18) NOT NULL DEFAULT '0',
  `COUNTER_BACK` int(18) NOT NULL DEFAULT '0',
  `ENTER_COUNTER_BACK` int(18) NOT NULL DEFAULT '0',
  `EXIT_COUNTER_BACK` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_PAGE_ID_ADV_ID` (`PAGE_ID`,`ADV_ID`),
  KEY `IX_DATE_STAT` (`DATE_STAT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_path`
--

DROP TABLE IF EXISTS `b_stat_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_path` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `PATH_ID` int(32) NOT NULL DEFAULT '0',
  `PARENT_PATH_ID` int(32) DEFAULT NULL,
  `DATE_STAT` date DEFAULT NULL,
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `COUNTER_ABNORMAL` int(18) NOT NULL DEFAULT '0',
  `COUNTER_FULL_PATH` int(18) NOT NULL DEFAULT '0',
  `PAGES` text COLLATE utf8_unicode_ci,
  `FIRST_PAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIRST_PAGE_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `FIRST_PAGE_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREV_PAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREV_PAGE_HASH` int(32) DEFAULT NULL,
  `LAST_PAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_PAGE_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `LAST_PAGE_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_PAGE_HASH` int(32) DEFAULT NULL,
  `STEPS` int(18) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `IX_PATH_ID_DATE_STAT` (`PATH_ID`,`DATE_STAT`),
  KEY `IX_PREV_PAGE_HASH_LAST_PAGE_HASH` (`PREV_PAGE_HASH`,`LAST_PAGE_HASH`),
  KEY `IX_DATE_STAT` (`DATE_STAT`)
) ENGINE=InnoDB AUTO_INCREMENT=2303402 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_path_adv`
--

DROP TABLE IF EXISTS `b_stat_path_adv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_path_adv` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ADV_ID` int(18) NOT NULL DEFAULT '0',
  `PATH_ID` int(32) NOT NULL DEFAULT '0',
  `DATE_STAT` date DEFAULT NULL,
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `COUNTER_BACK` int(18) NOT NULL DEFAULT '0',
  `COUNTER_FULL_PATH` int(18) NOT NULL DEFAULT '0',
  `COUNTER_FULL_PATH_BACK` int(18) NOT NULL DEFAULT '0',
  `STEPS` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_PATH_ID_ADV_ID_DATE_STAT` (`PATH_ID`,`ADV_ID`,`DATE_STAT`),
  KEY `IX_DATE_STAT` (`DATE_STAT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_path_cache`
--

DROP TABLE IF EXISTS `b_stat_path_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_path_cache` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `SESSION_ID` int(18) NOT NULL DEFAULT '0',
  `DATE_HIT` datetime DEFAULT NULL,
  `PATH_ID` int(32) DEFAULT NULL,
  `PATH_PAGES` text COLLATE utf8_unicode_ci,
  `PATH_FIRST_PAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATH_FIRST_PAGE_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PATH_FIRST_PAGE_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATH_LAST_PAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATH_LAST_PAGE_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PATH_LAST_PAGE_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATH_STEPS` int(18) NOT NULL DEFAULT '1',
  `IS_LAST_PAGE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IX_SESSION_ID` (`SESSION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12565163 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_phrase_list`
--

DROP TABLE IF EXISTS `b_stat_phrase_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_phrase_list` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_HIT` datetime DEFAULT NULL,
  `SEARCHER_ID` int(18) DEFAULT NULL,
  `REFERER_ID` int(18) DEFAULT NULL,
  `PHRASE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `URL_FROM` text COLLATE utf8_unicode_ci,
  `URL_TO` text COLLATE utf8_unicode_ci,
  `URL_TO_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SESSION_ID` int(18) DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_DATE_HIT` (`DATE_HIT`),
  KEY `IX_URL_TO_SEARCHER_ID` (`URL_TO`(100),`SEARCHER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_referer`
--

DROP TABLE IF EXISTS `b_stat_referer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_referer` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_FIRST` datetime DEFAULT NULL,
  `DATE_LAST` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SITE_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SESSIONS` int(18) NOT NULL DEFAULT '0',
  `HITS` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_SITE_NAME` (`SITE_NAME`),
  KEY `IX_B_STAT_REFERER_2` (`DATE_LAST`,`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4482 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_referer_list`
--

DROP TABLE IF EXISTS `b_stat_referer_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_referer_list` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `REFERER_ID` int(18) DEFAULT NULL,
  `DATE_HIT` datetime DEFAULT NULL,
  `PROTOCOL` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `URL_FROM` text COLLATE utf8_unicode_ci NOT NULL,
  `URL_TO` text COLLATE utf8_unicode_ci,
  `URL_TO_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SESSION_ID` int(18) DEFAULT NULL,
  `ADV_ID` int(18) DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_DATE_HIT` (`DATE_HIT`),
  KEY `IX_SITE_NAME` (`SITE_NAME`(100),`URL_TO`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_searcher`
--

DROP TABLE IF EXISTS `b_stat_searcher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_searcher` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_CLEANUP` datetime DEFAULT NULL,
  `TOTAL_HITS` int(18) NOT NULL DEFAULT '0',
  `SAVE_STATISTIC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `USER_AGENT` text COLLATE utf8_unicode_ci,
  `DIAGRAM_DEFAULT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HIT_KEEP_DAYS` int(18) DEFAULT NULL,
  `DYNAMIC_KEEP_DAYS` int(18) DEFAULT NULL,
  `PHRASES` int(18) NOT NULL DEFAULT '0',
  `PHRASES_HITS` int(18) NOT NULL DEFAULT '0',
  `CHECK_ACTIVITY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IX_B_STAT_SEARCHER_1` (`HIT_KEEP_DAYS`,`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_searcher_day`
--

DROP TABLE IF EXISTS `b_stat_searcher_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_searcher_day` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_STAT` date DEFAULT NULL,
  `DATE_LAST` datetime DEFAULT NULL,
  `SEARCHER_ID` int(18) NOT NULL DEFAULT '0',
  `TOTAL_HITS` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_SEARCHER_ID_DATE_STAT` (`SEARCHER_ID`,`DATE_STAT`)
) ENGINE=InnoDB AUTO_INCREMENT=6678 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_searcher_hit`
--

DROP TABLE IF EXISTS `b_stat_searcher_hit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_searcher_hit` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_HIT` datetime DEFAULT NULL,
  `SEARCHER_ID` int(18) NOT NULL DEFAULT '0',
  `URL` text COLLATE utf8_unicode_ci NOT NULL,
  `URL_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IP` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_AGENT` text COLLATE utf8_unicode_ci,
  `HIT_KEEP_DAYS` int(18) DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_STAT_SEARCHER_HIT_1` (`SEARCHER_ID`,`DATE_HIT`),
  KEY `IX_B_STAT_SEARCHER_HIT_2` (`HIT_KEEP_DAYS`,`DATE_HIT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_searcher_params`
--

DROP TABLE IF EXISTS `b_stat_searcher_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_searcher_params` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `SEARCHER_ID` int(18) NOT NULL DEFAULT '0',
  `DOMAIN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VARIABLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHAR_SET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SEARCHER_DOMAIN` (`SEARCHER_ID`,`DOMAIN`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_session`
--

DROP TABLE IF EXISTS `b_stat_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_session` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `GUEST_ID` int(18) DEFAULT NULL,
  `NEW_GUEST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `USER_ID` int(18) DEFAULT NULL,
  `USER_AUTH` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `C_EVENTS` int(18) NOT NULL DEFAULT '0',
  `HITS` int(18) NOT NULL DEFAULT '0',
  `FAVORITES` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `URL_FROM` text COLLATE utf8_unicode_ci,
  `URL_TO` text COLLATE utf8_unicode_ci,
  `URL_TO_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `URL_LAST` text COLLATE utf8_unicode_ci,
  `URL_LAST_404` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `USER_AGENT` text COLLATE utf8_unicode_ci,
  `DATE_STAT` date DEFAULT NULL,
  `DATE_FIRST` datetime DEFAULT NULL,
  `DATE_LAST` datetime DEFAULT NULL,
  `IP_FIRST` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IP_FIRST_NUMBER` bigint(20) DEFAULT NULL,
  `IP_LAST` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IP_LAST_NUMBER` bigint(20) DEFAULT NULL,
  `FIRST_HIT_ID` int(18) DEFAULT NULL,
  `LAST_HIT_ID` int(18) DEFAULT NULL,
  `PHPSESSID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADV_ID` int(18) DEFAULT NULL,
  `ADV_BACK` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REFERER1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REFERER2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REFERER3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STOP_LIST_ID` int(18) DEFAULT NULL,
  `COUNTRY_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CITY_ID` int(18) DEFAULT NULL,
  `FIRST_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_IP_FIRST_NUMBER_DATE_STAT` (`IP_FIRST_NUMBER`,`DATE_STAT`),
  KEY `IX_B_STAT_SESSION_4` (`USER_ID`,`DATE_STAT`),
  KEY `IX_DATE_STAT` (`DATE_STAT`),
  KEY `IX_B_STAT_SESSION_5` (`DATE_LAST`),
  KEY `IX_B_STAT_SESSION_6` (`GUEST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2039231 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stat_session_data`
--

DROP TABLE IF EXISTS `b_stat_session_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stat_session_data` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `DATE_FIRST` datetime DEFAULT NULL,
  `DATE_LAST` datetime DEFAULT NULL,
  `GUEST_MD5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SESS_SESSION_ID` int(18) DEFAULT NULL,
  `SESSION_DATA` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_GUEST_MD5` (`GUEST_MD5`)
) ENGINE=InnoDB AUTO_INCREMENT=2086395 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sticker`
--

DROP TABLE IF EXISTS `b_sticker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sticker` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAGE_URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PAGE_TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `MODIFIED_BY` int(18) NOT NULL,
  `CREATED_BY` int(18) NOT NULL,
  `PERSONAL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CONTENT` text COLLATE utf8_unicode_ci,
  `POS_TOP` int(11) DEFAULT NULL,
  `POS_LEFT` int(11) DEFAULT NULL,
  `WIDTH` int(11) DEFAULT NULL,
  `HEIGHT` int(11) DEFAULT NULL,
  `COLOR` int(11) DEFAULT NULL,
  `COLLAPSED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `COMPLETED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CLOSED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DELETED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `MARKER_TOP` int(11) DEFAULT NULL,
  `MARKER_LEFT` int(11) DEFAULT NULL,
  `MARKER_WIDTH` int(11) DEFAULT NULL,
  `MARKER_HEIGHT` int(11) DEFAULT NULL,
  `MARKER_ADJUST` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_sticker_group_task`
--

DROP TABLE IF EXISTS `b_sticker_group_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_sticker_group_task` (
  `GROUP_ID` int(11) NOT NULL,
  `TASK_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_stop_list`
--

DROP TABLE IF EXISTS `b_stop_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_stop_list` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_START` datetime DEFAULT NULL,
  `DATE_END` datetime DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SAVE_STATISTIC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IP_1` int(18) DEFAULT NULL,
  `IP_2` int(18) DEFAULT NULL,
  `IP_3` int(18) DEFAULT NULL,
  `IP_4` int(18) DEFAULT NULL,
  `MASK_1` int(18) DEFAULT NULL,
  `MASK_2` int(18) DEFAULT NULL,
  `MASK_3` int(18) DEFAULT NULL,
  `MASK_4` int(18) DEFAULT NULL,
  `USER_AGENT` text COLLATE utf8_unicode_ci,
  `USER_AGENT_IS_NULL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `URL_TO` text COLLATE utf8_unicode_ci,
  `URL_FROM` text COLLATE utf8_unicode_ci,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `MESSAGE_LID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `URL_REDIRECT` text COLLATE utf8_unicode_ci,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `TEST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_subscription`
--

DROP TABLE IF EXISTS `b_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_subscription` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `CONFIRM_CODE` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONFIRMED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_CONFIRM` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_SUBSCRIPTION_EMAIL` (`EMAIL`,`USER_ID`),
  KEY `IX_DATE_CONFIRM` (`CONFIRMED`,`DATE_CONFIRM`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_subscription_rubric`
--

DROP TABLE IF EXISTS `b_subscription_rubric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_subscription_rubric` (
  `SUBSCRIPTION_ID` int(11) NOT NULL,
  `LIST_RUBRIC_ID` int(11) NOT NULL,
  UNIQUE KEY `UK_SUBSCRIPTION_RUBRIC` (`SUBSCRIPTION_ID`,`LIST_RUBRIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_task`
--

DROP TABLE IF EXISTS `b_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_task` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LETTER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SYS` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BINDING` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'module',
  PRIMARY KEY (`ID`),
  KEY `ix_task` (`MODULE_ID`,`BINDING`,`LETTER`,`SYS`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_task_operation`
--

DROP TABLE IF EXISTS `b_task_operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_task_operation` (
  `TASK_ID` int(18) NOT NULL,
  `OPERATION_ID` int(18) NOT NULL,
  PRIMARY KEY (`TASK_ID`,`OPERATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_undo`
--

DROP TABLE IF EXISTS `b_undo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_undo` (
  `ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNDO_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNDO_HANDLER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONTENT` mediumtext COLLATE utf8_unicode_ci,
  `USER_ID` int(11) DEFAULT NULL,
  `TIMESTAMP_X` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_urlpreview_metadata`
--

DROP TABLE IF EXISTS `b_urlpreview_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_urlpreview_metadata` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `URL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `DATE_INSERT` datetime NOT NULL,
  `DATE_EXPIRE` datetime DEFAULT NULL,
  `TITLE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `IMAGE_ID` int(11) DEFAULT NULL,
  `IMAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMBED` mediumtext COLLATE utf8_unicode_ci,
  `EXTRA` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_URLPREVIEW_METADATA_URL` (`URL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_urlpreview_route`
--

DROP TABLE IF EXISTS `b_urlpreview_route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_urlpreview_route` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROUTE` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CLASS` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMETERS` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_URLPREVIEW_ROUTE_ROUTE` (`ROUTE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user`
--

DROP TABLE IF EXISTS `b_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOGIN` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CHECKWORD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_LOGIN` datetime DEFAULT NULL,
  `DATE_REGISTER` datetime NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PROFESSION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_WWW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_ICQ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_GENDER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_BIRTHDATE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PHOTO` int(18) DEFAULT NULL,
  `PERSONAL_PHONE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_FAX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_MOBILE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PAGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_STREET` text COLLATE utf8_unicode_ci,
  `PERSONAL_MAILBOX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_CITY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_STATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_ZIP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_COUNTRY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_NOTES` text COLLATE utf8_unicode_ci,
  `WORK_COMPANY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_DEPARTMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_POSITION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_WWW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_PHONE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_FAX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_PAGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_STREET` text COLLATE utf8_unicode_ci,
  `WORK_MAILBOX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_CITY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_STATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_ZIP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_COUNTRY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_PROFILE` text COLLATE utf8_unicode_ci,
  `WORK_LOGO` int(18) DEFAULT NULL,
  `WORK_NOTES` text COLLATE utf8_unicode_ci,
  `ADMIN_NOTES` text COLLATE utf8_unicode_ci,
  `STORED_HASH` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_BIRTHDAY` date DEFAULT NULL,
  `EXTERNAL_AUTH_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHECKWORD_TIME` datetime DEFAULT NULL,
  `SECOND_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONFIRM_CODE` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOGIN_ATTEMPTS` int(18) DEFAULT NULL,
  `LAST_ACTIVITY_DATE` datetime DEFAULT NULL,
  `AUTO_TIME_ZONE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIME_ZONE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIME_ZONE_OFFSET` int(18) DEFAULT NULL,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BX_USER_ID` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_login` (`LOGIN`,`EXTERNAL_AUTH_ID`),
  KEY `ix_b_user_email` (`EMAIL`),
  KEY `ix_b_user_activity_date` (`LAST_ACTIVITY_DATE`),
  KEY `IX_B_USER_XML_ID` (`XML_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=32576 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_access`
--

DROP TABLE IF EXISTS `b_user_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_access` (
  `USER_ID` int(11) DEFAULT NULL,
  `PROVIDER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACCESS_CODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `ix_ua_user_provider` (`USER_ID`,`PROVIDER_ID`),
  KEY `ix_ua_user_access` (`USER_ID`,`ACCESS_CODE`),
  KEY `ix_ua_access` (`ACCESS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_access_check`
--

DROP TABLE IF EXISTS `b_user_access_check`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_access_check` (
  `USER_ID` int(11) DEFAULT NULL,
  `PROVIDER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `ix_uac_user_provider` (`USER_ID`,`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_counter`
--

DROP TABLE IF EXISTS `b_user_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_counter` (
  `USER_ID` int(18) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '**',
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CNT` int(18) NOT NULL DEFAULT '0',
  `LAST_DATE` datetime DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `SENT` char(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '3000-01-01 00:00:00',
  PRIMARY KEY (`USER_ID`,`SITE_ID`,`CODE`),
  KEY `ix_buc_tag` (`TAG`),
  KEY `ix_buc_code` (`CODE`),
  KEY `ix_buc_ts` (`TIMESTAMP_X`),
  KEY `ix_buc_sent_userid` (`SENT`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_digest`
--

DROP TABLE IF EXISTS `b_user_digest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_digest` (
  `USER_ID` int(11) NOT NULL,
  `DIGEST_HA1` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_field`
--

DROP TABLE IF EXISTS `b_user_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_field` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_ID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD_NAME` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) DEFAULT NULL,
  `MULTIPLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `MANDATORY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SHOW_FILTER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SHOW_IN_LIST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EDIT_IN_LIST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IS_SEARCHABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SETTINGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_user_type_entity` (`ENTITY_ID`,`FIELD_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_field_confirm`
--

DROP TABLE IF EXISTS `b_user_field_confirm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_field_confirm` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `DATE_CHANGE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FIELD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FIELD_VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONFIRM_CODE` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_user_field_confirm1` (`USER_ID`,`CONFIRM_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_field_enum`
--

DROP TABLE IF EXISTS `b_user_field_enum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_field_enum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_FIELD_ID` int(11) DEFAULT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_user_field_enum` (`USER_FIELD_ID`,`XML_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_field_lang`
--

DROP TABLE IF EXISTS `b_user_field_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_field_lang` (
  `USER_FIELD_ID` int(11) NOT NULL DEFAULT '0',
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `EDIT_FORM_LABEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIST_COLUMN_LABEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIST_FILTER_LABEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HELP_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`USER_FIELD_ID`,`LANGUAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_group`
--

DROP TABLE IF EXISTS `b_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_group` (
  `USER_ID` int(18) NOT NULL,
  `GROUP_ID` int(18) NOT NULL,
  `DATE_ACTIVE_FROM` datetime DEFAULT NULL,
  `DATE_ACTIVE_TO` datetime DEFAULT NULL,
  UNIQUE KEY `ix_user_group` (`USER_ID`,`GROUP_ID`),
  KEY `ix_user_group_group` (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_hit_auth`
--

DROP TABLE IF EXISTS `b_user_hit_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_hit_auth` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `HASH` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `IX_USER_HIT_AUTH_1` (`HASH`),
  KEY `IX_USER_HIT_AUTH_2` (`USER_ID`),
  KEY `IX_USER_HIT_AUTH_3` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_option`
--

DROP TABLE IF EXISTS `b_user_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_option` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) DEFAULT NULL,
  `CATEGORY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` mediumtext COLLATE utf8_unicode_ci,
  `COMMON` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `ix_user_option_user` (`USER_ID`,`CATEGORY`)
) ENGINE=InnoDB AUTO_INCREMENT=3098 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_user_stored_auth`
--

DROP TABLE IF EXISTS `b_user_stored_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_user_stored_auth` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `DATE_REG` datetime NOT NULL,
  `LAST_AUTH` datetime NOT NULL,
  `STORED_HASH` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `TEMP_HASH` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IP_ADDR` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ux_user_hash` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=77507 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_utm_blog_post`
--

DROP TABLE IF EXISTS `b_utm_blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_utm_blog_post` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_BLOG_POST_1` (`FIELD_ID`),
  KEY `ix_utm_BLOG_POST_2` (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_utm_book_collection`
--

DROP TABLE IF EXISTS `b_utm_book_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_utm_book_collection` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_BOOK_COLLECTION_1` (`FIELD_ID`),
  KEY `ix_utm_BOOK_COLLECTION_2` (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_utm_forum_message`
--

DROP TABLE IF EXISTS `b_utm_forum_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_utm_forum_message` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_FORUM_MESSAGE_1` (`FIELD_ID`),
  KEY `ix_utm_FORUM_MESSAGE_2` (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_utm_iblock_3_section`
--

DROP TABLE IF EXISTS `b_utm_iblock_3_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_utm_iblock_3_section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_IBLOCK_3_SECTION_1` (`FIELD_ID`),
  KEY `ix_utm_IBLOCK_3_SECTION_2` (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_utm_iblock_6_section`
--

DROP TABLE IF EXISTS `b_utm_iblock_6_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_utm_iblock_6_section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_IBLOCK_6_SECTION_1` (`FIELD_ID`),
  KEY `ix_utm_IBLOCK_6_SECTION_2` (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_utm_libs_map`
--

DROP TABLE IF EXISTS `b_utm_libs_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_utm_libs_map` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_LIBS_MAP_1` (`FIELD_ID`),
  KEY `ix_utm_LIBS_MAP_2` (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_utm_user`
--

DROP TABLE IF EXISTS `b_utm_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_utm_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_USER_1` (`FIELD_ID`),
  KEY `ix_utm_USER_2` (`VALUE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4207 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_uts_blog_post`
--

DROP TABLE IF EXISTS `b_uts_blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_uts_blog_post` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_GRATITUDE` int(18) DEFAULT NULL,
  `UF_CATEGORY_CODE` text COLLATE utf8_unicode_ci,
  `UF_ANSWER_ID` text COLLATE utf8_unicode_ci,
  `UF_ORIGINAL_ID` text COLLATE utf8_unicode_ci,
  `UF_STATUS` int(18) DEFAULT NULL,
  PRIMARY KEY (`VALUE_ID`),
  KEY `UX_UF_BLOG_POST_IMPRTNT` (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_uts_book_collection`
--

DROP TABLE IF EXISTS `b_uts_book_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_uts_book_collection` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_EXALEAD_LINK` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_uts_forum_message`
--

DROP TABLE IF EXISTS `b_uts_forum_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_uts_forum_message` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_FORUM_MES_URL_PRV` int(11) DEFAULT NULL,
  PRIMARY KEY (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_uts_iblock_3_section`
--

DROP TABLE IF EXISTS `b_uts_iblock_3_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_uts_iblock_3_section` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_MAPS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_uts_iblock_6_section`
--

DROP TABLE IF EXISTS `b_uts_iblock_6_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_uts_iblock_6_section` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_LIBRARY` int(18) DEFAULT NULL,
  `UF_SLIDER` int(18) DEFAULT NULL,
  `UF_ADDED_FAVORITES` double DEFAULT NULL,
  `UF_VIEWS` double DEFAULT NULL,
  `UF_SHOW_NAME` int(18) DEFAULT NULL,
  PRIMARY KEY (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_uts_libs_map`
--

DROP TABLE IF EXISTS `b_uts_libs_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_uts_libs_map` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_LIBS_MAP` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_uts_user`
--

DROP TABLE IF EXISTS `b_uts_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_uts_user` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_TOKEN` text COLLATE utf8_unicode_ci,
  `UF_TOKEN_ADD_DATE` datetime DEFAULT NULL,
  `UF_NUM_ECHB` text COLLATE utf8_unicode_ci,
  `UF_SEARCH_PAGE_COUNT` text COLLATE utf8_unicode_ci,
  `UF_LIBRARY` int(18) DEFAULT NULL,
  `UF_STATUS` int(18) DEFAULT NULL,
  `UF_PASSPORT_NUMBER` text COLLATE utf8_unicode_ci,
  `UF_ISSUED` text COLLATE utf8_unicode_ci,
  `UF_CORPUS` text COLLATE utf8_unicode_ci,
  `UF_STRUCTURE` text COLLATE utf8_unicode_ci,
  `UF_FLAT` text COLLATE utf8_unicode_ci,
  `UF_BRANCH_KNOWLEDGE` int(18) DEFAULT NULL,
  `UF_EDUCATION` int(18) DEFAULT NULL,
  `UF_HOUSE2` text COLLATE utf8_unicode_ci,
  `UF_STRUCTURE2` text COLLATE utf8_unicode_ci,
  `UF_FLAT2` text COLLATE utf8_unicode_ci,
  `UF_PLACE_REGISTR` int(18) DEFAULT NULL,
  `UF_SCAN_PASSPORT1` int(18) DEFAULT NULL,
  `UF_SCAN_PASSPORT2` int(18) DEFAULT NULL,
  `UF_PASSPORT_SERIES` text COLLATE utf8_unicode_ci,
  `UF_RGB_CARD_NUMBER` text COLLATE utf8_unicode_ci,
  `UF_RGB_USER_ID` text COLLATE utf8_unicode_ci,
  `UF_REGISTER_TYPE` int(18) DEFAULT NULL,
  `UF_CITIZENSHIP` int(18) DEFAULT NULL,
  `UF_REGION` text COLLATE utf8_unicode_ci,
  `UF_REGION2` text COLLATE utf8_unicode_ci,
  `UF_AREA` text COLLATE utf8_unicode_ci,
  `UF_AREA2` text COLLATE utf8_unicode_ci,
  `UF_ID_REQUEST` text COLLATE utf8_unicode_ci,
  `UF_LIBRARIES` text COLLATE utf8_unicode_ci,
  `UF_BANK_NAME` text COLLATE utf8_unicode_ci,
  `UF_BANK_INN` text COLLATE utf8_unicode_ci,
  `UF_BANK_KPP` text COLLATE utf8_unicode_ci,
  `UF_BANK_BIC` text COLLATE utf8_unicode_ci,
  `UF_COR_ACCOUNT` text COLLATE utf8_unicode_ci,
  `UF_PERSONAL_ACCOUNT` text COLLATE utf8_unicode_ci,
  `UF_SETTLE_ACCOUNT` text COLLATE utf8_unicode_ci,
  `UF_FOUNDATION` int(18) DEFAULT NULL,
  `UF_PERSON_INN` text COLLATE utf8_unicode_ci,
  `UF_PERSON_KPP` text COLLATE utf8_unicode_ci,
  `UF_PERSON_OKPO` text COLLATE utf8_unicode_ci,
  `UF_PERSON_OGRN` text COLLATE utf8_unicode_ci,
  `UF_PERSON_OKATO` text COLLATE utf8_unicode_ci,
  `UF_PERSON_KBK` text COLLATE utf8_unicode_ci,
  `UF_POSITION_HEAD` text COLLATE utf8_unicode_ci,
  `UF_FIO_HEAD` text COLLATE utf8_unicode_ci,
  `UF_CONTACT_INFO` text COLLATE utf8_unicode_ci,
  `UF_CONTACT_PHONE` text COLLATE utf8_unicode_ci,
  `UF_CONTACT_MOBILE` text COLLATE utf8_unicode_ci,
  `UF_DOCUMENTS` text COLLATE utf8_unicode_ci,
  `UF_WCHZ` int(18) DEFAULT NULL,
  PRIMARY KEY (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote`
--

DROP TABLE IF EXISTS `b_vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CHANNEL_ID` int(18) NOT NULL DEFAULT '0',
  `C_SORT` int(18) DEFAULT '100',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NOTIFY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AUTHOR_ID` int(18) DEFAULT NULL,
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DATE_START` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DATE_END` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COUNTER` int(11) NOT NULL DEFAULT '0',
  `TITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'html',
  `IMAGE_ID` int(18) DEFAULT NULL,
  `EVENT1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EVENT2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EVENT3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNIQUE_TYPE` int(18) NOT NULL DEFAULT '2',
  `KEEP_IP_SEC` int(18) DEFAULT NULL,
  `DELAY` int(18) DEFAULT NULL,
  `DELAY_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESULT_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CHANNEL_ID` (`CHANNEL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote_answer`
--

DROP TABLE IF EXISTS `b_vote_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote_answer` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `QUESTION_ID` int(18) NOT NULL DEFAULT '0',
  `C_SORT` int(18) DEFAULT '100',
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `MESSAGE_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'html',
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `FIELD_TYPE` int(5) NOT NULL DEFAULT '0',
  `FIELD_WIDTH` int(18) DEFAULT NULL,
  `FIELD_HEIGHT` int(18) DEFAULT NULL,
  `FIELD_PARAM` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLOR` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_QUESTION_ID` (`QUESTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote_channel`
--

DROP TABLE IF EXISTS `b_vote_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote_channel` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `SYMBOLIC_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `C_SORT` int(18) DEFAULT '100',
  `FIRST_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `HIDDEN` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VOTE_SINGLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `USE_CAPTCHA` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote_channel_2_group`
--

DROP TABLE IF EXISTS `b_vote_channel_2_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote_channel_2_group` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CHANNEL_ID` int(18) NOT NULL DEFAULT '0',
  `GROUP_ID` int(18) NOT NULL DEFAULT '0',
  `PERMISSION` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_VOTE_CHANNEL_ID_GROUP_ID` (`CHANNEL_ID`,`GROUP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=262 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote_channel_2_site`
--

DROP TABLE IF EXISTS `b_vote_channel_2_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote_channel_2_site` (
  `CHANNEL_ID` int(18) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`CHANNEL_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote_event`
--

DROP TABLE IF EXISTS `b_vote_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote_event` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `VOTE_ID` int(18) NOT NULL DEFAULT '0',
  `VOTE_USER_ID` int(18) NOT NULL DEFAULT '0',
  `DATE_VOTE` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `STAT_SESSION_ID` int(18) DEFAULT NULL,
  `IP` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VALID` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IX_USER_ID` (`VOTE_USER_ID`),
  KEY `IX_B_VOTE_EVENT_2` (`VOTE_ID`,`IP`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote_event_answer`
--

DROP TABLE IF EXISTS `b_vote_event_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote_event_answer` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `EVENT_QUESTION_ID` int(18) NOT NULL DEFAULT '0',
  `ANSWER_ID` int(18) NOT NULL DEFAULT '0',
  `MESSAGE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_EVENT_QUESTION_ID` (`EVENT_QUESTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote_event_question`
--

DROP TABLE IF EXISTS `b_vote_event_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote_event_question` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `EVENT_ID` int(18) NOT NULL DEFAULT '0',
  `QUESTION_ID` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_EVENT_ID` (`EVENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote_question`
--

DROP TABLE IF EXISTS `b_vote_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote_question` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `VOTE_ID` int(18) NOT NULL DEFAULT '0',
  `C_SORT` int(18) DEFAULT '100',
  `COUNTER` int(11) NOT NULL DEFAULT '0',
  `QUESTION` text COLLATE utf8_unicode_ci NOT NULL,
  `QUESTION_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'html',
  `IMAGE_ID` int(18) DEFAULT NULL,
  `DIAGRAM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `REQUIRED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DIAGRAM_TYPE` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'histogram',
  `TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEMPLATE_NEW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_VOTE_ID` (`VOTE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_vote_user`
--

DROP TABLE IF EXISTS `b_vote_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_vote_user` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `STAT_GUEST_ID` int(18) DEFAULT NULL,
  `AUTH_USER_ID` int(18) DEFAULT NULL,
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `DATE_FIRST` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DATE_LAST` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LAST_IP` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=279 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b_xml_tree`
--

DROP TABLE IF EXISTS `b_xml_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_xml_tree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PARENT_ID` int(11) DEFAULT NULL,
  `LEFT_MARGIN` int(11) DEFAULT NULL,
  `RIGHT_MARGIN` int(11) DEFAULT NULL,
  `DEPTH_LEVEL` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VALUE` longtext COLLATE utf8_unicode_ci,
  `ATTRIBUTES` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_xml_tree_parent` (`PARENT_ID`),
  KEY `ix_b_xml_tree_left` (`LEFT_MARGIN`)
) ENGINE=InnoDB AUTO_INCREMENT=15430 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_category_history`
--

DROP TABLE IF EXISTS `lbc_category_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_category_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(100) NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id_idx` (`category`,`category_id`),
  KEY `timestamp_idx` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_locationplanimages`
--

DROP TABLE IF EXISTS `lbc_locationplanimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_locationplanimages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `LocationPlanId` int(11) NOT NULL,
  `ImagePath` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=300026 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_locationplans`
--

DROP TABLE IF EXISTS `lbc_locationplans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_locationplans` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MainDivisionId` int(11) NOT NULL,
  `ParentId` int(11) DEFAULT NULL,
  `Level` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Comments` text,
  `Keywords` text,
  `Links` text,
  `Rec_id` int(11) DEFAULT NULL,
  `Gr` int(11) DEFAULT NULL,
  `GrErr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDX_541D2DFF856A684C` (`ParentId`),
  CONSTRAINT `FK_541D2DFF856A684C` FOREIGN KEY (`ParentId`) REFERENCES `lbc_locationplans` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=300021 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_locationplans_closure`
--

DROP TABLE IF EXISTS `lbc_locationplans_closure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_locationplans_closure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ancestor` int(11) NOT NULL,
  `descendant` int(11) NOT NULL,
  `depth` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_C18D18893C6320F4` (`ancestor`,`descendant`),
  KEY `IDX_C6B54A58B4465BB` (`ancestor`),
  KEY `IDX_C6B54A589A8FAD16` (`descendant`),
  KEY `IDX_6A471B17E4AC3B1C` (`depth`),
  CONSTRAINT `FK_C6B54A589A8FAD16` FOREIGN KEY (`descendant`) REFERENCES `lbc_locationplans` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C6B54A58B4465BB` FOREIGN KEY (`ancestor`) REFERENCES `lbc_locationplans` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_locationplansectionimages`
--

DROP TABLE IF EXISTS `lbc_locationplansectionimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_locationplansectionimages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `LocationPlanSectionId` int(11) NOT NULL,
  `ImagePath` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=300534 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_locationplansections`
--

DROP TABLE IF EXISTS `lbc_locationplansections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_locationplansections` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `LocationPlanId` int(11) NOT NULL,
  `ParentId` int(11) DEFAULT NULL,
  `Level` int(11) NOT NULL,
  `Code` varchar(255) NOT NULL,
  `Name` text NOT NULL,
  `Comments` text,
  `Keywords` text,
  `Links` text,
  `Rec_id` int(11) DEFAULT NULL,
  `Gr` int(11) DEFAULT NULL,
  `GrErr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDX_41C42E2856A684C` (`ParentId`),
  CONSTRAINT `FK_41C42E2856A684C` FOREIGN KEY (`ParentId`) REFERENCES `lbc_locationplansections` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=300475 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_locationplansections_closure`
--

DROP TABLE IF EXISTS `lbc_locationplansections_closure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_locationplansections_closure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ancestor` int(11) NOT NULL,
  `descendant` int(11) NOT NULL,
  `depth` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_37FE6ECBBE04FC20` (`ancestor`,`descendant`),
  KEY `IDX_AFBA4EC0B4465BB` (`ancestor`),
  KEY `IDX_AFBA4EC09A8FAD16` (`descendant`),
  KEY `IDX_8460E6654814221F` (`depth`),
  CONSTRAINT `FK_AFBA4EC09A8FAD16` FOREIGN KEY (`descendant`) REFERENCES `lbc_locationplansections` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_AFBA4EC0B4465BB` FOREIGN KEY (`ancestor`) REFERENCES `lbc_locationplansections` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=707 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_maindivision`
--

DROP TABLE IF EXISTS `lbc_maindivision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_maindivision` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ParentId` int(11) DEFAULT NULL,
  `Level` int(11) NOT NULL,
  `Code` varchar(255) NOT NULL,
  `Name` text NOT NULL,
  `Comments` text,
  `Keywords` text,
  `Links` text,
  `IsCommon` int(11) NOT NULL,
  `Rec_id` int(11) DEFAULT NULL,
  `Gr` int(11) DEFAULT NULL,
  `GrErr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDX_ADE76031E9982EB8` (`ParentId`),
  CONSTRAINT `FK_ADE76031E9982EB8` FOREIGN KEY (`ParentId`) REFERENCES `lbc_maindivision` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=322999 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_maindivision_closure`
--

DROP TABLE IF EXISTS `lbc_maindivision_closure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_maindivision_closure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ancestor` int(11) NOT NULL,
  `descendant` int(11) NOT NULL,
  `depth` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_5F69D0BA16E61838` (`ancestor`,`descendant`),
  KEY `IDX_7D6803B7B4465BB` (`ancestor`),
  KEY `IDX_7D6803B79A8FAD16` (`descendant`),
  KEY `IDX_10A12DBFF20DAB27` (`depth`),
  CONSTRAINT `FK_7D6803B79A8FAD16` FOREIGN KEY (`descendant`) REFERENCES `lbc_maindivision` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_7D6803B7B4465BB` FOREIGN KEY (`ancestor`) REFERENCES `lbc_maindivision` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=366314 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_maindivisionimages`
--

DROP TABLE IF EXISTS `lbc_maindivisionimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_maindivisionimages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MainDivisionId` int(11) NOT NULL,
  `ImagePath` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=323423 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_maindivisionintroduction`
--

DROP TABLE IF EXISTS `lbc_maindivisionintroduction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_maindivisionintroduction` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MainDivisionId` int(11) NOT NULL,
  `TextPath` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=300006 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_maindivisionintroductionimages`
--

DROP TABLE IF EXISTS `lbc_maindivisionintroductionimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_maindivisionintroductionimages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MainDivisionIntroductionId` int(11) NOT NULL,
  `ImagePath` text NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDX_4FE5009250DDFB1B` (`MainDivisionIntroductionId`),
  CONSTRAINT `FK_4FE5009250DDFB1B` FOREIGN KEY (`MainDivisionIntroductionId`) REFERENCES `lbc_maindivisionintroduction` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=300183 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_specialdivision`
--

DROP TABLE IF EXISTS `lbc_specialdivision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_specialdivision` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MainDivisionId` int(11) NOT NULL,
  `ParentId` int(11) DEFAULT NULL,
  `Level` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Comments` text,
  `Keywords` text,
  `Links` text,
  `Rec_id` int(11) DEFAULT NULL,
  `Gr` int(11) DEFAULT NULL,
  `GrErr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDX_BE1EBA28856A684C` (`ParentId`),
  CONSTRAINT `FK_BE1EBA28856A684C` FOREIGN KEY (`ParentId`) REFERENCES `lbc_specialdivision` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=300028 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_specialdivision_closure`
--

DROP TABLE IF EXISTS `lbc_specialdivision_closure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_specialdivision_closure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `depth` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_specialdivisionimages`
--

DROP TABLE IF EXISTS `lbc_specialdivisionimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_specialdivisionimages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `SpecialDivisionId` int(11) NOT NULL,
  `ImagePath` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=300029 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_specialdivisionsectionimages`
--

DROP TABLE IF EXISTS `lbc_specialdivisionsectionimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_specialdivisionsectionimages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `SpecialDivisionSectionId` int(11) NOT NULL,
  `ImagePath` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=301547 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_specialdivisionsections`
--

DROP TABLE IF EXISTS `lbc_specialdivisionsections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_specialdivisionsections` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `SpecialDivisionId` int(11) NOT NULL,
  `ParentId` int(11) DEFAULT NULL,
  `Level` int(11) NOT NULL,
  `Code` varchar(255) NOT NULL,
  `Name` text NOT NULL,
  `Comments` text,
  `Keywords` text,
  `Links` text,
  `Rec_id` int(11) DEFAULT NULL,
  `Gr` int(11) DEFAULT NULL,
  `GrErr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDX_132BFF0B856A684C` (`ParentId`),
  CONSTRAINT `FK_132BFF0B856A684C` FOREIGN KEY (`ParentId`) REFERENCES `lbc_specialdivisionsections` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=301545 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lbc_specialdivisionsections_closure`
--

DROP TABLE IF EXISTS `lbc_specialdivisionsections_closure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lbc_specialdivisionsections_closure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `depth` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `n_ext_notaext`
--

DROP TABLE IF EXISTS `n_ext_notaext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `n_ext_notaext` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EXT_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `INSTALL` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='В таблице хранится информация об установленных расширениях';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_bookcatalog_author_books`
--

DROP TABLE IF EXISTS `neb_bookcatalog_author_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_bookcatalog_author_books` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EXALEAD_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SECOND_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_bookcatalog_author_id`
--

DROP TABLE IF EXISTS `neb_bookcatalog_author_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_bookcatalog_author_id` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `AUTHOR_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_bookcatalog_books`
--

DROP TABLE IF EXISTS `neb_bookcatalog_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_bookcatalog_books` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SECTION_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EXALEAD_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PREVIEW_PICTURE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREVIEW_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROMO_PICTURE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROMO` enum('Y','N') COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_bookcatalog_books_property`
--

DROP TABLE IF EXISTS `neb_bookcatalog_books_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_bookcatalog_books_property` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `BOOK_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PREVIEW_TEXT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_bookcatalog_section_id`
--

DROP TABLE IF EXISTS `neb_bookcatalog_section_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_bookcatalog_section_id` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `SECTION_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_bookcatalog_sections`
--

DROP TABLE IF EXISTS `neb_bookcatalog_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_bookcatalog_sections` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EXALEAD_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PARENT_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SECTION_TYPE` enum('NULL','LIBRARY','USER') COLLATE utf8_unicode_ci DEFAULT NULL,
  `BACKGROUND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROMO` enum('Y','N') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_books_data`
--

DROP TABLE IF EXISTS `neb_books_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_books_data` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_BOOK_ID` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `UF_BOOK_NAME` text,
  `UF_BOOK_AUTHOR` text,
  `UF_BOOK_PUBLISH_YEAR` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37455 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_books_info`
--

DROP TABLE IF EXISTS `neb_books_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_books_info` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BOOK_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LIB_ID` int(11) NOT NULL COMMENT 'ID библиотеки в экзалиде',
  `VIEWS` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BOOK_ID` (`BOOK_ID`),
  KEY `LIB_ID` (`LIB_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4296285 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_collection_collection`
--

DROP TABLE IF EXISTS `neb_collection_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_collection_collection` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EXALEAD_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LIBRARY_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MAIN` enum('Y','N') COLLATE utf8_unicode_ci DEFAULT NULL,
  `BACKGROUND` int(11) DEFAULT NULL,
  `SORT` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_collection_elements`
--

DROP TABLE IF EXISTS `neb_collection_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_collection_elements` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EXALEAD_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BOOK_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `COLLECTION_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_collection_library`
--

DROP TABLE IF EXISTS `neb_collection_library`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_collection_library` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EXALEAD_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SIGNATURE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSTAL_ADDRESS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHEDULE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHONE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SKYPE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `MAP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP_X` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `CODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LOGO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_funds_delete_messages`
--

DROP TABLE IF EXISTS `neb_funds_delete_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_funds_delete_messages` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BOOK_ID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `TIMESTAMMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_group_role_settings`
--

DROP TABLE IF EXISTS `neb_group_role_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_group_role_settings` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_GROUP_CODE` text COLLATE utf8_unicode_ci,
  `UF_ROLE` text COLLATE utf8_unicode_ci,
  `UF_SECTIONS_ACCESS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_group_role_settings_uf_sections_access`
--

DROP TABLE IF EXISTS `neb_group_role_settings_uf_sections_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_group_role_settings_uf_sections_access` (
  `ID` int(11) NOT NULL,
  `VALUE` int(11) NOT NULL,
  KEY `IX_UTM_HL13_189_ID` (`ID`),
  KEY `IX_UTM_HL13_189_VALUE` (`VALUE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_libs`
--

DROP TABLE IF EXISTS `neb_libs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_libs` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_ID` int(18) DEFAULT NULL,
  `UF_PARENTID` int(18) DEFAULT NULL,
  `UF_NAME` text COLLATE utf8_unicode_ci,
  `UF_DATE_OPEN` text COLLATE utf8_unicode_ci,
  `UF_STRUCTURE` text COLLATE utf8_unicode_ci,
  `UF_ADRESS` text COLLATE utf8_unicode_ci,
  `UF_REGION` text COLLATE utf8_unicode_ci,
  `UF_DISTRICT` text COLLATE utf8_unicode_ci,
  `UF_TOWN` text COLLATE utf8_unicode_ci,
  `UF_PHONE` text COLLATE utf8_unicode_ci,
  `UF_COMMENT_1` text COLLATE utf8_unicode_ci,
  `UF_ADR_FIAS` int(18) DEFAULT NULL,
  `UF_REC_ID` int(18) DEFAULT NULL,
  `UF_ADMINISTRATIVEARE` text COLLATE utf8_unicode_ci,
  `UF_LOCALITY` text COLLATE utf8_unicode_ci,
  `UF_POS` text COLLATE utf8_unicode_ci,
  `UF_AREA` text COLLATE utf8_unicode_ci,
  `UF_AREA2` text COLLATE utf8_unicode_ci,
  `UF_DESCRIPTION` text COLLATE utf8_unicode_ci,
  `UF_FULL_DESCRIPTION` text COLLATE utf8_unicode_ci,
  `UF_CITY` int(18) DEFAULT NULL,
  `UF_DATE_JOIN` datetime DEFAULT NULL,
  `UF_FULL_NAME` text COLLATE utf8_unicode_ci,
  `UF_CONTRACT` text COLLATE utf8_unicode_ci,
  `UF_RGB_ID` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6998 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_libs_area`
--

DROP TABLE IF EXISTS `neb_libs_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_libs_area` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_LIB_AREA_NAME` text COLLATE utf8_unicode_ci,
  `UF_LIB_AREA_NAME_S` text COLLATE utf8_unicode_ci,
  `UF_SORT` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_libs_city`
--

DROP TABLE IF EXISTS `neb_libs_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_libs_city` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_LIB_AREA` int(18) DEFAULT NULL,
  `UF_CITY_NAME` text COLLATE utf8_unicode_ci,
  `UF_IN_FAVORITES` int(18) DEFAULT NULL,
  `UF_IN_LIST` int(18) DEFAULT NULL,
  `UF_POS` text COLLATE utf8_unicode_ci,
  `UF_SORT` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2689 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_log_import_libs`
--

DROP TABLE IF EXISTS `neb_log_import_libs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_log_import_libs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `RGB_ID` int(11) NOT NULL,
  `NAME` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `ADDRESS` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INFO` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2179 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_log_import_libs_geocode`
--

DROP TABLE IF EXISTS `neb_log_import_libs_geocode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_log_import_libs_geocode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `RGB_ID` int(11) NOT NULL,
  `NAME` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `ADDRESS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INFO` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Y_LOCATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Y_STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Y_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `G_LOCATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `G_STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `G_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2243 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_log_import_workplaces`
--

DROP TABLE IF EXISTS `neb_log_import_workplaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_log_import_workplaces` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `RGB_ID` int(11) NOT NULL,
  `LIB_ID` int(11) NOT NULL,
  `NAME` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INFO` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2161 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_reader_session`
--

DROP TABLE IF EXISTS `neb_reader_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_reader_session` (
  `ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`,`BOOK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_stat_book_open`
--

DROP TABLE IF EXISTS `neb_stat_book_open`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_stat_book_open` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BOOK_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `REFERER` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEARCH_QUERY` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `BOOK_ID` (`BOOK_ID`),
  KEY `idx-timestamp` (`TIMESTAMP`)
) ENGINE=InnoDB AUTO_INCREMENT=5358636 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_stat_book_read`
--

DROP TABLE IF EXISTS `neb_stat_book_read`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_stat_book_read` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BOOK_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `UID` int(11) NOT NULL,
  `UID_GUEST` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `X_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `book_id` (`BOOK_ID`),
  KEY `UID` (`UID`)
) ENGINE=InnoDB AUTO_INCREMENT=496272 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_stat_book_view`
--

DROP TABLE IF EXISTS `neb_stat_book_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_stat_book_view` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BOOK_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LIB_ID` int(11) NOT NULL COMMENT 'ID библиотеки в экзалиде',
  `UID` int(11) DEFAULT NULL,
  `UID_GUEST` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `X_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `BOOK_ID` (`BOOK_ID`),
  KEY `LIB_ID` (`LIB_ID`),
  KEY `tt` (`BOOK_ID`,`X_TIMESTAMP`)
) ENGINE=InnoDB AUTO_INCREMENT=34344967 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Статистика просмотра книг';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_stat_book_view_most_popular`
--

DROP TABLE IF EXISTS `neb_stat_book_view_most_popular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_stat_book_view_most_popular` (
  `BOOK_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LIB_ID` int(11) NOT NULL DEFAULT '0',
  `VIEWS` int(11) NOT NULL,
  PRIMARY KEY (`LIB_ID`,`VIEWS`,`BOOK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Статистика просмотра книг(топ популярных)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_stat_edition`
--

DROP TABLE IF EXISTS `neb_stat_edition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_stat_edition` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `ID_BITRIXDB` int(10) NOT NULL,
  `ID_EXALED` int(10) NOT NULL,
  `TITLE_LIB` text NOT NULL,
  `COUNT_EDITION` int(8) NOT NULL,
  `COUNT_EDITION_DIG` int(8) NOT NULL,
  `DATE_STAT` datetime NOT NULL,
  `ACTIVE_IN_BITRIX` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18638 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_stat_library`
--

DROP TABLE IF EXISTS `neb_stat_library`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_stat_library` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LIBRARY_ID` int(11) NOT NULL,
  `PUBLICATIONS` int(11) NOT NULL,
  `COLLECTIONS` int(11) NOT NULL,
  `USERS` int(11) NOT NULL,
  `VIEWS` int(11) NOT NULL,
  `ACTIVE_USERS` int(11) DEFAULT NULL,
  `DELETED_USERS` int(11) DEFAULT NULL,
  `DOWNLOADS` int(11) DEFAULT NULL,
  `SEARCH_COUNT` int(11) DEFAULT NULL,
  `FEEDBACK_COUNT` int(11) DEFAULT NULL,
  `VIEWS_BOOK` int(11) DEFAULT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PUBLICATIONS_DIGITIZED` int(11) DEFAULT NULL,
  `PUBLICATIONS_DIGITIZED_DD` int(11) DEFAULT NULL,
  `PUBLICATIONS_DD` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `library_stat` (`LIBRARY_ID`,`DATE`)
) ENGINE=InnoDB AUTO_INCREMENT=3137 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_stat_log`
--

DROP TABLE IF EXISTS `neb_stat_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_stat_log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DT` date DEFAULT NULL,
  `ID_BOOK` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ID_LIB` int(11) DEFAULT NULL,
  `CNT_READ` int(11) DEFAULT NULL,
  `CNT_VIEW` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `ID_LIB` (`ID_LIB`)
) ENGINE=InnoDB AUTO_INCREMENT=2476851 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_stat_search`
--

DROP TABLE IF EXISTS `neb_stat_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_stat_search` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `QUERY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `UID` int(11) DEFAULT NULL,
  `UID_GUEST` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `X_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Статистика поиска';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_user_digitization`
--

DROP TABLE IF EXISTS `neb_user_digitization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_user_digitization` (
  `ID` int(11) unsigned NOT NULL,
  `USER_ID` int(11) unsigned NOT NULL,
  `IS_READY` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_userdata_books`
--

DROP TABLE IF EXISTS `neb_userdata_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_userdata_books` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `BOOK_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` enum('READ','OLD') COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_users_bookmarks_data`
--

DROP TABLE IF EXISTS `neb_users_bookmarks_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_users_bookmarks_data` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_UID` int(18) DEFAULT NULL,
  `UF_BOOK_ID` text COLLATE utf8_unicode_ci,
  `UF_NUM_PAGE` int(18) DEFAULT NULL,
  `UF_DATE_ADD` datetime DEFAULT NULL,
  `UF_PREVIEW` text COLLATE utf8_unicode_ci,
  `UF_BOOK_NAME` text COLLATE utf8_unicode_ci,
  `UF_BOOK_AUTHOR` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8401 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_users_books_collection_data`
--

DROP TABLE IF EXISTS `neb_users_books_collection_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_users_books_collection_data` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_UID` int(18) DEFAULT NULL,
  `UF_COLLECTION_ID` int(18) DEFAULT NULL,
  `UF_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `user_collection` (`UF_UID`,`UF_COLLECTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_users_books_data`
--

DROP TABLE IF EXISTS `neb_users_books_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_users_books_data` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_UID` int(18) DEFAULT NULL,
  `UF_BOOK_ID` text COLLATE utf8_unicode_ci,
  `UF_DATE_ADD` datetime DEFAULT NULL,
  `UF_CNT_QUOTES` int(18) DEFAULT NULL,
  `UF_CNT_BOOKMARKS` int(18) DEFAULT NULL,
  `UF_CNT_NOTES` int(18) DEFAULT NULL,
  `UF_READING` int(18) DEFAULT NULL,
  `UF_READING_PERCENT` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=65072 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_users_collections`
--

DROP TABLE IF EXISTS `neb_users_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_users_collections` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_UID` int(18) DEFAULT NULL,
  `UF_NAME` text COLLATE utf8_unicode_ci,
  `UF_DATE_ADD` datetime DEFAULT NULL,
  `UF_DATE_EDIT` datetime DEFAULT NULL,
  `UF_SORT` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2004 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_users_collections_links`
--

DROP TABLE IF EXISTS `neb_users_collections_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_users_collections_links` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_COLLECTION_ID` int(18) DEFAULT NULL,
  `UF_OBJECT_ID` int(18) DEFAULT NULL,
  `UF_TYPE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16837 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_users_notes_data`
--

DROP TABLE IF EXISTS `neb_users_notes_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_users_notes_data` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_UID` double DEFAULT NULL,
  `UF_BOOK_ID` text COLLATE utf8_unicode_ci,
  `UF_NUM_PAGE` int(18) DEFAULT NULL,
  `UF_TEXT` text COLLATE utf8_unicode_ci,
  `UF_DATE_ADD` datetime DEFAULT NULL,
  `UF_NOTE_AREA` text COLLATE utf8_unicode_ci,
  `UF_IMG_DATA` longtext COLLATE utf8_unicode_ci,
  `UF_TOP` int(18) DEFAULT NULL,
  `UF_LEFT` int(18) DEFAULT NULL,
  `UF_WIDTH` int(18) DEFAULT NULL,
  `UF_HEIGHT` int(18) DEFAULT NULL,
  `UF_DATE_UPDATE` datetime DEFAULT NULL,
  `UF_BOOK_NAME` text COLLATE utf8_unicode_ci,
  `UF_BOOK_AUTHOR` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1334 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_users_quo_data`
--

DROP TABLE IF EXISTS `neb_users_quo_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_users_quo_data` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_UID` int(18) DEFAULT NULL,
  `UF_BOOK_ID` text COLLATE utf8_unicode_ci,
  `UF_DATE_ADD` datetime DEFAULT NULL,
  `UF_TEXT` text COLLATE utf8_unicode_ci,
  `UF_IMG_DATA` longtext COLLATE utf8_unicode_ci,
  `UF_PAGE` int(18) DEFAULT NULL,
  `UF_TOP` int(18) DEFAULT NULL,
  `UF_LEFT` int(18) DEFAULT NULL,
  `UF_WIDTH` int(18) DEFAULT NULL,
  `UF_HEIGHT` int(18) DEFAULT NULL,
  `UF_BOOK_NAME` text COLLATE utf8_unicode_ci,
  `UF_BOOK_AUTHOR` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2395 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `neb_users_session_pcc`
--

DROP TABLE IF EXISTS `neb_users_session_pcc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neb_users_session_pcc` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_SESSION_ID` text,
  `UF_USER` text,
  `UF_SCANER_ID` text,
  `UF_CLOSE` int(18) DEFAULT NULL,
  `UF_USER_LOGIN` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=280 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plan_digitization`
--

DROP TABLE IF EXISTS `plan_digitization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_digitization` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_LIBRARY` int(18) DEFAULT NULL,
  `UF_EXALEAD_BOOK` text COLLATE utf8_unicode_ci,
  `UF_DATE_ADD` datetime DEFAULT NULL,
  `UF_DATE_FINISH` date DEFAULT NULL,
  `UF_COMMENT` text COLLATE utf8_unicode_ci,
  `UF_EXALEAD_PARENT` text COLLATE utf8_unicode_ci,
  `UF_NEB_USER` int(18) DEFAULT NULL,
  `UF_REQUEST_STATUS` int(18) DEFAULT NULL,
  `UF_BOOK_AUTHOR` text COLLATE utf8_unicode_ci,
  `UF_BOOK_FILE` int(18) DEFAULT NULL,
  `UF_ADD_TYPE` text COLLATE utf8_unicode_ci,
  `UF_BOOK_NAME` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `parent` (`UF_EXALEAD_PARENT`(100)),
  KEY `book_id` (`UF_EXALEAD_BOOK`(100)),
  KEY `lib_id` (`UF_LIBRARY`)
) ENGINE=InnoDB AUTO_INCREMENT=3956 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plan_digitization_log`
--

DROP TABLE IF EXISTS `plan_digitization_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_digitization_log` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ORDER_ID` int(11) unsigned NOT NULL,
  `USER_ID` int(11) unsigned NOT NULL,
  `COMMENT` text COLLATE utf8_unicode_ci,
  `STATUS_ID` int(11) DEFAULT NULL,
  `ACTION_NAME` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `searches_users`
--

DROP TABLE IF EXISTS `searches_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searches_users` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UF_DATE_ADD` datetime DEFAULT NULL,
  `UF_FOUND` int(18) DEFAULT NULL,
  `UF_QUERY` text COLLATE utf8_unicode_ci,
  `UF_MORE_OPTIONS` text COLLATE utf8_unicode_ci,
  `UF_URL` text COLLATE utf8_unicode_ci,
  `UF_UID` int(18) DEFAULT NULL,
  `UF_EXALEAD_PARAM` text COLLATE utf8_unicode_ci,
  `UF_MD5` text COLLATE utf8_unicode_ci,
  `UF_NAME` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1653 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_common_biblio_card`
--

DROP TABLE IF EXISTS `tbl_common_biblio_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_common_biblio_card` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ALIS` int(11) NOT NULL,
  `IdFromALIS` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `Author` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Name` text COLLATE utf8_unicode_ci,
  `SubName` text COLLATE utf8_unicode_ci,
  `PublishYear` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PublishPlace` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Publisher` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISBN` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Language` int(11) DEFAULT NULL,
  `CountPages` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Printing` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Format` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Series` varchar(450) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VolumeNumber` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VolumeName` varchar(450) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Collection` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AccessType` int(11) DEFAULT '15',
  `isLicenseAgreement` bit(1) DEFAULT b'0',
  `Responsibility` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Annotation` text COLLATE utf8_unicode_ci,
  `ContentRemark` text COLLATE utf8_unicode_ci,
  `CommonRemark` text COLLATE utf8_unicode_ci,
  `PublicationInformation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdfLink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IdParent` int(11) DEFAULT NULL,
  `CreationMethod` int(11) DEFAULT NULL,
  `CreationActor` int(11) DEFAULT NULL,
  `CreationDateTime` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `UpdatingActor` int(11) DEFAULT NULL,
  `UpdatingDateTime` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `BBKText` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UDKText` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LanguageText` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UDKThematicsCodeSequences` text COLLATE utf8_unicode_ci,
  `IdRubrics` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Indexes` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BX_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BX_IS_UPLOAD` int(11) DEFAULT NULL COMMENT '1 - загружена в экзалиду иначе нет',
  `BX_ALIS` int(11) NOT NULL COMMENT 'ID библиотеки из таблицы tbl_libraries_alis в битриксе',
  `BX_UID` int(11) NOT NULL COMMENT 'ID пользователя который добавиол книгу',
  `BX_LIB_ID` int(11) DEFAULT NULL COMMENT 'ID библиотеки которая добавила книгу',
  `BX_DELETE` int(11) DEFAULT NULL COMMENT 'Признак удаления - 1 - удалено',
  `EX_IdFromALIS` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'IdFromALIS на сервере Экзалиды',
  `EX_IdCard` int(11) NOT NULL COMMENT 'ID карточки на сервере экзалиды',
  `EX_pdfLink` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Ссылка на pdf га сервере Экзалиды',
  `toDel` int(11) DEFAULT '0' COMMENT '1-помечено на удаление',
  `request_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_price` double DEFAULT '0',
  `FullSymbolicId` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `request_type` (`request_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4156928 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AVG_ROW_LENGTH=384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_common_biblio_card_`
--

DROP TABLE IF EXISTS `tbl_common_biblio_card_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_common_biblio_card_` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ALIS` int(11) NOT NULL,
  `IdFromALIS` varchar(80) NOT NULL,
  `Author2` varchar(500) DEFAULT NULL,
  `Author` varchar(500) DEFAULT NULL,
  `Name` text,
  `SubName` text,
  `PublishYear` varchar(45) DEFAULT NULL,
  `PublishPlace` varchar(500) DEFAULT NULL,
  `Publisher` varchar(500) DEFAULT NULL,
  `ISBN` varchar(50) DEFAULT NULL,
  `Language` int(11) DEFAULT NULL,
  `CountPages` varchar(150) DEFAULT NULL,
  `Printing` varchar(45) DEFAULT NULL,
  `Format` varchar(45) DEFAULT NULL,
  `Series` varchar(450) DEFAULT NULL,
  `VolumeNumber` varchar(45) DEFAULT NULL,
  `VolumeName` varchar(450) DEFAULT NULL,
  `Collection` varchar(100) DEFAULT NULL,
  `AccessType` int(11) DEFAULT '15',
  `isLicenseAgreement` bit(1) DEFAULT b'0',
  `Responsibility2` varchar(500) DEFAULT NULL,
  `Responsibility` varchar(500) DEFAULT NULL,
  `Annotation` text,
  `ContentRemark` text,
  `CommonRemark` text,
  `PublicationInformation` varchar(255) DEFAULT NULL,
  `pdfLink` varchar(255) DEFAULT NULL,
  `IdParent` int(11) DEFAULT NULL,
  `CreationMethod` int(11) DEFAULT NULL,
  `CreationActor` int(11) DEFAULT NULL,
  `CreationDateTime` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `UpdatingActor` int(11) DEFAULT NULL,
  `UpdatingDateTime` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `BBKText` varchar(100) DEFAULT NULL,
  `UDKText` varchar(100) DEFAULT NULL,
  `LanguageText` varchar(32) DEFAULT NULL,
  `UDKThematicsCodeSequences` text,
  `IdRubrics` varchar(500) DEFAULT NULL,
  `Indexes` varchar(500) DEFAULT NULL,
  `FullSymbolicId` varchar(100) DEFAULT NULL,
  `ALIS2` int(11) DEFAULT '9',
  `ParentId2` int(11) DEFAULT NULL,
  `ParentIdFull` varchar(80) DEFAULT NULL,
  `ParentIdFull2` varchar(80) DEFAULT NULL,
  `FullSymbolicIdUnicode` varchar(300) CHARACTER SET cp1251 DEFAULT NULL,
  `IdPdfLink` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UK_tbl_common_biblio_card_` (`Id`,`ALIS`)
) ENGINE=InnoDB AUTO_INCREMENT=1812 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=384;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_common_biblio_card_bx`
--

DROP TABLE IF EXISTS `tbl_common_biblio_card_bx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_common_biblio_card_bx` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_BX` int(11) NOT NULL,
  `ID_EXALEAD` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_libraries_alis`
--

DROP TABLE IF EXISTS `tbl_libraries_alis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_libraries_alis` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Description` text CHARACTER SET utf8,
  `Type` int(11) NOT NULL,
  `Library` int(11) NOT NULL,
  `IpOrHostName` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `Port` smallint(5) unsigned DEFAULT NULL,
  `Login` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `Password` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `PathAndFileName` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `BX_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BX_IS_UPLOAD` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AVG_ROW_LENGTH=2340;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test_name`
--

DROP TABLE IF EXISTS `test_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_name` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turgenev`
--

DROP TABLE IF EXISTS `turgenev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turgenev` (
  `FullSymbolicId` varchar(255) DEFAULT NULL,
  `id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vchz`
--

DROP TABLE IF EXISTS `vchz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vchz` (
  `ID` double(53,30) NOT NULL DEFAULT '0.000000000000000000000000000000',
  `Библиотека` double(53,30) DEFAULT NULL,
  `комментарий` varchar(255) DEFAULT NULL,
  `Название` varchar(255) DEFAULT NULL,
  `Активность` varchar(255) DEFAULT NULL,
  `Сорт_` double(53,30) DEFAULT NULL,
  `Дата_изм_` varchar(255) DEFAULT NULL,
  `IP_адреса_или_диапазоны_адресов` varchar(255) DEFAULT NULL,
  `связанны ранее` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-31 16:50:20




INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (22, NULL, 1, 'Т', 'ИСТОРИЯ. ИСТОРИЧЕСКИЕ НАУКИ', '', '', '', 0, 698, 0, '');
INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (29, 22, 2, 'Т3', 'ИСТОРИЯ', '', '', '', 0, 699, 0, '');
INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (30, 29, 3, 'Т3(0)', 'Всемирная история', '', '', '', 0, 699, 0, '');
INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (31, 29, 3, 'Т3(2)', 'История СССР', '', '', '', 0, 699, 0, '');
INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (33, 29, 3, 'Т3(4/9)', 'ИСТОРИЯ ЗАРУБЕЖНЫХ СТРАН', '', '', '', 0, 700, 0, '');


INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366314, 22, 22, 0);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366315, 29, 29, 0);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366316, 30, 30, 0);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366317, 31, 31, 0);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366318, 33, 33, 0);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366319, 22, 29, 1);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366320, 29, 30, 1);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366321, 29, 31, 1);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366322, 29, 33, 1);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366323, 22, 30, 2);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366324, 22, 31, 2);
INSERT INTO neb_test_database.lbc_maindivision_closure (id, ancestor, descendant, depth) VALUES (366325, 22, 33, 2);

INSERT INTO b_user (ID, TIMESTAMP_X, LOGIN, PASSWORD, CHECKWORD, ACTIVE, NAME, LAST_NAME, EMAIL, LAST_LOGIN, DATE_REGISTER, LID, PERSONAL_PROFESSION, PERSONAL_WWW, PERSONAL_ICQ, PERSONAL_GENDER, PERSONAL_BIRTHDATE, PERSONAL_PHOTO, PERSONAL_PHONE, PERSONAL_FAX, PERSONAL_MOBILE, PERSONAL_PAGER, PERSONAL_STREET, PERSONAL_MAILBOX, PERSONAL_CITY, PERSONAL_STATE, PERSONAL_ZIP, PERSONAL_COUNTRY, PERSONAL_NOTES, WORK_COMPANY, WORK_DEPARTMENT, WORK_POSITION, WORK_WWW, WORK_PHONE, WORK_FAX, WORK_PAGER, WORK_STREET, WORK_MAILBOX, WORK_CITY, WORK_STATE, WORK_ZIP, WORK_COUNTRY, WORK_PROFILE, WORK_LOGO, WORK_NOTES, ADMIN_NOTES, STORED_HASH, XML_ID, PERSONAL_BIRTHDAY, EXTERNAL_AUTH_ID, CHECKWORD_TIME, SECOND_NAME, CONFIRM_CODE, LOGIN_ATTEMPTS, LAST_ACTIVITY_DATE, AUTO_TIME_ZONE, TIME_ZONE, TIME_ZONE_OFFSET, TITLE, BX_USER_ID, LANGUAGE_ID) VALUES (11985, '2016-09-01 12:51:42', 'operator@elar.ru', '9HUEOhG49a2ff7079fe4316175b7cec75e42de05', 'epdwrzYQ2bd9b7fae9aec9833c4d53f70e3d2750', 'Y', 'Оператор', '', 'operator@elar.ru', '2016-09-01 12:48:11', '2015-07-30 13:51:20', 's1', '', '', '', '', null, null, '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', null, '', '', null, '', null, null, '2015-07-30 13:51:20', '', null, 0, null, '', null, null, '', '8f5c2d18d17524fcdad2926a31bb4ab9', null);

INSERT INTO b_uts_user (VALUE_ID, UF_TOKEN, UF_TOKEN_ADD_DATE, UF_NUM_ECHB, UF_SEARCH_PAGE_COUNT, UF_LIBRARY, UF_STATUS, UF_PASSPORT_NUMBER, UF_ISSUED, UF_CORPUS, UF_STRUCTURE, UF_FLAT, UF_BRANCH_KNOWLEDGE, UF_EDUCATION, UF_HOUSE2, UF_STRUCTURE2, UF_FLAT2, UF_PLACE_REGISTR, UF_SCAN_PASSPORT1, UF_SCAN_PASSPORT2, UF_PASSPORT_SERIES, UF_RGB_CARD_NUMBER, UF_RGB_USER_ID, UF_REGISTER_TYPE, UF_CITIZENSHIP, UF_REGION, UF_REGION2, UF_AREA, UF_AREA2, UF_ID_REQUEST, UF_LIBRARIES, UF_BANK_NAME, UF_BANK_INN, UF_BANK_KPP, UF_BANK_BIC, UF_COR_ACCOUNT, UF_PERSONAL_ACCOUNT, UF_SETTLE_ACCOUNT, UF_FOUNDATION, UF_PERSON_INN, UF_PERSON_KPP, UF_PERSON_OKPO, UF_PERSON_OGRN, UF_PERSON_OKATO, UF_PERSON_KBK, UF_POSITION_HEAD, UF_FIO_HEAD, UF_CONTACT_INFO, UF_CONTACT_PHONE, UF_CONTACT_MOBILE, UF_DOCUMENTS, UF_WCHZ) VALUES (11985, '71644eeda65eb380607486e327e85178', '2016-09-01 11:51:42', null, null, null, 42, null, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'a:0:{}', null);

INSERT INTO b_user_group (USER_ID, GROUP_ID, DATE_ACTIVE_FROM, DATE_ACTIVE_TO) VALUES (11985, 3, null, null);
INSERT INTO b_user_group (USER_ID, GROUP_ID, DATE_ACTIVE_FROM, DATE_ACTIVE_TO) VALUES (11985, 4, null, null);
INSERT INTO b_user_group (USER_ID, GROUP_ID, DATE_ACTIVE_FROM, DATE_ACTIVE_TO) VALUES (11985, 13, null, null);

INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (1, '2016-04-19 12:38:41', 'Y', 1, 'N', 'Администраторы', 'Полный доступ к управлению сайтом.', 'a:12:{s:15:"SESSION_TIMEOUT";s:2:"15";s:15:"SESSION_IP_MASK";s:15:"255.255.255.255";s:13:"MAX_STORE_NUM";s:1:"1";s:13:"STORE_IP_MASK";s:15:"255.255.255.255";s:13:"STORE_TIMEOUT";s:4:"4320";s:17:"CHECKWORD_TIMEOUT";s:2:"60";s:15:"PASSWORD_LENGTH";s:1:"6";s:18:"PASSWORD_UPPERCASE";N;s:18:"PASSWORD_LOWERCASE";N;s:15:"PASSWORD_DIGITS";N;s:20:"PASSWORD_PUNCTUATION";N;s:14:"LOGIN_ATTEMPTS";s:1:"3";}', 'admin');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (2, '2014-07-02 10:34:14', 'Y', 2, 'Y', 'Все пользователи (в том числе неавторизованные)', 'Все пользователи, включая неавторизованных.', null, null);
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (3, '2016-03-28 14:04:35', 'Y', 3, 'N', 'Пользователи, имеющие право голосовать за рейтинг', 'В эту группу пользователи добавляются автоматически.', 'a:0:{}', 'RATING_VOTE');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (4, '2014-07-02 10:34:14', 'Y', 4, 'N', 'Пользователи имеющие право голосовать за авторитет', 'В эту группу пользователи добавляются автоматически.', null, 'RATING_VOTE_AUTHORITY');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (5, '2015-01-25 23:38:28', 'Y', 1000, 'N', 'Контент-редактор', '', 'a:0:{}', 'content');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (6, '2015-08-21 16:21:20', 'Y', 100, 'N', 'Библиотека - Администратор', '', 'a:0:{}', 'library_admin');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (7, '2015-06-09 14:43:36', 'Y', 200, 'N', 'Библиотека - Редактор', '', 'a:0:{}', 'library_editor');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (8, '2015-06-09 14:43:12', 'Y', 300, 'N', 'Библиотека - Контроллер', '', 'a:0:{}', 'library_contorller');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (9, '2014-10-21 16:55:39', 'Y', 500, 'N', 'Правообладатель', '', 'a:0:{}', 'rightholder');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (10, '2016-03-28 14:13:00', 'Y', 100, 'N', 'Контролер учетных записей', '', 'a:0:{}', '');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (11, '2015-01-16 17:11:19', 'Y', 100, 'N', 'Пользователи, не прошедшие проверку документов', '', 'a:0:{}', '');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (12, '2015-01-30 14:25:10', 'Y', 100, 'N', 'Специалист НЭБ', 'Пользователь ответчает в форуме, работает с обращениями через форму обратной связи', 'a:0:{}', '');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (13, '2016-05-12 16:02:31', 'Y', 100, 'N', 'Глобальный оператор НЭБ', '', 'a:0:{}', 'operator');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (14, '2016-04-20 10:48:05', 'Y', 100, 'N', 'Оператор ВЧЗ', '', 'a:0:{}', 'operator_wchz');
INSERT INTO b_group (ID, TIMESTAMP_X, ACTIVE, C_SORT, ANONYMOUS, NAME, DESCRIPTION, SECURITY_POLICY, STRING_ID) VALUES (15, '2016-05-24 21:02:07', 'Y', 500, 'N', 'Вход в админку', null, null, 'admin_panel');



INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (142, 'USER', 'UF_AREA', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (143, 'USER', 'UF_AREA2', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (150, 'USER', 'UF_BANK_BIC', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (148, 'USER', 'UF_BANK_INN', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (149, 'USER', 'UF_BANK_KPP', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (147, 'USER', 'UF_BANK_NAME', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:50;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (28, 'USER', 'UF_BRANCH_KNOWLEDGE', 'enumeration', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:3:{s:7:"DISPLAY";s:4:"LIST";s:11:"LIST_HEIGHT";i:5;s:16:"CAPTION_NO_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (139, 'USER', 'UF_CITIZENSHIP', 'enumeration', null, 500, 'N', 'N', 'N', 'Y', 'Y', 'Y', 'a:3:{s:7:"DISPLAY";s:4:"LIST";s:11:"LIST_HEIGHT";i:5;s:16:"CAPTION_NO_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (163, 'USER', 'UF_CONTACT_INFO', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (165, 'USER', 'UF_CONTACT_MOBILE', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (164, 'USER', 'UF_CONTACT_PHONE', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (151, 'USER', 'UF_COR_ACCOUNT', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (25, 'USER', 'UF_CORPUS', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (171, 'USER', 'UF_DOCUMENTS', 'file', '', 100, 'Y', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:10:"LIST_WIDTH";i:200;s:11:"LIST_HEIGHT";i:200;s:13:"MAX_SHOW_SIZE";i:0;s:16:"MAX_ALLOWED_SIZE";i:0;s:10:"EXTENSIONS";a:0:{}}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (29, 'USER', 'UF_EDUCATION', 'enumeration', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:3:{s:7:"DISPLAY";s:4:"LIST";s:11:"LIST_HEIGHT";i:5;s:16:"CAPTION_NO_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (162, 'USER', 'UF_FIO_HEAD', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (27, 'USER', 'UF_FLAT', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (33, 'USER', 'UF_FLAT2', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (154, 'USER', 'UF_FOUNDATION', 'enumeration', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:3:{s:7:"DISPLAY";s:4:"LIST";s:11:"LIST_HEIGHT";i:5;s:16:"CAPTION_NO_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (31, 'USER', 'UF_HOUSE2', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (144, 'USER', 'UF_ID_REQUEST', 'string', '', 100, 'N', 'N', 'N', 'N', 'N', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (24, 'USER', 'UF_ISSUED', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (146, 'USER', 'UF_LIBRARIES', 'iblock_element', '', 100, 'Y', 'N', 'I', 'Y', 'Y', 'N', 'a:5:{s:7:"DISPLAY";s:4:"LIST";s:11:"LIST_HEIGHT";i:5;s:9:"IBLOCK_ID";i:4;s:13:"DEFAULT_VALUE";s:0:"";s:13:"ACTIVE_FILTER";s:1:"Y";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (18, 'USER', 'UF_LIBRARY', 'iblock_element', '', 100, 'N', 'N', 'I', 'Y', 'Y', 'N', 'a:5:{s:7:"DISPLAY";s:4:"LIST";s:11:"LIST_HEIGHT";i:5;s:9:"IBLOCK_ID";i:4;s:13:"DEFAULT_VALUE";s:0:"";s:13:"ACTIVE_FILTER";s:1:"Y";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (12, 'USER', 'UF_NUM_ECHB', 'string', '', 100, 'N', 'N', 'I', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:40;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (23, 'USER', 'UF_PASSPORT_NUMBER', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (135, 'USER', 'UF_PASSPORT_SERIES', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (155, 'USER', 'UF_PERSON_INN', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (160, 'USER', 'UF_PERSON_KBK', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (156, 'USER', 'UF_PERSON_KPP', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (158, 'USER', 'UF_PERSON_OGRN', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (159, 'USER', 'UF_PERSON_OKATO', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (157, 'USER', 'UF_PERSON_OKPO', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (152, 'USER', 'UF_PERSONAL_ACCOUNT', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (34, 'USER', 'UF_PLACE_REGISTR', 'boolean', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:2:{s:13:"DEFAULT_VALUE";i:0;s:7:"DISPLAY";s:8:"CHECKBOX";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (161, 'USER', 'UF_POSITION_HEAD', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (140, 'USER', 'UF_REGION', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (141, 'USER', 'UF_REGION2', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (138, 'USER', 'UF_REGISTER_TYPE', 'enumeration', '', 100, 'N', 'N', 'I', 'Y', 'Y', 'N', 'a:3:{s:7:"DISPLAY";s:4:"LIST";s:11:"LIST_HEIGHT";i:5;s:16:"CAPTION_NO_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (136, 'USER', 'UF_RGB_CARD_NUMBER', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (137, 'USER', 'UF_RGB_USER_ID', 'string', '', 100, 'N', 'N', 'E', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (35, 'USER', 'UF_SCAN_PASSPORT1', 'file', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:10:"LIST_WIDTH";i:200;s:11:"LIST_HEIGHT";i:200;s:13:"MAX_SHOW_SIZE";i:0;s:16:"MAX_ALLOWED_SIZE";i:0;s:10:"EXTENSIONS";a:0:{}}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (36, 'USER', 'UF_SCAN_PASSPORT2', 'file', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:10:"LIST_WIDTH";i:200;s:11:"LIST_HEIGHT";i:200;s:13:"MAX_SHOW_SIZE";i:0;s:16:"MAX_ALLOWED_SIZE";i:0;s:10:"EXTENSIONS";a:0:{}}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (13, 'USER', 'UF_SEARCH_PAGE_COUNT', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (153, 'USER', 'UF_SETTLE_ACCOUNT', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (22, 'USER', 'UF_STATUS', 'enumeration', '', 100, 'N', 'N', 'I', 'Y', 'Y', 'Y', 'a:3:{s:7:"DISPLAY";s:8:"CHECKBOX";s:11:"LIST_HEIGHT";i:5;s:16:"CAPTION_NO_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (26, 'USER', 'UF_STRUCTURE', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (32, 'USER', 'UF_STRUCTURE2', 'string', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:20;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (10, 'USER', 'UF_TOKEN', 'string', '', 100, 'N', 'N', 'I', 'Y', 'Y', 'N', 'a:6:{s:4:"SIZE";i:35;s:4:"ROWS";i:1;s:6:"REGEXP";s:0:"";s:10:"MIN_LENGTH";i:0;s:10:"MAX_LENGTH";i:0;s:13:"DEFAULT_VALUE";s:0:"";}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (11, 'USER', 'UF_TOKEN_ADD_DATE', 'datetime', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:1:{s:13:"DEFAULT_VALUE";a:2:{s:4:"TYPE";s:4:"NONE";s:5:"VALUE";s:0:"";}}');
INSERT INTO b_user_field (ID, ENTITY_ID, FIELD_NAME, USER_TYPE_ID, XML_ID, SORT, MULTIPLE, MANDATORY, SHOW_FILTER, SHOW_IN_LIST, EDIT_IN_LIST, IS_SEARCHABLE, SETTINGS) VALUES (175, 'USER', 'UF_WCHZ', 'iblock_element', '', 100, 'N', 'N', 'N', 'Y', 'Y', 'N', 'a:5:{s:7:"DISPLAY";s:4:"LIST";s:11:"LIST_HEIGHT";i:5;s:9:"IBLOCK_ID";i:7;s:13:"DEFAULT_VALUE";s:0:"";s:13:"ACTIVE_FILTER";s:1:"N";}');


INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (1, 8, 'Новые', 'Y', 100, 'NEW');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (2, 8, 'В работе', 'N', 200, 'PROCESSING');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (3, 8, 'Внедрено', 'N', 300, 'COMPLETED');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (4, 22, 'Верифицированный', 'N', 10, 'user_status_verified');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (5, 28, 'Философские и политические науки/Philosophical and political science', 'N', 500, '1');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (6, 28, 'Психология/Psychology', 'N', 500, '2');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (7, 28, 'Социология/Sociology', 'N', 500, '3');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (8, 28, 'Физико-математические науки/Physical and mathematical sciences', 'N', 500, '4');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (9, 28, 'Химические науки/Chemical sciences', 'N', 500, '5');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (10, 28, 'Геолого-минералогические науки/Geological-mineralogical sciences', 'N', 500, '6');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (11, 28, 'Биологические науки/Biological sciences', 'N', 500, '7');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (12, 28, 'Технические науки/Technical sciences', 'N', 500, '8');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (13, 28, 'Сельскохозяйственные науки/Agricultural sciences', 'N', 500, '9');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (14, 28, 'Медицинские и фармацевтические науки/Medical and pharmaceutical sciences', 'N', 500, '10');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (15, 28, 'Исторические науки/Historical Sciences', 'N', 500, '11');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (16, 28, 'Экономические науки/Economics', 'N', 500, '12');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (17, 28, 'Юридические науки/Jurisprudence', 'N', 500, '13');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (18, 28, 'Военные науки/Military science', 'N', 500, '14');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (19, 28, 'Педагогические науки, культурологические, спорт/Pedagogical sciences, cultural, sports', 'N', 500, '15');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (20, 28, 'Филологические науки/Philology', 'N', 500, '16');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (21, 28, 'Искусствоведение, архитектура/Arts, Architecture', 'N', 500, '17');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (22, 28, 'Прочее/Others', 'N', 1500, '18');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (23, 29, 'Доктор наук/Doctor of Sciences', 'N', 500, '1');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (24, 29, 'Кандидат наук/Candidate of Sciences', 'N', 500, '2');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (25, 29, 'Аспирант/Postgraduate ', 'N', 500, '3');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (26, 29, 'Специалист с высшим образованием/Specialists with higher education', 'N', 500, '4');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (27, 29, 'Студент 1 курса/1st year student', 'N', 500, '5');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (28, 29, 'Студент 2 курса/2nd year student', 'N', 500, '6');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (29, 29, 'Студент 3 курса/3rd year student', 'N', 500, '7');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (30, 29, 'Студент 4 курса/4th year student', 'N', 500, '8');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (31, 29, 'Студент дипломник/Graduate student', 'N', 500, '9');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (32, 29, 'Дипломат/Diplomat', 'N', 500, '10');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (33, 29, 'Научный работник иностранец/Researcher foreigner', 'N', 500, '11');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (34, 29, 'Студент иностранец/Student foreigner', 'N', 500, '12');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (35, 29, 'Прочие (гость, турист, и т.д.)/Other (visitors, tourists, etc.)', 'N', 500, '13');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (36, 29, 'Специалист со средним и сред.специальным образованием/Specialist with secondary and special education environments', 'N', 500, '14');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (37, 138, 'Упрощенная', 'N', 500, 'simple');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (38, 138, 'Регистрация для обычных пользователей', 'N', 500, 'full');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (39, 138, 'Регистрация для читателей РГБ', 'N', 500, 'rgb');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (40, 138, 'Регистрация через ЕСИА', 'N', 500, 'ecia');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (41, 138, 'Правообладатель физическое лицо', 'N', 500, 'rightholder');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (42, 22, 'Зарегистрированный', 'Y', 20, 'user_status_registered');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (43, 22, 'VIP', 'N', 30, 'user_status_vip');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (44, 139, 'Австралия', 'N', 500, '1');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (45, 139, 'Австрия', 'N', 500, '2');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (46, 139, 'Азербайджан', 'N', 500, '3');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (47, 139, 'Албания', 'N', 500, '4');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (48, 139, 'Алжир', 'N', 500, '5');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (49, 139, 'Ангилья', 'N', 500, '6');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (50, 139, 'Ангола', 'N', 500, '7');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (51, 139, 'Андорра', 'N', 500, '8');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (52, 139, 'Антигуа и Барбуда', 'N', 500, '9');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (53, 139, 'Аргентина', 'N', 500, '10');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (54, 139, 'Армения', 'N', 500, '11');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (55, 139, 'Афганистан', 'N', 500, '12');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (56, 139, 'Багамы', 'N', 500, '13');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (57, 139, 'Бангладеш', 'N', 500, '14');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (58, 139, 'Барбадос', 'N', 500, '15');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (59, 139, 'Бахрейн', 'N', 500, '16');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (60, 139, 'Белоруссия', 'N', 500, '17');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (61, 139, 'Белиз', 'N', 500, '18');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (62, 139, 'Бельгия', 'N', 500, '19');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (63, 139, 'Бенин', 'N', 500, '20');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (64, 139, 'Болгария', 'N', 500, '21');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (65, 139, 'Боливия', 'N', 500, '22');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (66, 139, 'Босния и Герцеговина', 'N', 500, '23');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (67, 139, 'Ботсвана', 'N', 500, '24');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (68, 139, 'Бразилия', 'N', 500, '25');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (69, 139, 'Бруней', 'N', 500, '26');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (70, 139, 'Буркина Фасо', 'N', 500, '27');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (71, 139, 'Бурунди', 'N', 500, '28');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (72, 139, 'Бутан', 'N', 500, '29');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (73, 139, 'Вануату', 'N', 500, '30');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (74, 139, 'Ватикан', 'N', 500, '31');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (75, 139, 'Великобритания', 'N', 500, '32');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (76, 139, 'Венгрия', 'N', 500, '33');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (77, 139, 'Венесуэла', 'N', 500, '34');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (78, 139, 'Восточный Тимор', 'N', 500, '35');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (79, 139, 'Вьетнам', 'N', 500, '36');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (80, 139, 'Габон', 'N', 500, '37');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (81, 139, 'Гаити', 'N', 500, '38');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (82, 139, 'Гайана', 'N', 500, '39');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (83, 139, 'Гамбия', 'N', 500, '40');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (84, 139, 'Гана', 'N', 500, '41');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (85, 139, 'Гватемала', 'N', 500, '42');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (86, 139, 'Гвинея', 'N', 500, '43');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (87, 139, 'Гвинея-Бисау', 'N', 500, '44');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (88, 139, 'Германия', 'N', 500, '45');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (89, 139, 'Гондурас', 'N', 500, '46');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (90, 139, 'Гренада', 'N', 500, '47');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (91, 139, 'Греция', 'N', 500, '48');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (92, 139, 'Грузия', 'N', 500, '49');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (93, 139, 'Дания', 'N', 500, '50');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (94, 139, 'Джибути', 'N', 500, '51');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (95, 139, 'Доминика', 'N', 500, '52');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (96, 139, 'Доминиканская Республика', 'N', 500, '53');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (97, 139, 'Египет', 'N', 500, '54');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (98, 139, 'Замбия', 'N', 500, '55');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (99, 139, 'Зимбабве', 'N', 500, '56');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (100, 139, 'Израиль', 'N', 500, '57');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (101, 139, 'Индия', 'N', 500, '58');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (102, 139, 'Индонезия', 'N', 500, '59');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (103, 139, 'Иордания', 'N', 500, '60');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (104, 139, 'Ирак', 'N', 500, '61');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (105, 139, 'Иран', 'N', 500, '62');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (106, 139, 'Ирландия', 'N', 500, '63');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (107, 139, 'Исландия', 'N', 500, '64');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (108, 139, 'Испания', 'N', 500, '65');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (109, 139, 'Италия', 'N', 500, '66');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (110, 139, 'Йемен', 'N', 500, '67');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (111, 139, 'Кабо-Верде', 'N', 500, '68');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (112, 139, 'Казахстан', 'N', 500, '69');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (113, 139, 'Камбоджа', 'N', 500, '70');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (114, 139, 'Камерун', 'N', 500, '71');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (115, 139, 'Канада', 'N', 500, '72');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (116, 139, 'Катар', 'N', 500, '73');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (117, 139, 'Кения', 'N', 500, '74');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (118, 139, 'Кипр', 'N', 500, '75');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (119, 139, 'Киргизия', 'N', 500, '76');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (120, 139, 'Кирибати', 'N', 500, '77');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (121, 139, 'КНР', 'N', 500, '78');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (122, 139, 'Коморы', 'N', 500, '79');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (123, 139, 'Республика Конго', 'N', 500, '80');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (124, 139, 'ДР Конго', 'N', 500, '81');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (125, 139, 'Колумбия', 'N', 500, '82');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (126, 139, 'КНДР', 'N', 500, '83');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (127, 139, 'Республика Корея', 'N', 500, '84');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (128, 139, 'Коста-Рика', 'N', 500, '85');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (129, 139, 'Кот-д’Ивуар', 'N', 500, '86');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (130, 139, 'Куба', 'N', 500, '87');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (131, 139, 'Кувейт', 'N', 500, '88');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (132, 139, 'Лаос', 'N', 500, '89');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (133, 139, 'Латвия', 'N', 500, '90');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (134, 139, 'Лесото', 'N', 500, '91');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (135, 139, 'Либерия', 'N', 500, '92');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (136, 139, 'Ливан', 'N', 500, '93');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (137, 139, 'Ливия', 'N', 500, '94');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (138, 139, 'Литва', 'N', 500, '95');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (139, 139, 'Лихтенштейн', 'N', 500, '96');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (140, 139, 'Люксембург', 'N', 500, '97');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (141, 139, 'Маврикий', 'N', 500, '98');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (142, 139, 'Мавритания', 'N', 500, '99');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (143, 139, 'Мадагаскар', 'N', 500, '100');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (144, 139, 'Македония', 'N', 500, '101');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (145, 139, 'Малави', 'N', 500, '102');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (146, 139, 'Малайзия', 'N', 500, '103');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (147, 139, 'Мали', 'N', 500, '104');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (148, 139, 'Мальдивы', 'N', 500, '105');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (149, 139, 'Мальта', 'N', 500, '106');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (150, 139, 'Марокко', 'N', 500, '107');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (151, 139, 'Маршалловы Острова', 'N', 500, '108');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (152, 139, 'Мексика', 'N', 500, '109');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (153, 139, 'Мозамбик', 'N', 500, '110');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (154, 139, 'Молдавия', 'N', 500, '111');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (155, 139, 'Монако', 'N', 500, '112');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (156, 139, 'Монголия', 'N', 500, '113');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (157, 139, 'Мьянма', 'N', 500, '114');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (158, 139, 'Намибия', 'N', 500, '115');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (159, 139, 'Науру', 'N', 500, '116');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (160, 139, 'Непал', 'N', 500, '117');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (161, 139, 'Нигер', 'N', 500, '118');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (162, 139, 'Нигерия', 'N', 500, '119');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (163, 139, 'Нидерланды', 'N', 500, '120');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (164, 139, 'Никарагуа', 'N', 500, '121');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (165, 139, 'Новая Зеландия', 'N', 500, '122');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (166, 139, 'Норвегия', 'N', 500, '123');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (167, 139, 'ОАЭ', 'N', 500, '124');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (168, 139, 'Оман', 'N', 500, '125');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (169, 139, 'Пакистан', 'N', 500, '126');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (170, 139, 'Палау', 'N', 500, '127');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (171, 139, 'Панама', 'N', 500, '128');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (172, 139, 'Папуа', 'N', 500, '129');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (173, 139, 'Парагвай', 'N', 500, '130');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (174, 139, 'Перу', 'N', 500, '131');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (175, 139, 'Польша', 'N', 500, '132');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (176, 139, 'Португалия', 'N', 500, '133');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (177, 139, 'Россия', 'Y', 500, '134');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (178, 139, 'Руанда', 'N', 500, '135');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (179, 139, 'Румыния', 'N', 500, '136');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (180, 139, 'Сальвадор', 'N', 500, '137');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (181, 139, 'Самоа', 'N', 500, '138');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (182, 139, 'Сан-Марино', 'N', 500, '139');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (183, 139, 'Сан-Томе и Принсипи', 'N', 500, '140');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (184, 139, 'Саудовская Аравия', 'N', 500, '141');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (185, 139, 'Свазиленд', 'N', 500, '142');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (186, 139, 'Сейшельские Острова', 'N', 500, '143');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (187, 139, 'Сенегал', 'N', 500, '144');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (188, 139, 'Сент-Винсент и Гренадины', 'N', 500, '145');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (189, 139, 'Сент-Китс и Невис', 'N', 500, '146');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (190, 139, 'Сент-Люсия', 'N', 500, '147');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (191, 139, 'Сербия', 'N', 500, '148');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (192, 139, 'Сингапур', 'N', 500, '149');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (193, 139, 'Сирия', 'N', 500, '150');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (194, 139, 'Словакия', 'N', 500, '151');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (195, 139, 'Словения', 'N', 500, '152');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (196, 139, 'США', 'N', 500, '153');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (197, 139, 'Соломоновы Острова', 'N', 500, '154');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (198, 139, 'Сомали', 'N', 500, '155');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (199, 139, 'Судан', 'N', 500, '156');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (200, 139, 'Суринам', 'N', 500, '157');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (201, 139, 'Сьерра-Леоне', 'N', 500, '158');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (202, 139, 'Таджикистан', 'N', 500, '159');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (203, 139, 'Тайвань', 'N', 500, '160');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (204, 139, 'Таиланд', 'N', 500, '161');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (205, 139, 'Танзания', 'N', 500, '162');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (206, 139, 'Того', 'N', 500, '163');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (207, 139, 'Тонга', 'N', 500, '164');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (208, 139, 'Тринидад и Тобаго', 'N', 500, '165');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (209, 139, 'Тувалу', 'N', 500, '166');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (210, 139, 'Тунис', 'N', 500, '167');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (211, 139, 'Туркмения', 'N', 500, '168');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (212, 139, 'Турция', 'N', 500, '169');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (213, 139, 'Уганда', 'N', 500, '170');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (214, 139, 'Узбекистан', 'N', 500, '171');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (215, 139, 'Украина', 'N', 500, '172');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (216, 139, 'Уругвай', 'N', 500, '173');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (217, 139, 'Фиджи', 'N', 500, '174');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (218, 139, 'Филиппины', 'N', 500, '175');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (219, 139, 'Финляндия', 'N', 500, '176');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (220, 139, 'Франция', 'N', 500, '177');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (221, 139, 'Хорватия', 'N', 500, '178');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (222, 139, 'Центрально-Африканская республика', 'N', 500, '179');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (223, 139, 'Чад', 'N', 500, '180');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (224, 139, 'Черногория', 'N', 500, '181');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (225, 139, 'Чехия', 'N', 500, '182');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (226, 139, 'Чили', 'N', 500, '183');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (227, 139, 'Швейцария', 'N', 500, '184');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (228, 139, 'Швеция', 'N', 500, '185');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (229, 139, 'Шри-Ланка', 'N', 500, '186');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (230, 139, 'Эквадор', 'N', 500, '187');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (231, 139, 'Экваториальная Гвинея', 'N', 500, '188');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (232, 139, 'Эритрея', 'N', 500, '189');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (233, 139, 'Эстония', 'N', 500, '190');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (234, 139, 'Эфиопия', 'N', 500, '191');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (235, 139, 'ЮАР', 'N', 500, '192');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (236, 139, 'Южный Судан', 'N', 500, '193');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (237, 139, 'Ямайка', 'N', 500, '194');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (238, 139, 'Япония', 'N', 500, '195');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (239, 139, 'Прочее', 'N', 500, '196');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (240, 154, 'автор', 'N', 500, '3429a9c53c0dfc25421bdc1b89555b26');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (241, 154, 'соавтор', 'N', 500, 'd5a0463a6c3f12f16c30a4bdf1715a46');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (242, 154, 'наследник', 'N', 500, 'c1633e56dd1d34a818b6776dfab98a4e');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (243, 154, 'договор', 'N', 500, '95c94fd72815c69bad36032930f601c3');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (244, 154, 'иное', 'N', 500, '758f02b332fa246b5c17d10f926fe45b');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (245, 138, 'Правообладатель юридическое лицо', 'N', 500, 'rightholder_entity');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (246, 22, 'Заблокирован', 'N', 500, 'user_status_blocked');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (247, 22, 'закончился срок действия ЧБ', 'N', 500, 'user_status_expired');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (248, 176, 'На согласовании оператором НЭБ', 'N', 500, '1');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (249, 176, 'Согласовано', 'N', 500, '2');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (250, 176, 'На оцифровке', 'N', 500, '3');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (251, 176, 'Отменено', 'N', 500, '4');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (252, 176, 'Оцифровано', 'N', 500, '5');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (253, 176, 'Выполнено', 'N', 500, '6');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (264, 189, 'Библиотеки', 'N', 500, 'libraries');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (265, 189, 'Пользователи', 'N', 500, 'users');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (266, 189, 'ВЧЗ', 'N', 500, 'workplaces');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (267, 189, 'Статистика', 'N', 500, 'statistics');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (268, 189, 'Фонды', 'N', 500, 'funds');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (269, 189, 'План оцифровки', 'N', 500, 'digitization');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (270, 189, 'Новости', 'N', 500, 'news');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (271, 189, 'Форум', 'N', 500, 'forum');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (272, 189, 'Опросы', 'N', 500, 'polls');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (273, 189, 'Правообладтели', 'N', 500, 'rightholders');
INSERT INTO b_user_field_enum (ID, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID) VALUES (274, 189, 'Экспорт плана оцифровки', 'N', 500, 'digitization_export');


INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (5, 'ru', 'Категория', null, null, null, null);
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (6, 'ru', 'ID официального ответа', null, null, null, null);
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (7, 'ru', 'Дубликат', null, null, null, null);
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (8, 'ru', 'Статус', null, null, null, null);
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (10, 'en', 'Token access', 'Token access', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (10, 'ru', 'Ключ доступа', 'Ключ доступа', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (11, 'en', 'Token access update date', 'Token access update date', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (11, 'ru', 'Дата добавления ключа доступа', 'Дата добавления ключа доступа', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (12, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (12, 'ru', 'Номер электронного читательского билета', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (13, 'en', 'Count search', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (13, 'ru', 'Количество результатов поиска', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (14, 'ru', 'Привязка к книге в Exalead', null, null, null, null);
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (15, 'ru', 'Координаты библиотеки на карте', null, null, null, null);
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (16, 'en', 'Library', 'Library', 'Library', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (16, 'ru', 'Библиотека', 'Библиотека', 'Библиотека', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (17, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (17, 'ru', 'Показывать в слайдере', 'Показывать в слайдере', 'Показывать в слайдере', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (18, 'en', 'Library', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (18, 'ru', 'Библиотека', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (19, 'en', 'Added to favorites', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (19, 'ru', 'Добавленно в избранное', 'Добавленно в избранное', 'Добавленно в избранное', 'Добавленно в избранное', 'Добавленно в избранное');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (20, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (20, 'ru', 'Количество просмотров', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (22, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (22, 'ru', 'Статус', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (23, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (23, 'ru', 'Номер паспорта', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (24, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (24, 'ru', 'Кем выдан / как в паспорте', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (25, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (25, 'ru', 'Корпус', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (26, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (26, 'ru', 'Строение', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (27, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (27, 'ru', 'Квартира', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (28, 'en', 'Отрасль знаний', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (28, 'ru', 'Отрасль знаний', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (29, 'en', 'Образование', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (29, 'ru', 'Образование', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (31, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (31, 'ru', 'Дом', '', '', '', 'Проживания');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (32, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (32, 'ru', 'Строение', '', '', '', 'Проживания');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (33, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (33, 'ru', 'Квартира', '', '', '', 'Проживания');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (34, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (34, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (35, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (35, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (36, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (36, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (37, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (37, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (38, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (38, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (39, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (39, 'ru', 'Дата добавления', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (40, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (40, 'ru', 'Конечная дата оцифровки', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (41, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (41, 'ru', 'Дата добавления', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (42, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (42, 'ru', 'Найдено', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (43, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (43, 'ru', 'Запрос', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (44, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (44, 'ru', 'Дополнительные параметры', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (45, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (45, 'ru', 'Адрес', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (46, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (46, 'ru', 'ID пользователя', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (47, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (47, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (48, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (48, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (49, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (49, 'ru', 'Название', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (50, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (50, 'ru', 'Показывать название коллекции в слайдере', 'Показывать название коллекции в слайдере', 'Показывать название коллекции в слайдере', '', 'Показывать название коллекции в поиске');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (51, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (51, 'ru', 'ID пользователя', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (52, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (52, 'ru', 'Название', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (53, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (53, 'ru', 'Дота добавления', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (54, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (54, 'ru', 'Дата изменения', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (55, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (55, 'ru', 'ID пользователя', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (57, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (57, 'ru', 'ID книги', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (58, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (58, 'ru', 'Дата добавления', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (61, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (61, 'ru', 'ID Коллекции', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (62, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (62, 'ru', 'ID объекта', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (63, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (63, 'ru', 'Тип объекта', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (64, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (64, 'ru', 'Сортировка', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (65, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (65, 'ru', 'Количество цитат', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (66, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (66, 'ru', 'Количество закладок', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (67, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (67, 'ru', 'Количество заметок', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (68, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (68, 'ru', 'Читаю\\Прочитал', '', '', '', '0 не читал. 1 начал читать. 2 закончил');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (69, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (69, 'ru', 'Процент прочтения книги', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (70, 'en', 'ID', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (70, 'ru', 'ID', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (71, 'en', '(dont know)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (71, 'ru', '(неизвестное назначение)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (72, 'en', 'Name', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (72, 'ru', 'Название', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (73, 'en', 'Open date', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (73, 'ru', 'Дата открытия', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (74, 'en', 'Structure', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (74, 'ru', 'Структура', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (75, 'en', 'Address', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (75, 'ru', 'Адрес', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (76, 'en', 'Region (region)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (76, 'ru', 'Регион (region)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (77, 'en', 'District', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (77, 'ru', 'Район', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (78, 'en', 'Town', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (78, 'ru', 'Город', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (79, 'en', 'Phone', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (79, 'ru', 'Телефон', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (80, 'en', 'Comment', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (80, 'ru', 'Комментарий', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (81, 'en', '(dont know)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (81, 'ru', '(неизвестное назначение)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (82, 'en', '(dont know)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (82, 'ru', '(неизвестное назначение)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (83, 'en', 'Federal district', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (83, 'ru', 'Федеральный округ', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (84, 'en', 'Locality', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (84, 'ru', 'Населенный пункт', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (85, 'en', 'Coordinates', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (85, 'ru', 'Координаты', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (86, 'en', 'Region (area)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (86, 'ru', 'Регион (area)', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (87, 'en', 'Citi or region', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (87, 'ru', 'Город или регион', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (88, 'en', 'Description', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (88, 'ru', 'Описание', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (89, 'en', 'Full description', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (89, 'ru', 'Полное описание', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (90, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (90, 'ru', 'Федеральный округ', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (91, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (91, 'ru', 'Короткое название федерального округа', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (92, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (92, 'ru', 'Федеральный округ', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (93, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (93, 'ru', 'Город', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (94, 'en', 'City', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (94, 'ru', 'Город', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (95, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (95, 'ru', 'В избранных', '', '', '', 'Выводится в правой колонке формы выбора города');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (96, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (96, 'ru', 'В быстром выборе', '', '', '', 'Выводится в списке городов при выборе региона в форме выбора города');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (97, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (97, 'ru', 'Координаты', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (98, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (98, 'ru', 'Сортировка', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (99, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (99, 'ru', 'Сортировка', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (100, 'en', 'Comment', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (100, 'ru', 'Комментарий', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (101, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (101, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (102, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (102, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (103, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (103, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (104, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (104, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (105, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (105, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (106, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (106, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (107, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (107, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (108, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (108, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (109, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (109, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (110, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (110, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (111, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (111, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (112, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (112, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (113, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (113, 'ru', 'ID Пользователя', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (114, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (114, 'ru', 'ID Книги', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (115, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (115, 'ru', 'Номер страницы', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (116, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (116, 'ru', 'Дата добавления', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (117, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (117, 'ru', 'ID Пользователя', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (118, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (118, 'ru', 'ID Книги', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (119, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (119, 'ru', 'Номер страницы', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (120, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (120, 'ru', 'Текст заметки', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (121, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (121, 'ru', 'Дата добавления', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (122, 'en', 'Selected note text', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (122, 'ru', 'Выделенный текст заметки', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (123, 'en', 'data image base64', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (123, 'ru', 'data image base64', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (124, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (124, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (125, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (125, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (126, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (126, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (127, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (127, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (128, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (128, 'ru', 'Дата обновления', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (129, 'en', 'Library join date', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (129, 'ru', 'Дата подключения библиотеки', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (130, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (130, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (131, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (131, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (132, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (132, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (133, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (133, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (134, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (134, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (135, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (135, 'ru', 'Серия паспорта', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (136, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (136, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (137, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (137, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (138, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (138, 'ru', 'Тип регистрации', 'Тип регистрации', 'Тип регистрации', 'Тип регистрации', 'Тип регистрации');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (139, 'en', 'Citizenship', 'Citizenship', 'Citizenship', null, null);
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (139, 'ru', 'Гражданство', 'Гражданство', 'Гражданство', null, null);
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (140, 'en', 'Region', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (140, 'ru', 'Область/Регион', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (141, 'en', 'Region', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (141, 'ru', 'Область/Регион', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (142, 'en', 'Area', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (142, 'ru', 'Район', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (143, 'en', 'Area', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (143, 'ru', 'Район', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (144, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (144, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (146, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (146, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (147, 'en', 'Bank name', 'Bank name', 'Bank name', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (147, 'ru', 'Название банка', 'Название банка', 'Название банка', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (148, 'en', 'Bank INN', 'Bank INN', 'Bank INN', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (148, 'ru', 'ИНН банка', 'ИНН банка', 'ИНН банка', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (149, 'en', 'Bank KPP', 'Bank KPP', 'Bank KPP', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (149, 'ru', 'КПП банка', 'КПП банка', 'КПП банка', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (150, 'en', 'Bank BIC', 'Bank BIC', 'Bank BIC', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (150, 'ru', 'БИК банка', 'БИК банка', 'БИК банка', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (151, 'en', 'Correspondent account', 'Correspondent account', 'Correspondent account', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (151, 'ru', 'Корреспондентский счет', 'Корреспондентский счет', 'Корреспондентский счет', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (152, 'en', 'Personal account', 'Personal account', 'Personal account', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (152, 'ru', 'Лицевой счет', 'Лицевой счет', 'Лицевой счет', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (153, 'en', 'Settlement account', 'Settlement account', 'Settlement account', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (153, 'ru', 'Расчетный счет', 'Расчетный счет', 'Расчетный счет', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (154, 'en', 'Foundation', 'Foundation', 'Foundation', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (154, 'ru', 'Основание правообладания', 'Основание правообладания', 'Основание правообладания', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (155, 'en', 'Legal entity INN', 'Legal entity INN', 'Legal entity INN', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (155, 'ru', 'ИНН юридического лица', 'ИНН юридического лица', 'ИНН юридического лица', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (156, 'en', 'Legal entity KPP', 'Legal entity KPP', 'Legal entity KPP', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (156, 'ru', 'КПП юридического лица', 'КПП юридического лица', 'КПП юридического лица', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (157, 'en', 'Legal entity OKPO', 'Legal entity OKPO', 'Legal entity OKPO', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (157, 'ru', 'ОКПО юридического лица', 'ОКПО юридического лица', 'ОКПО юридического лица', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (158, 'en', 'Legal entity OGRN', 'Legal entity OGRN', 'Legal entity OGRN', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (158, 'ru', 'ОГРН юридического лица', 'ОГРН юридического лица', 'ОГРН юридического лица', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (159, 'en', 'Legal entity OKATO', 'Legal entity OKATO', 'Legal entity OKATO', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (159, 'ru', 'ОКАТО (ОКТМО) юридического лица', 'ОКАТО (ОКТМО) юридического лица', 'ОКАТО (ОКТМО) юридического лица', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (160, 'en', 'Legal entity KBK', 'Legal entity KBK', 'Legal entity KBK', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (160, 'ru', 'КБК юридического лица ', 'КБК юридического лица ', 'КБК юридического лица ', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (161, 'en', 'Position of the head', 'Position of the head', 'Position of the head', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (161, 'ru', 'Должность руководителя (полномочного представителя) ', 'Должность руководителя (полномочного представителя) ', 'Должность руководителя (полномочного представителя) ', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (162, 'en', 'Name of the head', 'Name of the head', 'Name of the head', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (162, 'ru', 'ФИО руководителя (полномочного представителя)', 'ФИО руководителя (полномочного представителя)', 'ФИО руководителя (полномочного представителя)', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (163, 'en', 'Information about the contact person', 'Information about the contact person', 'Information about the contact person', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (163, 'ru', 'ФИО контактного лица, полномочия контактного лица (Устав, доверенность, пр.)', 'ФИО контактного лица, полномочия контактного лица (Устав, доверенность, пр.)', 'ФИО контактного лица, полномочия контактного лица (Устав, доверенность, пр.)', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (164, 'en', 'Contact Phone', 'Contact Phone', 'Contact Phone', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (164, 'ru', 'Телефон контактного лица', 'Телефон контактного лица', 'Телефон контактного лица', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (165, 'en', 'Mobile phone contact person', 'Mobile phone contact person', 'Mobile phone contact person', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (165, 'ru', 'мобильный телефон контактного лица', 'мобильный телефон контактного лица', 'мобильный телефон контактного лица', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (167, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (167, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (168, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (168, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (169, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (169, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (170, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (170, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (171, 'en', 'Documents', 'Documents', 'Documents', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (171, 'ru', 'Перечень документов', 'Перечень документов', 'Перечень документов', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (172, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (172, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (173, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (173, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (174, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (174, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (175, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (175, 'ru', 'ВЧЗ', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (176, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (176, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (177, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (177, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (178, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (178, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (179, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (179, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (180, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (180, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (182, 'en', 'Full name', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (182, 'ru', 'Полное название', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (184, 'en', 'Contract', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (184, 'ru', 'Договор', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (185, 'en', 'RGB ID', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (185, 'ru', 'РГБ ID', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (186, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (186, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (187, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (187, 'ru', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (189, 'en', '', '', '', '', '');
INSERT INTO b_user_field_lang (USER_FIELD_ID, LANGUAGE_ID, EDIT_FORM_LABEL, LIST_COLUMN_LABEL, LIST_FILTER_LABEL, ERROR_MESSAGE, HELP_MESSAGE) VALUES (189, 'ru', '', '', '', '', '');
