<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

/**
 * Выдача ББК-массива разделов из Exalead
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class LibSearchFormBbk {

    private $iParent = '', $arResult = [];

    public function getResult() {

        $this->prepareQuery();
        $this->prepareModule();
        $this->getData();
        $this->showJson();
    }

    /**
     * Подготовка запроса
     *
     * @return bool|string
     */
    private function prepareQuery() {
        $this->iParent = intval($_REQUEST['id']);
    }

    /**
     * Подготовка модулей
     */
    private function prepareModule() {
        if (!Loader::includeModule('nota.exalead'))
            die();
    }

    private function getData() {

        $arResult = [];

        if (!Loader::includeModule('nota.exalead'))
            return $arResult;

        // формирование запроса

        $query = new SearchQuery($this->iParent > 0 ? 'bbk_parentid:' . $this->iParent : 'bbk_level:1');
        $query->setParam('hf', '100');
        $query->setParam('st', 'st_bbk');
        $query->setParam('of', 'json');
        $query->setParam('sl', 'sl_bbk');
        $query->setQueryType('json');

        // отправка запроса
        $client = new SearchClient();
        $res = $client->getResult($query);

        foreach ($res['hits'] as $arItem) {
            $iId = 0;  $sName = '';
            foreach ($arItem['metas'] as $arItem2) {
                if ($arItem2['name'] == 'bbk_id') {
                    $iId = intval($arItem2['value']);
                }
                elseif ($arItem2['name'] == 'bbk_fullname') {
                    $sName = $arItem2['value'];
                }
            }
            if ($iId && !empty($sName)) {
                $arResult[$iId] = $sName;
            }
        }

        $this->arResult =  $arResult;
    }

    /**
     * Вывод результата в виде JSON-массива
     */
    private function showJson() {
        MainHelper::showJson($this->arResult);
    }
}

$obAutoComplete = new LibSearchFormBbk();
$obAutoComplete->getResult();