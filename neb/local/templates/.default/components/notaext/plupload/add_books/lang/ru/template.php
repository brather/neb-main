<?php

$MESS['BROWSER_NOT_SUPPORTED'] = 'Ваш браузер не поддерживает загрузку файлов :-(';
$MESS['NO_FILE_SELECTED'] = 'Файл не выбран';
$MESS['SELECT_FILE'] = 'Выбрать файл';
$MESS['SELECT_FILES'] = 'Выбрать файлы';
$MESS['UPLOAD'] = 'Загрузить';
$MESS['PLUPLOAD_ADD_BOOKS_PDF_LINK'] = "Ссылка на pdf-версию книги";
$MESS['PLUPLOAD_ADD_BOOKS_ERROR_FORMAT'] = "Ошибка. Неверный формат файла";
$MESS['PLUPLOAD_ADD_BOOKS_LOAD_FILE'] = "Загрузите файл";
$MESS['PLUPLOAD_ADD_BOOKS_ADD'] = "Добавить";
$MESS['PLUPLOAD_ADD_BOOKS_UPDATE'] = "Обновить";
$MESS['PLUPLOAD_ADD_BOOKS_LOADED'] = "Загружено";

?>
