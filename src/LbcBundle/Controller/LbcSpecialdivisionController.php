<?php
/**
 * User: agolodkov
 * Date: 18.08.2016
 * Time: 12:42
 */

namespace LbcBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use LbcBundle\Entity\LbcSpecialdivision;

/**
 * @DI\Service("app.specialdivision", public=true, scope="request")
 */
class LbcSpecialdivisionController extends AbstractCategoryController
{
    /**
     * @DI\Inject("app.api.specialdivision")
     * @var \LbcBundle\Controller\Api\LbcLocationplansController
     */
    public $api;

    /**
     * @DI\Inject("app.api.maindivision")
     * @var \LbcBundle\Controller\Api\LbcMaindivisionController
     */
    public $ownerApi;

    /**
     * @param LbcSpecialdivision $entity
     */
    public function checkEntityActions($entity)
    {
        parent::checkEntityActions($entity);
        $em = $this->doctrine->getManager();
        foreach ($entity->getActions() as $key => $action) {
            switch ($action['code']) {
                case 'division-images':
                    if (!$em->getRepository('LbcBundle:LbcSpecialdivisionimages')->findOneBy(
                        [
                            'specialdivisionid' => $entity->getId(),
                        ]
                    )
                    ) {
                        $entity->removeAction($key);
                    }
                    break;
                case 'link-specialdivisionsections':
                    if (!$em->getRepository('LbcBundle:LbcSpecialdivisionsections')->findOneBy(
                        [
                            'specialdivisionid' => $entity->getId(),
                        ]
                    )
                    ) {
                        $entity->removeAction($key);
                    }
                    break;
            }
        }
    }

    /**
     * @return array
     */
    protected function _getRouterMaps()
    {
        return [
            'api_category_detail' => 'api_bbk_specialdivision_detail',
            'api_category_update' => 'api_bbk_specialdivision_update',
            'api_category_images' => 'api_bbk_specialdivision_images',
            'category'            => 'bbk_specialdivision',
            'category_index'      => 'bbk_specialdivision_index',
        ];
    }

    protected function _getIndexName()
    {
        return 'Специальные деления';
    }

    /**
     * @return array
     */
    protected function _getActionParams()
    {
        return [
            [
                'name' => 'Скан спц. делений',
                'code' => 'division-images',
                'id'   => 'division-images',
            ],
            [
                'name'  => 'Список разделов спец.делений',
                'code'  => 'link-specialdivisionsections',
                'link'  => true,
                'route' => 'bbk_specialdivision_specialdivisionsections',
            ],
        ];
    }
}