<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<!doctype html>
<html class="inmodalhtml">
<head>
	<title>wtf</title>
    <link href="/local/templates/adaptive/css/bootstrap.min.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic,800,800italic,300,300italic&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/local/templates/adaptive/css/font-awesome.min.css">
	<!--link rel="stylesheet" href="<?=MARKUP?>css/style.css"-->
	<link href="/local/templates/adaptive/css/style.css" rel="stylesheet">
	<link href="/local/templates/adaptive/css/low_vision.css" rel="stylesheet">
	<!--script type="text/javascript" src="/local/templates/adaptive/js/jquery/prejquery.js"></script-->
	<!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
	<script>
		if (self != top && typeof $ !== 'function') {
			window.$ = window.parent.jQuery;
		}
		var bodyClasses = $(window.parent.document).find('body').attr('class');
		$(document).find('body').addClass(bodyClasses).removeClass('modal-open');
	</script>

</head>
<body class="b-paidpopup b-uspopup inmodal">
<div class="modal-container">
<div class="container">

<?if(empty($arResult['PAID'])):?>
	<div class="b-scanpopup">
		<h2>Предпросмотр неоплаченного заказа</h2>
		<span class="b-popupsubtitle">изображения в низком качестве</span>
		<!--<a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arResult['ITEM']['ID']?>&action=pay" class="formbutton middle payorderlink">Оплатить</a> -->
	</div>
	<ul class="b-scanlist">
		<?
		if(!empty($arResult['FILES']))
		{
			foreach($arResult['FILES'] as $arItem)
			{
				// определяем место хранения закешированного файла и его название - хеш md5
				$sCacheFile = $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/scanned/"
					. md5_file($arItem['link']) . '.' . end(explode('.', $arItem['link']));

				// создаем кешированный файл
				if (!file_exists($sCacheFile))
					CFile::ResizeImageFile($arItem['link'], $sCacheFile, array('width' => 320, 'height' => 320), BX_RESIZE_IMAGE_PROPORTIONAL);

				// проверяем есть ли файл на диске
				if (file_exists($sCacheFile))
					$arItem['link'] = $sCacheFile;
				?>
				<li>
					<div class="b-scanlist_img">
						<img src="<?=image_to_base64($arItem['link'])?>">
					</div>
					<label class="checkbox">
						<input class="checkbox" type="checkbox" name="delete" data-file-toggler var="<?=$arItem['s']?>" value="<?=$arItem['id']?>" id="file_<?=$arItem['id']?>">
						<span class="lbl">Удалить</span>
					</label>
				</li>
			<?
			}
		}
		?>
	</ul>
	<?
	// Вывод пагинатора
	$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', '', array(
		'NAV_RESULT' => $arResult['NAV_RESULT'],
	));
	?>
	<div id="images_pay_control" class="container" data-skip="">
		<div>Сумма к оплате: <span id="final_price"><?=$arResult['PRICE']*$arResult['IMAGECOUNT']?></span> руб.</div>
		<div> 
			<a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arResult['ID']?>&action=pay" class="btn btn-primary pull-right" data-inmodal-pay="<?=$arResult['ID']?>" data-item="<?=$arResult['ID']?>">Оплатить изображения</a>
			<a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arResult['ID']?>&action=show&PAGEN_1=<?=$arResult['NAV_RESULT']->NavPageNomer?>" class="btn btn-default disabled delete">Удалить выбранное</a>
		</div>
	</div>

<? else: ?>

	<div class="b-scanpopup">
		<h2>Просмотр оплаченного заказа</h2>
		<span class="b-popupsubtitle">изображения в высоком качестве</span>
		<?
		if(count($arResult['FILES']) > 1)
		{
			?>
			<a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arResult['ID']?>&action=all_download" target="_blank" class="formbutton middle">скачать все</a>
		<?
		}
		?>
	</div>
	<ul class="b-scanlist">
		<?
		if(!empty($arResult['FILES']))
		{
			foreach($arResult['FILES'] as $k => $arItem)
			{
				?>
				<li>
					<div class="b-scanlist_img">
						<img src="<?=image_to_base64($arItem['link'])?>">
					</div>
					<div class="clearfix b-scanimg_act">
						<a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arResult['ID']?>&file=<?=$k?>&action=download" target="blank" class="b-scanlist_imgload left">скачать</a>
						<span class="b-scanlist_imgsize right"><?=round($arItem['size']/1024, 2)?> КБ</span>
					</div>
				</li>
			<?
			}
		}
		?>
	</ul>
	<?
	// Вывод пагинатора
	$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', '', array(
		'NAV_RESULT' => $arResult['NAV_RESULT'],
	));
	?>

<?  endif; ?>

<?
if($_REQUEST['Ajax_instances'] == 'Y' && $_REQUEST['listType'] == 'scan')
    exit();
?>

</span>

</div>
</div>
<script>
	var iframeDOCUMENT = $(document),
		parentDocument = $(window.parent.document); /*если наследовать $ из родительского окна (мы в ифрейме)*/
    iframeDOCUMENT.on('click','.pagination a', function(e){
        var href = $(this).attr('href');
        var listType = 'scan';

        if ($(this).attr('data-file') > 0){
            href = href+'&file='+$(this).attr('data-file');
            listType = 'delete';
        }

        if(href == '#' || href == '')
            return false;

        $.get(href, { Ajax_instances: "Y", listType: listType }, function( data ) {
            iframeDOCUMENT.find('.ajax-result').html(data);
        });
        return false;
    });

    iframeDOCUMENT.on('click','a.delete', function(){ 
    	/*console.log('delete');*/
        var href = $(this).attr('href');
        var list = null, res = '';
        list = iframeDOCUMENT.find('[data-file-toggler]:checked');
        list.each( function(ind) {
            res += $(this).val();
            if (ind < list.length - 1) res +=',';
        });
        if(res != '') {

            href = href + '&file=' + res;

            if (confirm("Вы подтверждаете удаление выбранных элементов?")) {

                $.get(href, {Ajax_instances: "Y", listType: "delete"}, function (data) {
                    iframeDOCUMENT.find('.ajax-result').html(data);
                });
            }
        }
        return false;
    });

    iframeDOCUMENT.on('click','[data-inmodal-pay]', function(e){
        var toggler = $(this),
        	id = toggler.data('inmodalPay'),
        	url = toggler.attr('href') || '/profile/scanning/';
    	/* console.log('payorderlink' + id + ' ' + url);*/

    	window.document.location.href = url;

    	/* $.ajax({
	         type: "POST",
	         url: url || '/profile/scanning/',
	         success: function(data) {
	         	console.log('success');
	             console.dir(data);
	         }
	     });
    	 $(parentDocument).find('[id="'+id+'"]').click()
         $("a#"+id).click();

         return false;*/
    });

    iframeDOCUMENT.on('change','[data-file-toggler]', function(e){
		var list = iframeDOCUMENT.find('[data-file-toggler]:checked'),
			deltext = 'Удалить выбранное';
		if (list.length > 0){
		   iframeDOCUMENT.find('#images_pay_control .delete').text(deltext + ' (' + list.length + ')').removeClass('disabled');
		   iframeDOCUMENT.find('#images_pay_control [data-inmodal-pay]').addClass('disabled');

		}else{
		   iframeDOCUMENT.find('#images_pay_control [data-inmodal-pay]').removeClass('disabled');
		   iframeDOCUMENT.find('#images_pay_control .delete').text(deltext).addClass('disabled');
		}
    });

    iframeDOCUMENT.on('click','.closepopup', function(e){
        /*location.reload();*/
    });
</script>
</body>
</html>