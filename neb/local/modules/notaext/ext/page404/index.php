<?

class page404 extends CExt
{
    var $EXT_ID = __CLASS__;
    var $EXT_VERSION;
    var $EXT_VERSION_DATE;
    var $EXT_NAME;
    var $EXT_DESCRIPTION;
    var $EXT_SORT;

    public function __construct()
    {
        parent::__construct();

        $this->EXT_VERSION = '1.0';
        $this->EXT_VERSION_DATE = '06.05.2014';

        $this->EXT_NAME = 'Страница 404';
        $this->EXT_DESCRIPTION = 'Добавляет на сайт страницу 404 и вешает события, связанные с ней';
        $this->EXT_SORT = 10000;

    }

    function onBeforeInstall()
    {

    }

    function onBeforeUninstall()
    {

    }

    function onAfterInstall()
    {
        RegisterModuleDependences(
            "main",
            "OnProlog",
            "notaext",
            'Bitrix\NotaExt\Ext\Page404\Handlers',
            "onStart",
            "100"
        );
        RegisterModuleDependences(
            "main",
            "OnEpilog",
            "notaext",
            'Bitrix\NotaExt\Ext\Page404\Handlers',
            "Redirect404",
            "100"
        );

    }

    function onAfterUninstall()
    {
        UnRegisterModuleDependences(
            "main",
            "OnProlog",
            "notaext",
            'Bitrix\NotaExt\Ext\Page404\Handlers',
            "onStart"
        );
        UnRegisterModuleDependences(
            "main",
            "OnEpilog",
            "notaext",
            'Bitrix\NotaExt\Ext\Page404\Handlers',
            "Redirect404"
        );
    }
}
