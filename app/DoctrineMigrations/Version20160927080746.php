<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160927080746 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $updateSql
            = "UPDATE %s
SET %s = REPLACE(%s, 'Передача_006\\\\', '');";

        $this->addSql(sprintf($updateSql, 'lbc_locationplanimages', 'ImagePath', 'ImagePath'));
        $this->addSql(sprintf($updateSql, 'lbc_locationplansectionimages', 'ImagePath', 'ImagePath'));
        $this->addSql(sprintf($updateSql, 'lbc_maindivisionimages', 'ImagePath', 'ImagePath'));
        $this->addSql(sprintf($updateSql, 'lbc_maindivisionintroduction', 'TextPath', 'TextPath'));
        $this->addSql(sprintf($updateSql, 'lbc_maindivisionintroductionimages', 'ImagePath', 'ImagePath'));
        $this->addSql(sprintf($updateSql, 'lbc_specialdivisionimages', 'ImagePath', 'ImagePath'));
        $this->addSql(sprintf($updateSql, 'lbc_specialdivisionsectionimages', 'ImagePath', 'ImagePath'));

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
