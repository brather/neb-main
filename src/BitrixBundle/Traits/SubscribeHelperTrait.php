<?php
/**
 * User: agolodkov
 * Date: 09.09.2016
 * Time: 10:53
 */

namespace BitrixBundle\Traits;


/**
 * Class SubscribeHelperTrait
 *
 * @package BitrixBundle\Traits
 */
trait SubscribeHelperTrait
{
    use BitrixHelperPropertyTrait;

    /**
     * @return \CPosting
     * @throws \Exception
     */
    public function forgeCPosting()
    {
        $this->_bitrix->checkIncludeThrowException();
        $this->_bitrix->includeModule('subscribe');

        return new \CPosting();
    }

}