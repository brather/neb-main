<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

if ($_REQUEST['PLAN_DIGITIZATION_PAGER'] == 'Y') {
    $APPLICATION->RestartBuffer();
}

if (!empty($arParams['q'])) {
    if ($arResult['COUNT'] > 0) { ?>
        <div class="b-add_digital js_digital">
            <table class="table">
                <tbody>
                <tr>
                    <th class="autor_cell">
                        <a <?= SortingExalead("document_authorsort") ?>><?= Loc::getMessage("T_LIB_AUTHOR") ?></a></th>
                    <th class="namedig_cell">
                        <a <?= SortingExalead("document_titlesort") ?>><?= Loc::getMessage("T_LIB_TITLE_DESCRIPTION") ?></a>
                    </th>
                    <th class="plan_cell">&nbsp;</th>
                    <th class="plan_cell">&nbsp;</th>
                </tr>
                <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                    <tr class="this-n-next-row search-result" data-edition-item id="<?= $arItem['~id'] ?>"
                        data-parent-id="<?= $arItem['idparent'] ?>">
                        <td class="pl15"><?= $arItem['authorbook'] ?><? echo $arParams['BOOK_ID'][$arItem['~id']]; ?></td>
                        <td class="pl15"><?= $arItem['title'] ?>
                            <div class="b-digital_act">
                                <a href="#" class="b-digital_desc"><?= Loc::getMessage("T_LIB_DESCRIPTION") ?></a>

                                <? if (!empty($arItem["BUTTON"]["LIB_NAME"])) { ?>
                                    <a href="#"
                                       class="b-digital_desc b-digital"><?= Loc::getMessage("T_LIB_BOOKS_ON_DIGITIZED") ?></a>
                                <? } ?>
                            </div>
                        </td>
                        <td>
                            <? if ($arItem["BUTTON"]["CLASS"]) { ?>
                                <!--<button class="btn btn-default btn-sm"
                                            data-toggle-edition-in-plan="<?= $arItem["BUTTON"]["CLASS"] ?>">
                                        <span class="glyphicon glyphicon-<?= $arItem["BUTTON"]["CLASS"] ?>" aria-hidden="true"></span>
                                    </button>-->

                                <label>
                                    <input type="checkbox"
                                           data-toggle-edition-in-plan="<?= $arItem["BUTTON"]["CLASS"] ?>"
                                           data-confirm="<?= Loc::getMessage('FAQ_QUESTION_DELETE_SECTION') ?>"
                                        <? if ($arItem["BUTTON"]["CLASS"] == 'minus'): ?> checked="checked"<? endif; ?>
                                    >
                                    <span class="lbl">
                                            <? if ($arItem["BUTTON"]["CLASS"] == 'minus'): ?>
                                                Удалить
                                            <? else: ?>
                                                Добавить
                                            <? endif; ?>
                                        </span>
                                </label>
                            <? } else { ?>
                                <?= $arItem["BUTTON"]["MESSAGE"] ?>
                            <?
                            } ?>
                        </td>
                        <td>
                            <form action="/local/tools/plan_digitization/comment.php"
                                  class="addCommentForm comment-form-<?= $arItem['~id'] ?>"
                                  <? if ($arItem["BUTTON"]["CLASS"] != 'minus'): ?>style="display:none;"<? endif; ?>
                            >
                                <input type="hidden" name="plan_id" value="<?= intval($_REQUEST['plan_id']) ?>"/>
                                <input type="hidden" name="book_id" value="<?= $arItem['~id'] ?>"/>
                                <input type="text" name="comment" value="<?= $arItem['PLAN']['UF_COMMENT'] ?>"
                                       placeholder="Комментарий" class="commentBook"/>
                                <button class="btn btn-default btn-sm">Сохранить</button>
                            </form>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td colspan="3">
                            <? if (!empty($arItem["BUTTON"]["LIB_NAME"])) { ?>
                                <div class="b-infobox rel b-infoboxdescr" data-link="digital">
                                    <a href="#" class="close"></a>
                                    <span class="tit iblock"><?= Loc::getMessage("T_LIB_DIGITIZING") ?></span>
                                    <br>
                                    <strong><?= $arItem["BUTTON"]["LIB_NAME"] ?>
                                        .</strong><?= Loc::getMessage("T_LIB_TIME_OF_DIGITIZED") ?>
                                    <?= $arItem["UF_DATE_FINISH_CURRENT"][0]['UF_DATE_FINISH'] ?>
                                </div><!-- /b-infobox -->
                            <?
                            } ?>
                            <div class="b-infobox rel b-infoboxdescr" data-link="descr">
                                <a href="#" class="close"></a>
                                <? if (!empty($arItem['authorbook'])) { ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= Loc::getMessage("T_LIB_DESC_AUTHOR") ?></span>
                                        <span class="iblock val"><?= $arItem['authorbook'] ?></span>
                                    </div>
                                <?
                                } ?>
                                <? if (!empty($arItem['title'])) { ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= Loc::getMessage("T_LIB_DESC_TITLE") ?></span>
                                        <span class="iblock val"><?= $arItem['title'] ?></span>
                                    </div>
                                <?
                                } ?>
                                <? if (!empty($arItem['publishyear'])) { ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= Loc::getMessage("T_LIB_DESC_DATE_OF_PUBLICATION") ?></span>
                                        <span class="iblock val"><?= $arItem['publishyear'] ?></span>
                                    </div>
                                <?
                                } ?>
                                <? if (!empty($arItem['library'])) { ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= Loc::getMessage("T_LIB_DESC_LIBRARY") ?></span>
                                        <span class="iblock val"><?= $arItem['library'] ?></span>
                                    </div>
                                <?
                                } ?>
                                <? if (!empty($arItem['countpages'])) { ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= Loc::getMessage("T_LIB_DESC_PAGES") ?></span>
                                        <span class="iblock val"><?= $arItem['countpages'] ?></span>
                                    </div>
                                <?
                                } ?>
                            </div><!-- /b-infobox -->
                        </td>
                    </tr>
                    <?
                }
                ?>
                </tbody>
            </table>
        </div>
        <script>
            $('.state').click(function () {
                return false;
            });
        </script>
        <?= $arResult['STR_NAV'] ?>
    <? } else { ?>
        <div class="b-search_empty">
            <h2><?= Loc::getMessage("T_LIB_BOOKS_NOT_FOUND") ?></h2>
            <span><?= Loc::getMessage("T_LIB_CHANGE_PARAM_OF_SEARCH") ?></span>
        </div>
    <?
    }
}


if ($_REQUEST['PLAN_DIGITIZATION_PAGER'] == 'Y') {
    exit();
}