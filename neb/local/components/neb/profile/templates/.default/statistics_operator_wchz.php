<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */

$labels = array();
$cardViews = [];
$graphs = [
    'neb_user_registers'        => [
        'title'         => 'Количество регистраций из ЭЧЗ',
        'date'          => [],
        'compareReport' => 'neb_user_registers_all_wchz',
    ],
    'neb_user_active'           => [
        'title'         => 'Количество активных читателей ЭЧЗ',
        'date'          => [],
        'compareReport' => 'neb_user_active_all_wchz',
    ],
    'neb_book_page_view'        => [
        'title'         => 'Количество просмотренных страниц изданий из ЭЧЗ',
        'date'          => [],
        'compareReport' => 'neb_book_page_view_all_wchz',
    ],
    'neb_book_document_view'    => [
        'title'         => 'Количество просмотров изданий НЭБ из ЭЧЗ (открыта хотя-бы одна страница издания)',
        'date'          => [],
        'compareReport' => 'neb_book_document_view_all_wchz',
    ],
    'neb_book_document_unique'  => [
        'title'         => 'Количество уникальных просмотров изданий НЭБ из ЭЧЗ разными пользователями (открыта хотя-бы одна страница издания)',
        'description'   => 'Просмотренный документ считается только 1 раз за период',
        'date'          => [],
        'compareReport' => 'neb_book_document_unique_all_wchz',
    ],
    'neb_book_downloads'        => [
        'title'         => 'Количество скачанных изданий НЭБ из ЭЧЗ',
        'date'          => [],
        'compareReport' => 'neb_book_downloads_all_wchz',
    ],
    'neb_feedback_count'        => [
        'title'         => 'Количество заявок по обратной связи из ЭЧЗ',
        'date'          => [],
        'compareReport' => 'neb_feedback_count_all_wchz',
    ],
    'neb_search_queries'        => [
        'title'         => 'Количество поисковых запросов из ЭЧЗ',
        'date'          => [],
        'compareReport' => 'neb_search_queries_all_wchz',
    ],
    'neb_book_card_view'        => [
        'title'         => 'Количество просмотров карточек открытых изданий и изданий, охраняемых авторским правом из ЭЧЗ',
        'date'          => [],
        'compareReport' => 'neb_book_card_view_all_wchz',
    ],
    'neb_book_card_view_closed' => [
        'title'         => 'Количество просмотров карточек изданий, охраняемых авторским правом, из ЭЧЗ',
        'date'          => [],
        'compareReport' => 'neb_book_card_view_closed_all_wchz',
    ],
];

$periodPart = 86400;
$dateTo = time();
$dateFrom = $dateTo - $periodPart * 30;
if (isset($_REQUEST['dateFrom']) && !empty($_REQUEST['dateFrom'])) {
    $dateFrom = strtotime($_REQUEST['dateFrom']);
}
if (isset($_REQUEST['dateTo']) && !empty($_REQUEST['dateTo'])) {
    $dateTo = strtotime($_REQUEST['dateTo']);
}


$user = nebUser::getCurrent()->getUser();

CModule::IncludeModule('iblock');

$reportOptions = [
    'dateFrom' => $dateFrom,
    'dateTo'   => $dateTo,
    'wchzId'   => (integer)$user['UF_WCHZ']
];
$dayFormat = 'd M';
$dateFromTmp = $dateFrom;
while ($dateFromTmp <= $dateTo) {
    $labels[] = date($dayFormat, $dateFrom);
    $dateFromTmp += $periodPart;
}
$values = array_fill(0, count($labels), 0);
$labelKeys = array_flip($labels);

/**
 * @param string $reportCode
 *
 * @return \Neb\Main\Stat\Report
 * @throws \Neb\Main\Stat\StatException
 */
$getReport = function ($reportCode) use (
    $graphs,
    $reportOptions,
    $dayFormat
) {
    $graph = &$graphs[$reportCode];

    return Neb\Main\Stat\Report::factory(
        'simple_tbl',
        array_merge(
            $reportOptions,
            [
                'table'     => $graph['table'],
                'dayFormat' => $dayFormat,
                'name'      => $graph['name'],
            ]
        )
    );
};

if (isset($_REQUEST['export'])
    && isset($graphs[$_REQUEST['export']])
) {
    $APPLICATION->RestartBuffer();
    require_once(__DIR__
        . '/../../../../../php_interface/include/PHPExcel.php');
    $export = \Neb\Main\Stat\ReportExport::factory(
        isset($_REQUEST['format']) ? $_REQUEST['format'] : 'xls',
        $getReport($_REQUEST['export'])
    );
    $export->saveTo('php://output');
    exit;
}
$yearSeconds = (85400 * 365);
if (($dateTo - $dateFrom) > $yearSeconds) {
    ShowError('Диапазон не должен быть больше 365 календарных дней');
    $dateFrom = $dateTo - ($yearSeconds - 86400);
}
foreach ($graphs as $code => &$graph) {
    $report = $getReport($code);
    $queryParams = [
        'report'   => $code,
        'dateFrom' => date('Y-m-d', $dateFrom),
        'dateTo'   => date('Y-m-d', $dateTo),
        'wchzId'   => (integer)$user['UF_WCHZ']
    ];
    $compareQueryParams = $queryParams;
    $compareQueryParams['report'] = $graph['compareReport'];

    $query = http_build_query($queryParams);
    $compareQuery = http_build_query($compareQueryParams);

    $token = COption::GetOptionString('neb.main', 'stats_reader_token');
    $host = str_replace(
        ['http:', 'http2:'], '',
        COption::GetOptionString('neb.main', 'stats_host')
    );

    $graph['url'] = $host . 'reports?'
        . 'access-token=' . $token . '&' . $query;
    $graph['exportUrl'] = $host . 'exports?'
        . 'access-token=' . $token . '&' . $query . '&periodItem=day&name=' . $graph['title'];
    $graph['compareUrl'] = $host . 'exports?'
        . 'access-token=' . $token . '&' . $compareQuery . '&name=' . $graph['title'];
}
unset($graph);
?>
<div class="iblock b-lib_fulldescr">
    <div class='b-add_digital js_digital'>
        <form
            action="<?= $APPLICATION->GetCurPage() ?>" method="GET"
            class="form-inline"
            name="form1">

            C 
            <div class="form-group">
                <div class="input-group">
                    <input type="text" id="date-from" name="dateFrom" size="10"
                        value="<?= ConvertTimeStamp($dateFrom) ?>"
                        class="form-control datepicker-from">
                    <span class="input-group-btn">
                        <a class="btn btn-default" onclick="BX.calendar({node: 'date-from', field: 'date-from', form: '', bTime: false, value: ''});">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </a>
                    </span>
                </div>
            </div>
            по
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="dateTo" id="date-to" size="10"
                        value="<?= ConvertTimeStamp($dateTo) ?>"
                        class="form-control datepicker-current">
                    <span class="input-group-btn">
                        <a class="btn btn-default" onclick="BX.calendar({node: 'date-to', field: 'date-to', form: '', bTime: false, value: ''});">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </a>
                    </span>
                </div>
            </div>  
            <input type='hidden'
                   name='LIBRARY_ID'>
            <input type="submit" class="btn btn-primary"
                   value="Применить"
                   style='float:none;'>
        </form>
        <br></div>
</div>
<hr>
<ul class="uleditstat">
    <? foreach ($graphs as $code => $graph) { ?>
        <li class="active-parent-li"><a href="" class="active-parent">
                <?= $graph['title'] ?><span class="icon"></span>
            </a>
            <ul class="submenu2" style="display: none;">
                <li>
                    <span
                        class="stat-description"><?= $graph['description'] ?></span>

                    <div data-chart-type="line"
                         style="min-width: 310px; height: 400px; margin: 0 auto"
                         data-chart-data-url="<?= $graph['url'] ?>"
                    >

                    </div>
                    <a class="exceldoc" target="_blank" href="<?= $graph['exportUrl'] ?>">
                        <img src="/local/templates/adaptive/img/file-icons/excel.png" width="16" height="16"/>Статистика по библиотеке
                    </a>

                    <!--a class="exceldoc" target="_blank" href="<?= $graph['compareUrl'] ?>">
                        <img src="/local/templates/adaptive/img/file-icons/excel.png" width="16" height="16"/>Сравнительная статистика
                    </a-->
                </li>
            </ul>
        </li>
    <? } ?>
</ul>
