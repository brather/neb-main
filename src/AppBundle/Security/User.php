<?php
/**
 * User: agolodkov
 * Date: 16.08.2016
 * Time: 16:43
 */

namespace AppBundle\Security;

/**
 * Class User
 *
 * @todo    Вытащить сюда из AppBundle\Security\UserProvider все что касается НЭБ
 * @todo    Модель также играет роль прокси для \nebUser для дальнейшего выпиливания \nebUser из проекта
 * @package AppBundle\Security
 */
class User extends \BitrixBundle\Security\User
{
    /**
     * @var \nebUser
     */
    private $_nebUser;

    /**
     * @param \nebUser $nebUser
     */
    public function setNebUser($nebUser)
    {
        $this->_nebUser = $nebUser;
    }

    /**
     * getLibraryArray - потому, что getLibrary должен будет возвращать модель библиотеки
     *
     * @return array|bool
     */
    public function getLibraryArray()
    {
        return $this->_nebUser->getLibrary();
    }
}