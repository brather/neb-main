<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("НЭБ в цифрах");

//количество электронных изданий и каталогов
$iBookCount = number_format(\Bitrix\Main\Config\Option::get('nota.exalead', 'book_count_total'), 0, 0, ' ');
$iBookCountCatalog = number_format(\Bitrix\Main\Config\Option::get('nota.exalead', 'book_count_catalog'), 0, 0, ' ');

?>
<br />

На сегодняшний день в каталоге национальной электронной библиотеки <?=$iBookCountCatalog?> записей.<br /><br />
Общее количество электронных документов в фондах НЭБ - <?=$iBookCount?>.<br /><br />

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>