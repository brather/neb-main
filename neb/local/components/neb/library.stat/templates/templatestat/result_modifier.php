<?php
/**
 * User: agolodkov
 * Date: 01.04.2015
 * Time: 13:56
 */
/**
 * @var CBitrixComponentTemplate $this
 * @var array                    $arResult
 * @var array                    $arParams
 */
use Bitrix\Main\Localization\Loc;

$names = array(
    'date'               => Loc::getMessage('LIBRARY_STAT_DATE'),
    'editions'           => Loc::getMessage('LIBRARY_STAT_NUMBER'),
    'digitized_editions' => Loc::getMessage('LIBRARY_STAT_NUMBER_COPY'),
    'reads'              => Loc::getMessage('LIBRARY_STAT_NUMBER_DOWNLOADED'),
    'views'              => Loc::getMessage('LIBRARY_STAT_NUMBER_READ'),
);
switch ($this->GetPageName()) {
    // Заполняем таблицу
    case 'export':
        $emptyItem = array(
            'date'               => null,
            'editions'           => 0,
            'digitized_editions' => 0,
            'reads'              => 0,
            'views'              => 0,
        );

        $statData = array();
        foreach ($arResult['booksStat'] as $item) {
            $date = (integer)str_replace('-', '', $item['date']);
            if (!isset($statData[$date])) {
                $statData[$date] = $emptyItem;
                $statData[$date]['date'] = $item['date'];
            }
            $statData[$date]['views'] += $item['views'];
            $statData[$date]['reads'] += $item['reads'];
        }
        foreach ($arResult['libraryStat'] as $item) {
            $date = (integer)str_replace('-', '', $item['date']);
            if (!isset($statData[$date])) {
                $statData[$date] = $emptyItem;
                $statData[$date]['date'] = $item['date'];
            }
            $statData[$date]['editions'] += $item['editions'];
            $statData[$date]['digitized_editions'] += $item['digitized_editions'];
        }
        krsort($statData);
        array_unshift($statData, $names);
        $arResult['statData'] = $statData;
        unset($statData);
        break;
    default:
        // Данные которые выводятся на графике
        $showStatistics = array(
            'reads'              => array(),
            'views'              => array(),
            'editions'           => array(),
            'digitized_editions' => array(),
        );
        // Значения которые могу складываться
        $summable = array(
            'reads' => true,
            'views' => true,
        );

        // Массив всех дней
        $dates = array();
        for ($i = 0; $arResult['daysCount'] > $i; $i++) {
            $date = $arParams['dateFrom'] + ($i * 86400);
            $dates[date('Y-m-d', $date)] = array();
        }

        // Заполняем массив с днями данными для каждого из дней
        $resultKeys = array('booksStat', 'libraryStat');
        foreach ($resultKeys as $resultKey) {
            if (!isset($arResult[$resultKey])) {
                continue;
            }
            foreach ($arResult[$resultKey] as $item) {
                if (!isset($dates[$item['date']])) {
                    continue;
                }
                if (!isset($dates[$item['date']])) {
                    $dates[$item['date']] = array();
                }
                $dates[$item['date']] = array_merge(
                    $dates[$item['date']],
                    $item
                );
            }
        }

        // Групируем дни по интервалу
        $groupDates = array();
        $dateStart = null;
        $date = null;
        foreach ($dates as $dateString => $dateItem) {
            if (null === $dateStart
                || $date - $dateStart >= $arResult['interval']
            ) {
                $dateStart = (integer)str_replace(
                    '-', '', $dateString
                );
                $date = $dateStart;
            }
            $date++;
            if (!isset($groupDates[$dateStart])) {
                if (empty($dateItem)) {
                    $dateItem['date'] = $dateString;
                }
                $groupDates[$dateStart] = $dateItem;
                continue;
            }
            foreach ($dateItem as $attributeName => $value) {
                if (isset($summable[$attributeName])) {
                    $groupDates[$dateStart]['reads'] += $value;
                }
            }
        }


        // Заполняем данные для графиков
        foreach ($groupDates as $item) {
            foreach (array_keys($showStatistics) as $key) {
                $dateTime = @strtotime($item['date']);
                if (false === $dateTime) {
                    $dateTime = $item['date'];
                }
                $format = 'd.m.Y';
                if (date('Y') === date('Y', $dateTime)) {
                    $format = 'd.m';
                }
                $dateTime = date($format, $dateTime);
                $showStatistics[$key]['dates'][] = $dateTime;
                if (!isset($item[$key])) {
                    $showStatistics[$key]['values'][] = 0;
                    continue;
                }
                $showStatistics[$key]['values'][] = (integer)$item[$key];
            }
        }

        $arResult['showStatistics'] = $showStatistics;
        unset($showStatistics);

        $widgetParams = array(
            'date' => date('Y-m-d', $arParams['dateFrom']) . ','
                . date('Y-m-d', $arParams['dateTo'])
        );
        $arResult['widgetUrl']
            = 'index.php?module=Widgetize&action=iframe&widget=1&idSite=1&period=range&disableLink=1&widget=1';
        $arResult['widgetUrl'] .= '&' . http_build_query($widgetParams);
        $arResult['grphicData'] = array(
            'editions'           => array(
                'id'           => 'container',
                'name'         => $names['editions'],
                'widgetParams' => array(
                    'moduleToWidgetize' => 'NebStatistics',
                    'actionToWidgetize' => 'getActionsEvolutionGraph',
                    'columns'           => array('neb_books_count'),
                    'actionType'        => 'books_count',
                ),
            ),
            'digitized_editions' => array(
                'id'           => 'containerdig',
                'name'         => $names['digitized_editions'],
                'widgetParams' => array(
                    'moduleToWidgetize' => 'NebStatistics',
                    'actionToWidgetize' => 'getActionsEvolutionGraph',
                    'columns'           => array('neb_books_count'),
                    'actionType'        => 'books_digit_count',
                ),
            ),
            'views'              => array(
                'id'           => 'containerRD',
                'name'         => $names['views'],
                'widgetParams' => array(
                    'idGoal'            => 1,
                    'moduleToWidgetize' => 'NebStatistics',
                    'actionToWidgetize' => 'getGoals',
                    'columns'           => array('nb_conversions'),
                ),
            ),
            'reads'              => array(
                'id'           => 'containerRW',
                'name'         => $names['reads'],
                'widgetParams' => array(
                    'idGoal'            => 3,
                    'moduleToWidgetize' => 'NebStatistics',
                    'actionToWidgetize' => 'getGoals',
                    'columns'           => array('nb_conversions'),
                ),
            ),
        );
        break;
}
