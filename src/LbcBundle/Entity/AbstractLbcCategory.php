<?php
/**
 * User: agolodkov
 * Date: 16.08.2016
 * Time: 15:29
 */

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Tree\Entity\MappedSuperclass\AbstractClosure;
use JMS\Serializer\Annotation as Serialization;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AbstractLbcCategory
 *
 */
abstract class AbstractLbcCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serialization\Groups({"list", "details"})
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ParentId", type="integer", nullable=true)
     * @Serialization\Groups({"list", "details"})
     */
    public $parentid;


    /**
     * @var integer
     *
     * @ORM\Column(name="Level", type="integer", nullable=false)
     */
    public $level;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="text", nullable=false)
     * @Serialization\Groups({"list", "details"})
     * @Assert\NotBlank()
     */
    public $name;


    /**
     * @var string
     *
     * @ORM\Column(name="Comments", type="text", nullable=true)
     * @Serialization\Groups({"list", "details"})
     */
    public $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="Keywords", type="text", nullable=true)
     * @Serialization\Groups({"list", "details"})
     */
    public $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="Links", type="text", nullable=true)
     * @Serialization\Groups({"list", "details"})
     */
    public $links;


    /**
     * @var integer
     *
     * @ORM\Column(name="Rec_id", type="integer", nullable=true)
     */
    public $recId;


    /**
     * @var integer
     *
     * @ORM\Column(name="Gr", type="integer", nullable=true)
     * @Serialization\Groups({"details"})
     */
    public $gr;

    /**
     * @var string
     *
     * @ORM\Column(name="GrErr", type="string", length=255, nullable=true)
     * @Serialization\Groups({"details"})
     */
    public $grerr;

    /**
     */
    public $parent;

    /**
     */
    public $children;


    /**
     * @var array
     */
    public $closures;

    /**
     * @var array
     */
    public $actions = [];

    /**
     * @var array
     * @Serialization\Groups({"details"})
     */
    public $options = [];

    /**
     * @var int
     */
    public $childCount = 0;

    /**
     * @var array
     */
    public $path;

    /**
     * @var string
     */
    public $categoryUrl;

    /**
     * @var string
     */
    public $optionsJson;

    public function addClosure(AbstractClosure $closure)
    {
        $this->closures[] = $closure;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     *
     * @return LbcLocationplans
     */
    public function setParentid($parentid)
    {
        $this->parentid = $parentid;

        return $this;
    }

    /**
     * Get parentid
     *
     * @return integer
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return LbcLocationplans
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LbcLocationplans
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return LbcLocationplans
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return LbcLocationplans
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set links
     *
     * @param string $links
     *
     * @return LbcLocationplans
     */
    public function setLinks($links)
    {
        $this->links = $links;

        return $this;
    }

    /**
     * Get links
     *
     * @return string
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Set recId
     *
     * @param integer $recId
     *
     * @return LbcLocationplans
     */
    public function setRecId($recId)
    {
        $this->recId = $recId;

        return $this;
    }

    /**
     * Get recId
     *
     * @return integer
     */
    public function getRecId()
    {
        return $this->recId;
    }

    /**
     * Set gr
     *
     * @param integer $gr
     *
     * @return LbcLocationplans
     */
    public function setGr($gr)
    {
        $this->gr = $gr;

        return $this;
    }

    /**
     * Get gr
     *
     * @return integer
     */
    public function getGr()
    {
        return $this->gr;
    }

    /**
     * Set grerr
     *
     * @param string $grerr
     *
     * @return LbcLocationplans
     */
    public function setGrerr($grerr)
    {
        $this->grerr = $grerr;

        return $this;
    }

    /**
     * Get grerr
     *
     * @return string
     */
    public function getGrerr()
    {
        return $this->grerr;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent(AbstractLbcCategory $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return array
     */
    public function getClosures()
    {
        return $this->closures;
    }

    /**
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param $key
     */
    public function removeAction($key)
    {
        unset($this->actions[$key]);
    }

    /**
     * @param array $actions
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
    }

    /**
     * @return int
     */
    public function getChildCount()
    {
        return $this->childCount;
    }

    /**
     * @param int $childCount
     */
    public function setChildCount($childCount)
    {
        $this->childCount = $childCount;
    }

    /**
     * @return array
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param array $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getCategoryUrl()
    {
        return $this->categoryUrl;
    }

    /**
     * @param string $categoryUrl
     */
    public function setCategoryUrl($categoryUrl)
    {
        $this->categoryUrl = $categoryUrl;
    }

    /**
     * @return boolean
     */
    public function hasPath()
    {
        return !empty($this->path);
    }

    /**
     * @return bool
     */
    public function hasDescription()
    {
        return !empty($this->links) || !empty($this->comments) || !empty($this->keywords);
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setOption($name, $value)
    {
        $this->options[$name] = $value;
    }
}