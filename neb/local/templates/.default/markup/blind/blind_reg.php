
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		
		<h2 class="mode">регистрация на портале</h2>
		<form action="index.php" class="b-form b-form_common b-regform">
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings01">Фамилия</label>
					<div class="field validate">
						<input type="text" data-validate="fio" data-required="required" value="" id="settings01" data-maxlength="30" data-minlength="2" name="sname" class="input" autocomplete="off">
						<em class="error required">Поле обязательно для заполнения</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>							
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings02">Имя</label>
					<div class="field validate">
						<input type="text" data-required="required" data-validate="fio" value="" id="settings02" data-maxlength="30" data-minlength="2" name="name" class="input"  autocomplete="off">	
						<em class="error required">Поле обязательно для заполнения</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>												
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings22">Отчество</label>
					<div class="field validate">
						<input type="text" data-validate="fio" value="" id="settings22" data-maxlength="30" data-minlength="2" name="name" class="input" autocomplete="off" >
						<em class="error required">Поле обязательно для заполнения</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>													
					</div>
				</div>
			</div>
				<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings04">Электронная почта <strong>(для входа на сайт)</strong></label>
					<div class="field validate">
						<input type="text" class="input" name="num" data-minlength="2" data-maxlength="30" id="settings04" data-validate="email" value="" data-required="required" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					
					<label for="settings05">Пароль</label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="required" value="" id="settings05" data-maxlength="30" name="pass" class="pass_status input" autocomplete="off">
						<em class="error required">Поле обязательно для заполнения</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings55">Подтвердить пароль</label>
					<div class="field validate">
						<input data-identity="#settings05" type="password" data-required="required" value="" id="settings55" data-maxlength="30" name="pass" class="input" data-validate="password" autocomplete="off">
						<em class="error identity ">Пароль не совпадает с введенным ранее</em>
						<em class="error required">Поле обязательно для заполнения</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings06">Дата рождения</label>
					<div class="field validate iblock seldate" data-yearsrestrict="12">
						<em class="error dateformat">Неверный формат даты</em>
						<em class="error required">Заполните</em>
						<em class="error yearsrestrict">Вам должно быть<br>не меньше 12 лет</em>
						<input type="hidden" class="realseldate" data-required="true" value="" id="settings06" name="REGISTER[PERSONAL_BIRTHDAY]" autocomplete="off">
						<select name="birthday" id=""  class="js_select w102 sel_day" data-required="true" autocomplete="off">
							<option value="-1">день</option>
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>
						</select>
						<label for="settings06" class="mt10" >Месяц рождения</label>
						<select name="birthmonth" id=""class="js_select w165 sel_month"  data-required="true"  autocomplete="off">
							<option value="-1">месяц</option>
							<option value="01">январь</option>
							<option value="02">февраль</option>
							<option value="03">март</option>
							<option value="04">апрель</option>
							<option value="05">май</option>
							<option value="06">июнь</option>
							<option value="07">июль</option>
							<option value="08">август</option>
							<option value="09">сентябрь</option>
							<option value="10">октябрь</option>
							<option value="11">ноябрь</option>
							<option value="12">декабрь</option>
						</select>
						<label for="settings06" class="mt10">Год рождения</label>
						<select name="birthyear" id=""  class="js_select w102 sel_year"  data-required="true" autocomplete="off">
							<option value="-1">год</option>
							<option value="2014">2014</option>
							<option value="2013">2013</option>
							<option value="2012">2012</option>
							<option value="2011">2011</option>
							<option value="2010">2010</option>
							<option value="2009">2009</option>
							<option value="2008">2008</option>
							<option value="2007">2007</option>
							<option value="2006">2006</option>
							<option value="2005">2005</option>
							<option value="2004">2004</option>
							<option value="2003">2003</option>
							<option value="2002">2002</option>
							<option value="2001">2001</option>
							<option value="2000">2000</option>
							<option value="1999">1999</option>
						</select>
					</div>									

				</div>
			</div>
			<div class="fieldrow nowrap ptop">
				<div class="fieldcell iblock">
					<label for="settings07">Пол</label>
					<div class="field validate">
						<select name="" id="settings07" data-required="required" class="js_select w270" autocomplete="off">																
							<option value="1">мужской</option>
							<option value="2">женский</option>
						</select>									
					</div>
				</div>
			</div>
			<hr>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock w700">
					<div class="checkwrapper ">
						<a href="#" title="Условия использования портала национальной электронной библиотеки">Условия использования портала национальной электронной библиотеки</a>
					</div>
				</div>
			</div>
			<div class="checkwrapper">
				<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb3" autocomplete="off"><label for="cb3" class="black"> Согласен с условиями использования портала НЭБ</label>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock w700">
					<div class="checkwrapper">
						<input class="checkbox" type="checkbox" name="" id="cb22" autocomplete="off"><label for="cb22" class="black">У меня есть единый электронный читательский билет</label>
					</div>
					<label for="settings21" class="disable mt10">Номер электронного читательского билета</label>
					<div class="field validate">
						<input type="text" disabled="disabled" data-validate="number" value="" id="settings21" data-maxlength="30" data-minlength="2" name="sname" class="input" autocomplete="off">										
					</div>
				</div> 
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<div class="checkwrapper">
						<a href="#" title="Как получить единый электронный читательский билет">Как получить единый электронный читательский билет</a>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell">
					<div class="field clearfix">
						<button class="formbutton btdisable" value="1" type="submit" disabled="disabled">Зарегистрироваться</button>

					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->



</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>