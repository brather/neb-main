<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160829084753 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE IF EXISTS `lbc_category_history`;');
        $this->addSql(
            "
CREATE TABLE `lbc_category_history` (
  `id`          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `category`    VARCHAR(100)    NOT NULL,
  `category_id` INT(11) UNSIGNED,
  `timestamp`   TIMESTAMP                DEFAULT NOW(),
  `data`        TEXT            NOT NULL,
  PRIMARY KEY (`id`),
  INDEX category_id_idx (`category`, `category_id`),
  INDEX timestamp_idx (`timestamp`)
)
    ENGINE = InnoDB;
    "
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
