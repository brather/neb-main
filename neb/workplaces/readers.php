<?php
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
$APPLICATION->SetTitle("Читателям");
$APPLICATION->SetPageProperty("main_title", "<h2 class='category-title'>Электронные читальные залы</h2>");
Loc::loadMessages(__FILE__);
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>