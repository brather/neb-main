<?php
$MESS["T_PLAN_ADD_DATE_FINISH_PLAN"] = "Planned date of execution";
$MESS["T_PLAN_ADD_SUBMIT"] = "Add";
$MESS["T_PLAN_ADD_TITLE"] = "Add plan of digitization";

$MESS["T_PLAN_ADD_AJAX_DATE_CREATE"] = "Date create: ";
$MESS["T_PLAN_ADD_AJAX_TITLE_DATE_F_PLAN"] = "Planned date of digitizing: ";
$MESS["T_PLAN_ADD_AJAX_STATUS"] = "Status: ";
$MESS["T_PLAN_ADD_AJAX_CHANGE"] = "Change";
$MESS["T_PLAN_ADD_AJAX_DELETE"] = "Delete";
$MESS["T_PLAN_ADD_AJAX_DELETE_CONFIRM"] = "Are you shure you want to delete plan?";
$MESS["T_PLAN_ADD_AJAX_STATUS_AGREEMENT"] = "In agreeing NEL operator";
$MESS["T_PLAN_ADD_AJAX_RESTART_PAGE"] = "To view, add and edit publications in the plan, please refresh the page";
$MESS["T_PLAN_ADD_AJAX_ERROR"] = "Failed to add the plan. Try later.";