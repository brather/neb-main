<?php
$MESS["UPDATE_RGB_TITLE"] = 'Updating information on the portal';
$MESS["UPDATE_RGB_TITLE_BROWSER"] = 'Addition of personal data RSL';
$MESS["UPDATE_RGB_LAST_NAME"] = 'Last name';
$MESS["UPDATE_RGB_NAME"] = 'First name';
$MESS["UPDATE_RGB_SECOND_NAME"] = 'Second name';
$MESS["UPDATE_RGB_CARD_NUMBER"] = 'Library card number RSL';
$MESS["UPDATE_RGB_PASSWORD_LK_RGB"] = 'Password will RSL';
$MESS["UPDATE_RGB_CONFIRM_PASSWORD"] = 'Confirm password';
$MESS["UPDATE_RGB_DATA_NOT_FOUND"] = 'Data on the library card number is not found in the RSL';
$MESS["UPDATE_RGB_FILL_IN"] = 'Fill in';
$MESS["UPDATE_RGB_INVALID_FORMAT"] = 'Invalid format';
$MESS["UPDATE_RGB_MORE_THAN_30_CHAR"] = 'More than 30 characters';
$MESS["UPDATE_RGB_AT_LEAST_6_CHAR"] = 'Password at least 6 characters';
$MESS["UPDATE_RGB_PASSWORD_MORE_THAN_30"] = 'Password more than 30 characters';
$MESS["UPDATE_RGB_PASSWORD_MISMATCH"] = 'Password mismatch';
$MESS["UPDATE_RGB_AGREE_FOR_POPUP"] = 'Before you register, you must read';
$MESS["UPDATE_RGB_AGREE_FOR_POPUP_TERMS"] = 'the Terms of Use';
$MESS["UPDATE_RGB_READ_TERMS"] = 'Read the Terms of Use';
$MESS["UPDATE_RGB_UPDATE"] = 'Update';
?>
