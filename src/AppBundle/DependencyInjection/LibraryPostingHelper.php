<?php
/**
 * User: agolodkov
 * Date: 06.09.2016
 * Time: 12:54
 */

namespace AppBundle\DependencyInjection;


use BitrixBundle\DependencyInjection\BitrixHelper;
use BitrixBundle\Entity\Posting\BPosting;
use BitrixBundle\Entity\Posting\BPostingEmail;
use BitrixBundle\Entity\Posting\BPostingFile;
use BitrixBundle\Form\Posting\BPostingType;
use AppBundle\Security\User;
use BitrixBundle\Traits\MainModuleHelperTrait;
use BitrixBundle\Traits\SubscribeHelperTrait;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PostingSearchHelper
 *
 * @package AppBundle\DependencyInjection
 */
class LibraryPostingHelper
{
    use SubscribeHelperTrait;

    /**
     * @var Request
     */
    private $_request;

    /**
     * @var Registry
     */
    private $_doctrine;

    /**
     * @var ContainerInterface
     */
    private $_container;

    /**
     * @var \CDBResult
     */
    private $_dbResult;

    /**
     * @var int
     */
    private $_rubricId;

    /**
     * @var int
     */
    private $_stepEmailsCount;

    /**
     * @var BPosting[]
     */
    private $_postingEntities = [];

    /**
     * @var User
     */
    private $_libraryAdmin;

    /**
     * @var int
     */
    private $_searchLimit;

    /**
     * @var int
     */
    private $_searchOffset;

    /**
     * @var int
     */
    private $_totalResult;

    /**
     * @param BitrixHelper       $bitrix
     * @param Request            $request
     * @param Registry           $doctrine
     * @param ContainerInterface $container
     * @param int                $rubricId
     * @param int                $stepEmailsCount
     *
     * @throws \Exception
     */
    public function __construct(
        BitrixHelper $bitrix,
        Request $request,
        Registry $doctrine,
        ContainerInterface $container,
        $rubricId,
        $stepEmailsCount
    ) {
        if ($rubricId < 0) {
            throw new \Exception('$rubricId must be greater than zero');
        }
        $this->setBitrix($bitrix);
        $this->_request = $request;
        $this->_doctrine = $doctrine;
        $this->_container = $container;
        $this->_rubricId = $rubricId;
        $this->_stepEmailsCount = $stepEmailsCount;
    }

    /**
     * @return $this
     */
    public function search()
    {
        $posting = $this->forgeCPosting();
        $searchParams = $this->_buildSearchParams();
        $this->_dbResult = $posting->GetList($searchParams['sort'], $searchParams['filter']);
        $total = $this->_dbResult->SelectedRowsCount();
        $this->setTotalResult($total);

        return $this;
    }

    /**
     * @todo Fuck Bitrix CPosting - нету LIMIT и OFFSET
     * @todo Выпилить добаный NavStart и сделать нормальный запрос с LIMIT и OFFSET
     * @return \BitrixBundle\Entity\Posting\BPosting[]
     */
    public function getEntities()
    {
        $offset = $this->getSearchOffset() + $this->getSearchLimit();
        $this->_dbResult->NavStart($this->getSearchLimit(), false, $offset / $this->getSearchLimit());
        while ($item = $this->_dbResult->Fetch()) {
            $this->_postingEntities[] = $this->_buildPostingEntity($item);
        }
        $postingIds = [];
        foreach ($this->_postingEntities as $key => $posting) {
            $postingIds[$key] = (integer)$posting->getId();
        }
        $postingIds = array_filter($postingIds);
        if (!empty($postingIds)) {
            $repository = $this->_doctrine->getRepository('BitrixBundle:Posting\BPostingEmail');
            $emails = $repository->findBy(['postingId' => $postingIds]);
            $postingIds = array_flip($postingIds);
            foreach ($emails as $email) {
                $id = $email->getPostingId();
                if (isset($postingIds[$id])) {
                    $this->_postingEntities[$postingIds[$id]]->addPostingEmail($email);
                }
            }
        }

        return $this->_postingEntities;
    }

    /**
     * @param FormInterface $form
     *
     * @return bool
     * @throws \Exception
     */
    public function createPosting(FormInterface $form)
    {
        $data = $form->all();
        foreach ($data as $field => $value) {
            $value = $value->getData();
            if ($value) {
                $data[strtoupper(Inflector::tableize($field))] = (string)$value;
            }
            unset($data[$field]);
        }
        $data['RUB_ID'] = [$this->_rubricId];
        $data['STATUS'] = 'P';
        $data['BODY_TYPE'] = 'html';
        $data['DIRECT_SEND'] = 'Y';
        /** Не знаю что за херня. Без этого - пустая тема в письме  */
        $data['CHARSET'] = 'Windows-1251';
        $posting = $this->forgeCPosting();
        /** @todo выпилить */
        $cFile = new \CFile();

        ob_start();
        try {
            $result = $posting->Add($data);
        } catch (\Exception $e) {
            ob_clean();
            throw $e;
        }
        ob_clean();
        $em = $this->_doctrine->getManager();
        if ($result > 0) {
            $documentRoot = realpath($this->_container->getParameter('dir.neb'));
            /** @var BPosting $entity */
            $entity = $form->getData();
            foreach ($entity->getPostingEmails() as $email) {
                $postingEmail = new BPostingEmail();
                $postingEmail->setPostingId($result);
                $postingEmail->setEmail($email);
                $em->persist($postingEmail);
            }
            foreach ($entity->getPostingFiles() as $file) {
                if ($fileId = $cFile->SaveFile(
                    $cFile->MakeFileArray($documentRoot . $file),
                    '/library-posting-attachments/'
                )
                ) {
                    $postingFile = new BPostingFile();
                    $postingFile->setPostingId($result);
                    $postingFile->setFileId($fileId);
                    $em->persist($postingFile);
                }
            }
            $em->flush();
            $em->clear();
        }

        return $result;
    }


    /**
     * @param $bitrixData
     *
     * @return \CPosting
     */
    private function _buildPostingEntity($bitrixData)
    {
        $entity = new BPosting();
        foreach ($bitrixData as $fieldName => $value) {
            $method = 'set' . ucfirst(Inflector::camelize(strtolower($fieldName)));
            if (method_exists($entity, $method)) {
                $entity->$method($value);
            }
        }
        /** @todo CPosting не отадает BODY через GetList. Придумать что-нибудь с этой хуйней */
        if ($postingDetail = $this->forgeCPosting()->GetByID($entity->getId())->Fetch()) {
            $entity->setBody($postingDetail['BODY']);
            $entity->setBodyText(strip_tags(html_entity_decode($postingDetail['BODY'])));
        }

        return $entity;
    }

    private function _getUserByFullName($likeFullName)
    {
        $users = array();

        $filter = array(
            'GROUPS_ID' => array(13),
            'NAME' => $likeFullName
        );

        $db = \CUser::GetList(($by = 'id'), ($order = 'asc'), $filter);

        if($u = $db->Fetch())
        {
            $users = $u['EMAIL'];
        }

        unset($u, $db, $filter);

        return $users;
    }

    /**
     * @return array
     */
    private function _buildSearchParams()
    {
        $query = $this->_request->query->all();
        $params = [
            'sort'   => [],
            'filter' => [
                'RUB_ID' => [$this->_rubricId],
            ],
        ];
        foreach ($query as $paramName => $value) {
            
            if (empty($value))
                continue;

            switch ($paramName) {
                case 'id':
                    $params['filter']['ID'] = $value;
                    break;
                case 'query':
                    if ($value) {
                        $params['filter']['BODY'] = "%$value%";
                    }
                    break;
                case 'library_email':
                    $value = trim($value);
                    if (!empty($value)) {
                        $params['filter']['TO'] = $value;
                    }
                    break;
                case 'sender':
                    $params['filter']['FROM'] = $this->_getSenderEmail($value);
                    break;
                case 'timestamp_from':
                    $params['filter']['TIMESTAMP_1'] = $value;
                    break;
                case 'timestamp_to':
                    $params['filter']['TIMESTAMP_2'] = $value;
                    break;
                case 'date_send_from':
                    $params['filter']['DATE_SENT_1'] = $value;
                    break;
                case 'date_send_to':
                    $params['filter']['DATE_SENT_2'] = $value;
                    break;
                case 'to_field':
                    $params['filter']['FROM'] = (count($this->_getUserByFullName( $value ))) ? $this->_getUserByFullName( $value ) : $value;
                    break;
            }
        }

        if ($libraryAdmin = $this->getLibraryAdmin()) {
            $library = $libraryAdmin->getLibraryArray();
            if (isset($library['PROPERTY_EMAIL_VALUE']) && !empty($library['PROPERTY_EMAIL_VALUE'])) {
                $params['filter']['TO'] = $library['PROPERTY_EMAIL_VALUE'];
            }
        }

        if ($_REQUEST['debug'] == 1)
            debugPre($params);

        return $params;
    }


    /**
     * @param $name
     *
     * @return array
     */
    private function _getSenderEmail($name)
    {
        return [];
    }

    /**
     * @return int
     */
    public function getStepEmailsCount()
    {
        return $this->_stepEmailsCount;
    }

    /**
     * @param int $stepEmailsCount
     */
    public function setStepEmailsCount($stepEmailsCount)
    {
        $this->_stepEmailsCount = $stepEmailsCount;
    }

    /**
     * @return User
     */
    public function getLibraryAdmin()
    {
        return $this->_libraryAdmin;
    }

    /**
     * @param User $libraryAdmin
     */
    public function setLibraryAdmin($libraryAdmin)
    {
        $this->_libraryAdmin = $libraryAdmin;
    }

    /**
     * @return int
     */
    public function getSearchLimit()
    {
        return $this->_searchLimit;
    }

    /**
     * @param int $searchLimit
     */
    public function setSearchLimit($searchLimit)
    {
        $this->_searchLimit = $searchLimit;
    }

    /**
     * @return int
     */
    public function getSearchOffset()
    {
        return $this->_searchOffset;
    }

    /**
     * @param int $searchOffset
     */
    public function setSearchOffset($searchOffset)
    {
        $this->_searchOffset = $searchOffset;
    }

    /**
     * @return int
     */
    public function getTotalResult()
    {
        return $this->_totalResult;
    }

    /**
     * @param int $totalResult
     */
    public function setTotalResult($totalResult)
    {
        $this->_totalResult = $totalResult;
    }
}