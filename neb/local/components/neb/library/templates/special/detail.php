<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
	$APPLICATION->IncludeComponent(
		"neb:library.detail",
		"special",
		Array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"CODE" => $arResult["VARIABLES"]["CODE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"LIST_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]['list'],
			"NEWS_DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]['news_detail'],
			"COLLECTION_URL" => $arMenu['collections'],
		),
		$component
	);

?>