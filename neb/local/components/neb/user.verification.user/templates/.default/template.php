<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>

<?foreach($arResult['ERRORS'] as $error) ShowError($error); ?>

<form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" class="new-reader-form nrf">

    <input type="hidden" name="EDIT[EDIT]" value="Y" />
    <input type="hidden" name="EDIT[ID]" value="<?= intval($arResult['VALUES']['ID']) ?>" />

    <div class="nrf-fieldset-title"><?= GetMessage(
            'USER_VERIFICATION_ENTER_SITE'
        ); ?></div>

    <div class="form-group">
        <label for="useremail">
            <em class="hint">*</em>
            <?= GetMessage('USER_VERIFICATION_EMAIL'); ?>
        </label>
        <input
                type="email"
                class="form-control"
                name="EDIT[EMAIL]"
                id="useremail"
                value="<?= $arResult['EDIT']['EMAIL'] ?>" />
    </div>

    <div class="form-group">
        <label for="userpassword">
            <em class="hint">*</em>
            <?= GetMessage('USER_VERIFICATION_PASSWORD_PROTECTION'); ?>
            (<?= GetMessage('USER_VERIFICATION_PASSWORD_MIN'); ?>)
        </label>
        <input
                type="password"
                class="form-control"
                value=""
                id="userpassword"
                name="EDIT[PASSWORD]"
        />
    </div>

    <div class="form-group">
        <label for="userconfirmpassword">
            <em class="hint">*</em>
            <?= GetMessage('USER_VERIFICATION_CONFIRM_PASSWORD'); ?>
        </label>
        <input
                class="form-control"
                type="password"
                value=""
                id="userconfirmpassword"
                name="EDIT[CONFIRM_PASSWORD]" />
    </div>

    <div class="form-group">
        <label for="usernumber">
            <em class="hint">*</em>
            <?=Loc::getMessage('USER_VERIFICATION_NUMBER');?>
        </label>
        <input type="text"
               value="<?=$arResult['EDIT']['NUMBER']?>"
               id="usernumber"
               name="EDIT[NUMBER]"
               class="form-control" />
    </div>

    <div class="form-group">
        <label>
            <em class="hint">*</em>
            <?=Loc::getMessage('USER_VERIFICATION_DATE');?>
        </label>
        <div class="input-group">
            <input
                    type="text"
                    class="form-control"
                    value="<?=$arResult['EDIT']['DATE']?>"
                    id="userdate"
                    name="EDIT[DATE]"
            />
            <span class="input-group-btn">
                    <a class="btn btn-default"
                       onclick="BX.calendar({node: 'userdate', field: 'userdate',  form: 'history', bTime: false, value: ''});"
                    >
                        <span class="glyphicon glyphicon-calendar"></span>
                    </a>
                </span>
        </div>
    </div>

    <hr />

    <div>
        <button class="btn btn-primary" value="save" type="submit"
                name="action"><?= GetMessage(
                'USER_VERIFICATION_SEND_DATA'
            ); ?></button>
        <?= GetMessage('USER_VERIFICATION_REGISTRY_ELP'); ?>
    </div>

</form>
