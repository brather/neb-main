<?php
/**
 * User: agolodkov
 * Date: 12.08.2016
 * Time: 14:44
 */

namespace LbcBundle\Controller\Api;

use BitrixBundle\Security\User;
use AppBundle\Security\UserProvider;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;
use FOS\RestBundle\View\View;
use Gedmo\Tree\Entity\Repository\ClosureTreeRepository;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use LbcBundle\DependencyInjection\LbcCategory;
use LbcBundle\DependencyInjection\LbcCategorySortingHelper;
use LbcBundle\Entity\AbstractLbcCategory;
use LbcBundle\Entity\LbcCategoryHistory;
use LbcBundle\Entity\LbcCategorySorting;
use LbcBundle\LbcBundle;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use BitrixBundle\Annotations\IncludeBitrix;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\User\UserInterface;


abstract class AbstractCategoryController
{

    /**
     * @DI\Inject("doctrine")
     * @var Registry
     */
    public $doctrine;

    /**
     * @DI\Inject("form.factory")
     * @var FormFactory
     */
    public $formFactory;

    /**
     * @DI\Inject("serializer")
     * @var Serializer
     */
    public $serializer;

    /**
     * @DI\Inject("neb.user_provider")
     * @var UserProvider
     */
    public $userProvider;

    /**
     * @DI\Inject("security.token_storage")
     * @var TokenStorage
     */
    public $tokenStorage;

    /**
     * @DI\Inject("lbc.category_sorting")
     * @var LbcCategorySortingHelper
     */
    public $categorySorting;

    /**
     * @DI\Inject("lbc.category")
     * @var LbcCategory
     */
    public $categoryHelper;

    /**
     * @DI\Inject("router")
     * @var Router
     */
    public $router;

    /**
     * Lists LbcMaindivision entities.
     *
     * @param int $id
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     *
     * @return array
     */
    public function categoryAction($id)
    {
        $id = (integer)$id;
        $condition = [];
        if ($id > 0) {
            $condition['parentid'] = $id;
        } else {
            $condition['level'] = 1;
        }

        $repository = $this->getRepository();
        $repository->childCount();
        $order = [];
        if (property_exists($repository->getClassName(), 'code')) {
            $order = [
                'iscommon' => 'desc',
                'codeSort' => 'asc',
            ];
        }
        /** @var AbstractLbcCategory[] $entities */
        $entities = $repository->findBy($condition, $order);
        foreach ($entities as $entity) {
            $entity->setChildCount($repository->childCount($entity));
        }
        /** @todo Переделать на Join */
        $this->categorySorting->sortEntities($entities);

        return [
            'entities' => $entities,
        ];
    }

    /**
     * Lists entities.
     *
     * @param int $id
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"details"})
     * @IncludeBitrix()
     *
     * @return array
     */
    public function categoryDetailAction($id)
    {
        $em = $this->doctrine->getManager();
        /** @var AbstractLbcCategory $entity */
        if (!$entity = $em->getRepository($this->getEntityName())->find($id)) {
            throw new NotFoundHttpException("Category $id not found");
        }
        $entity->setOption(
            'url',
            [
                'autoComplete' => $this->router->generate(
                    'api_bbk_category_autocomplete',
                    ['category' => $this->categoryHelper->getEntityCategoryName($entity)]
                )
            ]
        );

        return [
            'entity'  => $entity,
            'canEdit' => $this->userProvider->getCurrentUser()->hasRole(LbcBundle::BBK_EDITOR_ROLE),
        ];
    }

    /**
     * @param int $id
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     *
     * @throws \Exception
     * @return array
     */
    public function categoryChildsAction($id)
    {
        $fieldName = $this->getCategoryOwnerFieldName();
        if (empty($fieldName)) {
            throw new \Exception('Empty owner field name');
        }
        $id = (integer)$id;
        $condition = [
            $fieldName => $id,
        ];

        $repository = $this->getRepository();
        $entities = $repository->findBy($condition);

        return [
            'entities' => $entities,
        ];
    }

    /**
     * Edits an existing entity.
     *
     * @Method("PUT")
     * @Rest\View()
     * @IncludeBitrix()
     *
     */
    public function categoryPutAction(Request $request, $id)
    {
        $editForm = $this->_createCategoryForm($id, ['method' => 'PUT']);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->doctrine->getManager()->flush();

            return View::create(null, 204);
        }

        return $editForm;
    }

    /**
     * Edits an existing entity.
     *
     * @Method("PATCH")
     * @Rest\View()
     * @IncludeBitrix()
     *
     */
    public function categoryPatchAction(Request $request, $id)
    {
        $editForm = $this->_createCategoryForm($id, ['method' => 'PATCH']);
        $entityBefore = $editForm->getData();
        $entityBefore = clone $entityBefore;
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->_saveHistory($editForm->getData(), $entityBefore);
            $em = $this->doctrine->getManager();
            $em->merge($editForm->getData());
            $em->flush();

            return View::create(null, 204);
        }

        return $editForm;
    }

    /**
     * Edits an existing entity.
     *
     * @Method("POST")
     * @Rest\View(serializerGroups={"details"})
     * @IncludeBitrix()
     *
     */
    public function categoryPostAction(Request $request)
    {
        $entityClass = $this->getRepository()->getClassName();
        /** @var AbstractLbcCategory $entity */
        $entity = new $entityClass;
        $form = $this->formFactory->create(
            $this->getFormType(),
            $entity
        );
        $request = $request->request->all();
        $formData = $request[$form->getName()];
        /** @var AbstractLbcCategory $parent */
        if (isset($formData['parent']) && $parent = $this->getRepository()->find($formData['parent'])) {
            $formData['level'] = $parent->getLevel() + 1;
        }
        $form->submit($formData);

        if ($form->isValid()) {
            $this->doctrine->getManager()->persist($entity);
            $this->doctrine->getManager()->flush();

            return View::create($entity, 201);
        }

        return View::create($form, 400);
    }


    /**
     * Lists entities.
     *
     * @param int $id
     *
     * @Method("GET")
     * @Rest\View()
     *
     * @return array
     */
    public function categoryCheckActionsAction($id)
    {
        /** @var AbstractLbcCategory $entity */
        $entity = $this->doctrine->getManager()->getRepository($this->getEntityName())->find($id);

        return [
            'actions' => $entity->getActions()
        ];
    }

    /**
     * before entityManager->flush()
     *
     * @param AbstractLbcCategory $entity
     */
    protected function _saveHistory(AbstractLbcCategory $entity, AbstractLbcCategory $entityBefore)
    {
        $categoryId = $entity->getId();
        $categoryName = $this->_getCategoryName();
        $em = $this->doctrine->getManager();
        $historyRepository = $em->getRepository('LbcBundle:LbcCategoryHistory');
        $historyEntity = $historyRepository->findBy(
            [
                'category'   => $categoryName,
                'categoryId' => $categoryId,
            ]
        );
        $serializeContext = SerializationContext::create()->setGroups(['list']);
        if (!$historyEntity) {
            $history = new LbcCategoryHistory();
            $history->setCategory($categoryName);
            $history->setCategoryId($categoryId);
            $history->setData($this->serializer->serialize($entityBefore, 'json', clone $serializeContext));
            $em->persist($history);
        }
        $history = new LbcCategoryHistory();
        $history->setCategory($categoryName);
        $history->setCategoryId($categoryId);
        $history->setData($this->serializer->serialize($entity, 'json', clone $serializeContext));
        $em->persist($history);
    }

    /**
     * @return string
     */
    protected function _getCategoryName()
    {
        return strtolower(str_replace('LbcBundle:Lbc', '', $this->getEntityName()));
    }

    /**
     * @param int   $id
     * @param array $options
     *
     * @return \Symfony\Component\Form\Form
     */
    protected function _createCategoryForm($id, array $options = [])
    {
        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw new NotFoundHttpException('Unable to find ' . $this->getEntityName() . ' entity.');
        }

        return $this->formFactory->create(
            $this->getFormType(),
            $entity,
            $options
        );
    }

    /**
     * @return ClosureTreeRepository
     */
    public function getRepository()
    {
        return $this->doctrine->getManager()->getRepository($this->getEntityName());
    }

    /**
     * @return string
     */
    public function getCategoryOwnerFieldName()
    {
        return null;
    }

    /**
     * @return string
     */
    abstract public function getEntityName();

    /**
     * @return AbstractType
     */
    abstract public function getFormType();
}