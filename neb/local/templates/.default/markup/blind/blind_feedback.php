<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		
		<div class="b-plaintext">
			<h2>Обратная связь</h2>

			<form action="" class="b-form b-form_common b-feedbackform">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="fb_email">Электронная почта</label>
						<div class="field validate">
							<input type="text" class="input" name="email" data-minlength="2" data-maxlength="30" id="fb_email" data-validate="email" value="" data-required="required">									
						</div>
					</div>
				</div>

				<div class="fieldrow nowrap ptop">
					<div class="fieldcell iblock">
						<label for="fb_theme">Тема:</label>
						<div class="field">
							<select name="theme" id="fb_theme" data-required="required" class="js_select w370">
								<option value="1">Вопрос</option>
								<option value="2">Жалоба</option>
								<option value="3">Предложение</option>
								<option value="2">Другое</option>
							</select>
						</div>
					</div>
				</div>

				<div class="fieldrow nowrap">
					<div class="fieldcell iblock mt10">
						<em class="hint">*</em>
						<label for="fb_message">Ваше сообщение:</label>
						<div class="field validate txtarea">
							<textarea id="fb_message" data-maxlength="1500" data-required="required" data-minlength="2" name="message" class="input" ></textarea>											
						</div>
					</div>
				</div>


				<div class="fieldrow nowrap fieldrowaction">
					<div class="fieldcell ">
						<div class="field clearfix">
							<button class="formbutton" value="1" type="submit">Отправить письмо!</button>

						</div>
					</div>
				</div>

			</form>

		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>