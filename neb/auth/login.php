<?php require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog.php");

global $APPLICATION, $USER;

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

$rq = Application::getInstance()->getContext()->getRequest();
$sBackUrl = urldecode($rq->get('_backUrl'));

// если задан параметр обратного редиректа _backUrl
if (!empty($sBackUrl)) {
    $_SESSION['AUTH_REFERER'] = $sBackUrl;
    $_SESSION['AUTH_REFERER'] = parse_url($_SESSION['AUTH_REFERER'], PHP_URL_PATH)
        . '?' . parse_url($_SESSION['AUTH_REFERER'], PHP_URL_QUERY);
}
// проверка с какой страницы пользователь пришел на авторизацию
else {
    $_SESSION['AUTH_REFERER'] = ($_SERVER["HTTP_REFERER"] && $_SERVER["HTTP_REFERER"] != $APPLICATION->GetCurPage())
        ? $_SERVER["HTTP_REFERER"] : '/' ;
    $_SESSION['AUTH_REFERER'] = str_replace(
        array('http://195.74.82.201', $_SERVER["HTTP_HOST"], $_SERVER["HTTP_ORIGIN"], 'http://', 'https://'),
        '',
        $_SESSION['AUTH_REFERER']
    );
}

$APPLICATION->IncludeComponent("bitrix:system.auth.form", "adaptive",
    array(
        'LINK_REFERER' => !empty( $sBackUrl ) ? $sBackUrl : '/'
    ),
    false,
    array("HIDE_ICONS" => "Y")
);
if ($USER->IsAuthorized()) {
    $backUrl = '/';
    if (isset($_SESSION['AUTH_REFERER']) && !empty($_SESSION['AUTH_REFERER'])) {
        $backUrl = $_SESSION['AUTH_REFERER'];
    }

    if (!empty($sBackUrl))
        $backUrl = $sBackUrl;

    LocalRedirect($backUrl);
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog.php");