//document.addEventListener("DOMContentLoaded", function(){
    $.fn.uploaderInit = function () {
        $(this).each(function () {
            var fileUploadContainer = $(this),
                fileUploadInput,
                dataUrl = fileUploadContainer.data('url'),
                inputsName = fileUploadContainer.data('inputs-name')
                ;
            if ( $(fileUploadContainer).prop('tagName') == 'INPUT' ) {
                fileUploadInput = fileUploadContainer;
                fileUploadContainer = $('<div'
                    +' data-file-upload'
                    +' data-url="'+dataUrl+'"'
                    +' data-inputs-name="'+inputsName+'"'
                    +' class="'+fileUploadContainer.attr('class')+'"'
                    +' id="'+fileUploadContainer.attr('id')+'"'
                    +'></div>');
                fileUploadInput = fileUploadInput.wrap(fileUploadContainer);
                fileUploadContainer = fileUploadInput.parent('div');
                fileUploadInput.detach();
            }

            var initUploader = function () {
                var presentContainer = fileUploadContainer.find('[data-present-files]'),
                    presentItem = fileUploadContainer.find('[data-uploaded-file-present]'),
                    uploadInput = fileUploadContainer.find('[data-file-upload-input]'),
                    fileItem = fileUploadContainer.find('[data-file-item]')
                    ;

                uploadInput.fileupload({
                    url: dataUrl,

                    add: function (e, data) {
                        $.each(data.files, function (index, file) {
                            data.context = fileItem.clone().appendTo(presentContainer);
                            data.context.find('[data-file-client-name]').text(file.name);
                            data.submit();
                        });
                    },

                    progress: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        data.context.find('[data-file-item-progress] .file-progress-bar').css('width',progress + '%');
                    },

                    fail: function (e, data) {
                        var resTextObj = JSON.parse(data.jqXHR.responseText);
                        data.context.find('[data-file-server-name]').text('ошибка загрузки');
                        // data.context.find('[data-file-server-name]').text(data.textStatus);
                        data.context.find('[data-file-server-name]').attr('title',resTextObj["error"].exception[0].message);

                    },

                    done: function (e, data) {
                        var result = $.parseJSON(data.result),
                            present = presentItem.clone(),
                            presentLink = present.find('[data-present-link]'),
                            presentInput = present.find('[data-file-input]')
                            ;
                        // presentLink.attr('href', result.files[0].url);
                        // presentLink.text(result.files[0].name);
                        presentLink.detach();
                        if (inputsName) {
                            presentInput.attr('name', inputsName);
                            presentInput.val(result.files[0].url);
                        }
                        data.context.find('[data-file-server-name]').text('файл загружен').attr('title',result.files[0].name);
                        data.context.find('[data-file-server-name]').append(present);
                    }
                });
            };

            initUploader();
        });
    };

    $(function() {
        $('[data-file-upload]').uploaderInit();
    });
//});
