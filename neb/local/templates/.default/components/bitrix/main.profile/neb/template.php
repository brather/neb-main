<?php
/**
 * @global CMain $APPLICATION
 *
 * @param array  $arParams
 * @param array  $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

ShowError($arResult["strProfileError"]);
if ($arResult['DATA_SAVED'] == 'Y') {
    ShowNote(GetMessage('PROFILE_DATA_SAVED'));
}
?>

<div class="row">
    <div class="col-md-6 reader-self-change-password">
        <?
        if ($arResult['arUser']['EXTERNAL_AUTH_ID'] != 'socservices') {
            $APPLICATION->IncludeComponent("neb:main.password.reset", "neb", Array());
        }

        ?>
    </div>
    <div class="col-md-6 reader-self-profile-preview">
        <?
        $sImage = (intval($arResult['arUser']['PERSONAL_PHOTO']) > 0) ? CFile::GetPath(
            $arResult['arUser']['PERSONAL_PHOTO']
        ) : '';
        if (isset($sImage['SRC'])) {
            $sImage = $sImage['SRC'];
        }
        ?>
        <?
        if ('N' !== @$arParams['SHOW_FACE']) {
            $APPLICATION->IncludeComponent(
                "notaext:plupload",
                "profile",
                array(
                    "MAX_FILE_SIZE"      => "24",
                    "FILE_TYPES"         => "jpg,jpeg,png",
                    "DIR"                => "tmp_register",
                    "FILES_FIELD_NAME"   => "profile_file",
                    "MULTI_SELECTION"    => "N",
                    "CLEANUP_DIR"        => "Y",
                    "UPLOAD_AUTO_START"  => "Y",
                    "RESIZE_IMAGES"      => "Y",
                    "RESIZE_WIDTH"       => "110",
                    "RESIZE_HEIGHT"      => "110",
                    "RESIZE_CROP"        => "Y",
                    "RESIZE_QUALITY"     => "98",
                    "UNIQUE_NAMES"       => "Y",
                    "PERSONAL_PHOTO"     => $sImage,
                    "FULL_NAME"          => $arResult['arUser']['NAME'] . ' '
                        . $arResult['arUser']['LAST_NAME'],
                    "USER_EMAIL"         => $arResult['arUser']['EMAIL'],
                    "DATE_REGISTER"      => $arResult['arUser']['DATE_REGISTER'],
                    "UF_STATUS"          => $arResult['arUser']['UF_STATUS'],
                    "UF_REGISTER_TYPE"   => $arResult['arUser']['UF_REGISTER_TYPE'],
                    "USER_ROLE"          => $arResult['USER_ROLE'],
                    "UF_RGB_CARD_NUMBER" => $arResult['arUser']['UF_RGB_CARD_NUMBER'],
                ),
                false
            );
        }
        ?>
    </div>
</div>






<div class="reader-self-profile-social-attach">
<?php $APPLICATION->IncludeComponent(
        "bitrix:socserv.auth.split", ".default", array(
        "SHOW_PROFILES" => "Y",
        "ALLOW_DELETE"  => "Y"
    ),
        false
    ); ?>
    <div style="clear: left;"></div>
</div>
<br>
<br>




<style type="text/css">
    .profile-form {
        margin: 20px;
    }
</style>
<form method="post" name="userprofile" action="<?= $arResult["FORM_TARGET"] ?>"
      class="profile-form lk-reader-self-profile nrf"
      enctype="multipart/form-data">
    <?= $arResult["BX_SESSION_CHECK"] ?>
    <input type="hidden" name="lang" value="<?= LANG ?>"/>
    <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
    <input type="hidden" id="UF_ESIA_FIELDS"
           value="<?= $arResult['arUser']['UF_ESIA_FIELDS'] ?>"/>
    <input type="hidden" id="isESIAUser"
           value="<?= $arResult['isESIAUser'] ?>"/>

    <hr>
    <? include(__DIR__ . '/../form-parts/user-info.php') ?>

    <? if ($arResult["arLibrary"]): ?>
        <div class="form-group">
            <h2><?= GetMessage('PROPERTY_LIBRARY'); ?></h2>
            <label for="PROPERTY_SCAN_COST"><?= GetMessage(
                    'PROPERTY_LIBRARY_SCAN_COST'
                ); ?> </label>
            <input type="text" class="form-control" name="PROPERTY_SCAN_COST"
                   data-maxlength="20" maxlength="4" id="PROPERTY_SCAN_COST"
                   value="<?= $arResult["arLibrary"]['SCAN_COST'] ?>"/>
        </div>
    <? endif; ?>

    
    <br>

    <div class="form-group">
        <button
            name="save"
            class="btn btn-primary"
            value="1"
            type="submit">
            <?= GetMessage('SAVE'); ?>
        </button>
    </div>

</form>

<script>
    $(function(){

        function getAge(dateString) {
            var today = new Date();
            var d = parseInt(dateString[0] + dateString[1]),
                m = parseInt(dateString[3] + dateString[4]),
                y = parseInt(dateString[6] + dateString[7] + dateString[8] + dateString[9]),
                born = new Date(y, m - 1, d),
                age = Math.ceil( (today.getTime() - born) / (24 * 3600 * 365.25 * 1000) );
            return age;
        }
        jQuery.validator.addMethod("checkAge", function(value, element, params) {
                return ( getAge(value) > Number(params) );
            }
        );

        $('form[name="userprofile"]').validate({

            rules: rulesAndMessages.rules,
            messages: rulesAndMessages.messages,
            errorPlacement: function(error, element) {
                if ( element.attr('type') == "radio" ) {
                    error.appendTo( $(element).closest('.form-group').find('label')[0] );
                } else {
                    error.appendTo( $(element).closest('.form-group') );
                }
                $(element).closest('.form-group').toggleClass('has-error', true);
            },
            /* specifying a submitHandler prevents the default submit, good for the demo
             submitHandler: function() {

             },
            set this class to error-labels to indicate valid fields*/
            success: function(label) {
                /* set &nbsp; as text for IE
                label.html('"&nbsp;"').addClass("resolved");*/
                label.remove();
            },
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                $(element).parent().find("." + errorClass).removeClass("resolved");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
            }
        });

        $('#userbirthday').on('input change', function(){
            $(this).closest('form').validate().element(this);
        });

        $(document).on('keyup change', '[data-mirrored-by]', function(){
            var id = $(this).data('mirrored-by'),
                sameAddresses = $('#livwhereregistered').prop('checked');
            if (sameAddresses) {
                $('#'+id).val( $(this).val() ).trigger('change');
            }
        });

        $(document).on('change', '#livwhereregistered', function(){
            setRegType();
        });

        function setRegType () {
            var checked = $('#livwhereregistered').prop('checked');
            if ( checked ) {
                /* $('#livwhereregistered-val').val('1'); менять через дата-атрибут data-checkbox-for="FIELD_NAME", обработчик FRONT.init [data-checkbox-for] в adaptive/js/script.js*/
                $('[data-mirror]').each(function(){
                    var id = $(this).data('mirror');
                    $(this).val( $('#'+id).val() );
                });
                $('.living-address-group').addClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function(){
                    $(this).attr('readonly','true');
                });
            } else {
                /* $('#livwhereregistered-val').val('0');*/
                $('.living-address-group').removeClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function(){
                    $(this).removeAttr('readonly');
                });
            }
        }

        setRegType();

        $(document).on('click','[data-perpage-set]', function(e){
            e.preventDefault();
            var toggler = $(this),
                value = toggler.data('perpage-set'),
                widget = toggler.closest('[data-perpage-widget]'),
                input = widget.find('[name="UF_SEARCH_PAGE_COUNT"]');
            
            widget.find('[data-perpage-set]').toggleClass('current', false);
            toggler.toggleClass('current',true);
            $(input).val(value);
            /*console.log(value);*/
        });

    });
</script>