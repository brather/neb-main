<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 * @var array $arParams
 */
?>


<form>
    <input type="text" name="FIO" class=" b-text "
           value="<?= $arParams['FIO'] ?>"
           placeholder="<?= Loc::getMessage('FULL_NAME') ?>">
    <input type="submit" value="<?= Loc::getMessage('FIND') ?>" class="btn btn-primary">
</form>
<div class="lk-table">
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:50%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind">Логин</li>
        <li class="lk-table__header-kind">Роль</li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('NAME') ?></li>
        <li class="lk-table__header-kind">Email</li>
        <li class="lk-table__header-kind">Дата регистрации</li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('ACTION') ?></li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['USERS'] as $user) : ?>
            <ul class="lk-table__row">
                <li class="lk-table__col"><?= $user['LOGIN'] ?></li>
                <li class="lk-table__col"> <?= $user['ROLE'] ?></li>
                <li class="lk-table__col"><a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $user['ID'] ?>">
                        <?= $user['LAST_NAME'] ?> <?= $user['NAME'] ?> <?= $user['SECOND_NAME'] ?>
                    </a></li>
                <li class="lk-table__col"><?= $user['EMAIL'] ?></li>
                <li class="lk-table__col"><?= $user['DATE_REGISTER'] ?></li>
                <li class="lk-table__col"><button type="button" class="btn btn-primary" data-id="<?= $user['ID'] ?>">Верифицировать</button><?if($user['UF_WCHZ']):?>регистрация в ЭЧЗ<?endif;?></li>
            </ul>
        <?endforeach;?>
    </section>
    <?= $arResult['nav'] ?>
</div>
