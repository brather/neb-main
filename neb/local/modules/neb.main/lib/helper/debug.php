<?php
/**
 * User: agolodkov
 * Date: 18.07.2016
 * Time: 19:57
 */

namespace Neb\Main\Helper;

use \Bitrix\Main\Config\Option;

/**
 * Class Debug
 *
 * @package Neb\Main\Helper
 */
final class Debug
{
    /**
     * @var Debug
     */
    private static $_instance;
    /**
     * @var array
     */
    private $_events = [];
    /**
     * @var bool
     */
    private $_reline = false;
    /**
     * @var string
     */
    private $_env;

    /**
     * @return Debug
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     *
     */
    private function __construct()
    {
        $this->_env = ('cli' === php_sapi_name()) ? 'cli' : 'web';
    }

    /**
     * @param $msg
     * @param $type
     */
    public function weriteEvent($msg, $type)
    {
        $key = count($this->_events);
        if (true === $this->_reline) {
            $key--;
        }
        $this->_events[$key] = [
            'type' => $type,
            'msg'  => $msg,
        ];
        if ('cli' === $this->_env) {
            echo $type, ': ', $msg, (true === $this->_reline ? "\r" : PHP_EOL);
        }
        $this->_reline = false;
    }

    /**
     * @param string $msg
     */
    public static function info($msg)
    {
        self::event($msg, 'info');
    }
    /**
     * @param string $msg
     */
    public static function error($msg)
    {
        self::event($msg, 'error');
    }

    /**
     * @param string $msg
     * @param string $type
     */
    public static function event($msg, $type)
    {
        self::getInstance()->weriteEvent($msg, $type);
    }

    /**
     * @param string $msg
     * @param string $type
     */
    public static function reline($msg, $type)
    {
        self::getInstance()->_reline = true;
        self::getInstance()->weriteEvent($msg, $type);
    }

    /**
     *
     */
    public static function clearReline()
    {
        self::getInstance()->_reline = false;
        if ('cli' === self::getInstance()->_env) {
            echo PHP_EOL;
        }
    }

    /**
     * Отправка экстренного отладочного сообщения на почту разрабочтика
     *
     * @param $sTheme - тема письма
     * @param string $sMessage - тело письма
     */
    public static function sendMail($sTheme, $sMessage = '') {
        mail(
            Option::get('neb.main', 'debug_email'),
            'Отладка НЭБ.РФ: ' . $sTheme,
            $sMessage
        );
    }
}