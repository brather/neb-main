<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<?
	if(!empty($arResult["ITEMS"]))
	{
	?>
	<div class="b-viewhnews">
		<div class="b-newslider js_fourslider slidetype">
			<?
				foreach($arResult["ITEMS"] as $arItem)
				{
				?>
				<div>
					<div class="b-newsslider_item">
						<div class="date"><?=$arItem["ACTIVE_FROM"]?><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                        <div><?=$arItem["PREVIEW_TEXT"]?></div>
                        <?if ($arItem["PREVIEW_PICTURE"]["ID"] > 0):?><img src='<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>'><?endif;?>
                        <?if ($arItem['PROPERTIES']['type']['VALUE']):?><p>метка новости: <?=$arItem['PROPERTIES']['type']['VALUE']?></p><?endif;?>
					</div>
				</div>
				<?
				}
			?>
		</div>
	</div>
    <?
	}
    ?>