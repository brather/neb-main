<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    Nota\Library\Collection;

Loc::loadMessages(__FILE__);
Loader::IncludeModule('nota.library');

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
if($arRequest['action'] == 'sortBooks' && $arRequest['AJAX'] == 'Y' && check_bitrix_sessid()){
    Collection::sortBooks($_REQUEST['data']);
    //LocalRedirect($APPLICATION->GetCurPage());
    //print_r($_REQUEST['data']);
} elseif($arRequest['BOOK_UPDATE_SORT'] == 'Y' && $arRequest['BOOK_UPDATE_AJAX'] == 'Y' && !empty($arRequest['BOOK_ID'])) {
    $newBook = new CIBlockElement();
    $arFields = array(
        'SORT' => $arRequest['BOOK_VALUE_SORT']
    );
    $res = $el->Update($arRequest['BOOK_ID'], $arFields);
    if($res) {?>
        <p class="text-success"><?= Loc::getMessage('BOOK_UPDATE_MESS_SUCCESS'); ?></p>
    <?} else {?>
        <p class="text-danger"><?= Loc::getMessage('BOOK_UPDATE_MESS_ERROR'); ?></p>
    <?}
}