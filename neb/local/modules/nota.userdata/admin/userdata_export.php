<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aTabs = array();
$aTabs[] = array(
    "DIV" => "edit1",
    'TAB' => 'Экспорт',
    'TITLE' => 'Экспорт пользователей'

);

$tabControl = new CAdminTabControl('tabControl', $aTabs);

if ($_SERVER['REQUEST_METHOD'] == 'POST' and check_bitrix_sessid()) {
    $date_register_1 = $_REQUEST['date_register_1'];
    $date_register_2 = $_REQUEST['date_register_2'];
    pre($_REQUEST, 1);

    $arFilter = array(
        'ACTIVE' => 'Y',
        '!UF_SCAN_PASSPORT1' => false,
    );

    if (!empty($date_register_1))
        $arFilter['DATE_REGISTER_1'] = $date_register_1;

    if (!empty($date_register_2))
        $arFilter['DATE_REGISTER_2'] = $date_register_2;
    pre($arFilter, 1);

    $rsUsers = CUser::GetList(($by = "date_register_1"), ($order = "desc"), $arFilter, array(
        'FIELDS' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'EMAIL', 'PERSONAL_BIRTHDAY', 'PERSONAL_MOBILE', 'WORK_ZIP', 'WORK_CITY', 'WORK_STREET', 'WORK_COMPANY', 'PERSONAL_GENDER', 'PERSONAL_ZIP', 'PERSONAL_CITY', 'PERSONAL_STREET'),
        'SELECT' => array('UF_SCAN_PASSPORT1', 'UF_SCAN_PASSPORT2', 'UF_BRANCH_KNOWLEDGE', 'UF_HOUSE2', 'UF_STRUCTURE2', 'UF_FLAT2', 'UF_PASSPORT_SERIES', 'UF_PASSPORT_NUMBER', 'UF_CORPUS', 'UF_STRUCTURE', 'UF_FLAT', 'UF_EDUCATION')
    ));
    $arUsers = array();
    while ($arUser = $rsUsers->Fetch()) {
        if (!empty($arUser['UF_EDUCATION'])) {
            $arValEducation = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION', 'ID' => $arUser['UF_EDUCATION']));
            $arUser['UF_EDUCATION'] = $arValEducation[0]['XML_ID'];
        }

        if (!empty($arUser['UF_BRANCH_KNOWLEDGE'])) {
            $arValKnowledge = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE', 'ID' => $arUser['UF_BRANCH_KNOWLEDGE']));
            $arUser['UF_BRANCH_KNOWLEDGE'] = $arValKnowledge[0]['XML_ID'];
        }
        $arUsers[] = $arUser;
    }

    # шаблон json ответа
    $arJson = array(
        'branch_of_knowledge' => '',
        'home_address' => '',
        'citizenship' => '',
        'reader_category' => '',
        'job_study_place' => '',
        'email' => '',
        'mobile_phone' => '',
        'firstname' => '',
        'middlename' => '',
        'lastname' => '',
        'phone' => '',
        'birthday' => '',
        'password' => '',
        'passport_number' => '',
        'gender' => '',
        'registration_address' => '',
        'residence' => '',
    );


    if (!empty($arUsers)) {
        $patch = "/upload/userdata_export/";
        DeleteDirFilesEx($patch);

        $archivPatch = $patch . 'files.zip';
        $packarc = CBXArchive::GetArchive($_SERVER["DOCUMENT_ROOT"] . $archivPatch, 'ZIP');
        $arPackFiles = array();

        $patch = $_SERVER["DOCUMENT_ROOT"] . $patch;
        CheckDirPath($patch);

        foreach ($arUsers as $arUser) {
            CheckDirPath($patch . $arUser['ID'] . '/');
            if (!empty($arUser['UF_SCAN_PASSPORT1'])) {
                $scan1 = CFile::GetPath($arUser['UF_SCAN_PASSPORT1']);
                CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . $scan1, $patch . $arUser['ID'] . '/1' . pathinfo($scan1, PATHINFO_BASENAME));
                $arPackFiles[] = $patch . $arUser['ID'] . '/1' . pathinfo($scan1, PATHINFO_BASENAME);
            }

            if (!empty($arUser['UF_SCAN_PASSPORT2'])) {
                $scan2 = CFile::GetPath($arUser['UF_SCAN_PASSPORT2']);
                CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . $scan2, $patch . $arUser['ID'] . '/2' . pathinfo($scan2, PATHINFO_BASENAME));
                $arPackFiles[] = $patch . $arUser['ID'] . '/2' . pathinfo($scan2, PATHINFO_BASENAME);
            }

            if (!empty($arUser['UF_BRANCH_KNOWLEDGE']))
                $arJson['branch_of_knowledge'] = $arUser['UF_BRANCH_KNOWLEDGE'];

            if (!empty($arUser['UF_EDUCATION']))
                $arJson['reader_category'] = $arUser['UF_EDUCATION'];

            if (!empty($arUser['WORK_STREET']))
                $arJson['home_address'] = $arUser['WORK_ZIP'] . ' ' . $arUser['WORK_CITY'] . ' ' . $arUser['WORK_STREET'] . ' ' . $arUser['UF_HOUSE2'] . ' ' . $arUser['UF_STRUCTURE2'] . ' ' . $arUser['UF_FLAT2'];
            if (!empty($arUser['WORK_COMPANY']))
                $arJson['job_study_place'] = $arUser['WORK_COMPANY'];

            if (!empty($arUser['EMAIL']))
                $arJson['email'] = $arUser['EMAIL'];

            if (!empty($arUser['NAME']))
                $arJson['firstname'] = $arUser['NAME'];

            if (!empty($arUser['SECOND_NAME']))
                $arJson['middlename'] = $arUser['SECOND_NAME'];

            if (!empty($arUser['LAST_NAME']))
                $arJson['lastname'] = $arUser['LAST_NAME'];

            if (!empty($arUser['PERSONAL_MOBILE']))
                $arJson['phone'] = $arUser['PERSONAL_MOBILE'];

            if (!empty($arUser['PERSONAL_BIRTHDAY']))
                $arJson['birthday'] = ConvertDateTime($arUser['PERSONAL_BIRTHDAY'], "DD/MM/YYYY");

            if (!empty($arUser['UF_PASSPORT_SERIES']))
                $arJson['passport_number'] = $arUser['UF_PASSPORT_SERIES'] . $arUser['UF_PASSPORT_NUMBER'];

            if (!empty($arUser['PERSONAL_ZIP']))
                $arJson['registration_address'] = $arUser['PERSONAL_ZIP'] . ' ' . $arUser['PERSONAL_CITY'] . ' ' . $arUser['PERSONAL_STREET'] . ' ' . $arUser['UF_CORPUS'] . ' ' . $arUser['UF_STRUCTURE'] . ' ' . $arUser['UF_FLAT'];


            $strJson = json_encode(array($arJson));
            RewriteFile($patch . $arUser['ID'] . '/user.txt', $strJson);

            $arPackFiles[] = $patch . $arUser['ID'] . '/user.txt';
        }

        if ($packarc instanceof IBXArchive) {
            $packarc->SetOptions(
                array(
                    "COMPRESS" => true,
                    "STEP_TIME" => 120,
                    "ADD_PATH" => false,
                    "REMOVE_PATH" => $patch,
                    "CHECK_PERMISSIONS" => false
                )
            );

            /*	$arPackFiles = array();
            foreach($arResult['ITEM']['FILES'] as $file)
            {
            $newTmpFile = $_SERVER["DOCUMENT_ROOT"].'/upload/tmp/scan/'.$arResult['ITEM']['ID'].'/'.$file['description'];
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"].$file['link'], $newTmpFile);
            $arPackFiles[] = $newTmpFile;
            }*/

            $pRes = $packarc->Pack($arPackFiles);

            header('X-Accel-Redirect: ' . $archivPatch);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($archivPatch));
            exit;
        }
    }
}

$APPLICATION->SetTitle('Экспорт пользователей прошедших полную регистрацию');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

?>
    <form method="POST" target="_blank" id="form" name="form" action="<?= $APPLICATION->GetCurUri() ?>"
          enctype="multipart/form-data">
        <?= bitrix_sessid_post() ?>
        <?
        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td width="150">Дата регистрации:</td>
            <td><?= CalendarPeriod("date_register_1", htmlspecialcharsbx($date_register_1), "date_register_2", htmlspecialcharsbx($date_register_2), "form", "Y") ?></td>
        </tr>
        <?
        $tabControl->Buttons();
        ?>
        <input type="submit" class="adm-btn-save" title="Скачать" value="Скачать" name="export">
        <?
        $tabControl->End();
        ?>
    </form>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

?>