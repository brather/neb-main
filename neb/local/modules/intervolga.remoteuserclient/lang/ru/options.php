<?
$MESS["intervolga.remoteuserclient.OPTIONS_GENERAL_TAB"] = "Основные настройки";
$MESS["intervolga.remoteuserclient.OPTION_SERVER"] = "Сервер хранения пользователей (напр. http://example.com)";
$MESS["intervolga.remoteuserclient.OPTION_PAGE"] = "Страница для отправки запросов (напр. /api/soap/)";
$MESS["intervolga.remoteuserclient.OPTIONS_ACCESS_TAB"] = "Настройки доступа";
$MESS["intervolga.remoteuserclient.OPTION_LOGIN"] = "Логин для сервера пользователей";
$MESS["intervolga.remoteuserclient.OPTION_PASSWORD"] = "Пароль для сервера пользователей";