<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (class_exists('nota_exalead')) {
    return;
}

class nota_exalead extends CModule
{
    public $MODULE_ID = 'nota.exalead';
    public $MODULE_VERSION = '0.0.1';
    public $MODULE_VERSION_DATE = '2014-08-06 00:00:00';
    public $MODULE_NAME = 'Exalead';
    public $MODULE_DESCRIPTION = 'Модуль для работы с Exalead';
    public $MODULE_GROUP_RIGHTS = 'N';
    public $PARTNER_NAME = "Notamedia";
    public $PARTNER_URI = "http://www.notamedia.ru";

    public function DoInstall()
    {
        global $APPLICATION;
        if ($this->InstallDB()) {
            $this->InstallFiles();
        }
        $APPLICATION->IncludeAdminFile('Установка', __DIR__ . '/step1.php');
    }

    public function InstallDB()
    {
        global $APPLICATION, $DB;
        $errors = $DB->RunSQLBatch(__DIR__ . '/db/' . strtolower($DB->type) . '/install.sql');
        if ($errors) {
            $APPLICATION->ThrowException(implode('<br>', (array)$errors));
            return false;
        }
        RegisterModule($this->MODULE_ID);
        RegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "nota.exalead", "Nota\Exalead\Properties\Library", "GetIBlockPropertyDescription");

        return true;
    }

    public function InstallFiles($arParams = array())
    {
        //CopyDirFiles(__DIR__ . '/admin/', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin', true, true);
        return true;
    }

    public function DoUninstall()
    {
        global $APPLICATION, $step, $obModule;
        $step = IntVal($step);
        if ($step < 2)
            $APPLICATION->IncludeAdminFile('Удаление модуля ' . $this->MODULE_NAME, __DIR__ . '/unstep1.php');
        elseif ($step == 2) {
            if ($this->UnInstallDB(array("savedata" => $_REQUEST["savedata"]))) {
                $GLOBALS["CACHE_MANAGER"]->CleanAll();
                $this->UnInstallFiles(array('savedata' => $_REQUEST['savedata']));
            }
            $APPLICATION->IncludeAdminFile('Удаление модуля ' . $this->MODULE_NAME, __DIR__ . '/unstep2.php');
        }
    }

    public function UnInstallDB(array $arParams = array())
    {
        global $APPLICATION, $DB;
        if (!array_key_exists('savedata', $arParams) || $arParams['savedata'] !== 'Y') {
            $errors = $DB->RunSQLBatch(__DIR__ . '/db/' . strtolower($DB->type) . '/uninstall.sql');
            if ($errors) {
                $APPLICATION->ThrowException(implode('<br>', (array)$errors));
                return false;
            }
        }

        UnRegisterModule($this->MODULE_ID);
        UnRegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "nota.exalead", "Nota\Exalead\Properties\Library", "GetIBlockPropertyDescription");

        return true;
    }

    public function UnInstallFiles(array $arParams = array())
    {
        //DeleteDirFiles(__DIR__ . '/admin/', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/');
        if (!array_key_exists('savedata', $arParams) || $arParams['savedata'] !== 'Y') {
            DeleteDirFilesEx('upload/' . $this->MODULE_ID);
        }
        return true;
    }
}