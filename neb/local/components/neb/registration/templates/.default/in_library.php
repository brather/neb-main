<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var CMain            $APPLICATION
 * @var array            $arResult
 * @var CBitrixComponent $component
 * @var CUser            $USER
 */
/** тип регистрации как для обычных пользователей */
$USER->SetParam('REGISTER_TYPE', 'full');
$APPLICATION->IncludeComponent(
    'neb:main.register',
    'full_library',
    array(
        'SHOW_FIELDS'        => array(
            0  => 'EMAIL',
            1  => 'NAME',
            2  => 'SECOND_NAME',
            3  => 'LAST_NAME',
            4  => 'PERSONAL_GENDER',
            5  => 'PERSONAL_BIRTHDAY',
            6  => 'PERSONAL_STREET',
            7  => 'PERSONAL_CITY',
            8  => 'PERSONAL_ZIP',
            9  => 'WORK_COMPANY',
            10 => 'PERSONAL_MOBILE',
            11 => 'WORK_STREET',
            12 => 'WORK_CITY',
            13 => 'WORK_ZIP',
            14 => 'PERSONAL_NOTES',
            15 => 'UF_LIBRARY',
            16 => 'UF_STATUS',
        ),
        'REQUIRED_FIELDS'    => array(
            0  => 'EMAIL',
            1  => 'NAME',
            3  => 'LAST_NAME',
            4  => 'PERSONAL_GENDER',
            15 => 'UF_LIBRARY',
            16 => 'UF_STATUS',
        ),
        'AUTH'               => 'N',
        'LIBRARY_ID'         => $arResult['LIBRARY']['ID'],
        'UF_LIBRARIES'       => array(
            55
        ),
        'MESSAGE'            =>
            'Приветствуем Вас как нового пользователя НЭБ!',
        'USE_BACKURL'        => 'N',
        'SUCCESS_PAGE'       => '',
        'SET_TITLE'          => 'Y',
        'USER_PROPERTY'      => array(
            0  => 'UF_LIBRARY',
            1  => 'UF_STATUS',
            2  => 'UF_PASSPORT_NUMBER',
            3  => 'UF_ISSUED',
            4  => 'UF_CORPUS',
            5  => 'UF_STRUCTURE',
            6  => 'UF_FLAT',
            7  => 'UF_PASSPORT_SERIES',
            8  => 'UF_EDUCATION',
            9  => 'UF_HOUSE2',
            10 => 'UF_STRUCTURE2',
            11 => 'UF_FLAT2',
            12 => 'UF_PLACE_REGISTR',
            13 => 'UF_SCAN_PASSPORT1',
            14 => 'UF_SCAN_PASSPORT2',
            15 => 'UF_CITIZENSHIP',
            16 => 'UF_STATUS',
        ),
        'USER_PROPERTY_NAME' => '',
        'NEED_SCANS'         => 'N',
        'MAIL_EVENTS'        => array('USER_REGISTRATION'),
    ),
    $component
);
