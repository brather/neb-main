<?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$MOD_RIGHT = $APPLICATION->GetGroupRight("notaext");
if ($MOD_RIGHT != "D") {
    $aMenu = array(
        "parent_menu" => "global_menu_settings",
        "section" => "notaext",
        "sort" => 2000,
        "text" => GetMessage("NOTAEXT_MENU_MAIN"),
        "title" => GetMessage("NOTAEXT_MENU_MAIN_TITLE"),
        "icon" => "notaext_menu_icon",
        "page_icon" => "notaext_page_icon",
        "items_id" => "menu_notaext",

        "items" => array(
            array(
                "text" => GetMessage("NOTAEXT_MENU_EXT"),
                "more_url" => array('nota_ext_options.php', 'nota_ext.php'),
                "title" => GetMessage("NOTAEXT_MENU_EXT_TITLE"),
                "url" => "nota_ext.php?lang=" . LANGUAGE_ID,
            ),
        )
    );

    return $aMenu;
}
return false;
