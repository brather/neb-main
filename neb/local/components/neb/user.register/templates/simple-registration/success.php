<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arResult */
?>
<p>На указанный Вами адрес электронной почты направлено письмо.
    Для завершения процесса регистрации необходимо перейти по ссылке из письма.</p>