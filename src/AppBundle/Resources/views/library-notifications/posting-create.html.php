<?php
/** @var \Symfony\Bundle\FrameworkBundle\Templating\PhpEngine $view */
/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
/** @var \LbcBundle\DependencyInjection\MustacheEngine $mustache */
/** @var \Symfony\Bundle\FrameworkBundle\Templating\GlobalVariables $app */
/** @var \BitrixBundle\DependencyInjection\BitrixViewHelper $bitrixViewHelper */
/** @var \AppBundle\DependencyInjection\NebViewHelper $nebHelper */
/** @var array $data */
$bitrixViewHelper = $view['bitrix_view_helper'];
$nebHelper = $view['neb_view_helper'];

$nebHelper->templateHelper()->addJs(
    '/local/templates/adaptive/vendor/mustache.min.js',
    ['footer', 'afterDefer']
);
$nebHelper->templateHelper()->addJs(
    '/local/templates/adaptive/js/file.uploader.js',
    ['footer', 'afterDefer']
);
$nebHelper->templateHelper()->addJs(
    '/local/templates/adaptive/vendor/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
    ['footer', 'afterDefer']
);
$nebHelper->templateHelper()->addJs(
    '/local/templates/adaptive/vendor/blueimp-file-upload/js/jquery.iframe-transport.js',
    ['footer', 'afterDefer']
);
$nebHelper->templateHelper()->addJs(
    '/local/templates/adaptive/vendor/blueimp-file-upload/js/jquery.fileupload.js',
    ['footer', 'afterDefer']
);
$nebHelper->templateHelper()->addJs(
    '/local/templates/adaptive/vendor/tinymce/tinymce.min.js',
    ['footer', 'afterDefer']
);
$slots = $view['slots'];
$mustache = $view['mustache_engine'];
$view->extend('AppBundle::base.html.php');
$slots->set(
    'body',
    $mustache->render('notification/create-posting', $data)
);