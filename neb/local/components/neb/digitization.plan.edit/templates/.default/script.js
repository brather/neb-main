$(function(){
    //region Задание переменных
    var speddToggle = 200,                      // скорость анимации
        cls = {
            updatePlan  : '.js-update-plan',    // класс для ссылки на обновление свойств плана
            updateBook  : '.js-update-book',    // класс для ссылки на обновление свойств книги
            tooltip     : '.js-tooltip',        // класс для элемента над которым необходимо вывести tooltip
            list        : '.blip-list',         // класс для списка с изданиями
            item        : '.blip-item',         // класс элемента списка
            cBooks      : '.blip',
            dPlan       : 'blip-duplication',
            dBook       : 'blip-duplication-book',
            dNoCanceled : 'blip-duplication-no-canceled',
            selectBook  : '.book-status',
            disabled    : 'disabled'
        };
    //endregion
    //region Подключение tooltip
    $(cls.tooltip).tooltip();
    //endregion
    //region Обновление издания
    $(cls.updateBook).on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            tSelect = $($this.data('status-book')),
            canceledVal = tSelect.find('[data-canceled="Y"]').val(),
            value = tSelect.val(),
            parentSel = $this.closest(cls.item),
            send = {
                idStatus : value,
                idBook : $this.data('id-book')
            };
        if(!$this.hasClass(cls.disabled)) {
            startLoader('body');
            if(parentSel.hasClass(cls.dBook)) {
                if(canceledVal == value) {
                    parentSel.removeClass(cls.dNoCanceled);
                } else {
                    parentSel.removeClass(cls.dNoCanceled).addClass(cls.dNoCanceled);
                }
            }
            $.ajax({
                type : 'post',
                data : send,
                url : $this.data('ajax-url'),
                dataType: 'json',
                success : function(data) {
                    stopLoader();
                    if(data.ERROR) {
                        alert(data.ERROR);
                    }
                }
            });
        }
    });
    //endregion
    //region Обновление статуса плана
    $(document).on('click', cls.updatePlan, function(e){
        e.preventDefault();
        var $this = $(this),
            send = {
                DATA_PLAN_UPDATE_AJAX   : 'Y',
                DATA_PLAN_ID            : $this.data('plan-id'),
                DATA_PLAN_STATUS        : $($this.data('status-plan')).val(),
                DATA_BOOK_STATUS        : $('#ps-' + $($this.data('status-plan')).val()).val(),
                SET_STATUS_BOOK         : 'Y'
            },
            toggler = $(e.target),
            message = {
                title           : $this.data('confirm-title'),
                text            : $this.data('confirm-question'),
                confirmTitle    : $this.data('confirm-request')
            };
        if(!$this.hasClass(cls.disabled)) {
            if($(cls.list).hasClass(cls.dPlan)) {
                if($(cls.list).find('.' + cls.dNoCanceled).length > 0) {
                    $.when(FRONT.confirm.handle(message, toggler)).then(function(confirmed) {
                        if(confirmed) {
                            setUpdateStatusPlan();
                        }
                    });
                } else {
                    setUpdateStatusPlan();
                }
            }
            else {
                setUpdateStatusPlan();
            }
        }
        function setUpdateStatusPlan() {
            startLoader('body');
            $.ajax({
                type : 'post',
                data : send,
                url  : $this.data('ajax-url'),
                dataType: 'json',
                success : function(data) {
                    $.each(data.LIST_BOOKS, function (i,y) {
                        var selectSet = $('#blip-status-' + y.ID),
                            optionSelected = selectSet.find('[value=' + y.UF_REQUEST_STATUS + ']');
                        if(selectSet.val() != y.UF_REQUEST_STATUS) {
                            optionSelected.prop("selected", true);
                            selectSet.prepend(optionSelected);
                        }
                    });
                    stopLoader();
                }
            });
        }
    });
    //endregion

    // показ формы комментария к изданиям
    $(document).on('click', '.btn-comment', function(e){
        $(this).hide();
        $("#comment-form-" + $(this).attr('data-id-book')).show(300);
    });

    // изменения комментария
    $('.comment-form').on('submit', function(e){
        
        e.preventDefault();

        var form  = $(this);
        var bookId = $(this).attr('data-id-book');

        $.ajax({
            type: $(this).attr('method'),
            url:  $(this).attr('action'),
            data: $(this).serialize(),
            cache: false,
            dataType: "json",
            success: function(data) {
                if (data.result == 1) {
                    $(form).hide();
                    $(".btn-comment-" + bookId).show();
                }
            },
            error:  function(xhr, str){
                alert('При передаче данных возникла ошибка!');
            }
        });

        return false;
    });
});
//region Функция запуска лоадера
function startLoader(block) {
    stopLoader();
    var blkLoader = '<div id="blk-loader" class="blk-milk-shadow"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
    $(block).append(blkLoader);
}
//endregion
//region Функция остановки лоадера
function stopLoader() {
    $('#blk-loader').animate({ 'opacity' : 0 }, 300, function(){
        $(this).remove();
    });
}
//endregion