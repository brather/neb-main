<?

/**
 * Формируем меню для установленных расширений
 *
 * Class CMenuExt
 */

class CMenuExt
{
    function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {

        $arExtMenu = array();

        $strFolder = substr(__DIR__, 0, strrpos(__DIR__, '/')) . '/ext';
        $dirExts = CExt::getDir($strFolder);
        foreach ($dirExts as $ext) {
            $ext_dir = $strFolder . "/" . $ext;
            $ext_dir_info = $ext_dir . '/index.php';
            $ext_dir_menu = $ext_dir . '/admin/menu.php';
            if (file_exists($ext_dir_info) and file_exists($ext_dir_menu)) {
                include_once($ext_dir_info);
                if (class_exists($ext)) {
                    $info = new $ext;
                    $status = $info->IsInstalled();
                    if ($status === true) {
                        include_once($ext_dir_menu);
                        if (is_array($arMenu) and !empty($arMenu["items"]))
                            $aModuleMenu[] = $arMenu;
                    }
                }
            }
        }
    }
}
