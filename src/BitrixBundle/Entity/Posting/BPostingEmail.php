<?php
namespace BitrixBundle\Entity\Posting;


use Doctrine\ORM\Mapping as ORM;

/**
 * BPostingEmail
 *
 * @ORM\Table(name="b_posting_email", indexes={@ORM\Index(name="ix_posting_email_status", columns={"POSTING_ID", "STATUS"}), @ORM\Index(name="ix_posting_email_email", columns={"POSTING_ID", "EMAIL"})})
 * @ORM\Entity
 */
class BPostingEmail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="POSTING_ID", type="integer", nullable=false)
     */
    public $postingId;

    /**
     * @var string
     *
     * @ORM\Column(name="STATUS", type="string", length=1, nullable=false)
     */
    public $status = 'Y';

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=255, nullable=false)
     */
    public $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="SUBSCRIPTION_ID", type="integer", nullable=true)
     */
    public $subscriptionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=true)
     */
    public $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getPostingId()
    {
        return $this->postingId;
    }

    /**
     * @param int $postingId
     */
    public function setPostingId($postingId)
    {
        $this->postingId = $postingId;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * @param int $subscriptionId
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }


}
