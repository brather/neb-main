<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
//region Если запрос пришел из подборки, то добавляем параметр в get
if(!empty($arParams['PARENT_COLLECTION'])) {
	$parentSectionId = '?PARENT_SECT_ID=' . $arParams['PARENT_COLLECTION'];
} else {
	$parentSectionId = '';
}
//endregion
?>
<div class="b-createcoll">
	<a href="/profile/collection/add/<?= $parentSectionId ?>" class="b-btcreatecoll b-btcreate">
		<?= Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_ADD_COL') ?>
	</a>
</div>
<?if(empty($arResult['SECTIONS'])) {
	return false;
}?>
<div class="b-filter_wrapper">
	<a <?= SortingExalead("element_cnt") ?>><?= Loc::getMessage('COLLECTION_LIST_SORT_BY_COUNT') ?></a>
	<a <?= SortingExalead("name") ?>><?= Loc::getMessage('COLLECTION_LIST_SORT_BY_TITLE') ?></a>
	<a <?= SortingExalead("created") ?>><?= Loc::getMessage('COLLECTION_LIST_SORT_BY_DATE') ?></a>
</div>

<ul class="collections-list onecol-collection">
	<?if($arParams['PAGEN'] > 1) {
		$n = ($arParams['PAGEN'] - 1) * $arParams['SIZEN'];
	} else {
		$n = 0;
	}?>
	<?foreach($arResult['SECTIONS'] as $collection){
		$n++;?>
		<li class="collections-list-item cls">
			<div class="cls__wrapper">
				<span class="cls__num"><?= $n ?><?= Loc::getMessage("COLLECTION_LIST_DOT") ?></span>
				<h2><?= $collection['NAME'] ?></h2>
				<div class="cls__author"></div>
				<div class="collections-list-item__preview clsp">
					<!--div class="clsp__cover"></div-->
					<div class="clsp__details">
						<div class="clsp__editions-list"
							 data-section-id="<?= $collection['ID'] ?>"
							 data-count-all="<?= $collection['ELEMENT_CNT'] ?>"
							 data-count-in-slide="<?= $arParams["COUNT_BOOKS_IN_COLLECTION"] ?>"
							 data-iblock-id="<?= $arResult['IBLOCK_ID_COLLECTIONS'] ?>"
							 data-slide-editions="<?= $n ?>">
							<?$iBook = 0;?>
							<?foreach($arResult['BOOKS_IN_SECTIONS'][$collection['ID']] as $bookID) {?>
								<?$book = $arResult['BOOKS'][$bookID];
								if(($iBook + 1) % 4 == 0) {
									$dPlacement = 'left';
								} else {
									$dPlacement = 'right';
								}
								$iBook++;?>
								<div class="clsp__editions-list-item" data-book-id="<?= $book["BOOK_ID_IN_IBLOCK"] ?>">
									<a href="<?= $book['DETAIL_PAGE_URL'] ?>">
										<img class="loadingimg"
											 data-placement="<?= $dPlacement ?>"
											 title="<?= $book['title'] ?>"
											 src="<?= $book['IMAGE_URL'] ?>"
											 alt="<?= $book['title'] ?>">
									</a>
									<div class="b-lib_counter">
										<div class="icoviews">
											x <?= number_format($book['STAT']['VIEW_CNT'], 0, '', ' ') ?>
										</div>
									</div>
								</div>
							<?}?>
						</div>
						<div class="btn-group pull-right">
							<a href="/profile/collection/<?=$collection['ID']?>/" class="btn btn-primary">
								<?= $collection['ELEMENT_CNT'] ?>
								<?= GetEnding(
									$arSection['ELEMENT_CNT'],
									Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_BOOK_5'),
									Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_BOOK_1'),
									Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_BOOK_2')) ?>
								<?= Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_IN_COL') ?>
							</a>
							<div class="btn-group dropup">
								<button id="drop<?= $collection['ID'] ?>" data-toggle="dropdown" class="btn btn-default dropdown-toggle">
									<span class="buterbrod"></span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="drop<?=$collection['ID']?>">
									<li class="dropdown-header">
										<?= Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_MANAGE') ?>
									</li>
									<li>
										<a class="button_blue btadd closein add_books_to_fond"
											data-add-book-to-collection="<?= $collection['ID'] ?>"
											href="<?= $this->__folder . '/addBookForm.php?'.bitrix_sessid_get().'&COLLECTION_ID=' . $collection['ID'] ?>">
											<?= Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_ADD_BOOK') ?>
										</a>
									</li>
									<li>
										<a href="/profile/collection/edit/<?=$collection['ID']?>/" class="">
											<?= Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_EDIT') ?>
										</a>
									</li>
									<li>
										<a onclick="return confirm('Подборка <?=$collection['NAME']?> будет удалена. Вы уверены?')"
											href="/profile/collection/?<?= bitrix_sessid_get() ?>&COLLECTION_ID=<?= $collection['ID'] ?>&action=remove">
											<?= Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_DELETE') ?>
										</a>
									</li>
									<li>
										<a target="_blank"
										   href="/bitrix/admin/iblock_list_admin.php?PAGEN_1=1&SIZEN_1=20&IBLOCK_ID=6&type=collections&lang=ru&find_section_section=<?= $collection['ID'] ?>&by=sort&order=asc">
											<?= Loc::getMessage('COLLECTION_LIST_SORT') ?>
										</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="<?= $collection['SECTION_PAGE_URL'] ?>">
											<?= Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_PAGE') ?>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<button type="button" class="clsp__editions-list-prev"></button>
						<button type="button" class="clsp__editions-list-next"></button>
					</div>
				</div>
			</div>
		</li>
	<?}?>
</ul>
<?= $arResult['NAV_STRING'] ?>


