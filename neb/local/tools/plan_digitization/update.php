<?require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

use \Bitrix\Main\Loader,
    \Bitrix\Main\Application,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('highloadblock');
Loader::includeModule('nota.journal');
Loader::includeModule('nota.exalead');

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
$result = ['data' => []];
$arBookId = $arRequest['BOOK_ID'];

$obUser = new nebUser();
$library = $obUser->getLibrary();
if (!$obUser->IsAuthorized()) {
    $result = array(
        'error' => '',
        'message' => 'Вы не авторизованы!'
    );
}
if (!$obUser->isLibrary() && !$obUser->isOperator()) {
    $result = array(
        'error' => '403',
        'message' => 'Access deny'
    );
} elseif ($obUser->isLibrary() && empty($library['ID'])) {
    $result = array(
        'error' => '500',
        'message' => 'Empty library id'
    );
}

//region Вернем id блока с планами оцифровки
$idHLBlockDigitization = '';
$connection = Application::getConnection();
$sql = "SELECT ID,NAME FROM b_hlblock_entity WHERE NAME='" . HIBLOCK_CODE_DIGITIZATION_PLAN . "';";
$recordset = $connection->query($sql);
while($record = $recordset->fetch()) {
    $idHLBlockDigitization = $record['ID'];
}
//endregion
$idNewStatus = '';
if(empty($arRequest['STATUS'])) {
    //region Получим данные по свойству "Статус"
    //region Возвращаем id самого свойства
    $rsData = CUserTypeEntity::GetList(
        array("SORT" => "ASC"),
        array("ENTITY_ID" => "HLBLOCK_" . $idHLBlockDigitization, "FIELD_NAME" => "UF_REQUEST_STATUS")
    );
    $idPropStatus = '';
    if($arRes = $rsData->Fetch()) {
        $idPropStatus = $arRes["ID"];
    }
    //endregion
    //region Возвращаем массив значений свойства с id
    $arPropStatus = array();
    if($idPropStatus != '') {
        $obUserFieldEnum = new CUserFieldEnum();
        $rsStatus = $obUserFieldEnum->GetList(['SORT' => 'ASC'], ['USER_FIELD_ID' => $idPropStatus]);
        while ($arStatus = $rsStatus->Fetch()) {
            if($arStatus['VALUE'] == DIGITIZATION_STATUS_ON) {
                $idNewStatus = $arStatus['ID'];
            }
            $arPropStatus[$arStatus['ID']] = $arStatus;
        }
    }
    //endregion
    //endregion
} else {
    $idNewStatus = $arRequest['STATUS'];
}

$hlblock = HighloadBlockTable::getById($idHLBlockDigitization)->fetch();
$entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
$filter = array(
    'UF_LIBRARY' => $library['ID'],
    'UF_EXALEAD_BOOK' => $arBookId
);
$rsData = $entity_data_class::getList(array(
    "select" => array("ID"),
    "filter" => $filter
));
while ($arData = $rsData->fetch()) {
    $entity_data_class::update($arData['ID'], array('UF_PLAN_ID' => $arRequest['plan_id']));
    $entity_data_class::update($arData['ID'], array('UF_REQUEST_STATUS' => $idNewStatus));
    $entity_data_class::update($arData['ID'], array('UF_DATE_FINISH' => $arRequest['date']));
}

MainHelper::showJson($result);