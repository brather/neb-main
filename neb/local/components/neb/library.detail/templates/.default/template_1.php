<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__DIR__.'/template.php');

?>

<div class="row">
    <div class="col-md-3 col-sm-3 lk-sidebar" itemscope itemtype="http://schema.org/<?= $arResult['ITEM']['SCHEMA_TYPE']?>">

        <?$APPLICATION->ShowViewContent('lib_menu')?>

        <meta itemprop="name"  content="<?=trim($arResult['ITEM']['NAME'])?>"  />
        <? if (!empty($arResult['ITEM']['IMAGE'])): ?>
            <meta itemprop="image" content="<?=$arResult['ITEM']['IMAGE']?>" />
        <? endif; ?>
        <? if (!empty($arResult['ITEM']['MAP']) && false !== stripos($arResult['ITEM']['SCHEMA_TYPE'], 'library')): ?>
            <span itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
                <meta itemprop="latitude" content="<?= $arResult['ITEM']['MAP'][0] ?>" />
                <meta itemprop="longitude" content="<?= $arResult['ITEM']['MAP'][1] ?>" />
            </span>
        <? endif; ?>
        <? if (!empty($arResult['ITEM']['PROPERTY_URL_VALUE'])): ?>
            <a href="<?= $arResult['ITEM']['PROPERTY_URL_VALUE'] ?>" class="b-libsite" target="_blank"
               itemprop="url"><?= $component->getLinkView($arResult['ITEM']['PROPERTY_URL_VALUE']) ?></a>
        <? endif; ?>

        <? $APPLICATION->IncludeComponent(
            "neb:library.right_counter",
            "",
            Array(
                "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                "LIBRARY_ID" => $arResult['ITEM']["ID"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "COLLECTION_URL" => $arParams['COLLECTION_URL'],
            ),
            $component
        );
        ?>
        <?$APPLICATION->IncludeComponent("bitrix:voting.current", "right", Array(
            "CHANNEL_SID" => "LIBRARY_".$arResult['ITEM']['ID'],	// Группа опросов
            "VOTE_ID" => "",	// ID опроса
            "VOTE_ALL_RESULTS" => "N",	// Показывать варианты ответов для полей типа Text и Textarea
            "AJAX_MODE" => "Y",	// Включить режим AJAX
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CACHE_TIME" => "3600",	// Время кеширования (сек.)
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
        ),
            false
        );?>

        <dl class="lib-contacts">
            <? if (!empty($arResult['ITEM']['PROPERTY_ADDRESS_VALUE'])) { ?>
                <dt><?= Loc::getMessage('LIBRARY_DETAIL_ADDRESS'); ?>:</dt>
                <dd itemprop="address" itemtype="http://schema.org/PostalAddress"><?= $arResult['ITEM']['PROPERTY_ADDRESS_VALUE'] ?></dd>
            <? } ?>
            <? if (!empty($arResult['ITEM']['PROPERTY_SCHEDULE_VALUE'])) { ?>
                <dt class="lib_schedule"><?= Loc::getMessage('LIBRARY_DETAIL_WORK'); ?>:</dt>
                <dd<? if (false !== stripos($arResult['ITEM']['SCHEMA_TYPE'], 'library')):
                    ?> itemprop="openingHours"<? endif; ?>><?= $arResult['ITEM']['PROPERTY_SCHEDULE_VALUE'] ?></dd>
            <? } ?>
            <? if (!empty($arResult['ITEM']['PROPERTY_PHONE_VALUE'])) { ?>
                <dt><?= Loc::getMessage('LIBRARY_DETAIL_PHONE'); ?></dt>
                <dd>
                    <? foreach ($arResult['ITEM']['PROPERTY_PHONE_VALUE'] as $sPhone): ?>
                        <span itemprop="telephone"><?=trim($sPhone)?></span><br />
                    <? endforeach; ?>
                </dd>
            <? } ?>
            <? if (!empty($arResult['ITEM']['PROPERTY_EMAIL_VALUE'])) { ?>
                <dt><?= Loc::getMessage('LIBRARY_DETAIL_EMAIL'); ?></dt>
                <dd>
                    <a href="mailto:<?= $arResult['ITEM']['PROPERTY_EMAIL_VALUE'] ?>"><span
                                itemprop="email"><?= $arResult['ITEM']['PROPERTY_EMAIL_VALUE'] ?></span></a>
                </dd>
            <? } ?>
            <? if (!empty($arResult['ITEM']['PROPERTY_SKYPE_VALUE'])) { ?>
                <dt>Skype</dt>
                <dd><?= $arResult['ITEM']['PROPERTY_SKYPE_VALUE'] ?></dd>
            <? } ?>
            <? if (!empty($arResult['ITEM']['PROPERTY_CONTACTS_VALUE'])) { ?>
                <dt><?= Loc::getMessage('LIBRARY_DETAIL_CONTACT'); ?></dt>
                <dd>
                    <? foreach ($arResult['ITEM']['PROPERTY_CONTACTS_VALUE'] as $sContact): ?>
                        <span itemprop="employee"><?=trim($sContact)?></span><br />
                    <? endforeach; ?>
                </dd>
            <? } ?>
        </dl><!-- /.b-lib_contact -->
    </div>

    <div class="col-md-9 col-sm-9 lk-content">
        <div class="iblock b-lib_fulldescr">
            <div class="b-libstatus"><?=$arResult['ITEM']['PROPERTY_STATUS_VALUE']?></div>

            <div class="b-rolledtext" data-height="300">
                <div class="b-rolled_in ani_all_500_in">
                    <div class="b-rolled_gradient"></div>
                    <p><?=htmlspecialcharsBack(TxtToHTML($arResult['ITEM']['~PREVIEW_TEXT']))?></p>
                </div>
            </div>
        </div>
        <div style="height: 360px;">
        <span class="marker_lib hidden">
        <a href="#" onclick="return false;" class="b-elar_name_txt"><?=$arResult['ITEM']['NAME']?></a><br />
        <span class="b-elar_status"><?=$arResult['ITEM']['PROPERTY_STATUS_VALUE']?></span><br />
        <span class="b-map_elar_info">
            <?
            if(!empty($arResult['ITEM']['PROPERTY_ADDRESS_VALUE']))
            {
                ?>
                <span class="b-map_elar_infoitem"><span><?=Loc::getMessage('LIBRARY_DETAIL_ADDRESS_MAP');?>:</span><?=$arResult['ITEM']['PROPERTY_ADDRESS_VALUE']?></span><br />
            <?
            }
            if(!empty($arResult['ITEM']['PROPERTY_SCHEDULE_VALUE']))
            {
                ?>
                <span class="b-map_elar_infoitem"><span><?=Loc::getMessage('LIBRARY_DETAIL_WORK');?>:</span><?=$arResult['ITEM']['PROPERTY_SCHEDULE_VALUE']?></span>
            <?
            }
            ?>
        </span>
        <span class="b-mapcard_act clearfix">
            <span class="right neb b-mapcard_status"><?=Loc::getMessage('LIBRARY_DETAIL_USER');?></span>
        </span>
        </span>
        <div class="fmap" id="fmap" data-lat="<?=$arResult['ITEM']['MAP'][0]?>" data-lng="<?=$arResult['ITEM']['MAP'][1]?>" data-zoom="16"></div>
        <script src="//api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
        </div>
    </div>

    </div>
</div>
