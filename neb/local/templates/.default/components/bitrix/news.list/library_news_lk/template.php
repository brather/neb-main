<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<a href="/profile/news/add/" class="btn btn-primary"><?=GetMessage('NEWS_LIST_LIBRARY_CREATE');?></a>
<br>

<? if ( !empty($arResult["ITEMS"]) ) { ?>
	<ul class="lk-newslist">
		<? foreach($arResult["ITEMS"] as $arItem) { ?>
			<li class="lk-newslist__kind clearfix" data-news-list-item>
				<div class="lk-newslist__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
				<div class="lk-newslist__content">
					<h3 class="lk-newslist__header">
						<a href="/profile/news/edit/<?=$arItem['ID']?>/" title="<?=GetMessage('NEWS_LIST_LIBRARY_EDIT');?>">
							<?=$arItem["NAME"]?>
						</a>
					</h3>
					<p class="lk-newslist__text"><?=$arItem["~PREVIEW_TEXT"]?></p>
					<div class="lk-newslist__action">
						<div class="lk-newslist__action-public">
							<label>
								<input
									class="checkbox"
									data-publish
									data-news_id="<?=$arItem['ID'];?>"
									id="news-id<?=$arItem['ID'];?>"  <?=($arItem['ACTIVE'] == 'Y') ? 'checked="checked"' : ''?>
									type="checkbox"
									name="active[<?=$arItem['ID'];?>]">
								<span class="lbl"><?=GetMessage('NEWS_LIST_LIBRARY_PUBLISHED');?></span>
							</label>
						</div>
						<a 
							class="lk-newslist__action-delete btn btn-default btn-sm"
							data-delete-with-confirm
							href="/local/tools/library_news_lk/actions.php?action=delete&id=<?=$arItem['ID'];?>&back_url=<?=$APPLICATION->GetCurPageParam()?>"
						>
							<?=GetMessage('NEWS_LIST_LIBRARY_DELETE');?>
						</a>
					</div>
				</div>
			</li>
		<? } ?>
	</ul>
	
<? } ?>

<?=$arResult['NAV_STRING']?>
