<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader,
    Bitrix\NotaExt\Iblock,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('notaext');

$term = htmlspecialcharsEx(trim($_REQUEST['term']));
if (empty($term))
    return false;

$arFilter = array(
    'IBLOCK_ID' => array(
        Iblock\IblockTools::getIBlockId('basic_tags'),
        Iblock\IblockTools::getIBlockId('additional_tags')
    ),
    'NAME' => '%' . $term . '%'
);

$arSelect = array('ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_CODE');
$arSort = array('SORT' => 'DESC', 'NAME' => 'ASC');
$answer = array();

$arTags = Iblock\Element::getList($arFilter, 20, $arSelect, $arSort);

if (!empty($arTags['ITEMS'])) {
    foreach ($arTags['ITEMS'] as $arItem) {
        $answer[] = array("id" => $arItem['ID'], "text" => $arItem['~NAME']);
    }
}

if ($_REQUEST['search'] == 'y')
    MainHelper::showJson($answer);
else
    MainHelper::showJson(array('results' => $answer));