<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if ((int)$arResult['NavPageCount'] <= 1)
    return false;

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = $arResult["NavQueryString"];
foreach ($arParams['NAV_ADDITIONAL_PARAMS'] as $k => $v) { // дополнительные обязательные параметры
    $sParam = $k . '=' . $v;
    if (stripos($strNavQueryString, $sParam) === false)
        $strNavQueryString .= '&amp;' . $sParam;
}
$strNavQueryString = ($strNavQueryString != "" ? $strNavQueryString . "&amp;" : "");
$strNavQueryStringFull = ($strNavQueryString != "" ? "?" . $strNavQueryString : "");
$strNavQueryString = str_replace('dop_filter=', 'df=', $strNavQueryString);

// target="_parent"
global $navParent;
if (empty($navParent))
    $navParent = '_parent';
?>
<section class="text-center">
    <ul class="pagination">
        <?php if ($arResult["NavPageNomer"] > 1): ?>
        <li>
            <a href="<?= $arResult["sUrlPath"] . '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . ($arResult["NavPageNomer"] - 1) ?>" aria-label="Previous">
                <span aria-hidden="true" class="fa fa-angle-left"></span>
            </a>
        </li>
        <?php else: ?>
            <li>
                <a href="#" onclick="return false;" aria-label="Previous" class="pagination__active">
                    <span aria-hidden="true" class="fa fa-angle-left"></span>
                </a>
            </li>
        <?php endif; ?>

        <? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>

            <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                <li>
                    <a href="#" target="<?= $navParent ?>" onclick="return false;" class="pagination__active"><?= $arResult["nStartPage"] ?></a>
                </li>

            <? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
                <li>
                    <a target="<?= $navParent ?>" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" ><?= $arResult["nStartPage"] ?></a>
                </li>

            <? else: ?>
                <li>
                    <a target="<?= $navParent ?>" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
                </li>
            <? endif; ?>

            <? $arResult["nStartPage"]++ ?>
        <? endwhile ?>

        <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
            <li>
                <a href="<?= $arResult["sUrlPath"] . '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '='.($arResult["NavPageNomer"] + 1) ?>" target="<?= $navParent ?>" aria-label="Next">
                    <span aria-hidden="true" class="fa fa-angle-right"></span>
                </a>
            </li>
        <? else: ?>
            <li>
                <a href="#" onclick="return false;" aria-label="Next" class="pagination__active">
                    <span aria-hidden="true" class="fa fa-angle-right"></span>
                </a>
            </li>
        <? endif ?>
    </ul>
</section>