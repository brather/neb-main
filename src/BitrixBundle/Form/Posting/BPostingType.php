<?php

namespace BitrixBundle\Form\Posting;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BPostingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('timestampX')
            ->add('status')
            ->add('version')
            ->add('dateSent')
            ->add('sentBcc')
            ->add('fromField')
            ->add('toField')
            ->add('bccField')
            ->add('emailFilter')
            ->add('subject')
            ->add('bodyType')
            ->add('body')
            ->add('directSend')
            ->add('charset')
            ->add('msgCharset')
            ->add('subscrFormat')
            ->add('errorEmail')
            ->add('autoSendTime')
            ->add('bccToSend')
            ->add('postingEmails')
            ->add('postingFiles')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'      => 'BitrixBundle\Entity\Posting\BPosting',
                'csrf_protection' => false,
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bposting';
    }
}
