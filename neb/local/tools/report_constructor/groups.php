<?
/**
 * Вкладка с группировками
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!TblEntities::checkAccess()) {
    die('Permissions denied!');
}

if ($_REQUEST['entities']){
    $agregate= TblEntities::getAgregateType();
    $compare = TblEntities::getCompareType();
    foreach ($_REQUEST['entities'] as $item) {
        $res = TblEntities::getEntities($item);
        $arFields = TblEntities::getEntityFields($item);

        ?>
        <div class="steps__block_group">
        <?foreach ($arFields as $field):?>
            <div class="steps__row clearfix row">
                <div class="form-group col-sm-3">
                    <label for="group<?=$field['Id']?>">
                        <input type="checkbox" id="group<?=$field['Id']?>" name="group[]" value="<?=$field['Id']?>" class="checkbox-depend-toggler">
                        <span class="lbl"><?=$res[$item];?> - <?=$field['Description']?></span>
                    </label>
                </div>

                <div class="form-group col-sm-2">
                    <select id="" class="js-select form-control" name="agr<?=$field['Id']?>" data-select-picker>
                        <?foreach ($agregate as $id=>$name):?>
                        <option value=<?=$id?>><?=$name?></option>
                        <?endforeach;?>
                    </select>
                </div>

                <div class="form-group col-sm-2 checkbox-depend">
                    <select id="" class="js-select form-control" name="having_compare<?=$field['Id']?>" data-select-picker>
                        <?foreach ($compare as $id=>$name):?>
                        <option value=<?=$id?>><?=$name?></option>
                        <?endforeach;?>
                    </select>
                </div>

                <div class="form-group col-sm-2 checkbox-depend" style='width:130px;'>
                    <input type="text" placeholder="Значение" name='value_agr<?=$field['Id']?>' style='width:100px;'>
                </div>
            </div>
        <?endforeach;?>
        </div>
	<?
	}
	?>
	<h4 class="filter-list__title">Группировать по:</h4>
	<div>
		<?foreach ($_REQUEST['entities'] as $item) {
			$res = TblEntities::getEntities($item);
			$arFields = TblEntities::getEntityFields($item);
		?>
			<?foreach ($arFields as $field):?>
			<div class="steps__row clearfix row">
				<div class="form-group col-sm-2" style="width: 45px;padding-right: 0;">
					<a href="" class="sort-arrow sort-up">↑</a>
					<a href="" class="sort-arrow sort-down">↓</a>
				</div>
				<div class="form-group col-sm-3">
					<label for="groupby<?=$field['Id']?>">
						<input type="checkbox" id="groupby<?=$field['Id']?>" name="groupby[]" value="<?=$field['Id']?>" >
						<span class="lbl"><?=$res[$item];?> - <?=$field['Description']?></span>
					</label>
				</div>
			</div>
			<?endforeach;?>
		<?
		}
		?>
	</div>
<?
}
?>

