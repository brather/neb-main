<?
namespace Nota\UserData;

use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity;
use \Bitrix\Highloadblock\HighloadBlockTable;
use \Bitrix\Main\Type\DateTime;
use \Nota\UserData\Collections;

Loader::includeModule('highloadblock');

class Searches
{
    private static function getEntity_data_class($HIBLOCK = HIBLOCK_SEARCHSERS_USERS)
    {
        $hlblock           = HighloadBlockTable::getById($HIBLOCK)->fetch();
        $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $entity_data_class;
    }

    public static function delete($objectId)
    {
        if (empty($objectId))
            return false;

        global $USER;

        $entity_data_class = self::getEntity_data_class();

        $rsData = $entity_data_class::getList(array(
            "select" => array("ID"),
            "filter" => array('UF_UID' => $USER->GetID(), 'ID' => (int)$objectId)
        ));
        if ($arData = $rsData->fetch()) {
            $entity_data_class::delete($arData['ID']);
            Collections::removeLinkObject($arData['ID'], false, 'searches');
        }
    }
}