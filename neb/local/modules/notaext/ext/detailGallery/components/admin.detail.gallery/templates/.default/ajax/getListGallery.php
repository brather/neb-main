<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper;

$IBLOCK_GALLERY_ID = intval($_REQUEST['IBLOCK_GALLERY_ID']);
if ($IBLOCK_GALLERY_ID <= 0)
    exit();

$SECTION_ID = intval($_REQUEST['SECTION_ID']);
if ($SECTION_ID <= 0)
    exit();

global $USER;
if (!$USER->IsAuthorized())
    exit();

Loader::includeModule("iblock");

$iblock_permission = (new CIBlock())->GetPermission($IBLOCK_GALLERY_ID);
if ($iblock_permission < "R")
    exit();

$arResult = array();

$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PICTURE");
$arFilter = Array("IBLOCK_ID" => $IBLOCK_GALLERY_ID, "SECTION_ID" => $SECTION_ID, "!PREVIEW_PICTURE" => false, "!DETAIL_PICTURE" => false, "ACTIVE" => "Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arResult[] = array(
        'ID' => $arFields['ID'],
        'NAME' => $arFields['NAME'],
        'PREVIEW_TEXT' => $arFields['~PREVIEW_TEXT'],
        'PREVIEW_PICTURE' => CFile::GetPath($arFields['PREVIEW_PICTURE']),
        'DETAIL_PICTURE' => CFile::GetPath($arFields['DETAIL_PICTURE']),
    );
}

MainHelper::showJson($arResult);