<?php
/** @var \Symfony\Bundle\FrameworkBundle\Templating\PhpEngine $view */
/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
/** @var \LbcBundle\DependencyInjection\MustacheEngine $mustache */
/** @var \Symfony\Bundle\FrameworkBundle\Templating\GlobalVariables $app */
/** @var array $data */
$slots = $view['slots'];
$mustache = $view['mustache_engine'];
$view->extend('AppBundle::base.html.php');
$slots->set(
    'body',
    $mustache->render('notification/history', $data)
);