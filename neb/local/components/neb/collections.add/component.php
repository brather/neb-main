<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('nota.library');
define('MAX_DEPTH_LEVEL_PARENT_SECTION', '1');
use Nota\Library\Collection,
    \Bitrix\Main\Application;
//region Получаем данные из $_REQUEST
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
//endregion
//region Добавление раздела

if($arRequest['parent_section'] != '-') {
    $showInSlide = '';
    $showName = '';
} else {
    $showInSlide = isset($arRequest['is_show_in_slider']);
    $showName = isset($arRequest['is_show_name']);
}


if ($arRequest['action'] == 'addCollection' && isset($arRequest['name']) && check_bitrix_sessid()) {
    $return = Collection::add(
        $arRequest['name'],
        $arRequest['description'],
        $arRequest['background'],
        $arParams['LIBRARY_ID'],
        $showInSlide,
        $showName,
        $arRequest['parent_section']
    );
    if ($return === true) {
        LocalRedirect('/profile/collection/');
    } else {
        ShowError($return);
    }
}
//endregion
//region Возвращаем список подразделов
if(!empty($arParams['COLLECTION'])) {

    $resSubSect = \Bitrix\Iblock\SectionTable::getList(array(
        'order' => array('SORT' => 'ASC'),
        'filter' => array('IBLOCK_ID' => IBLOCK_ID_COLLECTION, 'IBLOCK_SECTION_ID' => $arParams['COLLECTION']),
        'select' => array('ID', 'NAME')
    ));
    while($obSubSect = $resSubSect->Fetch()) {
        $arResult['SUB_SECTIONS'][] = $obSubSect;
    }
}
//endregion
//region Редактирование раздела
if ($arRequest['action'] == 'editCollection' && isset($arRequest['id']) && check_bitrix_sessid()) {

    if($arRequest['is_active'] == 'on') $active = "Y";
    else $active = "N";

    $return = Collection::edit(
        $arRequest['id'],
        $arRequest['code'],
        $arRequest['name'],
        $active,
        $arRequest['description'],
        $arRequest['background'],
        $arParams['LIBRARY_ID'],
        $showInSlide,
        $showName,
        $arRequest['delete_image'],
        $arRequest['parent_section']
    );
    if ($return === true) {
        LocalRedirect('/profile/collection/');
    } else {
        ShowError($return);
    }
}
//endregion
//region Получение информации о подборке и редирект в случае несуществования подборки (подборку удалили)
if ((int)$arParams['COLLECTION'] > 0) {
    $result = $arResult['COLLECTION'] = Collection::getByID($arParams['COLLECTION']);
    //region редирект на добавление, если коллекция не сущестует
    if (!$result) {
        LocalRedirect('/profile/collection/add/');
    }
    //endregion
}
//endregion
//region Получим список разделов первого уровня для редактирования подборки (если отсутствут подразделы у подборки)
if(count($arResult['SUB_SECTIONS']) == 0) {
    $resParentSect = \Bitrix\Iblock\SectionTable::getList(array(
        'filter' => array('IBLOCK_ID' => IBLOCK_ID_COLLECTION, 'DEPTH_LEVEL' => MAX_DEPTH_LEVEL_PARENT_SECTION),
        'select' => array('ID', 'NAME'),
        'order' => array('NAME' => 'ASC', 'DATE_CREATE' => 'ASC')
    ));
    while($obParentSect = $resParentSect->Fetch()) {
        $arResult['PARENT_SECTIONS'][] = $obParentSect;
    }
}
//endregion
$edit = isset($arResult['COLLECTION']);
$edit ? $APPLICATION->SetTitle(GetMessage('COLLECTION_TITLE_EDIT_COLLECTION')) : $APPLICATION->SetTitle(GetMessage('COLLECTION_TITLE_NEW_COLLECTION'));
$this->IncludeComponentTemplate();