<?
##############################################
# Bitrix Site Manager Forum					 #
# Copyright (c) 2002-2009 Bitrix			 #
# http://www.bitrixsoft.com					 #
# mailto:admin@bitrixsoft.com				 #
##############################################
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vote/prolog.php");

$NUser = new nebUser();
$UserRole = $NUser->isLibrary();
$arLibrary = $NUser->getLibrary();

if ($UserRole === false or empty($arLibrary))
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$VOTE_RIGHT = nebLibrary::getVoteRight();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vote/include.php");

IncludeModuleLangFile(__FILE__);
$err_mess = "File: " . __FILE__ . "<br>Line: ";
define("HELP_FILE", "vote_list.php");
$old_module_version = CVote::IsOldVersion();

/********************************************************************
 * Actions
 ********************************************************************/
$VOTE_ID = intval($VOTE_ID);
$z = $DB->Query("SELECT ID FROM b_vote WHERE ID='$VOTE_ID'", false, $err_mess . __LINE__);
if (!($zr = $z->Fetch())) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    echo "<a href='nota_vote_list.php?lang=" . LANGUAGE_ID . "' class='navchain'>" . GetMessage("VOTE_VOTE_LIST") . "</a>";
    echo ShowError(GetMessage("VOTE_NOT_FOUND"));
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
    die();
}


$z = CVote::GetByID($VOTE_ID);
$zr = $z->Fetch();
$t = CVoteChannel::GetByID($zr["CHANNEL_ID"]);
$tr = $t->Fetch();
if ($tr['SYMBOLIC_NAME'] != 'LIBRARY_' . $arLibrary['ID'])
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if ($RESULT_TEMPLATE == "") $RESULT_TEMPLATE = $zr["RESULT_TEMPLATE"];
/********************************************************************
 * Form
 ********************************************************************/
$APPLICATION->SetTitle(str_replace("#ID#", "$VOTE_ID", GetMessage("VOTE_PAGE_TITLE")));
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = array(
    array(
        "TEXT" => GetMessage("VOTE_VOTE_LIST"),
        "ICON" => "btn_list",
        "LINK" => "/bitrix/admin/nota_vote_list.php?lang=" . LANGUAGE_ID
    )
);

$aMenu[] = array("SEPARATOR" => "Y");

$aMenu[] = array(
    "TEXT" => GetMessage("VOTE_QUESTIONS") . " [" . $zr["QUESTIONS"] . "]",
    "LINK" => "/bitrix/admin/nota_vote_question_list.php?lang=" . LANGUAGE_ID . "&VOTE_ID=" . $VOTE_ID,
    "ICON" => "btn_list",
    "TEXT_PARAM" => " [<a title='" . GetMessage("VOTE_QUESTIONS_ADD") . "' class=submenutext href='/bitrix/admin/nota_vote_question_edit.php?lang=" . LANGUAGE_ID . "&VOTE_ID=" . $VOTE_ID . "'>+</a>]",
);


echo ShowError($strError);

$context = new CAdminContextMenu($aMenu);
$context->Show();


$APPLICATION->IncludeComponent("bitrix:voting.result", "with_description", array(
        "VOTE_ID" => $VOTE_ID,
        "CACHE_TYPE" => "N",
        "VOTE_ALL_RESULTS" => 'Y'
    )
);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
