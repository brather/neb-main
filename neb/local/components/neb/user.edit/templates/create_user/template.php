<?
/**
 * profile/readers/new страница создания пользователя
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

/*	пользователь зарегистрирован */
if (!empty($arResult['VALUES']['USER_ID'])) {
    ?>
    <div class="b-useraddstep">
        <a href="<?= $APPLICATION->GetCurPage(); ?>" class="b-adduserimg right"><img
                src="<?= MARKUP ?>i/adduser.png" alt=""></a>

        <h2 class="mode"><?= GetMessage('LIB_NEW_USER_ADD_STEP_2'); ?></h2>

        <form action="" class="b-form b-form_common b-readerform">
            <dl class="info-dl">
                <dt><?= GetMessage('LIB_NEW_USER_AGREEMENT'); ?></dt>
                <dd>
                    <a href="<?= $APPLICATION->GetCurPage(
                    ); ?>?action=print&uid=<?= $arResult['VALUES']['USER_ID'] ?>"
                       target="_blank" class="btn btn-default"><?= GetMessage(
                            'LIB_NEW_USER_PRINT_AGREEMENT'
                        ); ?></a>
                    <a href="<?= $APPLICATION->GetCurPage(
                    ); ?>?action=save&uid=<?= $arResult['VALUES']['USER_ID'] ?>"
                       target="_blank" class="btn btn-link"><?= GetMessage(
                            'LIB_NEW_USER_SAVE_AGREEMENT'
                        ); ?></a>
                </dd>

                <dt><?= GetMessage('LIB_NEW_USER_LIBRARY_PASS'); ?></dt>
                <dd><?= $arResult['VALUES']['UF_NUM_ECHB'] ?></dd>

                <dt><span class="iblock"><?= GetMessage(
                        'LIB_NEW_USER_AGREEMENT_TEXT'
                    ); ?></dt>
                <dd>
		            <textarea readonly rows="30" class="form-control">
		                <? $APPLICATION->IncludeFile(
                            $this->__folder . '/agreement.php', Array(),
                            Array('SHOW_BORDER' => false, 'MODE' => 'php')
                        ) ?>
		            </textarea>
                </dd>
            </dl>
        </form>
    </div>
    <?
    return false;
}

if (count($arResult["ERRORS"]) > 0) {
    foreach ($arResult["ERRORS"] as $key => $error) {
        if (intval($key) == 0 && $key !== 0) {
            $arResult["ERRORS"][$key] = str_replace(
                "#FIELD_NAME#",
                "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;",
                $error
            );
        }
    }

    ShowError(implode("<br />", $arResult["ERRORS"]));
}
?>

<? /*script type="text/javascript">
	function setLogin(){
		$('#inpLOGIN').val($('#settings04').val());
		$('input#inpPERSONAL_NOTES').val($('input#settings05').val());
	}
</script*/ ?>

<form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform"
      enctype="multipart/form-data" class="new-reader-form nrf">
    <input type="hidden" name="form-type" value="register.form" />
    <script>
        var rulesAndMessages = {
            rules: {},
            messages: {}
        };
    </script>
    <?php foreach ($arParams['UF_LIBRARIES'] as $libRararyId) { ?>
        <input type="hidden" name="UF_LIBRARIES[]"
               value="<?php echo $libRararyId ?>"/>
    <? } ?>
    <input type="hidden" name="UF_LIBRARY"
           value="<?= $arParams['LIBRARY_ID'] ?>"/>
    <input type="hidden" name="ID"
           value="<?= $arResult['VALUES']['ID'] ?>"/>

    <!-- input type="hidden" name="REGISTER[LOGIN]" value="" id="inpLOGIN"/-->
    <input type="hidden" name="REGISTER[PERSONAL_NOTES]" value=""
           id="inpPERSONAL_NOTES"/>
    <?
    if (empty($arResult["VALUES"]['PASSWORD'])) {
        $arResult["VALUES"]['PASSWORD'] = randString(7);
    }
    ?>

    <div class="nrf-fieldset-title"><?= GetMessage(
            'LIB_NEW_USER_ENTER_SITE'
        ); ?></div>

    <div class="form-group">
        <label for="useremail">
            <em class="hint">*</em>
            <?= GetMessage('LIB_NEW_USER_EMAIL'); ?>
        </label>
        <input
            type="email"
            class="form-control"
            name="REGISTER[EMAIL]"
            id="useremail"
            value="<?= $arResult["VALUES"]['EMAIL'] ?>"
            required>
        <script>
            rulesAndMessages.rules["REGISTER[EMAIL]"] = {
                required: true,
                email: true,
                maxlength: 50,
                minlength: 3
            };
            rulesAndMessages.messages["REGISTER[EMAIL]"] = {
                required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                email: "<?=Loc::getMessage('LIB_NEW_USER_INCORRECT_EMAIL');?>",
                maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
            };
        </script>
    </div>
    <div class="form-group">
        <label for="userpassword">
            <em class="hint">*</em>
            <?= GetMessage('LIB_NEW_USER_PASSWORD_PROTECTION'); ?>
            (<?= GetMessage('LIB_NEW_USER_PASSWORD_MIN'); ?>)
        </label>
        <input
            type="password"
            class="form-control"
            value=""
            id="userpassword"
            name="REGISTER[PASSWORD]">
        <script>
            rulesAndMessages.rules["REGISTER[PASSWORD]"] = {
                required: true,
                maxlength: 30,
                minlength: 6
            };
            rulesAndMessages.messages["REGISTER[PASSWORD]"] = {
                required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
            };
        </script>
    </div>
    <div class="form-group">
        <label for="userconfirmpassword">
            <em class="hint">*</em>
            <?= GetMessage('REGISTER_FIELD_CONFIRM_PASSWORD'); ?>
        </label>
        <input
            class="form-control"
            type="password"
            value=""
            id="userconfirmpassword"
            name="REGISTER[CONFIRM_PASSWORD]"
            required><!-- pattern="[A-Za-z0-9]{6,}"  -->
        <script>
            rulesAndMessages.rules["REGISTER[CONFIRM_PASSWORD]"] = {
                required: true,
                maxlength: 30,
                minlength: 6,
                equalTo: "#userpassword"
            };
            rulesAndMessages.messages["REGISTER[CONFIRM_PASSWORD]"] = {
                required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>",
                equalTo: "<?=GetMessage('LIB_NEW_USER_PASSWORD_MATCH');?>"
            };
        </script>
    </div>

    <div id="full-reg-form" style="display: none;">
        <!-- full registration form begin  -->
        <div class="form-group">
            <label for="phonenumber">
                <?= GetMessage('LIB_NEW_USER_MOBILE'); ?>
            </label>
            <input type="text" class="form-control" name="REGISTER[PERSONAL_MOBILE]"
                   id="phonenumber"
                   data-masked
                   value="<?= $arResult["VALUES"]['PERSONAL_MOBILE'] ?>">
            <label class="phone_note"><?= GetMessage(
                    'LIB_NEW_USER_SMS_NOTIFICATIONS'
                ); ?></label>
        </div>

        <div class="nrf-fieldset-title"><?= GetMessage(
                'LIB_NEW_USER_PASSPORT_DATA'
            ); ?></div>
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="surname">
                    <em class="hint">*</em>
                    <?= GetMessage('REGISTER_FIELD_LAST_NAME'); ?>
                </label>
                <input
                    type="text"
                    class="form-control"
                    value="<?= $arResult["VALUES"]['LAST_NAME'] ?>"
                    id="surname"
                    name="REGISTER[LAST_NAME]"
                    required>
                <script>
                    rulesAndMessages.rules["REGISTER[LAST_NAME]"] = {
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["REGISTER[LAST_NAME]"] = {
                        required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                        maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                        minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>
            <div class="col-md-4 form-group">
                <label for="name">
                    <em class="hint">*</em>
                    <?= GetMessage('REGISTER_FIELD_NAME'); ?>
                </label>
                <input
                    type="text"
                    class="form-control"
                    value="<?= $arResult["VALUES"]['NAME'] ?>"
                    id="name"
                    name="REGISTER[NAME]"
                    required>
                <script>
                    rulesAndMessages.rules["REGISTER[NAME]"] = {
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["REGISTER[NAME]"] = {
                        required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                        maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                        minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>
            <div class="col-md-4 form-group">
                <label for="middlename">
                    <?= GetMessage('REGISTER_FIELD_SECOND_NAME'); ?>
                </label>
                <input
                    type="text"
                    class="form-control"
                    value="<?= $arResult["VALUES"]['SECOND_NAME'] ?>"
                    id="middlename"
                    name="REGISTER[SECOND_NAME]">
                <script>
                    rulesAndMessages.rules["REGISTER[SECOND_NAME]"] = {
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["REGISTER[SECOND_NAME]"] = {
                        maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                        minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>
        </div>
        <div class="row">
            <label for="usergender" class="col-xs-12">
                <em class="hint">*</em>
                <?= GetMessage('REGISTER_FIELD_PERSONAL_GENDER'); ?>
            </label>
            <? $GENDERS = array(
                'M' => GetMessage('USER_MALE'),
                'F' => GetMessage('USER_FEMALE'),
            );
            ?>
            <div class="col-md-4 form-group">
                <select name="REGISTER[PERSONAL_GENDER]" id="usergender"
                        class="form-control">
                    <?php
                    foreach ($GENDERS as $key => $value) {
                        echo '<option '
                            . (isset($arResult["VALUES"]['PERSONAL_GENDER'])
                            && $arResult["VALUES"]['PERSONAL_GENDER'] == $key
                                ? 'selected' : '') . ' value="' . $key . '">'
                            . $value . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label>
                    <em class="hint">*</em>
                    <?= GetMessage('REGISTER_FIELD_BIRTH_DATE'); ?>
                </label>

                <div class="input-group">
                    <input
                        type="text"
                        class="form-control"
                        data-required="true"
                        value="<?= $arResult["VALUES"]['PERSONAL_BIRTHDAY'] ?>"
                        id="userbirthday"
                        name="REGISTER[PERSONAL_BIRTHDAY]"
                        data-yearsrestrict="12"
                        >
                    <span class="input-group-btn">
                        <a class="btn btn-default"
                           id="calendarlabel"
                           onclick="BX.calendar({node: 'userbirthday', field: 'userbirthday',  form: '', bTime: false, value: ''});"
                            >
                            <span class="glyphicon glyphicon-calendar"></span>
                        </a>
                    </span>
                </div>
            </div>
        </div>
        <script>
            rulesAndMessages.rules["REGISTER[PERSONAL_BIRTHDAY]"] = {
                required: true,
                checkDateFormat: true,
                checkDateCorrect: true,
                checkAge: 12,
            };
            rulesAndMessages.messages["REGISTER[PERSONAL_BIRTHDAY]"] = {
                required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                checkDateFormat: "<?=Loc::getMessage('LIB_NEW_USER_DOB_FORMAT');?>",
                checkDateCorrect: "<?=Loc::getMessage('LIB_NEW_USER_DOB_INCORRECT');?>",
                checkAge: "<?=Loc::getMessage('LIB_NEW_USER_AGE_WITH_PLACEHOLDER');?>",
            };
        </script>
        <? /*?>
		<div class="fieldrow nowrap">
		<div class="fieldcell iblock mt10">
		<em class="hint">*</em>
		<label for="settings12">Пол</label>
		<div class="field validate">
		<span class="b-radio">
		<input type="radio" name="REGISTER[PERSONAL_GENDER]" class="radio" id="rb1" value="M"<?=($arResult["VALUES"]['PERSONAL_GENDER'] == "M" or empty($arResult["VALUES"]['PERSONAL_GENDER'])) ? " checked=\"checked\"" : ""?>>
		<label for="rb1">мужской</label>
		</span>
		<span class="b-radio">
		<input type="radio" name="REGISTER[PERSONAL_GENDER]" class="radio" id="rb2" value="F"<?=$arResult["VALUES"]['PERSONAL_GENDER'] == "F" ? " checked=\"checked\"" : ""?>>
		<label for="rb2">женский</label>
		</span>
		</div>
		</div>
		</div>

		<div class="fieldrow nowrap">
		<div class="fieldcell iblock">
		<em class="hint">*</em>
		<label for="settings04">Серия и номер паспорта</label>
		<div class="field validate">
		<input type="text" class="input tbpasport" name="UF_PASSPORT_NUMBER" data-minlength="2" data-maxlength="30" data-required="true" id="settings04" value="<?=htmlspecialcharsEx($_REQUEST['UF_PASSPORT_NUMBER'])?>">
		<em class="error required">Поле обязательно для заполнения</em>
		<em class="error validate ">Поле заполнено неверно</em>
		</div>
		</div>
		</div>
	<?*/ ?>
        <div class="row">
            <div class="col-xs-6 form-group">
                <label for="passserial">
                    <em class="hint">*</em>
                    <?= GetMessage('LIB_NEW_USER_PASSPORT_NUMBER'); ?>
                </label>
                <input type="text"
                       data-required="required"
                       value="<?= $arResult["VALUES"]["UF_PASSPORT_SERIES"] ?>"
                       id="passserial"
                       name="REGISTER[UF_PASSPORT_SERIES]"
                       class="form-control"
                       required>
                <script>
                    rulesAndMessages.rules["REGISTER[UF_PASSPORT_SERIES]"] = {
                        required: true
                    };
                    rulesAndMessages.messages["REGISTER[UF_PASSPORT_SERIES]"] = {
                        required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>"
                    };
                </script>
            </div>
            <div class="col-xs-6 form-group">
                <label>&nbsp;</label>
                <input
                    type="text"
                    data-required="required"
                    value="<?= $arResult["VALUES"]["UF_PASSPORT_NUMBER"] ?>"
                    id="passnumber"
                    name="REGISTER[UF_PASSPORT_NUMBER]"
                    class="form-control"
                    required>
                <script>
                    rulesAndMessages.rules["REGISTER[UF_PASSPORT_NUMBER]"] = {
                        required: true
                    };
                    rulesAndMessages.messages["REGISTER[UF_PASSPORT_NUMBER]"] = {
                        required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>"
                    };
                </script>
            </div>
        </div>

        <div class="form-group">
            <label for="citizenship">
                <em class="hint">*</em>
                <?= GetMessage('LIB_NEW_USER_CITITZENSHIP'); ?>
            </label>
            <select name="REGISTER[UF_CITIZENSHIP]" id="citizenship"
                    class="form-control">
                <?
                $arUF_CITIZENSHIP = nebUser::getFieldEnum(
                    array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'),
                    array("VALUE" => "ASC")
                );
                if (!empty($arUF_CITIZENSHIP)) {
                    $last = count($arUF_CITIZENSHIP);
                    foreach ($arUF_CITIZENSHIP as $arItem) {
                        if ($arItem['XML_ID'] == $last) {
                            $other = $arItem;
                            continue;
                        }
                        if ($arItem['DEF'] == "Y") {
                            $ruCitizenship = $arItem['ID'];
                        }
                        ?>
                        <option value="<?= $arItem['ID'] ?>"<?= ($arItem['DEF']
                            == "Y") ? ' selected="selected"'
                            : ''; ?>><?= $arItem['VALUE'] ?></option>
                        <?php
                    }
                    ?>
                    <option value="<?= $other['ID'] ?>"<?= ($other['DEF']
                        == "Y") ? ' selected="selected"'
                        : ''; ?>><?= $other['VALUE'] ?></option>

                    <?
                }
                ?>
            </select>

            <script>
                rulesAndMessages.rules["REGISTER[UF_CITIZENSHIP]"] = {
                    required: true
                };
                rulesAndMessages.messages["REGISTER[UF_CITIZENSHIP]"] = {
                    required: "<?=Loc::getMessage('LIB_NEW_USER_REQ');?>"
                };
            </script>

            <? /*script type="text/javascript">
				$(function(){
					var copyEducation = $('select[name="UF_EDUCATION"]').clone().removeClass("custom");
					function correctEducationField()
					{
						var cit = $('select[name="UF_CITIZENSHIP"] option:selected').val();
						var parentEducation = $('select[name="UF_EDUCATION"]').closest("div.field");
						parentEducation.children("span").remove();
						parentEducation.append(copyEducation.clone());
						$('#UF_EDUCATIONopts').remove();
						if(cit == <?php echo $ruCitizenship;?>)
						{
							$('select[name="UF_EDUCATION"] option').filter('[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
						}
						else
						{
							$('select[name="UF_EDUCATION"] option').not('[VALUE=""],[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
						}
						$('select[name="UF_EDUCATION"]').selectReplace();
					}
					correctEducationField();
					$('select[name="UF_CITIZENSHIP"]').change(function(){
						correctEducationField();
					});
				});
			</script*/ ?>
        </div>

        <? /*?>
		<div class="fieldrow nowrap">
		<div class="fieldcell iblock mt10 w700">
		<em class="hint">*</em>
		<label for="settings05">Кем выдан / как в паспорте</label>
		<div class="field validate">
		<input type="text" class="input" name="UF_ISSUED" data-minlength="2" data-maxlength="30" data-required="true"  id="settings05" value="<?=htmlspecialcharsEx($_REQUEST['UF_ISSUED'])?>">
		<em class="error required">Поле обязательно для заполнения</em>
		<em class="error validate ">Поле заполнено неверно</em>
		</div>
		</div>
		</div>
	<?*/ ?>

        <div class="nrf-fieldset-title"><?= GetMessage(
                'LIB_NEW_USER_SPECIALIZATION'
            ); ?></div>

        <div class="form-group">
            <label for="jobplace">
                <em class="hint">*</em>
                <?= GetMessage('LIB_NEW_USER_JOB_PLACE'); ?>
            </label>
            <em class="error required"><?= GetMessage(
                    'LIB_NEW_USER_FIELD_FILL'
                ); ?></em>
            <input
                type="text"
                class="form-control"
                value="<?= $arResult["VALUES"]['WORK_COMPANY'] ?>"
                id="jobplace"
                name="REGISTER[WORK_COMPANY]"
                required>
            <script>
                rulesAndMessages.rules["REGISTER[WORK_COMPANY]"] = {
                    required: true,
                    maxlength: 30,
                    minlength: 2
                };
                rulesAndMessages.messages["REGISTER[WORK_COMPANY]"] = {
                    required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                    maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                    minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
                };
            </script>
        </div>
        <div class="form-group">
            <label for="branchknowlege">
                <em class="hint">*</em>
                <?= GetMessage('LIB_NEW_USER_BRANCH_KNOWLEDGE'); ?>
            </label>
            <select name="REGISTER[UF_BRANCH_KNOWLEDGE]" id="branchknowlege"
                    class="form-control" data-required="true" required>
                <option value=""><?= GetMessage(
                        'LIB_NEW_USER_CHOOSE'
                    ); ?></option>
                <?
                $arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(
                    array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE')
                );
                if (!empty($arUF_BRANCH_KNOWLEDGE)) {
                    foreach ($arUF_BRANCH_KNOWLEDGE as $arItem) {
                        ?>
                        <option value="<?= $arItem['ID'] ?>"<?= $arItem['ID']
                        == $arResult["VALUES"]["UF_BRANCH_KNOWLEDGE"]
                            ? "selected"
                            : '' ?>><?= $arItem['VALUE'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
            <script>
                rulesAndMessages.rules["REGISTER[UF_BRANCH_KNOWLEDGE]"] = {
                    required: true
                };
                rulesAndMessages.messages["REGISTER[UF_BRANCH_KNOWLEDGE]"] = {
                    required: "<?=Loc::getMessage('LIB_NEW_USER_REQ');?>"
                };
            </script>
        </div>
        <div class="form-group">
            <label for="education">
                <?= GetMessage('LIB_NEW_USER_EDUCATION'); ?>
            </label>
            <select name="REGISTER[UF_EDUCATION]" id="education"
                    class="form-control" data-required="true" required>
                <option value=""><?= GetMessage(
                        'LIB_NEW_USER_CHOOSE'
                    ); ?></option>
                <?
                $arUF_EDUCATION = nebUser::getFieldEnum(
                    array('USER_FIELD_NAME' => 'UF_EDUCATION')
                );
                if (!empty($arUF_EDUCATION)) {
                    foreach ($arUF_EDUCATION as $arItem) {
                        ?>
                        <option <?= $arResult["VALUES"]['UF_EDUCATION']
                        == $arItem['ID'] ? 'selected="selected"' : '' ?>
                            value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
            <script>
                rulesAndMessages.rules["REGISTER[UF_EDUCATION]"] = {
                    required: true
                };
                rulesAndMessages.messages["REGISTER[UF_EDUCATION]"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>"
                };
            </script>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="nrf-fieldset-title"><?= GetMessage(
                        'LIB_NEW_USER_REGISTER_PLACE'
                    ); ?></div>
                <div class="checkbox">&nbsp;</div>
                <div class="form-group">
                    <label for="userzip">
                        <em class="hint">*</em>
                        <?= GetMessage('REGISTER_FIELD_PERSONAL_ZIP'); ?>
                    </label>
                    <input
                        type="text"
                        class="form-control"
                        name="REGISTER[PERSONAL_ZIP]"
                        id="userzip"
                        data-mirrored-by="livezip"
                        value="<?= $arResult["VALUES"]['PERSONAL_ZIP'] ?>"
                        required>
                    <script>
                        rulesAndMessages.rules["REGISTER[PERSONAL_ZIP]"] = {
                            required: true,
                            number: true
                        };
                        rulesAndMessages.messages["REGISTER[PERSONAL_ZIP]"] = {
                            required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                            number: "<?=Loc::getMessage('LIB_NEW_USER_ZIP_DIGITS');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="userregregion">
                        <!--em class="hint">*</em-->
                        <?= Loc::getMessage('MAIN_REGISTER_REGION'); ?>
                        <span class="minscreen"><?= Loc::getMessage(
                                'REGISTER_GROUP_ADDRESS_REG_NOTE'
                            ); ?></span>
                    </label>
                    <input
                        type="text"
                        value="<?= $arResult["VALUES"]['UF_REGION'] ?>"
                        id="userregregion"
                        data-mirrored-by="liveregregion"
                        name="REGISTER[UF_REGION]"
                        class="form-control">
                    <script>
                        rulesAndMessages.rules["REGISTER[UF_REGION]"] = {
                            required: false,
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["REGISTER[UF_REGION]"] = {
                            required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="userareareg">
                        <?= Loc::getMessage('MAIN_REGISTER_AREA'); ?>
                        <span class="minscreen"><?= Loc::getMessage(
                                'REGISTER_GROUP_ADDRESS_REG_NOTE'
                            ); ?></span>
                    </label>
                    <input
                        type="text"
                        value="<?= $arResult["VALUES"]['UF_AREA'] ?>"
                        id="userareareg"
                        data-mirrored-by="liveareareg"
                        name="REGISTER[UF_AREA]"
                        class="form-control">
                    <script>
                        rulesAndMessages.rules["REGISTER[UF_AREA]"] = {
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["REGISTER[UF_AREA]"] = {
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="usercity">
                        <em class="hint">*</em>
                        <?= GetMessage('REGISTER_FIELD_PERSONAL_CITY'); ?>
                    </label>
                    <input
                        type="text"
                        class="form-control"
                        name="REGISTER[PERSONAL_CITY]"
                        id="usercity"
                        data-mirrored-by="livecity"
                        value="<?= $arResult["VALUES"]['PERSONAL_CITY'] ?>"
                        required>
                    <script>
                        rulesAndMessages.rules["REGISTER[PERSONAL_CITY]"] = {
                            required: true,
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["REGISTER[PERSONAL_CITY]"] = {
                            required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                            maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>
                <div class="form-group">
                    <label for="userstreet">
                        <em class="hint">*</em>
                        <?= GetMessage('LIB_NEW_USER_STREET'); ?>
                    </label>
                    <input
                        type="text"
                        class="form-control"
                        name="REGISTER[PERSONAL_STREET]"
                        id="userstreet"
                        data-mirrored-by="livestreet"
                        value="<?= $arResult["VALUES"]['PERSONAL_STREET'] ?>"
                        required>
                    <script>
                        rulesAndMessages.rules["REGISTER[PERSONAL_STREET]"] = {
                            required: true,
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["REGISTER[PERSONAL_STREET]"] = {
                            required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                            maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>
                <div class="row">
                    <div class="col-xs-4 form-group">
                        <label for="userhouse" class="col-xs-12"
                               style="padding-left: 0;">
                            <em class="hint">*</em>
                            <?= GetMessage('LIB_NEW_USER_HOUSE'); ?>
                        </label>
                        <input
                            type="text"
                            class="form-control"
                            name="REGISTER[UF_CORPUS]"
                            id="userhouse"
                            data-mirrored-by="livehouse"
                            value="<?= $arResult["VALUES"]["UF_CORPUS"] ?>"
                            required>
                        <script>
                            rulesAndMessages.rules["REGISTER[UF_CORPUS]"] = {
                                required: true,
                                maxlength: 6
                            };
                            rulesAndMessages.messages["REGISTER[UF_CORPUS]"] = {
                                required: "<?=Loc::getMessage('LIB_NEW_USER_FIELD_FILL');?>",
                                maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>
                    <div class="col-xs-4 form-group">
                        <label for="userbuilding" class="col-xs-12"
                               style="padding-left: 0;"><?= GetMessage(
                                'LIB_NEW_USER_BUILDING'
                            ); ?></label>
                        <input
                            type="text"
                            class="form-control"
                            name="REGISTER[UF_STRUCTURE]"
                            id="userbuilding"
                            data-mirrored-by="livebuilding"
                            value="<?= $arResult["VALUES"]["UF_STRUCTURE"] ?>">
                        <script>
                            rulesAndMessages.rules["REGISTER[UF_STRUCTURE]"] = {
                                maxlength: 4
                            };
                            rulesAndMessages.messages["REGISTER[UF_STRUCTURE]"] = {
                                maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>
                    <div class="col-xs-4 form-group">
                        <label for="userappartment" class="col-xs-12"
                               style="padding-left: 0;"><?= GetMessage(
                                'LIB_NEW_USER_APPARTMENT'
                            ); ?></label>
                        <input
                            type="text"
                            class="form-control"
                            name="REGISTER[UF_FLAT]"
                            id="userappartment"
                            data-mirrored-by="liveappartment"
                            value="<?= $arResult["VALUES"]["UF_FLAT"] ?>">
                        <script>
                            rulesAndMessages.rules["REGISTER[UF_FLAT]"] = {
                                maxlength: 4
                            };
                            rulesAndMessages.messages["REGISTER[UF_FLAT]"] = {
                                maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="nrf-fieldset-title"><?= GetMessage(
                        'LIB_NEW_USER_LIVING_PLACE'
                    ); ?></div>

                <div class="form-group">
                    <label class="checkbox no-user-select">
                        <input
                            class="checkbox"
                            type="checkbox"
                            data-checkbox-for="REGISTER[UF_PLACE_REGISTR]"
                            <?= $arResult["VALUES"]["UF_PLACE_REGISTR"] ? 'checked' : '' ?>
                            id="livwhereregistered"
                            >
                        <span class="lbl"><?= GetMessage('LIB_NEW_USER_UF_REGISTER_PLACE'); ?></span>
                    </label>
                    <input
                        type="hidden"
                        name="REGISTER[UF_PLACE_REGISTR]"
                        value="<?=$arResult["VALUES"]["UF_PLACE_REGISTR"]?>"
                    >
                </div>

                <div class="living-address-group">

                    <div class="form-group">
                        <label for="livezip">
                            <em class="hint">*</em>
                            <?= GetMessage('REGISTER_FIELD_WORK_ZIP'); ?>
                        </label>
                        <input
                            type="text"
                            class="form-control"
                            name="REGISTER[WORK_ZIP]"
                            id="livezip"
                            data-mirror="userzip"
                            value="<?= $arResult["VALUES"]['WORK_ZIP'] ?>">
                        <script>
                            rulesAndMessages.rules["REGISTER[WORK_ZIP]"] = {
                                number: true
                            };
                            rulesAndMessages.messages["REGISTER[WORK_ZIP]"] = {
                                number: "<?=Loc::getMessage('LIB_NEW_USER_ZIP_DIGITS');?>"
                            };
                        </script>
                    </div>

                    <div class="form-group">
                        <label for="userregregion">
                            <!--em class="hint">*</em-->
                            <?= Loc::getMessage('MAIN_REGISTER_REGION'); ?>
                            <span class="minscreen"><?= Loc::getMessage(
                                    'REGISTER_GROUP_ADDRESS_REG_NOTE'
                                ); ?></span>
                        </label>
                        <input
                            type="text"
                            data-mirror="userregregion"
                            value="<?= $arResult["VALUES"]['UF_REGION2'] ?>"
                            id="liveregregion"
                            name="REGISTER[UF_REGION2]"
                            class="form-control">
                        <script>
                            rulesAndMessages.rules["REGISTER[UF_REGION2]"] = {
                                required: false,
                                maxlength: 30,
                                minlength: 2
                            };
                            rulesAndMessages.messages["REGISTER[UF_REGION2]"] = {
                                required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                                maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                                minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>

                    <div class="form-group">
                        <label for="userareareg">
                            <?= Loc::getMessage('MAIN_REGISTER_AREA'); ?>
                            <span class="minscreen"><?= Loc::getMessage(
                                    'REGISTER_GROUP_ADDRESS_REG_NOTE'
                                ); ?></span>
                        </label>
                        <input
                            type="text"
                            value="<?= $arResult["VALUES"]['UF_AREA2'] ?>"
                            id="liveareareg"
                            data-mirror="userareareg"
                            name="REGISTER[UF_AREA2]"
                            class="form-control">
                        <script>
                            rulesAndMessages.rules["REGISTER[UF_AREA2]"] = {
                                maxlength: 30,
                                minlength: 2
                            };
                            rulesAndMessages.messages["REGISTER[UF_AREA2]"] = {
                                maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                                minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>

                    <div class="form-group">
                        <label for="livecity">
                            <em class="hint">*</em>
                            <?= GetMessage('REGISTER_FIELD_WORK_CITY'); ?>
                        </label>
                        <input
                            type="text"
                            class="form-control"
                            name="REGISTER[WORK_CITY]"
                            id="livecity"
                            data-mirror="usercity"
                            value="<?= $arResult["VALUES"]['WORK_CITY'] ?>">
                        <script>
                            rulesAndMessages.rules["REGISTER[WORK_CITY]"] = {
                                maxlength: 30,
                                minlength: 2
                            };
                            rulesAndMessages.messages["REGISTER[WORK_CITY]"] = {
                                maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                                minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="livestreet">
                            <em class="hint">*</em>
                            <?= GetMessage('LIB_NEW_USER_STREET'); ?>
                        </label>
                        <input
                            type="text"
                            class="form-control"
                            name="REGISTER[WORK_STREET]"
                            id="livestreet"
                            data-mirror="userstreet"
                            value="<?= $arResult["VALUES"]['WORK_STREET'] ?>">
                        <script>
                            rulesAndMessages.rules["REGISTER[WORK_STREET]"] = {
                                maxlength: 30,
                                minlength: 2
                            };
                            rulesAndMessages.messages["REGISTER[WORK_STREET]"] = {
                                maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>",
                                minlength: "<?=Loc::getMessage('LIB_NEW_USER_LESS_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 form-group">
                            <label for="livehouse">
                                <em class="hint">*</em>
                                <?= GetMessage('LIB_NEW_USER_HOUSE'); ?>
                            </label>
                            <input
                                type="text"
                                class="form-control"
                                name="REGISTER[UF_HOUSE2]"
                                id="livehouse"
                                data-mirror="userhouse"
                                value="<?= $arResult["VALUES"]['UF_HOUSE2'] ?>">
                            <script>
                                rulesAndMessages.rules["REGISTER[UF_HOUSE2]"] = {
                                    maxlength: 6
                                };
                                rulesAndMessages.messages["REGISTER[UF_HOUSE2]"] = {
                                    maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>"
                                };
                            </script>
                            <em class="error required"><?= GetMessage(
                                    'LIB_NEW_USER_FIELD_FILL'
                                ); ?></em>
                        </div>
                        <div class="col-xs-4 form-group">
                            <label for="livebuilding"><?= GetMessage(
                                    'LIB_NEW_USER_BUILDING'
                                ); ?></label>
                            <input
                                type="text"
                                class="form-control"
                                name="REGISTER[UF_STRUCTURE2]"
                                id="livebuilding"
                                data-mirror="userbuilding"
                                value="<?= $arResult["VALUES"]['UF_STRUCTURE2'] ?>">
                            <script>
                                rulesAndMessages.rules["REGISTER[UF_STRUCTURE2]"] = {
                                    maxlength: 4
                                };
                                rulesAndMessages.messages["REGISTER[UF_STRUCTURE2]"] = {
                                    maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>"
                                };
                            </script>
                        </div>
                        <div class="col-xs-4 form-group">
                            <label for="liveappartment"><?= GetMessage(
                                    'LIB_NEW_USER_APPARTMENT'
                                ); ?></label>
                            <input
                                type="text"
                                class="form-control"
                                name="REGISTER[UF_FLAT2]"
                                id="liveappartment"
                                data-mirror="userappartment"
                                value="<?= $arResult["VALUES"]['UF_FLAT2'] ?>">
                            <script>
                                rulesAndMessages.rules["REGISTER[UF_FLAT2]"] = {
                                    maxlength: 4
                                };
                                rulesAndMessages.messages["REGISTER[UF_FLAT2]"] = {
                                    maxlength: "<?=Loc::getMessage('LIB_NEW_USER_MORE_PLACEHOLDER_SYMBOLS');?>"
                                };
                            </script>
                        </div>
                    </div>
                </div>
                <!-- /.living-address-group -->
            </div>
            <!-- /.col-sm-6 -->
        </div>
        <!-- ВКЛЮЧИТЬ АПЛОАДИЛКУ -->
        <?
        /*$sImage = (intval($arResult['arUser']["UF_SCAN_PASSPORT1"]) > 0)? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT1"]):'';
        if ($arResult['arUser']["UF_SCAN_PASSPORT1"]["SRC"]) {
            $sImage = $arResult['arUser']["UF_SCAN_PASSPORT1"]["SRC"];
        }*/
        ?>
        <? $APPLICATION->IncludeComponent(
            "notaext:plupload",
            "scan_passport1_reg",
            array(
                "MAX_FILE_SIZE"     => "24",
                "FILE_TYPES"        => "jpg,jpeg,png",
                "DIR"               => "tmp_register",
                "FILES_FIELD_NAME"  => "UF_SCAN_PASSPORT1",
                "MULTI_SELECTION"   => "N",
                "CLEANUP_DIR"       => "Y",
                "UPLOAD_AUTO_START" => "Y",
                "RESIZE_IMAGES"     => "N",
                "RESIZE_WIDTH"      => "0",
                "RESIZE_HEIGHT"     => "0",
                "RESIZE_CROP"       => "N",
                "RESIZE_QUALITY"    => "98",
                "UNIQUE_NAMES"      => "Y",
            ),
            false
        ); ?>

        <script>
            rulesAndMessages.rules['scan1'] = {
                required: true
            };
            /* rulesAndMessages.messages['scan1'] = {
                 required: '<?=GetMessage("ERROR_UPLOAD_SCAN");?>'
            };*/
        </script>

        <?
        $APPLICATION->IncludeComponent(
            "notaext:plupload",
            "scan_passport2_reg",
            array(
                "MAX_FILE_SIZE"     => "24",
                "FILE_TYPES"        => "jpg,jpeg,png",
                "DIR"               => "tmp_register",
                "FILES_FIELD_NAME"  => "UF_SCAN_PASSPORT2",
                "MULTI_SELECTION"   => "N",
                "CLEANUP_DIR"       => "Y",
                "UPLOAD_AUTO_START" => "Y",
                "RESIZE_IMAGES"     => "N",
                "RESIZE_WIDTH"      => "0",
                "RESIZE_HEIGHT"     => "0",
                "RESIZE_CROP"       => "N",
                "RESIZE_QUALITY"    => "98",
                "UNIQUE_NAMES"      => "Y",
                /*"PERSONAL_PHOTO" => $sImage,
                "FULL_NAME" => $arResult['arUser']['NAME'].' '.$arResult['arUser']['LAST_NAME'],
                "USER_EMAIL" => $arResult['arUser']['EMAIL'],
                "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
                "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
                "USER_ID" => $arParams["USER_ID"],*/
            ),
            false
        );
        ?>
        <script>
            rulesAndMessages.rules['scan2'] = {
                required: true
            };
            /* rulesAndMessages.messages['scan2'] = {
                 required: '<?=GetMessage("ERROR_UPLOAD_SCAN_WITH_RESIDENCE");?>'
            };*/
        </script>

    </div>
    <!-- full registration form end  -->

    <hr>

    <div class="form-group">
        <label>
            <input type="checkbox"
                   name="USER_VERIFY"
                   id="cb-verify"
                <?= isset($_REQUEST['USER_VERIFY'])
                && $_REQUEST['USER_VERIFY'] == 'Y' ? "checked" : null ?>
                   value="Y">
            <span class="lbl"><?= Loc::getMessage(
                    'REGISTER_FIELD_VERIFY'
                ); ?></span>
        </label>
    </div>

    <div>
        <button class="btn btn-primary" value="save" type="submit"
                name="action"><?= GetMessage(
                'LIB_NEW_USER_SEND_DATA'
            ); ?></button>
        <?= GetMessage('LIB_NEW_USER_REGISTRY_ELP'); ?>
    </div>

    <script>
        $(function () {
            $('#full-reg-form').find('input, select, textarea, button').each(function () {
                this.disabled = true
            }); /*Disable all hidden elements*/
            $('#cb-verify').on('change', function (e) {
                if (this.checked) {
                    $('#full-reg-form').css({display: 'block'});
                    $('#full-reg-form').find('input, select, textarea, button').each(function () {
                        this.disabled = false
                    }); /*Enable all fields*/
                    $('#livwhereregistered').trigger('change'); /*Check for additional fields*/
                } else {
                    $('#full-reg-form').css({display: 'none'});
                    $('#full-reg-form').find('input, select, textarea, button').each(function () {
                        this.disabled = true
                    });
                }
            });
            $('#cb-verify').trigger('change');
            function getDateAge(dateObj) {
                var today = new Date();
                var born = dateObj,
                    age = Math.ceil((today.getTime() - born) / (24 * 3600 * 365.25 * 1000));
                return age;
            }

            function dateStringToObj(dateString) {
                var a = dateString.split('.'),
                    dateObj = new Date(a[2], a[1] - 1, a[0]);
                return dateObj;
            }

            jQuery.validator.addMethod("checkAge", function (value, element, params) {
                    var inputDateObjValue = dateStringToObj(value);
                    return ( getDateAge(inputDateObjValue) > Number(params) );
                }
            );
            jQuery.validator.addMethod("checkDateFormat", function (value, element, params) {
                    var xxx = value.match(/^\d\d?\.\d\d?\.\d\d\d\d$/);
                    return xxx != null;
                }
            );
            jQuery.validator.addMethod("checkDateCorrect", function (value, element, params) {
                    var dateObj = dateStringToObj(value),
                        dateObjToStrLeadingZero = ('0' + String(dateObj.getDate())).slice(-2) + '.'
                            + ('0' + String(dateObj.getMonth() + 1)).slice(-2) + '.'
                            + String(dateObj.getFullYear()),
                        dateObjToStr = String(dateObj.getDate()) + '.'
                            + String(dateObj.getMonth() + 1) + '.'
                            + String(dateObj.getFullYear());
                    if (value == dateObjToStr || value == dateObjToStrLeadingZero) {
                        return true
                    }
                }
            );

            $('form[name="regform"]').validate({
                rules: rulesAndMessages.rules,
                messages: rulesAndMessages.messages,
                ignore: '[type="hidden"]:not(.tovalidate)',
                errorPlacement: function (error, element) {
                    if (element.attr('type') == "radio") {
                        error.appendTo($(element).closest('.form-group').find('label')[0]);
                    } else if ( element.attr('type') == "hidden" ) {
                        error.appendTo( $(element).closest('.setscan') );
                        $('body').scrollTo('.setscan');
                    } else {
                        error.appendTo($(element).closest('.form-group'));
                    }
                    $(element).closest('.form-group').toggleClass('has-error', true);
                },
                /*specifying a submitHandler prevents the default submit, good for the demo
                submitHandler: function() {
                     alert("valid!");
                },
                set this class to error-labels to indicate valid fields*/
                success: function (label) {
                    /* set &nbsp; as text for IE
                    label.html('"&nbsp;"').addClass("resolved");*/
                    label.remove();
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                    $(element).parent().find("." + errorClass).removeClass("resolved");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
                }
            });

            $('#userbirthday').on('input change', function () {
                $('form[name="regform"]').validate().element(this);
            });

            /* $('[data-mirror]').each(function () {
                 var id = $(this).data('mirror'),
                     thisId = $(this).prop('id');
                 $('#' + id).attr('data-mirrored-by', thisId);
            });*/

            $(document).on('keyup change', '[data-mirrored-by]', function () {
                var id = $(this).data('mirrored-by'),
                    sameAddresses = $('#livwhereregistered').prop('checked');
                if (sameAddresses) {
                    $('#' + id).val($(this).val()).trigger('change');
                }
            });

            $(document).on('change', '#livwhereregistered', function () {
                var checked = $(this).prop('checked');
                if (checked) {
                    $('[data-mirror]').each(function () {
                        var id = $(this).data('mirror');
                        $(this).val($('#' + id).val());
                    });
                    $('.living-address-group').addClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function () {
                        $(this).attr('readonly', 'true');
                    });
                } else {
                    $('.living-address-group').removeClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function () {
                        $(this).removeAttr('readonly');
                    });
                }
            });
            $('#livwhereregistered').trigger('change');

            $(document).on('click', '[data-perpage-set]', function (e) {
                e.preventDefault();
                var toggler = $(this),
                    value = toggler.data('perpage-set'),
                    widget = toggler.closest('[data-perpage-widget]'),
                    input = widget.find('[name="UF_SEARCH_PAGE_COUNT"]');

                widget.find('[data-perpage-set]').toggleClass('current', false);
                toggler.toggleClass('current', true);
                $(input).val(value);
                /*console.log(value);*/
            });
        });
    </script>

</form>
