<?php
CBitrixComponent::includeComponentClass("rest:rest.server");

CModule::IncludeModule('nota.userdata');
CModule::IncludeModule('nota.exalead');
CModule::IncludeModule("highloadblock");

use \Bitrix\Main\Type\Date;
use \Bitrix\Main\Entity\ExpressionField;
use \Nota\Exalead\Stat\StatBookReadTable;

/**
 * Компонент организует API для получения данных о прочтении книг. Сортировка книг по количеству прочтений
 *  https://docs.google.com/document/d/1Nxj__eio7bvaMpDRfaEAzeJ1o-9JI4mu5VnzTESRVBo/edit#
 * Пункт 8.6
 *
 * Class RestBookViewTop
 */
class RestBookViewTop extends RestServer
{
	protected $token, $from, $to, $id, $top;

	public function processRequest($token = null, $from = null, $to = null, $id = null, $top = null)
	{
        $this->token = !empty($token) ? trim($token) : false;
        $this->from = !empty($from) ? trim($from) : false;
        $this->to = !empty($to) ? trim($to) : false;
        $this->id = !empty($id) ? $id : false;
        $this->top = !empty($top) ? trim($top) : false;

        if(!$this->token || strlen($this->token) <= 0)
            return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));
		
		$tokenUser = CUser::GetList(
			($by = "id"),
			($order = "desc"),
			array(
				'ACTIVE' => 'Y',
				'UF_TOKEN' => $this->token,
			),
			array('FIELDS' => array('ID'))
		)->Fetch();

        if(!$tokenUser)
            return $this->restError(404, GetMessage('USER_NOT_FOUND'));

        return $this->GETResult();
	}

	protected function GETResult()
	{
        $result = StatBookReadTable::getList(Array(
            'select' => Array('BOOK_NAME' => 'BOOK.Name', 'COUNT', 'LIB_NAME' => 'BOOK.LIB.Name'),
            'filter' => Array(
                'BOOK.ALIS' => $this->id,
                '>=X_TIMESTAMP' => new Date($this->from),
                '<=X_TIMESTAMP' => new Date($this->to),
            ),
            'order' => Array('COUNT' => 'DESC'),
            'limit' => $this->top,
            'runtime' => Array(
                new ExpressionField('COUNT', 'COUNT(DISTINCT %s)', Array('ID')),
            )
        ));

        while($res = $result->Fetch()){
            $return[] = $res;
        }

        $this->arResult = $return;
        return 200;
	}
}