<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? $APPLICATION->SetTitle(""); ?>

<div class="container" lang="ru-RU">
    <div class="header-logo"></div>
    <div class="v-spacing-lg text-center" lang="ru-RU">Режим для слепых</div>
    <nav class="index-menu" role="navigation">
    	<a href="search.php" tabindex="1">Поиск изданий</a>
    	<a href="collections.php" tabindex="2">Подборки</a>
    	<a href="help.php" tabindex="3">Помощь</a>
    	<a href="/" tabindex="4">Возврат в режим для зрячих</a>
    </nav>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>