<?php

namespace LbcBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use LbcBundle\Entity\LbcMaindivision;
use LbcBundle\Entity\LbcMaindivisionintroduction;
use LbcBundle\LbcBundle;


/**
 * @DI\Service("app.maindivision", public=true, scope="request")
 */
class LbcMaindivisionController extends AbstractCategoryController
{

    /**
     * @DI\Inject("app.api.maindivision")
     * @var \LbcBundle\Controller\Api\LbcMaindivisionController
     */
    public $api;

    /**
     * @param LbcMaindivision $entity
     */
    public function checkEntityActions($entity)
    {
        parent::checkEntityActions($entity);
        $em = $this->doctrine->getManager();
        /**
         * @param LbcMaindivision $entity
         *
         * @return LbcMaindivisionintroduction
         */
        $getIntroduction = function (LbcMaindivision $entity) use ($em) {
            static $introduction;
            if (null === $introduction) {
                $introduction = $em->getRepository('LbcBundle:LbcMaindivisionintroduction')
                    ->findOneBy(
                        [
                            'maindivisionid' => $entity->getId(),
                        ]
                    );
            }

            return $introduction;
        };
        foreach ($entity->getActions() as $key => $action) {
            switch ($action['code']) {
                case 'division-introduction':
                    if (1 < $entity->getLevel()) {
                        $entity->removeAction($key);
                        break;
                    }
                    if (!$getIntroduction($entity)) {
                        $entity->removeAction($key);
                        break;
                    }
                    break;
                case 'division-introduction-images':
                    if (1 < $entity->getLevel()) {
                        $entity->removeAction($key);
                        break;
                    }
                    /** @var LbcMaindivisionintroduction $introduction */
                    if (!$introduction = $getIntroduction($entity)) {
                        $entity->removeAction($key);
                        break;
                    }
                    $introductionImages = $em
                        ->getRepository('LbcBundle:LbcMaindivisionintroductionimages')
                        ->findOneBy(
                            [
                                'maindivisionintroductionid' => $introduction->getId()
                            ]
                        );
                    if (!$introductionImages) {
                        $entity->removeAction($key);
                        break;
                    }
                    break;
                case 'division-images':
                    if (!$em->getRepository('LbcBundle:LbcMaindivisionimages')->findOneBy(
                        [
                            'maindivisionid' => $entity->getId(),
                        ]
                    )
                    ) {
                        $entity->removeAction($key);
                    }
                    break;
                case 'link-specialdivision':
                    if (!$em->getRepository('LbcBundle:LbcSpecialdivision')->findOneBy(
                        [
                            'maindivisionid' => $entity->getId(),
                        ]
                    )
                    ) {
                        $entity->removeAction($key);
                    }
                    break;
                case 'link-locationplans':
                    if (!$em->getRepository('LbcBundle:LbcLocationplans')->findOneBy(
                        [
                            'maindivisionid' => $entity->getId(),
                        ]
                    )
                    ) {
                        $entity->removeAction($key);
                    }
                    break;
            }
        }
    }

    /**
     * @return array
     */
    protected function _getRouterMaps()
    {
        return [
            'api_category_detail' => 'api_bbk_maindivision_detail',
            'api_category_update' => 'api_bbk_maindivision_update',
            'api_category_images' => 'api_bbk_maindivisionimages',
            'category'            => 'bbk_maindivision',
        ];
    }

    /**
     * @return string
     */
    protected function _getIndexName()
    {
        return 'Основные деления, классификации';
    }

    /**
     * @return array
     */
    protected function _getActionParams()
    {
        return [
            [
                'name' => 'Просмотр введения',
                'code' => 'division-introduction',
            ],
            [
                'name' => 'Скан введения',
                'code' => 'division-introduction-images',
                'roles' => [
                    LbcBundle::BBK_EDITOR_ROLE,
                ]
            ],
            [
                'name' => 'Скан осн. деления',
                'code' => 'division-images',
                'roles' => [
                    LbcBundle::BBK_EDITOR_ROLE,
                ]
            ],
            [
                'name'  => 'Список спец. делений',
                'code'  => 'link-specialdivision',
                'link'  => true,
                'route' => 'bbk_maindivision_specialdivision',
                'roles' => [
                    LbcBundle::BBK_EDITOR_ROLE,
                ]
            ],
            [
                'name'  => 'План расположения',
                'code'  => 'link-locationplans',
                'link'  => true,
                'route' => 'bbk_maindivision_locationplans',
                'roles' => [
                    LbcBundle::BBK_EDITOR_ROLE,
                ]
            ],
        ];
    }
}
