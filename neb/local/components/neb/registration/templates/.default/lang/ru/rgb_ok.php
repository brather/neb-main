<?php
$MESS['REG_RGB_OK_MESSAGE'] = 'Указанные в анкете данные успешно подтверждены.';
$MESS['REG_RGB_OK_LOGIN'] = 'Ваш логин';
$MESS['REG_RGB_OK_CARD_NUMBER'] = 'Ваш читательский билет';
$MESS['REG_RGB_OK_LINK_LK'] = 'Личный кабинет';
$MESS['REG_RGB_OK_LINK_SEARCH'] = 'Перейти к поиску';
$MESS['REG_RGB_OK_PRINT_AGREE'] = 'Распечатать соглашение с НЭБ';
?>