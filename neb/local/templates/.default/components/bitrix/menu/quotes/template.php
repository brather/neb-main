<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult))
{?>	
	<div class="b-searchresult">
		<ul class="b-profile_nav">
			<?foreach($arResult as $arItem)
			{?>	
				<?if($arItem['PARAMS']['CODE'] == 'lk')
				{?>
					<li>
						<a class="<?=$arItem['PARAMS']['CLASS']?><?if($arItem['PARAMS']['CODE'] == $_REQUEST['code']){ echo " current";}?>" href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
						<ul class="b-profile_subnav">
							<?foreach($arResult as $arItems){?>
								<?if($arItems['PARAMS']['DEPTH_LEVEL'] == '2'){?>
									<li><span class="b-profile_subnav_border"><a href="<?=$arItems['LINK']?>"><?if($arItems['PARAMS']['COUNT']){?><span class="b-profile_msgnum">3</span><?}?><?=$arItems['TEXT']?></a></span></li>
								<?}?>
							<?}?>
						</ul>
					</li>
				<?}else
				{?>
					<?if(!isset($arItem['PARAMS']['DEPTH_LEVEL']))
					{?>
						<li><a class="<?=$arItem['PARAMS']['CLASS']?><?if($arItem['PARAMS']['CODE'] == $_REQUEST['code']){ echo " current";}?>" href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a></li>
					<?}?>
				<?}?>
			<?}?>
		</ul>                 
	</div><!-- /.b-searchresult-->
<?}?>
<?/*
<div class="b-searchresult">
	<ul class="b-profile_nav">
		<li>
			<a href="#" class="">личный кабинет</a>
			<ul class="b-profile_subnav">
				<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
				<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
				<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
				<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
				<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
				<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
				<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
			</ul>
		</li>
		<li><a class="" href="#">моя библиотека</a></li>
		<li><a href="#" class="">цитаты</a></li>
		<li><a href="#" class="">закладки</a></li>
		<li><a href="#" class="">заметки</a></li>
	</ul>                 
</div><!-- /.b-searchresult-->
 */?>