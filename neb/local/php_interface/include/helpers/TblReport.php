<?

use \Bitrix\Main\Application;
use Neb\Main\Helper\DbHelper;

/**
 * Отчеты
 * Class TblReport
 */
class TblReport{

    public static function newReport($arFields){

        $connection = Application::getConnection();

        $arFields['report_name'] = trim($arFields['report_name']);
        $arFields['select']      = trim($arFields['select']);

        //создание нового отчета
        $strSql = DbHelper::getInsertQuery('tblReport', ['Name' => $arFields['report_name'], 'Query' => $arFields['select']]);
        $connection->queryExecute($strSql);
        $idReport = $connection->getInsertedId();

        if (empty($arFields['select'])) {

            global $DB;

            // добавление сущностей
            foreach ($arFields['entities'] as $entity){
                $ID = $DB->Insert("tblReportEntities", array('IdReport' => $idReport, 'IdEntity' => $entity), $err_mess.__LINE__);
                $entities[$entity] = $ID;
            }

            //добавление полей
            foreach ($arFields['fields'] as $field){
                $idReportEntity = self::getEntityID($field, $idReport);
                $DB->Insert("tblReportEntityFields", array('IdReportEntity' => $idReportEntity, 'IdEntityField' => $field), $err_mess.__LINE__);
            }

            //добавление связей
            foreach ($arFields['join'] as $join){
                $DB->Insert("tblReportEntityRelations", array('IdReportEntity1' => $entities[$arFields['entity_1_'.$join]], 'IdReportEntity2' => $entities[$arFields['entity_2_'.$join]], 'IdEntityRelation' => $join), $err_mess.__LINE__);
            }

            //добавление условий выборки (where)
            foreach ($arFields['filter'] as $k => $filter){
                if (trim($arFields['value_compare'][$k]) != ''){
                    $idReportEntity = self::getEntityID($filter, $idReport);
                    // определение типа поля
                    $type = TblEntities::getFieldType($filter);
                    $value = ($type == 'date' ? $DB->FormatDate(trim($arFields['value_compare'][$k]), "DD.MM.YYYY HH:MI:SS", "YYYY-MM-DD") : trim($arFields['value_compare'][$k]));
                    $DB->Insert("tblReportWhere", array('IdReportEntity' => $idReportEntity, 'IdEntityField' => $filter, 'IdReportCompareType' => $arFields['compare'][$k] , 'Value' => "'".$value."'"), $err_mess.__LINE__);
                }
            }

            // вычисления и having
            foreach ($arFields['group'] as $group){
                $idReportEntity = self::getEntityID($group, $idReport);
                $agregate_id = $DB->Insert("tblReportAgregate", array('IdReportEntity' => $idReportEntity, 'IdEntityField' =>$group, 'IdReportAgregateType' => $arFields['agr'.$group] ), $err_mess.__LINE__);
                $mass_agrId[$group] = $agregate_id;
                if (trim($arFields['value_agr'.$group]) != ''){
                    $DB->Insert("tblReportHaving", array('IdReportAgregate' => $agregate_id, 'IdReportCompareType' => $arFields['having_compare'.$group] , 'Value' => trim($arFields['value_agr'.$group])), $err_mess.__LINE__);
                }
            }
            // группировка
            foreach ($arFields['groupby'] as $i => $group){
                $idReportEntity = self::getEntityID($group, $idReport);
                $DB->Insert("tblReportGroup", array('IdReportEntity' => $idReportEntity, 'IdEntityField' => $group, 'Order' => $i+1), $err_mess.__LINE__);
            }

            //сортировка
            foreach ($arFields['sort'] as $k => $sort){
                    // проверяем нет ли функции агрегации
                    $res = preg_match('/agr(.*)/', $sort, $tmp);
                    if ($tmp[1] > 0) {
                        $sort = $tmp[1];
                        $DB->Insert("tblReportAgregateOrder", array( 'IdReportAgregate' => $mass_agrId[$sort], 'Order' => $k+1 , 'IdReportOrderType' => $arFields['order_agr'.$sort]), $err_mess.__LINE__);
                    } else {
                        $idReportEntity = self::getEntityID($sort, $idReport);
                        $DB->Insert("tblReportOrder", array('IdReportEntity' => $idReportEntity, 'IdEntityField' => $sort, 'Order' => $k+1 , 'IdReportOrderType' => $arFields['order'.$sort]), $err_mess.__LINE__);
                    }
            }
            // количество записей
            if ($arFields['limit'] > 0)
                $DB->Insert("tblReportLimit", array('IdReport' => $idReport, 'Value' => (int)$arFields['limit']), $err_mess.__LINE__);
            // смещение
            if ($arFields['offset'] > 0)
                $DB->Insert("tblReportOffset", array('IdReport' => $idReport, 'Value' => (int)$arFields['offset']), $err_mess.__LINE__);
        }

        return $idReport;
    }

    public static function editReport($arFields){

        $connection = Application::getConnection();

        $idReport = intval($arFields['report_id']);

        if (!empty(trim($arFields['select']))) {
            $strSql = DbHelper::getUpdateQuery('tblReport', ['Query' => trim($arFields['select'])], ['Id' => $idReport]);
            $connection->queryExecute($strSql);
            return $idReport;
        }

        $strSql = '	DELETE FROM tblReportEntities WHERE IdReport = '.$idReport;
        $connection->queryExecute($strSql);

        if (empty($arFields['select'])) {

            global $DB;

            // добавление сущностей
            foreach ($arFields['entities'] as $entity){
                $ID = $DB->Insert("tblReportEntities", array('IdReport' => $idReport, 'IdEntity' => $entity), $err_mess.__LINE__);
                $entities[$entity] = $ID;
            }

            //добавление полей
            foreach ($arFields['fields'] as $field){
                $idReportEntity = self::getEntityID($field, $idReport);
                $DB->Insert("tblReportEntityFields", array('IdReportEntity' => $idReportEntity, 'IdEntityField' => $field), $err_mess.__LINE__);
            }

            //добавление связей
            foreach ($arFields['join'] as $join){
                $DB->Insert("tblReportEntityRelations", array('IdReportEntity1' => $entities[$arFields['entity_1_'.$join]], 'IdReportEntity2' => $entities[$arFields['entity_2_'.$join]], 'IdEntityRelation' => $join), $err_mess.__LINE__);
            }

            //добавление условий выборки (where)
            foreach ($arFields['filter'] as $k => $filter){
                if (trim($arFields['value_compare'][$k]) != ''){
                    $idReportEntity = self::getEntityID($filter, $idReport);
                    $type = TblEntities::getFieldType($filter);
                    $value = ($type == 'date' ? $DB->FormatDate(trim($arFields['value_compare'][$k]), "DD.MM.YYYY HH:MI:SS", "YYYY-MM-DD") : trim($arFields['value_compare'][$k]));
                    $DB->Insert("tblReportWhere", array('IdReportEntity' => $idReportEntity, 'IdEntityField' => $filter, 'IdReportCompareType' => $arFields['compare'][$k] , 'Value' => "'".$value."'"), $err_mess.__LINE__);
                }
            }

            // вычисления и having
            foreach ($arFields['group'] as $group){
                $idReportEntity = self::getEntityID($group, $idReport);
                $agregate_id = $DB->Insert("tblReportAgregate", array('IdReportEntity' => $idReportEntity, 'IdEntityField' =>$group, 'IdReportAgregateType' => $arFields['agr'.$group] ), $err_mess.__LINE__);
                $mass_agrId[$group] = $agregate_id;
                if (trim($arFields['value_agr'.$group]) != ''){
                    $DB->Insert("tblReportHaving", array('IdReportAgregate' => $agregate_id, 'IdReportCompareType' => $arFields['having_compare'.$group] , 'Value' => trim($arFields['value_agr'.$group])), $err_mess.__LINE__);
                }
            }

            // группировка
            foreach ($arFields['groupby'] as $i => $group){
                $idReportEntity = self::getEntityID($group, $idReport);
                $DB->Insert("tblReportGroup", array('IdReportEntity' => $idReportEntity, 'IdEntityField' => $group, 'Order' => $i+1), $err_mess.__LINE__);
            }

            //сортировка
            foreach ($arFields['sort'] as $k => $sort){
                // проверяем нет ли функции агрегации
                $res = preg_match('/agr(.*)/', $sort, $tmp);
                if ($tmp[1] > 0) {
                    $sort = $tmp[1];
                    $DB->Insert("tblReportAgregateOrder", array( 'IdReportAgregate' => $mass_agrId[$sort], 'Order' => $k+1 , 'IdReportOrderType' => $arFields['order_agr'.$sort]), $err_mess.__LINE__);
                } else {
                    $idReportEntity = self::getEntityID($sort, $idReport);
                    $DB->Insert("tblReportOrder", array('IdReportEntity' => $idReportEntity, 'IdEntityField' => $sort, 'Order' => $k+1 , 'IdReportOrderType' => $arFields['order'.$sort]), $err_mess.__LINE__);
                }
            }
            // количество записей
            if ($arFields['limit'] > 0)
                $DB->Insert("tblReportLimit", array('IdReport' => $idReport, 'Value' => (int)$arFields['limit']), $err_mess.__LINE__);

            // смещение
            if ($arFields['offset'] > 0)
                $DB->Insert("tblReportOffset", array('IdReport' => $idReport, 'Value' => (int)$arFields['offset']), $err_mess.__LINE__);
        }
        return $idReport;
    }

	public static function deleteReport($id){

        $id = intval($id);

		if ($id <= 0)
		    return false;

		$strSql = '	DELETE
					FROM tblReport
					WHERE Id='.$id;

		Application::getConnection()->query($strSql);

		return true;
	}

    /**
     * Получение списка отчетов
     * @return array
     */
	public static function getReportsList(){

        $arResult = [];

        $strSql = "SELECT * FROM tblReport";
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[$row['Id']] = $row;
        }

		return $arResult;
	}

    /**
     * Получение отчета по его идентификатору
     *
     * @param $id
     * @return array
     */
    public static function getReportById($id) {

        $id = intval($id);

        if ($id <= 0) {
            return [];
        }

        $strSql = " SELECT *
                    FROM tblReport
                    WHERE Id=" . $id;

        $arResult = Application::getConnection()->query($strSql)->fetch();

        return $arResult;
    }

	public static function getData($id, $offset){

		$arReport = self::getReportById($id);
        $arTables = self::getEntities($id);
        $arReport['Query'] = trim($arReport['Query']);

        // хранимая процедура
        if (in_array('procedure', $arTables['type'])) {

            $sTbl = explode(' ', end($arTables['tbl']));
            $sTbl = $sTbl[0];

            $sParams = '';
            $arFilter  = self::getWhere($id, true);
            foreach ($arFilter as $sFilter) {
                $sParams .= (!empty($sParams) ? ', ' : '') . "'$sFilter'";
            }
            $select = "CALL $sTbl ($sParams);";
        }
        // прямой запрос без конструктора отчетов
        elseif (!empty($arReport['Query'])){
			$select = $arReport['Query'];
		}
		// запрос через конструктор отчетов
		else {
			
			$select = 'SELECT ';
			$fields = self::getFields($id);
            $aggregation = self::getAggregation($id);
			
			$result['fields'] = $fields['fields'];
			$select .= implode(', ', $fields['data']);

			if (!empty($aggregation)) {
				$select .= ' ,'. implode(' , ', $aggregation['fields']);
				$result['fields'] = array_merge($result['fields'] ,$aggregation['fieldsName']);
			}
			
			if (count($arTables['id']) > 1){

				// список связей
				$select .= ' FROM ';
                $relations = self::getRelation($arTables['id']);

                foreach ($relations as $key => $relation){
                    if ($key == 0) {
                        $select .= $arTables['tbl'][$relation['IdReportEntity1']];
                    }
                    $select .= ' ' . $relation['Name'] . ' ' . $arTables['tbl'][$relation['IdReportEntity2']]
                        . " ON " . $relation['field1'] . "=" . $relation['field2'];
                }
			}
			else {
				$select .= ' FROM '.implode(", ", $arTables['tbl']);
			}
			
			//список для where
			$arFilter  = self::getWhere($id);
            $arGroup   = self::getGroup($id);
            $arHaving  = self::getHaving($id);
            $arOrder   = self::getOrder($id);
			if (!empty($arFilter))
			    $select .=' WHERE '. implode(" AND ", $arFilter);
			if (!empty($arGroup))
			    $select .=' GROUP BY '. implode(", ", $arGroup);
			if (!empty($arHaving))
			    $select .=' HAVING '. implode(" AND ", $arHaving);
			if (!empty($arOrder))
			    $select .=' ORDER BY '. implode(", ", $arOrder);
		}

		// limit задается только для запросов, но не хранимых процедур
		if (false !== stripos($select, 'SELECT')) {
            if ($offset == 'all') {
                $select .= ' LIMIT 65525'; // с Лешей решили не перегружать отчет большими запросами
            } else {
                $select .= ' LIMIT ';
                if ($offset)
                    $select .= $offset . ',';
                $select .= '200';
            }
        }

		$result['sql'] = $select;
		$res = Application::getConnection()->query($select);
		$result['data'] = [];
		while ($row = $res->fetch()) {
            $result['data'][] = $row;
        }

        // вариант с запросом, а не конструктором - брать заголовки из запроса
        if (empty($result['fields'])) {
            $result['fields'] = array_keys($result['data'][0]);
        }

		return $result;
	}
	
	protected static function getEntityID($field, $idReport){

		$sql = 'SELECT re.Id as IdReportEntity
				FROM tblEntityFields  ef
				LEFT JOIN tblReportEntities  re ON re.IdEntity = ef.IdEntity
				where re.IdReport = '.$idReport.' and ef.Id='.$field;

        $row = Application::getConnection()->query($sql)->fetch();
        $iReport = intval($row['IdReportEntity']);

		return $iReport;
	}
	
	public static function getEntities($id){

        $arResult = [];

        $strSql = "	SELECT
		              re.id as id,
		              concat(e.DB,'.', e.Name, ' t', re.id) as tbl,
		              t.Name as type
					FROM tblReportEntities re 
					LEFT JOIN tblEntities e ON e.Id = re.IdEntity
					LEFT JOIN tblEntityTypes t ON t.Id = e.IdType
					WHERE re.IdReport=".$id;
        $results = Application::getConnection()->query($strSql);
        while ($row = $results->fetch()) {
            $arResult['type'][$row['id']] = $row['type'];
            $arResult['tbl'][$row['id']] = $row['tbl'];
            $arResult['id'][]   = $row['id'];
        }

		return $arResult;
	}
	
	public static function getEntitiesReport($id){

        $arResult = [];

		$strSql = "	SELECT idEntity 
					FROM tblReportEntities 
					WHERE IdReport=".$id;
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[] = $row['idEntity'];
		}

		return $arResult;
	}
	
	public static function getFieldsReport($id){

        $arResult = [];

		$strSql = "	SELECT ef.id
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportEntityFields ref ON re.Id = ref.IdReportEntity
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON e.Id = ef.IdEntity AND ref.IdEntityField=ef.Id
					WHERE re.IdReport=".$id;
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[] = $row['id'];
		}

		return $arResult;
	}
	
	public static function getRelation($entities){

        $arResult = [];

		$strSql = "	select rer.id, rer.IdReportEntity1, rer.IdReportEntity2,  ert.Name,  concat('t', rer.IdReportEntity1, '.', ef1.Name)  as field1,
					concat('t', rer.IdReportEntity2, '.', ef2.Name)  as field2, er.Id as rid
					from 
					tblReportEntityRelations rer 

					join tblEntityRelations er on er.Id= rer.IdEntityRelation
					join tblEntityRelationType ert on er.IdEntityRelationType = ert.Id
					join tblEntityFields ef1 on ef1.Id = er.IdField11
					join tblEntityFields ef2 on ef2.Id = er.IdField21
					WHERE rer.IdReportEntity1 IN (".implode(" , ", $entities).") and rer.IdReportEntity2 IN (".implode(" , ", $entities).")";
				
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[] = $row;
		}

		return $arResult;
	}
	
	
	public static function getRelationReports($entities){

        $arResult = [];

		$strSql = "	select rer.id, rer.IdReportEntity1, rer.IdReportEntity2,  ert.Name,  concat('t', rer.IdReportEntity1, '.', ef1.Name)  as field1,
					concat('t', rer.IdReportEntity2, '.', ef2.Name)  as field2
					from 
					tblReportEntityRelations rer 

					join tblEntityRelations er on er.Id= rer.IdEntityRelation
					join tblEntityRelationType ert on er.IdEntityRelationType = ert.Id
					join tblEntityFields ef1 on ef1.Id = er.IdField11
					join tblEntityFields ef2 on ef2.Id = er.IdField21
					WHERE rer.IdReportEntity1 IN (".implode(" , ", $entities).") and rer.IdReportEntity2 IN (".implode(" , ", $entities).")";
					
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[] = $row;
        }

		return $arResult;
	}

	protected static function getFields($id){

        $arResult = [];

		$strSql = "	SELECT CONCAT('t', re.Id, '.', ef.Name) Name , ef.Description
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportEntityFields ref ON re.Id = ref.IdReportEntity
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON e.Id = ef.IdEntity AND ref.IdEntityField=ef.Id
					WHERE r.Id=".$id.";
		";
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult['data'][] = $row['Name'];
            $arResult['fields'][] = $row['Description'];
		}

		return $arResult;
	}
	
	protected static function getWhere($id, $bProcedure = false){

	    $arResult = [];

        if ($bProcedure) {
            $sSelect = ' rw.Value as filter ';
        } else {
            $sSelect = '
            CONCAT("t", re.Id, ".", ef.Name, "" , rct.Name,
			    CASE
					WHEN  eft.Name = "ip" THEN CONCAT ("INET_ATON(\'", rw.Value,"\')")
					WHEN  eft.Name = "int" THEN  rw.Value
					ELSE CONCAT ("\'", rw.Value,"\'")
				END 
			) as filter
			';
        }

		$strSql = "	SELECT 
	                  $sSelect
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportWhere rw ON rw.IdReportEntity = re.Id
						JOIN tblReportCompareTypes rct ON rct.Id = rw.IdReportCompareType
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON ef.Id = rw.IdEntityField
						LEFT JOIN tblEntityFieldTypes eft ON ef.IdEntityFieldType = eft.Id
					WHERE r.Id=$id";
				
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[] = $row['filter'];
        }

		return $arResult;
	}
	
	protected static function getGroup($id){

        $arResult = [];

		$strSql = "	SELECT CONCAT('t', re.Id, '.', ef.Name) as Name
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportGroup rg ON rg.IdReportEntity = re.Id
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON ef.Id = rg.IdEntityField
					WHERE r.Id=".$id."
					ORDER BY rg.Order ASC;
					";
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[] = $row['Name'];
        }

		return $arResult;
	}
	
	public static function getGroupReports($id){

        $arResult = [];

		$strSql = "	SELECT ef.Id, ef.Description,  e.id as eid
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportGroup rg ON rg.IdReportEntity = re.Id
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON ef.Id = rg.IdEntityField
					WHERE r.Id=".$id."
					ORDER BY rg.`Order`;
					";
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult['data'][] = $row;
            $arResult['id'][] = $row['Id'];
		}

		return $arResult;
	}
	
	public static function getWhereReports($id){

        $arResult = [];

		$strSql = "	SELECT ef.Id as idfield,  rct.Id as idcompare, rw.Value, eft.Name as FieldType
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportWhere rw ON rw.IdReportEntity = re.Id
						JOIN tblReportCompareTypes rct ON rct.Id = rw.IdReportCompareType
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON ef.Id = rw.IdEntityField
						LEFT JOIN tblEntityFieldTypes eft ON ef.IdEntityFieldType = eft.Id
					WHERE r.Id=".$id.";
					";

		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult['data'][] = $row;
            $arResult['id'][]   = $row['idfield'];
		}

		return $arResult;
	}
	
	public static function getAggregationReports($id){

        $arResult = [];

		$strSql = "	SELECT ef.Id, rh.IdReportCompareType, rh.Value, ra.IdReportAgregateType
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportAgregate ra ON ra.IdReportEntity=re.Id
						JOIN tblReportAgregateTypes rat ON ra.IdReportAgregateType=rat.Id
						LEFT JOIN tblReportHaving rh ON rh.IdReportAgregate = ra.Id
						LEFT JOIN tblReportCompareTypes rct ON rct.Id = rh.IdReportCompareType
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON e.Id = ef.IdEntity AND ra.IdEntityField=ef.Id
					WHERE r.Id=".$id.";
					";		
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult['data'][$row['Id']] = $row;
            $arResult['id'][] = $row['Id'];
		}

		return $arResult;
	}
	
	protected static function getAggregation($id){

        $arResult = [];

		$strSql = "	SELECT CONCAT(rat.Name, '(t', re.Id, '.', ef.Name, ')') as agr, CONCAT(rat.Description, '(', ef.Description, ')') as fieldName
					FROM tblReport r 
					JOIN tblReportEntities re ON r.Id = re.IdReport 
					JOIN tblReportAgregate ra ON ra.IdReportEntity=re.Id 
					JOIN tblReportAgregateTypes rat ON ra.IdReportAgregateType=rat.Id
					JOIN tblEntities e ON e.Id = re.IdEntity 
					JOIN tblEntityFields ef ON e.Id = ef.IdEntity AND ra.IdEntityField=ef.Id
					WHERE r.Id=".$id.";
					";
					
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult['fields'][] = $row['agr'];
            $arResult['fieldsName'][] = $row['fieldName'];
		}

		return $arResult;
	}
	
	protected static function getHaving($id){

        $arResult = [];

		$strSql = "	SELECT CONCAT(rat.Name, '(t', re.Id, '.', ef.Name, ')', ' ', rct.Name, ' ', rh.Value) as h
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportAgregate ra ON ra.IdReportEntity=re.Id
						JOIN tblReportAgregateTypes rat ON ra.IdReportAgregateType=rat.Id
						JOIN tblReportHaving rh ON rh.IdReportAgregate = ra.Id
						JOIN tblReportCompareTypes rct ON rct.Id = rh.IdReportCompareType
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON e.Id = ef.IdEntity AND ra.IdEntityField=ef.Id
					WHERE r.Id=".$id.";
					";
					
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[] = $row['h'];
        }

		return $arResult;
	}
	
	public static function getOrderReports($id){

        $arResult = [];

		$strSql = "	SELECT ef.Id, ef.Description, ro.`Order`, e.id as eid, rot.id as OrderType
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportOrder ro ON ro.IdReportEntity = re.Id
						JOIN tblReportOrderTypes rot ON ro.IdReportOrderType = rot.id
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON ef.Id = ro.IdEntityField
					WHERE r.Id=".$id."
					;
					";
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult['data'][$row['Order']] = $row;
            $arResult['id'][] = $row['Id'];
		}
		
		$strSql = "	SELECT ef.Id, ef.Description, rao.`Order`, e.id as eid, rot.id as OrderType, rat.Id as agrType
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportAgregate ra ON ra.IdReportEntity = re.Id
						JOIN tblReportAgregateTypes rat ON ra.IdReportAgregateType=rat.Id
						JOIN tblReportAgregateOrder rao ON rao.IdReportAgregate = ra.Id
						JOIN tblReportOrderTypes rot ON rao.IdReportOrderType = rot.id
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON ef.Id = ra.IdEntityField
					WHERE r.Id=".$id."
					;
					";
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult['data'][$row['Order']] = $row;
            $arResult['agr_id'][] = $row['Id'];
		}
		ksort($arResult['data']);

		return $arResult;
	}
	
	protected static function getOrder($id){

        $arResult = [];

		$strSql = "	SELECT ro.`Order`, CONCAT('t', re.Id, '.',ef.Name , ' ', rot.Name) as Name
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportOrder ro ON ro.IdReportEntity = re.Id
						JOIN tblReportOrderTypes rot ON ro.IdReportOrderType = rot.id
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON ef.Id = ro.IdEntityField
					WHERE r.Id=".$id."
					;
					";
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[$row['Order']] = $row['Name'];
        }
		
		$strSql = "	SELECT rao.`Order`, CONCAT(rat.Name,'(t', re.Id, '.',ef.Name , ') ', rot.Name) as Name
					FROM 
						tblReport r
						JOIN tblReportEntities re ON r.Id = re.IdReport
						JOIN tblReportAgregate ra ON ra.IdReportEntity = re.Id
						JOIN tblReportAgregateTypes rat ON ra.IdReportAgregateType=rat.Id
						JOIN tblReportAgregateOrder rao ON rao.IdReportAgregate = ra.Id
						JOIN tblReportOrderTypes rot ON rao.IdReportOrderType = rot.id
						JOIN tblEntities e ON e.Id = re.IdEntity
						JOIN tblEntityFields ef ON ef.Id = ra.IdEntityField
					WHERE r.Id=".$id."
					;
					";
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[$row['Order']] = $row['Name'];
        }
		ksort($arResult);
		
		return $arResult;
	}
	
	
	protected static function getLimit($id){

        $arResult = [];

		$strSql = "	SELECT CONCAT('LIMIT ', rl.Value) as value
					FROM tblReport r JOIN tblReportLimit rl ON r.Id = rl.IdReport 
					WHERE r.Id=".$id.";
					";
        $row = Application::getConnection()->query($strSql)->fetch();
		if (!empty($row)) {
            $arResult = $row['value'];
        }

		return $arResult;
	}
	
	protected static function getOffset($id){

        $arResult = [];

		$strSql = "	SELECT CONCAT('OFFSET ', ro.Value) 
					FROM tblReport r JOIN tblReportOffset ro ON r.Id = ro.IdReport
					WHERE r.Id=".$id.";
					";
				
		$results = Application::getConnection()->query($strSql);
		if ($row = $results->fetch()) {
            $arResult = $row['value'];
        }

		return $arResult;
	}
}