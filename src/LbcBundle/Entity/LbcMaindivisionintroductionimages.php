<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;

/**
 * LbcMaindivisionintroductionimages
 *
 * @ORM\Table(name="lbc_maindivisionintroductionimages")
 * @ORM\Entity(repositoryClass="LbcBundle\Entity\Repository\LbcMaindivisionintroductionimagesRepository")
 */
class LbcMaindivisionintroductionimages extends AbstractLbcImages
{

    /**
     * @ORM\ManyToOne(targetEntity="LbcMaindivisionintroduction", inversedBy="images")
     * @ORM\JoinColumn(name="maindivisionintroductionid", referencedColumnName="Id")
     */
    private $introduction;

    /**
     * @var integer
     *
     * @ORM\Column(name="MainDivisionIntroductionId", type="integer", nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    private $maindivisionintroductionid;


    /**
     * Set maindivisionintroductionid
     *
     * @param integer $maindivisionintroductionid
     *
     * @return LbcMaindivisionintroductionimages
     */
    public function setMaindivisionintroductionid($maindivisionintroductionid)
    {
        $this->maindivisionintroductionid = $maindivisionintroductionid;

        return $this;
    }

    /**
     * Get maindivisionintroductionid
     *
     * @return integer
     */
    public function getMaindivisionintroductionid()
    {
        return $this->maindivisionintroductionid;
    }


    /**
     * @return mixed
     */
    public function getIntroduction()
    {
        return $this->introduction;
    }

    /**
     * @param mixed $introduction
     */
    public function setIntroduction($introduction)
    {
        $this->introduction = $introduction;
    }
}
