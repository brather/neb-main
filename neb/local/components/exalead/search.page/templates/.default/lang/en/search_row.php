<?php

$MESS['PUBLIC_PUBLICATION']     = 'Public publication'; //Открытое издание
$MESS['PROTECTED_PUBLICATINON'] = 'Copyright publication'; // Защищенное
$MESS['COPYRIGHT_PUBLICATINON'] = 'Suspended by the rightholder';

$MESS['EKBSON_CATALOG_RECORD'] = 'Directory entry EKBSON'; //Запись каталога ЭКБСОН
$MESS['CATALOG_RECORD'] = 'Directory entry'; // Запись каталога
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_READ'] = 'Read';// Читать
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_VIEW'] = 'View'; //Смотреть
$MESS['VIDEO_VIEW'] = 'View'; //Смотреть
$MESS['AUDIO_LISTEN'] = 'Listen'; //Слушать
$MESS['INCLUDED_IN'] = 'Includen in'; //Входит в состав
$MESS['CATEGORIES'] = 'Categories'; //Рубрики
$MESS['CATEGORIES_CLOSE'] = 'Categories close';
$MESS['KEYWORDS'] = 'Key words';
$MESS['KEYWORDS_CLOSE'] = 'Key words close';
$MESS[''] = '';