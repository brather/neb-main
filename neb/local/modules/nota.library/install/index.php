<?
class nota_library extends CModule
{
	var $MODULE_ID = 'nota.library';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
    var $MODULE_HOLDER;

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = "Nota Library";
		$this->MODULE_DESCRIPTION = "Модуль для управления библиотеками";

		$this->PARTNER_NAME = "Notamedia";
		$this->PARTNER_URI = "http://notamedia.ru";

        $moduleHolder = "local";
        $pathToInclude = $_SERVER['DOCUMENT_ROOT']."/".$moduleHolder."/modules/".$this->MODULE_ID."/include.php";
        if (!file_exists($pathToInclude))
            $moduleHolder = "bitrix";

        $this->MODULE_HOLDER = $moduleHolder;
	}

	function DoInstall()
	{
        CopyDirFiles($_SERVER['DOCUMENT_ROOT']."/".$this->MODULE_HOLDER."/modules/".$this->MODULE_ID."/install/admin", $_SERVER['DOCUMENT_ROOT']."/bitrix/admin");
        RegisterModuleDependences('main', 'OnAdminListDisplay', $this->MODULE_ID, 'CNotaLibrary', 'OnAdminListDisplay');
        RegisterModuleDependences('main', 'OnBuildGlobalMenu', $this->MODULE_ID, 'CNotaLibrary', 'OnBuildGlobalMenu');
        RegisterModuleDependences('main', 'OnAdminTabControlBegin', $this->MODULE_ID, 'CNotaLibrary', 'OnAdminTabControlBegin');
        RegisterModuleDependences('iblock', 'OnBeforeIBlockElementUpdate', $this->MODULE_ID, 'CNotaLibrary', 'OnBeforeIBlockElementUpdate');

        RegisterModule($this->MODULE_ID);
        return true;
	}

	function DoUninstall()
	{
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT']."/".$this->MODULE_HOLDER."/modules/".$this->MODULE_ID."/install/admin", $_SERVER['DOCUMENT_ROOT']."/bitrix/admin");
        UnRegisterModuleDependences('main', 'OnAdminListDisplay', $this->MODULE_ID, 'CNotaLibrary', 'OnAdminListDisplay');
        UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', $this->MODULE_ID, 'CNotaLibrary', 'OnBuildGlobalMenu');
        UnRegisterModuleDependences('main', 'OnAdminTabControlBegin', $this->MODULE_ID, 'CNotaLibrary', 'OnAdminTabControlBegin');
        UnRegisterModuleDependences('iblock', 'OnBeforeIBlockElementUpdate', $this->MODULE_ID, 'CNotaLibrary', 'OnBeforeIBlockElementUpdate');

        UnRegisterModule($this->MODULE_ID);
        return true;
	}
/*
    function GetModuleRightList()
    {
        $arr = array(
            "reference_id" => array("D", "R", "W"),
            "reference" => array(
                "[D] доступ запрещен",
                "[R] чтение",
                "[W] запись")
        );
        return $arr;
    }*/
}
?>
