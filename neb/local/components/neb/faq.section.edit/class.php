<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Iblock\ElementTable,
    \Bitrix\Iblock\SectionTable,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

class FaqSectionEdit extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $iblockId = '';
    private $minCountSymbol = 5;
    private $nameOfSection = '';
    private $activeSection = '';
    private $sectionId = '';
    private $sortIndex = '500';
    private $arRequest = array();
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        if(!empty($arParams["IBLOCK_ID"])) {
            $this->iblockId = $arParams["IBLOCK_ID"];
        }
        if(!empty($arParams["MIN_COUNT_SYMBOL"])) {
            $this->minCountSymbol = $arParams["MIN_COUNT_SYMBOL"];
        }
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->_getRequest();
        $this->_setDataInForm();
        $this->_getRequestNameSection();
        if($this->arRequest['DELETE_SECTION'] != 'on') {
            $this->_setDataInForm();
        }
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Возвращает данные о разделе из инфоблока для заполнения формы и редактирования раздела
    /** Возвращает данные о разделе из инфоблока для заполнения формы и редактирования раздела
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _setDataInForm() {
        $this->sectionId = $this->arRequest['sect_id'];
        if(!empty($this->arParams["URL_PAGE"])){
            $this->arResult['FORM_URL'] = $this->arParams['SEF_FOLDER']
                . str_replace('#SECTION_ID#', $this->arRequest['sect_id'], $this->arParams["URL_PAGE"]);
        }
        if(!empty($this->arRequest['SORT_SECTION'])) {
            $this->sortIndex = $this->arRequest['SORT_SECTION'];
        }

        $resSection = SectionTable::GetList(array(
            'filter' => array('IBLOCK_ID' => $this->iblockId, 'ID' => $this->sectionId),
            'select' => array('NAME', 'ACTIVE', 'ID', 'SORT')
        ));
        if($obSection = $resSection->Fetch()) {
            $this->arResult['SECTION'] = $obSection;
        } else {
            $this->arResult['ERROR'] = Loc::getMessage('SECTION_NOT_FOUND');
        }
    }
    //endregion
    //region Отправка данных из формы
    /**
     * Отправка данных из формы
     */
    private function _getRequestNameSection() {
        //region отправка формы произошла
        if($this->arRequest['CONTROL_UPDATE_SECTION']) {
            if($this->arRequest['DELETE_SECTION'] == 'on') {
                $this->_deleteSection();
            } elseif(!empty($this->arRequest["NAME_SECTION"])) {
                $this->nameOfSection = $this->_checkString($this->arRequest["NAME_SECTION"]);
                if($this->arRequest["ACTIVE_SECTION"] == 'on') $this->activeSection = 'Y';
                if(strlen($this->nameOfSection) > intval($this->minCountSymbol)) {
                    $this->_updateSectionToIblock();
                } else {
                    $this->arResult['ERROR'] = Loc::getMessage("ERROR_MIN_COUNT_SYMBOL");
                }
            } else {
                $this->arResult['ERROR'] = Loc::getMessage('ERROR_EMPTY_STRING');
            }
        }
        //endregion
    }
    //endregion
    //region Удаление раздела
    /**
     * Удаление раздела
     */
    private function _deleteSection() {
        $resItems = ElementTable::GetList(array(
            'filter' => array('IBLOCK_ID' => $this->iblockId, 'IBLOCK_SECTION_ID' => $this->sectionId),
            'select' => array('ID', 'NAME'),
        ));
        if($obItems = $resItems->Fetch()) {
            $this->arResult['ERROR'] = Loc::getMessage('ERROR_SECTION_NOT_EMPTY_1') . $this->sectionId . Loc::getMessage('ERROR_SECTION_NOT_EMPTY_2');
        } else {
            if(CIBlockSection::Delete($this->sectionId)) {
                $this->arResult['SUCCESS'] = Loc::getMessage('SECTION_DELETE_SUCCESS_1') . $this->sectionId . Loc::getMessage('SECTION_DELETE_SUCCESS_2');
                $this->_redirectToRootList();
            }
            else {
                $this->arResult['ERROR'] = Loc::getMessage('SECTION_DELETE_ERROR_1') . $this->sectionId . Loc::getMessage('SECTION_DELETE_ERROR_2');
            }
        }
    }
    //endregion
    //region Обновление раздела
    /**
     * Обновление раздела
     */
    private function _updateSectionToIblock() {
        $bs = new CIBlockSection;
        $arFields = array(
            "ACTIVE" => $this->activeSection,
            "IBLOCK_ID" => $this->iblockId,
            "NAME" => $this->nameOfSection,
            "SORT" => $this->sortIndex
        );
        if($bs->Update($this->sectionId, $arFields)) {
            $this->arResult['SUCCESS'] = Loc::getMessage('SECTION_UPDATE_SUCCESS_1') . $this->sectionId . Loc::getMessage('SECTION_UPDATE_SUCCESS_2');
        }
        else {
            $this->arResult['ERROR'] = Loc::getMessage('SECTION_UPDATE_ERROR_1') . $this->sectionId . Loc::getMessage('SECTION_UPDATE_ERROR_2');
        }
    }
    //endregion
    //region Перенаправление на список элементов при успешном удалении
    /**
     * Перенаправление на список элементов при успешном удалении
     */
    private function _redirectToRootList() {
        LocalRedirect($this->arParams['SEF_FOLDER']);
    }
    //endregion
    //region Проверка строки на коррктность
    /** Проверка строки на коррктность
     * @param $string - проверяемая строка
     * @return string
     */
    private function _checkString($string) {
        $string = trim($string);
        $string = stripslashes($string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        return $string;
    }
    //endregion
    //region Получение массива $_REQUEST (обертка D7)
    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public function _getRequest() {
        return $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }
    //endregion
}