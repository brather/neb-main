<?php
$MESS['FILE_UPLOAD_C_ERROR_FOLDER_NOT_FOUND'] = 'Ошибка! Путь для хранения файлов не найден.';
$MESS['FILE_UPLOAD_C_DELETE_FILE'] = ' Удалить';
$MESS['FILE_UPLOAD_C_EMPTY_FOLDER'] = 'На данный момент не загружено pdf файлов';
$MESS['FILE_UPLOAD_C_SIZE_B'] = 'байт';
$MESS['FILE_UPLOAD_C_SIZE_KB'] = 'Кб';
$MESS['FILE_UPLOAD_C_SIZE_MB'] = 'Мб';
$MESS['FILE_UPLOAD_C_SIZE_GB'] = 'Гб';