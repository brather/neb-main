<?
$MESS ['BOOKS_CLOSED_AUTHORIZATION_REQUIRED']
    = '<p>Издание, которое Вы пытаетесь открыть, защищено авторским правом. Полный доступ к изданиям, охраняемым авторским правом, можно получить <a href="/workplaces/" target="_blank">в электронном зале библиотеки</a>, участвующей в проекте.</p><p>Внимание! Для чтения изданий, охраняемых авторским правом, необходимо установить на компьютере <a href="/viewers/" target="_blank">программу просмотра</a>.</p>';
$MESS ['BOOKS_CLOSED_FORM_PASS'] = 'Пароль';
$MESS ['BOOKS_CLOSED_FORM_ENTER'] = 'Войти';
$MESS ['BOOKS_CLOSED_FORM_REG'] = 'Зарегистрироваться';
$MESS ['BOOKS_CLOSED_WRONG_PASS'] = 'Неверный логин или пароль';
$MESS ['BOOKS_CLOSED_ACCESS_DENY'] = 'У вас нет доступа к изданиям охраняемым авторским правом';
$MESS ['BOOKS_CLOSED_READ'] = 'Читать';
$MESS ['BOOKS_CLOSED_SUCCESS_REG'] = 'Вы успешно зарегистрированы как читатель РГБ';
$MESS ['BOOKS_CLOSED_SUCCESS_AUTH'] = 'Вы успешно авторизовались как читатель РГБ';
$MESS ['BOOKS_CLOSED_GET_FULL_ACCESS'] = 'Получить полный доступ';
$MESS ['WARNING_PROGRAM_VIEWING']
    = "Внимание! Для чтения произведений, охраняемых авторским правом, необходимо установить на компьютере";
$MESS ['WARNING_PROGRAM_VIEWING2'] = "программу просмотра";