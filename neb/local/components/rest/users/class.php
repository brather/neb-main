<?php

CBitrixComponent::includeComponentClass("rest:rest.server");
CModule::IncludeModule('nota.userdata');

use \Nota\UserData\rgb;

/**
 * Компонент организует API для получения данных о пользователях
 *
 * Class RestUsers
 */
class RestUsers extends RestServer
{
	protected $token, $from, $to;

	public function processRequest($token = null, $from = null, $to = null)
	{
        $this->token = !empty($token) ? trim($token) : false;
        $this->from = !empty($from) ? trim($from) : false;
        $this->to = !empty($to) ? trim($to) : false;

        if(!$this->token || strlen($this->token) <= 0)
            return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));

		$tokenUser = CUser::GetList(
			($by = "id"),
			($order = "desc"),
			array(
				'ACTIVE' => 'Y',
				'UF_TOKEN' => $this->token,
			),
			array('FIELDS' => array('ID'))
		)->Fetch();

        if(!$tokenUser)
            return $this->restError(404, GetMessage('USER_NOT_FOUND'));

        return $this->GETUsers();
	}

	/**
	* Отдает количество пользователей с фильтром по дате и активности
     *
     * Необходимо получить
     * 1. Число пользователей на дату from
     * 2. Число пользователей на дату to
     * 3. Кол-во регистрация за дату from
     * 4. Кол-во регистрация за дату to
	*/
	protected function GETUsers()
	{
        $by = "id";
        $order = "desc";
        $navParams = Array(
            'NAV_PARAMS' => Array(
                "nPageSize" => 1,
                "bDescPageNumbering" => "N",
                "bShowAll" => "N"
            )
        );

		$usersCountFrom    = CUser::GetList($by, $order, Array("DATE_REGISTER_2" => $this->from, "ACTIVE" => 'Y'), $navParams)->NavRecordCount;
		$usersCountTo      = CUser::GetList($by, $order, Array("DATE_REGISTER_2" => $this->to, "ACTIVE" => 'Y'), $navParams)->NavRecordCount;
		$usersRegisterFrom = CUser::GetList($by, $order, Array("DATE_REGISTER_1" => $this->from, "DATE_REGISTER_2" => $this->from, "ACTIVE" => 'Y'), $navParams)->NavRecordCount;
		$usersRegisterTo   = CUser::GetList($by, $order, Array("DATE_REGISTER_1" => $this->to, "DATE_REGISTER_2" => $this->to, "ACTIVE" => 'Y'), $navParams)->NavRecordCount;

        $this->arResult = Array(
            'COUNT_FROM' => $usersCountFrom,
            'COUNT_TO' => $usersCountTo,
            'REGISTER_FROM' => $usersRegisterFrom,
            'REGISTER_TO' => $usersRegisterTo
        );
        return 200;
	}


    public function setveryfied(){

        $this->arResult = array('error' => false, 'message' => '');

        try {
            $user = new nebUser();
            if (!$user->GetID())
                throw new \Exception('Пользователь не найден!');

            $rgb = new rgb();

            $RGBuser = $rgb->usersFind('email', $user->GetEmail());

            if(!$RGBuser['count']  )
                throw new \Exception('Пользователь не найден В РГБ!');

            if (!$user->setStatus(nebUser::USER_STATUS_VERIFIED))
                    throw new \Exception('Ошибка изменения статуса, попробуйте позже!');

            }catch (\Exception $e){
                $this->arResult = array('error' => true, 'message' => $e->getMessage());
        }

        return 200;
    }

}