<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arResult */
$APPLICATION->IncludeComponent(
	'neb:user.edit',
	'edit',
	[
		'USER_ID' => $arResult['VARIABLES']['USER_ID'],
		'VERIFY'  => 'Y'
	],
	$component
);

