<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/prolog.php");

use Bitrix\Main\Loader;
Loader::includeModule('notaext');

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

use Bitrix\NotaExt\Iblock;

$idItem = intval($_REQUEST['idItem']);

if (!check_bitrix_sessid() or !Bitrix\NotaExt\Ext\DetailGallery\Access::check())
    die(GetMessage("ACCESS_DENIED"));
if ($idItem <= 0)
    die(GetMessage("Empty ID"));


$arFilter = array(
    'IBLOCK_ID' => Bitrix\NotaExt\Ext\DetailGallery\Params::getParam('save_ib'),
    'ID' => $idItem,
);

$arSelect = array('ID', 'NAME', 'IBLOCK_ID', 'DETAIL_PICTURE', 'PREVIEW_TEXT', 'PROPERTY_BASIC_TAGS', 'PROPERTY_ADDITIONAL_TAGS');
$arSort = array();

$resResult = Iblock\Element::getList($arFilter, 1, $arSelect, $arSort);

if (empty($resResult['ITEM']))
    die('Not found photo');

$arFile = CFile::GetFileArray($resResult['ITEM']["DETAIL_PICTURE"]);

$libCropFolder = '/local/tools/notaext/detailGallery/libs/cropper-master/';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cropper</title>
    <link href="<?= $libCropFolder ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $libCropFolder ?>dist/cropper.css" rel="stylesheet">
    <link href="<?= $libCropFolder ?>demo/css/main.css?r<?= randString(7); ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Content -->
<div class="container" id="<?= $resResult['ITEM']['ID'] ?>">
    <div class="row">
        <div class="col-md-9">
            <!-- <h3 class="page-header">Demo:</h3> -->
            <div class="img-container">
                <img src="<?= $arFile['SRC'] ?>" alt="Picture">
            </div>
        </div>
        <div class="col-md-3">
            <!-- <h3 class="page-header">Preview:</h3> -->
            <div class="docs-preview clearfix">
                <div class="img-preview preview-lg"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 docs-buttons">
            <!-- <h3 class="page-header">Toolbar:</h3> -->
            <div class="btn-group">
                <button class="btn btn-primary" data-method="setDragMode" data-option="move" type="button" title="Move">
							<span class="docs-tooltip" data-toggle="tooltip">
								<span class="icon icon-move"></span>
							</span>
                </button>
                <button class="btn btn-primary" data-method="setDragMode" data-option="crop" type="button" title="Crop">
							<span class="docs-tooltip" data-toggle="tooltip">
								<span class="icon icon-crop"></span>
							</span>
                </button>
                <button class="btn btn-primary" data-method="zoom" data-option="0.1" type="button" title="Zoom In">
							<span class="docs-tooltip" data-toggle="tooltip">
								<span class="icon icon-zoom-in"></span>
							</span>
                </button>
                <button class="btn btn-primary" data-method="zoom" data-option="-0.1" type="button" title="Zoom Out">
							<span class="docs-tooltip" data-toggle="tooltip">
								<span class="icon icon-zoom-out"></span>
							</span>
                </button>
                <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button"
                        title="Rotate Left">
							<span class="docs-tooltip" data-toggle="tooltip">
								<span class="icon icon-rotate-left"></span>
							</span>
                </button>
                <button class="btn btn-primary" data-method="rotate" data-option="45" type="button"
                        title="Rotate Right">
							<span class="docs-tooltip" data-toggle="tooltip">
								<span class="icon icon-rotate-right"></span>
							</span>
                </button>
                <button title="Clear" type="button" data-method="clear" class="btn btn-primary">
							<span title="" data-toggle="tooltip" class="docs-tooltip"
                                  data-original-title="$().cropper(&quot;clear&quot;)">
								<span class="icon icon-remove"></span>
							</span>
                </button>
                <button title="Reset" type="button" data-method="reset" class="btn btn-primary">
							<span title="" data-toggle="tooltip" class="docs-tooltip"
                                  data-original-title="$().cropper(&quot;reset&quot;)">
								<span class="icon icon-refresh"></span>
							</span>
                </button>
            </div>

            <div class="btn-group AspectRatio" data-toggle="buttons">
                <label class="btn btn-primary active" data-method="setAspectRatio" data-option="1.7777777777777777"
                       title="Set Aspect Ratio">
                    <input class="sr-only" id="aspestRatio1" name="aspestRatio" value="1.7777777777777777" type="radio">
                    <span class="docs-tooltip" data-toggle="tooltip"
                          title="$().cropper(&quot;setAspectRatio&quot;, 16 / 9)">
								16:9
							</span>
                </label>
                <label class="btn btn-primary" data-method="setAspectRatio" data-option="1.3333333333333333"
                       title="Set Aspect Ratio">
                    <input class="sr-only" id="aspestRatio2" name="aspestRatio" value="1.3333333333333333" type="radio">
                    <span class="docs-tooltip" data-toggle="tooltip"
                          title="$().cropper(&quot;setAspectRatio&quot;, 4 / 3)">
								4:3
							</span>
                </label>
                <label class="btn btn-primary" data-method="setAspectRatio" data-option="1" title="Set Aspect Ratio">
                    <input class="sr-only" id="aspestRatio3" name="aspestRatio" value="1" type="radio">
                    <span class="docs-tooltip" data-toggle="tooltip"
                          title="$().cropper(&quot;setAspectRatio&quot;, 1 / 1)">
								1:1
							</span>
                </label>
                <label class="btn btn-primary" data-method="setAspectRatio" data-option="0.6666666666666666"
                       title="Set Aspect Ratio">
                    <input class="sr-only" id="aspestRatio4" name="aspestRatio" value="0.6666666666666666" type="radio">
                    <span class="docs-tooltip" data-toggle="tooltip"
                          title="$().cropper(&quot;setAspectRatio&quot;, 2 / 3)">
								2:3
							</span>
                </label>
                <label class="btn btn-primary" data-method="setAspectRatio" data-option="NaN" title="Set Aspect Ratio">
                    <input class="sr-only" id="aspestRatio5" name="aspestRatio" value="NaN" type="radio">
                    <span class="docs-tooltip" data-toggle="tooltip"
                          title="$().cropper(&quot;setAspectRatio&quot;, NaN)">
								Free
							</span>
                </label>
            </div>
            <? /*?>
						<div class="btn-group">
						<button class="btn btn-primary" data-method="getData" data-option="true" data-target="#putData" type="button">
						<span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;getData&quot;, true)">
						Rounded
						</span>
						</button>
						</div>
						<div class="btn-group">
						<button class="btn btn-primary" data-method="getImageData" data-option="true" data-target="#putData" type="button">
						<span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;getImageData&quot;, true)">
						Image data
						</span>
						</button>
						</div>
						<button class="btn btn-primary" data-method="setImageData" data-target="#putData" type="button">
						<span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setImageData&quot;, data)">
						Set Image Data
						</span>
						</button>
						<button class="btn btn-primary" data-method="getCropBoxData" data-option="" data-target="#putData" type="button">
						<span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;getCropBoxData&quot;)">
						Get Crop Box Data
						</span>
						</button>
						<button class="btn btn-primary" data-method="setCropBoxData" data-target="#putData" type="button">
						<span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setCropBoxData&quot;, data)">
						Set Crop Box Data
						</span>
						</button>
						<input class="form-control" id="putData" type="text" placeholder="Get data to here or set data with this value">
					<?*/ ?>
            <div class="edit-caption">
                <div class="title">Caption</div>
                <div class="field">
                    <textarea id="PREVIEW_TEXT" name="PREVIEW_TEXT"><?= $resResult['ITEM']['PREVIEW_TEXT'] ?></textarea>
                </div>
            </div>
        </div><!-- /.docs-buttons -->

    </div>
</div>

<!-- Alert -->
<div class="docs-alert"><span class="warning message"></span></div>

<!-- Scripts -->
<script src="<?= $libCropFolder ?>assets/js/jquery.min.js"></script>
<script src="<?= $libCropFolder ?>assets/js/bootstrap.min.js"></script>
<script src="<?= $libCropFolder ?>dist/cropper.js?r<?= randString(7); ?>"></script>
<script src="<?= $libCropFolder ?>demo/js/main.js?r<?= randString(7); ?>"></script>

</body>
</html>
