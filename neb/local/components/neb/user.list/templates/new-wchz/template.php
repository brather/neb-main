<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 * @var array $arParams
 */
?>

<a type="button" class="btn btn-primary btadd closein"
   href="/bitrix/admin/user_edit.php?lang=ru" target="_blank">
        <?= Loc::getMessage('ADD_USER') ?>
</a>


<div class="reader-search clearfix">
    <h3>Поиск пользователей</h3>

    <form>
        <input type="text" name="FIO" class="b-text reader-search__input"
               value="<?= $arParams['FIO'] ?>"
               placeholder="<?= Loc::getMessage('FULL_NAME') ?>">
        <button class="btn btn-primary" type="submit"
                value="<?= Loc::getMessage('FIND') ?>"><?= Loc::getMessage(
                'FIND'
            ) ?></button>
    </form>
</div>

<h3>Список пользователей</h3>
<div class="lk-table">
    <div class="lk-table__column" style="width:60%"></div>
    <div class="lk-table__column" style="width:30%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('NAME') ?>
        </li>
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('DATE_REGISTER') ?>
        </li>
        <li class="lk-table__header-kind"></li>
        <li class="lk-table__header-kind">Блокировка</li>
        <li class="lk-table__header-kind">Верификация</li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['items'] as $user) { ?>
            <ul class="lk-table__row">
                <li class="lk-table__col">
                    <a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $user['ID'] ?>"
                       target="_blank">
                        <?= $user['LAST_NAME'] ?> <?= $user['NAME'] ?> <?= $user['SECOND_NAME'] ?>
                    </a>
                </li>
                <li class="lk-table__col">
                    <?= $user['DATE_REGISTER'] ?>
                </li>
                <li class="lk-table__col">
                    <a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $user['ID'] ?>"
                       target="_blank">
                        <?= Loc::getMessage('EDIT') ?>
                    </a>
                </li>
                <li class="lk-table__col">
                    <?= $user['LOCKED'] ? 'Заблокирован' : '' ?>
                </li>
                <li class="lk-table__col">
                    <? if ($user['VERIFIED']) { ?>
                        Верифицирован
                    <? } elseif ($user['REQUEST']) { ?>
                        Запрос отправлен
                    <? } else { ?>
                        <button type="button"
                                class="btn btn-primary"
                                data-id="<?= $user['ID'] ?>">
                            Верифицировать
                        </button>
                    <? } ?>
                </li>
            </ul>
        <? } ?>
    </section>
</div>
<script type="text/javascript">
    /**
     * @todo выплить из шаблона
     */
    $(function () {
        $('ul.lk-table__row .btn-primary').click(function () {
            var user_id = $(this).attr('data-id');
            $.ajax({
                type: "PUT",
                url: "/rest_api/users/status/",
                data: "user_id=" + user_id + "&verify=true&preVerify=true&token=" + getUserToken(),
                success: function (data) {
                    if (true === data.success) {
                        location.reload();
                    }
                },
                error: function (data) {
                    var error = window.neb.objectPathSearch(data, 'responseJSON.errors.message');
                    if (error) {
                        alert(error);
                    }
                }
            });
        })
    })
</script>

<?= $arResult['nav'] ?>
