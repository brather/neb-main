<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 13.09.2016
 * Time: 11:00
 */

CBitrixComponent::includeComponentClass('neb:feedback');

/**
 * Class FeedbackHistoryComponent
 */
class FeedbackHistoryComponent extends FeedbackComponent
{
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->includeModules(['form', 'nota.exalead']);
        $this->getRequest();
        $this->getUser();
        $this->getTicket();
        $this->getHistory();

        $this->includeComponentTemplate();
    }

    /**
     * Получение истории заявки
     *
     * @return array
     */
    private function getHistory() {

        $arResult = array_reverse(unserialize($this->arTicket['FIELDS']['FB_HISTORY_LOG']['USER_TEXT']));

        // получение пользователей
        $arUsers = [];
        foreach ($arResult as $arItem)
            $arUsers[$arItem['USER_ID']] = $arItem['USER_ID'];
        $arUsers = $this->getUsers(array_values($arUsers));
        foreach ($arResult as &$arItem)
            $arItem['USER'] = $arUsers[$arItem['USER_ID']];
        
        $this->arResult['ITEMS'] = $arResult;
    }
}