<?php
$MESS = array(
    'AUTHOR_TITLE'       => 'Author',
    'IS_NEW'             => 'New',
    'ADD_BOOK'           => 'Add library',
    'PUBLISH_YEAR_TITLE' => 'Publish year',
    'ISBN_TITLE'         => 'ISBN',
    'PDF_TYTLE'          => 'PDF',
    'VIEWS'              => 'Views'
);