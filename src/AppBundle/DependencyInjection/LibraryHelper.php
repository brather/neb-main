<?php
/**
 * User: agolodkov
 * Date: 09.09.2016
 * Time: 10:43
 */

namespace AppBundle\DependencyInjection;


use BitrixBundle\DependencyInjection\BitrixHelper;
use BitrixBundle\Traits\IblockHelperTrait;

class LibraryHelper
{
    use IblockHelperTrait;

    /**
     * @param BitrixHelper $bitrix
     */
    public function __construct(BitrixHelper $bitrix)
    {
        $this->setBitrix($bitrix);
    }
}