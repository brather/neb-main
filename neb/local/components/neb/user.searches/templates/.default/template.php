<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CJSCore::Init(array('date'));

?>
<hr>
<form
	method="GET" name="form1" class="form-inline">
	Дата с 
	<div class="form-group">
		<div class="input-group">
			<input 
				type="text" 
				name="dateFrom" 
				id="dateFromInput" 
				size="10" 
				value="<?=$arParams['dateFrom']?>" 
				class="datepicker-from form-control"
				data-masked="99.99.9999"
			>
			<span class="input-group-btn">
				<a class="btn btn-default" onclick="BX.calendar({node: 'dateFromInput', field: 'dateFromInput',  form: '', bTime: false, value: ''});">
	                <span class="glyphicon glyphicon-calendar"></span>
	            </a>
			</span>
		</div>
	</div>
	по 
	<div class="form-group">
		<div class="input-group">
			<input 
				type="text" 
				name="dateTo" 
				id="dateToInput" 
				size="10" 
				value="<?=$arParams['dateTo']?>" 
				class="datepicker-current form-control"
				data-masked="99.99.9999"
			>
			<span class="input-group-btn">
				<a class="btn btn-default" onclick="BX.calendar({node: 'dateToInput', field: 'dateToInput',  form: '', bTime: false, value: ''});">
	                <span class="glyphicon glyphicon-calendar"></span>
	            </a>
			</span>
		</div>
	</div>
	<button class="btn btn-primary" type="submit" value="refresh">Обновить</button>
</form>
<? if (!empty($arResult['ERRORS'])) {
	foreach($arResult['ERRORS'] as $error) {
		ShowError($error);
		echo '<br>';
	}
} else { ?>
<br>
<ul class="b-filter_list_wrapper search-result__content-list clearfix">
	<?

		if(!empty($arResult['ITEMS']))
		{
			$i = 0;
			foreach($arResult['ITEMS'] as $arItem)
			{
				$i++;
				$idate = new DateTime($arItem['UF_DATE_ADD']);
				$datenotime = $idate->format('d.m.Y');
			?>
			<li class="search-result__content-list-kind clearfix" data-query-item data-page-item="<?=urlencode($arItem['ID'])?>">

				<!-- <button class="search-result__close"></button> -->
				<button class="btn btn-default button-remove" data-page-item-delete title="Удалить запрос?" data-modal-title="Удалить запрос из истории?" data-button-title="Удалить">&nbsp;</button>
				
				<div class="search-result__content-list-sidebar">
                    <span class="search-result__content-list-number"><?=$i+(($arResult['iNumPage']-1)*$arParams['ITEM_COUNT'])?>.</span>
                </div>

                <div class="search-result__content-main">

                	<div class="search-result__content-main-links">
	                	<? //Нет смысла в условии, если мы отображает все записи и отображение параграфа не входит в это условие
//							if(!empty($arItem['UF_QUERY']) or !empty($arItem['UF_NAME']))
//							{
//							?>
							<a title="<?=GetMessage("PROFILE_SEARCHES_YOU_SEARCHED");?>" href="<?=(SITE_TEMPLATE_ID=="special")?"/special":"";?><?=$arItem['UF_URL']?>" class="search-result__content-link-title"><?=empty($arItem['UF_QUERY']) || $arItem['UF_QUERY'] =='#all' ? 'Перейти к поиску' : $arItem['UF_QUERY']?></a>
							<?
//							}
//						?>						
					</div>
					<div class="saved-search-results-infoblock" data-slide-togglable>
						<div class="search-result__mini-header">
							<span class="search-result__searched-date">
								<?=GetMessage("PROFILE_SEARCHES_SAVED_REQUEST");?>: 
								<?if(!empty($arItem['UF_NAME'])){?><?=$arItem['UF_NAME']?>, <?}?>
								<?//=$arItem['UF_DATE_ADD']?>
								<?=$datenotime?>
							</span>
						
							<span class="search-result__finded-count">
								<?//=GetMessage("PROFILE_SEARCHES_FOUND");?>
								<a href="<?=(SITE_TEMPLATE_ID=="special")?"/special":"";?><?=$arItem['UF_URL']?>">
									<?=GetEnding($arItem['UF_FOUND'], GetMessage('PROFILE_SEARCHES_FOUND_5'), GetMessage('PROFILE_SEARCHES_FOUND_1'), GetMessage('PROFILE_SEARCHES_FOUND_2'))?>							
									<?=$arItem['UF_FOUND']?>
									<?=GetEnding($arItem['UF_FOUND'], GetMessage('PROFILE_SEARCHES_BOOKS_5'), GetMessage('PROFILE_SEARCHES_BOOKS_1'), GetMessage('PROFILE_SEARCHES_BOOKS_2'))?>
								</a>
							</span>
						</div>
						<dl class="sameline-light">
							<?foreach($arItem['SEARCH_PARAMS'] as $paramName => $paramValue) {
								?>
									<dt><?=$paramName?>:</dt>
									<dd><?=$paramValue?></dd>
								<?
								/*echo "<b>$paramName:</b> $paramValue <br>";*/
							}?>
						</dl>
						<span class="expand-collapse" data-slide-toggler data-expand-text="<?=GetMessage('PROFILE_SEARCHES_EXPAND')?>" data-collapse-text="<?=GetMessage('PROFILE_SEARCHES_COLLAPSE')?>"></span>
					</div>

					
					<?/*
						if(!empty($arItem['UF_MORE_OPTIONS'])){
						?>
						<div class="b-shresult_more iblock">
							<em><?=GetMessage("PROFILE_SEARCHES_REFINE");?>: </em>
							<?
								foreach($arItem['UF_MORE_OPTIONS'] as $arV):
									if(strpos($arV['text'], ':') !== false):
										$arT = explode(':', $arV['text']);
									?>

									    <p> <?=$arV['logic']?> <?=$arT[0]?> <em><?=$arT[1]?></em> </p>

									<? else: ?>

									    <p>  <?=$arV['logic']?> <em><?=$arV['text']?></em> </p>

									<? endif;

								endforeach;
							?>
						</div>
						<?
						}
					*/?>
					<?/*div class="b-shresult_act iblock">
						<a href="<?=(SITE_TEMPLATE_ID=="special")?"/special":"";?><?=$arItem['UF_URL']?>" class="b-litebt"><?=GetMessage("PROFILE_SEARCHES_REDO");?></a>

						<div class="b-shresult_selection rel">
							<a href="#" class="b-openermenu js_openmfavs" data-callback="reloadRightMenuBlock()" data-favs="<?=ADD_COLLECTION_URL?>list.php?t=searches&id=<?=urlencode($arItem['ID'])?>"><?=GetMessage("PROFILE_SEARCHES_MY_COLLETIONS");?></a>
							<div class="b-favs">										
								<form class="b-selectionadd collection" action="<?=ADD_COLLECTION_URL?>add.php" method="post">
									<input type="hidden" name="t" value="searches"/>
									<input type="hidden" name="id" value="<?=$arItem['ID']?>"/>
									<input type="submit" class="b-selectionaddsign" value="+">
									<span class="b-selectionaddtxt"><?=GetMessage("PROFILE_SEARCHES_CREATE_COLLECTION");?></span>
									<input type="text" name="name" class="input hidden">
								</form>
							</div><!-- /.b-favs  -->
						</div>

					</div*/?>
				</div>
			</li><!-- /.b-shresult_item -->
			<?
			}
		}
	?>
</ul><!-- /.b-shreult_list -->
<?=$arResult["NAV_STRING"]?>
<? } ?>