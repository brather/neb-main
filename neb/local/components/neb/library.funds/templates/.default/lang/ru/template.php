<?php
$MESS['LIBRARY_FUNDS_SORT_BY_AUTHOR'] = 'По автору';
$MESS['LIBRARY_FUNDS_SORT_BY_NAME'] = 'По названию';
$MESS['LIBRARY_FUNDS_SORT_BY_DATE'] = 'По дате';
$MESS['LIBRARY_FUNDS_SORT_BY_SHOW'] = 'Показать';

$MESS['LIBRARY_FUNDS_LEFT_AUTH'] = 'Требуется авторизация';
$MESS['LIBRARY_FUNDS_REMOVE_FROM_LIB'] = '<span>Удалить</span> из Моей библиотеки';
$MESS['LIBRARY_FUNDS_ADD_TO_LIB'] = '<span>Добавить</span> в Мою библиотеку';
$MESS['LIBRARY_FUNDS_AUTHOR'] = 'Автор';
$MESS['LIBRARY_FUNDS_YEAR'] = 'Год публикации';
$MESS['LIBRARY_FUNDS_PAGES'] = 'Количество страниц';
$MESS['LIBRARY_FUNDS_PUBLISH'] = 'Издательство';
$MESS['LIBRARY_FUNDS_SHOW_ALL_FUNDS'] = 'Показать все (Включая каталог печатных изданий)';