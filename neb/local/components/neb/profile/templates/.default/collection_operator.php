<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__DIR__.'/main_library.php'); // Для того, что б не копировать один и тот же текст
Loc::loadMessages(__FILE__);
?>

<div class="proflibadmincol">
<?
$APPLICATION->IncludeComponent(
    "neb:collections.list",
    "library_profile",
    array(
        "PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "IBLOCK_ID" => IBLOCK_ID_COLLECTION,
        "LIBRARY_ID" => $arResult['LIBRARY']['ID'],
        "SORT" => 'created',
        "ORDER" => 'DESC',
        "COUNT_BOOKS_IN_COLLECTION" => 8
    ),
    $component
);
?>
</div>

