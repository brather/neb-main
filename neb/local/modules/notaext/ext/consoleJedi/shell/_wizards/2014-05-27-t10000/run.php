﻿<?
// Настройки, чтобы скрипт добрался до Битрикса
$_SERVER['DOCUMENT_ROOT']=realpath(__DIR__."/../../s1");

// Задаём константы
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

// Подключаем Битрикс
require $_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/main/include/prolog_before.php';

Bitrix\NotaExt\Ext\consoleJedi\Consoleapp::checkIfUserIsAdequateOrDie('Вы точно хотите запустить визард?','Y');