<?php
/**
 * User: agolodkov
 * Date: 18.01.2016
 * Time: 12:21
 */

namespace Neb\Main\Stat;

use \Bitrix\Main\Config\Option;
use \Bitrix\Main\DB\Exception;
use \Bitrix\Main\Web\HttpClient;
use \Neb\Main\Helper\Debug;

/**
 * Class Tracker
 *
 * @package Neb\Main\Stat
 */
class Tracker
{
    /**
     * @param string $event
     * @param array  $data
     *
     * @return bool|string
     */
    public static function track($event, $data)
    {
        if (!isset($data['ip']) || empty($data['ip'])) {
            $data['ip'] = $_SERVER['REMOTE_ADDR'];
        }

        // дебаггер для подмены IP адреса
        if (!empty($_SESSION['DEBUG_IP'])) {
            $data['ip'] = $_SESSION['DEBUG_IP'];
        }
        
        list($statsHost, $accessToken) = static::getHostAndToken();

        $httpClient = new HttpClient();
        return $httpClient->post(
             $statsHost. 'tracks'
            . '?access-token=' . $accessToken,
            [
                'action' => $event,
                'data'   => $data,
            ]
        );
    }

    public static function periodDataTrack($type, $data, $period = 'daily_data')
    {
        $httpClient = new HttpClient();

        list($statHost, $tokenWrite) = static::getHostAndToken();

        $result = $httpClient->post(
            $statHost . 'period-datas'
            . '?access-token=' . $tokenWrite,
            [
                'type'   => $type,
                'period' => $period,
                'data'   => $data,
            ]
        );
        //Debug::info('periodDataTrack result: ' . $result);
        $result = json_decode($result, true);
        if (!isset($result['id'])) {
            return false;
        }

        return $result;
    }

    /**
     * For Local Checks
     */
    private static function getHostAndToken ()
    {
        $statsHost = Option::get('neb.main', 'stats_host');
        $accessToken = Option::get('neb.main', 'stats_writer_token');
        
        return array($statsHost, $accessToken);
    }
}