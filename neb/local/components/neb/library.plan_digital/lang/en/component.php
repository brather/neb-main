<?php
$MESS['LIB_PLAN_DIG_COMP_ALL_UPLOAD'] = 'Bulk upload ';
$MESS['LIB_PLAN_DIG_COMP_FOUNDS_FOR_DIGITIZATION'] = 'Library Funds for digitization';
$MESS['LIB_PLAN_DIG_COMP_DATA_ERROR'] = 'Wrong data!';
$MESS['LIB_PLAN_DIG_COMP_LOADED'] = 'Loaded';
$MESS['LIB_PLAN_DIG_COMP_DUPLICATION'] = 'Duplicates';
$MESS['LIB_PLAN_DIG_COMP_NOT_FOUND_IN_NEL'] = 'Not found in NEL';
$MESS['LIB_PLAN_DIG_COMP_DETALIZATION'] = 'Detalization of plan digitization .xls';
$MESS['LIB_PLAN_DIG_COMP_UNKNOWN_LIBRARY'] = 'Unknown library!';
$MESS['LIB_PLAN_DIG_COMP_ERROR_LOAD_FILE'] = 'Error load of file!';
$MESS['LIB_PLAN_DIG_COMP_ERROR_LOAD_BOOK_INFO'] = 'Error load information of books!';