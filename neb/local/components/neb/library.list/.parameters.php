<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// инфоблоки
if (!CModule::IncludeModule("iblock"))
    return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$obIBlock = new \CIBlock();
$rsIBlock = $obIBlock->GetList(
    array("sort" => "asc"),
    array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE" => "Y")
);
while ($arr = $rsIBlock->Fetch())
    $arIBlock[$arr["ID"]] = "[" . $arr["ID"] . "] " . $arr["NAME"];
unset($obIBlock, $rsIBlock, $arr);

// типы библиотек
$arLibType = array(
    'library'      => 'Библиотеки',
    'participants' => 'Библиотеки НЭБ',
    'workplaces'   => 'Электронные читальные залы'
);

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => 'IBLOCK_TYPE',
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => 'IBLOCK_ID',
            "TYPE" => "LIST",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
            "ADDITIONAL_VALUES" => "Y",
        ),
        "CACHE_TIME" => Array("DEFAULT" => 3600),
        "DETAIL_URL" => CIBlockParameters::GetPathTemplateParam(
            "DETAIL",
            "DETAIL_URL",
            'Шаблон пути детальной страницы',
            "",
            "URL_TEMPLATES"
        ),
        "LIBRARY_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => 'LIBRARY_TYPE',
            "TYPE" => "LIST",
            "VALUES" => $arLibType,
            "REFRESH" => "Y",
        ),
    ),
);