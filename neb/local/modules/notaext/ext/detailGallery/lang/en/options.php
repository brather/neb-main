<?
$MESS ['TAB_TITLE_SETTING'] = "Настройки расширения";
$MESS ['GROUP_SAVE_FOTO'] = "Параметры хранения фотографий";
$MESS ['GROUP_TAGS'] = "Параметры тегирования фото";
$MESS ['GROUP_OTHER'] = "Другие параметры";
$MESS ['IBLOCK_TYPE'] = "Тип инфоблока";
$MESS ['IBLOCK'] = "Инфоблок";
$MESS ['SAVE_ORIGINAL'] = "Сохранять оригинал";
$MESS ['DEFAULT_W'] = "Ширина";
$MESS ['DEF_W_NOTE'] = "Ширина после обработки оригинала";
$MESS ['DEFAULT_H'] = "Высота";
$MESS ['DEF_H_NOTE'] = "Высота после обработки оригинала";

$MESS ['SHOW_EDIT_FIELDS'] = "Отображать выбранные поля при редактировании фото";
$MESS ['SHOW_EDIT_GROUPS'] = "Поля которые можно задать сразу для всех фото при загрузке";
$MESS ['SHOW_FIELD_NAME'] = "Название";
$MESS ['SHOW_FIELD_CODE'] = "Код";
$MESS ['SHOW_FIELD_PREVIEW_TEXT'] = "Описание";

$MESS ['FIELD_TAGS'] = "Поле в котором будут храниться теги";
$MESS ['SAVE_SECTIONS'] = "Сохранять галерею в раздел ИБ";

$MESS ['UPLOAD_EXT'] = "Расширения файлов, которые можно загружать";
$MESS ['UPLOAD_MAX_SIZE_FILE'] = "Максимальный размер файла в Мб";
$MESS ['UPLOAD_QUALITY_FILE'] = "Качество ресайзов";
	
