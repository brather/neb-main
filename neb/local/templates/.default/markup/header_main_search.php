<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title></title>
	
	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />

	<script src="js/libs/modernizr.min.js"></script>

	<link rel="stylesheet" href="css/style.css">
	
	<script src="js/libs/jquery.min.js"></script>

	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>	
	<script>window.jQuery.ui || document.write('<script src="js/libs/jquery.ui.min.js">\x3C/script>')</script>
	<script src="js/libs/jquery.ui.touch-punch.min.js"></script>

	<script src="js/plugins.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/blind.js"></script>
	<script src="js/script.js"></script>
	<script>
		$(function() {
            $('html').addClass('mainhtml'); /* это для морды */
        }); /* DOM loaded */
	</script>
</head>
<body class="blind ">
	<div class="oldie hidden">Вы пользуетесь устаревшей версией браузера. <br/>Не все функции будут работать в нем корректно. <br/>Смените на более новую версию</div>
	<div class="homepage">
		<div class="b-fixedhead_h">
			<div class="b-fixedhead">	
				<header id="header" class="mainheader">		

					<div class="wrapper clearfix">


						<div class="b-headernav_login right">
							<a href="#" class="b-headernav_login_lnk js_register" data-width="255" data-height="170">Регистрация</a>
							<div class="b_register_popup b_login_popup" >
								<div class="b_login_popup_in bbox">
									<h4>Зарегистрироваться как</h4>
									<a href="#" class="formbutton">Читатель</a>
									<div class="b-rightholder">или <a href="#">правообладатель</a></div>
								</div>
							</div> 	<!-- /.b_login_popup -->	

							<a href="#" class="b-headernav_login_lnk current popup_opener" data-height="320"  data-width="550" >Войти</a> <!-- data-width="550"  --> 
							<div class="b_login_popup popup" >
								<div class="b_login_popup_in bbox">
									<div class="b-login_txt hidden"><a href="#">вход для библиотек</a> <a href="#">для правообладателей</a></div>

									<div class="b-loginform iblock rel">
										<h4>вход в личный кабинет</h4>
										<p class="change_pass_mess">Ваш пароль успешно изменен. <br/>Для продолжения работы с порталом введите Ваши адрес электронной почты и пароль, воспользуйтесь кнопкой «Войти».</p>
										<form action="" class="b-form b-formlogin">
											<div class="fieldrow nowrap">
												<div class="fieldcell iblock">
													<label class="lite" for="settings11">E-mail / Логин / ЕЭЧБ</label>
													<div class="field validate">
														<input type="text" data-required="required" value=""  id="settings11" data-maxlength="30" data-minlength="2" name="num" class="input">										
													</div>
												</div>

											</div>
											<div class="fieldrow nowrap">

												<div class="fieldcell iblock">
													<label for="settings10">Пароль</label>
													<div class="field validate">
														<input type="password" data-required="required" value="" id="settings10" data-maxlength="30" data-minlength="2" name="pass" class="input">										
													</div>
												</div>
											</div>
											<!--<em class="error_message">Такой e-mail не зарегистрирован</em>-->
											<div class="fieldrow nowrap fieldrowaction">
												<div class="fieldcell ">
													<div class="field clearfix">
														<button class="formbutton left" value="1" type="submit">Войти</button>
														<a href="#" class="formlink right js_passrecovery">забыли пароль?</a>
													</div>
												</div>
											</div>
										</form>
										<div class="b-passrform">								
											<a href="#" class="closepopup">Закрыть окно</a>
											<form action=""  class="b-passrecoveryform b-form hidden">
												<div class="fieldrow nowrap">
													<div class="fieldcell iblock">
														<label for="settings11">Введите электронную почту, указанную при регистрации</label>
														<div class="field validate">
															<input type="text" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="30" data-minlength="2" name="num" class="input">	
															<em class="error required">Поле обязательно для заполнения</em>									
														</div>
													</div>

													<div class="fieldrow nowrap fieldrowaction">
														<div class="fieldcell ">
															<div class="field clearfix">
																<button class="formbutton" value="1" type="submit">Восстановить</button>

															</div>
														</div>
													</div>
												</div>
											</form>
											<form action=""  class="b-passrecoveryform b-form">
												<div class="fieldrow nowrap">
													<div class="fieldcell iblock">
														<div class="b-warning">Адрес эл. почты не найден</div>
														<label for="settings11">Попробуйте ввести другой адрес</label>
														<div class="field validate">
															<input type="text" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="30" data-minlength="2" name="num" class="input">	
															<em class="error required">Поле обязательно для заполнения</em>									
														</div>
													</div>

													<div class="fieldrow nowrap fieldrowaction">
														<div class="fieldcell ">
															<div class="field clearfix">
																<button class="formbutton" value="1" type="submit">Восстановить</button>

															</div>
														</div>
													</div>
												</div>
											</form>
											<p class="b-passconfirm hidden">На почту выслано письмо с ссылкой на сброс пароля</p>
										</div>
									</div><!-- /.b-loginform -->
									<div class="b-login_social iblock">
										<h4>через социальные сети</h4>
										<a href="#" class="b-login_slnk vklink">вконтакте</a>
										<a href="#" class="b-login_slnk odnlink">одноклассники</a>
										<a href="#" class="b-login_slnk fblink">facebook</a>
										<a href="#" class="b-login_slnk esialink">Вход по ЕСИА</a>
									</div><!-- /.b-login_social -->

								</div>

							</div> 	<!-- /.b_login_popup -->
							<? if($_GET['PASSWORD_CHANGED']=='Y') {
								?>
								<script>
									$(function() {
										$('.b_login_popup').addClass('change_log_pass');
										$('.b-headernav_login_lnk').data('height', '430');
										$('.b-headernav_login_lnk').trigger('click');

									}); 
								</script>

								<?
							}
							?>
						</div>
						<div class="b-header_lang right ">
							<a href="#" class="en iblock">Eng</a>
							<a href="#" class="ru iblock"></a>
						</div>
				<!--<div class="b-headernav-apps right">
					<a href="#" class="b-headerapps_img"><span>3</span></a>						
					<a href="#" class="b-headerapps_lnk">Приложения для моб. устройств</a>
				</div>-->
				<nav class="b-headernav left">    
					<a href="#collections" class="b-headernav_lnk">Коллекции</a>
					<a href="#" class="b-headernav_lnk">Библиотеки</a>
					<a href="#" class="b-headernav_lnk">Форум</a>
				</nav>
				<span  class="testmode">Портал работает в тестовом режиме. В случае обнаружения технических проблем или некорректных данных - <a href="<?=URL_SCHEME?>://<?=SITE_HOST?>/feedback/">сообщите нам</a>.</span>
			</div>	<!-- /.wrapper -->			
			
		</header>
		<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="asearch" value="" autocomplete="off" data-src="search.php?session=MY_SESSION&moreparams=MORE_PARAMS">
							<select name="searchopt" id=""class="js_select b_searchopt">
								<option value="1">по всем полям</option>
								<option value="2">по дате</option>

							</select>
						</span>
						
					</div>
					
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
				<div class="b-search_ex">
					<div class="wrapper rel bbox">
						<div class="b_search_row js_search_row hidden" >
							<select name="logic" disabled="disabled" id="" class="ddl_logic">
								<option value="">или</option>
								<option value="">и</option>
								<option value="">не</option>
							</select>
							<select name="theme" disabled="disabled" id="" class="ddl_theme">
								<option value="">по всем полям</option>
								<option value="">по дате</option>
								<option value="foraccess">по доступу</option>
								<option value="">по названию</option>
								<option value="">по месту издательства</option>
							</select>
							<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
							<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
								<option value="">Свободный доступ</option>
								<option value="">Частичный доступ</option>
							</select>
							<a href="#" class="b-searchdelrow b-searchaddrow"><span class="b-searchaddrow_minus">-</span><span class="b-searchaddrow_lb js_delsearchrow">удалить условие</span></a>
							<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
						</div>
						<div class="b_search_row visiblerow">
							<select name="logic[0]" id="" class="js_select ddl_logic ">
								<option value="">или</option>
								<option value="">и</option>
								<option value="">не</option>
							</select>
							<select name="theme[0]" id="" class="js_select ddl_theme">
								<option value="">по всем полям</option>
								<option value="">по дате</option>
								<option value="foraccess">по доступу</option>
								<option data-asrc="authors.php?session=MY_SESSION&moreparams=MORE_PARAMS" value="">по названию</option>
								<option data-asrc="search.php?session=MY_SESSION&moreparams=MORE_PARAMS" value="">по месту издательства</option>
							</select>
							<input type="text" name="text[0]" class="b-text b_search_txt" id="" >
							<select name="taccess" id="" class="js_select b-access hidden">
								<option value="">Свободный доступ</option>
								<option value="">Частичный доступ</option>
							</select>
							<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

							<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
						</div>						

						<div class="b_search_date">
							<div class="b_search_datelb iblock">Дата публикации</div>
							<input class="b-text" type="text" id="js_searchdate_prev" value="1700"  />
							<div class="b_searchslider iblock js_searchslider"></div>
							<input class="b-text"  type="text" id="js_searchdate_next" value="2014" />

						</div>
						<div class="b_search_row clearfix">
							<div class="checkwrapper right">
								<input class="checkbox" type="checkbox" name="" id="cb39"><label for="cb39" class="black">искать в авторефератах и диссертациях</label> <br/>
								<input class="checkbox" type="checkbox" name="" id="cb40"><label for="cb40" class="black">Искать по полному тексту издания</label>
							</div>
							<input type="submit" class="formbutton" value="Принять">

						</div>
					</div>
				</div><!-- /.b-search_ex -->
			</form>		

			<div class="b-search wrapper">

			</div> <!-- /.b-search -->

			<div class="b-search_ex">
				<div class="wrapper rel">
					<div class="b_search_row js_search_row hidden" >
						<select name="logic" disabled="disabled" id="" class="ddl_logic">
							<option value="">или</option>
							<option value="">и</option>
							<option value="">не</option>
						</select>
						<select name="theme" disabled="disabled" id="" class="ddl_theme">
							<option value="">по всем полям</option>
							<option value="">по дате</option>
							<option value="foraccess">по доступу</option>
							<option value="">по названию</option>
						</select>
						<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
						<select name="taccess" disabled="disabled" id="" class="b-access hidden">
							<option value="">Свободный доступ</option>
							<option value="">Частичный доступ</option>
						</select>
						<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
					</div>
					<div class="b_search_row visiblerow">
						<select name="logic[0]" id="" class="js_select ddl_logic ">
							<option value="">или</option>
							<option value="">и</option>
							<option value="">не</option>
						</select>
						<select name="theme[0]" id="" class="js_select ddl_theme">
							<option value="">по всем полям</option>
							<option value="">по дате</option>
							<option value="foraccess">по доступу</option>
							<option value="">по названию</option>
						</select>
						<input type="text" name="text[0]" class="b-text b_search_txt" id="">
						<select name="taccess" id="" class="js_select b-access hidden">
							<option value="">Свободный доступ</option>
							<option value="">Частичный доступ</option>
						</select>
						<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

						<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
					</div>						

					<div class="b_search_date">
						<div class="b_search_datelb iblock">Дата публикации</div>
						<div class="b_searchslider iblock js_searchslider"></div>
						<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
						<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

					</div>
					<div class="b_search_row clearfix">
						<div class="checkwrapper right">
							<input class="checkbox js_btnsub" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
						</div>
						<div class="checkwrapper right">
							<input class="checkbox" type="checkbox" name="" id="cb39"><label for="cb39" class="black">искать в авторефератах и диссертациях</label>
						</div>
						<input type="submit" class="formbutton" value="Принять">

					</div>
				</div>
			</div><!-- /.b-search_ex -->


		</section>

		<div class="innersection innerwrapper clearfix">
			
				<div class="clearfix">
		<div class="b-mainblock left">
			<div class="b-searchresult">
				<span class="b-searchresult_label iblock">Найдено 4 403 результата ( <a href="#">4444</a> экземпляров изданий ).</span>
				<!--<ul class="b-searchresult_list iblock">
					<li><a href="#" class="current">43 <span class="under">книги</span></a></li>
					<li><a href="#">2 <span class="under">автора</span></a></li>
					<li><a href="#">25 <span class="under">упоминаний</span></a></li>
					<li><a href="#">1 <span class="under">видео</span></a></li>
					<li><a href="#">3 <span class="under">аудиокниги</span></a></li>
				</ul>	-->		                  
			</div><!-- /.b-searchresult-->
			<div class="b-searchresult_info rel">
				<div class="b-search_save">
					<form action="searchsave.json" class="searchsave_form" method="POST">
						<input type="hidden" id="ID_search" name="ID_search">
						<input type="hidden" id="ID_name" name="ID_name">
						<span class="b-searchresult_save rel">
							<input type="submit" class="b-searchresult_bt" value="Сохранить поисковый запрос">
						</span>

					</form>
					<div class="b-searchsave_popup">
						<a href="#" class="close"></a>
						<p>Поисковый запрос сохранен</p>
						<div class="b-search_tb">
							<input placeholder="Добавить название" type="text" name="" class="input" id="">
						</div>

					</div>
				</div>
				<div class="b-searchresult_line iblock">Станислав Лем</div><!--<a href="#" class="b-searchresult_more">уточнить поиск</a>-->
				<div class="b-searchresult_portal">
					<span class="b-searchresult_lb">Вы искали на портале:</span> <span class="b-searchresult_tag">Период с 1900 по 2000 год <a href="#" class="del"></a></span> и  <span class="b-searchresult_tag">Автора <a href="#" class="del"></a></span> 
				</div>
			</div>
		</div>
		<a href="#" class="set_opener iblock right">Настройки</a>
		<div class="b-side right">
			<div class="b-searchparam">
				<div class="checkwrapper">
					<label for="cb1" class="black">По порталу</label><input class="checkbox" type="checkbox" name="" id="cb1">
				</div>
				<div class="checkwrapper">
					<label for="cb2" class="black">По библиотекам</label><input class="checkbox" type="checkbox" name="" id="cb2">
				</div>
			</div><!-- /.b-searchparam -->
		</div><!-- /.b-side -->
	</div>
	<div class="b-mainblock">

	<div class="b-filter js_filter">
			<div class="b-filter_wrapper">	
				<a href="#" class="sort sort_opener">Сортировать</a>
				<span class="sort_wrap">
				<a href="#" class="sort up">По автору</a>
				<a href="#" class="sort">По названию</a>
				<a href="#" class="sort">По дате</a>
				</span>
				<span class="b-filter_act">
					<a href="#" title="Отобразить результаты поиска плиткой" class="b-filter_items"></a>
					<a href="#" title="Отобразить результаты поиска списком" class="b-filter_list current"></a>
					<span class="b-filter_show">Показать</span>
					<a href="#" class="b-filter_num b-filter_num_paging">10</a>
					<a href="#" class="b-filter_num b-filter_num_paging ">25</a>
					<a href="#" class="b-filter_num b-filter_num_paging">50</a>
					<a href="#" title="Показать все результаты поиска на странице" class="b-filter_num current">&hellip;</a>
				</span>
			</div>
		</div><!-- /.b-filter -->
		
	</div><!-- /.b-mainblock -->
		</div><!-- /.innersection -->



	</div>
</div>
