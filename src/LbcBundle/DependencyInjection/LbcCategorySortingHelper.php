<?php
/**
 * User: agolodkov
 * Date: 26.09.2016
 * Time: 15:20
 */

namespace LbcBundle\DependencyInjection;


use Doctrine\Bundle\DoctrineBundle\Registry;
use LbcBundle\Entity\AbstractLbcCategory;
use LbcBundle\Entity\LbcCategorySorting;
use LbcBundle\Entity\LbcMaindivision;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LbcCategorySortingHelper
{

    /**
     * @var Registry
     */
    private $_doctrine;
    /**
     * @var LbcCategory
     */
    private $_categoryHelper;

    /**
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine, LbcCategory $category)
    {
        $this->_doctrine = $doctrine;
        $this->_categoryHelper = $category;
    }

    /**
     * @param AbstractLbcCategory[] $entities
     */
    public function sortEntities(array &$entities)
    {
        $categoryMap = [];
        foreach ($entities as $key => $entity) {
            $categoryMap[$entity->getId()] = $key;
        }
        $sortingRepository = $this->_doctrine->getRepository('LbcBundle:LbcCategorySorting');
        $sortEntities = $sortingRepository->findBy(['categoryId' => array_keys($categoryMap)]);
        if (!empty($sortEntities)) {
            foreach ($sortEntities as $sort) {
                $categoryId = $sort->getCategoryId();
                $categoryMap[$categoryId] = $sort->getSort();
            }
            usort(
                $entities,
                function (AbstractLbcCategory $a, AbstractLbcCategory $b) use ($categoryMap) {
                    $a = $a->getId();
                    $b = $b->getId();
                    $a = isset($categoryMap[$a]) ? $categoryMap[$a] : 0;
                    $b = isset($categoryMap[$b]) ? $categoryMap[$b] : 0;

                    return ($a < $b) ? -1 : 1;
                }
            );
        }
    }


    /**
     * @param string $categoryType
     * @param int    $categoryId
     */
    public function generateCategorySorting($categoryType, $categoryId)
    {
        $sortingMap = $this->_buildCategorySort($categoryType, $categoryId);

        $em = $this->_doctrine->getManager();
        $repositorySorting = $em->getRepository('LbcBundle:LbcCategorySorting');
        $sortingEntities = $repositorySorting->findBy(['categoryId' => $categoryId]);
        foreach ($sortingEntities as $entitySort) {
            $categoryId = $entitySort->getCategoryId();
            if (isset($sortingMap[$categoryId])) {
                unset($sortingMap[$categoryId]);
            }
        }

        $this->_applySorting($categoryType, $sortingMap);
    }

    /**
     * @param string $categoryType
     * @param int    $categoryId
     * @param string $direction
     */
    public function moveCategory($categoryType, $categoryId, $direction = 'up')
    {
        $repository = $this->_categoryHelper->getCategoryRepository($categoryType);
        /** @var AbstractLbcCategory $entity */
        $entity = $repository->find($categoryId);
        $sortingMap = $this->_buildCategorySort($categoryType, $entity->getParentid());
        asort($sortingMap, SORT_NUMERIC);

        $canMove = true;
        end($sortingMap);
        $lastKey = key($sortingMap);
        reset($sortingMap);
        if ('up' === $direction && $categoryId == key($sortingMap)) {
            $canMove = false;
        }
        if ('down' === $direction && $categoryId == $lastKey) {
            $canMove = false;
        }
        if (isset($sortingMap[$categoryId]) && $canMove) {
            $sortingValue = $sortingMap[$categoryId];
            while (key($sortingMap) != $categoryId) {
                next($sortingMap);
            }
            if ('up' === $direction) {
                prev($sortingMap);
            } else {
                next($sortingMap);
            }
            $prevId = key($sortingMap);
            $sortingMap[$categoryId] = $sortingMap[$prevId];
            $sortingMap[$prevId] = $sortingValue;

            $this->_applySorting($categoryType, $sortingMap);
        }
    }

    /**
     * @param $categoryType
     * @param $sortingMap
     */
    private function _applySorting($categoryType, $sortingMap)
    {
        $categoryIds = [];
        foreach ($sortingMap as $categoryId => $sortItem) {
            $categoryIds[] = $categoryId;
        }
        if (!empty($categoryIds)) {
            $entitiesSorting = $this->_doctrine
                ->getManager()
                ->getRepository('LbcBundle:LbcCategorySorting')
                ->findBy(
                    [
                        'category'   => $categoryType,
                        'categoryId' => $categoryIds,
                    ]
                );
            foreach ($entitiesSorting as $key => $entity) {
                $entitiesSorting[$entity->getCategoryId()] = $entity;
                unset($entitiesSorting[$key]);
            }
        }
        if (!empty($sortingMap)) {
            $em = $this->_doctrine->getManager();
            foreach ($sortingMap as $categoryId => $sortingValue) {
                if (isset($entitiesSorting[$categoryId])) {
                    $sortingEntity = $entitiesSorting[$categoryId];
                    $sortingEntity->setSort($sortingValue);
                    $em->merge($sortingEntity);
                } else {
                    $sortingEntity = new LbcCategorySorting();
                    $sortingEntity->setCategory($categoryType);
                    $sortingEntity->setCategoryId($categoryId);
                    $sortingEntity->setSort($sortingValue);
                    $em->persist($sortingEntity);
                }
            }
            $em->flush();
            $em->clear();
        }
    }

    private function _buildCategorySort($categoryType, $categoryId)
    {
        $sortingMap = [];
        $repository = $this->_categoryHelper->getCategoryRepository($categoryType);
        /** @var AbstractLbcCategory[] $entities */
        $entities = $repository->findBy(
            ['parentid' => $categoryId],
            [
                'iscommon' => 'desc',
                'code'     => 'asc',
            ]
        );
        $sort = 10;

        foreach ($entities as $key => $entity) {
            $sortingMap[$entity->getId()] = $sort;
            $sort += 10;
        }
        if (!empty($sortingMap)) {
            $sortingEntities = $this->_doctrine->getManager()
                ->getRepository('LbcBundle:LbcCategorySorting')
                ->findBy(
                    [
                        'category'   => $categoryType,
                        'categoryId' => array_keys($sortingMap),
                    ]
                );
            foreach ($sortingEntities as $entity) {
                $sortingMap[$entity->getCategoryId()] = $entity->getSort();
            }
        }


        return $sortingMap;
    }

    /**
     * @param string $categoryType
     * @param int    $categoryId
     * @param int    $sort
     */
    public function changeCategorySorting($categoryType, $categoryId, $sort)
    {
        $em = $this->_doctrine->getManager();
        $repositorySorting = $em->getRepository('LbcBundle:LbcCategorySorting');
        if (!$sortingEntity = $repositorySorting->findOneBy(
            [
                'category'   => $categoryType,
                'categoryId' => $categoryId
            ]
        )
        ) {
            $sortingEntity->setSort($sort);
            $em->merge($sortingEntity);
        } else {
            $sortingEntity = new LbcCategorySorting();
            $em->persist($sortingEntity);
        }
        $em->flush();
        $em->clear();
    }
}