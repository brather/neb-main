<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? $APPLICATION->SetTitle(""); ?>

<div class="container v-padding">
    <div class="button-bar" role="toolbar">
    	<a href="<?=$APPLICATION->GetCurDir()?>" class="btn btn-default">Вернуться на главную</a>
    	<br/><a href="javascript:history(-1)" class="btn btn-default">К результатам поиска</a>
    </div>
    

	<h1>Пушкин запись номер 1</h1>
	
	<p class="h4">О произведении</p>
	<dl class="data-dl" id="itemidlabel" aria-hidden="true">
		<dt>Автор</dt>
		<dd>Пушкин А.С.</dd>
		<dt>Заглавие</dt>
		<dd>Пушкин запись номер 1</dd>
		<dt>Подзаглавие</dt>
		<dd>(В память пятидесятилетия) : С рис. памятника, предисл. и ст.: "Последние дни А.С. Пушкина по рассказам очевидцев"Портр. на тит. л. и на обл. типа Райта, грав. на Р. Видмар, И. В. Малиновский, С. С. Есаков, В. Д. Вольховский, А. Д. Илличевский, И. И. Пущин, А. А. Дельвиг, А. С. Пушкин и неизвестный лицеист - С. С. Фролову. 4 апреля 1817 г. Царское Село</dd>
		<dt>Год издания</dt>
		<dd>1909</dd>
		<dt>Количество страниц</dt>
		<dd>225</dd>
		<dt>Библиотека</dt>
		<dd>Российская государственная библиотека (РГБ)</dd>
		<dt>Входит в состав</dt>
		<dd><a href="#">Собрание сочинений Текст / Пушкин Статьи и заметки. Стихотворения 1829 г. Черновые наброски 1829 г. Стихотворения 1830 г. Наброски и отрывки. Стихотворения 1831-1834 гг.</a></dd>
		<dt>Доступ</dt>
		<dd>Открытый доступ</dd>
	</dl>
	<div class="button-bar">
		<button class="btn btn-primary" aria-describedby="itemidlabel">Читать</button>
	</div>

</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>