<?php
/**
 * User: agolodkov
 * Date: 17.08.2016
 * Time: 16:53
 */

namespace LbcBundle\Controller\Api;


use LbcBundle\Form\LbcLocationplansectionsType;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("app.api.locationplansections")
 */
class LbcLocationplansectionsController extends AbstractCategoryController
{
    /**
     * @return LbcLocationplansectionsType
     */
    public function getFormType()
    {
        return new LbcLocationplansectionsType();
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcLocationplansections';
    }

    /**
     * @return string
     */
    public function getCategoryOwnerFieldName()
    {
        return 'locationplanid';
    }
}