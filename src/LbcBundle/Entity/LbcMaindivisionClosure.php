<?php
/**
 * User: agolodkov
 * Date: 08.08.2016
 * Time: 16:08
 */

namespace LbcBundle\Entity;

use Gedmo\Tree\Entity\MappedSuperclass\AbstractClosure;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="lbc_maindivision_closure")
 * @ORM\Entity
 */
class LbcMaindivisionClosure extends AbstractClosure
{

}