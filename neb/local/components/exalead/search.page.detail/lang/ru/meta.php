<?

$MESS['META_INFO_AUTHOR'] = 'Автор';
$MESS['META_INFO_TITLE'] = 'Заглавие';
$MESS['META_INFO_SUBTITLE'] = 'Подзаглавие';
$MESS['META_INFO_RESPONSIBILITY'] = 'Ответственность';
$MESS['META_INFO_PUBLISH_PLACE'] = 'Место издания';
$MESS['META_INFO_PUBLISH'] = 'Издательство';
$MESS['META_INFO_YEAR'] = 'Год издания';
$MESS['META_INFO_PAGES'] = 'Количество страниц';
$MESS['META_INFO_SERIE'] = 'Серия';
$MESS['META_INFO_NOTE_CONTENT'] = 'Примечание содержания';
$MESS['META_INFO_BOOK_NOTE'] = 'Общее примечание';
$MESS['META_INFO_ANNOTATION'] = 'Аннотация';
$MESS['META_INFO_BBK'] = 'ББК';
$MESS['META_INFO_BBKFULL'] = 'ББК рубрики';
$MESS['META_INFO_UDC'] = 'УДК';
$MESS['META_INFO_UDCFULL'] = 'УДК рубрики';
$MESS['META_INFO_EDITION'] = 'Редакция';
$MESS['META_INFO_LIB'] = 'Библиотека';
$MESS['META_INFO_LANGUAGE'] = 'Язык';

$MESS['META_INFO_FUND_NUMBER'] = 'Номер фонда';
$MESS['META_INFO_FUND_NAME'] = 'Название фонда';
$MESS['META_INFO_INVENTORY_NUMBER'] = 'Номер описи';
$MESS['META_INFO_INVENTORY_NAME'] = 'Заголовок описи';
$MESS['META_INFO_UNIT_NUMBER'] = 'Номер дела';
$MESS['META_INFO_UNIT_NAME'] = 'Заголовок дела';
$MESS['META_INFO_DATES'] = 'Крайние даты';

$MESS['META_INFO_DESCRIPTION'] = 'Описание предмета';
$MESS['META_INFO_MATERIAL'] = 'Материал, техника исполнения';
$MESS['META_INFO_SIZE'] = 'Размеры';

$MESS['META_INFO_MPK'] = 'МПК';
$MESS['META_INFO_DECLARANT'] = 'Заявитель';
$MESS['META_INFO_PATENTHOLDER'] = 'Патентообладатель';
$MESS['META_INFO_REPRESENTATIVE'] = 'Представитель';
$MESS['META_INFO_PUBLISH_DATE'] = 'Дата публикации';
$MESS['META_INFO_PATENTCODE'] = 'Код вида документа';
$MESS['META_INFO_COLLECTION'] = 'Коллекция';

$MESS['META_INFO_BOOK'] = 'книга';
$MESS['META_INFO_PATENT'] = 'патентный документ';
$MESS['META_INFO_PATENT_DL'] = 'скачать описание патента в pdf';
$MESS['META_INFO_READ'] = 'читать онлайн, скачать книгу в pdf';
$MESS['META_INFO_WATCH'] = 'смотреть онлайн';
$MESS['META_INFO_LISTEN'] = 'слушать онлайн';
$MESS['META_INFO_KW'] = ', бесплатно, без регистрации, скачать бесплатно';