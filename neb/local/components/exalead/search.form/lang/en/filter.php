<?php

$MESS['EXTENDED_SEARCH_ALL_SECTION_SEARCH']           = 'Search category';
$MESS['EXTENDED_SEARCH_ALL_SECTION_LIBS']             = 'Libraries';
$MESS['EXTENDED_SEARCH_ALL_SECTION_MUSEUMS']          = 'Museums';
$MESS['EXTENDED_SEARCH_ALL_SECTION_ARCHIVES']         = 'Archives';

$MESS['EXTENDED_SEARCH_ALL_SEARCH_TYPE']              = 'Search type';
$MESS['EXTENDED_SEARCH_ALL_SEARCH_SIMPLE']            = 'Simple search';
$MESS['EXTENDED_SEARCH_ALL_SEARCH_EXENDED']           = 'Extended search';

$MESS['EXTENDED_SEARCH_ALL_FULLQUERY']                = 'Enter the query';
$MESS['EXTENDED_SEARCH_ALL_AUTHOR']                   = 'Author';
$MESS['EXTENDED_SEARCH_ALL_AUTHOR_PH_LIBRARY']        = 'Pushkin';
$MESS['EXTENDED_SEARCH_ALL_AUTHOR_PH_MUSEUM']         = 'Dmitriev Nikolay';
$MESS['EXTENDED_SEARCH_ALL_TITLE']                    = 'Title';
$MESS['EXTENDED_SEARCH_ALL_TITLE_PH_LIBRARY']         = 'Good fish';
$MESS['EXTENDED_SEARCH_ALL_TITLE_PH_MUSEUM']          = 'Portrait of Emperor Alexander III';
$MESS['EXTENDED_SEARCH_ALL_TITLE_PH_ARCHIVE']         = 'Revival Newspaper';

$MESS['EXTENDED_SEARCH_ALL_DATE']                     = 'Publish date';
$MESS['EXTENDED_SEARCH_ALL_LANGUAGE']                 = 'Language';

$MESS['EXTENDED_SEARCH_ALL_DECLARANT']                = 'Declarant';
$MESS['EXTENDED_SEARCH_ALL_DECLARANT_PH_LIBRARY']     = 'Центральное конструкторское бюро';
$MESS['EXTENDED_SEARCH_ALL_PATENTHPLDER']             = 'Patent holder';
$MESS['EXTENDED_SEARCH_ALL_PATENTHPLDER_PH_LIBRARY']  = '';

$MESS['EXTENDED_SEARCH_LIB_ACCESS']                   = 'Access to publicatios';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_ALL_NAME']          = 'All';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_ALL_SCREEN']        = 'All publications';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_OPEN_NAME']         = 'Open';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_OPEN_SCREEN']       = 'Access to open publications';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_CLOSED_NAME']       = 'Copyrighted publications';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_CLOSED_SCREEN']     = 'Access to copyrighted publications';

$MESS['EXTENDED_SEARCH_COLLECTIONS']                  = 'Collections';
$MESS["EXTENDED_SEARCH_CHOISE_COLLECTION"]            = 'choose collections';

$MESS['EXTENDED_SEARCH_LIB_TEXT']                     = 'Text';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA']              = 'Search area';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA_EBOOK_NAME']   = 'Catalog of e-books';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA_EBOOK_SCREEN'] = 'Catalog of e-books';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA_BOOK_NAME']    = 'Catalog of books';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA_BOOK_SCREEN']  = 'Catalog of books';
$MESS['EXTENDED_SEARCH_LIB_PUBLISHING']               = 'Publisher';
$MESS['EXTENDED_SEARCH_LIB_PUBLISHING_PH']            = 'Russian State Library';
$MESS['EXTENDED_SEARCH_LIB_PUBPLACE']                 = 'Publish place';
$MESS['EXTENDED_SEARCH_LIB_PUBPLACE_PH']              = 'Moscow';
$MESS['EXTENDED_SEARCH_LIB_LIBRARY']                  = 'Library';
$MESS['EXTENDED_SEARCH_LIB_LIBRARY_PH']               = 'RGB';
$MESS['EXTENDED_SEARCH_LIB_ISBN']                     = 'ISBN';
$MESS['EXTENDED_SEARCH_LIB_ISBN_PH']                  = '226611156';
$MESS['EXTENDED_SEARCH_LIB_BBK']                      = 'BBK code';
$MESS['EXTENDED_SEARCH_LIB_BBK_PH']                   = '83.3Р1';
$MESS['EXTENDED_SEARCH_SECTION_BBK']                  = 'Section BBK';
$MESS['EXTENDED_SEARCH_SECTION_BBK_PH']               = 'Biologic';
$MESS['EXTENDED_SEARCH_LIB_UD']                       = 'UD code';
$MESS['EXTENDED_SEARCH_LIB_UD_PH']                    = '811.14';
$MESS['EXTENDED_SEARCH_BBK_SECTIONS']                 = 'Sections';

$MESS['EXTENDED_SEARCH_MUSEUM_MUSEUM']                = 'Museum';
$MESS['EXTENDED_SEARCH_MUSEUM_MUSEUM_PH']             = 'The State Historical Museum';
$MESS['EXTENDED_SEARCH_MUSEUM_MATERIALS']             = 'Material';
$MESS['EXTENDED_SEARCH_MUSEUM_MATERIALS_PH']          = 'Canvas, oil';
$MESS['EXTENDED_SEARCH_MUSEUM_INVERNUMBER']           = 'Inventory number';
$MESS['EXTENDED_SEARCH_MUSEUM_INVERNUMBER_PH']        = 'I 5964';

$MESS['EXTENDED_SEARCH_ARCHIVE_ARCHIVE']              = 'Archive';
$MESS['EXTENDED_SEARCH_ARCHIVE_ARCHIVE_PH']           = 'State Archive of the Russian Federation (State Archive)';
$MESS['EXTENDED_SEARCH_ARCHIVE_FOUND']                = 'Fund';
$MESS['EXTENDED_SEARCH_NUMBER_FOUND']                 = 'fund title';
$MESS['EXTENDED_SEARCH_ARCHIVE_NAME']                 = 'title';
$MESS['EXTENDED_SEARCH_ARCHIVE_NUMBER']               = 'number';
$MESS['EXTENDED_SEARCH_ARCHIVE_FOUNDNUMBER_PH']       = 'Р5850';
$MESS['EXTENDED_SEARCH_ARCHIVE_NAME_PH']              = 'Редакция газеты Возрождение';
$MESS['EXTENDED_SEARCH_ARCHIVE_INVENTORY']            = 'Inventory';
$MESS['EXTENDED_SEARCH_ARCHIVE_CASE']           	  = 'Case';
$MESS['EXTENDED_SEARCH_ARCHIVE_INVNAME_PH']           = 'Опись дел личного происхождения';
$MESS['EXTENDED_SEARCH_ARCHIVE_UNITNAME_PH']          = 'Газета возрождение';

$MESS['EXTENDED_SEARCH_DEBUG_MODE']                   = 'Debug mode';