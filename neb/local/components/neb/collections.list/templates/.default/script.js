$(function(){
    var cls = {
        arrow   : '.slick-arrow',
        prev    : 'clsp__editions-list-prev',
        next    : 'clsp__editions-list-next',
        contain : '.clsp__details',
        list    : '.clsp__editions-list',
        item    : '.clsp__editions-list-item',
        slide   : '.slick-slide',
        cloned  : '.slick-cloned',
        active  : 'slick-active'
    };

    $(cls.item).find('a > img').tooltip();

    $(cls.list).on('afterChange', function(event, slick, currentSlide){
        var indexId = $(this).attr('data-slide-editions');
        var $this = $(this),
            contain = $this.closest('li'),
            send = {
                maxSlickIndex   : slick.slideCount - 1,
                countAll        : $this.data('count-all'),
                countInSlide    : $this.data('count-in-slide'),
                sectionId       : $this.data('section-id'),
                iblockId        : $this.data('iblock-id'),
                items           : []
            },
            indexItem = 0;
        // console.log( indexId );
        $this.find(cls.item).not(cls.cloned).each(function(i,y){
            send.items[indexItem] = $(y).data('book-id');
            indexItem++;
        });
        if(send.countAll > slick.slideCount) {
            startLoader(contain,indexId);
            // console.log('startLoader(contain); index = ', indexId);
            $.ajax({
                type : 'post',
                data : send,
                url : '/local/tools/collections/loadBooks.php',
                success : function(data){
                    
                    if ( $(data).length ) {
                        $this.slick('slickAdd', data);
                    }
                    // console.log('stopLoader(); index = ', indexId);
                    stopLoader(indexId);
                }
            });
        }
    });
});
//region Функция запуска лоадера
function startLoader(block,index) {
    var blkLoader = '<div id="blk-loader'+index+'" class="blk-milk-shadow"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
    $(block).append(blkLoader);
}
//endregion
//region Функция остановки лоадера
function stopLoader(index) {
    $('#blk-loader'+index).animate({ 'opacity' : 0 }, 300, function(){
        $(this).remove();
    });
}
//endregion