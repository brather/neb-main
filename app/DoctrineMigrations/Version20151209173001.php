<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151209173001 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->addSql(
            <<<SQL
DROP TABLE IF EXISTS neb_reader_session;
SQL
        );
        $this->addSql(
            <<<SQL
CREATE TABLE IF NOT EXISTS `neb_reader_session` (
  `ID`        VARCHAR(255)
              COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` TIMESTAMP               NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID`   VARCHAR(100)
              COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`, `BOOK_ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql(
            <<<SQL
            DROP TABLE neb_reader_session;
SQL
        );
    }
}
