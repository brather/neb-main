
<?
$pagename='';
?>
<? include("header_main_auth.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-searchresult noborder hidden">
		<ul class="b-profile_nav">
			<li>
				<a href="#" class="b-profile_navlk js_profilemenu current">личный кабинет</a>
				<ul class="b-profile_subnav">
					<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
					<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
				</ul>
			</li>
			<li><a class="b-profile_nav_lb" href="#">моя библиотека</a></li>
			<li><a href="#" class="b-profile_nav_qt">цитаты</a></li>
			<li><a href="#" class="b-profile_nav_bm ">закладки</a></li>
			<li><a href="#" class="b-profile_nav_notes">заметки</a></li>
			<li><a href="#" class="b-profile_nav_search">поисковые запросы</a></li>
		</ul>                 
	</div><!-- /.b-searchresult-->
	
	<div class="b-registration rel">
		
		<h2 class="mode">получение единого электронного читательского билета. Шаг 2 из 3</h2>
		<form action="." class="b-form b-form_common b-regform">
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings01">Паспорт</label>
					<div class="field validate">
						<em class="error required">Заполните</em>
						<div class="field iblock"><input type="text"  data-required="required" value="" id="settings01"  name="serial" class="input w110">	</div>									
						<div class="field iblock"><input type="text"  data-required="required" value="" id="settings02" name="number" class="input w190">	</div>									
					</div>
				</div>
			</div>
			<div class="b-form_header">
				<span class="iblock">Адрес регистрации</span><span class="iblock">Адрес проживания</span>
				<div class="checkwrapper">
					<input class="checkbox addrindently" type="addr" name="addr" id="cb3"><label for="cb3" class="black">по месту регистрации</label>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings03">Индекс <span class="minscreen">регистрации</span></label>
					<div class="field validate w140">
						<em class="error required">Заполните</em>
						<em class="error number">Некорректный почтовый индекс</em>
						<input type="text" data-validate="number" data-required="required" value="" id="settings03" name="index" class="input ">					
					</div>
				</div>
				<div class="fieldcell iblock inly">
					<label for="settings04">Индекс <span class="minscreen">проживания</span></label>
					<div class="field validate w140">
						<input type="text" data-validate="number" value="" id="settings04" name="index" class="input">					
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings05">Город <span class="minscreen">регистрации</span></label>
					<div class="field validate">
						<em class="error required">Заполните</em>
						<input type="text" data-required="required" value="" id="settings05" data-maxlength="30" data-minlength="2" name="city" class="input" >										
					</div>
				</div>
				<div class="fieldcell iblock inly">
					<label for="settings06">Город <span class="minscreen">проживания</span></label>
					<div class="field validate">
						<input type="text" value="" id="settings06" data-maxlength="30" data-minlength="2" name="city" class="input" >										
					</div>
				</div> 
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock ">
					<em class="hint">*</em>
					<label for="settings07">Улица <span class="minscreen">регистрации</span></label>
					<div class="field validate">
						<em class="error required">Заполните</em>
						<input type="text" value="" id="settings07" data-maxlength="30" data-minlength="2"data-required="true"  name="street" class="input">					
					</div>
				</div>
				<div class="fieldcell iblock inly">
					<label for="settings08">Улица <span class="minscreen">проживания</span></label>
					<div class="field validate">
						<input type="text" value="" id="settings08" data-maxlength="30" data-minlength="2" name="street" class="input">					
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<div class="field iblock fieldthree ">
						<label for="settings09">Дом:</label>
						<div class="field validate">
							<input type="text" class="input" name="house" data-maxlength="20" id="settings09" value=""  data-required="true"/>
							<em class="error required">Заполните</em>
						</div>
					</div>
					<div class="field iblock fieldthree">
						<label for="settings10">Строение: </label>
						<div class="field">
							<input type="text" class="input" name="flat" data-maxlength="20" maxlength="4" id="settings10" value="" />
						</div>
					</div>
					<div class="field iblock fieldthree">
						<label for="settings11">Квартира: </label>
						<div class="field">
							<input type="text" class="input" name="flat" data-maxlength="20" maxlength="4" id="settings11" value="" />
						</div>
					</div>
				</div>
				<div class="fieldcell iblock inly">
					<div class="field iblock fieldthree ">
						<label for="settings09">Дом:</label>
						<div class="field validate">
							<input type="text" class="input" name="house" data-maxlength="20" id="settings09" value=""/>
						</div>
					</div>
					<div class="field iblock fieldthree">
						<label for="settings10">Строение: </label>
						<div class="field">
							<input type="text" class="input" name="flat" data-maxlength="20" maxlength="4" id="settings10" value="" />
						</div>
					</div>
					<div class="field iblock fieldthree">
						<label for="settings11">Квартира: </label>
						<div class="field">
							<input type="text" class="input" name="flat" data-maxlength="20" maxlength="4" id="settings11" value="" />
						</div>
					</div>
				</div>
			</div>
			
			<hr>
			<div class="fieldcell nowrap photorow">
				<label>Загрузить скан разворота паспорта с  пропиской </label>
				<div class="field validate">
					<div class="setscan iblock">
						<input type="file" class="photofile" name="" id="" data-required="true">
						<a href="#">Загрузить изображение</a>
						<div class="setphoto_lb">или перетащите его на это поле</div>
						<div class="progressbar"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружается <span class="num">33</span>%</div></div>
					</div>
					
					<div class="b-scan_photo iblock">
						<img src="./pic/scan.jpg" alt="">
					</div>
					<em class="error required">Загрузите скан паспорта</em>
				</div>
			</div>
			<div class="fieldcell nowrap photorow">
				<label>Загрузить скан разворота паспорта с  пропиской </label>
				<div class="field validate">
					<div class="setscan iblock">
						
						<input type="file" class="photofile" name="" id="" data-required="true">
						<a href="#">Загрузить изображение</a>
						<div class="setphoto_lb">или перетащите его на это поле</div>
						<div class="progressbar"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружается <span class="num">33</span>%</div></div>
					</div>
					<div class="b-scan_photo iblock">
						<img src="./pic/scan.jpg" alt="">
					</div>
					<em class="error required">Загрузите скан паспорта</em>
				</div>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell">
					<div class="field clearfix">
						<a href="#" class="button_back">Назад</a>
						<button class="formbtn btdisable" value="1" type="submit" disabled="disabled">Перейти к шагу 3</button>
					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->



</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>