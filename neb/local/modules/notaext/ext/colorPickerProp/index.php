<?

class colorPickerProp extends CExt
{
    var $EXT_ID = __CLASS__;
    var $EXT_VERSION;
    var $EXT_VERSION_DATE;
    var $EXT_NAME;
    var $EXT_DESCRIPTION;
    var $EXT_SORT;

    public function __construct()
    {
        parent::__construct();

        $this->EXT_VERSION = '1.0';
        $this->EXT_VERSION_DATE = '15.05.2014';

        $this->EXT_NAME = 'Свойство ColorPicker';
        $this->EXT_DESCRIPTION = 'Свойство инфоблока для вывода поля с выбором цвета';
        $this->EXT_SORT = 1000;

    }

    function onBeforeInstall()
    {

    }

    function onBeforeUninstall()
    {

    }

    function onAfterInstall()
    {

    }

    function onAfterUninstall()
    {

    }
}
