<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * LbcSpecialdivision
 *
 * @Gedmo\Tree(type="closure")
 * @Gedmo\TreeClosure(class="LbcBundle\Entity\LbcLocationplansectionsClosure")
 * @ORM\Table(name="lbc_specialdivision")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\ClosureTreeRepository")
 */
class LbcSpecialdivision extends AbstractLbcCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="MainDivisionId", type="integer", nullable=false)
     */
    private $maindivisionid;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="LbcSpecialdivision", inversedBy="children")
     * @ORM\JoinColumn(name="parentid", referencedColumnName="Id", onDelete="CASCADE")
     */
    public $parent;

    /**
     * @ORM\OneToMany(targetEntity="LbcSpecialdivision", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    public $children;

    /**
     * Set maindivisionid
     *
     * @param integer $maindivisionid
     *
     * @return LbcSpecialdivision
     */
    public function setMaindivisionid($maindivisionid)
    {
        $this->maindivisionid = $maindivisionid;

        return $this;
    }

    /**
     * Get maindivisionid
     *
     * @return integer
     */
    public function getMaindivisionid()
    {
        return $this->maindivisionid;
    }
}
