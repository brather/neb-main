<?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arOptions = array(
    #Array("note"=>'note note'),
    Array('wWidth', GetMessage("S_WWIDTH"), '900', Array('text')),
    Array('wHeight', GetMessage("S_WHEIGHT"), '800', Array('text')),
);