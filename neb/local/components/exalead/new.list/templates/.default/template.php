<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if(empty($arResult['ITEMS'])) {
    return false;
}

?>

<section class="main-news container">
    <div class="row">

        <div class="main-news__title clearfix">
            <div class="main-news__title-icon col-md-1">
                <img src="/local/templates/adaptive/img/news.png" alt="">
            </div>
            <h3 class="col-md-11 main-news__title-header">
                <a href="/news/">Новости</a>
            </h3>
        </div>

        <div class="main-news__gallery">
            <? foreach($arResult['ITEMS'] as $key => $arItem):
                if($key >= 3) break;
            ?>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <a href="<?=$arItem['LINK']?>" class="main-news__gallery-link">
                        <img src="<?=$arItem['IMAGE_URL']?>" alt="<?= $arItem['TITLE']?>" />
                        <span><?=$arItem['TITLE']?></span>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</section>
