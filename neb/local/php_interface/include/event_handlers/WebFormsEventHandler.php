<?php
/**
 * User: agolodkov
 * Date: 21.01.2016
 * Time: 12:07
 */

use \Bitrix\Main\EventManager;
use \Bitrix\Main\Loader;
use \Nota\Exalead\SearchClient;
use \Nota\Exalead\SearchQuery;

EventManager::getInstance()->addEventHandler('form', 'onAfterResultAdd', ['WebFormsEventHandler', 'countFeedback']);
EventManager::getInstance()->addEventHandler('main', 'OnBeforeEventAdd', ['WebFormsEventHandler', 'OnBeforeEventAddHandler']);

class WebFormsEventHandler
{
    public static function countFeedback($webFormId)
    {
        if (1 === intval($webFormId) || 3 === intval($webFormId)) {
            \Neb\Main\Stat\Tracker::track('neb_feedback_count',[
                'user_id' => nebUser::getCurrent()->GetID(),
            ]);
        }
    }

    public static function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
    {
        $arFields['FB_BOOK_ID'] = trim($arFields['FB_BOOK_ID']);

        if ($event === "FORM_FILLING_SIMPLE_FORM_1" && !empty($arFields['FB_BOOK_ID']))
        {
            Loader::includeModule('nota.exalead');

            $query = new SearchQuery();
            $query->getById($arFields['FB_BOOK_ID']);

            $client = new SearchClient();
            $res = $client->getResult($query);

            $arFields['EX_BOOK_LINK'] = 'Издание: <a href="'.$_SERVER['HTTP_ORIGIN'].'/catalog/'
                .$arFields['FB_BOOK_ID'].'/">' . $res['authorbook_original'] . ': ' . $res['name'] . '</a>';
        }
    }
}
