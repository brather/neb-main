<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc,
    \Bitrix\NotaExt\Iblock\IblockTools,
    \Bitrix\Main\Application;


class FaqComplex extends CBitrixComponent {
    //region Задание свойств класса
    private $iblockId = '';
    private $page = 'template';
    private $arRequest = array();
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        if(!empty($arParams["IBLOCK_ID"])) {
            $this->iblockId = $arParams["IBLOCK_ID"];
        }
        elseif(!empty($arParams['IBLOCK_CODE'])) {
            $this->iblockId = IblockTools::getIBlockId($arParams["IBLOCK_CODE"]);
        }
        $this->arResult['IBLOCK_ID'] = $this->iblockId;
        if(!empty($arParams["ROOT_PATH"])) {
            $this->arResult["PATH"]["ROOT"] = $arParams["ROOT_PATH"];
        }
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->_getRequest();
        $this->_backToList();
        $this->_setSef();
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Метод возвращает html для кнопки "назад"
    private function _backToList() {
        if(!empty($this->arRequest['id'])) {
            $mess = Loc::getMessage('FAQ_EDIT_ITEM') . '<b>' . $this->arRequest['id'] . '</b>';
        } elseif($this->arRequest['sect_id']) {
            $mess = Loc::getMessage('FAQ_EDIT_SECT') . '<b>' . $this->arRequest['sect_id'] . '</b>';
        } else {
            $mess = '';
        }
        $this->arResult['BACK_TO_LIST'] = '
                            <p>
                                <a class="linkback-overall" href="' . $this->arParams['SEF_FOLDER'] . '">
                                    &larr; ' . Loc::getMessage('FAQ_BACK_TO_LIST') . '
                                </a>
                                <!--' . $mess . '-->
                            </p>

            ';
    }
    //endregion
    //region Определяет текущую страницу
    /**
     * Определяет текущую страницу
     */
    private function _setSef() {
        $this->page = 'template';
        $arDefaultUrlTemplates404 = array(
            "list"          => "/",
            "item"          => "item/?id=#ID#",
            "section"       => "section/?sect_id=#SECTION_ID#",
            "add_item"      => "add_item/",
            "add_section"   => "add_section/",
        );
        $arDefaultVariableAliases404 = array();
        $arDefaultVariableAliases = array();
        $arComponentVariables = array('ID', 'SECTION_ID');
        $SEF_FOLDER = "";
        $arUrlTemplates = array();
        if ($this->arParams["SEF_MODE"] == "Y") {
            $arVariables = array();
            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams["SEF_URL_TEMPLATES"]);
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $this->arParams["VARIABLE_ALIASES"]);
            $componentPage = CComponentEngine::ParseComponentPath($this->arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);
            if (StrLen($componentPage) <= 0) $componentPage = "template";
            CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
            $SEF_FOLDER = $this->arParams["SEF_FOLDER"];
        } else {
            $arVariables = array();
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $this->arParams["VARIABLE_ALIASES"]);
            CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);
            $componentPage = "";
            if (IntVal($arVariables["ELEMENT_ID"]) > 0) {
                $componentPage = "detail";
            } else {
                $componentPage = "list";
            }
        }
        $this->page = $componentPage;
    }
    //endregion
    //region Получение массива $_REQUEST (обертка D7)
    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public function _getRequest() {
        return $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }
    //endregion
}