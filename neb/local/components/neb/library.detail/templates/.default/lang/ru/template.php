<?php
$MESS['LIBRARY_DETAIL_ADDRESS'] = 'Почтовый адрес';
$MESS['LIBRARY_DETAIL_ADDRESS_MAP'] = 'Адрес';
$MESS['LIBRARY_DETAIL_WORK'] = 'График работы';
$MESS['LIBRARY_DETAIL_PHONE'] = 'Телефон';
$MESS['LIBRARY_DETAIL_EMAIL'] = 'Электронная почта';
$MESS['LIBRARY_DETAIL_CONTACT'] = 'Контактное лицо';
$MESS['LIBRARY_DETAIL_USER'] = 'Участник';
$MESS['LIBRARY_DETAIL_READMORE'] = 'Читать далее';
