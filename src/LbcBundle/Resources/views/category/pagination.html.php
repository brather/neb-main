<?
/** @var \LbcBundle\DependencyInjection\MustacheEngine $mustache */
/** @var \Symfony\Bundle\FrameworkBundle\Templating\Helper\RouterHelper $router */
$mustache = $view['mustache_engine'];
$router = $view['router'];
$data['pages'] = [];
foreach ($data['pagesInRange'] as $pageNum) {
    $page = [];
    $page['url'] = $router->path(
        $data['route'],
        array_replace($data['query'], [$data['pageParameterName'] => $pageNum])
    );
    $page['current'] = $pageNum == $data['current'];
    $page['page'] = $pageNum;
    $data['pages'][] = $page;
}
foreach (
    [
        'firstUrl'   => 'first',
        'previosUrl' => 'previos',
        'nextUrl'    => 'next',
        'lastUrl'    => 'last',
    ] as $paramName => $item
) {
    if (isset($data[$item])) {
        $data[$paramName] = $router->path(
            $data['route'],
            array_replace($data['query'], [$data['pageParameterName'] => $data[$item]])
        );
    }
}

echo $mustache->render(
    'bbk/pagination',
    $data
);