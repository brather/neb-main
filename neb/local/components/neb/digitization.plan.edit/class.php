<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application,
    \Bitrix\Main\UI\PageNavigation,
    \Bitrix\Iblock\ElementTable,
    \Bitrix\Iblock\IblockTable,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Nota\Exalead\LibraryBiblioCardTable,
    \Bitrix\Main\Loader;

class DigitizationPlanEditComponent extends CBitrixComponent {

    private $page = 'template';
    private $arRequest = array();
    private $checkPermission = true;
    private $sort = '';
    private $order = '';
    private $showDuplication = false;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }

    public function setModule() {
        Loader::includeModule('nota.exalead');
    }

    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->arRequest = self::getRequest();
        $this->_checkUserRole();
        if($this->checkPermission) {
            $this->setModule();
            $this->_setSortParam();
            $this->_getListLibrary();
            $this->_getListPlan();
            $this->_getListBooks();
            $this->_setUriDuplication();
            $this->getSortUrl();
        }
        $this->includeComponentTemplate($this->page);
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Установка переменных для сортировки
     */
    private function _setSortParam() {
        $this->sort = $this->arParams['BY_SORT_DEFAULT'];
        $this->order = $this->arParams['BY_ORDER_DEFAULT'];
        if(!empty($this->arRequest['sort'])) {
            $this->sort = $this->arRequest['sort'];
        }
        if(!empty($this->arRequest['order'])) {
            $this->order = $this->arRequest['order'];
        }
    }

    /**
     * Проверка првав доступа текущего пользователя
     */
    private function _checkUserRole() {
        $obUser   = new nebUser();
        if (!$obUser->IsAuthorized()) {
            $this->arResult['ERROR'] = Loc::getMessage('DIG_PLAN_EDIT_C_NO_AUTHORIZED');
            $this->checkPermission =false;
        }
        $arGroups = $obUser->getUserGroups();
        if(!in_array(UGROUP_OPERATOR_CODE, $arGroups)) {
            $this->arResult['ERROR'] = Loc::getMessage('DIG_PLAN_EDIT_C_NO_ROLE');
            $this->checkPermission = false;
        }
    }

    /**
     * Получим значения списка свойства типа список для HL блока (если не задан код свойства, то по умолчанию ищем статус)
     * @param $codeHLBlock - символьный код HL блока
     * @param bool $propertyCode - символьный код свойства
     * @return array
     */
    public function getListPropertyStatus($codeHLBlock, $propertyCode = false) {
        if(!$propertyCode) {
            $propertyCode = 'UF_REQUEST_STATUS';
        }
        $idBlockPlan = self::getIdHLBlock($codeHLBlock);
        $resProp = CUserTypeEntity::GetList(
            array(),
            array('FIELD_NAME' => $propertyCode, 'ENTITY_ID' => 'HLBLOCK_' . $idBlockPlan)
        )->Fetch();
        $cUserFieldEnum = new CUserFieldEnum();
        $rsUFValues = $cUserFieldEnum->GetList(array(), array('USER_FIELD_ID' => $resProp['ID']));
        $arReturn = array();
        while($obUFValues = $rsUFValues->Fetch()) {
            $arReturn[$obUFValues['ID']] = $obUFValues;
        }
        return $arReturn;
    }

    /**
     * Возвращаем массив планов на оцифровку
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getListPlan() {
        $arPropertyStatus = self::getListPropertyStatus(HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
        $this->arResult['PROPERTY_STATUS_BOOK'] = self::getListPropertyStatus(HIBLOCK_CODE_DIGITIZATION_PLAN);
        $this->arResult['PROPERTY_STATUS_PLAN'] = $arPropertyStatus;
        $arTmpStatus = array();
        foreach($this->arResult['PROPERTY_STATUS_PLAN'] as $idStatus => $arStatus) {
            $arTmpStatus[$arStatus['XML_ID']] = $arStatus;
        }
        $arProperty = array();
        foreach($arPropertyStatus as $idStatus => $arStatus) {
            $arProperty[] = $arStatus['ID'];
        }

        $dataClassPlan = self::setDataClass(HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
        $rsDataPlan = $dataClassPlan::getList(array(
            'filter' => array('UF_REQUEST_STATUS' => $arProperty),
            'select' => array('ID', 'UF_PLAN_NAME', 'UF_REQUEST_STATUS', 'UF_LIBRARY', 'UF_DATE_F_PLAN'),
            'order' => array('ID' => 'ASC')
        ));
        while($obDataPlan = $rsDataPlan->fetch()) {
            $idPlan = $obDataPlan['ID'];
            $this->arResult['LIST_PLAN'][$idPlan] = $obDataPlan;
            // Вернем список возможных статусов для плана
            $sortStatus = $this->arResult['PROPERTY_STATUS_PLAN'][$obDataPlan['UF_REQUEST_STATUS']]['SORT'];
            if($sortStatus <= $arTmpStatus['AGREED']['SORT'] || $sortStatus == $arTmpStatus['CANCELED']['SORT']) {
                $this->arResult['LIST_PLAN'][$idPlan]['LIST_STATUS'] = array(
                    'ACTIVE_SELECT' => 'Y',
                    'OPTIONS' => array(
                        $arTmpStatus['IN_AGREEING_NEL_OPERATOR']['ID'],
                        $arTmpStatus['AGREED']['ID'],
                        $arTmpStatus['CANCELED']['ID']
                    )
                );
            } elseif($sortStatus == $arTmpStatus['DIGITIZED']['SORT']) {
                $this->arResult['LIST_PLAN'][$idPlan]['LIST_STATUS'] = array(
                    'ACTIVE_SELECT' => 'Y',
                    'OPTIONS' => array(
                        $arTmpStatus['DIGITIZED']['ID'],
                        $arTmpStatus['DONE']['ID']
                    )
                );
            } else {
                $this->arResult['LIST_PLAN'][$idPlan]['LIST_STATUS'] = array(
                    'ACTIVE_SELECT' => 'N',
                    'OPTIONS' => array($arTmpStatus['DONE']['ID'])
                );
            }
        }
    }

    /**
     * Возвращаем список изданий плана
     */
    private function _getListBooks() {
        $sort = $this->sort;
        $order = $this->order;
        if(!empty($this->arRequest['idplan'])) {

            $idPlan = $this->arRequest['idplan'];

            $dataClass = self::setDataClass(HIBLOCK_CODE_DIGITIZATION_PLAN);

            $arFilterBooks = array('UF_PLAN_ID' => $idPlan);
            if (!empty($_REQUEST['AUTHOR'])) {
                $arFilterBooks['?UF_BOOK_AUTHOR'] = $_REQUEST['AUTHOR'];
            }
            if (!empty($_REQUEST['TITLE'])) {
                $arFilterBooks['?UF_BOOK_NAME'] = $_REQUEST['TITLE'];
            }
            if ($iStatus = intval($_REQUEST['STATUS'])) {
                $arFilterBooks['UF_REQUEST_STATUS'] = $iStatus;
            }
            
            $iTotalCount = $dataClass::getList([
                'filter' => $arFilterBooks,
                'select' => ['ID']
            ])->getSelectedRowsCount();

            $iPageLimit = $this->arParams['COUNT_ON_PAGE'];
            $obPageNavigation = new PageNavigation('plan');
            $obPageNavigation->allowAllRecords(true)->initFromUri();
            if($this->arRequest['plan'] == 'page-all')  {
                $iPageLimit = '';
            }
            $obPageNavigation->getLimit();

            $arAllowedStatus = array();
            foreach($this->arResult['PROPERTY_STATUS_BOOK'] as $idStatus => $arStatus) {
                if($arStatus['VALUE'] == DIGITIZATION_STATUS_DIGITIZED
                    || $arStatus['VALUE'] == DIGITIZATION_STATUS_DONE) {
                    $arAllowedStatus[] = $idStatus;
                }
            }

            if ($this->arRequest['show-duplication'] == 'off' || empty($this->arRequest['show-duplication'])) {

                $obPageNavigation->setRecordCount($iTotalCount)->setPageSize($iPageLimit);
                $iCurrentPage = $obPageNavigation->getCurrentPage() - 1;
                $startIndex = $iCurrentPage * $iPageLimit;

                $arQueryParams = array(
                    'filter' => $arFilterBooks,
                    'order' => array($sort => $order),
                    'select' => array(
                        'ID',
                        'UF_BOOK_NAME',
                        'UF_DATE_ADD',
                        'UF_BOOK_AUTHOR',
                        'UF_BOOK_YEAR_PUBLIC',
                        'UF_REQUEST_STATUS',
                        'UF_EXALEAD_BOOK',
                        'UF_LIBRARY',
                        'UF_NUMBER_FROM_READE',
                        'UF_DATE_READER',
                        'UF_COMMENT',
                    ),
                );
                if (strpos($_SERVER['REQUEST_URI'], 'page-all') === false) {
                    $arQueryParams['limit']  = $iPageLimit;
                    $arQueryParams['offset'] = $startIndex;
                }

                $rsDataBooks = $dataClass::getList($arQueryParams);
                $iBook = 0;
                while($obBooks = $rsDataBooks->fetch()) {
                    $startIndex++;
                    $this->arResult['LIST_BOOKS'][$iBook] = $obBooks;
                    $arDuplicate = $this->_checkBook($obBooks['UF_EXALEAD_BOOK'], $dataClass, $obBooks['ID']);
                    if(count($arDuplicate) > 0) {
                        $this->arResult['LIST_BOOKS'][$iBook]['DUPLICATE'] = $arDuplicate;
                        $this->showDuplication = true;
                    }
                    $this->arResult['LIST_BOOKS'][$iBook]['INDEX'] = $startIndex;
                    if(in_array($obBooks['UF_REQUEST_STATUS'], $arAllowedStatus)) {
                        $this->arResult['LIST_BOOKS'][$iBook]['DATA_EXALEAD'] = self::getFileBook($obBooks['UF_EXALEAD_BOOK']);
                    }
                    $iBook++;
                }
            }
            else {
                $arIdDuplicate = array();
                $rsDataBooks = $dataClass::getList(array(
                    'filter' => $arFilterBooks,
                    'select' => array('ID', 'UF_REQUEST_STATUS', 'UF_EXALEAD_BOOK'),
                ));
                $iBook = 0;
                $arDuplicate = array();
                while($obBooks = $rsDataBooks->fetch()) {
                    $arDuplicate[$obBooks['ID']] = $this->_checkBook($obBooks['UF_EXALEAD_BOOK'], $dataClass, $obBooks['ID']);
                    if(count($arDuplicate[$obBooks['ID']]) > 0) {
                        $arIdDuplicate[] = $obBooks['ID'];
                    }
                    $iBook++;
                }
                $arFilterBooks = array('ID' => $arIdDuplicate);
                $iTotalCount = $dataClass::getList(array(
                    'filter' => $arFilterBooks,
                    'select' => ['ID'])
                )->getSelectedRowsCount();

                $obPageNavigation->setRecordCount($iTotalCount)->setPageSize($iPageLimit);
                $iCurrentPage = $obPageNavigation->getCurrentPage() - 1;
                $startIndex = $iCurrentPage * $iPageLimit;

                $arQueryParams = array(
                    'filter' => $arFilterBooks,
                    'order' => array($sort => $order),
                    'select' => array(
                        'ID',
                        'UF_BOOK_NAME',
                        'UF_DATE_ADD',
                        'UF_BOOK_AUTHOR',
                        'UF_BOOK_YEAR_PUBLIC',
                        'UF_REQUEST_STATUS',
                        'UF_EXALEAD_BOOK',
                        'UF_LIBRARY',
                        'UF_NUMBER_FROM_READE',
                        'UF_DATE_READER',
                        'UF_DATE_READER',
                        'UF_COMMENT',
                    ),
                );
                if (strpos($_SERVER['REQUEST_URI'], 'page-all') === false) {
                    $arQueryParams['limit']  = $iPageLimit;
                    $arQueryParams['offset'] = $startIndex;
                }

                $rsDataBooks = $dataClass::getList($arQueryParams);
                $iBook = 0;
                while($obBooks = $rsDataBooks->fetch()) {
                    $startIndex++;
                    $this->arResult['LIST_BOOKS'][$iBook] = $obBooks;
                    $this->arResult['LIST_BOOKS'][$iBook]['DUPLICATE'] = $arDuplicate[$obBooks['ID']];
                    $this->showDuplication = true;
                    $this->arResult['LIST_BOOKS'][$iBook]['INDEX'] = $startIndex;
                    if(in_array($obBooks['UF_REQUEST_STATUS'], $arAllowedStatus)) {
                        $this->arResult['LIST_BOOKS'][$iBook]['DATA_EXALEAD'] = self::getFileBook($obBooks['UF_EXALEAD_BOOK']);
                    }
                    $iBook++;
                }
            }

            $arCurrentStatusPlan = $this->arResult['LIST_PLAN'][$this->arRequest['idplan']]['LIST_STATUS'];
            $arCurrentStatusBook = array('ACTIVE_SELECT' => $arCurrentStatusPlan['ACTIVE_SELECT']);
            foreach($arCurrentStatusPlan['OPTIONS'] as $idStPlan) {
                foreach($this->arResult['PROPERTY_STATUS_PLAN'] as $idStatus => $arStatus) {
                    if($arStatus['VALUE'] == $this->arResult['PROPERTY_STATUS_PLAN'][$idStPlan]['VALUE']) {
                        $arCurrentStatusBook['OPTIONS'][$idStatus] = $arStatus;
                    }
                }
            }
            $this->arResult['LIST_STATUS_BOOK'] = $arCurrentStatusBook;

            $this->arResult['NAV'] = $obPageNavigation;
        } else {
            $this->arResult['ERROR'] = Loc::getMessage('DIG_PLAN_EDIT_C_EMPTY_BOOK_ID');
        }
    }

    /**
     * Функция возвращает издание по коду fullSymbolicId из базы Exalead
     * @param $fullSymbolicId
     * @return array|bool|mixed|null|string
     */
    public function getFileBook($fullSymbolicId) {
        $biblioCards = LibraryBiblioCardTable::getByFullSymbolicId($fullSymbolicId);
        return $biblioCards[0];
    }

    /**
     * @param $exaleadId
     * @param $dataClass
     * @return array
     */
    public function _checkBook($exaleadId, $dataClass, $curId = false) {
        $arResult = array();
        $sort = $this->sort;
        $order = $this->order;
        $rsData = $dataClass::getList(array(
            'filter' => array('UF_EXALEAD_BOOK' => $exaleadId),
            'select' => array(
                'ID',
                'UF_BOOK_NAME',
                'UF_DATE_ADD',
                'UF_PLAN_ID',
                'UF_BOOK_AUTHOR',
                'UF_BOOK_YEAR_PUBLIC',
                'UF_REQUEST_STATUS',
                'UF_LIBRARY'
            ),
            'order' => array($sort => $order)
        ));
        while($obData = $rsData->fetch()) {
            if($curId) {
                if($curId != $obData['ID']) {
                    $arResult[] = $obData;
                }
            } else {
                $arResult[] = $obData;
            }
        }
        return $arResult;
    }

    /**
     * Возвращаем dataClass для работы с highload блоком изданий
     * @throws \Bitrix\Main\SystemException
     */
    public function setDataClass($codeHLBlock) {
        $hlBlockId = self::getIdHLBlock($codeHLBlock);
        $hlblock = HighloadBlockTable::getById($hlBlockId)->fetch();
        $entity = HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }

    /** Вернем список библиотек
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getListLibrary() {
        $rsBlock = IblockTable::getList(array(
            'filter' => array('CODE' => IBLOCK_CODE_LIBRARY),
            'select' => array('NAME', 'ID')
        ))->fetch();
        $rsLibrary = ElementTable::getList(array(
            'filter' => array('IBLOCK_ID' => $rsBlock['ID']),
            'select' => array('ID', 'NAME'),
            'order' => array('NAME' => 'ASC')
        ));
        while($obLibrary = $rsLibrary->fetch()) {
            $this->arResult['LIST_LIBRARY'][$obLibrary['ID']] = $obLibrary;
        }
    }

    /** Функция возвращает id HL блока по его коду
     * @param $codeHLBlock
     * @return mixed
     */
    public function getIdHLBlock($codeHLBlock) {
        $connection = Application::getConnection();
        $sql = "SELECT ID FROM b_hlblock_entity WHERE NAME='" . $codeHLBlock . "';";
        $recordset = $connection->query($sql)->fetch();
        return $recordset['ID'];
    }

    /**
     * Вернем ссылку на показ книг содержащихся в других планах на оцифровку
     */
    private function _setUriDuplication() {
        if($this->showDuplication) {
            $iReq = 0;
            $newRequest = '';
            $vDuplication = 'on';
            foreach($this->arRequest as $cCode => $vCode) {
                if($iReq == 0) {
                    $newRequest .= '?';
                } else {
                    $newRequest .= '&';
                }
                if($cCode != 'show-duplication') {
                    $newRequest .= $cCode . '=' . $vCode;
                } else {
                    if($vCode == 'on') {
                        $vDuplication = 'off';
                    }
                }
                $iReq++;
            }
            $this->arResult['DUPLICATION_URI'] = $this->arParams['SEF_FOLDER'];
            if($newRequest != '') {
                $this->arResult['DUPLICATION_URI'] .= $newRequest . '&';
            } else {
                $this->arResult['DUPLICATION_URI'] .= '?';
            }
            $this->arResult['DUPLICATION_URI'] .=  'show-duplication=' . $vDuplication;
        }
    }

    /**
     * Функция возвращает ссылку на сортировку по параметру $sort
     * @param $sort - параметр сортировки
     * @param $order - порядок сортировки
     */
    private function getSortUrl() {
        $sort = $this->sort;
        $order = $this->order;
        $arSort = array();
        // Получим все get параметры без значений сортировки
        $rString = '';
        $iString = 0;
        foreach($this->arRequest as $cReq => $vReq) {
            if($cReq != 'sort' && $cReq != 'order') {
                if($iString == 0) {
                    $rString .= '?';
                } else {
                    $rString .= '&';
                }
                $rString .= $cReq . '=' . $vReq;
                $iString++;
            }
        }
        foreach($this->arParams['BY_SORT_LIST'] as $vSort) {
            $arSort[$vSort]['URI'] = $this->arParams['SEF_FOLDER'] . $rString;
            if($rString != '') {
                $arSort[$vSort]['URI'] .= '&';
            } else {
                $arSort[$vSort]['URI'] .= '?';
            }
            if($vSort == $sort) {
                if($order == 'asc') {
                    $nOrder = 'desc';
                    $arSort[$vSort]['CLASS'] = 'sort up';
                } else {
                    $nOrder = 'asc';
                    $arSort[$vSort]['CLASS'] = 'sort down';
                }
            } else {
                $nOrder = 'asc';
            }
            $arSort[$vSort]['URI'] .= 'sort=' . $vSort . '&order=' . $nOrder;
        }
        $this->arResult['SORT_LIST'] = $arSort;
    }
}