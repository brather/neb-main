<?
/**
 * Добавление цитаты из открытого просмотровщика
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized())
    exit();

$book_id = htmlspecialcharsbx(trim($_REQUEST['book_id']));
if (empty($book_id))
    exit();

$cite = htmlspecialcharsbx(trim($_REQUEST['cite']));
$citeimg = trim($_REQUEST['citeimg']);
$citepage = intval($_REQUEST['citepage']);
$citetop = intval($_REQUEST['citetop']);
$citeleft = intval($_REQUEST['citeleft']);
$citewidth = intval($_REQUEST['citewidth']);
$citeheight = intval($_REQUEST['citeheight']);

use \Bitrix\Main\Loader,
    Nota\UserData\Quotes;

Loader::includeModule("nota.userdata");

$arFields = array(
    'BOOK_ID' => $book_id,
    'TEXT' => $cite,
    'IMG_DATA' => $citeimg,
    'PAGE' => $citepage,
    'TOP' => $citetop,
    'LEFT' => $citeleft,
    'WIDTH' => $citewidth,
    'HEIGHT' => $citeheight,
);

$id = Quotes::add($arFields);

if ($id > 0) {
    ?>
    <div class="b-hintadded hidden" data-id="<?= $id ?>">Цитата добавлена</div>
    <?
} else {
    ?>
    <div class="b-hintadded hidden" data-id="<?= rand(1000, 100000); ?>">Ошибка добавления цитаты</div>
    <?
}