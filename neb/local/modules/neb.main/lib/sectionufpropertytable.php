<?php

namespace Neb\Main\Iblock;

use \Bitrix\Main\Entity;

/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 03.02.2017
 * Time: 9:36
 */


class SectionUfPropertyTable extends Entity\DataManager
{
    /**
     * @var string Указывает на ID инфоблока
     */
    public static $IBlockID = IBLOCK_ID_COLLECTION;

    /**
     * Returns path to the file which contains definition of the class.
     *
     * @return string
     */
    public static function getFilePath()
    {
        return __FILE__;
    }

    /**
     * Returns DB table name for entity
     *
     * @param $IBlockID
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_uts_iblock_' . static::$IBlockID . '_section';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        global $DB;
        $arMap = array();

        $db = $DB->Query(
            'SELECT `COLUMN_NAME`, `DATA_TYPE`, `IS_NULLABLE`, `COLUMN_DEFAULT`, `COLUMN_KEY`
  FROM `INFORMATION_SCHEMA`.`COLUMNS`
  WHERE table_name = \'' . static::getTableName() . '\''
        );

        while ( $res = $db->Fetch() )
        {
            switch ( $res['DATA_TYPE'] )
            {
                case 'int':
                    $arMap[] = new Entity\IntegerField(
                        $res['COLUMN_NAME'],
                        array(
                            'required' => $res['IS_NULLABLE'] === 'NO',
                            'primary' => $res['COLUMN_KEY'] === 'PRI'
                        )
                    );
                    break;
                case 'double':
                    $arMap[] = new Entity\FloatField(
                        $res['COLUMN_NAME'],
                        array(
                            'required' => $res['IS_NULLABLE'] === 'NO',
                            'primary' => $res['COLUMN_KEY'] === 'PRI'
                        )
                    );
                    break;
                case 'text':
                    $arMap[] = new Entity\TextField(
                        $res['COLUMN_NAME'],
                        array(
                            'required' => $res['IS_NULLABLE'] === 'NO',
                            'primary' => $res['COLUMN_KEY'] === 'PRI'
                        )
                    );
                    break;
            }
        }

        return $arMap;
    }
}