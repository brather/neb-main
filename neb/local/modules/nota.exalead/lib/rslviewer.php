<?
namespace Nota\Exalead;

use \Bitrix\Main\Config\Option,
    \Bitrix\Main\Web\HttpClient,
    \Neb\Main\Helper\MainHelper,
    \Neb\Main\Helper\BooksHelper,
    \Neb\Main\Helper\FileHelper,
    \Neb\Main\Helper\UtilsHelper,
    \Nota\UserData\Exception;

/**
 * API просмотровщика
 *
 * Class RslViewer
 * @package Nota\Exalead
 */
class RslViewer
{
    private $protocol = '';
    private $ip = '';
    private $port = '';
    private $key = '';
    private $book_id = '';
    private $get_params = '';
    private $dop_params = '';

    private $status, $error;
    private $user_token;
    protected $_noKeepStatistic;

    public function __construct($book_id = '')
    {
        if (!empty($book_id))
            $this->book_id = urlencode($book_id);

        if (!defined('RSL_WB')) {
            $this->protocol = Option::get("nota.exalead", "viewer_protocol");
            if ($this->protocol == '')
                $this->protocol = 'https';
            $this->ip       = Option::get("nota.exalead", "viewer_ip");
            $this->port     = Option::get("nota.exalead", "viewer_port");
            $this->key      = Option::get("nota.exalead", "viewer_key");
        } else {
            $this->protocol = 'http';
            $this->ip       = "https://relar.rsl.ru";
            $this->port     = "443";
            $this->key      = "99Apaf8YwtAskq5sdfyNMnbD3SDU7qUH";
        }

        $nebUser = new \nebUser();
        $this->user_token = $nebUser->getToken();

        if (null === $this->_noKeepStatistic) {
            $this->_noKeepStatistic = isset($_REQUEST['no_keep_statistic']);
        }
    }

    /**
     * Устанавливает идентификатор книги
     * @param null $book_id
     * @return $this
     * @throws \Exception
     */
    public function setBookId($book_id = null)
    {
        if (empty($book_id))
            throw new \Exception('Пустой ID книги!');

        $this->book_id = urlencode($book_id);

        return $this;
    }

    /**
     * Получает идентификатор книги
     * @return string
     */
    public function getBookId()
    {
        return $this->book_id;
    }

    /**
     * Получает код статуса ответа
     * @return mixed
     */
    public function getHttpStatus()
    {
        return $this->status;
    }

    /**
     * Получает текстовое описание ошибки ответа
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->error;
    }

    /**
     * Устанавливает параметры подключения
     * @param $ip
     * @param $port
     * @param string $key
     * @param string $dop_params
     */
    public function setConnectParams($ip, $port, $key = '', $dop_params = '')
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->key = $key;
        $this->dop_params = $dop_params;
    }

    /**
     * 11. Получение страницы документа, определенной геометрии
     * GET
     * http://{BASEURL}/{CODE}/document/pages/{NUM}/(images|thumbnails)/(width|height|max){VAL}.(jpeg|tiff)[?{GET_PARAMS}]
     * Аналогично предыдущему, однако, страница возвращаеся со значением высоты или ширины равным <VAL>.
     * • при использовании image есть ограничение: 300 < (width|height) <= 2000
     * • при использовании thumbnails есть ограничение: 50 <= (width|height) <= 300, также эти картинки будут кешироваться средствами nginx.
     * • при использовании max в качестве измерения выходное изображение будет вписано в квадрат <VAL>x<VAL>.
     *
     *  ПРИМЕР ЗАПРОСА: http://10.2.0.44:8080/01000000005/document/pages/1/images/width800.tiff
     *  ПРИМЕР ОТВЕТА: {raw tiff image data}
     *
     * @param int $width
     * @param int $num
     * @return bool|mixed
     */
    public function getDocumentPage($width = 300, $num = 1)
    {
        $this->get_params = '/document/pages/' . $num . '/images/width' . $width . '.jpeg?ip=' .
            $_SERVER['REMOTE_ADDR'] . ($this->user_token['UF_TOKEN'] != NULL ? '&token=' . $this->user_token['UF_TOKEN'] : '');
        $result = self::query();
        return $result;
    }

    /**
     * Получение массива доступных ресурсов издания
     *
     * @return bool|mixed
     */
    public function getResources()
    {
        $this->get_params = '/resources';

        $result = self::query();
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Получение информации о документе
     *
     * @return bool|mixed
     */
    public function getDocumnetInfo()
    {
        $this->get_params = '/document/info';

        $result = self::query();
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Получение размера файла - байт
     * @param string $ext
     * @return int
     */
    public function getResourcesExtSize($ext = 'pdf')
    {
        if (empty($ext))
            return false;

        $this->get_params = '/resources/' . $ext . '/size';
        $result = self::query();

        return intval($result);
    }

    /**
     * Получение ссылки на файл
     * @param string $ext
     * @return bool|mixed
     */
    public function getResourcesLink($ext = 'pdf')
    {
        if (empty($ext))
            return false;

        $this->get_params = '/resources/' . $ext . '/link';
        $result = self::query();
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Отправляем запросы в rsl c возможностью задать заголовки и отключить получение тела ответа
     *
     * @param string $sMethod = GET
     * @param mixed $entityBody
     * @return bool|mixed
     */
    private function query($sMethod = 'GET', $entityBody = null)
    {
        session_write_close();

        $url = $this->protocol . "://" . trim($this->ip, '/') .':' . $this->port;
        if (!empty($this->book_id))
            $url .= '/' . $this->book_id;
        if (!empty($this->get_params))
            $url .= $this->get_params;

        if (!empty($this->dop_params))
            $url .= static::getConcatSymbol($url) . $this->dop_params;
        if (true === $this->_noKeepStatistic)
            $url .= static::getConcatSymbol($url) . 'no_keep_statistic=1';

        $url .= static::getConcatSymbol($url) . self::getOsSessionParams();

        $obHttpClient = new HttpClient();
        if (!MainHelper::checkSslVerification($url)) {
            $obHttpClient->disableSslVerification();
        }
        $obHttpClient->setTimeout(20);
        $obHttpClient->setHeader('DLIB-KEY', $this->key);
        $obHttpClient->query($sMethod, $url, $entityBody);

        $result       = $obHttpClient->getResult();
        $iHttpCode    = $obHttpClient->getStatus();
        $sError       = implode(', ', $obHttpClient->getError());

        $this->status = $iHttpCode;
        $this->error  = $sError;

        //debugPre($sMethod); debugPre($url); debugPre($entityBody); debugPre($result); die();

        if (!in_array($iHttpCode, [200, 206]))
            return false;

        return $result;
    }

    /**
     * Скачивание издания (только через cURL) (используется в /local/tools/exalead/getFiles.php)
     * 
     * @param string $ext
     * @return bool
     */
    public function getFile($ext = 'pdf')
    {
        if (empty($ext))
            return false;

        session_write_close();
        $iFileSize = self::getResourcesExtSize($ext);

        if ($iFileSize > 0) {

            $arHeaders = [];

            $arResourcesLink = self::getResourcesLink($ext);
            foreach ($arResourcesLink['headers'] as $sKey => $sVal) {
                if ($sKey != 'host')
                    $arHeaders[] = $sKey . ': ' . $sVal;
            }
            $ipSticker = (false === strpos($arResourcesLink['link'], '?')) ? '?' : '&';

            $downloadUrl = $arResourcesLink['link'] . $ipSticker . "ip=" . $_SERVER['REMOTE_ADDR'] . '&' . self::getOsSessionParams();
            $sMetaParams = ($this->user_token['UF_TOKEN'] != null ? '&token=' . $this->user_token['UF_TOKEN'] : '') . '&nostatistic=1';

            // шаг 1: получение только мета-информации файла (тип, размер, название)
            $ch = curl_init($downloadUrl . $sMetaParams);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $arHeaders);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, MainHelper::checkSslVerification($downloadUrl));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_exec($ch);
            $iHeaderContentLength = intval(curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD));
            $sHeaderContentType   = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
            curl_close($ch);
            preg_match('/^application\/([a-zA-Z]*)/', $sHeaderContentType, $arContentType);

            // из РНБ под видом pdf могут прийти zip-файлы
            if ('zip' === $arContentType[1]) {
                $ext = 'zip';
                $iFileSize = $iHeaderContentLength;
            }

            MainHelper::clearBuffer();

            // вырезаем из названия книги все символы, недопустимые в названии файла
            $sFileName = FileHelper::str2FileName($_REQUEST["name"]) . '.' . $ext;

            // в IE название файла должно быть всегда в urlencode
            if (UtilsHelper::isMicrosoftBrowser(true)) {
                $sFileName = urlencode($sFileName);
            }

            // шаг 2: получение самого файла и отдача его в браузер
            header('Content-Description: File Transfer');
            header('Content-Type: application/' . $ext);
            header("Content-Disposition: attachment; filename=" . $sFileName);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            if ($iFileSize != 1) {
                header('Content-Length: ' . $iFileSize);
            }

            $ch = curl_init($downloadUrl);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $arHeaders);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, MainHelper::checkSslVerification($downloadUrl));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_exec($ch);
            curl_close($ch);
            exit;
        }
    }

    public function getDocumentPageSearch($width = 300, $num = 1, $search)
    {
        $this->get_params = '/document/pages/' . $num . '/search/' . $search . '/render/ppi150.jpeg?page_format=width' . $width . '.jpeg&ip='
            . $_SERVER['REMOTE_ADDR'] . ($this->user_token['UF_TOKEN'] != NULL ? '&token=' . $this->user_token['UF_TOKEN'] : '');
        $result = self::query();
        return $result;
    }

    /**
     * @param int $num
     * @param string $type
     *
     * @return bool|mixed
     * @throws \Exception
     */
    public function getDocumentPageData($num = 1, $type = 'txt')
    {
        if ('txt' !== $type && 'html' !== $type)
            throw new \Exception('Unknown data type');

        $this->get_params = '/document/pages/'
            . $num . '/' . $type
            . '?ip=' . $_SERVER['REMOTE_ADDR']
            . (isset($this->user_token['UF_TOKEN'])
                ? '&token=' . $this->user_token['UF_TOKEN']
                : '');

        return self::query();
    }

    /**
     * Количество страниц в документе
     *
     * @return bool|mixed
     */
    public function getPagesCount()
    {
        $this->get_params = '/document/pages/count';

        $result = self::query();
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Получение номеров страниц в которых встречаются слова
     * @param $search
     * @return bool|mixed
     */
    public function getPageSearch($search)
    {
        if (empty($search))
            return false;

        $this->get_params = '/document/search/' . $search;
        $result = self::query();
        return $result;
    }

    public function getPageThumbl($height = 50, $num = 1)
    {
        $this->get_params = '/document/pages/' . $num . '/thumbnails/height' . $height . '.jpeg'
            .($num == 1 && $this->user_token['UF_TOKEN'] != null ? '?token=' . $this->user_token['UF_TOKEN'] : '');
        $result = self::query();
        return $result;
    }

    /**
     * Геометрия документа
     * @param int $num
     * @return bool|mixed
     */
    public function getGeometry($num = 1)
    {
        $this->get_params = '/document/pages/' . $num . '/geometry';
        $result = self::query();
        return $result;
    }

    /**
     * Проверяет призак доступа
     * @return bool|mixed
     */
    public function isAvailableType()
    {
        $this->get_params = '/document/type';
        $result = self::query();
        return $result;
    }

    public function getAccessComment()
    {
        $this->get_params = '/document/type/accesscomment';
        $result = self::query();
        return $result;
    }

    /**
     * Получает массив произведений в изданий
     * @return bool|mixed
     */
    public function getMarkupBooks($arParams = [])
    {
        $this->get_params = '/book_arts';
        if (is_array($arParams))
            $this->get_params .= '?' . http_build_query($arParams, "", "&");

        $result = self::query();
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Получает массив произведений в издании
     * @return bool|mixed
     */
    public function getMarkup($arParams = [])
    {
        $this->get_params = '/arts';
        if(is_array($arParams))
            $this->get_params .= '?' . http_build_query($arParams, "", "&");

        $result = self::query();
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Добавляет / изменяет произведение в издании
     */
    public function addMarkup($sMethod, $arParams)
    {
        $this->get_params = '/art';
        if ($sMethod != 'POST' && is_array($arParams)) // если не пост, то в гет параметры запихнуть
            $this->get_params .= '?' . http_build_query($arParams, "", "&");

        $result = self::query($sMethod, $arParams);
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Изменяет статус в произведениях издания
     */
    public function changeMarkupStatus($sStatus, $arParams)
    {
        $this->get_params = '/art/' . $sStatus;
        if(is_array($arParams))
            $this->get_params .= '?' . http_build_query($arParams, "", "&");

        $result = self::query();
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Получает предыдущую утвержденную версию изданий произведения
     */
    public function prevArtsMarkup($arParams)
    {
        $this->get_params = '/prev_arts/';
        if (is_array($arParams))
            $this->get_params .= '?' . http_build_query($arParams, "", "&");

        $result = self::query();
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Получает лог изменений разметки изданий
     * @return bool|mixed
     */
    public function getMarkupLog($arParams = [])
    {
        $this->get_params = '/logs';
        if (is_array($arParams))
            $this->get_params .= '?' . http_build_query($arParams, "", "&");

        $result = self::query();
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Получение ID библиотеки, ID книжки, возвращает это же + full symbolyc ID
     * На вход принимает POST запрос, тело которого содержит JSON следующего вида:
     * [{ALIS:17, idFromALIS:0000330740}, {ALIS:17, idFromALIS:RU_RGDB_BIBL_0000331273}, {ALIS:17, idFromALIS:RU_RGDB_BIBL_0000331531}]
     *
     * На выходе JSON, дополненный fullSymbolicId:
     * [{"ALIS":17,"idFromALIS":"0000330740","fullSymbolicId":"000207_000017_RU_RGDB_BIBL_0000330740"},{"ALIS":17,"idFromALIS":"RU_RGDB_BIBL_0000331273","fullSymbolicId":"000207_000017_RU_RGDB_BIBL_0000331273"},{"ALIS":17,"idFromALIS":"RU_RGDB_BIBL_0000331531","fullSymbolicId":"000207_000017_RU_RGDB_BIBL_0000331531"}]
     *
     * @param $data
     * @return bool|mixed
     */
    public function getSymbolicIds($data)
    {
        $this->get_params = '/getSymbolicIds';

        $result = self::query('POST', 'alisAndIds=' . json_encode($data));
        $result = self::isJson2Array($result);

        return $result;
    }

    /**
     * Извлечение текста из картинки по координатам.
     *
     * пример использования API:
     *
     * /000199_000009_004612677/document/pages/1/txt/crop?coordinates={"x1":"0.1",%20"x2":"0.9",%20"y1":"0.1",%20"y2":"0.9"}
     *
     * Параметр coordinates - координаты прямоугольника, обрезающего текст. Координаты указываются в процентах от верхнего левого угла страницы. (0.9 = 90%)
     *
     * @param $id
     * @param $page
     * @param $coordinates
     * @return bool|mixed
     * @throws \Exception
     */
    public function getTextByCoordinates($id, $page, $coordinates)
    {
        if (!$id || (strlen($id) < 10))
            throw new \Exception('Таки шо ви хотите от старого еврея без ID книги?');

        $page = intval($page);
        if (!$page)
            throw new \Exception('Страницу таки задать надо!');

        if (is_array($coordinates)) {
            $coordinates = json_encode($coordinates);
        } elseif (!is_string($coordinates)) {
            throw new \Exception('Координаты таки задать надо!');
        }

        $this->get_params = "/document/pages/$page/txt/crop?coordinates=" . urlencode($coordinates);
        $result = self::query();

        return $result;
    }

    /**
     * Получает признаки доступности книг
     *
     * @param array $arBooks
     * @return bool|mixed
     */
    public function isAvailableBooks($arBooks)
    {
        $arResult = [];

        if (empty($arBooks)) {
            return $arResult;
        }

        $sRequest = '';
        foreach ($arBooks as $i => $sBook) {
            $sRequest .= ($i ? '&' : '') . 'id='.$sBook;
        }

        $this->get_params = '/getBooksAccess';
        $result = self::query('POST', $sRequest);
        $result = self::isJson2Array($result);

        foreach ($result as $arBook) {
            $arResult[$arBook['fullSymbolicId']] = BooksHelper::getByCode($arBook['accessType']);
        }

        return $arResult;
    }

    /**
     * Метод декодирует JSON-массив в PHP-массив
     *
     * @param $string - входная строка
     * @return mixed - массив или строка
     */
    private static function isJson2Array($string) {

        $arJson = json_decode($string, true);

        if (json_last_error() == JSON_ERROR_NONE)
            $string = $arJson;

        return $string;
    }

    /**
     * Получает символ конкатенации параметра для URL
     *
     * @param $url
     * @return string
     */
    private static function getConcatSymbol($url) {
        return parse_url($url, PHP_URL_QUERY) ? '&' : '?';
    }

    private static function getOsSessionParams() {
        return 'os=online&session=' . session_id();
    }
}