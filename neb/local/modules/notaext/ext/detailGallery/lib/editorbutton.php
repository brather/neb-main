<?
namespace Bitrix\NotaExt\Ext\DetailGallery;

/*
Формируем кнопку в взуальном редактире для вставки галереи
*/
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class EditorButton
{
    public static function OnBeforeHTMLEditorScriptRuns($v)
    {
        if (!Access::check())
            return false;

        global $APPLICATION;
        $APPLICATION->AddHeadString('<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />', true);
        $APPLICATION->AddHeadString('<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/css/select2.css" />', true);

        $APPLICATION->AddHeadString('<script>window.jQuery || document.write(\'<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js">\x3C/script>\')</script>', true);
        $APPLICATION->AddHeadString('<script>window.jQuery.ui || document.write(\'<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js">\x3C/script>\')</script>', true);

        $APPLICATION->AddHeadString('<script src="/local/components/notaext/plupload/lib/plupload/js/plupload.full.min.js"></script><script src="/local/components/notaext/plupload/lib/plupload/js/i18n/' . LANGUAGE_ID . '.js"></script>', true);
        $APPLICATION->AddHeadString('<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.full.js"></script>', true);
        $APPLICATION->AddHeadString('<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/i18n/' . LANGUAGE_ID . '.js"></script>', true);

        \CJSCore::RegisterExt('detailGallery', array(
            'js' => '/local/tools/notaext/detailGallery/script.js',
            'css' => '/local/tools/notaext/detailGallery/styles.css',
            'lang' => '/local/modules/notaext/ext/detailGallery/lang/' . LANGUAGE_ID . '/functions_js.php',
        ));
        \CJSCore::Init(array("detailGallery"));

    }
}