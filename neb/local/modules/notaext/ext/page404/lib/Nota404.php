<?
namespace Bitrix\NotaExt\Ext\Page404;

class Nota404
{
    function Redirect404()
    {
        if (
            !defined('ADMIN_SECTION') &&
            defined("ERROR_404") &&
            defined("PATH_TO_404") &&
            file_exists($_SERVER["DOCUMENT_ROOT"] . PATH_TO_404)
        ) {
            global $APPLICATION;
            $APPLICATION->RestartBuffer();
            //CHTTP::SetStatus("404 Not Found"); // @todo: fix this in future
            include($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . "/header.php");
            include($_SERVER["DOCUMENT_ROOT"] . PATH_TO_404);
            include($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . "/footer.php");

        }
    }

    function onStart()
    {
        die('x');
        define ('PATH_TO_404','/404.php');
    }
}