<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
	\CUtil::InitJSCore(array("jquery"));
	$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);
?>
<div class="left" id="container_<?=$arParams['RAND_STR']?>">
	<p></p>
	<a href="#" class="tdu" id="add_file">Добавить файл</a>

    <?if (!empty($arParams['ALREADY_UPLOADED_FILE'])) {?>
        <span class="" id="uploaded_file"><a target="_blank" href="<?=$arParams['ALREADY_UPLOADED_FILE']?>">файл загружен (просмотр)</a></span>
    <?} else {?>
        <span class="hidden" id="uploaded_file"><a target="_blank" href="">файл загружен (просмотр)</a></span>
    <?}?>

    <span class="file-progress hidden" id="add_file_progress"></span>
	<div class="field validate addpdfield" id="add_file_error">
		<input type="text" data-required="required" value="<?=$arParams['ALREADY_UPLOADED_FILE']?>" name="<?=$arParams['FIELD_NAME']?>" class="input hidden" />
		<em class="error required">Поле обязательно для заполнения</em>	
		<em class="error upload"></em>	
	</div>		


</div>
<div class="pl_button" style="display: none;"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>

<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'add_file',
				drop_element: "drop_upload",
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'), 
				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
					unique_names: true,
					<?
					}
				?>

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				<?
					if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
					{
					?>
					resize: {
						width: <?=intval($arParams['RESIZE_WIDTH'])?>,
						height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
						<?
							if($arParams['RESIZE_CROP'] == 'Y')
							{
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=intval($arParams['RESIZE_QUALITY'])?>
					},
					<?
					}
				?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '1mb',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'
				},
				multi_selection: false,
				init: {

					FilesAdded: function(up, files) {
						uploader_<?=$arParams['RAND_STR']?>.start();

						$('#add_file_progress').addClass('hidden');
						$('#add_file_error').removeClass('error');
						$('#add_file_error em.error').hide();
                        $('#uploaded_file').addClass('hidden');

					},
					FileUploaded: function(up, file, response) {
						$('#add_file_error em.error').hide();
						$('#add_file_progress').addClass('hidden');
						var result = response.response;
						if (result) 
						{
							var obResponse = JSON.parse(result);
							$('input[name="<?=$arParams['FIELD_NAME']?>"]').val(obResponse.file);
                            $('#uploaded_file').removeClass('hidden');
                            $('#uploaded_file > a').attr('href', obResponse.file);
						}
					},
					UploadProgress: function(up, file) {
						$('#add_file_progress').removeClass('hidden');
						$('#add_file_progress').text(file.percent+ '%');
					},

					Error: function(up, err) {
						$('#add_file_error em.error').hide();
						
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
						if(err.code == -601)
						{
							$('#add_file_error').addClass('error');
							$('#add_file_error em.error.upload').text('Книга болжна быть в PDF формате');
							$('#add_file_error em.error.upload').show();

						}
					}
				}
			}
		);

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>
