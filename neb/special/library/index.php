<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Библиотеки");
?>

<?$APPLICATION->IncludeComponent(
    "neb:library",
    "special",
    array(
        "ITEMS_LIMIT" => "10",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/special/library/",
        "IBLOCK_TYPE" => "library",
        "IBLOCK_ID" => "4",
        "SEF_URL_TEMPLATES" => array(
            "list" => "/",
            "detail" => "#CODE#/",
            "funds" => "#CODE#/funds/",
            "collections" => "#CODE#/collections/",
            "news_detail" => "#CODE#/news-#ELEMENT_ID#-#ELEMENT_CODE#/",
        )
    ),
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>