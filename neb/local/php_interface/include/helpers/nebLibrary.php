<?

use \Bitrix\NotaExt\Iblock\Element,
    \Bitrix\NotaExt\Iblock\IblockTools,
    \Bitrix\Highloadblock\HighloadBlockTable;

class nebLibrary
{
    public static $federalLibraries
        = array(
            199,
            200,
            201,
            202,
            207,
            219
        );

    public function getLibByCode($code)
    {
        if (empty($code)) {
            return false;
        }

        $arResult = Element::getList(
            array(
                'IBLOCK_ID' => IblockTools::getIBlockId('IBLOCK_CODE_LIBRARY'),
                'CODE'      => $code
            ),
            1,
            array(
                'PROPERTY_PUBLICATIONS',
                'PROPERTY_COLLECTIONS',
                'skip_other'
            )
        );

        return $arResult;
    }

    public static function showCollection($code)
    {
        if (empty($code)) {
            return false;
        }

        $arResult = self::getLibByCode($code);
        if (!empty($arResult['ITEM']['PROPERTY_COLLECTIONS_VALUE'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function showPublication($code)
    {
        if (empty($code)) {
            return false;
        }

        $arResult = self::getLibByCode($code);
        if (!empty($arResult['ITEM']['PROPERTY_PUBLICATIONS_VALUE'])) {
            return true;
        } else {
            return false;
        }
    }


    public static function showNews($id)
    {
        if (empty($id)) {
            return false;
        }
        // новости для библиотеки

        $arResult = Element::getList(
            array('IBLOCK_ID' => 3,'PROPERTY_LIBRARY' => $id, '!PROPERTY_LIBRARY' => false, 'ACTIVE' => 'Y'),
            1,
            array(
                'ID',
            ),
            array(),
            array(),
            false,
            true
        );

        return !empty($arResult);
    }

    public function getVoteRight()
    {
        $NUser = new nebUser();
        $role = $NUser->getRole();
        $right = 'D';

        if ($role == UGROUP_LIB_CODE_ADMIN) {
            $right = 'W';
        } elseif ($role == UGROUP_LIB_CODE_EDITOR || $role == UGROUP_LIB_CODE_CONTROLLER) {
            $right = 'R';
        }

        return $right;
    }

    /**
     * Получить битриксовую библиотеку по полю $fname => $fvalue
     *
     * @param $fname  - название поля по которому искать
     * @param $fvalue - значение этого поля
     */
    public function getBitrixID($fname, $fvalue)
    {
        $arResult = Element::getList(
            array(
                'IBLOCK_ID' => IblockTools::getIBlockId('IBLOCK_CODE_LIBRARY'),
                $fname      => $fvalue
            ),
            1,
            array()
        );

        return $arResult;
    }

    public static function getLibraryIdByIblockId($id)
    {
        if (!\CModule::IncludeModule('iblock')) {
            throw new \Exception('module "iblock" not installed');
        }
        $element = new \CIBlockElement();
        $result = $element->GetList(
            array(),
            array('ID' => $id),
            array('PROPERTY_LIBRARY_LINK')
        );
        $libraryId = null;
        if (!$result = $result->Fetch()) {
            $result = array();
        }
        if (isset($result['PROPERTY_LIBRARY_LINK_VALUE'])) {
            $libraryId = intval($result['PROPERTY_LIBRARY_LINK_VALUE']);
        }

        return $libraryId;
    }

    /**
     * @param $id
     *
     * @return array
     * @throws Exception
     */
    public static function getLibraryIblockIdListByExalead($id)
    {
        if (!\CModule::IncludeModule('iblock')) {
            throw new \Exception('module "iblock" not installed');
        }
        if (!is_array($id) && intval($id) > 0) {
            $id = array($id);
        }
        foreach ($id as $key => $itemId) {
            $id[$key] = intval($itemId);
        }
        $id = array_filter($id);
        if (empty($id)) {
            throw new Exception('Empty id list');
        }
        $element = new \CIBlockElement();
        $dbResult = $element->GetList(
            array(),
            array('PROPERTY_LIBRARY_LINK' => $id),
            array('ID')
        );
        $libraryId = null;
        $result = array();
        while ($element = $dbResult->Fetch()) {
            $result[] = intval($element['ID']);
        }

        return $result;
    }


    public static function getIblockIdByExaleadId($exaleadId)
    {
        $iblockIds = static::getLibraryIblockIdListByExalead($exaleadId);

        return isset($iblockIds[0]) ? $iblockIds[0] : null;
    }


    /**
     * @return \Bitrix\Main\Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    public static function forgeCatalogEntity()
    {
        $entityName = HighloadBlockTable::compileEntity(
            HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch()
        )->getDataClass();

        return new $entityName;
    }
}