<?php
/**
 * User: agolodkov
 * Date: 11.08.2016
 * Time: 14:34
 */

namespace LbcBundle\DependencyInjection;

use ElarUtilsBundle\DependencyInjection\MustacheHelper;
use Symfony\Component\Templating\Helper\HelperInterface;

/**
 * Class MustacheEngine
 *
 */
class MustacheEngine extends MustacheHelper implements HelperInterface
{
    protected $charset = 'UTF-8';

    /**
     * @return string
     */
    public function getName()
    {
        return 'mustache_engine';
    }


    /**
     * Sets the default charset.
     *
     * @param string $charset The charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    /**
     * Gets the default charset.
     *
     * @return string The default charset
     */
    public function getCharset()
    {
        return $this->charset;
    }

}