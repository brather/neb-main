<?
if (!defined('LANG_CHARSET')) define('LANG_CHARSET', 'UTF-8');

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!CModule::IncludeModule('nota.collection')) {
    echo CAdminMessage::ShowMessage(Loc::getMessage('NOTA_COLLECTION_ACCESS_DENIED'));
    return false;
}
if (!CModule::IncludeModule('nota.exalead')) {
    echo CAdminMessage::ShowMessage(Loc::getMessage('NOTA_COLLECTION_ACCESS_DENIED'));
    return false;
}

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

use Nota\Collection\ElementsTable;
use Nota\Collection\LibraryTable;
use Nota\Collection\CollectionTable;

use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;

$sTableID = 'nota_collection_search1';
$oSort = array();
$arOrder = array();

if (!empty($_REQUEST['order'])) {
    $sort = htmlspecialcharsbx($_REQUEST['order']);
} else {
    $sort = 'asc';
}

$lAdmin = new CAdminList($sTableID, new CAdminSorting($sTableID, 'NAME', 'DESC'));

$arHeaders = array(
    array(
        "id" => "NAME",
        "content" => 'Имя',
        "sort" => "NAME",
        "default" => true,
        "align" => "left",
    ),
    array(
        "id" => "PUBLISHER",
        "content" => 'Издательство',
        "default" => true,
    ),
    array(
        "id" => "ID",
        "content" => 'Exalead id',
        "default" => true,
    )
);

$lAdmin->AddHeaders($arHeaders);
$lAdmin->AddAdminContextMenu(array(), false);
$query = new SearchQuery(htmlspecialcharsbx($_REQUEST['find']));

$query->setParam('s', $sort . '(document_titlesort)');

/*	постраничка */
$SIZEN_1 = CAdminResult::GetNavSize($sTableID);
$arNavigation = CDBResult::GetNavParams($SIZEN_1);
$PAGEN_1 = $arNavigation['PAGEN'];

$query->setPageLimit($SIZEN_1);

if ($PAGEN_1 > 1)
    $query->setNextPagePosition((($PAGEN_1 - 1) * $SIZEN_1));
/*	постраничка end*/

$client = new SearchClient();
$arResult['BOOK'] = $client->getResult($query);

if (count($arResult['BOOK']['ITEMS'])) {
    /*	постраничка */
    $arResultFetch = array();
    if (intval($arResult['BOOK']['START_ITEM']) > 0)
        $arResultFetch = array_fill(0, ($arResult['BOOK']['START_ITEM']), array('id' => '1', 'title' => '2', 'publisher' => '3'));

    $arResultFetch = array_merge($arResultFetch, $arResult['BOOK']['ITEMS']);

    $endar = ($arResult['BOOK']['COUNT'] - ($SIZEN_1 + $arResult['BOOK']['START_ITEM']));
    if ($endar > 0) {
        $arTmp = array_fill(0, $endar, array('id' => '1', 'title' => '2', 'publisher' => '3'));
        $arResultFetch = array_merge($arResultFetch, $arTmp);
    }

    $rs = new CDBResult;
    $rs->InitFromArray($arResultFetch);

    $rs = new CAdminResult($rs, $sTableID);
    $rs->NavStart();
    $lAdmin->NavText($rs->GetNavPrint(''));
    /*	постраничка end*/

    while ($arElement = $rs->Fetch()) {
        $row = $lAdmin->AddRow($str_ID, $arRes);
        $row->AddViewField('ID', $arElement['id']);
        $row->AddViewField('NAME', $arElement['title']);
        $row->AddViewField('PUBLISHER', $arElement['publisher']);

        $row->AddActions(array(
            array(
                "DEFAULT" => "Y",
                "TEXT" => 'Выбрать',
                "ACTION" => "javascript:SelEl('" . htmlspecialcharsbx(CUtil::JSEscape($arElement['id'])) . "', '" . htmlspecialcharsbx(CUtil::JSEscape($arElement['title'])) . "')",
            ),
        ));
    }
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");
$lAdmin->CheckListMode();
?>
    <script type="text/javascript">
        function SelEl(id, name) {
            window.opener.document.getElementById('<?=$_REQUEST['code']?>').value = id;
            /*window.opener.document.getElementById('sp_<?=$_REQUEST['code']?>').innerHTML = name;*/
            window.opener.getExaleadBook(id);
            window.close();
        }
    </script>
    <form method="GET" name="find_exalead_book_form"
          action="<?= $APPLICATION->GetCurPageParam('find=' . $_REQUEST['find'], array('find')); ?>">
        <?
        $arFindFields = Array(
            "find" => 'Введите запрос',
        );

        $oFilter = new CAdminFilter($sTableID . "_filter", $arFindFields);
        $oFilter->Begin();
        ?>
        <tr>
            <td><b>Введите запрос:</b></td>
            <td><input type="text" name="find" value="<?= htmlspecialcharsbx($_REQUEST['find']) ?>" size="47"></td>
        </tr>
        <?
        $oFilter->Buttons(
            array(
                "table_id" => $sTableID,
                "url" => $APPLICATION->GetCurPage() . '?find=' . htmlspecialcharsbx($_REQUEST['find']) . '&code=' . $_REQUEST['code'] . '&order=' . $_REQUEST['order'],
                "form" => "find_exalead_book_form"
            )
        );
        $oFilter->End();
        ?>
        <input type='hidden' name='code' value='<?= $_REQUEST['code'] ?>'>
        <input type='hidden' name='order' value='<?= $_REQUEST['order'] ?>'>
    </form>
<?

/*$lAdmin->AddGroupActionTable(
array(
'next' => 'Следущие записи'
)
);*/

$lAdmin->DisplayList();
echo ShowError($strWarning);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
?>