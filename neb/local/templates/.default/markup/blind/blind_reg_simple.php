
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		
		<h2 class="mode">регистрация на портале</h2>
		<form action="www.notamedia.ru" class="b-form b-form_common b-regform">
			<p class="note">Просим обратить особое внимание на заполнение анкеты корректными данными. <br/>В случае выявления несоответствий Ваш доступ может быть заблокирован.</p>
				<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings01">Фамилия</label>
					<div class="field validate">
						<input type="text" data-validate="fio" data-required="required" value="" id="settings01" data-maxlength="30" data-minlength="2" name="sname" class="input" autocomplete="off">
						<em class="error required">Поле обязательно для заполнения</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>							
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings02">Имя</label>
					<div class="field validate">
						<input type="text" data-required="required" data-validate="fio" value="" id="settings02" data-maxlength="30" data-minlength="2" name="name" class="input"  autocomplete="off">	
						<em class="error required">Поле обязательно для заполнения</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>												
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings22">Отчество</label>
					<div class="field validate">
						<input type="text" data-validate="fio" value="" id="settings22" data-maxlength="30" data-minlength="2" name="name" class="input" autocomplete="off" >
						<em class="error required">Поле обязательно для заполнения</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>													
					</div>
				</div>
			</div>
				<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings04">Электронная почта <strong>(для входа на сайт)</strong></label>
					<div class="field validate">
						<input type="text" class="input" name="num" data-minlength="2" data-maxlength="30" id="settings04" data-validate="email" value="" data-required="required" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					
					<label for="settings05">Пароль</label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="required" value="" id="settings05" data-maxlength="30" name="pass" class="pass_status input" autocomplete="off">
						<em class="error required">Поле обязательно для заполнения</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings55">Подтвердить пароль</label>
					<div class="field validate">
						<input data-identity="#settings05" type="password" data-required="required" value="" id="settings55" data-maxlength="30" name="pass" class="input" data-validate="password" autocomplete="off">
						<em class="error identity ">Пароль не совпадает с введенным ранее</em>
						<em class="error required">Поле обязательно для заполнения</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
				</div>
			</div>
			<hr>
				<div class="fieldrow nowrap">
				<div class="fieldcell iblock w700">
					<div class="checkwrapper ">
						<a href="#" title="Условия использования портала национальной электронной библиотеки">Условия использования портала национальной электронной библиотеки</a>
					</div>
				</div>
			</div>
			<div class="checkwrapper">
				<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb3" autocomplete="off"><label for="cb3" class="black"> Согласен с условиями использования портала НЭБ</label>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix">
						<button disabled="disabled" type="submit" value="1" class="formbutton btdisable">Зарегистрироваться</button>

					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->



</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>