<?

class imagesListSelectorProp extends CExt
{
    var $EXT_ID = __CLASS__;
    var $EXT_VERSION;
    var $EXT_VERSION_DATE;
    var $EXT_NAME;
    var $EXT_DESCRIPTION;
    var $EXT_SORT;

    public function __construct()
    {
        parent::__construct();

        $this->EXT_VERSION = '1.0';
        $this->EXT_VERSION_DATE = '17.11.2014';

        $this->EXT_NAME = 'Свойство imagesListSelector';
        $this->EXT_DESCRIPTION = 'Свойство инфоблока [список] с возможностью указания картинки для выбора.';
        $this->EXT_SORT = 1000;

    }

    function onBeforeInstall()
    {

    }

    function onBeforeUninstall()
    {

    }

    function onAfterInstall()
    {

    }

    function onAfterUninstall()
    {

    }
}
