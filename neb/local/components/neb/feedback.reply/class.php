<? use Bitrix\Main\Loader;
use Nota\Exalead\SearchClient;
use Nota\Exalead\SearchQuery;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 15.09.2016
 * Time: 10:00
 */

CBitrixComponent::includeComponentClass('neb:feedback');

/**
 * Class FeedbackReplyComponent
 */
class FeedbackReplyComponent extends FeedbackComponent
{
    private $arRequestForm;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent() {
        $this->includeModules(['form', 'nota.exalead']);
        $this->getRequest();
        $this->getUser();
        $this->getTicket();
        $this->getForm();
        $this->getRequestForm();
        $this->setActions();

        $this->includeComponentTemplate();
    }

    /**
     * Получение данных формы
     */
    private function getRequestForm() {

        $arResult = [];

        if ($this->arRequest['FEEDBACK']['ACTION'] == 'REPLY')
            foreach ($this->arRequest['FEEDBACK'] as $sCode => $sVal)
                $arResult[$sCode] = $sVal;

        $this->arRequestForm = $arResult;
    }

    /**
     * Действия пользователя
     */
    private function setActions() {

        $this->arResult['TICKET'] = $this->arTicket;

        $iUser    = $this->arUser['ID'];
        $bSend    = isset($this->arRequestForm['SEND']);
        $bSave    = isset($this->arRequestForm['SAVE']);
        $sMessage = trim($this->arRequestForm['MESSAGE']);
        $arFile   = [];
        $arFiles = array();

        // загрузка файла через jQuery-File-Upload
        $sFile    = str_replace('\\', '/', trim($this->arRequestForm['FILE']));
        if (!empty($sFile) && file_exists($_SERVER['DOCUMENT_ROOT'] . $sFile)) {
            $arFile = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . $sFile);
        }
        // загрузка файла класссическим способом через $_FILES
        elseif (!empty($_FILES['FEEDBACK'])) {
            foreach ($_FILES['FEEDBACK'] as $sCode => $arVal) {
                reset($arVal);
                $arFile[$sCode] = current($arVal);
            }
        } // если пришли файлы вместе с ответом. вложим их в письмо
        elseif (is_array($this->arRequest['posting_files']) && count($this->arRequest['posting_files']) > 0)
        {
            foreach ($this->arRequest['posting_files'] as $pathToFile)
            {
                $pathToFile = trim($pathToFile);
                if (!empty($pathToFile))
                {
                    $arFile = \CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . $pathToFile); // ??
                    $arFile['MODULE_ID'] = "neb.main"; // ??
                    $arFiles[] = \CFile::SaveFile($arFile, "neb.main");
                }
            }
        }

        if (empty($iUser) || empty($this->arTicket) || !($bSend || $bSave) || empty($sMessage))
            return;

        // установка идентификатора ответчика
        if (empty($this->arResult['ERROR']) && !$this->setTicketField('FB_USER_ID', $iUser))
            $this->arResult['ERROR'][] = 'Не удалось записать ответственного у заявки!';

        // установка ответа на вопрос
        if (empty($this->arResult['ERROR']) && !$this->setTicketField('FB_REPLY_TEXT', $sMessage))
            $this->arResult['ERROR'][] = 'Не удалось добавить ответ к заявке!';

        // добавление файла к ответу
        if (!empty($arFile['tmp_name'])) {
            if ($this->setTicketField('FB_REPLY_FILE', $arFile)) {
                unlink($arFile['tmp_name']);
                $this->getTicket();
            } else {
                $this->arResult['ERROR'][] = 'Не удалось добавить файл к заявке!';
            }
        }

        // получение массива файла для показа в визуалке или отправке на почту
        $iFile = intval($this->arTicket['FIELDS']['FB_REPLY_FILE']['USER_FILE_ID']);
        if ($iFile || count($arFiles) > 0)
        {
            $sMessage .= "\n\nВложенные файлы: ";
            for ($i = 0; $i < count($arFiles); $i++)
            {
                $arFile = $this->getFile($arFiles[$i]);
                $sMessage .= URL_SCHEME . '://' . $_SERVER['SERVER_NAME'] . $arFile['PATH'] . ' ['. $arFile['SIZE'] ."]\n";
            }

            $this->arTicket['FIELDS']['FB_REPLY_FILE']['FILE'] = $arFile;
        }

        // отправка ответа
        if ($bSend)
        {
            // установка статуса для заявки
            if (empty($this->arResult['ERROR']) && !$this->setTicketStatus('CLOSED'))
                $this->arResult['ERROR'][] = 'Не удалось установить статус "Закрыта" для данной заявки!';

            // добавление записи в лог
            if (empty($this->arResult['ERROR']) && !$this->setHistoryItem('Закрыта', $this->arUser['ID']))
                $this->arResult['ERROR'][] = 'Не удалось записать лог-историю заявки!';

            // отправка сообщение на почту заявителю
            if (empty($this->arResult['ERROR']) && !empty($this->arTicket['FIELDS']['FB_EMAIL']['USER_TEXT'])) {

                $this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']
                    = trim($this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']);

                $arMail = [
                    'NUMBER' => $this->arTicket['ID'],
                    'EMAIL'  => $this->arTicket['FIELDS']['FB_EMAIL']['USER_TEXT'],
                    'THEME'  => $this->arTicket['FIELDS']['FB_THEME']['ANSWER_TEXT'],
                    'TEXT'   => $this->arTicket['FIELDS']['FB_TEXT']['USER_TEXT'],
                    'INFO'   => $sMessage,
                    'DATE_CREATE' => $this->arTicket['DATE_CREATE']
                ];

                if (!empty($this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']))
                {
                    Loader::includeModule('nota.exalead');

                    $query = new SearchQuery();
                    $query->getById($this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']);

                    $client = new SearchClient();
                    $res = $client->getResult($query);

                    unset ($query, $client);

                    $arMail['EX_BOOK_LINK'] = 'Издание: <a href="'.$_SERVER['HTTP_ORIGIN'].'/catalog/'.$this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT'].'/">' . $res['authorbook_original'] . ': ' . $res['name'] . '</a>';
                }

                $this->sendMail('NEB_FEEDBACK_REPLY', $arMail, $arFiles);
            }
        }

        $this->getTicket(); // для показа результата в JSON
        $this->arResult['TICKET'] = $this->arTicket;

        // выдача результата: в режиме аякса вывод в виде JSON, иначе - редирект
        if ($this->arRequest['ajax'] == 'Y') {
            $this->arResult['TICKET']['USER'] = trim($this->arUser['LAST_NAME']
                . ' '. $this->arUser['NAME'] .' '. $this->arUser['SECOND_NAME']) ? : $this->arUser['LOGIN'];
            $this->arResult['json'] = true;
        } elseif (empty($this->arResult['ERROR'])) {
            LocalRedirect($this->arParams['FEEDBACK_URL']);
        }
    }
}