
<?
$pagename='';
?>
<? include("header_main_auth.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>		

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-searchresult noborder hidden">
		<ul class="b-profile_nav">
			<li>
				<a href="#" class="b-profile_navlk js_profilemenu current">личный кабинет</a>
				<ul class="b-profile_subnav">
					<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
					<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
				</ul>
			</li>
			<li><a class="b-profile_nav_lb" href="#">моя библиотека</a></li>
			<li><a href="#" class="b-profile_nav_qt">цитаты</a></li>
			<li><a href="#" class="b-profile_nav_bm ">закладки</a></li>
			<li><a href="#" class="b-profile_nav_notes">заметки</a></li>
			<li><a href="#" class="b-profile_nav_search">поисковые запросы</a></li>
		</ul>                 
	</div><!-- /.b-searchresult-->
	
	<div class="b-registration rel">
		
		<h2 class="mode">регистрация на портале</h2>
		<form action="" class="b-form b-form_common b-regform">
			<hr/>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings01">Фамилия</label>
					<div class="field validate">
						<input type="text" data-validate="fio" data-required="required" value="Гагарин" id="settings01" data-maxlength="30" data-minlength="2" name="sname" class="input">
						<em class="error required">Заполните</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>							
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings02">Имя</label>
					<div class="field validate">
						<input type="text" data-required="required" data-validate="fio" value="Юрий" id="settings02" data-maxlength="30" data-minlength="2" name="name" class="input" >	
						<em class="error required">Заполните</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>												
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings22">Отчество</label>
					<div class="field validate">
						<input type="text" data-validate="fio" value="" id="settings22" data-maxlength="30" data-minlength="2" name="name" class="input" >
						<em class="error required">Заполните</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>													
					</div>
				</div>
			</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings21" class="disable">Номер читательского билета РГБ</label>
					<div class="field validate">
						<input type="text" data-validate="number" value="" id="settings21" data-required="required" data-maxlength="30" data-minlength="2" name="sname" class="input">	
						<em class="error required">Заполните</em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings05">Пароль</label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="required" value="" id="settings05" data-maxlength="30" name="pass" class="pass_status input">										
						<em class="error required">Заполните</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
					<div class="passecurity">
						<div class="passecurity_lb">Защищенность пароля</div>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings55">Подтвердить пароль</label>
					<div class="field validate">
						<input data-identity="#settings05" type="password" data-required="required" value="" id="settings55" data-maxlength="30" name="pass" class="input" data-validate="password">										
						<em class="error identity ">Пароль не совпадает с введенным ранее</em>
						<em class="error required">Заполните</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
				</div>
			</div>
			<hr/>
			<div class="nookresult">
				<p class="error">Данные по номеру читательского билета не найдены в РГБ.</p>
				<p><a href="#" class="black">Пройдите процедуру полной регистрации</a></p>
			</div>
		</form>
	</div><!-- /.b-registration-->



</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>