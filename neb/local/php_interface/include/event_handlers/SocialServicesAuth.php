<?

use \Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler("main", "OnBeforeUserAdd", ["SocialServicesAuth", "beforeUserAdd"]);
EventManager::getInstance()->addEventHandler("main", "OnAfterUserAdd",  ["SocialServicesAuth", "afterUserAdd"]);

class SocialServicesAuth
{
    /**
     * @param $arFields
     * @return bool
     */
    public static function beforeUserAdd(&$arFields) {

        /**
         * При получении ответа из социльной сети (!!!) и отсутствии email в возвращенном профиле пользователя
         * перенаправляю на страницу регистрации для дозаполнения email
         */
        if (empty($_SESSION['ESIA_REGISTER']) && empty($arFields['EMAIL'])) {

            $_SESSION['ESIA_REGISTER'] = $arFields;
            static::JSRedirect('/auth/reg.php'); //LocalRedirect('/auth/reg.php');
        }
        /**
         * При заполненном массиве $_SESSION['ESIA_REGISTER'] удаляем поля EMAIL, LOGIN и UF_ESIA_FIELDS (???)
         */
        elseif (!empty($_SESSION['ESIA_REGISTER'])) {

            foreach ($_SESSION['ESIA_REGISTER'] as $k => $v) {
                if (in_array($k, ['EMAIL', 'LOGIN', 'UF_ESIA_FIELDS']))
                    continue;
                $arFields[$k] = $v;
            }
        }
        /**
         * При получении профиля из социалки и несовпадении логина и e-mail подменяем логин электронной почтой
         */
        elseif (!empty($arFields['EMAIL']) && !empty($arFields['LOGIN']) && $arFields['EMAIL'] !== $arFields['LOGIN']) {

            $arFields['LOGIN'] = $arFields['EMAIL'];
        }
    }

    /**
     * Выделено в отдельный метод на случай ошибки при добавлении пользователя
     * @param $arFields
     */
    public static function afterUserAdd(&$arFields) {
        unset($_SESSION['ESIA_REGISTER']);
    }

    /**
     * JS-редирект и закрытие попап-окна. Метод используется в классах: данном, EsiaWebService и RgbWebService.
     * @param string $path
     */
    public static function JSRedirect($path = '') {

        $obMain = new CMain();
        $obMain->RestartBuffer();

        if (!empty($path))
            $sRedirect = 'window.opener.location = "'.CUtil::JSEscape('http://'.$_SERVER['SERVER_NAME'].$path) . '";';
        else
            $sRedirect = '';

        die("<script type='text/javascript'>$sRedirect if (window.opener) { window.close();}</script>");
    }
}