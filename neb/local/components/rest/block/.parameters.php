<?php
$arComponentParameters = array(
    "PARAMETERS" => array(
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => Array("DEFAULT" => 3600),
        "SEF_MODE" => Array(
			'processRequest' => array(
				'NAME' 			=> GetMessage('PROCESS_REQUEST_NAME'),
				'DEFAULT' 		=> '/',
				'VARIABLES' 	=> array('token', 'action', 'ip', 'email', 'fingerprint'),
				'TYPE' 			=> array('GET')
			),
        ),
    ),
); 