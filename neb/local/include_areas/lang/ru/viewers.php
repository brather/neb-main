<?php
$MESS['APPLICATIONS_DESC'] = 'Для работы в НЭБ на мобильных устройствах установите одно из наших приложений.';
$MESS['APPLICATIONS_WIN'] = 'Приложение НЭБ для Windows';
$MESS['APPLICATIONS_MAC'] = 'Приложение НЭБ для Mac OS';
$MESS['APPLICATIONS_LINUX'] = 'Приложение НЭБ для Linux';
$MESS['APPLICATIONS_ANDROID'] = 'Приложение НЭБ для Android';
$MESS['APPLICATIONS_IOS'] = 'Приложение НЭБ для iOS';
$MESS['APPLICATIONS_WINPHONE'] = 'Приложение НЭБ для Windows Phone';
