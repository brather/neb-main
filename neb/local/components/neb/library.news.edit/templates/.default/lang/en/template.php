<?
$MESS['LIBRARY_NEWS_EDIT_REQ'] = "* - required fields";
$MESS['LIBRARY_NEWS_EDIT_MODE_EDIT'] = "editing news of the library";
$MESS['LIBRARY_NEWS_EDIT_MODE_ADD'] = "adding news of the library";
$MESS['LIBRARY_NEWS_EDIT_TITLE'] = "Title";
$MESS['LIBRARY_NEWS_EDIT_FILL'] = "Must be filled in";
$MESS['LIBRARY_NEWS_EDIT_SHORT_DESC'] = "Short description";
$MESS['LIBRARY_NEWS_EDIT_MAX_LEN'] = "More than 50000 characters";
$MESS['LIBRARY_NEWS_EDIT_MAX_LEN_255'] = "More than 255 characters";
$MESS['LIBRARY_NEWS_EDIT_MAX_LEN_1500'] = "More than 1500 characters";
$MESS['LIBRARY_NEWS_EDIT_MIN_LEN'] = "Less than 2 characters";
$MESS['LIBRARY_NEWS_EDIT_TEXT'] = "News text";
$MESS['LIBRARY_NEWS_EDIT_DATE'] = "News date";
$MESS['LIBRARY_NEWS_EDIT_ATTACH'] = "Attach video (insert link to www.youtube.com)";
$MESS['LIBRARY_NEWS_EDIT_PREVIEW'] = "View news review";
$MESS['LIBRARY_NEWS_EDIT_PUBLISH'] = "Publish on the website";
$MESS['LIBRARY_NEWS_EDIT_SAVE'] = "Save";
$MESS['LIBRARY_NEWS_EDIT_GO_TO'] = "Go to the";
$MESS['LIBRARY_NEWS_EDIT_NEWS_LIST'] = "list of news";
$MESS['LIBRARY_NEWS_EDIT_CANCEL'] = "Cancel";
?>
