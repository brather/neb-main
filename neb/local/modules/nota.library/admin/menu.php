<?
$aMenu = array();

CModule::IncludeModule("nota.library");
if(CNotaLibrary::isStaff())
{
	$aMenu = array(
		"parent_menu" => "global_menu_content",
		"section" => "nota.library",
		"module_id" => "nota.library",
		"sort" => 100,
		"text" => "Управление библиотекой",
		"title" => "Управление библиотекой",
		"icon" => "update_marketplace",
		"page_icon" => "",
		"items_id" => "menu_nota_library",
		"items" => array()
	);

	$aMenu["items"]= array(
		array(
			"text" => "Новости библиотеки",
			"title" => "Новости библиотеки",
			//"url" => "nota_library_news.php",
			"url" => "iblock_list_admin.php?IBLOCK_ID=3&type=library",
			"more_url" => array(
				"iblock_element_edit.php?IBLOCK_ID=3"
			)
		),
		array(
			"text" => "Выставки",
			"title" => "Выставки",
			//"url" => "nota_library_collections.php",
			"url" => "iblock_section_admin.php?IBLOCK_ID=6&type=collections",
			"more_url" => array(
				"iblock_section_edit.php?IBLOCK_ID=6&type=collections&ID=0",
				"iblock_list_admin.php?IBLOCK_ID=6&type=collections",
				"iblock_element_edit.php?IBLOCK_ID=6&type=collections",
			)
		),
		array(
			"text" => "Библиотека",
			"title" => "Библиотека",
			"url" => "nota_library_index.php",
		),

        array(
            "text" => "Пользователи",
            "title" => "Пользователи",
            "url" => "nota_library_user_list.php",
            "more_url" => array(
                "nota_library_user_edit.php"
            )
        ),

		array(
			"text" => "Опросы",
			"title" => "Опросы",
			"url" => "nota_vote_list.php",
			"more_url" => array(
				"nota_vote_list.php",
				"nota_vote_edit.php",
				"nota_vote_question_list.php",
				"nota_vote_question_edit.php",
			)
		),
	);
}

if (!empty($aMenu))
	return $aMenu;
else
	return false;
?>