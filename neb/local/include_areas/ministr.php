<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-footer_support">
    <div class="b-footer_support_tit"><?= Loc::getMessage('INCLUDE_MINISTR_TEXT') ?></div>
    <a class="ministr-link" href="http://mkrf.ru/" target="_blank"><img src="<?= MARKUP ?>i/ministr.png" alt=""></a>
</div>