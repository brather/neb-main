<?php
/**
 * User: agolodkov
 * Date: 02.04.2016
 * Time: 20:00
 */

namespace Neb\Main;

use Bitrix\Main\Entity\DataManager;

class PlanDigitizationLogTable extends DataManager
{
    public static function getTableName()
    {
        return 'plan_digitization_log';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID'          => array(
                'data_type' => 'integer',
                'primary'   => true,
            ),
            'TIMESTAMP'   => array(
                'data_type' => 'datetime',
            ),
            'ORDER_ID'    => array(
                'data_type' => 'integer',
            ),
            'USER_ID'     => array(
                'data_type' => 'integer',
            ),
            'COMMENT'     => array(
                'data_type' => 'text'
            ),
            'STATUS_ID'   => array(
                'data_type' => 'integer',
            ),
            'ACTION_NAME' => array(
                'data_type' => 'string'
            ),
        );
    }

}