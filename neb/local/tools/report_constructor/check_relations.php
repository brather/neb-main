<?
/**
 * Проверка связей для отчета
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!TblEntities::checkAccess()) {
    die('Permissions denied!');
}

//выбранные связи 
$relations = [];
if ($_REQUEST['join'])
    $relations = TblEntities::getRelationsId($_REQUEST['join']);

echo count($relations) == count($_REQUEST['entities']) ? 1 : 0;
