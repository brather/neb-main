<?
/**
 * Добавление цитаты из открытого просмотровщика
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized())
	exit();

$book_id = htmlspecialcharsbx(trim($_REQUEST['book_id']));
if(empty($book_id))
	exit();

$page 		= intval($_REQUEST['page']);
$preview 	= htmlspecialcharsbx(trim($_REQUEST['preview']));

CModule::IncludeModule("nota.userdata");
use Nota\UserData\Bookmarks;

$arFields = array(
	'BOOK_ID' 	=> $book_id,
	'NUM_PAGE' 	=> $page,
	'PREVIEW' 	=> $preview,
);

$id = Bookmarks::addFromArray($arFields);

// Определим тест ответа
$hintText = '';
if ( (bool)$id )
	$hintText = 'Закладка добавлена';
else
	$hintText = 'Ошибка добавления закладки';

if ( !(bool)$id )
	$id = rand(1000,100000);
?>

<div class="b-hintadded hidden" data-id="<?=$id?>"><?=$hintText?></div>