<?

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>
<div class="form-group" style="clear: left;">

<div class="nrf-fieldset-title">
    <?= GetMessage('GENERAL_CONST_PASSPORT'); ?>
</div>
    <script>
        var rulesAndMessages = {
            rules: {},
            messages: {}
        };
    </script>
    <div class="row">
        <div class="col-md-4 form-group">
            <label for="surname">
                <em class="hint">*</em>
                <?= GetMessage('LAST_NAME'); ?>
            </label>
            <input type="text" 
                data-required="required" 
                value="<?=$arResult["arUser"]['LAST_NAME']?>" 
                id="surname" 
                name="LAST_NAME" 
                class="form-control">
            <script>
                rulesAndMessages.rules["LAST_NAME"] = {
                    required: true,
                    maxlength: 30,
                    minlength: 2
                };
                rulesAndMessages.messages["LAST_NAME"] = {
                    required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                    maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                    minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                };
            </script>
        </div>
        <div class="col-md-4 form-group">
            <label for="firstname">
                <em class="hint">*</em>
                <?= GetMessage('NAME'); ?>
            </label>
            <input 
            type="text" 
            data-required="required" 
            data-validate="fio" 
            value="<?=$arResult["arUser"]['NAME']?>" 
            id="firstname" 
            name="NAME" 
            class="form-control" >
            <script>
                rulesAndMessages.rules["NAME"] = {
                    required: true,
                    maxlength: 30,
                    minlength: 2
                };
                rulesAndMessages.messages["NAME"] = {
                    required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                    maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                    minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                };
            </script>
        </div>
        <div class="col-md-4 form-group">
            <label for="midname">
                <?= GetMessage('SECOND_NAME'); ?>
            </label>
            <input type="text" 
            data-validate="fio" 
            value="<?=$arResult["arUser"]['SECOND_NAME']?>" 
            id="midname" 
            name="SECOND_NAME" 
            class="form-control" >
            <script>
                rulesAndMessages.rules["SECOND_NAME"] = {
                    maxlength: 30,
                    minlength: 2
                };
                rulesAndMessages.messages["SECOND_NAME"] = {
                    maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                    minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                };
            </script>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-xs-12">
            <em class="hint">*</em>
            <?= GetMessage('POL'); ?>
        </label>

        <label class="radio-inline">
            <input type="radio" name="PERSONAL_GENDER"  value="M"
            <? if ($arResult['arUser']['PERSONAL_GENDER'] == 'M') { ?> checked <? } ?>
            >
            <span class="lbl">
                <?= Loc::getMessage('MAIN_REGISTER_GENDER_MALE'); ?>
            </span>
        </label>
        <label class="radio-inline">
            <input type="radio" name="PERSONAL_GENDER" value="F"
            <? if ($arResult['arUser']['PERSONAL_GENDER'] == 'F') { ?> checked <? } ?>
            >
            <span class="lbl">
                <?= Loc::getMessage('MAIN_REGISTER_GENDER_FEMALE'); ?>
            </span>
        </label>    
        <script>
            rulesAndMessages.rules["PERSONAL_GENDER"] = {
                required: true
            };
            rulesAndMessages.messages["PERSONAL_GENDER"] = {
                required: "<?= Loc::getMessage('MAIN_REGISTER_SELECT'); ?>"
            };
        </script>
    </div>

    <div class="row">
        <div class="form-group col-md-4">
            <label>
                <em class="hint">*</em>
                <?= GetMessage('DATE'); ?>
            </label>
            <div class="input-group">
                <input 
                type="text" 
                class="form-control" 
                data-required="true" 
                value="<?=$arResult['arUser']['PERSONAL_BIRTHDAY']?>" 
                id="userbirthday" 
                name="PERSONAL_BIRTHDAY"
                data-yearsrestrict="12"
                >
                <span class="input-group-btn">
                    <a class="btn btn-default"
                    onclick="BX.calendar({node: 'userbirthday', field: 'userbirthday',  form: 'history', bTime: false, value: ''});"
                    >
                        <span class="glyphicon glyphicon-calendar"></span>
                    </a>
                </span>
            </div>
        </div>
        <script>
            rulesAndMessages.rules["PERSONAL_BIRTHDAY"] = {
                required: true,
                checkAge: 12
            };
            rulesAndMessages.messages["PERSONAL_BIRTHDAY"] = {
                required: "<?=GetMessage('MAIN_REGISTER_FILL');?>",
                checkAge: "<?=GetMessage('MAIN_REGISTER_AGE_WITH_PLACEHOLDER');?>"
            };
        </script>
    </div>

    <div class="row">

        <div class="col-xs-6 form-group">
            <label for="UF_PASSPORT_SERIES">
                <em class="hint">*</em>
                <?= GetMessage('MAIN_REGISTER_PASSPORT_NUMBER'); ?>
            </label>
            <input type="text"
                   value="<?= $arResult['arUser']['UF_PASSPORT_SERIES'] ?>"
                   id="UF_PASSPORT_SERIES"
                   name="UF_PASSPORT_SERIES"
                   class="form-control"
                   placeholder="<?= GetMessage(
                       'MAIN_REGISTER_PASSPORT_NUMBER_S'
                   ); ?>">
            <script>
                rulesAndMessages.rules["UF_PASSPORT_SERIES"] = {
                    required: true
                };
                rulesAndMessages.messages["UF_PASSPORT_SERIES"] = {
                    required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
                };
            </script>
        </div>
        <div class="col-xs-6 form-group">
            <label>&nbsp;</label>
            <input type="text"
                   value="<?= $arResult['arUser']['UF_PASSPORT_NUMBER'] ?>"
                   id="UF_PASSPORT_NUMBER"
                   name="UF_PASSPORT_NUMBER"
                   class="form-control"
                   placeholder="<?= GetMessage(
                       'MAIN_REGISTER_PASSPORT_NUMBER_N'
                   ); ?>">
            <script>
                rulesAndMessages.rules["UF_PASSPORT_NUMBER"] = {
                    required: true
                };
                rulesAndMessages.messages["UF_PASSPORT_NUMBER"] = {
                    required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
                };
            </script>
        </div>
    </div>

    <div class="form-group">
        <label for="UF_CITIZENSHIP">
            <em class="hint">*</em>
            <?= GetMessage('MAIN_REGISTER_CITIZENSHIP'); ?>
        </label>

        <select <? if (in_array(
            "UF_CITIZENSHIP", $arResult['arESIAFields']
        )): ?>disabled="disabled"
                <? else: ?>name="UF_CITIZENSHIP"<? endif; ?>
                id="UF_CITIZENSHIP" class="form-control">
            <?
            $arUF_CITIZENSHIP = nebUser::getFieldEnum(
                array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'),
                array("VALUE" => "ASC")
            );
            if (!empty($arUF_CITIZENSHIP)) {
                $last = count($arUF_CITIZENSHIP);
                foreach ($arUF_CITIZENSHIP as $arItem) {
                    if ($arItem['XML_ID'] == $last) {
                        $other = $arItem;
                        continue;
                    }
                    if ($arItem['DEF'] == "Y") {
                        $ruCitizenship = $arItem['ID'];
                    }
                    if (!empty($arResult["arUser"]['UF_CITIZENSHIP'])) {
                        ?>
                        <option
                            value="<?= $arItem['ID'] ?>"<?= ($arResult["arUser"]['UF_CITIZENSHIP']
                            == $arItem['ID']) ? ' selected="selected"'
                            : ''; ?>><?= $arItem['VALUE'] ?></option>
                        <?
                    } else {
                        ?>
                        <option
                            value="<?= $arItem['ID'] ?>"<?= ($arItem['DEF']
                            == "Y") ? ' selected="selected"'
                            : ''; ?>><?= $arItem['VALUE'] ?></option>
                        <?
                    }
                }
                ?>
                <option value="<?= $other['ID'] ?>"<?= ($other['DEF']
                    == "Y") ? ' selected="selected"'
                    : ''; ?>><?= $other['VALUE'] ?></option>

                <?
            }
            ?>
        </select>

        <script>
            rulesAndMessages.rules["UF_CITIZENSHIP"] = {
                required: true
            };
            rulesAndMessages.messages["UF_CITIZENSHIP"] = {
                required: "<?=GetMessage('MAIN_REGISTER_REQ');?>"
            };
        </script>
        
    </div>
    <div class="form-group">
        <label for="PERSONAL_NOTES">
            <?= GetMessage('ABOUT'); ?>
        </label>
            <textarea class="form-control" name="PERSONAL_NOTES"
                      id="PERSONAL_NOTES" data-minlength="2"
                      data-maxlength="800"
                      rows="5"
                ><?= $arResult['arUser']['PERSONAL_NOTES'] ?></textarea>
    </div>

<div class="nrf-fieldset-title">
    <?= GetMessage('MAIN_REGISTER_SPECIALIZATION'); ?>
</div>

    <div class="form-group">
        <label for="wk">
            <em class="hint">*</em>
            <?= GetMessage('MAIN_REGISTER_PLACE_OF_EMPLOYMENT'); ?>
        </label>

        <input type="text"
               value="<?= $arResult["arUser"]['WORK_COMPANY'] ?>"
               id="wk" 
               name="WORK_COMPANY" 
               class="form-control ">
        <script>
            rulesAndMessages.rules["WORK_COMPANY"] = {
                required: true,
                maxlength: 60,
                minlength: 2
            };
            rulesAndMessages.messages["WORK_COMPANY"] = {
                required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
            };
        </script>
    </div>
    <div class="form-group">
        <label for="known">
            <em class="hint">*</em>
            <?= GetMessage('MAIN_REGISTER_KNOWLEDGE'); ?>
        </label>
        <select
            name="UF_BRANCH_KNOWLEDGE"
            id="known"
            class="form-control">
            <option value="">
                <?= GetMessage('MAIN_REGISTER_SELECT'); ?>
            </option>
            <?
            $arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(
                array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE')
            );
            if (!empty($arUF_BRANCH_KNOWLEDGE)) {
                foreach ($arUF_BRANCH_KNOWLEDGE as $arItem) {
                    ?>
                    <option <?= $arResult["arUser"]['UF_BRANCH_KNOWLEDGE']
                    == $arItem['ID'] ? 'selected="selected"' : '' ?>
                        value="<?= $arItem['ID'] ?>">
                        <?= $arItem['VALUE'] ?>
                    </option>
                    <?
                }
            }
            ?>
        </select>
        <script>
            rulesAndMessages.rules["UF_BRANCH_KNOWLEDGE"] = {
                required: true
            };
            rulesAndMessages.messages["UF_BRANCH_KNOWLEDGE"] = {
                required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
            };
        </script>
    </div>

    <div class="form-group">
        <label for="useredu">
            <?= GetMessage('MAIN_REGISTER_EDUCATION'); ?>
        </label>

        <select name="UF_EDUCATION" id="useredu"
                class="form-control">
            <option value="">
                <?= GetMessage('MAIN_REGISTER_SELECT'); ?>
            </option>
            <?
            $arUF_EDUCATION = nebUser::getFieldEnum(
                array('USER_FIELD_NAME' => 'UF_EDUCATION')
            );
            if (!empty($arUF_EDUCATION)) {
                foreach ($arUF_EDUCATION as $arItem) {
                    ?>
                    <option <?= $arResult["arUser"]['UF_EDUCATION']
                    == $arItem['ID'] ? 'selected="selected"' : '' ?>
                        value="<?= $arItem['ID'] ?>">
                        <?= $arItem['VALUE'] ?>
                    </option>
                    <?
                }
            }
            ?>
        </select>
        <script>
            rulesAndMessages.rules["UF_EDUCATION"] = {
                required: false
            };
            rulesAndMessages.messages["UF_EDUCATION"] = {
                required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
            };
        </script>
    </div>

<div class="nrf-fieldset-title">
    <?= GetMessage('MAIN_REGISTER_LOGIN_BLOCK'); ?>
</div>

    <div class="form-group">
        <?
        if (($arResult['arUser']['EXTERNAL_AUTH_ID'] != 'socservices')) {
            ?>
            <label for="userlogin">
                <em class="hint">*</em>
                <?= GetMessage('MAIN_REGISTER_LOGIN'); ?>
            </label>
            <input type="text"
                   class="form-control"
                   name="LOGIN"
                   data-minlength="3"
                   data-maxlength="50"
                   id="userlogin"
                   value="<?= $arResult["arUser"]['LOGIN'] ?>">
            <?
        } else {
            ?>
            <input type="hidden" name="LOGIN" id="userlogin"
                   value="<?= $arResult["arUser"]['LOGIN'] ?>">
            <?
        }
        ?>
        <script>
            rulesAndMessages.rules["LOGIN"] = {
                required: true,
                maxlength: 50,
                minlength: 3
            };
            rulesAndMessages.messages["LOGIN"] = {
                required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
            };
        </script>
    </div>
    <div class="form-group">
        <label for="useremail">
            <em class="hint">*</em>
            <?= GetMessage('MAIN_REGISTER_EMAIL'); ?>
        </label>

        <input type="text"
               class="form-control"
               name="EMAIL"
               id="useremail"
               data-validate="email"
               value="<?= $arResult["arUser"]['EMAIL'] ?>">
        <script>
            rulesAndMessages.rules["EMAIL"] = {
                required: true,
                email: true
            };
            rulesAndMessages.messages["EMAIL"] = {
                required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>",
                email: "<?= Loc::getMessage('MAIN_REGISTER_EMAIL_ERROR'); ?>"
            };
        </script>
    </div>
    <div class="form-group">
        <label for="usermobile">            
            <?= GetMessage('REGISTER_FIELD_PERSONAL_MOBILE'); ?>
        </label>
        <input type="text"
               class="form-control"
               name="PERSONAL_MOBILE"
               id="usermobile"
               value="<?= $arResult["arUser"]['PERSONAL_MOBILE'] ?>">

    </div>


    <div class="row">
        <div class="col-sm-6">

            <div class="nrf-fieldset-title">
                <?= GetMessage('REGISTER_GROUP_ADDRESS_REGISTERED'); ?>
            </div>

            <div class="checkbox">&nbsp;</div>

            <div class="form-group">
                <label for="regpostindex">
                    <em class="hint">*</em>
                    <?= GetMessage('REGISTER_FIELD_PERSONAL_ZIP'); ?>
                </label>
                <input type="number"
                    class="form-control"
                    name="PERSONAL_ZIP"
                    id="regpostindex"
                    value="<?= $arResult["arUser"]['PERSONAL_ZIP'] ?>">
                <script>
                    rulesAndMessages.rules["PERSONAL_ZIP"] = {
                        required: true,
                        maxlength: 6
                    };
                    rulesAndMessages.messages["PERSONAL_ZIP"] = {
                        required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>
            <div class="form-group">
                <label for="regregion">
                    <?= GetMessage('MAIN_REGISTER_REGION'); ?>
                </label>
                <input type="text"
                       class="form-control"
                       name="UF_REGION"
                       id="regregion"
                       value="<?= $arResult["arUser"]['UF_REGION'] ?>">
                <script>
                    rulesAndMessages.rules["UF_REGION"] = {
                        required: false,
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["UF_REGION"] = {
                        required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                        minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>


            <div class="form-group">
                <label for="regarea">
                    <?= GetMessage('MAIN_REGISTER_AREA'); ?>
                </label>
                <input type="text"
                       class="form-control"
                       name="UF_AREA"
                       id="regarea"
                       value="<?= $arResult["arUser"]['UF_AREA'] ?>">
                <script>
                    rulesAndMessages.rules["UF_AREA"] = {
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["UF_AREA"] = {
                        maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                        minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>

            <div class="form-group">
                <label for="regcity">
                    <em class="hint">*</em>
                    <?= GetMessage('REGISTER_FIELD_PERSONAL_CITY'); ?>
                </label>
                <input type="text"
                       class="form-control"
                       name="PERSONAL_CITY"
                       id="regcity"
                       value="<?= $arResult["arUser"]['PERSONAL_CITY'] ?>">
                <script>
                    rulesAndMessages.rules["PERSONAL_CITY"] = {
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["PERSONAL_CITY"] = {
                        required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                        minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>

            <div class="form-group">
                <label for="regstreet">
                    <em class="hint">*</em>
                    <?= GetMessage('REGISTER_FIELD_PERSONAL_STREET'); ?>
                </label>
                <input type="text"
                       class="form-control"
                       name="PERSONAL_STREET"
                       id="regstreet"
                       value="<?= $arResult["arUser"]['PERSONAL_STREET'] ?>">
                <script>
                    rulesAndMessages.rules["PERSONAL_STREET"] = {
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["PERSONAL_STREET"] = {
                        required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                        minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>

            <div class="form-group">
                <label for="regdom">
                    <em class="hint">*</em>
                    <?= GetMessage('REGISTER_FIELD_PERSONAL_HOUSE'); ?>
                </label>
                <input type="text"
                       class="form-control"
                       name="UF_CORPUS"
                       id="regdom"
                       value="<?= $arResult["arUser"]['UF_CORPUS'] ?>">
                <script>
                    rulesAndMessages.rules["UF_CORPUS"] = {
                        required: true,
                        maxlength: 6
                    };
                    rulesAndMessages.messages["UF_CORPUS"] = {
                        required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>

            <div class="form-group">
                <label for="regkorpus">
                    <?= GetMessage('REGISTER_FIELD_PERSONAL_BUILDING'); ?>
                </label>

                <input type="text"
                       class="form-control"
                       name="UF_STRUCTURE"
                       id="regkorpus"
                       value="<?= $arResult["arUser"]['UF_STRUCTURE'] ?>">            
            </div>

            <div class="form-group">
                <label for="regkvartira">
                    <?= GetMessage('REGISTER_FIELD_PERSONAL_APPARTMENT'); ?>
                </label>

                <input type="text"
                       class="form-control"
                       name="UF_FLAT"
                       id="regkvartira"
                       value="<?= $arResult["arUser"]['UF_FLAT'] ?>">
                <script>
                    rulesAndMessages.rules["UF_FLAT"] = {
                        required: false,
                        minlength: 1,
                        maxlength: 6
                    };
                    rulesAndMessages.messages["UF_FLAT"] = {
                        required: "<?=GetMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=GetMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                        minlength: "<?=GetMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                    };
                </script>
            </div>
        </div>
        <div class="col-sm-6">

    
            <div class="nrf-fieldset-title">
                <?= GetMessage('REGISTER_GROUP_ADDRESS_RESIDENCE'); ?>
            </div>

            <div class="checkbox">
                <label for="livwhereregistered">
                    <input 
                    type="checkbox"
                    <?if ($arResult['arUser']['UF_PLACE_REGISTR'] == '1') {?>
                        checked="checked"
                    <?}?>
                    data-checkbox-for="UF_PLACE_REGISTR"
                    id="livwhereregistered">
                    <span class="lbl">
                        <?= GetMessage('REGISTER_GROUP_ADDRESS_CHECKBOX_NOTE'); ?>
                        <!--(<?=$arResult["arUser"]['UF_PLACE_REGISTR']?>)-->
                    </span>
                </label>
                <input
                    type="hidden"
                    name="UF_PLACE_REGISTR"
                    id="livwhereregistered-val"
                    value="<?=$arResult['arUser']['UF_PLACE_REGISTR']?>"
                >
            </div>

            <div class="living-address-group">
                <div class="form-group">
                    <label for="reindex">
                        <?= GetMessage('REGISTER_FIELD_PERSONAL_ZIP'); ?>
                    </label>

                    <input type="number"
                           class="form-control"
                           name="WORK_ZIP"
                           id="reindex"
                           data-mirror="regpostindex"
                           value="<?= $arResult["arUser"]['WORK_ZIP'] ?>">
                    <script>
                        rulesAndMessages.rules["WORK_ZIP"] = {
                            number: true
                        };
                        rulesAndMessages.messages["WORK_ZIP"] = {
                            number: "<?=Loc::getMessage('REGISTER_FIELD_PERSONAL_DIGITS_ONLY');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="livregion">
                        <?= GetMessage('MAIN_REGISTER_REGION'); ?>
                    </label>

                    <input type="text"
                           class="form-control"
                           name="UF_REGION2"
                           id="livregion"
                           data-mirror="regregion"
                           value="<?= $arResult["arUser"]['UF_REGION2'] ?>">
                    <script>
                        rulesAndMessages.rules["UF_REGION2"] = {
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["UF_REGION2"] = {
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="liverayon">
                        <?= GetMessage('MAIN_REGISTER_AREA'); ?>
                    </label>

                    <input type="text"
                           class="form-control"
                           name="UF_AREA2"
                           id="liverayon"
                           data-mirror="regarea"
                           value="<?= $arResult["arUser"]['UF_AREA2'] ?>">
                    <script>
                        rulesAndMessages.rules["UF_AREA2"] = {
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["UF_AREA2"] = {
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>


                <div class="form-group">
                    <label for="livingcity">
                        <?= GetMessage('REGISTER_FIELD_PERSONAL_CITY'); ?>
                    </label>

                    <input type="text"
                           class="form-control"
                           name="WORK_CITY"
                           id="livingcity"
                           data-mirror="regcity"
                           value="<?= $arResult["arUser"]['WORK_CITY'] ?>">
                    <script>
                        rulesAndMessages.rules["WORK_CITY"] = {
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["WORK_CITY"] = {
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="livingstreet">
                        <?= GetMessage('REGISTER_FIELD_PERSONAL_STREET'); ?>
                    </label>
                    <input type="text"
                           class="form-control"
                           name="WORK_STREET"
                           id="livingstreet"
                           data-mirror="regstreet"
                           value="<?= $arResult["arUser"]['WORK_STREET'] ?>">
                    <script>
                        rulesAndMessages.rules["WORK_STREET"] = {
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["WORK_STREET"] = {
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>
                <div class="form-group">
                    <label for="livinghouse">
                        <?= GetMessage('REGISTER_FIELD_PERSONAL_HOUSE'); ?>
                    </label>
                    <input type="text"
                           class="form-control"
                           name="UF_HOUSE2"
                           id="livinghouse"
                           data-mirror="regdom"
                           value="<?= $arResult["arUser"]['UF_HOUSE2'] ?>">
                    <script>
                        rulesAndMessages.rules["UF_HOUSE2"] = {
                            maxlength: 30,
                            minlength: 1
                        };
                        rulesAndMessages.messages["UF_HOUSE2"] = {
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>
                <div class="form-group">            
                    <label for="livingkorpus">
                        <?= GetMessage('REGISTER_FIELD_PERSONAL_BUILDING'); ?>
                    </label>
                    <input type="text"
                           class="form-control"
                           name="UF_STRUCTURE2"
                           id="livingkorpus"
                           data-mirror="regkorpus"
                           value="<?= $arResult["arUser"]['UF_STRUCTURE2'] ?>">
                    
                </div>
                <div class="form-group">
                    <label for="livingkvartira">
                        <?= GetMessage('REGISTER_FIELD_PERSONAL_APPARTMENT'); ?>
                    </label>
                    <input type="text"
                           class="form-control"
                           name="UF_FLAT2"
                           id="livingkvartira"
                           data-mirror="regkvartira"
                           value="<?= $arResult["arUser"]['UF_FLAT2'] ?>">
                    <script>
                        rulesAndMessages.rules["UF_FLAT2"] = {
                            maxlength: 6
                        };
                        rulesAndMessages.messages["UF_FLAT2"] = {
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>
            </div>

        </div>
    </div>


    <div class="form-group" data-perpage-widget-radio>
        <label><?= GetMessage('COUNT_SEARCH'); ?></label>
        <label for="v15">
            <input type="radio" name="UF_SEARCH_PAGE_COUNT" value="15" id="v15" 
                <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT'] == '15') { ?> checked <? } ?>
            />
            <span class="lbl">15</span>
        </label>
        <label for="v30">
            <input type="radio" name="UF_SEARCH_PAGE_COUNT" value="30" id="v30"
                <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT'] == '30') { ?> checked <? } ?>
            />
            <span class="lbl">30</span>
        </label>
        <label for="v45">
            <input type="radio" name="UF_SEARCH_PAGE_COUNT" value="45" id="v45"
                <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT'] == '45') { ?> checked <? } ?>
            />
            <span class="lbl">45</span>
        </label>
    </div>

<?
$sImage = (intval($arResult['arUser']["UF_SCAN_PASSPORT1"]) > 0)
    ? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT1"]) : '';
if (isset($sImage["SRC"])) {
    $sImage = $sImage["SRC"];
}
?>
<? 
// $APPLICATION->IncludeComponent(
//     "notaext:plupload",
//     "scan_passport1",
//     array(
//         "MAX_FILE_SIZE"     => "10",
//         "FILE_TYPES"        => "jpg,jpeg,png",
//         "DIR"               => "tmp_register",
//         "FILES_FIELD_NAME"  => "profile_file",
//         "MULTI_SELECTION"   => "N",
//         "CLEANUP_DIR"       => "Y",
//         "UPLOAD_AUTO_START" => "Y",
//         "RESIZE_IMAGES"     => "Y",
//         "RESIZE_WIDTH"      => "110",
//         "RESIZE_HEIGHT"     => "110",
//         "RESIZE_CROP"       => "Y",
//         "RESIZE_QUALITY"    => "98",
//         "UNIQUE_NAMES"      => "Y",
//         "PERSONAL_PHOTO"    => $sImage,
//         "FULL_NAME"         => $arResult['arUser']['NAME'] . ' '
//             . $arResult['arUser']['LAST_NAME'],
//         "USER_EMAIL"        => $arResult['arUser']['EMAIL'],
//         "DATE_REGISTER"     => $arResult['arUser']['DATE_REGISTER'],
//         "UF_STATUS"         => $arResult['arUser']['UF_STATUS'],
//     ),
//     false
// ); 
?>
<?
$sImage = (intval($arResult['arUser']["UF_SCAN_PASSPORT2"]) > 0)
    ? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT2"]) : '';
if (isset($sImage["SRC"])) {
    $sImage = $sImage["SRC"];
}
?>
<? 
// $APPLICATION->IncludeComponent(
//     "notaext:plupload",
//     "scan_passport2",
//     array(
//         "MAX_FILE_SIZE"     => "10",
//         "FILE_TYPES"        => "jpg,jpeg,png",
//         "DIR"               => "tmp_register",
//         "FILES_FIELD_NAME"  => "profile_file",
//         "MULTI_SELECTION"   => "N",
//         "CLEANUP_DIR"       => "Y",
//         "UPLOAD_AUTO_START" => "Y",
//         "RESIZE_IMAGES"     => "Y",
//         "RESIZE_WIDTH"      => "110",
//         "RESIZE_HEIGHT"     => "110",
//         "RESIZE_CROP"       => "Y",
//         "RESIZE_QUALITY"    => "98",
//         "UNIQUE_NAMES"      => "Y",
//         "PERSONAL_PHOTO"    => $sImage,
//         "FULL_NAME"         => $arResult['arUser']['NAME'] . ' '
//             . $arResult['arUser']['LAST_NAME'],
//         "USER_EMAIL"        => $arResult['arUser']['EMAIL'],
//         "DATE_REGISTER"     => $arResult['arUser']['DATE_REGISTER'],
//         "UF_STATUS"         => $arResult['arUser']['UF_STATUS'],
//     ),
//     false
// ); 
?>
