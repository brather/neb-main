(function ($) {
    $.fn.nebCharts = function () {
        var chart = $('<canvas width="900" height="400"></canvas>');
        $(this).each(function () {
            if (this.chartActive) {
                return;
            }
            this.chartActive = true;
            var thisChartContainer = this;
            var currentChart = chart.clone();
            FRONT.spinner.execute($(thisChartContainer).get(0), FRONT.spinner.graphOpts);
            $(thisChartContainer).append(currentChart);
            var labels = $(this).data('chart-labels');
            var lines = $(this).data('chart-lines');
            var dataUrl = $(this).data('chart-data-url');
            var showMeta = $(this).data('chart-show-meta');
            var options = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                scaleShowHorizontalLines: true,
                scaleShowVerticalLines: true,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

            };
            var buildChart = function () {
                var data = {
                    labels: labels,
                    datasets: []
                };
                for (var key in lines) {
                    data.datasets.push({
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: lines[key]
                    });
                }
                var ctx = currentChart.get(0).getContext("2d");
                new Chart(ctx).Line(data, options);
            };

            if (!lines) {
                $.ajax(dataUrl)
                    .done(function (data) {

                        $(thisChartContainer).find('.spinner').remove();
                        
                        var series = neb.objectPathSearch(data, 'xAxis.series');
                        labels = neb.objectPathSearch(data, 'yAxis.labels');
                        lines = [];
                        for (var key in series) {
                            if ($.isArray(series[key].data)) {
                                lines.push(series[key].data);
                            }
                        }
                        if (showMeta) {
                            /** @todo сделать для нескольких линий */
                            var avgValues = [];
                            if (lines[0]) {
                                avgValues[0] = 0;
                                for (var key in lines[0]) {
                                    avgValues[0] += lines[0][key];
                                }
                                avgValues[0] = avgValues[0] / lines[0].length;
                            }
                            var totalCount = neb.objectPathSearch(data, 'meta.totalCount');
                            $(thisChartContainer).parent().prepend(
                                '<p>'
                                + (totalCount[0] ? ('Общее количество: ' + totalCount[0]) : '')
                                + (avgValues[0] ? ('&nbsp;|&nbsp;' + 'В среднем: ' + avgValues[0].toFixed(2)) : '')
                                + '</p>'
                            );
                        }
                        buildChart();
                    })
                    .fail(function () {
                        $(thisChartContainer).find('.spinner').remove();
                        //console.log("error " + dataUrl);
                    });
            } else {
                buildChart();
            }

        });
    }
})(jQuery);

$(function () {
    $('[data-chart-type]:visible').nebCharts();
    $('.uleditstat .active-parent-li .active-parent').click(function(e) {
        e.preventDefault();
        $(this).parent().find('[data-chart-type]').nebCharts();
    });
});