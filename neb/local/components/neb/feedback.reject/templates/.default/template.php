<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$arRequest = $component->getRequest();
$APPLICATION->SetTitle(Loc::getMessage('FEEDBACK_REJECT_TITLE') . $arRequest['id']);

if ($arRequest['ajax'] == 'Y') {
    $component->restartBuffer();
}

if ($arResult['json'] == true) {
    $component->showJson($arResult);
}

?>

<div class="feedback-reject">

    <?=Loc::getMessage('FEEDBACK_REJECT_DATE_CREATE')?>: <?=$arResult['TICKET']['DATE_CREATE'];?><br /><br />

    <?=Loc::getMessage('FEEDBACK_REJECT_THEME')?>: <?=$arResult['TICKET']['FIELDS']['FB_THEME']['ANSWER_TEXT'];?><br /><br />

    <? $arBook = $arResult['TICKET']['FIELDS']['FB_BOOK_ID']['BOOK'];
    if (!empty($arBook['id'])): ?>
        <?=Loc::getMessage('FEEDBACK_REJECT_BOOK')?>: <a href="/catalog/<?=$arBook['id'];?>/"><?
            echo trim($arBook['authorbook'] .': '.$arBook['title']); ?></a><br /><br />
    <? endif; ?>

    <?=Loc::getMessage('FEEDBACK_REJECT_MESSAGE')?>: <?=$arResult['TICKET']['FIELDS']['FB_TEXT']['USER_TEXT'];?><br /><br />
    
    <?=Loc::getMessage('FEEDBACK_REJECT_WRITE_CAUSE')?><br /><br />

    <? foreach($arResult['ERROR'] as $sError): ?>
        <?ShowError($sError);?>
    <? endforeach; ?>

    <form role="form" id="rejectFormFeedback" method="post" action="<?=$arParams['FEEDBACK_REJECT_URL']?:$_SERVER['REDIRECT_URL'];
                                                                ?>?id=<?=$arRequest['id']?>&ajax=<?=$arRequest['ajax']?>">
        <input type="hidden" name="FEEDBACK[ACTION]" value="REJECT" />
        <div class="form-group">
            <textarea name="FEEDBACK[MESSAGE]" style="width:100%; height: 100px;" class="form-control"><?
                echo $arRequest['FEEDBACK']['MESSAGE'] ? : $arResult['TICKET']['FIELDS']['FB_REJECT_TEXT']['USER_TEXT'];
            ?></textarea>
        </div>
            <input class="btn btn-primary btn-lg" type="submit" value="<?=Loc::getMessage('FEEDBACK_REJECT_SUBMIT')?>" />
    </form>

</div>

<?if ($arRequest['ajax'] == 'Y')
    die();