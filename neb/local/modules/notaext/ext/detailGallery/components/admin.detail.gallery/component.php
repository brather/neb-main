<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/*
Компонент выводит код свойства "Галерея для детальной страницы" для инфоблока

$arParams
IBLOCK_ID - ID инфоблока в котором хранятся фото
ID - ID элемента материала для которого будут добавляться фото
WIDTH - Ширина слайда по умолчанию
HEIGHT - Высота слайда по умолчанию
MAX_WIDTH_ORIGINAL - Максимальная ширина оригинала
MAX_HEIGHT_ORIGINAL - Максимальная высота оригинала
*/

if (intval($arParams['IBLOCK_ID']) <= 0)
    return false;

if (intval($arParams['ID']) <= 0)
    return false;

if (intval($arParams['WIDTH']) <= 0)
    $arParams['WIDTH'] = 500;

if (intval($arParams['HEIGHT']) <= 0)
    $arParams['HEIGHT'] = 800;

if (intval($arParams['MAX_WIDTH_ORIGINAL']) <= 0)
    $arParams['MAX_WIDTH_ORIGINAL'] = 1600;

if (intval($arParams['MAX_HEIGHT_ORIGINAL']) <= 0)
    $arParams['MAX_HEIGHT_ORIGINAL'] = 1600;

if (empty($arParams['ID_UPLOADER_BLOCK']))
    $arParams['ID_UPLOADER_BLOCK'] = randString(7);

if (empty($arParams['FILE_EXTENSION']))
    $arParams['FILE_EXTENSION'] = 'jpg,jpeg,png';


CModule::IncludeModule("iblock");

$properties = CIBlockProperty::GetList(Array(), Array("ACTIVE" => "Y", "IBLOCK_ID" => $arParams['IBLOCK_ID'], 'CODE' => 'REAL_PICTURE'));
if ($properties->SelectedRowsCount() <= 0) {
    $arFields = Array(
        "NAME" => "Оригинал",
        "ACTIVE" => "Y",
        "SORT" => "100",
        "CODE" => "REAL_PICTURE",
        "PROPERTY_TYPE" => "F",
        "IBLOCK_ID" => $arParams['IBLOCK_ID']
    );
    $ibp = new CIBlockProperty;
    $PropID = $ibp->Add($arFields);
}

$this->IncludeComponentTemplate();

?>