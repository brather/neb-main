<?php
$MESS['DIGITIZATION_STAT_TITLE']      = 'Статистика по количеству поступивших, отработанных и находящихся в работе заявок';
$MESS['DIGITIZATION_STAT_DATE_FROM']  = 'Начало периода';
$MESS['DIGITIZATION_STAT_DATE_TO']    = 'Конец периода';

$MESS['DIGITIZATION_STAT_BOOK']       = 'Книга';
$MESS['DIGITIZATION_STAT_LIBRARY']    = 'Библиотека';
$MESS['DIGITIZATION_STAT_STATUS']     = 'Статус';

$MESS['DIGITIZATION_STAT_EXCEL']      = 'Выгрузить в .xls';
$MESS['DIGITIZATION_STAT_SHOW']       = 'Показать статистику';