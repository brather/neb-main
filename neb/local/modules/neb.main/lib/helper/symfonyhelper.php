<?php
/**
 * User: agolodkov
 * Date: 05.09.2016
 * Time: 17:59
 */

namespace Neb\Main\Helper;


use Bitrix\Main\Config\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Class SymfonyHelper
 *
 * @package Neb\Main\Helper
 */
final class SymfonyHelper
{
    const ENTER_FILE = '/symfony.php';
    /**
     * @var SymfonyHelper
     */
    private static $_instance;
    /**
     * @var
     */
    private $_kernel;
    /**
     * @var
     */
    private $_request;

    /**
     * @return SymfonyHelper
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * @return \AppKernel
     */
    public function getKernel()
    {
        if (!$this->_kernel instanceof \AppKernel) {
            require_once $_SERVER['DOCUMENT_ROOT'] . '/../app/bootstrap.php.cache';
            require_once $_SERVER['DOCUMENT_ROOT'] . '/../app/AppKernel.php';
            $debug = Configuration::getInstance()->get('exception_handling');
            $debug = isset($debug['debug']) ? $debug['debug'] : false;
            $this->_kernel = new \AppKernel(true === $debug ? 'dev' : 'prod', $debug);
            $this->_kernel->boot();
        }

        return $this->_kernel;
    }

    /**
     * @return array|mixed|Request
     */
    public function getRequest()
    {
        if (!$this->_request instanceof Request) {
            $this->getKernel();
            $this->_request = Request::createFromGlobals();
        }

        return $this->_request;
    }

    /**
     * @param string $scope
     *
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContainer($scope = null)
    {
        $container = $this->getKernel()->getContainer();
        if (null !== $scope) {
            $container->enterScope($scope);
            if ('request' === $scope) {
                $request = $this->getRequest();
                $container->set('request', $request, 'request');
            }
        }

        return $container;
    }

    public function handleRequest()
    {
        $kernel = $this->getKernel();
        $kernel->loadClassCache();
        $request = $this->getRequest();
        $response = $kernel->handle($request);
        $response->sendHeaders();
        $response->sendContent();
        $kernel->terminate($request, $response);
    }

    /**
     * Copy of \Symfony\Bundle\FrameworkBundle\Controller\Controller::forward
     *
     * @param       $controller
     * @param array $path
     * @param array $query
     *
     * @return Response
     * @see \Symfony\Bundle\FrameworkBundle\Controller\Controller::forward
     */
    public function forwardController($controller, array $path = array(), array $query = null)
    {
        $path['_controller'] = $controller;
        $server = $_SERVER;
        $server['SCRIPT_NAME'] = static::ENTER_FILE;
        $server['PHP_SELF'] = static::ENTER_FILE;
        $subRequest = $this->getRequest()->duplicate(
            $query,
            null,
            $path,
            null,
            null,
            $server
        );
        $this->resetBaseUrl();

        return $this->getContainer()->get('http_kernel')->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
    }

    /**
     * @param string $baseUrl
     */
    public function resetBaseUrl($baseUrl = '/')
    {
        $this->getContainer()->get('router')->getContext()->setBaseUrl($baseUrl);
    }

    /**
     * @param string $route
     * @param array  $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function forwardRequest($route, $params = [])
    {
        $server = [
            'REQUEST_URI' => $this->getContainer()->get('router')->generate($route, $params),
        ];
        $subRequest = $this->getRequest()->duplicate(null, null, null, null, null, $server);

        return $this->getContainer()->get('http_kernel')->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
    }
}