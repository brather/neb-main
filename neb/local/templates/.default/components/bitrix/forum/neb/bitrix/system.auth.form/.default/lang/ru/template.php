<?
$MESS ["AUTH_LOGIN_BUTTON"] = "Войти";
$MESS ["AUTH_CLOSE_WINDOW"] = "Закрыть";
$MESS ["AUTH_LOGIN"] = "Логин";
$MESS ["AUTH_PASSWORD"] = "Пароль";
$MESS ["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS ["AUTH_FORGOT_PASSWORD_2"] = "Забыли свой пароль?";
$MESS ["AUTH_REGISTER"] = "Регистрация";
$MESS ["AUTH_LOGOUT_BUTTON"] = "Выйти";
$MESS ['AUTH_CAPTCHA_PROMT'] = "Введите слово на картинке";
$MESS ['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND'] = "Адес. эл. почты не найден";
$MESS ['AUTH_PASS_RECOVERY_TITLE'] = "Восстановление пароля";
$MESS ['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC'] = "Введите адрес электронной почты, указанный при регистрации";
$MESS ['AUTH_PASS_RECOVERY_SUCCESS'] = "На почту выслано письмо с ссылкой на сброс пароля";
$MESS ['AUTH_PASS_RECOVERY_RECOVER'] = "Восстановить";
$MESS ['AUTH_CLOSE'] = "Закрыть";
?>