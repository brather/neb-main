<?php
/**
 *
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 13.04.2017
 * Time: 13:09
 */

namespace Neb\Main\Agents;

use \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\Loader,
    \Neb\Main\Helper\DbHelper;

/**
 * Class Sitemap - периодическая генерация карт сайта
 *
 * Документация и инструментарий:
 * https://www.sitemaps.org/ru/protocol.html
 * https://yandex.ru/support/webmaster/indexing-options/sitemap.xml
 * https://webmaster.yandex.ru/tools/sitemap/?tab=url
 *
 * @package Neb\Main\Agents
 */
class Sitemap
{
    const DB_ONLY_UNIQUE     = true;                           // Экспортировать только уникальные издания без дублиаций
    const DB_ONLY_DIGITIZED  = false;                          // Экспортировать только оцифрованные издания
    const FILE_LOCATION      = '/upload/sitemap';              // Расположение карт сайта
    const FILE_LOCATION_TMP  = '/upload/tmp_sitemap';          // Временное расположение карт сайта
    const FILE_2_GZIP        = true;                           // Упаковывать файлы в gzip
    const FILE_ITEMS_LIMIT   = 50000;                          // Максимальное количество элементов на один файл sitemap

    /**
     * Сохранение sitemaps.xml для изданий
     *
     * @return string
     * @throws \Exception
     */
    public static function save() {

        $sResult = "\\" . __METHOD__ . '();';

        try {

            self::prepareTmpDir();

            self::saveSitemap($sResult, 'Cards', 0.7);
            self::saveSitemap($sResult, 'Parts', 0.7);
            self::saveSitemap($sResult, 'Libraries', 0.8);
            self::saveSitemap($sResult, 'News', 0.9);
            self::saveSitemap($sResult, 'Collections', 0.9);
            self::saveIndex();

            self::move2PermanentDir();

        } catch (\Exception $e) {
            $log = new \CEventLog();
            $log->Log('ERROR', 'sitemap', 'neb.main', __FUNCTION__, strval($e));
        }

        return $sResult;
    }

    /**
     * Управление разделом
     */
    private static function prepareTmpDir() {
        Service::removeDirectory($_SERVER['DOCUMENT_ROOT'] . self::FILE_LOCATION_TMP);
        Service::createDirectory($_SERVER['DOCUMENT_ROOT'] . self::FILE_LOCATION_TMP);
    }

    /**
     * Итерационно сохраняет XML-карту сущности
     *
     * @param $sResult
     * @param $sEntity
     * @param float $fPriority
     */
    private static function saveSitemap($sResult, $sEntity, $fPriority = 0.5) {

        $iStep = $iLastId = 0;
        $sMethod = 'get' . $sEntity;
        $iTotalCount = self::$sMethod(0, true);

        while (($iStep == 0 || $iLastId > 0) && Service::addCheckDate($sResult)) {
            $iStep++;
            $arResult  = self::$sMethod($iLastId, false, $fPriority);
            $sFileName = self::getSitemapFileName($sEntity, $iTotalCount, $iStep);
            $iLastId   = self::writeSitemapFile($arResult, $sFileName);
        }
    }

    /**
     * Формирование имени файла
     *
     * @param $sEntity
     * @param $iTotalCount
     * @param $iStep
     * @return string
     */
    private static function getSitemapFileName($sEntity, $iTotalCount = 0, $iStep = 0) {

        $sFileName = '/sitemap_' . strtolower($sEntity);

        // дополнение нулями названий файлов для их упорядочивания
        if ($iTotalCount > 0) {
            $iDiff = strlen(ceil($iTotalCount / self::FILE_ITEMS_LIMIT)) - strlen($iStep);
            while ($iDiff > 0) {
                $iStep = '0' . $iStep;
                $iDiff--;
            }
        }

        $sFileName .= (false !== stripos($sEntity, 'index')) ? '.xml' : "_$iStep.xml.gz";

        return $sFileName;
    }

    /**
     * Получение XML-файлов из раздела
     *
     * @param $sDir
     * @return array
     */
    private static function getXmlFiles($sDir) {
        return glob($sDir . '/*.xml*');
    }

    /**
     * Получает с диска список файлов для создания их индекса
     */
    private static function saveIndex() {

        $arResult = [];
        $sTmpDir = $_SERVER['DOCUMENT_ROOT'] . self::FILE_LOCATION_TMP;

        $arFiles = self::getXmlFiles($sTmpDir);
        foreach($arFiles as $k => $sFile) {
            $arResult[] = [
                'loc'     => '/' . end(explode('/', $sFile)),
                'lastmod' => filemtime($sFile),
                'sort'    => (false !== stripos($sFile, 'cards') ? 0 : (false !== stripos($sFile, 'parts') ? 10000 : 20000)) + $k
            ];
        }
        unset($arFiles);

        usort($arResult, function ($a, $b) {
            return $a['sort'] - $b['sort'];
        });

        self::writeSitemapFile($arResult, self::getSitemapFileName('index'));
    }

    /**
     * Запись в файл
     *
     * @param $arResult
     * @param $sFileName
     * @return int
     */
    private static function writeSitemapFile(&$arResult, $sFileName) {

        $iResult = $iLen = $iMaxLastMod = 0;
        $sResultXml = '';

        if (empty($arResult)) {
            return $iResult;
        }

        $bIndex    = false !== stripos($sFileName, 'index');
        $sRootTag  = $bIndex ? 'sitemapindex' : 'urlset';
        $sItemTag  = $bIndex ? 'sitemap'      : 'url';

        $iMaxSize   = 10485760 - 100000; // Несжатый размер файла не должен превышать 10Мб + вычет запаса
        $iMaxLength = 2048;              // Максимальная длина URL в элементе <loc>

        foreach ($arResult as $iItem => $arItem) {
            if ($iMaxSize >= $iLen && $iMaxLength >= iconv_strlen($arItem['loc'], 'UTF-8')) {

                $sRes = "<$sItemTag>"
                    . '<loc>' . URL_SCHEME . '://' . SITE_HOST . $arItem['loc'] . '</loc>'
                    . '<lastmod>' . date(DATE_ATOM, $arItem['lastmod']) . '</lastmod>'
                    . ($bIndex ? '' : '<priority>' . $arItem['priority'] . '</priority>')
                    . "</$sItemTag>\n";

                $sResultXml .= $sRes;
                $iLen += strlen($sRes);
                $iResult = $arItem['id'];

                if ($iMaxLastMod <= $arItem['lastmod'])
                    $iMaxLastMod = $arItem['lastmod'];
            }
            unset($arResult[$iItem]);
        }
        unset($arResult);

        $sResultXml = '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>'
            . "<$sRootTag xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n" . $sResultXml . "</$sRootTag>";

        self::writeFile($sFileName, $sResultXml, $iMaxLastMod);

        return $iResult;
    }

    /**
     * Запись файла на диск
     *
     * @param $sFileName
     * @param $sData
     * @param int $iUnixTime
     */
    private static function writeFile($sFileName, &$sData, $iUnixTime = 0) {

        if (empty($sData)) {
            return;
        }

        if (false !== stripos($sFileName, '.gz')) {
            if (self::FILE_2_GZIP && function_exists('gzencode')) {
                $sData = gzencode($sData, 9);
            } else {
                $sFileName = str_replace('.gz', '', $sFileName);
            }
        }

        $sFilePath = $_SERVER['DOCUMENT_ROOT'] . self::FILE_LOCATION_TMP . $sFileName;

        file_put_contents($sFilePath, $sData);
        if ($iUnixTime) {
            touch ($sFilePath, $iUnixTime);
        }
    }

    /**
     * Перемещение XML-файлов на постоянное место хранения
     */
    private static function move2PermanentDir() {

        $sDirTmp = $_SERVER['DOCUMENT_ROOT'] . self::FILE_LOCATION_TMP;
        $sDir    = $_SERVER['DOCUMENT_ROOT'] . self::FILE_LOCATION;

        $arFiles = self::getXmlFiles($sDirTmp);
        if (!empty($arFiles)) {
            Service::removeDirectory($sDir);
            rename($sDirTmp, $sDir);
        }
    }

    /******************************************************************************************************************
     ************************************                СУЩНОСТИ              ****************************************
     ******************************************************************************************************************/

    /**
     * Получение каталога карточек изданий
     *
     * @param int $iLastId
     * @param bool $bCount
     * @param float $fPriority
     * @return array
     */
    private static function getCards($iLastId, $bCount, $fPriority) {
        return self::getCatalog('tbl_common_biblio_card', $iLastId, $bCount, $fPriority);
    }

    /**
     * Получение каталога разметки изданий
     *
     * @param int $iLastId
     * @param bool $bCount
     * @param float $fPriority
     * @return array
     */
    private static function getParts($iLastId, $bCount, $fPriority) {
        return self::getCatalog('tbl_common_biblio_card_parts', $iLastId, $bCount, $fPriority);
    }

    /**
     * Получение информации по карточкам/разметке каталога
     *
     * @param $sTable
     * @param $iLastId
     * @param $bCount
     * @param $fPriority
     * @return array|int
     */
    private static function getCatalog($sTable, $iLastId, $bCount, $fPriority) {

        $arResult = [];
        $sSelect = $sWhere = '';

        // получение соединения с БД "Библиотека" и установка кодировки
        $connection  = DbHelper::getLibraryConnection($bCount, $bCount);

        if (false !== stripos($sTable, 'parts')) {
            $sSelect = ', 1 as pdfLink, ArticleAccess AS AccessType ';
            $sWhere  = ' AND StatusModeration = "confirmed" AND ArticleAccess = 0 ';
        } else {
            $sSelect = ', pdfLink IS NOT NULL as pdfLink, AccessType';
            $sWhere  = self::DB_ONLY_DIGITIZED ? ' AND pdfLink IS NOT NULL ' : '';
            $sWhere .= self::DB_ONLY_UNIQUE    ? ' AND (IdParent IS NULL OR IdParent = 0 OR IdParent = Id)' : '';
        }

        // получение полного количества
        if ($bCount) {
            $sCatalogSQL = "SELECT COUNT(*) FROM $sTable WHERE 1=1 $sWhere LIMIT 1;";
            $arFields = $connection->query($sCatalogSQL)->fetchRaw();
            $arResult = intval($arFields['COUNT(*)']);
        }
        // оптимизированное по потреблению памяти получение изданий
        else {
            $iQuery = 2;
            $iLimit = self::FILE_ITEMS_LIMIT / $iQuery;
            $iDate = (new DateTime('01.01.' . date('Y') . ' 00:00:00'))->getTimestamp();
            for ($i = 0; $i < $iQuery; $i++) {

                $sCatalogSQL = <<<SQL
SELECT
  Id,
  FullSymbolicId,
  UNIX_TIMESTAMP(CreationDateTime) as CreationDateTime,
  UNIX_TIMESTAMP(UpdatingDateTime) as UpdatingDateTime
  $sSelect
FROM
  $sTable
WHERE
  Id > $iLastId
$sWhere
ORDER BY
  Id ASC
LIMIT $iLimit
;
SQL;
                $rsBooks = $connection->query($sCatalogSQL);
                while ($arFields = $rsBooks->fetch()) {
                    $iLastId = $arFields['Id'];
                    $arResult[] = [
                        'id'       => $arFields['Id'],
                        'loc'      => '/catalog/' . urlencode($arFields['FullSymbolicId']) . '/',
                        'lastmod'  => $arFields['UpdatingDateTime'] ? : ($arFields['CreationDateTime'] ? : $iDate),
                        'priority' => $arFields['pdfLink']
                                        ? (!$arFields['AccessType'] ? $fPriority : $fPriority - 0.1) : $fPriority - 0.2
                    ];
                }
                unset($sCatalogSQL, $rsBooks, $arFields);
            }
        }

        return $arResult;
    }

    /**
     * Получение массива коллекций / количества коллекций
     *
     * @param int $iId
     * @param bool $bCount
     * @param float $fPriority
     * @return array|int
     */
    private static function getCollections($iId, $bCount, $fPriority) {

        $arResult = [];

        if (!Loader::includeModule('iblock')) {
            return $arResult;
        }

        $rsCollections = \CIBlockSection::GetList(
            ['ID' => 'ASC'],
            ['IBLOCK_ID' => IBLOCK_ID_COLLECTION, '>ID' => $iId, 'ACTIVE' => 'Y'],
            false,
            ['ID', 'SECTION_PAGE_URL', 'TIMESTAMP_X'],
            ['nTopCount' => $bCount ? 0 : self::FILE_ITEMS_LIMIT]
        );
        // получение полного количества
        if ($bCount) {
            $arResult = intval($rsCollections->SelectedRowsCount());
        }
        // получение коллекций
        else {
            while ($arFields = $rsCollections->GetNext(true, false)) {
                $arResult[] = [
                    'id'       => $arFields['ID'],
                    'loc'      => $arFields['SECTION_PAGE_URL'],
                    'lastmod'  => (new DateTime($arFields['TIMESTAMP_X']))->getTimestamp(),
                    'priority' => $fPriority
                ];
            }
        }

        return $arResult;
    }

    /**
     * Получение массива новости / количества новостей
     *
     * @param int $iId
     * @param bool $bCount
     * @param float $fPriority
     * @return array
     */
    private static function getNews($iId, $bCount, $fPriority) {

        $arResult = [];

        if (!Loader::includeModule('iblock')) {
            return $arResult;
        }

        $rsNews = \CIBlockElement::GetList(
            ['ID' => 'ASC'],
            ['IBLOCK_ID' => IBLOCK_ID_NEWS, '>ID' => $iId, 'ACTIVE' => 'Y'],
            false,
            ['nTopCount' => $bCount ? 0 : self::FILE_ITEMS_LIMIT],
            ['ID', 'IBLOCK_ID', 'DETAIL_PAGE_URL', 'TIMESTAMP_X']
        );
        // получение полного количества
        if ($bCount) {
            $arResult = intval($rsNews->SelectedRowsCount());
        }
        // получение новостей
        else {
            while ($arFields = $rsNews->GetNext(true, false)) {
                $arResult[] = [
                    'id'       => $arFields['ID'],
                    'loc'      => $arFields['DETAIL_PAGE_URL'],
                    'lastmod'  => (new DateTime($arFields['TIMESTAMP_X']))->getTimestamp(),
                    'priority' => $fPriority
                ];
            }
        }

        return $arResult;
    }

    /**
     * Получение массива библиотек / количества библиотек
     *
     * @param int $iId
     * @param bool $bCount
     * @param float $fPriority
     * @return array
     */
    private static function getLibraries($iId, $bCount, $fPriority) {

        $arResult = [];

        if (!Loader::includeModule('iblock')) {
            return $arResult;
        }

        $rsNews = \CIBlockElement::GetList(
            ['ID' => 'ASC'],
            ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, '>ID' => $iId, 'ACTIVE' => 'Y'],
            false,
            ['nTopCount' => $bCount ? 0 : self::FILE_ITEMS_LIMIT],
            ['ID', 'IBLOCK_ID', 'DETAIL_PAGE_URL', 'TIMESTAMP_X']
        );
        // получение полного количества
        if ($bCount) {
            $arResult = intval($rsNews->SelectedRowsCount());
        }
        // получение новостей
        else {
            while ($arFields = $rsNews->GetNext(true, false)) {
                $arResult[] = [
                    'id'       => $arFields['ID'],
                    'loc'      => $arFields['DETAIL_PAGE_URL'],
                    'lastmod'  => (new DateTime($arFields['TIMESTAMP_X']))->getTimestamp(),
                    'priority' => $fPriority
                ];
            }
        }

        return $arResult;
    }
}