<?
#AddEventHandler("iblock", "OnIBlockPropertyBuildList", array('CIBlockPropertyElementMPhoto','GetUserTypeDescription'));
namespace Bitrix\NotaExt\Ext\detailGallery\Prop;

class PropertyDetailGallery
{
    function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => "E",
            "USER_TYPE" => "ElementDG",
            "DESCRIPTION" => '[detail_gallery] Фотогалерея для детальной страницы',
            "GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
            "GetSettingsHTML" => array(__CLASS__, 'GetSettingsHTML'),
            "PrepareSettings" => array(__CLASS__, 'PrepareSettings'),
        );
    }

    function GetPropertyFieldHtml($arProperty, $arValue, $strHTMLControlName)
    {
        $id = intval($_REQUEST['ID']);

        if ($arProperty['LINK_IBLOCK_ID'] <= 0)
            return 'Не задан инфоблок для хранения фото';

        if ($id <= 0) {
            return 'Сохраните материал прежде чем добавить для него фото';
        } else {

            ob_start();

            $GLOBALS['APPLICATION']->IncludeComponent(
                "notaext:admin.detail.gallery",
                "",
                Array(
                    "IBLOCK_ID" => $arProperty['LINK_IBLOCK_ID'],
                    "ID" => $id,
                    "PROPERTY_ID" => $arProperty['ID'],
                    "FILE_EXTENSION" => $arProperty['USER_TYPE_SETTINGS']['FILE_EXTENSION'],
                    "WIDTH" => $arProperty['USER_TYPE_SETTINGS']['WIDTH'],
                    "HEIGHT" => $arProperty['USER_TYPE_SETTINGS']['HEIGHT'],
                    "MAX_WIDTH_ORIGINAL" => $arProperty['USER_TYPE_SETTINGS']['MAX_WIDTH_ORIGINAL'],
                    "MAX_HEIGHT_ORIGINAL" => $arProperty['USER_TYPE_SETTINGS']['MAX_HEIGHT_ORIGINAL'],
                    "SHOW_ALIGN" => $arProperty['USER_TYPE_SETTINGS']['SHOW_ALIGN'],
                    "SHOW_WIDTH" => $arProperty['USER_TYPE_SETTINGS']['SHOW_WIDTH'],
                    "SHOW_HEIGHT" => $arProperty['USER_TYPE_SETTINGS']['SHOW_HEIGHT'],
                    "HTMLControlName" => $strHTMLControlName["VALUE"],
                    "HTMLControlNameDesc" => $strHTMLControlName["DESCRIPTION"],
                    "HTMLControlValue" => $arValue['VALUE'],
                    "HTMLControlDesc" => $arValue['DESCRIPTION'],
                ),
                false,
                array('HIDE_ICONS' => 'Y')
            );
            $strComponentHtml = @ob_get_contents();
            ob_get_clean();

            return $strComponentHtml;
        }
    }

    public function PrepareSettings($arFields)
    {
        $intWidth = intval(isset($arFields['USER_TYPE_SETTINGS']['WIDTH']) ? $arFields['USER_TYPE_SETTINGS']['WIDTH'] : 0);
        if ($intWidth <= 0) $intWidth = 500;

        $intHeight = intval(isset($arFields['USER_TYPE_SETTINGS']['HEIGHT']) ? $arFields['USER_TYPE_SETTINGS']['HEIGHT'] : 0);
        if ($intHeight <= 0) $intHeight = 800;

        $intMaxWidthOriginal = intval(isset($arFields['USER_TYPE_SETTINGS']['MAX_WIDTH_ORIGINAL']) ? $arFields['USER_TYPE_SETTINGS']['MAX_WIDTH_ORIGINAL'] : 0);
        if ($intMaxWidthOriginal <= 0) $intMaxWidthOriginal = 1600;

        $intMaxHeightOriginal = intval(isset($arFields['USER_TYPE_SETTINGS']['MAX_HEIGHT_ORIGINAL']) ? $arFields['USER_TYPE_SETTINGS']['MAX_HEIGHT_ORIGINAL'] : 0);
        if ($intMaxHeightOriginal <= 0) $intMaxHeightOriginal = 1600;

        $strFileExtension = !empty($arFields['USER_TYPE_SETTINGS']['FILE_EXTENSION']) ? $arFields['USER_TYPE_SETTINGS']['FILE_EXTENSION'] : 'jpg,jpeg,png';
        $strFileExtension = trim($strFileExtension);

        $strShowAlign = $arFields['USER_TYPE_SETTINGS']['SHOW_ALIGN'] == 'Y' ? 'Y' : 'N';
        $strShowWidth = $arFields['USER_TYPE_SETTINGS']['SHOW_WIDTH'] == 'Y' ? 'Y' : 'N';
        $strShowHeight = $arFields['USER_TYPE_SETTINGS']['SHOW_HEIGHT'] == 'Y' ? 'Y' : 'N';


        return array(
            'WIDTH' => $intWidth,
            'HEIGHT' => $intHeight,
            'MAX_WIDTH_ORIGINAL' => $intMaxWidthOriginal,
            'MAX_HEIGHT_ORIGINAL' => $intMaxHeightOriginal,
            'FILE_EXTENSION' => $strFileExtension,
            'SHOW_ALIGN' => $strShowAlign,
            'SHOW_WIDTH' => $strShowWidth,
            'SHOW_HEIGHT' => $strShowHeight,
        );
    }

    public function GetSettingsHTML($arFields, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array("MULTIPLE_CNT", "MULTIPLE", "SEARCHABLE", "FILTRABLE", "SMART_FILTER"),
        );

        $arSettings = self::PrepareSettings($arFields);

        return '
			<tr><td>Расширения загружаемых файлов</td><td><input type="text" name="' . $strHTMLControlName["NAME"] . '[FILE_EXTENSION]" value="' . $arSettings['FILE_EXTENSION'] . '"></td></tr>
			<tr><td>Ширина слайда по умолчанию</td><td><input type="text" name="' . $strHTMLControlName["NAME"] . '[WIDTH]" value="' . intval($arSettings['WIDTH']) . '"></td></tr>
			<tr><td>Высота слайда по умолчанию</td><td><input type="text" name="' . $strHTMLControlName["NAME"] . '[HEIGHT]" value="' . intval($arSettings['HEIGHT']) . '"></td></tr>
			<tr><td>Максимальная ширина оригинала</td><td><input type="text" name="' . $strHTMLControlName["NAME"] . '[MAX_WIDTH_ORIGINAL]" value="' . intval($arSettings['MAX_WIDTH_ORIGINAL']) . '"></td></tr>
			<tr><td>Максимальная высота оригинала</td><td><input type="text" name="' . $strHTMLControlName["NAME"] . '[MAX_HEIGHT_ORIGINAL]" value="' . intval($arSettings['MAX_HEIGHT_ORIGINAL']) . '"></td></tr>
			<tr><td>Показывать диалог выбора выравнивания</td><td><input type="checkbox" name="' . $strHTMLControlName["NAME"] . '[SHOW_ALIGN]" value="Y" ' . ($arSettings['SHOW_ALIGN'] == 'Y' ? 'checked' : '') . '></td></tr>
			<tr><td>Показывать диалог выбора ширины</td><td><input type="checkbox" name="' . $strHTMLControlName["NAME"] . '[SHOW_WIDTH]" value="Y" ' . ($arSettings['SHOW_WIDTH'] == 'Y' ? 'checked' : '') . '></td></tr>
			<tr><td>Показывать диалог выбора высоты</td><td><input type="checkbox" name="' . $strHTMLControlName["NAME"] . '[SHOW_HEIGHT]" value="Y" ' . ($arSettings['SHOW_HEIGHT'] == 'Y' ? 'checked' : '') . '></td></tr>
			';
    }
}