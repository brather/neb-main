<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>
<div class="b-headernav_markup add right">
    <div class="b_markup_markup add popup clearfix" data-class="clone-login-popup">
        <div class="b_markup_markup_in bbox clearfix">
            <h4>Добавить произведение</h4>

                <form action="/local/tools/viewer/markup_add.php" method="post" class="b-formchapter nrf" data-add-change-form target="_top">

                    <input type="hidden" name="book_id" id="book_id" value="<?=$arResult['BOOK']['id']?>" />
                    <input type="hidden" name="action" id="action" value="add" />

                    <div class="nrf-fieldset-title"></div>

                    <div class="form-group">
                        <label for="author">Автор / Авторы</label>
                        <input type="text" id="author" name="author" class="form-control" value="" />
                    </div>

                    <div class="form-group">
                        <label for="title">
                            <em class="hint">*</em>
                            Название
                        </label>
                        <input type="text" id="title" name="title" class="form-control" value="" autocomplete="off" />
                    </div>

                    <div class="form-group">
                        <label for="title">
                            <em class="hint">*</em>
                            Доступ
                        </label>
                        <select name="articleAccess" id="articleAccess" class="form-control">
                            <option value="0">Открытое</option>
                            <option value="1">Издание, охраняемое авторским правом</option>
                            <? //<option value="2">Заблокированное</option> #18787 ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="position">Позиция на странице</label>
                        <select name="position" id="position" class="form-control">
                            <option value="1">вверху</option>
                            <option value="0">внизу</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="startPage">
                            <em class="hint">*</em>
                            Начальный номер электронного образа
                        </label>
                        <input type="text" id="startPage" name="startPage" class="form-control" value="" autocomplete="off" />
                    </div>

                    <div class="form-group">
                        <label for="finishPage">
                            <em class="hint">*</em>
                            Конечный номер электронного образа
                        </label>
                        <input type="text" id="finishPage" name="finishPage" class="form-control" value="" autocomplete="off" />
                    </div>

                    <div class="form-group">
                        <label for="rangePage">Диапазон страниц в издании</label>
                        <input type="text" id="rangePage" name="rangePage" class="form-control" value="" />
                    </div>
                    
                    <em class="error_message">
                        <? if (!empty($arResult['ERROR_MESSAGE']['MESSAGE'])
                            || (is_string($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']))
                        ):
                            if (is_array($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']['MESSAGE']))
                                $arResult['ERROR_MESSAGE'] = $arResult['ERROR_MESSAGE']['MESSAGE'];
                            ?>
                            <?= $arResult['ERROR_MESSAGE']; ?>
                            <script type="text/javascript">
                                $(function () {
                                    $('#login-link').click();
                                    $('.b_markup_markup .error_message').show();
                                });
                            </script>
                        <? endif; ?>
                    </em>

                    <p><span class="red">*</span> - поля, обязательные для заполнения</p>

                    <div class="button-bar">
                        <button class="btn btn-primary pull-right" value="1" type="submit">Сохранить</button>
                    </div>
                </form>
            <div class="close-window clear-form"></div>
        </div>
    </div>
</div>

<div class="b-headernav_markup delete right">
    <div class="b_markup_markup delete popup clearfix" align="center" data-class="clone-login-popup">
        <div class="b_markup_markup_in bbox clearfix">
            <h4>Изменение статуса</h4>
            <div class="b-markupform rel b-loginform-column">

                <form action="/local/tools/viewer/markup_status.php" method="post" class="b-form" target="_top">

                    <input type="hidden" name="book_id" id="book_status_id" value="" />
                    <input type="hidden" name="status" id="book_status_code" value="" />

                    <p>Вы действительно хотите <span id="book_status_name"></span>?</p> <p id="book_status_title"></p>

                    <em class="error_message">
                        <? if (!empty($arResult['ERROR_MESSAGE']['MESSAGE'])
                            || (is_string($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']))
                        ):
                            if (is_array($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']['MESSAGE']))
                                $arResult['ERROR_MESSAGE'] = $arResult['ERROR_MESSAGE']['MESSAGE'];
                            ?>
                            <?= $arResult['ERROR_MESSAGE']; ?>
                            <script type="text/javascript">
                                $(function () {
                                    $('#login-link').click();
                                    $('.b_markup_markup .error_message').show();
                                });
                            </script>
                        <? endif; ?>
                    </em>

                    <div class="fieldrow nowrap fieldrowaction">
                        <div class="field clearfix">
                            <button class="formbutton left" value="1" type="submit">Да</button>
                            <div class="formbutton right close-win" value="0" type="submit">Нет</div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="close-window clear-form"></div>
        </div>
    </div>
</div>