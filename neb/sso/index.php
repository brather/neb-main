<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;
$APPLICATION->IncludeComponent(
    "neb:sso",
    ".default",
    array(
        "HOURS_LIMIT" => "24",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/sso/",
        "SEF_URL_TEMPLATES" => array(
            "list" => "/",
            "auth" => "auth/",
            "check_token" => "check_token/",
        )
    ),
    false
);