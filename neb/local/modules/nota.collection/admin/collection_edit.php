<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!CModule::IncludeModule('nota.collection')) {
    echo CAdminMessage::ShowMessage(Loc::getMessage('NOTA_COLLECTION_ACCESS_DENIED'));
    return false;
}

use Nota\Collection\CollectionTable;

$aTabs = array();
$aTabs[] = array(
    "DIV" => "edit1",
    'TAB' => Loc::getMessage('NOTA_COLLECTION_TAB'),
    'TITLE' => Loc::getMessage('NOTA_COLLECTION_TAB')

);

$tabControl = new CAdminTabControl('tabControl', $aTabs);

$arErrors = array();

if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && check_bitrix_sessid()) {
    $arFields = Array();
    $arFields["NAME"] = htmlspecialcharsEx($NAME);
    $arFields["LIBRARY_ID"] = $LIBRARY_ID;
    $arFields["MAIN"] = $MAIN;
    $arFields["SORT"] = intval($SORT);

    if (!empty($arFields["NAME"])) {
        if (!empty($_FILES['BACKGROUND']['tmp_name']) or ${"BACKGROUND_del"} == 'Y') {
            $arBACKGROUND = $_FILES['BACKGROUND'];
            $arBACKGROUND["del"] = ${"BACKGROUND_del"};
            $arBACKGROUND["old_file"] = ${"BACKGROUND_old_file"};
            $arBACKGROUND["MODULE_ID"] = "nota.collection";

            $arFields['BACKGROUND'] = (int)CFile::SaveFile($arBACKGROUND, 'main_slider_bg');
        }
    }

    if (strlen($ID) > 0) {
        $res = CollectionTable::update($ID, $arFields);
    } else {
        $res = CollectionTable::add($arFields);
        $ID = $res->getId();
    }

    if (!$res->isSuccess())
        $errors = $res->getErrorMessages();
    else {
        if (strlen($apply) <= 0)
            LocalRedirect('collection.php');
        else
            LocalRedirect($APPLICATION->GetCurPage() . "?lang=" . $lang . "&ID=" . UrlEncode($ID) . "&" . $tabControl->ActiveTabParam());
    }

}

if (strlen($ID) > 0)
    $APPLICATION->SetTitle(GetMessage("NOTA_COLLECTION_EDIT", array('#ID#' => $ID)));
else
    $APPLICATION->SetTitle(GetMessage("NOTA_COLLECTION_ADD"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

ClearVars("str_");

if (intval($ID) > 0) {
    $result = CollectionTable::getById($ID);
    if ($arRow = $result->fetch())
        extract($arRow, EXTR_PREFIX_ALL, 'str');
} else {
    $ID = '';
    $str_SORT = 100;
}


$aMenu = array(
    array(
        'TEXT' => Loc::getMessage('NOTA_COLLECTION_LIST'),
        'TITLE' => Loc::getMessage('NOTA_COLLECTION_LIST'),
        'LINK' => 'collection.php' . '?lang=' . LANGUAGE_ID,
        'ICON' => 'btn_list',
    )
);

if (strlen($ID) > 0) {
    $aMenu[] = array("SEPARATOR" => "Y");
    $aMenu[] = array(
        "TEXT" => GetMessage("NOTA_COLLECTION_NEW"),
        "TITLE" => GetMessage("NOTA_COLLECTION_NEW"),
        "LINK" => "collection_edit.php?lang=" . LANGUAGE_ID,
        "ICON" => "btn_new"
    );
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

if (!empty($errors))
    CAdminMessage::ShowMessage(implode("\n", $errors));

$map = CollectionTable::getMap();

?>
<form method="POST" id="form" name="form" action="<?= $APPLICATION->GetCurUri() ?>" enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="ID" value="<?= $ID ?>">
    <?
    $tabControl->Begin();
    $tabControl->BeginNextTab();

    if (strlen($ID) > 0) {
        ?>
        <tr>
            <td><?= $map['ID']['title'] ?>:</td>
            <td><?= $str_ID ?></td>
        </tr>
        <?
    }
    ?>
    <tr>
        <td><?= $map['LIBRARY_ID']['title'] ?>:</td>
        <?
        $arLib = array();
        $rsLibrary = Nota\Collection\LibraryTable::getList(array(
            'select' => array('NAME', 'ID'),
            'filter' => array(),
            'order' => array()
        ));

        while ($arLibrary = $rsLibrary->Fetch()) {
            $arLib['REFERENCE'][] = $arLibrary['NAME'];
            $arLib['REFERENCE_ID'][] = $arLibrary['ID'];
        }
        ?>
        <td><?= SelectBoxFromArray("LIBRARY_ID", $arLib, $str_LIBRARY_ID); ?></td>
    </tr>
    <tr>
        <td><?= $map['NAME']['title'] ?>:</td>
        <td><input type="text" name="NAME" size="50" maxlength="250" value="<?= htmlspecialcharsbx($str_NAME) ?>"></td>
    </tr>
    <tr>
        <td><?= $map['MAIN']['title'] ?>:</td>
        <td><?= InputType("checkbox", "MAIN", "Y", $str_MAIN); ?></td>
    </tr>
    <tr>
        <td><?= $map['BACKGROUND']['title'] ?>:</td>
        <td><?
            echo CFileInput::Show(
                "BACKGROUND",
                isset($str_BACKGROUND) ? $str_BACKGROUND : 0,
                array(
                    "IMAGE" => "Y",
                    "PATH" => "Y",
                    "FILE_SIZE" => "Y",
                    "DIMENSIONS" => "Y",
                    "IMAGE_POPUP" => "Y",
                    "MAX_SIZE" => array(
                        "W" => 500,
                        "H" => 500
                    )
                ),
                array(
                    'upload' => true,
                    'medialib' => false,
                    'file_dialog' => false,
                    'cloud' => false,
                    'del' => true,
                    'description' => false
                )
            );
            ?>
            <?
            if (intval($str_BACKGROUND) > 0) {
                ?>
                <input type="hidden" name="BACKGROUND_old_file" value="<?= $str_BACKGROUND ?>"/>
                <?
            }
            ?>
        </td>
    </tr>
    <tr>
        <td><?= $map['SORT']['title'] ?>:</td>
        <td><input type="text" name="SORT" size="5" maxlength="5" value="<?= intval($str_SORT) ?>"></td>
    </tr>
    <?
    $tabControl->Buttons(array("disabled" => false));
    $tabControl->End();
    ?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>



