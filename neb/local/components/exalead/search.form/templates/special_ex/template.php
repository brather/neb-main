<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>

<section class="innersection innerwrapper searchempty clearfix">
    <div class="b-mainblock left">

        <div class="b-plaintext">
            <h2>Расширенный поиск</h2>

            <form action="<?=!empty($arParams['ACTION_URL'])? $arParams['ACTION_URL'] : '/special/search/'?>" class="b-form b-form_common">
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <input type="text" name="q" class="b-search_fieldtb b-text" id="" value="">
                    </div>
                </div>
                <div class="checkwrapper mt10">
                    <input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" name="s_tp" value="Y" class="black">Искать только в полнотекстовых изданиях</label>
                </div>

                <div class="fieldrow nowrap mt10">
                    <div class="fieldcell iblock">
                        <label for="settings02">Автор</label>
                        <div class="field validate">
                            <input type="hidden" name="theme[0]" value="authorbook"/>
                            <input type="hidden" name="logic[0]" value="OR"/>
                            <input type="text" value="" id="settings02" data-minlength="2" name="text[0]" class="input"  autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <label for="settings03">Название</label>
                        <div class="field validate">
                            <input type="hidden" name="theme[1]" value="title"/>
                            <input type="hidden" name="logic[1]" value="OR"/>
                            <input type="text" value="" id="settings03" data-minlength="2" name="text[1]" class="input"  autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <label for="settings04">Издательство</label>
                        <div class="field validate">
                            <input type="hidden" name="theme[2]" value="publisher"/>
                            <input type="hidden" name="logic[2]" value="OR"/>
                            <input type="text" value="" id="settings04" data-minlength="2" name="text[2]" class="input"  autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <label for="settings05">Место издательства</label>
                        <div class="field validate">
                            <input type="hidden" name="theme[3]" value="publishplace"/>
                            <input type="hidden" name="logic[3]" value="OR"/>
                            <input type="text" value="" id="settings05" data-minlength="2" name="text[3]" class="input"  autocomplete="off">
                        </div>
                    </div>
                </div>
                <!--<div class="fieldrow nowrap ptop">
                    <div class="fieldcell iblock">
                        <label for="fb_theme">Доступ</label>
                        <div class="field">
                            <select name="theme" id="fb_theme" class="js_select w370">
                                <option value="1">Все</option>
                                <option value="2">Открытый доступ</option>
                                <option value="3">Закрытый доступ</option>
                            </select>
                        </div>
                    </div>
                </div>-->
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <label for="settings06">Год публикации от</label>
                        <div class="field validate">
                            <input type="text" value="1700" id="settings06" data-minlength="2" name="publishyear_prev" class="input" >
                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <label for="settings07">Год публикации до</label>
                        <div class="field validate">
                            <input type="text" value="2014" id="settings07" data-minlength="2" name="publishyear_next" class="input">
                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap fieldrowaction">
                    <div class="fieldcell ">
                        <div class="field clearfix">
                            <input type="submit" class="b-search_bth bbox" value="Найти">
                        </div>
                    </div>
                </div>
            </form>

        </div><!-- /.b-plaintext -->
    </div><!-- /.b-mainblock -->
</section>