<?php
CBitrixComponent::includeComponentClass("rest:rest.server");

CModule::IncludeModule('nota.userdata');
CModule::IncludeModule('nota.exalead');
CModule::IncludeModule("highloadblock");

use \Bitrix\Main\Type\Date;
use \Bitrix\Main\Entity\ExpressionField;
use \Nota\Exalead\BiblioCardTable;

/**
 * Компонент организует API для получения данных о прочтении книг
 * https://docs.google.com/document/d/1Nxj__eio7bvaMpDRfaEAzeJ1o-9JI4mu5VnzTESRVBo/edit#
 * Пункт 8.5
 *
 * Class RestBookViewBook
 */
class RestBookViewBook extends RestServer
{
	protected $token, $from, $to, $id;

	public function processRequest($token = null, $from = null, $to = null, $id = null)
	{
        $this->token = !empty($token) ? trim($token) : false;
        $this->from = !empty($from) ? trim($from) : false;
        $this->to = !empty($to) ? trim($to) : false;
        $this->id = !empty($id) ? trim($id) : false;

        if(!$this->token || strlen($this->token) <= 0)
            return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));
		
		$tokenUser = CUser::GetList(
			($by = "id"),
			($order = "desc"),
			array(
				'ACTIVE'   => 'Y',
				'UF_TOKEN' => $this->token,
			),
			array('FIELDS' => array('ID'))
		)->Fetch();

        if ( !$tokenUser || (int)$tokenUser['ID'] <= 0 )
            return $this->restError(404, GetMessage('USER_NOT_FOUND'));

        return $this->GETLibView();
	}

	protected function GETLibView()
	{
		$arResult = [];

        $result = BiblioCardTable::getList(Array(
            'select' => Array('BOOK_NAME' => 'Name', 'BOOK_DATE' => 'BX_TIMESTAMP', 'LIB_NAME' => 'LIB.Name', 'COUNT'),
            'filter' => Array(
                'IdFromALIS'         => $this->id,
                '>=READ.X_TIMESTAMP' => new Date($this->from),
                '<=READ.X_TIMESTAMP' => new Date($this->to),
            ),
            'runtime' => Array(
				new ExpressionField('COUNT', 'COUNT(DISTINCT %s)', Array('READ.ID')),
            )
        ));
        while($res = $result->Fetch()){
            $libName = $res['LIB_NAME'];
            unset($res['LIB_NAME']);
            $res['BOOK_DATE'] = $res['BOOK_DATE']->format('d.m.Y');
			$arResult[$libName][] = $res;
        }

        $this->arResult = $arResult;

        return 200;
	}
}