<?php
$MESS['COLLECTION_PAGE_TITLE'] = 'Collections';
$MESS['COLLECTION_PAGE_DESCRIPTION'] = 'This section presents the collections of electronic copies of publications, compiled by experts of libraries participating in the formation of a common fund of the National Digital Library';