<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($arParams["IBLOCK_ID"] < 1) {
    ShowError("IBLOCK_ID IS NOT DEFINED");
    return false;
}

if (!CModule::IncludeModule("iblock")) {
    ShowError("IBLOCK_MODULE_NOT_INSTALLED");
    return false;
}

if (check_bitrix_sessid()) {
    $newID = $_REQUEST['ID'];
    $ibObject = new CIBlockElement();
    $code = $_REQUEST['CODE'] ? $_REQUEST['CODE'] : CUtil::translit($_REQUEST['NAME'], LANGUAGE_ID);

    $arFields = array(
        'IBLOCK_ID' => $arParams["IBLOCK_ID"],
        'NAME' => htmlspecialchars(trim($_REQUEST['NAME'])),
        'CODE' => $code,
        'PREVIEW_TEXT' => htmlspecialchars($_REQUEST['PREVIEW_TEXT']),
        'DETAIL_TEXT' => $_REQUEST['DETAIL_TEXT'],
        'DETAIL_TEXT_TYPE' => 'html',
        'ACTIVE' => $_REQUEST['ACTIVE'] == 'Y' ? 'Y' : 'N',
        'DATE_ACTIVE_FROM' => $_REQUEST['DATE_ACTIVE_FROM'],
        'PROPERTY_VALUES' => array(
            'YOUTUBE' => trim($_REQUEST['YOUTUBE']),
            'LIBRARY' => $_REQUEST['LIBRARY_ID']
        )
    );

    // Если пришла новая - обновляем
    if (!empty($_REQUEST['DETAIL_PICTURE']))
        $arFields['DETAIL_PICTURE'] = CFile::MakeFileArray($_REQUEST['DETAIL_PICTURE']);


    // Обрабатываем галерею.
    $propGallery = array();
    if (!empty ($_REQUEST['GALLERY_old'])) {
        foreach ($_REQUEST['GALLERY_old'] as $photoID => $photoInfo) {
            if (!empty($photoID)) {
                $propGallery[$photoID] = array(
                    'VALUE' => $photoInfo['VALUE'],
                    'old_file' => $photoInfo['VALUE'],
                    'DESCRIPTION' => $photoInfo['DESCRIPTION'],
                    //'del' 			=> $photoInfo['del'] == 'Y' ? 'Y' : false
                );

                if ($photoInfo['del'] == 'Y')
                    $propGallery[$photoID]['del'] = 'Y';
            }
        }
    }
    if (!empty($_REQUEST['GALLERY'])) {
        foreach ($_REQUEST['GALLERY'] as $key => $newGalItem) {
            if (!empty($newGalItem['VALUE'])) {
                if ($newGalItem['del'] !== 'Y')
                    $propGallery[] = array(
                        'VALUE' => CFile::MakeFileArray($newGalItem['VALUE']),
                        'DESCRIPTION' => $newGalItem['DESCRIPTION'],
                    );
            }
        }
    }
    $arFields['PROPERTY_VALUES']['GALLERY'] = $propGallery;

    // Файл
    if ($_REQUEST['FILE_del'] == 'Y') {
        if (isset($_REQUEST['FILE_VALUE_ID']))
            $arFields['PROPERTY_VALUES']['FILE'][$_REQUEST['FILE_VALUE_ID']] = array(
                'VALUE' => $_REQUEST['FILE_ID_old'],
                'old_file' => $_REQUEST['FILE_ID_old'],
                'DESCRIPTION' => '',
                'del' => 'Y'
            );
        else
            $arFields['PROPERTY_VALUES']['FILE'] = array();
    } elseif (!empty($_REQUEST['FILE'])) {
        $arFields['PROPERTY_VALUES']['FILE'] = array(
            'VALUE' => CFile::MakeFileArray($_REQUEST['FILE']),
            'DESCRIPTION' => $_REQUEST['FILE_DESCRIPTION']
        );
    } elseif (!empty($_REQUEST['FILE_ID_old'])) {
        $arFields['PROPERTY_VALUES']['FILE'] = array(
            'VALUE' => CFile::GetFileArray($_REQUEST['FILE_ID_old']),
            'DESCRIPTION' => $_REQUEST['FILE_DESCRIPTION']
        );
    }

    if ((bool)$newID) {
        $res = $ibObject->Update($newID, $arFields);
        //issue #13703{
        if ($res)
            LocalRedirect("/profile/news/");
        else {
            ShowError($ibObject->LAST_ERROR);
        }
        //}issue #13703
    } else {
        $res = $ibObject->Add($arFields);
        if ($res)
            //LocalRedirect("/profile/news/edit/{$res}/"); //issue #13703
            LocalRedirect("/profile/news/");
        else {
            ShowError($ibObject->LAST_ERROR);
        }
    }
}

if (!empty($arParams['NEWS_ID'])) {
    $arFilter = array('ID' => $arParams['NEWS_ID'], 'IBLOCK_ID' => $arParams["IBLOCK_ID"], 'ACTIVE' => false, 'ACTIVE_DATE' => false);

    $arSelect = array(
        'ID',
        'PROPERTY_LIBRARY',
        'PROPERTY_GALLERY',
        'PROPERTY_YOUTUBE',
        'PROPERTY_FILE',
        'ACTIVE',
        'NAME',
        'CODE',
        'PREVIEW_TEXT',
        'DETAIL_TEXT',
        'DETAIL_PICTURE',
        'DATE_ACTIVE_FROM',
    );

    $arResult = Bitrix\NotaExt\Iblock\Element::getList($arFilter, 1, $arSelect);

    if (!empty($arResult['ITEM']['DETAIL_PICTURE'])) {
        $arResult['ITEM']['DETAIL_PICTURE'] = CFile::GetFileArray($arResult['ITEM']['DETAIL_PICTURE']);
    }

    if (!empty($arResult['ITEM']['PROPERTY_GALLERY_PROPERTY_VALUE_ID'])) {
        foreach ($arResult['ITEM']['PROPERTY_GALLERY_PROPERTY_VALUE_ID'] as $key => $valID) {
            $photoID = $arResult['ITEM']['PROPERTY_GALLERY_VALUE'][$key];
            $desc = $arResult['ITEM']['PROPERTY_GALLERY_DESCRIPTION'][$key];

            $arResult['ITEM']['GALLERY'][$valID] = CFile::GetFileArray($photoID);
            $arResult['ITEM']['GALLERY'][$valID]['DESCRIPTION'] = $desc;
        }
    }

    if (!empty ($arResult['ITEM']['PROPERTY_FILE_VALUE'])) {
        $arResult['ITEM']['FILE'] = CFile::GetFileArray($arResult['ITEM']['PROPERTY_FILE_VALUE']);
        $arResult['ITEM']['FILE']['DESCRIPTION'] = $arResult['ITEM']['PROPERTY_FILE_DESCRIPTION'];
        $arResult['ITEM']['FILE']['VALUE_ID'] = $arResult['ITEM']['PROPERTY_FILE_VALUE_ID'];
    }

    if (empty($arResult["ITEM"]['ID'])) {
        ShowError("404 Not Found");
        @define("ERROR_404", "Y");
        CHTTP::SetStatus("404 Not Found");
    }
}

// А так же получим данные о библиотеке
$nebUser = new nebUser();
$library = $nebUser->getLibrary();
$arResult['LIBRARY'] = $library;

$this->IncludeComponentTemplate();

if (!empty($arParams['NEWS_ID']))
    $APPLICATION->SetTitle("Редактирование новости библиотеки");
else
    $APPLICATION->SetTitle("Добавление новости библиотеки");