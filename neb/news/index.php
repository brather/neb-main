<?
define('STATIC_PAGE', true);
define('FULL_WIDTH_CLASS', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(LANGUAGE_ID == 'ru' ? "Новости" : "News");
$APPLICATION->SetPageProperty("main_title", "<h2 class='category-title'>Новости</h2>");

?>

<?
$APPLICATION->IncludeComponent(
    "neb:news",
    "",
    array(
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => '/news/',
        "SEF_URL_TEMPLATES" => array(
            "list" => "",
            "section" => "#SECTION_CODE#/",
            "element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
        )
    )
);
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>