<?php

class TheDistance
{
    public static function getCurPosition()
    {
        $ip = $_SERVER['REMOTE_ADDR'];

        #http://ipgeobase.ru:7020/geo?ip=195.133.194.22
        $arResult = array();

        $obCacheExt = new Bitrix\NotaExt\NPHPCacheExt();
        $arCacheParams['ip'] = $ip;

        if (!$obCacheExt->InitCache(__CLASS__ . __function__, $arCacheParams, '', 2592000)) {
            $strQueryText = QueryGetData(
                "ipgeobase.ru",
                7020,
                "/geo",
                "ip=" . $ip,
                $error_number,
                $error_text
            );

            if (!empty($strQueryText)) {
                $xml = simplexml_load_string($strQueryText);
                $arResult['lat'] = (string)$xml->ip->lat;
                $arResult['lng'] = (string)$xml->ip->lng;
                $arResult['city'] = (string)$xml->ip->city;
            }

            $obCacheExt->StartDataCache($arResult);
        } else {
            $arResult = $obCacheExt->GetVars();
        }
        return $arResult;
    }

    public static function getDistance($lat, $lng)
    {
        if (empty($lat) or empty($lng))
            return false;

        $arCurPos = self::getCurPosition();
        if (empty($arCurPos))
            return false;
        $distance = self::calculateTheDistance($arCurPos['lat'], $arCurPos['lng'], $lat, $lng);
        return $distance;
    }

    /*
    * Расстояние между двумя точками
    * $φA, $λA - широта, долгота 1-й точки,
    * $φB, $λB - широта, долгота 2-й точки
    */
    public static function calculateTheDistance($φA, $λA, $φB, $λB)
    {

        // перевести координаты в радианы
        $lat1 = $φA * M_PI / 180;
        $lat2 = $φB * M_PI / 180;
        $long1 = $λA * M_PI / 180;
        $long2 = $λB * M_PI / 180;

        // косинусы и синусы широт и разницы долгот
        $cl1 = cos($lat1);
        $cl2 = cos($lat2);
        $sl1 = sin($lat1);
        $sl2 = sin($lat2);
        $delta = $long2 - $long1;
        $cdelta = cos($delta);
        $sdelta = sin($delta);

        // вычисления длины большого круга
        $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
        $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

        //
        $ad = atan2($y, $x);
        $dist = $ad * 6372795;

        return $dist;
    }
}

/*
$lat1 = 77.1539;
$long1 = -139.398;
$lat2 = -77.1804;
$long2 = -139.55;

echo calculateTheDistance($lat1, $long1, $lat2, $long2) . " метров";
// Вернет "17166029 метров"
*/