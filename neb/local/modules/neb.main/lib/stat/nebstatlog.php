<?php
/**
 * User: agolodkov
 * Date: 14.08.2015
 * Time: 10:08
 */

namespace Neb\Main\Stat;


use Bitrix\Main\Entity\DataManager;

class NebStatLogTable extends DataManager
{
    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'neb_stat_log';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID'       => array(
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
            ),
            'DT'       => array(
                'data_type' => 'date'
            ),
            'ID_BOOK'  => array(
                'data_type' => 'string',
            ),
            'ID_LIB'   => array(
                'data_type' => 'integer',
            ),
            'CNT_READ' => array(
                'data_type' => 'integer',
            ),
            'CNT_VIEW' => array(
                'data_type' => 'integer',
            ),
        );
    }

}