$(function(){
	$(document).on('click', '[data-publish]', function(e){
		e.preventDefault();
		var checkbox = $(this),
			newsID = checkbox.data('news_id'),
			isActive = checkbox.prop('checked'),
			active = isActive ? 'Y' : 'N',
			message = {
	                title: isActive ? 'Опубликовать?' : 'Снять с публикации?',
	                // text: toggler.data('confirm-text') || undefined,
	                confirmTitle: "Да"
	            };
	        $.when( FRONT.confirm.handle(message) ).then(function(confirmed){
	            if(confirmed){
	                $.ajax({
						url: '/local/tools/library_news_lk/actions.php',
						data: {'id':newsID, 'action':'active', 'active':active},
						success: function(data){
							//console.log('success');
							checkbox.prop('checked', isActive);
						}
					});
	            }
	        });

	})
	.on('click', '[data-delete-with-confirm]', function(e){
		e.preventDefault();
		var toggler = $(this),
			delhref = toggler.attr('href'),
			listItem = toggler.closest('[data-news-list-item]'),
			message = {
		        title: 'Удалить новость?',
		        confirmTitle: "Удалить"
		    };
		$.when( FRONT.confirm.handle(message) ).then(function(confirmed){
	        if(confirmed){
	        	//console.log(delhref)
				$.ajax({
					url: delhref,
					success: function(data){
						$(listItem).slideUp();
					}
				});
	        }
	    });
	});
});