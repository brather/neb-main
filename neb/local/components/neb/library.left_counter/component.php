<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 3600;

$arParams['LIBRARY_ID'] = intval($arParams['LIBRARY_ID']);
if (empty($arParams['LIBRARY_ID']))
    return false;

$arParams['COLLECTION_URL'] = trim($arParams['COLLECTION_URL']);

if (empty($arParams['IBLOCK_ID']))
    return false;

use Bitrix\NotaExt\Iblock\Element;

$arResult = Element::getByID(
    $arParams['LIBRARY_ID'],
    array(
        'PREVIEW_PICTURE',
        'PROPERTY_PUBLICATIONS',
        'PROPERTY_COLLECTIONS',
        'PROPERTY_USERS',
        'PROPERTY_VIEWS',
        'PROPERTY_STATUS',
        'skip_other'
    )
);

if (!empty($arResult['PREVIEW_PICTURE'])) {
    $arResult['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE'], array('width' => 150, 'height' => 150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
}

$this->IncludeComponentTemplate();
