<?php
require($_SERVER['DOCUMENT_ROOT']
    . '/bitrix/modules/main/include/prolog_before.php');
/**
 * @var CMain $APPLICATION
 */
$APPLICATION->IncludeComponent(
    'exalead:similar.list',
    '',
    Array(
        'PARENT_ID' => $_GET['idparent']
    )
);
require($_SERVER['DOCUMENT_ROOT']
    . '/bitrix/modules/main/include/epilog_after.php');