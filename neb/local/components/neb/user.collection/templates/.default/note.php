<li class="search-result__content-list-kind clearfix" id="_<?=$arItem['ID']?>"  data-page-item="<?=$arItem['ID']?>" data-favorite-item >

    <button class="btn btn-default button-remove" data-id="<?=$arItem['ID']?>" data-item-type="notes" data-delete-favorite-edition title="Удалить заметку" data-modal-title="Удалить заметку?" data-button-title="Удалить">&nbsp;</button>

    <div class="search-result__content-list-sidebar">
        <span class="search-result__content-list-number"><?php echo ($key+1)+($arResult['iNumPage']-1)*15 ?>.</span>
        <span class="search-result__content-list-status-icon search-result__content-list-status-icon--open"></span>
    </div>




    <div class="search-result__content-main">

        <div class="search-result__content-main-links">
            <a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay search-result__content-link-title" data-width="955"><?=$arItem['UF_BOOK_NAME']?></a>
            <p class="search-result__content-link-info">
                <a href="/search/?f_field[authorbook]=f/authorbook/<?=urlencode(mb_strtolower(strip_tags(trim($arItem['UF_BOOK_AUTHOR']))))?>"
                ><?=$arItem['UF_BOOK_AUTHOR']?></a>
            </p>
        </div>

        <a
            target="_blank"
            href="javascript:void(0);"
            class="btn btn-primary search-result__btn-notes"
            onclick="readBook(event, this); return false;"
            data-link="<?= $arItem['UF_BOOK_ID'] ?>"
            data-options="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'page' => trim(
                            $arItem['UF_NUM_PAGE']
                        ),
                    )
                ),
                ENT_QUOTES,
                'UTF-8'
            )?>"><?=GetMessage("NOTES_TEXT");?> <?= $arItem['UF_NUM_PAGE']; ?></a>

        <div class="search-result__note-text">
            <?=$arItem['UF_TEXT']?>
            <?=$arItem['UF_NOTE_AREA']?>
        </div>

        <div class="search-result__my-bar" data-list-widget>
            <a
                class="search-result__my-favorites"
                href="#"
                data-list-toggler
                data-list-src="<?=ADD_COLLECTION_URL?>list.php?t=notes&id=<?=$arItem['ID']?>"
                data-edition-id="<?=$arItem['ID']?>"
            >
                <?=GetMessage("NOTES_IN_MY_COLLECTIONS");?>
            </a>
            <div class="my-favorites-content" data-list-wrapper>
                ...
            </div>
        </div>


    </div><!-- /.b-quote -->

</li><!-- /.b-note_item -->