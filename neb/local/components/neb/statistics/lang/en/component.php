<?php
$MESS = array(
    'GRAPHIC_USERS_TITLE'                     => 'Registred users',
    'GRAPHIC_ACTIVE_USERS_TITLE'              => 'Active users',
    'GRAPHIC_PUBLICATIONS_TITLE'              => 'Publications',
    'GRAPHIC_PUBLICATIONS_DD_TITLE'           => 'Publications (deduplicated)',
    'GRAPHIC_SEARCH_COUNT_TITLE'              => 'Search queries',
    'GRAPHIC_VIEWS_TITLE'                     => 'View pages',
    'GRAPHIC_DOWNLOADS_TITLE'                 => 'Downloads',
    'GRAPHIC_FEEDBACK_COUNT_TITLE'            => 'Feedbacks',
    'GRAPHIC_VIEWS_BOOK_TITLE'                => 'View books',
    'GRAPHIC_PUBLICATIONS_DIGITIZED_TITLE'    => 'Digitized publications',
    'GRAPHIC_PUBLICATIONS_DIGITIZED_DD_TITLE' => 'Digitized publications (deduplicated)',
);