<?php
namespace LbcBundle;


use BitrixBundle\DependencyInjection\BitrixHelper;
use BitrixBundle\Security\UserProvider;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * @backupGlobals disabled
 */
class HistoryTest extends \Codeception\Test\Unit
{
    const HTTP_ERROR_CODE = "Unexpected HTTP status code for GET %s";

    /**
     * @var \LbcBundle\UnitTester
     */
    protected $tester;

    /**
     * @var \appTestDebugProjectContainer
     */
    protected $_serviceContainer;

    protected function _before()
    {
        /** @var \Codeception\Module\Symfony2 $symfony */
        $symfony = $this->getModule('Symfony2');
        $this->_serviceContainer = $symfony->kernel->getContainer();
    }

    protected function _getClient()
    {
        return $this->_serviceContainer->get('test.client');
    }

    protected function _assertHttpSuccess(Client $client, $url)
    {
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), sprintf(static::HTTP_ERROR_CODE, $url));
    }

    /**
     * @backupGlobals enabled
     */
    public function testHistory()
    {
        /** @var BitrixHelper $bitrix */
        $bitrix = $this->_serviceContainer->get('bitrix_helper');
        $bitrix->requireProlog();
        global $USER_FIELD_MANAGER;
        $user = \CUser::GetByLogin('operator@elar.ru')->Fetch();
        $this->assertTrue(!empty($user['ID']), 'User operator@elar.ru not found');
        /** ************** **/

        $userFields = $USER_FIELD_MANAGER->GetUserFields('USER', $user['ID']);
        $this->assertTrue(!empty($userFields['UF_TOKEN']['VALUE']), 'User have empty token');
        /** ************** **/

        $client = $this->_getClient();
        $url = '/api/bbk/maindivision/22/?token=' . $userFields['UF_TOKEN']['VALUE'];
        $comment = 'Comment' . rand(1, 1000);
        $client->request(
            'PATCH',
            $url,
            [
                'lbcbundle_lbcmaindivision' => [
                    'comments' => $comment
                ]
            ]
        );
        $this->assertEquals(204, $client->getResponse()->getStatusCode(), sprintf(static::HTTP_ERROR_CODE, $url));
        /** ************** **/

        $client->request(
            'GET',
            '/api/bbk/maindivision/22/history/?token=' . $userFields['UF_TOKEN']['VALUE'],
            [],
            [],
            [
                'HTTP_ACCEPT' => 'application/json',
            ]
        );
        $content = $client->getResponse()->getContent();
        $content = json_decode($content, true);
        $lastItem = end($content['items']);

        foreach ($lastItem['fields'] as $field) {
            if ('comments' === $field['name']) {
                $this->assertEquals($field['value'], $comment);
                break;
            }
        }
        /** ************** **/
    }
}