<?php

/**
 * User: agolodkov
 * Date: 31.07.2015
 * Time: 10:37
 */
class UserEditComponent extends \CBitrixComponent
{
    /**
     * @var nebUser
     */
    protected $_user;

    /**
     * @param array $params
     *
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params = array_replace_recursive(
            [
                'MAIL_EVENTS' => [],
            ],
            $params
        );
        if (isset($params['USER_ID'])) {
            $params['USER_ID'] = (integer)$params['USER_ID'];
        } else {
            $params['USER_ID'] = 0;
        }

        return $params;
    }


    public function executeComponent()
    {
        try {
            \Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
            $this->arResult['success'] = false;
            $this->_applyUserAction();

            $this->arResult['userData'] = $this->_getUser()->getUser();
            $this->arResult['errors'] = $this->_getUser()->getErrors();

            $this->_prepareResults();

            $this->IncludeComponentTemplate($this->_getTemplateName());

            return;
        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

    protected function _prepareResults()
    {
    }

    /**
     * @return $this
     */
    protected function _applyUserAction()
    {
        $this->_applyRequestUserData();
        $user = $this->_getUser();
        $user->setValidationMapType($user->getOption('action'));
        if ('save' === $user->getOption('action')
            || 'save-strict' === $user->getOption('action')
        ) {
            $this->arResult['success'] = $user->save();
            if ($user->hasErrors()) {
                $this->arResult['success'] = false;
            } else {
                $this->_sendEmails();
            }
        }

        return $this;
    }

    protected function _applyRequestUserData()
    {
        $user = $this->_getUser();
        $user->applyUserData(
            $user->getRequest()->getRequestFieldsGroup('form')
        );
        $user->setOptions(
            $user->getRequest()->getRequestFieldsGroup('options')
        );
    }

    /**
     * @return $this
     */
    protected function _sendEmails()
    {
        if (is_array($this->arParams['MAIL_EVENTS'])) {
            $this->_getUser()->sendEmails(
                $this->arParams['MAIL_EVENTS']
            );
        }

        return $this;
    }

    /**
     * @return string
     */
    protected function _getTemplateName()
    {
        $template = 'template';
        if ($this->arResult['success']) {
            if (isset($this->arParams['SUCCESS_TEMPLATE'])) {
                $template = $this->arParams['SUCCESS_TEMPLATE'];
            }
        }

        return $template;
    }

    /**
     * @return \helpers\user\nebReader|nebUser
     */
    protected function _getUser()
    {
        if (null === $this->_user) {
            $this->_user = nebUser::forgeEmpty();
            $this->_user->setId($this->arParams['USER_ID']);
        }

        return $this->_user;
    }
}