<? require_once($_SERVER["DOCUMENT_ROOT"]
    . "/bitrix/modules/main/include/prolog_before.php");

/** @todo переделать это когданить */

CModule::IncludeModule("nota.exalead");
use Nota\Exalead\BiblioCardTable;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$book = null;
$BOOK_ID = isset($_REQUEST['BOOK_ID']) ? (int)$_REQUEST['BOOK_ID'] : false;
if ($BOOK_ID) {
    $book = BiblioCardTable::getRow(Array('filter' => Array('Id' => $BOOK_ID)));
    $book['views'] = 0;
    if (isset($book['FullSymbolicId']) && !empty($book['FullSymbolicId'])) {
        $bookView = \Nota\Exalead\BooksInfoTable::getRow(
            array(
                'filter' => array(
                    'BOOK_ID' => $book['FullSymbolicId'],
                ),
                'select' => array(
                    'VIEWS'
                )
            )
        );
        $book['views'] = (integer)$bookView['VIEWS'];
    }
}
$token = nebUser::getCurrent()->getToken();

?>
<!doctype html>
<html>
<div class="b-addbook" id="rh-book-calculator">
    <? if (null === $book) {
        ShowError('Книга не найдена');
    } else { ?>
        <h2><?= $book['Name'] ?></h2>
        <div>
            Автор: <b><?= $book['Author'] ?></b>
        </div>
        <div>
            Просмотров: <span class="count-views"><?= $book['views'] ?></span>
        </div>
        <form>
            <label>
                Цена за просмотр:
                <input type="text" class="one-view-price"/>
            </label>
        </form>
        Общая сумма: <span class="price-summ"></span>
        <div class="fieldrow nowrap fieldrowaction">
            <div class="fieldcell ">
                <div class="field clearfix">
                    <button class="formbutton left" value="1"
                            data-ok-message="Запрос отправлен">
                        Запросить стоимость просмотра
                    </button>
                </div>
            </div>
        </div>
        <script type="application/javascript">
            $(function () {
                var views = <?=$book['views']?>;
                var viewPrice = <?=(float)$book['view_price']?>;
                var saveTimeout = null;
                if(viewPrice) {
                    $('.one-view-price').val(viewPrice);
                }
                $('.one-view-price').on('keyup', function () {
                    var value = $(this).val();
                    value = parseFloat(value);
                    $('.price-summ').text((views * value).toFixed(2));

                    if (null !== saveTimeout) {
                        window.clearTimeout(saveTimeout);
                    }
                    saveTimeout = window.setTimeout(function () {
                        if (viewPrice !== value) {
                            viewPrice = value;
                            $.ajax({
                                type: 'PUT',
                                url: '/rest_api/book/request/',
                                data: {
                                    token: '<?=$token['UF_TOKEN']?>',
                                    request_id: '<?=$BOOK_ID?>',
                                    view_price: viewPrice
                                }
                            });
                        }
                    }, 500);
                });
                $('#rh-book-calculator').find('.formbutton').click(function () {
                    $.ajax({
                        type: 'POST',
                        url: '/rest_api/rightholder/request-view-price/',
                        data: {
                            token: '<?=$token['UF_TOKEN']?>',
                            book_id: '<?=$BOOK_ID?>'
                        },
                        success: function (data) {
                            if (data.hasOwnProperty('success')
                                && true === data.success) {
                                var button = $(this);
                                button.attr('disabled');
                                button.addClass('gray');
                                button.text(button.data('ok-message'));
                            }
                        }.bind(this)
                    });
                });
            });
        </script>
    <? } ?>
</div>
</html>
