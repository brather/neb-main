<?php
/**
 * User: agolodkov
 * Date: 01.04.2015
 * Time: 13:48
 */
/**
 * @var CBitrixComponentTemplate $this
 * @var CMain                    $APPLICATION
 * @var array                    $arResult
 * @var array                    $arParams
 */

$APPLICATION->RestartBuffer();
$fname = basename($APPLICATION->GetCurPage(), ".php");
// http response splitting defence
$fname = str_replace(array("\r", "\n"), "", $fname);
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: filename=" . $fname . ".xls");

echo '
	<html>
	<head>
	<title>' . $APPLICATION->GetTitle() . '</title>
	<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET
    . '">
	<style>
	td {mso-number-format:\@;}
	.number0 {mso-number-format:0;}
	.number2 {mso-number-format:Fixed;}
	</style>
	</head>
	<body>';

echo "<table border=\"1\">";

if (!empty($arResult['statData'])) {
    foreach ($arResult['statData'] as $arItem) {
        echo "<tr>";
        foreach ($arItem as $value) {
            echo '<td>', $value, '</td>';
        }
        echo "</tr>";
    }
}

echo "</table>";
echo '</body></html>';

exit();