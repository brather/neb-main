<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
/**
 * @var CMain $APPLICATION
 * @var array $arResult
 */

?>
</div>
</section>