<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->SetTitle("Статистика по времени, потребовавшемуся на обработку каждой заявки");

/**
 * Вывод всех заявок
 */
?>
<p>
    <a href="<?=$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['statistics_total']?>">Статистика по количеству</a>
    |
    <span href="<?=$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['statistics_time']?>">Статистика по времени исполнения</span>
</p>

<h2></h2>

<?php

$APPLICATION->IncludeComponent(
    "neb:feedback.statistics",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "TIME"               => 'Y',
        'FEEDBACK_URL'       => $arParams['SEF_FOLDER'],
        'FEEDBACK_STAT_URL'  => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['statistics_time']
    ),
    $component
);