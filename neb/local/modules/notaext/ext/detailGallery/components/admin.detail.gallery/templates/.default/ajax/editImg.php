<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!check_bitrix_sessid())
    die('Error sessid');

global $USER;
if (!$USER->IsAuthorized())
    die('Error auth');

$ELEMENT_ID = intval($_REQUEST['ELEMENT_ID']);

if ($ELEMENT_ID <= 0)
    die('Error ID');

CModule::IncludeModule("iblock");

$PREVIEW_TEXT = trim($_POST['DETAIL_GALLERY_PREVIEW_TEXT']);

$el = new CIBlockElement;
$res = $el->Update($ELEMENT_ID, array('PREVIEW_TEXT' => $PREVIEW_TEXT));
if (!$res) {
    die($res->LAST_ERROR);
}
