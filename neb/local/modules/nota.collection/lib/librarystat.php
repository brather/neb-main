<?
namespace Nota\Collection;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class LibraryTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> LIBRARY_ID int mandatory
 * <li> PUBLICATIONS int mandatory
 * <li> COLLECTIONS int mandatory
 * <li> USERS int mandatory
 * <li> VIEWS int mandatory
 * <li> DATE datetime mandatory default 'CURRENT_TIMESTAMP'
 * </ul>
 *
 * @package Bitrix\Stat
 **/
class LibraryStatTable extends Entity\DataManager
{
    const DEFAULT_STAT_DAYS = 30;
    const FEDERAL_LIBRARIES_ID = 999999999;

    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'neb_stat_library';
    }

    public static function getMap()
    {
        return array(
            'ID'                        => array(
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
                'title'        => Loc::getMessage('LIBRARY_ENTITY_ID_FIELD'),
            ),
            'LIBRARY_ID'                => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_LIBRARY_ID_FIELD'
                ),
                'default_value' => 0,
            ),
            'PUBLICATIONS'              => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_PUBLICATIONS_FIELD'
                ),
                'default_value' => 0,
            ),
            'COLLECTIONS'               => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_COLLECTIONS_FIELD'
                ),
                'default_value' => 0,
            ),
            'USERS'                     => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_USERS_FIELD'
                ),
                'default_value' => 0,
            ),
            'VIEWS'                     => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_VIEWS_FIELD'
                ),
                'default_value' => 0,
            ),
            'ACTIVE_USERS'              => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_ACTIVE_USERS_FIELD'
                ),
                'default_value' => 0,
            ),
            'DELETED_USERS'             => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_DELETED_USERS_FIELD'
                ),
                'default_value' => 0,
            ),
            'DOWNLOADS'                 => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_DOWNLOADS_FIELD'
                ),
                'default_value' => 0,
            ),
            'SEARCH_COUNT'              => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_SEARCH_COUNT_FIELD'
                ),
                'default_value' => 0,
            ),
            'FEEDBACK_COUNT'            => array(
                'data_type'     => 'integer',
                'required'      => true,
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_FEEDBACK_COUNT_FIELD'
                ),
                'default_value' => 0,
            ),
            'VIEWS_BOOK'                => array(
                'data_type'     => 'integer',
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_VIEWS_BOOK_FIELD'
                ),
                'default_value' => 0,
            ),
            'DATE'                      => array(
                'data_type'     => 'datetime',
                'required'      => true,
                'title'         => Loc::getMessage('LIBRARY_ENTITY_DATE_FIELD'),
                'default_value' => 0,
            ),
            'PUBLICATIONS_DIGITIZED'    => array(
                'data_type'     => 'integer',
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_PUBLICATIONS_DIGITIZED_FIELD'
                ),
                'default_value' => 0,
            ),
            'PUBLICATIONS_DIGITIZED_DD' => array(
                'data_type'     => 'integer',
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_PUBLICATIONS_DIGITIZED_DD_FIELD'
                ),
                'default_value' => 0,
            ),
            'PUBLICATIONS_DD'           => array(
                'data_type'     => 'integer',
                'title'         => Loc::getMessage(
                    'LIBRARY_ENTITY_PUBLICATIONS_DD_FIELD'
                ),
                'default_value' => 0,
            ),
        );
    }
}