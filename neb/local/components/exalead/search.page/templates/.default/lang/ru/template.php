<?
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO'] = 'Найдено 0 документов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND'] = 'К сожалению, ничего не найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SUGGEST'] = 'Возможно вы искали';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'Найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'Найден';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'Найдено';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'] = 'результатов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'] = 'результата';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'] = 'результат';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_SUGGEST'] = 'Возможно Вы имели в виду:';

$MESS['LIB_SEARCH_SIMPLE_QUERY'] = 'Поиск проведен по библиографической информации: ';
$MESS['LIB_SEARCH_DO_FULL_SEARCH'] = 'Искать по текстам изданий';
$MESS['LIB_SEARCH_ONLY_TEXT_QUERY'] = 'Запрашиваемая информация найдена только в текстах издания. ';
$MESS['LIB_SEARCH_ONLY_CATALOG_SEARCH'] = 'Искать в каталогах печатных книг';
$MESS['LIB_SEARCH_PAGE_FULL_SEARCH'] = "Поиск дополнительно произведен по текстам изданий.";

$MESS['LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY'] = 'Сохранить поисковый запрос';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED'] = 'Поисковый запрос сохранен';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE'] = 'Добавить название';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SPECIFY_SEARCH'] = 'уточнить поиск';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE'] = 'Вы искали на портале';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE'] = 'Искать только в отсканированных изданиях';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY'] = 'По каталогу печатных изданий';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_EMPTY'] = 'Введите поисковое выражение';
$MESS['LIB_SEARCH_PAGE_SETTINGS'] = 'Настройка';
$MESS['LIB_SEARCH_PAGE_FOUND_MORE_300000_RESULT'] = 'Найдено более 300 000 результатов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_COPIES'] = 'экземпляров изданий';
$MESS["CT_BSP_KEYBOARD_WARNING"] = "В запросе \"#query#\" восстановлена раскладка клавиатуры.";
$MESS['LIB_SEARCH_PAGE_INCLUDING'] = "включая";
$MESS['LIB_SEARCH_PAGE_DUPLICATED'] = "дублирующих записей";
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SOURCE'] = 'Источник';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_READ'] = 'Читать';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_VIEW'] = 'Смотреть';

$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_RELEVANCE'] = 'По релевантности';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_AUTHOR'] = 'По автору';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_NAME'] = 'По названию';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_DATE'] = 'По дате';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_SHOW'] = 'Показать';

$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_AUTHORS'] = 'Авторы';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_DATE'] = 'Дата';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_PUBLISH'] = 'Издательство';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS'] = 'Библиотеки';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS_library'] = 'Библиотеки';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS_archive'] = 'Архивы';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS_museum'] = 'Музеи';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_PLACE'] = 'Место издания';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_COLLECTION'] = 'Коллекции';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_TERMS'] = 'Термины';
$MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'] = 'следующие';

 
