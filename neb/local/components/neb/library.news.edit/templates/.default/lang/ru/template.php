<?
$MESS['LIBRARY_NEWS_EDIT_REQ'] = "* – обязательные поля";
$MESS['LIBRARY_NEWS_EDIT_MODE_EDIT'] = "Редактирование новости библиотеки";
$MESS['LIBRARY_NEWS_EDIT_MODE_ADD'] = "Добавление новости библиотеки";
$MESS['LIBRARY_NEWS_EDIT_TITLE'] = "Заголовок";
$MESS['LIBRARY_NEWS_EDIT_FILL'] = "Заполните";
$MESS['LIBRARY_NEWS_EDIT_SHORT_DESC'] = "Краткое описание";
$MESS['LIBRARY_NEWS_EDIT_MAX_LEN'] = "Не более 50000 символов";
$MESS['LIBRARY_NEWS_EDIT_MAX_LEN_255'] = "Не более 255 символов";
$MESS['LIBRARY_NEWS_EDIT_MAX_LEN_1500'] = "Не более 1500 символов";
$MESS['LIBRARY_NEWS_EDIT_MIN_LEN'] = "Не менее 2 символов";
$MESS['LIBRARY_NEWS_EDIT_TEXT'] = "Текст новости";
$MESS['LIBRARY_NEWS_EDIT_DATE'] = "Дата новости";
$MESS['LIBRARY_NEWS_EDIT_ATTACH'] = "Прикрепить видео (вставьте ссылку на www.youtube.com)";
$MESS['LIBRARY_NEWS_EDIT_PREVIEW'] = "Посмотреть превью новости";
$MESS['LIBRARY_NEWS_EDIT_PUBLISH'] = "Опубликовать на сайте";
$MESS['LIBRARY_NEWS_EDIT_SAVE'] = "сохранить";
$MESS['LIBRARY_NEWS_EDIT_GO_TO'] = "Перейти к";
$MESS['LIBRARY_NEWS_EDIT_NEWS_LIST'] = "списку новостей";
$MESS['LIBRARY_NEWS_EDIT_CANCEL'] = "Отменить";
?>
