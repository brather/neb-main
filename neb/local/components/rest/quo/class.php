<?php

CBitrixComponent::includeComponentClass("rest:rest.server");

use \Nota\UserData\Quotes,
	\Bitrix\Main\UserTable,
	\Bitrix\Main\Loader;

/**
 * Компонент организует API по приёму, передаче, удалению цитат
 *
 * Class RestQuotes
 */
class RestQuotes extends RestServer
{
	protected $token, $bookID, $numPage, $ID, $text, $uid;

	/**
	* Т.к. через ЧПУ мы не обработаем GET, PUT, DELETE - обрабатываем их тут.
	* Но помним уже, что в параметрах у нас стоит ограничение, и другие REQUEST_METHOD не пройдут.
	*/
	public function processRequest($token = null, $bookID = null, $numPage = null, $data = null,  $ID = null)
	{
		if ( isset($token) && !empty($token) )
			$this->token = trim($token);
			
		if ( isset($bookID) && !empty($bookID) )
			$this->bookID = trim($bookID);
			
		if ( isset($numPage) && !empty($numPage) )
			$this->numPage = trim($numPage);
			
		if ( isset($data) && !empty($data) )
			$this->data = trim($data);
		
		if ( isset($ID) && !empty($ID) )
			$this->ID 	= $ID;
	
		// Токен нам нужен везде - так что проверить его можно и тут.
		if ( strlen($this->token) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));
			
		// Теперь сразу найдём uid пользователя
		$arUser = UserTable::getList([
			'filter' => ['ACTIVE' => 'Y', 'UF_TOKEN' => $this->token],
			'select' => ['ID', 'LOGIN']
		])->fetch();

		if ( !$arUser || intval($arUser['ID']) <= 0 || $arUser['LOGIN'] == nebUser::ANONYMOUS_LOGIN)
			return $this->restError(404, GetMessage('USER_NOT_FOUND'));
		else
			$this->uid = $arUser['ID'];

		Loader::includeModule('nota.userdata');

		switch ( $_SERVER['REQUEST_METHOD'] )
		{
			case 'PUT':
				return $this->PUTQuo();
				break;
			case 'GET':
				return $this->GETQuotes();
				break;
			case 'DELETE':
				return $this->DELETEQuo();
				break;
		}
	}
	
	/**
	* Добавление цитаты
	*/
	protected function PUTQuo()
	{
		if ( !$this->bookID || strlen($this->bookID) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'book_id')));
	
		if ( !$this->numPage || strlen($this->numPage) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'num_page')));
		if ( !is_numeric($this->numPage) )
			return $this->restError(500, GetMessage('NUM_PAGE_INT'));
			
		if ( strlen($this->data) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'data')));
		else 
			list($left, $top, $width, $height) = explode(',', $this->data);
		
		$arFields = array(
		
			'BOOK_ID' => $this->bookID,
			'PAGE' => $this->numPage,
			'TOP' => $top,
			'LEFT' => $left,
			'WIDTH' => $width,
			'HEIGHT' => $height
		);

		$resultID = Quotes::add($arFields, $this->uid);
		$this->arResult = array(
			'resultID' => (int)$resultID
		);
		
		return 200;
	}
	
	/**
	* Отдаёт список всех цитат для книги
	*/
	protected function GETQuotes()
	{
		$result = Quotes::getListBook($this->bookID, $this->uid);
	
		if($result === false)
			$result = (object) array();
		$this->arResult = $result;
		
		return 200;
	}
	
	/**
	* Удаляет из цитат по token
	*/
	protected function DELETEQuo()
	{
		if ( !$this->ID || strlen($this->ID) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'ID')));

		// Удаляем с предварительным сопоставлением пользователю
		$result = Quotes::deleteWithTest($this->ID, $this->uid);

		if ( !$result ) {
			$this->restError(404, GetMessage('NOT_FOUND'));
		}

		$this->arResult = true;
		return 200;
	}
}