<?php

$MESS['LIB_SEARCH_FORM_TEMPLATE_ALL'] = 'по всем полям';
$MESS['LIB_SEARCH_FORM_TEMPLATE_NAME'] = 'название';
$MESS['LIB_SEARCH_FORM_TEMPLATE_MPUBLISHPLACE'] = 'место издательства';
$MESS['LIB_SEARCH_FORM_TEMPLATE_MPUBLISHER'] = 'издательство';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AUTHOR'] = 'автор';
$MESS['LIB_SEARCH_FORM_TEMPLATE_ISBN'] = 'ISBN';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TEXT'] = 'по тексту';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SEARCH'] = 'Расширенный поиск';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PORTAL'] = 'Искать в каталогах печатных книг';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TEXT'] = 'По каталогу печатных изданий';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FORACCESS'] = 'по доступу';
$MESS['LIB_SEARCH_FORM_TEMPLATE_OPEN_ACCESS'] = 'Открытый доступ';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CLOSE_ACCESS'] = 'Закрытый доступ';
$MESS['LIB_SEARCH_FORM_TEMPLATE_EXAMPLES'] = 'Пример: биография писателей';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SEARCH_FULL'] = 'Поиск по всем полям';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SEARCH_AUTHOR'] = 'По автору';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SEARCH_NAME'] = 'По заглавию';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FIND'] = 'Найти';

$MESS['LIB_SEARCH_FORM_TEMPLATE_OR'] = 'или';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AND'] = 'и';
$MESS['LIB_SEARCH_FORM_TEMPLATE_NO'] = 'не';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AUTHORBOOK'] = 'по автору';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TITLE'] = 'по названию';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PUBLISHER'] = 'по издательствам';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PUBLISHPLACE'] = 'по месту издания';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CATEGORY'] = 'по рубрикам';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE'] = 'по дате';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE_PUBLIC'] = 'Дата публикации';

$MESS['LIB_SEARCH_FORM_TEMPLATE_ADD'] = 'добавить условие';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DEL'] = 'удалить условие';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE_SUBMIT'] = 'Принять';

$MESS['LIB_SEARCH_FORM_TEMPLATE_ABOUT'] = 'О проекте';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FULLTEXT_SEARCH_ONLY'] = 'Искать только в отсканированных изданиях';
$MESS['LIB_SEARCH_FORM_TEMPLATE_STRICT_SEARCH'] = 'Искать по точной фразе';

$MESS['LIB_SEARCH_FORM_TEMPLATE_FIND_IN'] = 'Искать в найденном';

$MESS['LIB_SEARCH_FORM_TEMPLATE_FOR_FULL_TEXT'] = 'Искать по электронным копиям изданий';
$MESS['LIB_SEARCH_FORM_TEMPLATE_IN_DISERTATIONS'] = 'Искать в авторефератах и диссертациях';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CLEAN'] = 'Очистить поиск';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SCANNED_BOOKS'] = 'Отсканированные книги российских библиотек';
$MESS['LIB_SEARCH_FORM_TEMPLATE_BETA'] = 'beta-версия';
$MESS["CT_BSP_KEYBOARD_WARNING"] = "В запросе \"#query#\" восстановлена раскладка клавиатуры.";


$MESS["TAB_LIBRARY"] = "Библиотеки";
$MESS["TAB_MUSEUM"] = "Музеи";
$MESS["TAB_ARCHIVE"] = "Архивы";
$MESS["BUTTON_SEARCH"] = "Найти";
$MESS["SEARCH_PLACEHOLDER"] = "Поиск по всем полям";
$MESS["LIB_SEARCH_FORM_TEMPLATE_AUTHORSBOOK"] = 'Авторы';

$MESS["EXTENDED_SEARCH"] = "Расширенный поиск";
$MESS["PUBLISHED"] = "Дата публикации";
$MESS["PUBLISHED_FROM"] = "от";
$MESS["PUBLISHED_TO"] = "до";
$MESS["PUBLISHED_YEAR"] = "года";

$MESS["COLLECTIONS_SEARCH"] = "Искать по коллекциям: ";
$MESS["SEARCH_CHOISE_COLLECTION"] = 'выберите коллекцию';

$MESS["SEARCH_LOGIC_AND"] = 'и';
$MESS["SEARCH_LOGIC_OR"] = 'или';
$MESS["SEARCH_LOGIC_NOT"] = 'не';

$MESS["SEARCH_ALL"] = 'по всем полям';
$MESS["SEARCH_authorbook"] = 'по автору';
$MESS["SEARCH_title"] = 'по названию';
$MESS["SEARCH_publisher"] = 'по издательствам';
$MESS["SEARCH_publishplace"] = 'по месту издания';

$MESS["SEARCH_IN_CATALOG_OF_PUBLICATIONS"] = 'Искать в каталогах печатных изданий';
$MESS["SEARCH_BY_NEB_TEXTS"] = 'Искать в изданиях с электронной копией';
$MESS["SEARCH_EXACT_PHRASE"] = 'Искать по точной фразе';
$MESS["SEARCH_WITHIN_RESULTS"] = 'Искать в найденном';
$MESS["SEARCH_IN_CLOSED"] = 'Искать в изданиях, охраняемых авторским правом';
$MESS["SEARCH_DEBUG"] = 'Режим отладки';

$MESS["SEARCH_SIMPLE_SEARCH"] = '!!Простой поиск!!';
$MESS["SEARCH_AUTHOR_AND_OR_TITLE"] = 'Поиск изданий по автору и/или названию';
$MESS["SEARCH_AUTHOR_UPPER_TITLE"] = 'Автор';
$MESS["SEARCH_NAME_UPPER_TITLE"] = 'Название';
$MESS["SEARCH_NAME_UPPER_TITLE"] = 'Еще';

//$MESS[""] = '';