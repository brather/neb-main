<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));
?>
<h3>Выберите период</h3>
<form
    action="<?= $APPLICATION->GetCurPage() ?>?<? if ($arParams['FORMAT']
        == 'view'
    ): ?>form=view<? endif; ?>" method="GET" name="form1" class="form-inline">
    C 

    <div class="form-group">
        <div class="input-group">
            <input type="text" name="dateFrom" id="statDateFrom" size="10" value="<?=$arParams['dateFrom']?>" class="form-control datepicker-from">
            <span class="input-group-btn">
                <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'statDateFrom', field: 'statDateFrom', form: '', bTime: false, value: ''});">
                    <span class="glyphicon glyphicon-calendar"></span>
                </a>
            </span>
        </div>
    </div>
    
    по 

    <div class="form-group">
        <div class="input-group">
            <input type="text" name="dateTo" id="statDateTo" size="10" value="<?=$arParams['dateTo']?>" class="form-control datepicker-current">
            <span class="input-group-btn">
                <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'statDateTo', field: 'statDateTo', form: '', bTime: false, value: ''});">
                    <span class="glyphicon glyphicon-calendar"></span>
                </a>
            </span>
        </div>
    </div>
    
    
    <input type='hidden' value='<?= $arParams['LIBRARY_ID'] ?>'
           name='LIBRARY_ID'>
    <button class="btn btn-primary" type="submit" value="<?= Loc::getMessage('LIBRARY_STAT_REFRESH'); ?>">Обновить</button>
</form>
<h3>Статистика</h3>
<div>
     <ul class="uleditstat" data-type="lk-operator-stat">
         <? foreach ($arResult['charts'] as $code => $graph) { ?>
             <li class="active-parent-li"><a href="" class="active-parent">
                     <?= $graph['title'] ?><span class="icon"></span>
                 </a>
                 <ul class="submenu2" style="display: none;">
                     <li>
                         <span class="stat-description"><?=$graph['description']?></span>
                         <div data-chart-type="line"
                              style="min-width: 310px; height: 400px; margin: 0 auto"
                              data-chart-data-url="<?= $graph['url'] ?>"
                             >

                         </div>
                         <a style="position:relative; z-index: 100;"
                            target="_blank"
                            href="<?= $graph['exportUrl'] ?>">
                             <img
                                 src="/local/templates/adaptive/img/file-icons/excel.png"/>
                         </a>
                     </li>
                 </ul>
             </li>
         <? } ?>
     </ul>
</div>


<h3>Даты отчетов</h3>
<form action="/rest_api/library-publications-report/" method="GET" name="reportDatesForm" class="form-inline">

    <input type="hidden" name="token" value="<?=$arParams['userToken']?>" />
    
    <div>
        <div class="form-group" style="margin-right: 1ex;">
            <div class="input-group">
                <input type="text"  class="form-control datepicker-from" size="10" id="originalReportFrom" name="dateFrom" value="<?=$arParams['dateFrom']?>" />
                <span class="input-group-btn">
                    <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'originalReportFrom', field: 'originalReportFrom', form: '', bTime: false, value: ''});">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </a>
                </span>
            </div>
        </div>
        Дата отчета
    </div>
    <div style="margin-top: 1ex;">
        <div class="form-group" style="margin-right: 1ex;">
            <div class="input-group">
                <input type="text" size="10" class="form-control datepicker-current" id="originalReportTo" name="dateReportTo" value="<?=$arParams['dateTo']?>" />
                <span class="input-group-btn">
                    <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'originalReportTo', field: 'originalReportTo', form: '', bTime: false, value: ''});">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </a>
                </span>
            </div>
        </div>
        Дата предыдущего
    </div><br />

    <button class="btn btn-primary" type="submit"><?= Loc::getMessage('LIBRARY_PUBLICATIONS_EXPORT') ?></button>
</form><br />


<h3><?= Loc::getMessage('STAT_QUERY_EXPORT') ?></h3>
<span style="color: red;">Сбор ведётся с <?php echo $arResult['date_begin_stat']; ?></span>
<form action="/" method="GET" name="reportDatesForm" class="form-inline" style="margin: 1ex 0;">

    С 
    <div class="form-group" style="margin-right: 1ex;">
        <div class="input-group">
            <input type="text" id="date-from-11" name="date-from-11" size="10" value="<?php echo $arResult['date_begin_stat']; ?>" class="form-control">
            <span class="input-group-btn">
                <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'date-from-11', field: 'date-from-11', form: '', bTime: false, value: ''});">
                    <span class="glyphicon glyphicon-calendar"></span>
                </a>
            </span>
        </div>
    </div>    
    по 
    <div class="form-group" style="margin-right: 1ex;">
        <div class="input-group">
            <input type="text" name="date-to-11"  id="date-to-11" size="10" value="<?php echo date('d.m.Y'); ?>" class="form-control">
            <span class="input-group-btn">
                <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'date-to-11', field: 'date-to-11', form: '', bTime: false, value: ''});">
                    <span class="glyphicon glyphicon-calendar"></span>
                </a>
            </span>
        </div>
    </div>  
    

</form>
<br>
<div>
    <a target="_blank" href="javascript:void(0);return false;" class="btn btn-primary" onclick="get_first_report();return false;">
        <?= Loc::getMessage('STAT_QUERY_EXPORT_EXCEL') ?>
    </a>
</div>
<script>

        function get_first_report() {
            date_from = $('#date-from-11').val();
            date_to = $('#date-to-11').val();
            window.location.href = '/rest_api/library-publications-report/stat_query_report.php?date_from=' + date_from + '&date_to=' + date_to;
            return false;
        }

</script>