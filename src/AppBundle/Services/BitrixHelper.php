<?php
/**
 * User: agolodkov
 * Date: 12.05.2016
 * Time: 11:10
 */

namespace AppBundle\Services;


class BitrixHelper
{

    public function requireProlog($appPath)
    {
        $documentRoot = realpath(
            $appPath . '/../neb/'
        );
        $_SERVER['DOCUMENT_ROOT'] = $documentRoot;
        require_once($documentRoot
            . '/bitrix/modules/main/include/prolog_before.php');
    }
}