<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Электронные читальные залы');
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
require(dirname(__FILE__) . '/includes/header.php');
$APPLICATION->IncludeComponent(
    'neb:workplaces.list', '',
    [
        'WCHZ_NAME' => $_REQUEST['WCHZ_NAME'],
        'ORDER'     => $_REQUEST['order'],
        'BY'        => $_REQUEST['by'],
    ],
    $component
);
require(dirname(__FILE__) . '/includes/footer.php');
