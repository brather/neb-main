<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 11.10.2016
 * Time: 11:00
 */

use \Bitrix\Main\Loader,
    \Bitrix\Main\Application,
    \Bitrix\Iblock\ElementTable;

/**
 * Class UserVerificationComponent
 */
class UserVerificationComponent extends CBitrixComponent
{
    private   $componentPage = '';
    protected $arRequest = [], $arForm = [], $arTicket = [], $arUser = [], $arGroups = [], $arError = [];
    protected $iUserLibrary = 0;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams = $this->getSef($arParams);
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->includeComponentTemplate($this->componentPage);
    }

    /**
     * Роутинг ЧПУ
     * @param $arParams
     */
    private function getSef($arParams) {

        $arDefaultUrlTemplates404 = array(
            'list'          => '/',
            'user_add'      => 'user/add/',
            'user_edit'     => 'user/edit/#ID#/',
            'user_deact'    => 'user/deact/#ID#/',
            'ticket_add'    => 'ticket/add/',
            'ticket_edit'   => 'ticket/edit/#ID#/',
            'ticket_delete' => 'ticket/delete/#ID#/'
        );

        $arDefaultVariableAliases404 = array();

        $arDefaultVariableAliases = array();

        $arComponentVariables = array('ID');

        $SEF_FOLDER = '';
        $arUrlTemplates = array();

        if ($arParams['SEF_MODE'] == 'Y') {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams['SEF_URL_TEMPLATES']);
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams['VARIABLE_ALIASES']);

            $componentPage = CComponentEngine::ParseComponentPath($arParams['SEF_FOLDER'], $arUrlTemplates, $arVariables);

            if (StrLen($componentPage) <= 0) $componentPage = 'template';

            CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

            $SEF_FOLDER = $arParams['SEF_FOLDER'];

        } else {

            $arVariables = array();

            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams['VARIABLE_ALIASES']);
            CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

            $componentPage = '';
            if (IntVal($arVariables['ELEMENT_ID']) > 0) {
                $componentPage = 'detail';
            } else {
                $componentPage = 'list';
            }
        }
        
        $this->componentPage = $componentPage;

        $arParams['VARIABLES'] = $arVariables;

        return $arParams;
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    protected function getRequest() {
        return $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Получение текущего пользователя
     */
    protected function getUser() {
        global $USER;
        $this->arUser = static::getUserById($USER->GetID());
    }

    /**
     * Получение пользователя по идентификатору
     *
     * @param $iUser
     * @return array
     */
    protected static function getUserById($iUser) {
        if ($iUser > 0)
            return CUser::GetByID($iUser)->Fetch();
        else
            return [];
    }

    /**
     * Подключение модулей
     *
     * @param $arModules - массив модулей
     */
    protected static function includeModules($arModules) {
        foreach ($arModules as $sModule)
            Loader::includeModule($sModule);
    }

    /**
     * Получение ЭЧЗ по идентификатору
     *
     * @param $iWorkplace
     * @return array|false
     */
    protected static function getWorkplace($iWorkplace) {

        static::includeModules(['iblock']);

        $obIBlockElement = new \CIBlockElement;
        $arResult = $obIBlockElement->GetList(
            [],
            [
                'IBLOCK_ID' => IBLOCK_ID_WORKPLACES,
                'ID'        => $iWorkplace
            ],
            false,
            false,
            ["ID", "IBLOCK_ID", "ACTIVE", "NAME", 'PROPERTY_LIBRARY']
        )->Fetch();

        return $arResult;
    }
}