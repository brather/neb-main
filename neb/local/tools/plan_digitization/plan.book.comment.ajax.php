<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 28.12.2016
 * Time: 17:24
 */

define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

$arResult = ['result' => '0'];
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

$obNebUser = new nebUser();
if (!$obNebUser->IsAuthorized()) {
    MainHelper::showJson($arResult);
}

$bAccess = false;
$arUserGroups = $obNebUser->getUserGroups();
foreach ($arUserGroups as $iGroup => $sGroup) {
    if (false !== strpos('operator', $sGroup))
        $bAccess = true;
}

if (!$bAccess) {
    MainHelper::showJson($arResult);
}

Loader::includeModule('highloadblock');

$arParams = [
    'ID'        => intval($arRequest['ID']),
    'COMMENT'   => $arRequest['COMMENT'],
    'DATA_AJAX' => $arRequest['DATA_AJAX'] == 'Y'
];

if ($arParams['DATA_AJAX'] && $arParams['ID']) {

    $record = Application::getConnection()->query(
        "SELECT ID FROM b_hlblock_entity WHERE NAME='" . HIBLOCK_CODE_DIGITIZATION_PLAN . "';"
    )->fetch();

    $hlblock   = HighloadBlockTable::getById($record['ID'])->fetch();
    $dataClass = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

    $arPlanBook = $dataClass::getById($arParams['ID'])->fetch();
    if (empty($arPlanBook)) die();

    $result = $dataClass::update($arParams['ID'], ['UF_COMMENT' => $arParams['COMMENT']]);
    if ($result->isSuccess()) {
        $arResult['result'] = '1';
    }
}

MainHelper::showJson($arResult);