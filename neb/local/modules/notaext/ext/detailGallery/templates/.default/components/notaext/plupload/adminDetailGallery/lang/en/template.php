<?
$MESS ['EDBG_DRAG_FILES'] = "Drag files here";
$MESS ['EDBG_UPLOAD_BUTTON'] = "Upload from disc...";
$MESS ['EDBG_SAVE_BUTTON'] = "Continue to edit";
$MESS ['EDBG_REQUIRED_ERROR'] = "Fill in the required fields";
$MESS ['EDBG_DELETE_IMG'] = "Delete";
$MESS ['EDBG_DELETE_IMG_TITLE'] = "Delete this photo";
$MESS ['EDBG_DELETE_IMG_CONFIRM'] = "Are you sure you want to delete the file?";
$MESS ['EDBG_ICO_DATE'] = "Click to select a date";
$MESS ['EDBG_COMMON_TAGS'] = "Common tags";
