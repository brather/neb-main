<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$obUser = new CUser;

if (check_bitrix_sessid() && !empty($_REQUEST['file']) && $obUser->IsAuthorized()) {

    $USER_ID = intval($_REQUEST["USER_ID"]);

    if ($USER_ID) {
        CModule::IncludeModule('nota.userdata');

        $obCUser = new nebUser(); // Текущий пользователь
        $obEUser = new nebUser($USER_ID); // Редактируемый пользователь
        $aCLibrary = $obCUser->getLibrary();
        $aELibrary = $obEUser->getLibrary();

        if (!( UGROUP_LIB_CODE_ADMIN === $obCUser->getRole() && 'user' === $obEUser->getRole() // админ библиотеки редактирует фото пользователя
                || $obCUser->GetID() === $obEUser->GetID() // пользователь сам редактирует свое фото
        )) {
            die();
        }
    } else {
        $USER_ID = $obUser->GetID();
    }

    // формирование массива, описывающего файл
    $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $_REQUEST['file']);
    $arFile["MODULE_ID"] = "main";

    // обновление пользователя
    $obUser->Update($USER_ID, ['PERSONAL_PHOTO' => $arFile]);
}