<?
\Bitrix\Main\Loader::registerAutoLoadClasses(
    "notaext",
    array(
        'CExt' => 'classes/ext.php',
        'CMenuExt' => 'classes/menu_ext.php',
        'CExtOptions' => 'classes/CExtOptions.php',
    )
);