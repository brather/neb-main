<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use \Bitrix\Main\Localization\Loc;
global $APPLICATION;

Loc::loadMessages(__FILE__);
\Bitrix\Main\Loader::includeModule("intervolga.remoteuserclient");
$APPLICATION->SetTitle(Loc::getMessage("intervolga.remoteuserclient.PAGE_TITLE"));

CJSCore::Init();

/**
 * Max step time;
 */
$dTimeout = 10;
/**
 * Next upload step link.
 */
$sContinueLink = "";
/**
 * Upload problems.
 */
$arProblemsWithUsers = array();
/**
 * Stop uploading on first error.
 */
$bStopWhenProblem = TRUE;
/**
 * Clean all protected fields.
 */
$bCleanNoProblems = FALSE;
$iUploaded = 0;
$iCleaned = 0;
$iProblems = 0;

$bInProgress = FALSE;
/**
 * Process completed flag.
 */
$bEnd = FALSE;

$obUser = new CUser();

if (\Intervolga\RemoteUserClient\Rights::canWrite())
{
	if ($_REQUEST["upload"] == "Y" && check_bitrix_sessid())
	{
		$bInProgress = TRUE;
		/**
		 * ID to start with on next step.
		 */
		$iContinueId = 0;
		if (doubleval($_REQUEST["TIMEOUT"]))
		{
			$dTimeout = doubleval($_REQUEST["TIMEOUT"]);
		}
		$bStopWhenProblem = $_REQUEST["STOP_WHEN_PROBLEM"] == "Y";
		$bCleanNoProblems = $_REQUEST["CLEAN_NO_PROBLEMS"] == "Y";

		$iCleaned = intval($_REQUEST["CLEANED"]);
		$iUploaded = intval($_REQUEST["UPLOADED"]);
		$iProblems = intval($_REQUEST["PROBLEMS"]);

		$iTime = microtime(TRUE);
		$arFilter = array(
			">ID" => (intval($_REQUEST["ID"]) > 1 ? intval($_REQUEST["ID"]) : 1),
			"UF_IV_UPLOAD_PREVENT" => FALSE,
		);
		$rsUsers = \CUser::GetList(($sBy = "ID"), ($sOrder = "ASC"), $arFilter, Intervolga\RemoteUserClient\User::getUploadParams());

		while ($arUser = $rsUsers->Fetch())
		{
			$arProblems = Intervolga\RemoteUserClient\User::checkUpdate($arUser);
			if ($arProblems)
			{
				$iProblems++;
				$arProblemsWithUsers[$arUser["ID"]] = $arProblems;
			}
			else
			{
				$uploadResult = Intervolga\RemoteUserClient\User::upload($arUser);
				$obUser->Update($arUser["ID"], array("UF_IV_UPLOAD_DATE" => date("d.m.Y H:i:s")));
				if (!is_numeric($uploadResult))
				{
					$iProblems++;
					$arProblemsWithUsers[$arUser["ID"]] = $uploadResult;
				}
				else
				{
					$obUser->Update($arUser["ID"], array("UF_IV_REMOTE_ID" => $uploadResult));
					$iUploaded++;
					if ($bCleanNoProblems)
					{
						if ($arCleanProblems = Intervolga\RemoteUserClient\User::cleanLocalUser($arUser))
						{
							$arProblemsWithUsers[$arUser["ID"]] = array_merge(array(Loc::getMessage("intervolga.remoteuserclient.CLEAN_PROBLEMS")), $arCleanProblems);
						}
						else
						{
							$iCleaned++;
							$obUser->Update($arUser["ID"], array("UF_IV_UPLOAD_PREVENT" => 1));
						}
					}
				}
			}

			$bItsTime = microtime(TRUE) - $iTime >= $dTimeout;
			if ($bItsTime || ($bStopWhenProblem && $arProblems))
			{
				$iContinueId = $arUser["ID"];
				break;
			}
		}
		if (!$arUser)
		{
			$bEnd = TRUE;
		}

		if ($iContinueId && !$bEnd)
		{
			$sContinueLink = $APPLICATION->GetCurPageParam(
				http_build_query(array(
					"sessid" => bitrix_sessid(),
					"upload" => "Y",
					"TIMEOUT" => $dTimeout,
					"ID" => $iContinueId,
					"STOP_WHEN_PROBLEM" => $_REQUEST["STOP_WHEN_PROBLEM"],
					"CLEAN_NO_PROBLEMS" => $_REQUEST["CLEAN_NO_PROBLEMS"],
					"CLEANED" => $iCleaned,
					"UPLOADED" => $iUploaded,
					"PROBLEMS" => $iProblems,
				)),
				array("sessid", "upload", "TIMEOUT", "ID", "OK", "STOP_WHEN_PROBLEM", "CLEAN_NO_PROBLEMS", "CLEANED", "UPLOADED", "PROBLEMS")
			) . "#form";
		}
		if ($bEnd)
		{
			LocalRedirect($APPLICATION->GetCurPageParam(
				http_build_query(array(
					"OK" => "Y",
					"CLEANED" => $iCleaned,
					"UPLOADED" => $iUploaded,
					"PROBLEMS" => $iProblems,
				)),
				array("sessid", "upload", "TIMEOUT", "ID", "OK", "STOP_WHEN_PROBLEM", "CLEAN_NO_PROBLEMS", "CLEANED", "UPLOADED", "PROBLEMS"))
			);
		}
	}
	if ($_GET["OK"] == "Y")
	{
		$bInProgress = TRUE;
		$iCleaned = intval($_REQUEST["CLEANED"]);
		$iUploaded = intval($_REQUEST["UPLOADED"]);
		$iProblems = intval($_REQUEST["PROBLEMS"]);
	}
}

$sProblemsLog = "";
if ($arProblemsWithUsers)
{
	foreach ($arProblemsWithUsers as $iUser => $arUserProblems)
	{
		$sProblemsLog .= str_repeat("-", 80) . "\n";
		$sProblemsLog .= Loc::getMessage("intervolga.remoteuserclient.USER_PROBLEMS", array("#ID#" => $iUser)) . "\n";
		$sProblemsLog .= implode("\n", $arUserProblems);
		$sProblemsLog .= "\n";
	}
}

$arNotes = array();
$arErrorMessages = array();

if ($bEnd || $_GET["OK"] == "Y")
{
	$arNotes[] = Loc::getMessage("intervolga.remoteuserclient.UPLOAD_COMPLETE");
	$arNotes[] = "";
}

if ($iUploaded)
{
	$arNotes[] = Loc::getMessage("intervolga.remoteuserclient.UPLOADED_TOTAL", array("#TOTAL#" => $iUploaded));
}
if ($iCleaned)
{
	$arNotes[] = Loc::getMessage("intervolga.remoteuserclient.CLEANED_TOTAL", array("#TOTAL#" => $iCleaned));
}
if ($iProblems)
{
	$arErrorMessages[] = Loc::getMessage("intervolga.remoteuserclient.UPLOADED_ERRORS_TOTAL", array("#TOTAL#" => $iProblems));
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<? if (\Intervolga\RemoteUserClient\Rights::canWrite()): ?>
	<form action="<?=$APPLICATION->GetCurPageParam("", array("OK", "CLEANED", "UPLOADED", "PROBLEMS"))?>" method="get" id="form">
		<? if ($sProblemsLog): ?>
			<? if ($sProblemsLog): ?>
				<textarea cols="80" rows="15"><?=$sProblemsLog?></textarea>
				<div><?=Loc::getMessage("intervolga.remoteuserclient.TOTAL_PROBLEMS", array("#TOTAL#" => count($arProblemsWithUsers)))?></div>
			<? endif ?>
		<? endif ?>
		<? if ($arNotes): ?>
			<?CAdminMessage::ShowNote(implode("\n", $arNotes)) ?>
		<? endif ?>
		<? if ($arErrorMessages): ?>
			<?CAdminMessage::ShowMessage(implode("\n", $arErrorMessages)) ?>
		<? endif ?>
		<? if ($_GET["OK"] == "Y"): ?>
			<a href="/bitrix/admin/intervolga.remoteuserclient_upload.php"><?=Loc::getMessage("intervolga.remoteuserclient.AGAIN")?></a>
		<? endif ?>

		<?=bitrix_sessid_post()?>
		<input type="hidden" name="UPLOADED" value="<?=$iUploaded?>"/>
		<input type="hidden" name="PROBLEMS" value="<?=$iProblems?>"/>
		<input type="hidden" name="CLEANED" value="<?=$iCleaned?>"/>
		<? if ($bInProgress && strlen($sContinueLink)): ?>
			<a href="<?=$sContinueLink?>"> <?=Loc::getMessage("intervolga.remoteuserclient.CONTINUE")?></a>
			<? if (!$bStopWhenProblem || !$arProblemsWithUsers): ?>
				<p>
					<?=Loc::getMessage("intervolga.remoteuserclient.CONTINUE_IN_N_SECONDS", array("#SECONDS#" => 10))?>
					<a href="javascript:void(0)" id="timer-cancel"><?=Loc::getMessage("intervolga.remoteuserclient.CANCEL_RELOAD")?></a>
				</p>
				<script type="text/javascript">
					BX(function() {
						var iTimer = setInterval(function() {
							if (BX("timer").innerHTML > 1) {
								BX("timer").innerHTML = BX("timer").innerHTML - 1;
							}
							else
							{
								BX("timer").innerHTML = 0;
								clearInterval(iTimer);
								location.href = "<?=$sContinueLink?>";
							}
						}, 1000);
						BX.bind(BX("timer-cancel"), "click", function() {clearInterval(iTimer);})
					});
				</script>
			<? endif ?>
		<? elseif(!$bInProgress): ?>
			<?=BeginNote()?>
			<?=Loc::getMessage("intervolga.remoteuserclient.UPLOAD_FIELDS_BEFORE", array("#HREF#" => "/bitrix/admin/intervolga.remoteuserclient_upload_fields.php"))?>
			<?=EndNote()?>

			<div>
				<label>
					<input type="checkbox" value="Y" name="CLEAN_NO_PROBLEMS" <? if ($bCleanNoProblems): ?>checked="checked"<? endif ?>/>
					<?=Loc::getMessage("intervolga.remoteuserclient.CLEAN_NO_PROBLEMS")?>
				</label>
			</div>
			<div>
				<label>
					<input type="checkbox" value="Y" name="STOP_WHEN_PROBLEM" <? if ($bStopWhenProblem): ?>checked="checked"<? endif ?>/>
					<?=Loc::getMessage("intervolga.remoteuserclient.STOP_WHEN_PROBLEM")?>
				</label>
			</div>
			<div>
				<label for="TIMEOUT"><?=Loc::getMessage("intervolga.remoteuserclient.TIMEOUT")?></label>
				<input type="text" name="TIMEOUT" value="<?=($_GET["TIMEOUT"] ? intval($_GET["TIMEOUT"]) : 10) ?>" id="TIMEOUT"/>
			</div>
			<div>
				<label for="ID"><?=Loc::getMessage("intervolga.remoteuserclient.ID_START")?></label>
				<input type="text" name="ID" value="<?=($_GET["ID"] ? intval($_GET["ID"]) : 1) ?>" id="ID"/>
			</div>
			<div>
				<button type="submit" class="adm-btn adm-btn-save" name="upload" value="Y" style="padding: 3px 15px !important; height: 30px !important;"><?=Loc::getMessage("intervolga.remoteuserclient.UPLOAD")?></button>
			</div>
		<? endif ?>
	</form>
<? else: ?>
	<?CAdminMessage::ShowMessage(Loc::getMessage("intervolga.remoteuserclient.NO_ACCESS"))?>
<? endif ?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");