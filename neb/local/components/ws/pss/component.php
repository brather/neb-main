<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("webservice") || !CModule::IncludeModule("iblock") || !CModule::IncludeModule('highloadblock'))
    return;
use Bitrix\Highloadblock as HL;

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log_pss.txt");

class pssWS extends IWebService
{
    // проверка пользователя
    function IdentUser($USER){
        AddMessage2Log('user ---'.$USER);

        if(self::GetUserID($USER) > 0){
            return Array("IdentUserResult" => 1);
        }

        // добавление пользователя и отправка письма на почту с паролем
        $user = new CUser;
        $password = randString(6);

        $arFields = Array(
            "EMAIL"             => $USER,
            "LOGIN"             => $USER,
            "LID"               => "ru",
            "ACTIVE"            => "Y",
            "GROUP_ID"          => array(3,4),
            "PASSWORD"          => $password,
            "CONFIRM_PASSWORD"  => $password,
            "UF_STATUS" => 42,
            "UF_REGISTER_TYPE" => 37
        );

        $ID = $user->Add($arFields);
        if ($ID) {
            // отправка письма
            $arEventFields =  array(
                "LOGIN" => $USER,
                "PASSWORD" => $password
            );
            CEvent::Send("WS_PSS_ADD_USER", 's1', $arEventFields);
            return Array("IdentUserResult" => 0);
        }

        return Array("IdentUserResult" => -1);

    }

    // инициализация сессии
    function InitSession($ScanerID, $UserName){
        AddMessage2Log('scanner ---'.$ScanerID.';user ---'.$UserName);
        //проверка существует ли пользователь
        if (trim($UserName) != '') $arUser = self::GetUserID($UserName);

        if ($arUser > 0 && self::GetPagePrice($ScanerID) >= 0){


            //генерация guid
            $guid = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

            //логирование
            $data = array(
                "UF_SCANER_ID" => $ScanerID,
                "UF_USER" => $arUser,
                "UF_SESSION_ID" => $guid,
                "UF_USER_LOGIN" => $UserName
            );

            $HL_Infoblock_ID = 12;
            $hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $result = $entity_data_class::add($data);

            return Array("InitSessionResult" => $guid);
        }
        return Array("InitSessionResult" => '00000000-0000-0000-0000-000000000000');
    }


    // добавление изображения в заказ
    function SaveImage($SessionID, $NumberPage, $Image, $FileLength){

        if (strlen($Image) <= 0) return Array("SaveImageResult" => -1);

        CModule::IncludeModule('catalog');

        // проверяем есть ли сессия , находим пользователя
        if($arUser = self::GetUserSession($SessionID)){

            sleep($NumberPage);
            $userID = $arUser['UF_USER'];

            //найти идентификатор библиотек в bitrix
            $arSelect = Array("ID", "PROPERTY_BIBL");
            $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "PROPERTY_ID"=> $arUser['UF_SCANER_ID']);
            $res_lib = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
            if ($ob_lib = $res_lib->GetNextElement())
                $arFields_lib = $ob_lib->GetFields();

            $Library = $arFields_lib['PROPERTY_BIBL_VALUE'];

            // проверяем есть ли заказ
            $el = new CIBlockElement;

            $arSelect = Array("ID", "PROPERTY_PRICE", "PROPERTY_FILES_");
            $arFilter = Array("IBLOCK_ID"=>IBLOCK_PCC_ORDER, "ACTIVE"=>"Y", "=NAME" => $SessionID);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);

            $PRICE_TYPE_ID = 1;
            $CURRENCY = 'RUB';

            if (!$ob = $res->GetNextElement()){
                $PRICE = self::GetPagePrice($arUser['UF_SCANER_ID']);
                // создаем заказ
                $PROP = array();
                $PROP['ORDERID'] 		= $SessionID;
                $PROP['USERRFID'] 		= $userID;
                $PROP['STARTDATE'] 		= date("d.m.Y H:i:s", time());
                $PROP['FINISHDATE'] 	= date("d.m.Y H:i:s", time() + 30*86400);
                $PROP['PRICE'] 			= $PRICE;
                $PROP['LIBRARY'] 		= $Library;

                $arLoadProductArray = Array(
                    "IBLOCK_SECTION_ID" => false,
                    "IBLOCK_ID"      => IBLOCK_PCC_ORDER,
                    "PROPERTY_VALUES"=> $PROP,
                    "NAME"           => $SessionID,
                    "ACTIVE"         => "Y",
                );

                $SALE_ID = $el->Add($arLoadProductArray);

                $salePrice = 0;

                $arFields = array(
                    "ID" => $SALE_ID,
                    "QUANTITY" => 1,
                    "PURCHASING_PRICE" => $salePrice,
                    "PURCHASING_CURRENCY" => $CURRENCY,
                );

                if(CCatalogProduct::Add($arFields))
                {
                    $arFields = array(
                        "PRODUCT_ID" => $SALE_ID,
                        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                        "PRICE" => $salePrice,
                        "CURRENCY" => $CURRENCY,
                    );
                    $obPrice = new CPrice();
                    $res = $obPrice->Add($arFields);

                }

            }else{
                $arFields = $ob->GetFields();
                $SALE_ID = $arFields['ID'];
                $PRICE = $arFields['PROPERTY_PRICE_VALUE'];

                // количество сканов в заказе
                $PROP['FILES'] = $arFields['PROPERTY_FILES__VALUE'];
            }

            //сохранение файла
            $fileName = $_SERVER["DOCUMENT_ROOT"]."/upload/pcc/".md5($SessionID)."/".$NumberPage.'.jpeg';
            RewriteFile($fileName, $Image);
            list($w,$h)=getimagesize($fileName);
            $pic = ImageCreateFromjpeg($fileName);
            $color = imagecolorallocatealpha($pic,  0, 0, 0, 70);
            ImageTTFtext($pic, 30, 0, 30, $h-30, $color, $_SERVER["DOCUMENT_ROOT"].'/local/templates/.default/markup/font/tahoma.ttf', $arUser['UF_USER_LOGIN']);
            Imagejpeg($pic,$fileName);

            $PROP['FILES'][] = array('VALUE' => $fileName) ;
            $cnt_file = count($PROP['FILES']);

            AddMessage2Log('sessionId ---'.$SessionID.';file ---'.$fileName.'massFiles ---'.count($PROP['FILES']));
            CIBlockElement::SetPropertyValues($SALE_ID, IBLOCK_PCC_ORDER, $PROP['FILES'], 'FILES_');
            AddMessage2Log('sessionId ---'.$SessionID.';NumberPage ---'.$NumberPage.';countFiles ---'.$cnt_file);
            /*
			Изменяем цену в торговом каталоге
			*/
            $price = (int)$cnt_file * $PRICE;
            $arFields = array('PURCHASING_PRICE' => $price);// зарезервированное количество
            CCatalogProduct::Update($SALE_ID, $arFields);

            $db_res = CPrice::GetList(array(),array( "PRODUCT_ID" => $SALE_ID));
            if ($ar_res = $db_res->Fetch()){
                $arFields = array('PRICE' => $price);
                CPrice::Update($ar_res['ID'], $arFields);
            }

            AddMessage2Log('sessionId ---'.$SessionID.';imageSize ---'.$FileLength.';result --- 1');
            if ($SALE_ID > 0) return Array("SaveImageResult" => 1);
        }
        AddMessage2Log('sessionId ---'.$SessionID.';imageSize ---'.$FileLength.';result --- -1');
        return Array("SaveImageResult" => -1);
    }


    function CloseSession($SessionID){
        AddMessage2Log('sessionId ---'.$SessionID);
        // найти сессию
        $HL_Infoblock_ID = 12;
        $hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $entity_table_name = $hlblock['TABLE_NAME'];

        $rsData = $entity_data_class::getList(array(
            "select" => array('UF_USER', 'ID', 'UF_CLOSE'),
            "filter" => array("UF_SESSION_ID" => $SessionID, "UF_CLOSE" => NULL),
            "order" => array("ID"=>"DESC")
        ));
        $rsData = new CDBResult($rsData, entity_table_name);

        if($arRes = $rsData->Fetch()){
            sleep(4);
            $ID = $arRes['ID'];
            $entity_data_class::update($ID, array('UF_CLOSE' => '1'));
            $rsUsers = CUser::GetList(($by="id"), ($order="desc"), array('ID' => $arRes['UF_USER']), array('FIELDS'=> array('EMAIL'), array()));
            if ($arUser = $rsUsers->Fetch()){

                // отправка письма
                CEvent::Send("WS_PSS_ADD_SALE", 's1', array("LOGIN" => $arUser['EMAIL']));
            }

            return 1;
        }

        return -1;

    }

    function GetPagePrice($ScanerID){
        AddMessage2Log('scanner ---'.$ScanerID);

        $arSelect = Array("ID", "PROPERTY_PRICE");
        $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "PROPERTY_ID"=> $ScanerID);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
        if ($ob = $res->GetNextElement())
            $arFields = $ob->GetFields();


		if ( $arFields['PROPERTY_PRICE_VALUE']){
			return $arFields['PROPERTY_PRICE_VALUE'];
        }
        return - 1;
    }

    // поиск ID пользователя по идентификатору сессиии
    function GetUserSession($SessionID){

        $HL_Infoblock_ID = 12;
        $hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $entity_table_name = $hlblock['TABLE_NAME'];

        $rsData = $entity_data_class::getList(array(
            "select" => array('UF_USER', 'UF_USER_LOGIN', 'UF_SCANER_ID'),
            "filter" => array("UF_SESSION_ID" => $SessionID),
            "order" => array("ID"=>"DESC")
        ));
        $rsData = new CDBResult($rsData, entity_table_name);

        if($arRes = $rsData->Fetch())
            return $arRes;


    }


    // поиск пользователя
    function GetUserID($USER){

        //поиск по email
        $rsUsers = CUser::GetList(($by="id"), ($order="desc"), array('=EMAIL' => $USER), array('FIELDS'=> array('ID'), array()));
        if ($arUser = $rsUsers->Fetch())   return $arUser['ID'];

        //поиск по ECHB
        $rsUsers = CUser::GetList(($by="id"), ($order="desc"), array('UF_NUM_ECHB' => $USER), array('FIELDS'=> array('ID'), array()));
        if ($arUser = $rsUsers->Fetch())   return $arUser['ID'];

        //поиск по RGB_CARD_NUMBER
        $rsUsers = CUser::GetList(($by="id"), ($order="desc"), array('UF_RGB_CARD_NUMBER' => $USER), array('FIELDS'=> array('ID'), array()));
        if ($arUser = $rsUsers->Fetch())   return $arUser['ID'];

        //поиск по логину
        $rsUsers = CUser::GetList(($by="id"), ($order="desc"), array('LOGIN_EQUAL' => $USER), array('FIELDS'=> array('ID'), array()));
        if ($arUser = $rsUsers->Fetch())   return $arUser['ID'];

        return 0;
    }


    // метод GetWebServiceDesc возвращает описание сервиса и его методов
    function GetWebServiceDesc()
    {
        $wsdesc = new CWebServiceDesc();
        $wsdesc->wsname = "bitrix.webservice.pss"; // название сервиса
        $wsdesc->wsclassname = "pssWS"; // название класса
        $wsdesc->wsdlauto = true;
        $wsdesc->wsendpoint = CWebService::GetDefaultEndpoint();
        $wsdesc->wstargetns = CWebService::GetDefaultTargetNS();

        $wsdesc->classTypes = array();
        $wsdesc->structTypes = Array();

        $wsdesc->structTypes['User'] = array(
            'FIO' => array("varType" => 'string'),
            'IO' => array("varType" => 'string'),
            'LastName' => array("varType" => 'string'),
            'FirstName' => array("varType" => 'string'),
            'SecondName' => array("varType" => 'string'),
            'Balance' => array("varType" => 'float'),
            'OrderSumm' => array("varType" => 'float'),
        );

        $wsdesc->structTypes['Book'] = array(
            'Name' => array("varType" => 'string'),
            'IsCanScan' => array("varType" => 'bool'),
        );

        $wsdesc->structTypes['ScanPage'] = array(
            'UserRFID' => array("varType" => 'string'),
            'FileName' => array("varType" => 'string'),
            'Content' => array("varType" => 'base64Binary'),
        );

        $wsdesc->classes = array(
            "pssWS"=> array(
                "CloseSession" => array(
                    "type"      => "public",
                    "input"      => array(
                        "SessionID" => array("varType" => "string"),
                    ),
                    "output"   => array(
                        "CloseSessionResult" => array("varType" => "integer")
                    )

                ),

                "IdentUser" => array(
                    "type"      => "public",
                    "input"      => array(
                        "USER" => array("varType" => "string"),
                    ),
                    "output"   => array(
                        "IdentUserResult" => array("varType" => "integer")
                    )
                ),

                "InitSession" => array(
                    "type"      => "public",
                    "input"      => array(
                        "ScanerID" => array("varType" => "string"),
                        "UserName" => array("varType" => "string")
                    ),
                    "output"   => array(
                        "InitSessionResult" => array("varType" => "string")
                    ),

                ),

                "SaveImage" => array(
                    "type"      => "public",
                    "input"      => array(
                        "SessionID" => array("varType" => "string"),
                        "NumberPage" => array("varType" => "integer"),
                        "Image" => array("varType" => "base64Binary"),
                        "FileLength" => array("varType" => "integer"),
                    ),
                    "output"   => array(
                        "SaveImageResult" => array("varType" => "integer")
                    )
                ),

                /*
                "GetUserInfo" => array(
                    "type"      => "public",
                    "input"      => array(
                        "RFID" => array("varType" => "string"),
                        ),
                    "output"   => array(
                        "GetUserInfoResult" => array("varType" => "User")
                    ),
                    "httpauth" => "Y"
                ),

                "GetBookInfo" => array(
                    "type"      => "public",
                    "input"      => array(
                        "RFID" => array("varType" => "string"),
                        ),
                    "output"   => array(
                        "GetBookInfoResult" => array("varType" => "Book")
                    ),
                    "httpauth" => "Y"
                ),
                */
                "GetPagePrice" => array(
                    "type"      => "public",
                    "input"      => array(
                        "ScanerID" => array("varType" => "string"),
                    ),
                    "output"   => array(
                        "GetPagePriceResult" => array("varType" => "float")
                    )
                ),
                /*
                "InsertNewPage" => array(
                    "type" => "public",
                    "input" => array(
                        "page" => array("varType" => "ScanPage")
                    ),
                    "output"   => array(
                        "InsertNewPageResult" => array("varType" => "string")
                    ),
                    "httpauth" => "Y"
                ),

                "ScanPage" => array(
                    "type" => "public",
                    "input" => array(
                        "UserRFID" => array("varType" => "string"),
                        "FileName" => array("varType" => "string"),
                        "Content" => array("varType" => "base64Binary"),
                    ),
                    "httpauth" => "Y"
                ),
                */

            )
        );

        return $wsdesc;
    }
}

$arParams["WEBSERVICE_NAME"] = "bitrix.webservice.pss";
$arParams["WEBSERVICE_CLASS"] = "pssWS";
$arParams["WEBSERVICE_MODULE"] = "";

// передаем в компонент описание веб-сервиса
$APPLICATION->IncludeComponent(
    "bitrix:webservice.server",
    "",
    $arParams
);
die;
?>