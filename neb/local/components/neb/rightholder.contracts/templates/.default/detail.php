<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

$arContract = \RightHolder::getFirst($arResult['CONTRACTS']);

?>
<table cellspacing="0" cellpadding="0" border="1">
    <tr>
        <td>ID</td>
        <td><?=$arContract['ID']?></td>
    </tr>
    <? if (!empty($arContract['UF_NUMBER'])): ?>
        <tr>
            <td>Номер</td>
            <td><?=$arContract['UF_NUMBER']?></td>
        </tr>
    <? endif; ?>
    <tr>
        <td>Дата заключения</td>
        <td><?=$arContract['UF_DATE_FROM']?></td>
    </tr>
    <? if (!empty($arContract['UF_DATE_TO'])): ?>
        <tr>
            <td>Дата окончания</td>
            <td><?=$arContract['UF_DATE_TO']?></td>
        </tr>
    <? endif; ?>
    <tr>
        <td>Статус</td>
        <td><?=$arContract['STATUS']['VALUE']?></td>
    </tr>
    <tr>
        <td>Тип</td>
        <td><?=$arResult['ENUMS']['UF_TYPE'][$arContract['UF_TYPE']]['VALUE']?></td>
    </tr>
    <? if (!empty($arContract['UF_LEGAL_DOCUMENTS'])): ?>
        <tr>
            <td>Правоустанавливающие документы</td>
            <td>
                <? foreach ($arContract['UF_LEGAL_DOCUMENTS'] as $arFile):
                    $arFile = $arResult['FILES'][$arFile];
                    if (empty($arFile))
                        continue;
                    ?>
                    <a href="<?=$arFile['PATH']?>"><?=$arFile['ORIGINAL_NAME']?> (<?=$arFile['FORMAT_SIZE']?>)</a><br />
                <? endforeach; ?>
            </td>
        </tr>
    <? endif; ?>
    <? if (!empty($arContract['UF_COMMENT'])): ?>
        <tr>
            <td>Комментарий</td>
            <td><?=$arContract['UF_COMMENT']?></td>
        </tr>
    <? endif; ?>
</table>

<? if (!empty($arResult['BOOKS'])): ?>
    <br /><br />Книги:
    <table cellspacing="0" cellpadding="0" border="1">
        <tr>
            <th>Идентификатор</th>
            <th>Название</th>
            <th>Статус</th>
        </tr>
        <? foreach ($arResult['BOOKS'] as $sBook => $arBook): ?>
            <tr>
                <td><?=$sBook?></td>
                <td>
                    <?=$arBook['viewerOptions']['author']?><br />
                    <?=$arBook['viewerOptions']['title']?>
                </td>
                <td><?=$arBook['STATUS']?></td>
            </tr>
        <? endforeach; ?>
    </table>
<? endif; ?>

<? if (!empty($arResult['HISTORY'])): ?>
    <br /><br />
    История состояния договора:
    <table cellspacing="0" cellpadding="0" border="1">
        <tr>
            <td>Дата изменения</td>
            <td>Статус</td>
            <td>Комментарий</td>
        </tr>
        <? foreach ($arResult['HISTORY'][$arContract['ID']] as $arHistory): ?>
            <tr>
                <td><?=$arHistory['UF_DATE']?></td>
                <td><?=$arHistory['UF_STATUS']?></td>
                <td><?=$arHistory['UF_COMMENT']?></td>
            </tr>
        <? endforeach; ?>
    </table>
<? endif; ?>

<br /><br />

<? if ($arResult['ACTIONS']['PROCESSED']): // отправить на согласование ?>
    <form class="statusForm" action="?" method="GET">
        <div class="errorAction"></div>
        <div class="successAction"></div>
        <div class="formInfo">
            <input type="hidden" name="json" value="Y" />
            <input type="hidden" name="action" value="processed" />
            <input type="hidden" name="contract" value="<?=$arContract['ID']?>" />
            <input type="input" placeholder="Комментарий" name="comment" value="" style="width: 300px;" />
            <input type="submit" value="Отправить на подписание" />
        </div>
    </form>
<? endif; ?>

<? if ($arResult['ACTIONS']['DRAFT']): // вернуть на доработку ?>

    <form class="statusForm" action="?" method="GET">
        <div class="errorAction"></div>
        <div class="successAction"></div>
        <div class="formInfo">
            <input type="hidden" name="json" value="Y" />
            <input type="hidden" name="action" value="draft" />
            <input type="hidden" name="contract" value="<?=$arContract['ID']?>" />
            <input type="input" placeholder="Комментарий" name="comment" value="" style="width: 300px;" /><br />
            <input type="submit" value="Вернуть на доработку" />
        </div>
    </form>
<? endif; ?>

<? if ($arResult['ACTIONS']['SIGN']): // подписать ?>
    <hr />
    <form class="statusForm" action="?" method="GET">
        <div class="errorAction"></div>
        <div class="successAction"></div>
        <div class="formInfo">
            <input type="hidden" name="json" value="Y" />
            <input type="hidden" name="action" value="signed" />
            <input type="hidden" name="contract" value="<?=$arContract['ID']?>" />

            Номер договора:<br />
            <input type="input" name="number" value="" style="width: 250px;" /><br /><br />

            Дата заключения договора:<br />
            <div class="form-group" style="width:200px;">
                <div class="input-group">
                    <input
                        type="text"
                        name="dateFrom"
                        id="dateFromInput"
                        size="10"
                        value="<?=htmlspecialchars($_REQUEST['dateFrom'] ? : $arResult['CONTRACT']['UF_DATE'])?>"
                        class="datepicker-from form-control"
                        data-masked="99.99.9999"
                    >
                    <span class="input-group-btn">
                        <a class="btn btn-default" onclick="BX.calendar({node: 'dateFromInput', field: 'dateFromInput',  form: '', bTime: false, value: ''});">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </a>
                    </span>
                </div>
            </div>

            Срок действия договора:<br />
            <div class="form-group" style="width:200px;">
                <div class="input-group">
                    <input
                        type="text"
                        name="dateTo"
                        id="dateToInput"
                        size="10"
                        value="<?=htmlspecialchars($_REQUEST['dateTo'] ? : $arResult['CONTRACT']['UF_DATE_TO'])?>"
                        class="datepicker-from form-control"
                        data-masked="99.99.9999"
                    >
                    <span class="input-group-btn">
                        <a class="btn btn-default" onclick="BX.calendar({node: 'dateToInput', field: 'dateToInput',  form: '', bTime: false, value: ''});">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </a>
                    </span>
                </div>
            </div>

            Комментарий:<br />
            <input type="input" name="comment" value="" style="width: 250px;" /><br /><br />
            <input type="submit" value="Подписать" />
        </div>
    </form>

<? endif; ?>

<script type="text/javascript" language="javascript">
    $(function() {
        $(".statusForm").submit(function() {
            var form = $(this);
            var data = form.serialize();

            $.ajax({
                type: $(form).attr('method'),
                url: $(form).attr('action'),
                data: data,
                success: function (data) {
                    if (data.success == true) {
                        $(form).hide(500);
                        $('.errorAction').hide();
                        $('.successAction').html(data.message);
                        $('.successAction').show();

                        if (data.redirect == true) {
                            setTimeout("document.location.href='<?=$arParams['SEF_FOLDER']?>'", 1000);
                        }
                    } else {
                        $('.successAction').hide();
                        $('.errorAction').html('');
                        data.error.forEach(function(item, val, arr) {
                            $('.errorAction').append(item + "<br />");
                        });
                        $('.errorAction').show();
                    }
                },
                error: function (xhr, str) {
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
            return false;
        });
    });
</script>