function getCropParams()
{
	var params = [];

	var $image = $('.img-container > img');
	DataRes = $image.cropper('getData', true);
	ImageDataRes = $image.cropper('getImageData', true);
	CropBoxDataRes = $image.cropper('getCropBoxData');

	params.push({Data: DataRes, ImageData: ImageDataRes, CropBoxData: CropBoxDataRes, AspectRatio: $('div.AspectRatio label.active').data('option'), PREVIEW_TEXT: $('.edit-caption #PREVIEW_TEXT').val(), ID: $('.container').attr('id')});
	return params;
}

function setCropParameters()
{
	var id = $('.container').attr('id');
	var paramsB64 = parent.$('.item.eid'+id).attr('rel');
	if(!paramsB64)
		return false;
	if(paramsB64.length <= 0)
		return false;

	var strParam = atob(paramsB64);
	var params = eval(strParam);

	var $image = $('.img-container > img');
	
	$image.cropper('setAspectRatio', params.AspectRatio);
	$('div.AspectRatio label').removeClass('active');
	$('div.AspectRatio label[data-option="'+params.AspectRatio+'"]').addClass('active');
	
	$image.cropper('setImageData', params.ImageData);
	$image.cropper('setCropBoxData', params.CropBoxData);
	
	$('.edit-caption #PREVIEW_TEXT').val(params.PREVIEW_TEXT)
	
}

$(function () {

	'use strict';

	var console = window.console || { log: function () {} },
	$alert = $('.docs-alert'),
	$message = $alert.find('.message'),
	showMessage = function (message, type) {
		$message.text(message);

		if (type) {
			$message.addClass(type);
		}

		$alert.fadeIn();

		setTimeout(function () {
			$alert.fadeOut();
			}, 3000);
	};

	// -------------------------------------------------------------------------

	(function () {
		var $image = $('.img-container > img'),
		$dataX = $('#dataX'),
		$dataY = $('#dataY'),
		$dataHeight = $('#dataHeight'),
		$dataWidth = $('#dataWidth'),
		$dataRotate = $('#dataRotate'),
		options = {
			aspectRatio: 16 / 9,
			preview: '.img-preview',
			crop: function (data) {
				$dataX.val(Math.round(data.x));
				$dataY.val(Math.round(data.y));
				$dataHeight.val(Math.round(data.height));
				$dataWidth.val(Math.round(data.width));
				$dataRotate.val(Math.round(data.rotate));
			}
		};

		$image.on({
			'build.cropper': function (e) {
				//console.log(e.type);
			},
			'built.cropper': function (e) {
				//console.log(e.type);
			}
		}).cropper(options);

		setTimeout("setCropParameters()", 200);

		// Methods
		$(document.body).on('click', '[data-method]', function () {
			var data = $(this).data(),
			$target,
			result;

			if (data.method) {
				data = $.extend({}, data); // Clone a new one

				if (typeof data.target !== 'undefined') {
					$target = $(data.target);

					if (typeof data.option === 'undefined') {
						try {
							data.option = JSON.parse($target.val());
						} catch (e) {
							//console.log(e.message);
						}
					}
				}

				result = $image.cropper(data.method, data.option);

				if (data.method === 'getDataURL') {
					$('#getDataURLModal').modal().find('.modal-body').html('<img src="' + result + '">');
				}

				if ($.isPlainObject(result) && $target) {
					try {
						$target.val(JSON.stringify(result));
					} catch (e) {
						//console.log(e.message);
					}
				}

			}
		}).on('keydown', function (e) {

			switch (e.which) {
				case 37:
					e.preventDefault();
					$image.cropper('move', -1, 0);
					break;

				case 38:
					e.preventDefault();
					$image.cropper('move', 0, -1);
					break;

				case 39:
					e.preventDefault();
					$image.cropper('move', 1, 0);
					break;

				case 40:
					e.preventDefault();
					$image.cropper('move', 0, 1);
					break;
			}

		});
		}());
});
