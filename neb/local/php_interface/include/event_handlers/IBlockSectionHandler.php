<?

use \Bitrix\Main\EventManager;
use \Neb\Main\Helper\FileHelper;

EventManager::getInstance()->addEventHandler("iblock", "OnAfterIBlockSectionAdd",     ["IBlockSectionHandler", "set"]);
EventManager::getInstance()->addEventHandler("iblock", "OnAfterIBlockSectionUpdate",  ["IBlockSectionHandler", "set"]);
EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockSectionDelete", ["IBlockSectionHandler", "set"]);
EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockSectionAdd",    ["IBlockSectionHandler", "convertImageExtension"]);
EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockSectionUpdate", ["IBlockSectionHandler", "convertImageExtension"]);

class IBlockSectionHandler
{
    /**
     * Подсчитываем колличество коллекций в библиотеке при операциях с коллекциями
     */
    public function set($arFields)
    {
        $SECTION_ID = 0;

        // ID раздела
        if (!empty($arFields) and is_array($arFields) and intval($arFields['ID']) > 0)
            $SECTION_ID = $arFields['ID'];
        elseif (!is_array($arFields) and intval($arFields) > 0)
            $SECTION_ID = $arFields;

        // Cекция коллекции?
        $arSection = CIBlockSection::GetByID($SECTION_ID)->Fetch();
        if ($arSection['IBLOCK_CODE'] == IBLOCK_CODE_COLLECTIONS) {

            // ID привязанной библиотеки
            $arFilter = Array('IBLOCK_ID' => $arSection['IBLOCK_ID'], 'ID' => $arSection['ID'], 'ACTIVE' => 'Y');
            $ar_result = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'IBLOCK_ID', 'UF_LIBRARY'))->Fetch();

            if (intval($ar_result['UF_LIBRARY']) > 0) {

                // Сколько коллекций в библиотеке?
                $arFilter = Array('IBLOCK_ID' => $arSection['IBLOCK_ID'], 'ACTIVE' => 'Y', 'UF_LIBRARY' => $ar_result['UF_LIBRARY']);
                $cnt = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'IBLOCK_ID'))->SelectedRowsCount();

                //Запишем
                CIBlockElement::SetPropertyValuesEx($ar_result['UF_LIBRARY'], false, array('COLLECTIONS' => $cnt));
            }
        }
    }

    /**
     * Преобразует mime-тип изображения в расширение файла
     *
     * @param $arFields
     */
    public function convertImageExtension(&$arFields) {

        // обход стандартных полей и пользовательских свойтсв
        foreach ($arFields as &$arField) {
            if (is_array($arField) && !empty($arField['tmp_name'])) {
                $arField = FileHelper::convertTempImageFileExtension($arField);
            }
        }
    }
}