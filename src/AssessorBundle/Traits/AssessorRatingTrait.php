<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 14:35
 */

namespace AssessorBundle\Traits;


use AssessorBundle\Controller\Api\RatingApiInterface;
use AssessorBundle\Entity\Repository\CommonRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;


trait AssessorRatingTrait
{
    /**
     * @DI\Inject("doctrine")
     * @var Registry
     */
    public $doctrine;

    /**
     * @var QueryBuilder[]
     */
    private $_listQueryBuilder = [];

    /**
     * @DI\Inject("request")
     * @var Request
     */
    public $request;


    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->doctrine->getManager('neb.library');
    }

    /**
     * @param $entityName
     *
     * @return CommonRepository
     */
    public function getRepository($entityName)
    {
        return $this
            ->getEntityManager()
            ->getRepository($entityName);
    }

    /**
     * @param string $entityName
     *
     * @return null
     */
    public function getEntityCode($entityName)
    {
        $codes = [
            'AssessorBundle:TblQueryFrequency'    => RatingApiInterface::ENTITY_QUERY_FREQUENCY,
            'AssessorBundle:TblAuthorRating'      => RatingApiInterface::ENTITY_AUTHOR,
            'AssessorBundle:TblBookRating'        => RatingApiInterface::ENTITY_EDITIONS,
            'AssessorBundle:NebQuerySubstitution' => RatingApiInterface::ENTITY_SUBSTITUTION,
        ];

        return isset($codes[$entityName]) ? $codes[$entityName] : null;
    }


    /**
     * @param string $entityName
     * @param int    $limit
     * @param int    $offset
     * @param array  $order
     * @param array  $condition
     *
     * @return array
     */
    protected function _entities($entityName, $limit, $offset, $order = [], $condition = [])
    {
        $qb = $this->_getListQueryBuilder($entityName);
        $qb->setMaxResults($limit);
        $qb->setFirstResult($offset);
        foreach ($order as $field => $direction) {
            $qb->addOrderBy('t.' . $field, $direction);
        }
        foreach ($condition as $field => $value) {
            $qb->andWhere("t. $field = :$field");
            $qb->setParameter($field, $value);
        }
        $entities = $qb->getQuery()->getResult();

        return [
            'entities' => $entities,
        ];
    }

    /**
     * @param string $entityName
     *
     * @return QueryBuilder
     */
    protected function _getListQueryBuilder($entityName)
    {
        if (!isset($this->_listQueryBuilder[$entityName])) {
            $this->_listQueryBuilder[$entityName] = $this
                ->getEntityManager()
                ->getRepository($entityName)
                ->createQueryBuilder('t');
        }

        return $this->_listQueryBuilder[$entityName];
    }


}