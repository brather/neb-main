<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title></title>
	
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width"/>
	<link rel="icon" href="favicon.ico" type="image/x-icon" />

	<script src="js/libs/modernizr.min.js"></script>

	<link rel="stylesheet" href="css/style.css">
	
	<script src="js/libs/jquery.min.js"></script>
	
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>	
	<script>window.jQuery.ui || document.write('<script src="js/libs/jquery.ui.min.js">\x3C/script>')</script>
	<script src="js/plugins.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/blind.js"></script>
	<script src="js/script.js"></script>
</head>
<body class="">

	<div class="js_scroll">
	<div class="b_digitizing_popup_content">

	<form method="get" action="digitizing_step_2.php" class="searchform" >
		<div class="b-portalinfo clearfix wrapper">
			
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					
					<div class="b_search_set clearfix">
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>


		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<input type="submit" class="formbutton" value="Принять">
				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>		

			<div class="b-digitizing_mass_action">
				<a href="digitizing_step_3.php" class=""><span class="plus_ico"></span>
				<span>Добавить</span></a> все книги в план на оцифровку
			</div>

			<div class="b-add_digital js_digital">
				<table class="b-usertable tsize">
					<tbody><tr>
						<th class="autor_cell"><a href="/profile/plan_digitization/?by=document_authorsort&amp;order=asc#nav_start" class="">Автор</a></th>
						<th class="namedig_cell"><a href="/profile/plan_digitization/?by=document_titlesort&amp;order=asc#nav_start" class="">Название / Описание / Есть ли в планах на оцифровку</a></th>
						<th class=""><a href="/profile/plan_digitization/?by1=UF_DATE_ADD&amp;order1=asc#nav_start" class="sort up">Оцифровать <br>до даты</a></th>
						<th class=""><a>Комментарий</a></th>
						<th class="plan_cell"><a class="">Удалить <br>из плана </a></th>
					</tr>
					<tr class="search-result" id="RU_RGDB_BIBL_0000335099">
						<td class="pl15">Я. Шур</td>
						<td class="pl15">От костров до радио
							<div class="b-digital_act">
								<a href="#" class="b-digital_desc">Описание</a>
							</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Я. Шур</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">От костров до радио</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Выходные данные: </span><span class="iblock val">1942 г.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">88 с. с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная детская библиотека (РГДБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					<tr class="search-result" id="RU_RGDB_BIBL_0000334774">
						<td class="pl15">Я. Мексин</td>
						<td class="pl15">Стройка
							<div class="b-digital_act">
								<a href="#" class="b-digital_desc">Описание</a>
							</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Я. Мексин</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">Стройка</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Выходные данные: </span><span class="iblock val">1930 г.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">15 с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная детская библиотека (РГДБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					<tr class="search-result" id="RU_RGDB_BIBL_0000339839">
						<td class="pl15">Ян Черный</td>
						<td class="pl15">Рядовой Юрий Гаек
							<div class="b-digital_act">
								<a href="#" class="b-digital_desc">Описание</a>
							</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Ян Черный</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">Рядовой Юрий Гаек</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Выходные данные: </span><span class="iblock val">1931 г.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">128 с. с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная детская библиотека (РГДБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					<tr class="search-result" id="RU_RGDB_BIBL_0000340639">
						<td class="pl15">Я. Мексин</td>
						<td class="pl15">Стройка
							<div class="b-digital_act">
								<a href="#" class="b-digital_desc">Описание</a>
							</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Я. Мексин</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">Стройка</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Выходные данные: </span><span class="iblock val">1926 г.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">32 с. с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная детская библиотека (РГДБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					<tr class="search-result" id="RU_RGDB_BIBL_0000339690">
						<td class="pl15">Януш Корчак</td>
						<td class="pl15">Слава
							<div class="b-digital_act">
								<a href="#" class="b-digital_desc">Описание</a>
							</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Януш Корчак</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">Слава</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Выходные данные: </span><span class="iblock val">1918 г.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">38 с. с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная детская библиотека (РГДБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					<tr class="search-result" id="RU_RGDB_BIBL_0000340134">
						<td class="pl15">Я. Мексин</td>
						<td class="pl15">Переполох
							<div class="b-digital_act">
								<a href="#" class="b-digital_desc">Описание</a>
							</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Я. Мексин</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">Переполох</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Выходные данные: </span><span class="iblock val">1926 г.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">20 с. с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная детская библиотека (РГДБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					<tr class="search-result" id="RU_RGDB_BIBL_0000354673">
						<td class="pl15">Яхонтов</td>
						<td class="pl15">Вася носильщик
							<div class="b-digital_act">
								<a href="#" class="b-digital_desc">Описание</a>
							</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Яхонтов</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">Вася носильщик</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">15 с. с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная детская библиотека (РГДБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					<tr class="search-result" id="RU_RGDB_BIBL_0000359959">
						<td class="pl15">Я. М. Родде</td>
						<td class="pl15">Разныя истории и нравоучения, выбранныя в пользу обучающагося юношества российскому языку								<div class="b-digital_act">
							<a href="#" class="b-digital_desc">Описание</a>
						</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Я. М. Родде</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">Разныя истории и нравоучения, выбранныя в пользу обучающагося юношества российскому языку</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Выходные данные: </span><span class="iblock val">1789 г.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">128 с. с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная детская библиотека (РГДБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					<tr class="search-result" id="000199_000018_01003375855">
						<td class="pl15">Яшин, Владимир Николаевич </td>
						<td class="pl15">Информатика: аппаратные средства персонального компьютера : учебное пособие для студентов высших учебных заведений, обучающихся по специальности "Прикладная информатика (по областям)" и другим специальностям В. Н. Яшин 								<div class="b-digital_act">
							<a href="#" class="b-digital_desc">Описание</a>
						</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Яшин, Владимир Николаевич </span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">Информатика: аппаратные средства персонального компьютера : учебное пособие для студентов высших учебных заведений, обучающихся по специальности "Прикладная информатика (по областям)" и другим специальностям В. Н. Яшин </span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">252, [1] с. ил., табл. 22 см  с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная библиотека (РГБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					<tr class="search-result" id="000199_000018_01004265624">
						<td class="pl15">Ящура, Александр Игнатьевич </td>
						<td class="pl15">Система технического обслуживания и ремонта промышленных зданий и сооружений : справочник А. И. Ящура 								<div class="b-digital_act">
							<a href="#" class="b-digital_desc">Описание</a>
						</div>
					</td>
					<td class="pl15">19.12.2014</td>
					<td class="pl15"></td>
					<td>
						<div class="rel plusico_wrap">
							<div class="plus_ico"></div>
							<div class="b-hint rel"><span>Добавить</span> в План оцифровки</div>
						</div>
					</td>
				</tr>
				<tr class="scrolled">
					<td colspan="4">
						<div class="b-infobox rel b-infoboxdescr" data-link="descr">
							<a href="#" class="close"></a>
							
							<div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val">Ящура, Александр Игнатьевич </span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val">Система технического обслуживания и ремонта промышленных зданий и сооружений : справочник А. И. Ящура </span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Физическое описание: </span><span class="iblock val">308, [1] с. табл. 21 см  с.</span></div>
							<div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val">Российская государственная библиотека (РГБ)</span></div>
							
							</div><!-- /b-infobox -->
							
							<!-- /b-infobox -->
						</td>
					</tr>
					
				</tbody></table>
				</div>








				<div class="b-paging">
					<div class="b-paging_cnt"><a href="" onclick="return false;" class="b-paging_prev iblock"></a><a target="_parent" href="#" onclick="return false;" class="b-paging_num current iblock">1</a><a target="_parent" class="b-paging_num iblock" href="/profile/plan_digitization/?by1=UF_DATE_ADD&amp;order1=desc&amp;PAGEN_1=2">2</a><a target="_parent" class="b-paging_next iblock" href="/profile/plan_digitization/?by1=UF_DATE_ADD&amp;order1=desc&amp;PAGEN_1=2"></a></div>
				</div>





	</div>
	</div>
</body>
</html>