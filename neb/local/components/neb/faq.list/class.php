<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Iblock\SectionTable,
    \Bitrix\Iblock\ElementTable,
    \Bitrix\NotaExt\Iblock\IblockTools;

class FaqGetList extends CBitrixComponent
{
    private $page = 'template';
    private $iblockId = '';
    private $feedbackPage = '';

    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang()
    {
        Loc::loadMessages(__FILE__);
    }

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        if(!empty($arParams['IBLOCK_CODE'])) {
            $this->iblockId = IblockTools::getIBlockId($arParams["IBLOCK_CODE"]);
        }
        if (!empty($arParams["URL_FEEDBACK_PAGE"])) {
            $this->feedbackPage = $arParams["URL_FEEDBACK_PAGE"];
        }
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Выполнение компонента
     */
    public function executeComponent()
    {
        $this->_includeModules();
        $this->_setTitleForPage();
        $this->_setDescriptionForPage();
        $this->_setUriFeedback();
        $this->_setSectionInArrResult();
        $this->includeComponentTemplate($this->page);
    }

    /** Подключение модулей
     * @throws \Bitrix\Main\LoaderException
     */
    private function _includeModules()
    {
        Loader::IncludeModule('iblock');
    }

    /**
     * Возвращаем список разделов инфоблока
     */
    private function _getListSection()
    {
        $resSection = SectionTable::GetList(array(
            "filter" => array("IBLOCK_ID" => $this->iblockId),
            "select" => array("ID", "NAME", "ACTIVE"),
            "order" => array("SORT" => "ASC", 'DATE_CREATE' => 'ASC')
        ));
        $arResult = array();
        while ($obSection = $resSection->Fetch()) {
            $arResult[$obSection["ID"]] = array(
                "ID" => $obSection["ID"],
                "SECTION_NAME" => $obSection["NAME"],
                "ACTIVE" => $obSection["ACTIVE"],
            );
        }

        return $arResult;
    }

    /**
     * Возвращаем список элементов инфоблока
     */
    private function _getListElements()
    {
        $rsItems = ElementTable::GetList(array(
            "filter" => array("IBLOCK_ID" => $this->iblockId),
            "order" => array("SORT" => "ASC", 'DATE_CREATE' => 'ASC'),
            "select" => array("ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_TEXT", "ACTIVE")
        ));
        $arResult = array();
        while ($obItems = $rsItems->fetch()) {
            $arResult[] = array(
                "ID" => $obItems["ID"],
                "NAME" => $obItems["NAME"],
                "ACTIVE" => $obItems["ACTIVE"],
                "IBLOCK_SECTION_ID" => $obItems["IBLOCK_SECTION_ID"],
                "DETAIL_TEXT" => $obItems["DETAIL_TEXT"]
            );
        }
        return $arResult;
    }

    /**
     * Заполняем результирующий массив разделами и элементами
     */
    private function _setSectionInArrResult()
    {
        $this->arResult["SECTION_ITEMS"] = $this->_getListSection();
        $arItems = $this->_getListElements();
        foreach ($arItems as $iItem => $arItem) {
            $this->arResult["SECTION_ITEMS"][$arItem["IBLOCK_SECTION_ID"]]["ITEMS"][] = $arItem;
        }
    }

    /**
     * Возвращает заголовок из заданных параметров
     */
    private function _setTitleForPage()
    {
        if (!empty($this->arParams["PAGE_TITLE"])) {
            $this->arResult["PAGE_TITLE"] = $this->arParams["PAGE_TITLE"];
        }
        $this->arResult["SHOW_TITLE"] = $this->arParams["SHOW_TITLE"];
    }

    /**
     * Возвращает описание страницы из заданных параметров
     */
    private function _setDescriptionForPage()
    {
        if (!empty($this->arParams["PAGE_DESCRIPTION"])) {
            $this->arResult["PAGE_DESCRIPTION"] = $this->arParams["PAGE_DESCRIPTION"];
        }
    }

    /**
     * Возвращает текст со ссылкой на страницу обратной связи
     */
    private function _setUriFeedback()
    {
        if (!empty($this->arParams["URL_FEEDBACK_PAGE"])) {
            $this->arResult["URL_FEEDBACK_PAGE"] .= Loc::getMessage("DESCRIPTION_WITH_URL_FIRST")
                . $this->arParams["NAME_OF_PROJECT"]
                . Loc::getMessage("DESCRIPTION_WITH_URL_SECOND")
                . '<a href="' . $this->arParams["URL_FEEDBACK_PAGE"] . '">'
                . Loc::getMessage("DESCRIPTION_WITH_URL_THIRD") . '</a>';
        }
    }
}