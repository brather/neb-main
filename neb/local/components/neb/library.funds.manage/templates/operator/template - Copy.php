<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;
?>

<div class="b-addsearchmode clearfix">
    <!--<a class="button_blue btadd left popup_opener ajax_opener closein" href="<?=$APPLICATION->GetCurPageParam('action=editBookForm&' . bitrix_sessid_get(), array('action'))?>" data-popupsize="high" data-overlaybg="dark" data-width="632" >Добавить книгу</a>-->
    <div class="b-search_field right">
		<form method="get">
	        <input type="text" name="qbook" class="b-search_fieldtb b-text" id="asearch" value="<?=htmlspecialcharsbx($_REQUEST['qbook'])?>" autocomplete="off" data-src="search.php?session=MY_SESSION&moreparams=MORE_PARAMS">
	        <!--<select name="qbooktype" id=""class="js_select b_searchopt">
	            <option <?/*=$_REQUEST['qbooktype'] == 'all' ? 'selected="selected"' : ''*/?> value="all">по всем полям</option>
	            <option <?/*=$_REQUEST['qbooktype'] == 'Name' ? 'selected="selected"' : ''*/?> value="Name">по названию</option>
	            <option <?/*=$_REQUEST['qbooktype'] == 'Author' ? 'selected="selected"' : ''*/?> value="Author">по автору</option>
	        </select>-->
	        <input type="submit" class="b-search_bth bbox" value="Найти">
		</form>

    </div><!-- /.b-search_field-->
</div>

<div class="b-fondtable">

    <table class="b-usertable b-fondtb b-fondved">
        <tr>
            <th class="autor_cell"><a <?=SortingExalead("Author")?>>Автор</a></th>
            <th class="title_cell"><a <?=SortingExalead("Name")?>>Название</a></th>

            <th class="date_cell"><a <?=SortingExalead("CreationDateTime")?>>Дата <br> добавления</a></th>
            <th class="recycle_cell"><span class="b-recyclebin">Удалить <br> <span class="nowrap">с портала</span></span></th>

        </tr>
        <?foreach ($arResult['BOOKS'] as $book) {?>
            <tr>
                <td class="pl15"><?=$book['Author']?></td>
                <td class="pl15"><?=$book['Name']?></td>
                <td class="pl15"><?=$book['CreationDateTime']?></td>
                <td>
                    <div class="rel plusico_wrap minus">
                        <a onclick="return confirm('Вы уверены?')" href="<?=$APPLICATION->GetCurPageParam('action=deleteBook&bookId=' . $book['Id'] . '&' . bitrix_sessid_get(), array('action', 'bookId'))?>" class="js-vedeniefondov-deletebook plus_ico lib "></a>
                        <div class="b-hint rel"><span>Удалить</span> из Моей библиотеки</div>
                    </div>
                </td>
            </tr>
        <?}?>
    </table>
</div><!-- /.b-stats-->
