
<?
$pagename='';
?>
<? include("header_main_auth.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-searchresult noborder hidden">
		<ul class="b-profile_nav">
			<li>
				<a href="#" class="b-profile_navlk js_profilemenu current">личный кабинет</a>
				<ul class="b-profile_subnav">
					<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
					<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
				</ul>
			</li>
			<li><a class="b-profile_nav_lb" href="#">моя библиотека</a></li>
			<li><a href="#" class="b-profile_nav_qt">цитаты</a></li>
			<li><a href="#" class="b-profile_nav_bm ">закладки</a></li>
			<li><a href="#" class="b-profile_nav_notes">заметки</a></li>
			<li><a href="#" class="b-profile_nav_search">поисковые запросы</a></li>
		</ul>                 
	</div><!-- /.b-searchresult-->
	
	<div class="b-registration rel">
		
		<h2 class="mode">регистрация на портале</h2>
		<form action="www.notamedia.ru" class="b-form b-form_common b-regform">
			<p class="note">
				Полная регистрация возможна только после проверки введенных данных, приносим свои извинения за задержку в предоставлении доступа.
				<br/>Просим обратить особое внимание на заполнение анкеты корректными данными. 
				<br/>В случае выявления несоответствий Ваш доступ может быть заблокирован.
			</p>
			<p class="error_message">A user with this e-mail address (ann.dmi.krasnodar@gmail.com) already exists.</p>
			<div class="wrapfield">
				<div class="b-form_header"><span class="iblock">Паспортные данные</span></div>
				<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings01">Фамилия</label>
					<div class="field validate">
						<em class="error required">Заполните</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>	
						<input type="text" data-validate="fio" data-required="required" value="Гагарин" id="settings01" data-maxlength="30" data-minlength="2" name="sname" class="input">										
					</div>
				</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings02">Имя</label>
					<div class="field validate">
						<em class="error required">Заполните</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>	
						<input type="text" data-required="required" data-validate="fio" value="Юрий" id="settings02" data-maxlength="30" data-minlength="2" name="name" class="input" >										
					</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings22">Отчество</label>
						<div class="field validate">
							<em class="error required">Заполните</em>		
							<em class="error validate">Неверный формат</em>	
							<em class="error maxlength">Более 30 символов</em>	
							<input type="text" data-validate="fio" value="" id="settings22" data-maxlength="30" data-minlength="2"data-required="true"  name="name" class="input" >										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings06">Дата рождения</label>

						<div class="field validate iblock seldate" data-yearsrestrict="12">
							<em class="error dateformat">Неверный формат даты</em>
							<em class="error required">Заполните</em>
							<em class="error yearsrestrict">Вам должно быть<br>не меньше 12 лет</em>
							<input type="hidden" class="realseldate" data-required="true" value="" id="settings06" name="REGISTER[PERSONAL_BIRTHDAY]">
							<select name="birthday" id=""  class="js_select w102 sel_day"  data-required="true">
								<option value="-1">день</option>
								<option value="01">1</option>
								<option value="02">2</option>
							</select>
							
							<select name="birthmonth" id=""class="js_select w165 sel_month"  data-required="true" >
								<option value="-1">месяц</option>
								<option value="01">январь</option>
								<option value="02">февраль</option>
							</select>
							
							<select name="birthyear" id=""  class="js_select w102 sel_year"  data-required="true">
								<option value="-1">год</option>
								<option value="1980">1980</option>
								<option value="1981">1981</option>
							</select>
						</div>									

					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings01">Серия и номер паспорта</label>
						<div class="field validate">
							<em class="error required">Заполните</em>
							<div class="field iblock"><input type="text"  data-required="required" value="" id="settings01"  name="serial" class="input w110">	</div>									
							<div class="field iblock"><input type="text"  data-required="required" value="" id="settings02" name="number" class="input w190">	</div>									
						</div>
					</div>
				</div>
			</div>
			<div class="wrapfield">
				<div class="b-form_header"><span class="iblock">Сфера специализации</span></div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings10">Место работы/учебы</label>
						<div class="field validate">
							<em class="error required">Заполните</em>
							<input type="text" value="" id="settings10" data-maxlength="30" data-minlength="2" data-required="true" name="place" class="input" >										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings08">Отрасль знаний</label>
					<div class="field validate iblock">
						<em class="error required">Заполните</em>
						<select name="knowledge" id="settings08"  class="js_select w370"  data-required="true">
							<option value="">выберете</option>
							<option value="1">Информатика</option>
							<option value="2">Статистика</option>
						</select>
					</div>				
					</div> 
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings09">Образование. учёная степень</label>
					<div class="field validate iblock">
						<em class="error required">Заполните</em>
						<select name="education" id="settings09"  class="js_select w370"  data-required="true">
							<option value="">выберете</option>
							<option value="1">высшее</option>
							<option value="2">среднее</option>
						</select>
					</div>				
					</div> 
				</div>
			</div>

			
			<div class="b-form_header">Вход на сайт</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings04">Электронная почта </label>
					<div class="field validate">
						<em class="error required">Заполните</em>						
						<em class="error validate">Некорректный e-mail</em>
						<input type="text" class="input" name="num" data-minlength="2" data-maxlength="30" id="settings04" data-validate="email" value="" data-required="true">									
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<div class="passecurity">
						<div class="passecurity_lb">Защищенность пароля</div>

					</div>
					<label for="settings05"><span>не менее 6 символов</span>Пароль</label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="true" value="" id="settings05" data-maxlength="30" data-minlength="6" name="pass" class="pass_status input">										
						<em class="error">Заполните</em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings55">Подтвердить пароль</label>
					<div class="field validate">
						<em class="error validate">Пароль не совпадает с введенным ранее</em>
						<input data-identity="#settings05" type="password" data-required="true" value="" id="settings55" data-maxlength="30" data-minlength="2" name="pass" class="input" data-validate="password">								
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings11">Мобильный телефон</label>
					<div class="field validate">
						<input type="text" class="input maskphone" name="phone"  id="settings11"  value="">	
						<span class="phone_note">для смс-уведомлений о статусе Ваших заявок  на сайте РГБ</span>
					</div>
				</div>
			</div>
			<div class="wrapfield">
				<div class="b-form_header"><span class="iblock">Адрес регистрации</span></div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings03">Индекс <span class="minscreen">регистрации</span></label>
					<div class="field validate w140">
						<em class="error required">Заполните</em>
						<em class="error number">Некорректный почтовый индекс</em>
						<input type="text" data-validate="number" data-required="required" value="" id="settings03" name="index" class="input ">					
					</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings033">Область / Регион <span class="minscreen">регистрации</span></label>
					<div class="field validate">
						<em class="error required">Заполните</em>
						<input type="text" data-required="required" value="" id="settings033" data-maxlength="30" data-minlength="2" name="obl" class="input" >										
					</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings05">Город / Населенный пункт <span class="minscreen">регистрации</span></label>
					<div class="field validate">
						<em class="error required">Заполните</em>
						<input type="text" data-required="required" value="" id="settings05" data-maxlength="30" data-minlength="2" name="city" class="input" >										
					</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings08">Улица <span class="minscreen">регистрации</span></label>
						<div class="field validate">
							<input type="text" data-required="required"  value="" id="settings08" data-maxlength="30" data-minlength="2" name="street" class="input">					
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<div class="field iblock fieldthree ">
						<label for="settings09">Дом:</label>
						<div class="field validate">
							<input type="text" class="input" name="house" data-maxlength="20" id="settings09" value=""  data-required="true"/>
							<em class="error required">Заполните</em>
						</div>
					</div>
					<div class="field iblock fieldthree">
						<label for="settings10">Строение: </label>
						<div class="field">
							<input type="text" class="input" name="flat" data-maxlength="20" maxlength="4" id="settings10" value="" />
						</div>
					</div>
					<div class="field iblock fieldthree">
						<label for="settings11">Квартира: </label>
						<div class="field">
							<input type="text" class="input" name="flat" data-maxlength="20" maxlength="4" id="settings11" value="" />
						</div>
					</div>
				</div>
				</div>
			</div>
			<div class="wrapfield">
				<div class="b-form_header">
				<span class="iblock">Адрес проживания</span>
				<div class="checkwrapper">
					<input class="checkbox addrindently" type="addr" name="addr" id="cb3"><label for="cb3" class="black">по месту регистрации</label>
				</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings04">Индекс <span class="minscreen">проживания</span></label>
						<div class="field validate w140">
							<input type="text" data-validate="number" value="" id="settings04" name="index" class="input">					
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings034">Область / Регион <span class="minscreen">регистрации</span></label>
					<div class="field validate">
						<em class="error required">Заполните</em>
						<input type="text" data-required="required" value="" id="settings034" data-maxlength="30" data-minlength="2" name="obl" class="input" >										
					</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings06">Город / Населенный пункт<span class="minscreen">проживания</span></label>
						<div class="field validate">
							<input type="text" value="" id="settings06" data-maxlength="30" data-minlength="2" name="city" class="input" >										
						</div>
					</div> 
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings08">Улица <span class="minscreen">проживания</span></label>
						<div class="field validate">
							<input type="text" value="" id="settings08" data-maxlength="30" data-minlength="2" name="street" class="input">					
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<div class="field iblock fieldthree ">
							<label for="settings09">Дом:</label>
							<div class="field validate">
								<input type="text" class="input" name="house" data-maxlength="20" id="settings09" value=""/>
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings10">Строение: </label>
							<div class="field">
								<input type="text" class="input" name="flat" data-maxlength="20" maxlength="4" id="settings10" value="" />
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings11">Квартира: </label>
							<div class="field">
								<input type="text" class="input" name="flat" data-maxlength="20" maxlength="4" id="settings11" value="" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="fieldcell nowrap photorow">
				<label>Загрузить скан разворота паспорта с фотографией </label>
				<div class="field validate">
					<div class="setscan iblock">
						<input type="file" class="photofile" name="" id="" data-required="true">
						<a href="#">Загрузить изображение</a>
						<div class="setphoto_lb">или перетащите его на это поле</div>
						<div class="progressbar"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружается <span class="num">33</span>%</div></div>
					</div>
					
					<div class="b-scan_photo iblock">
						<img src="./pic/scan.jpg" alt="">
					</div>
					<em class="error required">Загрузите скан паспорта</em>
				</div>
			</div>
			<div class="fieldcell nowrap photorow">
				<label>Загрузить скан разворота паспорта с  пропиской </label>
				<div class="field validate">
					<div class="setscan iblock">
						
						<input type="file" class="photofile" name="" id="" data-required="true">
						<a href="#">Загрузить изображение</a>
						<div class="setphoto_lb">или перетащите его на это поле</div>
						<div class="progressbar"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружается <span class="num">33</span>%</div></div>
					</div>
					<div class="b-scan_photo iblock">
						<img src="./pic/scan.jpg" alt="">
					</div>
					<em class="error required">Загрузите скан паспорта</em>
				</div>
			</div>
			<hr>
			<div class="checkwrapper ">
				<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb3"><label for="cb3" class="black">Согласен с </label><a href="#">условиями использования</a> <label for="cb3" class="black">портала НЭБ</label>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix">
						<button disabled="disabled" type="submit" value="1" class="formbutton btdisable">Зарегистрироваться</button>

					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->



</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>