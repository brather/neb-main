<?php
/**
 * User: agolodkov
 * Date: 10.11.2016
 * Time: 18:36
 */

namespace AppBundle\Controller\Api;

use BitrixBundle\Annotations\IncludeBitrix;
use ElarUtilsBundle\DependencyInjection\MustacheHelper;
use FOS\RestBundle\View\View;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * Class ApiWorkplacesController
 *
 * @DI\Service("neb.api.collection", scope="request")
 */
class ApiCollectionController
{

    /**
     * @DI\Inject("util.mustache.helper")
     * @var MustacheHelper
     */
    public $mustacheHelper;

    /**
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @param int $id
     *
     * @return array
     */
    public function getCollectionData($id)
    {
        $arResult = [];
        $section = new \CIBlockSection();
        $arCollection = $section->GetList(
            [],
            [
                'IBLOCK_ID'  => IBLOCK_ID_COLLECTION,
                'ID'         => $id,
                'CNT_ACTIVE' => 'Y',
            ],
            true,
            [
                'ID',
                'NAME',
                'DESCRIPTION',
                'DETAIL_PICTURE',
                'TIMESTAMP_X',
                'ELEMENT_CNT',
                'UF_LIBRARY',
            ]
        )->Fetch();
        
        if (!empty($arCollection)) {
            $arElement = new \CIBlockElement();
            $arLibrary = $arElement->GetByID($arCollection['UF_LIBRARY'])->Fetch();
            if (!empty($arLibrary)) {
                $arLibrary = [
                    'id'   => $arLibrary['ID'],
                    'name' => $arLibrary['NAME'],
                    'url'  => $arLibrary['DETAIL_PAGE_URL'],
                ];
            }
            $sDomain = 'http://' . $_SERVER['SERVER_NAME'];
            $sDescription = str_replace('src="/upload', 'src="' . $sDomain . '/upload', $arCollection['DESCRIPTION']);
            $arImage = \CFile::GetFileArray($arCollection['DETAIL_PICTURE']);

            $arResult['dateUpdate']  = $arCollection['TIMESTAMP_X'];
            $arResult['library']     = $arLibrary;
            $arResult['booksCount']  = intval($arCollection['ELEMENT_CNT']);
            $arResult['description'] = $this->mustacheHelper->render(
                'collection/detail-html',
                [
                    'name'        => $arCollection['NAME'],
                    'image'       => !empty($arImage['SRC']) ? $sDomain . $arImage['SRC'] : '',
                    'description' => $sDescription,
                ]
            );

        }

        return View::create($arResult);
    }

}