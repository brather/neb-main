<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
global $APPLICATION;
$APPLICATION->SetTitle(GetMessage('FORM_FEEDBACK_OK_REQUEST_ACCEPTED'));
?>
<p><?=GetMessage('FORM_FEEDBACK_OK_EMAIL_SENT');?></p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>