<?php
/**
 * User: agolodkov
 * Date: 12.07.2016
 * Time: 10:52
 */

namespace AppBundle\Command;


use LbcBundle\Entity\AbstractLbcCategory;
use LbcBundle\Entity\Traits\CategoryCodeTrait;
use LbcBundle\Traits\LbcMigrationsTrait;
use Neb\Main\Stat\Tracker;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PrepareLbcAfterMigration extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('lbc:prepare-after-migration')
            ->setDescription('Modify data');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);
        ini_set('memory_limit', '1024M');

        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository('LbcBundle:LbcMaindivision');
        $limit = 100;
        $offset = 0;
        $total = 0;
        while (!empty($items = $repository->findBy([], null, $limit, $offset))) {
            /** @var CategoryCodeTrait $item */
            foreach ($items as $item) {
                $item->prepareSortCode();
                $em->merge($item);
                $total++;
            }
            $em->flush();
            $em->clear();
            $offset += $limit;
            echo "Prepared entity: {$total}\r";
        }
        echo PHP_EOL;

        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}