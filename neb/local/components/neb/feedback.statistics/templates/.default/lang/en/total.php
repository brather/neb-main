<?php
$MESS['FEEDBACK_STAT_TITLE']      = 'Statistics by orders count';
$MESS['FEEDBACK_STAT_DATE_FROM']  = 'Date from';
$MESS['FEEDBACK_STAT_DATE_TO']    = 'Date to';
$MESS['FEEDBACK_STAT_PERIOD']     = 'Statistics by period';
$MESS['FEEDBACK_STAT_REGISTERED'] = 'Registered order';
$MESS['FEEDBACK_STAT_CLOSED']     = 'Closed orders';
$MESS['FEEDBACK_STAT_PROCESSED']  = 'Processed order';
$MESS['FEEDBACK_STAT_HISTORY']    = 'beginning statistics from 22.08.2016';
$MESS['FEEDBACK_STAT_EXCEL']      = 'Export to .xls';
$MESS['FEEDBACK_STAT_SHOW']       = 'Show statistics';