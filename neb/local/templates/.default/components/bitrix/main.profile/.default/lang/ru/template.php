<?
$MESS ['CHANGE_PASSW'] = "Смена пароля";
$MESS ['NEW_PASS'] = "Новый пароль";
$MESS ['PASS_TWO'] = "Подтвердить новый пароль";
$MESS ['CONFIRM'] = "Подтвердить";
$MESS ['GENERAL_CONST'] = "основные настройки";
$MESS ['NAME'] = "Имя";
$MESS ['LAST_NAME'] = "Фамилия";
$MESS ['SECOND_NAME'] = "Отчество";
$MESS ['MEN'] = "мужской";
$MESS ['WOMEN'] = "женский";
$MESS ['DATE'] = "Дата рождения";
$MESS ['DAY'] = "день";
$MESS ['MONTH'] = "месяц";
$MESS ['YEAR'] = "год";
$MESS ['ABOUT'] = "Немного о себе";
$MESS ['SETTINGS'] = "настройки поиска";
$MESS ['COUNT_SEARCH'] = "Количество <br>результатов поиска";
$MESS ['MAX_COUNT'] = "Максимальное количество <br>документов по запросу";
$MESS ['SAVE'] = "Сохранить";
$MESS ['SETTING'] = "настройки профиля";
$MESS ['MAIL'] = "E-mail/ Логин/ ЕЭЧБ";
$MESS ['POL'] = "Пол";
$MESS ['ERROR_DATE_FORMAT'] = "Неверный формат даты";
$MESS ['CHOSE_DATE'] = "Заполните";
$MESS ['12_OLD'] = "Вам должно быть<br>не меньше 12 лет";
$MESS ['REQUIRED_PASS'] = "Поле обязательно для заполнения";
$MESS ['NOT_MATCH'] = "Пароль не совпадает с введенным ранее";

$MESS ['PROFILE_DATA_SAVED'] = "Указанные в анкете данные успешно сохранены";

$MESS["MAIN_REGISTER_LOGIN"] = 'Логин';
$MESS["MAIN_REGISTER_EMAIL"] = 'Электронная почта';
$MESS["MAIN_REGISTER_CITIZENSHIP"] = 'Гражданство';
$MESS["MAIN_REGISTER_REGION"] = 'Область / Регион';
$MESS["MAIN_REGISTER_AREA"] = 'Район';
$MESS["MAIN_REGISTER_SPECIALIZATION"] = 'Сфера специализации';
$MESS["MAIN_REGISTER_PLACE_OF_EMPLOYMENT"] = 'Место работы/учебы';
$MESS["MAIN_REGISTER_KNOWLEDGE"] = 'Отрасль знаний';
$MESS["MAIN_REGISTER_SELECT"] = 'выберите';
$MESS["MAIN_REGISTER_EDUCATION"] = 'Образование. Учёная степень';
$MESS["MAIN_REGISTER_REQ"] = 'Заполните';
$MESS["REGISTER_GROUP_ADDRESS_REGISTERED"] = "Адрес регистрации";
$MESS["REGISTER_GROUP_ADDRESS_RESIDENCE"] = "Адрес проживания";
$MESS["REGISTER_GROUP_ADDRESS_REG_NOTE"] = "регистрации";
$MESS["REGISTER_GROUP_ADDRESS_RES_NOTE"] = "проживания";
$MESS["REGISTER_GROUP_ADDRESS_CHECKBOX_NOTE"] = "по месту регистрации";
$MESS["REGISTER_FIELD_PERSONAL_STREET"] = "Улица";
$MESS["REGISTER_FIELD_PERSONAL_HOUSE"] = "Дом";
$MESS["REGISTER_FIELD_PERSONAL_BUILDING"] = "Строение";
$MESS["REGISTER_FIELD_PERSONAL_APPARTMENT"] = "Квартира";
$MESS["REGISTER_FIELD_PERSONAL_MAILBOX"] = "Почтовый ящик";
$MESS["REGISTER_FIELD_PERSONAL_CITY"] = "Город / Населенный пункт";
$MESS["REGISTER_FIELD_PERSONAL_STATE"] = "Область / край";
$MESS["REGISTER_FIELD_PERSONAL_ZIP"] = "Почтовый индекс";
$MESS["REGISTER_FIELD_PERSONAL_ZIP_INCORRECT"] = "Некорректный почтовый индекс";
$MESS["REGISTER_FIELD_PERSONAL_COUNTRY"] = "Страна";
$MESS["REGISTER_FIELD_PERSONAL_MOBILE"] = "Мобильный телефон";
$MESS["MAIN_REGISTER_PASSPORT_NUMBER"] = "Серия и номер паспорта";
$MESS["PROPERTY_LIBRARY"] = "Настройки библиотеки";
$MESS["PROPERTY_LIBRARY_SCAN_COST"] = "Стоимость сканируемой страницы";

$MESS["MAIN_REGISTER_REQ"] = "Обязательное поле";
$MESS["MAIN_REGISTER_FILL"] = "Заполните";
$MESS["MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS"] = "Более {0} символов";
$MESS["MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS"] = "Менее {0} символов";
$MESS["MAIN_REGISTER_AGE_WITH_PLACEHOLDER"] = 'Вы должны быть старше {0} лет';
?>