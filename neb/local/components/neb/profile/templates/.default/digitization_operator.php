<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->IncludeComponent(
    'neb:digitization.plan',
    '',
    [
        'LIBRARY_ID'     => $_REQUEST['library'],
        'REQUEST_STATUS' => $_REQUEST['request_status'],
        'EXPORT_URL'     => '/profile/digitization-export/'
    ],
    $component
);
