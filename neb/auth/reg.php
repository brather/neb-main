<?php require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog.php');

use \Bitrix\Main\Application;
global $APPLICATION, $USER;

$rq = Application::getInstance()->getContext()->getRequest();

/** @var array $arResult */
$APPLICATION->IncludeComponent(
    'neb:user.register',
    'simple-registration',
    [
        'SUCCESS_TEMPLATE'  => 'success',
        'NEED_CONFIRM'      => 'Y',
        'LINK_REFERER'      => (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '/',
    ],
    false
);
if ($USER->IsAuthorized()) {
    $backUrl = '/';
    if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
        $backUrl = $_SERVER['HTTP_REFERER'];

    if (isset($_SESSION['AUTH_REFERER']) && !empty($_SESSION['AUTH_REFERER']))
        $backUrl = $_SESSION['AUTH_REFERER'];

    if (!empty($rq->get('_backUrl')))
        $backUrl = $rq->get('_backUrl');

    LocalRedirect($backUrl);
}
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog.php');