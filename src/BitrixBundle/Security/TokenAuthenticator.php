<?php
/**
 * User: agolodkov
 * Date: 16.08.2016
 * Time: 17:05
 */

namespace BitrixBundle\Security;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Http\AccessMap;

class TokenAuthenticator extends AbstractGuardAuthenticator
{

    /**
     * @var AccessMap
     */
    protected $_accessMap;

    /**
     * @param AccessMap $accessMap
     */
    public function __construct(AccessMap $accessMap)
    {
        $this->_accessMap = $accessMap;
    }

    /**
     * @inheritdoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, 401);
    }

    /**
     * @inheritdoc
     */
    public function getCredentials(Request $request)
    {
        if (!$token = $request->query->get('token')) {
            return null;
        }
        $patterns = $this->_accessMap->getPatterns($request);
        if (in_array('IS_AUTHENTICATED_ANONYMOUSLY', $patterns[0])) {
            return null;
        }

        return [
            'token' => $request->query->get('token'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var UserProvider $userProvider */
        $token = $credentials['token'];

        return $userProvider->loadUserByToken($token);
    }

    /**
     * @inheritdoc
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if (!isset($credentials['token']) || empty($credentials['token'])) {
            throw new BadCredentialsException('Empty token');
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, 403);
    }

    /**
     * @inheritdoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
    }

    /**
     * @inheritdoc
     */
    public function supportsRememberMe()
    {
        return false;
    }

}