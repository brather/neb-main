<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? if (!empty($arResult['ERROR_MESSAGE'])) { ?>
    <script type="application/javascript">
        $(function () {
            FRONT.simplePopup(function () {
                var ok = $('<button type="button" class="btn btn-primary">Закрыть</button>');
                var message = $('<p class="popup-error alert alert-danger">' +
                    '<?=$arResult['ERROR_MESSAGE']?>' +
                    '</p>');
                this.applyHeader();
                this.applyFooter();
                this.popupBody.append(message);
                ok.click(function () {
                    this.popup.modal('hide');
                }.bind(this));
                this.popupFooter.append(ok);
            });
        })
    </script>
<? } ?>
