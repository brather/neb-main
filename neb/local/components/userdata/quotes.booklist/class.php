<?php

use Bitrix\Main\DB\MysqlResult;
use Nota\Bookcatalog\AuthorBookTable;
use Nota\Bookcatalog\BookTable;
use Nota\Bookcatalog\SectionTable;

use Nota\Bookcatalog\AuthorIdTable;
use Nota\Bookcatalog\SectionIdTable;

use Nota\UserData\UserBookTable;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;

CModule::IncludeModule('nota.userdata');
CModule::IncludeModule('nota.bookcatalog');
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);

class QuotesGetDataLibrary extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $arParams['CACHE_TIME'] = $arParams['CACHE_TIME'];
		$arParams['DATES'] = $arParams['DATES'];
        return $arParams;
    }
	
	/*
	 * Получаем список массива книг
	 * */
	 
	public function getBooks()
    {
    	/*echo "<pre>";
		print_r($_REQUEST);
		echo "</pre>";
		/*
		echo "<pre>";
		print_r($this -> arParams['DATES']);
		echo "</pre>";*/
		
		$rsComments = BookTable::getList(array(
            'select' => array_keys(BookTable::getMap()),
            'filter' => array(),
            'order' => array()
        ));
        $rsComments = new CDBResult($rsComments);
		
		if($_REQUEST['page'])
		{
			$nav_param = intval($_REQUEST['page']);
		}
		else
		{
			$nav_param = 10;
		}	
		
        $rsComments->NavStart($nav_param);

        while ($arComment = $rsComments->NavNext(false)) {
            $arResult['COMMENTS'][] = $arComment;
        }

        $this->arResult['PAGINATION'] = $rsComments->GetPageNavStringEx($navComponentObject, 'Страницы:', 'quotes');

		
		print_r($arResult['PAGINATION']);
		

		if($_REQUEST['sort'])
		{
			if($_REQUEST['sort'] == 'author')
			{
				$order = array(
					'AUTHOR_ID' => $_REQUEST['order']
				);
			}elseif($_REQUEST['sort'] == 'name')
			{
				$order = array(
					'NAME' => $_REQUEST['order']
				);
					
			}elseif($_REQUEST['sort'] == 'date')
			{
				$order = array(
					'DATE' => $_REQUEST['order']
				);
				
			}	
		}else
		{
			$order = array(
				'NAME' => 'ASC'
			);
		}
		
		
		
		if($this -> arParams['DATES']['BOOK']['OLD']['VALUES'] and $this -> arParams['DATES']['BOOK']['READ']['VALUES'])
		{
			$arAllBooks = array_merge($this -> arParams['DATES']['BOOK']['OLD']['VALUES'], $this -> arParams['DATES']['BOOK']['READ']['VALUES']);
		}
		elseif(!$this -> arParams['DATES']['BOOK']['READ']['VALUES'])
		{
			$arAllBooks = $this -> arParams['DATES']['BOOK']['OLD']['VALUES'];
		}
		elseif(!$this -> arParams['DATES']['BOOK']['OLD']['VALUES'])
		{
			$arAllBooks = $this -> arParams['DATES']['READ']['OLD']['VALUES'];
		}
		
		
		//if(!$_REQUEST['all_book'])
		//{
			if($_REQUEST['category'])
			{
				$arId = array();	
				foreach($_REQUEST['category'] as $arCategory)
				{	
					foreach($this -> arParams['DATES']['THEME'][htmlspecialcharsEx($arCategory)]['ID'] as $arIds)
					{
						$arCaetgoryId[] = $arIds;
					}	
				}
			}
			
			/*echo "<pre>";
			print_r($arId);
			echo "</pre>";*/
			
			if($_REQUEST['author'])
			{
				foreach($_REQUEST['author'] as $arAuthor)
				{	
					foreach($this -> arParams['DATES']['AUTHOR'][htmlspecialcharsEx($arAuthor)]['ID'] as $arIds)
					{
						$arAuthorId[] = $arIds;
					}	
				}
			}
			
			if($_REQUEST['dates'])
			{
				
			}
		//}
		
        $rsBook = BookTable::getList(
	        array(
	            'select' => array_keys(BookTable::getMap()),
	            'filter' => array('EXALEAD_ID' => $this -> arParams['DATES']['BOOK']['OLD']['VALUES']),
	            'order' => $order
	        )
		);
				
        while ($arBook = $rsBook->Fetch()) 
        {
        	
        	unset($authors);
			 $rsAuthor = AuthorBookTable::getList(array(
	            'select' => array_keys(AuthorBookTable::getMap()),
	            'filter' => array('ID' => $arBook['AUTHOR_ID']),
	            'order' => array()
	        ));
	
	        if($arAuthor = $rsAuthor->Fetch())
			{
				if($arAuthor['SECOND_NAME'])
				{
					$author = $arAuthor['SECOND_NAME'] . ' ';
				}
				if($arAuthor['NAME'])
				{
					$author .= ' ' . $arAuthor['NAME'] . ' ';
				}
				if($arAuthor['LAST_NAME'])
				{
					$author .= $arAuthor['LAST_NAME'];
				}
			}
	       	
			$file = CFile::ResizeImageGet($arBook['PREVIEW_PICTURE'], array('width'=>829, 'height'=>413), BX_RESIZE_IMAGE_PROPORTIONAL, true); 
	        $this->arResult['BOOK'][] = array(
					'NAME' => $arBook['NAME'],
					"AUTHOR" => $author,
					'PREVIEW_PICTURE' => $file['src'],
					'PREVIEW_TEXT' => $arBook['PREVIEW_TEXT']
			);
        }	
	}

	public function executeComponent()
    {

       	if ($this->StartResultCache(false, array())) {
       		
            if (!CModule::includeModule('nota.userdata')) {
                $this->AbortResultCache();
                throw new Exception(Loc::getMessage('QUOTES_MODULE_NOT_INSTALLED'));
            }
			
			$this->getBooks();
			$this->includeComponentTemplate();
        }
    }
}

?>