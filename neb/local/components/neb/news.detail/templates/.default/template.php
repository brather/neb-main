<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


?>

<div class="news-detail">
    <h3><?=$arResult['NAME']?></h3>
    <?if ($arResult['PROPERTIES']['type']['VALUE']):?>
    	<p>метка новости: <?=$arResult['PROPERTIES']['type']['VALUE']?></p>
    <?endif;?>
    <p class="news-item">
    	<?=$arResult['DETAIL_TEXT']?>
    </p>

</div>