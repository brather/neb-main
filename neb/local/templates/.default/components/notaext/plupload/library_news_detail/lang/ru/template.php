<?php

$MESS['BROWSER_NOT_SUPPORTED'] = 'Ваш браузер не поддерживает загрузку файлов :-(';
$MESS['NO_FILE_SELECTED'] = 'Файл не выбран';
$MESS['SELECT_FILE'] = 'Выбрать файл';
$MESS['SELECT_FILES'] = 'Выбрать файлы';
$MESS['UPLOAD'] = 'Загрузить';
$MESS['PLUPLOAD_LIBRARY_NEWS_DETAIL_ADD'] = "Добавить изображение";
$MESS['PLUPLOAD_LIBRARY_NEWS_DETAIL_LOAD'] = "Загрузить изображение";
$MESS['PLUPLOAD_LIBRARY_NEWS_DETAIL_DRAG'] = "или перетащите его на это поле";
$MESS['PLUPLOAD_LIBRARY_NEWS_DETAIL_LOADING'] = "Загружается";
