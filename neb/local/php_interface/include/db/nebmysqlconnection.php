<?php
/**
 * User: agolodkov
 * Date: 29.09.2015
 * Time: 17:10
 */
include __DIR__ . '/nebmysqlresult.php';
use Bitrix\Main\DB\MysqlConnection;
use Bitrix\Main\Diag\SqlTrackerQuery;

class NebMysqlConnection extends MysqlConnection
{
    /**
     * Returns database depended result of the query.
     *
     * @param resource                          $result       Result of internal query function.
     * @param \Bitrix\Main\Diag\SqlTrackerQuery $trackerQuery Debug collector object.
     *
     * @return NebMysqlResult
     */
    protected function createResult($result,
        SqlTrackerQuery $trackerQuery = null
    ) {
        return new NebMysqlResult($result, $this, $trackerQuery);
    }
}