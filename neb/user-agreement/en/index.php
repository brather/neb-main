<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Terms of Use");
?><h3>Rights and duties of the users </h3>
 <b>1. General Terms<br>
 </b><br>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 1.1. This agreement is an official proposal (a public offer) of National Electronic Library, hereinafter referred to as “NEB”, and contains all actualities of allowing access to library informational electronic resources of National Electronic Library located at the following address: <a href="http://_________"></a>http://_________ (hereinafter – the Website).<br>
 <br>
	 1.2. According to Paragraph 2 of Article 437 of Civil Code of Russia (hereinafter – CCR), in case of accepting of the following terms, an individual person accepting this public offer becomes “the User” (according to Paragraph 3 of Article 438 of CCR, acceptance of this public offer is equal to the agreement on terms, stated in the public offer), and NEB and the User, together or individually, become “the Party” or “the Parties” of the Agreement on terms of the public offer<br>
 <br>
	 1.3. The acceptance of terms stated in the current Public Offer is a fact of signing and/or confirming of your personal data and agreement with the terms of the Agreement of the Public Offer while going through registration in the Internet through the NEB-website _______________. Going through registration the User inputs his Name (login) and Password given to him in an encoded form. The User accepts full responsibility for safety of the User’s Name (login) and Password, particularly, for the lack of access to them for third parties.<br>
 <br>
	 1.4. NEB has a right to change or expand on the current Agreement at any moment without previous or sequential notification. The User follows any changes of the Agreement by himself and gets acquinted with the current edition of the Agreement. The following usage of the Website by the User after the current Agreement has been changed and/or expanded on means accepting and agreeing of the User on the changes and/or additions.<br>
 <br>
	 1.5. Going through the registration the User shall inform NEB and verify correctness of his personal data: last name, first name and middle name, Email, password, other data. In case of mistakes or discrepancy the User shall appeal to the NEB Technical support service via the Email: __________________.<br>
 <br>
	 1.6. Accepting the current Agreement, the User agrees to allow NEB to process his personal data given to NEB according to the terms of the current Agreement.<br>
 <br>
	 1.7. Using this Website, including browsing pages and materials of the Websute, the User confirms that he is acquaint and agrees with the rules of access to library electronic informational resources of the Website. This also means that the User unconditionally accepts the current Agreement and its terms.<br>
 <br>
</p>
 2. Main notions used for the current Agreement on terms of the Public Offer.&nbsp;<br>
 <br>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 2.1. With the purposes of the current Public Offer notions given below are used in following meanings: the Public Offer – an offer of NEB addressed to any individual person being a registered reader of National Electronic Library. Acceptance of the Public Offer – full and unconditional acceptance of the Public Offer by way of executing of actions stated in the current Agreement on terms of the Public Offer. Acceptance of the Public Offer makes Agreement on terms of the Public Offer. The User – an individual person being a registered reader of National Electronic Library, having done the Acceptance of the Public Offer by made the Agreement on terms of the Public Offer. <br>
 <br>
</p>
 3. Subject of the Agreement on terms of the Public Offer<br>
 <br>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 3.1. NEB provides the User a service of access to library electronic informational resources of National Electronic Library by means of a unique User’s name and a password within 3 (three) calendar days from the moment of the Acceptance of the Public Offer (hereinafter – Service) in amount of library electronic informational resource of the Website.<br>
 <br>
	 3.2. NEB provides Service via the Internet by means of the following website _____.<br>
 <br>
	 3.3. The Public Offer is an official document and its current edition is located on the Internet-website of NEB at the following address ________. <br>
 <br>
</p>
 4. The Acceptance of the Public Offer and making the Agreement on terms of the Public Offer&nbsp;<br>
 <br>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 4.1. The User does the Acceptance of the Public Offer via signing the current Agreement on terms of the Public Offer and/or confirming personal data and agreeing with the terms of the Agreement on terms of the Public Offer when registering in the Internet via the website ______. Confirming of personal data and agreeing with terms of the current Agreement on terms of the Public Offer when registering in the Internet via the website ____________ is considered equal to signing the current Agreement on terms of the Public Offer.<br>
 <br>
	 4.2. The date of the acceptance of the Public Offer is the date of making the current Agreement on terms of the Public Offer.<br>
 <br>
	 4.3. Service is provided free of charge. <br>
 <br>
</p>
 5. Rights and duties of Parties<br>
 <br>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 5.1. NEB is obliged to:<br>
 <br>
</p>
<p style="margin: 0 0 0 80px; border: none; padding: 0px;">
	 5.1.1. Provide Service according to the current Agreement on terms of the Public Offer.<br>
 <br>
	 5.1.2. Provide the User with a unique user’s name and a password. <br>
 <br>
</p>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 5.2. The User is obliged to: <br>
 <br>
</p>
<p style="margin: 0 0 0 80px; border: none; padding: 0px;">
	 5.2.1. Use Service of access to library electronic informational resources only for private needs and not to use it for business needs or getting self-profit. <br>
 <br>
	 5.2.2. Not to allow third parties to access to electronic informational resources possessed by him according to the Agreement on terms of the Public Offer, has no right to modify, sell, distribute materials of the Website entirely or partially if it violates the sole rights of third parties, particularly, author’s and related rights, and also sole rights on some invention, useful model, industrial sample or trademark.<br>
 <br>
	 5.2.3. It’s forbidden for the User to copy, download, reproduce, distribute or translate into other languages in an electronic or printed form any work located on the Website if it violates the sole rights of third parties, particularly, author’s and related rights and the sole rights on some invention, useful model, industrial sample or trademark.<br>
 <br>
	 5.2.4. The User accepts and agrees with the fact that materials located on the Website and necessary software connected with work of the Website are protected by the intellectual property law of the Russian Federation and other international laws. The User bears responsibility in case of his violation of intellectual rights of third parties, particularly, author’s and related rights, and also the sole right on some invention, useful model, industrial sample or trademark <br>
 <br>
</p>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 5.3. NEB has the following rights:<br>
 <br>
</p>
<p style="margin: 0 0 0 80px; border: none; padding: 0px;">
	 5.3.1. To limit or block access to electronic informational resources or take other measures concerning the User having violated the terms of the current Agreement or norms of current legal system or protected by the law of rights of third parties. <br>
 <br>
	 5.3.2. To modify the Website at its own discretion. To change the current Agreement unilaterally without any special notification. A new edition of the Agreement becomes effective from the moment of its uploading on the Website. <br>
 <br>
</p>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 5.4. The User has the following rights:<br>
 <br>
</p>
<p style="margin: 0 0 0 80px; border: none; padding: 0px;">
	 5.4.1. To refuse making the Agreement on terms of the Public Offer in any time from the moment stated in paragraph 4.2 of the current Agreement on terms of the Public Offer.<br>
 <br>
	 5.4.2. The user has a right to reproduce and use publications located on the Website and provided according to the current Agreement on terms of the Public Offer. This is possible free of charge due to CCR, part 4, chapter 70, article 1273. 1274.<br>
 <br>
</p><p style="margin: 0 0 0 40px; border: none; padding: 0px;">
5.5. NEB is not responsible for inappropriate providing of Service and/or executing of the Order concerning the User’s claims on the quality of the Internet connection connected with the quality of functioning of the nets of Internet service providers, policy of traffic exchange among internet service providers and other circumstances locating out of the zone of competence, influence and control of NEB.<br>
 <br>
</p>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 5.6. All claims on providing Service are sent to NEB in written form at the following address: __________________. All received information is processed within the terms defined by CCR.<br>
 <br>
</p>
 6. Terms and order of providing services.<br>
 <br>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 6.1. The User gets acquainted with the rules and amount of Services provided by NEB on the Website __________.<br>
 <br>
	 6.2. The User is provided with Service when the Agreement comes into effect on terms of the Public Offer.<br>
 <br>
	 6.3. Service is considered to be provided fully and appropriately if the Executer doesn’t get and satisfy NEB-complaint from the User before the end of period of Service providing. <br>
 <br>
</p>
 7. Other terms<br>
 <br>
<p style="margin: 0 0 0 40px; border: none; padding: 0px;">
	 7.1.All changes, additions, applications to the current Agreement on terms of the Public Offer or other information are an integral part of the Agreement and are obligatory to be executed from the moment of their posting on the Website if except as otherwise provided by NEB.<br>
 <br>
	 7.2. All arguments between the Parties are solved by means of negotiations. In case if such way of solving is not possible the arguments are solved judicially according to the Russian Legal System on the territory of EL location.&nbsp;<br>
 <br>
	 7.3. Relationships of the Parties according to the current Agreement are regulated by the current Legal System of the Russian Federation. <br>
 <br>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>