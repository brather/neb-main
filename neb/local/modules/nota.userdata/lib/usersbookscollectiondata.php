<?php
/**
 * User: agolodkov
 * Date: 17.09.2015
 * Time: 15:42
 */

namespace Nota\UserData;


use Bitrix\Main\Entity\DataManager;

/**
 * Class UsersBooksCollectionDataTable
 *
 * @package Nota\UserData
 */
class UsersBooksCollectionDataTable extends DataManager
{
    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'neb_users_books_collection_data';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID'               => array(
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
            ),
            'UF_UID'           => array(
                'data_type' => 'integer',
            ),
            'UF_COLLECTION_ID' => array(
                'data_type' => 'integer',
            ),
            'UF_DATE_ADD'      => array(
                'data_type' => 'datetime',
            ),
        );
    }

    /**
     * @param array $data
     *
     * @return int|null
     * @throws \Exception
     */
    public static function applyCollection($data)
    {
        $data = array_merge(
            array(
                'UF_UID'           => 0,
                'UF_COLLECTION_ID' => 0,
            ),
            $data
        );
        if (empty($data['UF_UID'])) {
            throw new \Exception('Not set UF_UID!');
        }
        if (empty($data['UF_COLLECTION_ID'])) {
            throw new \Exception('Not set UF_COLLECTION_ID!');
        }
        $row = static::getRow(
            array(
                'filter' => $data,
                'select' => array('ID')
            )
        );
        if (isset($row['ID'])) {
            return $row['ID'];
        }
        $addResult = static::add($data);

        return $addResult->getId();
    }
}