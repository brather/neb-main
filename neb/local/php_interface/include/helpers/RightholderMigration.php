<?php

use \Bitrix\Main\Loader,
    \Bitrix\Main\UserFieldTable,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Highloadblock\HighloadBlockTable;

/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 14.11.2016
 * Time: 18:23
 */
class RightholderMigration
{
    /** ================================================================= MIGRATION SCRIPT ====================================================================== */

    public static function migrationMethod() { // TODO: Закоментировать, предварительно проверив на prod-е результат

        $arDBHBlock = $arAddHBlock = $arEntityId = $arEnumEntityId = $arEventTypeId = [];

        $obUserTypeEntity = new \CUserTypeEntity();
        $obUserFieldEnum  = new \CUserFieldEnum();
        $obEventMessage   = new \CEventMessage;

        $arEntity = self::getMigrationEntities();

        // ================================== ШАГ 1: ДОБАВЛЕНИЕ ТАБЛИЦ H-БЛОКОВ =======================================

        Loader::includeModule('highloadblock');

        // Выбор таблиц из БД
        $rsHIBlocks = HighloadBlockTable::getList(['filter' => ['NAME' => array_keys($arEntity['HBLOCKS'])]]);
        while ($arItem = $rsHIBlocks->fetch())
            $arDBHBlock[$arItem['NAME']] = $arItem['ID'];
        unset($rsHIBlocks, $arItem);

        // Добавление таблиц в БД
        foreach ($arEntity['HBLOCKS'] as $k => $v) {
            if ($arDBHBlock[$k])
                continue;

            $obAddResult = HighloadBlockTable::add(['NAME' => $k, 'TABLE_NAME' => $v]);
            if ($obAddResult->isSuccess())
                $arDBHBlock[$k] = $obAddResult->getId();
        }

        // ============================= ШАГ 2: ДОБАВЛЕНИЕ ПОЛЕЙ К ТАБЛИЦАМ H-БЛОКОВ ==================================

        foreach ($arDBHBlock as $k => $v)
            $arEntityId[] = 'HLBLOCK_' . $v;

        $rsFields = UserFieldTable::getList(['filter' => ['ENTITY_ID' => $arEntityId]]);
        while ($arItem = $rsFields->fetch())
            $arDBUserFields[$arItem['ENTITY_ID']][$arItem['FIELD_NAME']] = $arItem;
        unset($rsFields, $arItem);

        foreach ($arDBHBlock as $k => $v) {
            foreach ($arEntity['USER_FIELDS'][$k] as $k2 => &$v2) {
                if (!empty($arDBUserFields['HLBLOCK_' . $v][$k2]))
                    continue;

                $v2['ENTITY_ID'] = 'HLBLOCK_' . $v;
                $v2['FIELD_NAME'] = $k2;

                if ($iUserEntity = $obUserTypeEntity->Add($v2))
                    $arDBUserFields[$v2['ENTITY_ID']][$v2['FIELD_NAME']] = $v2;
            }
        }

        // ============================= ШАГ 3: ДОБАВЛЕНИЕ ЗНАЧЕНИЙ СВОЙСТВ-СПИСКОВ ==================================

        foreach ($arDBUserFields as $k => $v)
            foreach ($v as $k2 => $v2)
                if ($v2['USER_TYPE_ID'] == 'enumeration') {
                    $arEnumEntity[$v2['ENTITY_ID']][$v2['FIELD_NAME']] = $v2['ID'];
                    $arEnumEntityId[] = $v2['ID'];
                }

        // выборка значений свойств типа "список"
        if (!empty($arEnumEntityId)) {
            $rsUserFieldEnum = $obUserFieldEnum->GetList(
                array('SORT' => 'ASC'),
                array("USER_FIELD_ID" => $arEnumEntityId)
            );
            while ($arItem = $rsUserFieldEnum->Fetch())
                $arDBEnumValues[$arItem['USER_FIELD_ID']][$arItem['XML_ID']] = $arItem['ID'];
            unset($rsUserFieldEnum, $arItem);
        }

        // Добавление значений
        $n = 0;
        foreach ($arEntity['ENUM_VALUES'] as $sHBlock => $arProps) {
            foreach ($arProps as $sProp => $arValues) {
                foreach ($arValues as $sValCode => $sValInfo) {

                    // проверка на присутствие в БД
                    $iEnumId = intval($arEnumEntity['HLBLOCK_'.$arDBHBlock[$sHBlock]][$sProp]);
                    if ($iEnumId && empty($arDBEnumValues[$iEnumId][$sValCode])) {
                        $sValInfo['USER_FIELD_ID'] = $iEnumId;
                        $sValInfo['XML_ID'] = $sValCode;

                        $obUserFieldEnum->SetEnumValues($iEnumId, ['n0'=>$sValInfo]);

                        $n++;
                    }
                }
            }
        }

        // ================================ ШАГ 4: ДОБАВЛЕНИЕ ПОЧТОВЫХ СОБЫТИЙ =======================================

        foreach ($arEntity['EVENT_TYPE'] as $arEventType)
            $arEventTypeId[$arEventType['EVENT_NAME']] = $arEventType['EVENT_NAME'];
        $arEventTypeId = array_values($arEventTypeId);

        // выбор всех почтовых типов из БД
        $rsEventType = CEventType::GetList();
        while ($arItem = $rsEventType->Fetch())
            $arDBEventType[$arItem['LID']][$arItem['EVENT_NAME']] = $arItem['ID'];
        unset($rsEventType, $arItem);

        // добавление отсутствующих типов в БД
        foreach ($arEntity['EVENT_TYPE'] as $arEventType)
            if (empty($arDBEventType[$arEventType['LID']][$arEventType['EVENT_NAME']])
                && $iType = CEventType::Add($arEventType)
            )
                $arDBEventType[$arEventType['LID']][$arEventType['EVENT_NAME']] = $iType;

        // выборка всех почтовых шаблонов
        $rsEventMessage = $obEventMessage->GetList($by="site_id", $order="desc", ['TYPE_ID' => $arEventTypeId]);
        while ($arItem = $rsEventMessage->Fetch())
            $arDBEventMessage[$arItem['EVENT_NAME']] = $arItem['ID'];
        unset($rsEventMessage, $arItem);

        // добавление отсутствующих типов в БД
        foreach ($arEntity['EVENT_MESSAGE'] as $arEventMessage)
            if (empty($arDBEventMessage[$arEventMessage['EVENT_NAME']])
                && $iMessage = $obEventMessage->Add($arEventMessage)
            )
                $arDBEventMessage[$arEventMessage['EVENT_NAME']] = $iMessage;

        // ==================================== ШАГ 4: ДОБАВЛЕНИЕ АГЕНТОВ ============================================

        $rsAgents = CAgent::GetList();
        while ($arItem = $rsAgents->Fetch())
            $arDBAgents[$arItem['NAME']] = $arItem['ID'];
        unset($rsAgents, $arItem);

        foreach ($arEntity['AGENTS'] as $arAgent)
            if (empty($arDBAgents[$arAgent['NAME']]) && $iAgent = CAgent::Add($arAgent))
                $arDBAgents[$arAgent['NAME']] = $iAgent;
    }

    /**
     * Скрипт миграции: получение полей инфоблоков и пользовательских свойств
     * @return array
     */
    private static function getMigrationEntities() {

        // Таблицы H-Блоков в БД
        $arHBlocks = [
            'RightHoldersContracts'        => 'neb_rightholders_contracts',
            'RightHoldersContractsHistory' => 'neb_rightholders_contracts_history'
        ];

        $sContracts = $sHistory = '';
        foreach ($arHBlocks as $k => $v) {
            if (empty($sContracts))
                $sContracts = $k;
            else
                $sHistory = $k;
        }

        // ID пользователя
        $arUserFields[$sContracts]['UF_USER_ID'] = [
            'USER_TYPE_ID'  => 'integer',
            'XML_ID'        => 'user_id',
            'SORT'          => '10',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'Y',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => '20',
                'MIN_VALUE' => '0',
                'MAX_VALUE' => '0',
                'DEFAULT_VALUE' => ''
            ]
        ];
        // Номер договора
        $arUserFields[$sContracts]['UF_NUMBER'] = [
            'USER_TYPE_ID'  => 'string',
            'XML_ID'        => 'number',
            'SORT'          => '20',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => '20',
                'ROWS' => '1',
                'REGEXP' => '',
                'MIN_LENGTH' => '0',
                'MAX_LENGTH' => '0',
                'DEFAULT_VALUE' => ''
            ]
        ];
        // Дата заключения договора
        $arUserFields[$sContracts]['UF_DATE_FROM'] = [
            'USER_TYPE_ID'  => 'date',
            'XML_ID'        => 'date_from',
            'SORT'          => '30',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => [
                    'TYPE'  => 'NOW',
                    'VALUE' => ''
                ]
            ]
        ];
        // Дата окончания договора
        $arUserFields[$sContracts]['UF_DATE_TO'] = [
            'USER_TYPE_ID'  => 'date',
            'XML_ID'        => 'date_from',
            'SORT'          => '40',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => [
                    'TYPE'  => 'NOW',
                    'VALUE' => ''
                ]
            ]
        ];
        // Тип договора
        $arUserFields[$sContracts]['UF_TYPE'] = [
            'USER_TYPE_ID'  => 'enumeration',
            'XML_ID'        => 'type',
            'SORT'          => '50',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DISPLAY' => 'LIST',
                'LIST_HEIGHT' => '5',
                'CAPTION_NO_VALUE' => ''
            ]
        ];
        // Статус договора
        $arUserFields[$sContracts]['UF_STATUS'] = [
            'USER_TYPE_ID'  => 'enumeration',
            'XML_ID'        => 'status',
            'SORT'          => '60',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DISPLAY' => 'LIST',
                'LIST_HEIGHT' => '5',
                'CAPTION_NO_VALUE' => ''
            ]
        ];
        // Правоустанавливающие документы
        $arUserFields[$sContracts]['UF_LEGAL_DOCUMENTS'] = [
            'USER_TYPE_ID'  => 'file',
            'XML_ID'        => 'legal_documents',
            'SORT'          => '70',
            'MULTIPLE'      => 'Y',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => '20',
                'LIST_WIDTH' => '200',
                'LIST_HEIGHT' => '200',
                'MAX_SHOW_SIZE' => '0',
                'MAX_ALLOWED_SIZE' => '0',
                'EXTENSIONS' => []
            ]
        ];
        // Книги
        $arUserFields[$sContracts]['UF_BOOK'] = [
            'USER_TYPE_ID'  => 'exalead_book',
            'XML_ID'        => 'book',
            'SORT'          => '80',
            'MULTIPLE'      => 'Y',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => '20',
                'ROWS' => '1',
                'REGEXP' => '',
                'MIN_LENGTH' => '0',
                'MAX_LENGTH' => '0',
                'DEFAULT_VALUE' => ''
            ]
        ];
        // Комментарий
        $arUserFields[$sContracts]['UF_COMMENT'] = [
            'USER_TYPE_ID'  => 'string',
            'XML_ID'        => 'comment',
            'SORT'          => '90',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => '30',
                'ROWS' => '5',
                'REGEXP' => '',
                'MIN_LENGTH' => '0',
                'MAX_LENGTH' => '0',
                'DEFAULT_VALUE' => ''
            ]
        ];

        // ID договора
        $arUserFields[$sHistory]['UF_CONTRACT'] = [
            'USER_TYPE_ID'  => 'integer',
            'XML_ID'        => 'contract_id',
            'SORT'          => '10',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => '20',
                'MIN_VALUE' => '0',
                'MAX_VALUE' => '0',
                'DEFAULT_VALUE' => ''
            ]
        ];
        // Дата изменения статуса
        $arUserFields[$sHistory]['UF_DATE'] = [
            'USER_TYPE_ID'  => 'datetime',
            'XML_ID'        => 'date',
            'SORT'          => '20',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => [
                    'TYPE' => 'NONE',
                    'VALUE' => ''
                ]
            ]
        ];
        // Статус договора
        $arUserFields[$sHistory]['UF_STATUS'] = [
            'USER_TYPE_ID'  => 'string',
            'XML_ID'        => 'status',
            'SORT'          => '30',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => [
                    'SIZE' => '20',
                    'ROWS' => '1',
                    'REGEXP' => '',
                    'MIN_LENGTH' => '0',
                    'MAX_LENGTH' => '0',
                    'DEFAULT_VALUE' => ''
                ]
            ]
        ];
        // Комментарий к статусу
        $arUserFields[$sHistory]['UF_COMMENT'] = [
            'USER_TYPE_ID'  => 'string',
            'XML_ID'        => 'comment',
            'SORT'          => '40',
            'MULTIPLE'      => 'N',
            'MANDATORY'     => 'N',
            'SHOW_FILTER'   => 'N',
            'SHOW_IN_LIST'  => 'Y',
            'EDIT_IN_LIST'  => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => [
                    'SIZE' => '20',
                    'ROWS' => '1',
                    'REGEXP' => '',
                    'MIN_LENGTH' => '0',
                    'MAX_LENGTH' => '0',
                    'DEFAULT_VALUE' => ''
                ]
            ]
        ];

        // ============= СПИСОЧНЫЕ СВОЙСТВА ==========
        $arEnumValues[$sContracts]['UF_STATUS']['draft'] = [
            'VALUE' => 'Черновик',
            'DEF' => 'N',
            'SORT' => '10'
        ];
        $arEnumValues[$sContracts]['UF_STATUS']['processed'] = [
            'VALUE' => 'На согласовании',
            'DEF' => 'N',
            'SORT' => '20'
        ];
        $arEnumValues[$sContracts]['UF_STATUS']['signed'] = [
            'VALUE' => 'Подписан',
            'DEF' => 'N',
            'SORT' => '30'
        ];

        $arEnumValues[$sContracts]['UF_TYPE']['simple_free'] = [
            'VALUE' => 'простая (не исключительная) лицензия, безвозмездная',
            'DEF' => 'N',
            'SORT' => '10'
        ];
        $arEnumValues[$sContracts]['UF_TYPE']['simple_compensatory'] = [
            'VALUE' => 'простая (не исключительная) лицензия, возмездная',
            'DEF' => 'N',
            'SORT' => '20'
        ];
        $arEnumValues[$sContracts]['UF_TYPE']['exclusive_free'] = [
            'VALUE' => 'исключительная лицензия, безвозмездная',
            'DEF' => 'N',
            'SORT' => '30'
        ];
        $arEnumValues[$sContracts]['UF_TYPE']['exclusive_compensatory'] = [
            'VALUE' => 'исключительная лицензия, возмездная',
            'DEF' => 'N',
            'SORT' => '40'
        ];

        // ============= ТИПЫ ПОЧТОВЫХ СОБЫТИЙ ==========

        $arEventType[] = [
            'LID' => 'ru',
            'EVENT_NAME' => 'NEB_RIGHTHOLDER_CHANGE',
            'NAME' => 'Изменение статуса договора',
            'DESCRIPTION' =>
                '#NAME# - Имя пользователя
#EMAIL# - E-mail пользователя
#ID# - ID договора
#DATE_FROM# - Дата договора
#STATUS# - Статус
#INFORMATION# - Информация'
        ];
        $arEventType[] = [
            'LID' => 'en',
            'EVENT_NAME' => 'NEB_RIGHTHOLDER_CHANGE',
            'NAME' => 'Changed contract status',
            'DESCRIPTION' =>
                '#NAME# - Username
#EMAIL# - User e-mail
#ID# - Contracts ID
#DATE_FROM# - Date from
#STATUS# - Status
#INFORMATION# - Информация'
        ];
        $arEventType[] = [
            'LID' => 'ru',
            'EVENT_NAME' => 'NEB_RIGHTHOLDER_CONTRACT',
            'NAME' => 'Новый договор правообладателя',
            'DESCRIPTION' =>
                '#ID# - ID договора
#EMAIL# - E-mail оператора
#USER# - Пользователь
#DATE# - Дата
#TYPE# - Тип
#STATUS# - Статус
#LEGAL# - Правоустанавливающие документы
#BOOK# - Книги
#COMMENT# - Комментарий'
        ];
        $arEventType[] = [
            'LID' => 'en',
            'EVENT_NAME' => 'NEB_RIGHTHOLDER_CONTRACT',
            'NAME' => 'New rightholoders contract',
            'DESCRIPTION' =>
                '#ID# - Contracts ID
#EMAIL# - Operator\'s e-mail
#USER# - User
#DATE# - Date
#TYPE# - Type
#STATUS# - Status
#LEGAL# - Legal documents
#BOOK# - Books
#COMMENT# - Comment'
        ];
        $arEventType[] = [
            'LID' => 'ru',
            'EVENT_NAME' => 'NEB_RIGHTHOLDER_REMINDER',
            'NAME' => 'Напоминание о перезаключении договора',
            'DESCRIPTION' =>
                '#NAME# - Пользователь
#EMAIL# - E-mail
#ID# - ID договора
#NUMBER# - Номер договора
#DATE_FROM# - Дата начала действия
#DATE_TO# - Дата окончания действия
#TYPE# - Тип
#STATUS# - Статус
#DOCUMENT# - Правоустанавливающие документы
#BOOK# - Произведения
#COMMENT# - Комментарий'
        ];
        $arEventType[] = [
            'LID' => 'en',
            'EVENT_NAME' => 'NEB_RIGHTHOLDER_REMINDER',
            'NAME' => 'Сontract renegotiation remind',
            'DESCRIPTION' =>
                '#NAME# - User
#EMAIL# - E-mail
#ID# - Contract ID
#NUMBER# - Contracts number
#DATE_FROM# - Date from
#DATE_TO# - Date to
#TYPE# - Type
#STATUS# - Status
#DOCUMENT# - Legal documents
#BOOK# - Books
#COMMENT# - Comment'
        ];

        // Почтовые сообщения
        $arEventMessage[] = [
            'EVENT_NAME' => 'NEB_RIGHTHOLDER_CHANGE',
            'ACTIVE'     => 'Y',
            'LID'        => SITE_ID,
            'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
            'EMAIL_TO'   => '#EMAIL#',
            'SUBJECT'    => '#SITE_NAME#: Изменение статуса договора',
            'BODY_TYPE'  => 'text',
            'MESSAGE'    =>
                'Информационное сообщение сайта #SITE_NAME#
------------------------------------------

Здравствуйте, #NAME#.

Наш оператор рассмотрел Ваш договор ##ID# от #DATE_FROM# на защиту авторских прав и перевел его в статус "#STATUS#"

#INFORMATION#

Просмотреть договор: http://#SERVER_NAME#/profile/#ID#/

Письмо сгенерировано автоматически.'
        ];
        $arEventMessage[] = [
            'EVENT_NAME' => 'NEB_RIGHTHOLDER_CONTRACT',
            'ACTIVE'     => 'Y',
            'LID'        => SITE_ID,
            'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
            'EMAIL_TO'   => '#EMAIL#',
            'SUBJECT'    => '#SITE_NAME#: Новый договор правообладателя',
            'BODY_TYPE'  => 'text',
            'MESSAGE'    =>
                'Информационное сообщение сайта #SITE_NAME#
------------------------------------------

На сайте #SITE_NAME# добавлен новый договор правообладателя.

Информация:

Пользователь: #USER#
Дата: #DATE#
Тип: #TYPE#
Статус: #STATUS#
Комментарий: #COMMENT#
Правоустанавливающие документы:
#LEGAL#
Книги:
#BOOK#

http://#SERVER_NAME#/profile/rightholders-contracts/#ID#/

Сообщение сгенерировано автоматически.'
        ];
        $arEventMessage[] = [
            'EVENT_NAME' => 'NEB_RIGHTHOLDER_REMINDER',
            'ACTIVE'     => 'Y',
            'LID'        => SITE_ID,
            'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
            'EMAIL_TO'   => '#EMAIL#',
            'SUBJECT'    => '#SITE_NAME#: Напоминание о перезаключении договора',
            'BODY_TYPE'  => 'text',
            'MESSAGE'    =>
                'Информационное сообщение сайта #SITE_NAME#
------------------------------------------

Здравствуйте, #NAME#.

Напоминаем, что на сайте #SITE_NAME# с Вами был заключен следующий договор о защите авторских прав, который истекает #DATE_TO#:

ID: #ID#
Номер: #NUMBER#
Дата начала: #DATE_FROM#
Дата окончания: #DATE_TO#
Тип: #TYPE#
Статус: #STATUS#
Комментарий: #COMMENT#
Документы: #DOCUMENT#
Книги: #BOOK#

Для продления договора перейдите по следующей ссылке: http://#SERVER_NAME#/profile/contracts/#ID#/

Сообщение сгенерировано автоматически.'
        ];

        // ============= АГЕНТЫ ==========

        $arAgents[] = [
            'MODULE_ID'      => 'main',
            'USER_ID'        => '',
            'SORT'           => 0,
            'NAME'           => 'Neb\Main\Agent\RightholderNotify::send(10);',
            'ACTIVE'         => 'Y',
            'LAST_EXEC'      => '',
            'NEXT_EXEC'      => (new DateTime())->add('+60 minute'),
            'AGENT_INTERVAL' => '86400',
            'IS_PERIOD'      => 'Y'
        ];

        return [
            'HBLOCKS'       => $arHBlocks,
            'USER_FIELDS'   => $arUserFields,
            'ENUM_VALUES'   => $arEnumValues,
            'EVENT_TYPE'    => $arEventType,
            'EVENT_MESSAGE' => $arEventMessage,
            'AGENTS'        => $arAgents
        ];
    }
}