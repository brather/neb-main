<?php

use \Neb\Main\Library\NebLibsCityTable;

/**
 * User: agolodkov
 * Date: 05.12.2015
 * Time: 15:25
 */
class LibraryCitiesComponent extends \Neb\Main\ListComponent
{
    /**
     * @return $this
     */
    public function prepareParams()
    {
        return $this;
    }

    /**
     * @return $this
     */
    public function loadList()
    {
        $this->arResult['items'] = NebLibsCityTable::getByCityLib()->fetchAll();

        return $this;
    }
}