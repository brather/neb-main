<?
namespace Bitrix\NotaExt\Ext\sectionElementSearch;
/*
Подменяем обычный диалог поиска\выбора элемента - диалогом с навигацией по разделам
*/
class IblockSectionElementSearch
{
    function OnEndBufferContent(&$content)
    {
        if (defined('ADMIN_SECTION') and ADMIN_SECTION === true) {
            $page = $GLOBALS['APPLICATION']->GetCurPage();
            if ($page == '/bitrix/admin/iblock_element_edit.php') {
                /*
                Меняем ссылку на попап
                */
                $content = str_replace('/admin/iblock_element_search.php?lang', '/admin/nota_iblock_sections_element_search.php?lang', $content);
                /*
                Увеличиваем ширину попапа
                */
                $width = \COption::GetOptionString('notaext', 'sectionElementSearch_wWidth', 900);
                $height = \COption::GetOptionString('notaext', 'sectionElementSearch_wHeight', 800);

                $content = str_replace('900,', $width . ',', $content);
                $content = str_replace(', 600', ', ' . $height, $content);
            }
        }
    }
}

/*
	#test	
	use Bitrix\NotaExt\Ext\sectionElementSearch\IblockSectionElementSearch;
	AddEventHandler("main", "OnEndBufferContent", Array('Bitrix\NotaExt\Ext\sectionElementSearch\IblockSectionElementSearch', "OnEndBufferContent"));

*/