<?php
/**
 * User: agolodkov
 * Date: 05.10.2016
 * Time: 18:31
 */

namespace AppBundle\DependencyInjection;

use AppBundle\DependencyInjection\Search\Exalead\Condition;
use AppBundle\DependencyInjection\Search\Exalead\EditionsSearchResult;
use AppBundle\DependencyInjection\Search\Exalead\QueryBuilder;
use AppBundle\DependencyInjection\Search\Exalead\SearchClient;
use AppBundle\DependencyInjection\Search\Exalead\SearchResult;
use AppBundle\DependencyInjection\Search\Exalead\SuggestSearchResult;
use AppBundle\DependencyInjection\Search\SearchException;
use BitrixBundle\DependencyInjection\BitrixHelper;
use JMS\DiExtraBundle\Annotation as DI;

class NebSearcher
{
    const PAGE_SIZE = 15;
    const DEFAULT_METHOD = '/search-api/search';

    /**
     * @var BitrixHelper
     */
    protected $_bitrix;

    /**
     * @var SearchClient
     */
    protected $_client;

    /**
     * @var array
     */
    protected static $_suggestPathMap
        = [
            'author' => '/suggest/service/suggest_author',
            'title'  => '/suggest/service/suggest_title',
        ];

    /**
     * @param SearchClient $client
     */
    public function __construct(SearchClient $client, BitrixHelper $bitrix)
    {
        $this->_client = $client;
        $this->_bitrix = $bitrix;
    }

    /**
     * @param QueryBuilder $builder
     *
     * @return SearchResult
     */
    public function search(QueryBuilder $builder)
    {
        $url = static::DEFAULT_METHOD;
        $url .= '?' . $builder->buildQuery();

        $this->_client->getResult($url);

        return new EditionsSearchResult($this->_client->getResult($url));
    }

    /**
     * @param QueryBuilder $builder
     * @param string       $type
     *
     * @return SuggestSearchResult
     * @throws SearchException
     */
    public function suggests(QueryBuilder $builder, $type)
    {
        if (!$path = static::getSuggestPath($type)) {
            throw new SearchException("Suggestion '$type' not exists");
        }
        $url = $path;
        $url .= '?' . $builder->buildQuery();

        $this->_client->getResult($url);

        return new SuggestSearchResult($this->_client->getResult($url));
    }

    /**
     * @param string $suggest
     *
     * @return null
     */
    public static function getSuggestPath($suggest)
    {
        return isset(static::$_suggestPathMap[$suggest]) ? static::$_suggestPathMap[$suggest] : null;
    }

    /**
     * @param array $parameters
     *
     * @return QueryBuilder
     */
    public function buildSearchQuery($parameters = [])
    {
        $query = $parameters['q'];
        if (!$query) {
            $query = '#all';
        }
        $condition = new Condition($query);
        if (isset($parameters['fields'])) {
            $condition->andCondition($parameters['fields']);
        }
        $qb = new QueryBuilder($condition);
        $qb->setLimit(static::PAGE_SIZE);

        return $qb;
    }


    /**
     * @param array  $parameters
     * @param array  $componentParams
     * @param string $template
     *
     * @return string
     */
    public function buildNebSearchComponent($parameters = [], $componentParams = [], $template = '')
    {
        ob_start();
        $request = $_REQUEST;
        $get = $_GET;
        $_REQUEST = array_replace_recursive($_REQUEST, $parameters);
        $_GET = array_replace_recursive($_GET, $parameters);
        $this->_bitrix->getApplication()->IncludeComponent(
            'exalead:search.page',
            $template,
            $componentParams,
            false
        );
        $_REQUEST = $request;
        $_GET = $get;

        return ob_get_clean();
    }
}