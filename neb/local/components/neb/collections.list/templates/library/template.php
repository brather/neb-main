<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?
	if(empty($arResult['SECTIONS']))
		return false;

?>
	<?
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			$APPLICATION->RestartBuffer();
	?>
	<h2><?= GetMessage('COLLECTION_TITLE') ?></h2>
	<ul class="collections-list onecol-collection">
		<?
			if($arParams['PAGEN'] > 1)
				$n = ($arParams['PAGEN'] - 1) * $arParams['SIZEN'];
			else
				$n = 0;

			foreach($arResult['SECTIONS'] as $arSection)
			{
				if(empty($arResult['BOOKS_IN_SECTIONS'][$arSection['ID']]))
					continue;
				$n++;
			?>
			<li class="collections-list-item cls">
				<div class="cls__wrapper">
					<span class="cls__num"><?=$n?>.</span>
					<?
						if(collectionUser::isAdd()){
						?>
							<div class="cls__add-books" 
								data-add-to-favorites-widget data-section-id="<?=$arSection['ID']?>" 
								data-add-href="<?=ADD_COLLECTION_URL?>list.php?t=books_collection&id=<?=$arSection['ID']?>&action=checked" 
								data-add-toggler-text="<?=Loc::getMessage('COLLECTION_DETAIL_ADD_TO_MY_COL')?>"
								data-remove-href="<?=ADD_COLLECTION_URL?>removeBook.php?t=books_collection&id=<?=$arSection['ID']?>"
								data-remove-toggler-text="<?php echo Loc::getMessage('COLLECTION_DETAIL_REMOVE_FROM_MY_COL')?>"
							>
								<div class="cls_add-books-toggler-wrapper">
									<?php if(true === $arSection['IN_MY_COLLECTIONS']):?>
				                        <a href="<?=ADD_COLLECTION_URL?>removeBook.php?t=books_collection&id=<?=$arResult['SECTION']['ID']?>" data-remove-collection-from-favorites>
				                            <?php echo Loc::getMessage('COLLECTION_DETAIL_REMOVE_FROM_MY_COL')?>
				                        </a>
				                    <?php else: ?>
				                        <a href="<?=ADD_COLLECTION_URL?>list.php?t=books_collection&id=<?=$arResult['SECTION']['ID']?>&action=checked" data-add-collection-to-favorites>
				                            <?=Loc::getMessage('COLLECTION_DETAIL_ADD_TO_MY_COL')?>
				                        </a>
				                    <?php endif; ?>
				                </div>
							</div>
						<?
						}
					?>
					<h2><a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a></h2>
					
					<?
						if(!empty($arResult['LIBRARY'][$arSection['UF_LIBRARY']]))
						{
						?>
							<div class="cls__author"><em><?=Loc::getMessage('COLLECTION_LIST_AUTHOR'); ?>:</em> <a href="<?=$arResult['LIBRARY'][$arSection['UF_LIBRARY']]['DETAIL_PAGE_URL']?>" class="b-sorcelibrary"><?=$arResult['LIBRARY'][$arSection['UF_LIBRARY']]['NAME']?></a></div>
						<?
						}
					?>
					<div class="collections-list-item__preview clsp">
						<?
							$coverBookID = $arResult['SECTIONS_BOOKS']['COVER'][$arSection['ID']];

							if(!empty($arResult['BOOKS'][$coverBookID]))
							{

								$arItem = $arResult['BOOKS'][$coverBookID];
							?>
							<div class="clsp__cover">
								<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="clsp__image"><img src="<?=$arItem['IMAGE_URL']?>&width=131&height=194" class="b-bookboard_img loadingimg" alt="<?=$arItem['title']?>" title="<?=$arItem['title']?>"></a>
								<div class="clsp__title"><?=trimming_line($arItem['title'], 45)?></div>
								<? if (!empty($arItem['authorbook'])) { ?>
									<? foreach (
										$arItem['authorbook_for_link']
										as $key =>
										$author
									) { ?>
										<a href="/search/?f_field[authorbook]=f/authorbook/<?=urlencode(mb_strtolower(strip_tags(trim($author))))?>"
										><?= $author ?></a>
										<? if ($key + 1 < count(
												$arItem['authorbook_for_link']
											)
										) { ?>, <? } ?>
									<? } ?>
								<? } ?>
							</div>
							<?
							}

						?>
						<div class="clsp__details">
							<div class="clsp__editions-list" data-slide-editions="<?=$n?>">
								<?
									if(!empty($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']])){
										$i = 0;
										foreach($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']] as $bookID)
										{
											$arItem = $arResult['BOOKS'][$bookID];
											if(empty($arItem))
												continue;
											$i++;
										?>
										<div class="clsp__editions-list-item">
											<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
												<img src="<?=$arItem['IMAGE_URL']?>&width=82&height=140" alt="<?=$arItem['title']?>" title="<?=$arItem['title']?>">
											</a>
										</div>
										<?
											if(($i%4) == 0){
											?>
										
											<?
											}
										}
									}
								?>
							</div>
							<a href="<?=$arSection['SECTION_PAGE_URL']?>" class="btn btn-primary pull-right">
								<?=$arSection['ELEMENT_CNT']?>
								<?=GetEnding($arSection['ELEMENT_CNT'], Loc::getMessage('COLLECTION_LIST_BOOK_5'), Loc::getMessage('COLLECTION_LIST_BOOK_1'), Loc::getMessage('COLLECTION_LIST_BOOK_2'))?>
								<?=Loc::getMessage('COLLECTION_LIST_IN_COL'); ?>
							</a>
							<button type="button" class="clsp__editions-list-prev"></button>
							<button type="button" class="clsp__editions-list-next"></button>
						</div>
					</div><!-- /.collections-list-item__preview clsp -->
				</div><!-- /.cls__wrapper -->
			</li>
			<?
			}
		?>
	</ul>
	
	<?=$arResult['NAV_STRING']?>
	<?
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			exit();
	?>
