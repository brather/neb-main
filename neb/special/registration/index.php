<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Регистрация");
?>

<?$APPLICATION->IncludeComponent(
    "neb:registration",
    "special",
    Array(
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/special/registration/",
        "SEF_URL_TEMPLATES" => Array("main"=>"/","full"=>"full/","rgb"=>"rgb/","ecia"=>"ecia/","simple"=>"simple/"),
        "VARIABLE_ALIASES" => Array("main"=>Array(),"full"=>Array(),"rgb"=>Array(),"ecia"=>Array(),"simple"=>Array(),),
        "VARIABLE_ALIASES" => Array(
            "main" => Array(),
            "full" => Array(),
            "rgb" => Array(),
            "ecia" => Array(),
            "simple" => Array(),
        )
    )
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>