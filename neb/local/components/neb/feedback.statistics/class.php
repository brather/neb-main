<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 15.09.2016
 * Time: 15:00
 */

use \Bitrix\Main\Application,
    \Neb\Main\Helper\MainHelper;

CBitrixComponent::includeComponentClass('neb:feedback');

/**
 * Class FeedbackStatisticsComponent
 */
class FeedbackStatisticsComponent extends FeedbackComponent
{
    private $componentPage;
    private $arRequestForm;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {

        if ($arParams['TIME'] == 'Y')
            $this->componentPage = 'time';
        else
            $this->componentPage = 'total';

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent() {

        $this->includeModules(['form']);
        $this->getUser();
        $this->getRequest();
        $this->getRequestForm();
        $this->getForm(['SID'=> 'SIMPLE_FORM_1'], ['STATUSES']);
        $this->getStatistics();

        if ($this->arRequestForm['EXPORT'] == 'Y')
            $this->getExport();
        else
            $this->includeComponentTemplate($this->componentPage);
    }

    /**
     * Получение данных формы
     */
    private function getRequestForm() {

        $arResult = [];

        if ($this->arRequest['FEEDBACK']['ACTION'] == 'STAT')
            foreach ($this->arRequest['FEEDBACK'] as $sCode => $sVal)
                $arResult[$sCode] = $sVal;

        $this->arRequestForm = $arResult;
    }

    /**
     * Получение полной статистики
     * @return array
     */
    private function getStatistics() {

        $arResult = [];

        $sDateFrom = $this->arRequestForm['DATE_FROM'];
        $sDateTo   = $this->arRequestForm['DATE_TO'];
        
        if (empty($sDateFrom) && empty($sDateTo))
            return $arResult;

        if (empty($this->arForm['STATUSES']))
            $this->arResult['ERROR'][] = 'Не найдено ни одного статуса веб-формы!';

        // запрос сгруппированного количества статусов формы обратной связи
        if (empty($this->arResult['ERROR'])) {

            $obFormResult = new CFormResult();

            // СТАТИСТИКА ПО ВРЕМЕНИ
            if ($this->arParams['TIME'] == 'Y') {

                $totalTime = 0;

                foreach ($this->arForm['STATUSES'] as $arStatus) {
                    if ($arStatus['DESCRIPTION'] !== 'CLOSED')
                        continue;

                    // получение массива результатов формы
                    $arTickets = [];
                    $arFilter = [
                        "STATUS_ID"   => $arStatus['ID'],
                        "TIMESTAMP_1" => $sDateFrom,
                        "TIMESTAMP_2" => $sDateTo
                    ];
                    if ($this->iUserLibrary)
                        $arFilter["FIELDS"][] = ["CODE" => "FB_LIBRARY_ID", "VALUE" => $this->iUserLibrary];
                    
                    $rsOrder = $obFormResult->GetList(
                        $this->arForm['ID'], $by = "s_id", $order = "asc", $arFilter, $is_filtered, "N"
                    );
                    while ($arFields = $rsOrder->Fetch()) {
                        $arFields['TIME'] = strtotime($arFields['TIMESTAMP_X']) - strtotime($arFields['DATE_CREATE']);
                        $totalTime += $arFields['TIME'];

                        $arTickets[$arFields['ID']] = $arFields;
                    }
                    unset($arFilter, $rsOrder, $arFields);

                    $arUsers = $this->getReplyedUsers($this->arForm['ID'], array_keys($arTickets));

                    // формирование результата
                    foreach ($arTickets as $arTicket) {
                        $arTicket['USER'] = $arUsers[$arTicket['ID']];
                        $arResult[$arTicket['ID']] = $arTicket;
                    }
                }

                $this->arResult['COUNT']   = count($arResult);
                $this->arResult['AVERAGE'] = $this->arResult['COUNT'] > 0 ? $totalTime / $this->arResult['COUNT'] : 0;
                $this->arResult['ITEMS']   = $arResult;
            }
            // СТАТИСТИКА ПО ЗАЯВКАМ
            else {

                foreach ($this->arForm['STATUSES'] as $arStatus) {

                    if ($arStatus['DESCRIPTION'] === 'REGISTERED') {
                        $arFilter = [
                            "DATE_CREATE_1" => $sDateFrom,
                            "DATE_CREATE_2" => $sDateTo
                        ];
                    } else {
                        $arFilter = [
                            "STATUS_ID"   => $arStatus['ID'],
                            "TIMESTAMP_1" => $sDateFrom,
                            "TIMESTAMP_2" => $sDateTo
                        ];
                    }
                    if ($this->iUserLibrary)
                        $arFilter["FIELDS"][] = ["CODE" => "FB_LIBRARY_ID", "VALUE" => $this->iUserLibrary];

                    $arStatus['COUNT'] = intval($obFormResult->GetList(
                        $this->arForm['ID'], $by = "s_id", $order = "asc", $arFilter, $is_filtered, "N"
                    )->SelectedRowsCount());

                    $arResult[$arStatus['DESCRIPTION']] = $arStatus;
                }

                $this->arResult['ITEMS'] = $arResult;
            }
        }
    }

    /**
     * Получнеие массива исполнителей для конкретных ответов конкретной формы
     *
     * @param $iForm
     * @param $arResults
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    private static function getReplyedUsers($iForm, $arResults) {

        $arResult = $arUsers = [];

        // получение поля USER_ID из формы обратной связи
        $obFormField  = new CFormField();
        $arField = $obFormField->GetBySID('FB_USER_ID', $iForm)->Fetch();

        // получение массива идентификаторов назначенных исполнителей к результатам формы
        if (!empty($arResults)) {
            $sQuery = '
              SELECT
                RESULT_ID, 
                USER_TEXT
              FROM
                b_form_result_answer
              WHERE
                FIELD_ID = ' . $arField['ID'] . '
                    AND RESULT_ID IN (' . implode(',', $arResults) . ');';
            $rsRows = Application::getConnection()->query($sQuery);
            while ($arRow = $rsRows->fetch())
                $arResult[$arRow['RESULT_ID']] = $arRow['USER_TEXT'];
        }

        // получение массива пользователей
        if (!empty($arResult))
            $arUsers = static::getUsers(array_values($arResult));

        // простановка в соответствие результату данных о пользователе
        foreach ($arResult as &$user)
            $user = $arUsers[$user];

        return $arResult;
    }

    /**
     * Экспорт данных в Excel
     */
    private function getExport() {

        $arExcel = [];

        if ($this->arParams['TIME'] == 'Y') {

            $arExcel['META']['TITLE'] = 'Статистика по срокам обработки заявок';
            $arExcel['META']['DESCRIPTION'] = "Статистика по срокам обработки заявок c " . $this->arRequestForm['DATE_FROM'] . ' по ' . $this->arRequestForm['DATE_TO'];
            $arExcel['META']['FILENAME'] = $arExcel['META']['TITLE'];
            $arExcel['ITEMS'][] = ['A' => $arExcel['META']['DESCRIPTION']];
            $arExcel['ITEMS'][] = ['A' => "ID заявки", 'B' => "Исполнитель", 'C' => 'Срок исполнения'];
            foreach ($this->arResult['ITEMS'] as $arItem) {
                $arExcel['ITEMS'][] = [
                    'A' => $arItem['ID'],
                    'B' => trim($arItem['USER']['LAST_NAME'].' '.$arItem['USER']['NAME'].' '.$arItem['USER']['SECOND_NAME']),
                    'C' => str_replace(['#DAYS#', '#HOURS#', '#MINUTES#', '#SECONDS#'], ['д', 'ч', 'мин', 'сек'], static::getTime($arItem['TIME']))
                ];
            }
            $arExcel['ITEMS'][] = ['A' => ""];
            $arExcel['ITEMS'][] = ['A' => "Всего заявок закрыто за период: " . intval($this->arResult['COUNT'])];
            $arExcel['ITEMS'][] = ['A' => "Средний срок отработки заявки: " . str_replace(
                    ['#DAYS#', '#HOURS#', '#MINUTES#', '#SECONDS#'], ['д', 'ч', 'мин', 'сек'], static::getTime($this->arResult['AVERAGE'])
                )];

        } else {

            $arExcel['META']['TITLE'] = 'Статистика по количеству заявок';
            $arExcel['META']['DESCRIPTION'] =  "Статистика по количеству заявок c " . $this->arRequestForm['DATE_FROM'] . ' по ' . $this->arRequestForm['DATE_TO'];
            $arExcel['META']['FILENAME'] = $arExcel['META']['TITLE'];
            $arExcel['ITEMS'][] = ['A' => $arExcel['META']['DESCRIPTION']];
            $arExcel['ITEMS'][] = ['A' => "Подано заявок", 'B' => intval($this->arResult['ITEMS']['REGISTERED']['COUNT'])];
            $arExcel['ITEMS'][] = ['A' => "Закрыто заявок", 'B' => intval($this->arResult['ITEMS']['CLOSED']['COUNT'])];
            $arExcel['ITEMS'][] = ['A' => "Заявок в работе*", 'B' => intval($this->arResult['ITEMS']['PROCESSED']['COUNT'])];
            $arExcel['ITEMS'][] = ['A' => ""];
            $arExcel['ITEMS'][] = ['A' => "* история статусов \"в работе\" ведется с 22.08.2016"];
        }
        
        static::export2Excel($arExcel);
    }

    /**
     * Экспорт данных в Excel
     */
    private static function export2Excel($arExcel) {

        // Выполняет сбпрос буффера
        MainHelper::clearBuffer();

        // Генерация Excel-файла
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel.php");
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel/IOFactory.php");

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("НЭБ.РФ")
            ->setLastModifiedBy("НЭБ.РФ")
            ->setTitle($arExcel['META']['TITLE'])
            ->setSubject($arExcel['META']['TITLE'])
            ->setDescription($arExcel['META']['DESCRIPTION']);

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Статистика по обращениям');

        foreach ($arExcel['ITEMS'] as $row => $arItem)
            foreach ($arItem as $sCode => $sValue)
                $objPHPExcel->getActiveSheet()->setCellValue($sCode . ($row + 1), $sValue);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$arExcel['META']['FILENAME'].'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

        exit;
    }

    /**
     * Формирование оформленной строки со временем
     * @param $iTime
     * @return string
     */
    public static function getTime($iTime) {

        $days = floor($iTime / (3600 * 24));
        $hour = floor(($iTime - $days * 3600 * 24) / 3600);
        $sec  = $iTime - ($hour * 3600) - $days * 3600 * 24;
        $min  = floor($sec / 60);
        $sec  = $sec - ($min * 60);

        $sResult = '';
        if ($days > 0)
            $sResult .= $days . '#DAYS# ';
        if ($hour > 0)
            $sResult .= $hour . '#HOURS# ';
        if ($min > 0)
            $sResult .= $min . '#MINUTES# ';
        if ($sec > 0)
            $sResult .= $sec . '#SECONDS# ';

        return trim($sResult);
    }
}