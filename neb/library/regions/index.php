<?php
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
/**
 * @var array $arResult
 * @var CMain $APPLICATION
 */
$APPLICATION->IncludeComponent(
    'neb:regions', 'library', array()
);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");