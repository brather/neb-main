<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\UserTable,
    \Neb\Main\Helper\MainHelper;

$strError = '';
$arResult = $arStatuses = [];

$arMessage = [
    1 => 'token not found',
    2 => 'token has expired'
];

if (!nebUser::checkElar() && $_SERVER['REQUEST_METHOD'] != 'POST')
    $strError = 'Error method';

if (empty($_REQUEST['token']))
    $strError = $arMessage[1];

if (empty($strError)) {
    $token = trim($_REQUEST['token']);

    //22 == свойсто статус пользователя
    $obUserFieldEnum = new CUserFieldEnum();
    $rsStatus = $obUserFieldEnum->GetList(['SORT' => 'ASC'], ['USER_FIELD_ID' => '22']);
    while ($arStatus = $rsStatus->Fetch())
        $arStatuses[$arStatus['ID']] = $arStatus['XML_ID'];

    $arUser = UserTable::getList([
        'filter' => ['UF_TOKEN' => $token, 'ACTIVE' => 'Y'],
        'select' => [
            'ID', 'LOGIN', 'EMAIL', 'LAST_NAME', 'NAME', 'SECOND_NAME', 'DATE_REGISTER',
            'UF_TOKEN_ADD_DATE', 'UF_TOKEN', 'UF_STATUS', 'UF_RGB_USER_ID'
        ]
    ])->fetch();

    if (!empty($arUser)) {

        // получение символьных кодов групп пользователя (для модуля разметки изданий)
        $obUser           = new nebUser($arUser['ID']);
        $arUser['GROUPS'] = array_values($obUser->getUserGroups()); // только коды групп

        $arUser['UF_STATUS'] = $arStatuses[$arUser['UF_STATUS']]; // статус пользователя
        $arUser['DATE_REGISTER'] = (new DateTime($arUser['DATE_REGISTER']))->toString();

        $date = $arUser['UF_TOKEN_ADD_DATE'];
        $stmp = (new DateTime($date))->add('+' . $arParams['HOURS_LIMIT'] . ' hours')->getTimestamp(); // прибавка даты

        if (time() < $stmp) {

            $arUser['TOKEN'] = $arUser['UF_TOKEN'];

            unset($arUser['UF_TOKEN'], $arUser['UF_TOKEN_ADD_DATE']);

            $arUser['USER_IP'] = $_SERVER['REMOTE_ADDR'];

            $arResult = $arUser;

        } else {
            $strError = $arMessage[2];
        }

    } else {
        $strError = $arMessage[1];
    }
}

MainHelper::showJson(['result' => $arResult, 'error' => $strError]);