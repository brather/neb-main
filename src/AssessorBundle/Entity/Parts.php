<?php

namespace AssessorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parts
 *
 * @ORM\Table(name="parts")
 * @ORM\Entity
 */
class Parts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdBiblioCard", type="integer", nullable=true)
     */
    private $idbibliocard;

    /**
     * @var string
     *
     * @ORM\Column(name="Author", type="text", nullable=true)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="Title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="Title2", type="text", nullable=true)
     */
    private $title2;

    /**
     * @var integer
     *
     * @ORM\Column(name="StartPage", type="integer", nullable=true)
     */
    private $startpage;

    /**
     * @var integer
     *
     * @ORM\Column(name="FinishPage", type="integer", nullable=true)
     */
    private $finishpage;

    /**
     * @var string
     *
     * @ORM\Column(name="RangePage", type="string", length=255, nullable=true)
     */
    private $rangepage;

    /**
     * @var string
     *
     * @ORM\Column(name="KeyWords", type="string", length=255, nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="FullSymbolicId", type="string", length=100, nullable=true)
     */
    private $fullsymbolicid;

    /**
     * @var string
     *
     * @ORM\Column(name="Path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var integer
     *
     * @ORM\Column(name="Position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="AuthorRaiting", type="integer", nullable=true)
     */
    private $authorraiting;

    /**
     * @var integer
     *
     * @ORM\Column(name="TitleRaiting", type="integer", nullable=true)
     */
    private $titleraiting;

    /**
     * @var string
     *
     * @ORM\Column(name="AuthorNorm", type="text", nullable=true)
     */
    private $authornorm;

    /**
     * @var string
     *
     * @ORM\Column(name="TitleNorm", type="text", nullable=true)
     */
    private $titlenorm;


}
