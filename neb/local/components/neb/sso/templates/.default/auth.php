<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\UserTable,
    \Neb\Main\Helper\MainHelper;

$strError = '';
$arResult = $arStatuses = [];

$arMessage = array(
    1 => 'Логин или пароль введен не верно',
    2 => 'Не заполнен логин или пароль'
);

if (!nebUser::checkElar() && $_SERVER['REQUEST_METHOD'] != 'POST')
    $strError = 'Error method';

if (empty($strError)) {
    $login = trim($_REQUEST['email']);
    $password = trim($_REQUEST['password']);
    if (empty($login) || empty($password))
        $strError = $arMessage[2];
}

if (empty($strError)) {

    //22 == свойсто статус пользователя
    $obUserFieldEnum = new CUserFieldEnum();
    $rsStatus = $obUserFieldEnum->GetList(['SORT' => 'ASC'], ['USER_FIELD_ID' => '22']);
    while ($arStatus = $rsStatus->Fetch())
        $arStatuses[$arStatus['ID']] = $arStatus['XML_ID'];

    // проверка по email или номеру ЕЧБ
    $arUser = UserTable::getList([
        'filter' => [['LOGIC'=>'OR', ['=EMAIL' => $login], ['=UF_NUM_ECHB' => $login]], 'ACTIVE' => 'Y'],
        'select' => [
            'ID', 'LOGIN', 'PASSWORD', 'EMAIL', 'LAST_NAME', 'NAME', 'SECOND_NAME', 'DATE_REGISTER',
            'UF_STATUS', 'UF_RGB_USER_ID', 'UF_NUM_ECHB'
        ]
    ])->fetch();

    if (!empty($arUser)) {

        // получение символьных кодов групп пользователя (для модуля разметки изданий)
        $obUser           = new nebUser($arUser['ID']);
        $arUser['GROUPS'] = array_values($obUser->getUserGroups()); // только коды групп

        $arUser['DATE_REGISTER'] = (new DateTime($arUser['DATE_REGISTER']))->toString();
        $arUser['UF_STATUS'] = $arStatuses[$arUser['UF_STATUS']];

        // проверяем валидность пароля
        if (strlen($arUser["PASSWORD"]) > 32) {
            $salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
            $db_password = substr($arUser["PASSWORD"], -32);
        } else {
            $salt = "";
            $db_password = $arUser["PASSWORD"];
        }

        $user_password = md5($salt . $password);

        if ($db_password === $user_password) {

            unset($arUser['PASSWORD']);
            $arResult = $arUser;
            $arResult['TOKEN'] = md5(serialize($arUser) . randString(10));

            $obUser = new CUser;
            if (true === filter_var($_REQUEST['doAuth'], FILTER_VALIDATE_BOOLEAN)) {
                $obUser->Authorize($arUser['ID']);
            }
            $arFields = [
                "UF_TOKEN"          => $arResult['TOKEN'],
                "UF_TOKEN_ADD_DATE" => (new DateTime())->toString(),
            ];
            $obUser->Update($arUser['ID'], $arFields);

            $arResult['USER_IP'] = $_SERVER['REMOTE_ADDR'];
        } else {
            $strError = $arMessage[1];
        }
    } else {
        $strError = $arMessage[1];
    }
}

MainHelper::showJson(['result' => $arResult, 'error' => $strError]);