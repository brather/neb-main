<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);
Asset::getInstance()->addJs('/local/tools/ckeditor/ckeditor.js');

$edit = isset($arResult['ITEM']);

CJSCore::Init(array('date'));

?>
<? /*=$edit ? GetMessage('LIBRARY_NEWS_EDIT_MODE_EDIT') : GetMessage('LIBRARY_NEWS_EDIT_MODE_ADD');*/ ?>
<form action="<?= $APPLICATION->GetCurPage(); ?>" class="new-article-fomr naf" method="post" name="form_edit_news">
    <script>
        var rulesAndMessages = {
            rules: {},
            messages: {}
        };
    </script>
    <input type="hidden" name="ID" value="<?= $edit ? $arResult['ITEM']['ID'] : '' ?>"/>
    <input type="hidden" name="CODE" value="<?= $arResult['ITEM']['CODE'] ?>"/>
    <input type="hidden" name="LIBRARY_ID" value="<?= $arResult['LIBRARY']['ID'] ?>">
    <?= bitrix_sessid_post() ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="inputNAME">
                    <em class="hint">*</em>
                    <?= GetMessage('LIBRARY_NEWS_EDIT_TITLE'); ?>
                </label>
                <input type="text"
                       data-required="required"
                       value="<?= isset($_POST['NAME']) ? $_POST['NAME'] : $arResult['ITEM']['NAME'] ?>"
                       id="inputNAME"
                       data-maxlength="255"
                       data-minlength="2"
                       name="NAME"
                       class="form-control">
            </div>
            <script>
                rulesAndMessages.rules["NAME"] = {
                    required: true,
                    maxlength: 255,
                    minlength: 2
                };
                rulesAndMessages.messages["NAME"] = {
                    required: "<?=GetMessage('LIBRARY_NEWS_EDIT_FILL');?>",
                    maxlength: "<?=GetMessage('LIBRARY_NEWS_EDIT_MAX_LEN_255');?>",
                    minlength: "Не менее 2 символов"
                };
            </script>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>
                    <em class="hint">*</em>
                    <?= GetMessage('LIBRARY_NEWS_EDIT_SHORT_DESC'); ?>
                </label>
					<textarea
                        name="PREVIEW_TEXT"
                        id="iPREVIEW_TEXT"
                        data-maxlength="1500"
                        data-minlength="2"
                        class="form-control"
                        rows="5"
                    ><?= isset($_POST['PREVIEW_TEXT']) ? strip_tags($_POST['PREVIEW_TEXT']) : strip_tags($arResult['ITEM']['PREVIEW_TEXT']) ?></textarea>
            </div>
            <script>
                rulesAndMessages.rules["PREVIEW_TEXT"] = {
                    required: true,
                    maxlength: 1500,
                    minlength: 2
                };
                rulesAndMessages.messages["PREVIEW_TEXT"] = {
                    required: "<?=GetMessage('LIBRARY_NEWS_EDIT_FILL');?>",
                    maxlength: "<?=GetMessage('LIBRARY_NEWS_EDIT_MAX_LEN_1500');?>",
                    minlength: "Не менее 2 символов"
                };
            </script>
        </div>
    </div>
    <? $APPLICATION->IncludeComponent(
        "notaext:plupload",
        "library_news_detail",
        array(
            "MAX_FILE_SIZE" => "24",
            "FILE_TYPES" => "jpg,jpeg,png",
            "DIR" => "libnews_detail",
            "FILES_FIELD_NAME" => "DETAIL_PICTURE",
            "MULTI_SELECTION" => "N",
            "CLEANUP_DIR" => "N",
            "UPLOAD_AUTO_START" => "Y",
            "RESIZE_IMAGES" => "Y",
            "RESIZE_WIDTH" => "300",
            "RESIZE_HEIGHT" => "300",
            "RESIZE_CROP" => "N",
            "RESIZE_QUALITY" => "98",
            "UNIQUE_NAMES" => "Y",
            'DETAIL_PICTURE' => $arResult['ITEM']['DETAIL_PICTURE']['SRC']
        ),
        false
    ); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="iDETAIL_TEXT">
                    <em class="hint">*</em>
                    <?= GetMessage('LIBRARY_NEWS_EDIT_TEXT'); ?>
                </label>
					<textarea
                            aria-required="true"
                            required="required"
                            data-required="required"
                        data-maxlength="50000"
                        data-minlength="2"
                        name="DETAIL_TEXT"
                        class="form-control"
                        id="iDETAIL_TEXT"
                    >
						<?= isset($_POST['DETAIL_TEXT']) ? $_POST['DETAIL_TEXT'] : $arResult['ITEM']['DETAIL_TEXT'] ?>
					</textarea>
            </div>
            <script>
                rulesAndMessages.rules["DETAIL_TEXT"] = {
                    required: true,
                    maxlength: 50000,
                    minlength: 2
                };
                rulesAndMessages.messages["DETAIL_TEXT"] = {
                    required: "<?=GetMessage('LIBRARY_NEWS_EDIT_FILL');?>",
                    maxlength: "<?=GetMessage('LIBRARY_NEWS_EDIT_MAX_LEN');?>",
                    minlength: "<?=GetMessage('LIBRARY_NEWS_EDIT_MIN_LEN');?>"
                };
            </script>
        </div>
    </div>

    <div class="row">

        <div class="form-group col-xs-6 col-md-3">
            <label for="pub_date">
                <em class="hint">*</em>
                <?= GetMessage('LIBRARY_NEWS_EDIT_DATE'); ?>
            </label>
            <em style="display: none;" class="error required"><?= GetMessage('LIBRARY_NEWS_EDIT_FILL'); ?></em>
            <input
                type="text"
                name="DATE_ACTIVE_FROM"
                id="article_pub_date"
                class="form-control"
                onclick="BX.calendar({node: this, field: article_pub_date, form: history, bTime: false, value: ''});"
                value="<?= isset($_POST['DATE_ACTIVE_FROM']) ? $_POST['DATE_ACTIVE_FROM'] : $arResult['ITEM']['DATE_ACTIVE_FROM'] ?>"
            >
        </div>
        <script>
            rulesAndMessages.rules["DATE_ACTIVE_FROM"] = {
                required: true,
                maxlength: 1500,
                minlength: 2
            };
            rulesAndMessages.messages["DATE_ACTIVE_FROM"] = {
                required: "<?=GetMessage('LIBRARY_NEWS_EDIT_FILL');?>",
                maxlength: "<?=GetMessage('LIBRARY_NEWS_EDIT_MAX_LEN_1500');?>",
                minlength: "<?=GetMessage('LIBRARY_NEWS_EDIT_MIN_LEN');?>"
            };
        </script>
        <? //=CalendarDate("DATE_ACTIVE_FROM", isset($_POST['DATE_ACTIVE_FROM'])?$_POST['DATE_ACTIVE_FROM']:$arResult['ITEM']['DATE_ACTIVE_FROM'], "form_edit_news", "15", 'data-required="required"')?>
    </div>
    <? $APPLICATION->IncludeComponent(
        "notaext:plupload",
        "library_news_gallery",
        array(
            "MAX_FILE_SIZE" => "24",
            "FILE_TYPES" => "jpg,jpeg,png",
            "DIR" => "libnews_gallery",
            "FILES_FIELD_NAME" => "GALLERY",
            "MULTI_SELECTION" => "Y",
            "CLEANUP_DIR" => "N",
            "UPLOAD_AUTO_START" => "Y",
            "RESIZE_IMAGES" => "Y",
            "RESIZE_WIDTH" => "600",
            "RESIZE_HEIGHT" => "340",
            "RESIZE_CROP" => "Y",
            "RESIZE_QUALITY" => "98",
            "UNIQUE_NAMES" => "Y",
            'GALLERY' => $arResult['ITEM']['GALLERY']
        ),
        false
    ); ?>
    <? /*<div class="fieldcell nowrap photorow">
			<label>Прикрепить фотогалерею</label>
			<div class="field validate">
				<div class="setscan iblock">
					<input type="file" class="photofile" name="" id="" multiple >
					<a href="#">Загрузить изображение (одно или несколько)</a>
					<div class="setphoto_lb">или перетащите на это поле</div>
				</div>
			</div>
			<div class="b_photobl">
				<div class="b_photo iblock">
					<img src="./pic/pic_43.jpg" alt="photo">
				</div>
				<div class="descrphoto iblock">
					<label for="settings13">Описание фотографии</label>
					<div class="field validate">
						<textarea class="input" data-minlength="2" data-maxlength="1500" value="" id="settings13" name=""></textarea>
					</div>
				</div>
			</div>
			<div class="b_photobl">
				<div class="b_photo iblock">
					<img src="./pic/pic_44.jpg" alt="photo">
					<div class="progressbar"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружается <span class="num">33</span>%</div></div>
				</div>
				<div class="descrphoto iblock">
					<label for="settings12">Описание фотографии</label>
					<div class="field validate">
						<textarea class="input" rows="1" cols="1" data-minlength="2" data-maxlength="1500" value="" id="settings12" name=""></textarea>
					</div>
				</div>
			</div>
		</div>*/ ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="iPROPERTY_YOUTUBE_VALUE">
                    <?= GetMessage('LIBRARY_NEWS_EDIT_ATTACH'); ?>
                </label>
                <input
                    type="text"
                    name="YOUTUBE"
                    class="form-control"
                    id="iPROPERTY_YOUTUBE_VALUE"
                    value="<?= isset($_POST['YOUTUBE']) ? $_POST['YOUTUBE'] : $arResult['ITEM']['PROPERTY_YOUTUBE_VALUE'] ?>"
                >
            </div>
        </div>
    </div>

    <? $APPLICATION->IncludeComponent(
        "notaext:plupload",
        "library_news_file",
        array(
            "MAX_FILE_SIZE" => "24",
            "FILE_TYPES" => "jpg,jpeg,png,doc,docx,ppt,xls,xlsx,pdf",
            "DIR" => "libnews_file",
            "FILES_FIELD_NAME" => "FILE",
            "MULTI_SELECTION" => "N",
            "CLEANUP_DIR" => "N",
            "UPLOAD_AUTO_START" => "Y",
            "RESIZE_IMAGES" => "Y",
            "RESIZE_WIDTH" => "800",
            "RESIZE_HEIGHT" => "600",
            "RESIZE_CROP" => "Y",
            "RESIZE_QUALITY" => "98",
            "UNIQUE_NAMES" => "N",
            'FILE' => $arResult['ITEM']['FILE']
        ),
        false
    ); ?>

    <div class="row" style="margin-top: 2em;">

        <div class="col-xs-6 col-md-3">
            <div class="checkwrapper">
                <label class="checkbox-label">
                    <input
                        class="checkbox"
                        type="checkbox"
                        name="ACTIVE"
                        value="Y"
                        <?= (isset($_POST['ACTIVE']) ? $_POST['ACTIVE'] : $arResult['ITEM']['ACTIVE']) === 'Y' ? 'checked="checked"' : '' ?>
                        id="iACTIVE"
                    >
						<span class="lbl">
							<?= GetMessage('LIBRARY_NEWS_EDIT_PUBLISH'); ?>
						</span>
                </label>
            </div>
        </div>
        <div class="col-xs-6">
            <a href="" id="preview_show_button"
               data-preview="/library/<?= $arResult['LIBRARY']['CODE'] ?>/news-preview-preview/"><?= GetMessage('LIBRARY_NEWS_EDIT_PREVIEW'); ?></a><br/>
        </div>
        <!-- <div class="col-xs-4">
				<?= GetMessage('LIBRARY_NEWS_EDIT_GO_TO'); ?> <a href="<?= $arParams['URL_LIST'] ?>"><?= GetMessage('LIBRARY_NEWS_EDIT_NEWS_LIST'); ?></a>
			</div>	 -->
    </div>

    <div class="row" style="margin-top: 2em;">
        <div class="col-xs-3">
            <button
                class="btn btn-primary btn-lg"
                value="1"
                type="submit"
                onclick="putEditorData(); return true;"><?= GetMessage('LIBRARY_NEWS_EDIT_SAVE'); ?></button>
        </div>
        <div class="col-xs-3 col-xs-offset-6">
            <a href="/profile/news/"
               class="btn btn-default btn-lg pull-right"><?= GetMessage('LIBRARY_NEWS_EDIT_CANCEL'); ?></a>
        </div>
    </div>

    <div class="fieldrow nowrap fieldrowaction">
        <div class="field clearfix">


        </div>
    </div>
</form>
<p style="margin-top: 2em;"><?= GetMessage('LIBRARY_NEWS_EDIT_REQ'); ?></p>

<script>
    $(function () {

        var $articleform = $('form[name="form_edit_news"');

        $articleform.on('submit', function () {
            /*console.log('triggered submmit')*/
        });

        /*console.log(articleform.length);*/
        $articleform.validate({
            ignore: [],
            rules: rulesAndMessages.rules,
            messages: rulesAndMessages.messages,
            errorPlacement: function (error, element) {
                if (element.attr('type') == "radio") {
                    error.appendTo($(element).closest('.form-group').find('label')[0]);
                } else {
                    error.appendTo(element.parent());
                }

                $(element).closest('.form-group').toggleClass('has-error', true);
            },
            /* specifying a submitHandler prevents the default submit, good for the demo
                   submitHandler: function(form) {
                       console.log("valid!");
                   },
             set this class to error-labels to indicate valid fields*/
            success: function (label) {
                /* set &nbsp; as text for IE
                label.html("&nbsp;").addClass("resolved"); */
                label.remove()
            },
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                $(element).parent().find("." + errorClass).removeClass("resolved");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
            }
        });

        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].on('change', function() {
                CKEDITOR.instances[i].updateElement();
                $(CKEDITOR.instances[i].element.$).trigger('change');
            });
        }

        $(document).on('change', '#iDETAIL_TEXT', function(){
            console.log('fire');
            $('form[name="form_edit_news"]').validate().element(this);
        });

        $('#article_pub_date').on('change input', function () {
            $('form[name="form_edit_news"]').validate().element(this);
        });

    });
</script>