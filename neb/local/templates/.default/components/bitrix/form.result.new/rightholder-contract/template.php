<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult["FORM_HEADER"] = str_replace(
    '<form', '<form class="b-form b-form_common b-feedbackform"',
    $arResult["FORM_HEADER"]
);

?>
<style>
    .field  input {
        display: block;
        opacity: 1;
        position: relative;
        float: left;
        margin-right: 10px;
    }
    .fieldcell label{
        margin-top: 15px;
    }
    #field_USE_WAYS label.radio{
        float: left;
        margin-left: -10px;
    }

</style>
<script type="text/javascript">
    $(function() {
        $('#field_USE_WAYS input').addClass('radio').parent().addClass('radio');
    });
</script>
<section class="innersection innerwrapper searchempty clearfix">
    <div class="b-mainblock left">
        <div class="b-plaintext">
            <? if ($arResult["isFormErrors"] == "Y"
            ): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
            <?= $arResult["FORM_HEADER"] ?>

            <?
            foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') { ?>
                    <div id="field_<?= $FIELD_SID ?>">
                        <? echo $arQuestion["HTML_CODE"]; ?>
                    </div>
                <? } else { ?>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <?if ($arQuestion["REQUIRED"]
                                == "Y"
                            ) { ?>
                                <em class="hint">*</em>
                            <?
                            } ?>
                            <label
                                for="bt_captcha"><?= $arQuestion["CAPTION"] ?>
                                <?= $arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y"
                                    ?
                                    "<br />" . $arQuestion["IMAGE"]["HTML_CODE"]
                                    : "" ?>:</label>

                            <div id="field_<?= $FIELD_SID ?>"
                                 class="field validate <?= $arQuestion['FIELD_CLASS'] ?>">
                                <?= $arQuestion["HTML_CODE"] ?>
                            </div>
                        </div>
                    </div>
                <?
                }
            }
            ?>
            <hr>
            <input type="hidden" name="captcha_sid"
                   value="<?= htmlspecialcharsbx(
                       $arResult["CAPTCHACode"]
                   ); ?>"/>

            <div class="fieldrow nowrap">
                <div class="fieldcell iblock">
                    <em class="hint">*</em>
                    <label for="bt_captcha"><?= GetMessage(
                            "FORM_FIELD_CAPTCHA_CODE"
                        ) ?>:</label>

                    <div class="field validate captcha">
                        <input type="text" class="input" name="captcha_word"
                               id="bt_captcha" value=""
                               data-required="required">
                        <img
                            src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx(
                                $arResult["CAPTCHACode"]
                            ); ?>" width="180" height="40"/>
                    </div>
                </div>
            </div>

            <br>
            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell ">
                    <div class="field clearfix">
                        <button class="btn btn-primary" value="1" type="submit"
                                name="web_form_submit"><?= GetMessage(
                                "FORM_BUTTON_SUBMIT"
                            ) ?>
                        </button>

                    </div>
                </div>
            </div>

            <?= $arResult["FORM_FOOTER"] ?>
        </div>
    </div>
</section>
