<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Назначение ответственного по заявке
 */

$APPLICATION->IncludeComponent(
    "neb:feedback.appoint",
    ".default",
    array(
        "COMPONENT_TEMPLATE"   => ".default",
        'FEEDBACK_URL'         => $arParams['SEF_FOLDER'],
        'FEEDBACK_APPOINT_URL' => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['appoint']
    ),
    $component
);