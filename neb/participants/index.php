<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('PARTICIPANTS_PAGE_TITLE'));
\Bitrix\Main\Page\Asset::getInstance()->addCss('/local/templates/adaptive/css/library-map.css');

$APPLICATION->IncludeComponent(
		"neb:library.list",
		"",
		Array(
				"IBLOCK_TYPE" => 'library',
				"IBLOCK_ID" => 4,
				"CACHE_TIME" => 3600,
				"ROWS_PER_PAGE" => "20",
				"DETAIL_URL" => '/library/#CODE#/',
				"LIBRARY_TYPE" => "participants"
		),
		false
);
?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>