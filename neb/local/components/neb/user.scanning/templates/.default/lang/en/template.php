<?
$MESS['SCANNING_TITLE'] = 'Orders for scanning';
$MESS['SCANNING_ORDER_DATE'] = 'Order date';
$MESS['SCANNING_NUMBER_IMAGES'] = 'Number <br />images';
$MESS['SCANNING_ORDER_AMOUNT'] = 'Order <br />amount';
$MESS['SCANNING_PREVIEW'] = 'Preview';
$MESS['SCANNING_STATUS'] = 'Status';
$MESS['SCANNING_CURRENCY'] = 'rub.';
$MESS['SCANNING_SEE'] = 'See';
$MESS['SCANNING_PAY'] = 'Pay';
$MESS['SCANNING_PAY_UP'] = 'up';
$MESS['SCANNING_PAID'] = 'Paid';
