$(function() {

	$(document).on('click','[data-add-books-to-date]',function(e){
		e.preventDefault();
	    var toggler = $(this),
	        toggled = e.target,
			modalMarkup = $('#universal-modal').clone(true).insertAfter('#universal-modal');
    	modalMarkup.removeAttr('id');

	    $(modalMarkup)
	    .one('show.bs.modal', function(e){
	        var modal = $(this),
	            button = $(e.relatedTarget),
	            iframeSrc = button.attr('href'),
	            iframe = $('<iframe/>').attr('src',iframeSrc).css({
	                'height':'100%',
	                'width':'100%',
	                'border':'none'
	            });

	        modal
	            .find('.modal-title').text('Добавить книгу')
	            .end().find('.modal-dialog').addClass('fixed-modal modal-lg no-footer-bar')
	            .end().find('.modal-footer').remove()
	            .end().find('.modal-body').css('overflow','hidden').append(iframe);
	    })
	    .one('hidden.bs.modal', function(e){
	        var modal = $(this);
	        $(modalMarkup).detach();

	        if ($('#milk-shadow').length == 0) {
	            el = $('<div/>').attr('id','milk-shadow');
	            $('body').append(el);
	        }
	        $('#milk-shadow').toggleClass('draw', true);        
	        FRONT.spinner.execute( $('#milk-shadow')[0] );
	        $('body').toggleClass('tag-change', true);
	        $('.BackToTop').remove();
	        window.location.reload();
	    })	    
	    .modal('show', toggler);
	});

	$( document ).on( "click", ".plan-digitalization .plus_ico", function(e) {
		var toggler = $(e.target),
			rootrow = toggler.closest('tr'),
			childrow = rootrow.next(),
			book_id = $(this).closest('[data-item-widget]').attr('id'),
			tr_block = $(this).closest('.search-result'),
			tr_blockAppendix = $(tr_block).next(),
			message = {
                title: 'Удалить?',
                text: 'Вы уверены, что хотите удалить эту книгу из планов на оцифровку?',
                confirmTitle: "Удалить"
            };

		$.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed){
            	$.post(
            		"/local/tools/plan_digitization/remove.php", 
            		{ sessid: BX.message('bitrix_sessid'), BOOK_ID: book_id }, 
            		function(){
					// tr_block.fadeOut(1000,function(){$(this).remove()});
					// tr_blockAppendix.fadeOut(1000,function(){$(this).remove()});
				})
				.success(function(){
					//Уменьшаем счетчик книг
					var counterEl = $('.digitizing-count'),
						counter = parseInt($('.digitizing-count').text())-1;
					counterEl.text(counter);
					$(rootrow).fadeOut(1000,function(){$(this).remove()}); 
					$(childrow).fadeOut(1000,function(){$(this).remove()});
				});
            }
        });
	});

});