<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-side right">
	<form name='category' id="category" method="POST">
	<div class="b-libfilter">
		<h4><?=Loc::getMessage('QUOTES_MY_LIBRARY_TEMPLATE_TITLE')?></h4>
		<div class="b-libfilter_item current"><span class="b-libfilter_num"><?=$arResult['BOOK']['COUNT']?></span><span class="b-libfilter_name">Все книги</span></div>
		<div class="b-libfilter_item "><span class="b-libfilter_num"><?=$arResult['BOOK']['READ']['COUNT']?></span><span class="b-libfilter_name">Сейчас читаю</span></div>
		<div class="b-libfilter_item "><span class="b-libfilter_num"><?=$arResult['BOOK']['OLD']['COUNT']?></span><span class="b-libfilter_name">Прочитал</span></div>
		
		
			
			<div class="b-libfilter_item b-libfilter_useritem">
				<span class="b-libfilter_num"><?=$arResult['BOOK']['COUNT']?></span>
				<div class="checkwrapper" id="category_all_div">
					<label for="cb11" class="black b-libfilter_name"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_ALL_BOOKS')?></label>
					<input <?if($_REQUEST['all_book']){?>checked="checked"<?}?> class="" type="checkbox" name="all_book" value="1" id="cb11">
				</div>			
			</div>
			<?if(!$_REQUEST['category'])
			{
				$_REQUEST['category'] = array(
					'0'
				);
			}?>
			<?foreach($arResult['THEME'] as $arThemes){?>
				<div class="b-libfilter_item current b-libfilter_useritem">
					<span class="b-libfilter_num"><?=$arThemes['COUNT']?></span>
					<div class="checkwrapper">
						<label for="cb21" class="black b-libfilter_name"><?=$arThemes['VALUES']['NAME']?></label><input <?if(in_array($arThemes['VALUES']['EXALEAD_ID'], $_REQUEST['category'])){?>checked="checked"<?}?> value="<?=$arThemes['VALUES']['EXALEAD_ID']?>" class="" type="checkbox" name="category[]" id="cb21">
					</div>			
				</div>
			<?}?>
		
		<div class="b-libfilter_action clearfix">
			<a href="#" class="b-libfilter_remove"></a>
			<a href="#" class="b-libfilter_add"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_CREATE')?></a>
		</div>
			
	</div><!-- /.b-side_libfilter -->
		<div class="b-sidenav">
			<a href="#" class="b-sidenav_title"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_AUTHOR')?></a>
				<ul class="b-sidenav_cont">
					<?if(!$_REQUEST['author'])
					{
						$_REQUEST['author'] = array(
							'0'
						);
					}?>
					<?foreach($arResult['AUTHOR'] as $arAuthors){?>
						<li class="clearfix">
							<div class="b-sidenav_value left"><?=$arAuthors['VALUES']['SECOND_NAME'] . ' ' . $arAuthors['VALUES']['NAME'] . ' ' . $arAuthors['VALUES']['LAST_NAME']?></div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black"><?=$arAuthors['COUNT']?></label><input <?if(in_array($arThemes['VALUES']['EXALEAD_ID'], $_REQUEST['author'])){?>checked="checked"<?}?> value="<?=$arAuthors['VALUES']['EXALEAD_ID']?>" class="" type="checkbox" name="author[]" id="cb2">
							</div>
						</li>
					<?}?>		
				</ul>				
			<a href="#" class="b-sidenav_title"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_THEME')?></a>
				<ul class="b-sidenav_cont">
					<?/*if(!$_REQUEST['author'])
					{
						$_REQUEST['author'] = array(
							'0'
						);
					}*/?>
					<?foreach($arResult['DATES'] as $arDates){?>
						<li class="clearfix">
							<div class="b-sidenav_value left"><?=$arDates['DATE']['FIRST'] . ' - ' . $arDates['DATE']['END']?></div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black"><?=$arDates['COUNT']?></label><input value="<?=$arDates['DATE']['FIRST']?>" class="" type="checkbox" name="dates[]" id="cb2">
							</div>
						</li>
					<?}?>		
					<li><a href="#" class="b_sidenav_contmore"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_NEXT')?></a></li>
				</ul>
		</div> <!-- /.b-sidenav -->
	</form>
</div><!-- /.b-side -->
<script>
	$(function(){
		$('.b-libfilter_item input').libfilterCheck();
		$('.b-sidenav_cont input').libfilterCheck();
	});
</script>