<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 19.10.2016
 * Time: 14:00
 */

CBitrixComponent::includeComponentClass('neb:feedback');

/**
 * Class FeedbackAddComponent - не используется в проекте, т.к. пока хватает bitrix:form.result.new, но можно будет заменить при надобности
 */
class FeedbackAddComponent extends FeedbackComponent
{
    private $arRequestForm = [];

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->includeModules(['form']);

        $this->getRequest();
        $this->getRequestForm();
        $this->getUser();
        $this->getForm(['SID' => 'SIMPLE_FORM_1']);
        $this->setResult();

        $this->arResult['FORM'] = $this->arForm;
        $this->arResult['USER'] = $this->arUser;
        $this->includeComponentTemplate();
    }

    /**
     * Получение данных формы
     */
    private function getRequestForm() {

        $arResult = [];

        if ($this->arRequest['FEEDBACK']['AJAX'] == 'Y')
            foreach ($this->arRequest['FEEDBACK'] as $sCode => $sVal)
                $arResult[htmlspecialcharsbx($sCode)] = htmlspecialcharsbx($sVal);

        $this->arRequestForm = $arResult;
    }

    /**
     * Записывает данные результата в БД
     * TODO: Можно также впилить защиту для сессии от множества ответов на единицу времени
     *
     * @throws Exception
     * @throws \Bitrix\Main\SystemException
     */
    private function setResult() {

        if ($this->arRequestForm['AJAX'] != 'Y' || empty($this->arForm))
            return ;

        $this->arResult['SUCCESS'] = false;

        // проверка сессии
        if (empty($this->arRequestForm['BITRIX_SESSID']))
            $this->arResult['ERROR'][] = 'Не передан идентификатор сессии!';
        elseif ($this->arRequestForm['BITRIX_SESSID'] != bitrix_sessid())
            $this->arResult['ERROR'][] = 'Ваша сессия устарела, обновите страницу!';

        // проверка темы
        if (empty($this->arRequestForm['THEME']))
            $this->arResult['ERROR'][] = 'Не передан идентификатор темы!';
        elseif (empty($this->arForm['FIELDS']['FB_THEME']['ANSWERS'][$this->arRequestForm['THEME']]))
            $this->arResult['ERROR'][] = 'Не существует такой темы!';

        // проверка электропочты
        if (empty($this->arUser['EMAIL'])) {
            if (empty($this->arRequestForm['EMAIL']))
                $this->arResult['ERROR'][] = 'Не задан адрес электронной почты!';
            elseif (!check_email($this->arRequestForm['EMAIL']))
                $this->arResult['ERROR'][] = 'Передан не верный адрес электронной почты!';
        } else {
            $this->arRequestForm['EMAIL'] = $this->arUser['EMAIL'];
        }

        // проверка сообщения
        if (empty($this->arRequestForm['MESSAGE']))
            $this->arResult['ERROR'][] = 'Не передано сообщение!';
        elseif (mb_strlen($this->arRequestForm['MESSAGE']) < 3)
            $this->arResult['ERROR'][] = 'Сообщение слишком короткое!';

        // суть-боль...
        $iResult = 0;
        if (empty($this->arResult['ERROR'])) {

            // битриксы - говнокодеры.... (не спрашивайте за чем это, просто примите за данность суровой реальности)
            if (!$this->arUser['ID'] && !empty($_SESSION["SESS_LAST_USER_ID"]))
                unset($_SESSION["SESS_LAST_USER_ID"]);

            // формирование массива полей результата
            $arValues = [];

            // e-mail
            $arAnswer = $this->arForm['FIELDS']['FB_EMAIL']['ANSWERS'];
            if (!empty($arAnswer)) {
                reset($arAnswer); $arAnswer = current($arAnswer);
                $arValues['form_' . $arAnswer['FIELD_TYPE'] . '_' . $arAnswer['ID']] = $this->arRequestForm['EMAIL'];
            }
            // theme
            foreach ($this->arForm['FIELDS']['FB_THEME']['ANSWERS'] as $arAnswer)
                if ($this->arRequestForm['THEME'] == $arAnswer['ID'])
                    $arValues['form_' . $arAnswer['FIELD_TYPE'] . '_' . $this->arForm['FIELDS']['FB_THEME']['SID']] = $arAnswer['ID'];
            // message
            $arAnswer = $this->arForm['FIELDS']['FB_TEXT']['ANSWERS'];
            if (!empty($arAnswer)) {
                reset($arAnswer); $arAnswer = current($arAnswer);
                $arValues['form_' . $arAnswer['FIELD_TYPE'] . '_' . $arAnswer['ID']] = $this->arRequestForm['MESSAGE'];
            }

            // запишем в базу результат
            $obFormResult = new CFormResult();
            if ($iResult = $obFormResult->Add($this->arForm['ID'], $arValues, "N", $this->arUser['ID'])) {
                $obFormResult->SetEvent($iResult);
                $obFormResult->Mail($iResult);
                $this->arResult['SUCCESS'] = true;
            }
            else {
                global $strError;
                $this->arResult['ERROR'][] = $strError;
            }
        }

        $this->showJson($this->arResult);
        //$this->restartBuffer(); debugPre($this->arResult); die();
    }
}