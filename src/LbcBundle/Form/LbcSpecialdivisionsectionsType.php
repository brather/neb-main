<?php

namespace LbcBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LbcSpecialdivisionsectionsType extends AbstractLbcCategoryType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('specialdivisionid')
            ->add('code');
    }

    public function getEntityName()
    {
        return 'LbcBundle\Entity\LbcSpecialdivisionsections';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lbcbundle_lbcspecialdivisionsections';
    }
}
