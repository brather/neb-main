<?php
CBitrixComponent::includeComponentClass('rest:rest.server');
CModule::IncludeModule('iblock');

/**
 * Class RestFunds
 */
class RestFunds extends RestServer
{
    /**
     * @var array
     */
    protected $_params
        = array(
            'bookId'    => null,
            'libraryId' => null,
            'reasonMsg' => null,
        );

    protected $_methodAllowGroups = array(
        'deleteAction' => array(
            13 => true,
        )
    );

    /**
     * @return int
     * @throws Exception
     */
    public function deleteAction()
    {
        if (!CModule::IncludeModule('nota.exalead')) {
            throw new Exception('Module "nota.exalead" not installed', 500);
        }
        if (empty($this->_params['bookId'])) {
            throw new Exception('Parameter "bookId" is required');
        }

        $this->arResult = array(
            'deletedFunds' => \Nota\Exalead\LibraryBiblioCardTable::deleteFunds(
                $this->_params['bookId'],
                $this->_params['libraryId']
            )
        );
        if(!empty($this->_params['reasonMsg'])) {
            \Neb\Main\FundsDeleteMessagesTable::add(
                array(
                    'BOOK_ID' => $this->_params['bookId'],
                    'MESSAGE' => $this->_params['reasonMsg'],
                )
            );
        }

        return 200;
    }
}