<?php
/**
 * User: agolodkov
 * Date: 31.07.2015
 * Time: 10:37
 */

/**
 * Class NebLibraryPartyComponent
 */
\CModule::IncludeModule("highloadblock");
CBitrixComponent::includeComponentClass('neb:workplaces.list');

use \Bitrix\Highloadblock as HL;
use \Bitrix\Main\Entity;
use \Neb\Main\Library\NebLibsCityTable;

class WorkplacesComponent extends WorkplacesListComponent
{

    /**
     * @var array
     */
    public $_defaultResult
        = array(
            'IBLOCK_ID' => IBLOCK_ID_WORKPLACES,
            'cityId'    => null,
        );

    /**
     * @return $this
     */
    public function loadList()
    {
        NebMainHelper::includeModule('iblock');

        //получение текущего города пользователя
        $currentCity = false;
        if ($this->arParams['cityId']) {
            setcookie('library_city', $this->arParams['cityId'], time() + 60 * 60 * 24 * 365 * 100, '/');
            $_COOKIE['library_city'] = $this->arParams['cityId'];
        }

        //установлен ли город в куках?
        $arResult["IS_COOKIE_SET"] = isset($_COOKIE['library_city'])  && intval($_COOKIE['library_city']) > 0;

        if($arResult["IS_COOKIE_SET"]){
            //установим id из куки, если она есть
            $currentCity = intval($_COOKIE['library_city']);
        } else {
            //установим id из базы. ищем LIKE по списку городов
            $queryCity = TheDistance::getCurPosition();

            $cityData = NebLibsCityTable::getList(array(
                "select" => array("ID", "UF_CITY_NAME"),
                "filter" => array("%UF_CITY_NAME" => $queryCity["city"]),
                "limit" => 1
            ))->Fetch();

            if($cityData["ID"] > 0)
                $currentCity = $cityData["ID"];
            else //установим из константы
                $currentCity = LIBRARY_DEFAULT_CITY;
        }

        // выбранный город
        $cityData = NebLibsCityTable::getByID($currentCity)->Fetch();
        $cityData["UF_POS"] = explode(" ", $cityData["UF_POS"]);
        $this->arResult["CITY"] = $cityData;

        if ($currentCity > 0) {
            $queryString = "
            SELECT bie.*, nl.*
            FROM b_iblock_element bie
            JOIN b_iblock_element_prop_s7 bieps7 ON bieps7.IBLOCK_ELEMENT_ID = bie.ID
            JOIN neb_libs nl ON nl.ID = bieps7.PROPERTY_23
            WHERE bie.IBLOCK_ID = %d AND bie.ACTIVE = 'Y' AND nl.UF_CITY = %d;";
            $queryString = sprintf($queryString, IBLOCK_ID_WORKPLACES, $currentCity);

            $this->arResult['items']
                = \Bitrix\Iblock\ElementTable::getEntity()
                ->getConnection()
                ->query($queryString)
                ->fetchAll();

            // получение ссылок библиотек
            foreach ($this->arResult['items'] as $arItem)
                $arLibrary[$arItem["UF_ID"]] = $arItem["UF_ID"];

            if (!empty($arLibrary)) {
                $obIBlockElement = new \CIBlockElement();
                $rsLibs = $obIBlockElement->GetList(
                    array(),
                    array(
                        "IBLOCK_ID"             => IBLOCK_ID_LIBRARY,
                        "ACTIVE"                => "Y",
                        "PROPERTY_LIBRARY_LINK" => array_values($arLibrary)
                    ),
                    false,
                    false,
                    array("ID", "IBLOCK_ID", "DETAIL_PAGE_URL", "PROPERTY_LIBRARY_LINK")
                );
                while ($arItem = $rsLibs->GetNext(true, false))
                    $arLibrary[$arItem['PROPERTY_LIBRARY_LINK_VALUE']] = $arItem['DETAIL_PAGE_URL'];
                unset($obIBlockElement, $rsLibs);
            }

            foreach ($this->arResult['items'] as &$arItem) {
                if (!empty($arLibrary[$arItem["UF_ID"]]))
                    $arItem['link'] = $arLibrary[$arItem["UF_ID"]];
                $arItem["UF_POS"] = explode(' ', $arItem["UF_POS"]);
            }

        } else {
            $this->loadElemets();
        }

        return $this;
    }
}