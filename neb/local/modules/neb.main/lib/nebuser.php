<?php
/**
 * User: agolodkov
 * Date: 06.08.2015
 * Time: 10:36
 */
namespace Neb\Main;

use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\UserTable;

class NebUserTable extends UserTable
{
    public static function getMap()
    {
        return array_merge_recursive(
            parent::getMap(),
            array(
                'USER_GROUP'        => new ReferenceField(
                    'USER_GROUP',
                    'Bitrix\Main\UserGroupTable',
                    array('=this.ID' => 'ref.USER_ID'),
                    array('join_type' => 'LEFT')
                ),
                'USER_STAT_SESSION' => new ReferenceField(
                    'USER_STAT_SESSION',
                    'Neb\Main\StatSessionTable',
                    array('=this.ID' => 'ref.USER_ID'),
                    array('join_type' => 'LEFT')
                ),
            )
        );
    }
}