<?php
/**
 * User: agolodkov
 * Date: 28.09.2015
 * Time: 11:00
 */

/**
 * Class CreateBookRequestGroupComponent
 */
class CreateBookRequestGroupComponent
    extends \Neb\Main\Component\CreateBooksComponent
{
    /**
     * @param $params
     *
     * @throws Exception
     * @throws \Bitrix\Main\LoaderException
     */
    protected function _prepareRequestParams(&$params)
    {
        NebMainHelper::includeModule('nota.exalead');
        $params['books'] = array();

        if (is_array($_POST['BOOK_NUM_ID'])) {
            $booksKeys = $_POST['BOOK_NUM_ID'];
        } else {
            $booksKeys = array($_POST['BOOK_NUM_ID']);
        }
        $bookFiles = array();
        foreach ($booksKeys as $key) {
            if (isset($_POST['BOOK_ID_' . $key])
                && isset($_POST['FILE_PDF_' . $key])
            ) {
                $bookFiles[$_POST['BOOK_ID_' . $key]] = $_POST['FILE_PDF_'
                . $key];
            }
        }
        $booksIds = array_filter(array_keys($bookFiles));
        if (!empty($booksIds)) {
            $biblioCards
                = \Nota\Exalead\LibraryBiblioCardTable::getByFullSymbolicId(
                $booksIds
            );
            foreach ($biblioCards as $item) {
                $item = array_intersect_key(
                    $item,
                    array(
                        'ALIS'           => true,
                        'Author'         => true,
                        'Name'           => true,
                        'PublishYear'    => true,
                        'BBKText'        => true,
                        'ISBN'           => true,
                        'FullSymbolicId' => true,
                    )
                );
                if (isset($bookFiles[$item['FullSymbolicId']])) {
                    $item['pdfLink'] = $bookFiles[$item['FullSymbolicId']];
                    $params['books'][] = $item;
                }
            }
        }
    }
}