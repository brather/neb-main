<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class ReportConstructorComponent extends CBitrixComponent
{
    /**
     * Подготовка параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        if ($_REQUEST['report_id'] && trim($_REQUEST['select']) != ''){
            // редактирование отчета
            $arResult['idReport'] = \TblReport::editReport($_REQUEST);
            LocalRedirect("/local/tools/report_constructor/report.php?id=".$arResult['idReport']);
        }

        if ($_REQUEST['add_report'] == 'Y' || trim($_REQUEST['select']) != ''){
            // создание нового отчета
            $arResult['idReport'] = \TblReport::newReport($_REQUEST);
            LocalRedirect("/local/tools/report_constructor/report.php?id=".$arResult['idReport']);
        }

        // выбор всех сущностей
        $arResult['entities'] = \TblEntities::getEntitiesExtend();

        //выбор функций вычисления
        $arResult['functions'] = \TblEntities::getAgregateType();
        $arResult['compare'] = \TblEntities::getCompareType();
        $arResult['order'] = \TblEntities::getOrderType();

        if ($_REQUEST['id'] > 0) {
            // структура для отчета
            //выбор сущностей
            $arResult['report']['id'] = (int)$_REQUEST['id'];
            $arResult['report']['data'] = \TblReport::getReportById($arResult['report']['id']);
            $arResult['report']['entities'] = \TblReport::getEntitiesReport($arResult['report']['id']);
            $arResult['report']['fields'] = \TblReport::getFieldsReport($arResult['report']['id']);

            //связи сущностей
            if (count($arResult['report']['entities']) > 1){
                $arResult['report']['relations'] = \TblEntities::getEntityRelations($arResult['report']['entities']);
                $tables = \TblReport::getEntities($arResult['report']['id']);
                $joins = \TblReport::getRelation($tables['id']);
                foreach ($joins as $join)
                    $arResult['report']['joins'][] = $join['rid'];
            }

            // выбор полей сущностей
            foreach ($arResult['report']['entities'] as $id)
                $arResult['report']['entityFields'][$id] = \TblEntities::getEntityFields($id);

            //поля для фильтрации
            $arResult['report']['where'] = \TblReport::getWhereReports($arResult['report']['id']);

            //поля для группировки
            $arResult['report']['agrigate'] = \TblReport::getAggregationReports($arResult['report']['id']);
            $arResult['report']['group'] = \TblReport::getGroupReports($arResult['report']['id']);

            //поля для сортировки
            $arResult['report']['order'] = \TblReport::getOrderReports($arResult['report']['id']);
        }

        $this->arResult = $arResult;

        $this->includeComponentTemplate();
    }
}
