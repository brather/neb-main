<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 */
?>
<section class="container hall-list">
    <h1>Регионы</h1>

    <? if (!empty($arResult['city'])): ?>
        <div class="hall-list__main">
            <? foreach ($arResult['city'] as $arCity): ?>
                <a href="/library/?city_id=<?=$arCity['ID']?>" class="hall-list__main-link"><?=$arCity['NAME']?></a>
            <? endforeach; ?>
        </div>
    <? endif; ?>

    <? if (!empty($arResult['items'])): ?>
        <ul class="hall-list__secondary">
            <? foreach ($arResult['items'] as $item): ?>
                <li>
                    <a href="/library/cities/?region_name=<?= $item['UF_REGION'] ?>"
                       class="hall-list__secondary-link"><?= $item['UF_REGION'] ?></a>
                </li>
            <? endforeach; ?>
        </ul>
    <? endif; ?>
</section>