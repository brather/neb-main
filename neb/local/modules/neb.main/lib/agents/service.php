<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 14.04.2017
 * Time: 11:50
 */

namespace Neb\Main\Agents;

use \Exception;

class Service
{
    /**
     * Получение агента по названию метода (используется в других агентах)
     *
     * @param $method
     * @return array
     * @throws Exception
     */
    public static function getAgentByExecute($method)
    {
        $rsAgents = \CAgent::GetList([], ['NAME' => $method]);
        if (!$arAgent = $rsAgents->Fetch()) {
            throw new Exception('Not found agent');
        }

        return $arAgent;
    }

    /**
     * @param string $method
     *
     * @return mixed
     * @throws Exception
     */
    public static function getAgentNextExec($method)
    {
        $agent = static::getAgentByExecute($method);

        return $agent['NEXT_EXEC'];
    }

    /**
     * Добавление времени к дате следующей проверки агента
     *
     * @param $sMethod
     * @param int $iSeconds
     * @return bool
     */
    public static function addCheckDate($sMethod, $iSeconds = 3600) {

        $bResult = false;

        $arAgent = self::getAgentByExecute($sMethod);

        if (intval($arAgent['ID']) > 0) {
            $bResult = false !== \CAgent::Update(
                $arAgent['ID'],
                ['DATE_CHECK' => ConvertTimeStamp(time() + $iSeconds, 'FULL')]
            );
        }

        return $bResult;
    }

    /**
     * Создание раздела
     *
     * @param $sDir
     */
    public static function createDirectory($sDir) {
        if (!is_dir($sDir)) {
            mkdir($sDir, BX_DIR_PERMISSIONS);
        }
    }

    /**
     * Удаление раздела
     *
     * @param $sDir
     * @param bool $bDelRoot
     */
    public static function removeDirectory($sDir, $bDelRoot = true) {
        if (is_dir($sDir) && $objs = glob($sDir . '/*')) {
            foreach($objs as $obj) {
                is_dir($obj) ? self::removeDirectory($obj, $bDelRoot) : unlink($obj);
            }
        }
        if ($bDelRoot) {
            rmdir($sDir);
        }
    }

    /**
     * Установка предельного размера выделяемой памяти
     *
     * @param $sMemoryLimit
     */
    public static function setMemoryLimit($sMemoryLimit) {
        if (preg_replace('/[^0-9]/', '', @ini_get('memory_limit')) < preg_replace('/[^0-9]/', '', $sMemoryLimit)) {
            @ini_set('memory_limit', $sMemoryLimit);
        }
    }
}