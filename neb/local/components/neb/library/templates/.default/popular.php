<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

$this->SetViewTarget('lib_menu', $pos);
include_once('menu.php');
$this->EndViewTarget();

?>
<div class="row">
    <div class="col-md-3 col-sm-3 lk-sidebar">
        <?$APPLICATION->ShowViewContent('lib_menu')?>
        <?
        $APPLICATION->IncludeComponent(
            "neb:library.right_counter",
            "",
            Array(
                "IBLOCK_ID" => IBLOCK_ID_LIBRARY,
                "LIBRARY_ID" => $arResult["VARIABLES"]["ID"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "COLLECTION_URL" => $arParams['COLLECTION_URL'],
            ),
            $component
        );
        ?>
    </div>
    <div class="col-md-9 col-sm-9 lk-content">
        <h2>Популярные издания</h2>
        <br />
        <?$APPLICATION->IncludeComponent(
            "neb:books.popular",
            "",
            Array(
                "LIBRARY_ID" =>  $arResult["VARIABLES"]["ID"],
                "CODE"       => $arResult["VARIABLES"]["CODE"],
                "ITEM_COUNT" => 12,
                "CACHE_TIME" => $arParams["CACHE_TIME"]
            ),
            $component
        );
        ?>
    </div>
</div>
