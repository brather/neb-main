<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 29.05.2017
 * Time: 15:06
 */

namespace Neb\Main\Helper;

/**
 * Class BooksHelper
 *
 * @package Neb\Main\Helper
 */
class BooksHelper
{
    private static $STATUS = [
        'open'    => 0,
        'close'   => 1,
        'blocked' => 2
    ];

    public static function getByCode($sCode)
    {
        if (isset(self::$STATUS[$sCode])) {
            return self::$STATUS[$sCode];
        } else {
            return null;
        }
    }

    public static function getByNumber($iNumber)
    {
        if ($iNumber) {
            foreach (self::$STATUS as $sCode => $iCode) {
                if ($iCode == $iNumber) {
                    return $sCode;
                }
            }
        }

        return null;
    }
}