<?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arMenu = array(
    "parent_menu" => "global_menu_settings",
    "section" => "testadmin",
    "sort" => 2000,
    "text" => 'testmenu' . GetMessage("TNOTAEXT_MENU_MAIN"),
    "title" => 'testmenu' . GetMessage("TNOTAEXT_MENU_MAIN_TITLE"),
    "icon" => "notatestext_menu_icon",
    "page_icon" => "notatestext_page_icon",
    "items_id" => "menu_notatestext",

    "items" => array(
        array(
            "text" => 'testmenu' . GetMessage("TNOTAEXT_MENU_EXT"),
            "more_url" => array(),
            "title" => 'testmenu' . GetMessage("TNOTAEXT_MENU_EXT_TITLE"),
            "url" => "admintestfile.php?lang=" . LANGUAGE_ID,
        ),
    )
);
