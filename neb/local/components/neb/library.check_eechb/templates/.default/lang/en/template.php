<?
$MESS['LIBRARY_CHECK_EECHB_CHECK_ELP'] = 'Verification of the Electronic Library pass';
$MESS['LIBRARY_CHECK_EECHB_GIVEN_DATA'] = 'Given data';
$MESS['LIBRARY_CHECK_EECHB_FIO_SHORT'] = 'Last/First/Middle Name';
$MESS['LIBRARY_CHECK_EECHB_REQ'] = 'Required field';
$MESS['LIBRARY_CHECK_EECHB_INCORRECT'] = 'Incorrect field data';
$MESS['LIBRARY_CHECK_EECHB_ELP_NUMBER'] = 'Number of the Electronic Library pass';
$MESS['LIBRARY_CHECK_EECHB_CHECK'] = 'Check';
$MESS['LIBRARY_CHECK_EECHB_404'] = 'In the NEB database matches is not found';
$MESS['LIBRARY_CHECK_EECHB_CHECK_CORRECT'] = 'Check the correctness of data or ';
$MESS['LIBRARY_CHECK_EECHB_CREATE_NEW'] = 'add new user';
$MESS['LIBRARY_CHECK_EECHB_FIO'] = 'Last/First<br />/Middle Name';
$MESS['LIBRARY_CHECK_EECHB_ELP_NUMBER_SHORT'] = 'ELP number';
$MESS['LIBRARY_CHECK_EECHB_REG_DATE'] = 'Registration <br /> date';
$MESS['LIBRARY_CHECK_EECHB_STATUS'] = 'Status';
$MESS['LIBRARY_CHECK_EECHB_VERIFIED'] = 'Verified';
$MESS['LIBRARY_CHECK_EECHB_ACTION'] = 'Action';
$MESS['LIBRARY_CHECK_EECHB_EDIT'] = 'Edit';
$MESS['LIBRARY_CHECK_EECHB_DELETE'] = 'Remove';
$MESS['LIBRARY_CHECK_EECHB_BLOCK'] = 'Block';
?>
