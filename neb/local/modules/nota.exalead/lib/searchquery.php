<?
namespace Nota\Exalead;

use \Nota\Exalead\SearchClient;
use \Bitrix\NotaExt\WriteLog;
use \Bitrix\Main\Config\Option;
use \Symfony\Component\Config\Definition\Exception\Exception;

class SearchQuery
{
    private $arParams = array();
    public $ip = '';
    public $port = '';
    protected $url = '/search-api/search';
    protected $queryType = 'search';
    public $nav;
    protected $_timeout = 30;

    public function __construct($query = null)
    {
        if ($query) {
            $this->setQuery($query);
            static::setPageLimit(10);
            static::setParam('l', 'ru');
        }

        $this->ip   = Option::get('nota.exalead', 'exalead_ip');
        $this->port = Option::get('nota.exalead', 'exalead_port');

        return $this;

    }

    public function setQuery($query)
    {
        static::setParam('q', trim($query));
    }

    /**
     * Получить обложку
     *
     * @param $uri
     * @param string $source
     * @param int $width
     * @param int $height
     * @return bool|string
     */
    public function getThumbnail($uri, $source = 'JapanConnector', $width = 300, $height = 410)
    {
        if (empty($uri))
            return false;

        // Корректируем размер, что бы потом сделать ресайз
        $ns_widht = intval($width * 0.1) + $width;
        $ns_height = intval($height * 0.1) + $height;

        $this->queryType = 'thumbnail';
        $this->url = '/thumbnail';
        static::setParam('uri', trim($uri));
        static::setParam('source', $source);
        static::setParam('width', $ns_widht);
        static::setParam('height', $ns_height);

        $thumbnailFolder = "/upload/tmp_thumbnail/" . substr($uri, -3) . '/';
        $fileName = $thumbnailFolder . $uri . "w" . $width . "h" . $height . ".png";
        //if(!file_exists($_SERVER["DOCUMENT_ROOT"].$fileName)){

        //$logF=new WriteLog($_SERVER['DOCUMENT_ROOT'].'/queryThumbnailLog.txt');
        //$logF->addLog('start_'.$uri);
        //$ptime = getmicrotime();

        $arFile = \CFile::MakeFileArray('http://' . $this->ip . ':' . $this->port . $this->url . '?' . http_build_query($this->arParams));
        if (substr($arFile['type'], 0, 4) == 'text') # если вместо картинки приходит ошибка в текстовом виде
            return false;
        //$logF->addLog('end_'.$uri.'_'.round(getmicrotime()-$ptime, 3));

        if (empty($arFile['tmp_name']))
            return false;

        //пока выключаем обрезку лишних полей, т.к. если у книги нет полей, а просто название на белом фоне, то функция не может нормально сработать
        PrepThumbnail::crop($arFile['tmp_name']);

        \CFile::ResizeImage($arFile, array('width' => $width, 'height' => $height));

        CheckDirPath($_SERVER["DOCUMENT_ROOT"] . $thumbnailFolder);
        CopyDirFiles($arFile['tmp_name'], $_SERVER["DOCUMENT_ROOT"] . $fileName);

        //}

        return $fileName;

    }

    public function getById($id)
    {
        if (empty($id))
            return false;
        
        $this->queryType = 'getById';
        $this->setQuery('id:"' . $id . '"');
        static::setParam('l', 'ru');
    }

    public function getByArIds(array $arIds)
    {
        TrimArr($arIds);
        if (empty($arIds))
            return false;
        $arIds = array_map(create_function('$v', 'return "id:\"".$v."\"";'), $arIds);
        $q = implode(" OR ", $arIds);
        $this->__construct($q);
    }

    /**
     * Возможно вы имели в виду
     *
     * @param $query
     * @return bool
     */
    public function getSpellcheck($query)
    {
        if (empty($query))
            return false;
        $this->queryType = 'spellcheck';
        $this->url = '/spellcheck';
        $this->setQuery($query);
        $this->setParam('l', 'ru');
    }

    public function getSuggestTitleDisser($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title_disser';

        $this->setQuery($query);

        return $this;
    }

    public function getSuggestAuthorDisser($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author_disser';

        $this->setQuery($query);
        return $this;
    }

    public function getSuggestTitleAllDisser($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title_all_disser';

        $this->setQuery($query);

        return $this;
    }

    public function getSuggestAuthorAllDisser($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author_all_disser';

        $this->setQuery($query);

        return $this;
    }

    public function getSuggestTitlePatent($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title_patent';

        $this->setQuery($query);

        return $this;
    }

    public function getSuggestAuthorPatent($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author_patent';

        $this->setQuery($query);

        return $this;
    }

    public function getSuggestTitleDisserPatent($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title_disser_patent';

        $this->setQuery($query);

        return $this;
    }

    public function getSuggestAuthorDisserPatent($query)
    {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author_disser_patent';

        $this->setQuery($query);

        return $this;
    }
    
    /**
     * результатов на странице
     *
     * @param $hf
     */
    public function setPageLimit($hf)
    {
        if ((int)$hf > 0)
            static::setParam('hf', intval($hf));
    }

    /**
     * позиция для следующей выборки
     *
     * @param $b
     */
    public function setNextPagePosition($b)
    {
        if ((int)$b > 0)
            static::setParam('b', intval($b));
    }

    public function setUrl($url)
    {
        $url = trim($url);
        if (!empty($url))
            $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getQueryType()
    {
        return $this->queryType;
    }

    public function setQueryType($type = 'search')
    {
        $this->queryType = $type;

        return $this;
    }

    public static function sanitizeParam($p)
    {
        $sanitizedValue = trim($p);
        $sanitizedValue = preg_replace('#й#i', 'и', $sanitizedValue);
        $sanitizedValue = preg_replace('#ё#i', 'е', $sanitizedValue);

        return $sanitizedValue;
    }

    /**
     * Удаление невалидных символов из строки запроса
     *
     * @param $q - запрос
     * @return string - результат
     */
    public static function sanitizeQuery($q)
    {
        $sanitizedValue = trim($q);
        $sanitizedValue = preg_replace('#\(#', '', $sanitizedValue);
        $sanitizedValue = preg_replace('#\)#', '', $sanitizedValue);
        $sanitizedValue = preg_replace('#"#',  '', $sanitizedValue);
        $sanitizedValue = preg_replace('#:#',  '', $sanitizedValue);
        $sanitizedValue = preg_replace('#\=#', '', $sanitizedValue);
        $sanitizedValue = str_replace(['&'],   ' ',$sanitizedValue);

        return $sanitizedValue;
    }

    public function setParam($param, $value, $isarray = false)
    {
        if ($isarray) {
            $this->arParams[$param][] = trim($value);
        } else {
            $this->arParams[$param] = trim($value);
        }

        return $this;
    }

    public function unsetParam($name)
    {
        if (isset($this->arParams[$name]))
            unset($this->arParams[$name]);

        return $this;
    }

    public function getParameter($param)
    {
        if (empty($param))
            return false;
        return $this->arParams[$param];
    }

    public function getParameters()
    {
        return $this->arParams;
    }

    public function addParamToQuery($param, $type)
    {
        if (!in_array($type, ['AND', 'OR']))
            throw new \Exception('Неверный тип!');

        if (empty($param))
            throw new \Exception('Пустой параметр!');

        $this->setParam('q', $this->getParameter('q') . ' ' . $type . ' ' . trim($param));

        return $this;
    }

    /**
     * Постраничная навигация
     */
    public function setNavParams()
    {
        $item_count = static::getParameter('hf');
        if ((int)$item_count <= 0)
            $item_count = 15;
        $this->nav = \CDBResult::GetNavParams($item_count);

        if ($this->nav['PAGEN'] > 1)
            static::setNextPagePosition((($this->nav['PAGEN'] - 1) * $item_count));
    }

    /**
     * @param int $seconds
     */
    public function setTimeout($seconds)
    {
        $this->_timeout = $seconds;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->_timeout;
    }
}