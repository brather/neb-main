<?
global $GLOBAL, $APPLICATION;
$GLOBAL['PAGE'] = 'MAIN';
$GLOBALS['filter'] = array('!PROPERTY_arhive' => 25);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("НЭБ - Национальная электронная библиотека");
$APPLICATION->SetPageProperty("keywords", "читать онлайн, скачать книгу, без регистрации, книги, издания, библиотека, авторефераты, диссертации, научная литература, учебная литература, ноты, карты, периодические издания, библиотека школьника, аудио и видео документы, патентные документы");
$APPLICATION->SetPageProperty("description", "Национальная электронная библиотека – это крупнейший библиотечный проект, уникальный интеллектуальный ресурс и начало нового этапа в развитии всех библиотек страны.");
\Neb\Main\Helper\UtilsHelper::setCanonicalUrl('/');
?>
<?
$APPLICATION->IncludeComponent(
	"collection:main.slider",
	".default",
	array(
		"PAGE_URL" => "/catalog/#BOOK_ID#/",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "collections",
		"IBLOCK_ID" => "6"
	),
	false
);
?>

<?$APPLICATION->IncludeComponent(
    "exalead:search.form",
    "redesign",
    Array(
        "PAGE" => (''),
        "POPUP_VIEW" => 'Y',
        "ACTION_URL" => '/search/',
        "REQUEST_PARAMS" => $requestParams
    )
);?>

<?$APPLICATION->IncludeComponent(
    "exalead:bbk.list",
    ".default",
    Array(
        "PAGE" => (''),
        "POPUP_VIEW" => 'Y',
        "ACTION_URL" => $APPLICATION->GetCurPage(),
        "REQUEST_PARAMS" => $requestParams,
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36001",
    )
);?>

<main>
    <?$APPLICATION->IncludeComponent(
        "neb:news.list",
        "main",
        array(
            "DATE" => "50",
            "COUNT" => COption::GetOptionString('neb.main', 'main_page_news_count'),
            "PAGE_URL" => $APPLICATION->GetCurPage(),
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "1",
            'IBLOCK_ID' => LANGUAGE_ID == 'ru' ? 11 : 12,
            'IBLOCK_TYPE' => 'newsNEB',
            'PARENT_SECTION_CODE' => 'news',
            "NEWS_COUNT" => COption::GetOptionString('neb.main', 'main_page_news_count'),
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "SET_TITLE" => 'N'
        ),
        false
    );?>
    <?$APPLICATION->IncludeComponent(
        "neb:news.yl",
        ".default",
        array(
            "COUNT" => COption::GetOptionString('neb.main', 'main_page_news_count_yl'),
            "PAGE_URL" => $APPLICATION->GetCurPage(),
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36001",
        ),
        false
    );?>

    <?$APPLICATION->IncludeFile("/local/include_areas/viewers.php", Array(), Array("MODE" => "php", "NAME" => "Приложения НЭБ"));?>
</main>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>