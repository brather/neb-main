<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'NAME'          => GetMessage('PROCESS_REQUEST_NAME'),
                'DEFAULT'       => '/',
                'VARIABLES'     => array('bookId', 'session'),
                'TYPE'          => array('PUT'),
                'DOCUMENTATION' => array(
                    'GROUP'       => 'CONTROL',
                    'DESCRIPTION' => '',
                    'PARAMETERS'  => array()
                ),
            ),
        ),
    ),
); 