<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once(substr(__DIR__, 0, strrpos(__DIR__, '/')) . "/prolog.php");
\Bitrix\Main\Loader::IncludeModule(ADMIN_MODULE_NAME);

$MOD_RIGHT = $APPLICATION->GetGroupRight(ADMIN_MODULE_NAME);

if ($MOD_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$isAdmin = $MOD_RIGHT >= "W";

use Bitrix\NotaExt\Utils as Ut;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$id = $_REQUEST["id"];

$strFolder = substr(__DIR__, 0, strrpos(__DIR__, '/')) . '/ext';
$arExt = array();

$handle = @opendir($strFolder);
if ($handle) {
    while (false !== ($dir = readdir($handle))) {
        if (is_dir($strFolder . "/" . $dir) and $dir != "." and $dir != "..") {
            $ext_dir = $strFolder . "/" . $dir;
            $ext_dir_info = $ext_dir . '/index.php';
            if (file_exists($ext_dir_info)) {
                include_once($ext_dir_info);
                if (class_exists($dir)) {
                    $info = new $dir;

                    $arExt[$dir]["EXT_ID"] = $info->EXT_ID;
                    $arExt[$dir]["EXT_NAME"] = $info->EXT_NAME;
                    $arExt[$dir]["EXT_DESCRIPTION"] = $info->EXT_DESCRIPTION;
                    $arExt[$dir]["EXT_VERSION"] = $info->EXT_VERSION;
                    $arExt[$dir]["EXT_VERSION_DATE"] = $info->EXT_VERSION_DATE;
                    $arExt[$dir]["EXT_SORT"] = $info->EXT_SORT;
                    $arExt[$dir]["IsInstalled"] = $info->IsInstalled();
                    $arExt[$dir]["EXT_OPTIONS"] = $info->getOptions();

                }
            }
        }
    }
    closedir($handle);

    uasort($arExt, create_function('$a, $b', 'if($a["EXT_SORT"] == $b["EXT_SORT"]) return strcasecmp($a["EXT_NAME"], $b["EXT_NAME"]); return ($a["EXT_SORT"] < $b["EXT_SORT"])? -1 : 1;'));
}

if ($isAdmin && check_bitrix_sessid()) {
    if (strlen($_REQUEST["uninstall"]) > 0 || strlen($_REQUEST["install"]) > 0) {
        $ext_dir = $strFolder . "/" . $id;
        $ext_dir_info = $ext_dir . '/index.php';
        if (file_exists($ext_dir_info)) {
            include_once($ext_dir_info);
            if (class_exists($id)) {
                $info = new $id;
                if ($info->IsInstalled() && strlen($_REQUEST["uninstall"]) > 0) {
                    $info->Uninstall();
                    LocalRedirect($APPLICATION->GetCurPage() . "?lang=" . LANGUAGE_ID);
                } elseif (!$info->IsInstalled() && strlen($_REQUEST["install"]) > 0) {
                    $info->Install();
                    LocalRedirect($APPLICATION->GetCurPage() . "?lang=" . LANGUAGE_ID);
                }
            }
        }
    }
}

$APPLICATION->SetTitle(GetMessage("TITLE"));
require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

?>
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="list-table">
    <tr class="heading">
        <td width="60%"><b><?= GetMessage("MOD_NAME") ?></b></td>
        <td><b><?= GetMessage("MOD_VERSION") ?></b></td>
        <td><b><?= GetMessage("MOD_DATE_UPDATE") ?></b></td>
        <td><b><?= GetMessage("MOD_SETUP") ?></b></td>
        <td><b><?= GetMessage("MOD_ACTION") ?></b></td>
    </tr>
    <?
    foreach ($arExt as $info) {
        ?>
        <tr>
            <td>
                <b><?= htmlspecialcharsex($info["EXT_NAME"]) ?></b> <br/>
                <? echo $info["EXT_DESCRIPTION"] ?><br/>
                <i><b><?= htmlspecialcharsbx($info["EXT_ID"]) ?></b></i>
                <?
                if ($info['EXT_OPTIONS'] !== false and $info["IsInstalled"]) {
                    ?>
                    <br/>
                    <a href="nota_ext_options.php?lang=<?= LANGUAGE_ID ?>&ext_id=<?= $info["EXT_ID"] ?>"><?= GetMessage("MOD_SETTINGS") ?></a>
                    <?
                }
                ?>
            </td>
            <td><?= $info["EXT_VERSION"] ?></td>
            <td nowrap><?= $info["EXT_VERSION_DATE"]; ?></td>
            <td nowrap><? if ($info["IsInstalled"]) {
                    ?><?= GetMessage("MOD_INSTALLED") ?><?
                } else {
                    ?><span class="required"><?= GetMessage("MOD_NOT_INSTALLED") ?></span><?
                } ?></td>
            <td>
                <form action="<?= $APPLICATION->GetCurPage() ?>" method="GET"
                      id="form_for_<?= htmlspecialcharsbx($info["EXT_ID"]) ?>">
                    <input type="hidden" name="action" value=""
                           id="action_for_<?= htmlspecialcharsbx($info["EXT_ID"]) ?>">
                    <input type="hidden" name="lang" value="<?= LANG ?>">
                    <input type="hidden" name="id" value="<?= htmlspecialcharsbx($info["EXT_ID"]) ?>">
                    <?= bitrix_sessid_post() ?>
                    <?
                    if ($info["IsInstalled"]) {
                        ?>
                        <input <? if (!$isAdmin) echo "disabled" ?> type="submit" name="uninstall"
                                                                    value="<?= GetMessage("MOD_DELETE") ?>">
                        <?
                    } else {
                        ?>
                        <input <? if (!$isAdmin) echo "disabled" ?> type="submit" class="adm-btn-green" name="install"
                                                                    value="<?= GetMessage("MOD_INSTALL_BUTTON") ?>">
                        <?
                    }
                    ?>
                </form>
            </td>
        </tr>
        <?
    }
    ?>
</table>
<?
require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin.php");
?>
