<?php
/**
 * User: agolodkov
 * Date: 05.06.2015
 * Time: 19:43
 */

use \Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler('main', 'OnBeforeEventAdd', ['MailEventHandler', 'buildRightHolderRequest']);


class MailEventHandler
{
    public static function buildRightHolderRequest(&$eventName, &$siteId,
                                                   &$eventFields, &$templateId
    ) {
        if (
            true === $eventFields['IS_MIDIFIED']
            || RIGHTHOLDER_CONTRACT_EMAIL_TEMPLATE_ID !== (integer)$templateId
            || 'SIMPLE_FORM_2' !== $eventFields['RS_FORM_SID']
        ) {
            return;
        }
        /**
         * @var CUser $USER
         */
        global $USER;

        $headSeparator = '*******************************';
        $lineSeparator = '<br>';
        $groupName = array(
            'USER'           => 'Физическое лицо',
            'ENTITY'         => 'Юридическое лицо',
            'BANK'           => 'Платежные реквизиты',
            'RH'             => 'Основание правообладания',
            'ENTITY_CONTACT' => 'Контакты',
        );
        $fieldNames = array(
            'USER'           => array(
                'NAME'               => 'Имя',
                'LAST_NAME'          => 'Фамилия',
                'SECOND_NAME'        => 'Отчество',
                'EMAIL'              => 'E-Mail',
                'PERSONAL_PHONE'     => 'Телефона',
                'PERSONAL_BIRTHDATE' => 'Дата рождения',
                'UF_PASSPORT_SERIES' => 'Серия пасспорта',
                'UF_PASSPORT_NUMBER' => 'Номер пасспорта',
                'REG_ADDRESS'        => 'Адрес регистрации',
                'HOME_ADDRESS'       => 'Адрес проживания',
            ),
            'ENTITY'         => array(
                'LEGAL_ADDRESS'   => 'Юридический адрес',
                'MAIL_ADDRESS'    => 'Почтовый адрес',
                'UF_PERSON_INN'   => 'ИНН юридического лица',
                'UF_PERSON_KPP'   => 'КПП юридического лица',
                'UF_PERSON_OKPO'  => 'ОКПО юридического лица',
                'UF_PERSON_OGRN'  => 'ОГРН юридического лица',
                'UF_PERSON_OKATO' => 'ОКАТО (ОКТМО) юридического лица',
                'UF_PERSON_KBK'   => 'КБК юридического лица',
            ),
            'BANK'           => array(
                'UF_BANK_NAME'        => 'Наименование банка',
                'UF_BANK_INN'         => 'ИНН банка',
                'UF_BANK_KPP'         => 'КПП банка',
                'UF_BANK_BIC'         => 'БИК банка',
                'UF_COR_ACCOUNT'      => 'Корреспондентский счет',
                'UF_PERSONAL_ACCOUNT' => 'Лицевой счет',
            ),
            'RH'             => array(
                'UF_FOUNDATION' => 'Основание правообладания',
                'UF_DOCUMENTS'  => 'Перечень документов',
            ),
            'ENTITY_CONTACT' => array(
                'UF_POSITION_HEAD' => 'Должность руководителя (полномочного представителя)',
                'UF_FIO_HEAD'      => 'ФИО руководителя (полномочного представителя)',
                'UF_CONTACT_INFO'  => 'ФИО контактного лица, полномочия контактного лица (Устав, доверенность, пр.)',
                'UF_CONTACT_PHONE' => 'Телефон контактного лица',
            ),
        );

        try {
            $ids = json_decode($eventFields['PUBLICATIONS_LIST_RAW']);
            $ids = array_filter($ids);
            if (empty($ids)) {
                throw new Exception('Empty publications list');
            }
            CModule::IncludeModule('nota.exalead');
            $books = new \Nota\Exalead\BiblioCardTable();
            $result = $books->getList(
                array(
                    'filter' => array(
                        'Id' => $ids,
                    ),
                )
            );
            $eventFields['PUBLICATIONS_LIST']
                = "<table>
<tr>
    <th>Название</th>
    <th>Автор</th>
    <th>ISBN</th>
    <th>Год издания</th>
    <th>Дата добавления</th>
</tr>";
            while ($item = $result->fetch()) {
                $item['BX_TIMESTAMP'] = strtotime($item['BX_TIMESTAMP']);
                $item['BX_TIMESTAMP'] = date('Y-m-d', $item['BX_TIMESTAMP']);
                $eventFields['PUBLICATIONS_LIST']
                    .= "<tr>
        <td>{$item['Name']}</td>
        <td>{$item['Author']}</td>
        <td>{$item['ISBN']}</td>
        <td>{$item['PublishYear']}</td>
        <td>{$item['BX_TIMESTAMP']}</td>
    </tr>";
            }
            $eventFields['PUBLICATIONS_LIST'] .= "</table>";
        } catch (Exception $e) {
            AddMessage2Log($e->getMessage());
        }

        try {
            $user = $USER->GetByID($eventFields['RS_USER_ID']);
            $user = $user->Fetch();
            if (!isset($user['ID'])) {
                throw new Exception('Not found user');
            }
            $eventFields['USER_EMAIL'] = $user['EMAIL'];
            $fields = array();
            $groups = array();
            $legalAddress
                = $user['PERSONAL_ZIP'] . ' ' . $user['PERSONAL_CITY'] . ' '
                . $user['PERSONAL_STREET'] . ' ' . $user['UF_CORPUS'] . ' '
                . $user['UF_STRUCTURE'] . ' ' . $user['UF_FLAT'];

            $user['REG_ADDRESS'] = $legalAddress;
            $user['HOME_ADDRESS'] = $user['UF_PLACE_REGISTR']
                ? $legalAddress
                : (
                    $user['WORK_ZIP'] . ' ' . $user['WORK_CITY'] . ' '
                    . $user['WORK_STREET'] . ' ' . $user['UF_HOUSE2'] . ' '
                    . $user['UF_STRUCTURE2'] . ' ' . $user['UF_FLAT2']
                );

            if ((integer)$user['UF_FOUNDATION'] > 0) {
                $foundation = CUserFieldEnum::GetList(
                    array(), array('ID' => $user['UF_FOUNDATION'])
                );
                $foundation = $foundation->Fetch();
                if (isset($foundation['VALUE'])) {
                    $user['UF_FOUNDATION'] = $foundation['VALUE'];
                }
            }

            $documentsFiles = array();
            $user['UF_DOCUMENTS'] = array_filter($user['UF_DOCUMENTS']);
            foreach ($user['UF_DOCUMENTS'] as $key => $fileId) {
                $file = CFile::GetFileArray($fileId);
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $file['SRC'])) {
                    $documentsFiles[] = $file['SRC'];
                    $file = pathinfo($file['SRC']);
                    $file = $file['basename'];
                    $user['UF_DOCUMENTS'][$key] = $file;
                } else {
                    unset($user['UF_DOCUMENTS']);
                }
            }
            $user['UF_DOCUMENTS'] = implode(
                $lineSeparator, $user['UF_DOCUMENTS']
            );

            if (nebUser::REGISTER_TYPE_RH_ENTITY
                === (integer)$user['UF_REGISTER_TYPE']
            ) {
                $user['LEGAL_ADDRESS'] = $user['REG_ADDRESS'];
                $user['MAIL_ADDRESS'] = $user['HOME_ADDRESS'];
                $groups = array(
                    'ENTITY',
                    'BANK',
                    'RH',
                    'ENTITY_CONTACT',
                );
            } elseif (nebUser::REGISTER_TYPE_RH
                === (integer)$user['UF_REGISTER_TYPE']
            ) {
                $groups = array(
                    'USER',
                    'BANK',
                    'RH',
                );
            }

            foreach ($groups as $group) {
                $content = '';
                $content .= $groupName[$group] . $lineSeparator
                    . $headSeparator . $lineSeparator;
                foreach ($fieldNames[$group] as $fieldCode => $fieldName) {
                    $content .= $fieldName . ': ' . $user[$fieldCode]
                        . $lineSeparator;
                }
                $fields[] = $content;
            }

            $eventFields['RIGHTHOLDER_FIELDS'] = implode(
                $lineSeparator . $lineSeparator, $fields
            );

            $eventFields['IS_MIDIFIED'] = true;
            CEvent::Send(
                $eventName,
                $siteId,
                $eventFields,
                'Y',
                $templateId,
                $documentsFiles
            );

            return false;
        } catch (Exception $e) {
            AddMessage2Log($e->getMessage());
        }
    }
}