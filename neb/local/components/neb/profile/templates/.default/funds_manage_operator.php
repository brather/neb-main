<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper clearfix ">


    <div class="b-breadcrumb bot">
        <h2><?=GetMessage('FUNDS_MANAGE_FUNDS_IN_NEL');?></h2>
    </div>

    <? $APPLICATION->IncludeComponent(
        "neb:library.funds.manage",
        "",
        Array(
            "LIBRARY_ID" => $arResult['LIBRARY']['ID'],
            "COUNTONPAGE" => 30
        ),
        false
    );
    ?>

</section>
