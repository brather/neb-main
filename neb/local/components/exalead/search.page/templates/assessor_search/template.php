<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(realpath(__DIR__ . '/../.default/template.php'));
?>
<button class="btn btn-success"
        data-save-sorting
        data-save-url="/api/assessor/query-rating/sort-editions/"
        data-sort-context='<?= json_encode($arResult['sortingData']) ?>'>
    Сохранить сортировку
</button><br /><br />

<main>
    <section class="search-result">
        <?php if (!empty($arResult['ITEMS'])): ?>
            <main class="search-result__content">
                <div class="row reverse-row">
                    <div>
                        <ul class="search-result__content-list clearfix">
                            <?php foreach ($arResult['ITEMS'] as $index => $arItem): ?>
                                <div class="item-container" data-edition-id="<?=$arItem['id']?>">
                                    <?php $APPLICATION->IncludeFile(
                                        '/local/components/exalead/search.page/templates/.default/search_row.php',
                                        array('arParams' => $arParams, 'arResult' => $arResult, 'index' => $index,
                                              'arItem'   => $arItem)
                                    ); ?>
                                </div>
                            <?php endforeach; ?>

                        </ul>
                    </div>
            </main>
        <?php endif; ?>
    </section>
</main>
<!-- /Контент КОНЕЦ -->