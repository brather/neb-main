<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    Bitrix\NotaExt\Iblock,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('notaext');

$query = htmlspecialcharsEx(trim($_REQUEST['query']));
if (empty($query))
    return false;

$arFilter = array(
    'IBLOCK_ID' => Bitrix\NotaExt\Ext\DetailGallery\Params::getParam('save_ib'),
    array(
        'LOGIC' => 'OR',
        array('NAME' => '%' . $query . '%'),
        array('PROPERTY_BASIC_TAGS.NAME' => '%' . $query . '%'),
        array('PROPERTY_ADDITIONAL_TAGS.NAME' => '%' . $query . '%'),
        array('PROPERTY_CREDIT' => '%' . $query . '%'),
        array('PREVIEW_TEXT' => '%' . $query . '%'),
    ),
    '!PREVIEW_PICTURE' => false
);

$arSelect = array('ID', 'NAME', 'IBLOCK_ID', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'PREVIEW_TEXT', 'PROPERTY_BASIC_TAGS', 'PROPERTY_ADDITIONAL_TAGS');
$arSort = array('SORT' => 'DESC', 'NAME' => 'ASC');

$resResult = Iblock\Element::getList($arFilter, 50, $arSelect, $arSort);

$arResult = array();
if (!empty($resResult)) {
    foreach ($resResult['ITEMS'] as $item) {
        #$item['PROPERTY_BASIC_TAGS_VALUE']
        #$item['PROPERTY_ADDITIONAL_TAGS_VALUE']

        $arItem = array();
        $arItem['ID'] = $item['ID'];
        $arItem['NAME'] = $item['NAME'];
        $arItem['PREVIEW_TEXT'] = $item['PREVIEW_TEXT'];
        $arItem['PREVIEW_PICTURE'] = CFile::GetPath($item['PREVIEW_PICTURE']);
        $arItem['DETAIL_PICTURE'] = CFile::GetPath($item['DETAIL_PICTURE']);
        $arResult[] = $arItem;
    }
}

MainHelper::showJson($arResult);
