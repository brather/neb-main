
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="text" name="" class="b-search_fieldtb b-text" id="" value="Станислав Лем">
						<!--<select name="searchopt" id=""class="js_select b_searchopt">
							<option value="1">по всем полям</option>
							<option value="2">по дате</option>

						</select>-->
						<input type="submit" class="b-search_bth bbox" value="Найти">
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right">
						<input class="checkbox js_btnsub" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		<div class="b-typicaltxt">
			<h2>Получение читательского билета РГБ</h2>
			<p>Запись в библиотеку предполагает создание читательского билета РГБ и предоставление доступа: </p>
			<ul class="b-commonlist">
				<li>в читальные залы библиотеки с возможностью заказывать и получать книги из фондов РГБ;</li>
				<li>ко всем услугам библиотеки;</li>
				<li>к электронным ресурсам, лицензионным базам данных и электронным версиям изданий.</li>
			</ul>
			<p>Читательский билет определяется уникальным номером и выдается сроком на пять лет. </p>
			<p>При удаленной записи в библиотеку создается электронный читательский билет. Пластиковый читательский билет с фотографией для прохода в читальные залы РГБ можно получить при личном визите в группу записи читателей.</p>
			<h3>Способы записи:</h3>
			<h4>Запись онлайн</h4>
			<p>Услуга предоставляется гражданам Российской Федерации старше 18 лет (студентам вузов с любого возраста). </p>
			<p><strong>Необходимые электронные копии документов:</strong></p>
			<ul class="b-commonlist">
				<li>разворот паспорта (военного билета) с личными данными;</li>
				<li>разворот паспорта с текущей регистрацией;</li>
				<li>для студентов младше 18 лет — студенческий билет или зачетная книжка.</li>
			</ul>
			<p>Также понадобится номер банковской карты для идентификации личности. (При получении электронного читательского билета оплата не взимается! Для идентификации на карте будет заблокирована, а затем разблокирована сумма 5 руб. 1 коп.). <a href="#">Запись онлайн</a> возможна для зарегистрированных на сайте РГБ читателей и доступна из Личного кабинета. </p>
			<h4>Очная запись</h4>
			<p>Читательский билет РГБ можно получить по адресу: </p>
			<ul class="b-commonlist">
				<li>г. Москва, ул. Воздвиженка, д. 3/5, Российская государственная библиотека, подъезд 2, группа записи читателей;</li>
				<li>г. Химки, Московская обл., ул. Библиотечная, д. 15, Российская государственная библиотека, читальный зал отдела диссертаций;</li>
				<li>г. Москва, ул. Образцова, д. 11, Еврейский музей и Центр толерантности, отдел РГБ.</li>
			</ul>
			<p>Право пользования читальными залами библиотеки предоставляется всем гражданам Российской Федерации и других государств старше 18 лет. Студенты высших учебных заведений могут записаться в РГБ с любого возраста.</p>
			<p>При записи выдается пластиковый читательский билет с фотографией. Фотосъемка оплачивается посетителем. Стоимость фотосъемки — 100 руб.</p>
			<p>Изготовление дубликата читательского билета (взамен утраченного) — 100 руб.</p>
			<p>Читательский билет дает право пользования фондами библиотеки и не может быть передан другому лицу.</p>
			<p><strong>Необходимые документы:</strong></p>
			<ul class="b-commonlist">
				<li>для граждан России — паспорт или военный билет с отметкой о регистрации по месту жительства или месту пребывания;</li>
				<li>для граждан других государств — документ с переводом на русский язык, удостоверяющий личность, с визой или регистрацией ОВИРа (посольства, консульства);</li>
				<li>для лиц с высшим образованием — документ о высшем образовании;</li>
				<li>для студентов — студенческий билет или зачетная книжка.</li>
			</ul>
			<h4>Запись с отправкой документов по почте</h4>
			<p>Читательский билет РГБ можно получить по адресу: </p>
			<ul class="b-commonlist">
				<li>г. Москва, ул. Воздвиженка, д. 3/5, Российская государственная библиотека, подъезд 2, группа записи читателей;</li>
				<li>г. Химки, Московская обл., ул. Библиотечная, д. 15, Российская государственная библиотека, читальный зал отдела диссертаций;</li>
				<li>г. Москва, ул. Образцова, д. 11, Еврейский музей и Центр толерантности, отдел РГБ.</li>
			</ul>
			<p>Право пользования читальными залами библиотеки предоставляется всем гражданам Российской Федерации и других государств старше 18 лет. Студенты высших учебных заведений могут записаться в РГБ с любого возраста.</p>
			<p>При записи выдается пластиковый читательский билет с фотографией. Фотосъемка оплачивается посетителем. Стоимость фотосъемки — 100 руб.</p>
			<p>Изготовление дубликата читательского билета (взамен утраченного) — 100 руб.</p>
			<p>Читательский билет дает право пользования фондами библиотеки и не может быть передан другому лицу.</p>
			<p><strong>Необходимые документы:</strong></p>
			<ul class="b-commonlist">
				<li>для граждан России — паспорт или военный билет с отметкой о регистрации по месту жительства или месту пребывания;</li>
				<li>для граждан других государств — документ с переводом на русский язык, удостоверяющий личность, с визой или регистрацией ОВИРа (посольства, консульства);</li>
				<li>для лиц с высшим образованием — документ о высшем образовании;</li>
				<li>для студентов — студенческий билет или зачетная книжка.</li>
			</ul>

		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
	

</section>

</div><!-- /.homepage -->

<div class="b-mylibrary">
	<div class="wrapper">
		<div class="b-mylibrarytit">Моя библиотека</div>
		<ul class="b-mylibrarylist">
			<li>
				<a href="#">Сейчас  читаю</a> <span class="b-num">2</span>
			</li>
			<li><a href="#">В закладках</a><span class="b-num">14</span></li>
			<li><a href="#">Буду читать</a><span class="b-num">47</span></li>
		</ul>
	</div>
</div><!-- /.b-mylibrary -->
<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<!--<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu js_openmenu">мои подборки</a>
	
	<ul class="b-selectionlist">
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb8">	<label for="cb8" class="black">Любимые авторы</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb9">	<label for="cb9" class="black">Научно-популярная фантастика</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb10"><label for="cb10" class="black">Ракеты и люди</label>

		</li>	
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb14"><label for="cb14" class="lite">Отметить как прочитанное</label>

		</li>
		<li><a href="#" class="b-selection_add"><span>+</span>Cоздать подборку</a></li>
	</ul>
</div>--><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>