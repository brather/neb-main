<?php
$MESS['NAME'] = 'Name';
$MESS['NAME_TITLE'] = 'Name';
$MESS['FULL_NAME'] = 'Full name';
$MESS['FIND'] = 'Find';
$MESS['EDIT'] = 'Edit';
$MESS['DELETE'] = 'Delete';
$MESS['ACTION'] = 'Action';
$MESS['CONFIRM'] = 'Confirm';
$MESS['REFUSE'] = 'Refuse';
$MESS['CONFIRMED'] = 'Confirmed';
$MESS['REFUSED'] = 'Refused';