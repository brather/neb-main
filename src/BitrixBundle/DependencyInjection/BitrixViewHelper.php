<?php
/**
 * User: agolodkov
 * Date: 10.08.2016
 * Time: 14:45
 */

namespace BitrixBundle\DependencyInjection;


use Symfony\Component\Templating\Helper\Helper;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class BitrixViewHelper
 *
 * @DI\Service("bitrix.view.helper")
 * @DI\Tag("templating.helper", attributes = {"alias" = "bitrix_view_helper"})
 */
class BitrixViewHelper extends Helper
{
    /**
     * @var BitrixHelper
     */
    private $_bitrix;

    /**
     * @DI\InjectParams({
     *     "bitrix" = @DI\Inject("bitrix_helper")
     * })
     * @param BitrixHelper $bitrix
     */
    public function __construct(BitrixHelper $bitrix)
    {
        $this->_bitrix = $bitrix;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bitrixViewHelper';
    }

    /**
     * @param $path
     */
    public function addJs($path)
    {
        $this->_bitrix->getAsset()->addJs($path);
    }

    /**
     * @return \Bitrix\Main\Page\Asset
     */
    public function getAsset() {
        return $this->_bitrix->getAsset();
    }


    /**
     * @param $path
     */
    public function addCss($path)
    {
        $this->_bitrix->getAsset()->addCss($path);
    }

    public function showHeader()
    {
        $this->_bitrix->requireHeader();
    }

    public function showFooter()
    {
        $this->_bitrix->requireFooter();
    }

    /**
     * @return BitrixHelper
     */
    public function getBitrix()
    {
        return $this->_bitrix;
    }

}