<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
if(defined('STATIC_PAGE'))
{
    ?>
		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
</section>
<?
}
?>
</div><!-- /.homepage -->
<footer>
    <div class="wrapper">
        <div class="b-footer_row clearfix">
            <?$APPLICATION->IncludeFile("/local/include_areas/logo_bottom.php", Array(), Array("MODE" => "text"));?>
            <div class="footer-container">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
                    "ROOT_MENU_TYPE" => "bottom1_" . LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
                    "ROOT_MENU_TYPE" => "bottom2_" . LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
                    "ROOT_MENU_TYPE" => "bottom3_" . LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                    false
                );?>
            </div>
        </div>
        <div class="b-footer_row clearfix">
            <?$APPLICATION->IncludeFile("/local/include_areas/copyright.php", Array(), Array("MODE" => "text"));?>
            <?$APPLICATION->IncludeFile("/local/include_areas/ministr.php", Array(), Array("MODE" => "text"));?>
        </div>
    </div><!-- /.wrapper -->
</footer>
    
<!--popup добавить в подборки-->
<div class="selectionBlock">
    <a href="#" class="close"></a>
    <span class="selection_lb"><?=GetMessage('FOOTER_ADD_TO')?> <?=GetMessage('FOOTER_MY_COLLECTIONS')?></span>
    <form class="b-selectionadd collection" action="<?=ADD_COLLECTION_URL?>add.php" method="post">
        <input type="hidden" name="t" value="books"/>
        <input type="submit" class="b-selectionaddsign" value="+">
        <span class="b-selectionaddtxt"><?=GetMessage('FOOTER_CREATE_COLLECTION')?></span>
        <input type="text" name="name" class="input hidden">
    </form>
</div><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->

<?
    $ALERT_REMOVE_MESSAGE = $APPLICATION->GetPageProperty("ALERT_REMOVE_MESSAGE");
?>

<!--popup Удалить из подборки--><div class="b-removepopup hidden"><p><?=empty($ALERT_REMOVE_MESSAGE)? GetMessage('FOOTER_REMOVE_BOOK').'?' : $ALERT_REMOVE_MESSAGE?></p><a href="#" class="formbutton btremove"><?=GetMessage('FOOTER_ACCEPT_REMOVE')?></a><a href="#" class="formbutton gray"><?=GetMessage('FOOTER_KEEP_BOOK')?></a></div><!--/popup Удалить из подборки-->

</body>
</html>