<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Вход через социальные сети");
?>

<?
$APPLICATION->IncludeComponent("bitrix:system.auth.form", "special_soc",
    Array(
        "FORGOT_PASSWORD_URL" => "/special/auth/",
        "SHOW_ERRORS" => "N"
    ),
    false
);
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>