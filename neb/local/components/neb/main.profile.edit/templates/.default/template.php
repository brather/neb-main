<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 * profile/readers/edit
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>
<div class="profile-container-readers-profile" data-component="profile/readers/edit">
    <h2><?= GetMessage('SETTING'); ?></h2>

    <? ShowError($arResult["strProfileError"]); ?>
    <?
    if ($arResult['DATA_SAVED'] == 'Y')
        // ShowNote(GetMessage('PROFILE_DATA_SAVED'));
        LocalRedirect("/profile/readers/");
    ?>
    <?
    if ($arResult['arUser']['EXTERNAL_AUTH_ID'] != 'socservices') {
        ?>

        <? $APPLICATION->IncludeComponent("neb:main.password.reset", "", Array(
            "USER_ID" => $arParams["USER_ID"],
        )); ?>

        <?
    }
    ?>
    <form method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>" class="exist-reader-form nrf"
          enctype="multipart/form-data">
        <script>
            var rulesAndMessages = {
                rules: {},
                messages: {}
            };
        </script>
        <?= $arResult["BX_SESSION_CHECK"] ?>
        <input type="hidden" name="lang" value="<?= LANG ?>"/>
        <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
        <!--div class="b-form_header"><?= GetMessage('GENERAL_CONST'); ?></div-->

        <div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_FIELD_PASSPORT'); ?></div>

        <div class="row">
            <div class="col-md-4 form-group">
                <label for="surname">
                    <em class="hint">*</em>
                    <?= Loc::getMessage('MAIN_REGISTER_LASTNAME'); ?>
                </label>
                <input type="text"
                       data-required="required"
                       value="<?= $arResult["arUser"]['LAST_NAME'] ?>"
                       id="surname"
                       name="LAST_NAME"
                       class="form-control">
                <script>
                    rulesAndMessages.rules["LAST_NAME"] = {
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["LAST_NAME"] = {
                        required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?>",
                        minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_2_SYMBOLS');?>"
                    };
                </script>
            </div>
            <div class="col-md-4 form-group">
                <label for="firstname">
                    <em class="hint">*</em>
                    <?= Loc::getMessage('MAIN_REGISTER_FIRSTNAME'); ?>
                </label>
                <input
                    type="text"
                    data-required="required"
                    data-validate="fio"
                    value="<?= $arResult["arUser"]['NAME'] ?>"
                    id="firstname"
                    name="NAME"
                    class="form-control">
                <script>
                    rulesAndMessages.rules["NAME"] = {
                        required: true,
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["NAME"] = {
                        required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?>",
                        minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_2_SYMBOLS');?>"
                    };
                </script>
            </div>
            <div class="col-md-4 form-group">
                <label for="midname">
                    <?= Loc::getMessage('MAIN_REGISTER_MIDNAME'); ?>
                </label>
                <input type="text"
                       value="<?= $arResult["arUser"]['SECOND_NAME'] ?>"
                       id="midname"
                       name="SECOND_NAME"
                       class="form-control">
                <script>
                    rulesAndMessages.rules["SECOND_NAME"] = {
                        required: false,
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["SECOND_NAME"] = {
                        required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?>",
                        minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_2_SYMBOLS');?>"
                    };
                </script>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-xs-12">
                <em class="hint">*</em>
                <?= Loc::getMessage('MAIN_REGISTER_GENDER'); ?>
            </label>

            <label class="radio-inline">
                <input type="radio"
                       name="PERSONAL_GENDER"
                    <?= ('M' === $arResult['arUser']['PERSONAL_GENDER'] ? 'checked' : '') ?>
                       value="M">
                <i class="lbl"></i>
                <?= Loc::getMessage('MAIN_REGISTER_GENDER_MALE'); ?>
            </label>
            <label class="radio-inline">
                <input type="radio"
                       name="PERSONAL_GENDER"
                    <?= ('F' === $arResult['arUser']['PERSONAL_GENDER'] ? 'checked' : '') ?>
                       value="F">
                <i class="lbl"></i>
                <?= Loc::getMessage('MAIN_REGISTER_GENDER_FEMALE'); ?>
            </label>
            <script>
                rulesAndMessages.rules["PERSONAL_GENDER"] = {
                    required: true
                };
                rulesAndMessages.messages["PERSONAL_GENDER"] = {
                    required: "<?= Loc::getMessage('MAIN_REGISTER_SELECT'); ?>"
                };
            </script>
        </div>

        <div class="row">
            <div class="col-md-4 form-group">
                <label for="userbirthday">
                    <em class="hint">*</em>
                    <?= Loc::getMessage('MAIN_REGISTER_BIRTHDAY'); ?>
                </label>
                <div class="input-group">
                    <input
                        type="text"
                        class="form-control"
                        data-required="true"
                        value="<?= $arResult['arUser']['PERSONAL_BIRTHDAY'] ?>"
                        id="userbirthday"
                        name="PERSONAL_BIRTHDAY"
                        data-yearsrestrict="12"
                    >
						<span class="input-group-btn">
		                    <a class="btn btn-default"
                               id="calendarlabel"
                               onclick="BX.calendar({node: 'userbirthday', field: 'userbirthday',  form: '', bTime: false, value: ''});"
                            >
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </a>
		                </span>
                </div>
                <script>
                    rulesAndMessages.rules["PERSONAL_BIRTHDAY"] = {
                        required: true,
                        checkDateFormat: true,
                        checkDateCorrect: true,
                        checkAge: 12,
                    };
                    rulesAndMessages.messages["PERSONAL_BIRTHDAY"] = {
                        required: "<?=Loc::getMessage('MAIN_REGISTER_FILL');?>",
                        checkDateFormat: "<?=Loc::getMessage('MAIN_REGISTER_DOB_FORMAT');?>",
                        checkDateCorrect: "<?=Loc::getMessage('MAIN_REGISTER_DOB_INCORRECT');?>",
                        checkAge: "<?=Loc::getMessage('MAIN_REGISTER_AGE_WITH_PLACEHOLDER');?>"
                    };
                </script>
            </div>
        </div>

        <div class="row">
            <label for="passerial" class="col-xs-12">
                <em class="hint">*</em>
                <?= Loc::getMessage('REGISTER_FIELD_PASSPORT_NUMBERS'); ?>
            </label>
            <div class="col-md-4 form-group">
                <input type="text"
                       data-required="required"
                       value="<?= $arResult["arUser"]['UF_PASSPORT_SERIES'] ?>"
                       id="passerial"
                       name="UF_PASSPORT_SERIES"
                       class="form-control">
                <script>
                    rulesAndMessages.rules["UF_PASSPORT_SERIES"] = {
                        required: true
                    };
                    rulesAndMessages.messages["UF_PASSPORT_SERIES"] = {
                        required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>"
                    };
                </script>
            </div>
            <div class="col-md-4 form-group">
                <input type="text"
                       data-required="required"
                       value="<?= $arResult["arUser"]['UF_PASSPORT_NUMBER'] ?>"
                       id="passnumber"
                       name="UF_PASSPORT_NUMBER"
                       class="form-control">
                <script>
                    rulesAndMessages.rules["UF_PASSPORT_NUMBER"] = {
                        required: true
                    };
                    rulesAndMessages.messages["UF_PASSPORT_NUMBER"] = {
                        required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>"
                    };
                </script>
            </div>
        </div>


        <div class="form-group">
            <label for="UF_CITIZENSHIP">
                <em class="hint">*</em>
                <?= Loc::getMessage('MAIN_REGISTER_CITIZENSHIP'); ?>
            </label>
            <select name="UF_CITIZENSHIP" id="UF_CITIZENSHIP" class="form-control" data-required="true">
                <?
                $arUF_CITIZENSHIP = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'), array("VALUE" => "ASC"));
                if (!empty($arUF_CITIZENSHIP)) {
                    $last = count($arUF_CITIZENSHIP);
                    foreach ($arUF_CITIZENSHIP as $arItem) {
                        if ($arItem['XML_ID'] == $last) {
                            $other = $arItem;
                            continue;
                        }
                        if ($arItem['DEF'] == "Y") {
                            $ruCitizenship = $arItem['ID'];
                        }
                        ?>
                        <option
                            value="<?= $arItem['ID'] ?>"<?= ($arItem['DEF'] == "Y") ? ' selected="selected"' : ''; ?>><?= $arItem['VALUE'] ?></option>
                        <?php
                    }
                    ?>
                    <option
                        value="<?= $other['ID'] ?>"<?= ($other['DEF'] == "Y") ? ' selected="selected"' : ''; ?>><?= $other['VALUE'] ?></option>

                    <?
                }
                ?>
            </select>

            <script type="text/javascript">
                /*
                 $(function(){
                 	var copyEducation = $('select[name="UF_EDUCATION"]').clone().removeClass("custom");
                 	function correctEducationField()
                 	{
                 		var cit = $('select[name="UF_CITIZENSHIP"] option:selected').val();
                 		var parentEducation = $('select[name="UF_EDUCATION"]').closest("div.field");
                 		parentEducation.children("span").remove();
                 		parentEducation.append(copyEducation.clone());
                 		$('#UF_EDUCATIONopts').remove();
                 		if(cit == <?php echo $ruCitizenship;?>)
                 		{
                 			$('select[name="UF_EDUCATION"] option').filter('[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
                 		}
                 		else
                 		{
                 			$('select[name="UF_EDUCATION"] option').not('[VALUE=""],[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
                 		}
                 		$('select[name="UF_EDUCATION"]').selectReplace();
                 	}
                 	correctEducationField();
                 	$('select[name="UF_CITIZENSHIP"]').change(function(){
                 		correctEducationField();
                 	});
                 });
                */
            </script>
        </div>

        <div class="nrf-fieldset-title"><?= Loc::getMessage('MAIN_REGISTER_SPECIALIZATION'); ?></div>

        <div class="form-group">
            <label for="mainemployment">
                <?= Loc::getMessage('MAIN_REGISTER_PLACE_OF_EMPLOYMENT'); ?>
            </label>
            <input type="text"
                   value="<?= $arResult["arUser"]['WORK_COMPANY'] ?>"
                   id="mainemployment"
                   data-required="true"
                   name="WORK_COMPANY"
                   class="form-control">
            <script>
                rulesAndMessages.rules["WORK_COMPANY"] = {
                    required: false,
                    maxlength: 60,
                    minlength: 2
                };
                rulesAndMessages.messages["WORK_COMPANY"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                    maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                    minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                };
            </script>
        </div>

        <div class="form-group">
            <label for="branchknowledge">
                <?= Loc::getMessage('MAIN_REGISTER_KNOWLEDGE'); ?>
            </label>
            <select name="UF_BRANCH_KNOWLEDGE" id="branchknowledge" class="form-control" data-required="true">
                <option value=""><?= Loc::getMessage('MAIN_REGISTER_SELECT'); ?></option>
                <?
                $arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE'));
                if (!empty($arUF_BRANCH_KNOWLEDGE)) {
                    foreach ($arUF_BRANCH_KNOWLEDGE as $arItem) {
                        ?>
                        <option <?= $arResult["arUser"]['UF_BRANCH_KNOWLEDGE'] == $arItem['ID'] ? 'selected="selected"' : '' ?>
                            value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
            <script>
                rulesAndMessages.rules["UF_BRANCH_KNOWLEDGE"] = {
                    required: false
                };
                rulesAndMessages.messages["UF_BRANCH_KNOWLEDGE"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>"
                };
            </script>
        </div>

        <div class="form-group">
            <label for="maineducation">
                <?= Loc::getMessage('MAIN_REGISTER_EDUCATION'); ?>
            </label>
            <select name="UF_EDUCATION" id="maineducation" class="form-control" data-required="true">
                <option value=""><?= Loc::getMessage('MAIN_REGISTER_SELECT'); ?></option>
                <?
                $arUF_EDUCATION = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION'));
                if (!empty($arUF_EDUCATION)) {
                    foreach ($arUF_EDUCATION as $arItem) {
                        ?>
                        <option <?= $arResult["arUser"]['UF_EDUCATION'] == $arItem['ID'] ? 'selected="selected"' : '' ?>
                            value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
            <script>
                rulesAndMessages.rules["UF_EDUCATION"] = {
                    required: false
                };
                rulesAndMessages.messages["UF_EDUCATION"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>"
                };
            </script>
        </div>

        <div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_GROUP_SIGN'); ?></div>

        <div class="form-group">
            <label for="userlogin">
                <em class="hint">*</em>
                <?= Loc::getMessage('MAIN_REGISTER_LOGIN'); ?>
            </label>
            <input
                type="text"
                class="form-control"
                name="LOGIN"
                id="userlogin"
                value="<?= $arResult["arUser"]['LOGIN'] ?>"
                data-required="true">
            <script>
                rulesAndMessages.rules["LOGIN"] = {
                    required: true,
                    maxlength: 50,
                    minlength: 3
                };
                rulesAndMessages.messages["LOGIN"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                    maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                    minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                };
            </script>
        </div>
        <div class="form-group">
            <label for="useremail">
                <em class="hint">*</em>
                <?= Loc::getMessage('MAIN_REGISTER_EMAIL'); ?>
            </label>
            <input
                type="text"
                class="form-control"
                name="EMAIL"
                id="useremail"
                value="<?= $arResult["arUser"]['EMAIL'] ?>"
                data-required="true">
            <script>
                rulesAndMessages.rules["EMAIL"] = {
                    required: true,
                    email: true,
                    maxlength: 50,
                    minlength: 3
                };
                rulesAndMessages.messages["EMAIL"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                    email: "<?=Loc::getMessage('MAIN_REGISTER_EMAIL_FORMAT');?>",
                    maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                    minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                };
            </script>
        </div>

        <div class="form-group">
            <label for="usermobile">
                <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_MOBILE'); ?>
                <span class="phone_note"><?= Loc::getMessage('REGISTER_FIELD_PERSONAL_MOBILE_NOTE'); ?></span>
            </label>
            <input
                type="text"
                class="form-control"
                name="PERSONAL_MOBILE"
                id="usermobile"
                data-masked
                style="font-family:monospace"
                value="<?= $arResult["arUser"]['PERSONAL_MOBILE'] ?>">
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_REGISTERED'); ?></div>
                <div class="checkbox">&nbsp;</div>
                <div class="form-group">
                    <label for="userregzip">
                        <em class="hint">*</em>
                        <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP'); ?>
                    </label>
                    <input
                        type="number"
                        data-validate="number"
                        data-required="required"
                        value="<?= $arResult["arUser"]['PERSONAL_ZIP'] ?>"
                        id="userregzip"
                        name="PERSONAL_ZIP"
                        class="form-control">
                    <script>
                        rulesAndMessages.rules["PERSONAL_ZIP"] = {
                            required: true,
                            number: true
                        };
                        rulesAndMessages.messages["PERSONAL_ZIP"] = {
                            required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                            number: "<?=Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP_INCORRECT');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="userregregion">
                        <em class="hint">*</em>
                        <?= Loc::getMessage('MAIN_REGISTER_REGION'); ?>
                        <span class="minscreen"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?></span>
                    </label>
                    <input
                        type="text"
                        data-required="required"
                        value="<?= $arResult["arUser"]['UF_REGION'] ?>"
                        id="userregregion"
                        name="UF_REGION"
                        class="form-control">
                    <script>
                        rulesAndMessages.rules["UF_REGION"] = {
                            required: false,
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["UF_REGION"] = {
                            required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="userareareg">
                        <?= Loc::getMessage('MAIN_REGISTER_AREA'); ?>
                    </label>
                    <input
                        type="text"
                        value="<?= $arResult["arUser"]['UF_AREA'] ?>"
                        id="userareareg"
                        name="UF_AREA"
                        class="form-control">
                    <script>
                        rulesAndMessages.rules["UF_AREA"] = {
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["UF_AREA"] = {
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="userregcity">
                        <em class="hint">*</em>
                        <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_CITY'); ?>
                    </label>
                    <input
                        type="text"
                        data-required="required"
                        value="<?= $arResult["arUser"]['PERSONAL_CITY'] ?>"
                        id="userregcity"
                        name="PERSONAL_CITY"
                        class="form-control">
                    <script>
                        rulesAndMessages.rules["PERSONAL_CITY"] = {
                            required: true,
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["PERSONAL_CITY"] = {
                            required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>

                <div class="form-group">
                    <label for="userregstreet">
                        <em class="hint">*</em>
                        <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_STREET'); ?>
                        <span class="minscreen"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?></span>
                    </label>
                    <input
                        type="text"
                        value="<?= $arResult["arUser"]['PERSONAL_STREET'] ?>"
                        id="userregstreet"
                        data-required="true"
                        name="PERSONAL_STREET"
                        class="form-control">
                    <script>
                        rulesAndMessages.rules["PERSONAL_STREET"] = {
                            required: true,
                            maxlength: 30,
                            minlength: 2
                        };
                        rulesAndMessages.messages["PERSONAL_STREET"] = {
                            required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                        };
                    </script>
                </div>

                <div class="row">
                    <div class="col-md-4 form-group">
                        <label for="userreghouse">
                            <em class="hint">*</em>
                            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_HOUSE'); ?>:
                        </label>
                        <input
                            type="text"
                            class="form-control"
                            name="UF_CORPUS"
                            id="userreghouse"
                            value="<?= $arResult["arUser"]['UF_CORPUS'] ?>"
                            data-required="true"/>
                        <script>
                            rulesAndMessages.rules["UF_CORPUS"] = {
                                required: true,
                                maxlength: 4
                            };
                            rulesAndMessages.messages["UF_CORPUS"] = {
                                required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                                maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="userregcorp"><?= Loc::getMessage('REGISTER_FIELD_PERSONAL_BUILDING'); ?>: </label>
                        <input
                            type="text"
                            class="form-control"
                            name="UF_STRUCTURE"
                            id="userregcorp"
                            value="<?= $arResult["arUser"]['UF_STRUCTURE'] ?>"/>
                        <script>
                            rulesAndMessages.rules["UF_STRUCTURE"] = {
                                maxlength: 4
                            };
                            rulesAndMessages.messages["UF_STRUCTURE"] = {
                                maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="userregapart"><?= Loc::getMessage('REGISTER_FIELD_PERSONAL_APPARTMENT'); ?>
                            : </label>
                        <input
                            type="text"
                            class="form-control"
                            name="UF_FLAT"
                            id="userregapart"
                            value="<?= $arResult["arUser"]['UF_FLAT'] ?>"/>
                        <script>
                            rulesAndMessages.rules["UF_FLAT"] = {
                                maxlength: 4
                            };
                            rulesAndMessages.messages["UF_FLAT"] = {
                                maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RESIDENCE'); ?></div>
                <div class="form-group">
                    <label class="checkbox">
                        <input
                            class="checkbox"
                            type="checkbox"
                            data-checkbox-for="UF_PLACE_REGISTR"
                            id="livwhereregistered"
                            <?= $arResult['arUser']['UF_PLACE_REGISTR'] == '1' ? 'checked' : '' ?>
                        >
                        <span class="lbl"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_CHECKBOX_NOTE'); ?></span>
                    </label>
                    <input
                        type="hidden"
                        name="UF_PLACE_REGISTR"
                        value="<?= $arResult['arUser']['UF_PLACE_REGISTR'] ?>"
                    >
                </div>

                <div class="living-address-group">

                    <div class="form-group">
                        <label for="livingzip">
                            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP'); ?>
                        </label>
                        <input
                            type="number"
                            data-validate="number"
                            value="<?= $arResult["arUser"]['WORK_ZIP'] ?>"
                            id="livingzip"
                            data-mirror="userregzip"
                            name="WORK_ZIP"
                            class="form-control">
                        <script>
                            rulesAndMessages.rules["WORK_ZIP"] = {
                                number: true
                            };
                            rulesAndMessages.messages["WORK_ZIP"] = {
                                number: "<?=Loc::getMessage('REGISTER_FIELD_PERSONAL_DIGITS_ONLY');?>"
                            };
                        </script>
                    </div>

                    <div class="form-group">
                        <label for="livingregion">
                            <?= Loc::getMessage('MAIN_REGISTER_REGION'); ?>
                            <span class="minscreen"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?></span>
                        </label>
                        <input
                            type="text"
                            value="<?= $arResult["arUser"]['UF_REGION2'] ?>"
                            id="livingregion"
                            data-mirror="userregregion"
                            name="UF_REGION2"
                            class="form-control">
                        <script>
                            rulesAndMessages.rules["UF_REGION2"] = {
                                maxlength: 30,
                                minlength: 2
                            };
                            rulesAndMessages.messages["UF_REGION2"] = {
                                maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                                minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>

                    <div class="form-group">
                        <label for="userarealive">
                            <?= Loc::getMessage('MAIN_REGISTER_AREA'); ?>
                            <span class="minscreen"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?></span>
                        </label>
                        <input
                            type="text"
                            value="<?= $arResult["arUser"]['UF_AREA2'] ?>"
                            id="userarealive"
                            data-mirror="userareareg"
                            name="UF_AREA2"
                            class="form-control">
                        <script>
                            rulesAndMessages.rules["UF_AREA2"] = {
                                maxlength: 30,
                                minlength: 2
                            };
                            rulesAndMessages.messages["UF_AREA2"] = {
                                maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                                minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>

                    <div class="form-group">
                        <label for="usercityliving">
                            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_CITY'); ?>
                        </label>
                        <input
                            type="text"
                            value="<?= $arResult["arUser"]['WORK_CITY'] ?>"
                            id="usercityliving"
                            data-mirror="userregcity"
                            name="WORK_CITY"
                            class="form-control">
                        <script>
                            rulesAndMessages.rules["WORK_CITY"] = {
                                maxlength: 30,
                                minlength: 2
                            };
                            rulesAndMessages.messages["WORK_CITY"] = {
                                maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                                minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>

                    <div class="form-group">
                        <label for="userstreetliving">
                            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_STREET'); ?>
                            <span class="minscreen"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?></span>
                        </label>
                        <input
                            type="text"
                            value="<?= $arResult["arUser"]['WORK_STREET'] ?>"
                            id="userstreetliving"
                            data-mirror="userregstreet"
                            name="WORK_STREET"
                            class="form-control">
                        <script>
                            rulesAndMessages.rules["WORK_STREET"] = {
                                maxlength: 30,
                                minlength: 2
                            };
                            rulesAndMessages.messages["WORK_STREET"] = {
                                maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                                minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                            };
                        </script>
                    </div>

                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="userhouseliving">
                                <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_HOUSE'); ?>:
                            </label>
                            <input
                                type="text"
                                class="form-control"
                                name="UF_HOUSE2"
                                data-maxlength="20"
                                id="userhouseliving"
                                data-mirror="userreghouse"
                                value="<?= $arResult["arUser"]['UF_HOUSE2'] ?>"/>
                            <script>
                                rulesAndMessages.rules["UF_HOUSE2"] = {
                                    maxlength: 4
                                };
                                rulesAndMessages.messages["UF_HOUSE2"] = {
                                    maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>"
                                };
                            </script>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="userstructliving">
                                <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_BUILDING'); ?>:
                            </label>
                            <input
                                type="text"
                                class="form-control"
                                name="UF_STRUCTURE2"
                                data-maxlength="20"
                                maxlength="4"
                                id="userstructliving"
                                data-mirror="userregcorp"
                                value="<?= $arResult["arUser"]['UF_STRUCTURE2'] ?>"/>
                            <script>
                                rulesAndMessages.rules["UF_STRUCTURE2"] = {
                                    maxlength: 4
                                };
                                rulesAndMessages.messages["UF_STRUCTURE2"] = {
                                    maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>"
                                };
                            </script>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="userappartliving">
                                <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_APPARTMENT'); ?>:
                            </label>
                            <input
                                type="text"
                                class="form-control"
                                name="UF_FLAT2"
                                id="userappartliving"
                                data-mirror="userregapart"
                                value="<?= $arResult["arUser"]['UF_FLAT2'] ?>"/>
                            <script>
                                rulesAndMessages.rules["UF_FLAT2"] = {
                                    maxlength: 4
                                };
                                rulesAndMessages.messages["UF_FLAT2"] = {
                                    maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>"
                                };
                            </script>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings07"><?= GetMessage('ABOUT'); ?></label>
					<div class="field validate">
						<textarea class="textarea" name="PERSONAL_NOTES" id="settings07" data-minlength="2" data-maxlength="800" ><?= $arResult['arUser']['PERSONAL_NOTES'] ?></textarea>
					</div>
				</div>
			</div-->

        <!-- ВКЛЮЧИТЬ АПЛОАДИЛКУ -->
        <?
        $sImage = (intval($arResult['arUser']["UF_SCAN_PASSPORT1"]) > 0) ? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT1"]) : '';
        if (isset($arResult['arUser']["UF_SCAN_PASSPORT1"]["SRC"])) {
            $sImage = $arResult['arUser']["UF_SCAN_PASSPORT1"]["SRC"];
        }
        ?>
        <? $APPLICATION->IncludeComponent(
            "notaext:plupload",
            "scan_passport1",
            array(
                "MAX_FILE_SIZE" => "24",
                "FILE_TYPES" => "jpg,jpeg,png",
                "DIR" => "tmp_register",
                "FILES_FIELD_NAME" => "profile_file",
                "MULTI_SELECTION" => "N",
                "CLEANUP_DIR" => "Y",
                "UPLOAD_AUTO_START" => "Y",
                "UNIQUE_NAMES" => "Y",
                "PERSONAL_PHOTO" => $sImage,
                "FULL_NAME" => $arResult['arUser']['NAME'] . ' ' . $arResult['arUser']['LAST_NAME'],
                "USER_EMAIL" => $arResult['arUser']['EMAIL'],
                "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
                "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
                "USER_ID" => $arParams["USER_ID"],
            ),
            false
        ); ?>

        <!-- ВКЛЮЧИТЬ АПЛОАДИЛКУ -->
        <?
        $sImage = (intval($arResult['arUser']["UF_SCAN_PASSPORT2"]) > 0) ? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT2"]) : '';
        if (isset($arResult['arUser']["UF_SCAN_PASSPORT2"]["SRC"])) {
            $sImage = $arResult['arUser']["UF_SCAN_PASSPORT2"]["SRC"];
        }
        ?>
        <? $APPLICATION->IncludeComponent(
            "notaext:plupload",
            "scan_passport2",
            array(
                "MAX_FILE_SIZE" => "24",
                "FILE_TYPES" => "jpg,jpeg,png",
                "DIR" => "tmp_register",
                "FILES_FIELD_NAME" => "profile_file",
                "MULTI_SELECTION" => "N",
                "CLEANUP_DIR" => "Y",
                "UPLOAD_AUTO_START" => "Y",
                "UNIQUE_NAMES" => "Y",
                "PERSONAL_PHOTO" => $sImage,
                "FULL_NAME" => $arResult['arUser']['NAME'] . ' ' . $arResult['arUser']['LAST_NAME'],
                "USER_EMAIL" => $arResult['arUser']['EMAIL'],
                "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
                "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
                "USER_ID" => $arParams["USER_ID"],
            ),
            false
        ); ?>
        <div class="nrf-fieldset-title"><?= GetMessage('SETTINGS'); ?></div>
        <div class="form-group" data-perpage-widget>
            <label><?= GetMessage('COUNT_SEARCH'); ?></label>
            <div class="perpage-setting-widget">
                <div class="radio">
                    <label>
                        <input type="radio" name="UF_SEARCH_PAGE_COUNT"
                               value="15"
                            <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT']
                                == 15
                            ) { ?> checked <? } ?>/>
                        <span class="lbl">15</span>
                    </label>
                    <label>
                        <input type="radio" name="UF_SEARCH_PAGE_COUNT"
                               value="30"
                            <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT']
                                == 30
                            ) { ?> checked <? } ?>/>
                        <span class="lbl">30</span>
                    </label>
                    <label>
                        <input type="radio" name="UF_SEARCH_PAGE_COUNT"
                               value="45"
                            <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT']
                                == 45
                            ) { ?> checked <? } ?>/>
                        <span class="lbl">45</span>
                    </label>
                </div>
            </div>

            <? /*
					 <div class="fieldcell iblock hidden">
					 <label for="settings09"><?=GetMessage('MAX_COUNT');?></label>
					 <div class="field validate">
					 <input type="text" value="" id="settings09" data-maxlength="300" data-minlength="2" name="pass" class="input docnum">
					 </div>
					 </div>
				 */ ?>
        </div>
        <div class="form-group">
            <?
            $verifyMessage = Loc::getMessage(
                'REGISTER_FIELD_VERIFY'
            );
            if ($arResult['SENT_REQUEST']
                && nebUser::USER_STATUS_VERIFIED
                !== (integer)$arResult['arUser']['UF_STATUS']
            ) {
                $verifyMessage = Loc::getMessage(
                    'REGISTER_REPEAT_VERIFY'
                );
                ?>
                Отправлен запрос на верификацию&nbsp;
            <? } ?>
            <label>
                <input
                    type="checkbox"
                    name="USER_VERIFY"
                    id="cb-verify"
                    value="Y"
                    <?php if ('Y'
                        === $arResult['arForumUser']['USER_VERIFY']
                        || nebUser::USER_STATUS_VERIFIED
                        === (integer)$arResult['arUser']['UF_STATUS']
                    ) {
                        echo 'checked';
                    } ?>
                >
                <span class="lbl"><?= $verifyMessage; ?></span>
            </label>
        </div>

        <div class="fieldrow nowrap fieldrowaction">
            <div class="fieldcell ">
                <div class="field clearfix">
                    <button name="save"
                            class="btn btn-primary"
                            value="1"
                            type="submit"><?= GetMessage(
                            'SAVE'
                        ); ?></button>
                    <?= GetMessage('GRB_REGISTRY') ?>
                </div>
            </div>
        </div>

        <script>
            $(function () {

                function getDateAge(dateObj) {
                    var today = new Date();
                    var born = dateObj,
                        age = Math.ceil((today.getTime() - born) / (24 * 3600 * 365.25 * 1000));
                    return age;
                }

                function dateStringToObj(dateString) {
                    var a = dateString.split('.'),
                        dateObj = new Date(a[2], a[1] - 1, a[0]);
                    return dateObj;
                }

                jQuery.validator.addMethod("checkAge", function (value, element, params) {
                        var inputDateObjValue = dateStringToObj(value);
                        return ( getDateAge(inputDateObjValue) > Number(params) );
                    }
                );
                jQuery.validator.addMethod("checkDateFormat", function (value, element, params) {
                        var xxx = value.match(/^\d\d?\.\d\d?\.\d\d\d\d$/);
                        return xxx != null;
                    }
                );
                jQuery.validator.addMethod("checkDateCorrect", function (value, element, params) {
                        var dateObj = dateStringToObj(value),
                            dateObjToStrLeadingZero = ('0' + String(dateObj.getDate())).slice(-2) + '.'
                                + ('0' + String(dateObj.getMonth() + 1)).slice(-2) + '.'
                                + String(dateObj.getFullYear()),
                            dateObjToStr = String(dateObj.getDate()) + '.'
                                + String(dateObj.getMonth() + 1) + '.'
                                + String(dateObj.getFullYear());
                        if (value == dateObjToStr || value == dateObjToStrLeadingZero) {
                            return true
                        }
                    }
                );

                $('form[name="form1"]').validate({
                    rules: rulesAndMessages.rules,
                    messages: rulesAndMessages.messages,
                    errorPlacement: function (error, element) {
                        var el = $(element);
                        if (element.attr('type') == "radio") {
                            error.appendTo(el.closest('.form-group').find('label')[0]);
                        } else {
                            error.appendTo(el.closest('.form-group'));
                        }
                        $(element).closest('.form-group').toggleClass('has-error', true);
                    },
                    /*specifying a submitHandler prevents the default submit, good for the demo
                     submitHandler: function() {
                         alert("valid!");
                     },
                     set this class to error-labels to indicate valid fields*/
                    success: function (label) {
                        /* set &nbsp; as text for IE
                        label.html('"&nbsp;"').addClass("resolved");*/
                        label.remove();
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                        $(element).parent().find("." + errorClass).removeClass("resolved");
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
                    }
                });

                $('#userbirthday').on('input change', function () {
                    $(this).closest('form').validate().element(this);
                });

                /*$('[data-mirror]').each(function(){
                     var id = $(this).data('mirror'),
                         thisId = $(this).prop('id');
                     $('#'+id).attr('data-mirrored-by', thisId);
                });*/

                $(document).on('keyup change', '[data-mirrored-by]', function () {
                    var id = $(this).data('mirrored-by'),
                        sameAddresses = $('#livwhereregistered').prop('checked');
                    if (sameAddresses) {
                        $('#' + id).val($(this).val()).trigger('change');
                    }
                });

                $(document).on('click', '#livwhereregistered', function () {
                    /*console.log('checkbox click');*/
                });
                $(document).on('change', '#livwhereregistered', function () {
                    /*console.log('checkbox change');*/
                    var checked = $(this).prop('checked');
                    if (checked) {
                        $('[data-mirror]').each(function () {
                            var id = $(this).data('mirror');
                            $(this).val($('#' + id).val());
                        });
                        $('.living-address-group').addClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function () {
                            $(this).attr('readonly', 'true');
                        });
                    } else {
                        $('.living-address-group').removeClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function () {
                            $(this).removeAttr('readonly');
                        });
                    }
                });
                if ($('#livwhereregistered').prop('checked')) {
                    $('#livwhereregistered').trigger('change');
                }
                $(document).on('click', '[data-perpage-set]', function (e) {
                    e.preventDefault();
                    var toggler = $(this),
                        value = toggler.data('perpage-set'),
                        widget = toggler.closest('[data-perpage-widget]'),
                        input = widget.find('[name="UF_SEARCH_PAGE_COUNT"]');

                    widget.find('[data-perpage-set]').toggleClass('current', false);
                    toggler.toggleClass('current', true);
                    $(input).val(value);
                    /*console.log(value);*/
                });
            });
        </script>

    </form>
</div><!-- profile-container -->

<!-- ВКЛЮЧИТЬ АПЛОАДИЛКУ -->
<?
$sImage = (intval($arResult['arUser']['PERSONAL_PHOTO']) > 0) ? CFile::GetPath($arResult['arUser']['PERSONAL_PHOTO']) : '';
if (isset($arResult['arUser']['PERSONAL_PHOTO']['SRC'])) {
    $sImage = $arResult['arUser']['PERSONAL_PHOTO']['SRC'];
}
?>
<? $APPLICATION->IncludeComponent(
    "notaext:plupload",
    "profile",
    array(
        "MAX_FILE_SIZE" => "24",
        "FILE_TYPES" => "jpg,jpeg,png",
        "DIR" => "tmp_register",
        "FILES_FIELD_NAME" => "profile_file",
        "MULTI_SELECTION" => "N",
        "CLEANUP_DIR" => "Y",
        "UPLOAD_AUTO_START" => "Y",
        "RESIZE_IMAGES" => "Y",
        "RESIZE_WIDTH" => "110",
        "RESIZE_HEIGHT" => "110",
        "RESIZE_CROP" => "Y",
        "RESIZE_QUALITY" => "98",
        "UNIQUE_NAMES" => "Y",
        "PERSONAL_PHOTO" => $sImage,
        "FULL_NAME" => $arResult['arUser']['NAME'] . ' ' . $arResult['arUser']['LAST_NAME'],
        "USER_EMAIL" => $arResult['arUser']['EMAIL'],
        "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
        "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
        "USER_ID" => $arParams["USER_ID"],
        "FOR_LIBRARY_USER" => "Y",
    ),
    false
); ?>