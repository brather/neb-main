<?
$MESS['QUOTES_SORT'] = 'Сортировать';
$MESS['QUOTES_SORT_AUTHOR'] = 'По автору';
$MESS['QUOTES_SORT_NAME'] = 'По названию';
$MESS['QUOTES_SORT_DATE'] = 'По дате';
$MESS['QUOTES_SHOW'] = 'Показать';
$MESS['QUOTES_SETTINGS'] = 'Настройки';
$MESS['QUOTES_REMOVE'] = 'Удалить';
$MESS['QUOTES_FROM_QUOTES'] = 'из Моих цитат';
$MESS['QUOTES_AUTHOR'] = 'Автор';
$MESS['QUOTES_BOOK'] = 'Книга';
$MESS['QUOTES_TEXT'] = 'Цитата на странице ';
$MESS['QUOTES_MY_COLLECTIONS'] = 'Мои подборки';
$MESS['QUOTES_CREATE_COLLECTION'] = 'cоздать подборку';
$MESS['PROFILE_MU_NUMBER_LC'] = 'Номер ЕЭЧБ';
?>