<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<?
if ((int)$arResult['VALUES']['USER_ID'] > 0
    && $arResult["USE_EMAIL_CONFIRMATION"] === "Y"
) {
    ?>
    <section class="innersection innerwrapper clearfix">
        <div class="b-formsubmit">
            <span class="b-successlink"><?= GetMessage(
                    "REGISTER_SUCCESSFUL"
                ) ?></span>

            <p><?= GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
        </div>
    </section>
    <?
    return;
}
?>

<?/*script type="text/javascript">
    function setlogin() {
        $('input#LOGIN').val($('input#settings04').val());
    }
</script*/?>

<h2 class="full-reg"><?= GetMessage("REGISTER_TITLE") ?></h2>
<?
if (count($arResult["ERRORS"]) > 0) {
    foreach ($arResult["ERRORS"] as $key => $error) {
        if (intval($key) == 0 && $key !== 0) {
            $arResult["ERRORS"][$key] = str_replace(
                "#FIELD_NAME#",
                "&quot;" . GetMessage("REGISTER_FIELD_" . $key)
                . "&quot;", $error
            );
        }
    }
    ShowError(implode("<br />", $arResult["ERRORS"]) . '<br /><br />');
    ?>
<?
}
?>
<?
if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"
    && (int)$arResult['VALUES']['USER_ID'] > 0
) {
    ?>
    <p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
<?
}
?>
<form method="post" action="<?= POST_FORM_ACTION_URI ?>"
      class="new-reader-form nrf" name="regform"
      enctype="multipart/form-data"
      id="regform">
    <?php require(dirname(__FILE__)
        . '/../../template-parts/full-form.php'); ?>
</form>

