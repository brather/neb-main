<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
/**
 * @var CMain $APPLICATION
 */
$APPLICATION->SetTitle('');
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('REGISTRATION_PAGE_TITLE'));
\Bitrix\Main\Page\Asset::getInstance()->addCss(
    '/local/templates/.default/markup/css/registration.css'
);
\Bitrix\Main\Page\Asset::getInstance()->addJs(
    '/local/templates/.default/markup/js/registration.js'
);
?>
<? $APPLICATION->IncludeComponent(
    'neb:registration',
    '',
    Array(
        'SEF_MODE'          => 'Y',
        'SEF_FOLDER'        => '/registration/',
        'SEF_URL_TEMPLATES' => Array(
            'main'          => '/',
            'full'          => 'full/',
            'in_library'    => 'in_library/',
            'rgb'           => 'rgb/',
            'ecia'          => 'ecia/',
            'simple'        => 'simple/',
            'rh-individual' => 'rh-individual/',
            'rh-entity'     => 'rh-entity/',
        ),
        'VARIABLE_ALIASES'  => Array(
            'main'          => Array(),
            'full'          => Array(),
            'rgb'           => Array(),
            'ecia'          => Array(),
            'simple'        => Array(),
            'rh-individual' => Array(),
            'rh-entity'     => Array(),
        )
    )
); ?>
<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>