<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title></title>
	
	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />

	<script src="js/libs/modernizr.min.js"></script>

	<link rel="stylesheet" href="css/style.css">
	
	<script src="js/libs/jquery.min.js"></script>

	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>	
	<script>window.jQuery.ui || document.write('<script src="js/libs/jquery.ui.min.js">\x3C/script>')</script>
	<script src="js/libs/jquery.ui.touch-punch.min.js"></script>

	<script src="js/plugins.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/blind.js"></script>
	<script src="js/script.js"></script>
	<script>
		$(function() {
            $('html').addClass('mainhtml'); /* это для морды */
        }); /* DOM loaded */
	</script>
</head>
<body class="blind ">
	<div class="oldie hidden">Вы пользуетесь устаревшей версией браузера. <br/>Не все функции будут работать в нем корректно. <br/>Смените на более новую версию</div>
	<div class="homepage">
		
		<header id="header" class="mainheader">		

			<div class="wrapper clearfix">

				
				<div class="b-headernav_login right">
					<a href="#" class="b-headernav_login_lnk js_register" data-width="255" data-height="170">Регистрация</a>
					<div class="b_register_popup b_login_popup" >
						<div class="b_login_popup_in bbox">
							<h4>Зарегистрироваться как</h4>
							<a href="#" class="formbutton">Читатель</a>
							<div class="b-rightholder">или <a href="#">правообладатель</a></div>
						</div>
					</div> 	<!-- /.b_login_popup -->	

					<a href="#" class="b-headernav_login_lnk current popup_opener" data-height="320"  data-width="550" >Войти</a> <!-- data-width="550"  --> 
					<div class="b_login_popup popup" >
						<div class="b_login_popup_in bbox">
							<div class="b-login_txt hidden"><a href="#">вход для библиотек</a> <a href="#">для правообладателей</a></div>
							
							<div class="b-loginform iblock rel">
								<h4>вход в личный кабинет</h4>
								<p class="change_pass_mess">Ваш пароль успешно изменен. <br/>Для продолжения работы с порталом введите Ваши адрес электронной почты и пароль, воспользуйтесь кнопкой «Войти».</p>
								<form action="" class="b-form b-formlogin">
									<div class="fieldrow nowrap">
										<div class="fieldcell iblock">
											<label class="lite" for="settings11">E-mail / Логин / ЕЭЧБ</label>
											<div class="field validate">
												<input type="text" data-required="required" value=""  id="settings11" data-maxlength="30" data-minlength="2" name="num" class="input">										
											</div>
										</div>

									</div>
									<div class="fieldrow nowrap">

										<div class="fieldcell iblock">
											<label for="settings10">Пароль</label>
											<div class="field validate">
												<input type="password" data-required="required" value="" id="settings10" data-maxlength="30" data-minlength="2" name="pass" class="input">										
											</div>
										</div>
									</div>
								<!--<em class="error_message">Такой e-mail не зарегистрирован</em>-->
									<div class="fieldrow nowrap fieldrowaction">
										<div class="fieldcell ">
											<div class="field clearfix">
												<button class="formbutton left" value="1" type="submit">Войти</button>
												<a href="#" class="formlink right js_passrecovery">забыли пароль?</a>
											</div>
										</div>
									</div>
								</form>
								<div class="b-passrform">								
									<a href="#" class="closepopup">Закрыть окно</a>
									<form action=""  class="b-passrecoveryform b-form hidden">
										<div class="fieldrow nowrap">
											<div class="fieldcell iblock">
												<label for="settings11">Введите электронную почту, указанную при регистрации</label>
												<div class="field validate">
													<input type="text" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="30" data-minlength="2" name="num" class="input">	
													<em class="error required">Поле обязательно для заполнения</em>									
												</div>
											</div>

											<div class="fieldrow nowrap fieldrowaction">
												<div class="fieldcell ">
													<div class="field clearfix">
														<button class="formbutton" value="1" type="submit">Восстановить</button>

													</div>
												</div>
											</div>
										</div>
									</form>
									<form action=""  class="b-passrecoveryform b-form">
										<div class="fieldrow nowrap">
											<div class="fieldcell iblock">
												<div class="b-warning">Адрес эл. почты не найден</div>
												<label for="settings11">Попробуйте ввести другой адрес</label>
												<div class="field validate">
													<input type="text" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="30" data-minlength="2" name="num" class="input">	
													<em class="error required">Поле обязательно для заполнения</em>									
												</div>
											</div>

											<div class="fieldrow nowrap fieldrowaction">
												<div class="fieldcell ">
													<div class="field clearfix">
														<button class="formbutton" value="1" type="submit">Восстановить</button>

													</div>
												</div>
											</div>
										</div>
									</form>
									<p class="b-passconfirm hidden">На почту выслано письмо с ссылкой на сброс пароля</p>
								</div>
							</div><!-- /.b-loginform -->
							<div class="b-login_social iblock">
								<h4>через социальные сети</h4>
								<a href="#" class="b-login_slnk vklink">вконтакте</a>
								<a href="#" class="b-login_slnk odnlink">одноклассники</a>
								<a href="#" class="b-login_slnk fblink">facebook</a>
								<a href="#" class="b-login_slnk esialink">Вход по ЕСИА</a>
							</div><!-- /.b-login_social -->

						</div>

					</div> 	<!-- /.b_login_popup -->
					<? if($_GET['PASSWORD_CHANGED']=='Y') {
?>
   <script>
						$(function() {
								$('.b_login_popup').addClass('change_log_pass');
								$('.b-headernav_login_lnk').data('height', '430');
								$('.b-headernav_login_lnk').trigger('click');
								
							}); 
					</script>

<?
 }
?>
				</div>
				<div class="b-header_lang right ">
					<a href="#" class="en iblock">Eng</a>
					<a href="#" class="ru iblock"></a>
				</div>
				<!--<div class="b-headernav-apps right">
					<a href="#" class="b-headerapps_img"><span>3</span></a>						
					<a href="#" class="b-headerapps_lnk">Приложения для моб. устройств</a>
				</div>-->
				<nav class="b-headernav left">    
					<a href="#collections" class="b-headernav_lnk">Коллекции</a>
					<a href="#" class="b-headernav_lnk">Библиотеки</a>
					<a href="#" class="b-headernav_lnk">Форум</a>
				</nav>
				<span  class="testmode">Портал работает в тестовом режиме. В случае обнаружения технических проблем или некорректных данных - <a href="<?=URL_SCHEME?>://<?=SITE_HOST?>/feedback/">сообщите нам</a>.</span>
			</div>	<!-- /.wrapper -->			
			
		</header>