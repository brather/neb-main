<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 * @var array $arParams
 */

?>
<a href="<?=$arParams['ADD_URL'];?>" type="button" class="btn btn-primary">
    <?=Loc::getMessage('WORKPLACES_TYPES_ADD');?>
</a>
<div class="lk-table">
    <div class="lk-table__column" style="width:10%"></div>
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:40%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind"><?= Loc::getMessage('WORKPLACES_TYPES_ID'); ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('WORKPLACES_TYPES_XML_ID'); ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('WORKPLACES_TYPES_VALUE'); ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('WORKPLACES_TYPES_SORT'); ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('WORKPLACES_TYPES_COUNT'); ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('WORKPLACES_TYPES_EDIT'); ?></li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['ITEMS'] as $arItem) : ?>
            <ul class="lk-table__row">
                <li class="lk-table__col">
                    <?=$arItem['ID']?>
                </li>
                <li class="lk-table__col">
                    <?=$arItem['XML_ID']?>
                </li>
                <li class="lk-table__col">
                    <? if (intval($arItem['COUNT']) > 0):?>
                        <a href="/bitrix/admin/iblock_list_admin.php?PAGEN_1=1&SIZEN_1=20&type=library&IBLOCK_ID=<?=$arItem['IBLOCK_ID'];
                        ?>&lang=<?=LANGUAGE_ID?>&set_filter=Y&adm_filter_applied=0&find_el_property_<?=$arItem['PROPERTY_ID'];
                        ?>=<?=$arItem['ID']?>" target="_blank">
                            <?=$arItem['VALUE']?>
                        </a>
                    <? else: ?>
                        <?=$arItem['VALUE']?>
                    <? endif; ?>
                </li>
                <li class="lk-table__col">
                    <?=$arItem['SORT']?>
                </li>
                <li class="lk-table__col">
                    <? if (intval($arItem['COUNT']) > 0):?>
                        <a href="/bitrix/admin/iblock_list_admin.php?PAGEN_1=1&SIZEN_1=20&type=library&IBLOCK_ID=<?=$arItem['IBLOCK_ID'];
                        ?>&lang=<?=LANGUAGE_ID?>&set_filter=Y&adm_filter_applied=0&find_el_property_<?=$arItem['PROPERTY_ID'];
                        ?>=<?=$arItem['ID']?>" target="_blank">
                            <?=intval($arItem['COUNT'])?>
                        </a>
                    <? else: ?>
                        <?=intval($arItem['COUNT'])?>
                    <? endif; ?>
                </li>
                <li class="lk-table__col"><a href="<?=str_replace('#ID#', $arItem['ID'], $arParams['EDIT_URL']);
                    ?>" type="button" class="btn btn-primary">
                        <?=Loc::getMessage('WORKPLACES_TYPES_EDIT');?>
                </a></li>
            </ul>
        <?endforeach;?>
    </section>
</div>