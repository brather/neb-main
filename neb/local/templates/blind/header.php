<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<!DOCTYPE html>
<html lang="ru-RU"> <!--  lang="<?= LANGUAGE_ID ?>" -->
<head>
    <meta charset="<?= SITE_CHARSET ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><? $APPLICATION->ShowTitle() ?> — специальная версия НЭБ.РФ</title>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <script data-skip-moving="true" src="<?=SITE_TEMPLATE_PATH?>/js/headtop.js"></script>
    <?$APPLICATION->ShowHead(); ?>
    <script data-skip-moving="true" src="<?=SITE_TEMPLATE_PATH?>/js/head.js"></script>
    <link data-skip-moving="true" href="<?=SITE_TEMPLATE_PATH?>/css/template.css" rel="stylesheet"/>
</head>
<body class="blind-version">
    <? $APPLICATION->ShowPanel() ?>