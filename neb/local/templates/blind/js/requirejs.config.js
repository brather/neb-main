requirejs.config({
  baseUrl: './assets/js',
  deps: ['app/main'],
  paths: {
    'bootstrap': 'vendor/bootstrap'
  }
});