<?php

define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

$documentRoot = realpath(dirname(__FILE__) . '/../neb/');
$_SERVER['DOCUMENT_ROOT'] = $documentRoot;
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');
while (ob_get_level()) {
    ob_end_clean();
}
/**
 * @var CDatabase $DB
 */

$countStats = function ($libraryId = 0) {
    if ($libraryId > 0) {
        $libId = nebLibrary::getLibraryIdByIblockId($libraryId);
        $statAggregator = new \Neb\Main\Stat\StatisticAggregator(
            ['libraryExaleadIdsArray' => [$libId]]
        );
    } else {
        $statAggregator = new \Neb\Main\Stat\StatisticAggregator();
    }
    $statAggregator->prepareLibraryStatDates();

    $statAggregator->loadExaleadData();
    $exaleadResult = $statAggregator->getExaleadResult();
    $booksCount = $exaleadResult['COUNT'];
    $booksDigitCount = $statAggregator->countPublicationsDigitized();
    $booksDigitCount = $booksDigitCount[date('d.m.Y')];

    $filter = [];
    if ($libraryId > 0) {
        $filter = ['UF_LIBRARY' => $libraryId];
    }
    $rsUsers = CUser::GetList($order, $by, $filter);
    $rsUsers->NavStart();


    $dataParams = [
        'date' => date('Y-m-d'),
    ];
    if ($libraryId > 0) {
        $dataParams['library_id'] = $libraryId;
    }
    $dataParams['value_num'] = $booksCount;
    \Neb\Main\Stat\Tracker::periodDataTrack(
        'library_books',
        $dataParams
    );
    $dataParams['value_num'] = $booksDigitCount;
    \Neb\Main\Stat\Tracker::periodDataTrack(
        'library_books_digitized',
        $dataParams
    );
    $dataParams['value_num'] = $rsUsers->NavRecordCount;
    \Neb\Main\Stat\Tracker::periodDataTrack(
        'library_user_registers',
        $dataParams
    );
    if ($libraryId > 0) {
        echo 'Library ', $libraryId, ' saved', PHP_EOL;
    } else {
        echo 'Total saved ', PHP_EOL;
    }
};

$timeStart = microtime(true);
CModule::IncludeModule('iblock');
$rsLibraries = CIBlockElement::GetList(
    [],
    [
        'IBLOCK_ID' => IBLOCK_ID_LIBRARY,
        'ACTIVE'    => 'Y',
    ]
);
while ($library = $rsLibraries->Fetch()) {
    $countStats($library['ID']);
}
$countStats();


echo PHP_EOL;
echo 'Time: ', microtime(true) - $timeStart, ' seconds', PHP_EOL;
echo 'Memory: ', memory_get_peak_usage() / (1024 * 1024), ' Mb', PHP_EOL;
echo PHP_EOL;