<?
$MESS["intervolga.remoteuserclient.SOAP_ERROR"] = "Ошибка в SOAP-запросе #CODE#: #STRING#";
$MESS["intervolga.remoteuserclient.WEBSERVICES_NOT_INSTALLED"] = "Модуль веб-сервисов не установлен";
$MESS["intervolga.remoteuserclient.OPTION_SERVER_NOT_SET"] = "Не задан сервер хранения пользователей";
$MESS["intervolga.remoteuserclient.OPTION_SERVER_NOT_SET"] = "Не задана страница для обращения на сервере хранения пользователей";
$MESS["intervolga.remoteuserclient.OPTION_SERVER_NOT_SET"] = "Не задан логин сервера хранения пользователей";
$MESS["intervolga.remoteuserclient.OPTION_SERVER_NOT_SET"] = "Не задан пароль сервера хранения пользователей";
$MESS["intervolga.remoteuserclient.FILE_PATH_NOT_FOUND"] = "Путь к файлу [#ID#] не найден";
$MESS["intervolga.remoteuserclient.FILE_NOT_FOUND"] = "Файл [#FILE#] не существует";
$MESS["intervolga.remoteuserclient.FILE_NOT_OPENED"] = "Файл [#FILE#] не может быть прочитан";
$MESS["intervolga.remoteuserclient.CANT_GET_RESPONSE"] = "Не удалось получить ответ на SOAP-запрос";