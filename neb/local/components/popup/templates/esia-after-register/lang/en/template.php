<?php
$MESS['ESIA_ACCESS_QUESTION'] = 'You have limited access to read books,<br>
                you can get full access your user profile. <br>
                Get full access?';
$MESS['ESIA_ACCESS_QUESTION_YES'] = 'Yes';
$MESS['ESIA_ACCESS_QUESTION_NO'] = 'No';