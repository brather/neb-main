<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section class="innerwrapper clearfix">
    <div class="b-registration b-registration-popup rel">
        <form id="regform"  enctype="application/x-www-form-urlencoded" name="regform" class="b-form b-form_common b-regform" action="/popup/" method="post">

            <p class="note" style="margin: 0 auto;">
                Вы являетесь читателем РГБ и можете получить полный доступ к изданиям. Получить доступ?
            </p>

            <p class="note" style="margin: 0 auto;display: none;color:red;" id="error_setveryfied" >

            </p>

            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell " style="margin: 0 auto;">
                    <div class="field clearfix divdisable">
                        <button class="formbutton" value="1" type="button" onclick="sendrequest(1); return false;">Да</button>
                        <button class="formbutton" value="1" type="button" onclick="document.location.href = '/profile';">Нет</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.b-registration-->
</section>
<script type="text/javascript">

    function sendrequest(){
        $('#error_setveryfied').hide();

        $.get( "/rest_api/users/?action=setveryfied", function( data ) {
            if(data.error){
                $('#error_setveryfied').text(data.message).show();
            }else{
               document.location.href = '/profile';
            }
        });
        return false;
    }

</script>