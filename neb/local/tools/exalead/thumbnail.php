<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
    // if the browser has a cached version of this image, send 304
    header('Last-Modified: ' . $_SERVER['HTTP_IF_MODIFIED_SINCE'], true, 304);
    exit;
}
$url = '';
if (!empty($_REQUEST['url']))
    $url = htmlspecialchars(urldecode($_REQUEST['url']));

$source = '';
if (!empty($_REQUEST['source']))
    $source = htmlspecialchars(urldecode($_REQUEST['source']));

if (empty($source))
    $source = 'JapanConnector';

@$width = intval($_REQUEST['width']);
@$height = intval($_REQUEST['height']);
if (empty($url))
    die('Empty url');

if ($width == 0)
    $width = 80;

if ($height == 0)
    $height = 125;

/**
 * Проверем существование файла, минуя подключение ядра и вызова функций
 */
$thumbnailFolder = "/upload/tmp_thumbnail/" . substr($url, -3) . '/';

$fileName = $thumbnailFolder . $url . "h" . $height . "w" . $width . ".png";

if (file_exists($_SERVER["DOCUMENT_ROOT"] . $fileName)) {
    header('Content-Length: ' . filesize($_SERVER['DOCUMENT_ROOT'] . $fileName));
    header('Content-Type: '   . mime_content_type($_SERVER['DOCUMENT_ROOT'] . $fileName));
    echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . $fileName);
    exit();
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
session_write_close();

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient,
    \Nota\Exalead\RslViewer;

Loader::includeModule('nota.exalead');

$resultThumbnail = false;

/**
 * Получаем картинку из сервиса доступа
 */
$width_  = ($width  < 100 ? 100 : $width);
$height_ = ($height < 100 ? 100 : $height);

// цикл на случай передачи id произведения (разметки)
for ($i = 0; $i <= 1; $i++) {

    // запрос к Exalead для определения epub (литрес)
    $query = new SearchQuery();
    $query->getById($url);

    $client = new SearchClient();
    $arBook = $client->getResult($query);

    if (!empty($arBook['id']) && in_array('epub', $arBook['fileExt'])) {
        break;
    }

    // запрос к сервису выдачи
    $rsl = new RslViewer($url);

    // размер первой страницы
    list($width_img, $height_img) = explode("x", $rsl->getGeometry(1));
    if (intval($width_img) > intval($height_img)) {
        $height_ = intval(($height_img * $width_) / $width_img);
    }

    if ($height_ > 0) {
        $rawJpg = $rsl->getPageThumbl($height_, 1);
    }

    if (empty($rawJpg) && $i == 0) {
        $url = str_replace(strrchr($url, '_'), '', $url);
    } else {
        break;
    }
}

if (!empty($rawJpg)) {
    RewriteFile($_SERVER["DOCUMENT_ROOT"] . $fileName, $rawJpg);
    $resultThumbnail = $fileName;
}

if ($resultThumbnail !== false) {
    /*
    Устанавливаем кешь только если картинка получена
    */
    #header("Cache-Control: private, max-age=10800, pre-check=10800");
    #header("Pragma: private");
    // Set to expire in 2 days
    #header("Expires: " . date(DATE_RFC822,strtotime(" 2 day")));

} else {

    $thumbnailFolder = '/upload/tmp_thumbnail_empty/';
    $fileName = $thumbnailFolder . "w" . $width . "h" . $height . ".png";

    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $fileName)) {
        $resultThumbnail = '/local/images/book_empty.png';

        $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $resultThumbnail);

        CFile::ResizeImage($arFile, array('width' => $width, 'height' => $height));

        CopyDirFiles($arFile['tmp_name'], $_SERVER["DOCUMENT_ROOT"] . $fileName);
    }

    $resultThumbnail = $fileName;
}

if ($resultThumbnail !== false) {
    MainHelper::clearBuffer();
    header('Content-Length: ' . filesize($_SERVER['DOCUMENT_ROOT'] . $resultThumbnail));
    header('Content-Type: '   . mime_content_type($_SERVER['DOCUMENT_ROOT'] . $resultThumbnail));
    echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . $resultThumbnail);
}