<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('nota.userdata');
use Nota\UserData\rgb;

$users = new CUser();
$rgb = new rgb();
$arFilter = array('=UF_REGISTER_TYPE' => 38, '>ID' => 8774);
$by ='id';
$order = 'asc';
$select = array("SELECT" => array('ID',"UF_SCAN_PASSPORT1",));

$rsData = $users->getList($by, $order, $arFilter, $select);
while($arData = $rsData->Fetch()):
    $rgb->pushUserIntoRGB($arData['ID']);
endwhile;