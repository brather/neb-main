<?
$MESS ['EDBG_DIALOG_TITLE'] = "Gallery for a detailed view";
$MESS ['EDBG_BUTTON_NAME'] = "Gallery";
$MESS ['EDBG_CONTECST_EDIT_GALLERY'] = "Edit gallery";

/*search tab*/
$MESS ['EDBG_CHECKED_PHOTO'] = "Choose a photo";

/* Edit tab	*/
$MESS ['EDBG_PHOTO_HIDE'] = "Hide";
$MESS ['EDBG_PHOTO_HIDE_TITLE'] = "Hide this photo";
$MESS ['EDBG_PHOTO_EDIT'] = "Edit";
$MESS ['EDBG_PHOTO_EDIT_TITLE'] = "Edit this photo";

/* Edit dialog	*/
$MESS ['EDBG_PHOTO_EDIT_TITLE_DIALOG'] = "Edit";
$MESS ['EDBG_PHOTO_EDIT_APPLY_CLOSE'] = "Apply and close";
$MESS ['EDBG_PHOTO_EDIT_APPLY_NEXT'] = "Apply and go to next photo";
$MESS ['EDBG_PHOTO_EDIT_CANCEL'] = "Cancel changes and close";
