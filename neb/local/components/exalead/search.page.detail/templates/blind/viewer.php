<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$sPath = $_SERVER["DOCUMENT_ROOT"]. $component->getPath() . '/templates/neb/viewer.php';

if (file_exists($sPath)) {
    require_once $sPath;
} else {
    die('Viewer not found!');
}