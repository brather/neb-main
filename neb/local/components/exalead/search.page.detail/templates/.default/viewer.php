<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$arResult['BOOK']['URL'] = urldecode($arResult['VARIABLES']['BOOK_ID']);
global $USER;
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?=$arResult['BOOK']['title']?></title>
		<meta charset="utf-8">

        <link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />
		<link href="<?=MARKUP?>css/style.css" type="text/css"  data-template-style="true"  rel="stylesheet" />
		<link rel="stylesheet" href="/local/components/exalead/search.page.detail/templates/.default/styles.css" type="text/css" media="screen">
		<link rel="stylesheet" href="/local/components/exalead/search.page.detail/templates/.default/js/jquery.arcticmodal-0.3.css">
		<link rel="stylesheet" href="/local/components/exalead/search.page.detail/templates/.default/js/themes/simple.css">

		<!-- Bootstrap -->
		<link href="/local/templates/adaptive/css/bootstrap.min.css" rel="stylesheet">
		<!-- Fonts -->
		<link href='//fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic,800,800italic,300,300italic&amp;subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/local/templates/adaptive/css/font-awesome.min.css">
		<!-- jquery ui -->
		<link rel="stylesheet" href="/local/templates/adaptive/vendor/jquery-ui/jquery-ui.css">
		<!-- Important Owl stylesheet -->
		<!-- <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css"> -->

		<link rel="stylesheet" type="text/css" href="/local/templates/adaptive/vendor/custom-select/bootstrap-select.min.css"/>

		<link href="/local/templates/adaptive/css/style.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<script src="/local/templates/.default/markup/js/libs/jquery.min.js" type="text/javascript"></script>
		<script src="/local/components/exalead/search.page.detail/templates/.default/js/jquery.arcticmodal-0.3.min.js"></script>
		<script src="/bitrix/js/jqRotate.js" type="text/javascript"></script>
		<script src="/bitrix/js/jquery.Jcrop.js" type="text/javascript"></script>
		<script src="/local/components/exalead/search.page.detail/templates/.default/js/jquery.mousewheel.min.js" type="text/javascript"></script>
		<script src="/local/components/exalead/search.page.detail/templates/.default/script.js?version=2" type="text/javascript"></script>

		<script src="/bitrix/js/main/core/core.min.js?143349360765494" type="text/javascript"></script>
	</head>
	<body>
<!--	<div class="viewer-header">
		<img src="" alt="" class="viewer-header__logo">
		<div class="viewer-header__title">
			<p>Пушкин Александр Сергеевич</p>
			<h2>Сказка о золотом петушке</h2>
		</div>
		<div class="viewer-header__controls">
			<ul>
				<li><a href="#">Описание книги</a></li>
				<li><a href="#">Добавить в избранное</a></li>
				<li><a href="#">Сохранить книгу</a></li>
			</ul>
		</div>
	</div>-->

	<div class="page-book">
	<div class=""></div>
	<span class="turn-page">Свернуть экран</span>
		<!-- left -->
		<div class="left">
			<a href="/" class="logo"></a>

			<div class="left-tools">
				<ul>
					<li><span class="bookmark-tools" title="Добавить страницу в закладки">В закладки</span></li>
					<li><span class="toggle-view image-now" title="Переключить вид">Показывать как текст</span></li>
					<li><span class="scale-page" title="Смотреть страницу во весь экран">Во весь экран</span></li>
					<li class="sub">
						<span class="rotate" title="Поворот страницы">Повернуть</span>
						<ul>
							<li><span class="rotate-left" title="Повернуть налево"></span></li>
							<li><span class="rotate-right" title="Повернуть направо"></span></li>
						</ul>
					</li>
					<li><span class="save" title="Сохранить страницу">Сохранить страницу</span></li>
					<li><span class="question" title="Руководство пользователя"><a href='/static/docs/neb_user_guide.pdf'>Руководство пользователя</a></span></li>
				</ul>
			</div>
		</div>
		<!-- end left -->
		
		<!-- right -->
		<div class="right">
			<div class="top-right-tools clearfix">
				<ul class="clearfix">
					<!--<li><span class="toggle-right-tools"></span></li>-->
					<li><span class="properties-book" title="Описание книги">Описание книги</span></li>
					<!--<li><span class="search-book">Поиск по книге</span></li>-->
					<li><span class="favorite-book<?if(!empty($arResult['USER_BOOKS'])):?> active<?endif;?>" title="<?if(!empty($arResult['USER_BOOKS'])):?>В личном кабинете<?else:?>Добавить в личный кабинет<?endif;?>" data-collection="/local/tools/collections/list.php?t=books&id=<?=$arResult['BOOK']['URL']?>" data-url="/local/tools/collections/removeBook.php?id=<?=$arResult['BOOK']['URL']?>"><?if(!empty($arResult['USER_BOOKS'])):?>Удалить из избранного<?else:?>Добавить в избранное<?endif;?></span></li>
					<li>
					<?php if ($USER->IsAuthorized()): ?>
						<a class="save-book-link allowed" title="Сохранить книгу" href='/local/tools/exalead/getFiles.php?book_id=<?=$arResult['BOOK']['URL']?>&name=<?=$arResult['BOOK']['title']?>'>
							<span class="save-book" title="Сохранить книгу">Сохранить книгу</span>
						</a>
					<?php else:  ?>
						<a class="save-book-link" href='#'>
							<span class="save-book" title="Сохранить книгу">Сохранить книгу</span>
						</a>
					<?php endif  ?>
						<div class="loading-book"><div>Загрузка началась</div></div>
					</li>
				</ul>
			</div>
			<div class="right-tools">
				<!--<ul>
					<li><span class="miniatures"></span></li>
					<li><span class="bookmark"></span></li>
					<li><span class="quote"></span></li>
					<li><span class="note"></span></li>
				</ul>-->


				<div class="properties-book-content">
					<h2>Описание книги</h2>
					<?if($arResult['BOOK']['authorbook']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR')?>:</h3> <span><?=$arResult['BOOK']['authorbook']?></span></div><?endif;?>
					<?if($arResult['BOOK']['title']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE')?>:</h3> <span><?=$arResult['BOOK']['title']?></span></div><?endif;?>
					<?if($arResult['BOOK']['subtitle']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SUBTITLE')?>:</h3> <span><?=$arResult['BOOK']['subtitle']?></span></div><?endif;?>
					<?if($arResult['BOOK']['responsibility']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_RESPONSIBILITY')?>:</h3> <span><?=$arResult['BOOK']['responsibility']?></span></div><?endif;?>
					<?if($arResult['BOOK']['publishplace']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>:</h3> <span><?=$arResult['BOOK']['publishplace']?></span></div><?endif;?>
					<?if($arResult['BOOK']['publisher']):?><div><h3><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH')?>:</h3> <span><?=$arResult['BOOK']['publisher']?></span></div><?endif;?>
					<?if($arResult['BOOK']['year']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR')?>:</h3> <span><?=$arResult['BOOK']['year']?></span></div><?endif;?>
					<?if($arResult['BOOK']['countpages']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES')?>:</h3> <span><?=$arResult['BOOK']['countpages']?></span></div><?endif;?>
					<?if ($arResult['BOOK']['series']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE')?>:</h3> <span><?=$arResult['BOOK']['series']?></span></div><?endif;?>
					<?if($arResult['BOOK']['isbn']):?><div><h3>ISBN:</h3> <span><?=$arResult['BOOK']['isbn']?></span></div><?endif;?>
					<?if($arResult['BOOK']['issn']):?><div><h3>ISSN:</h3> <span><?=$arResult['BOOK']['issn']?></span></div><?endif;?>
					<?if($arResult['BOOK']['contentnotes']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT')?>:</h3> <span><?=$arResult['BOOK']['contentnotes']?></span></div><?endif;?>
					<?if($arResult['BOOK']['notes']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE')?>:</h3> <span><?=$arResult['BOOK']['notes']?></span></div><?endif;?>
					<?if($arResult['BOOK']['annotation']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ANNOTATION')?>:</h3> <span><?=$arResult['BOOK']['annotation']?></div><?endif;?>
					<?if($arResult['BOOK']['edition']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_EDITION')?>:</h3> <span><?=$arResult['BOOK']['edition']?></span></div><?endif;?>
					<?if($arResult['BOOK']['bbk']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BBK')?>:</h3> <span><?=$arResult['BOOK']['bbk']?></span></div><?endif;?>
					<?if($arResult['BOOK']['udk']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UDC')?>:</h3> <span><?=$arResult['BOOK']['udk']?></span></div><?endif;?>
					<?if($arResult['BOOK']['library']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB')?>:</h3> <span><?=$arResult['BOOK']['library']?></span></div><?endif;?>
				</div>

				<div class="right-tools-kind">
					<span class="right-tools-label note" title="Заметки">Заметки</span>
					<div class="note-content">
						<?foreach($arResult['BOOK']['NOTE'] as $note):?>
						<div>
							<a p="<?=$note["NUM_PAGE"]?>"><span><?=$note["NUM_PAGE"]?> страница</span>
							<span><?=$note["TEXT"]?></span>
							</a>
							<span class='delete' id='<?=$note["ID"]?>'></span>
						</div>
						<?endforeach;?>
					</div>
				</div>

				<div class="right-tools-kind">
					<span class="right-tools-label bookmark" title="Закладки">Закладки</span>
					<div class="bookmark-content">
						<?foreach ($arResult['BOOK']['MARK'] as $mark):?>
						<div>
							<a p="<?=$mark['page']?>"><span><?=$mark['page']?></span> страница</a>
							<span class='delete' id='<?=$mark['id']?>'></span>
						</div>
						<?endforeach;?>
					</div>
				</div>

				<div class="right-tools-kind">
					<span class="right-tools-label quote" title="Цитаты">Цитаты</span>
					<div class="quote-content">
						<?foreach($arResult['BOOK']['QUOTES'] as $quotes):?>
						<div>
							<a p="<?=$quotes["UF_PAGE"]?>"><span><?=$quotes["UF_PAGE"]?> страница</span>
							<img src='<?=$quotes["UF_IMG_DATA"]?>'>
							</a>
							<span class='delete' id='<?=$quotes["ID"]?>'></span>
						</div>
						<?endforeach;?>
					</div>
				</div>

				<div class="right-tools-kind">
					<span class="right-tools-label right-tools-label--last search" title="Поиск по тексту">Поиск по тексту</span>
					<div class='search-book-content'>
								<div class='search_'>
									<div class="clearfix">
										<input name='search_text' id = 'search_text' type="text">
										<button class="btn btn-primary search-submit-btn" id='search_submit' type='submit' value='Искать' title="Искать">
											<span class="fa fa-search"></span>
										</button>
									</div>
									<div class="search_preloader">
									</div>
									<div class='search_result empty'>
									</div>
								</div>
					</div>
				</div>


			</div>
		</div>
		<!-- end right -->
		
		<!-- main -->
		<div class="main">
			<div class="view-content">
			<div class="book-title">
				<div class="book-title-name"><?=$arResult['BOOK']['title']?></div>
				<div class="book-title-author">
					<?if($arResult['BOOK']['authorbook']):?><?=$arResult['BOOK']['authorbook']?><?endif;?>
				</div>
			</div>
			<div class="view-page">
				<div class="prev-page" title="Предыдущая страница"></div>
				<span class="add-bookmark" title="Добавить страницу в закладки"></span>
				<div class="viewer">
					<div class='viewer_img'>
						<img id='image' class='zoomer-image'>
					</div>
				</div>
				<div class="preloader"></div>
				<div class="quote-book">
					<div class="quote-icon"></div>
					<div class="quote-book-content"></div>
				</div>
				<div class="note-book">
					<div class="note-icon"></div>
					<div class="note-book-content"></div>
				</div>
				<div id='selectedrect' style='display:none'>
					<div id="selectedrect-wrapper" class="selectedrect-wrapper">
					</div>
					<div id='selected_rect_menu'>
						<a href='#' class="selected-increase">Увеличить фрагмент</a>
						<a href='#' class="selected-reduce selected-reduce--disabled">Уменьшить фрагмент</a>
						<a href='#' id='b-textLayer_quotes'>Добавить в цитаты</a>
						<a href='#' id='b-textLayer_notes'>Добавить в заметки</a>
					</div>
					<div class='layer_notes' style='display:none;'>
						<textarea id='text_note'></textarea>
						<input type='button' value='Добавить' id='add_note'>
					</div>
					<div class="crop-text-block">
						<p>Выделенный текст</p>
						<input type="text" class="crop-text">
						<div class="search_preloader"></div>
					</div>
				</div>
				<div class="next-page" title="Следующая страница"></div>
				<div class="text-view-wrapper">
					<div class="text-view"></div>
				</div>		
			</div>
			<input type='hidden' id="page-number" value='<?=(intval($_REQUEST['page'])>0?intval($_REQUEST['page']):1)?>'>
			<input type='hidden' id="book_id" value='<?=$arResult['BOOK']['URL']?>'>
			<input type='hidden' id="count_page" value='<?=$arResult['BOOK']['pageCount']?>'>
			</div>
				<div class="navigation">
					<ul class="clearfix">
						<li><span class="first fa fa-angle-double-left" title="В начало" p=1></span></li>
						<li><span class="prev fa fa-angle-left" title="Назад"></span></li>
						<li><span class='page_1'></span></li>
						<li><span class='page_2'></span></li>
						<li><span class="page_3"></span></li>
						<li>...</li>
						<li><span p=<?=$arResult['BOOK']['pageCount']?>><?=$arResult['BOOK']['pageCount']?></span></li>
						<li><span class="next fa fa-angle-right" title="Вперёд"></span></li>
						<li><span class="last fa fa-angle-double-right" title="В конец" p=<?=$arResult['BOOK']['pageCount']?>></span></li>
					</ul>
					<!--<span class="toggle-page-navigation"></span>-->
					<div class="page-navigation">
						<label>Переход к странице</label>
						<input type="text" class='page'>
						<span class="go"></span>
					</div>
				</div>
			
		</div>
	<div style="display: none;">
	    <div class="box-modal" id="exampleModal">
	        <div class="box-modal_close arcticmodal-close">закрыть</div>
			<p></p>
	    </div>
	</div>
		<!-- end main -->
		<!-- footer -->
		<div class="footer">
			<div class="footer-content">
				<div class="footer-top">
					<a href="/" class="footer-logo"><img src="/local/components/exalead/search.page.detail/templates/.default/images/footer-logo.png" alt=""></a>	
					<ul class="footer-menu"> 
						<li><a href="/about/">О проекте</a></li>
						<li><a href="/participants/">Участники</a></li>
						<li><a href="/for-libraries/legal-information/">Правовая информация</a></li>
					</ul>
					<ul class="footer-menu"> 

						<li><a href="/for-individuals/">Для читателей</a></li>
						<li><a href="/for-libraries/">Для библиотек</a></li>

						<li><a href="/user-agreement/">Пользовательское соглашение</a></li>
					</ul>
					<ul class="footer-menu"> 

						<li><a href="/faq/">Часто задаваемые вопросы</a></li>
						<li><a href="/feedback/">Обратная связь</a></li>
						<li><a href="/forum/">Форум</a></li>
					</ul>
				</div>
				<div class="foter-bottom">	
					<div class="copy">Все права защищены. Полное или частичное копирование материалов запрещено,<br> при согласованном использовании материалов необходима ссылка на ресурс. <br>
					Полное или частичное копирование произведений запрещено, согласование <br> использования произведений производится с их авторами.</div>	
					<div class="support">
						<div class="create">Разработчик АО «ЭЛАР»<br>По заказу</div>
						<a href="http://mkrf.ru/" target="_blank"><img src="/local/components/exalead/search.page.detail/templates/.default/images/ministr.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-small">
			<a href="http://mkrf.ru/" target="_blank"><img src="/local/components/exalead/search.page.detail/templates/.default/images/minkult.png" alt=""></a>
		</div>
		<!-- end footer -->
		<div id='big_image_holder' style='display:none;' align="center">
		      <div class="image-wrapper">
		          <img id="big_image" class='big_image original_image' style='display:none;' >
	                <div class="prev-page"></div>
	                <div class="next-page"></div>
	            </div>
		</div>

	</div>
	<?

	$APPLICATION->IncludeComponent("bitrix:system.auth.form", "for_viewer", 
		Array(
			"FORGOT_PASSWORD_URL" => "/auth/",
			"SHOW_ERRORS" => "N",
			"PROFILE_URL" => "/catalog2/"
		),
		false
	);
	?>

	<script type="text/javascript">
        /*$(function() {
            if($( "em:contains('Неверный логин или пароль')" )){
				console.log('111');
			}else{
				console.log('222');
			}
		});*/
	</script>

	</body>
</html>