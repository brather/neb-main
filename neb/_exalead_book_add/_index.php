<?
	/*
	Скрипт сохраняет атрибуты книги в таблице tbl_common_biblio_card и отправляет их в экзалиду
	ВНИМАНИЕ: Для работы скрипта нужен консольный пакет pdftotext 
	*/
	/*	
	Доступ к MySQL
	*/
	$DBHost = "localhost";
	$DBLogin = "neb";
	$DBPassword = "2PYshmi5aF6FnY";
	$DBName = "test";

	/*	
	Ключ проверки доступа
	*/
	$KEY = '44QFSp3TZji05gAKNlEy';

	/*	
	Доступ к Exalead для добавления книги
	*/
	$EX_IP = '213.208.168.8';
	$EX_PORT = 10002;
	$EX_CONNECTOR_NAME = 'test';

	/*	
	Путь для хранения PDF файлов
	*/

	$SAVE_DIR = '/_exalead_book_add/files/';


	/*	
	Push Api Exalead Library
	*/

	$papi = 'papi/';

	/************************************************************************************/
	/************************************START*******************************************/

	if(empty($_REQUEST['KEY']) or $_REQUEST['KEY'] != $KEY)
		die('Error KEY');

	$LIB_ID = (int)$_REQUEST['LIB_ID'];	
	$LIB_NAME = htmlspecialchars($_REQUEST['LIB_NAME']);
	$IdBxcard = (int)$_REQUEST['Id'];

	if(empty($LIB_ID) or empty($LIB_NAME))
		die('Empty LIB data');	

	require_once('database.php');
	require_once('functions.php');
	$db = new DBConnect($DBHost, $DBLogin, $DBPassword, $DBName, 3306);

	/*	
	Ищем таблицу связей tbl_common_biblio_card_bx
	*/
	$strSql = 'show tables like "tbl_common_biblio_card_bx" ;';
	if($db->query($strSql))
	{
		if(!$db->fetch_array())
			$db->query('
				CREATE TABLE IF NOT EXISTS `tbl_common_biblio_card_bx` (
				`ID` int(11) NOT NULL AUTO_INCREMENT,
				`ID_BX` int(11) NOT NULL,
				`ID_EXALEAD` int(11) NOT NULL,
				PRIMARY KEY (`ID`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
			');
	}

	/*	
	Проверяем, есть ли библиотека, если нет, то добавляем
	*/
	$alis_id_lib = false;

	//$db->query('select * from tbl_libraries_alis where Library = '.$LIB_ID);
	if($arRes = $db->fetch())
	{
		//$alis_id_lib = $arRes['Id'];
        print_r($arRes);
	}
	else
	{
		$db->query('INSERT INTO tbl_libraries_alis SET Name = "'.$LIB_NAME.'", Library = "'.$LIB_ID.'"');
		$alis_id_lib = $db->insert_id();

	}


	if(empty($alis_id_lib))
		die('Error alis ID');

	/*	
	Формируем массив полей для книги
	*/

	$arFieldsMapBookCard = array(
		'ALIS',
		'IdFromALIS',
		'Author',
		'Name',
		'SubName',
		'PublishYear',
		'PublishPlace',
		'Publisher',
		'ISBN',
		'Language',
		'CountPages',
		'Format',
		'Series',
		'VolumeNumber',
		'VolumeName',
		'Collection',
		'AccessType',
		'isLicenseAgreement',
		'Responsibility',
		'Annotation',
		'ContentRemark',
		'CommonRemark',
		'PublicationInformation',
		'pdfLink',
		#'CreationDateTime',
		#'UpdatingDateTime',
		'BBKText',
		'UDKText',
		'LanguageText',
	);

	$arFields = array();

	foreach($arFieldsMapBookCard as $field)
	{
		if(!empty($_POST[$field]))
			$arFields[$field] = htmlspecialchars(trim($_POST[$field]));
	}

	$arFields['ALIS'] = $alis_id_lib;

	/*
	Генерим IdFromALIS
	*/
	setIdFromALIS($arFields, $LIB_ID);

	/*
	Сохраняем pdf файл
	*/

	if(!file_exists($_SERVER['DOCUMENT_ROOT'].$SAVE_DIR))
		mkdir($_SERVER['DOCUMENT_ROOT'].$SAVE_DIR, 0775, true);

	chmod($_SERVER['DOCUMENT_ROOT'].$SAVE_DIR, 0775);		

	$fileExt = strtolower(pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION));
	if($fileExt != 'pdf')
		die('Error file extension');

	if($_FILES['upload']['error'] > 0)
		die('Error file upload');

	$fileName = $SAVE_DIR.$arFields['IdFromALIS'].'.'.$fileExt;	
	$fileNameTxt = $SAVE_DIR.$arFields['IdFromALIS'].'.txt';	

	move_uploaded_file($_FILES['upload']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].$fileName);

	$arFields['pdfLink'] = $fileName;


	/*	
	Выполняем запрос на добавление карточки книги
	*/
	$cardId = false;
	$strSql = 'select * from tbl_common_biblio_card where IdFromALIS = "'.$arFields['IdFromALIS'].'"';
	$db->query($strSql);
	$isNew = true;
	if($arRes = $db->fetch())
	{
		/* Обноавляем данные */
		$arFields['UpdatingDateTime'] = 'now()';
		$db->update('tbl_common_biblio_card', 'IdFromALIS = "'.$arFields['IdFromALIS'].'"', $arFields);
		$arFields['UpdatingDateTime'] = date('Y-m-d H:i:s');
		$cardId = $arRes['Id'];
		$isNew = false;
	}
	else
	{
		/*	Добавляем */
		$arFields['CreationDateTime'] = 'now()';
		$cardId = $db->insert('tbl_common_biblio_card', $arFields);
		if($IdBxcard > 0)
			$db->insert('tbl_common_biblio_card_bx', array('ID_BX' => $IdBxcard, 'ID_EXALEAD' => $cardId));
		$arFields['CreationDateTime'] = date('Y-m-d H:i:s');
	}


	if($cardId === false)
		die('Error save card');

	/*	
	Получаем текст из PDF
	$fileNameTxt
	*/
	exec('pdftotext -enc UTF-8 '.$_SERVER['DOCUMENT_ROOT'].$fileName.' '.$_SERVER['DOCUMENT_ROOT'].$fileNameTxt);

	chmod($_SERVER['DOCUMENT_ROOT'].$fileNameTxt, 0775);
	if(file_exists($_SERVER['DOCUMENT_ROOT'].$fileNameTxt)){
		$pdfContent = file_get_contents($_SERVER['DOCUMENT_ROOT'].$fileNameTxt);
		unlink($_SERVER['DOCUMENT_ROOT'].$fileNameTxt);
	}


	/*
	Добавляем данные в экзалиду 
	$isNew

	*/
	$pdfContent = substr($pdfContent, 0, 500);
	//$pdfContent = str_replace(array("\n", "\r"), ' ', $pdfContent);
	
	$pdfContent = htmlspecialchars($pdfContent, ENT_QUOTES);
	
	//$pdfContent = 'FRFLTVBZ YFER CCCH  KBNTHFNEHYST G F V Z N Y B R TESTTEXT2';
	#$pdfContent = 'АКАДЕМИЯ НАУК СССР  ЛИТЕРАТУРНЫЕ П А М Я Т Н И К';
	//$arFields['path'] = 'Path/To/Example/'.$arFields['IdFromALIS'];
	print_r($arFields);
	//$arFields['authorbook'] = 'kjfhksdjfsdkjfsdkfjsf';
	unset($arFields['Author']);
	unset($arFields['Name']);
	unset($arFields['PublishYear']);
	
	$arFields['id'] = $arFields['IdFromALIS'];
	
	require_once($papi.'PushAPI.inc');
	require_once($papi.'example/blog-style/tick.inc');

	try {
		echo "Create\n";
		$papi = PushAPIFactory::createHttp($EX_IP, $EX_PORT, $EX_CONNECTOR_NAME);

		
		#$papi->deleteDocument($arFields['IdFromALIS']);
		
		echo "Ping\n";
		$papi->ping();

		$myPart1 = new Part(
			array(
				'content' => $pdfContent,
				'directives' =>	array(
					'mimeHint' => 'text',
					'encoding' => 'utf-8'
				)
			)
		);
		
		$myDoc1 = new Document(
			array(
				'uri' 	=> $arFields['IdFromALIS'],
				'parts' => $myPart1,
				'metas' => $arFields
			)
		);

		print_r($myDoc1);
		tickSet('serialization');

		echo "Checking if document exists\n";
		$stamp = $papi->getDocumentStatus($arFields['IdFromALIS']);
		if ($stamp !== false) {
			echo "Yes, its stamp is " . $stamp . "\n";
		} else {
			echo "No, it does not yet exist\n";
		}
		echo "Pushing documents\n";
		$resp = $papi->addDocument(array($myDoc1));
		tickSet('pushing');

		echo "Set checkpoint\n";
		$serial = $papi->setCheckpoint(0, true);

		$cp = $papi->enumerateCheckpointInfo();
		foreach($cp as $k => $v) {
			echo "Checkpoint: name='" . $k . "' value='" . $v . "'\n";
		}

		$se = $papi->enumerateSyncedEntries($arFields['IdFromALIS'], PushAPI::RECURSIVE_DOCUMENTS);
		foreach($se as $url => $stamp) {
			echo "Entry: url='" . $url . "' stamp='" . $stamp . "'\n";
		}

		echo "Waiting for checkpoint serial " . $serial . "\n";

		echo "Triggering indexing job\n";
		$papi->triggerIndexingJob();
		tickSet('checkpoint+trigger');

		echo "Waiting for documents to be searchable ..\n";
		while(!$papi->areDocumentsSearchable($serial)) {
			echo "Waiting .. current  checkpoint: " . $papi->getCheckpoint()
			. "        " . "\r";
			sleep(3);
		}
		tickSet('indexing');

		/* Update a document */
		/*$docUpdate = new Document(
			array(
				'uri' => $arFields['IdFromALIS'],
				'metas' => $arFields
			)
		);
		//$resp = $papi->updateDocument($docUpdate, array_keys($arFields));
		$resp = $papi->updateDocument($myDoc1, array_keys($arFields));
		$serial = $papi->setCheckpoint(0, true);
		$papi->triggerIndexingJob();
		while(!$papi->areDocumentsSearchable($serial)) {
			echo "Waiting .. current  checkpoint: " . $papi->getCheckpoint()
			. "        " . "\r";
			sleep(3);
		}
		tickSet('updating');*/

		$papi->ping();
		$papi->close();
		echo "All done!\n";

		tickShow();
	} catch(PushAPIFactory $e)  {
		echo "Error: " . $e->getMessage() . "\n";
	}





	exit();

	try {

		echo "Create<br />\n";
		$papi = PushAPIFactory::createHttp($EX_IP, $EX_PORT, $EX_CONNECTOR_NAME);

		echo "Ping<br />\n";
		$papi->ping();

		$myPart1 = new Part(
			array(
				'content' => $pdfContent,
				'directives' =>	array(
					'mimeHint' => 'text/html',
					'encoding' => 'utf-8'
				)
			)
		);

		unset($arFields['UpdatingDateTime']);

		$myDoc1 = new Document(
			array(
				'uri' 	=>	$arFields['IdFromALIS'],
				'parts' =>	$myPart1,
				'metas' =>	$arFields
			)
		);

		$stamp = $papi->getDocumentStatus($arFields['IdFromALIS']);
		var_dump($stamp);

		$resp = $papi->addDocument(array($myDoc1));
		$serial = $papi->setCheckpoint(0, true);
		$cp = $papi->enumerateCheckpointInfo();
		$papi->triggerIndexingJob();


		$papi->ping();
		$papi->close();

	} catch(PushAPIFactory $e)  {	
		echo "Error: " . $e->getMessage() . "\n";
	}


	print_r($arFields);
	print_r($pdfContent);

?>