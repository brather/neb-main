<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LbcSpecialdivisionimages
 *
 * @ORM\Table(name="lbc_specialdivisionimages")
 * @ORM\Entity
 */
class LbcSpecialdivisionimages extends AbstractLbcImages
{

    /**
     * @var integer
     *
     * @ORM\Column(name="SpecialDivisionId", type="integer", nullable=false)
     */
    private $specialdivisionid;


    /**
     * Set specialdivisionid
     *
     * @param integer $specialdivisionid
     * @return LbcSpecialdivisionimages
     */
    public function setSpecialdivisionid($specialdivisionid)
    {
        $this->specialdivisionid = $specialdivisionid;

        return $this;
    }

    /**
     * Get specialdivisionid
     *
     * @return integer 
     */
    public function getSpecialdivisionid()
    {
        return $this->specialdivisionid;
    }

}
