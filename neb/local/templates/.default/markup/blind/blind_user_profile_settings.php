 
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php"  accesskey="0"  class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
			<div class="b-profile_side">
				<div class="b-profile_name">Николай Петров</div>
				<div class="b-profile_reg">Зарегистрирован: 24.04.2014</div>
				<div class="b-profile_reg">ограниченный доступ</div>
				<ul class="b-profile_info">
					<li><a href="blind_user_profile_settings.php" class="set_prof">настройки профиля</a></li>
					<li><a href="#" class="help" title="Помощь">Помощь</a></li>
					<li><a href="#" title="Выйти">Выйти</a></li>	
				</ul>
			</div>
		


		<div class="b-profile_set b-profile_brd rel">
				<h2>Смена пароля</h2>
				<hr>
				<form action="" class="b-form b-form_common b-profile_passform">
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings087">Текущий пароль</label>
							<div class="field validate">
								<input type="password" data-validate="password" data-required="true" value="" id="settings087" data-maxlength="30" data-minlength="2" name="pass" class="input">										
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings01">Новый пароль</label>
							<div class="field validate">
								<input type="password" data-validate="password" data-required="true" value="" id="settings01" data-maxlength="30" data-minlength="6" name="pass" class="input">										
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings02">Подтвердить новый пароль</label>
							<div class="field validate">
								<input data-identity="#settings01" type="password" data-required="true" value="" id="settings02" data-maxlength="30" data-minlength="6" name="pass" class="input" data-validate="password">										
							</div>
						</div>
					</div>


					<div class="fieldrow nowrap fieldrowaction">
						<div class="fieldcell ">
							<div class="field clearfix">
								<button class="formbtn" value="1" type="submit">Подтвердить</button>
								<div class="messege_ok hidden">
									<a href="#" class="closepopup">Закрыть окно</a>
									<p>На Вашу электронную почту, указанную при регистрации, отправлено письмо. <br/>Для подтверждения нового пароля воспользуйтесь ссылкой из письма. <br/>Срок действия ссылки - 24 часа.</p>
								</div>
							</div>
						</div>
					</div>
				</form>
				<h2>основные настройки</h2>
				<hr>
				<form action="" class="b-form b-form_common b-profile_setform">
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings03">Отображаемое имя</label>
							<div class="field validate">
								<input type="text" data-required="true" value="" id="settings03" data-maxlength="30" data-validate="alpha" data-minlength="2" name="pass" class="input">										
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings04">Адрес электронной почты</label>
							<div class="field validate">
								<input type="text" class="input" name="num" data-minlength="2" data-maxlength="30" id="settings04" data-validate="email" value="">									
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings05">Пол</label>
							<div class="field validate">
								<select name="" id="" class="js_select w270"  data-required="true">									
									<option value="1">мужской</option>
									<option value="2">женский</option>
								</select>									
							</div>
						</div>
					</div>
					
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">

							<label for="settings06">Дата рождения</label>
							<div class="field validate iblock seldate">
								<em class="error dateformat">Неверный формат даты</em>
								<em class="error required">Заполните</em>
								<em class="error yearsrestrict">Вам должно быть<br>не меньше 12 лет</em>
								<input type="hidden" class="realseldate" data-required="true" value="" id="settings06" name="REGISTER[PERSONAL_BIRTHDAY]">
								<select name="birthday" id=""  class="js_select w102 sel_day"  data-required="true">
									<option value="">день</option>
									<option value="01">1</option>
									<option value="02">2</option>
								</select>
								
								<select name="birthmonth" id=""class="js_select w165 sel_month"  data-required="true" >
									<option value="">месяц</option>
									<option value="01">январь</option>
									<option value="02">февраль</option>
								</select>
								
								<select name="birthyear" id=""  class="js_select w102 sel_year"  data-required="true">
									<option value="">год</option>
									<option value="1980">1980</option>
									<option value="1981">1981</option>
								</select>
							</div>									

						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings07">Немного о себе</label>
							<div class="field validate">
								<textarea class="textarea" name="about" id="settings07" data-minlength="2" data-maxlength="800" ></textarea>							
							</div>
						</div>
					</div>
		
					<h2>настройки поиска</h2>
					<hr>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock b-srch_numset">
							<label for="settings08">Количество <br>результатов поиска</label>
							<div class="field validate searchnum_set">
								<input type="text" value="" data-maxlength="300" data-minlength="2" data-validate="number" id="settings08" name="SEARCH[RESULT_NUMBER]"  class="input docnum">							
							</div>
						</div>
						<div class="fieldcell iblock">
							<label for="settings09">Максимальное количество <br>документов по запросу</label>
							<div class="field validate">
								<input type="text" value="" id="settings09" data-maxlength="300" data-minlength="2" data-validate="number" name="pass" class="input docnum">										
							</div>
						</div>

					</div>
					<div class="fieldrow nowrap fieldrowaction">
						<div class="fieldcell ">
							<div class="field clearfix">
								<button class="formbtn" value="1" type="submit">Сохранить</button>

							</div>
						</div>
					</div>
				</form>
			</div><!-- /.b-profilesettings-->


		</div><!-- /.b-mainblock -->
	</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>