<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
	if(!empty($arResult["ITEMS"]))
	{
	?>
		<div class="other-news-block">
			<h2>Другие новости</h2>
			<?
				foreach($arResult["ITEMS"] as $arItem)
				{
				?>
				<div class="other-news-block__item">
					<div><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>	
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>	
					<p><?=$arItem["~PREVIEW_TEXT"]?></p>		
				</div>
				<?
				}
			?>
		</div>
	<?
	}
?>