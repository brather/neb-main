-- Добавил в репозиторий, чтобы не искать в рэдмайне

-- <экзалидовская tbl_libraries>
DROP TRIGGER IF EXISTS update_neb_library;
CREATE TRIGGER `update_neb_library` AFTER UPDATE ON `tbl_libraries`
FOR EACH ROW BEGIN
  IF (@disableTriggerLibraryUpdate = 1)
  THEN
    SET @disableTriggerLibraryUpdate = NULL;
  ELSE
    SET @disableTriggerLibraryUpdate = 1;
    IF EXISTS(
        SELECT *
        FROM neb_beta.neb_libs
        WHERE ID = NEW.Id
    )
    THEN
      UPDATE neb_beta.neb_libs
      SET
        UF_ID           = NEW.IdFromALIS,
        UF_NAME         = NEW.Name,
        UF_STRUCTURE    = NEW.Structure,
        UF_ADRESS       = NEW.Address,
        UF_REGION       = NEW.Region,
        UF_DISTRICT     = NEW.District,
        UF_TOWN         = NEW.Settelement,
        UF_PHONE        = NEW.ChangedPhone,
        UF_COMMENT_1    = NEW.Comment,
        UF_LIBRARY_TYPE = NEW.IdLibraryType
      WHERE ID = NEW.Id;
    ELSE
      INSERT INTO neb_beta.neb_libs
      SET
        ID              = NEW.Id,
        UF_ID           = NEW.Id,
        UF_NAME         = NEW.Name,
        UF_STRUCTURE    = NEW.Structure,
        UF_ADRESS       = NEW.Address,
        UF_REGION       = NEW.Region,
        UF_DISTRICT     = NEW.District,
        UF_TOWN         = NEW.Settelement,
        UF_PHONE        = NEW.ChangedPhone,
        UF_COMMENT_1    = NEW.Comment,
        UF_LIBRARY_TYPE = NEW.IdLibraryType;
    END IF;
  END IF;
END;

DROP TRIGGER IF EXISTS insert_neb_library;
CREATE TRIGGER `insert_neb_library` AFTER INSERT ON `tbl_libraries`
FOR EACH ROW BEGIN
  IF NOT EXISTS(
      SELECT *
      FROM neb_beta.neb_libs
      WHERE ID = NEW.Id
  )
  THEN
    INSERT INTO neb_beta.neb_libs
    SET
      ID              = NEW.Id,
      UF_ID           = NEW.Id,
      UF_NAME         = NEW.Name,
      UF_STRUCTURE    = NEW.Structure,
      UF_ADRESS       = NEW.Address,
      UF_REGION       = NEW.Region,
      UF_DISTRICT     = NEW.District,
      UF_TOWN         = NEW.Settelement,
      UF_PHONE        = NEW.ChangedPhone,
      UF_COMMENT_1    = NEW.Comment,
      UF_LIBRARY_TYPE = NEW.IdLibraryType;
  END IF;
END;

DROP TRIGGER IF EXISTS delete_neb_library;
CREATE TRIGGER `delete_neb_library` AFTER DELETE ON `tbl_libraries`
FOR EACH ROW BEGIN
  IF EXISTS(
      SELECT *
      FROM neb_beta.neb_libs
      WHERE ID = OLD.Id
  )
  THEN
    DELETE FROM neb_beta.neb_libs
    WHERE ID = OLD.Id;
  END IF;
END;
-- </экзалидовская tbl_libraries>

-- <битриксовая neb_libs>
DROP TRIGGER IF EXISTS update_neb_bitrix_library;
CREATE TRIGGER `update_neb_bitrix_library` AFTER UPDATE ON `neb_libs`
FOR EACH ROW BEGIN
  REPLACE INTO neb_stats.neb_libraries_list
  SET
    id             = new.ID,
    name           = new.UF_NAME,
    neb_partner_id = (
      SELECT IBLOCK_ELEMENT_ID
      FROM b_iblock_element_prop_s4
      WHERE PROPERTY_25 = new.ID
      LIMIT 1
    );

  IF (@disableTriggerLibraryUpdate = 1)
  THEN
    SET @disableTriggerLibraryUpdate = NULL;
  ELSE
    SET @disableTriggerLibraryUpdate = 1;
    IF EXISTS(
        SELECT *
        FROM neb.tbl_libraries
        WHERE Id = NEW.ID
    )
    THEN
      UPDATE neb.tbl_libraries
      SET
        IdFromALIS    = NEW.UF_ID,
        Name          = NEW.UF_NAME,
        Structure     = NEW.UF_STRUCTURE,
        Address       = NEW.UF_ADRESS,
        Region        = NEW.UF_REGION,
        District      = NEW.UF_DISTRICT,
        Settelement   = NEW.UF_TOWN,
        ChangedPhone  = NEW.UF_PHONE,
        Comment       = NEW.UF_COMMENT_1,
        IdLibraryType = (
          SELECT IF(XML_ID IS NULL OR XML_ID = '', 0, CAST(XML_ID AS UNSIGNED))
          FROM b_user_field_enum
          WHERE ID = NEW.UF_LIBRARY_TYPE
        ),
        LibraryScale = (
          SELECT `VALUE`
          FROM b_user_field_enum
          WHERE ID = NEW.UF_LIBRARY_SCALE
        )
      WHERE Id = NEW.ID;
    ELSE
      INSERT INTO neb.tbl_libraries
      SET
        Id            = NEW.ID,
        SourceALIS    = 1,
        IdFromALIS    = NEW.UF_ID,
        Name          = NEW.UF_NAME,
        Structure     = NEW.UF_STRUCTURE,
        Address       = NEW.UF_ADRESS,
        Region        = NEW.UF_REGION,
        District      = NEW.UF_DISTRICT,
        Settelement   = NEW.UF_TOWN,
        ChangedPhone  = NEW.UF_PHONE,
        Comment       = NEW.UF_COMMENT_1,
        IdLibraryType = (
          SELECT IF(XML_ID IS NULL OR XML_ID = '', 0, CAST(XML_ID AS UNSIGNED))
          FROM b_user_field_enum
          WHERE ID = NEW.UF_LIBRARY_TYPE
        ),
        LibraryScale = (
          SELECT `VALUE`
          FROM b_user_field_enum
          WHERE ID = NEW.UF_LIBRARY_SCALE
        );
    END IF;
  END IF;
END;

DROP TRIGGER IF EXISTS insert_neb_bitrix_library;
CREATE TRIGGER `insert_neb_bitrix_library` AFTER INSERT ON `neb_libs`
FOR EACH ROW BEGIN
  REPLACE INTO neb_stats.neb_libraries_list
  SET
    id             = new.ID,
    name           = new.UF_NAME,
    neb_partner_id = (
      SELECT IBLOCK_ELEMENT_ID
      FROM b_iblock_element_prop_s4
      WHERE PROPERTY_25 = new.ID
      LIMIT 1
    );

  IF NOT EXISTS(
      SELECT *
      FROM neb.tbl_libraries
      WHERE Id = NEW.ID
  )
  THEN
    INSERT INTO neb.tbl_libraries
    SET
      Id            = NEW.ID,
      SourceALIS    = 1,
      IdFromALIS    = NEW.UF_ID,
      Name          = NEW.UF_NAME,
      Structure     = NEW.UF_STRUCTURE,
      Address       = NEW.UF_ADRESS,
      Region        = NEW.UF_REGION,
      District      = NEW.UF_DISTRICT,
      Settelement   = NEW.UF_TOWN,
      ChangedPhone  = NEW.UF_PHONE,
      Comment       = NEW.UF_COMMENT_1,
      IdLibraryType = (
        SELECT IF(XML_ID IS NULL OR XML_ID = '', 0, CAST(XML_ID AS UNSIGNED))
        FROM b_user_field_enum
        WHERE ID = NEW.UF_LIBRARY_TYPE
      ),
      LibraryScale = (
        SELECT `VALUE`
        FROM b_user_field_enum
        WHERE ID = NEW.UF_LIBRARY_SCALE
      );
  END IF;
END;

DROP TRIGGER IF EXISTS delete_neb_bitrix_library;
CREATE TRIGGER `delete_neb_bitrix_library` AFTER DELETE ON `neb_libs`
FOR EACH ROW BEGIN
  IF EXISTS(
      SELECT *
      FROM neb.tbl_libraries
      WHERE Id = OLD.ID
  )
  THEN
    DELETE FROM neb.tbl_libraries
    WHERE Id = OLD.ID;
  END IF;
END;
-- </битриксовая neb_libs>