<?
$MESS["AUTH_REISTER_BUTTON"] = "Регистрация";
$MESS["AUTH_LOGIN_BUTTON"] = "Войти";
$MESS["AUTH_TITLE"] = 'вход в личный кабинет';
$MESS["AUTH_NUMBER"] = "Номер читательского билета";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_BIRTDAY"] = "Дата вашего рождения";
$MESS["AUTH_PASS"] = "Пароль";
$MESS["AUTH_FORGOT_PASS"] = "забыли пароль?";
$MESS["AUTH_EXIT"] = "Выйти";
$MESS["AUTH_LINK_PERSONAL"] = "Личный кабинет";
$MESS["AUTH_MAIL"] = "E-mail / Логин / ЕЭЧБ";

$MESS["AUTH_SOCSERVICES"] = "через социальные сети";

$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND'] = 'Адрес эл. почты не найден';
$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC'] = 'Введите адрес электронной почты,<br>указанный при регистрации';
$MESS['AUTH_PASS_RECOVERY_REQUIRED_FIELD'] = 'Поле обязательно для заполнения';
$MESS['AUTH_PASS_RECOVERY_RECOVER'] = 'Восстановить пароль';
$MESS['AUTH_PASS_RECOVERY_TITLE'] = 'Восстановление пароля';
$MESS['AUTH_PASS_RECOVERY_SUCCESS'] = 'На почту выслано письмо с ссылкой на сброс пароля';

$MESS["AUTH_REISTER_AS"] = "Зарегистрироваться как";
$MESS["AUTH_READER"] = "Читатель";
$MESS["AUTH_OR"] = "или";
$MESS["AUTH_RIGHTHOLDER"] = "правообладатель";
$MESS["AUTH_CLOSE"] = "Отмена";

$MESS["AUTH_PASSWORD_CHANGED"] = 'Ваш пароль успешно изменен. <br/>Для продолжения работы с порталом заново авторизуйтесь.';
$MESS['NATIONAL_ELECTRONIC_LIBRARY'] = 'Национальная электронная библиотека';
$MESS['AUTHORIZATION'] = 'Авторизация';
$MESS['SIGN_WITH_IN'] = 'Войти через сторонние сервисы';
$MESS['AUTH_GOSUSLUGI'] = 'Авторизация через ГосУслуги';
$MESS['AUTH_RGB'] = 'Авторизация через РГБ';
$MESS['AUTH_VKONTAKTE'] = 'Авторизация через Vkontakte';
$MESS['AUTH_ODNOKLASSINIKI'] = 'Авторизация через Одноклассники';
$MESS['AUTH_FACEBOOK'] = 'Авторизация через Facebook';
$MESS['AUTH_LOGIN'] = 'E-mail / Логин / ЕЭЧБ';
$MESS['AUTH_PASSWORD'] = 'Пароль';
$MESS['AUTH_SIGN_IN'] = 'Войти';
$MESS['AUTH_CANCEL'] = 'Отмена';
$MESS['AUTH_FORGOT'] = 'Забыли пароль?';
$MESS['REGISTER'] = 'Зарегистрироваться';
$MESS[''] = '';
$MESS[''] = '';
$MESS[''] = '';
$MESS[''] = '';
$MESS[''] = '';
$MESS[''] = '';
