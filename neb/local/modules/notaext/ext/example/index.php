<?

/**
 * Class example
 * Базовый класс для расширения.
 * Его название должно соответствовать названию расширения
 *
 * Если вам нужно сделать какие-то дополнительные действия (добавить какие-то ещё файлы, удалить, переместить и т.п.)
 * то вы можете воспользоваться коллбэками и прописать в них создание на сайте всех необходимых папок-файлов.
 */
class example extends CExt
{
    var $EXT_ID = __CLASS__;
    var $EXT_VERSION;
    var $EXT_VERSION_DATE;
    var $EXT_NAME;
    var $EXT_DESCRIPTION;
    var $EXT_SORT;

    public function __construct()
    {
        parent::__construct();

        $this->EXT_VERSION = '1.0';
        $this->EXT_VERSION_DATE = '06.05.2014';

        $this->EXT_NAME = 'Example';
        $this->EXT_DESCRIPTION = 'Description';
        $this->EXT_SORT = 10000;

    }

    /**
     * Копирование файлов происходит автоматически,
     * регистрация хэндлеров (для пользовательских свойств и т.п.) происходит автоматически
     * но если Ваше расширение требует дополнительных действий - Вы можете воспользоваться ниже представленными коллбэками.
     */

    function onBeforeInstall()
    {

    }

    function onBeforeUninstall()
    {

    }

    function onAfterInstall()
    {

    }

    function onAfterUninstall()
    {

    }
}

?>