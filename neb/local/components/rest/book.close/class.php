<?php
CBitrixComponent::includeComponentClass('rest:rest.server');

use \Bitrix\Main\Loader;

/**
 * Компонент открывает/закрывает доступ к изданиям
 *
 * Class RestBookClose
 */
class RestBookClose extends RestServer
{
    /**
     * @var array
     */
    protected $_params
        = array(
            'book_id' => null,
            'close'   => null,
            'comment' => null,
        );

    /**
     * @var array
     */
    protected $_methodAllowGroups
        = array(
            'putAction' => array(
                1 => true,  // Администратор
                13 => true, // Глобальный оператор
                39 => true, // Оператор НЭБ_Доступ к изданиям
            ),
        );

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams()
    {
        if (empty($this->_params['book_id'])) {
            throw new Exception('Не передан обязательный параметр "book_id"');
        }
        if (null === $this->_params['close']) {
            throw new Exception('Не передан обязательный параметр "close"');
        }
        $this->_params['close'] = filter_var(
            $this->_params['close'],
            FILTER_VALIDATE_BOOLEAN
        );
        if (is_array($this->_params['book_id'])) {
            $this->_params['book_id'] = array_filter($this->_params['book_id']);
        }

        return $this;
    }

    protected function _getBook()
    {
        $book = \Nota\Exalead\LibraryBiblioCardTable::getByFullSymbolicId(
            $this->_params['book_id']
        );
        if (empty($book) || !isset($book[0]['Id'])) {
            throw new Exception("Book '{$this->_params['book_id']}' not found");
        }

        return $book[0];
    }

    protected function _getAlis($alis)
    {
        return \Nota\Exalead\BiblioAlisTable::getList(
            [
                'filter' => [
                    'Id'            => $alis,
                    // '!IpOrHostName' => null, ?? wtf? шо це таки? и как таки это понимать ?!
                ]
            ]
        )->fetch();
    }

    protected function _getLibraryEmail($libraryId)
    {
        Loader::includeModule('iblock');

        $obIBlockElement = new CIBlockElement();
        $arLibrary = $obIBlockElement->GetList(
            [],
            ['PROPERTY_LIBRARY_LINK' => $libraryId],
            false,
            false,
            ['PROPERTY_EMAIL']
        )->Fetch();
        if (isset($arLibrary['PROPERTY_EMAIL_VALUE'])) {
            return $arLibrary['PROPERTY_EMAIL_VALUE'];
        }

        return null;
    }


    /**
     * @return int
     * @throws Exception
     */
    public function putAction()
    {
        //debugLog($this->_params);

        Loader::includeModule('nota.exalead');
        $logParams = [
            'user_id'    => nebUser::getCurrent()->GetID(),
            'library_id' => 0,
            'book_id'    => $this->_params['book_id'],
            'comment'    => $this->_params['comment'],
            'error'      => null,
            'closed'     => $this->_params['close'],
        ];
        try {
            $this->arResult = [
                'success' => false,
                'data'    => [],
            ];
            $book = $this->_getBook();
            $alis = $this->_getAlis($book['ALIS']);

            if(isset($alis['Library'])) {
                $logParams['library_id'] = $alis['Library'];
            }
            // отправка запроса в стороннюю библиотеку
            if (!empty($alis['IpOrHostName'])) {
                if (!isset($this->_params['comment'])) {
                    throw new Exception(
                        'Не передан комментарий для библиотеки'
                    );
                } elseif (!empty($alis['Library'])) {
                    $obEvent = new CEvent();
                    $email = $this->_getLibraryEmail($alis['Library']);
                    if ($email) {
                        if (
                        $obEvent->Send(
                            'BOOK_CLOSING_REQUEST',
                            SITE_ID,
                            [
                                'LIBRARY_EMAIL' => $email,
                                'COMMENT'       => $this->_params['comment'],
                                'BOOK_ID'       => $this->_params['book_id'],
                                'AUTHOR'        => $book['Author'],
                                'TITLE'         => $book['Name'],
                                'SUB_TITLE'     => $book['SubName'],
                                'PUB_PLACE'     => $book['PublishPlace'],
                                'PUBLISHING'    => $book['Publisher'],
                                'YEAR'          => $book['PublishYear'],
                                'PAGES_COUNT'   => $book['CountPages'],
                                'INFO'          => $book['CommonRemark'],
                                'CLOSE_MESSAGE' => $book['close']
                                    ? 'закрыто'
                                    : 'открыто',
                            ]
                        )
                        ) {
                            $this->arResult = [
                                'success' => true,
                                'data'    => [
                                    'result'       => 'sent_to_library',
                                    'libraryEmail' => $email,
                                ]
                            ];
                        } else {
                            throw new Exception(
                                'Не удалось отправить Email администратору библиотеки'
                            );
                        }
                    } else {
                        throw new Exception('Нет Email адреса библиотеки');
                    }
                }
            }
            // смена доступа у локальных книг
            else {
                if ((boolean)$book['AccessType'] === (boolean)$this->_params['close']) {
                    throw new Exception('Книга уже ' . ($this->_params['close'] ? 'закрыта' : 'открыта'));
                }
                if (
                \Nota\Exalead\LibraryBiblioCardTable::setClosed(
                    $this->_params['book_id'],
                    $this->_params['close']
                )
                ) {
                    $this->arResult = [
                        'success' => true,
                        'data'    => [
                            'result' => 'saved_to_db',
                        ]
                    ];
                } else {
                    throw new Exception('Не удалось изменить статус книги');
                }
            }
            \Neb\Main\Stat\Tracker::track(
                'neb_book_change_access_action',
                $logParams
            );
        } catch (Exception $e) {
            $logParams['error'] = $e->getMessage();
            \Neb\Main\Stat\Tracker::track(
                'neb_book_change_access_action',
                $logParams
            );
            throw $e;
        }


        return 200;
    }

    public function getAction()
    {
        Loader::includeModule('nota.exalead');
        
        $book = $this->_getBook();
        $alis = $this->_getAlis($book['ALIS']);
        $libraryEmail = $this->_getLibraryEmail($alis['Library']);
        $this->arResult['data'] = [
            'libraryEmail'    => $libraryEmail,
            'externalLibrary' => empty($alis['IpOrHostName'])
                ? false
                : true,
        ];

        return 200;
    }
}