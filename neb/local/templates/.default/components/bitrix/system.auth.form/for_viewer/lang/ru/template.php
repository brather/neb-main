<?
$MESS["AUTH_REISTER_BUTTON"] = "Регистрация";
$MESS["AUTH_LOGIN_BUTTON"] = "Войти";
$MESS["AUTH_TITLE"] = 'вход в личный кабинет';
$MESS["AUTH_NUMBER"] = "Номер читательского билета";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_BIRTDAY"] = "Дата вашего рождения";
$MESS["AUTH_PASS"] = "Пароль";
$MESS["AUTH_FORGOT_PASS"] = "забыли пароль?";
$MESS["AUTH_EXIT"] = "Выйти";
$MESS["AUTH_LINK_PERSONAL"] = "Личный кабинет";
$MESS["AUTH_MAIL"] = "E-mail / Логин / ЕЭЧБ";

$MESS["AUTH_SOCSERVICES"] = "через социальные сети";

$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND'] = 'Адрес эл. почты не найден';
$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC'] = 'Введите электронную почту, указанную при регистрации';
$MESS['AUTH_PASS_RECOVERY_REQUIRED_FIELD'] = 'Поле обязательно для заполнения';
$MESS['AUTH_PASS_RECOVERY_RECOVER'] = 'Восстановить';
$MESS['AUTH_PASS_RECOVERY_SUCCESS'] = 'На почту выслано письмо с ссылкой на сброс пароля';

$MESS["AUTH_REISTER_AS"] = "Зарегистрироваться как";
$MESS["AUTH_READER"] = "Читатель";
$MESS["AUTH_OR"] = "или";
$MESS["AUTH_RIGHTHOLDER"] = "правообладатель";
$MESS["AUTH_CLOSE"] = "Закрыть окно";

$MESS["AUTH_PASSWORD_CHANGED"] = 'Ваш пароль успешно изменен. <br/>Для продолжения работы с порталом заново авторизуйтесь.';
?>