<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
 
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<link href="/local/templates/.default/markup/css/style.css" type="text/css" data-template-style="true" rel="stylesheet">
<link href="/user-agreement/style.css?2" type="text/css" data-template-style="true" rel="stylesheet">
<style>
	body{height:auto;}
</style>
<h3><?=GetMessage('AGREEMENT_TEXT_TITLE')?> </h3>
<ul class="b-commonlist offerta-list">
	<li>1.1. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_1')?></li>
	<li>1.2. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_2')?></li>
	<li>1.3. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_3')?></li>
	<li>1.4. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_4')?></li>
	<li>1.5. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_5')?></li>
	<li>1.6. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_6')?></li>
	<li>1.7. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_7')?></li>
	<li>
		1.8. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_8')?>
		<br>
		<strong><?=GetMessage('AGREEMENT_USER_RIGHTS_TITLE')?></strong>
		<ul class="b-commonlist">
			<li><?=GetMessage('AGREEMENT_USER_RIGHTS_LIST_1')?></li>
			<li><?=GetMessage('AGREEMENT_USER_RIGHTS_LIST_2')?></li>
			<li><?=GetMessage('AGREEMENT_USER_RIGHTS_LIST_3')?></li>
		</ul>
		<strong><?=GetMessage('AGREEMENT_BIBL_RIGHTS_TITLE')?></strong>
		<ul class="b-commonlist">
			<li><?=GetMessage('AGREEMENT_BIBL_RIGHTS_LIST_1')?></li>
			<li><?=GetMessage('AGREEMENT_BIBL_RIGHTS_LIST_2')?></li>
			<li><?=GetMessage('AGREEMENT_BIBL_RIGHTS_LIST_3')?></li>
		</ul>
	</li>
	<li>1.9. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_9')?></li>
	<li>1.10. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_10')?></li>
	<li>1.11. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_11')?></li>
	<li>1.12. <?=GetMessage('AGREEMENT_TEXT_COMMON_LIST_12')?></li>
</ul>
