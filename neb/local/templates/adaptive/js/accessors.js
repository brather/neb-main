var FRONT = FRONT || {};
FRONT.accessors = {
	ratings: {}
}
FRONT.assets = FRONT.assets || {}
// FRONT.accessors.ratings.autocomplete = function(container) {

// 	var items = $(container).find('[data-filter-request]'),
// 		search = false,
// 		successes = {//парсинг ответа
// 	        default: function(data, entityField, response) {
// 	        	console.log('default');
// 	            var queries = [];
// 	            if (data.entities) {
// 	                for (var key in data.entities) {
// 	                    if(data.entities[key][entityField]) {
// 	                        queries.push(data.entities[key][entityField]);
// 	                    }
// 	                }
// 	            }
// 	            response(queries);
// 	        },
// 	        search: function(data, entityField, response) {
// 	        	console.log('search');
// 	            var queries = [];
// 	            if (data) {
// 	                for (var i = 0; i < data.length; i += 1) {
// 	                    if(typeof data[i] == 'object') {
// 	                        queries.push(data[i]['label']);
// 	                    }
// 	                }
// 	            }
// 	            response(queries);
// 	        }
// 	    };


// 	this.widget = container;

// 	items.each(function(){
// 		var search = typeof undefined !== typeof $(this).attr('data-search-auto-complete');
// 		var options = {
// 			success: search ? successes.search : successes.default
// 		};
// 		applyAutoComplete( $(this) , options );
// 	});

// 	function applyAutoComplete (input, options) {
// 		var sourceUrl = input.data('src'),
// 			entityField = input.data('entity-field'),
// 			entityList = {},
// 			queryParam = input.data('query-param') || 'query',
// 			fieldParam = input.data('field-param') || 'entityField',
// 			_success = options.success ? successes.search : successes.default;

//         input.autocomplete({
//             serviceUrl: sourceUrl,
//             delay: 500,
//             minLength: 1,
//             source: function (request, response) {
// 				var queryData = {};
// 				queryData[queryParam] = input.val().toString();
// 				queryData[fieldParam] = entityField;
//                 $.ajax({
//                     url: sourceUrl,
//                     dataType: 'json',
//                     data: queryData,
//                     success: function (data) {
// 						entityList = {};
// 						_success(data, entityList, entityField, response, request);
//                     }
//                 });
//             },

//             select: function( event, ui ) {
//             	var url = window.location.href,
//             		search; 
//             	if ( url.indexOf('authors') != -1 ) {
//             	} else if ( url.indexOf('editions') != -1 || url.indexOf('substitustion') != -1 ) {
//             	} else {
//             		search = '?query=' + ui.item["value"];
//             		window.location.search = search;
//             	}
//             },

//             autofocus: true
//         })
// 	}

// }
FRONT.accessors.ratings.model = function(el) {
	var item = el,
		widgetIsDirty = false,
		addUrlTemplate = '/api/assessor/{entity}/create/',
		autoCompleteSource = FRONT.getAutoCompleteSource() + '?s_in_closed=on&is_full_search=on&sessid='+ FRONT.getSessId(),
		itemId,widget,widgetType,authorTemplate,editionTemplate,requestTemplate,complexRequestTemplate,itemTemplate;


	authorTemplate = $('<dd data-new-item data-action="' + addUrlTemplate.replace('{entity}', 'author_rating') + '">'
		+'<span><input type="text" data-write-required name="authorrating[author]" data-save-input placeholder="Введите автора" data-filter-request data-entity-field="author" data-search-auto-complete data-query-param="term" data-field-param="field" data-src="' + autoCompleteSource +'"/></span>'
		+'<span><input type="number" data-write-required name="authorrating[rating]" data-save-input  placeholder="0"/></span>'
		+'<span>'
			+'<button class="btn btn-default button-save" data-save-item="" disabled>&nbsp;</button>\n'
			+'<button class="btn btn-default button-reset" data-reset-item="">&nbsp;</button>\n'
			+'<button class="btn btn-default button-remove" data-remove-item="" data-title="Удалить автора?">&nbsp;</button>'
		+'</span>'
	+'</dd>');
	editionTemplate = $('<dd data-new-item data-action="' + addUrlTemplate.replace('{entity}', 'edition_rating') + '">'
		+'<span><input type="text" name="bookrating[author]" data-save-input placeholder="Введите автора" data-filter-request data-entity-field="author" data-search-auto-complete data-query-param="term" data-field-param="field" data-src="' + autoCompleteSource +'" /></span>'
		+'<span><input type="text" name="bookrating[title]" data-write-required data-save-input placeholder="Введите заголовок" data-filter-request data-entity-field="title" data-search-auto-complete data-query-param="term" data-field-param="field" data-src="' + autoCompleteSource +'"/></span>'
		+'<span><textarea data-save-textarea name="bookrating[keywords]" placeholder="Введите Ключевые слова"></textarea></span>'
		+'<span><input type="number" name="bookrating[rating]" data-write-required data-save-input placeholder="0"/></span>'
		+'<span>'
			+'<button class="btn btn-default button-save" data-save-item="" disabled>&nbsp;</button>\n'
			+'<button class="btn btn-default button-reset" data-reset-item="">&nbsp;</button>\n'
			+'<button class="btn btn-default button-remove" data-remove-item="" data-title="Удалить издание?">&nbsp;</button>'
		+'</span>'
	+'</dd>');
	requestTemplate = $('<dd data-new-item data-action="' + addUrlTemplate.replace('{entity}', 'query_frequency') + '">'
		+'<span><input type="text" data-write-required name="queryfrequency[query]" data-save-input placeholder="Введите поисковый запрос" data-filter-request data-entity-field="query" data-src="/api/assessor/query_frequency/autocomplete/"/></span>'
		+'<span></span>'
		+'<span>'
			+'<button class="btn btn-default button-save" data-save-item="" disabled>&nbsp;</button>\n'
			+'<button class="btn btn-default button-reset" data-reset-item="">&nbsp;</button>\n'
			+'<button class="btn btn-default button-remove" data-remove-item="" data-title="Удалить запрос?" data-description="Удалить исправленную выдачу по запросу?">&nbsp;</button>'
		+'</span>'
	+'</dd>');
	complexRequestTemplate = $('<dd data-new-item data-action="' + addUrlTemplate.replace('{entity}', 'substitution') + '">'
		+'<span><input type="text" data-write-required name="querysubstitution[originalQuery]" data-save-input placeholder="Введите поисковый запрос"/></span>'
		+'<span><input type="text" data-write-required name="querysubstitution[modifiedQuery]" data-save-input placeholder="Введите измененный поисковый запрос"/></span>'
		+'<span><input type="number" data-write-required name="querysubstitution[priority]" data-save-input placeholder="0"/></span>'
		+'<span>'
			+'<button class="btn btn-default button-save" data-save-item="" disabled>&nbsp;</button>\n'
			+'<button class="btn btn-default button-reset" data-reset-item="">&nbsp;</button>\n'
			+'<button class="btn btn-default button-remove" data-remove-item="" data-title="Удалить запрос?">&nbsp;</button>'
		+'</span>'
	+'</dd>');

	if (item) {
		itemId = item.data('item');
		// widget = item.closest('[data-widget]');
	}

	var _prepareApiUrl = function (url) {
		return url + (-1 === url.indexOf('?') ? '?' : '&') + 'token=' + getUserToken()
	};

	return {
		removeItem: function(toggler) {
			var message = {
	                title: toggler.data('title') || 'Удалить запись?',
	                confirmTitle: "Удалить",
	                text: toggler.data('description') || "Вы действительно хотите удалить запись из рейтинга?"
	            },
	            widget = toggler.closest('[data-widget]');
			if ( $(item).data('newItem') !== undefined ) {
				item.detach();
				this.dirtySwitch(widget);
			} else {
				toggler.addClass('action-inprogress');
                toggler.prop('disabled',true);
		        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
		        	var url = item.data('action');
					url = _prepareApiUrl(url);
					
		            if(confirmed){
		                $.ajax({
		                    url: url,
		                    data: {},
		                    method: 'DELETE',
		                    beforeSend: function(jqXHR,settings){
		                        
		                    },
		                    success: function(data){
		                    	if ( isJSON(data) ) {
									$(item).fadeOut(function(){
										this.remove();
									});
		                    	} else {
		                    		FRONT.infomodal({title:"Произошла ошибка, запись не удалена."});
		                    		
		                    	}
		                    }
		                }).fail(function( jqXHR, textStatus, errorThrown ){
		                	if ( jqXHR.hasOwnProperty('status') && jqXHR["status"] == 200 || jqXHR.hasOwnProperty('status') && jqXHR["status"] == 404 ) {
		                		$(item).fadeOut(function(){
									this.remove();
								});
		                	}
		                })
		                .always(function(){
		                	toggler.removeClass('action-inprogress');
                    		toggler.prop('disabled',false)
		                });
		            } else {
		            	toggler.removeClass('action-inprogress');
                		toggler.prop('disabled',false);
		            }
		        });
			}
		},
		changeItem: function(){
			var isDirty = false,
				widget = item.closest('[data-widget]'),
				saveable = true;

			item.find('input, textarea').each(function(){
				var input = $(this);
				if (input.val() !== input.get(0).defaultValue) {
					isDirty = true;
				}
				if (input.val() == '' && input.get(0).hasAttribute("data-write-required") ) {
					saveable = false;
				}
			});
			widgetIsDirty = isDirty;
			item.toggleClass('dirty',isDirty);
			this.dirtySwitch(widget);
			item.find('[data-save-item]').prop('disabled',!saveable);
		},
		resetItem: function(){
			item.find('input, textarea').each(function(){
				var el = $(this);
				el.val( el.get(0).defaultValue ).trigger('change').trigger('blur');
			});
			if (item[0].hasAttribute("data-new-item")) {
				item.find('[data-remove-item]').click();
			}
		},
		saveItem: function(toggler){
			var widget = toggler.closest('[data-widget]'),
				controller = this,
				url = item.data('action'),
				currentItem = item.get(0),
				saveMethod = (currentItem && currentItem.hasAttribute('data-item')) ? 'PATCH' : 'POST'
				;
			url = _prepareApiUrl(url);


			$.ajax({
                url: url,
                data: item.find('[data-save-input], [data-save-textarea]').serialize(),
                method: saveMethod,
                beforeSend: function(jqXHR,settings){
                    toggler.addClass('action-inprogress');
                    toggler.prop('disabled',true);
                },
                success: function(data){
                	var itemId = 'zaz',
                		widget = item.closest('[data-widget]');

                	if ( data.hasOwnProperty('id') ) {
                		itemId = data["id"];
                		item.find('input, textarea').each(function(){
	                		$(this)[0].defaultValue = $(this).val();

	                	});
	                	if ( item[0].hasAttribute("data-new-item") ) {
		                	item.removeAttr('data-new-item');
		                	item.attr('data-item',itemId);
	                	}

						controller.applySaved(widget,itemId);

                		controller.dirtySwitch(widget);
                	}

                	if ( isJSON(data) ) {
                	}
                	//  else {
                	// 	FRONT.infomodal({title:"Произошла ошибка, запись не сохранена."});
                	// }
                }
            }).always(function(){
        		toggler.removeClass('action-inprogress');
            	toggler.prop('disabled',false);
            }).fail(function( jqXHR, textStatus, errorThrown ){
            	var mesStack, mes = '';
            	if ( typeof data == 'object' && data.hasOwnProperty('author')) {
            	}

            	if ( typeof jqXHR.responseJSON == 'object') {
            		mesStack = jqXHR.responseJSON.children;            		
            		for (var mestype in mesStack) {
            			if (typeof mesStack[mestype].errors == 'object') {
            				mes += mesStack[mestype].errors;
            			}
            		}
            	}

            	if (mes) {
					FRONT.infomodal({
						title:"Произошла ошибка, запись не сохранена.", 
						textContent: mes 
					});
            	}
            });
		},
		addItem: function(toggler){
			widgetIsDirty = true;
			var widget = toggler.closest('[data-widget]');

			if (widget[0].hasAttribute("data-widget-editions")) {
				itemTemplate = editionTemplate;
			}
			if (widget[0].hasAttribute("data-widget-authors")) {
				itemTemplate = authorTemplate;
			}
			if (widget[0].hasAttribute("data-widget-requests")) {
				itemTemplate = requestTemplate;
			}
			if (widget[0].hasAttribute("data-widget-complexrequests")) {
				itemTemplate = complexRequestTemplate;
			}

			var item = itemTemplate
				.addClass('new')
				.insertAfter( widget.find('dt')),
				input = item.find('input');
			toggler.attr('disabled',true);
			setTimeout(function(){
				item.css('opacity','1');
			},100);
			input.first().focus();
			// FRONT.accessors.ratings.autocomplete(item);
			FRONT.search.autocompleteSimple(item);
			// this.applyAutoComplete(input);
			this.dirtySwitch(widget)
		},
		dirtySwitch: function(widget){
			widget.toggleClass('dirty-widget',widgetIsDirty);
			widget.find('dt [data-add-item]').prop('disabled',widgetIsDirty);

			widget.find('[data-item]').each(function(){
				var thisDirty = $(this).hasClass('dirty');
				if (widgetIsDirty) {
					if (thisDirty) {
						$(this).find('input').prop('disabled',false);
						$(this).find('button').prop('disabled',false);
					} else {
						$(this).find('input').prop('disabled',true);
						$(this).find('button').prop('disabled',true);
					}
				} else {
					$(this).find('input').prop('disabled',false);
					$(this).find('button').prop('disabled',false);
				}
            });
        },

		applySaved: function(widget,itemId) {
			if ( widget.get(0).hasAttribute('data-widget-authors') ) {
				item.find('[type="text"]').each(function(){
					$(this).parent().html( $(this).val() );
				});
				item.find('[type="number"]').each(function(){
					$(this).get(0).defaultValue = $(this).val();
				});
				item.attr('data-action','/api/assessor/author_rating/'+itemId+'/').removeClass('new dirty');
				item.data('action','/api/assessor/author_rating/'+itemId+'/');
			}
			if ( widget.get(0).hasAttribute('data-widget-editions') ) {
				item.find('[type="text"]').each(function(){
					$(this).parent().html( $(this).val() );
				});
				item.find('[type="number"]').each(function(){
					$(this).get(0).defaultValue = $(this).val();
				});
				item.find('textarea').each(function(){
					if ( !$(this).parent().find('i').length ) {
						$(this).parent().prepend( $('<i>...</i>') );
					}
					$(this).parent().find('i').show();
					$(this).get(0).defaultValue = $(this).val();
				});
				item.attr('data-action','/api/assessor/edition_rating/'+itemId+'/').removeClass('new dirty');
				item.data('action','/api/assessor/edition_rating/'+itemId+'/');
			}
			if ( widget.get(0).hasAttribute('data-widget-requests') ) {
				item.find('[type="text"]').each(function(){
					var inp = $(this),
						link = $('<a>').attr('href','/profile/assessor-workplace/search/?queryId=' + itemId).text( inp.val() );

					inp.get(0).defaultValue = inp.val();
					inp.hide();
					inp.parent().append( link );
				});
				item.find('button').detach();
				item.attr('data-action','/api/assessor/query_frequency/'+itemId+'/').removeClass('new dirty');
				item.data('action','/api/assessor/query_frequency/'+itemId+'/');
			}
			if ( widget.get(0).hasAttribute('data-widget-complexrequests') ) {
				item.find('[type="text"],[type="number"]').each(function(){
					$(this).get(0).defaultValue = $(this).val();
				});
				item.attr('data-action','/api/assessor/substitution/'+itemId+'/').removeClass('new dirty');
				item.data('action','/api/assessor/substitution/'+itemId+'/');
			}
		}


  //       applyAutoComplete: function (input) {
  //           var sourceUrl = input.data('src'),
  //               entityField = input.data('entity-field'),
  //               entityList = {}
  //               ;
  //           input.autocomplete({
  //               serviceUrl: sourceUrl,
  //               delay: 500,
  //               minLength: 1,
  //               source: function (request, response) {
  //                   $.ajax({
  //                       url: sourceUrl,
  //                       dataType: 'json',
  //                       data: {
		// 					query: input.val().toString(),
		// 					entityField: entityField
		// 				},
  //                       success: function (data) {
  //                           var queries = [];
  //                           entityList = {};
  //                           if (data.entities) {
  //                               for (var key in data.entities) {
  //                                   if(data.entities[key][entityField]) {
  //                                       queries.push(data.entities[key][entityField]);
  //                                       entityList[data.entities[key][entityField]] = data.entities[key].id;
  //                                   }
  //                               }
  //                           }
  //                           response(queries);
  //                       }
  //                   });
  //               },
  //               autofocus: true
  //           })
		// }
	}
}

$(function(){

	var Author = FRONT.accessors.ratings.model,
		Rating = FRONT.accessors.ratings.model;

    $(document).on('click','[data-widget-ratings] [data-remove-item]',function(e){
		var toggler = $(e.target),
            itemEl = toggler.closest('[data-item]'),
            rating;
		if (!itemEl.length) {
        	itemEl = $(toggler).closest('[data-new-item]');
        }
		rating = new Rating(itemEl);

        rating.removeItem(toggler);
    })
	.on('change keyup','[data-widget-ratings] input, [data-widget-ratings] textarea',function(e){
		var input = $(this),
			itemEl = input.closest('[data-item],[data-new-item]'),
			rating = new Rating(itemEl);

		rating.changeItem();
	})
	.on('click','[data-widget-ratings] [data-reset-item]',function(e){
		var toggler = $(e.target),
            itemEl = toggler.closest('[data-item]'),
            rating;
        if (!itemEl.length) {
        	itemEl = $(toggler).closest('[data-new-item]');
        }
        rating = new Rating(itemEl);

        rating.resetItem();
	})
	.on('click','[data-widget-ratings] [data-save-item]',function(e){
		var toggler = $(e.target),
            itemEl = toggler.closest('[data-item]');
        if (!itemEl.length) {
        	itemEl = toggler.closest('[data-new-item]');
        }
    	var rating = new Rating(itemEl);

        rating.saveItem(toggler);
	})
	.on('click','[data-widget-ratings] [data-add-item]',function(e){
		var toggler = $(e.target),
			rating = new Rating();

		rating.addItem(toggler);
	})
	.on('keyup','[data-widget-ratings] [data-new-item] input, [data-widget-ratings] [data-new-item] textarea', function(e){
		var empty = true;
		if (e.keyCode === 27) {
			$(this).closest('[data-new-item]').find('input,textarea').each(function(){
				if ($(this).val() != '') {
					empty = false;
				}
			});
			if (empty) {
				$(this).closest('[data-new-item]').find('[data-remove-item]').click();
			}
		}
	})
	.on('click','[data-widget-editions] i',function(){
		$(this).toggle();
		$(this).next().focus();
	})
	.on('blur','[data-widget-editions] textarea',function(){
		var el = $(this);
		if ( el.val() == el[0].defaultValue ) {
			el.prevAll('i').show();
		}
	})
		.on('keypress', '[data-filter-request]', function (e) {
			if (e.which == 13) {
				// var entityField = $(this).data('entity-field');
				var entityField = $(this).data('post');
                    url = FRONT.utils.removeQueryParameter(location.href, entityField);
                location.href = url
                    + (-1 === url.indexOf('?') ? '?' : '&')
                    + entityField + '=' + $(this).val()
            }
		})
		.on('click', '[data-save-sorting]', function () {
			var editions = [],
				sortContext = $(this).data('sort-context'),
				url = $(this).data('save-url')
				;
			$('.search-result__content-list div.item-container.sorting-item').each(function () {
				var editionId = $(this).data('edition-id');
				editions.push(editionId);
				$(this).removeClass('sorting-item');
			});
			sortContext.editions = editions;
			url = FRONT.utils.addUrlTokem(url);

			$.ajax({
				url: url,
				data: {querysorting: sortContext},
				method: 'POST',
				success: function(data){
					location.reload();
				},
				error: function(data) {
					FRONT.infomodal({title:"Произошла ошибка."});
				}
			});

	});

    var widget = $('[data-widget-ratings]');
    // var	widgetControl = FRONT.accessors.ratings.autocomplete(widget);

    // var rating = new Rating();
    // rating.applyAutoComplete($('[data-filter-request]'));

	$('.search-result__content-main-read-btn').hide();
	$('ul.search-result__content-list').sortable({
		handle: '.search-result__content-list-kind',
		update: function (e, object) {
			$(object.item.context).prevAll().addClass('sorting-item');
			$(object.item.context).addClass('sorting-item');
		}
	});
});