<?php
/**
 * @var array $arResult
 */
if (isset($arResult['userData'])) {
    $arResult['arUser'] = $arResult['userData'];
} else {
    $arResult['arUser'] = [];
}

if (isset($arResult['arUser']['ID'])) {
    $arResult['ID'] = $arResult['arUser']['ID'];
}