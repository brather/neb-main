<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var array                    $arResult
 * @var array                    $arParams
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent         $component
 */
$templatePath = $_SERVER['DOCUMENT_ROOT'] . $this->__folder . '/';
$arResult['contentTemplatePath'] = $templatePath
    . $arResult['componentPage'] . '.php';
$arResult['leftTemplatePath'] = $templatePath . 'left_layout/'
    . $arResult['USER_ROLE'] . '.php';
if (!file_exists($arResult['leftTemplatePath'])) {
    $arResult['leftTemplatePath'] = $templatePath . 'left_layout/main.php';
}