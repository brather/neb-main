<?php

use \Neb\Main\Helper\DbHelper;
use \Neb\Main\Library\NebLibsCityTable;

/**
 * User: agolodkov
 * Date: 05.12.2015
 * Time: 14:41
 */
class CitiesComponent extends \Neb\Main\ListComponent
{
    /**
     * @var array
     */
    public $_defaultResult
        = array(
            'regionName' => null,
        );

    /**
     * @return $this
     */
    public function prepareParams()
    {
        return $this;
    }

    /**
     * @return $this
     */
    public function loadList()
    {
        if (null === $this->arParams['regionName']) {
            $this->_loadTable(new NebLibsCityTable());
        } else {
            $queryString
                = "
SELECT nlc.*
FROM neb_libs nl
  JOIN neb_libs_city nlc ON nlc.ID = nl.UF_CITY
WHERE nl.UF_REGION = '%s'
GROUP BY nlc.ID;";
            $queryString = sprintf(
                $queryString,
                DbHelper::mysqlEscapeString($this->arParams['regionName'])
            );
            $this->arResult['items']
                = NebLibsCityTable::getEntity()
                ->getConnection()
                ->query($queryString)
                ->fetchAll();
        }

        return $this;
    }
}