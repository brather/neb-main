<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title></title>
	
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width"/>
	<link rel="icon" href="favicon.ico" type="image/x-icon" />

	<script src="js/libs/modernizr.min.js"></script>

	<link rel="stylesheet" href="css/style.css">
	
	<script src="js/libs/jquery.min.js"></script>
	<script type="text/javascript" src="js/slick.min.js"></script>

	<script src="js/plugins.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	<script>window.jQuery.ui || document.write('<script src="js/libs/jquery.ui.min.js">\x3C/script>')</script>
	<script src="js/jquery.knob.js"></script>
	<script src="js/script.js"></script>
	<script>
	$(function() {
        $('html').addClass('mainhtml'); /* это для морды */
    }); /* DOM loaded */
	</script>

</head>
<body>
	<div class="homepage">
		<section class="slidebooklist">
			<a href="#" class="backpage">back</a>
			<div class="wrapper">
				<div class="b-addbook_popuptit clearfix">
					<span class="b-allbooks right">всего 2 книги в коллекции</span>
					<h2>Добавленные издания<span>+2</span></h2>
				</div>
				<div class="book_slider">
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_35.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_36.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_37.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_38.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_35.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_40.jpg"></a>
				</div>
				<p class="tcenter"><a href="#" class="button_orange">Сохранить</a></p>
			</div>
		</section><!--/slidebooklist-->
		<section class="mainsection innerpage" >	
			<form action="" class="searchform">
				<div class="b-search wrapper">
					<div class="b-search_field">
						<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
						<div class="b_search_set clearfix">
							<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>
						</div> <!-- /.b_search_set -->
					</div>
				</div> <!-- /.b-search -->
				<div class="b-search_ex">
					<div class="wrapper rel">
						<div class="b_search_row js_search_row hidden" >
							<select name="logic" disabled="disabled" id="" class="ddl_logic">
								<option value="">или</option>
								<option value="">и</option>
								<option value="">не</option>
							</select>
							<select name="theme" disabled="disabled" id="" class="ddl_theme">
								<option value="">по всем полям</option>
								<option value="">по дате</option>
								<option value="foraccess">по доступу</option>
								<option value="">по названию</option>
								<option value="">по месту издательства</option>
							</select>
							<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
							<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
								<option value="">Свободный доступ</option>
								<option value="">Частичный доступ</option>
							</select>
							<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
						</div>
						<div class="b_search_row visiblerow">
							<select name="logic[0]" id="" class="js_select ddl_logic ">
								<option value="">или</option>
								<option value="">и</option>
								<option value="">не</option>
							</select>
							<select name="theme[0]" id="" class="js_select ddl_theme">
								<option value="">по всем полям</option>
								<option value="">по дате</option>
								<option value="foraccess">по доступу</option>
								<option value="">по названию</option>
								<option value="">по месту издательства</option>
							</select>
							<input type="text" name="text[0]" class="b-text b_search_txt" id="">
							<select name="taccess" id="" class="js_select b-access hidden">
								<option value="">Свободный доступ</option>
								<option value="">Частичный доступ</option>
							</select>
							<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

							<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
						</div>						
						
						<div class="b_search_date">
							<div class="b_search_datelb iblock">Дата публикации</div>
							<div class="b_searchslider iblock js_searchslider"></div>
							<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
							<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />
							
						</div>
						<div class="b_search_row clearfix">
						<div class="checkwrapper right">
								<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
						</div>
						<input type="submit" class="formbutton" value="Принять">
						
						</div>
					</div>
				</div><!-- /.b-search_ex -->
			</form>			
			

		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="0"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right">
						<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innerwrapper clearfix">
		<div class="b-searchresult">
			<span class="b-searchresult_label iblock">Найдено 74 документа:</span>
			<ul class="b-searchresult_list iblock">
				<li><a href="#" class="current">43 <span class="under">книги</span></a></li>
				<li><a href="#">25 <span class="under">рукописей</span></a></li>
				<li><a href="#">1 <span class="under">видео</span></a></li>
				<li><a href="#">3 <span class="under">аудиокниги</span></a></li>
			</ul>			            
		</div><!-- /.b-searchresult-->
	<div class="b-mainblock left">
		<div class="b-filter js_filter b-filternums">
			<div class="b-filter_wrapper">			
				<a href="#" class="sort up">По автору</a>
				<a href="#" class="sort">По названию</a>
				<a href="#" class="sort">По дате</a>
			</div>
		</div><!-- /.b-filter -->
			<div class="b-result-doc b-collection_booklist">
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_4.jpg"></a>
						<a href="#" class="btnadd">Добавить в коллекцию</a>
					</div>
					<div class="iblock b-result-docinfo">
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
						</ul>
						<div class="b-collstat">
						<div class="b-lib_counter">
							<span class="b-lib_counter_lb">Читатели</span>
							<span class="icouser">х 108</span>
						</div>
						<div class="b-lib_counter">
							<span class="b-lib_counter_lb">Просмотры</span>
							<span class="icoviews">х 15 678</span>
						</div>
						<div class="b-lib_counter">
							<span class="b-lib_counter_lb">В избранном</span>
							<span class="icofav">х 15 678</span>
						</div>
						</div>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#"  class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							
						</div>
					</div>
				</div>
				<div class="b-result-docitem">
					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_4.jpg"></a>
						
					</div>
					<div class="iblock b-result-docinfo">
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
						</ul>
						<div class="b-collstat">
						<div class="b-lib_counter">
							<span class="b-lib_counter_lb">Читатели</span>
							<span class="icouser">х 108</span>
						</div>
						<div class="b-lib_counter">
							<span class="b-lib_counter_lb">Просмотры</span>
							<span class="icoviews">х 15 678</span>
						</div>
						<div class="b-lib_counter">
							<span class="b-lib_counter_lb">В избранном</span>
							<span class="icofav">х 15 678</span>
						</div>
						</div>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> <a href="#"  class="b-sorcelibrary">Рязанская областная библиотека</a></div>
						</div>
					</div>
					<div class="b-collaction clearfix">
						<a href="ajax_fake_book_remove.php" class="right button_red popup_opener ajax_opener" data-width="330" data-height="125">Убрать из коллекции</a>
						<a href="#" class="b-editcollection">Сделать обложкой коллекции</a>
					</div>

				</div>
				
			</div><!-- /.b-result-doc -->

	</div><!-- /.b-mainblock -->
	<div class="b-side right">
		<div class="b-sidenav">
			<a href="#" class="b-sidenav_title">Авторы</a>
			
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">Лем С.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">40</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Големский Т. В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">23</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лемин В.В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">2</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лиманн А.Н.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">3</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Голем Л. К.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">15</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>				
					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">дата</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">2000 - 2050</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1950 - 2000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1900 - 1950</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1850 - 1900</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1800 - 1850</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1750 - 1800</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1700 - 1750</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1650 - 1700</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1600 - 1650</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1550 - 1600</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1400 - 1550</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1350 - 1400</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1300 - 1350</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1350 - 1300</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1200 - 1350</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1150 - 1200</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1100 - 1150</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1050 - 1100</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">1000 - 1050</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix hidden">
						<div class="b-sidenav_value left">950 - 1000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li><a href="#" class="b_sidenav_contmore js_moreopen">+ следующие</a></li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">тематика</a>
			<div class="b-sidenav_cont">	
				
				<div class="themesearch">
					<input type="text" class="b-sidenav_tb" placeholder="Поиск по тематике">
					<a href="#" class="b-sidenav_srch">Расширеный список</a>
				</div>
				<ul class="b-sidenav_cont_list b-sidenav_cont_listmore">
					<li class="clearfix">					
						<div class="b-sidenav_value left">Горное дело </div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Пищевые производства</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Радиоэлектроника </div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Строительство </div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Техника и технические 
							науки в целом </div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>
						</li>

						<li><a href="ajax_fake_theme.html" class="b_sidenav_contmore js_ajax_theme" >+ следующие</a></li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">Библиотеки</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">Библиотеки</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
				<a href="#" class="b-sidenav_title">издательство</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">издательство</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
			</div> <!-- /.b-sidenav --> 

		</div><!-- /.b-side -->

	</section>

</div><!-- /.homepage -->

	<? include("footer.php"); ?>
	<!--popup добавить в подборки-->
<!--<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu js_openmenu">мои подборки</a>
	
	<ul class="b-selectionlist">
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb8">	<label for="cb8" class="black">Любимые авторы</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb9">	<label for="cb9" class="black">Научно-популярная фантастика</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb10"><label for="cb10" class="black">Ракеты и люди</label>

		</li>	
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb14"><label for="cb14" class="lite">Отметить как прочитанное</label>

		</li>
		<li><a href="#" class="b-selection_add"><span>+</span>Cоздать подборку</a></li>
	</ul>
</div>--><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>