<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * Open graph prefix
 */

global $APPLICATION;

$APPLICATION->AddBufferContent(
    function () use ($APPLICATION) {
        $sResult  = 'og: http://ogp.me/ns#';
        $sType    = $APPLICATION->GetPageProperty('og:type');
        $sResult .= $sType ? " $sType: http://ogp.me/ns/$sType#" : '';
        $sResult = ' prefix="'.$sResult.'"';
        return $sResult;
    }
);