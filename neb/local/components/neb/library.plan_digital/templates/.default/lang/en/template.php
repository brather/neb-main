<?
$MESS['LIBRARY_PLAN_DIGITAL_IN_LIST'] = "Books in the digitizing list";
$MESS['LIBRARY_PLAN_DIGITAL_PRINT'] = "Print";
$MESS['LIBRARY_PLAN_DIGITAL_EXPORT_EXCEL'] = "Export into excel";
$MESS['LIBRARY_PLAN_DIGITAL_ADD_BOOKS'] = "Add books";
$MESS['LIBRARY_PLAN_DIGITAL_AUTHOR'] = "Author";
$MESS['LIBRARY_PLAN_DIGITAL_NAMEDIG'] = "Title / Description / Added to the digitizing list or not";
$MESS['LIBRARY_PLAN_DIGITAL_DATEDIG'] = "Digitize <br/>up to the date";
$MESS['LIBRARY_PLAN_DIGITAL_COMMENT'] = "Commentary";
$MESS['LIBRARY_PLAN_DIGITAL_REMOVE_FROM_PLAN'] = "Remove from the Digitizing List";
$MESS['LIBRARY_PLAN_DIGITAL_DESCRIPTION'] = "Description";
$MESS['LIBRARY_PLAN_DIGITAL_IN_DIG'] = "In the digitizing list";
$MESS['LIBRARY_PLAN_DIGITAL_REMOVE'] = "Removing request";
$MESS['LIBRARY_PLAN_DIGITAL_FROM_PLAN'] = "from the Digitizing List";
$MESS['LIBRARY_PLAN_DIGITAL_HEADING'] = "from the Digitizing List";
$MESS['LIBRARY_PLAN_DIGITAL_OUTPUT'] = "Given data";
$MESS['LIBRARY_PLAN_DIGITAL_PHYS_DESC'] = "Физическое описание";
$MESS['LIBRARY_PLAN_DIGITAL_LIB'] = "Libraries";
$MESS['LIBRARY_PLAN_DIGITAL_WHO_DIG'] = "Оцифровывает";
?>
