<?
namespace Bitrix\NotaExt;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class NotaextTable
 *
 * Fields:
 * <ul>
 * <li> EXT_ID string(255) mandatory
 * <li> INSTALL string(1) mandatory
 * <li> TIMESTAMP_X datetime mandatory default 'CURRENT_TIMESTAMP'
 * </ul>
 *
 * @package Bitrix\Ext
 **/
class NotaExtTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'n_ext_notaext';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('NOTAEXT_ENTITY_ID_FIELD'),
            ),
            'EXT_ID' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateExtId'),
                'title' => Loc::getMessage('NOTAEXT_ENTITY_EXT_ID_FIELD'),
            ),
            'INSTALL' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateInstall'),
                'title' => Loc::getMessage('NOTAEXT_ENTITY_INSTALL_FIELD'),
            ),
            'TIMESTAMP_X' => array(
                'data_type' => 'datetime',
                'required' => false,
                'title' => Loc::getMessage('NOTAEXT_ENTITY_TIMESTAMP_X_FIELD'),
            ),
        );
    }

    public static function validateExtId()
    {
        return array(
            new Entity\Validator\Length(null, 255),
        );
    }

    public static function validateInstall()
    {
        return array(
            new Entity\Validator\Length(null, 1),
        );
    }
}
