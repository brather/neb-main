<?
$MESS['CONDITIONSINNDER_LICENSE_AGREEMENT'] = "       ЛИЦЕНЗИОННЫЙ ДОГОВОР ";
$MESS['CONDITIONSINNDER_CITY_YEAR'] = "г. Москва                                                                                                                                             2014 г.";
$MESS['CONDITIONSINNDER_IN_AGREEMENT'] = "_____________________________________, именуемый в дальнейшем \"Лицензиар\", с одной стороны, и федеральное государственное бюджетное учреждение «Российская государственная библиотека», являющееся юридическим лицом по законодательству Российской Федерации, именуемое в дальнейшем «Лицензиат», в лице Генерального директора Вислого А.И., действующего на основании Устава, с другой Стороны, вместе именуемые \"Стороны\", заключили настоящий договор о нижеследующем.";
$MESS['CONDITIONSINNDER_GRANT_OF_LICENSE'] = "1. ПРЕДМЕТ ДОГОВОРА";
$MESS['CONDITIONSINNDER_GRANT_OF_LICENSE_1_1'] = "1.1. Лицензиар предоставляет Лицензиату в порядке и на условиях, изложенных в настоящем Договоре, право использования нижеследующих Произведений.";
$MESS['CONDITIONSINNDER_GRANT_OF_LICENSE_1'] = "1. _______";
$MESS['CONDITIONSINNDER_GRANT_OF_LICENSE_2'] = "2. _______ ";
$MESS['CONDITIONSINNDER_VARIANT'] = "Вариант: ";
$MESS['CONDITIONSINNDER_VARIANT_TEXT'] = "«…,указанных в Приложении N 1 к настоящему Договору». ";
$MESS['CONDITIONSINNDER_GRANT_OF_LICENSE_1_2'] = "1.2. Предоставление Лицензиату права использования указанных в договоре Произведений осуществляется на условиях простой (неисключительной) лицензии. За Лицензиаром сохраняется право выдачи лицензий другим лицам; ";
$MESS['CONDITIONSINNDER_TRANSFERRED_RIGHTS'] = "2. ПЕРЕДАВАЕМЫЕ ПРАВА";
$MESS['CONDITIONSINNDER_TRANSFERRED_RIGHTS_1'] = "2.1. Основные условия предоставления лицензии на использование Произведений: ";
$MESS['CONDITIONSINNDER_TRANSFERRED_RIGHTS_1_1'] = "Лицензиар предоставляет Лицензиату право использовать произведения всеми разрешенными способами, в том числе, но не ограничиваясь:  ";
$MESS['CONDITIONSINNDER_TRANSFERRED_RIGHTS_1_2'] = "- воспроизведение (без ограничения тиража), включая запись в цифровой форме, то есть изготовление одного и более экземпляра произведения или его части в любой материальной форме, в том числе в форме звуко- или видеозаписи, изготовление в трех измерениях одного и более экземпляра двухмерного произведения и в двух измерениях одного и более экземпляра трехмерного произведения. При этом запись произведения на электронном носителе, в том числе запись в память ЭВМ, также считается воспроизведением; ";
$MESS['CONDITIONSINNDER_TRANSFERRED_RIGHTS_1_3'] = "- распространение, публичный показ, публичное исполнение, импорт, прокат, сообщение в эфир, сообщение по кабелю, перевод и иную переработку, практическую реализацию; ";
$MESS['CONDITIONSINNDER_TRANSFERRED_RIGHTS_1_4'] = "- доведение до всеобщего сведения, включая использование в открытых и закрытых сетях, включение в базы данных и мультимедийную продукцию, регистрацию товарных знаков.     ";
$MESS['CONDITIONSINNDER_TRANSFERRED_RIGHTS_2'] = "2.2. Лицензиар разрешает осуществить обнародование Произведений любым способом по усмотрению Лицензиата. Лицензиар вправе указывать имя Лицензиата при использовании Произведений. Лицензиар разрешает также осуществлять использование Произведений без указания его имени по усмотрению Лицензиата. ";
$MESS['CONDITIONSINNDER_TRANSFERRED_RIGHTS_3'] = "2.3. Лицензиар гарантирует, что заключение настоящего Договора не приведет к нарушению авторских прав или иных прав интеллектуальной собственности третьих лиц, а также что им не заключались и не будут заключаться в дальнейшем какие-либо договоры, а также что им не заключались и не будут заключаться в дальнейшем какие-либо договоры, противоречащие настоящему Договору или делающие невозможным его выполнение. ";
$MESS['CONDITIONSINNDER_TRANSFERRED_RIGHTS_4'] = "2.4. Лицензиату предоставляется право предоставлять третьим лицам права на использование Произведений в пределах прав и способов использования, предусмотренных настоящим Договором, т.е. согласие на заключение сублицензионных договоров. Такое право предоставляется с даты заключения настоящего Договора и не требует заключения дополнительных соглашений и выдачи дополнительных разрешений. ";
$MESS['CONDITIONSINNDER_TERRITORY_AND_TERMS'] = "3. ТЕРРИТОРИЯ И СРОКИ ";
$MESS['CONDITIONSINNDER_TERRITORY_AND_TERMS_1'] = "3.1. Права на использование Произведений способами, указанными в п. 2.1 настоящего Договора, передаются Лицензиаром Лицензиату для использования на территории всего мира. ";
$MESS['CONDITIONSINNDER_TERRITORY_AND_TERMS_2'] = "3.2. Права на использование Произведений способами, указанными в п. 2.1 настоящего Договора, передаются Лицензиаром Лицензиату на срок действия исключительных прав на Произведения. ";
$MESS['CONDITIONSINNDER_TERRITORY_AND_TERMS_3'] = "3.3. Лицензиар может использовать исключительные права на Произведения; при этом предоставит третьим лицам права на использование Произведений способами, указанными в п. 2.1 настоящего Договора, не ранее чем через пять лет после заключения настоящего Договора. ";
$MESS['CONDITIONSINNDER_REWARD'] = "4. ВОЗНАГРАЖДЕНИЕ";
$MESS['CONDITIONSINNDER_REWARD_1'] = "4.1. Права, указанные в п. 2.1., предоставляются Лицензиату на безвозмездной основе, вознаграждение не выплачивается. ";
$MESS['CONDITIONSINNDER_LIABILITY'] = "5. ОТВЕТСТВЕННОСТЬ СТОРОН";
$MESS['CONDITIONSINNDER_LIABILITY_1'] = "5.1. За неисполнение или ненадлежащее исполнение Сторонами обязательств, принятых на себя в соответствии с настоящим Договором, Стороны несут ответственность в соответствии с действующим российским законодательством и настоящим Договором. ";
$MESS['CONDITIONSINNDER_LIABILITY_2'] = "5.2. Сторона, не исполнившая или ненадлежащим образом исполнившая обязательства по настоящему договору, обязана возместить другой стороне причиненные таким неисполнением убытки. ";
$MESS['CONDITIONSINNDER_LIABILITY_3'] = "5.3.Стороны освобождаются от ответственности за неисполнение или ненадлежащее исполнение своих обязательств по настоящему Договору в случае действия обстоятельств непреодолимой силы, прямо или косвенно препятствующих исполнению настоящего Договора, то есть таких обстоятельств, которые не зависят от воли Сторон, не могли быть ими предвидены в момент заключения Договора и предотвращены разумными средствами при их наступлении (форс-мажор). ";
$MESS['CONDITIONSINNDER_CONFLICT_RESOLUTION'] = "6. РАЗРЕШЕНИЕ СПОРОВ";
$MESS['CONDITIONSINNDER_CONFLICT_RESOLUTION_1'] = "6.1. Все споры и разногласия Сторон, вытекающие из условий настоящего Договора, подлежат урегулированию путем переговоров. В случае их безрезультатности, одна из Сторон обязана направить другой Стороне письменную претензию с изложением своих требований. ";
$MESS['CONDITIONSINNDER_CONFLICT_RESOLUTION_2'] = "6.2. Сторона, получившая такую претензию, обязана в течение 15 (пятнадцати) рабочих дней дать на нее мотивированный ответ. В случае неполучения в указанный срок ответа на претензию, а также если споры и разногласия не будут сняты своевременно поступившим ответом на претензию, указанные споры и разногласия подлежат разрешению в суде по месту нахождения Лицензиата в соответствии с действующим законодательством РФ. ";
$MESS['CONDITIONSINNDER_DISSOLUTION'] = "7. РАСТОРЖЕНИЕ ДОГОВОРА";
$MESS['CONDITIONSINNDER_DISSOLUTION_1'] = "7.1.Стороны вправе досрочно расторгнуть настоящий договор по взаимному письменному согласию.        ";
$MESS['CONDITIONSINNDER_DISSOLUTION_2'] = "7.2. Договор может быть расторгнут Лицензиаром в одностороннем порядке в случае: ";
$MESS['CONDITIONSINNDER_DISSOLUTION_3'] = "- использования Лицензиатом Произведений способом, не предусмотренным п. 2.1. настоящего Договора.    ";
$MESS['CONDITIONSINNDER_ADDITIONAL_TERMS'] = "8. ДОПОЛНИТЕЛЬНЫЕ УСЛОВИЯ И ЗАКЛЮЧИТЕЛЬНЫЕ ПОЛОЖЕНИЯ";
$MESS['CONDITIONSINNDER_ADDITIONAL_TERMS_1'] = "8.1. Любые изменения и дополнения к настоящему договору действительны при условии, если они совершены в письменной форме и подписаны сторонами или надлежаще уполномоченными на то представителями сторон. ";
$MESS['CONDITIONSINNDER_ADDITIONAL_TERMS_2'] = "8.2. Стороны обязуются письменно извещать друг друга об изменении своих реквизитов в течение пяти рабочих дней с даты возникновения таких изменений. ";
$MESS['CONDITIONSINNDER_ADDITIONAL_TERMS_4'] = "8.4. Все уведомления и сообщения в рамках настоящего договора должны направляться сторонами друг другу в письменной форме. ";
$MESS['CONDITIONSINNDER_ADDITIONAL_TERMS_5'] = "8.5. Настоящий договор составлен в двух экземплярах, имеющих одинаковую юридическую силу, из которых один находится у Лицензиара, второй - у Лицензиата.       ";
$MESS['CONDITIONSINNDER_ADDRESSES_REQUISITES'] = "9. АДРЕСА И ПЛАТЕЖНЫЕ РЕКВИЗИТЫ СТОРОН";
$MESS['CONDITIONSINNDER_LICENSOR'] = "Лицензиар";
$MESS['CONDITIONSINNDER_ADDRESS'] = "Адрес";
$MESS['CONDITIONSINNDER_PASSPORT'] = "Паспорт";
$MESS['CONDITIONSINNDER_TELEPHONE'] = "Тел.";
$MESS['CONDITIONSINNDER_TIN'] = "ИНН";
$MESS['CONDITIONSINNDER_INSURANCE'] = "Страховое свидетельство";
$MESS['CONDITIONSINNDER_LICENSEE'] = "Лицензиат";
$MESS['CONDITIONSINNDER_SIGNATURES'] = "ПОДПИСИ СТОРОН";
$MESS['CONDITIONSINNDER_LICENSEE_NAME'] = "Вислый А.И.";
$MESS['CONDITIONSINNDER_STAMP_PLACE'] = "м.п.";
