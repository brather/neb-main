<?php

namespace AssessorBundle\Entity;

use AssessorBundle\Traits\AssessorEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as Serialization;

/**
 * NebQuerySubstitution
 *
 * @ORM\Table(name="neb_query_substitution", uniqueConstraints={@ORM\UniqueConstraint(name="UK_neb_query_substitution_OriginalQuery", columns={"OriginalQuery"})})
 * @ORM\Entity(repositoryClass="AssessorBundle\Entity\Repository\CommonRepository")
 * @UniqueEntity(fields="originalQuery", message="Такой запрос уже существует")
 */
class NebQuerySubstitution implements AssessorRatingEntityInterface
{
    use AssessorEntityTrait;
    /**
     * @var integer
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serialization\Groups({"list", "details"})
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="OriginalQuery", type="string", length=333, nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    public $originalQuery;

    /**
     * @var string
     *
     * @ORM\Column(name="ModifedQuery", type="string", length=4095, nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    public $modifiedQuery;

    /**
     * @var integer
     *
     * @ORM\Column(name="Priority", type="integer", nullable=true)
     * @Serialization\Groups({"list", "details"})
     */
    public $priority;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOriginalQuery()
    {
        return $this->originalQuery;
    }

    /**
     * @param string $originalQuery
     */
    public function setOriginalQuery($originalQuery)
    {
        $this->originalQuery = $originalQuery;
    }

    /**
     * @return string
     */
    public function getModifiedQuery()
    {
        return $this->modifiedQuery;
    }

    /**
     * @param string $modifiedQuery
     */
    public function setModifiedQuery($modifiedQuery)
    {
        $this->modifiedQuery = $modifiedQuery;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }
}
