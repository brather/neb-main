<?php
$MESS['REG_TITLE'] = 'Registration on the Portal';
$MESS['REG_SELECT'] = 'Select the method of registration';
$MESS['REG_SIMPLE'] = 'Simplified registration';
$MESS['REG_FULL'] = 'Full Registration';
$MESS['REG_RGB'] = 'Registration for the readers RGB';
$MESS['REG_ESIA'] = 'Register with ESIA';
$MESS['REG_STANDART'] = 'Standart Registration';
$MESS['REG_IN_LIBRARY'] = 'Registration in a library-partner NEB';
$MESS['REG_NEXT'] = 'Next';
