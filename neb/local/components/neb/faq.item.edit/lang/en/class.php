<?php
$MESS["ERROR_MIN_COUNT_SYMBOL"] = "Error! You have entered too few characters.";
$MESS["ERROR_EMPTY_STRING"] = "Error! You did not fill one of the fields.";
$MESS["ERROR"] = "Error!";
$MESS["ERROR_ITEM_EXIST"] = "Error! This element exist. Please, enter another element name.";
$MESS["EDIT_SUCCESS_1"] = "Edit element with id=<b>";
$MESS["EDIT_SUCCESS_2"] = "</b> was successfully!";
$MESS["ITEM_DELETE_SUCCESS_1"] = "Success delete item with id=<b>";
$MESS["ITEM_DELETE_SUCCESS_2"] = "</b>!";
$MESS["ITEM_DELETE_ERROR_1"] = "Error! Item with id=<b>";
$MESS["ITEM_DELETE_ERROR_2"] = "</b> removal has taken place.";
$MESS["ITEM_DELETE_ERROR_NOT_PERMISSION"] = "Error! Item removal has taken place. Not permission.";
$MESS["ITEM_NOT_FOUND_1"] = "Item with id=<b>";
$MESS["ITEM_NOT_FOUND_2"] = "</b> not found.";
$MESS["ERROR_DETAIL_TEXT_EMPTY"] = "Error! Please, enter detail text.";