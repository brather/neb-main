<?php

/**
 * Получение информацииции о книгах (в шапку сайта и мобильные приложения)
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Bitrix\Main\Config\Option,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient;

if (!Loader::includeModule('nota.exalead') || !Loader::includeModule('iblock'))
	return false;

$arResult = array();

//количество электронных изданий и каталогов
$arResult['BOOK_COUNT']         = Option::get('nota.exalead', 'book_count_total');
$arResult['BOOK_COUNT_CATALOG'] = Option::get('nota.exalead', 'book_count_catalog');

if (!isset($_REQUEST['without_news'])) {

	// новые поступления
	$arParams['q'] = 'filesize>0';
	$query = new SearchQuery($arParams['q']);
	$query->setPageLimit(1);
	$query->setParam('s', 'desc(document_dateindex)');
	$query->setParam('sl', 'sl_statistic3');
	$query->setPageLimit(20);

	$client = new SearchClient();
	$arResult['NEW_BOOKS'] = $client->getResult($query);

	$arCode = [];
	$rsItems = CIBlockElement::GetList(
		array('SORT' => 'DESC'),
		array(
			'IBLOCK_CODE'          => 'popular.books',
			'!PROPERTY_EXALEAD_ID' => false
		),
		false,
		array('nTopCount' => 5),
		array(
			'ID',
			'IBLOCK_ID',
			'PROPERTY_EXALEAD_ID'
		)
	);
	while ($arItems = $rsItems->Fetch()) {
		$arCode[] = $arItems['PROPERTY_EXALEAD_ID_VALUE'];
	}

	$query = new SearchQuery();
	$query->getByArIds($arCode);
	$query->setPageLimit(5);

	$client = new SearchClient();
	$arResult['POPULAR_BOOKS'] = $client->getResult($query);
}

MainHelper::showJson($arResult);