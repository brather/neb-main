<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arResult */
?>
<p>Читатель зарегистрирован в НЭБ:</p>
- <a href="/profile/readers/edit/<?= $arResult['userData']['ID'] ?>/">Настройка профиля читателя</a><br>
- <a href="/profile/readers/">Список читателей НЭБ</a>
