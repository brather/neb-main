<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
$curPage = $APPLICATION->GetCurPage();
?>
<div class="container-fluid">
    <div class="row">
        <?$arCountOnPage = array(10, 15, 30, 60, 100);?>
        <?if($arResult["COUNT_ITEMS"] > $arCountOnPage[0]) {?>
            <div class="container-fluid">
                <form action="<?= $APPLICATION->GetCurPage() ?>" method="get">
                    <?= $arResult['HTML_INPUT_HIDDEN'] ?>
                    <?if($arResult["COUNT_ITEMS"] > $arCountOnPage[0]){?>
                        <div class="form-group">
                            <div class="row text-right">
                                <label for="count_on_page" class="col-xs-8"><?= Loc::getMessage("LIB_ADD_PLAN_FROM_READER_COUNT_ITEMS_ON_PAGE") ?></label>
                                <div class="col-xs-4 text-left">
                                    <div class="row">
                                        <select class="form-control" name="count_on_page" id="pl-count">
                                            <?foreach($arCountOnPage as $vCount){?>
                                                <?if($arResult["COUNT_ITEMS"] > $vCount) {?>
                                                    <option value="<?= $vCount ?>"<?if($arRequest["count_on_page"] == $vCount){?> selected<?}?>>
                                                        <?= $vCount ?>
                                                    </option>
                                                <?}?>
                                            <?}?>
                                            <option value="all"<?if($arRequest["count_on_page"] == 'all'){?> selected<?}?>><?= Loc::getMessage('LIB_ADD_PLAN_FROM_READER_SHOW_ALL') ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?}?>
                    <div class="form-group">
                        <div class="row text-right">
                            <button class="btn btn-primary"><?= Loc::getMessage("LIB_ADD_PLAN_FROM_READER_APPLY") ?></button>
                        </div>
                    </div>
                </form>
            </div>
        <?}?>
    </div>
    <?if($arResult['COUNT_ITEMS'] > 1){?>
        <div class="row text-right">
            <div class="btn-group js-btn-group">
                <label class="btn btn-default js-all"
                       data-plan-id="<?= $arRequest['plan_id'] ?>"
                       data-ssid="<?= bitrix_sessid() ?>"
                       data-date="<?= $arRequest['date'] ?>"
                       data-label="<?= Loc::getMessage('LIB_ADD_PLAN_FROM_READER_REMOVE') ?>"
                       data-status="<?= $arResult['ID_STATUS_ADD'] ?>"
                       data-action="plus">
                    <input type="radio" name="options">
                    <?= Loc::getMessage('LIB_ADD_PLAN_FROM_READER_SELECT_ALL_ADD') ?>
                </label>
                <label class="btn btn-default js-all"
                       data-plan-id="<?= $arRequest['plan_id'] ?>"
                       data-ssid="<?= bitrix_sessid() ?>"
                       data-date="<?= $arRequest['date'] ?>"
                       data-label="<?= Loc::getMessage('LIB_ADD_PLAN_FROM_READER_ADD') ?>"
                       data-status="<?= $arResult['ID_STATUS_REMOVE'] ?>"
                       data-action="minus">
                    <input type="radio" name="options">
                    <?= Loc::getMessage('LIB_ADD_PLAN_FROM_READER_SELECT_ALL_REMOVE') ?>
                </label>
            </div>
        </div>
        <div class="row"><hr></div>
    <?}?>
    <div class="row">
        <table class="table table-bordered table-d-map-list" id="list-books">
            <thead>
            <tr>
                <?foreach($arResult['SORT'] as $iSorp => $arSort) {?>
                    <th>
                        <a href="<?= $curPage . $arSort['HREF'] ?>"><?= $arSort['NAME'] ?></a>
                    </th>
                <?}?>
                <th><?= Loc::getMessage("LIB_ADD_PLAN_FROM_READER_ADD_TO_PLAN") ?></th>
            </tr>
            </thead>
            <?$stIndex = 1;
            if($arRequest['page'] > 1) {
                $stIndex = ($arRequest['page'] - 1) * $arResult['COUNT_ON_PAGE'] + 1;
            }?>
            <?foreach($arResult['LIST_ITEMS'] as $iItem => $arItem) {?>
                <?$author = '';
                $title = '';
                if(!empty($arItem['UF_BOOK_NAME'])){
                    $title = $arItem['UF_BOOK_NAME'];
                } elseif($arResult['LIST_BOOKS'][$arItem['UF_EXALEAD_BOOK']]['UF_BOOK_NAME']) {
                    $title = $arResult['LIST_BOOKS'][$arItem['UF_EXALEAD_BOOK']]['UF_BOOK_NAME'];
                } else {
                    $title = $arItem['UF_EXALEAD_BOOK'];
                }
                if(!empty($arItem['UF_BOOK_AUTHOR'])){
                    $author = $arItem['UF_BOOK_AUTHOR'];
                } elseif($arResult['LIST_BOOKS'][$arItem['UF_EXALEAD_BOOK']]['UF_BOOK_AUTHOR']) {
                    //$author = $arResult['LIST_BOOKS'][$arItem['UF_EXALEAD_BOOK']]['UF_BOOK_AUTHOR'];
                } else {
                    //$author = $arItem['UF_EXALEAD_BOOK'];
                }?>
                <tr>
                    <td>
                        <a class="d-map-list-info"
                           href="#"
                           title="id=<?= $arItem["ID"] ?>"
                           onclick="event.preventDefault();">
                            <?= $stIndex ?>
                        </a>
                    </td>
                    <td>
                        <?= $title ?>
                        <?if(!empty($arItem['UF_COMMENT'])) {?>
                            <br>
                            <a href="#" class="js-next">
                                <?= Loc::getMessage("LIB_ADD_PLAN_FROM_READER_SHOW_COMMENT") ?>
                            </a>
                        <?}?>
                    </td>
                    <td><?= $author ?></td>
                    <td><?= $arItem["UF_DATE_ADD"] ?></td>
                    <td><?= $arItem["UF_DATE_FINISH"] ?></td>
                    <td class="text-center">
                        <?if($arItem["ARR_BTN"]["MESS"] == '') {?>
                            <label for="add-book-<?= $arItem["ID"] ?>">
                                <input type="checkbox"
                                       class="js-checkbox"
                                       id="book-<?= $arItem["ID"] ?>"
                                       data-current="<?= $arItem["ARR_BTN"]["CLASS"] ?>"
                                       data-label-add="<?= $arItem["ARR_BTN"]["LABEL_ADD"] ?>"
                                       data-label-remove="<?= $arItem["ARR_BTN"]["LABEL_REMOVE"] ?>"
                                       name="add-book-<?= $arItem["ID"] ?>"
                                       data-date="<?= $arRequest['date'] ?>"
                                       data-plan-id="<?= $arRequest['plan_id'] ?>"
                                       data-exalead-id="<?= trim($arItem["UF_EXALEAD_BOOK"]) ?>"
                                       data-status-add="<?= $arItem["ARR_BTN"]["ID_STATUS_ACTION_ADD"] ?>"
                                       data-status-remove="<?= $arItem["ARR_BTN"]["ID_STATUS_ACTION_REMOVE"] ?>"
                                       data-ssid="<?= bitrix_sessid() ?>"
                                    <?= $arItem["ARR_BTN"]["CHECKED"] ?>>
                                <p class="lbl">
                                    <?if($arItem["ARR_BTN"]["CLASS"] == 'minus'){?>
                                        <?= $arItem["ARR_BTN"]["LABEL_REMOVE"] ?>
                                    <?} else {?>
                                        <?= $arItem["ARR_BTN"]["LABEL_ADD"] ?>
                                    <?}?>
                                </p>
                            </label>
                        <?} else {?>
                            <?= $arItem["ARR_BTN"]["MESS"] ?>
                        <?}?>
                    </td>
                </tr>
                <?if(!empty($arItem['UF_COMMENT'])) {?>
                    <tr style="display: none">
                        <td></td>
                        <td colspan="5"><small><?= $arItem['UF_COMMENT'] ?></small></td>
                    </tr>
                <?}?>
                <?$stIndex++;?>
            <?}?>
        </table>
        <?= $arResult["PAGINATION"] ?>
    </div>
</div>