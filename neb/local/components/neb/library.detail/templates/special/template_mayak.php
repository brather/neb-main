<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__DIR__.'/template.php');
?>

<section class="innersection innerwrapper searchempty clearfix">
	<div data-template="<?=$APPLICATION->ShowProperty('view_template_index');?>"></div>
	<div data-theme="<?=$APPLICATION->ShowProperty('view_theme_index');?>"></div>

	<div class="menu__wrap">
		<div class="menu">
			<div class="prim-nav cf">
				<ul>
					<li class="active"><a href="/library/central-city-public-library-named-v-v-mayakovsky/">О библиотеке</a></li>
					<li><a href="/library/central-city-public-library-named-v-v-mayakovsky/funds/">Фонды</a></li>
					<li><a href="/library/central-city-public-library-named-v-v-mayakovsky/collections/">Коллекции</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="slider-container">
		<div class="slider-wrapper">
			<div class="slider-outer">
				<div class="slider cf js-slider">
					<div class="s__item">
						<div class="s__item__text-wrap">
							<div class="s__item__text">Открытие фотовыставки В.Мизина "По островам Финского залива"</div>
							<div class="s__item__date">с 26 декабря 2014 по 26 января 2015</div>
							<?//<a class="s__item__button" href="#">Записаться</a>?>
						</div>
						<img src="/local/templates/rnb/uploads/slider/1.jpg" alt=""/>
					</div>

					<div class="s__item">
						<div class="s__item__text-wrap">
							<div class="s__item__text">Литературная игра «А.С. Грибоедов и его время» в формате «Игры разума»</div>
							<div class="s__item__date">23 января 2015 г. в 18.30</div>
							<?//<a class="s__item__button" href="#">Записаться</a>?>
						</div>
						<img src="/local/templates/rnb/uploads/slider/1.jpg" alt=""/>
					</div>

					<?/*<div class="s__item">
						<div class="s__item__text-wrap">
							<div class="s__item__text">
								День открытых дверей в
								публичной библиотеке имени В.В. Маяковского
							</div>
							<div class="s__item__date">с 2 октября 2014 до 5 ноября 2014</div>
							<?//<a class="s__item__button" href="#">Записаться</a>?>
						</div>
						<img src="/local/templates/rnb/uploads/slider/1.jpg" alt=""/>
					</div>*/?>
				</div>
			</div>
		</div>
	</div><?/* class="slider-container" */?>

	<div class="main">
		<div class="content main-news__wrap cf">
			<div class="contacts">
				<span class="contacts__title">Добро пожаловать в библиотеку.</span>
				<div class="address">наб. р. Фонтанки, 44, <br>Санкт-Петербург, Россия</div>
				<div class="work-time__wrap">
					<span class="work-time">Часы работы:</span>
					<span class="work-time">Пн - Сб - с 11 до 20</span>
					<span class="work-time">Воскресенье - с 11 до 18</span>
					<span class="work-time">Последний четверг каждого месяца – санитарный день</span>
				</div>
				<a class="contacts__link" href="#map">Карта проезда</a>
				<a class="contacts__link">Получить абонемент</a>
			</div>

			<div class="main-news">
				<img src="/local/templates/rnb/uploads/main-news__photo.jpg"/>
				<div class="main-news__photo-filter"></div>
				<div class="main-news__title">Мастеркласс</div>
				<div class="main-news__content">
					<span class="news__text main-news__text">Мастер-класс по вокалу. Ведущий – Арсений Фоканов. (6+) <br/>
					    <a class="main-news__link">Посетить</a>
						<span class="date main-news__date">13 января 2015 (вторник)</span>
					</span>
				</div>
			</div>
		</div>

		<div class="news cf">
			<div class="news__item">
				<div class="new__item-header">
					<a class="news__item__link">
						<div class="news__item-photo">
							<img src="/local/templates/rnb/uploads/news-photo-1.jpg" alt=""/>
						</div>
						<span>Чемпионы русской поэзии</span>
					</a>
					<?/*<span class="date">10.12.2014</span>*/?>
				</div>
				<span class="news__text">Репортаж с Санкт-Петербургского чемпионата поэзии автор Екатерина Преснякова звукорежиссер Александр Самаров СПБГУКиТ</span>
			</div>

			<div class="news__item">
				<div class="new__item-header">
					<a class="news__item__link">
						<div class="news__item-photo">
							<img src="/local/templates/rnb/uploads/news-photo-2.jpg" alt=""/>
						</div>
						<span>Городской круглый стол</span>
					</a>
					<?/*<span class="date">08.12.2014</span>*/?>
				</div>
				<span class="news__text">«Формирование информационного общества в Санкт-Петербурге: вчера, сегодня, завтра»</span>
			</div>

			<div class="news__item">
				<div class="new__item-header">
					<a class="news__item__link">
						<div class="news__item-photo">
							<img src="/local/templates/rnb/uploads/news-photo-1.jpg" alt=""/>
						</div>
						<span>Курс изучения турецкого языка</span>
					</a>
					<?/*<span class="date">26.11.2014</span>*/?>
				</div>
				<span class="news__text">C 12 января 2015 года в библиотеке стартует трехмесячный курс изучения турецкого языка "Турецкий для начинающих</span>
			</div>
		</div><?/* class="news cf" */?>

		<?/* ABOUT LIBRARY
			<div class="b-lib_descr">
			<div class="iblock b-lib_fulldescr">
			<div class="b-libstatus"><?=$arResult['ITEM']['PROPERTY_STATUS_VALUE']?></div>
			<h2><?=$arResult['ITEM']['NAME']?></h2>
			<a href="<?=$arResult['ITEM']['PROPERTY_URL_VALUE']?>" class="b-libsite"
                target="_blank"><?=$component->getLinkView($arResult['ITEM']['PROPERTY_URL_VALUE'])?></a>
			<div class="b-rolledtext" data-height="300">
			<div class="b-rolled_in ani_all_500_in">
			<p><?=htmlspecialcharsBack(TxtToHTML($arResult['ITEM']['~PREVIEW_TEXT']))?></p>
			</div>
			<a href="#" class="slidetext">Читать далее</a>
			</div>
			</div>
		</div>*/?>

		<div class="content cf">
			<span class="marker_lib hidden">
				<?/*<a href="#" onclick="return false;" class="b-elar_name_txt"><?=$arResult['ITEM']['NAME']?></a><br />*/?>
				<span class="b-elar_status"><?=$arResult['ITEM']['PROPERTY_STATUS_VALUE']?></span><br />
				<span class="b-map_elar_info">
					<?if(!empty($arResult['ITEM']['PROPERTY_ADDRESS_VALUE'])){?>
						<span class="b-map_elar_infoitem"><span><?=Loc::getMessage('LIBRARY_DETAIL_ADDRESS_MAP');?>:</span><?=$arResult['ITEM']['PROPERTY_ADDRESS_VALUE']?></span><br />
						<?}?>
					<?if(!empty($arResult['ITEM']['PROPERTY_SCHEDULE_VALUE'])){?>
						<span class="b-map_elar_infoitem"><span><?=Loc::getMessage('LIBRARY_DETAIL_WORK');?>:</span><?=$arResult['ITEM']['PROPERTY_SCHEDULE_VALUE']?></span>
						<?}?>
				</span>
				<span class="b-mapcard_act clearfix">
					<span class="right neb b-mapcard_status"><?=Loc::getMessage('LIBRARY_DETAIL_USER');?><img class="uchastnik_neb" src="/local/templates/rnb/images/uchastnik_neb.jpg"></span>
				</span>
			</span>
			<div class="map">
				<div id="map"></div>
				<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
				<script type="text/javascript">
					ymaps.ready(init); /* on MAP READY
					var cont = $(this);
					var bal = cont.parent().find('.marker_lib.hidden');*/
					var infobox = $('.marker_lib.hidden').html();

					function init () {
						var myMap = new ymaps.Map('map', {
							center: [<?=$arResult['ITEM']['MAP'][0]?>, <?=$arResult['ITEM']['MAP'][1]?>],
							zoom: 12,
							/*type: 'yandex#publicMap',*/
							behaviors: ["drag", "dblClickZoom"],
							controls: [
								"fullscreenControl",
								/*"geolocationControl",
								"routeEditor",
								"rulerControl",
								"searchControl",
								"trafficControl",*/
								"typeSelector",
								"zoomControl",
							]
						});
						ymaps.option.presetStorage.add('neb', {
							iconLayout:"default#imageWithContent",
							iconImageHref: '/local/templates/rnb/images/map-marker.png',
							iconImageSize: [42, 58],
							iconImageOffset: [-21, -58],
							iconContentOffset:[-21, 0]
						});

						var startPlaceMark = new ymaps.Placemark(
							[<?=$arResult['ITEM']['MAP'][0]?>, <?=$arResult['ITEM']['MAP'][1]?>], {
								balloonContentHeader: '<?=$arResult["ITEM"]["NAME"]?>',
								balloonContent: '<span class="marker_lib here">'+infobox+'</span>',
								hintContent: '<?=$arResult["ITEM"]["NAME"]?>'
							}
						);
						startPlaceMark.options.set("preset", "neb");
						myMap.geoObjects.add(startPlaceMark);
						startPlaceMark.balloon.open();
					}
				</script>
			</div><?// class="map" ?>
		</div><?/* class="content cf" MAP & POPULAR BOOKS */?>
		<div style="margin-top: 102px; padding-left: 15px;">
			<?$APPLICATION->IncludeComponent(
					"neb:books.popular",
					"",
					#"rnb",
					Array(
						"LIBRARY_ID" => $arResult['ITEM']["ID"],
						"ITEM_COUNT" => 8,
						"CACHE_TIME" => $arParams["CACHE_TIME"],
					),
					$component
				);?>
		</div>
		<div class="catalog cf">


			<?/*
				<div class="catalog__item">
				<a class="catalog__item__link" href="#">
				<div class="catalog__item__image">
				<img src="/local/templates/rnb/uploads/catalog-photo-1.jpg">
				</div>
				<span class="">Jamie's Comfort Food</span>
				</a>
				<span class="catalog__item-description">Автор:
				<a class="catalog__item-author" href="#">Джейми Оливер</a>
				</span>
				</div>
			*/?>

			<?/*
				<div class="pagination">
				<a class="pagination-item pagination-item_prev" href="#"></a>
				<a class="pagination-item pagination-item_active" href="#">1</a>
				<a class="pagination-item" href="#">2</a>
				<a class="pagination-item" href="#">3</a>
				<a class="pagination-item" href="#">4</a>
				<a class="pagination-item" href="#">5</a>
				<a class="pagination-item pagination-item_next" href="#"></a>
				</div>
			*/?>
		</div>
	</div>

	<script type="text/javascript" src="/local/templates/rnb/js/common.js"></script>

	<link rel="stylesheet" type="text/css" href="/local/templates/rnb/css/style_mayak.css"/>
	<script type="text/javascript" src="/local/templates/rnb/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript">
		document.documentElement.className += " js";
		document.documentElement.className = document.documentElement.className.replace( /(?:^|\s)no-js(?!\S)/ , '' );
	</script>
</section>