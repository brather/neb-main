
<?
$pagename='';
?>
<? include("header_main_auth.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb bbox b-text" id="" value="Станислав Лем">							
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>	
						
					</div>
					<div class="b_search_set clearfix">
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
		

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-searchresult noborder">
		<ul class="b-profile_nav">
			<li>
				<a href="#" class="b-profile_navlk js_profilemenu ">личный кабинет</a>
				<ul class="b-profile_subnav">
					<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
					<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
				</ul>
			</li>
			<li><a class="b-profile_nav_lb current" href="#">моя библиотека</a></li>
			<li><a href="#" class="b-profile_nav_qt">цитаты</a></li>
			<li><a href="#" class="b-profile_nav_bm ">закладки</a></li>
			<li><a href="#" class="b-profile_nav_notes">заметки</a></li>
		</ul>                 
	</div><!-- /.b-searchresult-->
	<div class="b-razdel">
			<select class="js_select">
				<option value="-1">Выберите раздел</option>
				<option value="user_profile_new.php">личный кабинет</option>
				<option value="user_profile_favorites.php" selected="selected">моя библиотека</option>
				<option value="user_profile_lk_quo.php">цитаты</option>
				<option value="user_profile_lk_bookmark.php">закладки</option>
				<option value="user_profile_lk_notes.php">заметки</option>
				<option value="user_profile_lk_searchresult.php">поисковые запросы</option>
			</select>
		</div>
	<div class="b-mainblock left">
		
		<div class="b-filter js_filter">
			<div class="b-filter_wrapper">
				<a href="#" class="sort sort_opener">Сортировать</a>
				<span class="sort_wrap">
				<a href="#" class="sort up">По автору</a>
				<a href="#" class="sort">По названию</a>
				<a href="#" class="sort">По дате</a>
				</span>
				<span class="b-filter_act">
					<a href="#" title="Отобразить все книги плиткой" class="b-filter_items"></a>
					<a href="#" title="Отобразить все книги списком" class="b-filter_list current"></a>
					<span class="b-filter_show">Показать</span>
					<span class="b-filter_num current">10</span>
					<span class="b-filter_num">25</span>
					<span class="b-filter_num">50</span>
				</span>
			</div>
		</div><!-- /.b-filter -->
		<a href="#" class="set_opener iblock right">Настройки</a>

		<div class="b-filter_items_wrapper">
			<div class="b-result-doc b-result-docfavorite js_sortable ">
				<div class="b-result-docitem iblock removeitem">


					<div class="b-result-docinfo">
						<span class="num">1.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" href="#"></a>
						</div> <!-- meta -->
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>

								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_4.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							<div class="b-result_selection">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>


							</div><!-- /.b-result_selection  -->
							<div class="b-result-type ">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">2.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>								
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_27.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							<div class="b-result_selection">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
									
								</div>
								

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type ">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">3.</span>	
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_29.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">футурологический конгресс</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							<div class="b-result_selection ">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type ">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">4.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_30.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							<div class="b-result_selection ">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type ">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">5.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_30.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">Сумма технологии</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							<div class="b-result_selection">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type ">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->		
				<div class="b-result-docitem iblock removeitem">
					
					
					<div class="b-result-docinfo">
						<span class="num">6.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<div class="b-result-docphoto">
							<div class="iblock b-fav_info b-fav_info_mode">
								<span class="b-fav_info_quote">х 2</span>
								<span class="b-fav_info_note">х 1</span>
							</div>
							<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_29.jpg"></a>
							<div class="b-loadprogress">
								<div class="b-loadlabel"></div>
								<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
							</div>						
						</div>
						<h2><a href="#">футурологический конгресс</a></h2>
						<a href="#" class="b-book_autor">Станислав Лем</a>
						<div class="b-result_sorce clearfix">
							<div class="b-result_selection ">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type ">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->	

			</div><!-- /.b-result-doc -->

		</div><!-- /.b-filter_items_wrapper -->



		<div class="b-filter_list_wrapper">
			<div class="b-result-doc b-result-docfavorite">
				<div class="b-result-docitem removeitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_4.jpg"></a>
						<div class="b-loadprogress">
							<div class="b-loadlabel"></div>
							<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
						</div>						
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">1.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>						
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений</p>					

						<div class="b-result_sorce clearfix">
							<div class="b-result_selection left">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								<div class="iblock b-fav_info">
									<span class="b-fav_info_quote">х 2</span>
									<span class="b-fav_info_bmark">х 1</span>
									<span class="b-fav_info_note">х 1</span>
								</div>

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type right">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem removeitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_27.jpg"></a>		
						<div class="b-loadprogress">
							<div class="b-loadlabel"></div>
							<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="65">
						</div>				
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">2.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>						
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений</p>					

						<div class="b-result_sorce clearfix">
							<div class="b-result_selection left">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								<div class="iblock b-fav_info">
									<span class="b-fav_info_quote">х 2</span>
									<span class="b-fav_info_note">х 1</span>
								</div>

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type right">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem removeitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_29.jpg"></a>			
						<div class="b-loadprogress">
							<div class="b-loadlabel"></div>
							<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="12">
						</div>			
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">3.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>						
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений</p>					

						<div class="b-result_sorce clearfix">
							<div class="b-result_selection left">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								<div class="iblock b-fav_info">
									<span class="b-fav_info_quote">х 2</span>
									<span class="b-fav_info_note">х 1</span>
								</div>

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type right">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem removeitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_30.jpg"></a>		
						<div class="b-loadprogress">
							<div class="b-loadlabel"></div>
							<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="53">
						</div>				
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">4.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>						
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений</p>					

						<div class="b-result_sorce clearfix">
							<div class="b-result_selection left">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								<div class="iblock b-fav_info">
									<span class="b-fav_info_quote">х 2</span>
									<span class="b-fav_info_note">х 1</span>
								</div>

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type right">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->
				<div class="b-result-docitem removeitem">

					<div class="iblock b-result-docphoto">
						<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_4.jpg"></a>	
						<div class="b-loadprogress">
							<div class="b-loadlabel"></div>
							<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="38">
						</div>				
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">5.</span>
						<div class="meta minus">
							<div class="b-hint rel">Удалить из Моей библиотеки</div>
							<a class="b-bookadd fav" data-remove="removeonly" data-collection="ajax_favs.html" href="#"></a>
						</div> <!-- meta -->
						<h2><a href="#">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#">Станислав Лем</a></li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>						
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений</p>					
						<div class="b-result_sorce clearfix">
							<div class="b-result_selection left ">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-favs="ajax_favs.html">мои подборки</a>
									<div class="b-favs">
										
										<form class="b-selectionadd" action="">
											<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
											<span class="b-selectionaddtxt">cоздать подборку</span>
											<input type="text" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								<div class="iblock b-fav_info">
									<span class="b-fav_info_quote">х 2</span>
									<span class="b-fav_info_note">х 1</span>
								</div>

							</div><!-- /.b-result_selection  -->
							<div class="b-result-type right">
								<span class="b-result-type_txt">pdf</span>
								<span class="b-result-type_txt">текст</span>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>

				</div><!-- /.b-result-docitem -->			

			</div><!-- /.b-result-doc -->

		</div>
		
		<div class="b-paging">
			<div class="b-paging_cnt">
				<a href="" class="b-paging_prev iblock"></a>
				<a href="" class="b-paging_num current iblock">1</a>
				<a href="" class="b-paging_num iblock">2</a>
				<a href="" class="b-paging_num iblock">3</a>
				<a href="" class="b-paging_num iblock">4</a>
				<a href="" class="b-paging_next iblock"></a>
			</div>
		</div><!-- /.b-paging -->
	</div><!-- /.b-mainblock -->

	<div class="b-side right">
		<div class="b-libfilter">
			<h4>Моя библиотека</h4>
			<!--<div class="b-libfilter_item current"><span class="b-libfilter_num">53</span><a href="#" class="b-libfilter_name">Все книги</a></div>
			<div class="b-libfilter_item "><span class="b-libfilter_num">1</span><a href="#" class="b-libfilter_name">Сейчас читаю</a></div>
			<div class="b-libfilter_item "><span class="b-libfilter_num">21</span><a href="#" class="b-libfilter_name">Прочитал</a></div>-->
			<div class="js_sortable">
				<div class="b-libfilter_item b-libfilter_useritem">
					<span class="b-libfilter_num">1</span>
					<div class="checkwrapper">
						<a href="#" class="black b-libfilter_name">Любимые авторы</a><input class="checkbox" type="checkbox" name="" id="cb11">
					</div>			
				</div>
				<div class="b-libfilter_item current b-libfilter_useritem b-libfilterrow">
					<span class="b-libfilter_num">2</span>
					<div class="checkwrapper">
						<a href="#" class="black b-libfilter_name ">Научно-популярная фантастика</a><input class="checkbox" type="checkbox" name="" id="cb21">
					</div>			
				</div>
				<div class="b-libfilter_item  b-libfilter_useritem">
					<span class="b-libfilter_num">1</span>
					<div class="checkwrapper">
						<a href="#" class="black b-libfilter_name">Ракеты и люди</a><input class="checkbox" type="checkbox" name="" id="cb31">
					</div>			
				</div>
			</div>
			<div class="b-libfilter_action clearfix">
				<a href="#" class="b-libfilter_remove"></a>
				<form action="" class="b-selectionadd">
					<input type="submit" value="+" data-collection="ajax_favs.html" class="b-selectionaddsign">
					<span class="b-selectionaddtxt ">cоздать подборку</span>
					<input type="text" class="input hidden">
				</form>
			</div>
			<div class="b-removepopup" ><p>Вы действительно хотите удалить коллекцию?</p><a class="formbutton btremove" href="#">Удалить</a><a class="formbutton gray" href="#">Оставить</a></div>
			
		</div><!-- /.b-side_libfilter -->
		
		<div class="b-sidenav">
			<a href="#" class="b-sidenav_title">Авторы</a>
			
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">Лем С.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">40</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Големский Т. В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">23</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лемин В.В.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">2</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Лиманн А.Н.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">3</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">Голем Л. К.</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">15</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>				
					</li>
				</ul>
			</div>
			<a href="#" class="b-sidenav_title">тематика</a>
			<div class="b-sidenav_cont">	
				<ul class="b-sidenav_cont_list">
					<li class="clearfix">
						<div class="b-sidenav_value left">1950 - 2000</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">21</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>
					</li>
					<li class="clearfix">
						<div class="b-sidenav_value left">1900 - 1950</div>
						<div class="checkwrapper type2 right">
							<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
						</div>

					</li>
					<li><a href="#" class="b_sidenav_contmore">+ следующие</a></li>
				</ul>
			</div>
		</div> <!-- /.b-sidenav -->

	</div><!-- /.b-side -->
	
</section>

</div><!-- /.homepage -->

<div class="b-mylibrary b-mylibrary_section">
	<div class="wrapper">
		<div class="b-libsection"><a href="#">Личный кабинет</a></div>
		<div class="b-mylibselect rel">
			<a href="#" class="js_openmenu">Подборки</a>
			<ul class="b-mylibselect_list">
				<li><a href="#">Любимые авторы</a></li>
				<li><a href="#">Научно-популярная фантастика</a></li>
				<li><a href="#">Ракеты и люди</a></li>
			</ul>
		</div>
		<ul class="b-mylibrarylist">
			<li>
				<a href="#">Сейчас  читаю</a> <span class="b-num">2</span>
			</li>			
			<li><a href="#">Буду читать</a><span class="b-num">47</span></li>
			<li><a href="#">Просмотрено сегодня</a><span class="b-num">14</span></li>
		</ul>
	</div>
</div><!-- /.b-mylibrary -->
<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu">мои подборки</a>
	

	<form class="b-selectionadd" action="">
		<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
		<span class="b-selectionaddtxt">Cоздать подборку</span>
		<input type="text" class="input hidden">
	</form>
</div><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
<!--popup Удалить из подборки--><div class="b-removepopup hidden"><p>Удалить книгу из Моей библиотеки?</p><a href="#" class="formbutton btremove">Удалить</a><a href="#" class="formbutton gray">Оставить</a></div><!--/popup Удалить из подборки-->

</body>
</html>