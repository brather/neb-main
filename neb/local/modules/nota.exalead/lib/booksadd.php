<?

namespace Nota\Exalead;

use \Bitrix\Main\Config\Option,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\Main\Entity,
    Nota\Journal\J;

Loader::includeModule('highloadblock');
Loader::includeModule('nota.journal');

/**
 * Добавление книги в в таблицу для последующей отправки в экзалид
 * 
 * Class BooksAdd
 * @package Nota\Exalead
 */
class BooksAdd
{
    /**
     * Добавляем книгу
     *
     * ###Описания полей таблицы tbl_common_biblio_card###
     *
     * @param $arFields array
     *                  Id автоматически генерируется
     *                  IdFromALIS id АБИС, нужно присвоить уникальное значение в НЭБе
     *                  `Library` Id библиотеки из справочника
     *                  `Author` Автор
     *                  `Name` Название издания
     *                  `SubName` Подзаголовок
     *                  `PublishYear` Год издания,
     *                  `PublishPlace` Место издания,
     *                  `Publisher` Издатель,
     *                  `ISBN`,
     *                  `Language` язык,
     *                  `CountPages`количество страниц
     *                  `Format` формат
     *                  `Series` серия
     *                  `VolumeNumber` номер тома
     *                  `VolumeName` название тома
     *                  `Collection` Коллекция
     *                  `AccessType` тип доступа
     *                  `isLicenseAgreement` наличие лицензионного соглашения
     *                  `Responsibility` Ответственность
     *                  `Annotation` аннотация,
     *                  `ContentRemark` Примечание на содержание,
     *                  `CommonRemark` Примечание
     *                  `PublicationInformation` информация о публикации,
     *                  `pdfLink` Ссылка на pdf-файл,
     *                  `CreationDateTime` дата создания
     *                  `UpdatingDateTime` дата изменения
     *                  `BBKText` шифры ББК
     *                  `UDKText` шифры УДК,
     *                  `LanguageText` язык издания
     *
     * Дополнительные параметры
     * BX_TIMESTAMP - дата изменения\добавления
     * BX_IS_UPLOAD - признак, выгружена ли книга, если книга измененв, то надо установить в null
     * BX_ALIS - ID книги в справочнике библиотек
     * BX_UID - ID пользователя который добавил книгу
     * BX_LIB_ID - ID Библиотеки из ИБ для которой была добавлена книга
     * BX_DELETE - признак удаления \ 1 - удалено
     *
     * @return bool
     */
    public static function add($arFields)
    {

        if (empty($arFields)) {
            return false;
        }

        if ($arFields['PDF_LINK']) {
            $arFields['PDF_LINK'] = self::getPdfLink(
                $arFields['PDF_LINK'],
                $arFields['Id'],
                $arFields['LIB_ID']
            );
        }
        $libId = null;
        if (!empty($arFields['LIB_ID'])) {
            $libId = $arFields['LIB_ID'];
        }

        return LibraryBiblioCardTable::setPdfLink(
            $arFields['PDF_LINK'],
            $arFields['Id'],
            $libId
        );
    }

    public static function getPdfLink($pdfLink, $id, $libId = null)
    {
        $SAVE_DIR = Option::get("nota.exalead", "book_add_file_path");
        $DIR_FOR_EXALEAD = Option::get("nota.exalead", "book_add_file_path_2");
        $copyFileName = $SAVE_DIR . $libId . "/" . str_replace(
                array(" ", ".", "-", "("), array("_", "", "_", ""), $id
            ) . ".pdf";
        if (CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . $pdfLink, $copyFileName)
            === true
        ) {
            DeleteDirFilesEx(GetDirPath($pdfLink));
            $pdfLink = str_replace($SAVE_DIR, $DIR_FOR_EXALEAD, $copyFileName);
        }

        return $pdfLink;
    }

    /**
     * @param string $pdfLink
     * @param string $id
     * @param null   $libId
     *
     * @return mixed|null
     */
    public static function buildPdfLink($pdfLink, $id, $libId = null)
    {
        $saveDirectory = Option::get('nota.exalead','book_add_file_path');
        $exaleadDirectory = Option::get('nota.exalead','book_add_file_path_2');
        $copyFileName = $saveDirectory . $libId . '/'
            . str_replace(
                array(' ', '.', '-', '('),
                array('_', '', '_', ''),
                $id
            )
            . '.pdf';
        if (CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . $pdfLink, $copyFileName)
            === true
        ) {
            return str_replace(
                $saveDirectory, $exaleadDirectory, $copyFileName
            );
        }

        return null;
    }

    public static function addPublication($arFields, $libId)
    {

        if (empty($arFields) or empty($arFields['ALIS']) or empty($libId)) {
            return false;
        }

        if (empty($arFields['IdFromALIS'])) {
            self::setIdFromALIS($arFields, $libId);
            if (empty($arFields['IdFromALIS'])) {
                return false;
            }
        };


        global $DB, $USER;
        $strSql = 'select * from tbl_common_biblio_card where IdFromALIS = "'
            . $arFields['IdFromALIS'] . '"';
        $res = $DB->Query($strSql, false, $err_mess . __LINE__);
        if ($arRes = $res->Fetch()) {
            return $arRes['Id'];
        }

        $arFields['Language'] = 170;
        $arFields['AccessType'] = 1;
        $arFields['LanguageText'] = 'rus';
        $arFields['BX_ALIS'] = $arFields['ALIS'];
        $arFields['BX_UID'] = $USER->GetID();

        if ($libId != LIB_ID_RIGHTHOLDER) {
            J::add(
                'rightholder', 'add',
                Array('BOOK_ID' => $arFields['IdFromALIS'])
            );
        } else {
            J::add(
                'book', 'add', Array('BOOK_ID' => $arFields['IdFromALIS'],
                                     'AUTHOR'  => $arFields['Author'],
                                     'NAME'    => $arFields['Name'],
                                     'YEAR'    => $arFields['PublishYear'],
                                     'LIB'     => $libId,
                                     'LANG'    => $arFields['LanguageText'],
                                     'BBK'     => $arFields['BBKText'])
            );
        }

        foreach ($arFields as &$Field) {
            $Field = '"' . $DB->ForSql($Field) . '"';
        }

        return $DB->Insert(
            "tbl_common_biblio_card", $arFields, $err_mess . __LINE__
        );
    }

    public static function edit($bookId, $arFields)
    {
        if (intval($bookId) <= 0 || empty($arFields)) {
            return false;
        }

        if (empty($arFields['pdfLink']) or !file_exists(
                $_SERVER['DOCUMENT_ROOT'] . $arFields['pdfLink']
            )
        ) {
            return false;
        }

        $copyFileName = "/upload/books_pdf/" . pathinfo(
                $_SERVER["DOCUMENT_ROOT"] . $arFields['pdfLink'],
                PATHINFO_BASENAME
            );
        if ($arFields['pdfLink'] != $copyFileName) {
            if (CopyDirFiles(
                $_SERVER["DOCUMENT_ROOT"] . $arFields['pdfLink'],
                $_SERVER["DOCUMENT_ROOT"] . $copyFileName
            )) {
                $arFields['pdfLink'] = $copyFileName;
            } else {
                return false;
            }
        }

        $arFields['BX_IS_UPLOAD'] = '0';

        if ($arFields['ALIS'] == LIB_ID_RIGHTHOLDER) {
            J::add('rightholder', 'edit', Array('BOOK_ID' => $bookId));
        } else {
            J::add(
                'book', 'edit',
                Array('BOOK_ID' => $bookId, 'AUTHOR' => $arFields['Author'],
                      'NAME'    => $arFields['Name'],
                      'YEAR'    => $arFields['PublishYear'],
                      'LIB'     => $arFields['ALIS'],
                      'LANG'    => $arFields['LanguageText'],
                      'BBK'     => $arFields['BBKText'])
            );
        }

        global $DB;
        foreach ($arFields as &$Field) {
            $Field = '"' . $DB->ForSql($Field) . '"';
        }

        return $DB->Update(
            'tbl_common_biblio_card', $arFields, "WHERE ID='" . $bookId . "'",
            $err_mess . __LINE__
        );
    }

    /**
     * Генерим поле IdFromALIS
     *
     * @param $arFields
     * @param $libId
     */
    public static function setIdFromALIS(&$arFields, $libId)
    {
        $balnk = '000000';
        $IdFromALIS = substr_replace(
            $balnk, $libId, strlen($balnk) - strlen($libId)
        );
        $IdFromALIS .= '_';
        $IdFromALIS .= substr_replace(
            $balnk, $arFields['ALIS'],
            strlen($balnk) - strlen($arFields['ALIS'])
        );
        $IdFromALIS .= '_';
        $IdFromALIS .= substr(
            md5(
                $arFields['Author'] . $arFields['Name']
                . $arFields['PublishYear']
            ), 0, 10
        );

        $arFields['IdFromALIS'] = $IdFromALIS;
    }

    /**
     * Проверяем есть ли данная баблиотека, добавляем если нет
     *
     * @param        $id
     * @param string $name
     *
     * @return bool|string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function setLibrary($id, $name = '')
    {
        if (intval($id) <= 0) {
            return false;
        }

        global $DB;

        $hlblock = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

        $rsData = $entity_data_class::getList(
            array(
                "select" => array("ID", "UF_ID", "UF_NAME"),
                "filter" => array('UF_ID' => (int)$id),
            )
        );

        if ($arData = $rsData->fetch()) {
            $name = $arData['UF_NAME'];
        }

        $strSql = 'select * from tbl_libraries_alis where Library = ' . $id;
        $res = $DB->Query($strSql, false, $err_mess . __LINE__);
        if ($arRes = $res->Fetch()) {
            return $arRes['Id'];
        } else {
            return $DB->Insert(
                "tbl_libraries_alis",
                array('Name'    => "'" . trim($DB->ForSql($name)) . "'",
                      'Library' => $id), $err_mess . __LINE__
            );
        }
    }

    /**
     * Тоже что и setLibrary только добавляется в базу экзалида
     * Не работает!
     *
     * @todo Для добавления ALIS нужно нужно еще поле type с ним надо что то решить
     *
     * @param        $libraryId
     * @param string $libraryName
     *
     * @return int|null
     * @throws \Exception
     */
    public static function applyLibraryAlis($libraryId, $libraryName = '')
    {
        $alisRow = BiblioAlisTable::getRow(
            array(
                'filter' => array(
                    'Library' => (integer)$libraryId,
                ),
                'select' => array(
                    'Id',
                )
            )
        );
        $alisId = null;
        if (null === $alisRow) {
            $libraryRow = \nebLibrary::forgeCatalogEntity()
                ->getRow(
                    array(
                        'filter' => array(
                            'UF_ID' => (integer)$libraryId,
                        ),
                        'select' => array(
                            'UF_NAME'
                        )
                    )
                );
            if (isset($libraryRow['UF_NAME'])) {
                $libraryName = $libraryRow['UF_NAME'];
            }
            $addResult = BiblioAlisTable::add(
                array(
                    'Library' => $libraryId,
                    'Name'    => $libraryName,
                )
            );
            if ($addResult->isSuccess()) {
                $alisId = $addResult->getId();
            }
        } else {
            $alisId = $alisRow['Id'];
        }

        return $alisId;
    }

    /**
     * Получить список книг для текущего пользователя
     *
     * @param        $arrFilter
     * @param array  $arSelect
     * @param string $by
     * @param string $order
     *
     * @return array|bool
     */
    public static function getListBooksCurrentUser($arrFilter, $arSelect = [], $by = 'BX_TIMESTAMP', $order = 'DESC' ) {
        global $USER, $DB;
        $arFilter = array(
            'and BX_UID = ' . $USER->GetID(),
            'and (BX_DELETE != 1 or BX_DELETE is null)'
        );

        if (!empty($arrFilter)) {
            $arFilter = array_merge($arFilter, $arrFilter);
        }
        if (empty($arSelect)) {
            $arSelect = array(
                'Id', 'Name', 'Author', 'BBKText', 'PublishYear', $DB->DateToCharFunction("BX_TIMESTAMP", 'SHORT')
                . ' BX_TIMESTAMP_', 'pdfLink', 'toDel', 'ISBN', 'FullSymbolicId', 'status'
            );
        }

        return self::getListBooks($arFilter, $arSelect, $by, $order);
    }

    public static function getListBooksLibrary($libraryId, $arrFilter,  $arSelect = [], $by = 'BX_TIMESTAMP', $order = 'DESC') {
        global $USER, $DB;
        $arFilter = array(
            'and BX_LIB_ID = ' . $libraryId,
            'and (BX_DELETE != 1 or BX_DELETE is null)'
        );

        if (!empty($arrFilter)) {
            $arFilter = array_merge($arFilter, $arrFilter);
        }

        if (empty($arSelect)) {
            $arSelect = array(
                'Id', 'Author', 'Name', 'SubName', 'PublishYear', 'Publisher',
                'PublishPlace', 'ISBN', 'Printing', 'CreationDateTime',
                'Annotation', 'Series', 'Format',
                'VolumeNumber', 'VolumeName', 'ContentRemark', 'CommonRemark',
                'pdfLink', 'CreationDateTime', 'BBKText', 'UDKText',
                $DB->DateToCharFunction("BX_TIMESTAMP", 'SHORT')
                . ' BX_TIMESTAMP_',
                'pdfLink'
            );
        }

        return self::getListBooks($arFilter, $arSelect, $by, $order);
    }

    /**
     * @param        $arFilter
     * @param array  $arSelect
     * @param string $by
     * @param string $order
     *
     * @return array|bool
     */
    public static function getListBooks($arFilter, $arSelect = array('*'), $by = 'BX_TIMESTAMP', $order = 'DESC') {
        if (empty($arFilter)) {
            return false;
        }

        global $DB;

        $strSql = 'select ' . implode(",", $arSelect)
            . ' from tbl_common_biblio_card where 1=1 ' . implode(
                " ", $arFilter
            ) . ' ORDER BY ' . $by . ' ' . $order;
        #pre($strSql,1);
        $res = $DB->Query($strSql, false, $err_mess . __LINE__);
        $arResult = array();
        while ($arRes = $res->Fetch()) {
            $arResult[] = $arRes;
        }

        return $arResult;
    }

    /**
     * @method remove
     * @param  int $id
     *
     * @return bool
     */
    public static function remove($id)
    {
        if ((int)$id <= 0) {
            return false;
        }

        global $DB, $USER;

        $strSql
            = 'select Id, Author, Name, PublishYear, ALIS, LanguageText, BBKText from tbl_common_biblio_card where BX_UID='
            . $USER->GetID() . ' and Id = ' . (int)$id;
        $res = $DB->Query($strSql, false, $err_mess . __LINE__);
        if ($arRes = $res->Fetch()) {

            if ($arRes['ALIS'] == LIB_ID_RIGHTHOLDER) {
                J::add(
                    'rightholder', 'delete', Array('BOOK_ID' => $arRes['Id'])
                );
            } else {
                J::add(
                    'book', 'delete', Array('BOOK_ID' => $arRes['Id'],
                                            'AUTHOR'  => $arRes['Author'],
                                            'NAME'    => $arRes['Name'],
                                            'YEAR'    => $arRes['PublishYear'],
                                            'LIB'     => $arRes['ALIS'],
                                            'LANG'    => $arRes['LanguageText'],
                                            'BBK'     => $arRes['BBKText'])
                );
            }

            $arFields = array(
                'BX_DELETE'    => 1,
                'BX_IS_UPLOAD' => 'null'
            );
            $DB->Update(
                'tbl_common_biblio_card', $arFields,
                'where Id = ' . $arRes['Id']
            );
        }
    }


    /**
     * @param array $fields
     * @param array $library
     *
     * @return Entity\AddResult|Entity\UpdateResult
     * @throws \Exception
     */
    public static function applyAddBookRequest($fields, $library)
    {
        $booksAdd = new static();
        $fields['ALIS'] = $booksAdd->setLibrary(
            $library['LIBRARY_ID'],
            $library['LIBRARY_NAME']
        );

        BooksAdd::setIdFromALIS($fields, $library['LIBRARY_ID']);

        $fields['BX_UID'] = \nebUser::getCurrent()->GetID();

        $request = null;
        if (isset($fields['Id'])) {
            $request['Id'] = $fields['Id'];
        } elseif (isset($fields['IdFromALIS'])) {
            $request = BiblioCardTable::getRow(
                array(
                    'filter' => array(
                        'IdFromALIS' => $fields['IdFromALIS']
                    ),
                    'select' => array(
                        'Id',
                    )
                )
            );
        }
        if (null !== $request) {
            return BiblioCardTable::update($request['Id'], $fields);
        }

        return BiblioCardTable::add($fields);
    }
}