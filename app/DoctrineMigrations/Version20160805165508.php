<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use LbcBundle\Traits\LbcMigrationsTrait;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160805165508 extends AbstractMigration implements ContainerAwareInterface
{
    use LbcMigrationsTrait;

    /**
     * @var Container
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        error_reporting(E_ERROR);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '1024M');

        $em = $this->container->get('doctrine')->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $this->addSql('DROP TABLE IF EXISTS `lbc_locationplansectionimages`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_locationplanimages`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_locationplansections_closure`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_locationplansections`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_locationplans_closure`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_locationplans`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_maindivisionintroductionimages`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_maindivisionintroduction`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_maindivisionimages`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_maindivision_closure`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_maindivision`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_specialdivisionsectionimages`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_specialdivisionsections_closure`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_specialdivisionsections`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_specialdivisionimages`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_specialdivision_closure`;');
        $this->addSql('DROP TABLE IF EXISTS `lbc_specialdivision`;');


        $this->addSql('CREATE TABLE `lbc_locationplanimages` (
  `Id` INTEGER NOT NULL AUTO_INCREMENT,
  `LocationPlanId` INTEGER NOT NULL,
  `ImagePath` TEXT NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_locationplans` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `MainDivisionId` INTEGER NOT NULL,
  `ParentId` INTEGER,
  `Level` INTEGER NOT NULL,
  `Name` TEXT NOT NULL,
  `Comments` TEXT,
  `Keywords` TEXT,
  `Links` TEXT,
  `Rec_id` INTEGER,
  `Gr` INTEGER,
  `GrErr` VARCHAR(255),
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_locationplansectionimages` (
  `Id` INTEGER NOT NULL AUTO_INCREMENT,
  `LocationPlanSectionId` INTEGER NOT NULL,
  `ImagePath` TEXT NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_locationplansections` (
  `Id` INTEGER NOT NULL,
  `LocationPlanId` INTEGER NOT NULL,
  `ParentId` INTEGER,
  `Level` INTEGER NOT NULL,
  `Code` TEXT NOT NULL,
  `Code_sort` TEXT,
  `Name` TEXT NOT NULL,
  `Comments` TEXT,
  `Keywords` TEXT,
  `Links` TEXT,
  `Rec_id` INTEGER,
  `Gr` INTEGER,
  `GrErr` VARCHAR(255),
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_maindivision` (
  `Id` INTEGER NOT NULL,
  `ParentId` INTEGER,
  `Level` INTEGER NOT NULL,
  `Code` VARCHAR(255) NOT NULL,
  `Code_sort` VARCHAR(255),
  `Name` TEXT NOT NULL,
  `Comments` TEXT,
  `Keywords` TEXT,
  `Links` TEXT,
  `IsCommon` INTEGER NOT NULL,
  `Rec_id` INTEGER,
  `Gr` INTEGER,
  `GrErr` VARCHAR(255),
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_maindivisionimages` (
  `Id` INTEGER NOT NULL AUTO_INCREMENT,
  `MainDivisionId` INTEGER NOT NULL,
  `ImagePath` TEXT NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_maindivisionintroduction` (
  `Id` INTEGER NOT NULL,
  `MainDivisionId` INTEGER NOT NULL,
  `TextPath` TEXT NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_maindivisionintroductionimages` (
  `Id` INTEGER NOT NULL AUTO_INCREMENT,
  `MainDivisionIntroductionId` INTEGER NOT NULL,
  `ImagePath` TEXT NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_specialdivision` (
  `Id` INTEGER NOT NULL,
  `MainDivisionId` INTEGER NOT NULL,
  `ParentId` INTEGER,
  `Level` INTEGER NOT NULL,
  `Name` TEXT NOT NULL,
  `Comments` TEXT,
  `Keywords` TEXT,
  `Links` TEXT,
  `Rec_id` INTEGER,
  `Gr` INTEGER,
  `GrErr` VARCHAR(255),
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_specialdivisionimages` (
  `Id` INTEGER NOT NULL AUTO_INCREMENT,
  `SpecialDivisionId` INTEGER NOT NULL,
  `ImagePath` TEXT NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_specialdivisionsectionimages` (
  `Id` INTEGER NOT NULL AUTO_INCREMENT,
  `SpecialDivisionSectionId` INTEGER NOT NULL,
  `ImagePath` TEXT NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');
        $this->addSql('CREATE TABLE `lbc_specialdivisionsections` (
  `Id` INTEGER NOT NULL,
  `SpecialDivisionId` INTEGER NOT NULL,
  `ParentId` INTEGER,
  `Level` INTEGER NOT NULL,
  `Code` TEXT NOT NULL,
  `Code_sort` TEXT,
  `Name` TEXT NOT NULL,
  `Comments` TEXT,
  `Keywords` TEXT,
  `Links` TEXT,
  `Rec_id` INTEGER,
  `Gr` INTEGER,
  `GrErr` VARCHAR(255),
  PRIMARY KEY (`Id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;');

        $instance = $this;
        $addSql = function ($handle) use ($instance) {
            while (!feof($handle)) {
                $line = fgets($handle);
                $line = trim($line);
                if (strlen($line) > 30) {
                    $instance->addSql($line);
                }
            }
        };
        $handle = fopen(__DIR__ . '/sql/replaced_bbk_full.sql', 'r');
        $addSql($handle);
        fclose($handle);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
