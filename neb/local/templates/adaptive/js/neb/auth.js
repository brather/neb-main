$(function () {
    $(document).on('click','.socservice-auth-with-alert',function (e) {
        e.preventDefault();
        var that = $(this);

        FRONT.simplePopup(function () {
            var ok = $('<button type="button" class="btn btn-primary">Разрешить</button>');
            var cancel = $('<button type="button" class="btn btn-primary">Отклонить</button>');
            var message = $('<p class="text-info">'
                + 'Нажимая "Разрешить" вы разрешаете приложению и оператору Национальной электронной библиотеки – федеральному государственному бюджетному учреждению «Российская государственная библиотека» использовать ваши данные в соответствии с <a href="/user-agreement/" target="_blank">Пользовательским соглашением.</a>'
                + '</p>');
            var link = that.data('service-popup');

            this.applyHeader();
            this.applyFooter();
            this.popupBody.append(message);
            ok.click(function () {
                BX.util.popup(that.data('service-popup'), 800, 600);
                this.popup.modal('hide');
            }.bind(this));
            cancel.click(function () {
                this.popup.modal('hide');
            }.bind(this));
            this.popupFooter.append(ok);
            this.popupFooter.append(cancel);
        });
    });

    $(document).on('click','[data-sm-confirm]',function(e){
        e.preventDefault();
        var linkEl = $(this),
            uriExtracted = linkEl.data('onclick').slice(15,-12),
            simpleChecked = 'BX.util.popup(\'' == linkEl.data('onclick').substr(0,15);
        if (simpleChecked) {

            var toggler = $(e.target),
                message = {
                    modalSize: 'modal-normal',
                    title: undefined,
                    obj: $('<p>Нажимая "Разрешить" вы разрешаете приложению и оператору Национальной электронной библиотеки – федеральному государственному бюджетному учреждению «Российская государственная библиотека» использовать ваши данные в соответствии с <a href="/user-agreement/" target="_blank">Пользовательским соглашением.</a></p>'),
                    confirmTitle: "Разрешить"
                };
            $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
                if (confirmed) {
                    BX.util.popup(uriExtracted, 800, 600);
                }
            });

        }

    });

});