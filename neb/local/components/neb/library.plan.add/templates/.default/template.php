<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<?
use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;
Loc::loadMessages(__FILE__);
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

$clsCollapse = '';
if($arRequest["DATA_ADD_PLAN"] == "Y") {
    $clsCollapse = ' in';
}

CJSCore::Init(array('date'));

?>
<div class="container-fluid" id="container-for-ajax">
    <div class="row"></div>
</div>
<div id="accordion-add-plan">
    <dl class="panel advanced-list-expandable advanced-list-decorated">
        <dt>
            <a data-toggle="collapse" data-parent="#accordion-add-plan" href="#accordion-add-plan-body">
                <?= Loc::getMessage("T_PLAN_ADD_TITLE") ?>
            </a>
        </dt>
        <div id="accordion-add-plan-body" class="collapse<?= $clsCollapse ?>">
            <dd>
                <div class="container-fluid">
                    <div class="row"><hr></div>
                    <form action="<?= $arParams["SEF_FOLDER"] ?>"
                          data-ajax="/local/tools/plan_digitization/plan.add.ajax.php"
                          data-sef-folder="<?= $arParams["SEF_FOLDER"] ?>"
                          id="DATE_ADD_PLAN"
                          class="form-horizontal"
                          method="post">
                        <input type="hidden" name="DATA_UF_LIBRARY" value="<?= $arResult["UF_LIBRARY"] ?>">
                        <input type="hidden"
                               name="ARR_LANG_INPUT"
                               data-lang-date-create="<?= Loc::getMessage("T_PLAN_ADD_AJAX_DATE_CREATE")?>"
                               data-lang-date-digitization="<?= Loc::getMessage("T_PLAN_ADD_AJAX_TITLE_DATE_F_PLAN") ?>"
                               data-lang-status="<?= Loc::getMessage("T_PLAN_ADD_AJAX_STATUS") ?>"
                               data-lang-change="<?= Loc::getMessage("T_PLAN_ADD_AJAX_CHANGE") ?>"
                               data-lang-delete="<?= Loc::getMessage("T_PLAN_ADD_AJAX_DELETE") ?>"
                               data-lang-delete-confirm="<?= Loc::getMessage("T_PLAN_ADD_AJAX_DELETE_CONFIRM") ?>"
                               data-lang-agreement="<?= Loc::getMessage("T_PLAN_ADD_AJAX_STATUS_AGREEMENT") ?>"
                               data-lang-restatr-page="<?= Loc::getMessage("T_PLAN_ADD_AJAX_RESTART_PAGE") ?>"
                               data-lang-error="<?= Loc::getMessage("T_PLAN_ADD_AJAX_ERROR") ?>">
                        <div class="container-fluid">
                            <input type="hidden" name="DATA_ADD_PLAN" value="Y" />
                            <div class="form-group">
                                <label class="control-label col-xs-6" for="DATA_UF_DATE_F_PLAN"><?= Loc::getMessage("T_PLAN_ADD_DATE_FINISH_PLAN") ?></label>
                                <div class="col-xs-6 form-group">
                                    <div class="input-group">
                                        <input type="text"
                                               name="DATA_UF_DATE_F_PLAN"
                                               class="form-control"
                                               id="DATA_UF_DATE_F_PLAN"
                                               value="<?= $arRequest['DATA_UF_DATE_F_PLAN'] ?>"
                                               data-masked="99.99.9999">
                                            <span class="input-group-btn">
                                                <a class="btn btn-default"
                                                   id="CALENDAR_DATE_F_PLAN"
                                                   onclick="BX.calendar({node: 'DATA_UF_DATE_F_PLAN', field: 'DATA_UF_DATE_F_PLAN',  form: '', bTime: false, value: ''});">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </a>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="container-fluid text-right">
                                    <button class="btn btn-primary" type="submit"><?= Loc::getMessage("T_PLAN_ADD_SUBMIT") ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </dd>
        </div>
    </dl>
</div>
