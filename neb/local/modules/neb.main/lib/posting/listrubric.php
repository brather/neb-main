<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 25.01.2017
 * Time: 18:25
 */

namespace Bitrix\Posting;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ListRubricTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> LID string(2) mandatory
 * <li> CODE string(100) optional
 * <li> NAME string(100) optional
 * <li> DESCRIPTION string optional
 * <li> SORT int optional default 100
 * <li> ACTIVE bool optional default 'Y'
 * <li> AUTO bool optional default 'N'
 * <li> DAYS_OF_MONTH string(100) optional
 * <li> DAYS_OF_WEEK string(15) optional
 * <li> TIMES_OF_DAY string(255) optional
 * <li> TEMPLATE string(100) optional
 * <li> LAST_EXECUTED datetime optional
 * <li> VISIBLE bool optional default 'Y'
 * <li> FROM_FIELD string(255) optional
 * </ul>
 *
 * @package Bitrix\Posting
 **/

class ListRubricTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_list_rubric';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('RUBRIC_ENTITY_ID_FIELD'),
            ),
            'LID' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateLid'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_LID_FIELD'),
            ),
            'CODE' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateCode'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_CODE_FIELD'),
            ),
            'NAME' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateName'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_NAME_FIELD'),
            ),
            'DESCRIPTION' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('RUBRIC_ENTITY_DESCRIPTION_FIELD'),
            ),
            'SORT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RUBRIC_ENTITY_SORT_FIELD'),
            ),
            'ACTIVE' => array(
                'data_type' => 'boolean',
                'values' => array('N', 'Y'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_ACTIVE_FIELD'),
            ),
            'AUTO' => array(
                'data_type' => 'boolean',
                'values' => array('N', 'Y'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_AUTO_FIELD'),
            ),
            'DAYS_OF_MONTH' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateDaysOfMonth'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_DAYS_OF_MONTH_FIELD'),
            ),
            'DAYS_OF_WEEK' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateDaysOfWeek'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_DAYS_OF_WEEK_FIELD'),
            ),
            'TIMES_OF_DAY' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateTimesOfDay'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_TIMES_OF_DAY_FIELD'),
            ),
            'TEMPLATE' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateTemplate'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_TEMPLATE_FIELD'),
            ),
            'LAST_EXECUTED' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('RUBRIC_ENTITY_LAST_EXECUTED_FIELD'),
            ),
            'VISIBLE' => array(
                'data_type' => 'boolean',
                'values' => array('N', 'Y'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_VISIBLE_FIELD'),
            ),
            'FROM_FIELD' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateFromField'),
                'title' => Loc::getMessage('RUBRIC_ENTITY_FROM_FIELD_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for LID field.
     *
     * @return array
     */
    public static function validateLid()
    {
        return array(
            new Main\Entity\Validator\Length(null, 2),
        );
    }
    /**
     * Returns validators for CODE field.
     *
     * @return array
     */
    public static function validateCode()
    {
        return array(
            new Main\Entity\Validator\Length(null, 100),
        );
    }
    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return array(
            new Main\Entity\Validator\Length(null, 100),
        );
    }
    /**
     * Returns validators for DAYS_OF_MONTH field.
     *
     * @return array
     */
    public static function validateDaysOfMonth()
    {
        return array(
            new Main\Entity\Validator\Length(null, 100),
        );
    }
    /**
     * Returns validators for DAYS_OF_WEEK field.
     *
     * @return array
     */
    public static function validateDaysOfWeek()
    {
        return array(
            new Main\Entity\Validator\Length(null, 15),
        );
    }
    /**
     * Returns validators for TIMES_OF_DAY field.
     *
     * @return array
     */
    public static function validateTimesOfDay()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for TEMPLATE field.
     *
     * @return array
     */
    public static function validateTemplate()
    {
        return array(
            new Main\Entity\Validator\Length(null, 100),
        );
    }
    /**
     * Returns validators for FROM_FIELD field.
     *
     * @return array
     */
    public static function validateFromField()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}