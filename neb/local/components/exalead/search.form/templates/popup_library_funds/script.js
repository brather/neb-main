$(function() {
	url = window.location.href;
	if (url.indexOf('/search/') + 1)
	{
		var offset = $("#fixed_form").offset();
		$(window).scroll(function()	{
			if ($(window).scrollTop() >= offset.top-60){
				$("#fixed_form").addClass("b-fixedhead");
				if($('.js_extraform').hasClass("open")){
					$("#fixed_height").css("height","465px");
					$('#search_page_block').css('margin-top', '400px');
				}
				else{
					$("#fixed_height").css("height","204px");
					$('#search_page_block').css('margin-top', '110px');
				}
			}
			else{
				$("#fixed_form").removeClass("b-fixedhead");
				$("#fixed_height").css("height","auto");
				$('#search_page_block').css('margin-top', '10px');
			}

			if($(window).height() < ($('.b-search_ex').height() + $('.b-search_field').height() + 150)){
            	$('.b-search_ex').slideUp();
			}
			
            $('#fixed_height').css('height','auto');


        });
	}
	
	$('#cb38, #cb1').change(function(){
		var checkbox;

		if (this.id == 'cb38'){
			checkbox = 'cb1';
		}else{
			checkbox = 'cb38';
		}
				
		$('#cb38').val('');
		$('#cb1').val('');
		if ($(this).prop("checked") === false){
			$("#"+checkbox).attr("checked","checked");
			$("#"+checkbox).parent('span').parent('.radio').addClass('checked');
			$(this).val('Y');
		}
		else{
			$("#"+checkbox).removeAttr("checked");
			$("#"+checkbox).parent('span').parent('.radio').removeClass('checked');
		}
		$('.search-filter-btn .radio.checked input').val('Y');
	});

	$('.b_exsearch_row').each(function(index){
		$(this).find('.category-opener').data('count', $(this).find('.exsearch-category-list input:checked').length);
		$(this).find('.category-opener-count').text($(this).find('.exsearch-category-list input:checked').length);

		$(this).find('.inpselblock .selopener').eq(1).click(function(event){
			$('.exsearch-category-list').slideUp(100);
		});
	})
	$('.category-opener').each(function(){
		var text = getCategoryText($(this).next().find('input.checkbox:checked').length, $(this).next().find('input.checkbox').length);
		$(this).html(text);
	});
	$('.exsearch-category-list input').change(function(event) {
		var text = getCategoryText($(this).closest('.exsearch-category-list').find('input.checkbox:checked').length, $(this).closest('.exsearch-category-list').find('input.checkbox').length);
		$(this).closest('.exsearch-category-list').prev().html(text);
	});
	$('.exsearch-category-list a').click(function(event) {
		event.preventDefault();
		var text = '';
		var inputCheckboxes = $(this).find('input.checkbox');
		var spanCheckbox = $(this).children('span.checkbox');
		var categoryCount = $(this).parent().prev().data('count');

		if ($(this).children('span.checkbox').hasClass("checked") === false){
			spanCheckbox.addClass('checked');
			inputCheckboxes.attr("checked","checked");
		}
		else {
			spanCheckbox.removeClass('checked');
			inputCheckboxes.removeAttr("checked","checked");
		}

		text = getCategoryText($(this).parent().find('input.checkbox:checked').length, $(this).parent().find('input.checkbox').length);
		$(this).parent().prev().html(text);
	});


});