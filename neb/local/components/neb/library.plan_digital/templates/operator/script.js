$(function() {
	$( document ).on( "click", ".plan-digitalization .plus_ico", function() {
		var book_id = $(this).closest('.item-container').attr('id');
		var tr_block = $(this).closest('.item-container');
		//console.log(book_id);
		//if($(this).closest('.plusico_wrap').hasClass('minus'))
		//{
		//	console.log('+');
		//	$.post("/local/tools/plan_digitization/add.php", { sessid: BX.message('bitrix_sessid'), BOOK_ID: book_id } );
		//}
		//else
		//{
		//	console.log('-');
		if(confirm('Вы уверены, что хотите удалить эту книгу из планов на оцифровку?')) {
			$.post("/local/tools/plan_digitization/remove.php", { sessid: BX.message('bitrix_sessid'), BOOK_ID: book_id }, function(){
				tr_block.remove();
			})
			.success(function(){
				//Уменьшаем счетчик книг
				if ($('.digitizing-count').text()>0){
					$('.digitizing-count').text(parseInt($('.digitizing-count').text())-1);
				}
			});
		}
		//}
	});

});