<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 3600;

$arParams['ITEM_COUNT'] = intval($arParams['ITEM_COUNT']);

if (empty($arParams['ITEM_COUNT'])) $arParams['ITEM_COUNT'] = 12;

if (!CModule::IncludeModule('nota.exalead'))
    return false;

use \Neb\Main\Library\LibsTable;
use \Nota\Exalead\Stat\StatBookViewTable;
use \Nota\Exalead\SearchQuery;
use \Nota\Exalead\SearchClient;

// сортировка
$by    = trim(htmlspecialcharsEx($_REQUEST['by']));
$order = trim(htmlspecialcharsEx($_REQUEST['order']));

if ($this->StartResultCache(false, array($by, $order))) {

    $arFilter = array();

    // Шаг 1: Популярные издания - делаем выборку по просмотрам за месяц
    if (!empty($arParams['LIBRARY_ID'])) {
        $arLib = Bitrix\NotaExt\Iblock\Element::getByID($arParams['LIBRARY_ID'], array('PROPERTY_LIBRARY_LINK', 'skip_other'));
        if (!empty($arLib['PROPERTY_LIBRARY_LINK_VALUE'])) {
            $arLibExRes = LibsTable::getById($arLib['PROPERTY_LIBRARY_LINK_VALUE']);
            $arLibEx = $arLibExRes->fetch();
            $arFilter['LIB_ID'] = $arLibEx['UF_ID'];
        }
    }

    // Шаг 2: Выбираем идентификаторы изданий библиотеки, отсортированные по числу показов
    $limit = $arParams['ITEM_COUNT']
        + 54; // Магическое число - выбираем больше на случай если вдруг в экзалиде нехватает книг
    $libWhere = 'LIB_ID ' . ((integer)$arFilter['LIB_ID'] > 0
            ? '= ' . (integer)$arFilter['LIB_ID']
            : 'IS NULL');
    $strSql = "
        SELECT BOOK_ID, VIEWS
        FROM neb_stat_book_view_most_popular
        WHERE $libWhere AND HAS_FILE = 1
        ORDER BY VIEWS DESC
        LIMIT 0, $limit
    ";
    $rsResSBV = \Bitrix\Main\Application::getConnection()->query($strSql);
    $arBook = array();
    while ($arStat = $rsResSBV->fetch()) {
        if (strlen($arStat['BOOK_ID']) <= 10)
            continue;
        $arBook[$arStat['BOOK_ID']] = $arStat['VIEWS'];
        $arResult['POPULAR_BOOKS_VIEWS'][$arStat['BOOK_ID']] = $arStat['cnt'];
    }

    if (!empty($arBook)) {

        // Шаг 3.1: Выбираем издания библиотеки из экзолид
        $query = new SearchQuery();
        $query->getByArIds(array_keys($arBook));
        $query->setPageLimit($limit);

        $client = new SearchClient();
        $arBookExalead = $client->getResult($query);

        $arResult['POPULAR_BOOKS'] = $arBookExalead['ITEMS'];

        // Шаг 3.2: Оставляем в массиве только самые популярные издания
        foreach ($arResult['POPULAR_BOOKS'] as &$book) {
            if (!isset($arBook[$book['id']]))
                continue;
            $book['VIEWS'] = $arBook[$book['id']];
            unset($book['library']);
        }
        usort($arResult['POPULAR_BOOKS'], function ($a, $b) {
            return -($a['VIEWS'] - $b['VIEWS']);
        });
        $arResult['POPULAR_BOOKS'] = array_slice($arResult['POPULAR_BOOKS'], 0, 12);


        // Шаг 4: При заданной пользователем сортировке делаем выборку конечного отсортированного массива из экзалид
        if (!empty($by) && !empty($order)) {

            foreach ($arResult['POPULAR_BOOKS'] as $arItem)
                $arResBooks[] = $arItem['id'];

            // Шаг 3.1: Выбираем издания библиотеки
            $query = new SearchQuery();
            $query->getByArIds($arResBooks);
            $query->setPageLimit($limit);
            $query->setParam('s', $order . '(' . $by . ')');

            $client = new SearchClient();
            $arBookExalead = $client->getResult($query);
            $arResult['POPULAR_BOOKS'] = $arBookExalead['ITEMS'];
        }

        $arBookExalead["ITEMS"] = $arResult['POPULAR_BOOKS'];
        $arResult = $arBookExalead;
    }

    $this->EndResultCache();
}

$this->IncludeComponentTemplate();