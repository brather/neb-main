<?php
/**
 * User: agolodkov
 * Date: 05.10.2016
 * Time: 10:29
 */

namespace AssessorBundle\Controller\Api;

use AssessorBundle\Form\TblQueryFrequencyType;
use AssessorBundle\Traits\AssessorRatingTrait;
use BitrixBundle\Annotations\IncludeBitrix;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\View\View;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class ApiQueryFrequency
 *
 * @DI\Service("assessor.api.query_frequency", scope="request")
 */
class ApiQueryFrequencyController implements RatingApiInterface
{
    use AssessorRatingTrait;

    /**
     * @param QueryBuilder $qb
     *
     * @return void
     */
    public function handleAutoComplete($qb)
    {
        $query = $this->request->query->get('query');
        $qb->where('t.query LIKE :query');
        $qb->setParameter('query', $query . '%');
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'AssessorBundle:TblQueryFrequency';
    }

    /**
     * @return TblQueryFrequencyType
     */
    public function getFormType()
    {
        return new TblQueryFrequencyType();
    }

    /**
     * @param int $id
     *
     * @IncludeBitrix()
     *
     * @return static
     * @throws \Exception
     */
    public function deleteAction($id)
    {
        $id = (integer)$id;
        $em = $this->getEntityManager();
        if (!$em->getRepository('AssessorBundle:TblQueryFrequency')->find($id)) {
            throw new NotFoundHttpException('Query not found');
        }
        $editionsRating = $em->getRepository('AssessorBundle:TblQueryRating')->findBy(['idquery' => $id]);
        foreach ($editionsRating as $entity) {
            $em->remove($entity);
        }
        $em->flush();
        $em->clear();
        $editionsRating = $em->getRepository('AssessorBundle:TblQueryRating')->findBy(['idquery' => $id]);
        if (!empty($editionsRating)) {
            throw new UnprocessableEntityHttpException('Cannot delete query sorting');
        }

        return View::create(null, 200);
    }
}