<?php
/**
 * User: agolodkov
 * Date: 18.08.2016
 * Time: 12:42
 */

namespace LbcBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use LbcBundle\Entity\LbcSpecialdivisionsections;

/**
 * @DI\Service("app.specialdivisionsections", public=true, scope="request")
 */
class LbcSpecialdivisionsectionsController extends AbstractCategoryController
{
    /**
     * @DI\Inject("app.api.specialdivisionsections")
     * @var \LbcBundle\Controller\Api\LbcSpecialdivisionsectionsController
     */
    public $api;

    /**
     * @DI\Inject("app.api.specialdivision")
     * @var \LbcBundle\Controller\Api\LbcSpecialdivisionController
     */
    public $ownerApi;

    /**
     * @param LbcSpecialdivisionsections $entity
     */
    public function checkEntityActions($entity)
    {
        parent::checkEntityActions($entity);
        $em = $this->doctrine->getManager();
        foreach ($entity->getActions() as $key => $action) {
            switch ($action['code']) {
                case 'division-images':
                    if (!$em->getRepository('LbcBundle:LbcSpecialdivisionsectionimages')->findOneBy(
                        [
                            'specialdivisionsectionid' => $entity->getId(),
                        ]
                    )
                    ) {
                        $entity->removeAction($key);
                    }
                    break;
            }
        }
    }

    /**
     * @return array
     */
    protected function _getRouterMaps()
    {
        return [
            'api_category_detail' => 'api_bbk_specialdivisionsection_detail',
            'api_category_update' => 'api_bbk_specialdivisionsection_update',
            'api_category_images' => 'api_bbk_specialdivisionsection_images',
            'category'            => 'bbk_specialdivisionsections',
            'category_index'      => 'bbk_specialdivisionsections_index',
        ];
    }

    protected function _getIndexName()
    {
        return 'Разделы специальных делений';
    }

    /**
     * @return array
     */
    protected function _getActionParams()
    {
        return [
            [
                'name' => 'Скан разделов спц. делений',
                'code' => 'division-images',
            ],
        ];
    }
}