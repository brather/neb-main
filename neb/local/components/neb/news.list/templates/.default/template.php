<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<br>
<a href="/subscribe/?RUBRIC_ID=1"><?=GetMessage('SUBSCRIBE_LINK')?></a>
<section class="main-news">
    <div class="row">
        <div class="main-news__gallery main-news__gallery--general">
            <?php foreach($arResult['ITEMS'] as $key => $item): ?>
                <div class="col-md-4 col-sm-6 col-xs-6 main-news__gallery-item" <? if($key % 3 == 0) :?>style="clear:both;"<?endif;?>>
                    <a href="<?php echo $item['DETAIL_PAGE_URL']?>" class="main-news__gallery-link"<?if(!empty($item["PREVIEW_PICTURE"]['SRC'])){?> style="background-image: url('<?php echo $item["PREVIEW_PICTURE"]['SRC'];  ?>')"<?}?> >
                        <img src="<?php echo $item["PREVIEW_PICTURE"]['SRC'];  ?>" alt="<?php echo $item['NAME']?>" class="main-news__gallery-image">
                    </a>
                    <div class="main-news-item__section-name"><?=$item['SECTION_NAME']?></div>
                    <a href="<?php echo $item['DETAIL_PAGE_URL']?>"><?php echo $item['NAME']?></a>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>