
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right">
						<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		<nav class="b-commonnav noborder">
			<a href="#" class="current">О библиотеке</a>
			<a href="#">фонды</a>
			<a href="#">коллекции</a>
		</nav>
		<div class="b-lib_descr">			
			<div class="iblock b-lib_fulldescr">
				<div class="b-libstatus">Федеральная библиотека</div>
				<h2>Российская государственная библиотека (РГБ)</h2>
				<a href="#" class="b-libsite">www.rsl.ru</a>
				<div class="b-rolledtext" data-height="295">
					<div class="b-rolled_in ani_all_500_in">
						<p>В стенах Российской государственной библиотеки находится уникальное собрание отечественных и зарубежных документов на 367 языках мира; объем ее фонда превышает 43 млн. единиц хранения. Здесь имеются специализированные собрания карт, нот, звукозаписей, редких книг, диссертаций, газет и других видов изданий. Центральный основной фонд насчитывает более 29 млн. ед. хранения: книг, журналов, продолжающихся изданий, документов для служебного пользования. Он является базовым собранием в подсистеме основных документных фондов РГБ. </p>
						<p>ГПИБ России является государственным хранилищем литературы по истории, историческим наукам и смежным дисциплинам. Фонд библиотеки — около 4 млн экз., в том числе книг около 2,5 млн экз. Включает издания на русском языке, языках народов Российской Федерации и иностранных языках.</p>
						<p>В стенах Российской государственной библиотеки находится уникальное собрание отечественных и зарубежных документов на 367 языках мира; объем ее фонда превышает 43 млн. единиц хранения. Здесь имеются специализированные собрания карт, нот, звукозаписей, редких книг, диссертаций, газет и других видов изданий. Центральный основной фонд насчитывает более 29 млн. ед. хранения: книг, журналов, продолжающихся изданий, документов для служебного пользования. Он является базовым собранием в подсистеме основных документных фондов РГБ. </p>
						<p>ГПИБ России является государственным хранилищем литературы по истории, историческим наукам и смежным дисциплинам. Фонд библиотеки — около 4 млн экз., в том числе книг около 2,5 млн экз. Включает издания на русском языке, языках народов Российской Федерации и иностранных языках.</p>
					
					</div>
					<a href="#" class="slidetext">Читать далее</a>
				</div>
			</div>
			<a href="#" class="b-lib_photoslink">Фотогалерея</a>
			</div>
		</div>
		<div class="b-side right">
			<div class="b-libfond">
				<div class="b-fond_img">
					<img src="./pic/pic_31.png" alt="">
				</div>
				<span class="b-fondinfo_number_lb">В фонде библиотеки</span>
				<span class="b-portalinfo_numbers">
					<span class="b-portalinfo_number mr iblock">1</span>
					<span class="b-portalinfo_number iblock">0</span>
					<span class="b-portalinfo_number iblock">5</span>
					<span class="b-portalinfo_number mr iblock">1</span>
					<span class="b-portalinfo_number iblock">2</span>
					<span class="b-portalinfo_number iblock">1</span>
					<span class="b-portalinfo_number iblock">7</span></span>
					<span class="b-fondinfo_number_lb">изданий</span>
					<div class="b-lib_collect">
						<span class="b-fondinfo_number_lb">Библиотека собрала</span>
						<span class="b-portalinfo_numbers">

							<span class="b-portalinfo_number iblock">2</span>
							<span class="b-portalinfo_number iblock">1</span>
							<span class="b-portalinfo_number iblock">7</span></span>
							<span class="b-fondinfo_number_lb">коллекций</span>
							<a href="#" class="button_mode button_revers">посмотреть все</a>
						</div>
						<div class="b-lib_counter">
							<div class="b-lib_counter_lb">Количество читателей</div>
							<div class="icouser">х 108</div>
						</div>
						<div class="b-lib_counter">
							<div class="b-lib_counter_lb">Количество просмотров</div>
							<div class="icoviews">х 15 678</div>
						</div>
					</div><!-- /.b-libfond -->
		</div>
		<div class="b-mainblock left">
			<div class="b-lib_descr">		
			<div class="b-lib_map">
				<ul class="iblock b-lib_contact">
					<li><span class="title">Почтовый адрес:</span><span class="value"> 12324, г. Москва, 
						ул. Воздвиженка, д. 3/5.</span></li>
						<li><span class="title">График работы:</span> <span class="value">Ежедневно с 10.00 до 18.00</span></li>			
						<li><span class="title">Телефон</span> <span class="value">8 (495) 783-41-09 <br>8 (495) 783-41-23 / факс</span></li>		
						<li><span class="title">Электронная почта</span> <span class="value"><a href="#">info@rsl.ru</a></span></li>
						<li><span class="title">Skype</span> <span class="value"><a href="#">rsl_lib</a></span></li>	

					</ul><!-- /.b-lib_contact -->
					<div class="b-lib_mapwrapper iblock">
						<div class="fmap" id="fmap" data-lat="55.8858831574" data-lng="37.6034869031" data-zoom="16">

						</div>
						<span class="marker_lib hidden">
							<a href="#" class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</a><br />
							<span class="b-elar_status">Федеральная библиотека</span><br />
							<span class="b-map_elar_info">
								<span class="b-map_elar_infoitem"><span>Адрес:</span>Москва, ул. Череповецкая, 17</span><br />
								<span class="b-map_elar_infoitem"><span>График работы:</span>Ежедневно с 10.00 до 18.00</span>
							</span>
							<span class="b-mapcard_act clearfix">
								<span class="right neb b-mapcard_status">Участник</span>
								<a href="#" class="button_mode">перейти в библиотеку</a>
							</span>
						</span>
					</div>
					
				</div>

			</div><!-- /.b-lib_descr -->
			

		</div><!-- /.b-mainblock -->
		<div class="b-side right">
					<div class="b-popularbooks">
						<h2>популярные издания</h2>

						<div class="b-popularslider" data-slides="one">				

							<div class="b-result-docitem iblock ">

								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock popup_opener" href="#" ><img alt="" class="loadingimg" src="./pic/pic_4.jpg"></a>

										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">Памятники императору Александру I, а …</a></h2>
									<a href="#" class="b-book_autor">Педашенко<br> Стефан Алексеевич</a>
									<div class="icoviews">х 15 678</div>
									
								</div>

							</div><!-- /.b-result-docitem -->
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_27.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">Сумма технологии</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_29.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">футурологический конгресс</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_30.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">Сумма технологии</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>								
								</div>

							</div><!-- /.b-result-docitem -->
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_30.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">Сумма технологии</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->		
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img class="loadingimg" alt="" src="./pic/pic_29.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">футурологический конгресс</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->	


							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock popup_opener" href="#" ><img class="loadingimg" alt="" src="./pic/pic_4.jpg"></a>

										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">Сумма технологии</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img alt=""  class="loadingimg" src="./pic/pic_27.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">Сумма технологии</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_29.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">футурологический конгресс</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_30.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">Сумма технологии</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_30.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">Сумма технологии</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->		
							<div class="b-result-docitem iblock ">


								<div class="b-result-docinfo">

									<a href="#" class="b-result-remove"></a>
									<div class="b-result-docphoto">
										<div class="iblock b-fav_info b-fav_info_mode">
											<span class="b-fav_info_quote">х 2</span>
											<span class="b-fav_info_note">х 1</span>
										</div>
										<a class="b_bookpopular_photo iblock" href="#"><img alt="" class="loadingimg" src="./pic/pic_29.jpg"></a>
										<div class="b-loadprogress">
											<div class="b-loadlabel"></div>
											<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="78">
										</div>						
									</div>
									<h2><a href="#">футурологический конгресс</a></h2>
									<a href="#" class="b-book_autor">Станислав Лем</a>
									<div class="icoviews">х 15 678</div>

								</div>

							</div><!-- /.b-result-docitem -->	



						</div>
					</div><!-- /.b-popularbooks -->
<div class="b-popularbooks b-threebook">
		<h2>популярные издания</h2>
		<div class="b-popularslider slidetype" data-slides="three"><div class="b-result-doc b-result-docfavorite"><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_006511249/" style="position: relative;"><img alt=" Пусть все идет, как идет и будет, как будет : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_006511249&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_006511249/"> Пусть все идет, как идет и будет, к…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D0%B1%D0%BE%D1%80%D0%B8%D1%81+%D1%8F%D1%88%D0%B8%D0%BD&amp;" class="b-book_autor">Яшин<br> Борис Владимирович Борис Яшин</a>
													<div class="icoviews">х 1</div>
																	</div>
				<span class="num">10.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_004474859/" style="position: relative;"><img alt=" Valse des sylphes :. Choix de compositions : classiques et moderne " src="/local/tools/exalead/thumbnail.php?url=000199_000009_004474859&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_004474859/"> Valse des sylphes :. Choix de compo…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%8D%D0%BB%D0%BB%D1%8C%2C+%D0%B0%D0%BB%D1%8C%D1%84%D1%80%D0%B5%D0%B4&amp;" class="b-book_autor">Яэлль<br> Альфред A. Jaell ; revues<br> doigetees et classees par ordre de difficulte par Rodolphe Strobl</a>
													<div class="icoviews">х 4</div>
																	</div>
				<span class="num">11.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000179224/" style="position: relative;"><img alt=" Влияние педагогически организованной деятельности на поведение подростков и старшеклассников : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000179224&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000179224/"> Влияние педагогически организованно…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%89%D1%83%D0%BA%2C+%D1%81.+%D0%BB.&amp;" class="b-book_autor">Ящук<br> С. Л.</a>
													<div class="icoviews">х 7</div>
																	</div>
				<span class="num">12.</span></div></div></div><div class="slick-slide" style="width: 229px;"><div class="b-result-doc b-result-docfavorite"><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000439_000034_RZN%7C%7C%7CBIBL%7C%7C%7C0000162468/"> Воспитательная деятельность школьно…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%89%D0%B5%D0%BD%D0%BA%D0%BE+%D0%BC.%D0%BC.%2C+%D0%B2%D1%83%D0%BB%D1%8C%D1%84%D0%BE%D0%B2+%D0%B1.%D0%B7.%2C+%D1%85%D0%BE%D0%B7%D0%B5+%D1%81.%D0%B5.&amp;" class="b-book_autor">Ященко М.М.<br> Вульфов Б.З.<br> Хозе С.Е.</a>
													<div class="icoviews">х 5</div>
																	</div>
				<span class="num">1.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000321859/" style="position: relative;"><img alt=" Бухгалтерский учет и оценка эффективности вложений в финансовые активы : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000321859&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000321859/"> Бухгалтерский учет и оценка эффекти…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%89%D1%83%D1%80%D0%BA%D0%B8%D0%BD%D0%B0%2C+%D0%B3%D0%B0%D0%BB%D0%B8%D0%BD%D0%B0+%D0%B3%D0%B5%D0%BE%D1%80%D0%B3%D0%B8%D0%B5%D0%B2%D0%BD%D0%B0&amp;" class="b-book_autor">Ящуркина<br> Галина Георгиевна</a>
													<div class="icoviews">х 11</div>
																	</div>
				<span class="num">2.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000228415/" style="position: relative;"><img alt=" Организация инновационной деятельности педагогического училища : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000228415&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000228415/"> Организация инновационной деятельно…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%89%D0%B5%D0%BD%D0%BA%D0%BE%2C+%D1%81%D0%B5%D1%80%D0%B3%D0%B5%D0%B8+%D0%B2%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&amp;" class="b-book_autor">Ященко<br> Сергей Васильевич</a>
													<div class="icoviews">х 6</div>
																	</div>
				<span class="num">3.</span></div></div></div><div class="slick-slide" style="width: 229px;"><div class="b-result-doc b-result-docfavorite"><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000018661/" style="position: relative;"><img alt=" Достижение высшей эффективности общественного производства : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000018661&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000018661/"> Достижение высшей эффективности общ…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D0%B0%D0%BD+%D0%BF%D1%80%D0%B8+%D1%86%D0%BA+%D0%BA%D0%BF%D1%81%D1%81.+%D0%BA%D0%B0%D1%84%D0%B5%D0%B4%D1%80%D0%B0+%D1%8D%D0%BA%D0%BE%D0%BD%D0%BE%D0%BC%D0%B8%D0%BA%D0%B8+%D0%BD%D0%B0%D1%80.+%D1%85%D0%BE%D0%B7-%D0%B2%D0%B0&amp;" class="b-book_autor">Ященко<br> Юрий Петрович АН при ЦК КПСС. Кафедра экономики нар. хоз-ва</a>
													<div class="icoviews">х 1</div>
																	</div>
				<span class="num">4.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_001308824/" style="position: relative;"><img alt=" Применение микропроцессоров в технике связи " src="/local/tools/exalead/thumbnail.php?url=000199_000009_001308824&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_001308824/"> Применение микропроцессоров в техни…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D0%BB.+%D0%B5.+%D1%8F%D1%89%D1%83%D0%BA%3B+%D0%BE%D0%B4%D0%B5%D1%81.+%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D1%82%D0%B5%D1%85%D0%BD.+%D1%81%D0%B2%D1%8F%D0%B7%D0%B8+%D0%B8%D0%BC.+%D0%B0.+%D1%81.+%D0%BF%D0%BE%D0%BF%D0%BE%D0%B2%D0%B0&amp;" class="b-book_autor">Ящук<br> Леонид Емельянович Л. Е. Ящук; Одес. электротехн. связи им. А. С. Попова</a>
													<div class="icoviews">х 1</div>
																	</div>
				<span class="num">5.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000103738/" style="position: relative;"><img alt=" Развитие экспериментальных методов исследований с молекулярными пучками, направленное на увеличение чувствительности экспериментов по поиску эффектов нарушения временной инвариантности в молекулах : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000103738&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000103738/"> Развитие экспериментальных методов …</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D0%B8%D0%BD-%D1%82+%D1%8F%D0%B4%D0%B5%D1%80%D0%BD%D0%BE%D0%B8+%D1%84%D0%B8%D0%B7%D0%B8%D0%BA%D0%B8+%D0%B8%D0%BC.+%D0%B1.+%D0%BF.+%D0%BA%D0%BE%D0%BD%D1%81%D1%82%D0%B0%D0%BD%D1%82%D0%B8%D0%BD%D0%BE%D0%B2%D0%B0&amp;" class="b-book_autor">Ящук<br> Валерий Владимирович Ин-т ядерной физики им. Б. П. Константинова</a>
													<div class="icoviews">х 2</div>
																	</div>
				<span class="num">6.</span></div></div></div><div class="slick-slide slick-active" style="width: 229px;"><div class="b-result-doc b-result-docfavorite"><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_004474848/" style="position: relative;"><img alt=" Мечты русского пленного воина : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_004474848&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_004474848/"> Мечты русского пленного воина : </a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%81%D0%BE%D1%87.+%D0%BA.%D1%84.+%D1%8F%D1%89%D0%B8%D0%BA%D0%BE%D0%B2%D0%B0&amp;" class="b-book_autor">Ящиков<br> К.Ф. соч. К.Ф. Ящикова</a>
													<div class="icoviews">х 2</div>
																	</div>
				<span class="num">7.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000273902/" style="position: relative;"><img alt=" Агроэкологическая эффективность поверхностного компостирования растительных остатков на черноземе типичном в условиях лесостепи ЦЧЗ : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000273902&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000273902/"> Агроэкологическая эффективность пов…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%89%D0%B5%D0%BD%D0%BA%D0%BE%2C+%D0%BD%D0%B0%D1%82%D0%B0%D0%BB%D0%B8%D1%8F+%D0%B0%D0%BD%D0%B0%D1%82%D0%BE%D0%BB%D1%8C%D0%B5%D0%B2%D0%BD%D0%B0&amp;" class="b-book_autor">Ященко<br> Наталия Анатольевна</a>
													<div class="icoviews">х 3</div>
																	</div>
				<span class="num">8.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000071871/" style="position: relative;"><img alt=" Профилактика ассоциативных заболеваний овец, вызываемых стронгилятами и эймериями в степной зоне Украинской СССР : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000071871&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000071871/"> Профилактика ассоциативных заболева…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D0%B2%D0%B0%D1%81%D1%85%D0%BD%D0%B8%D0%BB.+%D0%B2%D1%81%D0%B5%D1%81%D0%BE%D1%8E%D0%B7.+%D0%B8%D0%BD-%D1%82+%D0%B3%D0%B5%D0%BB%D1%8C%D0%BC%D0%B8%D0%BD%D1%82%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B8+%D0%B8%D0%BC.+%D0%BA.+%D0%B8.+%D1%81%D0%BA%D1%80%D1%8F%D0%B1%D0%B8%D0%BD%D0%B0&amp;" class="b-book_autor">Ященко<br> Николай Федорович ВАСХНИЛ. Всесоюз. ин-т гельминтологии им. К. И. Скрябина</a>
													<div class="icoviews">х 1</div>
																	</div>
				<span class="num">9.</span></div></div></div><div class="slick-slide" style="width: 229px;"><div class="b-result-doc b-result-docfavorite"><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_006511249/" style="position: relative;"><img alt=" Пусть все идет, как идет и будет, как будет : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_006511249&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_006511249/"> Пусть все идет, как идет и будет, к…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D0%B1%D0%BE%D1%80%D0%B8%D1%81+%D1%8F%D1%88%D0%B8%D0%BD&amp;" class="b-book_autor">Яшин<br> Борис Владимирович Борис Яшин</a>
													<div class="icoviews">х 1</div>
																	</div>
				<span class="num">10.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_004474859/" style="position: relative;"><img alt=" Valse des sylphes :. Choix de compositions : classiques et moderne " src="/local/tools/exalead/thumbnail.php?url=000199_000009_004474859&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_004474859/"> Valse des sylphes :. Choix de compo…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%8D%D0%BB%D0%BB%D1%8C%2C+%D0%B0%D0%BB%D1%8C%D1%84%D1%80%D0%B5%D0%B4&amp;" class="b-book_autor">Яэлль<br> Альфред A. Jaell ; revues<br> doigetees et classees par ordre de difficulte par Rodolphe Strobl</a>
													<div class="icoviews">х 4</div>
																	</div>
				<span class="num">11.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000179224/" style="position: relative;"><img alt=" Влияние педагогически организованной деятельности на поведение подростков и старшеклассников : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000179224&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000179224/"> Влияние педагогически организованно…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%89%D1%83%D0%BA%2C+%D1%81.+%D0%BB.&amp;" class="b-book_autor">Ящук<br> С. Л.</a>
													<div class="icoviews">х 7</div>
																	</div>
				<span class="num">12.</span></div></div></div><div class="slick-slide slick-cloned" style="width: 229px;"><div class="b-result-doc b-result-docfavorite"><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000439_000034_RZN%7C%7C%7CBIBL%7C%7C%7C0000162468/" style="position: relative;"><img alt=" Воспитательная деятельность школьной комсомольской организации / [М.М. Ященко, Б.З. Вульфов, С.Е. Хозе и др.] ; под ред. М.М. Ященко " src="/local/tools/exalead/thumbnail.php?url=000439_000034_RZN|||BIBL|||0000162468&amp;source=000439_Ryzanskaya_OUNB&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000439_000034_RZN%7C%7C%7CBIBL%7C%7C%7C0000162468/"> Воспитательная деятельность школьно…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%89%D0%B5%D0%BD%D0%BA%D0%BE+%D0%BC.%D0%BC.%2C+%D0%B2%D1%83%D0%BB%D1%8C%D1%84%D0%BE%D0%B2+%D0%B1.%D0%B7.%2C+%D1%85%D0%BE%D0%B7%D0%B5+%D1%81.%D0%B5.&amp;" class="b-book_autor">Ященко М.М.<br> Вульфов Б.З.<br> Хозе С.Е.</a>
													<div class="icoviews">х 5</div>
																	</div>
				<span class="num">1.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000321859/" style="position: relative;"><img alt=" Бухгалтерский учет и оценка эффективности вложений в финансовые активы : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000321859&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000321859/"> Бухгалтерский учет и оценка эффекти…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%89%D1%83%D1%80%D0%BA%D0%B8%D0%BD%D0%B0%2C+%D0%B3%D0%B0%D0%BB%D0%B8%D0%BD%D0%B0+%D0%B3%D0%B5%D0%BE%D1%80%D0%B3%D0%B8%D0%B5%D0%B2%D0%BD%D0%B0&amp;" class="b-book_autor">Ящуркина<br> Галина Георгиевна</a>
													<div class="icoviews">х 11</div>
																	</div>
				<span class="num">2.</span></div><div class="b-result-docitem iblock">
					<div class="b-result-docinfo">
						<a href="#" class="b-result-remove"></a>
						<div class="b-result-docphoto"><a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000228415/" style="position: relative;"><img alt=" Организация инновационной деятельности педагогического училища : " src="/local/tools/exalead/thumbnail.php?url=000199_000009_000228415&amp;source=RGBData&amp;width=80&amp;height=125" class="loadingimg"></a></div>
						<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/000199_000009_000228415/"> Организация инновационной деятельно…</a></h2>
						<a href="/search/?f_field[authorbook]=f%2Fauthorbook%2F%D1%8F%D1%89%D0%B5%D0%BD%D0%BA%D0%BE%2C+%D1%81%D0%B5%D1%80%D0%B3%D0%B5%D0%B8+%D0%B2%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&amp;" class="b-book_autor">Ященко<br> Сергей Васильевич</a>
													<div class="icoviews">х 6</div>
																	</div>
				<span class="num">3.</span></div></div></div></div>
	</div>
					<div class="b-viewhnews">
						<h2>новости библиотеки</h2>

						<div class="b-newslider slidetype">
							<div>
								<div class="b-newsslider_item">
									<div class="date">10 сентября</div>	
									<a href="#">В Ульяновске появилась библиотека XXI века</a>	
									<p>На III Международном форуме «Культура нового поколения» в Ульяновске генеральный директор Библиотеки им. Рудомино, директор благотворительного фонда «Институт толерантности» </p>		

								</div>
							</div>
							<div>
								<div class="b-newsslider_item">
									<div class="date">5 сентября</div>	
									<a href="#">В Москве появятся круглосуточные библиотеки</a>	
									<p>В следующем году в каждом округе может появиться круглосуточная библиотека, сообщает Московский городской библиотечный центр (МГБЦ)</p>	
								</div>	
							</div>
							<div>
								<div class="b-newsslider_item">
									<div class="date">10 сентября</div>	
									<a href="#">Российской национальной библиотеке — 200 лет!</a>	
									<p>На III Международном форуме «Культура нового поколения» в Ульяновске генеральный директор Библиотеки им. Рудомино, директор благотворительного фонда «Институт толерантности» </p>	
								</div>	
							</div>
							<div>
								<div class="b-newsslider_item">
									<div class="date">14 сентября</div>	
									<a href="#">В Ульяновске появилась библиотека XXI века</a>	
									<p>6 сентября в петербургской Российской национальной библиотеке начинается цикл лекций, посвященных 200-летию со дня ее открытия</p>		
								</div>
							</div>
							<div>
								<div class="b-newsslider_item">
									<div class="date">23 сентября</div>	
									<a href="#">Российская государственная библиотека: идти в ногу со временем</a>	
									<p>«Открытая библиотека», совместный проект РГБ и ОАО «АК «Транснефть», при поддержке краудсорсинговой платформы citycelebrity.ru открывают прием проектов по развитию РГБ на 2013–2014 годы</p>	
								</div>	
							</div>
							<div>
								<div class="b-newsslider_item">
									<div class="date">10 сентября</div>	
									<a href="#">В Ульяновске появилась библиотека XXI века</a>	
									<p>На III Международном форуме «Культура нового поколения» в Ульяновске генеральный директор Библиотеки им. Рудомино, директор благотворительного фонда «Институт толерантности» </p>	
								</div>	
							</div>
							<div>
								<div class="b-newsslider_item">
									<div class="date">5 сентября</div>	
									<a href="#">В Москве появятся круглосуточные библиотеки</a>	
									<p>В следующем году в каждом округе может появиться круглосуточная библиотека, сообщает Московский городской библиотечный центр (МГБЦ)</p>	
								</div>	
							</div>
							<div>
								<div class="b-newsslider_item">
									<div class="date">10 сентября</div>	
									<a href="#">Российской национальной библиотеке — 200 лет!</a>	
									<p>На III Международном форуме «Культура нового поколения» в Ульяновске генеральный директор Библиотеки им. Рудомино, директор благотворительного фонда «Институт толерантности» </p>	
								</div>
							</div>
							<div>
								<div class="b-newsslider_item">
									<div class="date">14 сентября</div>	
									<a href="#">В Ульяновске появилась библиотека XXI века</a>	
									<p>6 сентября в петербургской Российской национальной библиотеке начинается цикл лекций, посвященных 200-летию со дня ее открытия</p>
								</div>	
							</div>
							<div>
								<div class="b-newsslider_item">
									<div class="date">23 сентября</div>	
									<a href="#">Российская государственная библиотека: идти в ногу со временем</a>	
									<p>«Открытая библиотека», совместный проект РГБ и ОАО «АК «Транснефть», при поддержке краудсорсинговой платформы citycelebrity.ru открывают прием проектов по развитию РГБ на 2013–2014 годы</p>		
								</div>
							</div>
						</div> <!-- /.b-newslider -->

					</div><!-- /.b-viewhnews -->
				</div><!-- /.b-side -->
				
				

			</section>
			<!-- 
			data-lat="55.8858831574" data-lng="37.6034869031" data-zoom="16" - начальные параметры карты: масштаб и координаты адреса библиотеки
		-->

	</div><!-- /.homepage -->


	<? include("footer.php"); ?>
	<script src="//api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>

	<!--popup добавить в подборки-->
<!--<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu js_openmenu">мои подборки</a>
	
	<ul class="b-selectionlist">
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb8">	<label for="cb8" class="black">Любимые авторы</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb9">	<label for="cb9" class="black">Научно-популярная фантастика</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb10"><label for="cb10" class="black">Ракеты и люди</label>

		</li>	
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb14"><label for="cb14" class="lite">Отметить как прочитанное</label>

		</li>
		<li><a href="#" class="b-selection_add"><span>+</span>Cоздать подборку</a></li>
	</ul>
</div>--><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>