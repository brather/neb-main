<?php
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
/**
 * @var array $arResult
 * @var CMain $APPLICATION
 */
$APPLICATION->IncludeComponent(
    'neb:cities', 'library', array(
        'regionName' => isset($_GET['region_name'])
            ? $_GET['region_name']
            : null
    )
);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");