<?php
/**
 * User: agolodkov
 * Date: 18.01.2016
 * Time: 11:14
 */

namespace Neb\Main\Stat;


abstract class Report
{
    /**
     * @var int
     */
    protected $_dateFrom;

    /**
     * @var int
     */
    protected $_dateTo;

    /**
     * @var string
     */
    protected $_dayFormat;

    /**
     * @var string
     */
    protected $_name;

    abstract function getData();

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        foreach ($options as $optionName => $optionValue) {
            if (property_exists($this, $optionName)) {
                $this->$optionName = $optionValue;
                continue;
            }
            $optionName = '_' . $optionName;
            if (property_exists($this, $optionName)) {
                $this->$optionName = $optionValue;
            }
        }
        if (null === $this->_dateFrom) {
            $this->_dateFrom = time() - 86400;
        }
        if (null === $this->_dateTo) {
            $this->_dateTo = time();
        }
    }


    /**
     * @param string $report
     * @param array  $options
     *
     * @return self
     * @throws StatException
     */
    public static function factory($report, $options)
    {
        $class = '\Neb\Main\Stat\Report\\'
            . str_replace(' ', '', ucwords(str_replace('_', ' ', $report)));
        if (!class_exists($class)) {
            throw new StatException("Report $report not found.");
        }

        return new $class($options);
    }


    /**
     * @return int
     */
    public function getDateFrom()
    {
        return $this->_dateFrom;
    }

    /**
     * @param int $dateFrom
     */
    public function setDateFrom($dateFrom)
    {
        $this->_dateFrom = $dateFrom;
    }

    /**
     * @return int
     */
    public function getDateTo()
    {
        return $this->_dateTo;
    }

    /**
     * @param int $dateTo
     */
    public function setDateTo($dateTo)
    {
        $this->_dateTo = $dateTo;
    }

    /**
     * @return string
     */
    public function getDayFormat()
    {
        return $this->_dayFormat;
    }

    /**
     * @param string $dayFormat
     */
    public function setDayFormat($dayFormat)
    {
        $this->_dayFormat = $dayFormat;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }


}