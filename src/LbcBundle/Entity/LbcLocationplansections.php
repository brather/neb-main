<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;
use LbcBundle\Entity\Traits\CategoryCodeTrait;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * LbcLocationplansections
 *
 * @Gedmo\Tree(type="closure")
 * @Gedmo\TreeClosure(class="LbcBundle\Entity\LbcLocationplansectionsClosure")
 * @ORM\Table(name="lbc_locationplansections")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\ClosureTreeRepository")
 */
class LbcLocationplansections extends AbstractLbcCategory
{
    use CategoryCodeTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="LocationPlanId", type="integer", nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    public $locationplanid;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="LbcLocationplansections", inversedBy="children")
     * @ORM\JoinColumn(name="ParentId", referencedColumnName="Id", onDelete="CASCADE")
     */
    public $parent;

    /**
     * @ORM\OneToMany(targetEntity="LbcLocationplansections", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    public $children;

    /**
     * Set locationplanid
     *
     * @param integer $locationplanid
     *
     * @return LbcLocationplansections
     */
    public function setLocationplanid($locationplanid)
    {
        $this->locationplanid = $locationplanid;

        return $this;
    }

    /**
     * Get locationplanid
     *
     * @return integer
     */
    public function getLocationplanid()
    {
        return $this->locationplanid;
    }
}
