<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;
$APPLICATION->IncludeComponent(
    "rest:book.view.lib",
    "",
    array(
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/rest_api/book/view/lib/",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "3600",
        "DOCUMENTATION" => "Y",
        "SERVICE_NAME" => "Книги. Просмотры.",
        "SEF_URL_TEMPLATES" => array(
            "processRequest" => "",
        )
    ),
    false
);