<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\EventManager;
IncludeModuleLangFile(__FILE__);

class intervolga_remoteuserclient extends CModule
{
	public static function getModuleId()
	{
		return basename(dirname(__DIR__));
	}

	public static function getComponentsSubDir()
	{
		return substr(self::getModuleId(), 0, strpos(self::getModuleId(), "."));
	}

	public function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__) . "/version.php");
		$this->MODULE_ID = self::getModuleId();
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("intervolga.remoteuserclient.MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("intervolga.remoteuserclient.MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("intervolga.remoteuserclient.PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("intervolga.remoteuserclient.PARTNER_URI");
	}

	public function DoInstall()
	{
		$this->InstallDB();
		$this->InstallFiles();
		$this->InstallEvents();
		RegisterModule(self::getModuleId());
	}

	public function DoUninstall()
	{
		UnRegisterModule(self::getModuleId());
		$this->UnInstallEvents();
		$this->UnInstallFiles();
		$this->UnInstallDB();
	}

	protected static function installUserFields()
	{
		global $USER_FIELD_MANAGER;

		$bUploadDateFound = FALSE;
		$bUploadPreventFound = FALSE;
		$bRemoteIdFound = FALSE;

		$arUserFields = $USER_FIELD_MANAGER->GetUserFields("USER", 0, "ru");
		foreach ($arUserFields as $arUserField)
		{
			if ($arUserField["FIELD_NAME"] == "UF_IV_UPLOAD_DATE")
			{
				$bUploadDateFound = TRUE;
			}
			if ($arUserField["FIELD_NAME"] == "UF_IV_UPLOAD_PREVENT")
			{
				$bUploadPreventFound = TRUE;
			}
			if ($arUserField["FIELD_NAME"] == "UF_IV_REMOTE_ID")
			{
				$bRemoteIdFound = TRUE;
			}
		}
		$obUserTypeEntity = new \CUserTypeEntity();
		if (!$bUploadDateFound)
		{
			$obUserTypeEntity->add(array(
				"ENTITY_ID" => "USER",
				"FIELD_NAME" => "UF_IV_UPLOAD_DATE",
				"USER_TYPE_ID" => "datetime",
				"MULTIPLE" => "N",
				"MANDATORY" => "N",
				"SHOW_FILTER" => "I",
				"SHOW_IN_LIST" => "Y",
				"IS_SEARCHABLE" => "N",
				"EDIT_FORM_LABEL" => array("ru" => GetMessage("intervolga.remoteuserclient.USER_UPLOAD_DATE")),
				"LIST_COLUMN_LABEL" => array("ru" => GetMessage("intervolga.remoteuserclient.USER_UPLOAD_DATE")),
				"LIST_FILTER_LABEL" => array("ru" => GetMessage("intervolga.remoteuserclient.USER_UPLOAD_DATE")),
			));
		}
		if (!$bUploadPreventFound)
		{
			$obUserTypeEntity->add(array(
				"ENTITY_ID" => "USER",
				"FIELD_NAME" => "UF_IV_UPLOAD_PREVENT",
				"USER_TYPE_ID" => "boolean",
				"MULTIPLE" => "N",
				"MANDATORY" => "N",
				"SHOW_FILTER" => "I",
				"SHOW_IN_LIST" => "Y",
				"IS_SEARCHABLE" => "N",
				"EDIT_FORM_LABEL" => array("ru" => GetMessage("intervolga.remoteuserclient.USER_UPLOAD_PREVENT")),
				"LIST_COLUMN_LABEL" => array("ru" => GetMessage("intervolga.remoteuserclient.USER_UPLOAD_PREVENT")),
				"LIST_FILTER_LABEL" => array("ru" => GetMessage("intervolga.remoteuserclient.USER_UPLOAD_PREVENT")),
			));
		}
		if (!$bRemoteIdFound)
		{
			$obUserTypeEntity->add(array(
				"ENTITY_ID" => "USER",
				"FIELD_NAME" => "UF_IV_REMOTE_ID",
				"USER_TYPE_ID" => "double",
				"MULTIPLE" => "N",
				"MANDATORY" => "N",
				"SHOW_FILTER" => "I",
				"SHOW_IN_LIST" => "Y",
				"IS_SEARCHABLE" => "N",
				"EDIT_FORM_LABEL" => array("ru" => GetMessage("intervolga.remoteuserclient.USER_REMOTE_ID")),
				"LIST_COLUMN_LABEL" => array("ru" => GetMessage("intervolga.remoteuserclient.USER_REMOTE_ID")),
				"LIST_FILTER_LABEL" => array("ru" => GetMessage("intervolga.remoteuserclient.USER_REMOTE_ID")),
			));
		}
	}

	public function InstallDB()
	{
		if (is_dir($d = dirname(__FILE__) . "/db/"))
		{
			global $DB;
			$DB->RunSQLBatch($d . strtolower($DB->type) . "/install.sql");
		}
		self::installUserFields();

		return TRUE;
	}

	public function UnInstallDB($arParams = array())
	{
		if (is_dir($d = dirname(__FILE__) . "/db/"))
		{
			global $DB;
			$DB->RunSQLBatch($d . strtolower($DB->type) . "/uninstall.sql");
		}

		return TRUE;
	}

	public function InstallFiles($arParams = array())
	{
		// Create admin page include files
		if (is_dir($sAdminPath = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . self::getModuleId() . '/admin'))
		{
			if ($dir = opendir($sAdminPath))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || $item == 'menu.php')
					{
						continue;
					}
					$sFileContent = '<?require $_SERVER["DOCUMENT_ROOT"] . "/local/modules/' . self::getModuleId() . '/admin/' . $item . '";?>';
					file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . self::getModuleId() . '_' . $item, $sFileContent);
				}
				closedir($dir);
			}
		}

		CopyDirFiles(__DIR__."/components", $_SERVER["DOCUMENT_ROOT"] . "/local/components/intervolga/", TRUE, TRUE);

		return TRUE;
	}

	public function UnInstallFiles()
	{
		// Remove admin page include files
		if (is_dir($sAdminPath = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . self::getModuleId() . '/admin'))
		{
			if ($dir = opendir($sAdminPath))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || $item == 'menu.php')
					{
						continue;
					}
					unlink($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/' . self::getModuleId() . '_'.$item);
				}
				closedir($dir);
			}
		}

		return TRUE;
	}

	public function InstallEvents()
	{
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onBeforeUserAdd()
		 */
		RegisterModuleDependences("main", "OnBeforeUserAdd", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onBeforeUserAdd");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onAfterUserAdd()
		 */
		RegisterModuleDependences("main", "OnAfterUserAdd", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onAfterUserAdd");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onBeforeUserUpdate()
		 */
		RegisterModuleDependences("main", "OnBeforeUserUpdate", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onBeforeUserUpdate");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onAfterUserUpdate()
		 */
		RegisterModuleDependences("main", "OnAfterUserUpdate", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onAfterUserUpdate");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onBeforeUserDelete()
		 */
		RegisterModuleDependences("main", "OnBeforeUserDelete", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onBeforeUserDelete");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onUserDelete()
		 */
		RegisterModuleDependences("main", "OnUserDelete", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onUserDelete");
		return TRUE;
	}

	public function UnInstallEvents()
	{
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onBeforeUserAdd()
		 */
		UnRegisterModuleDependences("main", "OnBeforeUserAdd", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onBeforeUserAdd");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onAfterUserAdd()
		 */
		UnRegisterModuleDependences("main", "OnAfterUserAdd", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onAfterUserAdd");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onBeforeUserUpdate()
		 */
		UnRegisterModuleDependences("main", "OnBeforeUserUpdate", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onBeforeUserUpdate");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onAfterUserUpdate()
		 */
		UnRegisterModuleDependences("main", "OnAfterUserUpdate", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onAfterUserUpdate");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onBeforeUserDelete()
		 */
		UnRegisterModuleDependences("main", "OnBeforeUserDelete", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onBeforeUserDelete");
		/**
		 * @see \Intervolga\RemoteUserClient\EventHandlers\Main::onUserDelete()
		 */
		UnRegisterModuleDependences("main", "OnUserDelete", self::getModuleId(), "\\Intervolga\\RemoteUserClient\\EventHandlers\\Main", "onUserDelete");
		return TRUE;
	}
}