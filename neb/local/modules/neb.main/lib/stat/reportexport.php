<?php
/**
 * User: agolodkov
 * Date: 18.01.2016
 * Time: 16:35
 */

namespace Neb\Main\Stat;


class ReportExport
{
    /**
     * @var Report
     */
    protected $_report;

    /**
     * @param Report $report
     */
    public function __construct($report)
    {
        $this->_report = $report;
    }

    public function saveTo($file)
    {
        $title = 'Report - ' . $this->_report->getName()
            . ' ' . date(
                $this->_report->getDayFormat(),
                $this->_report->getDateFrom()
            )
            . ' - ' . date(
                $this->_report->getDayFormat(),
                $this->_report->getDateTo()
            );
        $excel = new \PHPExcel();
        $excel->getProperties()
            ->setTitle($title)
            ->setSubject($title);
        $sheet = $excel->setActiveSheetIndex(0);

        $sheet
            ->setCellValue('A1', 'date')
            ->setCellValue('B1', 'value');

        $index = 2;
        foreach ($this->_report->getData() as $item) {
            $sheet
                ->setCellValue('A' . $index, $item['date'])
                ->setCellValue('B' . $index, $item['cnt']);
            $index++;
        }

        /** Считаем текущую ширину А */
        $sheet->calculateColumnWidths();
        $aWidth = $sheet->getColumnDimension('A')->getWidth();
        /** Добавляем в А титл и считем ширину */
        $sheet->insertNewRowBefore();
        $sheet->setCellValue('A1', $title);
        $sheet->calculateColumnWidths();
        $aWidthTitle = $sheet->getColumnDimension('A')->getWidth();
        /** Мержым колонки и выставляем ширину */
        foreach (['A', 'B'] as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(false);
        }
        $sheet->mergeCells('A1:B1');
        $sheet->getColumnDimension('A')->setWidth($aWidth);
        $sheet->getColumnDimension('B')->setWidth(
            $aWidthTitle
            - $aWidth
        );

        $excel->getActiveSheet()->setTitle('Statistics');
        $excel->setActiveSheetIndex(0);
        die;

        header('Content-Type: application/vnd.ms-excel');
        header(
            'Content-Disposition: attachment;filename="' . $title . '.xlsx"'
        );
        header(
            'Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'
        );
        $excelWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $excelWriter->save($file);
    }

    /**
     * @param string $format
     * @param Report $report
     *
     * @return static
     * @throws StatException
     */
    public static function factory($format, $report)
    {
        if ('xls' !== $format) {
            throw new StatException("format $format not found");
        }

        return new static($report);
    }
}