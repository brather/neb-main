<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

CJSCore::Init(array('date'));

?>
<?
if ($arResult["BACKURL"] <> '') {
    ?>
    <input type="hidden" name="backurl"
           value="<?= $arResult["BACKURL"] ?>"/>
<?
}
?>
<input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value=""/>
<input type="hidden" name="REGISTER[PERSONAL_NOTES]" id="PERSONAL_NOTES" value=""/>

<p class="note">
    <?= Loc::getMessage('MAIN_REGISTER_NOTE'); ?>
</p>

<script>
    var rulesAndMessages = {
        rules: {},
        messages: {},
        groups: {}
    };
</script>

<div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_FIELD_PASSPORT'); ?></div>

<div class="form-group">
    <label for="usersurname">
        <em class="hint">*</em>
        <?=Loc::getMessage('MAIN_REGISTER_LASTNAME');?>
    </label>
    <input type="text" data-required="required" 
        value="<?=$arResult["VALUES"]['LAST_NAME']?>"  
        id="usersurname" 
        name="REGISTER[LAST_NAME]" 
        class="form-control">
    <script>
        rulesAndMessages.rules["REGISTER[LAST_NAME]"] = {
            required: true,
            maxlength: 30,
            minlength: 2
        };
        rulesAndMessages.messages["REGISTER[LAST_NAME]"] = {
            required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?>",
            minlength: "Не менее 2 символов",
            /*wrongformat: "<?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?>"*/
        };
    </script>
</div>

<div class="form-group">
    <label for="userfirstname">
        <em class="hint">*</em>
        <?= Loc::getMessage('MAIN_REGISTER_FIRSTNAME'); ?>
    </label>

    <input type="text" 
            data-required="required"
            value="<?= $arResult["VALUES"]['NAME'] ?>"
            id="userfirstname"
            name="REGISTER[NAME]"
            class="form-control">
    <script>
        rulesAndMessages.rules["REGISTER[NAME]"] = {
            required: true,
            maxlength: 30,
            minlength: 2
        };
        rulesAndMessages.messages["REGISTER[NAME]"] = {
            required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?>",
            minlength: "Не менее 2 символов",
        };
    </script>
</div>
<div class="form-group">
    <label for="usermiddlename">
        <em class="hint">*</em>
        <?= Loc::getMessage('MAIN_REGISTER_MIDNAME'); ?></label>
    <input type="text"
           value="<?= $arResult["VALUES"]['SECOND_NAME'] ?>"
           id="usermiddlename"           
           name="REGISTER[SECOND_NAME]" 
           class="form-control">
    <script>
        rulesAndMessages.rules["REGISTER[SECOND_NAME]"] = {
            required: true,
            maxlength: 30,
            minlength: 2
        };
        rulesAndMessages.messages["REGISTER[SECOND_NAME]"] = {
            required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?>",
            minlength: "Не менее 2 символов",
        };
    </script>
</div>

<div class="form-group">
    <label for="fullbirthday">
        <em class="hint">*</em>
        <?= Loc::getMessage('MAIN_REGISTER_BIRTHDAY'); ?>
    </label>
    <input type="text" class="form-control"
           data-required="true"
           value="<?= $arResult["VALUES"]['PERSONAL_BIRTHDAY'] ?>"
           id="fullbirthday"
           name="REGISTER[PERSONAL_BIRTHDAY]"
           onclick="BX.calendar({node: this, field: fullbirthday,  form: history, bTime: false, value: ''});">
    <script>
        rulesAndMessages.rules["REGISTER[PERSONAL_BIRTHDAY]"] = {
            required: true,
            checkAge: true
            /*
             function(element){
                 var age = getAge( $("#fullbirthday").val() );
                 console.log( Boolean(age < 12) );
                 return ( Boolean(age < 12) );
             }
            */
        };
        rulesAndMessages.messages["REGISTER[PERSONAL_BIRTHDAY]"] = {
            required: "<?= Loc::getMessage('REGISTER_FIELD_PERSONAL_BIRTHDAY'); ?>"
        };
    </script>
</div>

<div class="form-group row">
    <label class="col-xs-12">
        <em class="hint">*</em>
        <?= Loc::getMessage('MAIN_REGISTER_GENDER'); ?>
    </label>

    <label class="radio-inline">
        <input type="radio" 
        name="REGISTER[PERSONAL_GENDER]" 
        <?= ('M' === $arResult['VALUES']['PERSONAL_GENDER']?'selected':'') ?> 
        value="M">
            <i class="lbl"></i>
            <?= Loc::getMessage('MAIN_REGISTER_GENDER_MALE'); ?>
    </label>
    <label class="radio-inline">
        <input type="radio" 
        name="REGISTER[PERSONAL_GENDER]" 
        <?= ('F' === $arResult['VALUES']['PERSONAL_GENDER']?'selected':'') ?> 
        value="F">
            <i class="lbl"></i>
            <?= Loc::getMessage('MAIN_REGISTER_GENDER_FEMALE'); ?>
    </label>    
    <script>
        rulesAndMessages.rules["REGISTER[PERSONAL_GENDER]"] = {
            required: true
        };
        rulesAndMessages.messages["REGISTER[PERSONAL_GENDER]"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_SELECT'); ?>"
        };
    </script>
</div>
<div class="row">
    <label for="passserial" class="col-xs-12">
        <em class="hint">*</em>
        <?= Loc::getMessage(
            'REGISTER_FIELD_PASSPORT_NUMBERS'
        ); ?></label>

    <div class="col-xs-6 form-group">
        <input type="text"
             data-required="required"
             value="<?= $arResult["VALUES"]['UF_PASSPORT_SERIES'] ?>"
             id="passserial"
             name="UF_PASSPORT_SERIES"
             class="form-control">
        <script>
            rulesAndMessages.rules["UF_PASSPORT_SERIES"] = {
                required: true
            };
            rulesAndMessages.messages["UF_PASSPORT_SERIES"] = {
                required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
            };
        </script>
    </div>
    <div class="col-xs-6 form-group">
        <input type="text"
             data-required="required"
             value="<?= $arResult["VALUES"]['UF_PASSPORT_NUMBER'] ?>"
             id="settings022"
             name="UF_PASSPORT_NUMBER"
             class="form-control">
        <script>
            rulesAndMessages.rules["UF_PASSPORT_NUMBER"] = {
                required: true
            };
            rulesAndMessages.messages["UF_PASSPORT_NUMBER"] = {
                required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
            };
        </script>
    </div>
</div>
<div class="form-group">
    <label for="usercitizenship">
        <em class="hint">*</em>
        <?= Loc::getMessage('MAIN_REGISTER_CITIZENSHIP'); ?>
    </label>
    
    <select name="UF_CITIZENSHIP" id="usercitizenship"
            class="form-control" data-required="true">
        <?
        $arUF_CITIZENSHIP = nebUser::getFieldEnum(
            array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'),
            array("VALUE" => "ASC")
        );
        if (!empty($arUF_CITIZENSHIP)) {
            $last = count($arUF_CITIZENSHIP);
            foreach ($arUF_CITIZENSHIP as $arItem) {
                if ($arItem['XML_ID'] == $last) {
                    $other = $arItem;
                    continue;
                }
                if ($arItem['DEF'] == "Y") {
                    $ruCitizenship = $arItem['ID'];
                }
                $selected = false;
                if (!isset($arResult['VALUES']['UF_CITIZENSHIP'])
                    && $arItem['DEF'] == "Y"
                ) {
                    $selected = true;
                } elseif ($arItem['ID']
                    == $arResult['VALUES']['UF_CITIZENSHIP']
                ) {
                    $selected = true;
                }
                ?>
                <option
                    value="<?= $arItem['ID'] ?>"<?= $selected ? ' selected="selected"'
                    : ''; ?>><?= $arItem['VALUE'] ?></option>
            <?php
            }
            ?>
            <option
                value="<?= $other['ID'] ?>"<?= (
                (!isset($arResult['VALUES']['UF_CITIZENSHIP'])
                    && $other['DEF'] == "Y")
                || $other['ID'] == $arResult['VALUES']['UF_CITIZENSHIP']
            ) ? ' selected="selected"'
                : ''; ?>><?= $other['VALUE'] ?></option>
        <?
        }
        ?>
    </select>
</div>

<div class="nrf-fieldset-title"><?= Loc::getMessage('MAIN_REGISTER_SPECIALIZATION'); ?></div>
<div class="form-group">
    <label for="emplyement">
        <em class="hint">*</em>
        <?= Loc::getMessage('MAIN_REGISTER_PLACE_OF_EMPLOYMENT'); ?>
    </label>        
    <input type="text"
           value="<?= $arResult["VALUES"]['WORK_COMPANY'] ?>"
           id="emplyement"
           data-required="true"
           name="REGISTER[WORK_COMPANY]" class="form-control">
    <script>
        rulesAndMessages.rules["REGISTER[WORK_COMPANY]"] = {
            required: true
        };
        rulesAndMessages.messages["REGISTER[WORK_COMPANY]"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
        };
    </script>
</div>
<div class="form-group">
    <label for="userknown">
        <em class="hint">*</em>
        <?= Loc::getMessage(
            'MAIN_REGISTER_KNOWLEDGE'
        ); ?></label>

    <em class="error required"><?= Loc::getMessage(
            'MAIN_REGISTER_REQ'
        ); ?></em>
    <select name="UF_BRANCH_KNOWLEDGE" id="userknown"
            class="form-control" data-required="true">
        <option value=""><?= Loc::getMessage(
                'MAIN_REGISTER_SELECT'
            ); ?></option>
        <?
        $arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(
            array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE')
        );
        if (!empty($arUF_BRANCH_KNOWLEDGE)) {
            foreach ($arUF_BRANCH_KNOWLEDGE as $arItem) {
                ?>
                <option <?=
                $arResult["VALUES"]['UF_BRANCH_KNOWLEDGE']
                == $arItem['ID'] ? 'selected="selected"'
                    : '' ?>
                    value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
            <?
            }
        }
        ?>
    </select>
    <script>
        rulesAndMessages.rules["UF_BRANCH_KNOWLEDGE"] = {
            required: true
        };
        rulesAndMessages.messages["UF_BRANCH_KNOWLEDGE"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
        };
    </script>

</div>
<div class="form-group">
    <label for="usereducation">
        <em class="hint">*</em>
        <?= Loc::getMessage(
            'MAIN_REGISTER_EDUCATION'
        ); ?></label>

    <em class="error required"><?= Loc::getMessage(
            'MAIN_REGISTER_REQ'
        ); ?></em>
    <select name="UF_EDUCATION" id="usereducation"
            class="form-control" data-required="true">
        <option value=""><?= Loc::getMessage(
                'MAIN_REGISTER_SELECT'
            ); ?></option>
        <?
        $arUF_EDUCATION = nebUser::getFieldEnum(
            array('USER_FIELD_NAME' => 'UF_EDUCATION')
        );
        if (!empty($arUF_EDUCATION)) {
            foreach ($arUF_EDUCATION as $arItem) {
                ?>
                <option <?=
                $arResult["VALUES"]['UF_EDUCATION']
                == $arItem['ID'] ? 'selected="selected"'
                    : '' ?>
                    value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
            <?
            }
        }
        ?>
    </select>
    <script>
        rulesAndMessages.rules["UF_EDUCATION"] = {
            required: true
        };
        rulesAndMessages.messages["UF_EDUCATION"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
        };
    </script>
</div>


<div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_GROUP_SIGN'); ?></div>

<div class="form-group">
    <label for="useremail">
        <em class="hint">*</em>
        <?= Loc::getMessage(
            'MAIN_REGISTER_EMAIL'
        ); ?></label>

    <em class="error validate"><?= Loc::getMessage(
            'MAIN_REGISTER_EMAIL_FORMAT'
        ); ?></em>
    <em class="error required"><?= Loc::getMessage(
            'MAIN_REGISTER_REQ'
        ); ?></em>
    <input type="text" class="form-control" name="REGISTER[EMAIL]"
           id="useremail" data-validate="email"
           value="<?= $arResult["VALUES"]['EMAIL'] ?>"
           data-required="true">
    <script>
        rulesAndMessages.rules["REGISTER[EMAIL]"] = {
            required: true,
            email: true
        };
        rulesAndMessages.messages["REGISTER[EMAIL]"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>",
            email: "В адресе электронной почты обнаружена ошибка"
        };
    </script>
</div>
<div class="form-group">
    <!--div class="passecurity">
        <div class="passecurity_lb">Защищенность пароля</div>
    </div-->
    <label for="userpassword">
        <em class="hint">*</em>
        <?= Loc::getMessage('REGISTER_FIELD_PASSWORD'); ?></label>
        <?= Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN_VALID'); ?>
    <input type="password"
           data-required="true"
           value="<?= $arResult["VALUES"]['PASSWORD'] ?>"
           id="userpassword"
           name="REGISTER[PASSWORD]"
           class="form-control">
    <script>
        rulesAndMessages.rules["REGISTER[PASSWORD]"] = {
            required: true,
            minlength: 6,
            maxlength: 30,
            latin: true
        };
        rulesAndMessages.messages["REGISTER[PASSWORD]"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>",
            minlength: "<?= Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN'); ?>",
            maxlength: "<?= Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX'); ?>"
        };
    </script>
</div>
<div class="form-group">
    <label for="reenterpassword">
        <em class="hint">*</em>
        <?= Loc::getMessage(
            'REGISTER_FIELD_CONFIRM_PASSWORD'
        ); ?></label>
    <input type="password"
           data-required="true"
           value="<?= $arResult["VALUES"]['CONFIRM_PASSWORD'] ?>"
           id="reenterpassword"
           name="REGISTER[CONFIRM_PASSWORD]" class="form-control">
    <script>
        rulesAndMessages.rules["REGISTER[CONFIRM_PASSWORD]"] = {
            required: true,
            minlength: 6,
            maxlength: 30,
            equalTo: "#userpassword"
        };
        rulesAndMessages.messages["REGISTER[CONFIRM_PASSWORD]"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>",
            minlength: "<?= Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN'); ?>",
            maxlength: "<?= Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX'); ?>",
            equalTo: "<?= Loc::getMessage('MAIN_REGISTER_PASSWORD_MISMATCH'); ?>"
        };
    </script>
</div>
<div class="form-group">
    <label for="mobilephone">
        <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_MOBILE'); ?>
        <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_MOBILE_NOTE'); ?>
    </label>
    <input type="text" class="form-control"
           name="REGISTER[PERSONAL_MOBILE]" id="mobilephone"
           value="<?= $arResult["VALUES"]['PERSONAL_MOBILE'] ?>">
</div>


<div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_REGISTERED'); ?></div>

<div class="form-group">
    <label for="postindex">
        <em class="hint">*</em>
        <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP'); ?>
        (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?>)
    </label>

    <em class="error number"><?= Loc::getMessage(
            'REGISTER_FIELD_PERSONAL_ZIP_INCORRECT'
        ); ?></em>
    <input type="number"
           data-required="required"
           value="<?= $arResult["VALUES"]['PERSONAL_ZIP'] ?>"
           id="postindex" name="REGISTER[PERSONAL_ZIP]"           
           class="form-control ">
    <script>
        rulesAndMessages.rules["REGISTER[PERSONAL_ZIP]"] = {
            required: true
        };
        rulesAndMessages.messages["REGISTER[PERSONAL_ZIP]"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
        };
    </script>
</div>
<div class="form-group">
    <label for="region">
        <em class="hint">*</em>
        <?= Loc::getMessage('MAIN_REGISTER_REGION'); ?> 
        (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?>)
    </label>
    <input type="text" data-required="required"
           value="<?= $arResult["VALUES"]['UF_REGION'] ?>"
           id="region"
           name="UF_REGION"
           class="form-control">
    <script>
        rulesAndMessages.rules["UF_REGION"] = {
            required: true
        };
        rulesAndMessages.messages["UF_REGION"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
        };
    </script>
</div>
<div class="form-group">
    <label for="regionrayon">
        <em class="hint">*</em>
        <?= Loc::getMessage('MAIN_REGISTER_AREA'); ?>
        (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?>)
    </label>

    <input type="text"
           value="<?= $arResult["VALUES"]['UF_AREA'] ?>"
           id="regionrayon"
           name="UF_AREA"
           class="form-control">
    <script>
        rulesAndMessages.rules["UF_AREA"] = {
            required: true
        };
        rulesAndMessages.messages["UF_AREA"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
        };
    </script>
</div>

<div class="form-group">
    <label for="cityreg">
        <em class="hint">*</em>
        <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_CITY'); ?>
        (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?>)
    </label>
    <input type="text" data-required="required"
           value="<?= $arResult["VALUES"]['PERSONAL_CITY'] ?>"
           id="cityreg"
           name="REGISTER[PERSONAL_CITY]" class="form-control">
    <script>
        rulesAndMessages.rules["REGISTER[PERSONAL_CITY]"] = {
            required: true
        };
        rulesAndMessages.messages["REGISTER[PERSONAL_CITY]"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
        };
    </script>
</div>
<div class="form-group">
    <label for="streetreg">
        <em class="hint">*</em>
        <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_STREET'); ?>
        (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?>)
    </label>
    <input type="text"
           value="<?= $arResult["VALUES"]['PERSONAL_STREET'] ?>"
           id="streetreg"
           data-required="true"
           name="REGISTER[PERSONAL_STREET]"
           class="form-control">
    <script>
        rulesAndMessages.rules["REGISTER[PERSONAL_STREET]"] = {
            required: true
        };
        rulesAndMessages.messages["REGISTER[PERSONAL_STREET]"] = {
            required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
        };
    </script>
</div>
<div class="row">
    <div class="form-group col-xs-4">
        <label for="housereg">
            <em class="hint">*</em>
            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_HOUSE'); ?>:
        </label>

        <input type="text" class="form-control"
               name="UF_CORPUS"
               id="housereg"
               value="<?= $arResult["VALUES"]['UF_CORPUS'] ?>"
               data-required="true"/>            
        <script>
            rulesAndMessages.rules["UF_CORPUS"] = {
                required: true
            };
            rulesAndMessages.messages["UF_CORPUS"] = {
                required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
            };
        </script>
    </div>
    <div class="form-group col-xs-4">
        <label for="regstroenie">
            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_BUILDING'); ?>:
        </label>
        <input type="text" class="form-control"
               name="UF_STRUCTURE"
               maxlength="4" id="regstroenie"
               value="<?= $arResult["VALUES"]['UF_STRUCTURE'] ?>"/>
    </div>
    <div class="form-group col-xs-4">
        <label for="apartmentreg">
            <em class="hint">*</em>
            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_APPARTMENT'); ?>:
        </label>
        <input type="text" class="form-control" name="UF_FLAT"
               maxlength="4"
               id="apartmentreg"
               value="<?= $arResult["VALUES"]['UF_FLAT'] ?>"/>
        <script>
            rulesAndMessages.rules["UF_FLAT"] = {
                required: true
            };
            rulesAndMessages.messages["UF_FLAT"] = {
                required: "<?= Loc::getMessage('MAIN_REGISTER_REQ'); ?>"
            };
        </script>
    </div>
</div>


<div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RESIDENCE'); ?></div>

<div class="form-group">
    <label for="onreg" class="black">
        <input type="checkbox" name="UF_PLACE_REGISTR" id="onreg">
        <span class="lbl"><?= Loc::getMessage('REGISTER_GROUP_ADDRESS_CHECKBOX_NOTE'); ?></span>
    </label>
    <?
    if (!empty($arResult["VALUES"]['UF_PLACE_REGISTR'])) {
        ?>
        <?/*script type="text/javascript">
             $(function(){
                 $('input[name="UF_PLACE_REGISTR"]').click();
             });
        </script*/?>
    <?
    }
    ?>
    <?/*script type="text/javascript">
         $(function(){
             $('#cb3r').change(function () {
                 $(this).attr('value', +$(this)[0].checked);
                 $(".hidden-fieldgroup").each(function () {
                     $(this).attr('value', $("input[name='" + $(this).attr('name') + "'][type != 'hidden']").attr('value'));
                 })
             });
         });
    </script*/?>
</div>
 
<!--input type="hidden" class="hidden-fieldgroup" data-validate="number" value="" name="REGISTER[WORK_ZIP]"-->
<div class="living-address-group">
    <div class="form-group">
        <label for="liveindex">
            <em class="hint">*</em>
            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP'); ?>
            (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?>)
        </label>
        <input type="number" data-required="required"
               value="<?= $arResult["VALUES"]['WORK_ZIP'] ?>"
               id="liveindex" name="REGISTER[WORK_ZIP]"
               data-mirror="postindex"
               class="form-control">
        <script>
            rulesAndMessages.rules["REGISTER[WORK_ZIP]"] = {
                required: true
            };
            rulesAndMessages.messages["REGISTER[WORK_ZIP]"] = {
                required: "<?=Loc::getMessage('MAIN_REGISTER_FILL');?>"
            };
        </script>
    </div>

    <!--input type="hidden" class="hidden-fieldgroup" value="" name="UF_REGION2"-->

    <div class="form-group">
        <label for="liveregion">
            <em class="hint">*</em>
            <?= Loc::getMessage('MAIN_REGISTER_REGION'); ?>
            (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?>)
        </label>
        <input type="text"
               value="<?= $arResult["VALUES"]['UF_REGION2'] ?>"
               id="liveregion" data-required="required"
               name="UF_REGION2"
               data-mirror="region"
               class="form-control">
        <script>
            rulesAndMessages.rules["UF_REGION2"] = {
                required: true
            };
            rulesAndMessages.messages["UF_REGION2"] = {
                required: "<?=Loc::getMessage('MAIN_REGISTER_FILL');?>"
            };
        </script>
    </div>

    <!--input type="hidden" class="hidden-fieldgroup" value="" name="UF_AREA2"-->

    <div class="form-group">
        <label for="livearea">
            <em class="hint">*</em>
            <?= Loc::getMessage('MAIN_REGISTER_AREA'); ?>
            (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?>)
        </label>
        <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
        <input type="text"
               value="<?= $arResult["VALUES"]['UF_AREA2'] ?>"
               id="livearea" data-required="required"
               name="UF_AREA2"
               data-mirror="regionrayon"
               class="form-control">
        <script>
            rulesAndMessages.rules["UF_AREA2"] = {
                required: true
            };
            rulesAndMessages.messages["UF_AREA2"] = {
                required: "<?=Loc::getMessage('MAIN_REGISTER_FILL');?>"
            };
        </script>
    </div>

    <!--input type="hidden" class="hidden-fieldgroup" value="" name="REGISTER[WORK_CITY]"-->

    <div class="form-group">
        <label for="livecity">
            <em class="hint">*</em>
            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_CITY'); ?>
            (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?>)
        </label>
        <input type="text"
               value="<?= $arResult["VALUES"]['WORK_CITY'] ?>"
               id="livecity" data-required="required"
               name="REGISTER[WORK_CITY]"
               data-mirror="cityreg"
               class="form-control">
        <script>
            rulesAndMessages.rules["REGISTER[WORK_CITY]"] = {
                required: true
            };
            rulesAndMessages.messages["REGISTER[WORK_CITY]"] = {
                required: "<?=Loc::getMessage('MAIN_REGISTER_FILL');?>"
            };
        </script>
    </div>

    <!--input type="hidden" class="hidden-fieldgroup" value="" name="REGISTER[WORK_STREET]"-->

    <div class="form-group">
        <label for="livestreet">
            <em class="hint">*</em>
            <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_STREET'); ?>
            (<?= Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?>)
        </label>
        <input type="text"
               value="<?= $arResult["VALUES"]['WORK_STREET'] ?>"
               id="livestreet" data-required="required"
               data-mirror="streetreg"
               name="REGISTER[WORK_STREET]" class="form-control">
        <script>
            rulesAndMessages.rules["REGISTER[WORK_STREET]"] = {
                required: true
            };
            rulesAndMessages.messages["REGISTER[WORK_STREET]"] = {
                required: "<?=Loc::getMessage('MAIN_REGISTER_FILL');?>"
            };
        </script>
    </div>

    <!--input type="hidden" class="hidden-fieldgroup" value="" name="UF_HOUSE2">
    <input type="hidden" class="hidden-fieldgroup" value="" name="UF_STRUCTURE2">
    <input type="hidden" class="hidden-fieldgroup" value="" name="UF_FLAT2"-->

    <div class="row">
        <div class="form-group col-xs-4">
            <label for="livehouse">
                <em class="hint">*</em>
                <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_HOUSE'); ?>:
            </label>
            <input type="text" class="form-control" data-required="required"
                   name="UF_HOUSE2"
                   id="livehouse"
                   data-mirror="housereg"
                   value="<?= $arResult["VALUES"]['UF_HOUSE2'] ?>"/>
            <script>
                rulesAndMessages.rules["UF_HOUSE2"] = {
                    required: true
                };
                rulesAndMessages.messages["UF_HOUSE2"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_FILL');?>"
                };
            </script>
        </div>
        <div class="form-group col-xs-4">
            <label for="livebuilding">
                <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_BUILDING'); ?>:
            </label>
            <input type="text" class="form-control" data-required="required"
                   name="UF_STRUCTURE2"
                   maxlength="4" id="livebuilding"
                   data-mirror="regstroenie"
                   value="<?= $arResult["VALUES"]['UF_STRUCTURE2'] ?>"/>
        </div>
        <div class="form-group col-xs-4">
            <label for="liveappartment">
                <em class="hint">*</em>
                <?= Loc::getMessage('REGISTER_FIELD_PERSONAL_APPARTMENT'); ?>:
            </label>
            <input type="text" class="form-control" name="UF_FLAT2"
                   maxlength="4" data-required="required"
                   id="liveappartment"
                   data-mirror="apartmentreg"
                   value="<?= $arResult["VALUES"]['UF_FLAT2'] ?>"/>
            <script>
                rulesAndMessages.rules["UF_FLAT2"] = {
                    required: true
                };
                rulesAndMessages.messages["UF_FLAT2"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_FILL');?>"
                };
            </script>
        </div>
    </div>
</div>

<?php if ('Y' === $arParams['NEED_SCANS']) { ?>
    <hr>
    <? $APPLICATION->IncludeComponent(
        "notaext:plupload",
        "scan_passport1_reg",
        array(
            "MAX_FILE_SIZE"     => "10",
            "FILE_TYPES"        => "jpg,jpeg,png",
            "DIR"               => "tmp_register",
            "FILES_FIELD_NAME"  => "profile_file",
            "MULTI_SELECTION"   => "N",
            "CLEANUP_DIR"       => "Y",
            "UPLOAD_AUTO_START" => "Y",
            "RESIZE_IMAGES"     => "Y",
            "RESIZE_WIDTH"      => "4000",
            "RESIZE_HEIGHT"     => "4000",
            "RESIZE_CROP"       => "Y",
            "RESIZE_QUALITY"    => "98",
            "UNIQUE_NAMES"      => "Y",
        ),
        false
    ); ?>


    <? $APPLICATION->IncludeComponent(
        "notaext:plupload",
        "scan_passport2_reg",
        array(
            "MAX_FILE_SIZE"     => "10",
            "FILE_TYPES"        => "jpg,jpeg,png",
            "DIR"               => "tmp_register",
            "FILES_FIELD_NAME"  => "profile_file",
            "MULTI_SELECTION"   => "N",
            "CLEANUP_DIR"       => "Y",
            "UPLOAD_AUTO_START" => "Y",
            "RESIZE_IMAGES"     => "Y",
            "RESIZE_WIDTH"      => "4000",
            "RESIZE_HEIGHT"     => "4000",
            "RESIZE_CROP"       => "Y",
            "RESIZE_QUALITY"    => "98",
            "UNIQUE_NAMES"      => "Y",
        ),
        false
    ); ?>
<? } ?>

<hr>

















<div class="fileupload-widget" data-upload-widget>
    <span class="btn btn-link fileinput-button">
        <input id="fileupload1" data-fileuploader-exemplar type="file" name="files[0]">
        Загрузите скан разворота паспорта с фотографией
    </span>
    <span class="selected-filename"></span>
    <span class="btn btn-primary" data-upload-button>Отправить</span>
</div>
<div class="fileupload-widget" data-upload-widget>
    <span class="btn btn-link fileinput-button">
        <input id="fileupload2" data-fileuploader-exemplar type="file" name="files[1]">
        Загрузите скан разворота паспорта с пропиской
    </span>
    <span class="selected-filename"></span>
    <span class="btn btn-primary" data-upload-button>Отправить</span>
</div>
<script>
$(function () {
    $('[data-fileuploader-exemplar]').each(function(index){
        var $inputFile = $(this),
            $index = index
            $inputId = $(this).attr('id');
        $(this).fileupload({
            dataType: 'json',
            /* url: '/fileupload/server/php/', */
            url: '/uploaded/',
            autoUpload: false,
            context: $(this)[0],
            formData: {
                widgetContext: $inputId
            },
            dropZone: $(this),
            done: function (e, data) {  
                $($inputFile).parent().next().text('');
                $.each(data.result.files, function (index, file) {                    
                    $($inputFile).parent().next().append(file.name);

                });
            },
            add: function(e, data, $inputFile_) {                
                /*console.log(data);*/
                $(e.target).closest('[data-upload-widget]').find('[data-upload-button]')
                    .off('click')
                    .on('click', function(){                        
                        $(data).submit();
                    });
                $.each(data.files, function (index, file) {
                    /*console.log('yo'+file.name+' index'+index);*/
                    $(e.target).parent().next().append(file.name);
                });
            },
            /*submit: function (e, data) {
                 console.log('www');
                 data.formData = {xxx: 'yomf'};
                 $(e.target).fileupload({
                       formData: {
                              param1: 'test'
                       }
                 });
             },*/
        });

    });

    $('[data-fileuploader-exemplar]').each(function(){
        var self = this;
        $.ajax({
        /* Uncomment the following to send cross-domain cookies:
        xhrFields: {withCredentials: true},
        paramName: widgetContext,*/
            url: $(this).fileupload('option', 'url'),
            dataType: 'json',
            context: $(this)[0]
        }).done(function (result) {
            /*console.log(result);*/
            $(this).fileupload('option', 'done')
                .call(this, null, {result: result});
        });
    });

});
</script>

<hr>
<!-- old 
<a href="/local/components/neb/registration/templates/.default/ajax_agreement.php">
    <?= Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP_TERMS'); ?>
    <?= GetMessage("AUTH_REGISTER") ?>
    <?= Loc::getMessage('MAIN_REGISTER_READ_TERMS'); ?><br/>
</a>
 /old -->
<input type="hidden" name="termsreaded" value="false"/>
<input type="hidden" name="termsagreed" value="false"/>

<p>
    <?= Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP'); ?> 
    <a href="#" data-agree-dialog>
        <?= Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP_TERMS'); ?>
    </a>
</p>
<p>
    <button type="submit" class="btn btn-primary" disabled>
        <?= Loc::getMessage('MAIN_REGISTER_REGISTER'); ?>
    </button>
    <button class="btn btn-default" id="cancelbutton">Отмена</button>
</p>



<script>
    $(function(){
        var regForm = $('.new-reader-form');
        /* пользовательское соглашение */
        $(document).on('click', '[data-agree-dialog]', function(e){
            e.preventDefault();
            $('#universal-modal').modal();
        }).on('change', '#agreecheckbox', function(){
            $('[name="termsagreed"]', regForm).val( $(this).prop('checked') ).trigger('change');
        }).on('change', '[name="termsagreed"]', function(){
            if ( JSON.parse( $('[name="termsagreed"]').val() ) ) {
                $('[type="submit"]', regForm).removeAttr('disabled');
            } else {
                $('[type="submit"]', regForm).attr('disabled','disabled');
            }
        });
        $('#universal-modal').on('show.bs.modal', function(){
            var modal = $(this);
            modal.find('.modal-dialog').toggleClass('modal-lg', true).toggleClass('fixed-modal', true);
            modal.find('.modal-title').text('Пользовательское соглашение');
            var opts = {
                  lines: 17 /* The number of lines to draw*/
                , length: 18 /* The length of each line */
                , width: 7 /* The line thickness */
                , radius: 19 /* The radius of the inner circle */
                , scale: 0.55 /* Scales overall size of the spinner */
                , corners: 0 /* Corner roundness (0..1) */
                , color: '#00708c' /* #rgb or #rrggbb or array of colors */
                , opacity: 0 /* Opacity of the lines */
                , rotate: 0 /* The rotation offset */
                , direction: 1 /* 1: clockwise, -1: counterclockwise */
                , speed: 1 /* Rounds per second */
                , trail: 99 /* Afterglow percentage */
                , fps: 20 /* Frames per second when using setTimeout() as a fallback for CSS */
                , zIndex: 1 /* The z-index (defaults to 2000000000) */
                , className: 'spinner' /* The CSS class to assign to the spinner */
                , top: '50%' /* Top position relative to parent */
                , left: '50%' /* Left position relative to parent */
                , shadow: false /* Whether to render a shadow */
                , hwaccel: false /* Whether to use hardware acceleration */
                , position: 'absolute' /* Element positioning */
                };
            var target = modal.find('.modal-body')[0];
            var spinner = new Spinner(opts).spin(target);
            var checkbox = $('<input>')
                .attr('type','checkbox')
                .prop('id','agreecheckbox')
                .prop('checked', JSON.parse( $('[name="termsagreed"]', regForm).val() ) );
            if ( $(checkbox).prop('checked') === false && $('[name="termsreaded"').val() == 'false' ) {
                $(checkbox).attr('disabled','disabled');
            }
            var checkboxlabel = $('<span>')
                .attr('class','lbl')
                .text('Я согласен с правилами');
            var checkboxcontrol = $('<label>')
                .attr('class','agree-control')
                .append(checkbox)
                .append(checkboxlabel);
            modal.find('.modal-body').load('/user-agreement/agree-text.php', function(){
                modal.find('.modal-footer').prepend(checkboxcontrol);
                spinner.stop;                
            }).on('scroll', function(){
                var docHeight = $(this).find('.agree-list').height(),
                    viewportHeight = $(this).height(),
                    scrolled = $(this)[0].scrollTop;

                if ( scrolled > (docHeight - viewportHeight - 200) && $(checkbox).attr('disabled') == 'disabled' ) {
                    $('[name="termsreaded"').val('true');
                    $(checkbox).removeAttr('disabled');
                }
            });
        }).on('hide.bs.modal', function(){
            $(this)
                .find('.modal-dialog').toggleClass('modal-lg', false).toggleClass('fixed-modal', false).end()
                .find('.modal-title').text('').end()
                .find('.modal-body').text('').off('scroll').end()
                .find('.agree-control').remove();
        });
        /*./пользовательское соглашение*/

        /* валидация */

        function getAge(dateString) {
            var today = new Date();
            var d = parseInt(dateString[0] + dateString[1]),
                m = parseInt(dateString[3] + dateString[4]),
                y = parseInt(dateString[6] + dateString[7] + dateString[8] + dateString[9]),
                born = new Date(y, m - 1, d),
                age = Math.ceil( (today.getTime() - born) / (24 * 3600 * 365.25 * 1000) );
            return age;
        }

        jQuery.validator.addMethod("latin", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9\w]{6,30}/.test(value);
        }, "Используйте только латинские буквы и цифры");
        jQuery.validator.addMethod("FullDate", function() {
            /*if all values are selected */
            if($("#PERSONAL_BIRTHDAY_D").val() != "" && $("#PERSONAL_BIRTHDAY_M").val() != "" && $("#PERSONAL_BIRTHDAY_Y").val() != "") {
                return true;
            } else {
                return false;
            }
        }, "Введите дату рождения");
        jQuery.validator.addMethod("checkAge", function(value, element) {
            /*only compare date if all items have a value
            console.log( getAge(value) );*/
            return ( getAge(value) > 11 );
            }, 'Вы должны быть старше 12 лет'
        );



        $('#regform').validate({
            rules: rulesAndMessages.rules,
            messages: rulesAndMessages.messages,            
            errorPlacement: function(error, element) {
                if ( element.attr('type') == "radio" ) {
                    error.appendTo( $(element).closest('.form-group').find('label')[0] );
                } else {
                    error.appendTo(element.parent());
                }
                $(element).closest('.form-group').toggleClass('has-error', true);
            },
            /*specifying a submitHandler prevents the default submit, good for the demo*/
            submitHandler: function() {
                alert("valid!");
            },
            /* set this class to error-labels to indicate valid fields*/
            success: function(label) {
                /* set &nbsp; as text for IE */
                label.html("&nbsp;").addClass("resolved");
            },
            highlight: function(element, errorClass, validClass) {        
                $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                $(element).parent().find("." + errorClass).removeClass("resolved");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
            }
        });
        
        $('#fullbirthday').on('change', function(){
            $('#regform').validate().element(this);
        });

        /*$('[data-mirror]').each(function(){
             var id = $(this).data('mirror'),
                thisId = $(this).prop('id');
             $('#'+id).attr('data-mirrored-by', thisId);
        });*/

        function mirrorValue(id) {

        }

        $(document).on('keyup change', '[data-mirrored-by]', function(){
            var id = $(this).data('mirrored-by'),
                sameAddresses = $('#onreg').prop('checked');
            if (sameAddresses) {
                $('#'+id).val( $(this).val() );
            }
        });

        $(document).on('change', '#onreg', function(){
            var checked = $(this).prop('checked');
            if ( checked ) {
                $('[data-mirror]').each(function(){
                    var id = $(this).data('mirror');
                    $(this).val( $('#'+id).val() );
                });
                $('.living-address-group').find('input[type="text"],input[type="number"]').each(function(){
                    $(this).attr('readonly','true');
                });
            } else {
                $('.living-address-group').find('input[type="text"],input[type="number"]').each(function(){
                    $(this).removeAttr('readonly');
                });
            }            
        });

        $("#cancelbutton").on('click', function(e){
            e.preventDefault();
        });

    });
</script>
