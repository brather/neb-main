<?php
/**
 * User: agolodkov
 * Date: 17.05.2016
 * Time: 9:32
 */

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BitrixCelarCache extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('bitrix:cache:clear')
            ->setDescription('Load library statistics')
            ->addArgument(
                'run-time',
                InputArgument::OPTIONAL,
                'run time seconds'
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);

        $this->getContainer()
            ->get('bitrix')
            ->requireProlog(
                $this->getContainer()
                    ->get('kernel')
                    ->getRootDir()
            );

        if (!class_exists('CFileCacheCleaner')) {
            require_once($_SERVER['DOCUMENT_ROOT']
                . '/bitrix/modules/main/classes/general/cache_files_cleaner.php');
        }


        $curentTime = mktime();
        $endTime = time() + (integer)$input->getArgument('run-time');
        $path = '';
        $obCacheCleaner = new \CFileCacheCleaner('all');
        if (!$obCacheCleaner->InitPath($path)) {
            $output->writeln('Произошла ошибка');

            return;
        }

        $obCacheCleaner->Start();
        $deleteCount = 0;
        $deleteSize = 0;
        while ($file = $obCacheCleaner->GetNextFile()) {
            if (is_string($file)) {
                $date_expire = $obCacheCleaner->GetFileExpiration($file);
                if ($date_expire) {
                    if ($date_expire < $curentTime) {
                        $deleteSize += filesize($file);
                        unlink($file);
                        $deleteCount++;
                    }
                }
                if ($endTime > 0 && time() >= $endTime) {
                    break;
                }
            }
        }

        $output->writeln(
            'Files: ' . $deleteCount
        );
        $output->writeln(
            'Files size: ' . ($deleteSize / 1024 / 1024) . ' Mb'
        );

        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}