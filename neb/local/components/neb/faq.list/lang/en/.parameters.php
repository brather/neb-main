<?php
$MESS["T_IBLOCK_DESC_ASC"] = "Ascending";
$MESS["T_IBLOCK_DESC_DESC"] = "Descending";
$MESS["T_IBLOCK_DESC_LIST_ID"] = "Information block code";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Information block type";

$MESS["TITLE_BLOCK_NAME_PROJECT"] = "Name of project";
$MESS["TITLE_BLOCK_NAME_PROJECT_VALUE"] = "NEL";
$MESS["TITLE_BLOCK_NAME"] = "Name of block for user";
$MESS["TITLE_BLOCK_NAME_VALUE"] = "Questions and answers";
$MESS["SHOW_TITLE_BLOCK_NAME"] = "Show name of block for user";
$MESS["TITLE_BLOCK_DESCRIPTION"] = "Description for user";
$MESS["TITLE_BLOCK_DESCRIPTION_VALUE"] = "In this section you finded frequently questions from users.";
$MESS["TITLE_BLOCK_URL_FEEDBACK_PAGE"] = "Url page for feedback";
?>