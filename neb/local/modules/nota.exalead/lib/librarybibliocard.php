<?php
/**
 * User: agolodkov
 * Date: 21.08.2015
 * Time: 10:36
 */

namespace Nota\Exalead;

use \Bitrix\Main\Application;
use \Bitrix\Main\DB\MysqliConnection;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Type\DateTime;
use \Neb\Main\Helper\DbHelper;
use \Neb\Main\Stat\ReaderSessionTable;

/**
 * Class LibraryBiblioCardTable
 *
 * @package Nota\Exalead
 */
class LibraryBiblioCardTable extends BiblioCardTable
{
    /**
     * @return string
     */
    public static function getConnectionName()
    {
        $sConnectionName = 'library';
        static::setConnectionCharset($sConnectionName);
        return $sConnectionName;
    }

    /**
     * Установка кодировки соединения
     */
    private static function setConnectionCharset($sConnectionName) {
        $connection = Application::getInstance()->getConnectionPool()->getConnection($sConnectionName);
        if ($connection instanceof MysqliConnection)
            mysqli_set_charset($connection->getResource(), 'utf8');
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array_merge(
            array_intersect_key(
                parent::getMap(),
                array(
                    'Id'                     => true,
                    'ALIS'                   => true,
                    'IdFromALIS'             => true,
                    'Author2'                => true,
                    'Author'                 => true,
                    'Name'                   => true,
                    'SubName'                => true,
                    'PublishYear'            => true,
                    'PublishPlace'           => true,
                    'Publisher'              => true,
                    'ISBN'                   => true,
                    'Language'               => true,
                    'CountPages'             => true,
                    'Printing'               => true,
                    'Format'                 => true,
                    'Series'                 => true,
                    'VolumeNumber'           => true,
                    'VolumeName'             => true,
                    'Collection'             => true,
                    'AccessType'             => true,
                    'isLicenseAgreement'     => true,
                    'Responsibility2'        => true,
                    'Responsibility'         => true,
                    'Annotation'             => true,
                    'ContentRemark'          => true,
                    'CommonRemark'           => true,
                    'PublicationInformation' => true,
                    'pdfLink'                => true,
                    'IdParent'               => true,
                    'CreationMethod'         => true,
                    'CreationActor'          => true,
                    'CreationDateTime'       => true,
                    'UpdatingActor'          => true,
                    'UpdatingDateTime'       => true,
                    'BBKText'                => true,
                    'UDKText'                => true,
                    'LanguageText'           => true,
                    'IdRubrics'              => true,
                    'Indexes'                => true,
                    'FullSymbolicId'         => true,
                    'ParentId2'              => true,
                    'ParentIdFull'           => true,
                    'ParentIdFull2'          => true,
                    'IdPdfLink'              => true,
                    'Title'                  => true,
                    'PublishYearNorm'        => true,
                    'PublishYearMin'         => true,
                    'PublishYearMax'         => true,
                    'RSLId'                  => true,
                    'IsSchool'               => true,
                    'CountView'              => true,
                    'UDKFull'                => true,
                    'BBKFull'                => true,
                    'NewCollection'          => true,
                )
            ),
            array(
                'CountView'      => array(
                    'data_type'     => 'integer',
                    'title'         => Loc::getMessage(
                        'BIBLIO_CARD_ENTITY_COUNTVIEW_FIELD'
                    ),
                    'default_value' => 0,
                ),
                'is_delete'      => array(
                    'data_type'     => 'integer',
                    'default_value' => 0,
                ),
                'FullSymbolicId' => array(
                    'data_type' => 'string',
                ),
            )
        );
    }

    /**
     * @param string $bookId
     * @param string $session
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function applyViewBook($bookId, $session)
    {
        if (!ReaderSessionTable::checkSession($session, $bookId)) {
            $connection = static::getEntity()->getConnection();

            $connection->query(
                sprintf(
                    <<<SQL
UPDATE tbl_common_biblio_card
SET CountView = CountView + 1, UpdatingDateTime = NOW()
WHERE FullSymbolicId = '%s';
SQL
                    , static::_mysqlEscapeString($bookId)
                )
            );
            ReaderSessionTable::apply($session, $bookId);

            return $connection->getAffectedRowsCount() > 0 ? true : false;
        }

        return false;
    }

    /**
     * @param string $string
     *
     * @return string
     */
    private static function _mysqlEscapeString($string)
    {
        return DbHelper::mysqlEscapeString($string);
    }

    /**
     * @param string $bookId
     * @param bool   $closed
     *
     * @return bool
     */
    public static function setClosed($bookId, $closed)
    {
        $closed = (boolean)$closed;
        $connection = static::getEntity()->getConnection();

        $bookId = static::_mysqlEscapeString($bookId);
        $book = $connection->query(
            sprintf(
                <<<SQL
SELECT AccessType
FROM tbl_common_biblio_card
WHERE FullSymbolicId = '%s';
SQL
                , static::_mysqlEscapeString($bookId)
            )
        )->fetch();

        $book['AccessType'] = (boolean)$book['AccessType'];
        if ($book['AccessType'] !== $closed) {
            $connection->query(
                sprintf(
                    <<<SQL
UPDATE tbl_common_biblio_card
SET AccessType = %d
WHERE FullSymbolicId = '%s';
SQL
                    , (integer)$closed
                    , static::_mysqlEscapeString($bookId)
                )
            );

            return $connection->getAffectedRowsCount() > 0 ? true : false;
        }

        return false;
    }


    /**
     * @param string $pdfPath
     * @param string $bookId
     * @param null   $libraryId
     *
     * @return bool
     */
    public static function setPdfLink($pdfPath, $bookId, $libraryId = null)
    {
        $query
            = <<<SQL
UPDATE tbl_common_biblio_card tcbc
%s
SET tcbc.pdfLink = '%s', tcbc.UpdatingDateTime = NOW()
WHERE FullSymbolicId = '%s'
%s
SQL;
        $joinItems = array();
        $whereItems = array();
        if (null !== $libraryId) {
            $libraryId = (integer)$libraryId;
            $joinItems[]
                = <<<SQL
INNER JOIN tbl_libraries_alis tla ON tla.Id = tcbc.ALIS
SQL;
            $whereItems[]
                = <<<SQL
AND tla.Library = $libraryId
SQL;
        }
        $query = sprintf(
            $query,
            implode($joinItems),
            static::_mysqlEscapeString($pdfPath),
            static::_mysqlEscapeString($bookId),
            implode($whereItems)
        );
        $connection = static::getEntity()->getConnection();
        $connection->query($query);

        return $connection->getAffectedRowsCount() > 0 ? true : false;
    }

    /**
     * @param string $bookId
     * @param null   $libraryId
     *
     * @return bool
     */
    public static function deleteFunds($bookId, $libraryId = null)
    {
        $set = array(
            'tcbc.pdfLink = CONCAT(tcbc.pdfLink, "_")',
            'tcbc.UpdatingDateTime = NOW()',
            'tcbc.is_delete = 1',
        );
        $query
            = <<<SQL
UPDATE tbl_common_biblio_card tcbc
%s
SET %s
WHERE FullSymbolicId = '%s'
%s
SQL;
        $joinItems = array();
        $whereItems = array(
            <<<SQL
AND tcbc.is_delete is null
SQL
        );
        if (null !== $libraryId) {
            $libraryId = (integer)$libraryId;
            $joinItems[]
                = <<<SQL
INNER JOIN tbl_libraries_alis tla ON tla.Id = tcbc.ALIS
SQL;
            $whereItems[]
                = <<<SQL
AND tla.Library = $libraryId
SQL;
        }
        $query = sprintf(
            $query,
            implode(PHP_EOL, $joinItems),
            implode(',', $set),
            static::_mysqlEscapeString($bookId),
            implode(PHP_EOL, $whereItems)
        );
        $connection = static::getEntity()->getConnection();
        $connection->query($query);

        return $connection->getAffectedRowsCount();
    }

    /**
     * @param string|array $id
     *
     * @return array
     * @throws \Exception
     */
    public static function getByFullSymbolicId($id)
    {
        if (is_string($id)) {
            $id = array($id);
        }
        $id = array_filter($id);
        if (empty($id)) {
            throw new \Exception('Empty FullSymbolicId');
        }
        foreach ($id as $key => $value) {
            $id[$key] = '"' . static::_mysqlEscapeString($value) . '"';
        }
        $query
            = <<<SQL
SELECT *
FROM tbl_common_biblio_card
WHERE FullSymbolicId IN (
  %s
);
SQL;
        $query = sprintf($query, implode(',' . PHP_EOL, $id));
        $result = static::getEntity()->getConnection()->query($query);

        return $result->fetchAll();
    }

    /**
     * @param int $requestId
     *
     * @return int|null
     * @throws \Exception
     */
    public static function confirmRequest($requestId)
    {
        $bookData = static::_buildBookDataFromRequest($requestId);
        $bookRecordId = null;
        if (empty($bookData['FullSymbolicId'])) {
            $addResult = static::add($bookData);
            if ($addResult->isSuccess()) {
                $bookRecordId = $addResult->getId();
            } else {
                throw new \Exception(implode($addResult->getErrorMessages()));
            }
        } else {
            $book = static::getByFullSymbolicId($bookData['FullSymbolicId']);
            if (isset($book[0])) {
                $book = $book[0];
            }

            if (!isset($book['Id'])) {
                throw new \Exception(
                    'Book ' . $bookData['FullSymbolicId'] . ' not found'
                );
            }
            unset($bookData['ALIS']);
            unset($bookData['FullSymbolicId']);
            unset($bookData['IdFromALIS']);
            $updatingResult = static::update($book['Id'], $bookData);
            if ($updatingResult->getAffectedRowsCount()) {
                $bookRecordId = $book['Id'];
            }
        }
        if ($bookRecordId > 0) {
            BiblioCardTable::update($requestId, array('status' => 'confirmed'));
        }

        return $bookRecordId;
    }

    /**
     * @param int $requestId
     *
     * @return array
     * @throws \Exception
     */
    protected static function _buildBookDataFromRequest($requestId)
    {
        $request = BiblioCardTable::getRow(
            array(
                'filter' => array(
                    'Id' => $requestId,
                ),
                'select' => array(
                    'Author',
                    'Name',
                    'IdFromALIS',
                    'PublishYear',
                    'ISBN',
                    'pdfLink',
                    'FullSymbolicId',
                    'request_type',
                ),
            )
        );
        if (null === $request) {
            throw new \Exception('Request not found');
        }
        $rowAlis = BiblioAlisTable::getRow(
            array(
                'filter' => array(
                    'PathAndFileName' => $request['request_type']
                ),
                'select' => array(
                    'Id'
                )
            )
        );
        if (null === $rowAlis) {
            throw new \Exception(
                'Alis not found for type ' . $request['request_type']
            );
        }
        $request['ALIS'] = $rowAlis['Id'];

        /** Достаем ID библиотеки */
        $alisData = BiblioAlisTable::getById($request['ALIS'])->fetch();
        $library = 0;
        if (isset($alisData['Library'])) {
            $library = $alisData['Library'];
        }

        /** Сохраняем файло и получаем его path для экзалида */
        if (isset($request['pdfLink'])) {
            $request['pdfLink'] = BooksAdd::buildPdfLink(
                $request['pdfLink'],
                $request['IdFromALIS'],
                str_pad((string)$library, 6, '0', STR_PAD_LEFT)
            );
        }

        $request['UpdatingDateTime'] = new DateTime();

        $request = array_filter($request);

        return array_intersect_key(
            $request,
            array(
                'Author'           => true,
                'Name'             => true,
                'PublishYear'      => true,
                'ISBN'             => true,
                'pdfLink'          => true,
                'ALIS'             => true,
                'FullSymbolicId'   => true,
                'IdFromALIS'       => true,
                'UpdatingDateTime' => true,
            )
        );
    }
}