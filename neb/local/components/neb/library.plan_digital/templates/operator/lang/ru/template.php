<?
$MESS['LIBRARY_PLAN_DIGITAL_IN_LIST'] = "Книг в плане на оцифровку";
$MESS['LIBRARY_PLAN_DIGITAL_PRINT'] = "Печать";
$MESS['LIBRARY_PLAN_DIGITAL_EXPORT_EXCEL'] = "Экспорт в Excel";
$MESS['LIBRARY_PLAN_DIGITAL_ADD_BOOKS'] = "Добавить издания";
$MESS['LIBRARY_PLAN_DIGITAL_AUTHOR'] = "Автор";
$MESS['LIBRARY_PLAN_DIGITAL_NAMEDIG'] = "Название / Описание / Есть ли в планах на оцифровку";
$MESS['LIBRARY_PLAN_DIGITAL_DATE_ADD'] = "Дата добавления запроса";
$MESS['LIBRARY_PLAN_DIGITAL_DATEDIG'] = "Оцифровать <br />до даты";
$MESS['LIBRARY_PLAN_DIGITAL_COMMENT'] = "Комментарий";
$MESS['LIBRARY_PLAN_DIGITAL_REMOVE_FROM_PLAN'] = "Удалить <br>из плана";
$MESS['LIBRARY_PLAN_DIGITAL_DESCRIPTION'] = "Описание";
$MESS['LIBRARY_PLAN_DIGITAL_IN_DIG'] = "На оцифровке";
$MESS['LIBRARY_PLAN_DIGITAL_REMOVE'] = "Удалить";
$MESS['LIBRARY_PLAN_DIGITAL_FROM_PLAN'] = "из Плана оцифровки";
$MESS['LIBRARY_PLAN_DIGITAL_HEADING'] = "Заглавие";
$MESS['LIBRARY_PLAN_DIGITAL_OUTPUT'] = "Выходные данные";
$MESS['LIBRARY_PLAN_DIGITAL_PHYS_DESC'] = "Физическое описание";
$MESS['LIBRARY_PLAN_DIGITAL_LIB'] = "Библиотека";
$MESS['LIBRARY_PLAN_DIGITAL_WHO_DIG'] = "Оцифровывает";
?>
