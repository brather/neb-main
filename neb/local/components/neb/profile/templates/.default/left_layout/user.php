<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<!-- МЕНЮ В САЙДБАРЕ -->
<? $APPLICATION->IncludeComponent(
    'bitrix:menu', 'lk_left', Array(
    'ROOT_MENU_TYPE'        => 'left_' . LANGUAGE_ID,
    'MENU_CACHE_TYPE'       => 'N',
    'MENU_CACHE_TIME'       => '3600',
    'MENU_CACHE_USE_GROUPS' => 'N',
    'MENU_CACHE_GET_VARS'   => array(0 => '',),
    'MAX_LEVEL'             => '2',
    'CHILD_MENU_TYPE'       => '',
    'USE_EXT'               => 'Y',
    'DELAY'                 => 'N',
    'ALLOW_MULTI_SELECT'    => 'N',
),
    false
); ?>

<!-- /МЕНЮ В САЙДБАРЕ -->
<? include __DIR__ . '/../includes/side-right.php'; ?>

<? $APPLICATION->IncludeComponent(
    'neb:user.right.menu', '',
    Array(
        'TYPE'              => 'books',
        'COLLECTION_URL'    => $arResult['FOLDER']
            . $arResult['URL_TEMPLATES']['my_library_collection'],
        'ALL_BOOKS_URL'     => $arResult['FOLDER']
            . $arResult['URL_TEMPLATES']['my_library'],
        'READING_BOOKS_URL' => $arResult['FOLDER']
            . $arResult['URL_TEMPLATES']['my_library_reading'],
        'READ_BOOKS_URL'    => $arResult['FOLDER']
            . $arResult['URL_TEMPLATES']['my_library_read'],
    )
    , false
);
?>

<?
$APPLICATION->IncludeComponent(
    'exalead:search.page.viewer.right',
    '',
    Array(
        'PARAMS' => $arReturn['arParams'],
        'RESULT' => $arReturn['arResult'],
    ),
    $component
);
?>

