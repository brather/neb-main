<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper clearfix ">

    <? $APPLICATION->IncludeComponent(
        "neb:library.funds.manage",
        "operator",
        Array(
            "COUNTONPAGE" => 30
        ),
        false
    );
    ?>
</section>
