<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * User: D-Efremov
 * Date: 15.04.2016
 * Time: 16:06
 */

use \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper,
    \Neb\Main\Library\NebLibsCityTable;

Loader::includeModule("highloadblock");
Loader::includeModule("nota.exalead");

$obDBResult = new \CDBResult(); //постраничная навигация
$obPageOption = new \CPageOption();
$obIBlockElement = new \CIBlockElement();

$obPageOption->SetOptionString("main", "nav_page_in_session", "N");

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

// тип выбираемых данных
if(!isset($arParams["LIBRARY_TYPE"]))
	$arParams["LIBRARY_TYPE"] = 'library';

/*******************************************************************************************************************
 *                                       Установка текущего города
 *******************************************************************************************************************/

//получение текущего города пользователя
$currentCity = false;
if ($_GET['city_id']) {
	setcookie('library_city', $_GET['city_id'], time() + 60 * 60 * 24 * 365 * 100, '/');
	$_COOKIE['library_city'] = $_GET['city_id'];
}

//установлен ли город в куках?
$arResult["IS_COOKIE_SET"] = isset($_COOKIE['library_city'])  && intval($_COOKIE['library_city']) > 0;

if($arResult["IS_COOKIE_SET"]){
	//установим id из куки, если она есть
	$currentCity = intval($_COOKIE['library_city']);
} else {
	//установим id из базы. ищем LIKE по списку городов
	$queryCity = TheDistance::getCurPosition();

	$cityData = NebLibsCityTable::getList(array(
		"select" => array("ID", "UF_CITY_NAME"),
		"filter" => array("%UF_CITY_NAME" => $queryCity["city"]),
		"limit" => 1
	))->Fetch();

	if($cityData["ID"] > 0)
		$currentCity = $cityData["ID"];
	else //установим из константы
		$currentCity = LIBRARY_DEFAULT_CITY;
}

//данные о текущем городе
$cityData = NebLibsCityTable::getByID($currentCity)->Fetch();
$cityData["UF_POS"] = explode(" ", $cityData["UF_POS"]);
$arResult["CITY"] = $cityData;

$arNavigation = $obDBResult->GetNavParams(array("nPageSize" => $arParams['ROWS_PER_PAGE']));
if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
	$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];

$arFindBookLibs = $arNebLibrary = $arNebLibraryMap = $arWorkplaces = $arWorkplacesMap = array();

/*******************************************************************************************************************
 *                                Поиск книги по ближайшим библиотекам на странице книги
 *******************************************************************************************************************/
if (!empty($_REQUEST['book_idparent2'])) {
	$query = new \Nota\Exalead\SearchQuery('idparent2:(' . str_replace(array('/', '\\'), '', $_REQUEST['book_idparent2']) . ') ');
	$query->setPageLimit(1000);
	$query->setParam('sl', 'sl_nofuzzy');
	$client = new \Nota\Exalead\SearchClient();
	$result = $client->getResult($query);
	if (!empty($result['ITEMS']))
		foreach ($result['ITEMS'] as $library)
			$arFindBookLibs[intval($library['idlibrary'])] = true;
} elseif (isset($_REQUEST['idlibrary']) && !empty($_REQUEST['idlibrary'])) {
	$arFindBookLibs[intval($_REQUEST['idlibrary'])] = true;
}

if($this->StartResultCache(false, array($arParams["LIBRARY_TYPE"], $arResult["CITY"], $arNavigation, $arResult["IS_COOKIE_SET"], $arFindBookLibs))){

	//для правильной нумерации библиотек при переходе по страниц
	$arResult["NAV_OFFSET"] = ((is_set($_GET['PAGEN_1']) ? $_GET['PAGEN_1'] : 1)-1) * intval($arParams['ROWS_PER_PAGE']);

	/*******************************************************************************************************************
	 *                    Выборка данных по библиотекам, участникам НЭБ и виртуальным читальным залам
	 *******************************************************************************************************************/

	// Выборка всех библиотек + вычисление координаты от установленного города до каждой библиотеки (их нет на странице участниц НЭБ)
	if ($arParams["LIBRARY_TYPE"] != 'participants') {

		$arFilter = array();
		if (!empty($arFindBookLibs))
			$arFilter['ID'] = array_keys($arFindBookLibs);

		$libraryHLBlock = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
		$libraryEntity = HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();
		$libraryData = $libraryEntity::getList(array(
			"select" => array("ID", "UF_NAME", "UF_ADRESS", "UF_POS"),
			"order"  => array("ID"),
			"filter" => $arFilter,
		));
		while ($arItem = $libraryData->Fetch()) {

			if (isset($arItem['UF_POS'])) {

				$arItem["UF_POS"] = explode(" ", $arItem["UF_POS"]);

				// убираем нули в конце - иначе глючит карта
				$arItem["UF_POS"][0] = rtrim($arItem["UF_POS"][0], '0');
				$arItem["UF_POS"][1] = rtrim($arItem["UF_POS"][1], '0');

				$arItem['DISTANCE'] = intval(TheDistance::calculateTheDistance(
					$arItem["UF_POS"][1],
					$arItem["UF_POS"][0],
					$arResult["CITY"]["UF_POS"][1],
					$arResult["CITY"]["UF_POS"][0]
				));
			}

			$arAllLibrary[] = array(
				'ID'       => $arItem['ID'],
				'NAME'     => $arItem['UF_NAME'],
				'ADDRESS'  => $arItem['UF_ADRESS'],
				'UF_POS'   => $arItem['UF_POS'],
				'DISTANCE' => $arItem['DISTANCE'],
			);
		}
		unset($libraryHLBlock, $libraryEntity, $libraryData, $arItem);
	}

	// выборка библиотек-участниц НЭБ (их нет на странице залов НЭБ)
	if ($arParams["LIBRARY_TYPE"] != 'workplaces') {
		$rsLibraries = $obIBlockElement->GetList(
			array(),
			array(
				"IBLOCK_ID"     => IBLOCK_ID_LIBRARY,
				"ACTIVE"        => "Y",
				"!PROPERTY_MAP" => false
			),
			false,
			false,
			array(
				"ID",
				"IBLOCK_ID",
				"NAME",
				"DETAIL_PAGE_URL",
				"PROPERTY_MAP",
				"PROPERTY_ADDRESS",
				"PROPERTY_SCHEDULE",
				"PROPERTY_LIBRARY_LINK",
				"PROPERTY_STATUS"
			)
		);
		while ($arItem = $rsLibraries->GetNext(true, false)) {

			// расчет местоположения
			if (!empty($arItem['PROPERTY_MAP_VALUE'])) {

				$arItem['PROPERTY_MAP_VALUE'] = explode(",", $arItem['PROPERTY_MAP_VALUE']);

				if (!empty($arItem['PROPERTY_MAP_VALUE'])) // меняем местами - требование формата вывода
					$arItem['PROPERTY_MAP_VALUE'] = array($arItem['PROPERTY_MAP_VALUE'][1], $arItem['PROPERTY_MAP_VALUE'][0]);

				// убираем нули в конце - иначе глючит карта
				$arItem['PROPERTY_MAP_VALUE'][0] = rtrim($arItem['PROPERTY_MAP_VALUE'][0], '0');
				$arItem['PROPERTY_MAP_VALUE'][1] = rtrim($arItem['PROPERTY_MAP_VALUE'][1], '0');

				$arItem["DISTANCE"] = intval(TheDistance::calculateTheDistance(
					$arItem['PROPERTY_MAP_VALUE'][1],
					$arItem['PROPERTY_MAP_VALUE'][0],
					$arResult["CITY"]["UF_POS"][1],
					$arResult["CITY"]["UF_POS"][0]
				));
			}

			$arNebLibrary[$arItem['ID']] = array(
				'ID'       => $arItem['ID'],
				'NAME'     => $arItem['NAME'],
				'ADDRESS'  => $arItem['PROPERTY_ADDRESS_VALUE']?:'', // может быть не заполнен
				'IS_NEB'   => $arParams["LIBRARY_TYPE"] == 'library',
				'URL'      => $arItem['DETAIL_PAGE_URL'],
				'SCHEDULE' => $arItem['PROPERTY_SCHEDULE_VALUE']?:'',
				'STATUS'   => $arItem['PROPERTY_STATUS_VALUE']?:'',
				'UF_POS'   => $arItem['PROPERTY_MAP_VALUE'],
				'DISTANCE' => $arItem['DISTANCE'],
			);

			$arNebLibraryMap[$arItem['PROPERTY_LIBRARY_LINK_VALUE']] = $arItem['ID'];
		}
		unset($rsLibraries, $arItem);
	}

	// выборка всех читальных залов (их нет на странице участниц НЭБ)
	if ($arParams["LIBRARY_TYPE"] != 'participants') {
		$rsReadRooms = $obIBlockElement->GetList(
			array(),
			array(
				"IBLOCK_ID" => IBLOCK_ID_WORKPLACES,
				"ACTIVE" => "Y"
			),
			false,
			false,
			array('ID', 'IBLOCK_ID', 'PROPERTY_LIBRARY')
		);
		while ($arItem = $rsReadRooms->GetNext(true, false)) {
			$arWorkplaces[$arItem['ID']] = $arItem;
			$arWorkplacesMap[$arItem['PROPERTY_LIBRARY_VALUE']] = $arItem['ID'];
		}
		unset($rsReadRooms, $arItem);
	}

	/*******************************************************************************************************************
	 *                                       Формирование результата
	 *******************************************************************************************************************/

	if ($arParams["LIBRARY_TYPE"] == 'library') {

		$arResult["ITEMS"] = $arAllLibrary;

		// добавление к основным библиотекам библиотек НЭБ и ВЧЗ
		foreach($arResult["ITEMS"] as &$item) {
			$arLib = $arNebLibrary[$arNebLibraryMap[$item['ID']]];
			$arWorkplace = $arWorkplaces[$arWorkplacesMap[$item["ID"]]];
			if (!empty($arLib)) {

				//подменим детальную страницу и другие свойства для связанных библиотек
				$item["IS_NEB"]   = true;
				$item["URL"]      = $arLib["URL"];
				$item["SCHEDULE"] = $arLib["SCHEDULE"];
				$item["STATUS"]   = $arLib["STATUS"];

				if (strrpos($item["STATUS"], "/") !== false)	{
					$arProperties = explode("/", $item["STATUS"]);
					if (LANGUAGE_ID == 'ru')
						$item["STATUS"] = $arProperties[0];
					else if (LANGUAGE_ID == 'en')
						$item["STATUS"] = $arProperties[1];
				}

				if ($item["STATUS"])
					$item["DISTANCE"] = -10000000 - $item["DISTANCE"];
				else
					$item["DISTANCE"] = -1000000 - $item["DISTANCE"];

				// проверка есть ли ВЧЗ
				if (!empty($arWorkplace))
					$item["WCHZ"] = true;
			}
		}
	}
	// Библиотеки-участники НЭБ
	else if ($arParams["LIBRARY_TYPE"] == 'participants') {

		$arResult["ITEMS"] = $arNebLibrary;
	}

	unset($arAllLibrary, $arNebLibrary, $arNebLibraryMap, $arWorkplaces, $arWorkplacesMap);

	//сортировка библиотек по удаленности
	usort($arResult["ITEMS"], function($a, $b){
		return ($a["DISTANCE"] - $b["DISTANCE"]);
	});

	//постраничная навигация
    foreach($arResult["ITEMS"] as &$arItem)
    	$arItems2[] = $arItem;
    $obDBResult->InitFromArray($arResult["ITEMS"]);
    $obDBResult->NavStart(intval($arParams['ROWS_PER_PAGE']));
    while($item = $obDBResult->Fetch())
    	$arResult["ITEMS_PAGEN"][] = $item;

    $arResult['ITEMS'] = $arItems2;

	$arResult["NAVIGATION"] = $obDBResult->GetPageNavStringEx();

	$this->SetResultCacheKeys(array(
		"ITEMS_PAGEN",
		"NAVIGATION",
		"ITEMS",
	));

	$this->EndResultCache();
}

unset($obDBResult, $obPageOption, $obIBlockElement);

// отправка данных в виде JSON-массива
if($_REQUEST['json'] == 'Y'){

	$APPLICATION->RestartBuffer();
	$arPoints = $arJson = array();

	//поиск по подстроке
	if($_REQUEST["type"] == "city" && isset($_REQUEST["term"])){
		$query = htmlspecialcharsbx($_REQUEST["term"]);

		$cityData = NebLibsCityTable::getList(array(
			"select" => array("id" => "ID", "label" => "UF_CITY_NAME"),
			"filter" => array("%UF_CITY_NAME" => $query)
		));
		while($c = $cityData->Fetch())
			$arJson[] = $c;

	} else {

		if(isset($arResult['ITEMS'])){

			foreach($arResult['ITEMS'] as $arItem) {
                $arPoints[] = array(
                    'id' => $arItem['ID'],
                    'address' => $arItem['ADDRESS'],
                    'name' => $arItem['NAME'],
                    'status' => $arItem['STATUS'], //есть не всегда
                    'parten' => $arItem["IS_NEB"],
                    'wchz' => $arItem["WCHZ"],
                    'link' => $arItem['URL'], //есть не всегда
                    'available' => $arItem['SCHEDULE'], //есть не всегда
                    'coordinates' => array($arItem["UF_POS"][1], $arItem["UF_POS"][0]),
                );
            }

			$arJson['libs'][] = array(
				'id' => 1,
				'placemarksize' => array(34, 48),
				'placemarkicon' => '/local/templates/.default/markup/i/pin.png',
				'regions' =>array(
					array(
						'current' => true,
						'city' 	=> $arResult["CITY"]["UF_CITY_NAME"],
						'id'	=> $arResult["CITY"]["ID"],
						'coordinates'	=> array($arResult["CITY"]['UF_POS'][1], $arResult["CITY"]['UF_POS'][0]),
						'metro' =>
						array(
							array(
								'station' => '',
								'id' => 1,
								'points' => $arPoints,
							)
						)
					)
				)
			);
		}
	}

    MainHelper::showJson($arJson);
}

unset($arResult['ITEMS']);

$arResult['FIND_LOCATION'] = !$arResult['IS_COOKIE_SET'] && !isset($_GET['city_id']);

$this->IncludeComponentTemplate();