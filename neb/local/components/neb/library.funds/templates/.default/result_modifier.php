<?
CModule::IncludeModule("nota.userdata");
use Nota\UserData\Books;

if ($USER->IsAuthorized())
    $arResult['USER_BOOKS'] = Books::getListCurrentUser();
