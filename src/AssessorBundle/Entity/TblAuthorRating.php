<?php

namespace AssessorBundle\Entity;

use AssessorBundle\Traits\AssessorEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as Serialization;

/**
 * TblAuthorRaiting
 *
 * @ORM\Table(name="tbl_author_raiting", uniqueConstraints={@ORM\UniqueConstraint(name="UK_tbl_author_raiting_Author", columns={"Author"})})
 * @ORM\Entity(repositoryClass="AssessorBundle\Entity\Repository\CommonRepository")
 * @UniqueEntity(fields="author", message="Такой автор уже существует")
 */
class TblAuthorRating implements AssessorRatingEntityInterface
{
    use AssessorEntityTrait;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serialization\Groups({"list", "details"})
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Author", type="string", length=330, nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    public $author;

    /**
     * @var integer
     *
     * @ORM\Column(name="Raiting", type="integer", nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    public $rating;

    /**
     * @var integer
     *
     * @ORM\Column(name="BookCount", type="integer", nullable=false)
     */
    public $bookcount = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return int
     */
    public function getBookcount()
    {
        return $this->bookcount;
    }

    /**
     * @param int $bookcount
     */
    public function setBookcount($bookcount)
    {
        $this->bookcount = $bookcount;
    }
}
