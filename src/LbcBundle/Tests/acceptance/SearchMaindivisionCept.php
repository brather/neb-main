<?php use LbcBundle\AcceptanceTester;
$I = new AcceptanceTester($scenario);
$I->wantTo('Search maindivision');
$query = 'ПЕРИОД СТАНОВЛЕНИЯ И ФОРМИРОВАНИЯ ФЕОДАЛЬНЫХ ОТНОШЕНИИ (V-XI вв.)';
$I->amOnPage('/bbk/search/?maindivision=' . urlencode($query));
$I->see('Т3(4А)41');