<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
	//\CUtil::InitJSCore(array("jquery"));
	$APPLICATION->AddHeadString('<script>window.jQuery || document.write(\'<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js">\x3C/script>\')</script>',true);
	$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);
?>

<div id="container_<?=$arParams['RAND_STR']?>" class="pl_button">
	<input type="button" id="plupload_pickfiles_<?=$arParams['RAND_STR']?>" value="<?=$arParams['MULTI_SELECTION'] == 'Y' ? GetMessage('SELECT_FILES') : GetMessage('SELECT_FILE')?>"/>&nbsp;
	<?
		if($arParams['UPLOAD_AUTO_START'] == 'N'){
		?>
		<input type="button" id="plupload_uploadfiles_<?=$arParams['RAND_STR']?>" value="<?=GetMessage('UPLOAD')?>"/><br />
		<?
		}
	?>
</div>

<div id="plupload_filelist_<?=$arParams['RAND_STR']?>"><?=GetMessage('BROWSER_NOT_SUPPORTED')?></div>
<pre id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>

<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'plupload_pickfiles_<?=$arParams['RAND_STR']?>',
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'), 

				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
					unique_names: true,
					<?
					}
				?>
				
				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				<?
					if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
					{
					?>
					resize: {
						width: <?=intval($arParams['RESIZE_WIDTH'])?>,
						height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
						<?
							if($arParams['RESIZE_CROP'] == 'Y')
							{
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=intval($arParams['RESIZE_QUALITY'])?>
					},
					<?
					}
				?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '1mb',
				dragdrop: true,
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'
				},
				multi_selection: <?=$arParams['MULTI_SELECTION'] == 'Y' ? 'true' : 'false'?>,
				init: {
					PostInit: function() {
						document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').innerHTML = '';
					<?
						if($arParams['UPLOAD_AUTO_START'] == 'N'){
						?>
						document.getElementById('plupload_uploadfiles_<?=$arParams['RAND_STR']?>').onclick = function() {
						uploader_<?=$arParams['RAND_STR']?>.start();
						return false;
						}
					<?
						}
					?>
				},
				FilesAdded: function(up, files) {
					<?
						if($arParams['MULTI_SELECTION'] != 'Y'){
						?>
						document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').innerHTML = '';
						<?
						}
					?>
					plupload.each(files, function(file) {
						document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
					});
					
					<?
						if(!empty($arParams['UPLOAD_AUTO_START']) and $arParams['UPLOAD_AUTO_START'] == 'Y'){
						?>
						uploader_<?=$arParams['RAND_STR']?>.start();
						<?
						}
					?>					
				},
				UploadProgress: function(up, file) {
					<?
						if($arParams['MULTI_SELECTION'] != 'Y'){
						?>
						document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
						<?
						} else {
						?>
						document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
						<?
						}
					?>
				},
				FileUploaded: function(up, file, response) {
					var result = response.response;
					if (result) {
						var obResponse = JSON.parse(result);
						<?
							if($arParams['MULTI_SELECTION'] == 'Y'){
								$arParams['FILES_FIELD_NAME'] = $arParams['FILES_FIELD_NAME']."[][VALUE]";	
							}

						?>
						document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').innerHTML += '<input type="hidden" name="<?=$arParams['FILES_FIELD_NAME']?>" value="' + obResponse.file + '">';
					}
				},
				Error: function(up, err) {
					document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
				}
			}
		});

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>