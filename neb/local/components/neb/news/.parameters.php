<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "SEF_MODE" => array(
            "list" => array(
                "NAME" => "LIST PAGE",
                "DEFAULT" => "",
                "VARIABLES" => array()
            ),
            "section" => array(
                "NAME" => "SECTION PAGE",
                "DEFAULT" => "#SECTION_CODE#/",
                "VARIABLES" => array("SECTION_CODE")
            ),
            "element" => array(
                "NAME" => "DETAIL PAGE",
                "DEFAULT" => "#SECTION_CODE#/#ELEMENT_CODE#/",
                "VARIABLES" => array("SECTION_CODE", "ELEMENT_CODE")
            ),
        ),
    ),
);