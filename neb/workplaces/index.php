<?php
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);


$APPLICATION->SetTitle(Loc::getMessage('WORKPLACES'));
$APPLICATION->SetPageProperty(
    "main_title", "<h2 class='category-title'>".Loc::getMessage('VIRTUAL_READING_ROOMS')."</h2>"
);

\Bitrix\Main\Page\Asset::getInstance()->addCss('/local/templates/adaptive/css/library-map.css');
/**
 * @var array $arResult
 * @var CMain $APPLICATION
 */
echo '<p>С помощью карты или формы поиска найдите наиболее подходящий вам по расположению электронный читальный зал - специально оборудованное помещение, в котором организован доступ к фондам НЭБ</p>';
$APPLICATION->IncludeComponent(
    'neb:workplaces', 'neb', array(
        'cityId' => $_GET['city_id']
    )
);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");