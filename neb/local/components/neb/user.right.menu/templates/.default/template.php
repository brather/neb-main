<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<h3><?=GetMessage("USER_RIGHT_TITLE");?></h3>

<?/*form action="<?=$this->__folder?>/remove.php"*/?>
	<div class="side-collections" data-tag-side-widget data-remove-url="<?=$this->__folder?>/remove.php" data-edit-url="/local/tools/collections/edit.php">
		<? if ( !empty($arResult['COLLECTIONS']) ) {
			if($_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
				$APPLICATION->RestartBuffer();

			foreach($arResult['COLLECTIONS'] as $arItem)
			{
				?>				
				<div class="side-collections__item itemid<?=(!empty($arParams['COLLECTION_ID']) and $arParams['COLLECTION_ID'] == $arItem['ID']) ? ' current':''?>" data-tag-side-item="<?=$arItem['ID']?>">
					<a href="<?=$arItem['URL']?>" class="side-collections__item-title" data-tag-title><?=$arItem['UF_NAME']?></a>
					<span class="side-collections__item-count">(<?=(int)$arItem['cnt']?>)</span>					
					<button class="side-collections__item-save" data-tag-rename title="Сохранить"></button>
					<button class="side-collections__item-edit" data-tag-edit title="<?php echo GetMessage("USER_RIGHT_EDIT");?>"></button>
					<button class="side-collections__item-delete" data-tag-delete title="<?php echo GetMessage("USER_RIGHT_REMOVE");?>"></button>
				</div>
				<?
			}

			if($_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
				exit();
		}
		elseif ( $_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )
		{
			$APPLICATION->RestartBuffer();
			exit();
		}?>
	</div>
<?/*/form*/?>

<div class="side-collections__control" data-add-tag-widget-placeholder>
	<a href="#" class="side-collections__add"><?=GetMessage("USER_RIGHT_CREATE_COLLECTION");?></a>
</div>

   		