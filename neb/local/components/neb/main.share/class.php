<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Блок "Поделиться". Документация:
 * http://api.yandex.ru/share/
 * http://api.yandex.ru/share/doc/dg/concepts/share-button-ov.xml
 * https://habrahabr.ru/post/278459/
 * http://dev.1c-bitrix.ru/community/webdev/user/30640/blog/12501/
 */
class YandexShareComponent  extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        global $APPLICATION;

        $sDomain = URL_SCHEME . '://' . $_SERVER['HTTP_HOST']; // SITE_HOST

        if (empty($arParams['DATA_URL'])) {
            $arParams['DATA_URL'] = $sDomain . $APPLICATION->GetCurUri();
        } else {
            $arParams['DATA_URL'] = $sDomain . $arParams['DATA_URL'];
        }

        if (!empty($arParams['DATA_TITLE'])) {
            $arParams['DATA_TITLE'] = $this->prepareStr($arParams['DATA_TITLE']);
        }
        if (!empty($arParams['DATA_DESCRIPTION'])) {
            $arParams['DATA_DESCRIPTION'] = $this->prepareStr($arParams['DATA_DESCRIPTION']);
        }

        if (!empty($arParams['DATA_IMAGE'])) {
            $arParams['DATA_IMAGE'] = $sDomain . $arParams['DATA_IMAGE'];
        }
        // требуется для вконтакта, не умеющего обрабатывать Open Graph
        else {
            $arParams['DATA_IMAGE'] = $sDomain . "/local/templates/adaptive/img/logo-socialnetworks.png";
        }

        if (empty($arParams['THEME_SERVICES'])) {
            $arParams['THEME_SERVICES'] = 'vkontakte,facebook,odnoklassniki';
        } else {
            $arParams['THEME_SERVICES'] = implode(',', $arParams['THEME_SERVICES']);
        }

        $arParams['THEME_LANG']    = LANGUAGE_ID;
        $arParams['THEME_BARE']    = $arParams['THEME_BARE'] == 'Y';
        $arParams['THEME_COUNTER'] = $arParams['THEME_COUNTER'] == 'Y';

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Функция подготоваливает строки title и description к публикации
     *
     * @param $str
     * @param int $iMaxLength
     * @return mixed|string
     */
    private function prepareStr($str, $iMaxLength = 250) {

        $str  = strip_tags(htmlspecialchars_decode($str));              // декод html-сучностей и удаление тегов
        $str  = trim(str_replace(["'", '"', '&', '<', '>'], '', $str)); // удаление спецсимволов и обрезка строки
        $str  = preg_replace("/\s{2,}/",' ',$str);                      // удаление лишних пробелов (больше 2 шт)
        $iLen = mb_strlen($str);                                        // получение длины предварительной строки
        $str  = mb_substr($str, 0, $iMaxLength);                        // ограничение максимальной длины
        if ($iLen > $iMaxLength) {
            $str .= '...';
        }
        return $str;
    }

    /**
     * Устанавливает свойства страницы для Open Graph
     */
    private function setPageProperties() {

        global $APPLICATION;

        if (!empty($this->arParams['DATA_TITLE'])) {
            $APPLICATION->SetPageProperty('title', htmlspecialchars_decode($this->arParams['DATA_TITLE']));
        }
        if (!empty($this->arParams['DATA_DESCRIPTION'])) {
            $APPLICATION->SetPageProperty('description', htmlspecialchars_decode($this->arParams['DATA_DESCRIPTION']));
        }
        if (!empty($this->arParams['DATA_IMAGE'])) {
            $APPLICATION->SetPageProperty('image', htmlspecialchars_decode($this->arParams['DATA_IMAGE']));
        }
    }

    public function executeComponent()
    {
        $this->setPageProperties();
        $this->includeComponentTemplate();
    }
}