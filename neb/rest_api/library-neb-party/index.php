<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;
$APPLICATION->IncludeComponent(
    'rest:library.neb.party',
    '',
    array(
        'SEF_MODE'          => 'Y',
        'SEF_FOLDER'        => '/rest_api/library-neb-party/',
        'CACHE_TYPE'        => 'N',
        'CACHE_TIME'        => '3600',
        'DOCUMENTATION'     => 'Y',
        'SERVICE_NAME'      => 'API Участников НЭБ',
        'SEF_URL_TEMPLATES' => array(
            'processRequest' => '',
        )
    ),
    false
);