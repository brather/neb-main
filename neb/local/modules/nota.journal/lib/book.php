<?php
namespace Nota\Journal;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);


class BookTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'neb_journal.book';
    }

    public static function getConnectionName()
    {
        return 'journal';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            new Entity\EnumField('EVENT', array(
                'values' => array('ADD', 'EDIT', 'DELETE', 'VIEW')
            )),
            'USER' => array(
                'data_type' => 'integer',
            ),
            'USER_DATA' => array(
                'data_type' => 'text',
            ),
            'TIMESTAMP' => array(
                'data_type' => 'datetime',
            ),
            'BOOK_ID' => array(
                'data_type' => 'text',
            ),
            'AUTHOR' => array(
                'data_type' => 'text',
            ),
            'NAME' => array(
                'data_type' => 'text',
            ),
            'YEAR' => array(
                'data_type' => 'string',
            ),
            'LIB' => array(
                'data_type' => 'text',
            ),
            'PLACE' => array(
                'data_type' => 'text',
            ),
            'PUBLISHER' => array(
                'data_type' => 'text',
            ),
            'LANG' => array(
                'data_type' => 'string',
            ),
            'BBK' => array(
                'data_type' => 'text',
            ),
            'COLLECTION' => array(
                'data_type' => 'text',
            ),
            'FUND' => array(
                'data_type' => 'text',
            ),
            'METHOD' => array(
                'data_type' => 'text',
            ),
            'VOLUME' => array(
                'data_type' => 'text',
            ),
            'SYSTEM' => array(
                'data_type' => 'text',
            ),
        );
    }
}