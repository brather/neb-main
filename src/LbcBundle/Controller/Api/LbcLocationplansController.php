<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 17:11
 */

namespace LbcBundle\Controller\Api;


use Doctrine\Bundle\DoctrineBundle\Registry;
use LbcBundle\Form\LbcLocationplansType;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * Class LbcMaindivisionController
 *
 * @Route("/api/bbk")
 * @DI\Service("app.api.locationplans", scope="request", public=true)
 */
class LbcLocationplansController extends AbstractCategoryController
{
    /**
     * @return LbcLocationplansType
     */
    public function getFormType()
    {
        return new LbcLocationplansType();
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcLocationplans';
    }

    /**
     * @return string
     */
    public function getCategoryOwnerFieldName()
    {
        return 'maindivisionid';
    }
}