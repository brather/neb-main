<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 20:01
 */

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serialization;


class Suggest
{
    /**
     * @var string
     * @Serialization\Groups({"list", "details"})
     */
    public $entry;

    /**
     * @var int
     * @Serialization\Groups({"list", "details"})
     */
    public $score;

    /**
     * @var string
     * @Serialization\Groups({"list", "details"})
     */
    public $source;
}