<?php
$MESS['REMOVE_BOOK_AJAX_DELETE_SUCCESS'] = 'The publication has been successfully removed from the plan.';
$MESS['REMOVE_BOOK_AJAX_DELETE_ERROR'] = 'Failure to remove publications from the plan. Try later.';
$MESS['REMOVE_BOOK_AJAX_UPDATE_SUCCESS'] = 'Issue has been successfully updated.';
$MESS['REMOVE_BOOK_AJAX_UPDATE_ERROR'] = 'Error updating publications. Try to update later.';