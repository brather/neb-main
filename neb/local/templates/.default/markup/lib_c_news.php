
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			
		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right">
						<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		<nav class="b-commonnav noborder">
			<a href="#" class="current">О библиотеке</a>
			<a href="#">фонды</a>
			<a href="#">коллекции</a>
		</nav>
		<div class="b-typical_news">
			<h2 class="mode">новости библиотеки</h2>
			<div class="anonce clearfix">
				<img src="./pic/pic_45.jpg" alt="" align="left">  
				<div class="descr">
					<span class="date">10 сентября</span>	
					<h3>В Ульяновске появилась библиотека XXI века</h3>
				</div>
			</div>
			<p>Сотню картин, ставших классикой мирового кинематографа, предложили ученые из Российского института истории исскуств, Санкт-Петербургского государственного университета кино и телевидения, Научно-исследовательского института киноисскуства, Госфильмофонда России, а так же - члены Союза кинематографистов РФ.</p>
			<p>Принять участие в формировании "большого списка" омг любой желабщий - Министерство культуры устроило публичное голосование на своем сайте. После чего эксперты отобрали те самые "100 фильмов зарубежной классики". Среди рекомендованного - Луис Бунюэль, Джордж Лукас, Акира Куросава, Эмир Кустурица. С аннотациями, датой выхода, списком актеров.</p>
			<div class="b-slidernews">
				<div class="slide">
					<img src="./pic/pic_46.jpg" alt="">  
					<div class="text">Маяковский: "В этой жизни помереть не трудно"</div>
				</div>
				<div class="slide">
					<img src="./pic/pic_46.jpg" alt="">  
					<div class="text">Маяковский: "В этой жизни помереть не трудно"</div>
				</div>
				<div class="slide">
					<img src="./pic/pic_46.jpg" alt="">  
					<div class="text">Маяковский: "В этой жизни помереть не трудно"</div>
				</div>
				<div class="slide">
					<img src="./pic/pic_46.jpg" alt="">  
					<div class="text">Маяковский: "В этой жизни помереть не трудно"</div>
				</div>
			</div>
			<p><iframe width="100%" height="394" src="//www.youtube.com/embed/ycp5Wn3RguE" frameborder="0" allowfullscreen></iframe></p>
			<p><a href="#" class="icpdf"><span>Скачать календарный план</span></a></p>
		</div><!-- /.b-typical_news -->
			<div class="b-viewhnews">
					<h2>другие новости</h2>

					<div class="b-newslider slidetype">
						<div>
							<div class="b-newsslider_item">
								<div class="date">10 сентября</div>	
								<a href="#">В Ульяновске появилась библиотека XXI века</a>	
								<p>На III Международном форуме «Культура нового поколения» в Ульяновске генеральный директор Библиотеки им. Рудомино, директор благотворительного фонда «Институт толерантности» </p>		

							</div>
						</div>
						<div>
							<div class="b-newsslider_item">
								<div class="date">5 сентября</div>	
								<a href="#">В Москве появятся круглосуточные библиотеки</a>	
								<p>В следующем году в каждом округе может появиться круглосуточная библиотека, сообщает Московский городской библиотечный центр (МГБЦ)</p>	
							</div>	
						</div>
						<div>
							<div class="b-newsslider_item">
								<div class="date">10 сентября</div>	
								<a href="#">Российской национальной библиотеке — 200 лет!</a>	
								<p>На III Международном форуме «Культура нового поколения» в Ульяновске генеральный директор Библиотеки им. Рудомино, директор благотворительного фонда «Институт толерантности» </p>	
							</div>	
						</div>
						<div>
							<div class="b-newsslider_item">
								<div class="date">14 сентября</div>	
								<a href="#">В Ульяновске появилась библиотека XXI века</a>	
								<p>6 сентября в петербургской Российской национальной библиотеке начинается цикл лекций, посвященных 200-летию со дня ее открытия</p>		
							</div>
						</div>
						<div>
							<div class="b-newsslider_item">
								<div class="date">23 сентября</div>	
								<a href="#">Российская государственная библиотека: идти в ногу со временем</a>	
								<p>«Открытая библиотека», совместный проект РГБ и ОАО «АК «Транснефть», при поддержке краудсорсинговой платформы citycelebrity.ru открывают прием проектов по развитию РГБ на 2013–2014 годы</p>	
							</div>	
						</div>
						<div>
							<div class="b-newsslider_item">
								<div class="date">10 сентября</div>	
								<a href="#">В Ульяновске появилась библиотека XXI века</a>	
								<p>На III Международном форуме «Культура нового поколения» в Ульяновске генеральный директор Библиотеки им. Рудомино, директор благотворительного фонда «Институт толерантности» </p>	
							</div>	
						</div>
						<div>
							<div class="b-newsslider_item">
								<div class="date">5 сентября</div>	
								<a href="#">В Москве появятся круглосуточные библиотеки</a>	
								<p>В следующем году в каждом округе может появиться круглосуточная библиотека, сообщает Московский городской библиотечный центр (МГБЦ)</p>	
							</div>	
						</div>
						<div>
							<div class="b-newsslider_item">
								<div class="date">10 сентября</div>	
								<a href="#">Российской национальной библиотеке — 200 лет!</a>	
								<p>На III Международном форуме «Культура нового поколения» в Ульяновске генеральный директор Библиотеки им. Рудомино, директор благотворительного фонда «Институт толерантности» </p>	
							</div>
						</div>
						<div>
							<div class="b-newsslider_item">
								<div class="date">14 сентября</div>	
								<a href="#">В Ульяновске появилась библиотека XXI века</a>	
								<p>6 сентября в петербургской Российской национальной библиотеке начинается цикл лекций, посвященных 200-летию со дня ее открытия</p>
							</div>	
						</div>
						<div>
							<div class="b-newsslider_item">
								<div class="date">23 сентября</div>	
								<a href="#">Российская государственная библиотека: идти в ногу со временем</a>	
								<p>«Открытая библиотека», совместный проект РГБ и ОАО «АК «Транснефть», при поддержке краудсорсинговой платформы citycelebrity.ru открывают прием проектов по развитию РГБ на 2013–2014 годы</p>		
							</div>
						</div>
					</div> <!-- /.b-newslider -->
					
				</div><!-- /.b-viewhnews -->


	</div><!-- /.b-mainblock -->
	<div class="b-side right">
			<div class="b-libfond">
				<div class="b-fond_img">
					<img src="./pic/pic_31.png" alt="">
				</div>
				<span class="b-fondinfo_number_lb">В фонде библиотеки</span>
				<span class="b-portalinfo_numbers">
					<span class="b-portalinfo_number mr iblock">1</span>
					<span class="b-portalinfo_number iblock">0</span>
					<span class="b-portalinfo_number iblock">5</span>
					<span class="b-portalinfo_number mr iblock">1</span>
					<span class="b-portalinfo_number iblock">2</span>
					<span class="b-portalinfo_number iblock">1</span>
					<span class="b-portalinfo_number iblock">7</span></span>
					<span class="b-fondinfo_number_lb">изданий</span>
					<div class="b-lib_collect">
						<span class="b-fondinfo_number_lb">Библиотека собрала</span>
						<span class="b-portalinfo_numbers">

							<span class="b-portalinfo_number iblock">2</span>
							<span class="b-portalinfo_number iblock">1</span>
							<span class="b-portalinfo_number iblock">7</span></span>
							<span class="b-fondinfo_number_lb">коллекций</span>
							<a href="#" class="button_mode button_revers">посмотреть все</a>
						</div>
						<div class="b-lib_counter">
							<div class="b-lib_counter_lb">Количество читателей</div>
							<div class="icouser">х 108</div>
						</div>
						<div class="b-lib_counter">
							<div class="b-lib_counter_lb">Количество просмотров</div>
							<div class="icoviews">х 15 678</div>
						</div>
					</div><!-- /.b-libfond -->
					<div class="b-quiz">
						<div class="b-quizico"></div>
						<h2>Какую книгу вы бы хотели видеть на портале НЭБ в ближайшее время?</h2>
						<form action="" class="b-form b-form_common b-formquiz">
							<div class="fieldrow nowrap">
								<div class="fieldcell iblock">
									<div class="field validate">
										<div class="checkwrapper ">
											<input class="checkbox" type="checkbox" name="" id="cb3">
											<label for="cb3" class="black">Белый шум <span class="autor">Дон Делилло</span></label>
										</div>
									</div>
								</div>
							</div>
						
							<div class="fieldrow nowrap">
								<div class="fieldcell iblock">
									<div class="field validate">
										<div class="checkwrapper ">
											<input class="radio" type="radio" name="" id="cb11">
											<label for="cb11" class="black">Женщина французского лейтенанта <span class="autor">Дон Делилло</span></label>
										</div>
									</div>
								</div>
							</div>

							<div class="b-quizitem">
							<div class="b-quizresult"><div style="width:30%;"></div></div>
								<div class="iblock b-quizpersnt">30%</div>
								<label for="cb5" class="black">Дорога перемен <span class="autor">Энтони Берджесс</span></label>
							</div>
							<div class="b-quizitem">

								<div class="b-quizresult"><div style="width:12%;"></div></div>
								<div class="iblock b-quizpersnt">12%</div>
								<label for="cb5" class="black">Дорога перемен <span class="autor">Энтони Берджесс</span></label>
							</div>
							
							<div class="fieldrow nowrap fieldrowaction">
								<div class="fieldcell w700  mt25">
									<div class="field clearfix">
										<button class="b-search_bth" value="1" type="submit" >Голосовать</button>		
									</div>
								</div>
							</div>

						</form>
					</div><!-- /.b-quiz -->
		</div><!-- /.b-side -->
	

</section>

</div><!-- /.homepage -->

<div class="b-mylibrary">
	<div class="wrapper">
		<div class="b-mylibrarytit">Моя библиотека</div>
		<ul class="b-mylibrarylist">
			<li>
				<a href="#">Сейчас  читаю</a> <span class="b-num">2</span>
			</li>
			<li><a href="#">В закладках</a><span class="b-num">14</span></li>
			<li><a href="#">Буду читать</a><span class="b-num">47</span></li>
		</ul>
	</div>
</div><!-- /.b-mylibrary -->
<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<!--<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu js_openmenu">мои подборки</a>
	
	<ul class="b-selectionlist">
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb8">	<label for="cb8" class="black">Любимые авторы</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb9">	<label for="cb9" class="black">Научно-популярная фантастика</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb10"><label for="cb10" class="black">Ракеты и люди</label>

		</li>	
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb14"><label for="cb14" class="lite">Отметить как прочитанное</label>

		</li>
		<li><a href="#" class="b-selection_add"><span>+</span>Cоздать подборку</a></li>
	</ul>
</div>--><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>