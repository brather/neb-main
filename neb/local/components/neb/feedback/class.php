<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 12.09.2016
 * Time: 11:00
 */

use \Bitrix\Main\Loader,
    \Bitrix\Main\Page\Asset,
    \Bitrix\Main\Application,
    \Bitrix\Main\Mail\Event,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\UserTable,
    \Neb\Main\Helper\MainHelper,
    \Neb\Main\Helper\TemplateHelper,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient;

/**
 * Class FeedbackComponent
 */
class FeedbackComponent extends CBitrixComponent
{
    private   $componentPage = '';
    protected $arRequest = [], $arForm = [], $arTicket = [], $arUser = [], $arGroups = [], $arError = [];
    protected $iUserLibrary = 0;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams = $this->getSef($arParams);
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->setHeadContent();
        $this->includeComponentTemplate($this->componentPage);
    }

    /**
     * Добавляет скрипты и стили в шапку сайта
     */
    private static function setHeadContent() {
        //Asset::getInstance()->addCss('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css');
        Asset::getInstance()->addCss('/local/templates/adaptive/vendor/select2-4.0.3/dist/css/select2.min.css');
        Asset::getInstance()->addCss('/local/components/neb/profile/css/style.css');
        //Asset::getInstance()->addJs('//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js');
        Asset::getInstance()->addJs('/local/components/neb/profile/js/script.js');
        Asset::getInstance()->addString('<meta itemprop="name" content="Название сайта"/>'); // шо це таке?

        TemplateHelper::getInstance()->addJs(
            '/local/templates/adaptive/vendor/mustache.min.js',
            ['footer', 'afterDefer']
        );
        TemplateHelper::getInstance()->addJs(
            '/local/templates/adaptive/js/file.uploader.js',
            ['footer', 'afterDefer']
        );
        TemplateHelper::getInstance()->addJs(
            '/local/templates/adaptive/vendor/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
            ['footer', 'afterDefer']
        );
        TemplateHelper::getInstance()->addJs(
            '/local/templates/adaptive/vendor/blueimp-file-upload/js/jquery.iframe-transport.js',
            ['footer', 'afterDefer']
        );
        TemplateHelper::getInstance()->addJs(
            '/local/templates/adaptive/vendor/blueimp-file-upload/js/jquery.fileupload.js',
            ['footer', 'afterDefer']
        );
    }

    /**
     * Роутинг ЧПУ
     * @param $arParams
     */
    private function getSef($arParams) {
        $arDefaultUrlTemplates404 = array(
            "list"             => "/",
            "appoint"          => "appoint/",
            "responsible"      => "responsible/",
            "reply"            => "reply/",
            "reject"           => "reject/",
            "history"          => "history/",
            "statistics"       => 'statistics/',
            "statistics_total" => 'statistics/total/',
            "statistics_time"  => 'statistics/time/'
        );

        $arDefaultVariableAliases404 = array();

        $arDefaultVariableAliases = array();

        $arComponentVariables = array('SECTION', 'CODE');

        $SEF_FOLDER = "";
        $arUrlTemplates = array();

        if ($arParams["SEF_MODE"] == "Y") {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

            $componentPage = CComponentEngine::parseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

            if (StrLen($componentPage) <= 0) $componentPage = "template";

            CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

            $SEF_FOLDER = $arParams["SEF_FOLDER"];

        } else {

            $arVariables = array();

            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
            CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

            $componentPage = "";
            if (IntVal($arVariables["ELEMENT_ID"]) > 0) {
                $componentPage = "detail";
            } else {
                $componentPage = "list";
            }
        }

        $this->componentPage = $componentPage;

        return $arParams;
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    public function getRequest() {
        return $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Подключение модулей
     *
     * @param $arModules - массив модулей
     * @throws \Bitrix\Main\LoaderException
     */
    public static function includeModules($arModules) {
        foreach ($arModules as $sModule)
            Loader::includeModule($sModule);
    }

    /**
     * Получение текущего пользователя
     */
    public function getUser() {

        $obUser   = new nebUser();

        if (!$obUser->IsAuthorized())
            return;

        $arUsers  = $obUser->getUser();
        $arGroups = $obUser->getUserGroups();

        if (in_array('library_admin', $arGroups) && $arUsers['UF_LIBRARY'])
            $iUserLibrary = $arUsers['UF_LIBRARY'];

        $this->arUser       = $arUsers;
        $this->arGroups     = $arGroups;
        $this->iUserLibrary = $iUserLibrary;
    }

    /**
     * Получение всех данных формы, включая статусы, вопросы, поля и ответы
     *
     * @param $arFormFilter - фильтр по полю формы
     * @param array $arSelect - выбираемые сущности формы, по умолчанию выбирают все
     * @return array
     */
    public function getForm($arFormFilter = [], $arSelect = []) {

        $arResult = [];
        $sSelect = $sJoin = $sOrder = $sWhere = '';

        // фильтр только для одиночного условия формы
        if (empty($arFormFilter)) {
            if ($this->arTicket['FORM_ID'] > 0)
                $arFormFilter['ID'] = $this->arTicket['FORM_ID'];
            else
                return $arResult;
        }
        if (!empty($arFormFilter))
            $sWhere = 'WHERE';
        foreach ($arFormFilter as $sCode => $sVal)
            $sWhere .= ' f.' . $sCode . " = '" . $sVal . "'";

        // выборка дополнительных сущностей
        if (empty($arSelect) || in_array('STATUSES', $arSelect)) {
            $sSelect .= ',
                s.ID AS S_ID,
                s.FORM_ID AS S_FORM_ID,
                s.TIMESTAMP_X AS S_TIMESTAMP_X,
                s.ACTIVE AS S_ACTIVE,
                s.C_SORT AS S_C_SORT,
                s.TITLE AS S_TITLE,
                s.DESCRIPTION AS S_DESCRIPTION,
                s.DEFAULT_VALUE AS S_DEFAULT_VALUE,
                s.CSS AS S_CSS,
                s.HANDLER_OUT AS S_HANDLER_OUT,
                s.HANDLER_IN AS S_HANDLER_IN,
                s.MAIL_EVENT_TYPE AS S_MAIL_EVENT_TYPE
            ';
            $sJoin .= '
                LEFT JOIN b_form_status AS s ON s.FORM_ID = f.ID
            ';
        }
        if (empty($arSelect) || in_array('FIELDS', $arSelect)) {
            $sSelect .= ',
                q.ID AS Q_ID,
                q.FORM_ID AS Q_FORM_ID,
                q.TIMESTAMP_X AS Q_TIMESTAMP_X,
                q.ACTIVE AS Q_ACTIVE,
                q.TITLE AS Q_TITLE,
                q.TITLE_TYPE AS Q_TITLE_TYPE,
                q.SID AS Q_SID,
                q.C_SORT AS Q_C_SORT,
                q.ADDITIONAL AS Q_ADDITIONAL,
                q.REQUIRED AS Q_REQUIRED,
                q.IN_FILTER AS Q_IN_FILTER,
                q.IN_RESULTS_TABLE AS Q_IN_RESULTS_TABLE,
                q.IN_EXCEL_TABLE AS Q_IN_EXCEL_TABLE,
                q.FIELD_TYPE AS Q_FIELD_TYPE,
                q.IMAGE_ID AS Q_IMAGE_ID,
                q.COMMENTS AS Q_COMMENTS,
                q.FILTER_TITLE AS Q_FILTER_TITLE,
                q.RESULTS_TABLE_TITLE AS Q_RESULTS_TABLE_TITLE
            ';
            $sJoin .= '
                LEFT JOIN b_form_field  AS q ON q.FORM_ID = f.ID
            ';
            $sOrder .= ',
                q.C_SORT ASC
            ';
        }
        if (empty($arSelect) || in_array('ANSWERS', $arSelect)) {
            $sSelect .= ',
                a.ID AS A_ID,
                a.FIELD_ID AS A_FIELD_ID,
                a.TIMESTAMP_X AS A_TIMESTAMP_X,
                a.MESSAGE AS A_MESSAGE,
                a.C_SORT AS A_C_SORT,
                a.ACTIVE AS A_ACTIVE,
                a.VALUE AS A_VALUE,
                a.FIELD_TYPE AS A_FIELD_TYPE,
                a.FIELD_WIDTH AS A_FIELD_WIDTH,
                a.FIELD_HEIGHT AS A_FIELD_HEIGHT,
                a.FIELD_PARAM AS A_FIELD_PARAM
            ';
            $sJoin .= '
                LEFT JOIN b_form_answer AS a ON a.FIELD_ID = q.ID
            ';
            $sOrder .= ',
                a.C_SORT ASC
            ';
        }

        // основной запрос
        $sQuery = "
            SELECT
              f.ID AS F_ID,
              f.TIMESTAMP_X AS F_TIMESTAMP_X,
              f.NAME AS F_NAME,
              f.SID AS F_SID,
              f.BUTTON AS F_BUTTON,
              f.C_SORT AS F_C_SORT,
              f.FIRST_SITE_ID AS F_FIRST_SITE_ID,
              f.IMAGE_ID AS F_IMAGE_ID,
              f.USE_CAPTCHA AS F_USE_CAPTCHA,
              f.DESCRIPTION AS F_DESCRIPTION,
              f.DESCRIPTION_TYPE AS F_DESCRIPTION_TYPE,
              f.FORM_TEMPLATE AS F_FORM_TEMPLATE,
              f.USE_DEFAULT_TEMPLATE AS F_USE_DEFAULT_TEMPLATE,
              f.SHOW_TEMPLATE AS F_SHOW_TEMPLATE,
              f.MAIL_EVENT_TYPE AS F_MAIL_EVENT_TYPE,
              f.SHOW_RESULT_TEMPLATE AS F_SHOW_RESULT_TEMPLATE,
              f.PRINT_RESULT_TEMPLATE AS F_PRINT_RESULT_TEMPLATE,
              f.EDIT_RESULT_TEMPLATE AS F_EDIT_RESULT_TEMPLATE,
              f.FILTER_RESULT_TEMPLATE AS F_FILTER_RESULT_TEMPLATE,
              f.TABLE_RESULT_TEMPLATE AS F_TABLE_RESULT_TEMPLATE,
              f.USE_RESTRICTIONS AS F_USE_RESTRICTIONS,
              f.RESTRICT_USER AS F_RESTRICT_USER,
              f.RESTRICT_TIME AS F_RESTRICT_TIME,
              f.RESTRICT_STATUS AS F_RESTRICT_STATUS,
              f.STAT_EVENT1 AS F_STAT_EVENT1,
              f.STAT_EVENT2 AS F_STAT_EVENT2,
              f.STAT_EVENT3 AS F_STAT_EVENT3
              $sSelect
            FROM
              b_form AS f
            $sJoin
            $sWhere
            ORDER BY
              f.ID ASC
              $sOrder
        ";
        $rsRecords = Application::getConnection()->query($sQuery);
        while ($arItem = $rsRecords->fetch()) {
            foreach ($arItem as $sCode => $sVal) {
                $sPrefix = mb_substr($sCode, 0, 2);
                $sCode   = mb_substr($sCode, 2, mb_strlen($sCode));
                if ('F_' === $sPrefix)
                    $arResult[$sCode] = $sVal;
                elseif ('S_' === $sPrefix)
                    $arResult['STATUSES'][$arItem['S_DESCRIPTION']?:$arItem['S_TITLE']][$sCode] = $sVal;
                elseif ('Q_' === $sPrefix && $arItem['Q_ACTIVE'] == 'Y')
                    $arResult['FIELDS'][$arItem['Q_SID']][$sCode] = $sVal;
                elseif ($arItem['A_ID'] > 0 && $arItem['A_ACTIVE'] == 'Y')
                    $arResult['FIELDS'][$arItem['Q_SID']]['ANSWERS'][$arItem['A_ID']][$sCode] = $sVal;
            }
        }

        return $this->arForm = $arResult;
    }

    /**
     * Получение заявки
     *
     * @param bool $bSelectFiles - выбирать значения полей типа "Файл"
     */
    public function getTicket($bSelectFiles = false) {

        $arResult = [];

        $iOrder = intval($this->arRequest['id']);
        if ($iOrder > 0) {
            $obFormResult = new CFormResult();
            $arResult = $obFormResult->GetByID($iOrder)->Fetch();
            $arFields = $obFormResult->GetDataByID($iOrder, [], $arForm, $arTicket2);
            foreach ($arFields as $arItem) {
                reset($arItem); $arItem = current($arItem);

                if ('file' == $arItem['FIELD_TYPE'] && $arItem['USER_FILE_ID'] > 0)
                    $arItem['FILE'] = static::getFile($arItem['USER_FILE_ID']);

                if ('FB_BOOK_ID' == $arItem['SID'] && !empty($arItem['USER_TEXT']))
                    $arItem['BOOK'] = $this->getBookById($arItem['USER_TEXT']);

                $arResult['FIELDS'][$arItem['SID']] = $arItem;
            }
        }

        if (empty($arResult['ID']) || empty($arResult['FIELDS']))
            $this->arResult['ERROR'][] = 'Заявка №' . $iOrder . ' не найдена!';
        elseif ($this->iUserLibrary > 0 && $this->iUserLibrary != $arResult['FIELDS']['FB_LIBRARY_ID']['USER_TEXT'])
            $this->arResult['ERROR'][] = 'Заявка №' . $iOrder . ' отправлена в другую библиотеку!';
        else
            $this->arTicket = $arResult;
    }

    /**
     * Установка статуса для заявки
     *
     * @param $sCode
     * @return bool
     */
    public function setTicketStatus($sCode) {

        $bResult = false;

        $arStatus = $this->arForm['STATUSES'][$sCode];
        if (empty($this->arTicket['ID']) || empty($arStatus))
            return $bResult;

        $obFormResult = new CFormResult();
        $bResult = $obFormResult->SetStatus($this->arTicket['ID'], $arStatus['ID'], 'N');

        return $bResult;
    }

    /**
     * Устанавливает значениет поля в текущем варианте ответа
     *
     * @param $sCode
     * @param $sValue
     * @return bool
     */
    public function setTicketField($sCode, $sValue) {

        // для вопросов - устанавливает поле в форме [id ответа => значение]
        $arAnswer = $this->arForm['FIELDS'][$sCode]['ANSWERS'];
        if (!empty($arAnswer)) {
            reset($arAnswer);
            $arAnswer = current($arAnswer);
            $sValue = [$arAnswer['ID'] => $sValue];
        }

        $obFormResult = new CFormResult();
        $bResult = $obFormResult->SetField($this->arTicket['ID'], $sCode, $sValue);

        return $bResult;
    }

    /**
     * Сохранение новой записи в историю заявки в сериализованном виде
     */
    public function setHistoryItem($sStatus, $iUser) {

        $bResult = false;

        if (empty($sStatus) || empty($iUser))
            return $bResult;

        $arHistory   = unserialize($this->arTicket['FIELDS']['FB_HISTORY_LOG']['USER_TEXT']);
        $arHistory[] = [
            'STATUS'  => $sStatus,
            'DATE'    => (new DateTime())->toString(),
            'USER_ID' => $iUser
        ];
        $bResult = $this->setTicketField('FB_HISTORY_LOG', serialize($arHistory));

        return $bResult;
    }

    /**
     * Отправка письма на электронную почту
     *
     * @param       $sEventType
     * @param       $arFields
     * @param array $arFiles
     *
     * @return \Bitrix\Main\Entity\AddResult
     */
    public static function sendMail($sEventType, $arFields, $arFiles = array()) {
        return Event::send(["EVENT_NAME" => $sEventType, "LID" => SITE_ID, "C_FIELDS" => $arFields, "FILE" => $arFiles]);
    }

    /**
     * Получение пользователей по массиву идентификаторов
     *
     * @param $arUsers
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getUsers($arUsers) {

        $arResult = [];

        $rsUsers = UserTable::getList([
            'filter' => ['ID' => $arUsers],
            'select' => ['ID', 'EMAIL', 'LAST_NAME', 'NAME', 'SECOND_NAME']
        ]);
        while ($arFields = $rsUsers->fetch())
            $arResult[$arFields['ID']] = $arFields;

        return $arResult;
    }

    /**
     * Получение книги по её ID из Exalead
     *
     * @param $sBookId
     * @return array
     */
    public static function getBookById($sBookId) {

        $query = new SearchQuery();
        $query->getById($sBookId);

        $client = new SearchClient();
        $arResult = $client->getResult($query);

        return $arResult;
    }

    /**
     * Получение информации о файле
     *
     * @param $iFile
     * @return array
     */
    public static function getFile($iFile) {

        if ($iFile < 1)
            return [];

        $arResult         = CFile::GetByID($iFile)->Fetch();
        $arResult['SIZE'] = CFile::FormatSize($arResult['FILE_SIZE']);
        $arResult['PATH'] = CFile::GetPath($iFile);

        return $arResult;
    }

    /**
     * Выполняет сбпрос буффера
     */
    public static function restartBuffer() {
        MainHelper::clearBuffer();
    }

    /**
     * Выдает массив в браузер в виде JSON
     * @param $arResult
     */
    public static function showJson($arResult) {
        MainHelper::showJson($arResult);
    }
}