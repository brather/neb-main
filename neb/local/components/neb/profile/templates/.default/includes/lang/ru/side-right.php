<?php
$MESS['PROFILE_MU_REGISTER'] = 'Зарегистрирован';
$MESS['PROFILE_MU_ACCESS_R'] = 'Ограниченный доступ';
$MESS['PROFILE_MU_ACCESS_F'] = 'Полный доступ';
$MESS['PROFILE_MU_PROFILE'] = 'Настройки профиля';
$MESS['PROFILE_MU_ACCESS_GET'] = 'Получить полный доступ';
$MESS['PROFILE_MU_VIEW_ALL_MESSAGES'] = 'Мои сообщения';
$MESS['PROFILE_MU_NUMBER_LC'] = 'Номер ЕЭЧБ';
