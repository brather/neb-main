<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->IncludeComponent(
    "neb:collections.list",
    "special",
    array(
        "PAGE_URL" => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['seaction'],
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
        "IBLOCK_ID" => $arParams['IBLOCK_ID']
    ),
    false
);