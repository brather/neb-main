<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if (!isset($arParams['UF_LIBRARIES'])) {
    $arParams['UF_LIBRARIES'] = array();
}
if (!in_array($arParams['LIBRARY_ID'], $arParams['UF_LIBRARIES'])) {
    $arParams['UF_LIBRARIES'][] = $arParams['LIBRARY_ID'];
}