<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arComponentParameters = array(
    "PARAMETERS" => array(
        "TYPE_EDITOR" => Array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("EDITOR_MODE"),
            "TYPE" => "LIST",
            "VALUES" => array(
                'TYPE_1' => Loc::getMessage("EDITOR_SIMPLIFIED"), 
                'TYPE_2' => Loc::getMessage("EDITOR_FULL")
            ),
        ),

        'INIT_ID' => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("EDITOR_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => '',
        ),

        'TEXT' => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("EDITOR_CONTENT_DEFAULT"),
            "TYPE" => "STRING",
            "DEFAULT" => $_POST['content'],
        ),

        'TEXTAREA_NAME' => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("EDITOR_TEXTAREA_NAME"),
            "TYPE" => "STRING",
            "DEFAULT" => 'content',
        ),

        'TEXTAREA_ID' => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("EDITOR_TEXTAREA_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => 'content',
        ),

        'TEXTAREA_WIDTH' => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("EDITOR_WIDTH"),
            "TYPE" => "STRING",
            "DEFAULT" => '100%',
        ),

        'TEXTAREA_HEIGHT' => array(
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage("EDITOR_HEIGHT"),
            "TYPE" => "STRING",
            "DEFAULT" => '300',
        ),
    )
);
?>