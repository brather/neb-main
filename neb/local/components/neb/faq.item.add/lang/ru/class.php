<?php
$MESS["ERROR_MIN_COUNT_SYMBOL"] = "Ошибка! Вы ввели слишком мало символов.";
$MESS["ERROR_EMPTY_STRING"] = "Ошибка! Вы не заполнили одно из полей.";
$MESS["ERROR"] = "Ошибка!";
$MESS["ERROR_ITEM_EXIST"] = "Ошибка! Элемент с таким именем уже существует. Пожалуйста введите другое название элемента.";
$MESS["ADD_ITEM_SUCCESS_1"] = "Добавление элемента с id=<b>";
$MESS["ADD_ITEM_SUCCESS_2"] = "</b> произведено успешно!";
$MESS["ADD_ITEM_ERROR"] = "Ошибка добавления элемента!";
$MESS["ERROR_SECTION_NAME_EMPTY"] = "Ошибка! Выберите пожалуйста раздел.";
$MESS["ERROR_DETAIL_TEXT_EMPTY"] = "Ошибка! Введите детальный текст.";