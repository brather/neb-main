<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

#if(empty($arParams['ajax']) and (bool)$arParams['IS_SHOW_PROTECTED'] === false)
if(empty($arParams['ajax']))
    include_once($_SERVER['DOCUMENT_ROOT'].'/local/include_areas/books_closed.php');

$iLag = 0;

?>
<!-- Контент НАЧАЛО-->
<main>
    <section class="search-result">
        <?php if(!empty($arResult['ITEMS'])): ?>
            <main class="search-result__content">
                <ul class="search-result__content-list clearfix">

                    <?php foreach($arResult['ITEMS'] as $index => $arItem):
                        $index = $index - $iLag;
                        // книга может быть в коллекциях, но не Exalead
                        if (empty($arItem['id'])) {
                            $iLag++;
                            continue;
                        }?>
                        <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/search_row.php', array('arParams' => $arParams,'arResult' => $arResult, 'index' => $index,'arItem' => $arItem )); ?>
                    <?php endforeach; ?>

                </ul>
            </main>
        <?php endif; ?>
    </section>
    <?php echo !empty($arResult['STR_NAV']) ? htmlspecialcharsBack($arResult['STR_NAV']) : ''; ?>
</main>
<!-- /Контент КОНЕЦ -->
