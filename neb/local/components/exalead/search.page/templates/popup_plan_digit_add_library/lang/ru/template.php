<?php
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO'] = 'Найдено 0 документов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND'] = 'К сожалению, ничего не найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SUGGEST'] = 'Возможно вы искали';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'Найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'Найден';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'Найдено';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'] = 'результатов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'] = 'результата';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'] = 'результат';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY'] = 'Сохранить поисковый запрос';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED'] = 'Поисковый запрос сохранен';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE'] = 'Добавить название';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SPECIFY_SEARCH'] = 'уточнить поиск';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE'] = 'Вы искали на портале';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE'] = 'Искать только в полнотекстовых изданиях';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY'] = 'По каталогу печатных изданий';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_EMPTY'] = 'Введите поисковое выражение';
$MESS['LIB_SEARCH_PAGE_SETTINGS'] = 'Настройка';

$MESS["RM_BOOK_DIGITIZED"] = "Книга оцифровывается";
$MESS["RM_BOOK_DELETE"] = "Удалить из Плана оцифровки";
$MESS["RM_BOOK_IN_PLAN"] = "Книга оцифровывается по плану ";
$MESS["RM_BOOK_ADD_TO_PLAN"] = "Добавить в План оцифровки";

$MESS["T_LIB_BOOKS_NOT_FOUND"] = "К сожалению, ничего не найдено";
$MESS["T_LIB_CHANGE_PARAM_OF_SEARCH"] = "Попробуйте изменить критерии поиска.";
$MESS["T_LIB_BOOKS_ON_DIGITIZED"] = "На оцифровке";
$MESS["T_LIB_AUTHOR"] = "Автор";
$MESS["T_LIB_TITLE_DESCRIPTION"] = "Название / Описание";
$MESS["T_LIB_DESCRIPTION"] = "Описание";
$MESS["T_LIB_DIGITIZING"] = "Оцифровывает";
$MESS["T_LIB_TIME_OF_DIGITIZED"] = "  Срок оцифровки:";
$MESS["T_LIB_DESC_AUTHOR"] = "Автор: ";
$MESS["T_LIB_DESC_TITLE"] = "Заглавие: ";
$MESS["T_LIB_DESC_DATE_OF_PUBLICATION"] = "Дата пубдликации: ";
$MESS["T_LIB_DESC_LIBRARY"] = "Библиотека: ";
$MESS["T_LIB_DESC_PAGES"] = "Страниц: ";