<?php
CBitrixComponent::includeComponentClass("rest:rest.server");
CModule::IncludeModule('iblock');

/**
 * Class RestLibraryNebParty
 */
class RestLibraryNebParty extends RestServer
{
    /**
     * @var array
     */
    protected $_methodAllowGroups
        = array(
            'deleteAction' => array(
                13 => true,
            )
        );

    /**
     * @return int
     * @throws Exception
     */
    public function deleteAction()
    {
        $elementId = (integer)$_GET['id'];
        if ($elementId < 1) {
            throw new Exception(
                GetMessage('NO_REQUIRED', array('#PARAM#' => 'id')), 500
            );
        }

        $element = new CIBlockElement();
        if (true !== $element->Update($elementId, array('ACTIVE' => 'N'))) {
            throw new Exception($element->LAST_ERROR, 500);
        }

        $this->arResult = true;

        return 200;
    }
}