<div class="b-addbook b-addfondbook">
	<h2>Добавление новой книги</h2>
	<form action="" class="b-form b-form_common b-addbookform">
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50c">			
				<label for="settings03">Автор (сведения об интеллектуальной ответственности)</label>
				<div class="field validate">
					<input type="text"  data-validate="fio" data-required="required" value="" id="settings03" data-maxlength="30" data-minlength="2" name="name" class="input w50p" >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">			
				<label for="settings02">Полное заглавие произведения</label>
				<div class="field validate">
					<input type="text" data-required="required" value="" id="settings02" data-maxlength="30" data-minlength="2" name="name" class="input" >		
					<em class="error required">Поле обязательно для заполнения</em>								
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">			
				<label for="settings22">Сведения, относящиеся к заглавию</label>
				<div class="field validate">
					<input type="text" data-required="required" value="" id="settings22" data-maxlength="30" data-minlength="2" name="name" class="input" >		
					<em class="error required">Поле обязательно для заполнения</em>								
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w20pc">			
				<label for="settings23">Номер подчасти</label>
				<div class="field validate">
					<input type="text" data-validate="number"  data-required="required" value="" id="settings23" data-maxlength="30" data-minlength="2" name="name" class="input b-yeartb" >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>

		</div>	
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w76">			
				<label for="settings21">Название подчасти</label>
				<div class="field validate">
					<input type="text" data-validate="alpha"  data-required="required" value="" id="settings21" data-maxlength="30" data-minlength="2" name="name" class="input" >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt0 w20pc">			
				<label for="settings25">Год издания</label>
				<div class="field validate">
					<input type="text"  data-validate="number" data-required="required" value="" id="settings25" data-maxlength="30" data-minlength="2" name="name" class="input b-yeartb" >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">			
				<label for="settings20">Издательство или издающая организация (точное название)</label>
				<div class="field validate">
					<input type="text" data-required="required" value="" id="settings20" data-maxlength="30" data-minlength="2" name="name" class="input" >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50c">			
				<label for="settings19">Место издания</label>
				<div class="field validate">
					<input type="text" data-required="required" value="" id="settings19" data-maxlength="30" data-minlength="2" name="name" class="input w50p" >		
					<em class="error required">Поле обязательно для заполнения</em>	
					<em class="error validate">Поле заполнено неверно</em>								
				</div>
			</div>
		</div>
		
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10 w50c">			
				<label for="settings18">Международные стандартные книжные номера (ISBN)</label>
				<div class="field validate">
					<input type="text" data-validate="number"  data-required="required" value="" id="settings18" data-maxlength="30" data-minlength="2" name="name" class="input w50p" >		
					<em class="error required">Поле обязательно для заполнения</em>	
					<em class="error validate">Поле заполнено неверно</em>								
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings17">Язык</label>
				<div class="field validate">
					<select name="" id="" class="js_select w288"  data-required="true">									
						<option value="1">русский</option>
						<option value="2">нерусский</option>
					</select>									
				</div>
			</div>
		</div>	
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w20pc">			
				<label for="settings16">Тираж</label>
				<div class="field validate">
					<input type="text" data-validate="number"  data-required="required" value="" id="settings16" data-maxlength="30" data-minlength="2" name="name" class="input b-yeartb" >		
					<em class="error required">Поле обязательно для заполнения</em>			
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50c">			
				<label for="settings15">Серия</label>
				<div class="field validate">
					<input type="text" data-required="required" value="" id="settings15" data-maxlength="30" data-minlength="2" name="name" class="input w50p" >		
					<em class="error required">Поле обязательно для заполнения</em>								
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w20pc">			
				<label for="settings14">Номер тома</label>
				<div class="field validate">
					<input type="text" data-validate="number"  data-required="required" value="" id="settings14" data-maxlength="30" data-minlength="2" name="name" class="input b-yeartb" >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
			
		</div>	
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w76">			
				<label for="settings13">Заглавие тома</label>
				<div class="field validate">
					<input type="text" data-validate="alpha"  data-required="required" value="" id="settings13" data-maxlength="30" data-minlength="2" name="name" class="input " >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>			
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10">			
				<label for="settings12">Примечание о содержании</label>
				<div class="field validate">
					<input type="text" data-required="required" value="" id="settings12" data-maxlength="30" data-minlength="2" name="name" class="input" >		
					<em class="error required">Поле обязательно для заполнения</em>								
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">			
				<label for="settings11">Примечание общего характера</label>
				<div class="field validate">
					<input type="text" data-required="required" value="" id="settings11" data-maxlength="30" data-minlength="2" name="name" class="input" >		
					<em class="error required">Поле обязательно для заполнения</em>								
				</div>
			</div>
		</div>


		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10">
				<label for="settings10">Аннотация/реферат/резюме</label>
				<div class="field validate">
					<textarea data-maxlength="800" data-minlength="2" id="settings10" name="about" class="textarea"></textarea>							
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50">			
				<label for="settings07">Код ББК</label>
				<div class="field validate">
					<input type="text" data-validate="number"  data-required="required" value="" id="settings07" data-maxlength="30" data-minlength="2" name="name" class="input " >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
			
		</div>	
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50">			
				<label for="settings09">УДК</label>
				<div class="field validate">
					<input type="text" data-validate="number"  data-required="required" value="" id="settings09" data-maxlength="30" data-minlength="2" name="name" class="input " >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w20pc">			
				<label for="settings06">Международные стандартные серийные номера (ISSN) </label>
				<div class="field validate">
					<input type="text" data-validate="number" data-required="required" value="" id="settings06" data-maxlength="30" data-minlength="2" name="name" class="input b-yeartb" >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>	
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock b-pdfadd">
				<div class="left">
					<p>Ссылка на pdf-версию книги</p>
					<a href="#" class="tdu">Добавить pdf</a>
				</div>
				<div class="right">
					<p>Дата добавления </p>
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><span class="b-fieldtext">12.12.2014</span><a class="b-fieldeedit" href="#"></a></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_size2"></span><a class="b-editablsubmit iblock" href="#"></a></div>
					</div>
				</div>


			</div>
		</div>
		<div class="fieldrow nowrap fieldrowaction">
			<div class="fieldcell ">
				<div class="field clearfix">
					<a href="#" class="formbutton gray right btrefuse">Отказаться</a>
					<button class="formbutton left" value="1" type="submit">Разместить произведение</button>

				</div>
			</div>
		</div>
	</form>
</div>