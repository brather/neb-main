<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<? if ($arResult['NAV_NAME_NUMBER'] <= 1) { ?>
	<!--<div class="collection-illustration">
		<img src="<?= $arResult['SECTION']['DETAIL_PICTURE'] ?>">
	</div>-->
	<div class="row">
		<div class="col-md-9">
			<?if(collectionUser::isAdd()){?>
				<div class="col-detailed-add-books"
					 data-add-to-favorites-widget
					 data-section-id="<?=$arResult['SECTION']['ID']?>"
					 data-add-href="<?=ADD_COLLECTION_URL?>list.php?t=books_collection&id=<?=$arResult['SECTION']['ID']?>&action=checked"
					 data-add-toggler-text="<?=Loc::getMessage('COLLECTION_DETAIL_ADD_TO_MY_COL')?>"
					 data-remove-href="<?=ADD_COLLECTION_URL?>removeBook.php?t=books_collection&id=<?=$arResult['SECTION']['ID']?>"
					 data-remove-toggler-text="<?php echo Loc::getMessage('COLLECTION_DETAIL_REMOVE_FROM_MY_COL')?>">
					<div class="col-detailed-add-books-toggler-wrapper">
	                    <?php if(true === $arResult['IN_MY_COLLECTIONS']):?>
	                        <a href="<?=ADD_COLLECTION_URL?>removeBook.php?t=books_collection&id=<?=$arResult['SECTION']['ID']?>" data-remove-collection-from-favorites>
	                            <?php echo Loc::getMessage('COLLECTION_DETAIL_REMOVE_FROM_MY_COL')?>
	                        </a>
	                    <?php else: ?>
	                        <a href="<?=ADD_COLLECTION_URL?>list.php?t=books_collection&id=<?=$arResult['SECTION']['ID']?>&action=checked" data-add-collection-to-favorites>
	                            <?=Loc::getMessage('COLLECTION_DETAIL_ADD_TO_MY_COL')?>
	                        </a>
	                    <?php endif; ?>
	                </div>
				</div>
			<?}?>
			<div class="b-collupdate"><em><?=Loc::getMessage('COLLECTION_DETAIL_UPDATED'); ?>:</em> <?=$arResult['SECTION']['TIMESTAMP_X']?></div>
			<?if(!empty($arResult['LIBRARY'])) {?>
				<div class="b-result_sorce_info">
					<em><?=Loc::getMessage('COLLECTION_DETAIL_AUTHOR'); ?>:</em> <a class="b-sorcelibrary" href="<?=$arResult['LIBRARY']['DETAIL_PAGE_URL']?>"><?=$arResult['LIBRARY']['NAME']?></a>
				</div>
			<?}?>
			
			<?if(count($arResult["AR_SUB_SECTIONS"]) > 1) {?>
				<ul>
					<?foreach($arResult["AR_SUB_SECTIONS"] as $iSubSection => $arSubSection) {?>
						<li>
							<a href="<?= $arSubSection["SECTION_PAGE_URL"] ?>">
								<?= $arSubSection["NAME"] ?>
							</a>
						</li>
					<?}?>
				</ul>
			<?} else {?>
				<p>
					<a href="<?= $arResult["AR_SUB_SECTIONS"][0]["SECTION_PAGE_URL"] ?>">
						<?= $arResult["AR_SUB_SECTIONS"][0]["NAME"] ?>
					</a>
				</p>
			<?}?>
			<p><?= $arResult['SECTION']['~DESCRIPTION'] ?></p>
			<?$APPLICATION->IncludeComponent(
				"exalead:search.page.viewer.left",
				"",
				Array(
					"PARAMS" => array_merge($arParams, array('TITLE_MODE_BLOCK' => 'Отобразить издания коллекции плиткой', 'TITLE_MODE_LIST' => 'Отобразить издания коллекции списком')),
					"RESULT" => $arResult,
				),
				$component
			);?>
		</div>
		<div class="col-md-3">
			<div class="b-favside mtop">
				<div class="b-favside_img js_flexbackground">
					<?if(!empty($arResult['COVER_IMAGES'])){?>
						<img src="<?=$arResult['COVER_IMAGES']?>&width=131&height=194" class="real loadingimg" alt="">
					<?}?>
				</div>
				<div class="b-favside_books">
					<?=$arResult['SECTION']['CNT']?> <?=GetEnding($arResult['SECTION']['CNT'], Loc::getMessage('COLLECTION_DETAIL_BOOK_5'), Loc::getMessage('COLLECTION_DETAIL_BOOK_1'), Loc::getMessage('COLLECTION_DETAIL_BOOK_2'))?> <?=Loc::getMessage('COLLECTION_DETAIL_IN_COL'); ?>
				</div>
				<hr>
				<?if(!empty($arResult['SECTION']['UF_ADDED_FAVORITES'])){?>
					<div class="b-lib_counter">
						<div class="b-lib_counter_lb"><?=Loc::getMessage('COLLECTION_DETAIL_ADDED_TO_FAV'); ?></div>
						<div class="icofavs">х <?=number_format($arResult['SECTION']['UF_ADDED_FAVORITES'], 0, '', ' ')?></div>
					</div>
				<?}
				if(!empty($arResult['SECTION']['UF_VIEWS'])){?>
					<div class="b-lib_counter">
						<div class="b-lib_counter_lb"><?=Loc::getMessage('COLLECTION_DETAIL_VIEWS'); ?></div>
						<div class="icoviews">х <?=number_format($arResult['SECTION']['UF_VIEWS'], 0, '', ' ')?></div>
					</div>
				<?}?>
			</div>
		</div>
	</div>
<? } else { ?>
	<?$APPLICATION->IncludeComponent(
		"exalead:search.page.viewer.left",
		"",
		Array(
			"PARAMS" => array_merge($arParams,
				array('TITLE_MODE_BLOCK' => 'Отобразить издания коллекции плиткой',
					  'TITLE_MODE_LIST'  => 'Отобразить издания коллекции списком')
			),
			"RESULT" => $arResult,
		),
		$component
	);?>
<?}?>