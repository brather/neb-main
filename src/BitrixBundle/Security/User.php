<?php
/**
 * User: agolodkov
 * Date: 16.08.2016
 * Time: 16:43
 */

namespace BitrixBundle\Security;


use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{

    private $_username;
    /**
     * @var string
     */
    private $_email;
    private $_roles = [];
    private $_token;

    /**
     * @var array
     */
    private $_userRawData;

    public function hasRole($role)
    {
        return in_array($role, $this->_roles);
    }

    /**
     * @inheritdoc
     */
    public function getRoles()
    {
        return $this->_roles;
    }

    /**
     * @inheritdoc
     */
    public function getPassword()
    {
    }

    /**
     * @inheritdoc
     */
    public function getSalt()
    {
    }

    /**
     * @inheritdoc
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->_roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->_token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->_token = $token;
    }

    /**
     * @return array
     */
    public function getUserRawData()
    {
        return $this->_userRawData;
    }

    /**
     * @param array $userRawData
     */
    public function setUserRawData($userRawData)
    {
        $this->_userRawData = $userRawData;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }
}