<?php
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO'] = 'Documents not found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND'] = 'Sorry, nothing found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SUGGEST'] = 'Maybe you are looking for';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'Documents';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'Document';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'Documents';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'] = 'found';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY'] = 'Save the search request';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED'] = 'Search request saved';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE'] = 'Add a title';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SPECIFY_SEARCH'] = 'Specify the search';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE'] = 'Search on the website';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE'] = 'Website';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY'] = 'Libraries';

$MESS['LIB_SEARCH_PAGE_POPUP_CHOOSE_BOOK'] = 'Before adding the publication is necessary to choose a book from the list.';
$MESS['LIB_SEARCH_PAGE_BOOKS_EXIST_IN_COLLECTION'] = ' (already stored in the current collection)';

$MESS['LIB_SEARCH_AUTHOR'] = 'Author: ';
$MESS['LIB_SEARCH_PUBLIC_YEAR'] = 'Year of publication: ';
$MESS['LIB_SEARCH_COUNT_OF_PAGES'] = 'Count of pages: ';
$MESS['LIB_SEARCH_PUBLIC_HOUSE'] = 'Publishing house: ';
$MESS['LIB_SEARCH_SOURCE'] = 'Source: ';
$MESS['LIB_SEARCH_CANCEL_SELECT'] = 'Cancel selected';
$MESS['LIB_SEARCH_ADD_SELECT'] = 'Add selected';