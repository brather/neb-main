<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Типы ЭЧЗ');

/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
$APPLICATION->IncludeComponent(
    'neb:workplaces.types',
    '',
    [
        "LIST_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['workplaces_types'],
        "ADD_URL"  => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['workplaces_types_add'],
        "EDIT_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['workplaces_types_edit']
    ],
    $component
);