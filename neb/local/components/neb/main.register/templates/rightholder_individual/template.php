<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$partsPath = dirname(__FILE__) . '/../../template-parts';
require($partsPath . '/rh-header.php');
if ((int)$arResult['VALUES']['USER_ID'] > 0
    && $arResult["USE_EMAIL_CONFIRMATION"] === "Y"
) {
    return;
}
require($partsPath . '/passport-fields.php');
require($partsPath . '/rh-common.php');
require($partsPath . '/rh-footer.php');
