<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<!-- Слайдер -->
<section class="main-slider">
    <div class="container">
        <div class="main-slider__wrapper">
                <? $x = 0;
                foreach ($arResult['ITEMS'] as $key => $arSection):

                    $arSection['IMAGE'] = [];

                    // при наличии изображения - ресайзить его до 180x180
                    if ($arSection['DETAIL_PICTURE'] > 0) {
                        $arSection['IMAGE'] = CFile::ResizeImageGet($arSection['DETAIL_PICTURE'],
                                    ['width' => 180, 'height' => 180], BX_RESIZE_IMAGE_PROPORTIONAL, true);
                        if (false === stripos($arSection['IMAGE']['src'], 'cache'))
                            $arSection['IMAGE'] = [];
                    }

                    // заглушка при отсутствии изображения
                    if (empty($arSection['IMAGE']['src'])) {
                        $x++;
                        if ($x > 5) $x = 0;
                        $arSection['IMAGE'] = ['src' => '/local/images/background/' . $x . '.jpg'];
                    }

                    ?>
                    <div class="main-slider__kind">
                        <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="main-slider__kind-link"
                           <? if (!empty($arSection['IMAGE']['src'])): ?>
                               style="background-image: url('<?= $arSection['IMAGE']['src'] ?>');
                                background-position: <?= $arSection['POSITION'] ?>;
                               "
                           <? endif; ?>
                        >
                            <img class="main-slider__kind-slide" src="<?= $arSection['BACKGROUND'] ?>" alt="<?= $arSection['NAME'] ?>">
                        </a>
                        <!--p>Заголовок</p-->
                        <? if ($arSection['SHOW_NAME']): ?>
                            <p>
                                <a href="<?=$arSection['SECTION_PAGE_URL']?>"><?= $arSection['NAME'] ?></a>
                            </p>
                        <? endif; ?>
                    </div>
                <? endforeach; ?>
        </div>
    </div>
</section>
<!-- /Слайдер КОНЕЦ -->