<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

global $APPLICATION, $USER;

?><!-- Поисковая форма -->
<div class="container searches-block hidden">
    <form class="three-simple-search tss"
          data-session-id="<?= bitrix_sessid()?>"
          action="<?=!empty($arParams['ACTION_URL'])? $arParams['ACTION_URL'] : '/search/'?>"
          method="get">
        <input type="hidden" class="hidden" name="publishyear_prev" value="900"/>
        <input type="hidden" class="hidden" name="publishyear_next" value="2016"/>
        <input type="hidden" class="hidden" name="s_in_closed" value="on"/>
        <input type="hidden" class="hidden" name="is_full_search" value="on"/>
        <input type="hidden" class="hidden" name="librarytype" value="library"/>
        <fieldset>
            <legend><?= Loc::getMessage('SEARCH_SIMPLE_SEARCH') ?></legend>
            <div class="tss-row">
                <div class="tss-cell">
                    <input type="text"
                           name="q"
                           class="form-control input-lg"
                           data-autocomplete-item
                           data-autocomplete-full-api
                           placeholder="Пушкин, сказка о царе Салтане"
                           data-virtual-keyboard
                           value="<?= htmlspecialcharsEx($arRequest['QUERY']); ?>"/>
                </div>
                <div class="tss-cell">
                    <button class="btn btn-lg btn-primary"><?= Loc::getMessage('BUTTON_SEARCH') ?></button>
                </div>
            </div>
            <div class="appendix-row">
                <div class="three-collections-string tcs" data-string-checkbox-widget>
                    <select multiple="true"
                            name="newcollection[]"
                            langcollection="<?= Loc::getMessage('SEARCH_CHOISE_COLLECTION') ?>">
                        <?foreach($arResult['newcollection'] as $sRu => $sEn) {?>
                            <?$selected = '';
                            if(in_array($sRu, $arRequest['newcollection'])) {
                                $selected = ' selected="true"';
                            }?>
                            <option value="<?= $sRu; ?>"<?= $selected ?>>
                                <?= LANGUAGE_ID == 'en' ? $sEn : $sRu ?>
                            </option>
                        <?}?>
                    </select>
                    <span class="tcs-title"><?= Loc::getMessage('COLLECTIONS_SEARCH') ?></span>
                    <div class="more-list" data-other-widget>
                        <a href="#"
                           data-more-button
                           data-toggle="dropdown"
                           class="tcs-item more-toggler"
                           data-selected="false">
                            <?= Loc::getMessage('SEARCH_NAME_UPPER_TITLE') ?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" data-other-container>
                            <?foreach($arResult['newcollection'] as $sRu => $sEn) {?>
                                <?$selected = '';
                                if(in_array($sRu, $arRequest['newcollection'])) {
                                    $selected = ' data-selected="true"';
                                }?>
                                <a href="#" 
                                   class="tcs-item" 
                                   data-tcs-item data-value="<?= $sRu; ?>"<?= $selected ?>>
                                    <?= LANGUAGE_ID == 'en' ? $sEn : $sRu ?>
                                </a>
                            <?}?>
                        </ul>
                    </div>
                </div>
                <div class="tss-extended-toggler">
                    <a href="/search/extended/" class="btn btn-link btn-lg"><?= Loc::getMessage('EXTENDED_SEARCH') ?></a>
                </div>
            </div>
        </fieldset>
    </form>
    <form class="three-middle-search tms" 
          data-session-id="<?= bitrix_sessid()?>" 
          action="<?=!empty($arParams['ACTION_URL'])? $arParams['ACTION_URL'] : '/search/'?>" 
          method="get">
        <input type="hidden" class="hidden" name="publishyear_prev" value="900"/>
        <input type="hidden" class="hidden" name="publishyear_next" value="2016"/>
        <input type="hidden" class="hidden" name="s_in_closed" value="on"/>
        <input type="hidden" class="hidden" name="is_full_search" value="on"/>
        <input type="hidden" class="hidden" name="librarytype" value="library"/>
        <fieldset>
            <legend><?= Loc::getMessage('SEARCH_AUTHOR_AND_OR_TITLE') ?></legend>
            <div class="tms-row">
                <div class="tms-cell">
                    <label><?= Loc::getMessage('SEARCH_AUTHOR_UPPER_TITLE') ?></label>
                </div>
                <div class="tms-cell">
                    <input type="text" 
                           name="q_author" 
                           class="form-control input-lg" 
                           data-autocomplete-item 
                           data-query-field="authorbook" 
                           data-entity-field="title" 
                           placeholder="Пушкин"
                           data-virtual-keyboard 
                        <?if(isset($arRequest['q_author'])) {?>
                            value="<?= htmlspecialcharsEx($arRequest['q_author']) ?>"
                        <?}?>
                    />
                </div>
                <div class="tms-cell">
                    <button class="btn btn-lg btn-primary"><?= Loc::getMessage('BUTTON_SEARCH') ?></button>
                </div>
            </div>
            <div class="tms-row">
                <div class="tms-cell">
                    <label><?= Loc::getMessage('SEARCH_NAME_UPPER_TITLE') ?></label>
                </div>
                <div class="tms-cell">
                    <input
                        type="text"
                        name="q_name"
                        class="form-control input-lg"
                        data-autocomplete-item
                        data-query-field="title"
                        data-entity-field="title"
                        data-filtered-by="authorbook"
                        placeholder="Сказка о царе Салтане"
                        data-virtual-keyboard
                        <? if (isset($arRequest['q_name'])) {?>
                            value="<?= htmlspecialcharsEx($arRequest['q_name']) ?>"
                        <?}?> />
                </div>
                <div class="tms-cell">
                    <a class="btn btn-lg btn-link" href="#">
                        <?= Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_SEARCH') ?>
                    </a>
                </div>
            </div>
        </fieldset>
    </form>
</div>
<section class="main-search container">
    <div class="row">
        <ul class="nav nav-tabs col-md-12 hidden-xs secondary-tabs">
            <li role="presentation" <?if((!empty($arRequest['librarytype']) && ($arRequest['librarytype']=='library')) || empty($arRequest['librarytype']) ) {?> class="active" <?}?> >
                <a href="#"
                   class="main-search__label main-search__label--library"
                   data-type="library"
                   title="Поиск по библиотекам">
                    <?= Loc::getMessage('TAB_LIBRARY') ?>
                </a>
            </li>
            <li role="presentation" <?if(!empty($arRequest['librarytype']) && ($arRequest['librarytype']=='museum')) {?> class="active" <?}?> >
                <a href="#"
                   class="main-search__label main-search__label--museum"
                   data-type="museum"
                   title="Поиск по музеям">
                    <?= Loc::getMessage('TAB_MUSEUM') ?>
                </a>
            </li>
            <li role="presentation" <?if(!empty($arRequest['librarytype']) && ($arRequest['librarytype']=='archive')) {?> class="active" <?}?> >
                <a href="#"
                   class="main-search__label main-search__label--archive"
                   data-type="archive"
                   title="Поиск по архивам">
                    <?= Loc::getMessage('TAB_ARCHIVE') ?>
                </a>
            </li>
        </ul>
        <?$addGetParams = '';
        if(!empty($arRequest['plan_id'])){
            $addGetParams .= '<input type="hidden" name="plan_id" value="' . $arRequest['plan_id'] . '" />';
        } elseif(!empty($arParams['REQUEST_PARAMS']['plan_id'])) {
            $addGetParams .= '<input type="hidden" name="plan_id" value="' . $arParams['REQUEST_PARAMS']['plan_id'] . '" />';
        }
        if(!empty($arRequest['date'])){
            $addGetParams .= '<input type="hidden" name="date" value="' . $arRequest['date'] . '" />';
        } elseif(!empty($arParams['REQUEST_PARAMS']['date'])) {
            $addGetParams .= '<input type="hidden" name="date" value="' . $arParams['REQUEST_PARAMS']['date'] . '" />';
        }?>
        <form class="main-search__form col-md-12"
              action="<?= !empty($arParams['ACTION_URL'])? $arParams['ACTION_URL'] : '/search/' ?>"
              method="get"
              enctype="application/x-www-form-urlencoded"
              id="main_search_form">
            <?= $addGetParams ?>
            <!-- action="/search/" -->
            <div class="main-search__general">
                <div class="main-search__general-field">
                    <?if(!empty($arParams['REQUEST_PARAMS']['COLLECTION_ID'])) {?>
                        <input type="hidden" name="COLLECTION_ID" class="redmine17300" value="<?= $arParams['REQUEST_PARAMS']['COLLECTION_ID'] ?>">
                    <?}?>
                    <input type="text"
                           name="q"
                           value="<?= htmlspecialcharsEx($arRequest['QUERY']); ?>"
                           class="form-control main-search__general-field-input main-search_field-input"
                           placeholder="<?= Loc::getMessage('SEARCH_PLACEHOLDER') ?>"
                           data-src="<?= $componentPath . '/autocomplete.php?' . bitrix_sessid_get() ?>"
                           data-type="main-search-autocomplete"
                           data-library="<?= !empty($arRequest['librarytype']) ? $arRequest['librarytype'] : 'library'?>"
                           data-virtual-keyboard
                           id="asearch"
                           maxlength="1000"
                        <?if (mb_strlen($arRequest['q_author']) > 0 ||  mb_strlen($arRequest['q_name']) > 0) {?>
                           disabled="disabled"
                        <?}?>/>
                </div>
                <div class="main-search__author-main-field">
                    <input type="text"
                           name="q_author"
                           class="form-control main-search_field-input"
                           id="author_search"
                           data-autocomplete="1"
                           autocomplete="off"
                           data-type="additional-condition"
                           data-filter="authorbook"
                           maxlength="300"
                           data-src="<?= $componentPath . '/autocomplete.php?field=author&' . bitrix_sessid_get() ?>"
                           data-virtual-keyboard
                           placeholder="<?= Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_SEARCH_AUTHOR') ?>"
                        <?if (isset($arRequest['q_author'])) {?>
                            value="<?= htmlspecialcharsEx($arRequest['q_author']) ?>"
                        <?}?>
                        <?if (mb_strlen($arRequest['QUERY']) > 0) {?>
                            disabled="disabled"
                        <?}?> />
                </div>
                <div class="main-search__title-main-field">
                    <input type="text"
                           name="q_name"
                           class="form-control main-search_field-input"
                           id="name_search"
                           data-autocomplete="1"
                           autocomplete="off"
                           data-type="additional-condition"
                           data-filter="title"
                           maxlength="300"
                           data-src="<?= $componentPath . '/autocomplete.php?field=title&' . bitrix_sessid_get() ?>"
                           data-virtual-keyboard
                           placeholder="<?= Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_SEARCH_NAME') ?>"
                        <?if (isset($arRequest['q_name'])) {?>
                            value="<?= htmlspecialcharsEx($arRequest['q_name']) ?>"
                        <?}?>
                        <?if (mb_strlen($arRequest['QUERY']) > 0) {?>
                            disabled="disabled"
                        <?}?>/>
                </div>
                <div class="all-clean-general">
                    <a href="#" class="main-search__general-clean">+</a>
                </div>
                <?/*
                    <div class="input-group-btn btn-group-lg">
                        <button type="button" class="btn btn-default main-search__exsearch-btn" title="<?= Loc::getMessage('EXTENDED_SEARCH') ?>">
                            <span class="main-search__exsearch-btn-img"></span>
                        </button>
                    </div>
                    */?>
            </div>
            <div class="main-search__exsearch" <?if( array_filter($arRequest['text'])
                || ($arRequest['publishyear_prev'] != SEARCH_BEGIN_YEAR && $component->validatePrevDate($arRequest['publishyear_prev'], $arRequest['publishyear_next']))
                || ($arRequest['publishyear_next'] != date("Y") && $component->validateNextDate($arRequest['publishyear_prev'], $arRequest['publishyear_next']))
                || array_filter($arRequest['newcollection']) ) {?> style="display: block" <?}?>>
                <a href="#" class="main-search__exsearch-close"></a>
                <div class="main-search__exsearch-date clearfix">
                    <span class="main-search__exsearch-date-title"><?= Loc::getMessage('PUBLISHED') ?></span>
                    <label for="exsearch-date-from" class="main-search__exsearch-date-from">
                        <span><?= Loc::getMessage('PUBLISHED_FROM') ?></span>
                        <input id="exsearch-date-from"
                               type="text"
                               minlength="1"
                               maxlength="4"
                               class="main-search__exsearch-date-from-input"
                               name="publishyear_prev"
                               value="<?= $component->validatePrevDate($arRequest['publishyear_prev'], $arRequest['publishyear_next']) ? $arRequest['publishyear_prev'] : SEARCH_BEGIN_YEAR ?>" />
                    </label>
                    <div class="main-search__exsearch-date-range"></div>
                    <label for="exsearch-date-to" class="main-search__exsearch-date-to">
                        <span><?= Loc::getMessage('PUBLISHED_TO') ?></span>
                        <input id="exsearch-date-to"
                               type="text"
                               minlength="1"
                               maxlength="4"
                               class="main-search__exsearch-date-to-input"
                               name="publishyear_next"
                               value="<?= $component->validateNextDate($arRequest['publishyear_prev'], $arRequest['publishyear_next']) ? $arRequest['publishyear_next'] : date("Y") ?>" />
                        <span><?= Loc::getMessage('PUBLISHED_YEAR') ?></span>
                    </label>
                </div>
                <div class="col-md-12 main-search__exsearch-divider"></div>
                <div class="row">
                    <div class="col-md-12 clearfix">
                        <label class="main-search__exsearch-collection-label">
                            <?= Loc::getMessage('COLLECTIONS_SEARCH') ?>
                        </label>
                        <select class="main-search__exsearch-collection-select"
                                id="search-collections"
                                multiple name="newcollection[]"
                                langcollection="<?= Loc::getMessage('SEARCH_CHOISE_COLLECTION') ?>">
                            <?foreach($arResult['newcollection'] as $sRu => $sEn) {?>
                                <?$selected = '';
                                if(in_array($sRu, $arRequest['newcollection'])){
                                    $selected = ' selected="selected"';
                                }?>
                                <option value="<?= $sRu; ?>"<?= $selected ?>>
                                    <?= LANGUAGE_ID == 'en' ? $sEn : $sRu ?>
                                </option>
                            <?}?>
                        </select>
                    </div>
                </div>
                <?if(!empty($arRequest['theme']) and is_array($arRequest['theme'])) {
                    $i = 0;
                    foreach($arRequest['theme'] as $k => $v) {
                        if(count($arRequest['theme']) > 1) {
                            if ($v != 'foraccess' and empty($arRequest['text'][$k])) {
                                continue;
                            }
                        }?>
                        <div class="row main-search__additional-fields" data-add-search-widget>
                            <div class="col-md-12 main-search__exsearch-divider"></div>
                            <div class="col-sm-2 col-xs-6">
                                <div class="form-group">
                                    <select name="logic[<?= $i ?>]" class="form-control selectpicker">
                                        <option value="AND" <?= $arRequest['logic'][$k] == 'AND' ? 'selected="selected"' : ''?> >
                                            <?= Loc::getMessage('SEARCH_LOGIC_AND') ?>
                                        </option>
                                        <option value="OR" <?= $arRequest['logic'][$k] == 'OR' ? 'selected="selected"' : ''?> >
                                            <?= Loc::getMessage('SEARCH_LOGIC_OR') ?>
                                        </option>
                                        <option value="NOT" <?= $arRequest['logic'][$k] == 'NOT' ? 'selected="selected"' : ''?>>
                                            <?= Loc::getMessage('SEARCH_LOGIC_NOT') ?>
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <select name="theme[<?= $i ?>]" class="form-control selectpicker">
                                        <option value="ALL" <?= $v == 'ALL' ? 'selected="selected"' : ''?> >
                                            <?= Loc::getMessage('SEARCH_ALL') ?>
                                        </option>
                                        <option value="authorbook" <?= $v == 'authorbook' ? 'selected="selected"' : ''?> >
                                            <?= Loc::getMessage('SEARCH_authorbook') ?>
                                        </option>
                                        <option value="title" <?= $v == 'title' ? 'selected="selected"' : ''?> >
                                            <?= Loc::getMessage('SEARCH_title') ?>
                                        </option>
                                        <option value="publisher" <?= $v == 'publisher' ? 'selected="selected"' : ''?> >
                                            <?= Loc::getMessage('SEARCH_publisher') ?>
                                        </option>
                                        <option value="publishplace" <?= $v == 'publishplace' ? 'selected="selected"' : ''?> >
                                            <?= Loc::getMessage('SEARCH_publishplace') ?>
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group relative">
                                    <input name="text[<?= $i ?>]"
                                           type="text"
                                           class="form-control"
                                           value="<?php echo htmlspecialcharsbx($arRequest['text'][$k])?>"
                                           data-autocomplete
                                           data-type="additional-condition"
                                           data-src="<?= $componentPath . '/autocomplete.php?' . bitrix_sessid_get() ?>"
                                           data-virtual-keyboard
                                           maxlength="300"/>
                                </div>
                            </div>
                            <div class="col-sm-1 col-xs-12 text-right">
                                <?if($k == count($arRequest['theme'])) {?>
                                    <a class="btn btn-default add-next-row hidden">+</a>
                                    <a class="btn btn-default del-this-row">&minus;</a>
                                <?} else {?>
                                    <a class="btn btn-default add-next-row">+</a>
                                    <a class="btn btn-default del-this-row">&minus;</a>
                                <?}?>
                            </div>
                        </div>
                        <?$i++;?>
                    <?}?>
                <?} else {?>
                    <div class="row main-search__additional-fields" data-add-search-widget>
                        <div class="col-md-12 main-search__exsearch-divider"></div>
                        <div class="col-sm-2 col-xs-6">
                            <div class="form-group">
                                <select name="logic[0]" class="form-control selectpicker">
                                    <option value="AND"><?= Loc::getMessage('SEARCH_LOGIC_AND') ?></option>
                                    <option value="OR"><?= Loc::getMessage('SEARCH_LOGIC_OR') ?></option>
                                    <option value="NOT"><?= Loc::getMessage('SEARCH_LOGIC_NOT') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <select name="theme[0]" class="form-control selectpicker">
                                    <option value="ALL"><?= Loc::getMessage('SEARCH_ALL') ?></option>
                                    <option value="authorbook"><?= Loc::getMessage('SEARCH_authorbook') ?></option>
                                    <option value="title"><?= Loc::getMessage('SEARCH_title') ?></option>
                                    <option value="publisher"><?= Loc::getMessage('SEARCH_publisher') ?></option>
                                    <option value="publishplace"><?= Loc::getMessage('SEARCH_publishplace') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group relative">
                                <input name="text[0]"
                                       type="text"
                                       class="form-control"
                                       data-autocomplete
                                       data-type="additional-condition"
                                       data-src="<?= $componentPath . '/autocomplete.php?' . bitrix_sessid_get() ?>"
                                       maxlength="300"
                                       data-virtual-keyboard
                                />
                            </div>
                        </div>
                        <div class="col-xs-1 text-right col-xs-12">
                            <a class="btn btn-default add-next-row">+</a>
                            <a class="btn btn-default del-this-row">&minus;</a>
                        </div>
                    </div>
                <?}?>
                <!-- add here -->
                <div id="additional-placeholder"></div>
                <div class="col-md-12 main-search__exsearch-divider"></div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 main-search__link">
                        <label for="cb223">
                            <input type="checkbox"
                                   id="cb223"
                                   name="s_in_closed"
                                   value="on" <?if(true === $arParams['s_in_closed']) {?> checked="checked" <?}?>>
                            <span class="lbl">
                                <?= Loc::getMessage('SEARCH_IN_CLOSED') ?></span>
                        </label>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 main-search__exsearch-buttons" style="float: right;">
                        <button type="submit"
                                class="btn btn-primary main-search__submit-btn main-search__submit-btn--exsearch pull-right">
                            <?= Loc::getMessage('BUTTON_SEARCH') ?>
                        </button>
                    </div>
                </div>
            </div>

            <div class="addsearch-outside">
                <div class="ad-search-control<?if(array_filter($arRequest['text'])
                    || ($arRequest['publishyear_prev'] != SEARCH_BEGIN_YEAR && $component->validatePrevDate($arRequest['publishyear_prev'], $arRequest['publishyear_next']))
                    || ($arRequest['publishyear_next'] != date("Y") && $component->validateNextDate($arRequest['publishyear_prev'], $arRequest['publishyear_next']))
                    || array_filter($arRequest['newcollection'])) {?> hidden<?}?>">
                    <button type="submit" class="btn btn-primary main-search__submit-btn">
                        <span class="main-search__submit-btn-text"><?= Loc::getMessage('BUTTON_SEARCH'); ?></span>
                        &nbsp;
                        <span class="fa fa-search"></span>
                    </button>
                    <a href="#" class="main-search__exsearch-trigger">
                        <?= Loc::getMessage('EXTENDED_SEARCH') ?>
                    </a>
                </div>
                <div style="margin-left: -15px;" class="aftercontrol-row">
                    <div class="col-md-6 col-sm-6 radio main-search__exsearch-radio">
                        <label for="cb1">
                            <input type="radio" name="s_tc" id="cb1" value="on"
                                <?if(!empty($arRequest['s_tc'])) {?> checked="checked" <?}?>
                                <?if($arParams['SEARCH_IN_CATALOG'] == 'N') {?> disabled="disabled" <?}?>/>
                            <span class="lbl"><?= Loc::getMessage('SEARCH_IN_CATALOG_OF_PUBLICATIONS') ?></span>
                        </label>
                    </div>
                    <div class="col-md-6 col-sm-6 radio main-search__exsearch-radio">
                        <label for="cb38">
                            <input type="radio" name="is_full_search" id="cb38" value="on"
                                <?if(empty($arRequest['s_tc']) || !empty($arRequest['is_full_search'])
                                    || $arParams['SEARCH_IN_CATALOG'] == 'N'){?> checked="checked"<?}?> />
                            <span class="lbl"><?= Loc::getMessage('SEARCH_BY_NEB_TEXTS') ?></span>
                        </label>
                    </div>
                </div>
                <div style="margin-left: -15px; clear: left;" class="aftercontrol-row">
                    <div class="col-md-6 col-sm-6 checkbox main-search__exsearch-checkbox" >
                        <label for="cb4">
                            <input type="checkbox"
                                   id="cb4"
                                   name="s_strict"
                                   value="on" <?if(!empty($arRequest['s_strict'])) {?> checked="checked" <?}?>>
                            <span class="lbl"><?= Loc::getMessage('SEARCH_EXACT_PHRASE') ?></span>
                        </label>
                    </div>
                    <?if(!empty($arRequest['q'])) {?>
                        <div class="col-md-6 col-sm-6 checkbox main-search__exsearch-checkbox">
                            <label for="cb2">
                                <input type="checkbox"
                                       id="cb2"
                                       name="s_in_results"
                                       value="on" <?if(!empty($arRequest['s_in_results'])) {?> checked="checked" <?}?>>
                                <span class="lbl"><?= Loc::getMessage('SEARCH_WITHIN_RESULTS') ?></span>
                            </label>
                        </div>
                    <?}?>
                    <!--<div class="col-md-6 col-sm-6 checkbox main-search__exsearch-checkbox">
                        <label for="cb223">
                            <input type="checkbox"
                            id="cb223"
                            name="s_in_closed"
                            value="on" <?if(true === $arParams['s_in_closed']) {?> checked="checked" <?}?> >
                            <span class="lbl"><?= Loc::getMessage('SEARCH_IN_CLOSED') ?></span>
                        </label>
                    </div>-->
                    <?if ($USER->IsAdmin() || nebUser::checkElar() || EXALEAD_DEBUG_MODE) {?>
                        <div class="col-md-6 col-sm-6 checkbox main-search__exsearch-checkbox">
                            <label for="debug_cb">
                                <input type="checkbox" id="debug_cb" name="debug" value="1"
                                    <?if(EXALEAD_DEBUG_MODE) {?> checked="checked" <?}?> />
                                <span class="lbl"><?= Loc::getMessage('SEARCH_DEBUG') ?></span>
                            </label>
                        </div>
                    <?}?>
                </div>
                <div class="clearfix"></div>
            </div>
            <input class="main-search__type-input"
                   type="hidden"
                   name="librarytype"
                   value="<?= !empty($arRequest['librarytype']) ? htmlspecialcharsbx($arRequest['librarytype']) : 'library'; ?>" />
            <?if(!empty($arRequest['f_field'])) {?>
                <?foreach($arRequest['f_field'] as $key => $field) {?>
                    <?if(!is_array($field)) {
                        $field = array($field);
                    }?>
                    <?foreach($field as $fieldValue) {?>
                        <input type="hidden" value="<?= $fieldValue ?>" name="f_field[<?= $key ?>][]" />
                    <?}?>
                <?}?>
            <?}?>
            <?if (!empty($arRequest['category_id'])) {?>
                <input type="hidden" name="category_id" value="<?= htmlspecialcharsbx($arRequest['category_id']) ?>" />
            <?}?>
            <?if (!empty($arRequest['category_name'])) {?>
                <input type="hidden" name="category_name" value="<?= htmlspecialcharsbx($arRequest['category_name']) ?>" />
            <?}?>
        </form>
        <div class="row main-search__additional-fields ms-ad-template" data-add-search-widget>
            <div class="col-md-12 main-search__exsearch-divider"></div>
            <div class="col-sm-2 col-xs-6">
                <div class="form-group">
                    <select name="logic" class="form-control">
                        <option value="AND"><?= Loc::getMessage('SEARCH_LOGIC_AND') ?></option>
                        <option value="OR"><?= Loc::getMessage('SEARCH_LOGIC_OR') ?></option>
                        <option value="NOT"><?= Loc::getMessage('SEARCH_LOGIC_NOT') ?></option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group">
                    <select name="theme" class="form-control">
                        <option value="ALL"><?= Loc::getMessage('SEARCH_ALL') ?></option>
                        <option value="authorbook"><?= Loc::getMessage('SEARCH_authorbook') ?></option>
                        <option value="title"><?= Loc::getMessage('SEARCH_title') ?></option>
                        <option value="publisher"><?= Loc::getMessage('SEARCH_publisher') ?></option>
                        <option value="publishplace"><?= Loc::getMessage('SEARCH_publishplace') ?></option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="form-group relative">
                    <input name="text" type="text" class="form-control"
                        data-autocomplete
                        data-type="additional-condition"
                        data-src="<?= $componentPath . '/autocomplete.php?' . bitrix_sessid_get() ?>"
                        maxlength="300" />
                </div>
            </div>
            <div class="col-xs-1 col-xs-12 text-right">
                <a class="btn btn-default add-next-row">+</a>
                <a class="btn btn-default del-this-row">&minus;</a>
            </div>
        </div>
    </div>
</section>
<!-- /Поисковая форма КОНЕЦ-->

<?$APPLICATION->IncludeFile(
    '/local/components/exalead/search.form/templates/.default/search_tags.php',
    array(
        'arParams' => $arParams,
        'arResult' => $arResult,
        'component' => $component
    ));?>