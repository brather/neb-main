<?php
/**
 * User: agolodkov
 * Date: 04.10.2016
 * Time: 17:27
 */

namespace AssessorBundle\DependencyInjection;


use BitrixBundle\DependencyInjection\BitrixHelper;
use GuzzleHttp\Client;

class StatsApiReader
{
    private $_bitrix;
    private $_statsApi;

    public function __construct(BitrixHelper $bitrix, Client $statsApi)
    {
        $this->_bitrix = $bitrix;
        $this->_statsApi = $statsApi;
    }

    public function getReport($name, $dateFrom = null, $dateTo = null)
    {
        $query = [
            'report' => $name,
        ];
        if ($dateFrom) {
            $query['dateFrom'] = date('Y-m-d', $dateFrom);
        }
        if ($dateTo) {
            $query['dateTo'] = date('Y-m-d', $dateTo);
        }
        $query = http_build_query($query);
        $token = $this->_bitrix->getOption('neb.main', 'stats_reader_token');
        $host = $this->_bitrix->getOption('neb.main', 'stats_host');
        $url = $host . 'reports?access-token=' . $token . '&' . $query;
        $response = $this->_statsApi->get($url);
        return json_decode($response->getBody(), true);
    }
}