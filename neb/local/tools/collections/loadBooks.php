<?php require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('nota.library');
CModule::IncludeModule('nota.exalead');

use \Bitrix\Main\Application,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient,
    \Nota\Library\Collection;

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

if(!empty($arRequest)) {

    $arFilterBooks = array(
        'IBLOCK_ID'         => $arRequest['iblockId'],
        'IBLOCK_SECTION_ID' => $arRequest['sectionId'],
        '!ID'               => $arRequest['items']
    );

    $rsBooks = \Bitrix\Iblock\ElementTable::getList(array(
        'filter' => $arFilterBooks,
        'select' => array('ID', 'NAME'),
        'limit' => $arRequest['countInSlide']
    ));
    $arBooksId = array();
    $arBooksIBlockId = array();
    while($obBook = $rsBooks->fetch()) {
        $idExaleadBook = CIBlockElement::GetProperty($arRequest['iblockId'], $obBook['ID'], array("sort" => "asc"), array('CODE' => 'BOOK_ID'))->Fetch();
        $arBooksId[] = $idExaleadBook["VALUE"];
        $arBooksIBlockId[$idExaleadBook["VALUE"]] = $obBook['ID'];
    }

    $query = new SearchQuery();
    $client = new SearchClient();
    $query->setParam('sl', 'sl_nofuzzy');
    $query->getByArIds($arBooksId);
    $query->setPageLimit(count($arBooksId));
    $result = $client->getResult($query);

    $arReturn = array();?>
    <?foreach($result['ITEMS'] as $iBook => $arBook) {?>
        <?$statView = Collection::getBookStat($arBook['id'], true);?>
        <div class="clsp__editions-list-item"
             data-book-id="<?= $arBooksIBlockId[$arBook['id']] ?>">
            <a href="<?= $arBook['DETAIL_PAGE_URL'] ?>">
                <img class="loadingimg"
                     src="<?= $arBook['IMAGE_URL'] ?>"
                     alt=<?= $arBook['title'] ?>>
            </a>
            <div class="b-lib_counter">
                <div class="icoviews">
                    x <?= number_format($statView['VIEW_CNT'], 0, '', ' ') ?>
                </div>
            </div>
        </div>

    <?}
}