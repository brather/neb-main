<?
$MESS['MARKUP_LOG_SORT']            = 'Сортировать';
$MESS['MARKUP_LOG_SORT_AUTHOR']     = 'По автору';
$MESS['MARKUP_LOG_SORT_MAIN_TITLE'] = 'По названию';
$MESS['MARKUP_LOG_SORT_DATE']       = 'По дате';
$MESS['MARKUP_LOG_RESET']           = 'Сбросить';

$MESS['MARKUP_LOG_TITLE']           = 'Поиск по изданиям';
$MESS['MARKUP_LOG_FROM']            = 'C';
$MESS['MARKUP_LOG_TO']              = 'по';
$MESS['MARKUP_LOG_TOTAL']           = 'Всего найдено';
$MESS['MARKUP_LOG_AUTHOR']          = 'Автор';
$MESS['MARKUP_LOG_BOOK']            = 'Издание';
$MESS['MARKUP_LOG_WATCH']           = 'Просмотр';
$MESS['MARKUP_LOG_LOG']             = 'Лог';

$MESS['MARKUP_LOG_STATUS_ADDED']         = 'Добавлено';
$MESS['MARKUP_LOG_STATUS_CONSIDERATION'] = 'Утверждение';
$MESS['MARKUP_LOG_STATUS_CONFIRMED']     = 'Утверждено';
$MESS['MARKUP_LOG_STATUS_DELETED']       = 'Удалено';

