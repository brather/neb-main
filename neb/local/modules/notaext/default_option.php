<?
$notaext_default_option = array(#"typograph_mdash_test" => "wewewe",
);

/*
Устанавливаем опции по умолчанию для расширений
*/
$dirExt = __DIR__ . '/ext/';
$extDir = scandir($dirExt);
if (!empty($extDir)) {
    foreach ($extDir as $dir) {
        if ($dir == '.' or $dir == '..' or !is_dir($dirExt . $dir))
            continue;

        if (file_exists($dirExt . $dir . '/default_option.php')) {
            include_once($dirExt . $dir . '/default_option.php');
            $notaext_default_option = array_merge($notaext_default_option, ${$dir . '_default_option'});
        }
    }
}
