<?
/**
 * проверка на наличие заклыдки
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

// $book_id = '000199_000009_005422931';
// $page=1;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized())
	exit();

CModule::IncludeModule("nota.userdata");
use Nota\UserData\Bookmarks;


$book_id = htmlspecialcharsbx(trim($_REQUEST['book_id']));
$page = intval($_REQUEST['page']);

//$book_id = htmlspecialcharsbx(trim($_REQUEST['book_id']));
if(empty($book_id))
	exit();

// CModule::IncludeModule("nota.userdata");
// use Nota\UserData\Bookmarks;

$result = Bookmarks::getMark($book_id,$page);

echo $result['ID'];