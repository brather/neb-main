<?php

CBitrixComponent::includeComponentClass('rest:rest.server');

use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\SectionElementTable;
use \Bitrix\Main\Application;
use Bitrix\Main\DB\SqlExpression;
use Bitrix\Main\Loader;
use Neb\Main\Iblock\SectionUfPropertyTable;

/**
 * Class RestCollection
 */
class RestCollection extends RestServer
{
    /**
     * @var array
     */
    protected $_params  = ['action'=> null, 'limit' => null, 'offset' => null, 'collectionid' => null];

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams()
    {
        $arRequest = static::getRequest();

        foreach ($this->_params as $sParam => $sValue)
            $this->_params[$sParam] = trim($arRequest[$sParam]);

        return $this;
    }

    /**
     * @return int
     */
    public function getAction()
    {
        switch ($this->_params['action']) {
            case 'getcollectionlist':
                $this->getCollectionList();
                break;
            case 'getcollectioncontent':
                $this->getCollectionContent();
                break;
            default:
                $this->restError(403, 'Parameter "action" incorrect!');
        }

        return 200;
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    protected static function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Получение Первого уровня коллекций только если у либо у их предков
     * есть активные элементы.
     */
    protected function getCollectionList()
    {
        $arResult   = array();
        $tmpResult  = array();


        Loader::includeModule('iblock');
        Loader::includeModule('neb.main');

        /**
         * Выборка Разделов Ифоблока IBLOCK_ID_COLLECTION
         * c LEFT JOIN по таблице b_file
         * лимиты и офсеты указываем исходя из пришедших параметров
         * */
        $db = \Bitrix\Iblock\SectionTable::getList(array(
            'select'    => array(
                'ID',
                'NAME',
                'DEPTH_LEVEL',
                'SUBDIR'    => 'FILE.SUBDIR',
                'FILE_NAME' => 'FILE.FILE_NAME',
                'SLIDER' => 'UF_PROP.UF_SLIDER'
            ),
            'filter'    => array(
                '=IBLOCK_ID'         => IBLOCK_ID_COLLECTION,
                '=GLOBAL_ACTIVE'     => 'Y',
            ),
            'order'     => array('LEFT_MARGIN' => 'ASC'),
            'limit'     => ((int)$this->_params['limit'] > 0)? (int)$this->_params['limit'] : false,
            'offset'    => ((int)$this->_params['offset'] > 0)? (int)$this->_params['offset'] : false,
            'runtime'   => array(
                new \Bitrix\Main\Entity\ReferenceField(
                    'FILE',
                    '\Bitrix\Main\FileTable',
                    array('=this.DETAIL_PICTURE' => 'ref.ID'),
                    array('join_type' => 'LEFT')
                ),
                new \Bitrix\Main\Entity\ReferenceField(
                    'UF_PROP',
                    '\Neb\Main\Iblock\SectionUfPropertyTable',
                    array('=this.ID' => 'ref.VALUE_ID'),
                    array('join_type' => 'LEFT')
                )
            )
        ));

        /* построение дерева, что бы каждый раздел знал ID своего первого предка */
        $arTree     = array();
        $fLevel = 0;

        /* Перебор и запись в массив резульаттов arResult */
        while ($res = $db->fetch())
        {
            if ($res['DEPTH_LEVEL'] == 1)
            {
                $fLevel = $res['ID'];

                if ($res['SLIDER'] == 1)
                {
                    if (!empty($res['SUBDIR']) && !empty($res['FILE_NAME']))
                        $sPicture = URL_SCHEME . '://' . $_SERVER['SERVER_NAME']
                            . '/upload/' . $res['SUBDIR'] . '/' . $res['FILE_NAME'];
                    else
                        $sPicture = '';

                    $tmpResult[$res['ID']] = array(
                        'id' => $res['ID'],
                        'name' => $res['NAME'],
                        'picture' => $sPicture
                    );
                }
            }

            $arTree[$res['ID']] = $fLevel;
        }

        $implode = implode(',', array_keys($arTree));
        $in = new SqlExpression($implode);

        $db = ElementTable::getList(array(
            'select' => array('IBLOCK_SECTION_ID'),
            'filter' => array(
                '=IBLOCK_ID' => IBLOCK_ID_COLLECTION,
                '=ACTIVE' => 'Y',
                '@IBLOCK_SECTION_ID' => $in,
            ),
            'group' => array('IBLOCK_SECTION_ID')
        ));

        while ($res = $db->fetch())
        {
            if ( isset( $arTree[ $res['IBLOCK_SECTION_ID'] ] ) )
            {
                $idSection = $arTree[ $res['IBLOCK_SECTION_ID'] ];

                if ((int)$idSection > 0)
                    $arResult[$idSection] = $idSection;
            }
        }

        foreach ($tmpResult as $key => $item)
            if (!isset($arResult[$key]))
                unset($tmpResult[$key]);

        // требование от Леши, чтобы на выходе был массив, а не словарь с ключами
        $this->arResult['result'] = array_values($tmpResult);
    }

    /**
     * Получение идентификаторов подборов из коллекций
     *
     * @return bool
     */
    protected function getCollectionContent() {

        $arResult = $arTreeSections = [];

        $iIBlock = IBLOCK_ID_COLLECTION;

        $iCollection = intval($this->_params['collectionid']);
        if ($iCollection < 1)
            $this->restError(404, 'Parameter "collectionid" is empty');

        // проверка на существование коллекции и её активность
        $sQuery =  <<<SQL
SELECT
  s.ID,
  s.ACTIVE,
  s.LEFT_MARGIN,
  s.RIGHT_MARGIN
FROM
  b_iblock_section as s
  
WHERE
  s.IBLOCK_ID =  $iIBlock AND s.ID = $iCollection
SQL;
        $arCollection = Application::getConnection()->query($sQuery)->fetch();
        if ($arCollection['ID'] <= 0)
            $this->restError(404, 'The collection is not found!');
        elseif ($arCollection['ACTIVE'] != 'Y')
            $this->restError(403, 'The collection is not active!');

        // выборка дерева глобально активных разделов
        $rsSections = CIBlockSection::GetList(
            ['LEFT_MARGIN' => 'ASC'],
            [
                '=IBLOCK_ID'     => $iIBlock,
                '>=LEFT_MARGIN'  => $arCollection['LEFT_MARGIN'],
                '<=RIGHT_MARGIN' => $arCollection['RIGHT_MARGIN'],
                'GLOBAL_ACTIVE'  => 'Y'
            ],
            false,
            ['ID']
        );
        while ($arFields = $rsSections->Fetch()) {
            $arTreeSections[] = $arFields['ID'];
        }
        $sCollections = implode(',', $arTreeSections);

        if (!empty($sCollections)) {

            // основной запрос
            $sQuery = <<<SQL
SELECT
  e.ID,
  v.VALUE
  
FROM
  b_iblock_element as e
  
INNER JOIN b_iblock_section_element as s
  ON e.ID = s.IBLOCK_ELEMENT_ID AND s.IBLOCK_SECTION_ID IN ($sCollections)

INNER JOIN b_iblock_property as p
  ON e.IBLOCK_ID = p.IBLOCK_ID AND p.CODE = "BOOK_ID"

INNER JOIN b_iblock_element_property as v
  ON e.ID = v.IBLOCK_ELEMENT_ID AND p.ID = v.IBLOCK_PROPERTY_ID

WHERE
  e.IBLOCK_ID =  $iIBlock
    AND e.ACTIVE = "Y"

ORDER BY 
  e.SORT ASC,
  e.ID ASC
SQL;

            if (intval($this->_params['limit']) > 0)
                $sQuery .= ' LIMIT ' . intval($this->_params['limit']);

            if (intval($this->_params['offset']) > 0)
                $sQuery .= ' OFFSET ' . intval($this->_params['offset']);

            $rsCollections = Application::getConnection()->query($sQuery);
            while ($arFields = $rsCollections->fetch())
                $arResult[] = $arFields['VALUE'];
        }

        $this->arResult['result'] = $arResult;
    }
}