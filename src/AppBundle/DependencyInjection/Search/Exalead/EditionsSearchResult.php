<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 13:30
 */

namespace AppBundle\DependencyInjection\Search\Exalead;


use AppBundle\Entity\Edition;
use AppBundle\Entity\GroupCategory;
use AppBundle\Entity\SearchResultEntityInterface;
use AppBundle\Traits\ExaleadParseResultTrait;

class EditionsSearchResult extends SearchResult
{
    use ExaleadParseResultTrait;

    /**
     * @return SearchResultEntityInterface[]
     */
    public function buildEntities()
    {
        $entities = [];
        $xml = $this->getXml();
        $bookData = $this->parseBookData($xml);
        if (!empty($bookData['items'])) {
            foreach ($bookData['items'] as $item) {
                $entity = new Edition();
                $this->_applyEntityData($entity, $item);
                $entities[] = $entity;
            }
        }


        return $entities;
    }

    /**
     * @param string $group
     *
     * @return array
     */
    public function buildAnswerGroupEntities($group)
    {
        $entities = [];
        $xml = $this->getXml();
        $groupsData = $this->parseAnswerGroups($xml);
        if (isset($groupsData[$group])) {
            foreach ($groupsData[$group] as $item) {
                $entity = new GroupCategory();
                $this->_applyEntityData($entity, $item);
                $entities[] = $entity;
            }
        }

        return $entities;
    }

    /**
     * @param $entity
     * @param $data
     */
    protected function _applyEntityData($entity, $data)
    {
        foreach ($data as $field => $value) {
            $method = 'set' . ucfirst($field);
            if (method_exists($entity, $method)) {
                $entity->$method($value);
            } elseif (property_exists($entity, $field)) {
                $entity->$field = $value;
            }
        }
    }
}