/* Author:
NOTAMEDIA http://notamedia.ru/
*/
var OLDBROWSER = !Modernizr.backgroundsize || !Modernizr.svg, WINDOW, HTML, DOCUMENT, BODY;
var backgroundsize = Modernizr.backgroundsize;

(function($) { //create closure
	$.fn.cleanWS = function(options) {
		this.each(function() {
			var iblock = this,
				par = iblock.parentNode,
				prev = iblock.previousSibling,
				next = iblock.nextSibling;
			while (prev) {
				var newprev = prev.previousSibling;
				if (prev.nodeType === 3 && prev.nodeValue) {
					for (var i = prev.nodeValue.length - 1; i > -1; i--) {
						var cc = prev.nodeValue.charCodeAt(i);
						if (cc == 9 || cc == 10 || cc == 32) {
							prev.nodeValue = prev.nodeValue.slice(0, i);
						} else {
							break;
						}
					}
				}
				if (prev.nodeType == 8) par.removeChild(prev); // remove comment
				prev = newprev;
			}
			while (next) {
				var newnext = next.nextSibling;
				if (next.nodeType == 3 && next.nodeValue) {
					while (next.nodeValue.length) {
						var cc = next.nodeValue.charCodeAt(0);
						if (cc == 9 || cc == 10 || cc == 32) {
							next.nodeValue = next.nodeValue.slice(1);
						} else {
							break;
						}
					}
				}
				if (next.nodeType == 8) par.removeChild(next); // remove comment
				next = newnext;
			}

		});
	};
	//end of closure
})(jQuery);

// uipopup
(function($) { //create closure
	$.fn.uipopup = function(options) {
		this.each(function() {
			//			$(this).on('click', function(e) {
			//				e.preventDefault()\;
			var lnk = $(this),
				popup = $('.popup'),
				H = (lnk.data('height')) ? lnk.data('height') : 'auto',
				W = (lnk.data('width')) ? lnk.data('width') : 560,
				url = lnk.attr('href'),
				clone = popup.clone(),
				hp = $('.homepage ');
			var scroll_pos = $(window).scrollTop();
			var closebtn = $('<a href="#" class="closepopup">Закрыть окно</a>');
			var page = $(lnk).data('page');

			if (!$(this).hasClass('grad') && !$(this).hasClass('no_scroll')) {
				$(".ui-dialog .ui-dialog-content").dialog('close');
				$(window).scrollTop(scroll_pos);
			}

			if ($(this).hasClass('no_scroll')) {
				var curentPopup = $('.b-bookpopup');
				var prevHeight = curentPopup.height();
				curentPopup.height(prevHeight);
				curentPopup.css('border', '2px solid #007894');
				curentPopup.children('div').css('border', 'none').fadeOut('slow');
				curentPopup.prepend('<div class="boockard-preloader"></div>');
				$('.boockard-preloader').fadeIn('slow');
				$.ajax({
					url: lnk.attr('href'),
					method: 'GET',
					success: function(html) {
						//Вставляем контент
						$('.boockard-preloader').fadeOut();
						curentPopup.html('');
						curentPopup.html(html);
						curentPopup.height('auto');

						//Кнопка закрытия диалога
						if (lnk.hasClass('closein')) {
							if (lnk.hasClass('coverlay')) {
								closebtn = curentPopup.find('.closepopup');
							} else {
								closebtn.appendTo(curentPopup);
							}
						} else {
							if (!lnk.hasClass('button_red') && !lnk.hasClass('coverlay')) closebtn.appendTo('body');
						}
						closebtn = curentPopup.find('.closepopup');
						closebtn.addClass('bookclose');
						//Скрываем кнопку закрытия окна, если оно открыто на отдельной странице
						if ($(curentPopup).parents('section.innersection').length === 1) {
							closebtn.hide();
						}
						closebtn.click(function(event) {
							event.preventDefault();
							$('.ui-widget-overlay').click();
						});
						//Обработчики
						curentPopup.find('.js_multipleslider').mulislider();
						$('.iblock', curentPopup).cleanWS();
						$('.b-form', curentPopup).initForm();
						$('.b-line_social', curentPopup).share();
						$('.book_slider', curentPopup).bookslider();
						$('.js_threeslide', curentPopup).threeslide();
						$('.js_flex_bgimage', curentPopup).flexbgimage();
						$('.js_description', curentPopup).descrPopup();
						$('.boockard-read-button').click(function(event) {
							$('.boockard-error-msg').hide();
							readBook(event, this);
							setTimeout(function() {
								if ($('.boockard-read-button').data('load') != 'load') $('.boockard-error-msg').text('Ошибка сервера. Пожалуйста, повторите запрос').show();
							}, 1000);
						});
						curentPopup.closest('.ui-dialog ').addClass('bookcard');
						curentPopup.css('border', 'none');
						$('.read-width-btn').click(function() {
							var list = $('.read-with-wrapper');
							if (list.hasClass('open')) {
								list.removeClass('open');
								$(this).removeClass('opened');
								list.slideUp('slow');
							} else {
								list.addClass('open');
								$(this).addClass('opened');
								list.slideDown('slow');
							}
						});


						var mappage = false;
						$('.link-nearest-library').click(function(e) {
							e.preventDefault();
							var mapContainer = $('.library-map');
							mapContainer.slideToggle({
								start: function() {
									if (false === mappage) {
										mapContainer.mappage();
										mappage = true;
									}
								}
							});
							e.stopImmediatePropagation();
						});
						$('.same-selector').click(function(event) {
							$('.same-selector').removeClass('same-selector--active');
							$(this).addClass('same-selector--active');
						});
						//Правим ссылку для селектора вывода экземпляров
						$('.same-selector-all').attr('href', $('.same-selector-all').attr('href')+'?PAGEN_1='+page);
						;
					},
					error: function() {
						curentPopup.children('div').css('border', '2px solid #007894').fadeIn();
						$('.boockard-error-msg').html('Произошла ошибка, попробуйте еще раз');
					}
				});
				return false;
			}
			/*
							if (lnk.closest('.ui-dialog').hasClass('popup')) {
								clone.dialog("destroy").remove();
								ajaxclone.dialog("destroy").remove();
							}
			*/
			if (lnk.hasClass('ajax_opener')) {
				var ajaxclone = $('<div class="uipopupcontainer hidden"></div>');
				ajaxclone.appendTo('body');
				var prevUrl = false;
				ajaxclone.dialog({
					closeOnEscape: true,
					dialogClass: 'no-close ajaxpopup',
					modal: true,
					draggable: false,
					resizable: false,
					width: ($(window).width() > 995) ? W : "100%",
					height: H,
					position: (lnk.data('posmy') && lnk.data('posat')) ? {
						my: (lnk.data('posmy')) ? lnk.data('posmy') : "",
						at: (lnk.data('posat')) ? lnk.data('posat') : "",
						of: window
					} : {
						my: "center top",
						at: "center top",
						of: window
					},
					open: function() {
						$('.ui-widget-content').css({
							'background': 'none'
						});
						$.ajax({
							url: lnk.attr('href'),
							method: 'GET',
							success: function(html) {
								ajaxclone.html(html);
								if (typeof(PopupCallbacks) !== 'undefined') {
									$.each(PopupCallbacks, function(idx, callback) {
										callback();
									});
								}
								$('.ui-widget-content').css({
									'background': '#ffffff'
								});
								if (lnk.hasClass('closein')) {
									if (lnk.hasClass('coverlay')) {
										closebtn = ajaxclone.find('.closepopup');
									} else {
										closebtn.appendTo(ajaxclone);
									}
								} else {
									if (!lnk.hasClass('button_red') && !lnk.hasClass('coverlay')) closebtn.appendTo('body');
								}
								ajaxclone.find('.js_multipleslider').mulislider();
								$('.iblock', ajaxclone).cleanWS();
								$('.b-form', ajaxclone).initForm();
								$('.b-line_social', ajaxclone).share();

								function initGraph(graph) {
									var cont = graph,
										tit = lnk.data('graph-title'),
										url = cont.data('sorce'),
										name = lnk.data('graph-name'),
										x = lnk.data('graph-x'),
										y = lnk.data('graph-y');
									var xname = [];
									if (x) {
										xname = x.split(',');
									}
									$.ajax({
										url: url,
										type: 'GET',
										async: true,
										dataType: "json",
										success: function(data) {
											cont.highcharts({
												chart: {
													// Edit chart spacing
													spacingBottom: 15,
													spacingTop: 12,
													spacingLeft: 10,
													spacingRight: 20,

													// Explicitly tell the width and height of a chart
													width: null,
													height: null
												},

												title: {
													text: tit,
													x: -20, //center
													style: {
														fontSize: '22px'
													}
												},
												subtitle: {
													text: '',
													x: -20
												},
												xAxis: {
													categories: xname,
													labels: {
														style: {
															fontSize: '14px'
														}
													}
												},
												yAxis: {
													title: {
														text: y,
														style: {
															fontSize: '14px'
														}
													},
													plotLines: [{
														value: 0,
														width: 1,
														color: '#808080'
													}],
													labels: {
														style: {
															fontSize: '14px'
														}
													}
												},

												tooltip: {
													valueSuffix: 'просмотра'
												},
												plotOptions: {
													line: {
														dataLabels: {
															enabled: true,
															style: {
																fontSize: '14px'
															}
														},
														enableMouseTracking: false
													}
												},
												legend: {
													itemStyle: {
														fontSize: '14px'
													}
												},
												series: [data]
											});
										}
									});
								}

								var gg = $('#highcharts', ajaxclone);
								initGraph(gg);
								$('.b-fieldeditable', ajaxclone).fieldEditable();
								$('.js_select', ajaxclone).selectReplace();
								$('input.checkbox:not(".custom")', ajaxclone).replaceCheckBox();

								if (lnk.hasClass('noclose')) {
									closebtn.addClass('noclose');
								}
								var overlay = $('.ui-widget-overlay');
								if (lnk.data('overlaybg') == 'dark') {
									overlay.addClass('dark');
								}


								if (lnk.hasClass('coverlay')) {
									$('.book_slider', ajaxclone).bookslider();
									$('.js_threeslide', ajaxclone).threeslide();
									$('.js_flex_bgimage', ajaxclone).flexbgimage();
									$('.js_description', ajaxclone).descrPopup();

									$(window).scrollTop(0);
									var bookimg = ajaxclone.find('.b-bookframe_img');
									closebtn = ajaxclone.find('.closepopup');
									closebtn.addClass('bookclose');
									overlay.css({
										'background': '#777',
										'background-size': '100% auto',
										'opacity': 0
									});

									bookimg.on('load', function() {
										overlay.animate({
											'opacity': 1
										}, 1500, 'easeOutQuad');
									});

									closebtn.click(function(e) {
										e.preventDefault();
										ajaxclone.dialog("close");

									});
									ajaxclone.closest('.ui-dialog ').addClass('bookcard');

									// А вот так  меняется ссылка
									if (url != window.location.href) {
										//var location = window.history.location || window.location;
										//history.pushState(null, null, this.href);
										prevUrl = window.location.href;
										window.history.pushState(null, null, url);
									}
								}

								if (lnk.data('popupsize') == 'high') {
									hp.height(ajaxclone.height());
								}
							}
						});

						ajaxclone.removeClass('hidden');
						closebtn.show();

						/*$(window).one('resize.popup', function(){
						ajaxclone.dialog("close");
						});*/

						closebtn.click(function(e) {
							e.preventDefault();
							ajaxclone.dialog("close");

						});
					},

					close: function() {
						ajaxclone.remove();
						hp.height('');
						hp.css('overflow', '');
						if (prevUrl)
							window.history.pushState(null, null, prevUrl);
						closebtn.hide();
					},
				});

				$('.ui-widget-overlay').click(function() {
					//Close the dialog
					ajaxclone.dialog("close");
				});


				/*$('.btrefuse ').on('click', function() {
					alert(1);
					$(ajaxclone.closest('.ui-dialog')).remove();
						ajaxclone.remove();
						return false;
				}); */


			} else {
				clone.dialog({
					closeOnEscape: true,
					dialogClass: 'no-close commonpopup '+popup.data('class')+'',
					modal: true,
					draggable: false,
					resizable: false,
					width: ($(window).width() > 540) ? W : 300,
					height: 'auto',
					//height: ($(window).width() > 540) ? H: H+225,
					position: (lnk.data('posmy') && lnk.data('posat')) ? {
						my: (lnk.data('posmy')) ? lnk.data('posmy') : "",
						at: (lnk.data('posat')) ? lnk.data('posat') : "",
						of: lnk
					} : {
						my: "right top",
						at: "right bottom",
						of: lnk
					},

					open: function() {
						/*window.setTimeout(function() {
							clone.dialog("option", "position", {
								my: "right top",
								at: "right bottom",
								of: lnk
							});
							}, 50); */
						/*$(window).one('resize.clone', function(){
						clone.dialog("close");
						}); */

						if (clone.hasClass('change_log_pass')) {
							$('<a href="#" class="closepopup">Закрыть окно</a>').appendTo(clone);
							$('.ui-widget-overlay').addClass('dark');
							$('.change_log_pass .closepopup').click(function(e) {
								e.preventDefault();
								clone.dialog("close");

							});
						}

						clone.find('.b-form').initForm();
					},
					close: function() {
						clone.remove();

					}
				});


				$('.ui-widget-overlay').click(function() {
					//Close the dialog
					clone.dialog("close");

				});

			}
			//			});
		});
	};
	//end of closure
})(jQuery);

var touch = ($('html').hasClass('no-touch'))? false : true;

$(function(){
	//document.onload

	if(document.all && !window.atob){
	// код для IE9 и ниже
		$('.oldie').oldiePopup();
		return false;
	}

	WINDOW = $(window);
	HTML = $('html');
	DOCUMENT = $(document);
	BODY = $('body');


	$('.iblock').cleanWS();
	//$('#highcharts').initGraph();
	//$('.b-bookpage').cardPage();
	$('.b-quote_item').quoteInit();
	$('.js_slider_single').slick({
		adaptiveHeight: true,
		dots: true,
		cssEase: 'ease',
		speed: 600
	});

	$('.js_slider_single_nodots').each(function(){
	if($(this).find('> div').length > 1){

		$(this).slick({
				dots: false,
				cssEase: 'ease',
				speed: 600
			});
		}
	});
	$('.js_scroll').mCustomScrollbar();
	$('.b-line_social').share();
	$('.b-changecity').cityChoose();
	$('.b-libfilter').libFilter();
	$('.js_digital').digitalForm();
	$('.b-search_save').searchSave();
	/*$('.closepage').closePage(); */
	$( '.tabs').tabs();
	$('.js_editcollection').editCollection();
	$('.b-popularslider').popularSlider();
	$('.js_description').descrPopup();
	$('.tbcalendar').datepicker();
	$('.js_multipleslider').mulislider();
	$('.searchnum_set').searchNum();
	$('.echbcell').ticketCheck();
	$('.b-form').initForm();
	$('.seldate').seldate(); // всегда до кастомизации select

	$('.js_register').registerPopup();
	$('.b-fieldeditable').fieldEditable();

	$( ".js_sortable" ).sortable();
	$( ".js_sortable" ).disableSelection();

	$('.js_flex_bgimage').flexbgimage();
	$('input.radio').replaceRadio();
	$('input.checkbox:not(".custom")').replaceCheckBox();


	$('.js_select').selectReplace();
	$('.b-sidenav').sideAccord(); // аккордеон менюшки сайдбара
	$('.knob').knobFunk();
	$('.js_fourslider').fourSlider();
	// $('.b-newslider').newsSlider();
	$('.js_threeslide').threeslide();


	$('.b-gostlist li').copyFunction();

	$('.searchform').searchforminit(); // инициализация формы поиска

	//$('.popup_opener').uipopup();
	DOCUMENT.on('click','.popup_opener', function(e){
		e.preventDefault();
		$(this).uipopup();
	});

	$('.tbpasport').inputmask("9999 999999");
	$('.maskphone').inputmask("+7 (999) 999-99-99");
	// отключаем пока, удаляем с перезагрузкой страницы
	//$('.b-searchresult_tag .del').removeTag(); //удаление параметра поиска


	$('.js_filter').filterInit(); // инициализация фильтра

	$('.pass_status').checkPass();

	$('.js_ajax_morebook').ajaxUpdate();
	$('.js_ajax_bookupload').ajax_bookupload();

	$('.b-rolledtext').rolledText();

	$('.set_opener, .sort_opener').openHideBlock();
	$('.b-razdel .js_select').doItOnChange();
	$('.add_books_to_digitizing').add_books_to_digitizing();
	$('.add_books_to_fond').add_books_to_fond();

	DOCUMENT.on( "click", "em.error",function(e) {
		$(this).fadeOut();
	});


	DOCUMENT.on( "click", ".b-bookadd",function(e) {
		e.preventDefault();
		$('.selectionClone').remove();
		var cont = $(this), topPos = cont.offset(), par = cont.closest('.meta'),
		hint = $('.b-hint', par), block = $('.selectionBlock'),
		clone = block.clone(true) ,
		form = $('.b-selectionadd', clone),
		myfavslink = $('.b-openermenu', clone),
		item = cont.closest('.b-result-docitem'),
		metalabel = par.siblings('.metalabel');

		clone.addClass('selectionClone');

		if (cont.hasClass('fav')) {
			$('body').append('<div class="b-removepopup"></div>');
			var htmltxt = $('body').find('.b-removepopup.hidden').html();

			var removepopup = $('body > .b-removepopup:not(".hidden")');
				removepopup.html(htmltxt);
			var bt_remove = $('.btremove', removepopup),
			bt_cancel = $('.gray', removepopup);


			removepopup.css('top', topPos.top + 74);
			removepopup.show();
			if (!par.hasClass('minus') ) { metalabel.html(cont.data('plus')); metalabel.removeClass('minuslb'); }


			bt_remove.click(function(e){
				e.preventDefault();

				if ( !(cont.data('normitem') == 'Y')) {
					item.fadeTo('slow', 0.5, function() { // удаляем книжку
						$.ajax({
							url: cont.data('url'),
							complete: function(data){ // ответ от сервера
								if(cont.data('callback') && cont.data('callback').length > 0) // выполняем callback функцию если она прописана
									eval(cont.data('callback'));
							}
						});

						$(this).remove();
					});
				}
				else
				{
					$.ajax({
						url: cont.data('url'),
						complete: function(data){ // ответ от сервера

							if(cont.parent().parent().hasClass('b-addcollecion')){
								$('.search-result .b-bookadd').removeClass('fav');
							}
							if(cont.data('callback') && cont.data('callback').length > 0){
								eval(cont.data('callback'));
							} // выполняем callback функцию если она прописана
						}
					});
				}

				if ( !(cont.data('remove') == 'removeonly')) { // для старниц, где нет добавления
					cont.removeClass('fav');
					par.removeClass('minus');
					metalabel.html(cont.data('plus'));
					metalabel.removeClass('minuslb');
					hint.html(hint.data('plus'));
				}

				removepopup.remove();
				cont.prev().html('<span>Добавить</span> в Мою библиотеку');

			});
			bt_cancel.click(function(e){
				e.preventDefault();
				removepopup.remove();
				cont.prev().html('<span>Удалить</span> из Моей библиотеки');
			});

		} else {
			cont.addClass('fav');
			par.addClass('minus');

			metalabel.html(cont.data('minus'));
			metalabel.addClass('minuslb');
				DOCUMENT.off("click.meta").on("click.meta", function(e){
						var targ = $(e.target);
						if (!targ.hasClass('selectionClone') && targ.parents('.selectionClone').length < 1) {
							closeNavbar();
							DOCUMENT.off("click.meta").off("keyup.meta");
						}
					});

					DOCUMENT.off("keyup.meta").on('keyup.meta', function(e) {
						if (e.keyCode == 27) {
							closeNavbar();
							DOCUMENT.off("click.meta").off("keyup.meta");
						}   // esc
					});
					DOCUMENT.on('click','.selectionClone .close', function(e){
						closeNavbar();
						return false;
					});

			var nbT; //таймер 3с для исчезновения всплывашки
			function closeNavbar(){
				clone.remove();
				if(cont.closest('.slick-slider').hasClass('narrow_panel')){
						cont.closest('.slick-list').css({'z-index':'0'});
						cont.closest('.slick-slider').css({'overflow': 'hidden'});
						cont.closest('.b-bookboard_main').css({'overflow': 'hidden'});
					}
			}
			/*
			nbT = window.setTimeout(function(){
					closeNavbar();
					},2000);
			/*
			clone.mouseenter(function() {
				window.clearTimeout(nbT);
			})
			.mouseleave(function() {
				nbT = window.setTimeout(function(){
				closeNavbar();
			},2000);

			});
			*/

			hint.html(hint.data('minus'));
			if($(cont).hasClass('b-coll-list-kind')){
				par = cont.closest('.b-collection-item');
				clone.addClass('b-list-in-coll');
			}
			par.append(clone);
			clone.show();


			myfavslink.click(function(e){
				e.preventDefault();
				clone.remove();
			});

			form.formAddSelection();
			$.ajax({
				url: cont.data('collection'),
				method: 'GET',
				success: function(html){

					form.before(html).fadeIn('slow');
					if(cont.parent().parent().hasClass('b-addcollecion')){
						$('.search-result .b-bookadd').addClass('fav');
					}
					if(cont.closest('.slick-slider').hasClass('narrow_panel')){
						cont.closest('.slick-list').css({'z-index':'1'});
						cont.closest('.slick-slider').css({'overflow': 'visible'});
						cont.closest('.b-bookboard_main').css({'overflow': 'visible'});
					}
					$('input.checkbox:not(".custom")', clone).replaceCheckBox();



				}
			});
			cont.prev().html('<span>Удалить</span> из Моей библиотеки');
			return false;

		}


	});

	DOCUMENT.on( "click", ".js_openmfavs",function(e) {
		e.preventDefault();
		var link = $(this), menu = link.siblings('.b-favs'), form =$('.b-selectionadd', menu);
		if(!(link.hasClass('open'))) {
			$.ajax({
				url: link.data('favs'),
				method: 'GET',
				success: function(html){
					menu.find('.b-selectionlist').remove(); // EA
					form.before(html).fadeIn('slow');
					$('input.checkbox:not(".custom")', menu).replaceCheckBox();
					form.formAddSelection();
					menu.slideDown(200, 'easeOutQuad', function() { //EA
						link.addClass('open');
					});
				}
			});
			DOCUMENT.off("click.bfavs").on("click.bfavs", function(e){
				var targ = $(e.target);
				if (!targ.hasClass('b-favs') && targ.parents('.b-favs').length < 1) {
					menu.slideUp(200, 'easeOutQuad', function() { //EA
						link.removeClass('open');
					});
					DOCUMENT.off("click.bfavs").off("keyup.bfavs");
				}
			});
			DOCUMENT.off("keyup.bfavs").on('keyup.bfavs', function(e) {
				if (e.keyCode == 27) {
					menu.slideUp(200, 'easeOutQuad', function() { //EA
						link.removeClass('open');
					});
					DOCUMENT.off("click.bfavs").off("keyup.bfavs");
				}   // esc
			});
			//return false;
		} else {
			menu.slideUp(200, 'easeOutQuad', function() { //EA
				link.removeClass('open');
				DOCUMENT.off("click.bfavs").off("keyup.bfavs");
			});
		}
	});


	DOCUMENT.on( "click", ".js_passrecovery",function(e) {
		e.preventDefault();
		var link =$(this);
		var passform = $('.b-passrform');
		passform.addClass('cont_fade_popup');
			if (link.hasClass('open')) {
				passform.fadeOut(200, 'easeOutQuad', function(){
					link.removeClass('open');
					DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
				});

			} else {
				passform.fadeIn(200, 'easeOutQuad', function(){
					link.addClass('open');
					DOCUMENT.off("click.qbqbqb").on("click.qbqbqb", function(e){
						var targ = $(e.target);
						if (!targ.hasClass('cont_fade_popup') && targ.parents('.cont_fade_popup').length < 1) {
							passform.fadeOut(200, 'easeOutQuad', function(){
								link.removeClass('open');
							});
							DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
						}
					});
					 DOCUMENT.off("keyup.qbqbqb").on('keyup.qbqbqb', function(e) {
						if (e.keyCode == 27) {
							passform.fadeOut(200, 'easeOutQuad', function() { //EA
								link.removeClass('open');
							});
							DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
						}   // esc
					 });

					DOCUMENT.on("click",'.closepopup', function(e) {
							passform.fadeOut(200, 'easeOutQuad', function() { //EA
								link.removeClass('open');
							});
							DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
					 });
				});
			}
	});

	DOCUMENT.on( "click", ".b-headernav_login_lnk",function() {
		$('.b-headernav_login_lnk').removeClass('current');
		$(this).addClass('current');
	});


	//$('.js_ajax_loadresult').click(function(e){  // ajax заглушка на кнопку обновить
	DOCUMENT.on( "click", ".js_ajax_loadresult", function(e){  // ajax заглушка на кнопку обновить
		e.preventDefault();
		var a = $(this), resultdoc = $('.b-result-doc');
		a.css('background-image', 'url("/local/templates/.default/markup/i/ajax_loader1.gif")');
		$.ajax({
			url: a.attr('href'),
			method: 'GET',
			success: function(html){
				var ajaxinner = $($(html).html());
				a.closest('div.rel').before(html).fadeIn('slow');
				$('.knob').knobFunk();
				$('.iblock').cleanWS();
				a.css('background-image', 'url("/local/templates/.default/markup/i/ico_load.png")');
			}
		});
		return false;
	});

	$('.js_ajax_loaddata').click(function(e){  // ajax заглушка на кнопку обновить
		e.preventDefault();
		var a = $(this), resultdoc = a.closest('.b-sidenav_cont_list');
		$.ajax({
			url: a.attr('href'),
			method: 'GET',
			success: function(html){
				$(html).insertBefore(a.parent()).fadeIn('slow');
				$('.iblock').cleanWS();
				$('input.checkbox:not(".custom")').replaceCheckBox();
			}
		});
		return false;
	});

	$('.js_ajax_theme').on('click', function(e){  // ajax заглушка на кнопку следующие темы
		e.preventDefault();
		var a = $(this), BLOCK =$('.b-sidenav_cont_list');
		$.ajax({
			url: a.attr('href'),
			method: 'GET',
			success: function(html){
				$(html).insertBefore(a.parent()).fadeIn('slow');
				/* a.before(html).fadeIn('slow');*/
				$('input.checkbox:not(".custom")').replaceCheckBox();
			}

		});
		return false;
	});



	DOCUMENT.on( "click", ".js_profilemenu, .js_openmenu",function(e) {
		e.preventDefault();
		var link = $(this), menu = link.siblings('ul');
		menu.addClass('slidedown_list_container');
			if (link.hasClass('open')) {
				menu.slideUp(200, 'easeOutQuad', function(){
					link.removeClass('open');
					DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
				});
				//quoteaction.removeClass('visible');

			} else {
				menu.slideDown(200, 'easeOutQuad', function(){
					link.addClass('open');
					DOCUMENT.off("click.qbqbqb").on("click.qbqbqb", function(e){
						var targ = $(e.target);
						if (!targ.hasClass('slidedown_list_container') && targ.parents('.slidedown_list_container').length < 1) {
							menu.slideUp(200, 'easeOutQuad', function(){
								link.removeClass('open');
							});
							DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
						}
					});
					 DOCUMENT.off("keyup.qbqbqb").on('keyup.qbqbqb', function(e) {
						if (e.keyCode == 27) {
							menu.slideUp(200, 'easeOutQuad', function() { //EA
								link.removeClass('open');
							});
							DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
						}   // esc
					 });

				});
				//quoteaction.addClass('visible');
			}
	});





	$('.js_moreopen').opened();
	$( 'input[autocomplete="off"]' ).autocompliteFunc();
	$('.loadingimg').bindImageLoad();

	$('.mappage').mappage();
	$('.fmap').mapones();

	DOCUMENT.on('click','.canceldeletebook', function(e){
		e.preventDefault();
		var popup = $(this).closest('.uipopupcontainer ');
		popup.dialog('close');
	}).on('click','.deletebook', function(e){
		e.preventDefault();
		var link = $(this), popup = link.closest('.uipopupcontainer ');
		$.ajax({
			url: link.attr('href'),
			cache: false
		}).done(function(html) {
			popup.dialog('close');
			//тут она как то удаляется и обновляется список книг
		});
	});
	$('.book_slider').bookslider();




	$('.b-editlibform').addrowForm();
	$('.b-slidernews').slick({
		dots: false,
		infinite: true,
		cssEase: 'ease',
		slide: '.slide',
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 600
	});


/* не понятно где используется
	$('.b-mainblock .b-newslider').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		cssEase: 'ease',
		speed: 600
	});*/


	// uipopup
	DOCUMENT.on('click','.btrefuse', function(e){
	  e.preventDefault();
	  var popup = $(this).closest('.uipopupcontainer');
	  popup.remove();
	  $('.homepage').height('auto');
	  //$('.closepopup').remove();
	  //popup.dialog('close');
	 });




}); // DOM loaded
$(window).load(function(){
	if ($(window).width() > 993 ) {
		$('.js_bookinfo_height').bookinfo_height(); // выравнивает высоту столбцов
	}
	$('.loadimg').remove();

});




(function() { //create closure
	$.fn.fourSlider = function() {
		this.each(function() {

			var cont = $(this);
			cont.slick({
				dots: false,
				cssEase: 'ease',

				slidesToShow: 4,
				slidesToScroll: 4,
				speed: 600
			});

		});
	}
	//end of closure
})(jQuery);

(function() { //create closure
	$.fn.newsSlider = function() {
		this.each(function() {

			var cont = $(this);

			if (cont.parents().hasClass('b-side')) {
				cont.slick({
					dots: false,
					cssEase: 'ease',
					slidesToShow: 1,
					slidesToScroll: 1,
					speed: 600
				});

			} else {
				cont.slick({
					dots: false,
					cssEase: 'ease',
					slidesToShow: 4,
					slidesToScroll: 4,
					speed: 600
				});
			}


		});


	}
	//end of closure
})(jQuery);

(function() { //create closure
	$.fn.threeslide = function() {
		this.each(function() {
			var cont = $(this);
			cont.slick({
				dots: false,
				cssEase: 'ease',
				slide: '.slide',
				slidesToShow: 3,
				slidesToScroll: 1,
				speed: 600
			});
		});
	}
	//end of closure
})(jQuery);


(function() { //create closure
	$.fn.bookslider = function() {
		this.each(function() {
			var cont = $(this);
			cont.slick({
				dots: false,
				cssEase: 'ease',
				slide: '.slide',
				slidesToShow: 5,
				slidesToScroll: 1,
				speed: 600
			});
		});
	}
	//end of closure
})(jQuery);



(function(cash) { //create closure
	$.fn.dateslider = function(options) {
		this.each(function() {
			var cont = $(this),
			tbdate_prev = $( "#js_searchdate_prev"),
			tbdate_next = $( "#js_searchdate_next")
			/*date_prev = $("<span class='slider_span'></span>"),
			date_next = $("<span class='slider_span'></span>")*/;
            var curYear = new Date().getFullYear();
            var beginYear = 900;
            cont.slider({				range: true,
                min: beginYear,
                max: curYear,
                values: [ beginYear, curYear ],
				create: function( event, ui ) {

					cont.slider( "values", 0, tbdate_prev.val());
					cont.slider( "values", 1, tbdate_next.val());

					/*$('a:eq(0)', cont).append(date_prev);
					$('a:eq(1)', cont).append(date_next);


					date_prev.text(tbdate_prev.val());
					date_next.text(tbdate_next.val());*/
				},
				slide: function( event, ui ) {
					tbdate_prev.val( ui.values[ 0 ]);
					tbdate_next.val( ui.values[ 1 ] );
					/*date_prev.text(ui.values[ 0 ]);
					date_next.text(ui.values[ 1 ]);*/
				}
			});
			tbdate_prev.keyup(function(e){
						if(tbdate_prev.val().length == 4){
							cont.slider( "values", 0, tbdate_prev.val());
						}
					});
			tbdate_next.keyup(function(e){
						if(tbdate_next.val().length == 4){
							cont.slider( "values", 1, tbdate_next.val());
						}
					});


		});
	}
	//end of closure
})(jQuery);

(function(cash) { //create closure
	$.fn.copyFunction = function(options) {
		this.each(function() {
			var cont = $(this), par = cont.parents('.b-gostlist'),
			par_ofset = par.offset(), item = cont.closest('.b-quote_item');


			var copylink = new ZeroClipboard(cont, {
			  moviePath: "ZeroClipboard.swf"
			});

			copylink.on("load", function(copylink) {

				  copylink.on("complete", function(copylink, args) {
					// выводим результат
				  //  console.log('args.text');

				  });

			});
			copylink.on( "ready", function( readyEvent ) {

			  copylink.on( "aftercopy", function( event ) {
				// `this` === `client`
				// `event.target` === the element that was clicked
			   // event.target.style.display = "none";
				var copyinfo = $('<div class="b-copyinfo">'+par.attr('data-ok-text')+'</div>').appendTo(item);
				copyinfo.fadeIn(400);

				var nbT;
				function closePopup(){
					copyinfo.fadeOut();
					copyinfo.remove();
				}

				nbT = window.setTimeout(function(){

					closePopup();
					window.clearTimeout(nbT);

				},2000);


			  } );
			} );


		});
	}
	//end of closure
})(jQuery);


(function() { //create closure
	$.fn.quoteInit = function() {
		this.each(function() {
			var cont = $(this),
			img = $('.b-quoteimg', cont), text = $('p', cont), ta = $('.input', cont),
			button = $('.b-quoteformat', cont), par = cont.parents('.b-quote_list');

			$('.b-quoteformat', cont).click(function(e){
				e.preventDefault();
				var lnk = $(this);

				if(lnk.hasClass('btpdf')){
					text.addClass('hidden');
					ta.addClass('hidden');
					img.removeClass('hidden');

					$(this).text(lnk.data('text'));
					$(this).removeClass('btpdf');
					$(this).addClass('bttxt');
					cont.removeClass('textshow');
				} else
					if(lnk.hasClass('bttxt')){

						text.addClass('hidden');
						img.addClass('hidden');

						ta.removeClass('hidden');
						ta.focus();

						$(this).text(lnk.data('pdf'));
						$(this).removeClass('bttxt');
						$(this).addClass('btpdf');
						cont.removeClass('textshow');
					}

				var oldtxt = '';
				ta.on('focus', function(){
					oldtxt = ta.val();
				});
				ta.on('blur', function(){
					text.text(ta.val());
					$(this).addClass('hidden');
					text.removeClass('hidden');
					cont.addClass('textshow');
					img.addClass('hidden');
					if (oldtxt != ta.val()) {
						$.ajax({
							url: par.data('action'),
							type: 'post',
							data: { quotetext: ta.val(), id: ta.attr('id') },
                            dataType: 'json'
						}).done(function(data) {
							if (data.status == 'success') {
								//console.log(data);
								location.reload();
							} else {
								//console.log(data);
							}
							return false;
						});
					}
					oldtxt = '';
				});

				/*ta.focusout(function(){
				var ta_val = ta.val();
				text.text(ta_val);

				$.ajax({
				url: par.data('action'),
				type: 'post',
				data: { name: "John", location: "Boston" },
            	dataType: 'json'
				}).done(function(data) {
				if (data.status == 'success') {
				alert(data);
				location.reload();

				} else {
				alert(data);
				}
				return false;
				});
				$(this).addClass('hidden');
				text.removeClass('hidden');
				cont.addClass('textshow');
				img.addClass('hidden');
				}); */
			});

		});
	}
	//end of closure
})(jQuery);





(function(cash) { //create closure
	$.fn.removeTag = function(options) {
		this.each(function() {
			var del = $(this);

			del.click(function(e){
				e.preventDefault();
				var tag = $(this).parents('.b-searchresult_tag');
				tag.fadeTo('slow', 0.5, function() {
					/* раскомментировать
					$.ajax({
					url: a.attr('href'),
					complete: function(data){ // ответ от сервера
					*/

					// имитация положительного ответа сервера

					/* раскомментировать
					}
					});
					*/
					$(this).remove();
				});

			});

		});
	}
	//end of closure
})(jQuery);


(function($) { //create closure
	$.fn.libFilter = function(options) {
		this.each(function() {
			var cont = $(this), btremove = $('.b-libfilter_remove', cont),
			checks = $('input[type="checkbox"]', cont), removepopup = $('.b-removepopup', cont), form = $('.b-selectionadd', cont),
            deleteButtons = cont.find('.b-libfilter_item .neb-icon-delete'),
            editButtons = cont.find('.b-libfilter_item .neb-icon-pen'),
            editForm = cont.find('.edit-form');

			checks.change(function() {
				var stylecheck = $(this).parents('.checkbox');
				if($(this).attr('checked')){
					stylecheck.addClass('show');
				}else{
					stylecheck.removeClass('show');
				}
				if(checks.parents('.checkbox').hasClass('show')){  //если есть что удалять
					btremove.show();
				} else { //если нечего
					btremove.hide();
				}
			});

			btremove.click(function(e){
				e.preventDefault();
				removepopup.fadeIn();

			});
			$('.formbutton', removepopup).click(function(e){
				e.preventDefault();
				removepopup.fadeOut();	 // тут добавить удаление
			});

            deleteButtons.click(function () {
                $(this).parent().find('input.checkbox').attr('checked', true)
                removepopup.fadeIn();
            });

            editButtons.click(function() {
                var that = $(this),
                    offset = that.offset(),
                    form = editForm.find('form'),
                    name = form.find('input[name="name"]'),
                    nameString = that.parent().find('.b-libfilter_name').text();
                name.val(nameString);
                name.removeClass('hidden');  name.off(); // Хрен пойми откуда после сабмита или блюра добавляется класс hidden

                name.blur(function() {
                    editForm.hide();
                });
                editForm.show();
                offset.left += 15;
                editForm.offset(offset);
                form.find('input.idValue').val(that.parent().find('input.checkbox').val());
                name.focus();
            });

			form.formAddSelection();

		});
	}
	//end of closure
})(jQuery);


(function(cash) { //create closure
	$.fn.knobFunk = function(options) {
		this.each(function() {
			var cont = $(this);
			cont.knob({
				'draw' : function (v) {
					var inp = this.i, val = inp.val(), label = inp.parents('.b-loadprogress').find('.b-loadlabel');
					label.text(val + '%');
				}
			});

		});
	}
	//end of closure
})(jQuery);






(function(cash) { //create closure аккордеон справа
	$.fn.sideAccord = function(options) {
		this.each(function() {
			var cont = $(this), title = $('.b-sidenav_title', cont), searchEx = $('.b-sidenav_srch', cont);
			title.click(function(e){
				e.preventDefault();
				var bt = $(this), accblock = bt.next('.b-sidenav_cont');
				bt.toggleClass('open');
				accblock.slideToggle();
			});
			searchEx.click(function(e){
				e.preventDefault();
				var par = $(this).closest('.b-sidenav_cont'), ul = $('.b-sidenav_cont_list', par);
				ul.slideToggle();
			});

		});
	}
	//end of closure
})(jQuery);

(function(cash) { //create closure аккордеон справа
	$.fn.libfilterCheck = function(options) {
		this.each(function() {
			var cont = $(this);
			cont.addClass('checkbox');
			cont.replaceCheckBox();
		});
	}
	//end of closure
})(jQuery);



(function(cash) { //create closure
	$.fn.bookinfo_height = function(options){
		this.each(function(){

			var H = 0, el = $(this).children();
			el.height('').removeClass('evenlyready');
			el.each(function(){
				H = Math.max(H, $(this).height());

			});
			el.height(H).addClass('evenlyready');
		});

	}
	//end of closure
})(jQuery);


(function(cash) { //create closure
	$.fn.searchforminit = function(options){

		$('.category-opener').click(function(event) {
			event.preventDefault();
			$('.exsearch-category-list').slideUp(100);
			var categoryList = $(this).next();
			if(!$(categoryList).is(':visible')){
				$(categoryList).slideDown(200);
			}
			else {
				$(categoryList).slideUp(200);
			}
		});


		this.each(function(){
			var form = $(this), btn = $('.js_addsearchrow'),  search_row = $('.js_search_row'),
			extraform_lnk = $('.js_extraform', form), extraform = $('.b-search_ex', form);
			var iselect = $('.js_search_row select', form);

			$('.js_searchslider', form).dateslider(); // инициализация слайдера
			extraform_lnk.on('click', function(e){
				e.preventDefault();
				if (!($(this).hasClass('open'))){
					$('.b-search.wrapper').addClass('no-border');
					extraform_lnk.addClass('open');
				}
				else{
					$('.b-search.wrapper').removeClass('no-border');
					extraform_lnk.removeClass('open');
				}
				extraform.slideToggle(300);
			});


			DOCUMENT.on('submit',form,function(e){
				var vr =  extraform.find('.visiblerow'), inp = vr.find('input:text');
				inp.each(function(){
					if ($(this).val() !== '') { extraform.addClass('open'); }

				});
			});

			form.find('.b-searchaddrow:not(".b-searchdelrow")').on('click', function(event){
				event.preventDefault();
				var searcInput = $(this).parent().children('.b_search_txt');

				if($(searcInput).val()){
					searcInput.removeClass('error');
					var clone_row = search_row.clone(true);
					search_row.before(clone_row);


	                $(this).hide().next().show();
	                clone_row.removeClass('js_search_row hidden');
					clone_row.addClass('clearfix visiblerow');
					var n = $('.visiblerow', form).length - 1,
					logic= $('.ddl_logic', clone_row), logic_name = logic.attr("name") + '['+ n+ ']',
					theme = $('.theme', clone_row), theme_name = theme.attr("name") + '['+ n+ ']',
					text = $('.b_search_txt', clone_row), text_name = text.attr("name") + '['+ n+ ']',
					test = $('.inpseltxt', clone_row),
					access = $('.b-access', clone_row), access_name = access.attr("name") + '[' + n + ']';

					test.addClass('class_name');
					logic.attr("name", logic_name);
					theme.attr("name", theme_name);
					text.attr("name", text_name);
					access.attr("name", access_name);

					clone_row.children().removeAttr('disabled');
					var select = $('select', clone_row);
					select.removeAttr('disabled');

					select.selectReplace();

				}
				else {
					searcInput.addClass('error');
				}

			});

			form.find('.b-searchdelrow').on('click', function(e){
				e.preventDefault();
				$(this).parent().remove();

			});

			$('select.ddl_theme', form).bind('change', function(e){
				e.preventDefault();
				var sel =$(this);
				var way = sel.val();
				if(way == 'foraccess'){
					sel.closest('.b_search_row').find('input.b_search_txt').addClass('hidden');
					sel.closest('.b_search_row').find('.inpselblock.hidden select').removeClass('hidden');
					sel.closest('.b_search_row').find('.inpselblock.hidden').removeClass('hidden').find('.inpseltxt').width('270px');
				}else {
					sel.closest('.b_search_row').find('.b-access').parent().addClass('hidden');
					sel.closest('.b_search_row').find('input.b_search_txt').removeClass('hidden');
				}
				if(!sel.find('option:selected').data('asrc')){
					sel.closest('.b_search_row').find('input.b_search_txt').removeClass('ui-autocomplete-input');
					sel.closest('.b_search_row').find('input.b_search_txt').prev('.ui-helper-hidden-accessible').remove();
					sel.closest('.b_search_row').find('input.b_search_txt').removeAttr('autocomplete');
					sel.closest('.b_search_row').find('input.b_search_txt').removeAttr('data-src');
					var clone = sel.closest('.b_search_row').find('input.b_search_txt');
					sel.closest('.b_search_row').find('input.b_search_txt').remove();
					clone.insertBefore(sel.closest('.b_search_row').find('.b-searchaddrow').first());
					sel.closest('.b_search_row').find('input.b_search_txt').autocompliteFunc();
					$('.b_search_txt').autocompleteHover();
					$('.b_search_txt').enableEnterSearch();

				}else{
					sel.closest('.b_search_row').find('input.b_search_txt').removeClass('ui-autocomplete-input');
					sel.closest('.b_search_row').find('input.b_search_txt').prev('.ui-helper-hidden-accessible').remove();
					var clone = sel.closest('.b_search_row').find('input.b_search_txt');
					sel.closest('.b_search_row').find('input.b_search_txt').remove();
					clone.insertBefore(sel.closest('.b_search_row').find('.b-searchaddrow').first());
					sel.closest('.b_search_row').find('input.b_search_txt').attr('autocomplete','off');
					sel.closest('.b_search_row').find('input.b_search_txt').attr('data-src',sel.find('option:selected').data('asrc'));
					sel.closest('.b_search_row').find('input.b_search_txt').autocompliteFunc();
					$('.b_search_txt').autocompleteHover();
					$('.b_search_txt').enableEnterSearch();
				}
				return false;
			});
			var btnsub = $('.js_btnsub', form);
				/*btnsub.bind('change', function(e){
					form.trigger('submit');
				});*/
			var clean = $('.js_cleaninp', form);
			var dt_prev = $('.js_searchslider', form).slider( "option", "min" );
			var dt_next = $('.js_searchslider', form).slider( "option", "max" );
			$('.js_cleaninp').on('click', function(e){
				dt_prev = $('#js_searchdate_prev').val();
				dt_next = $('#js_searchdate_next').val();
				form.find('input.b-text').val('').focus();
				/*
				form.find('input[type="checkbox"]').removeAttr('checked');
				form.find('input[type="checkbox"]').closest('.field').removeClass('checked');
				form.find('select.ddl_theme').removeAttr('selected');
				form.find('select.ddl_theme').parent().find('.inpseltxt').text(form.find('select.ddl_theme option:eq(0)').text());
				$('.optcontainer').find('.selected').removeClass('selected');
				$('.optcontainer').find('a:first-child').addClass('selected');
				form.find('select.b-access').removeAttr('selected').parent().addClass('hidden');
				form.find('select.b-access').closest('.b_search_row').find('input.b_search_txt').removeClass('hidden');
				*/
				//form.find('.js_searchslider').slider( "values", [ dt_prev, dt_next ] );
				form.find('#js_searchdate_prev').val(dt_prev);
				form.find('#js_searchdate_next').val(dt_next);
				$(this).hide();

				return false;
			});
		});

	}
	//end of closure
})(jQuery);


// flexible background from image
(function(cash) { //create closure
	$.fn.flexbgimage = function(options) {
		this.each(function() {
			var img = $(this);
			if(img.get(0).tagName != 'IMG') return false;
			var par = img.parent(), fleximg = new Image();
			// картинки превратить в фон
			par.css('background-image','url("'+img.attr('src')+'")');
			if (img.data('bgposition')) {
				par.css('background-position',img.data('bgposition'));
			} else {
				par.css('background-position','50% 50%');
			}
			if (backgroundsize) par.css('background-size', 'cover');
			par.data('src', img.attr('src'));
			fleximg.src = par.data('src');
			// убить оригинальный img
			img.css('opacity', 0);
			function flexbg(e){
				if (backgroundsize) return false;
				var w = par.width(), h = par.height();
				var bK = w/h, iK = par.data('imgW')/par.data('imgH');
				var bsize = (iK>bK)? 'auto 100%' : '100% auto';
				par.css('background-size', bsize);
			}
			par.on('reFlexBgImage', flexbg);
			// отловить загрузку картинки
			$(fleximg).bind('load',function(){
				if (!img.data('loaded')) {
					img.data('w',img.width()).data('h',img.height()).data('loaded',true);
					par.data('imgW',img.data('w')).data('imgH',img.data('h'));
					par.trigger('checkload');
					par.addClass('js_flex_bgimage').trigger('reFlexBgImage');
					img.remove();
				}
			});
			try {
				if (fleximg.complete && !img.data('loaded')) {
					//      $(fleximg).trigger('load');
				}
			} catch (e) {
			}
		});
	}
	//end of closure
})(jQuery);

(function(cash) { //create closure ф-и для фильтра
	$.fn.filterInit = function(options) {
		this.each(function() {
			var cont = $(this), lnk_list = $('.b-filter_list', cont), lnk_items = $('.b-filter_items', cont),
			list = $('.b-filter_list_wrapper'), items =$('.b-filter_items_wrapper'),
			pages = $('.b-filter_num'), paging = $('.b-paging'), ajaxresult = $('.b-result-docmore');
			lnk_list.click(function(e){
				e.preventDefault();
				lnk_items.removeClass('current');
				$(this).addClass('current');
				items.hide(100);
				list.fadeIn(400);
			});
			pages.click(function(e){
				e.preventDefault();
				var lnk = $(this);
				pages.removeClass('current');
				lnk.addClass('current');
				if (lnk.hasClass('b-filter_num_paging')) {
					paging.removeClass('hidden');
					ajaxresult.addClass('hidden');
				} else {
					paging.addClass('hidden'); ajaxresult.removeClass('hidden');
				}

			});

			lnk_items.click(function(e){
				e.preventDefault();
				lnk_list.removeClass('current');
				$(this).addClass('current');
				list.hide(100);
				items.fadeIn(400);
			});


		});
	}
	//end of closure
})(jQuery);




(function($) { //create closure
	$.fn.initForm = function(options){
		this.each(function(){
			var form = $(this),  agreebox = $('.agreebox', form), bsubmit = form.find('button'), shwn = $('.showon', form);


			var addr = $('.addrindently', form);
			addr.change(function(){
				if($(this).attr('checked')){
					form.find('.inly input').attr('disabled','disabled').data('required','');

				}else{
					form.find('.inly input').removeAttr('disabled').data('required','required');

				}

			});


			form.find('.seldate').each(function(){
				var block = $(this);
				block.find('select').on('change',function(){
					testSelDate(block);
				});
			});

			form.submit(function(e) {
				var form = $(this);



				form.formValidator();

				form.find('.seldate').each(function(){
					var block = $(this), realseldate = block.find('.realseldate');
					if (testSelDate(block)=='error') {
						form.data('valid',false);
					} else {
						realseldate.removeClass('error');
					}
				});

				if (form.data('valid')=== true) {
					// $.ajax({
					//     url: form.attr('action'),
					//     type: 'post',
					//     data: form.serialize(),
					//     cache: false
					// }).done(function(data) {
					//     data = $.parseJSON(data);
					//     if (data.status == 'success') {
					//         alert(data.msg);
					//         location.reload();
					//     } else {
					//         alert(data.msg);
					//     }
					//     return false;
					// });
					if(form.hasClass('b-profile_passform')){
						e.preventDefault();
					var c = bsubmit.parent().find('.messege_ok').clone(true);
							c.removeClass('hidden');
									$.ajax({
										 url: form.attr('action'),
										 type: 'post',
										 data: form.serialize(),
										 cache: false
									 }).done(function(text) {
											if (text == '1') {
												form.addClass('pass_changed');
												form.find('input').val('').removeClass('ok');
												form.find('.validate').removeClass('ok');
												 c.dialog({
													closeOnEscape: true,
													modal: true,
													draggable: false,
													resizable: false,
													width: 480,
													dialogClass: 'ajaxpopup',
													position: "center",
													open: function(){
														$('.ui-button').addClass('formbutton');
														$('.ui-widget-overlay').addClass('black');
														c.find('.closepopup').click(function() {
																	//Close the dialog
																	c.dialog( "destroy" ).remove();
																	return false;
														});
													},
													buttons: {
														Ok: function() {
															$( this ).dialog( "destroy" ).remove();
														}
													},
													close: function() {
														popup.dialog("close").remove();
													}
												});

												$('.ui-widget-overlay').click(function() {
													//Close the dialog
													c.dialog( "destroy" ).remove();
												});
										 return false;
									} else {
										if(!form.hasClass('pass_changed')) {
											$('<em class="error" style="display:block;">'+text+'</em>').insertBefore(form.find('.fieldrow:last-child button'));
											 return false;
										} else {
											$('<em class="error" style="display:block;">Вы уже отправили запрос на смену пароля</em>').insertBefore(form.find('.fieldrow:last-child button'));
											 return false;
										}

									}
								});
					}else{
						return true;
					}

				} else {

					var firstError = form.find('.field.error:eq(0)');
					var errormess = firstError.find('em.error:visible');
					var set = {
						'font-size': errormess.css('font-size'),
						'color': errormess.css('color'),
						'background-color': errormess.css('background-color'),
						'margin-left':  parseInt(errormess.css('margin-left')),
						'opacity': 1
					};
					errormess = form.find('em.error:visible');
					errormess.css({/*'font-size': '40px',*/ 'color': '#fff', /*'background-color': '#f00',*/ 'margin-left': -350, 'opacity': 0});
					$.scrollTo(firstError, {
						'offset':-50,
						'duration':500,
						'easing': 'easeOutCubic',
						'onAfter': function(){
							window.setTimeout(function(){
								errormess.animate(set, 750, 'easeOutCubic', function(){
									errormess.css({'font-size': '', 'color': '', 'background-color': '', 'margin-left': '', 'opacity': ''});
								});
							}, 200);
						}
					});



					firstError.trigger('focus');

					return false;
				}

				//	return false;
			});

			agreebox.on('change',function(){

				if(agreebox.attr ("checked")) {
					bsubmit.prop("disabled", false);

					bsubmit.removeClass('btdisable');
				} else {
					bsubmit.prop("disabled", true);

					bsubmit.addClass('btdisable');
				}


			});
				shwn.on('change',function(){

				if(shwn.attr ("checked")) {
					shwn.closest('.checkwrapper').next().find('input').prop("disabled", false);
					shwn.closest('.checkwrapper').next().find('.field').removeClass('disbl');
					shwn.closest('.checkwrapper').next().find('label').removeClass('disbl');
				} else {
				   shwn.closest('.checkwrapper').next().find('input').prop("disabled", true);
					shwn.closest('.checkwrapper').next().find('.field').addClass('disbl');
					shwn.closest('.checkwrapper').next().find('label').addClass('disbl');
				}


			});
			var scan = form.find('.setscan input[type="file"]');
			scan.on('change',function(){

				if($(this).val() !='') {
					bsubmit.prop("disabled", false);

					bsubmit.removeClass('btdisable');
				} else {
					bsubmit.prop("disabled", true);

					bsubmit.addClass('btdisable');
				}
			});


			var fullreg = form.find('.radiolist #rb1');

			fullreg.on('change bind',function(){

				if(fullreg.attr("checked")) {
					fullreg.closest('.radiolist').find('.sub_radio input').prop("disabled", false);
					fullreg.closest('.radiolist').find('.sub_radio .disable').prev().find('input').prop("disabled", true);
					fullreg.closest('.radiolist').find('.sub_radio .b-radio:first-child input').attr('checked','checked');
					fullreg.closest('.radiolist').find('.sub_radio .b-radio:first-child .radio').addClass('checked');
				} else {
					fullreg.closest('.radiolist').find('.sub_radio input').removeAttr('checked');
					fullreg.closest('.radiolist').find('.sub_radio .radio').removeClass('checked');
					fullreg.closest('.radiolist').find('.sub_radio input').prop("disabled", true);
				}


			});




		});
	}
	//end of closure
})(jQuery);


(function($) { //create closure сохранить поисковой запрос
	$.fn.searchSave = function(options) {
		this.each(function() {
			var cont = $(this), form = $('.searchsave_form', cont);

			$("body").on('submit', '.searchsave_form', function(e) {
				e.preventDefault();

				var popup = $('.b-searchsave_popup');

				$.ajax({
					type: "POST",
					url: form.attr('action'),
					data: form.serialize(),
					dataType: "json"
				})
				.success(function(json) {
					// имитация положительного ответа сервера
					if(json.ID <= 0)
						return false;

					//var tbox = form.find('.input');
					var tbox = $('.b-searchsave_popup .input');
					popup.fadeIn();
					DOCUMENT.off("click.b-search_save").on("click.b-search_save", function(e){
						var targ = $(e.target);
						if (!targ.hasClass('b-searchsave_popup') && targ.parents('.b-searchsave_popup').length < 1) {
							closePopup();
							DOCUMENT.off("click.b-search_save").off("keyup.b-search_save");
						}
					});

					DOCUMENT.off("keyup.b-search_save").on('keyup.b-search_save', function(e) {
						if (e.keyCode == 27) {
							closePopup();
							DOCUMENT.off("click.b-search_save").off("keyup.b-search_save");
						}   // esc
					});
					popup.find('.close').on('click',function(e){
						closePopup();
						return false;
					});
					var nbT;
					function closePopup(){
						popup.fadeOut();
					}

					nbT = window.setTimeout(function(){
					closePopup();
					},2000);

					 popup.mouseenter(function() {

						window.clearTimeout(nbT);
					  })
					  .mouseleave(function() {
						nbT = window.setTimeout(function(){
							closePopup();
						},2000);

					  });




					window.clearTimeout(nbT);

					tbox.keypress(function(e){
						if(e.keyCode==13){
							$.ajax({
								type: "POST",
								url: form.attr('action'),
								data:{saveId: json.ID, name: tbox.val()}
							})
							.success(function(json) {
								// имитация положительного ответа сервера
								closePopup();
								return false;

							});
						}
					});
				});
			});
		});
	}
	//end of closure
})(jQuery);



(function($){//create closure
	$.fn.ajaxUpdate = function(options){
		var link = $(this), BOOKBLOCK = link.prev('.b-bookboard_list');

		link.click(function(e){
			e.preventDefault();
			BX.showWait('js_ajax_morebook');
			$.ajax({
				url: link.attr('href'),
				method: 'GET',
				success: function(html){
					var ajaxinner = $($(html).html()), lnk = $('.js_ajax_morebook', ajaxinner);
					BOOKBLOCK.remove();
					link.before(html).fadeIn('slow');
					$('.js_ajax_morebook').ajaxUpdate();
					$('.iblock').cleanWS();

					//$('.popup_opener').uipopup(); !!! listener set up over DOCUMENT
					link.remove();
					BX.closeWait();
				}
			});
			return false;
		});

	}//close closure
})(jQuery);



(function($){//create closure
	$.fn.ajax_bookupload = function(options){
		var link = $(this), BOOKPOP = $('.b_bookpopular');

		link.click(function(e){
			e.preventDefault();
			$.ajax({
				url: link.attr('href'),
				method: 'GET',
				success: function(html){
					var ajaxinner = $($(html).html()), lnk = $('.js_ajax_morebook', ajaxinner);
					BOOKPOP.remove();
					link.before(html).fadeIn('slow');
					$('.js_ajax_bookupload').ajax_bookupload();
					$('.iblock').cleanWS();
					//$('.popup_opener').uipopup(); !!! listener set up over DOCUMENT
					link.remove();
				}
			});
			return false;
		});

	}//close closure
})(jQuery);



/* $('.js_ajax_bookupload').click(function(e){ // ajax заглушка на кнопку обновить
e.preventDefault();
var a = $(this), BOOKPOP = $('.b_bookpopular');
$.ajax({
url: a.attr('href'),
method: 'GET',
success: function(html){

BOOKPOP.remove();
a.before(html).fadeIn('slow');
$('.iblock').cleanWS();

}


});

return false;
});
*/




(function() {
	$.fn.opened = function(options) {
		this.each(function() {
			var a = $(this), par =a.closest('ul');
			a.click(function(e){
				e.preventDefault();
                if (5 > par.find('li.hidden:lt(5)').removeClass('hidden').size()){
                    a.parent().addClass('hidden');
                }
			});

		});
	}
	//end of closure
})(jQuery);

(function() {  //чекбокс читальельского билета и его проверка
	$.fn.ticketCheck = function(options) {
		this.each(function() {
			var cont = $(this),
			cbox =cont.find('input.checkbox'),
			label = cont.find('label.disable'), input = cont.find('.input');


			cbox.on('change',function(){

				if(cbox.attr("checked")) {

					input.prop("disabled", false);
					label.removeClass('disable');
				} else {

					input.prop("disabled", true);
					label.addClass('disable');
				}
			});

		});
	}
	//end of closure
})(jQuery);


(function() {
	$.fn.autocompliteFunc = function(options) {
		var word, wordStart, wordEnd, targetWord;
		this.each(function() {
			var sInput = $(this);

            if((typeof(sInput.attr('rel')) == 'undefined') || (sInput.attr('rel') != '0') ){
                showUri = true;
            }else{
                showUri = false;
            }

			$.widget( "custom.catcomplete", $.ui.autocomplete, {
				_create: function() {
					this._super();
					this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
				},
				_renderMenu: function( ul, items ) {
					var that = this,
					currentCategory = "",
					categotyTitle = "";

					$.each( items, function( index, item ) {
						var li;
						if ( item.category != currentCategory ) {
							ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
							currentCategory = item.category;
						}
						li = that._renderItemData( ul, item );

						if (item.category) {
							if(item.category == 'Названия книг'){categotyTitle = 'title'}
							if(item.category == 'Автор'){categotyTitle = 'authorbook'}
							li.attr( "aria-label", item.category + " : " + item.label );
							li.attr( "data-category", categotyTitle);
							li.attr( "data-request", $(li).text());
						}

						$(li).mouseover(function() {
						    $('.ui-autocomplete .ui-menu-item').mouseover(function(){
								$('.ui-autocomplete li').removeClass('list-hover');
								$(this).addClass('list-hover');
							});
							sInput.attr('data-category', $(li).data('category'));
							sInput.attr('data-request', $(li).text());
						});
						$('.ui-autocomplete li').removeClass('list-hover');
						if($('.ui-autocomplete a').hasClass('ui-state-focus')){
							$('.ui-state-focus').parent().addClass('list-hover');
						}

						//Выделение слова при поиске
						targetWord = that.term.toLowerCase().replace(/\s+/g," ").trim();
						word = li[0].children[0].innerHTML;
						if(word.toLowerCase().indexOf(targetWord)==0){
							wordStart = word.slice(0, targetWord.length).bold();
							wordEnd = word.slice(targetWord.length);
							li[0].children[0].innerHTML = wordStart + wordEnd;
						}

                    	$(li).click(function(event){
                        	event.preventDefault();
							if(sInput.attr('id') == 'asearch'){
                        		$('#asearch').val($(this).text());
                        		$('.b-search_ex').hide();
                        		$('#theme-input-0').val($(this).data('category'));
                        		$('#text-input-0').val($(this).data('request'));
							}
                        	$('.b-search_bth').click();
                    	});

                       /*if(showUri){
                            $(li).find('a').attr('href','/search/?q='+item.label);
                            $(li).find('a').attr('target','_parent');
                        }*/

					});
				},
				select: function(e, ui) {},
				response: function(e, ui) {}
			});


			if(sInput.attr('id') === 'asearch'){
				//Перевод из поисковых подсказок сразу в соответствующую категорию
				sInput.attr('data-category', $($('.list-hover')).data('category'));
				sInput.attr('data-request', $('.list-hover').text());
				if($('.list-hover').data('category') !== undefined || $('.list-hover').data('request') !== undefined){
            		$('#theme-input-0').val($('.list-hover').data('category'));
            		$('#text-input-0').val($('.list-hover').data('request'));
				}

				//Отправляем на сервер данные о поисковой форме при выводе автокомплита
				//Подменяется sInput.data('src'), добавляются соответствующие параметры
				var trueSrc = sInput.data('src');
				sInput.keydown(function(){
					var newSrc = '';
					var row = $('.visiblerow');

					newSrc += '&';
					newSrc += $('.b_search_set input:checked').attr('name') + '=' + $('.b_search_set input:checked').val();

					$('select', row).each(function(){
						newSrc += '&';
						newSrc += $(this).attr('name') + '=' + $(this).val();
					});
					$('input:checked', row).each(function(){
						newSrc += '&';
						newSrc += $(this).attr('name') + '=' + $(this).val();
					});
					$('input[type=text]', row).each(function(){
						if($(this).val() !== ''){
							newSrc += '&';
							newSrc += $(this).attr('name') + '=' + $(this).val();
						}
					});

					newSrc += '&';
					newSrc += $('#js_searchdate_prev').attr('name') + '=' + $('#js_searchdate_prev').val();

					newSrc += '&';
					newSrc += $('#js_searchdate_next').attr('name') + '=' + $('#js_searchdate_next').val();

					$('.b_search_row .fln input:checked').each(function(){
						newSrc += '&';
						newSrc += $(this).attr('name') + '=' + $(this).val();
					});

					sInput.data('src', (trueSrc+newSrc).toString());
				});
			}

			if(sInput.data('src')){
				sInput.catcomplete({
					delay: 0,
					minLength: 3,
					source: function(request, response){
						$.ajax({
							url: sInput.data('src'),
							dataType: 'json',
							data: {
								term: sInput.val().toString()
							},
							success: function(data){
								response(data);
							}
						});
					},
					autofocus: true
				});
			}
		});


	}
	//end of closure
})(jQuery);


(function($) { //create closure
	$.fn.seldate = function() {
		var block = $(this), realinp = block.find('.realseldate'), presetdate = realinp.val();
		if (presetdate && presetdate!=''){
			presetdate = presetdate.split('.');
			var dd = block.find('.sel_day'), mm = block.find('.sel_month'), yy = block.find('.sel_year');
			dd.val(presetdate[0]);
			mm.val(presetdate[1]);
			yy.val(presetdate[2]);
		}
	}
})(jQuery);



(function() {  //настройки количества результатов поиска
	$.fn.searchNum = function() {
		this.each(function() {
			var cont = $(this),
			a =cont.find('a'),
			hf = cont.find('input');

			a.click(function(e){
				e.preventDefault();
				a.removeClass('current');
				$(this).addClass('current ');
				hf.val(parseInt($(this).text()));
			});

		});
	}
	//end of closure
})(jQuery);

(function() {  // cлайдер из  5 фото
	$.fn.mulislider = function() {
		this.each(function() {
			var cont = $(this);
			cont.slick({
				infinite: true,
				slidesToShow: 5,
				slidesToScroll: 5,
				cssEase: 'ease',
				speed: 600
			});

		});
	}
	//end of closure
})(jQuery);


(function() {  //страница карточки книги
	$.fn.cardPage = function() {
		this.each(function() {
			var cont = $(this),
			bookimg = cont.find('.b-bookframe_img'),
			bg = $('.b-book_bg');

			bg.css({'background':'url("'+bookimg.attr('src')+'") no-repeat scroll 0 0 rgba(0, 0, 0, 0)','background-size':'100% auto','opacity': 0});
			bg.blurjs({
				source: '.b-book_bg',
				radius:4,
				overlay: 'rgba(255,255,255,0.4)'
			});
			bookimg.on('load', function(){
				bg.animate({'opacity': 1}, 1500, 'easeOutQuad');
			});


		});
	}
	//end of closure
})(jQuery);

(function() {  //страница карточки книги
	$.fn.digitalForm = function() {
		this.each(function() {
			var cont = $(this), lnk = $('.b-digital_desc', cont),  close = $('.close', cont);
			var BODY = $('body')
			lnk.click(function(e){
				e.preventDefault();
				BODY.trigger('heightchangestart');


				var tr = $(this).closest('tr'), descrTR = tr.next('tr.scrolled'),
				digitalpopup = $('.b-infobox[data-link="digital"]', descrTR),
				descrpopup = $('.b-infobox[data-link="descr"]', descrTR);

				if (!$(this).hasClass('current')) {
					$('.b-infobox', cont).hide();
					if ($(this).hasClass('b-digital')) {
						digitalpopup.slideDown(250, 'easeOutQuad', function(){
							BODY.trigger('heightchangeend');
						});
					} else {
						descrpopup.slideDown(250, 'easeOutQuad', function(){
							BODY.trigger('heightchangeend');
						});
					}
				}
				else {
					return false;
				}

				lnk.removeClass('current');
				$(this).addClass('current');
			});
			close.click(function(e){
				e.preventDefault();
				BODY.trigger('heightchangestart');
				var box = $(this).closest('.b-infobox');
				box.slideUp(250, 'easeOutQuad', function(){
							BODY.trigger('heightchangeend');
						});
				lnk.removeClass('current');
			});


		});
	}
	//end of closure
})(jQuery);

(function() {  //страница карточки книги
	$.fn.descrPopup = function() {
		this.each(function() {
			var cont = $(this), lnk = $('.js_descr_link', cont),  close = $('.close', cont);
			lnk.click(function(e){
				e.preventDefault();

				var descrpopup = $('.b-infobox[data-link="descr"]', cont);

				descrpopup.slideToggle();


			});
			close.click(function(e){
				e.preventDefault();
				var box = $(this).closest('.b-infobox');
				box.slideUp();

			});


		});
	}
	//end of closure
})(jQuery);




(function() {  //страница карточки книги
	//	$.fn.addDigital = function() {
	//this.each(function() {
	//	var plus_ico = $(this);

	$( document ).on( "click", ".plusico_wrap .plus_ico",function(e) {
		var lnk = $(this),  par = $(this).closest('.plusico_wrap'), hint = par.find('.b-hint'), html_remove, html_add;

		if (lnk.hasClass('plus_digital') ) {
			e.preventDefault();
			html_remove = hint.data('minus'),
			html_add = hint.data('plus');
			par.toggleClass('minus');
			if ( par.hasClass('minus')) {
				hint.html(html_remove);
			} else {
				hint.html(html_add);
			}
		}

		if (lnk.hasClass('lib') ) {
			html_remove = hint.data('minus'),
			html_add = hint.data('plus');
			/*удаление*/
		}
	});
	//	});
	//}
	//end of closure
})(jQuery);


(function() {  //популярные издания
	$.fn.popularSlider = function() {
		this.each(function() {
			var cont = $(this), items = $('.b-result-docitem', cont), num = 1, list;
			if (cont.data('slides') == 'one') {

			cont.slick({
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				cssEase: 'ease',
				speed: 600
			});


			}
			else {

				items.each(function(i,elem){
				$(this).append( "<span class='num'>" + num + ".</span>" );
				num++;

				if ( i == 0 ) {
					list = $("<div class='b-result-doc b-result-docfavorite'></div>").appendTo(cont);
				}
				$(this).appendTo(list);
				var slidenum = 6;

				if (cont.data('slides') == 'three' ) { slidenum = 3; }
				if (i%slidenum==0 && i > 0) {
					list = $("<div class='b-result-doc b-result-docfavorite'></div>").appendTo(cont);
					$(this).appendTo(list);

				}


			});
			cont.addClass('slidetype');

			var lists = $('.b-result-docfavorite', cont);
			lists.wrap('<div />');

			cont.slick({
				dots: false,
				cssEase: 'ease',
				speed: 600
			});



			}



		});
	}
	//end of closure
})(jQuery);


(function() {  //популярные издания
	$.fn.editCollection = function() {
		this.each(function() {
			var link = $(this), block = $('.js_addbook'),
			h = parseInt($(window).height()) - parseInt($('footer').outerHeight()),
			close = $('.backpage', block),
			hp = $(".homepage");


			link.click(function(e){
				e.preventDefault();
				block.height(h + 70);
				block.fadeIn();

				$("html, body").animate({ scrollTop: 0 }, "fast");
				hp.height(h);
				hp.css('overflow', 'hidden');

			});
			close.click(function(e){
				block.hide();
				hp.height('auto');
				hp.css('overflow', 'auto');
			});
			$( window ).resize(function() {
				block.hide();
				hp.height('auto');
				hp.css('overflow', 'auto');
			});

		});
	}
	//end of closure
})(jQuery);
var map;

/**
 * @link https://code.google.com/a/eclipselabs.org/p/muathuoc/source/browse/trunk/Scripts/quickpager.jquery.js?r=196
 */
(function($) {
    $.fn.quickPagerLibraries = function(options) {

        var defaults = {
            pageSize: 10,
            currentPage: 1,
            showCount: 5,
            holder: null,
            pagerLocation: 'after'
        };

        options = $.extend(defaults, options);
        return this.each(function() {
            var selector = $(this);
            var pageCounter = 1;
            var maxPage = 1;
            options.holder = selector.parents('.b-elar_usrs').find('div.b-paging');
            selector.children().each(function(i){
                if (i < pageCounter * options.pageSize && i >= (pageCounter - 1) * options.pageSize) {
                    $(this).addClass('simplePagerPage' + pageCounter);
                }
                else {
                    $(this).addClass('simplePagerPage' + (pageCounter + 1));
                    pageCounter++;
                }
            });
            maxPage = pageCounter;

            selector.children().hide();
            selector.children('.simplePagerPage' + options.currentPage).show();

            if(pageCounter <= 1) {
                return;
            }
            if(pageCounter > options.showCount) {
                pageCounter = options.showCount;
            }

            var buildPageNavigation = function() {
                options.holder.empty();
                var startPage = 1;
                var pagerSide = Math.floor(pageCounter/2);
                if(pagerSide < options.currentPage) {
                    startPage = options.currentPage - pagerSide;
                }
                var showMaxPage = pageCounter + startPage - 1;
                if(showMaxPage > maxPage) {
                    showMaxPage = maxPage;
                }

                var pageNav = '<div class="b-paging_cnt">';
                pageNav += '<a rel="'
                + (options.currentPage > 1 ? options.currentPage - 1 : options.currentPage)
                + '" href="#" class="b-paging_prev iblock simplePageNav'
                + (options.currentPage > 1 ? options.currentPage - 1 : options.currentPage) + '"></a>';
                for (var i = startPage; i <= showMaxPage; i++) {
                    if (i == options.currentPage) {
                        pageNav += '<a rel="' + i + '" class="b-paging_num current iblock simplePageNav' + i + '" onlcik="return false;">' + i + '</a>';
                    }
                    else {
                        pageNav += '<a rel="' + i + '" class="b-paging_num iblock simplePageNav' + i + '" href="#">' + i + '</a>';
                    }
                }
                pageNav += '<a href="#" rel="' + (options.currentPage + 1) + '" class="b-paging_next iblock simplePageNav' + (options.currentPage + 1) + '"></a>';
                pageNav += '</div>';
                $(options.holder).append(pageNav);
                if(1 >= options.currentPage) {
                    $(options.holder).find('a.b-paging_prev').addClass('current');
                    $(options.holder).find('a.b-paging_prev').click(function (e) {
                        e.preventDefault();
                    });
                }
                if(maxPage <= options.currentPage) {
                    $(options.holder).find('a.b-paging_next').addClass('current');
                    $(options.holder).find('a.b-paging_next').click(function (e) {
                        e.preventDefault();
                    });
                }

                options.holder.find('a').click(function () {
                    if($(this).hasClass('current')) {
                        return false;
                    }
                    var clickedLink = $(this).attr('rel');
                    options.currentPage = parseInt(clickedLink);
                    selector.children().hide();
                    selector.find('.simplePagerPage' + clickedLink).show();
                    buildPageNavigation();
                    return false;
                });
            };
            buildPageNavigation();
        });
    }
})(jQuery);

// maps page
(function ($) { //create closure
    var LibraryMap = function (options) {
        options = options || {};
        this.points = options.points || [];
        if (!options.map) {
            throw new Error('Not set yandex map!');
        }
        var params = ['map', 'cluster', 'startBounds'];
        for(var i = 0; i < params.length; i++) {
            if(!options.hasOwnProperty('cluster')) {
                continue;
            }
            this[params[i]] = options[params[i]];
        }
        this.options = options;
    };
    LibraryMap.prototype = $.extend(LibraryMap.prototype, {
        map: null,
        cluster: null,
        points: {},
        myPosition: null,
        searchPoint: null,
        startBounds: [],
        options: {},
        showNearest: false,
        findInPoints: function (query) {
            var points = [];
            for (var key in this.points) {
                if (!this.points.hasOwnProperty(key) || !this.points[key].hasOwnProperty('libraryPoint')) {
                    continue;
                }
                var point = this.points[key].libraryPoint;
                if (point.name.toLowerCase().indexOf(query.toLowerCase()) != -1) {
                    points.push(this.points[key]);
                }
            }
            return points;
        },
        findNearlyPoint: function (point, pointsList) {
            pointsList = pointsList || this.points;
            var coordSystem = this.map.options.get('projection').getCoordSystem();
            var sourcePoint = point.geometry.getCoordinates();
            if (!pointsList[0] || !pointsList[0].hasOwnProperty('geometry')) {
                return null;
            }
            var minDist = coordSystem.getDistance(sourcePoint, pointsList[0].geometry.getCoordinates());
            if(pointsList[0].hasOwnProperty('libraryPoint')) {
                pointsList[0].libraryPoint.distance = minDist;
            }
            var closestPointIdx = 0;

            var nearlyPoint = pointsList[0];
            for (var i = 1, l = pointsList.length; i < l; ++i) {
                if (!pointsList[i].hasOwnProperty('geometry')) {
                    continue;
                }
                var dist = coordSystem.getDistance(sourcePoint, pointsList[i].geometry.getCoordinates());
                if(pointsList[i].hasOwnProperty('libraryPoint')) {
                    pointsList[i].libraryPoint.distance = dist;
                }
                if (minDist > dist) {
                    minDist = dist;
                    closestPointIdx = i;
                    nearlyPoint = pointsList[i];
                }
            }
            nearlyPoint.distance = minDist;
            return nearlyPoint;
        },
        showNearlyPoint: function (points) {
            points = points || this.points;
            this.findMyPosition(function (myPosition) {
                var nearlyPoint = this.findNearlyPoint(myPosition, points);
                this.showPoint(nearlyPoint, {
                    callback: function () {
                        nearlyPoint.events.once('overlaychange', function () {
                            nearlyPoint.balloon.open();
                        });
                    }
                });

            }.bind(this));
            return this;
        },
        /**
         * @todo add refresh method
         * @param points
         * @param flag
         * @returns {LibraryMap}
         */
        visiblePoints: function (points, flag) {
            points = points || this.points;
            function setVisibleFlag(points, flag) {
                  for(var i = 0; i < points.length; i++) {
                      points[i].visible = flag;
                  }
            }
            if(true === flag) {
                this.cluster.add(points);
                setVisibleFlag(points, true);
            } else {
                this.cluster.remove(points);
                setVisibleFlag(points, false);
            }
            return this;
        },
        findByAddress: function (query) {
            ymaps.geocode(query, {
                boundedBy: this.startBounds,
                results: 1,
                strictBounds: true
            }).then(
                function (res) {
                    this.searchPoint = res.geoObjects.get(0);
                    if (this.searchPoint) {
                        this.map.geoObjects.add(this.searchPoint);
                        this.showPoint(this.searchPoint);
                    } else if(this.options.hasOwnProperty('mapContainer')) {
                        this.options.mapContainer.trigger('empty-search-result');
                    }
                }.bind(this),
                function (err) {
                }
            );
            return this;
        },
        clearAddressPoint: function () {
            if (null !== this.searchPoint) {
                this.map.geoObjects.remove(this.searchPoint);
            }
            return this;
        },
        showPoint: function (point, options) {
            this.applyMapBounds(this.buildBounds(point), options);
        },
        buildBounds: function (point) {
            var coordinates = point.geometry.getCoordinates();
            var bounds = [
                coordinates.slice(0),
                coordinates.slice(0)
            ];
            if (point.hasOwnProperty('libraryPoint')) {
                return bounds;
            }

            var shieldValue = 2;
            var nearlyPoint = this.findNearlyPoint(point);
            if (null !== nearlyPoint && nearlyPoint.hasOwnProperty('geometry')) {
                var boundsNearly = nearlyPoint.geometry.getBounds();

                if (shieldValue > Math.abs(bounds[1][0] - boundsNearly[1][0])
                    && shieldValue > Math.abs(bounds[1][1] - boundsNearly[1][1])
                    && shieldValue > Math.abs(bounds[0][0] - boundsNearly[0][0])
                    && shieldValue > Math.abs(bounds[0][1] - boundsNearly[0][1])
                ) {
                    for (var key in boundsNearly) {
                        if (!boundsNearly.hasOwnProperty(key)) {
                            continue;
                        }
                        if ('string' === typeof  boundsNearly[key][0]) {
                            boundsNearly[key][0] = parseFloat(boundsNearly[key][0]);
                        }
                        if ('string' === typeof  boundsNearly[key][1]) {
                            boundsNearly[key][1] = parseFloat(boundsNearly[key][1]);
                        }
                    }
                    if (bounds[0][0] > boundsNearly[0][0]) {
                        bounds[0][0] = boundsNearly[0][0];
                    }
                    if (bounds[0][1] > boundsNearly[0][1]) {
                        bounds[0][1] = boundsNearly[0][1];
                    }

                    if (bounds[1][0] < boundsNearly[1][0]) {
                        bounds[1][0] = boundsNearly[1][0];
                    }
                    if (bounds[1][1] < boundsNearly[1][1]) {
                        bounds[1][1] = boundsNearly[1][1];
                    }
                }
            }
            return bounds;
        },
        applyMapBounds: function (bounds, options) {
            options = options || {};
            var callback = options.callback || function () {
                };
            this.map.setBounds(bounds, {
                checkZoomRange: true, // проверяем наличие тайлов на данном масштабе.
                callback: function () {
                    var mapBounds = this.map.getBounds();
                    var mapLatValue = mapBounds[1][0] - mapBounds[0][0];
                    var topMarginValue = mapLatValue / 4;
                    var boundIndex = 0;
                    if (bounds[0][0] < bounds[1][0]) {
                        boundIndex = 1;
                    }
                    var topMargin = mapBounds[1][0] - bounds[boundIndex][0];
                    if (topMargin < topMarginValue) {
                        bounds[boundIndex][0] += topMarginValue;
                        this.map.setBounds(bounds, {checkZoomRange: true});
                        callback();
                    } else {
                        callback();
                    }
                }.bind(this)
            });
            return this;
        },
        findMyPosition: function (callback) {
            if(null !== this.myPosition) {
                callback(this.myPosition);
                return this;
            }
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    this.myPosition = new ymaps.GeoObject({
                            geometry: {
                                type: "Point",
                                coordinates: [position.coords.latitude, position.coords.longitude]
                            }
                        },
                        {
                            iconImageHref: '/local/templates/.default/markup/i/geolocation.png',
                            iconImageSize: [24, 24],
                            iconImageOffset: [-12, -12]
                        });
                    this.map.geoObjects.add(this.myPosition);
                    callback(this.myPosition);
                }.bind(this));
            }
            return this;
        },
        getSubsQuery: function(query) {
            query = query.toLowerCase();
            var subsQueries = {};
            subsQueries['ргб'] = 'российская государственная библиотека (ргб)';
            subsQueries['российская государственная библиотека'] = 'российская государственная библиотека (ргб)';
            if(subsQueries.hasOwnProperty(query)) {
                return subsQueries[query];
            }
            return query;
        },
        getInSightItems: function() {
            var inSightPoints = [];
            var mapBounds = this.map.getBounds();
            for (var key in this.points) {
                if(!this.points.hasOwnProperty(key) && !this.points[key].hasOwnProperty('libraryPoint')) {
                    continue;
                }
                if (false === this.points[key].visible || false === this.points[key].options.get('visible')) {
                    continue;
                }
                var coordinates = this.points[key].libraryPoint.coordinates;
                if ('string' === typeof  coordinates[0]) {
                    coordinates[0] = parseFloat(coordinates[0]);
                }
                if ('string' === typeof  coordinates[1]) {
                    coordinates[1] = parseFloat(coordinates[1]);
                }
                if (mapBounds[0][0] > coordinates[0]
                    || mapBounds[0][1] > coordinates[1]
                    || mapBounds[1][0] < coordinates[0]
                    || mapBounds[1][1] < coordinates[1]
                ) {
                    continue;
                }
                inSightPoints.push(this.points[key].libraryPoint);
            }

            return inSightPoints;
        }
    });
    var LibraryList = function (libraries, options) {
        this.libraries = libraries;
        this.options = $.extend(this.options, (options || {}));
        if(this.options.hasOwnProperty('events')) {
            this.events = this.options.events;
        }
    };
    LibraryList.prototype = $.extend(LibraryList.prototype, {
        libraries: {},
        options: {
            sortNearly: false
        },
        events: {},
        render: function() {
            var listSection  = $('#library-list');
            var list = listSection.find('div ol.b-elar_usrs_list');
            list.empty();
            listSection.find('.b-paging_cnt').hide();
            if(true === this.options.sortNearly) {
                this.libraries.sort(function(a, b) {
                    return a.distance - b.distance;
                });
            }
            for (var key in this.libraries) {
                if(!this.libraries.hasOwnProperty(key)) {
                    continue;
                }
                var item = this.libraries[key];
                var template = '<li>' +
                    '<span class="num">' + (++key) + '</span>' +
                    '<div class="b-elar_name iblock">' +
                    (
                        null !== item.link
                            ? '<a href="' + item.link + '" class="b-elar_name_txt">' + item.name + '</a>'
                            : '<span class="b-elar_name_txt">' + item.name + '</span>'
                    ) +
                    (
                        true === item.parten
                            ? (
                        '<div class="b-elar_status neb">' +
                        '<div class="icon">' +
                        '<div class="b-hint">Участник проекта НЭБ</div>' +
                        '</div>' +
                        (item.status ? item.status : '') +
                        '</div>'
                        )
                            : ''
                    ) +
                    '</div>' +
                    '<div class="b-elar_address iblock">' +
                    '<div class="b-elar_address_txt">' +
                    '<div class="icon">' +
                    '<div class="b-hint">Адрес</div>' +
                    '</div>' +
                    item.address +
                    ' </div>' +
                    '<a href="#" class="b-elar_address_link" data-lat="' +
                    item.coordinates[0] +
                    '" data-lng="' + item.coordinates[1] + '">Показать на карте</a>' +
                    '</div>' +
                    '</li>';
                list.append(template);
            }
            for(var i in this.events) {
                if(!this.events.hasOwnProperty(i)) {
                    continue;
                }
                switch(i) {
                    case 'clickShowMapLink':
                        list.find('.b-elar_address_link').click(this.events[i]);
                        break;
                }
            }

            list.quickPagerLibraries();
        }
    });
	$.fn.mappage = function(options) {
        var libraryMap;
        var boundChangeTimeOut = null;
        var buildListTimeOut = 1000;

        var clickShowMapLink = function(){
            var that = $(this);
            if (!that.data('lat') || !that.data('lng')) {
                return false
            }

            $.scrollTo('.b-map_search_filtr', {
                duration: 650,
                easing: 'easeOutBack'
            });

            setTimeout(function () {
                map.panTo([that.data('lat'), that.data('lng')], {
                    flying: false,
                    delay: 0,
                    duration: 600
                });
            }, 0);

            map.setZoom(16, {duration: 600});
            setTimeout(function () {
                libraryMap
                    .visiblePoints(null, false)
                    .visiblePoints(null, true);
                for (var m = 0; m < arr.length; m++) {
                    var coords = arr[m].geometry.getCoordinates();
                    if (coords[0] == that.data('lat') && coords[1] == that.data('lng')) {
                        map.geoObjects.add(arr[m]);
                        arr[m].balloon.open();

                    }
                }
            }.bind(this), 300);
        };

		this.each(function() {
            var autoCompleteItems = [];
            var mapContainer = $(this);
			var defaults = {};
            var startBounds = [];
			var errors = 0,
			msg = '',
			o;
			if (typeof options != 'string') {
				o = $.extend(defaults, options);
			} else {
				o = defaults;
			}
			var cont = $(this);
			var maproot = $('#ymap'), form = cont.find('.b-map_search_filtr'), exmarker = cont.find('.marker_lib.hidden');
            ymaps.ready(init); // on MAP READY

			function init() {
                map = new ymaps.Map("ymap", {
					center: [(maproot.data('lat')) ? maproot.data('lat') : 55.76, (maproot.data('lng')) ? maproot.data('lng') : 37.64],
					zoom: (maproot.data('zoom')) ? maproot.data('zoom') : 10
				});
				map.controls.add('zoomControl', {
					left: 20,
					top: maproot.height()/2
				});
				map.events.add('click', function(e) {
					if(map.balloon.isOpen()){
						map.balloon.close();
					}
				});
                startBounds = map.getBounds();
                map.events.add('boundschange', function () {
                    if(null !== boundChangeTimeOut) {
                        window.clearTimeout(boundChangeTimeOut);
                    }
                    boundChangeTimeOut = window.setTimeout(
                        function () {
                            if(null === libraryMap) {
                                return;
                            }
                            (new LibraryList(
                                libraryMap.getInSightItems(),
                                {
                                    events: {clickShowMapLink: clickShowMapLink},
                                    sortNearly: (null === libraryMap.myPosition ? false : true)
                                }
                            ))
                                .render();
                        }.bind(this),
                        buildListTimeOut
                    );
                }.bind(this));

				var clusterIcon = [{
							href: '/local/templates/.default/markup/i/clustericon.png',
							size: [55, 55],
							offset: [-20, -20]
						}],
				clusterNumbers = [10,40],
				clusterer = new ymaps.Clusterer({
					clusterIcons: clusterIcon,
					clusterNumbers: clusterNumbers
				});
				clusterer.options.set({gridSize: 80});
				map.behaviors.enable(['scrollZoom']);
				if (!maproot.data('path')) {
					alert('Не указан путь к файлу с точками');
					return false;
				}
				var arr =[];
                var coordsEqual = {};
				function resetPlacemarks(libs) {
					for (var i = 0; i < libs.length; i++) {

						var id = libs[i].id;
						libs[i].pmset = new ymaps.GeoObjectCollection({}, {
							iconImageHref: libs[i].placemarkicon,
							iconImageSize: libs[i].placemarksize,
							iconImageOffset: [-libs[i].placemarksize[0] / 2, -libs[i].placemarksize[1] / 2]
						});

						for (var k = 0; k < libs[i].regions.length; k++) { // добавить в нее точки
							var city = libs[i].regions[k].city,
							coordinates = libs[i].regions[k].coordinates,
							metro = libs[i].regions[k].metro; // metro array (minimum, one item)
							for (var m = 0; m < metro.length; m++) {
								var station = metro[m].station;
								for (var p = 0; p < metro[m].points.length; p++) {
									var point = metro[m].points[p];
                                    var buildContent = function (point) {
                                        return (
                                        '<span class="marker_lib">' +
                                        (
                                            point.link
                                                ? '<a href="' + point.link + '" class="b-elar_name_txt">' + point.name + '</a><br />'
                                                : '<span class="b-elar_name_txt">' + point.name + '</span><br />'
                                        )
                                        + (
                                            point.status
                                                ? '<span class="b-elar_status">' + point.status + '</span><br />'
                                                : ''
                                        )
                                        + '<span class="b-map_elar_info">'
                                        + '<span class="b-map_elar_infoitem">'
                                        + '<span>'
                                        + exmarker.find('.b-map_elar_info .addr span:first-child').text()
                                        + '</span>'
                                        + ((station) ? ' м. ' + station + ', ' : '') + point.address
                                        + '</span><br />' +
                                        (
                                            point.available
                                                ? '<span class="b-map_elar_infoitem">'
                                            + '<span>'
                                            + exmarker.find('.b-map_elar_info .graf  span:first-child').text()
                                            + '</span>'
                                            + point.available
                                            + '</span>' : ''
                                        )
                                        + '</span>'
                                        + '<span class="b-mapcard_act clearfix">'
                                        + (
                                            point.parten
                                                ? '<span class="right neb b-mapcard_status">'
                                            + exmarker.find('.b-mapcard_act .b-mapcard_status').text()
                                            + '</span>'
                                                : ''
                                        ) +
                                        (
                                            point.link
                                                ? '<a href="' + point.link + '" class="button_mode">'
                                            + exmarker.find('.b-mapcard_act .button_mode').text()
                                            + '</a>'
                                                : ''
                                        )
                                        + '</span>'
                                        + '</span>'
                                        );
                                    };

									var placemark = new ymaps.GeoObject({
										geometry: {
										  type: "Point",
										  coordinates: [point.coordinates[0], point.coordinates[1]]
									},
											  properties: {

										balloonContent: buildContent(point)


										}
									}, {
											balloonCloseButton: true,
											balloonOffset: [207, 160],
											balloonMaxWidth: 410,
											balloonMinWidth: 210,
											balloonAutoPan: true,
											hideIconOnBalloonOpen: false,
											iconImageHref: '/local/templates/.default/markup/i/pin.png', //i/pin.png
											iconImageSize: [34, 48],
                                            nImageOffset: [-17, -48]

										  });

                                    var coordinatesKey = point.coordinates[0] + '-' + point.coordinates[1];
                                    if(coordsEqual.hasOwnProperty(coordinatesKey)) {
                                        var balloonContent = coordsEqual[coordinatesKey].properties.get('balloonContent');
                                        coordsEqual[coordinatesKey].properties.set('balloonContent',
                                            coordsEqual[coordinatesKey].properties.get('balloonContent')
                                            + buildContent(point)
                                        );
                                    } else {
                                        coordsEqual[coordinatesKey] = placemark;
                                        placemark.libraryPoint = point;
                                        libs[i].pmset.add(placemark);
                                        clusterer.add(placemark);
                                        arr.push(placemark);
                                    }
								}
							}
						}
					}
                    map.geoObjects.add(clusterer);
				}

				maproot.on('libsloaded', function(e, libs) {
					resetPlacemarks(libs);
                    for(var index = 0; index < arr.length; index++) {
                        if(!arr[index].hasOwnProperty('libraryPoint')) {
                            continue;
                        }
                        autoCompleteItems.push(arr[index].libraryPoint.name);
                        autoCompleteItems.push(arr[index].libraryPoint.address);
                    }
                    if(1 === maproot.data('show-nearly')) {
                        libraryMap.showNearlyPoint();
                        libraryMap.showNearest = true;
                    }
                    if(field.val()) {
                        findLibrary();
                    }
				});

				$.getJSON(maproot.data('path'), function(json) {
					$.each(json, function(key, val) {
						maproot.trigger('libsloaded', [val]);
					});
				});

                mapContainer.on('empty-search-result', function() {
                    form.val('');
                    var emptyPopup = $('<div class="b_popup">' +
                    '<p>Ничего не найдено</p>' +
                    '<button class="find-nearly">Ближайшая библиотека</button>' +
                    '<br><button class="continue">Продолжить</button>' +
                    '</div>');
                    emptyPopup.find('.continue').click(function () {
                        emptyPopup.dialog('close');
                    });
                    emptyPopup.find('.find-nearly').click(function () {
                        field.val('');
                        form.submit();
                        emptyPopup.dialog('close');
                    });
                    emptyPopup.dialog({
                        closeOnEscape: true,
                        modal: true,
                        draggable: false,
                        resizable: false,
                        width: 300,
                        height: 150,
                        open: function () {
                            $(this).parents('.ui-dialog').parent().find('.ui-widget-overlay').addClass('dark');
                        }
                    });
                });

                libraryMap = new LibraryMap({
                    points: arr,
                    map: map,
                    mapContainer: mapContainer,
                    cluster: clusterer,
                    startBounds: startBounds
                });

                var findLibrary = function() {
                    libraryMap.clearAddressPoint();
                    var foundPoints = libraryMap.findInPoints(libraryMap.getSubsQuery(field.val()));
                    if (foundPoints.length > 0) {
                        libraryMap
                            .showNearlyPoint(foundPoints)
                            .visiblePoints(null, false)
                            .visiblePoints(foundPoints, true);
                    } else {
                        libraryMap
                            .visiblePoints(null, false)
                            .visiblePoints(null, true)
                            .findByAddress(field.val());
                    }
                };

                form.on('submit', function (e) {
                    if(true === libraryMap.showNearest) {
                        return;
                    }
                    e.preventDefault();
                    findLibrary();
                });

				DOCUMENT.on( "click", ".setLinkPoint",function(e) {
					e.preventDefault();
					if($(this).hasClass("setPointAndReload")){
						//при клике на "Да, верно" ставим город в куку и обновляем страницу
						setCityAndReload($(this).data("city-id"));
					} else {
						//при клике на название города ставим его на карте
						setLinkPoint($(this).data("lat"), $(this).data("lng"));
					}

				});
				function setLinkPoint(link_lat, link_lng){
					ymaps.geocode(link_lat + ", " + link_lng, {
						boundedBy: startBounds,
						results: 1,
                        strictBounds: true
					}).then(
						function(res) {
							var firstGeoObject = res.geoObjects.get(0),
								coords = firstGeoObject.geometry.getCoordinates(),
								bounds = firstGeoObject.properties.get('boundedBy');
							//				                bounds = firstGeoObject.properties.get('boundedBy');
							map.setBounds(bounds, {
								//checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
							});
							map.setZoom(10);
						});
				}

				var availabletext = [],
				boundedBy = [],
				field = $(".searchonmap");


				field.autocomplete({
					source: autoCompleteItems,
					minLength: 3,
					appendTo: field.parents('.b-map_search_filtr')
				}).click(function() {
					//$(this).autocomplete('search');
				}).keyup(function(event) {
					//var inp = $(this),
					//val = inp.val();
					//availabletext = [], boundedBy = [];
					//if (val.length > 4) { //autocomplit
					//	//////////////////////////////////////////////////////////////////
					//	//map.setBounds(map.getBounds())
					//	ymaps.geocode(val, {
					//		boundedBy: map.getBounds(),
					//		results: 5,
                     //       strictBounds: true
					//	}).then(function(res) {
					//		res.geoObjects.each(function(key) {
					//			if (availabletext.indexOf(key.properties.get('name')) < 0) {
					//				availabletext.push(key.properties.get('name'));
					//				boundedBy.push(key.properties.get('boundedBy'));
					//				boundedBy.push(map.getBounds());
					//			}
					//		});
					//	});
					//	inp.autocomplete("option", "source", availabletext);
					//	/////////////////////////////////////////////////////////////////
					//}

				});


				cont.parent().find('.b-elar_address_link').click(clickShowMapLink);

			}
		});
	}
	//end of closure
})(jQuery);



(function() { //create closure
	$.fn.registerPopup = function() { // попап регистрации
		this.each(function() {
			var lnk = $(this), popup = $('.b_register_popup').clone( true ),
			H = (lnk.data('height')) ? lnk.data('height') : 595,
			W = (lnk.data('width')) ? lnk.data('width') : 560;
			lnk.click(function(){
				popup.dialog({
					closeOnEscape: true,
					dialogClass: 'no-close',
					modal: true,
					draggable: false,
					resizable: false,
					width:W,
					height: H,
					dialogClass: 'commonpopup '+$(popup).data('class')+'',
					position: {
								my: "right top",
								at: "right bottom",
								of: lnk
							},
					open: function(){
						// window.setTimeout(function() {
						// 	popup.dialog("option", "position", {
						// 		my: "right top",
						// 		at: "right bottom",
						// 		of: lnk
						// 	});
						// 	}, 50);

						popup.find('.b-form').initForm();
					},
					close: function() {
						popup.dialog("close").remove();
					}
				});

				$('.ui-widget-overlay').click(function() {
					//Close the dialog
					popup.dialog("close");

				});
			});

		});
	}
	//end of closure
})(jQuery);



//функция устанавливает cookie c нужным id города и обновляет страницу
function setCityAndReload(city_id){
	var date = new Date( new Date().getTime() + 1000 * 60 * 60 * 24 * 365); //1 год
	document.cookie="library_city="+city_id+"; path=/; expires="+date.toUTCString();
	if(window.location.href.indexOf('/special/library/') > -1)
	{
		window.location.href = '/special/library/';
	}
	else
	{
		window.location.href = '/library/';
	}
}

(function() { //create closure
	$.fn.cityChoose = function() { // попап регистрации

		this.each(function() {
			var bt_popup = $('.js_changecity'),
			popup = $('.b-citychangepopup').clone( true ), line = $('.b-citypopup'),
			close_line = $('.close', line);


			close_line.click(function(e){
				e.preventDefault();
				line.fadeOut(300);
			});


			bt_popup.click(function(e){
				e.preventDefault();
				line.fadeOut(300);
				popup.dialog({
					closeOnEscape: true,
					dialogClass: 'no-close',
					modal: true,
					draggable: false,
					resizable: false,
					width:600,
					dialogClass: 'citypopup',
					open: function(){
						window.setTimeout(function() {
							popup.dialog("option", "position", {
								my: "center top+68",
								at: "center top",
								of: window
							});
							}, 50);

							$('.iblock', popup).cleanWS();
							$('input.checkbox:not(".custom")', popup).replaceCheckBox();
							var overlay = $('.ui-widget-overlay');
							overlay.addClass('black'), region = $('.b-handcityregion', popup);


							var form  = $('.b-map_search_form', popup), tbox = $('.input', form);

							region.click(function(){

								var reg = $(this), par = reg.closest('.b-handcityitem'), list = $('.b-cityquisklist', par);
								par.toggleClass('open');
								list.slideToggle();
							});

							$('.b-city', form).click(function(e){
								e.preventDefault();
								//tbox.val($(this).text());
								setCityAndReload($(this).data('city-id'));
							});


							var availabletext = [],
								boundedBy = [];


                            tbox.focus();
							tbox.autocomplete({
								source: availabletext,
								minLength: 3,
								appendTo: tbox.parents(form),
								select: function( event, ui ) {
									$(form).find(".formbutton").attr("data-city-id", ui.item.id);
								}
							}).click(function() {
								$(this).autocomplete('search');
							}).keyup(function(event) {
								var inp = $(this),
								ajax_path = $(inp).data('path'),
								val = inp.val();
								availabletext = [], boundedBy = [];
								if (val.length > 2) { //autocomplit
/*										//////////////////////////////////////////////////////////////////
									 //map.setBounds(map.getBounds())
									 ymaps.geocode(val, {
									 boundedBy: map.getBounds(),
									 results: 5
									 }).then(function(res) {
									 res.geoObjects.each(function(key) {
									 if (availabletext.indexOf(key.properties.get('name')) < 0) {
									 availabletext.push(key.properties.get('name'));
									 boundedBy.push(key.properties.get('boundedBy'));
									 boundedBy.push(map.getBounds());
									 }
									 });
									 });
									 inp.autocomplete("option", "source", availabletext);
									 /////////////////////////////////////////////////////////////////*/

									inp.autocomplete("option", "source", ajax_path);
									}

								});


							form.on('submit', function(e) {
								e.preventDefault();
								var city_id = $(form).find(".formbutton").attr("data-city-id");
                                if(!city_id) {
                                    var value = form.find('input.input').val();
                                    var loadedList = form.find('ul.ui-autocomplete li');
                                    if(value && loadedList.length > 0) {
                                        loadedList.get(0).click();
                                        city_id = $(form).find(".formbutton").attr("data-city-id");
                                    }
                                }
								if(city_id > 0) {
                                    setCityAndReload(city_id);
                                }
								line.fadeOut(300);
								popup.dialog("close");
							});



					},
					close: function() {
						popup.dialog("close").remove();
					}
				});
				DOCUMENT.on( "click", ".closepopup",function(e) {
					e.preventDefault();
					popup.dialog("close");

				});
				$('.ui-widget-overlay').click(function() {
					//Close the dialog
					popup.dialog("close");

				});
			});

		});
	}
	//end of closure
})(jQuery);

// maps page
(function($) { //create closure
	$.fn.mapones = function(options) {
		this.each(function() {
			var defaults = {};
			var errors = 0,
			msg = '',
			o;
			if (typeof options != 'string') {
				o = $.extend(defaults, options);
			} else {
				o = defaults;
			}
			var cont = $(this);
			ymaps.ready(init); // on MAP READY
			var map, placemark, bal = cont.parent().find('.marker_lib.hidden');
			var html = bal.html(), maproot = $('#ymap');

			function init() {
				map =  new ymaps.Map("fmap", {
					center: [(cont.data('lat')) ? cont.data('lat') : 55.76, (cont.data('lng')) ? cont.data('lng') : 37.64],
					zoom: (cont.data('zoom')) ? cont.data('zoom') : 10,
					type: 'yandex#publicMap'
				});

				map.controls.add('zoomControl', {
				left: 20,
				top: maproot.height()/2 + 10
				});
				map.events.add('click', function(e) {
					if(map.balloon.isOpen()){
						map.balloon.close();
					}

				});
				map.behaviors.enable(['scrollZoom']);
				placemark = new ymaps.Placemark(map.getCenter(), {
					balloonContent: '<span class="marker_lib">'+html+'</span>'
					}, {
						balloonCloseButton: true,
						balloonOffset: (cont.width() < 410)? [175, 180]:[207, 180],
						balloonMaxWidth: (cont.width() < 410)? 265:410,
						balloonMinWidth: (cont.width() < 410)? 265:410,
						balloonAutoPan: true,
						hideIconOnBalloonOpen: false,
						iconLayout: 'default#image',
						iconImageHref: '/local/templates/.default/markup/i/pin.png', //i/pin.png
						iconImageSize: [34, 48],
						iconImageOffset: [-17, -24]
				});
				map.geoObjects.add(placemark);
				placemark.balloon.open();

			}
		});
	}
	//end of closure
})(jQuery);


(function(cash) { //create closure
	$.fn.addrowForm = function(options){
		this.each(function(){
			var form = $(this), btn = $('.b-addrow', form), irow = $('.phonerow', form);


			var n = 0;
			btn.on('click', function(e){
				e.preventDefault();
				n=n+1;
				var clone_row = irow.clone(true);
				var idinp = clone_row.find('input').attr('id');
				clone_row.find('input').val('');
				clone_row.find('input').attr('id', idinp+'_'+n);
				clone_row.removeClass('phonerow');
				clone_row.insertBefore(btn);
			});

		});

	}
	//end of closure
})(jQuery);


(function($) {
	$.fn.formAddSelection = function() {
		this.each(function() {
			var form = $(this), tbox = $('.input', form), addlink = $('.b-selectionaddtxt', form);
			addlink.click(function(){
				$(this).addClass('hidden');
				tbox.removeClass('hidden')
			});
			tbox.focusout(function(){
				addlink.removeClass('hidden');
				$(this).addClass('hidden');
			});
			form.submit(function(e) {
				/* раскомментировать
				$.ajax({
				url: a.attr('href'),
				complete: function(data){ // ответ от сервера
				*/

				// имитация положительного ответа сервера

				/* раскомментировать
				}
				});
				*/
			});
			tbox.keypress(function(e){
				if(e.keyCode==13){
					/* раскомментировать
					$.ajax({
					url: a.attr('href'),
					complete: function(data){ // ответ от сервера
					*/

					// имитация положительного ответа сервера

					/* раскомментировать
					}
					});
					*/
					// alert(tbox.val());
				}
			});

		});
	}
	//end of closure
})(jQuery);




(function($) {
	$.fn.fieldEditable = function() { //редактируемое поле в таблице
		this.each(function() {
			var cont = $(this), text = $('.b-fieldtext', cont), edit = $('.b-fieldeedit', cont), tbox = $('.txt', cont),
			titblock = $('.b-fondtitle', cont), editblock = $('.b-fieldeditform', cont), bt = $('.b-editablsubmit', cont),
			mt = editblock.css('margin-top').replace("px", "");

			tbox.val(text.text());

			function hideeditfield(){
				titblock.removeClass('hidden');
				editblock.addClass('hidden');
				editblock.css({
					'margin-top': mt,
					'visibility': 'hidden'
				});
			}
			function showeditfield(){
				titblock.addClass('hidden');
				editblock.removeClass('hidden');
				editblock.css({
					'margin-top': 0,
					'visibility': 'visible'
				});

			}

			edit.click(function(e){
				e.preventDefault();
				showeditfield();
			});

			bt.click(function(e){
				e.preventDefault();
				text.text(tbox.val());
				hideeditfield();
			});

			tbox.keypress(function(e){

				if(e.keyCode==13){
					text.text(tbox.val());
					hideeditfield();
				}
			});


		});
	}
	//end of closure
})(jQuery);

(function(cash) { //create closure
	$.fn.bindImageLoad = function() {
		this.each(function(){

			var img = $(this), w = img.width(), h = img.height(), par = img.closest('a');

			if(!img.data('title'))
				img.data('title', '');
			if(!img.data('autor'))
				img.data('autor', '')

			par.css('position', 'relative');
			$('<span class="loadimg bbox"><div class="b-bookhover_tit">' + img.data('title') + '</div> <span class="b-autor">' + img.data('autor') + '</span></span>').appendTo(par);


			var loadimg = par.find('.loadimg');
			loadimg.height(h + 20);
			loadimg.width(w + 50);



		/*	$(img).bind('load',function(){
			 if (!img.data('loaded')) {
			  img.data('w',img.width()).data('h',img.height()).data('loaded',true);
			  scitem.data('imgW',img.data('w')).data('imgH',img.data('h'));

			 }
			});

			try {
			 if (!img.data('loaded')) {
			 }
			} catch (e) {
			}
*/

		});

	}
	//end of closure
})(jQuery);



// добавление новой подборки
$(document).on( "submit", "form.b-selectionadd.collection",function(e) {

	var form = $(this);
	var formAction = form.attr('action');
	var openButton = form.closest('.b-shresult_selection, .b-myselection_list').find('.js_openmfavs');
	if(openButton.length == 0){
		var openButton = form.closest('.meta.minus').find('.b-bookadd.fav');
		openButton.data('favs', openButton.data('collection'));
		formAction = formAction +'?id=' + form.closest('.b-result-docitem').attr('id');
	}

	var menu = form.closest('.b-shresult_selection, .b-myselection_list').find('.b-favs');
	if(menu.length == 0){
		var menu = form.closest('.meta.minus').find('.selectionBlock.selectionClone');
	}

	$.ajax({
		type: 	form.attr('method'),
		url: 	formAction,
		data: 	form.serialize(),
		dataType: "json"
	})
	.success(function(json) {
		form.find('.b-selectionaddtxt').removeClass('hidden');
		form.find('input.input').addClass('hidden');

		if(openButton.data('favs') && openButton.data('favs').length > 0){
			// обновляем список
			$.ajax({
				url: openButton.data('favs'),
				method: 'GET',
				success: function(html){
					menu.find('.b-selectionlist').remove();
					form.before(html).fadeIn();
					$('input.checkbox:not(".custom")', menu).replaceCheckBox();
					form.formAddSelection();
				}
			});

			if(openButton.data('callback') && openButton.data('callback').length > 0) // выполняем callback функцию если она прописана
				eval(openButton.data('callback'));
		}
		else
		{
			if(form.data('callback') && form.data('callback').length > 0) // выполняем callback функцию если она прописана
				eval(form.data('callback'));

		}
	});

	return false;
});

// добавление объекта в подборку
$(document).on( "change", "input.checkbox.InCollection",function(e) {
	var input = $(this);
	var fav = input.closest('.b-shresult_selection, .b-result_selection, .b-myselection_list').find('.js_openmfavs');
	if(fav.length == 0){
		var fav = input.closest('.meta.minus').find('.b-bookadd.fav');
		if(fav.length > 0)
			fav.data('favs', fav.data('collection'));
	}

	$.ajax({
		url: fav.data('favs'),
		method: 'POST',
		data: { idCollection : input.val(), action : input.attr('checked') },
		success: function(html){
			if(fav.data('callback') && fav.data('callback').length > 0) // выполняем callback функцию если она прописана
				eval(fav.data('callback'));

		}
	});
});


(function($) { //create closure
	$.fn.share = function(options) {
		this.each(function() {
			var block = $(this);

			if (!block.hasClass('ready')) {
				var fb = 'http://www.facebook.com/share.php?u=%url%&t=%title%';
				var odn = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
				var vk = 'http://vkontakte.ru/share.php?url=%url%&title=%title%';


				// fb
				block.find('.fblink ').on('click', function(e) {
					e.preventDefault();
					window.open(encodeURI(fb.replace(/\%url\%/, $(this).attr('href')).replace(/%title%/, ''+$(this).attr('title')+'')), 'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no');

				});
				// mail
				block.find('.odnlink').on('click', function(e) {
					e.preventDefault();
					window.open(encodeURI(odn.replace(/\%url\%/, $(this).attr('href')).replace(/%title%/, ''+$(this).attr('title')+'')), 'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no');
				});
				// vk
				block.find('.vklink').on('click', function(e) {
					e.preventDefault();
					window.open(encodeURI(vk.replace(/\%url\%/, $(this).attr('href')).replace(/%title%/, ''+$(this).attr('title')+'')), 'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no');
				});


				block.addClass('ready');
			}

		});
	}
	//end of closure
})(jQuery);


(function(cash) { //create closure
	$.fn.rolledText = function(){
		this.each(function(){
			var cont = $(this), 
				text =  cont.find('.b-rolled_in'), 
				h = cont.data('height') - 100, 
				next = cont.find('.slidetext');
			if(text.length){
				blok_height = text.height();

				if( blok_height > h ) {
					text.height(h);
				}
				else {
					next.hide();
					text.removeClass('b-rolled_in');
				}

				next.click(function(e){
					e.preventDefault();
					//blok_height = text.height();
					text.height(blok_height);
					text.addClass('b-full');
					// $(this).text('Скрыть');
					next.hide();
				});
			}

		});
	}
	//end of closure
})(jQuery);

/*(function(cash) { //create closure
	$.fn.closePage = function(){
		this.each(function(){
			var cont = $(this);
				cont.click(function(e){
					e.preventDefault();
					console.log(window.name);
					 winref = window.open('', 'thatname', '', true);
 					 winref.close();
				   // window.open('','_self').close();
				});
		});
	}
	//end of closure
})(jQuery);
*/

(function() { //create closure
	$.fn.add_books_to_digitizing = function(){
		this.each(function(){
			var A = $(this), T;
			A.on('click', function(e){
				e.preventDefault();
				var popup = $('.digitizing_popup');
				if (popup.length>0) {
					popup.remove();
				}
				popup =  $('<div class="digitizing_popup"></div>').appendTo('BODY');
				var loadtext = (A.data('loadtxt'))? A.data('loadtxt') : '';
				var loading = $('<span class="digitizing_popup_loading">'+ loadtext +'</span>').appendTo(popup);

				loading.hide();

				popup.dialog({
					modal: true,
					draggable: false,
					resizable: false,
					//maxWidth: 480,
					create: function(e,ui){

						if (A.data('width')) popup.dialog("option", "width", A.data('width'));
						var title = $('<h2>'+A.text()+'</h2>').appendTo(popup);
						popup.dialog("option", "position", { my: "center top", at: "center top", of: $('.innersection') });

						var bottomclose = $('<a class="button_mode bottompopupclose" href="#">Сохранить</a>').appendTo(popup);
						bottomclose.on('click', function(e){
							e.preventDefault();
							popup.dialog('close');
						});
						var toppopupclose = $('<a href="#" class="button_mode toppopupclose">Сохранить</a>').appendTo(popup);
						toppopupclose.on('click', function(e){
							e.preventDefault();
							//alert('dddd')
							popup.dialog('close');
						});

					},
					open: function(){
						$('.ui-widget-overlay').addClass('digitoverlay').on('click', function(e){
							popup.dialog('close');
						});
						$(this).parent().css('top','150px');

						loading.show();
						var popframe = $('<iframe src="'+A.attr('href')+'" id="ifr_popup" height="200" scrolling="no" frameborder="0" ></iframe>').appendTo(popup);
						popframe.width('100%');

						var domframe = popframe.get(0);
						domframe.onload = function(e){
							var ifr = e.target;
							var ifrwnd = ifr.contentWindow;
							var ifrdoc = (ifr.contentWindow || ifr.contentDocument);
							if (ifrdoc.document) ifrdoc = ifrdoc.document;
							ifrdoc = $(ifrdoc);
							var BODY = ifrwnd.$('body');
							if(BODY.outerHeight(true) > 350){
							popframe.height(BODY.outerHeight(true));
							}
							BODY.on('heightchangestart', function(){

								BODY.css({'height':BODY.height(), 'overflow':'hidden'});
							});
							BODY.on('heightchangeend', function(){
								BODY.css({'height':'auto', 'overflow':''});
								if(BODY.outerHeight(true) > 350){
									popframe.height(BODY.outerHeight(true));
								}
							});


							var iframeWindow = $('#ifr_popup');
							var searchField = BODY.find('.b-search_ex');
							BODY.find('.b-search_exlink').on('click',function(){
								setTimeout(function(){
									if(BODY.find('.b-search_exlink').hasClass('open')){
										iframeWindow.height(parseInt(iframeWindow.height())+parseInt(searchField.height()));
									}
									else {
										iframeWindow.height(parseInt(iframeWindow.height())-parseInt($('.b-search_ex').height()));
									}
								} , 1500);
							});

							loading.hide();

						};

					},
					close: function(e,ui){
						popup.dialog('destroy');
						popup.remove();
						location.reload();
					}
				});

			});
		});
	}//end of closure
})(jQuery);

(function() { //create closure
	$.fn.add_books_to_fond = function(){
		this.each(function(){
			var A = $(this), T;
			A.on('click', function(e){
				e.preventDefault();
				var popup = $('.fond_popup');
				if (popup.length>0) {
					popup.remove();
				}
				popup =  $('<div class="fond_popup"></div>').appendTo('BODY');
				var loading = $('<span class="digitizing_popup_loading">'+A.data('loadtxt')+'</span>').appendTo(popup);

				loading.hide();

				popup.dialog({
					modal: true,
					draggable: false,
					resizable: false,
					minHeight: 810,
					dialogClass: 'ajaxpopup',
					//maxWidth: 480,
					create: function(e,ui){

						if (A.data('width')) popup.dialog("option", "width", A.data('width'));
						var title = $('<h2>'+A.text()+'</h2>').appendTo(popup);
						popup.dialog("option", "position", { my: "center top", at: "center top", of: $(window) });
						var toppopupclose = $('<a class="closepopup" href="#">Закрыть окно</a>').appendTo(popup);
						toppopupclose.on('click', function(e){
							e.preventDefault();
							//alert('dddd')
							popup.dialog('close');
						});
					},
					open: function(){
						$('.ui-widget-overlay').addClass('dark').on('click', function(e){
							popup.dialog('close');
						});
						loading.show();
						var popframe = $('<iframe src="'+A.attr('href')+'" height="750" scrolling="no" frameborder="0"></iframe>').appendTo(popup);
						popframe.width('100%');

						var domframe = popframe.get(0);
						domframe.onload = function(e){
							var ifr = e.target;
							var ifrwnd = ifr.contentWindow;
							var ifrdoc = (ifr.contentWindow || ifr.contentDocument);
							if (ifrdoc.document) ifrdoc = ifrdoc.document;
							ifrdoc = $(ifrdoc);
							var BODY = ifrwnd.$('body');
							//popframe.height(BODY.outerHeight(true));
							BODY.on('heightchangestart', function(){

								BODY.css({'height':BODY.height(), 'overflow':'hidden'});
							});
							BODY.on('heightchangeend', function(){

								BODY.css({'height':'auto', 'overflow':''});
								//popframe.height(BODY.outerHeight(true));
							});
							popframe.contents().find('.btrefuse').on('click', function(e){
								popup.dialog('close');
							});
							loading.hide();
						};
					},
					close: function(e,ui){
						popup.dialog('destroy');
						popup.remove();
					}
				});

			});
		});
	}//end of closure
})(jQuery);

(function() { //create closure
				$.fn.oldiePopup = function() { // попап регистрации

					this.each(function() {
						var popup = $(this).clone( true );
						popup.removeClass('hidden');
						popup.dialog({
							closeOnEscape: true,
							modal: true,
							draggable: false,
							resizable: false,
							width: 480,
							dialogClass: 'ajaxpopup',
							position: "center",
							open: function(){
								$('.ui-widget-overlay').addClass('black');
							},
							close: function() {
								popup.dialog("close").remove();
							}
						});

						$('.ui-widget-overlay').click(function() {
							//Close the dialog
							popup.dialog("close");
						});
					});
				}
				//end of closure
})(jQuery);

(function() { //create closure
	$.fn.openHideBlock = function() { // показывать скрытые блоки
		this.each(function() {
			var lnk =$(this);
			if(!lnk.hasClass('sort')){
				lnk.on('click',function(e) {
					e.preventDefault();
					if($('.b-sidenav .close').length == 0)
						$('<a href="#" class="close"></a>').prependTo($('.b-sidenav'));
					$('.b-sidenav').fadeIn();
					$('.b-sidenav .close').on('click', function(e) {
						e.preventDefault();
						$(this).parent().fadeOut();
						$(this).remove();
					});
				});
			}else {
				var lock = true;
				lnk.on('click',function(e) {
					e.preventDefault();
					if(lock){
						$('.sort_wrap').css({'display':'block'});
						if($('.sort_wrap div').length == 0){
							$('.sort_wrap').find('a').wrapAll('<div></div>');
						}
						lock=false;
					}else{
						$('.sort_wrap').css({'display':'none'});
						lock=true;
					}
				});
			}
		});
	}
	//end of closure
})(jQuery);

(function($) { //create closure
	$.fn.doItOnChange = function(options){
		this.each(function(){
			var inp = $(this);
			$(this).change(function(){
				var way = $(this).val();
				if(typeof(ajaxUpdate) == "function")
					ajaxUpdate(way);
				else
					window.location = way;
			});
		});
	}
	//end of closure
})(jQuery);


(function() { //create closure
	$.fn.autoregPopup = function(params) { // попап регистрации
        params = params || {};
        var openCallback = ('function' === typeof params.open
            ? params.open
            : function () {
        });
		this.each(function(){
			var popup = $(this).clone(true);
			popup.removeClass('hidden');
			popup.dialog({
				closeOnEscape: true,
				modal: true,
				draggable: false,
				resizable: false,
				width: 670,
				dialogClass: 'autoreg_popup',
				position: "center",
				open: function(){
					$('.ui-widget-overlay').addClass('black');
					popup.find('.closepopup').click(function() {
						//Close the dialog
						popup.dialog("close").remove();
						$('.ui-widget-overlay').remove();
						return false;
					});
                    openCallback();
				},
				close: function() {
					popup.dialog("close").remove();
				}
			});

			$('.ui-widget-overlay').click(function() {
				//Close the dialog
				popup.dialog("close");
			});
		});
	};
	//end of closure
})(jQuery);

(function() { //create closure
	$.fn.simplePopup = function() { // попап регистрации
		this.each(function(){
			var popup = $(this).clone(true);
			popup.removeClass('hidden');
			popup.dialog({
				closeOnEscape: true,
				modal: true,
				draggable: false,
				resizable: false,
				width: 670,
				//dialogClass: 'autoreg_popup',
				position: "center",
				open: function(){
					$('.ui-widget-overlay').addClass('black');
					popup.find('.closepopup').click(function() {
						//Close the dialog
						popup.dialog("close").remove();
						$('.ui-widget-overlay').remove();
						return false;
					});
				},
				close: function() {
					popup.dialog("close").remove();
				}
			});
			$('.ui-widget-overlay').click(function() {
				//Close the dialog
				popup.dialog("close");
			});
		});
	};
	//end of closure
})(jQuery);

(function() { //create closure
	$.fn.autocompleteHover = function() { // выделение поисковых позиций при выборе с клавиатуре
		$(this).on('keydown',function(){
			$('.ui-autocomplete li').removeClass('list-hover');
	    	 if($('.ui-autocomplete a').hasClass('ui-state-focus')){
	    	 	$('.ui-state-focus').parent().addClass('list-hover');
	    	 }
	    });
	};
	//end of closure
})(jQuery);

(function() { //create closure
	$.fn.enableEnterSearch = function() { // выделение поисковых позиций при выборе с клавиатуре
		$(this).keydown(function(event){
			if(event.keyCode == 13 ||  event.keyCode== 108){
				event.preventDefault();
				$('.b-search_ex').hide();
				$('.b-search_field .b-search_bth').click();
			}
		});
	};
	//end of closure
})(jQuery);

(function () {
	$.fn.sendRestRequest = function (options) {
		var url = $(this).data('url');
		var requestMethod = $(this).data('method');
		var userToken = $('meta[name=user-token]').attr('content');
		var that = this;

		if ('function' === typeof options) {
			options = {success: options};
		}
		if ('function' === typeof options.success) {
			var successCallback = options.success;
			options.success = function () {
				var args = [];
				args.push.apply(args, arguments);
				args.push(this);
				successCallback.apply(that, args);
			}
		}
		if ('function' === typeof options.error) {
			var errorCallback = options.error;
			options.success = function () {
				var args = [];
				args.push.apply(args, arguments);
				args.push(this);
				errorCallback.apply(that, args);
			}
		}

		options = $.extend({
			url: url,
			type: requestMethod,
			parameters: {}
		}, options);

		options.url += (0 > options.url.indexOf('?')) ? '?' : '&';
		options.url += 'token=' + userToken;
		for (var key in options.parameters) {
			if (options.parameters.hasOwnProperty(key)) {
				options.url += '&' + key + '=' + encodeURIComponent(options.parameters[key]);
			}
		}
		$.ajax(options);
	};
	$.fn.activeDeleteRequest = function (options) {
		$(this).click(function(e) {
			e.preventDefault();
			var confirmMsg = $(this).data('confirm-msg');
			if (confirmMsg && confirm(confirmMsg)) {
				$(this).sendRestRequest(options);
			}
		});
	};
})(jQuery);

(function () {
    $.fn.buildHighCharts = function () {
        $(this).each(function () {
            var categories = $(this).data('categories'),
                title = $(this).data('title'),
                dataValues = $(this).data('values');
            $(this).highcharts({
                title: false,
                xAxis: {
                    categories: categories
                },
                yAxis: {
                    title: {
                        text: title
                    },
                    plotLines: [{
                        value: 100,
                        width: 3,
                        color: '#808080'
                    }],
                    min: 0,
                    labels: {
                        formatter: function () {
                            if (this.value >= 1000000000) {
                                return this.value / 1000000000 + ' млрд';
                            } else if (this.value >= 1000000) {
                                return this.value / 1000000 + ' млн';
                            } else if (this.value >= 1000) {
                                return this.value / 1000 + ' тыс';
                            }
                            return this.value
                        }
                    }
                },

                legend: true,
                series: [{
                    name: title,
                    data: dataValues
                }]
            });
        });
    };
})(jQuery);

/** Из статистики библиотекаря **/
(function($){$.fn.dcAccordion=function(options){var defaults={classParent:'dcjq-parent',classActive:'active',classArrow:'dcjq-icon',classCount:'dcjq-count',classExpand:'dcjq-current-parent',eventType:'click',hoverDelay:300,menuClose:true,autoClose:true,autoExpand:false,speed:'slow',saveState:true,disableLink:true,showCount:false,cookie:'dcjq-accordion'};var options=$.extend(defaults,options);this.each(function(options){var obj=this;setUpAccordion();if(defaults.saveState==true){checkCookie(defaults.cookie,obj)}if(defaults.autoExpand==true){$('li.'+defaults.classExpand+' > a').addClass(defaults.classActive)}resetAccordion();if(defaults.eventType=='hover'){var config={sensitivity:2,interval:defaults.hoverDelay,over:linkOver,timeout:defaults.hoverDelay,out:linkOut};$('li a',obj).hoverIntent(config);var configMenu={sensitivity:2,interval:1000,over:menuOver,timeout:1000,out:menuOut};$(obj).hoverIntent(configMenu);if(defaults.disableLink==true){$('li a',obj).click(function(e){if($(this).siblings('ul').length>0){e.preventDefault()}})}}else{$('li a',obj).click(function(e){$activeLi=$(this).parent('li');$parentsLi=$activeLi.parents('li');$parentsUl=$activeLi.parents('ul');if(defaults.disableLink==true){if($(this).siblings('ul').length>0){e.preventDefault()}}if(defaults.autoClose==true){autoCloseAccordion($parentsLi,$parentsUl)}if($('> ul',$activeLi).is(':visible')){$('ul',$activeLi).slideUp(defaults.speed);$('a',$activeLi).removeClass(defaults.classActive)}else{$(this).siblings('ul').slideToggle(defaults.speed);$('> a',$activeLi).addClass(defaults.classActive)}if(defaults.saveState==true){createCookie(defaults.cookie,obj)}})}function setUpAccordion(){$arrow='<span class="'+defaults.classArrow+'"></span>';var classParentLi=defaults.classParent+'-li';$('> ul',obj).show();$('li',obj).each(function(){if($('> ul',this).length>0){$(this).addClass(classParentLi);$('> a',this).addClass(defaults.classParent).append($arrow)}});$('> ul',obj).hide();if(defaults.showCount==true){$('li.'+classParentLi,obj).each(function(){if(defaults.disableLink==true){var getCount=parseInt($('ul a:not(.'+defaults.classParent+')',this).length)}else{var getCount=parseInt($('ul a',this).length)}$('> a',this).append(' <span class="'+defaults.classCount+'">('+getCount+')</span>')})}}function linkOver(){$activeLi=$(this).parent('li');$parentsLi=$activeLi.parents('li');$parentsUl=$activeLi.parents('ul');if(defaults.autoClose==true){autoCloseAccordion($parentsLi,$parentsUl)}if($('> ul',$activeLi).is(':visible')){$('ul',$activeLi).slideUp(defaults.speed);$('a',$activeLi).removeClass(defaults.classActive)}else{$(this).siblings('ul').slideToggle(defaults.speed);$('> a',$activeLi).addClass(defaults.classActive)}if(defaults.saveState==true){createCookie(defaults.cookie,obj)}}function linkOut(){}function menuOver(){}function menuOut(){if(defaults.menuClose==true){$('ul',obj).slideUp(defaults.speed);$('a',obj).removeClass(defaults.classActive);createCookie(defaults.cookie,obj)}}function autoCloseAccordion($parentsLi,$parentsUl){$('ul',obj).not($parentsUl).slideUp(defaults.speed);$('a',obj).removeClass(defaults.classActive);$('> a',$parentsLi).addClass(defaults.classActive)}function resetAccordion(){$('ul',obj).hide();$allActiveLi=$('a.'+defaults.classActive,obj);$allActiveLi.siblings('ul').show()}});function checkCookie(cookieId,obj){var cookieVal=$.cookie(cookieId);if(cookieVal!=null){var activeArray=cookieVal.split(',');$.each(activeArray,function(index,value){var $cookieLi=$('li:eq('+value+')',obj);$('> a',$cookieLi).addClass(defaults.classActive);var $parentsLi=$cookieLi.parents('li');$('> a',$parentsLi).addClass(defaults.classActive)})}}function createCookie(cookieId,obj){var activeIndex=[];$('li a.'+defaults.classActive,obj).each(function(i){var $arrayItem=$(this).parent('li');var itemIndex=$('li',obj).index($arrayItem);activeIndex.push(itemIndex)});$.cookie(cookieId,activeIndex,{path:'/'})}}})(jQuery);
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);

$(function(){
	$('ul.uleditstat').dcAccordion({
		classParent: 'active-parent',
		classArrow: 'icon',
		eventType: 'click',
		autoClose: true,
		saveState: false,
		disableLink: true,
		showCount: false,
		speed: 'fast',
		cookie: 'uleditstat',
	});
});


//Получаем значение даты в строковом формате для вывода в инпуты
function getOptionalDate(date){
	var day = date.getUTCDate().toString();
	if(day.length<2) day = '0' + day;
	var month = (date.getUTCMonth() + 1).toString();
	if(month.length<2) month = '0' + month;
	var year = date.getUTCFullYear();
	var resultDate = day + '.' + month + '.' + year;
	return resultDate;
}
//Обработчик всплывающей подсказки
(function() { //create closure
	$.fn.exclamationOpener = function() { // выделение поисковых позиций при выборе с клавиатуре

		$(this).hover(function() {
			var popup = $(this).next();
			//Позиционирование всплывающего окна, чтобы не уползала за экран
			var popupHeight = popup.outerHeight();
			var openerCoords = $(this).offset();
			var topWindowCoords = $('body').scrollTop();
			var openerBottomCoords = $(this).height();
			var popupTop = openerBottomCoords + 15;

			if((openerCoords.top - topWindowCoords) < popupHeight){
				$(popup).addClass('popup-window-bottom');
				popup.css({
					'top': popupTop+'px',
					'bottom' : 'auto'
				});
			}
			else {
				$(popup).removeClass('popup-window-bottom');
				popup.css({
					'top': 'auto',
					'bottom': popupTop+'px',

				});
			}

			popup.fadeIn('fast', function(){
				popup.addClass('popup-hover');
			});
			event.stopPropagation;
		}, function() {
			var popup = $(this).next();
			setTimeout(function() {
				if(!popup.hasClass('popup-self')){
					popup.fadeOut('fast', function(){
						popup.removeClass('popup-hover');
					});
				}
			}, 500);
		});

		$(this).next().hover(function() {
			$(this).fadeIn();
			$(this).addClass('popup-self');
			event.stopPropagation;
		},
		function() {
				var popup = $(this);
					popup.removeClass('popup-self')
				setTimeout(function() {
					popup.fadeOut('slow', function(){
						popup.removeClass('popup-self');
					});
				}, 300);
		});

		$(this).click(function(){
			event.preventDefault();
		});

		$(document).click(function(event){
			if(event.target) {
				if($(event.target).closest('.popup-window').length==0  && !event.target.classList.contains('exclamation-opener')) {
					$('.popup-window').fadeOut().removeClass('popup-hover');
				}
			}
		});
	};
	//end of closure
})(jQuery);


function getCategoryText(number, listLength){
	if(number === listLength){
		return 'Выбраны все коллекции';
	}
	else {
		if(number===0){
			return 'Коллекции не выбраны';
		}
		if(number===1){
			return 'Выбрана ' + number + ' коллекция';
		}
		if(number>=2 && number <=4){
			return 'Выбраны ' + number + ' коллекции';
		}
		if(number>=5 && number <=20){
			return 'Выбрано ' + number + ' коллекций';
		}
	}

}

function authorizationErrorMsg(msg) {
	var targetBlock = $('.b_login_popup .error_message');
	targetBlock.html('').html(msg).show();
}

function messageOpen(cont) { // всплывающее сообщение
/*	if(h !== undefined) {
		document.location=h;
	  var time = (new Date()).getTime();
	  setTimeout(function(){
	   var now = (new Date()).getTime();
	   if((now-time)<400) {
		}
	  }, 300);*/
		var c = $(cont).clone(true);
		c.removeClass('hidden');
		c.dialog({
			closeOnEscape: true,
			modal: true,
			draggable: false,
			resizable: false,
			width: 400,
			dialogClass: 'ajaxpopup',
			position: "center center",
			open: function(){
				$('.ui-button').addClass('formbutton');
				$('.ui-widget-overlay').addClass('black');
				c.find('.closepopup').click(function() {
							//Close the dialog
							c.dialog( "destroy" ).remove();
							return false;
				});
			},
			buttons: {
				Ok: function() {
					$( this ).dialog( "destroy" ).remove();
				}
			},
			close: function() {
				popup.dialog("close").remove();
			}
		});

		$('.ui-widget-overlay').click(function() {
			//Close the dialog
			c.dialog( "destroy" ).remove();
		});
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

// device detector
function testDevice() {
	var _doc_element, _find, _user_agent;
	window.device={};
	_doc_element=window.document.documentElement;
	_user_agent=window.navigator.userAgent.toLowerCase();
	device.ios=function(){return device.iphone() || device.ipod() || device.ipad();};
	device.iphone=function(){return _find('iphone');};
	device.ipod=function(){return _find('ipod');};
	device.ipad=function(){return _find('ipad');};
	device.android=function(){return _find('android');};
	device.androidPhone=function(){return device.android() && _find('mobile');};
	device.androidTablet=function(){return device.android() && !_find('mobile');};
	device.blackberry=function(){return _find('blackberry') || _find('bb10') || _find('rim');};
	device.blackberryPhone=function(){return device.blackberry() && !_find('tablet');};
	device.blackberryTablet=function(){return device.blackberry() && _find('tablet');};
	device.windows=function(){return _find('windows');};
	device.windowsPhone=function(){return device.windows() && _find('phone');};
	device.windowsTablet=function(){return device.windows() && _find('touch');};
	device.fxos=function(){return (_find('(mobile;') || _find('(tablet;')) && _find('; rv:');};
	device.fxosPhone=function(){return device.fxos() && _find('mobile');};
	device.fxosTablet=function(){return device.fxos() && _find('tablet');};
	device.meego=function(){return _find('meego');};
	device.mobile=function(){return device.androidPhone() || device.iphone() || device.ipod() || device.windowsPhone() || device.blackberryPhone() || device.fxosPhone() || device.meego();};
	device.tablet=function(){return device.ipad() || device.androidTablet() || device.blackberryTablet() || device.windowsTablet() || device.fxosTablet();};
	device.portrait=function(){return Math.abs(window.orientation)!==90;};
	device.landscape=function(){return Math.abs(window.orientation)===90;};
	_find = function(needle){return _user_agent.indexOf(needle) !== -1;};
}
testDevice();

$(function(){
    $('#regform input').on('focus', function(){
        var elParent;
        if ($(this).attr('name') == 'UF_PASSPORT_SERIES')
            elParent = $(this).parent().parent();
        else
            elParent = $(this).parent();
        elParent.find('em.error').click();
        elParent.removeClass('error');
    });
    $('#regform a.selopener').bind('click', function(){
        var elParent;
        elParent = $(this).parent().parent();
        elParent.find('em.error').click();
        elParent.removeClass('error');
    });
    $('#setphoto_scan1, #setphoto_scan2').on('click', function(){
        var elParent;
        elParent = $(this).parent().parent();
        elParent.find('em.error').click();
        elParent.removeClass('error');
    });

	$('#asearch').autocompleteHover();
	$('.b_search_txt').autocompleteHover();
	$('#asearch').enableEnterSearch();
	$('.b_search_txt').enableEnterSearch();
	$('#asearch').keyup(function(event) {
		if($(this).val().length > 0) {
			$('.clean-link').show();
		}
		else {
			$('.clean-link').hide();
		}
	});

	$('.exclamation-opener').exclamationOpener();

    $('.popup-window-closedbook-link').click(function(){
        if(!$('.library-map').is(':visible')){
            $('.link-nearest-library').click();
        }
    });

	$('.bookclose-blank').click(function(event){
		event.preventDefault();
		location.href = '/';
	});

    //Костыль для добавления датапикера в поле field_VALIDITY битриксового компонента
    var cloneInput = $('#field_VALIDITY input').clone().addClass('datepicker');
    $('#field_VALIDITY').html('').html(cloneInput);

	var currentDate = new Date();
    $( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      maxDate: '+4y',
      showOn: "button",
      buttonImage: "/bitrix/images/icons/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Выберите дату"
    });
    if(!$('.datepicker-current').val()){
        $('.datepicker-current').val(getOptionalDate(currentDate));
    }

    if(!$('.datepicker-from').val()){
        $('.datepicker-from').val(getOptionalDate(new Date(currentDate.getUTCFullYear(), currentDate.getUTCMonth()-1, currentDate.getUTCDate())));
    }

	$('.boockard-read-button').click(function(event){
        if(!$(this).hasClass('boockard-read-button--disabled')){
            $('.boockard-error-msg').hide();
            readBook(event, this);
            setTimeout(function(){
                if($('.boockard-read-button').data('load') != 'load') $('.boockard-error-msg').text('Ошибка сервера. Пожалуйста, повторите запрос').show();
            }, 5000);
        }
	});

	if($('.b-bookpopup').parent('section') != ''){$('.bookclose').hide();}

	/*Убираем плейсхолдер при фокусе на поле поиска*/
	var mainSearchPlaceholder = ''
	$('#asearch').focus(function(){
		mainSearchPlaceholder = $(this).attr('placeholder');
		$(this).attr('placeholder','');
	}).focusout(function(){
		$(this).attr('placeholder',mainSearchPlaceholder);
	});

	$('.rest-action.delete-row').activeDeleteRequest(function (data) {
		if (data.hasOwnProperty('deletedFunds') && data.deletedFunds > 0) {
			$(this).parent().prepend($(this).data('delete-msg'));
			$(this).remove();
		}
	});
	$('.rest-action.fund-delete').click(function (e) {
		e.preventDefault();
		var message = prompt($(this).data('prompt-msg'));
		$(this).sendRestRequest({
			success: function (data) {
				if (data.hasOwnProperty('deletedFunds') && data.deletedFunds > 0) {
					$(this).parent().prepend($(this).data('delete-msg'));
					$(this).remove();
				}
			},
			parameters: {
				reasonMsg: message
			}
		});
	});

	$('.rest-action.confirm').click(function (e) {
		e.preventDefault();
		var confirmMsg = $(this).data('confirm-msg');
		if (confirmMsg && confirm(confirmMsg)) {
			$(this).sendRestRequest({
				success: function (data) {
					if (data.hasOwnProperty('success') && true === data.success) {
						$(this).parent().text($(this).data('result-msg'));
					}
				}
			});
		}
	});

	$('.graphicContainer').buildHighCharts();

	$('#fixed_form').submit(function(event) {
		if($('#asearch').val().search(/\.\S/)>0){
                event.preventDefault();
                var result = '';

                for(var i=0;i<$('#asearch').val().length;i++){
                    if($('#asearch').val()[i]=='.' && $('#asearch').val()[i+1]!=' '){
                        result += $('#asearch').val()[i]+' ';
                    }
                    else {
                        result += $('#asearch').val()[i];
                    }
                }
                $('#asearch').val(result);
                $('#fixed_form').submit();
            }
    });

	//Кнопка закрыть для всплывающих окон
	$('.close-window').click(function(event){
		event.preventDefault();
		$(this).parent().hide();
	});
	$(document).on('click','.clear-form',function(e){
		e.preventDefault();
		$(this).parent().find('form').each(function(){
			$(this)[0].reset();
		});
	});

	$(document).click(function(event){

		if(event.target) {
			if($(event.target).closest('.exsearch-category-list').length==0 && !event.target.classList.contains('category-opener') ){
				$('.exsearch-category-list').slideUp(300);
				$('.category-opener').removeClass('category-opener--active');
			}
		}
	});

    //Прячем уведомление о установке просмотровщика по клику на крест
    $('.cookie-closer').click(function(){
    	var closerName = $(this).data('name');
    	// +1 день от текущего момента
		var date = new Date;
		date.setDate(date.getDate() + 183);
		var options = {'expires': date}
		setCookie('cookieHide', closerName, options);
    	$(this).parent().slideUp();
    });
    if(!getCookie('cookieHide')) {
    	$('.header-viewer-warning').slideDown();
    }

    //Отображение иконок мобильных приложений в зависимости от устройства
    if(isMobile.any()){
    	if(isMobile.iOS()){$('header .b-headerapps_img').css('display','inline-block');}
    	if(isMobile.Android()){$('header .b-headergplay_img').css('display','inline-block');}
    	if(isMobile.Windows()){$('header .b-winphone_img').css('display','inline-block');}
    }
    else{
    	//$('.b-neb_viewer').css('display','inline-block');
    }

	/** @todo remove this shit */
	$('input.one-view-price-input').change(function () {
		var value = $(this).val();
		value = parseFloat(value);
		var table = $(this).parent().parent().parent();
		var views = table.find('td.views span');
		if(views.length > 0) {
			views = views.text();
			views = parseInt(views);
			if(views > 0) {
				table.find('td.summ span').text((views * value).toFixed(2));
			}
		}
	});
});




