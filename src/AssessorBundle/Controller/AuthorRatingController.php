<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 14:43
 */

namespace AssessorBundle\Controller;


use AssessorBundle\Controller\Api\ApiAuthorRatingController;
use AssessorBundle\Controller\Api\RatingApiInterface;
use AssessorBundle\Traits\ControllerAssessorRatingTrait;
use BitrixBundle\Annotations\IncludeBitrix;
use JMS\DiExtraBundle\Annotation as DI;
use BitrixBundle\Annotations\Template as BitrixTemplate;

class AuthorRatingController
{
    use ControllerAssessorRatingTrait;

    /**
     * @DI\Inject("assessor.api.author_rating")
     * @var ApiAuthorRatingController
     */
    public $api;

    /**
     * @BitrixTemplate("AssessorBundle:assessor:authors.html.php")
     * @IncludeBitrix()
     *
     * @return array
     */
    public function listAction()
    {
        if ($query = $this->request->query->get('author')) {
            $qb = $this->_getListQueryBuilder($this->api->getEntityName());
            if (is_array($query)) {
                foreach ($query as $key => $value) {
                    $qb->orWhere($qb->expr()->like('t.author', "?$key"));
                    $query[$key] = "%$value%";
                }
                $qb->setParameters($query);
            } else {
                $qb->where($qb->expr()->like('t.author', ':query'));
                $qb->setParameter('query', "%$query%");
            }
        }
        if (!$order = $this->request->query->get('order')) {
            $order = ['rating' => 'desc'];
        }
        $result = $this->getListData($this->api->getEntityName(), $order);

        return $result;
    }
}