<?
$MESS['MARKUP_LOG_SORT']            = 'Sort';
$MESS['MARKUP_LOG_SORT_AUTHOR']     = 'By author';
$MESS['MARKUP_LOG_SORT_MAIN_TITLE'] = 'By book';
$MESS['MARKUP_LOG_SORT_DATE']       = 'By date';
$MESS['MARKUP_LOG_RESET']           = 'Reset';

$MESS['MARKUP_LOG_TITLE']           = 'Book search';
$MESS['MARKUP_LOG_FROM']    = 'From';
$MESS['MARKUP_LOG_TO']      = 'to';
$MESS['MARKUP_LOG_TOTAL']   = 'Total find';
$MESS['MARKUP_LOG_AUTHOR']  = 'Author';
$MESS['MARKUP_LOG_BOOK']    = 'Book';
$MESS['MARKUP_LOG_WATCH']   = 'Watch';
$MESS['MARKUP_LOG_LOG']     = 'Log';

$MESS['MARKUP_LOG_STATUS_ADDED']         = 'Added';
$MESS['MARKUP_LOG_STATUS_CONSIDERATION'] = 'Consideration';
$MESS['MARKUP_LOG_STATUS_CONFIRMED']     = 'Confirmed';
$MESS['MARKUP_LOG_STATUS_DELETED']       = 'Deleted';