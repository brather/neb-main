<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<h2><?=GetMessage("SCANNING_TITLE");?></h2>
<!--Окно для предпросмотра
<div id="ui-widget-overlay" class="ui-widget-overlay ui-front dark"></div>
<div id="ui-dialog" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front no-close ajaxpopup" tabindex="-1" role="dialog" aria-describedby="ui-id-10" aria-labelledby="ui-id-11" style="height: auto; width: 100%; top: 0px; left: 0px; display: block; background: rgb(255, 255, 255);">
    <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
		<span id="ui-id-11" class="ui-dialog-title">&nbsp;</span>
		<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title="close">
			<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>
			<span class="ui-button-text">close</span>
		</button>
	</div>		
    <div id="iframe-result"></div>
</div>
-->
<div class="lk-table">
	<div class="lk-table__column" style="width:10%"></div>
	<div class="lk-table__column" style="width:20%"></div>
	<div class="lk-table__column" style="width:10%"></div>
	<div class="lk-table__column" style="width:10%"></div>
	<div class="lk-table__column" style="width:20%"></div>
	<div class="lk-table__column" style="width:20%"></div>
	<div class="lk-table__column" style="width:10%"></div>
	<ul class="lk-table__header">
        <li class="lk-table__header-kind"><a <?=SortingExalead("ID")?>><?=GetMessage("SCANNING_ORDER_ID");?></a></li>
        <li class="lk-table__header-kind"><a <?=SortingExalead("STARTDATE")?>><?=GetMessage("SCANNING_ORDER_DATE");?></a></li>
        <li class="lk-table__header-kind"><a <?=SortingExalead("IMAGECOUNT")?>><?=GetMessage("SCANNING_NUMBER_IMAGES");?></a></li>
        <li class="lk-table__header-kind"><a <?=SortingExalead("PRICE")?>><?=GetMessage("SCANNING_ORDER_AMOUNT");?></a></li>
        <li class="lk-table__header-kind"><a><?=GetMessage("SCANNING_PREVIEW");?></a></li>
        <li class="lk-table__header-kind"><a <?=SortingExalead("PAID")?>><?=GetMessage("SCANNING_STATUS");?></a></li>
        <li class="lk-table__header-kind"><a><?=GetMessage("SCANNING_ACTIONS");?></a></li>
    </ul>
	<?
		if(!empty($arResult['ITEMS']))
		{
			?>
			<section class="lk-table__body">
			<?
			foreach($arResult['ITEMS'] as $arItem)
            {
			?>
                <? if ($arItem['IMAGECOUNT'] > 0):?>
					<ul class="lk-table__row">
		                <li class="lk-table__col">
		                    <div class="rel plusico_wrap">
		                        <div><?=$arItem['ID']?></div>
		                        <?if ($arItem['LIBRARY']):?><div class="del scan-num-hint"><?=$arItem['LIBRARY']?></div><?endif;?>
		                    </div>
		                </li>
						<li class="lk-table__col"><?=$arItem['DATE']?></li>
						<li class="lk-table__col"><?=$arItem['IMAGECOUNT']?></li>
						<li class="lk-table__col"><?=$arItem['PRICE']*$arItem['IMAGECOUNT']?> <?=GetMessage("SCANNING_CURRENCY");?></li>
						<li class="lk-table__col">
							<span class="nowrap">
								<a id="preview_<?=$arItem['ID']?>" 
									href="#"
									_onClick="PreviewClick('preview_<?=$arItem['ID']?>')" 
									data-href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arItem['ID']?>&action=show"
									data-show-scan-order-details="<?=$arItem['ID']?>"
								>
									<?=GetMessage("SCANNING_SEE");?>
								</a>
							</span>
						</li>
						<li class="lk-table__col">
							<?
								if(empty($arItem['PAID']))
		                        {
									?><a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arItem['ID']?>&action=pay" id="<?=$arItem['ID']?>" class="formbutton middle payorderlink" data-show-scan-order-pay="<?=$arItem['ID']?>"><?=GetMessage("SCANNING_PAY");?></a>&nbsp;<span class="b-paytime"><?=GetMessage("SCANNING_PAY_UP");?>&nbsp;<?=$arItem['PAY_UP_DATE']?></span><?
		                        }
								else
								{
								?><span class="b-paid"><?=GetMessage("SCANNING_PAID");?></span><span class="b-paytime"><?=$arItem['PAID_DATE']?></span>
		                        <?
								}
							?>
						</li>
						<li class="lk-table__col">
							<a 
								href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arItem['ID']?>&action=delete_form" 
								data-delete-order="<?=$arItem['ID']?>" 
								data-item-id="<?=$arItem['ID']?>"
							>
								удалить
							</a><br/>
							<? if(!empty($arItem['PAID']) && count($arItem['IMAGECOUNT']) >= 1){?>
								<span style="white-space: nowrap;"><a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arItem['ID']?>&action=all_download" target="_blank" class="formbutton middle">скачать</a> <?=round($arItem['FILES_SIZE']/(1024*1024), 2)?> МБ</span>
							<? }?>
						</li>
					</ul>
                <?endif;?>
			<?
			}
		?>
		</section>
		<?
		}
	?>
	
	<div id="ui-WD"></div>
	<script>
		
	</script>
	
</div>