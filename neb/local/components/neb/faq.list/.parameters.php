<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(!CModule::IncludeModule("iblock")) return;
use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;
Loc::loadMessages(__FILE__);
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-" => " "));
$arIBlocks = array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID" => $arRequest["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch()) $arIBlocks[$arRes["ID"]] = $arRes["NAME"];
$arSorts = array("ASC"=>Loc::getMessage("T_IBLOCK_DESC_ASC"), "DESC"=>Loc::getMessage("T_IBLOCK_DESC_DESC"));

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("T_IBLOCK_DESC_LIST_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("T_IBLOCK_DESC_LIST_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => '={$arRequest["ID"]}',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
        ),
        "PAGE_TITLE" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => Loc::getMessage("TITLE_BLOCK_NAME"),
            "TYPE" => "TEXT",
            "DEFAULT" => Loc::getMessage("TITLE_BLOCK_NAME_VALUE")
        ),
        "SHOW_TITLE" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => Loc::getMessage("SHOW_TITLE_BLOCK_NAME"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => ''
        ),
        "PAGE_DESCRIPTION" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => Loc::getMessage("TITLE_BLOCK_DESCRIPTION"),
            "TYPE" => "TEXT",
            "DEFAULT" => Loc::getMessage("TITLE_BLOCK_DESCRIPTION_VALUE")
        ),
        "NAME_OF_PROJECT" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => Loc::getMessage("TITLE_BLOCK_NAME_PROJECT"),
            "TYPE" => "TEXT",
            "DEFAULT" => Loc::getMessage("TITLE_BLOCK_NAME_PROJECT_VALUE")
        ),
        "URL_FEEDBACK_PAGE" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => Loc::getMessage("TITLE_BLOCK_URL_FEEDBACK_PAGE"),
            "TYPE" => "TEXT",
            "DEFAULT" => "/faq/"
        )
    ),
);