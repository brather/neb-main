<?php

CBitrixComponent::includeComponentClass('rest:rest.server');

use \Bitrix\Main\Application,
    \Bitrix\Main\UserTable,
    \Bitrix\Main\Loader,
    \Bitrix\Iblock\IblockTable;

/**
 * Class RestHosts
 */
class RestHosts extends RestServer
{
    /**
     * @var array
     */
    protected $_arAccessGroups = ['operator', 'operator_wchz'];
    protected $_params         = ['action'=> null, 'token' => null, 'fingerprint' => null, 'host' => null, 'os' => null];
    protected $arUser          = [];

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams()
    {
        $arRequest = static::getRequest();

        /**
         * Леша попросил выпилить проверку токена, временно
         * unset($this->_params['token']);
         */
        foreach ($this->_params as $sParam => $sValue) {
            if ($arRequest['action'] == 'check' && in_array($sParam, ['host', 'os']))
                $this->_params[$sParam] = trim($arRequest[$sParam]);
            elseif (!isset($arRequest[$sParam]))
                $this->restError(403, 'Parameter "' . $sParam . '" is required!');
            elseif (empty($arRequest[$sParam]))
                $this->restError(403, 'Parameter "' . $sParam . '" is empty!');
            else
                $this->_params[$sParam] = trim($arRequest[$sParam]);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getAction()
    {
        /**
         * Лёша попросил выпилить проверку ВЧЗ
         * $this->checkWorkplace();
         */
        $this->switchAction();
        return 200;
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    protected static function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Проверка токена пользователя и принадлежность его к группам глобального оператора или оператора эчз
     */
    protected function checkUser() {

        $this->arUser = UserTable::getList([
            'filter' => ['ACTIVE' => 'Y', 'UF_TOKEN' => $this->_params['token']],
            'select' => ['ID', 'EMAIL', 'UF_LIBRARY', 'UF_WCHZ']
        ])->fetch();

        if (empty($this->arUser))
            $this->restError(401, GetMessage('USER_NOT_FOUND'));

        $obUser   = new nebUser($this->arUser['ID']);
        $arGroups = $obUser->getUserGroups();
        $arGroups = array_intersect($arGroups, $this->_arAccessGroups);

        if (empty($arGroups))
            $this->restError(403, 'Role access error!');
    }

    /**
     * Проверка IP пользователя на принадлежность какому-либо ЭЧЗ
     */
    protected function checkWorkplace() {

        $bAccess = (new NebWorkplaces())->checkAccess();

        if (!$bAccess)
            $this->restError(403, 'Request not from workplace!');
    }

    /**
     * Выбор действия
     *
     * @throws Exception
     */
    private function switchAction() {
        switch ($this->_params['action']) {
            case 'insert':
                $this->checkUser();
                $this->addTrustedMachine();
                break;
            case 'check':
                $this->checkTrustedMachine();
                break;
            default:
                $this->restError(403, 'Parameter "action" incorrect!');
        }
    }

    protected function addTrustedMachine() {

        if (!Loader::includeModule('iblock'))
            return false;

        $arIBlock = IblockTable::getList([
            "filter" => ['CODE' => IBLOCK_CODE_TRUSTED_MACHINES, 'ACTIVE' => 'Y'],
            "select" => ['ID'],
        ])->fetchRaw();

        if (empty($arIBlock['ID']))
            $this->restError(500, 'IBlock not found!');

        $obIBlockElement = new CIBlockElement();
        $arMachine = $obIBlockElement->GetList(
            [],
            [
                'IBLOCK_ID'            => $arIBlock['ID'],
                'PROPERTY_FINGERPRINT' => $this->_params['fingerprint']
            ],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'ACTIVE']
        )->Fetch();

        if (!empty($arMachine))
            $this->restError(409, 'Mashine is exists!');

        $arFields = [
            "IBLOCK_ID"         => $arIBlock['ID'],
            "IBLOCK_SECTION_ID" => false,
            "NAME"              => $this->_params['fingerprint'],
            "PROPERTY_VALUES"   => [
                'FINGERPRINT'  => $this->_params['fingerprint'],
                'HOST'         => $this->_params['host'],
                'OS'           => $this->_params['os'],
                'USER_ID'      => intval($this->arUser['ID']),
                'LIBRARY_ID'   => intval($this->arUser['UF_LIBRARY']),
                'WORKPLACE_ID' => intval($this->arUser['UF_WCHZ']),
            ],
            "ACTIVE"            => "Y"
        ];

        if ($iMashine = $obIBlockElement->Add($arFields)) {
            $this->arResult['success'] = 'true';
            $this->arResult['id'] = $iMashine;
        } else {
            $this->restError(500, $obIBlockElement->LAST_ERROR);
        }
    }

    protected function checkTrustedMachine() {

        if (!Loader::includeModule('iblock'))
            return false;

        $obIBlockElement = new CIBlockElement();
        $arMachine = $obIBlockElement->GetList(
            [],
            [
                'IBLOCK_CODE'          => IBLOCK_CODE_TRUSTED_MACHINES,
                'PROPERTY_FINGERPRINT' => $this->_params['fingerprint'],
                'PROPERTY_OS'          => $this->_params['os']
            ],
            false,
            false,
            [
                'ID', 'IBLOCK_ID', 'ACTIVE',
                'PROPERTY_LIBRARY_ID', 'PROPERTY_LIBRARY_ID.PROPERTY_LIBRARY_LINK',
                'PROPERTY_WORKPLACE_ID', 'PROPERTY_WORKPLACE_ID.NAME'
            ]
        )->Fetch();

        if (empty($arMachine['ID'])) {
            $this->arResult['result'] = 0;
        } elseif ($arMachine['ACTIVE'] == 'Y') {
            $this->arResult['result']         = 1;
            $this->arResult['library_id']     = intval($arMachine['PROPERTY_LIBRARY_ID_VALUE']);
            $this->arResult['neblib_id']      = intval($arMachine['PROPERTY_LIBRARY_ID_PROPERTY_LIBRARY_LINK_VALUE']);
            $this->arResult['workplace_id']   = intval($arMachine['PROPERTY_WORKPLACE_ID_VALUE']);
            $this->arResult['workplace_name'] = $arMachine['PROPERTY_WORKPLACE_ID_NAME'];
        }
        else {
            $this->arResult['result'] = 2;
        }
    }
}