<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$arRequest = $component->getRequest();

CJSCore::Init(array('date'));

?>

    <form action="<?=$arParams['FEEDBACK_URL']?>" class="expanded form-form feedback-form" method="get">
    	<div class="ext-search">
	        <input type="hidden" name="FEEDBACK[ACTION]" value="LIST" />

	        <div class="row">
	            <div class="form-group col-md-4">
	                <div class="row">
	                    <div class="col-sm-7 col-xs-4">
	                        <label for="ticket_id">
	                            <?=Loc::getMessage('FEEDBACK_LIST_TICKET_ID')?>
	                        </label>
	                    </div>
	                    <div class="col-sm-5 col-xs-8">
	                        <input type="text" id="ticket_id" value="<?=$arRequest['FEEDBACK']['ID']?>" name="FEEDBACK[ID]" class="form-control" />
	                    </div>
	                </div>
	            </div>
	            <div class="form-group col-md-4">
	                <div class="row">
	                    <div class="col-sm-7 col-xs-4">
	                        <label for="user_id">
	                            <?=Loc::getMessage('FEEDBACK_LIST_USER_ID')?>
	                        </label>
	                    </div>
	                    <div class="col-sm-5 col-xs-8">
	                        <input type="text" id="user_id" value="<?=$arRequest['FEEDBACK']['USER_ID']?>" name="FEEDBACK[USER_ID]" class="form-control" />
	                    </div>
	                </div>
	            </div>
	        </div>

	        <!-- <div class="nrf-fieldset-title"></div> -->
	        <div class="row">
	            <div class="col-sm-4 col-xs-12">
	                <label>
	                    <?=Loc::getMessage('FEEDBACK_LIST_DATE_REGISTER')?>
	                </label>
	            </div>
	            <div class="form-group col-sm-4 col-xs-6 from-to-grid">
	                <?//=Loc::getMessage('FEEDBACK_LIST_DATE_FROM')?>
	                <div class="form-group">
	                    <div class="input-group">
	                        <input
	                            type="text"
	                            value="<?=$arRequest['FEEDBACK']['REGISTER_DATE_FROM']?>"
	                            name="FEEDBACK[REGISTER_DATE_FROM]"
	                            id="register_date_from"
	                            class="form-control"
	                        />
	                        <span class="input-group-btn">
	                            <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'register_date_from', field: 'FEEDBACK[REGISTER_DATE_FROM]', form: '', bTime: false, value: ''});">
	                                <span class="glyphicon glyphicon-calendar"></span>
	                            </a>
	                        </span>
	                    </div>
	                </div>
	            </div>
	            <div class="form-group col-sm-4 col-xs-6">
	                <?//=Loc::getMessage('FEEDBACK_LIST_DATE_TO')?>
	                <div class="form-group">
	                    <div class="input-group">
	                        <input
	                            type="text"
	                            value="<?=$arRequest['FEEDBACK']['REGISTER_DATE_TO']?>"
	                            name="FEEDBACK[REGISTER_DATE_TO]"
	                            id="register_date_to"
	                            class="form-control"
	                        />
	                        <span class="input-group-btn">
	                            <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'register_date_to', field: 'FEEDBACK[REGISTER_DATE_TO]', form: '', bTime: false, value: ''});">
	                                <span class="glyphicon glyphicon-calendar"></span>
	                            </a>
	                        </span>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row">
	            <div class="col-sm-4 col-xs-12">
	                <label>
	                    <?=Loc::getMessage('FEEDBACK_LIST_DATE_CLOSE')?>
	                </label>
	            </div>
	            <div class="form-group col-sm-4 col-xs-6 from-to-grid">
	                <?//=Loc::getMessage('FEEDBACK_LIST_DATE_FROM')?>
	                <div class="form-group">
	                    <div class="input-group">
	                        <input
	                            type="text"
	                            value="<?=$arRequest['FEEDBACK']['CLOSE_DATE_FROM']?>"
	                            name="FEEDBACK[CLOSE_DATE_FROM]"
	                            id="close_date_from"
	                            class="form-control"
	                        />
	                        <span class="input-group-btn">
	                            <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'close_date_from', field: 'FEEDBACK[CLOSE_DATE_FROM]', form: '', bTime: false, value: ''});">
	                                <span class="glyphicon glyphicon-calendar"></span>
	                            </a>
	                        </span>
	                    </div>
	                </div>
	            </div>
	            <div class="form-group col-sm-4 col-xs-6">
	                <?//=Loc::getMessage('FEEDBACK_LIST_DATE_TO')?>
	                <div class="form-group">
	                    <div class="input-group">
	                        <input
	                            type="text"
	                            value="<?=$arRequest['FEEDBACK']['CLOSE_DATE_TO']?>"
	                            name="FEEDBACK[CLOSE_DATE_TO]"
	                            id="close_date_to"
	                            class="form-control"
	                        />
	                        <span class="input-group-btn">
	                            <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'close_date_to', field: 'FEEDBACK[CLOSE_DATE_TO]', form: '', bTime: false, value: ''});">
	                                <span class="glyphicon glyphicon-calendar"></span>
	                            </a>
	                        </span>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row">
	            <div class="form-group col-sm-4">
	                <?//=Loc::getMessage('FEEDBACK_LIST_THEME')?>
	                <select id="ticket_theme" class="form-control" name="FEEDBACK[THEME]" data-select-picker>
	                    <option value=""><?=Loc::getMessage('FEEDBACK_LIST_ALLTHEMES')?></option>
	                    <? foreach ($arResult['THEMES'] as $arItem): ?>
	                        <option value="<?=$arItem['VALUE']?>" <? if ($arItem['VALUE'] === $arRequest['FEEDBACK']['THEME']):
	                            ?> selected="selected"<? endif; ?>><?=$arItem['MESSAGE']?></option>
	                    <? endforeach; ?>
	                </select>
	            </div>
	            <div class="form-group col-sm-4">
	                <?//=Loc::getMessage('FEEDBACK_LIST_STATUS')?>
	                <select id="ticket_status" class="form-control" name="FEEDBACK[STATUS]" data-select-picker>
	                    <option value=""><?=Loc::getMessage('FEEDBACK_LIST_ALLSTATUSES')?></option>
	                    <? foreach ($arResult['STATUSES'] as $arItem): ?>
	                        <option value="<?=$arItem['ID']?>" <? if ($arRequest['FEEDBACK']['STATUS'] == $arItem['ID']):
	                            ?> selected="selected"<? endif; ?>><?=$arItem['TITLE']?></option>
	                    <? endforeach; ?>
	                </select>
	            </div>
	            <div class="form-group col-sm-4">
	                <?//=Loc::getMessage('FEEDBACK_LIST_EXECUTOR')?>
	                <select id="ticket_executor" class="form-control" name="FEEDBACK[EXECUTOR]" data-select-picker>
	                    <option value=""><?=Loc::getMessage('FEEDBACK_LIST_ALLEXECUTORS')?></option>
	                    <? foreach ($arResult['EXECUTORS'] as $arItem): ?>
	                        <option value="<?=$arItem['ID']?>" <? if ($arRequest['FEEDBACK']['EXECUTOR'] == $arItem['ID']):
	                            ?> selected="selected"<? endif; ?>><?=trim($arItem['LAST_NAME'].' '.$arItem['NAME'].' '.$arItem['SECOND_NAME'])?></option>
	                    <? endforeach; ?>
	                </select>
	            </div>
	        </div>

	        <div class="row">
	            <div class="col-sm-3 col-xs-4">
	                <label for="ticket_text">
	                    <?=Loc::getMessage('FEEDBACK_LIST_TEXT')?>
	                </label>
	            </div>
	            <div class="form-group col-sm-6 col-xs-8">
	                <input type="text" id="ticket_text" value="<?=$arRequest['FEEDBACK']['TEXT']?>" name="FEEDBACK[TEXT]" class="form-control" />
	            </div>
	            <div class="form-group col-sm-3 col-xs-12 text-right">
	                <input type="submit" class="btn btn-primary btn-lg" value="<?=Loc::getMessage('FEEDBACK_LIST_SHOW')?>" />
	            </div>
	        </div>
	    </div>
    </form>

<div>
	<? foreach($arResult['ERROR'] as $sError): ?>
		<?ShowError($sError);?>
	<? endforeach; ?>

	<? if (empty($arResult['ITEMS'])): ?>

		<p><?=Loc::getMessage('FEEDBACK_LIST_NOT_FOUND')?></p>

	<? else: ?>

		<div id="context" data-toggle="context" data-target="#context-menu" class="contexmenufix">
			<dl class="responsive-table-layout biblio-feedback" data-biblio-feedback>
				<dt>
					<span><a <?=SortingExalead("id")?>>№</a></span>
					<span>
						<a <?=SortingExalead("date_create")?>><?=Loc::getMessage('FEEDBACK_LIST_DATE_CREATE')?></a>/<br />
						<a <?=SortingExalead("timestamp")?>><?=Loc::getMessage('FEEDBACK_LIST_DATE_TSP')?></a>
					</span>
					<span>
						<a <?=SortingExalead("user_id")?>>ID</a>&nbsp;/<br />
						E-mail
					</span>
					<span><?=Loc::getMessage('FEEDBACK_LIST_THEME')?></span>
					<span><?=Loc::getMessage('FEEDBACK_LIST_TEXT')?></span>
					<span>
						<a <?=SortingExalead("status")?>><?=Loc::getMessage('FEEDBACK_LIST_STATUS')?></a>&nbsp;/<br />
						<?=Loc::getMessage('FEEDBACK_LIST_EXECUTOR')?>
					</span>
                    <span class="hidden">Ресурс</span>
				</dt>

				<? foreach ($arResult['ITEMS'] as $arItem): ?>
					<dd data-feedback-item data-item-number="<?=$arItem["ID"] ?>" class="<?
						// наполнение контекстного меню
						$sStatus = $arResult['STATUSES'][$arItem["STATUS_ID"]]['DESCRIPTION'];
						if ('REGISTERED' === $sStatus)
							echo ' feedback-registered';
						if ('REGISTERED' === $sStatus || 'PROCESSED' === $sStatus)
							echo ' feedback-reply feedback-reject';
						if ('CLOSED' === $sStatus)
							echo ' feedback-closed';
						echo ' feedback-history';
					?>">
                        <span><?=$arItem["ID"] ?></span>
						<span><?=$arItem["date_from"]?> <?=$arItem["TIMESTAMP_X"]?></span>
						<span>
							<? if ($arItem["USER_ID"]>0):?>
								<i><?=$arItem['USER']["ID"]?></i>
								<i><a href="mailto:<?=$arItem['USER']["EMAIL"]?>"><?=$arItem['USER']["EMAIL"]?></i></a>
							<? else: ?>
								<i><?=GetMessage("FORM_NOT_REGISTERED")?></i>
								<i><a href="mailto:<?=$arItem['ANSWER']['FB_EMAIL']['USER_TEXT']?>"><?echo
									$arItem['ANSWER']['FB_EMAIL']['USER_TEXT']?></i></a>
							<? endif; ?>
						</span>
						<span>
							<?=current(explode('/', $arItem['ANSWER']['FB_THEME']['ANSWER_TEXT']));?>
						</span>
						<span>
							<strong data-view-detail-message>
								<?=$arItem['ANSWER']['FB_TEXT']['USER_TEXT'];?>
							</strong>
						</span>
						<span>
							<div data-status><?=$arItem["STATUS_TITLE"]?></div>
							<div data-executor-text><?$arEx = $arResult['EXECUTORS'][$arItem['ANSWER']['FB_USER_ID']['USER_TEXT']]?></div>
							<div data-executor-name><?=$arEx['LAST_NAME'].' '.$arEx['NAME'].' '.$arEx['SECOND_NAME']?></div>
						</span>
                        <span class="hidden"><a href="/catalog/<?= $arItem['ANSWER']['FB_BOOK_ID']['USER_TEXT']?>/" target="_blank"><?= $arItem['ANSWER']['FB_BOOK_ID']['USER_TEXT']?></a></span>
					</dd>
				<? endforeach;  ?>
			</dl>
		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.pagenavigation",
			"",
			array(
				"NAV_OBJECT" => $arResult['NAV'],
				"SEF_MODE" => "Y",
			),
			false
		);?>

	<? endif; ?>
</div>