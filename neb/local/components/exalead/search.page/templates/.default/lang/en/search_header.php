<?php

$MESS['CONVERTED_TO_ALL'] = 'Did you leave the search bar empty and we have brought all the products that meet other search filters'; // Вы оставили строку поиска пустой, и мы вывели все произведения, удовлетворяющие прочим фильтрам поиска.
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'Documents';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'Document';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'Documents';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'] = 'found';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_RESULT_5'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_RESULT_1'] = 'found';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_RESULT_2'] = 'found';

$MESS['LIB_SEARCH_PAGE_FULL_SEARCH'] = "Search for further publications produced by the texts."; // Поиск дополнительно произведен по текстам изданий.
$MESS['MORE_RESULTS'] = 'More results for your query can be found at'; // Больше результатов по вашему запросу можно найти на сайте
$MESS['ARCHIVE_OF_THE_RUSSIAN_FEDERATION'] = 'State Archive of the Russian Federation'; //Государственного архива Российской Федерации
$MESS['LAYOUT_CHANGED'] = 'Исправлена раскладка клавиатуры в';

$MESS['SEARCH_STRICT'] = 'Show search results <a href="#LINK#">by an exact phrase</a>';
$MESS['SEARCH_STRICT_RESULTS'] = 'results by an exact phrase';

$MESS['SEARCH_BY_ORIGINAL_QUERY'] = 'Search by original query'; //Искать по оригинальному запросу запросу
$MESS['SEARCH_IN_RUSSIAN'] = 'Search in Russian'; // Искать на русском
$MESS['SEARCH_IN_ENGLISH'] = 'Search in English'; //Искать на английском
$MESS['SEARCH_IN_DEUTSCH'] = 'Suche auf Deutsch'; //Искать на немецком
$MESS['SAVE_QUERY'] = 'Save query'; //Сохранить поисковый запрос
$MESS['SAVED'] = 'Saved';