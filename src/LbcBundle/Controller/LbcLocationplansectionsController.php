<?php
/**
 * User: agolodkov
 * Date: 18.08.2016
 * Time: 12:41
 */

namespace LbcBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use LbcBundle\Entity\LbcLocationplansections;

/**
 * @DI\Service("app.locationplansections", public=true, scope="request")
 */
class LbcLocationplansectionsController extends AbstractCategoryController
{
    /**
     * @DI\Inject("app.api.locationplansections")
     * @var \LbcBundle\Controller\Api\LbcLocationplansectionsController
     */
    public $api;

    /**
     * @DI\Inject("app.api.locationplans")
     * @var \LbcBundle\Controller\Api\LbcLocationplansController
     */
    public $ownerApi;

    /**
     * @param LbcLocationplansections $entity
     */
    public function checkEntityActions($entity)
    {
        parent::checkEntityActions($entity);
        $em = $this->doctrine->getManager();
        foreach ($entity->getActions() as $key => $action) {
            switch ($action['code']) {
                case 'division-images':
                    if (!$em->getRepository('LbcBundle:LbcLocationplansectionimages')->findOneBy(
                        [
                            'locationplansectionid' => $entity->getId(),
                        ]
                    )
                    ) {
                        $entity->removeAction($key);
                    }
                    break;
            }
        }
    }

    /**
     * @return array
     */
    protected function _getRouterMaps()
    {
        return [
            'api_category_detail' => 'api_bbk_locationplansection_detail',
            'api_category_update' => 'api_bbk_locationplansection_update',
            'api_category_images' => 'api_bbk_locationplansection_images',
            'category'            => 'bbk_locationplansections',
            'category_index'      => 'bbk_locationplansections_index',
        ];
    }

    protected function _getIndexName()
    {
        return 'Разделы планов расположения';
    }

    /**
     * @return array
     */
    protected function _getActionParams()
    {
        return [
            [
                'name' => 'Скан разелов плана',
                'code' => 'division-images',
            ],
        ];
    }
}