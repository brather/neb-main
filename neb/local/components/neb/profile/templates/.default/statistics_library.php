<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

CJSCore::Init(array('date'));

$APPLICATION->SetTitle('Статистика библиотеки');
CModule::IncludeModule('iblock');

$arUser = CUser::GetByID($USER->GetID())->Fetch();

$library = \Bitrix\NotaExt\Iblock\Element::getList(
    array('IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ID' => $arUser['UF_LIBRARY']),
    1,
    array(
        'ID',
        'PROPERTY_EXALEAD_FILTER',
        'skip_other',
        'SHOW_COUNTER',
        'PROPERTY_LIBRARY_LINK'
    )
);
$arResult['library'] = $library['ITEM'];

Loc::loadMessages(__FILE__);
$periodPart = 86400;
$dateTo = time();
$dateFrom = $dateTo - $periodPart * 30;
if (isset($_REQUEST['dateFrom']) && !empty($_REQUEST['dateFrom'])) {
    $dateFrom = strtotime($_REQUEST['dateFrom']);
}
if (isset($_REQUEST['dateTo']) && !empty($_REQUEST['dateTo'])) {
    $dateTo = strtotime($_REQUEST['dateTo']);
}

$yearSeconds = (85400 * 365);
if (($dateTo - $dateFrom) > $yearSeconds) {
    ShowError('Диапазон не должен быть больше 365 календарных дней');
    $dateFrom = $dateTo - ($yearSeconds - 86400);
}
$beginYesterDay = (date('Y-m-d') === date('Y-m-d', $dateTo))
    ? ($dateTo - 86400)
    : $dateTo;
$graphs = [
    'neb_library_books_count'                       => [
        'title'       => 'Количество дедуплицированных карточек изданий библиотеки',
        'date'        => [],
        'meta'        => false,
        'dateTo'      => $beginYesterDay,
    ],
    'neb_library_books_digitized_count'             => [
        'title'  => 'Количество дедуплицированных электронных копий изданий библиотеки',
        'date'   => [],
        'meta'   => false,
        'dateTo' => $beginYesterDay,
    ],
    /*'neb_book_library_document_view'                => [
        'title' => 'Количество просмотров изданий библиотеки (открыта хотя-бы одна страница издания)',
        'date'  => [],
    ],
    'neb_book_library_downloads'                    => [
        'title' => 'Количество скачанных изданий библиотеки через портал',
        'date'  => [],
    ],*/
    'neb_book_library_page_view'                    => [
        'title'       => 'Количество просмотренных страниц изданий',
        'date'        => [],
    ],
    'neb_library_search_queries'                    => [
        'title' => 'Количество поисковых запросов с применением фильтра по библиотеке',
        'date'  => [],
    ],
    'neb_library_user_registers'                    => [
        'title'  => 'Количество зарегистрированных пользователей библиотеки',
        'date'   => [],
        'meta'   => false,
        'dateTo' => $beginYesterDay,
    ],
    'neb_library_service_cnt_book_downloads'        => [
        'title'       => 'Количество скачанных изданий библиотеки, хранящихся в НЭБ',
        'description' => '',
        'date'        => [],
        'meta'        => false,
        'dateTo'      => $beginYesterDay,
    ],
    'neb_library_service_cnt_book_views'            => [
        'title'       => 'Количество просмотров изданий библиотеки (открыта хотя-бы одна страница издания)',
        'description' => '',
        'date'        => [],
        'meta'        => false,
        'dateTo'      => $beginYesterDay,
    ],
    'neb_library_service_cnt_book_downloads_unique' => [
        'title'       => 'Количество скачанных изданий, без учета повторных скачиваний',
        'description' => '',
        'date'        => [],
        'meta'        => false,
        'dateTo'      => $beginYesterDay,
    ],
    'neb_library_service_cnt_book_views_unique'     => [
        'title'       => 'Количество уникальных просмотров изданий библиотеки (открыта хотя-бы одна страница издания)',
        'description' => '',
        'date'        => [],
        'meta'        => false,
        'dateTo'      => $beginYesterDay,
    ],
];
foreach ($graphs as $code => &$graph) {
    $query = http_build_query(
        [
            'report'     => $code,
            'dateFrom'   => date('Y-m-d', $dateFrom),
            'dateTo'     => date(
                'Y-m-d',
                isset($graph['dateTo'])
                    ? $graph['dateTo']
                    : $dateTo
            ),
            'library_id' => (integer)$arResult['library']['ID'],
        ]
    );
    if (isset($graph['meta']) && true !== $graph['meta']) {
        $graph['meta'] = false;
    } else {
        $graph['meta'] = true;
    }
    $token = COption::GetOptionString('neb.main', 'stats_reader_token');
    $host = str_replace(
        ['http:', 'http2:'], '',
        COption::GetOptionString('neb.main', 'stats_host')
    );
    $graph['url'] = $host . 'reports?'
        . 'access-token=' . $token . '&' . $query;
    $graph['exportUrl'] = $host . 'exports?'
        . 'access-token=' . $token . '&' . $query . '&periodItem=day&name=' . $graph['title'];
}
unset($graph);
?>
<div class="iblock b-lib_fulldescr">
    <div class='b-add_digital js_digital'>
        <form
            action="<?= $APPLICATION->GetCurPage() ?>" method="GET"
            name="form1" class="form-inline">

            C
            <div class="form-group">
                <div class="input-group">                    
                    <input type="text" id="date-from" name="dateFrom" size="10"
                     value="<?= ConvertTimeStamp($dateFrom) ?>"
                     class="form-control">
                    <span class="input-group-btn">
                        <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'date-from', field: 'date-from', form: '', bTime: false, value: ''});">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </a>
                    </span>
                </div>
            </div>
            по 
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="dateTo" id="date-to" size="10"
                      value="<?= ConvertTimeStamp($dateTo) ?>"
                      class="form-control">
                    <span class="input-group-btn">
                        <a id="calendarlabel" class="btn btn-default" onclick="BX.calendar({node: 'date-to', field: 'date-to', form: '', bTime: false, value: ''});">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </a>
                    </span>
                </div>
            </div>



            <input type='hidden'
                   name='LIBRARY_ID'>
            <input type="submit" class="btn btn-primary"
                   value="Применить"
                   style='float:none;'>
        </form>
        <br></div>
</div>
<hr>
<ul class="uleditstat">
    <? foreach ($graphs as $code => $graph) { ?>
        <li class="active-parent-li"><a href="" class="active-parent">
                <?= $graph['title'] ?><span class="icon"></span>
            </a>
            <ul class="submenu2" style="display: none;">
                <li>
                    <span
                        class="stat-description"><?= $graph['description'] ?></span>

                    <div data-chart-type="line"
                         style="min-width: 310px; height: 400px; margin: 0 auto"
                         data-chart-data-url="<?= $graph['url'] ?>"
                         data-chart-show-meta="<?= $graph['meta'] ?>"
                        >
                    </div>
                    <a style="position:relative; z-index: 100;" target="_blank"
                       href="<?= $graph['exportUrl'] ?>">
                        <img
                            src="/local/templates/adaptive/img/file-icons/excel.png"/>
                    </a>
                </li>
            </ul>
        </li>
    <? } ?>
</ul>
