<?php
/**
 * User: agolodkov
 * Date: 12.05.2016
 * Time: 10:49
 */

namespace AppBundle\Command;

use Neb\Main\Stat\StatisticAggregator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadLibraryStatsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stats:libraries:load')
            ->setDescription('Load library statistics')
            ->addArgument(
                'date-from',
                InputArgument::OPTIONAL,
                'load date from'
            )
            ->addArgument(
                'date-to',
                InputArgument::OPTIONAL,
                'load date to'
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);

        $this->getContainer()
            ->get('bitrix')
            ->requireProlog(
                $this->getContainer()
                    ->get('kernel')
                    ->getRootDir()
            );;

        $parameters = [];
        $dateFrom = $input->getArgument('date-from');
        $dateTo = $input->getArgument('date-to');
        if (!empty($dateFrom)) {
            $parameters['dateFrom'] = $dateFrom;
        }
        if (!empty($dateTo)) {
            $parameters['dateTo'] = $dateTo;
        }

        $output->writeln(
            'Start loading statistics('
            . ($dateFrom ? (' dateFrom: ' . $dateFrom) : '')
            . ($dateTo ? (' dateTo: ' . $dateTo) : '')
            . ')'
        );

        StatisticAggregator::loadLibrariesStatistics($parameters);


        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}