<?php
/**
 * User: agolodkov
 * Date: 09.09.2016
 * Time: 10:53
 */

namespace BitrixBundle\Traits;


use BitrixBundle\DependencyInjection\BitrixHelper;

trait BitrixHelperPropertyTrait
{
    /**
     * @var BitrixHelper
     */
    protected $_bitrix;

    /**
     * @return BitrixHelper
     */
    public function getBitrix()
    {
        return $this->_bitrix;
    }

    /**
     * @param BitrixHelper $bitrix
     */
    public function setBitrix($bitrix)
    {
        $this->_bitrix = $bitrix;
    }
}