<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));
?>
<div id="upload-dialog-1" class="upload-dialog-1">
	<div class="button-upload">
		<input type="button" id="detail-gallery-upload-button-files" name="upload-button-files" value="<?=Loc::getMessage('EDBG_UPLOAD_BUTTON');?>"/>
		<input type="button" id="detail-gallery-save-button" name="save-button" class="hidden" value="<?=Loc::getMessage('EDBG_SAVE_BUTTON');?>"/>
	</div>
	<div class="message-center"><?=Loc::getMessage('EDBG_DRAG_FILES');?></div>
	<div class="items-block hidden">
		<form action="/local/tools/notaext/detailGallery/saveimg.php" method="post" class="dg-form" name="dg-form-name">
			<?=bitrix_sessid_post()?>
			<div class="basic-fields">
				<div class="tags-field">
					<div><?=Loc::getMessage('EDBG_COMMON_TAGS');?></div>
					<select class="js-dg-tags form-control" name="basic-tags-all[]" id="basic-tags-all" multiple="multiple"></select>
				</div>
			</div>
			<div class="items-list">

			</div>
		</form>
	</div>
</div>

<div style="display: none;" id="container_<?=$arParams['RAND_STR']?>" class="pl_button"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>

<script type="text/javascript">
	$(function() {
		/* Upload tab */
		$('#detail-gallery-save-button').click(function() {
			var requiredError = false; 
			$( "#upload-dialog-1 .items-list input:text.required, #upload-dialog-1 .items-list textarea.required, #upload-dialog-1 .items-list select.required" ).each(function() {
				if($(this).hasClass('js-tags'))
				{
					if($(this).find('option:selected').length <= 0)
					{
						$(this).closest('span').find('.select2-selection--multiple').addClass('required-error');
						requiredError = true; 
					}
					else
						$(this).closest('span').find('.select2-selection--multiple').removeClass('required-error');
				}
				else
				{
					if($(this).val().length <= 0)
					{
						$(this).addClass('required-error');
						requiredError = true;
					}
					else
						$(this).removeClass('required-error');
				}
			});

			if(requiredError === true)
			{
				alert('<?=Loc::getMessage('EDBG_REQUIRED_ERROR');?>');
				return false;
			}
			
			var form = $('form.dg-form');
			
			BX.showWait();

			$.ajax({
				type: $(form).attr('method'),
				url: $(form).attr('action'),
				data: $(form).serialize(),
				dataType: 'json',
				cache: false
			})
			.done(function( data ) 
				{
					BX.closeWait();
					removeAllItems();
					initEditTab(data.result);
				}
			);
		});

		
		$( "body" ).off('change','.img-fields .required');
		$( "body" ).on('change','.img-fields .required', function(e){
			if($(this).hasClass('js-tags'))
			{
				if($(this).find('option:selected').length > 0)
					$(this).closest('span').find('.select2-selection--multiple').removeClass('required-error'); 
			}
			else
				$(this).removeClass('required-error');
		});
	
		initSelect2Tags($(".js-dg-tags"));
	
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'detail-gallery-upload-button-files',
				drop_element: "upload-dialog-1",
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'), 
				unique_names: true,
				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '1000kb',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>',
					aTHUMBNAIL_HEIGHT : <?=$arParams['THUMBNAIL_HEIGHT']?>,
					aTHUMBNAIL_WIDTH : <?=$arParams['THUMBNAIL_WIDTH']?>

				},
				multi_selection: true,
				init: {

					FilesAdded: function(up, files) {
						$('#detail-gallery-save-button').addClass('hidden');
						$('.upload-dialog-1 .message-center').addClass('hidden');
						$('.upload-dialog-1 .items-block').removeClass('hidden');

						$.each( files, function( key, file ) {
							var item_html = '<div class="item" id="'+file.id+'">'+
											   '<input type="hidden" name="bgitems[]" value="'+file.id+'"/>'+
											   '<input type="hidden" name="bgitemsname['+file.id+']" value="'+file.name+'"/>'+
											   '<input type="hidden" class="originalfile" name="originalfile['+file.id+']" value=""/>'+
											   '<input type="hidden" class="thumbnailfile" name="thumbnailfile['+file.id+']" value=""/>'+
											   '<div class="img-block">'+
													'<div class="pogress">0%</div>'+
													'<img class="hidden" src="">'+
													'<div class="delete hidden" title="<?=Loc::getMessage('EDBG_DELETE_IMG_TITLE');?>"><?=Loc::getMessage('EDBG_DELETE_IMG');?></div>'+
											   '</div>'+
												'<div class="img-fields">'+
													'<div class="field"><span class="label">Caption</span><span><textarea class="required" name="PREVIEW_TEXT['+file.id+']"></textarea></span></div>'+
													'<div class="field"><span class="label">Credit</span><span><input class="required" type="text" name="CREDIT['+file.id+']" value=""/></span></div>'+
													'<div class="field"><span class="label">Tags</span><span><select class="js-tags required" multiple="multiple" id="tags_'+file.id+'" name="DG_TAGS['+file.id+'][]"></select></span></div>'+
													'<div class="field"><span class="label">Licence</span><span><input class="required" type="text" name="LICENCE['+file.id+']" value=""/></span></div>'+
													'<div class="field"><span class="label">Licence expiration</span><span><input class="required date" type="text" name="LICENCE_EXPIRATION['+file.id+']" value=""/><span class="adm-calendar-icon" onclick="BX.calendar({node:this, field:\'LICENCE_EXPIRATION['+file.id+']\', form: \'dg-form-name\', bTime: false, bHideTime: true});" title="<?=Loc::getMessage('EDBG_ICO_DATE');?>"></span></span></div>'+
												'</div>'+
											'</div>';
											
							$('.upload-dialog-1 .items-block .items-list').append(item_html);
							initSelect2Tags($('#tags_'+file.id));
						});
						up.start();
					},
					FileUploaded: function(up, file, response) {
						var result = response.response;
						
						if (result) {
							var obResponse = JSON.parse(result);
							$('#'+file.id+' .img-block .pogress').addClass('hidden');
							$('#'+file.id+' .img-block img').attr('src', obResponse.thumbnail);
							$('#'+file.id+' .img-block img').removeClass('hidden');
							$('#'+file.id+' .img-block .delete').removeClass('hidden');
							$('#'+file.id+' input.originalfile').val(obResponse.file);
							$('#'+file.id+' input.thumbnailfile').val(obResponse.thumbnail);
						}
					},
					UploadProgress: function(up, file) {
						$('#'+file.id+' .img-block .pogress').text(file.percent+'%');
					},
					UploadComplete: function(up, file) {
						$('#detail-gallery-save-button').removeClass('hidden');
					},
					Error: function(up, err) {
						/*console.log(err);*/
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
					}
				}
			}
		);

		uploader_<?=$arParams['RAND_STR']?>.init();
		
		$( "body" ).off('click','#upload-dialog-1 .img-block .delete');
		$( "body" ).on('click','#upload-dialog-1 .img-block .delete', function(e){
			e.preventDefault();
			if (confirm("<?=Loc::getMessage('EDBG_DELETE_IMG_CONFIRM');?>")) {
				uploader_<?=$arParams['RAND_STR']?>.removeFile($(this).closest('div.item').attr('id'));
				$(this).closest('div.item').remove();
			}

			if($('#upload-dialog-1 .items-block .item').length <=0)
			{
				$('#detail-gallery-save-button').addClass('hidden');
				$('#upload-dialog-1 .items-block').addClass('hidden');
				$('#upload-dialog-1 .message-center').removeClass('hidden');
			}
		});
		
		function removeAllItems()
		{
			$( "#upload-dialog-1 .items-block .item" ).each(function() {
				uploader_<?=$arParams['RAND_STR']?>.removeFile($(this).attr('id'));
				$(this).remove();

			});

			$('#detail-gallery-save-button').addClass('hidden');
			$('#upload-dialog-1 .items-block').addClass('hidden');
			$('#upload-dialog-1 .message-center').removeClass('hidden');
		}
		
	});
</script>