<?

/**
 * Метаинформация о сущностях для отчетов
 * Class TblEntities
 */

use \Bitrix\Main\Application;

class TblEntities
{
    /**
     * Доступ к конструктору отчетов есть только у админа и оператора
     */
    public static function checkAccess() {
        return (new nebUser())->hasGroup([UGROUP_ADMIN, UGROUP_OPERATOR_CODE, 'operator_report_constructor']);
    }

    /**
     * Получение скоращенного описания типов сущностей
     *
     * @param int $id
     * @return array
     */
    public static function getEntities($id = 0)
    {
        $id = intval($id);
        $arResult = [];

		$strSql = 'SELECT * FROM tblEntities';
		if ($id > 0) {
            $strSql .= " WHERE Id=" . $id;
        }
        $results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[$row['Id']] = $row['Description'];
		}
		
		return $arResult;
    }

    /**
     * Получение расширенного описания типов сущностей
     *
     * @param int $id
     * @return array
     */
    public static function getEntitiesExtend($id = 0)
    {
        $id = intval($id);
        $arResult = [];

        $strSql = '
SELECT
  e.Id,
  e.Description,
  et.Name as Type
FROM tblEntities as e
JOIN tblEntityTypes as et on et.Id = e.IdType';
        if ($id > 0) {
            $strSql .= " WHERE e.IdType=" . $id;
        }
        $results = Application::getConnection()->query($strSql);
        while ($row = $results->fetch()) {
            $arResult[$row['Id']] = $row;
        }

        return $arResult;
    }

    public static function getEntityFields($entity)
    {
        $arResult = [];

		if ($entity > 0){
			$strSql = '	SELECT ef.*, eft.Name as FieldType 
						FROM tblEntityFields ef 
						LEFT JOIN tblEntityFieldTypes eft 
						ON   ef.IdEntityFieldType = eft.Id 
						WHERE IdEntity='.$entity.';';
			$results = Application::getConnection()->query($strSql);
			while ($row = $results->fetch()) {
                $arResult[$row['Id']] = $row;
			}
		}

		return $arResult;
    }
	
	public static function getField($id)
    {
        $arResult = [];

		if ($id > 0){
			$strSql = 'SELECT * FROM tblEntityFields WHERE Id='.$id.';';
			$results = Application::getConnection()->query($strSql);
			if ($row = $results->fetch()) {
				$res_entity = self::getEntities($row['IdEntity']);
				$row['EntityName'] = $res_entity[$row['IdEntity']];
                $arResult = $row;
			}
		}

		return $arResult;
    }
	
	public static function getAgregateType()
    {
        $arResult = [];

		$strSql = 'SELECT * FROM tblReportAgregateTypes;';
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[$row['Id']] = $row['Description'];
		}

		return $arResult;
    }
	
	public static function getCompareType()
    {
        $arResult = [];

		$strSql = 'SELECT * FROM tblReportCompareTypes;';
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[$row['Id']] = $row['Name'];
        }

		return $arResult;
    }
	
	public static function getOrderType()
    {
        $arResult = [];

		$strSql = 'SELECT * FROM tblReportOrderTypes;';
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch())
            $arResult[$row['id']] = $row['Description'];

		return $arResult;
    }
	
	public static function getEntityRelations($massEntities)
    {
        $arResult = [];
		$strSql = '	SELECT er.Id, er.Description, ert.Name, ef1.Description field1, ef2.Description field2, e1.Description tbl1, e2.Description tbl2, e1.id tblid1, e2.id tblid2
					FROM tblEntityRelations er
					JOIN tblEntityFields ef1 ON er.IdField11 = ef1.Id 
					JOIN tblEntityFields ef2 ON er.IdField21 = ef2.Id
					JOIN tblEntities e1 ON e1.Id = ef1.IdEntity
					JOIN tblEntities e2 ON e2.Id = ef2.IdEntity
					JOIN tblEntityRelationType ert ON ert.Id = er.IdEntityRelationType
					WHERE ef1.IdEntity in ('.implode(" , ", $massEntities).') AND ef2.IdEntity in ('.implode(" , ", $massEntities).');';
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult['join'][] = $row;
            $arResult['entities'][] = $row['tblid1'];
            $arResult['entities'][] = $row['tblid2'];
		}
		if ($arResult['entities']) {
            $arResult['entities'] = array_unique($arResult['entities']);
        }

		return $arResult;
    }
	
	public static function getRelationsId($join)
    {
        $arResult = [];

		$strSql = "	SELECT er.Id, er.Description,   e1.id tblid1, e2.id tblid2
					FROM tblEntityRelations er
					JOIN tblEntityFields ef1 ON er.IdField11 = ef1.Id 
					JOIN tblEntityFields ef2 ON er.IdField21 = ef2.Id
					JOIN tblEntities e1 ON e1.Id = ef1.IdEntity
					JOIN tblEntities e2 ON e2.Id = ef2.IdEntity
					WHERE er.Id IN (".implode(" , ", $join).") ";
				
		$results = Application::getConnection()->query($strSql);
		while ($row = $results->fetch()) {
            $arResult[$row['tblid1']] = $row['tblid1'];
            $arResult[$row['tblid2']] = $row['tblid2'];
		}

		return $arResult;
	}
	
	public static function getFieldType($field)
	{
        $arResult = [];

		$strSql = '	SELECT eft.Name 
					FROM tblEntityFields ef 
					LEFT JOIN tblEntityFieldTypes eft ON ef.IdEntityFieldType = eft.Id
					WHERE ef.Id = '.$field.';';

		$results = Application::getConnection()->query($strSql);
		if ($row = $results->fetch()) {
            $arResult = $row['Name'];
        }

		return $arResult;
	}
}