<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 17:35
 */

namespace LbcBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use LbcBundle\Entity\AbstractLbcCategory;
use LbcBundle\Entity\LbcLocationplans;


/**
 * @DI\Service("app.locationplans", public=true, scope="request")
 */
class LbcLocationplansController extends AbstractCategoryController
{

    /**
     * @DI\Inject("app.api.locationplans")
     * @var \LbcBundle\Controller\Api\LbcLocationplansController
     */
    public $api;

    /**
     * @DI\Inject("app.api.maindivision")
     * @var \LbcBundle\Controller\Api\LbcMaindivisionController
     */
    public $ownerApi;

    /**
     * @param LbcLocationplans $entity
     */
    public function checkEntityActions($entity)
    {
        parent::checkEntityActions($entity);
        $em = $this->doctrine->getManager();
        foreach ($entity->getActions() as $key => $action) {
            switch ($action['code']) {
                case 'division-images':
                    if (!$em->getRepository('LbcBundle:LbcLocationplanimages')->findOneBy(
                        [
                            'locationplanid' => $entity->getId(),
                        ]
                    )
                    ) {
                        $entity->removeAction($key);
                    }
                    break;
                case 'link-locationplansections':
                    if (!$em->getRepository('LbcBundle:LbcLocationplansections')->findOneBy(
                        [
                            'locationplanid' => $entity->getId(),
                        ]
                    )
                    ) {
                        $entity->removeAction($key);
                    }
                    break;
            }
        }
    }

    /**
     * @return array
     */
    protected function _getRouterMaps()
    {
        return [
            'api_category_detail' => 'api_bbk_locationplan_detail',
            'api_category_update' => 'api_bbk_locationplan_update',
            'api_category_images' => 'api_bbk_locationplanimages',
            'category'            => 'bbk_locationplan',
            'category_index'      => 'bbk_locationplan_index',
        ];
    }

    protected function _getIndexName()
    {
        return 'Планы расположения';
    }

    /**
     * @return array
     */
    protected function _getActionParams()
    {
        return [
            [
                'name' => 'Скан плана расположения',
                'code' => 'division-images',
            ],
            [
                'name'  => 'Список разделов плана',
                'code'  => 'link-locationplansections',
                'link'  => true,
                'route' => 'bbk_locationplan_locationplansections',
            ],
        ];
    }
}
