<?php
/** @var array $data */
/**
 * @param string $template
 * @param string $parts
 */
$render = function ($template, $parts) use ($view, $data) {
    /** @var \Symfony\Bundle\FrameworkBundle\Templating\PhpEngine $view */
    /** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
    /** @var \LbcBundle\DependencyInjection\MustacheEngine $mustache */
    /** @var \Symfony\Bundle\FrameworkBundle\Templating\GlobalVariables $app */
    $slots = $view['slots'];
    $mustache = $view['mustache_engine'];
    $mustache->setPartial($parts);
    $view->extend('::base.html.php');
    $slots->set(
        'body',
        $mustache->render($template, $data)
    );
};

