<?

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Data\Cache,
    \Bitrix\Main\GroupTable,
    \Bitrix\Main\Entity\ReferenceField,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\UserTable,
    \Bitrix\Main\UserGroupTable,
    \Bitrix\Main\Config\Option,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\NotaExt\NPHPCacheExt,

    \Neb\Main\Helper\IpHelper,
    \Nota\Journal\J;

class nebUser extends CUser
{
    const REGISTER_TYPE_SIMPLE = 37;
    const REGISTER_TYPE_FULL = 38;
    const REGISTER_TYPE_RGB = 39;
    const REGISTER_TYPE_ESIA = 40;
    const REGISTER_TYPE_RH = 41;
    const REGISTER_TYPE_RH_ENTITY = 245;

    const USER_STATUS_VERIFIED = 4;
    const USER_STATUS_REGISTERED = 42;
    const USER_STATUS_VIP = 43;

    const USER_STATUS_BLOCKED = 246;
    const USER_STATUS_EXPIRED = 247;

    const WORKPLACE_IPS = 'PROPERTY_24';

    const GROUP_OPERATOR_NEB = 13;
    const GROUP_OPERATOR_WCHZ = 14;

    const ERROR_NOT_FULL_REGISTER = 100;

    const ANONYMOUS_LOGIN = 'anonymous@rusneb.ru'; // логин анонимного читателя ВЧЗ

    const ELAR_IP = ['213.208.168.3', '84.47.187.80'];

    public $USER_ID = 0;
    public $arUserGroups = [];
    public $role = 'user';
    public $EICHB = '';
    public $error = '';
    protected $_status;

    protected static $_arUserRegisterTypes;
    protected $_userRegisterTypeId;
    protected static $_integrityGroupFields
        = array(
            'min'         => array(
                'EMAIL'     => true,
                'NAME'      => true,
                'LAST_NAME' => true,
            ),
            'full'        => array(
                'EMAIL'               => true,
                'NAME'                => true,
                'LAST_NAME'           => true,
                'PERSONAL_BIRTHDAY'   => true,
                'PERSONAL_GENDER'     => true,
                'PERSONAL_ZIP'        => true,
                'PERSONAL_CITY'       => true,
                'PERSONAL_STREET'     => true,
                'WORK_COMPANY'        => true,
                'UF_CORPUS'           => true,
                'UF_BRANCH_KNOWLEDGE' => true,
                'UF_CITIZENSHIP'      => true,
                'UF_PASSPORT_SERIES'  => true,
                'UF_PASSPORT_NUMBER'  => true,
                'UF_SCAN_PASSPORT1'   => true,
                'UF_SCAN_PASSPORT2'   => true,
            ),
            'homeAddress' => array(
                'WORK_ZIP'    => true,
                'WORK_CITY'   => true,
                'WORK_STREET' => true,
                'UF_HOUSE2'   => true,
            ),
        );

    protected $_userRegisterTypeCode;
    protected $_data;
    protected $_formRequest;
    protected $_errors;
    protected $_options;
    protected $_validationMapType = 'save';

    /**
     * @var []
     */
    protected $_sectionRights;

    /**
     * @var static
     */
    protected static $_currentUser;

    private $_isInWorkplace = null;

    /**
     * @return static
     */
    public static function getCurrent()
    {
        if (null === static::$_currentUser) {
            static::$_currentUser = static::forgeUser();
        }

        return static::$_currentUser;
    }

    public function __construct($USER_ID = 0)
    {
        global $USER;
        if (empty($USER_ID)) {
            $USER_ID = $USER->GetID();
        }

        /*if (!$USER_ID && !self::checkAuthAccessMode() && NebWorkplaces::checkAccess()) {
            $arUser  = self::getAnonymousUser();
            $USER_ID = $arUser['ID'];
        }*/

        $this->USER_ID = $USER_ID;

        Loc::loadMessages(__FILE__);

        return $this;
    }

    /**
     * Получает анонимного читателя ЭЧЗ из БД
     */
    public static function getAnonymousUser()
    {
        $arUser = UserTable::getList([
            'filter' => ['LOGIN' => static::ANONYMOUS_LOGIN],
            'select' => ['ID', 'LOGIN', 'UF_TOKEN']
        ])->fetch();

        return $arUser;
    }

    /**
     * Проверяет разрешен ли доступ к закрытым изданиям только зарегистрированным пользователям из ЭЧЗ
     */
    public static function checkAuthAccessMode() {

        Loader::includeModule('neb.main');

        return Option::get('neb.main', 'auth_access_mode') == 'Y';
    }

    /**
     * Запрос электронного читательского билета пользователя
     * @return array
     */
    public function getWorkPlaceTicket() {

        $arResult = [];

        if (!$this->IsAuthorized() || $this->USER_ID < 1)
            return $arResult;

        $arWorkPlaces = NebWorkplaces::getWorkplacesByIp();

        if (empty($arWorkPlaces) || !Loader::includeModule('highloadblock'))
            return $arResult;

        reset($arWorkPlaces); $arWorkPlaces = current($arWorkPlaces);

        // запрос билета пользователя
        $arHIblock      = HighloadBlockTable::getList(['filter' => ['NAME' => 'UsersTickets']])->fetchRaw();
        $obUsersTickets = HighloadBlockTable::compileEntity($arHIblock)->getDataClass();
        $arResult         = $obUsersTickets::getList(['filter' => [
            '>=UF_DATE'  => new DateTime(),
            'UF_WCHZ_ID' => $arWorkPlaces['ID'],
            'UF_USER_ID' => $this->USER_ID
        ]])->fetchRaw();

        return $arResult;
    }

    public static function getFieldGroupKeys($type)
    {
        $addSaveFields = [
            'ACTIVE'               => true,
            'CONFIRM_CODE'         => true,
            'UF_TOKEN'             => true,
            'UF_TOKEN_ADD_DATE'    => true,
            'UF_NUM_ECHB'          => true,
            'UF_STATUS'            => true,
            'UF_RGB_CARD_NUMBER'   => true,
            'UF_RGB_USER_ID'       => true,
            'UF_REGISTER_TYPE'     => true,
            'UF_ID_REQUEST'        => true,
            'UF_ESIA_FIELDS'       => true,
            'UF_IV_UPLOAD_DATE'    => true,
            'UF_IV_UPLOAD_PREVENT' => true,
            'UF_IV_REMOTE_ID'      => true,
            'UF_WCHZ'              => true,
            'UF_PREVERIFY_TIME'    => true,
        ];
        $libraryFields = [
            'UF_LIBRARY'   => true,
            'UF_LIBRARIES' => true,
            'ID'           => true,
        ];
        $filesFiedls = [
            'PERSONAL_PHOTO'    => true,
            'UF_SCAN_PASSPORT1' => true,
            'UF_SCAN_PASSPORT2' => true,
        ];
        $formFields = [
            'LOGIN'                => true,
            'PASSWORD'             => true,
            'CONFIRM_PASSWORD'     => true,
            'NAME'                 => true,
            'LAST_NAME'            => true,
            'EMAIL'                => true,
            'PERSONAL_PROFESSION'  => true,
            'PERSONAL_WWW'         => true,
            'PERSONAL_ICQ'         => true,
            'PERSONAL_GENDER'      => true,
            'PERSONAL_BIRTHDATE'   => true,
            'PERSONAL_PHONE'       => true,
            'PERSONAL_FAX'         => true,
            'PERSONAL_MOBILE'      => true,
            'PERSONAL_PAGER'       => true,
            'PERSONAL_STREET'      => true,
            'PERSONAL_MAILBOX'     => true,
            'PERSONAL_CITY'        => true,
            'PERSONAL_STATE'       => true,
            'PERSONAL_ZIP'         => true,
            'PERSONAL_COUNTRY'     => true,
            'PERSONAL_NOTES'       => true,
            'WORK_COMPANY'         => true,
            'WORK_DEPARTMENT'      => true,
            'WORK_POSITION'        => true,
            'WORK_WWW'             => true,
            'WORK_PHONE'           => true,
            'WORK_FAX'             => true,
            'WORK_PAGER'           => true,
            'WORK_STREET'          => true,
            'WORK_MAILBOX'         => true,
            'WORK_CITY'            => true,
            'WORK_STATE'           => true,
            'WORK_ZIP'             => true,
            'WORK_COUNTRY'         => true,
            'WORK_PROFILE'         => true,
            'WORK_LOGO'            => true,
            'WORK_NOTES'           => true,
            'PERSONAL_BIRTHDAY'    => true,
            'SECOND_NAME'          => true,
            'UF_PASSPORT_NUMBER'   => true,
            'UF_ISSUED'            => true,
            'UF_CORPUS'            => true,
            'UF_STRUCTURE'         => true,
            'UF_FLAT'              => true,
            'UF_BRANCH_KNOWLEDGE'  => true,
            'UF_EDUCATION'         => true,
            'UF_HOUSE2'            => true,
            'UF_STRUCTURE2'        => true,
            'UF_FLAT2'             => true,
            'UF_PLACE_REGISTR'     => true,
            'UF_PASSPORT_SERIES'   => true,
            'UF_REGION'            => true,
            'UF_REGION2'           => true,
            'UF_AREA'              => true,
            'UF_AREA2'             => true,
            'UF_BANK_NAME'         => true,
            'UF_BANK_INN'          => true,
            'UF_BANK_KPP'          => true,
            'UF_BANK_BIC'          => true,
            'UF_COR_ACCOUNT'       => true,
            'UF_PERSONAL_ACCOUNT'  => true,
            'UF_SETTLE_ACCOUNT'    => true,
            'UF_FOUNDATION'        => true,
            'UF_PERSON_INN'        => true,
            'UF_PERSON_KPP'        => true,
            'UF_PERSON_OKPO'       => true,
            'UF_PERSON_OGRN'       => true,
            'UF_PERSON_OKATO'      => true,
            'UF_PERSON_KBK'        => true,
            'UF_POSITION_HEAD'     => true,
            'UF_FIO_HEAD'          => true,
            'UF_CONTACT_INFO'      => true,
            'UF_CONTACT_PHONE'     => true,
            'UF_CONTACT_MOBILE'    => true,
            'UF_DOCUMENTS'         => true,
            'UF_CITIZENSHIP'       => true,
            'UF_SEARCH_PAGE_COUNT' => true,
        ];
        $options = [
            'USER_VERIFY' => true,
            'action'      => true,
        ];
        $fields = [];
        switch ($type) {
            case 'save':
                $fields = array_merge(
                    $fields,
                    $formFields,
                    $filesFiedls,
                    $addSaveFields,
                    $libraryFields
                );
                break;
            case 'form':
                $fields = array_merge(
                    $fields,
                    $formFields,
                    $filesFiedls
                );
                if ('library_admin' === static::getCurrent()->getRole()) {
                    $fields = array_merge(
                        $fields,
                        $libraryFields
                    );
                }
                break;
            case 'options':
                $fields = $options;
                break;
            case 'files':
                $fields = $filesFiedls;
                break;
        }

        return $fields;
    }

    public function makeValidationMap($type)
    {
        $stringFields = [
            'LOGIN'                => true,
            'PASSWORD'             => true,
            'CONFIRM_PASSWORD'     => true,
            'NAME'                 => true,
            'LAST_NAME'            => true,
            'EMAIL'                => true,
            'PERSONAL_PROFESSION'  => true,
            'PERSONAL_WWW'         => true,
            'PERSONAL_ICQ'         => true,
            'PERSONAL_GENDER'      => true,
            'PERSONAL_BIRTHDATE'   => true,
            'PERSONAL_PHONE'       => true,
            'PERSONAL_FAX'         => true,
            'PERSONAL_MOBILE'      => true,
            'PERSONAL_PAGER'       => true,
            'PERSONAL_STREET'      => true,
            'PERSONAL_MAILBOX'     => true,
            'PERSONAL_CITY'        => true,
            'PERSONAL_STATE'       => true,
            'PERSONAL_ZIP'         => true,
            'PERSONAL_COUNTRY'     => true,
            'PERSONAL_NOTES'       => true,
            'WORK_COMPANY'         => true,
            'WORK_DEPARTMENT'      => true,
            'WORK_POSITION'        => true,
            'WORK_WWW'             => true,
            'WORK_PHONE'           => true,
            'WORK_FAX'             => true,
            'WORK_PAGER'           => true,
            'WORK_STREET'          => true,
            'WORK_MAILBOX'         => true,
            'WORK_CITY'            => true,
            'WORK_STATE'           => true,
            'WORK_ZIP'             => true,
            'WORK_COUNTRY'         => true,
            'WORK_PROFILE'         => true,
            'WORK_LOGO'            => true,
            'WORK_NOTES'           => true,
            'PERSONAL_BIRTHDAY'    => true,
            'SECOND_NAME'          => true,
            'UF_PASSPORT_NUMBER'   => true,
            'UF_CORPUS'            => true,
            'UF_STRUCTURE'         => true,
            'UF_FLAT'              => true,
            'UF_HOUSE2'            => true,
            'UF_STRUCTURE2'        => true,
            'UF_FLAT2'             => true,
            'UF_PASSPORT_SERIES'   => true,
            'UF_REGION'            => true,
            'UF_REGION2'           => true,
            'UF_AREA'              => true,
            'UF_AREA2'             => true,
            'UF_BANK_NAME'         => true,
            'UF_BANK_INN'          => true,
            'UF_BANK_KPP'          => true,
            'UF_BANK_BIC'          => true,
            'UF_COR_ACCOUNT'       => true,
            'UF_PERSONAL_ACCOUNT'  => true,
            'UF_SETTLE_ACCOUNT'    => true,
            'UF_PERSON_INN'        => true,
            'UF_PERSON_KPP'        => true,
            'UF_PERSON_OKPO'       => true,
            'UF_PERSON_OGRN'       => true,
            'UF_PERSON_OKATO'      => true,
            'UF_PERSON_KBK'        => true,
            'UF_POSITION_HEAD'     => true,
            'UF_FIO_HEAD'          => true,
            'UF_CONTACT_INFO'      => true,
            'UF_CONTACT_PHONE'     => true,
            'UF_CONTACT_MOBILE'    => true,
            'UF_SEARCH_PAGE_COUNT' => true,
        ];
        $map = [];
        foreach (static::getFieldGroupKeys('save') as $fieldName => $true) {
            $map[$fieldName] = [
                'required' => false,
            ];
            if (isset($stringFields[$fieldName])) {
                $map[$fieldName]['type'] = 'string';
            }
        }
        switch ($type) {
            case 'save-strict':
            case 'full-access':
                foreach (
                    static::$_integrityGroupFields['full'] as $fieldName =>
                    $true
                ) {
                    $map[$fieldName]['required'] = true;
                }
                if (!$this->_data['UF_PLACE_REGISTR']) {
                    foreach (
                        static::$_integrityGroupFields['homeAddress'] as
                        $fieldName =>
                        $true
                    ) {
                        $map[$fieldName]['required'] = true;
                    }
                }
                break;
        }

        return $map;
    }

    public static function getFieldName($field)
    {
        return Loc::getMessage('NAME_FIELD_' . $field);
    }

    public function setValidationMapType($type)
    {
        $this->_validationMapType = $type;
    }

    /**
     * @param int $uid
     */
    public function setId($uid)
    {
        $this->USER_ID = $uid;
        $this->role = 'user';
    }

    /**
     * Получает все группы пользователей с использованием кеширования
     * @return array
     */
    public static function getAllGroups() {

        $arResult = [];

        $cacheTime = 3600; // 1 час
        $cacheDir  = CACHE_DIRECTORY . '/' . __CLASS__ . '/'. __FUNCTION__;
        $cacheId   = md5( $cacheDir );

        $obCache = Cache::createInstance();
        if ($obCache->initCache($cacheTime, $cacheId, $cacheDir)) {

            $arResult = $obCache->getVars();

        } elseif ($obCache->startDataCache()) {

            $rsUserGroups = GroupTable::getList([
                'filter' => ['ACTIVE' => 'Y'],
                'select' => ['ID', 'STRING_ID']
            ]);
            while ($arFields = $rsUserGroups->fetch()) {
                $arResult[$arFields['ID']] = $arFields['STRING_ID'];
            }
            
            $obCache->endDataCache($arResult);
        }

        return $arResult;
    }

    /**
     * Получает массив групп пользователя в формате [id => str] с использованием кеширования
     *
     * @param $iUserId
     * @param $bClean - только для сброса кеша
     * @return array
     */
    public static function getUserGroupsInfo($iUserId, $bClean = false){

        $arResult = [];

        if (intval($iUserId) < 1)
            return $arResult;

        $cacheTime = 3600; // 1 час
        $cacheDir  = CACHE_DIRECTORY . '/' . __CLASS__ . '/'. __FUNCTION__;
        $cacheId   = md5( $cacheDir . $iUserId );

        $obCache = Cache::createInstance();

        if ($bClean) {
            $obCache->clean($cacheId, $cacheDir);
            return $arResult;
        }

        if ($obCache->initCache($cacheTime, $cacheId, $cacheDir)) {

            $arResult = $obCache->getVars();

        } elseif ($obCache->startDataCache()) {

            $rsUserGroups = UserGroupTable::getList([
                'filter' => ['USER_ID' => $iUserId],
                'select' => ['GROUP_ID', 'GROUP.STRING_ID'],
                'runtime' => [
                    new ReferenceField(
                        'GROUP',
                        '\Bitrix\Main\GroupTable',
                        ['=this.GROUP_ID' => 'ref.ID'],
                        ['join_type' => 'LEFT']
                    ),
                ]
            ]);
            while ($arFields = $rsUserGroups->fetch()) {
                $arResult[$arFields['GROUP_ID']] = $arFields['MAIN_USER_GROUP_GROUP_STRING_ID'];
            }

            $obCache->endDataCache($arResult);
        }

        return $arResult;
    }

    /**
     * Получает массив групп пользователя id => str
     *
     * @return array
     */
    public function getUserGroups() {
        $this->arUserGroups = $this->getUserGroupsInfo($this->USER_ID);
        return $this->arUserGroups;
    }

    /**
     * @method    getRole
     * Определяем роль текущего пользователя
     * - обычный пользователь
     * - администратор библиотеки
     * - редактор библиотеки
     * - контроллер библиотеки
     *
     * @return bool|string
     */
    public function getRole()
    {
        if (empty($this->USER_ID)) {
            return false;
        }

        $arUserGroups = static::getUserGroups();

        // Если свопадений не найдено, возвращаем роль по умолчанию 'user'
        if (!empty($arUserGroups)) {
            if (array_search(UGROUP_MARKUP_APPROVER, $arUserGroups)) {
                $this->role = UGROUP_MARKUP_APPROVER;
            }
            
            if (array_search(UGROUP_LIB_CODE_ADMIN, $arUserGroups)) {
                $this->role = UGROUP_LIB_CODE_ADMIN;
            }

            if (array_search(UGROUP_LIB_CODE_EDITOR, $arUserGroups)) {
                $this->role = UGROUP_LIB_CODE_EDITOR;
            }

            if (array_search(UGROUP_LIB_CODE_CONTROLLER, $arUserGroups)) {
                $this->role = UGROUP_LIB_CODE_CONTROLLER;
            }

            if (array_search(UGROUP_RIGHTHOLDER_CODE, $arUserGroups)) {
                $this->role = UGROUP_RIGHTHOLDER_CODE;
            }

            if (array_search(UGROUP_ASSESSOR, $arUserGroups)) {
                $this->role = UGROUP_ASSESSOR;
            }

            if (array_search(UGROUP_OPERATOR_CODE, $arUserGroups)) {
                $this->role = UGROUP_OPERATOR_CODE;
            }

            if (array_search(UGROUP_OPERATOR_WCHZ, $arUserGroups)) {
                $this->role = UGROUP_OPERATOR_WCHZ;
            }
        }
        if (UGROUP_OPERATOR_CODE !== $this->role) {

            $arGroups = [];
            $arAllGroups = self::getAllGroups();
            foreach ($arAllGroups as $sCode)
                if (false !== strpos($sCode, UGROUP_OPERATOR_CODE) && 'operator_wchz' !== $sCode)
                    $arGroups[] = $sCode;

            if ([] !== array_intersect_key(array_flip($arGroups), array_flip($arUserGroups))) {
                $this->role = UGROUP_OPERATOR_CODE;
            }
        }

        return $this->role;
    }

    /**
     * Проверяет принадлежит ли пользователь хотя бы одной группе
     *
     * @return bool
     */
    public function hasGroup($arGroups)
    {
        $bResult = false;
        $arUserGroups = $this->getUserGroups();

        foreach ($arUserGroups as $sGroup) {
            if (in_array($sGroup, $arGroups)) {
                $bResult = true;
                break;
            }
        }

        return $bResult;
    }

    /**
     * Является ли пользователь сотрудником библиотеки
     *
     * @return bool
     */
    public function isLibrary()
    {
        return $this->hasGroup([UGROUP_LIB_CODE_ADMIN, UGROUP_LIB_CODE_EDITOR, UGROUP_LIB_CODE_CONTROLLER]);
    }

    /**
     * Является ли пользователь сотрудником глобальным оператором
     *
     * @return bool
     */
    public function isOperator()
    {
        return $this->hasGroup([UGROUP_OPERATOR_CODE]);
    }

    /**
     * Получить поля библиотеки к которой привязан пользователь
     */
    public function getLibrary()
    {
        $arUser = self::getUser();
        if (empty($arUser)) {
            return false;
        }

        if (empty($arUser['UF_LIBRARY'])) {
            $this->role = 'user';

            return false;
        }

        $arLibrary = Bitrix\NotaExt\Iblock\Element::getByID(
            $arUser['UF_LIBRARY'],
            array('ID', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_STATUS',
                  'PROPERTY_LIBRARY_LINK', 'skip_other', 'PROPERTY_SCAN_COST', 'PROPERTY_EMAIL')
        );
        if (strrpos($arLibrary["PROPERTY_STATUS_VALUE"], "/") !== false) {
            $arProperties = explode("/", $arLibrary["PROPERTY_STATUS_VALUE"]);
            if (LANGUAGE_ID == 'ru') {
                $arLibrary["PROPERTY_STATUS_VALUE"] = $arProperties[0];
            } else {
                if (LANGUAGE_ID == 'en') {
                    $arLibrary["PROPERTY_STATUS_VALUE"] = $arProperties[1];
                }
            }
        }

        return $arLibrary;
    }

    /**
     * Установить ЕЭЧБ
     * @return bool
     */
    public function setEICHB()
    {
        $arUser = self::getUser();
        if (empty($arUser)) {
            return false;
        }

        if (empty($arUser['UF_NUM_ECHB'])) {
            $EICHB = $arUser['ID'] . date('dmyhms', MakeTimeStamp($arUser['DATE_REGISTER']));
            if (strlen($EICHB) > 8) {
                $EICHB = substr($EICHB, 0, 8);
            }

            $user = new static;
            $user->Update($arUser['ID'], ["UF_NUM_ECHB" => $EICHB]);

            if (Loader::includeModule('nota.journal')) {
                J::add('eechb', 'add', ['FIO' => trim($arUser['LAST_NAME'] . ' ' . $arUser['NAME']), 'NUMBER' => $EICHB]);
            }

        } else {
            $EICHB = $arUser['UF_NUM_ECHB'];
        }
        $this->EICHB = $EICHB;
    }

    /**
     * Сделать пользователя верифицированным
     */
    public function setActive()
    {
        $arUser = self::getUser();
        if (empty($arUser)) {
            return false;
        }

        $user = new static;
        $user->Update($arUser['ID'], ["UF_STATUS" => USER_ACTIVE_ENUM_ID]);
    }

    /**
     * @param bool
     *
     * @return array|array
     */
    public function getUser()
    {
        if (null === $this->_data) {
            if (empty($this->USER_ID)) {
                return false;
            }

            $this->_data = CUser::GetByID($this->USER_ID)->Fetch();
        }

        return $this->_data;
    }

    public function refreshUserData()
    {
        $this->_data = null;
        $this->_data = $this->getUser();
    }

    /**
     * Получить значения поля типа СПИСОК
     *
     * @param $arFilter
     * @param array $arOrder
     * @return array|bool
     */
    public static function getFieldEnum($arFilter, $arOrder = array('SORT' => 'ASC')) {

        if (empty($arFilter)) {
            return false;
        }

        $arResult = array();

        if (!empty($arFilter['USER_FIELD_NAME'])) {
            if (!is_array($arSort) || count($arSort) <= 0) {
                $arSort = array($by => $order);
            }
            $rsData = CUserTypeEntity::GetList(
                $arSort, array('FIELD_NAME' => $arFilter['USER_FIELD_NAME'])
            );
            if ($arRes = $rsData->Fetch()) {
                unset($arFilter['USER_FIELD_NAME']);
                $arFilter['USER_FIELD_ID'] = $arRes['ID'];
            } else {
                return false;
            }
        }

        $obUserFieldEnum = new CUserFieldEnum();
        $rsGender = $obUserFieldEnum->GetList($arOrder, $arFilter);
        while ($arGender = $rsGender->GetNext()) {
            if (strrpos($arGender["VALUE"], "/") !== false) {
                $arGenders = explode("/", $arGender["VALUE"]);
                if (LANGUAGE_ID == 'ru') {
                    $arGender["VALUE"] = $arGenders[0];
                } elseif (LANGUAGE_ID == 'en') {
                    $arGender["VALUE"] = $arGenders[1];
                }
            }
            $arResult[] = $arGender;
        }

        return $arResult;
    }

    /**
     * Добавить пользователя
     *
     * @param $arFields
     * @return bool|int|string
     */
    public function Add($arFields)
    {
        $user = new parent;

        $ID = $user->Add($arFields);
        if (intval($ID) > 0) {
            $this->USER_ID = $ID;
        } else {
            $this->error = $user->LAST_ERROR;
        }

        return $ID;
    }

    public static function isShowProtectedContent()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            //return true;

            $user = new self;
            $arUser = $user->getUser();
            //if(!empty($arUser['UF_RGB_USER_ID']))
            // заглушка по просьбе клиента, пускаем к закрытым изданиям всех у кого загружен паспорт
            if (!empty($arUser['UF_SCAN_PASSPORT1'])
                || !empty($arUser['UF_RGB_USER_ID']) || intval($arUser['UF_REGISTER_TYPE']) == 39) {
                return true;
            }
        }

        if (NebWorkplaces::checkAccess()) {
            return true;
        }

        return false;
    }

    /**
     * Получение списка значений для UF_REGISTER_TYPE
     * Ключи массива [ID] => [XML_ID, VALUE], при необходимости другие
     *
     * @return array
     */
    public static function getAllUserRegisterTypes()
    {
        if (!empty(static::$_arUserRegisterTypes)) {
            return static::$_arUserRegisterTypes;
        }

        $obCacheExt = new NPHPCacheExt();
        $arCacheParams['FIELD_NAME'] = 'UF_REGISTER_TYPE';
        $cacheTag = 'NEBUSER_ENUM';

        if (!$obCacheExt->InitCache(__METHOD__, $arCacheParams, $cacheTag)) {
            $arRegTypes = static::getFieldEnum(
                array('USER_FIELD_NAME' => 'UF_REGISTER_TYPE')
            );
            /*
             * keys: id, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID
             */
            if (is_array($arRegTypes) && count($arRegTypes) > 0) {
                $arTypes = array();
                foreach ($arRegTypes AS $regType) {
                    $arTypes[$regType['ID']] = array(
                        //'USER_FIELD_ID' => $regType['USER_FIELD_ID'],
                        'VALUE'  => $regType['VALUE'],
                        //'DEF' => $regType['DEF'],
                        //'SORT' => $regType['SORT'],
                        'XML_ID' => $regType['XML_ID']
                    );
                }
            }

            static::$_arUserRegisterTypes = $arTypes;
            $obCacheExt->StartDataCache(
                static::$_arUserRegisterTypes
            );
        } else {
            static::$_arUserRegisterTypes = $obCacheExt->GetVars();
        }

        return static::$_arUserRegisterTypes;
    }

    /**
     * Получение ID значения UF_REGISTER_TYPE для данного пользователя
     *
     * @return int
     */
    public function getUserRegisterTypeId()
    {
        if (empty($this->_userRegisterTypeId)) {

            $obCacheExt = new NPHPCacheExt();
            $arCacheParams['USER_ID'] = $this->USER_ID;
            $cacheTag = 'NEBUSER_UF_REGTYPE';

            if (!$obCacheExt->InitCache(__METHOD__, $arCacheParams, $cacheTag)) {
                $rsUser = CUser::GetList(
                    $by = array(), $order = array(),
                    array(
                        "ID" => $this->USER_ID,
                    ),
                    array(
                        "SELECT" => array(
                            "USER_FIELD_ID" => "UF_REGISTER_TYPE",
                        ),
                    )
                );
                if ($rsUser) {
                    $arUser = $rsUser->Fetch();
                }
                $this->_userRegisterTypeId = $arUser['UF_REGISTER_TYPE'];

                $obCacheExt->StartDataCache(
                    $this->_userRegisterTypeId
                );
            } else {
                $this->_userRegisterTypeId = $obCacheExt->GetVars();
            }
        }

        return $this->_userRegisterTypeId;
    }

    /**
     * Получение XML_ID значения UF_REGISTER_TYPE для данного пользователя
     *
     * @return array() keys: XML_ID, VALUE;
     */
    public function getUserRegisterTypeCode()
    {
        $arTypes = static::getAllUserRegisterTypes();

        return $arTypes[$this->getUserRegisterTypeId()];
    }

    /**
     * Получает токен пользователя и дату последего его сброса
     *
     * @return array
     */
    public function getToken()
    {
        $data = $this->getUser();

        return array('UF_TOKEN'          => $data['UF_TOKEN'],
                     'UF_TOKEN_ADD_DATE' => $data['UF_TOKEN_ADD_DATE']);
    }

    /**
     * Документировать
     *
     * @return bool
     */
    public function checkToken()
    {
        $token = $this->getToken();
        if (!$token['UF_TOKEN_ADD_DATE']) {
            return false;
        }

        $time = strtotime($token['UF_TOKEN_ADD_DATE']);

        return (($time - time()) > 3600);
    }

    /**
     * Обновляет токен и дату токена у пользователя
     *
     * @param bool $bOnlyDate - обновить только дату (используется в агенте)
     *
     * @return bool
     */
    public function setNewToken($bOnlyDate = false)
    {
        $arUpdate = [
            'UF_TOKEN'          => md5(microtime(true)),
            'UF_TOKEN_ADD_DATE' => ConvertTimeStamp(
                time() + 24 * 60 * 60, 'FULL'
            )
        ];

        if ($bOnlyDate) {
            unset($arUpdate['UF_TOKEN']);
        }

        return $this->Update($this->USER_ID, $arUpdate);
    }

    /**
     * Запускает перегенерирование токена текущего пользователя
     */
    public function regenToken()
    {
        if (!$this->checkToken()) {
            $this->setNewToken();
        }
    }

    /**
     * @param $status
     *
     * @return bool
     */
    public function setStatus($status)
    {
        $this->_status = (integer)$status;

        return $this->Update(
            $this->USER_ID, array(
                'UF_STATUS' => $status,
            )
        );
    }

    /**
     * @param string $group
     *
     * @return array
     * @throws Exception
     */
    public function getEmptyGroupFields($group = 'min')
    {
        $emptyFields = array();
        if (!isset(static::$_integrityGroupFields[$group])) {
            return $emptyFields;
        }
        $userData = $this->getUser();
        if (!isset($userData['ID'])) {
            throw new Exception('Пользователь не найден', 1);
        }
        $checkGrounp = function ($groupFields) use ($userData, &$emptyFields) {
            foreach ($groupFields as $fieldName) {
                if (!isset($userData[$fieldName])
                    || empty($userData[$fieldName])
                ) {
                    $emptyFields[] = $fieldName;
                }
            }
        };
        $checkGrounp(array_keys(static::$_integrityGroupFields[$group]));
        // Если не стоит галочка "мето жительство = место прописки" то проверяем также место жительства
        if ('full' === $group && !$userData['UF_PLACE_REGISTR']) {
            $checkGrounp(
                array_keys(static::$_integrityGroupFields['homeAddress'])
            );
        }

        return $emptyFields;
    }

    /**
     * @param string $group
     *
     * @return bool
     * @throws Exception
     */
    public function checkIntegrityGroup($group = 'min')
    {
        return array() === $this->getEmptyGroupFields($group);
    }

    /**
     * @param int $uid
     *
     * @return \helpers\user\nebReader|static
     */
    public static function forgeUser($uid = null)
    {
        $instance = new static($uid);
        if ('user' === $instance->getRole()) {
            $instance = new \helpers\user\nebReader($instance->USER_ID);
        }

        return $instance;
    }


    /**
     * @return \helpers\user\nebReader|nebUser
     */
    public static function forgeEmpty()
    {
        $user = static::forgeUser();
        $user->USER_ID = 0;

        return $user;
    }

    /**
     * Поиск пользователя по Логину, Email  ЕЭЧБ
     *
     * @param $login
     * @return array|bool
     * @throws \Bitrix\Main\LoaderException
     */
    public static function UserFields($login)
    {
        if (empty($login)) {
            return false;
        }

        $arUser = UserTable::getList([
            'filter' => [['LOGIC'=>'OR', ['=LOGIN' => $login], ['=EMAIL' => $login], ['=UF_NUM_ECHB' => $login]], 'ACTIVE' => 'Y'],
            'select' => [
                'ID', 'LOGIN', 'PASSWORD', 'EMAIL', 'LAST_NAME', 'NAME', 'SECOND_NAME', 'DATE_REGISTER',
                'UF_STATUS', 'UF_RGB_USER_ID', 'UF_NUM_ECHB'
            ]
        ])->fetch();

        return $arUser;
    }

    /**
     * @param null $userId
     *
     * @return bool
     */
    public function canVerify($userId = null)
    {
        if (null !== $userId) {
            $this->USER_ID = $userId;
        }
        $user = $this->getUser();
        if (static::USER_STATUS_REGISTERED === intval($user['UF_STATUS']) || empty($user['UF_STATUS'])
        ) {
            return true;
        }

        return false;
    }

    public static function getGroupSubordinate($group)
    {
        $filter = array();
        if (is_string($group)) {
            $filter['GROUP.STRING_ID'] = $group;
        } else {
            $filter['ID'] = $group;
        }
        $groupSubordinate = \Neb\Main\GroupSubordinateTable::getList(
            array(
                'filter' => $filter
            )
        );
        $groupSubordinate = $groupSubordinate->fetch();
        $groupSubordinate = explode(
            ',', $groupSubordinate['AR_SUBGROUP_ID']
        );
        $groupSubordinate = array_filter($groupSubordinate);
        $groupSubordinate = array_map('intval', $groupSubordinate);

        return $groupSubordinate;
    }

    /**
     * Проверка принадлежности текущего пользователю одному из ЭЧЗ
     * @return bool
     */
    public function isInWorkplace()
    {
        if (null !== $this->_isInWorkplace) {
            return $this->_isInWorkplace;
        }

        Loader::includeModule('iblock');

        $arFilter = [
            'IBLOCK_ID'         => IBLOCK_ID_WORKPLACES,
            self::WORKPLACE_IPS => [$_SERVER['REMOTE_ADDR']],
            'ACTIVE'            => 'Y',
        ];
        $obIBlockElement = new CIBlockElement();
        $arRes = $obIBlockElement->GetList([], $arFilter, false, false, ['ID', 'IBLOCK_ID'])->Fetch();

        $this->_isInWorkplace = $arRes['ID'] > 0;

        return $this->_isInWorkplace;

    }

    public function getStatus()
    {
        if (null === $this->_status) {
            $user = $this->getUser();
            $this->_status = intval($user['UF_STATUS']);
        }

        return $this->_status;
    }

    public static function allowReadClosedBooks()
    {
        $user = static::getCurrent();

        return ($user->IsAuthorized() && $user->isInWorkplace()) || static::USER_STATUS_VIP === $user->getStatus();
    }

    public function verify()
    {
        throw new Exception('Верификация отключена');
        $user = $this->getUser();
        if (
            0 !== (integer)$user['UF_STATUS']
            && static::USER_STATUS_REGISTERED
            !== (integer)$user['UF_STATUS']
        ) {
            throw new Exception(
                'Недопустимый статус. Пользователь должен быть в статусе "Зарегистрированный"'
            );
        }
        $emptyFields = $this->getEmptyGroupFields('full');
        if (!empty($emptyFields)) {
            foreach ($emptyFields as $fieldName) {
                $this->addError(
                    Loc::getMessage('ERROR_REQUIRED_FIELD', ['#FIELD_NAME#' => $this->getFieldName($fieldName)])
                );
            }
            throw new Exception(
                'У пользователя не заполнены обязательные поля',
                static::ERROR_NOT_FULL_REGISTER
            );
        }
        $rgb = new \Nota\UserData\rgb();
        $pushResult = $rgb->pushUserIntoRGB($this);
        if (!isset($pushResult['id_request']) || empty($pushResult['id_request'])
        ) {
            throw new Exception($pushResult['message']);
        }

        return $pushResult;
    }

    /**
     * Проверяет временная ли верификация у пользователя
     * Если временная и время истекло то меняет статус на "Зарегистрированный"
     *
     * @param int $userId
     *
     * @return bool
     */
    public static function checkTemporaryVerification($userId = 0)
    {
        $user = new static($userId);
        $userData = $user->getUser();
        $verfySettings = Option::get('neb.main', 'temporary_verification_hours');
        if (empty($userData['UF_PREVERIFY_TIME'])) {
            return false;
        }
        if ((time() - MakeTimeStamp($userData['UF_PREVERIFY_TIME'])) > ((float)$verfySettings * 3600)
        ) {
            $fields = ['UF_PREVERIFY_TIME' => ''];
            if (nebUser::USER_STATUS_VERIFIED === intval($userData['UF_STATUS'])) {
                $fields['UF_STATUS'] = nebUser::USER_STATUS_REGISTERED;
            }
            $user->Update($user->USER_ID, $fields);

            return false;
        }

        return true;
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function applyUserData($data)
    {
        if (is_array($data)) {
            $userData = $this->getUser();
            if (!is_array($userData)) {
                $userData = [];
            }
            $this->_data = array_replace_recursive($userData, $data);
            if (!$this->USER_ID && isset($this->_data['ID']) && !empty($this->_data['ID'])
            ) {
                $this->setId($this->_data['ID']);
            }
        }

        return $this;
    }

    /**
     * @return \Neb\Main\UserFormRequest
     */
    public function getRequest()
    {
        if (null === $this->_formRequest) {
            $this->_formRequest = \Neb\Main\UserFormRequest::buildRequest();
        }

        return $this->_formRequest;
    }


    /**
     * @return bool
     */
    public function save()
    {
        try {
            if (empty($this->_data)) {
                throw new Exception('Нет данных для сохранения!');
            }
            $userVerify = $this->getOption('USER_VERIFY');
            if (!empty($userVerify)) {
                throw new Exception('Верификация отключена');
            }
            $this->applyUserLibrary();
            $this->validate();
            $userData = $this->getUser();
            $userData = array_intersect_key(
                $userData, static::getFieldGroupKeys('save')
            );
            foreach (static::getFieldGroupKeys('files') as $fieldName => $true)
            {
                if (isset($userData[$fieldName])
                    && !is_array($userData[$fieldName])
                ) {
                    unset($userData[$fieldName]);
                }
            }
            if ($this->USER_ID > 0) {
                if (!$this->Update($this->USER_ID, $userData)) {
                    $this->addError($this->LAST_ERROR);
                } else {
                    $this->refreshUserData();
                }
            } else {
                if (!$userId = $this->Add($userData)) {
                    $this->addError($this->error);
                } else {
                    $this->setId($userId);
                    $this->setEICHB();
                    $this->refreshUserData();
                }
            }
            if (!$this->hasErrors() && $this->USER_ID > 0) {
                if ('Y' === $this->getRequest()->getField('USER_VERIFY')
                    && nebUser::USER_STATUS_VERIFIED
                    !== (integer)$userData['UF_STATUS']
                ) {
                    $this->verify();
                    if ('library_admin' === static::getCurrent()->getRole()
                    ) {
                        $this->setStatus(static::USER_STATUS_VERIFIED);
                    }
                } elseif ('N' === $this->getRequest()->getField(
                        'USER_VERIFY'
                    )
                    && nebUser::USER_STATUS_VERIFIED
                    === (integer)$userData['UF_STATUS']
                ) {
                    throw new Exception('Верификация отключена');
                    $this->setStatus(nebUser::USER_STATUS_REGISTERED);
                }
                $this->refreshUserData();
            }
        } catch (Exception $e) {
            $this->addError($e->getMessage());
        }

        return !$this->hasErrors();
    }

    public function applyUserLibrary()
    {
        if ('library_admin' === static::getCurrent()->getRole()
            && ($this->USER_ID <= 0 || 'user' === $this->getRole())
        ) {
            $currentUser = static::getCurrent()->getUser();
            if (!isset($this->_data['UF_LIBRARIES'])) {
                $this->_data['UF_LIBRARIES'] = [];
            }
            if ($currentUser['UF_LIBRARY']
                && !in_array(
                    $currentUser['UF_LIBRARY'],
                    $this->_data['UF_LIBRARIES']
                )
            ) {
                $this->_data['UF_LIBRARIES'][] = $currentUser['UF_LIBRARY'];
            }
            if (!empty($this->_data['UF_LIBRARIES'])
                && empty($this->_data['UF_LIBRARY'])
            ) {
                unset($this->_data['UF_LIBRARY']);
            }
        }
    }

    public function validate()
    {
        $result = true;
        if (isset($this->_data['PASSWORD'])) {
            if (!isset($this->_data['CONFIRM_PASSWORD'])
                || empty($this->_data['CONFIRM_PASSWORD'])
                || empty($this->_data['PASSWORD'])
            ) {
                unset($this->_data['PASSWORD']);
            } elseif ($this->_data['PASSWORD']
                !== $this->_data['CONFIRM_PASSWORD']
            ) {
                $result = false;
                $this->addError('Пароли не совпадают');
            }
        }
        if ('Y' === $this->getOption('USER_VERIFY')) {
            $this->setValidationMapType('full-access');
        }
        foreach (
            $this->makeValidationMap($this->_validationMapType)
            as $fieldName => $map
        ) {
            if (
                isset($map['type'])
                && 'string' === $map['type']
                && isset($this->_data[$fieldName])
            ) {
                $this->_data[$fieldName] = trim($this->_data[$fieldName]);
            }
            if (true === $map['required']
                && (
                    !isset($this->_data[$fieldName])
                    || empty($this->_data[$fieldName])
                )
            ) {
                $this->addError(
                    Loc::getMessage('ERROR_REQUIRED_FIELD', ['#FIELD_NAME#' => $this->getFieldName($fieldName)])
                );
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Проверка принадлежности текущего пользователя к библиотеке $libraryId
     * @param number $libraryId - идентификатор библиотеки
     *
     * @return bool
     */
    public function checkInLibrary($libraryId)
    {
        $bResult = false;
        if ($this->USER_ID) {
            $arUser = UserTable::getList([
                'filter' => ['ID' => $this->USER_ID, 'UF_LIBRARIES' => $libraryId],
                'select' => ['ID']
            ])->fetch();
            if (isset($arUser['ID'])) {
                $bResult = true;
            }
        }

        return $bResult;
    }

    public function create()
    {
        return !$this->hasErrors();
    }

    public function sendEmails($events, $immediate = false)
    {
        $arEventFields = $this->getUser();

        unset(
            $arEventFields["PASSWORD"],
            $arEventFields["CONFIRM_PASSWORD"]
        );

        $event = new \CEvent;
        foreach ($events as $eventName)
        {
            if ($eventName == "NEW_USER_CONFIRM")
            {
                $arEventFields["DATETIME_CREATE"] = date('d.m.Y H:i');
                $arEventFields["DAYS_AUTH_CONF"]  = Option::get('main', 'new_user_registration_cleanup_days', 3);
            }

            if (true === $immediate) {
                $event->SendImmediate($eventName, SITE_ID, $arEventFields);
            } else {
                $event->Send($eventName, SITE_ID, $arEventFields);
            }
        }
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->_options = $options;
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function setDataField($name, $value)
    {
        $this->_data[$name] = $value;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getDataField($name)
    {
        if (isset($this->_data[$name])) {
            return $this->_data[$name];
        }

        return null;
    }

    /**
     * @param string $optionName
     *
     * @return mixed
     */
    public function getOption($optionName)
    {
        if (isset($this->_options[$optionName])) {
            return $this->_options[$optionName];
        }

        return null;
    }


    /**
     * @param $message
     */
    public function addError($message)
    {
        $this->_errors[] = $message;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return !empty($this->_errors);
    }


    /**
     * Получение прав доступа к разделу для определенной роли пользователя
     * @param string $sGroup - по умолчанию для оператора
     * @return array
     */
    public function getSectionsRights($sGroup = 'operator')
    {
        $arResult = ['profile_edit'];
        $arGroups = $this->getUserGroups();
        foreach ($arGroups as $sCode)
            if (false !== strpos($sCode, $sGroup . '_'))
                $arResult[] = str_replace($sGroup . '_', '', $sCode);

        return $arResult;
    }

    /**
     * @param string $section
     *
     * @return bool
     */
    public function checkSectionAccess($section)
    {
        $arSections = $this->getSectionsRights();
        foreach ($arSections as $allowedSection) {
            if (0 === strpos($section, $allowedSection)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Метод определяет, является ли текущий пользователь разработчиком Elar
     * @param null $userIp
     * @return bool
     */
    public static function checkElar($userIp = null) {
        
        if (empty($userIp)) {
            $userIp = IpHelper::getRemoteIp();
        }

        return in_array($userIp, self::ELAR_IP) || self::checkDemo() || self::checkLocal();
    }

    /**
     * Проверяет является ли текущий сервер демо-сервером
     * @return bool
     */
    public static function checkDemo() {
        return false !== stripos($_SERVER['DOCUMENT_ROOT'], 'neb_demo');
    }

    /**
     * Проверяет является ли текущий компьютер локальным
     * @return bool
     */
    public static function checkLocal() {
        return false !== stripos($_SERVER['DOCUMENT_ROOT'], ':')
                || false !== stripos($_SERVER['DOCUMENT_ROOT'], 'PhpStormProjects');
    }
}