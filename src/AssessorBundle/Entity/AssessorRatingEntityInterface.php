<?php
/**
 * User: agolodkov
 * Date: 10.10.2016
 * Time: 18:22
 */

namespace AssessorBundle\Entity;


interface AssessorRatingEntityInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getUpdateUrl();

    /**
     * @param string $updateUrl
     */
    public function setUpdateUrl($updateUrl);
}