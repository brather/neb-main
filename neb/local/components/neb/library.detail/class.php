<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader;
use \Bitrix\Main\Page\Asset;
use \Bitrix\NotaExt\Iblock\Element;
use \Bitrix\NotaExt\Iblock\IblockTools;

/**
 * Class LibraryDetailComponent
 */
class LibraryDetailComponent extends CBitrixComponent
{
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        Asset::getInstance()->addCss('/local/templates/adaptive/css/library-map.css');

        if (!isset($arParams["CACHE_TIME"])) {
            $arParams["CACHE_TIME"] = 3600;
        }

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        global $APPLICATION;

        if (empty($this->arParams['CODE']) || empty($this->arParams['IBLOCK_ID']))
            return false;

        $viewTemplate = '';
        $viewTheme = '';

        $this->arResult = Element::getList(
            array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'CODE' => $this->arParams['CODE']),
            1,
            array(
                'PREVIEW_PICTURE',
                'PREVIEW_TEXT',
                'PROPERTY_MAP',
                'PROPERTY_STATUS',
                'PROPERTY_ADDRESS',
                'PROPERTY_SCHEDULE',
                'PROPERTY_PHONE',
                'PROPERTY_EMAIL',
                'PROPERTY_SKYPE',
                'PROPERTY_URL',
                'PROPERTY_COLLECTIONS',
                'PROPERTY_CONTACTS',
                'PROPERTY_VIEW_TEMPLATE',
                'PROPERTY_VIEW_THEME',
            ),
            array(),
            array(),
            false,
            true
        );

        // выбор схемы микроразметки в зависимости от наличия логотипа
        if (!empty($this->arResult['ITEM']['PREVIEW_PICTURE'])) {
            $this->arResult['ITEM']['IMAGE']
                = URL_SCHEME . '://' . SITE_HOST . CFile::GetPath($this->arResult['ITEM']['PREVIEW_PICTURE']); // $_SERVER['HTTP_HOST']

            $this->arResult['ITEM']['SCHEMA_TYPE'] = 'Library';
        } else {
            $this->arResult['ITEM']['SCHEMA_TYPE'] = 'Organization';
        }

        if (!empty($this->arResult['ITEM']['PROPERTY_URL_VALUE'])) {
            $this->arResult['ITEM']['PROPERTY_URL_VALUE'] = trim($this->arResult['ITEM']['PROPERTY_URL_VALUE']);
            if (false === mb_stripos($this->arResult['ITEM']['PROPERTY_URL_VALUE'], 'http')) {
                $this->arResult['ITEM']['PROPERTY_URL_VALUE'] = 'http://' . $this->arResult['ITEM']['PROPERTY_URL_VALUE'];
            }
        }

        if (isset($this->arResult['ITEM']['PROPERTY_MAP_VALUE'])) {
            $this->arResult['ITEM']['MAP'] = explode(',', $this->arResult['ITEM']['PROPERTY_MAP_VALUE']);
        }

        if (isset($this->arResult['ITEM']['PROPERTY_VIEW_TEMPLATE_ENUM_ID'])) {
            $viewTemplate = IblockTools::GetPropertyEnumXmlId('biblioteki_4', 'VIEW_TEMPLATE', $this->arResult['ITEM']['PROPERTY_VIEW_TEMPLATE_ENUM_ID']);
        }

        if (isset($this->arResult['ITEM']['PROPERTY_VIEW_THEME_ENUM_ID'])) {
            $viewTheme = IblockTools::GetPropertyEnumXmlId($this->arParams['IBLOCK_ID'], 'VIEW_THEME', $this->arResult['ITEM']['PROPERTY_VIEW_THEME_ENUM_ID']);
        }

        if (strrpos($this->arResult["ITEM"]["PROPERTY_STATUS_VALUE"], "/") !== false) {
            $arProperties = explode("/", $this->arResult["ITEM"]["PROPERTY_STATUS_VALUE"]);
            if (LANGUAGE_ID == 'ru') {
                $this->arResult["ITEM"]["PROPERTY_STATUS_VALUE"] = $arProperties[0];
            } else if (LANGUAGE_ID == 'en') {
                $this->arResult["ITEM"]["PROPERTY_STATUS_VALUE"] = $arProperties[1];
            }
        }

        //$viewTemplate = '';
        $this->IncludeComponentTemplate($viewTemplate);

        if (!empty($this->arResult['ITEM']['NAME'])) {
            $APPLICATION->SetTitle($this->arResult['ITEM']['NAME']);
        }

        if (strlen($viewTheme) > 0) {
            $APPLICATION->SetPageProperty('view_theme', $viewTheme);
        }

        /* crouches in order to success */
        if (strlen($viewTemplate) > 0) {
            $APPLICATION->SetPageProperty('view_template_index', $viewTemplate);
        }
        if (strlen($viewTheme) > 0) {
            $APPLICATION->SetPageProperty('view_theme_index', $viewTheme);
        }
        /* crouches in order to success */

        if (Loader::includeModule('iblock')) {
            CIBlockElement::CounterInc($this->arResult['ITEM']['ID']);
        }
    }

    /**
     * Формирование ссылки для просмотра в публичной части
     *
     * @param $sLink
     * @return string
     */
    public static function getLinkView($sLink) {
        return rtrim(str_replace(['http://', 'https://'], '', $sLink), '/');
    }
}