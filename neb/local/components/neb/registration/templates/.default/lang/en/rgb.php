<?php
$MESS['REG_RGB_TITLE'] = 'Registration on the Portal';
$MESS['REG_RGB_SIGN_UP'] = 'Sign up';
$MESS['REG_RGB_LAST_NAME'] = 'Last Name';
$MESS['REG_RGB_NAME'] = 'Name';
$MESS['REG_RGB_SECOND_NAME'] = 'Second Name';
$MESS['REG_RGB_CARD_NUMBER'] = 'Library card number RSL';
$MESS['REG_RGB_PASSWORD'] = 'Password will RSL';
$MESS['REG_RGB_PASSWORD_CONFIRM'] = 'Confirm password';
$MESS['REG_RGB_ERROR_FILL_IT'] = 'Fill in';
$MESS['REG_RGB_ERROR_INVALID_FORMAT'] = 'Invalid format';
$MESS['REG_RGB_ERROR_MORE_THAN_30'] = 'More than 30 characters';
$MESS['REG_RGB_ERROR_PASSWORD_MORE_THAN_30'] = 'Password more than 30 characters';
$MESS['REG_RGB_ERROR_AT_LEAST_6'] = 'Password at least 6 characters';
$MESS['REG_RGB_ERROR_PASSWORD_IDENTITY'] = 'Password does not match with the earlier';
$MESS['REG_RGB_ERROR_RGB_DATA_NOT_FOUND'] = 'Data on the library card number is not found in the RSL.';
$MESS['REG_RGB_ERROR_COMPLETE_FULL_REG'] = 'Complete the full registration procedure';
$MESS["REG_RGB_AGREE_FOR_POPUP"] = 'Before you register, you must read';
$MESS["REG_RGB_AGREE_FOR_POPUP_TERMS"] = 'the Terms of Use';
$MESS["REG_RGB_READ_TERMS"] = 'Read the Terms of Use';

