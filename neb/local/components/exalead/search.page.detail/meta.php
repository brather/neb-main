<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class BookMeta - класс для управления метаинформацией книги
 */
class BookMeta
{
    private static $_instance;
    private $arBook = [], $arResult = [], $bViewer = false;

    public static function getInstance() {
        if (!isset(self::$_instance) || (self::$_instance === NULL)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function get($arBook, $bViewer) {

        $this->arBook = $arBook;
        $this->bViewer = $bViewer;
        unset($arBook, $bViewer);

        $this->getTitle();
        $this->getKeywords();
        $this->getDescription();
        $this->getDescription(true);
        $this->setMetaInfo();
        $this->setCanonicalUrl();

        return $this->arResult;
    }

    /**
     * Установка метатегов Open Graph
     */
    private function setMetaInfo() {

        if (empty($this->arBook))
            return;

        global $APPLICATION;

        $sAuthors = $this->getAuthors();
        $sTitle   = trim($this->arBook['title']) . (!empty($sAuthors) ? ' - ' . $sAuthors : '');
        $sIsbn    = BookUtils::getIsbn($this->arBook['isbn']);
        $sYear    = BookUtils::getYear($this->arBook['publishyear']);
        $arTags = [];
        if (!empty($this->arBook['collection_new']))
            $arTags[] = $this->getCollectionName();
        foreach ($this->arBook['bbkfull'] as $arBbk)
            $arTags[] = trim(end($arBbk));

        $APPLICATION->SetPageProperty('og:title', $sTitle);
        $APPLICATION->SetPageProperty('og:url', $this->arBook['DETAIL_PAGE_URL']);
        $APPLICATION->SetPageProperty('og:type', 'book');
        $APPLICATION->SetPageProperty('book:isbn', $sIsbn);
        $APPLICATION->SetPageProperty('book:release_date', $sYear);
        $APPLICATION->SetPageProperty('book:tag', $arTags);
    }

    /**
     * Установка канонического URL
     * Если у книги есть родительская книга - установить канонический URL на её родителя
     */
    private function setCanonicalUrl() {

        if (!empty($this->arBook['parent'])) {
            $sUrl = '/catalog/' . urlencode($this->arBook['parent']) . '/';
        } else {
            $sUrl = $this->arBook['DETAIL_PAGE_URL'];
        }

        \Neb\Main\Helper\UtilsHelper::setCanonicalUrl($sUrl);
    }

    /**
     * Формирование title
     */
    private function getTitle () {

        $sAuthors = $this->getAuthors();
        $sSeo     = $this->getSeoTitle();

        $sTitle = trim($this->arBook['title']); // ? Книга ".."
        if (!empty($sAuthors))                  // Авторы
            $sTitle .= ' - ' . $sAuthors;
        if (!empty($this->arBook['idbook']))    // Разметка
            $sTitle .= ' - ' . $this->arBook['namebook'];
        if (!empty($sSeo) && !$this->bViewer)
            $sTitle .= ' - ' . $sSeo;

        $this->arResult['TITLE'] = $sTitle;
    }

    /**
     * Формирование ключевых слов
     */
    private function getKeywords() {

        $sAuthors = $this->getAuthors($this->arBook);

        $sResult = "";

        if (!empty($this->arBook['title'])) {
            $sResult .= Loc::getMessage('META_INFO_BOOK') . ', ' . trim($this->arBook['title']);
        }
        if (!empty($sAuthors)) {
            $sResult .= ', ' . $sAuthors;
        }
        if (!empty($this->arBook['collection_new'])) {
            $sResult .= ', ' . $this->getCollectionName();
        }

        if (isset($this->arBook['fileExt']) && !empty($this->arBook['fileExt'])) {
            if ('Y' == $this->arBook['IS_READ'] && in_array('pdf', $this->arBook['fileExt']) && 1 > $this->arBook['isprotected']) { // TODO: ? epub и литрес
                // поля патентных документов
                if ($this->isPatent()) {
                    $sResult .= ', ' . Loc::getMessage('META_INFO_PATENT_DL');
                }
                // прочие издания
                else {
                    $sResult .= ', ' . Loc::getMessage('META_INFO_READ') . Loc::getMessage('META_INFO_KW');
                }
            } elseif (true === $this->arBook['IS_MEDIA_VIDEO']) {
                $sResult .= ', ' . Loc::getMessage('META_INFO_WATCH') . Loc::getMessage('META_INFO_KW');
            } elseif (true === $this->arBook['IS_MEDIA_AUDIO']) {
                $sResult .= ', ' . Loc::getMessage('META_INFO_LISTEN') . Loc::getMessage('META_INFO_KW');
            }
        }

        $this->arResult['KEYWORDS'] = $sResult;
    }

    /**
     * Получение описания страницы/описания для Schema.org
     * @param bool $bSchema
     */
    private function getDescription ($bSchema = false) {

        $arDescription = $arSchema = [];

        // подзаглавие
        if (!empty($this->arBook['subtitle'])) {
            $arDescription[] = Loc::getMessage('META_INFO_SUBTITLE') . ': ' . $this->arBook['subtitle'];
        }

        // ответственный
        if (!empty($this->arBook['responsibility'])) {
            $arDescription[] = Loc::getMessage('META_INFO_RESPONSIBILITY') . ': ' . $this->arBook['responsibility'];
        }

        // место издания
        if (!empty($this->arBook['publishplace']) && !$bSchema) {
            $arDescription[] = Loc::getMessage('META_INFO_PUBLISH_PLACE') . ': ' . $this->arBook['publishplace'];
        }

        // издательство
        if (!empty($this->arBook['publisher']) && !$bSchema) {
            $arDescription[] = Loc::getMessage('META_INFO_PUBLISH') . ': ' . $this->arBook['publisher'];
        }

        // год издания
        if (!empty($this->arBook['publishyear']) && !$bSchema) {
            $arDescription[] = Loc::getMessage('META_INFO_YEAR') . ': ' . $this->arBook['publishyear'];
        }

        // количество страниц
        if (!empty($this->arBook['countpages']) && !$bSchema) {
            $arDescription[] = Loc::getMessage('META_INFO_PAGES') . ': ' . $this->arBook['countpages'];
        }

        // серия
        if (!empty($this->arBook['series'])) {
            $arDescription[] = Loc::getMessage('META_INFO_SERIE') . ': ' . $this->arBook['series'];
        }

        // isbn
        if (!empty($this->arBook['isbn']) && !$bSchema) {
            $arDescription[] = 'ISBN: ' . $this->arBook['isbn'];
        }

        // issn
        if (!empty($this->arBook['issn'])) {
            $arDescription[] = 'ISSN: ' . $this->arBook['issn'];
        }

        // общее примечание
        if (!empty($this->arBook['contentnotes'])) {
            $arDescription[] = Loc::getMessage('META_INFO_NOTE_CONTENT') . ': ' . $this->arBook['contentnotes'];
        }

        // примечание содержания
        if (!empty($this->arBook['notes'])) {
            $arDescription[] = Loc::getMessage('META_INFO_BOOK_NOTE') . ': ' . $this->arBook['notes'];
        }

        // аннотация
        if (!empty($this->arBook['annotation'])) {
            $arDescription[] = Loc::getMessage('META_INFO_ANNOTATION') . ': ' . $this->arBook['annotation'];
        }

        // ббк
        if (!empty($this->arBook['bbk'])) {
            $arDescription[] = Loc::getMessage('META_INFO_BBK') . ': ' .  $this->arBook['bbk'];
        }

        // удк
        if (!empty($this->arBook['udk'])) {
            $arDescription[] = Loc::getMessage('META_INFO_UDC') . ': ' .  $this->arBook['udk'];
        }

        // редакция
        if (!empty($this->arBook['edition']) && !$bSchema) {
            $arDescription[] = Loc::getMessage('META_INFO_EDITION') . ': ' .  $this->arBook['edition'];
        }

        // библиотека
        if (!empty($this->arBook['library'])) {
            $arDescription[] = Loc::getMessage('META_INFO_LIB') . ': ' .  $this->arBook['library'];
        }

        // язык
        if (!empty($this->arBook['lang']) && !$bSchema) {
            $arDescription[] = Loc::getMessage('META_INFO_LANGUAGE') . ': ' .  $this->arBook['lang'];
        }

        // АРХИВЫ

        // номер фонда
        if (!empty($this->arBook['fundnumber'])) {
            $arDescription[] = Loc::getMessage('META_INFO_FUND_NUMBER') . ': ' .  $this->arBook['fundnumber'];
        }

        // заголовок описи
        if (!empty($this->arBook['inventorynumber'])) {
            $arDescription[] = Loc::getMessage('META_INFO_FUND_NUMBER') . ': ' .  $this->arBook['inventorynumber'];
        }

        // номер описи
        if (!empty($this->arBook['inventoryname'])) {
            $arDescription[] = Loc::getMessage('META_INFO_INVENTORY_NAME') . ': ' .  $this->arBook['inventoryname'];
        }

        // номер дела
        if (!empty($this->arBook['unitnumber'])) {
            $arDescription[] = Loc::getMessage('META_INFO_UNIT_NUMBER') . ': ' .  $this->arBook['unitnumber'];
        }

        // заголовок дела
        if (!empty($this->arBook['unitname'])) {
            $arDescription[] = Loc::getMessage('META_INFO_UNIT_NAME') . ': ' .  $this->arBook['unitname'];
        }

        // даты
        if (!empty($this->arBook['dates'])) {
            $arDescription[] = Loc::getMessage('META_INFO_DATES') . ': ' .  $this->arBook['dates'];
        }

        // МУЗЕИ

        // описание предмета
        if (!empty($this->arBook['description'])) {
            $arDescription[] = Loc::getMessage('META_INFO_DESCRIPTION') . ': ' .  $this->arBook['description'];
        }

        // материал
        if (!empty($this->arBook['material'])) {
            $arDescription[] = Loc::getMessage('META_INFO_MATERIAL') . ': ' .  $this->arBook['material'];
        }

        // размеры
        if (!empty($this->arBook['size'])) {
            $arDescription[] = Loc::getMessage('META_INFO_SIZE') . ': ' .  $this->arBook['size'];
        }

        // ПРАВООБЛАДАТЕЛЬ

        // мпк
        if (!empty($this->arBook['mpk'])) {
            $arDescription[] = Loc::getMessage('META_INFO_MPK') . ': ' .  $this->arBook['mpk'];
        }

        // заявитель
        if (!empty($this->arBook['declarant'])) {
            $arDescription[] = Loc::getMessage('META_INFO_DECLARANT') . ': ' .  $this->arBook['declarant'];
        }

        // патентообладатель
        if (!empty($this->arBook['patentholder']) && !$bSchema) {
            $arDescription[] = Loc::getMessage('META_INFO_PATENTHOLDER') . ': ' .  $this->arBook['patentholder'];
        }

        // представитель
        if (!empty($this->arBook['representative'])) {
            $arDescription[] = Loc::getMessage('META_INFO_REPRESENTATIVE') . ': ' .  $this->arBook['representative'];
        }

        // дата публикации
        if (!empty($this->arBook['publish_date'])) {
            $arDescription[] = Loc::getMessage('META_INFO_PUBLISH_DATE') . ': ' .  $this->arBook['publish_date'];
        }

        // Код вида документа
        if (!empty($this->arBook['patentcode'])) {
            $arDescription[] = Loc::getMessage('META_INFO_PATENTCODE') . ': ' .  $this->arBook['patentcode'];
        }

        // Коллекция
        if (!empty($this->arBook['collection_new'])) {
            $arDescription[] = Loc::getMessage('META_INFO_COLLECTION') . ': ' .  $this->getCollectionName();
        }

        $this->arResult[$bSchema ? 'SCHEMA_DESCRIPTION' : 'DESCRIPTION']
            = htmlspecialchars(implode(', ', $arDescription) ? : $this->arBook['title']);
    }

    /**
     * Формирование авторов - если фамилии автора нет в title, то добавлять в title
     *
     * @return string
     */
    private function getAuthors () {

        $arAuthors = [];
        $arAuthor = BookUtils::getAuthorsSurname($this->arBook['authorbook_for_link']);
        foreach ($arAuthor as $sFullName => $sSurname) {
            if (false === mb_stripos($this->arBook['title'], $sSurname)) {
                $arAuthors[] = $sFullName;
            }
        }
        if (empty($arAuthors)) {
            $arAuthors[] = trim($this->arBook['responsibility']);
        }

        return implode(', ', $arAuthors);
    }

    /**
     * Слова для продвижения для страницы карточки издания
     *
     * @return string
     */
    private function getSeoTitle () {

        $sSeo = '';
        if (isset($this->arBook['fileExt']) && !empty($this->arBook['fileExt'])) {
            if ('Y' == $this->arBook['IS_READ']
                && in_array('pdf', $this->arBook['fileExt']) && 1 > $this->arBook['isprotected']) // TODO: ? epub и литрес
            {
                // поля патентных документов
                if ($this->isPatent()) {
                    $sSeo .= Loc::getMessage('META_INFO_PATENT') . ', ' . Loc::getMessage('META_INFO_PATENT_DL');
                }
                // прочие издания
                else {
                    $sSeo .= Loc::getMessage('META_INFO_READ');
                }
            } elseif (true === $this->arBook['IS_MEDIA_VIDEO']) {
                $sSeo .= Loc::getMessage('META_INFO_WATCH');
            } elseif (true === $this->arBook['IS_MEDIA_AUDIO']) {
                $sSeo .= Loc::getMessage('META_INFO_LISTEN');
            }
        }

        return $sSeo;
    }

    /**
     * Получает название поисковой коллекции
     *
     * @return string
     */
    private function getCollectionName() {
        return trim(str_replace(['[', ']'], '', $this->arBook['collection_new']));
    }

    private function isPatent() {
        return !empty($this->arBook['mpk']) || !empty($this->arBook['declarant'])
            || !empty($this->arBook['patentholder']) || !empty($this->arBook['patentcode']);
    }
}