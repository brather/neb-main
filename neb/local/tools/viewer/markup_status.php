<?
/**
 * Изменение статуса произведения в издании в открытом просмотровщике
 */
define('STOP_STATISTICS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\RslViewer;

global $USER;
if (!$USER->IsAuthorized() || !Loader::includeModule('nota.exalead'))
    exit();

$obUser       = new nebUser($USER->GetID());
$arToken      = $obUser->getToken();
$arUserGroups = $obUser->getUserGroups();

$sBookId  = trim($_REQUEST['book_id']);
$sStatus  = strtolower(trim($_REQUEST['status']));
$arParams = ['token' => $arToken['UF_TOKEN']];

if (empty($sBookId) || empty($sStatus) || empty($arParams['token']))
    exit();

/*
 * Проверка пользователя:
 * 1. Обязательна принадлежность группе модуля разметки изданий
 * 2. Если текущий пользователь только от группы редакторов, то разрешить ему только удалять издания
 */
if (!in_array(UGROUP_MARKUP_APPROVER, $arUserGroups) && (!in_array(UGROUP_MARKUP_EDITOR, $arUserGroups)
                                || (in_array(UGROUP_MARKUP_EDITOR, $arUserGroups) && $sStatus != 'delete'))
) {
    exit();
}

$obRslViewer = new RslViewer($sBookId);
$arResult    = $obRslViewer->changeMarkupStatus($sStatus, $arParams);

MainHelper::showJson($arResult);