<?php

CBitrixComponent::includeComponentClass('rest:rest.server');

use \Bitrix\Main\Application,
    \Bitrix\Main\UserTable,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\Main\Entity,
    \Bitrix\Main\Type\Date;

/**
 * Компонент организует API для получения данных о библиотеках
 *
 * Class RestLibrary
 */
class RestLibrary extends RestServer
{
    /**
     * @var array
     */
    protected $arUser  = [];

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams()
    {
        return $this;
    }

    /**
     * @return int
     */
    public function getAction()
    {
        $this->switchAction();
        return 200;
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    protected static function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Проверка токена пользователя и принадлежность его к группам глобального оператора или оператора эчз
     */
    protected function checkUser() {

        $this->arUser = UserTable::getList([
            'filter' => ['ACTIVE' => 'Y', 'UF_TOKEN' => $this->_params['token']],
            'select' => ['ID', 'EMAIL', 'UF_LIBRARY', 'UF_WCHZ']
        ])->fetch();

        if (empty($this->arUser))
            $this->restError(401, GetMessage('USER_NOT_FOUND'));
    }

    /**
     * Выбор действия
     *
     * @throws Exception
     */
    private function switchAction() {
        $arRequest = static::getRequest();
        switch ($arRequest['action']) {
            case 'check':
                $this->checkLibrary();
                break;
            default:
                $this->checkUser();
                $this->getLibrary();
        }
    }

    /**
     * Получение настроек библиотеки по её идентификатору (#19549)
     * @return bool
     * @throws \Bitrix\Main\LoaderException
     */
    protected function checkLibrary() {

        if (!Loader::includeModule('iblock'))
            return false;

        $arRequest = static::getRequest();

        $idLibrary = intval($arRequest['idlibrary']);

        $obIBlockElement = new CIBlockElement();
        $arLibrary = $obIBlockElement->GetList(
            [],
            [
                'IBLOCK_ID'             => IBLOCK_ID_LIBRARY,
                'PROPERTY_LIBRARY_LINK' => $idLibrary
            ],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'ACTIVE', 'PROPERTY_IS_CHECK_IP', 'PROPERTY_IS_CHECK_FINGERPRINT']
        )->Fetch();

        if ($arLibrary['ID']) {

            $this->arResult = [
                'id'                 => $arLibrary['ID'],
                'ischeckip'          => $arLibrary['PROPERTY_IS_CHECK_IP_VALUE'] == 'Y',
                'ischeckfingerprint' => $arLibrary['PROPERTY_IS_CHECK_FINGERPRINT_VALUE'] == 'Y'
            ];
            return 200;
        } else {

            $this->restError(404, 'Library not found!');
        }
    }

    /**
     * Получение списка библиотек за период
     *
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    protected function getLibrary()
    {
        Loader::includeModule('highloadblock');

        $arRequest = static::getRequest();

        $from = !empty($arRequest['from']) ? trim($arRequest['from']) : false;
        $to   = !empty($arRequest['to'])   ? trim($arRequest['to'])   : false;

        $hlblock = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libs    = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

        //получим общее число библиотек с установленной датой подключения
        $libsCount = $libs::getList(array(
            'select'  => Array('CNT'),
            'filter'  => Array('!=UF_DATE_JOIN' => null),
            'runtime' => Array(
                new Entity\ExpressionField('CNT', 'COUNT(*)')
            )
        ));

        $result['TOTAL_COUNT'] = $libsCount->fetch();
        $result['TOTAL_COUNT'] = $result['TOTAL_COUNT']['CNT'];

        //7.1. Список библиотек - участников проекта за дату.
        $joinedLibsDB = $libs::getList(array(
            'select' => Array('ID', 'NAME' => 'UF_NAME', 'DATE_JOIN' => 'UF_DATE_JOIN'),
            'filter' => Array('>=UF_DATE_JOIN' => new Date($from), '<=UF_DATE_JOIN' => new Date($to)),
        ));
        while($res = $joinedLibsDB->fetch()){
            $result['TOTAL_JOINED_COUNT']++;
            $res['DATE_JOIN'] = $res['DATE_JOIN']->format('d.m.Y');
            $result['JOINED_LIBS'][] = $res;
        }
        $result['TOTAL_JOINED_PERCENT_COUNT'] = round(($result['TOTAL_JOINED_COUNT']/$result['TOTAL_COUNT']) * 100);

        //7.2. 7.3 группировка по месяцу
        $from_sql = date('Y-m-d', strtotime($from));
        $to_sql   = date('Y-m-d', strtotime($to));

        $strSql = "
            SELECT
                MONTH(UF_DATE_JOIN) as MONTH,
                COUNT(ID) as COUNT FROM neb_libs
            WHERE
                (UF_DATE_JOIN BETWEEN '" . $from_sql . "' AND '".$to_sql."')
            GROUP BY
                MONTH(UF_DATE_JOIN)
        ";
        $resultSql = Application::getConnection()->query($strSql);
        while($res = $resultSql->fetch()){
            $res['PERCENT_COUNT'] = round(($res['COUNT'] / $result['TOTAL_COUNT']) * 100);
            $result['JOINED_LIBS_BY_MONTH'][] = $res;
        }

        $this->arResult = Array($result);
        return 200;
    }
}