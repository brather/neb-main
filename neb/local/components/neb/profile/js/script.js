/**
 * Created by VAndryushchenko on 16.08.2016.
 */

var FRONT = FRONT || {};

var mouseDownEl, mouseUpEl, selectionContent;

function getSelectionText() {
    var txt = '';
    if (txt = window.getSelection) {// Не IE, используем метод getSelection
        txt = window.getSelection().toString();
    } else { // IE, используем объект selection
        txt = document.selection.createRange().text;
    }
    return txt;
}

function getSelectionBoundaryElement(isStart) {
    var range, sel, container;
    if (document.selection) {
        range = document.selection.createRange();
        range.collapse(isStart);
        return range.parentElement();
    } else {
        sel = window.getSelection();
        if (sel.getRangeAt) {
            if (sel.rangeCount > 0) {
                range = sel.getRangeAt(0);
            }
        } else {
            // Old WebKit
            range = document.createRange();
            range.setStart(sel.anchorNode, sel.anchorOffset);
            range.setEnd(sel.focusNode, sel.focusOffset);

            // Handle the case when the selection was selected backwards (from the end to the start in the document)
            if (range.collapsed !== sel.isCollapsed) {
                range.setStart(sel.focusNode, sel.focusOffset);
                range.setEnd(sel.anchorNode, sel.anchorOffset);
            }
        }

        if (range) {
            container = range[isStart ? "startContainer" : "endContainer"];

            // Check if the container is a text node and return its parent if so
            return container.nodeType === 3 ? container.parentNode : container;
        }
    }
}
function getSelectedNode() {
    if (document.selection)
        return document.selection.createRange().parentElement();
    else
    {
        var selection = window.getSelection();
        if (selection.rangeCount > 0)
            return selection.getRangeAt(0).startContainer.parentNode;
    }
}


$(function(){

    $(document).on('mousedown',function(e){
        mouseDownEl = $(e.target).get(0);
    });


    $(document).on('mouseup', function(e){
        mouseUpEl = $(e.target).get(0);
        //тут надо сделать чтобы содержимое с выделенным текстом не схлопывалось при маусауте

        var selectedTextStartParentNode = getSelectionBoundaryElement(true);
        var selectedTextEndParentNode = getSelectionBoundaryElement(false);
        var hoveredContainer;

        if ( $(mouseDownEl).is(selectedTextStartParentNode) && $(mouseDownEl).is(selectedTextEndParentNode) ) {
            //console.log ( 'текст выделен внутри ' + $(mouseDownEl).prop('tagName') );
            //в разметке содержимое в элементе с [data-view-detail-message]
        }

        if ( getSelectionText() ) {
            //console.log('yo');
        }

        var sel = $(selectedTextStartParentNode);
    });



});