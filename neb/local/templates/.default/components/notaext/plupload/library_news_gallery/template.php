<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

\CUtil::InitJSCore(array("jquery"));
$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$files = array();
if(isset($_POST['GALLERY'])) {
	foreach($_POST['GALLERY'] as $item) {
		if(isset($item['VALUE']) && !empty($item['VALUE'])) {
			$files[] = $item;
		}
	}
}
?>

<div class="fieldcell nowrap photorow gallery_list">
	<label><?=GetMessage('PLUPLOAD_LIBRARY_NEWS_GALLERY_ATTACH');?></label>
	<div class="field validate" id="drag_container">
		<div class="setscan iblock" id="container_<?=$arParams['RAND_STR']?>">
			<input type="hidden" class="photofile" name="GALLERY[]" value="">
			<a href="#" id="setphoto_GALLERY"><?=GetMessage('PLUPLOAD_LIBRARY_NEWS_GALLERY_LOAD');?></a>
			<div class="setphoto_lb"><?=GetMessage('PLUPLOAD_LIBRARY_NEWS_GALLERY_DRAG');?></div>
		</div>
	</div>
	<div class="b_photobl dummy row" style="<?=isset($files[0])?'':'display:none;'?>">
		<div class="b_photo col-xs-3">
			<img src="<?=isset($files[0])?$files[0]['VALUE']:''?>" alt="" class="uploaded_img" style="max-width: 100%;">
			<div class="progressbar" style="display: none;">
				<div class="progress">
					<span style="width:0%;"></span>
				</div>
				<div class="text"><?=GetMessage('PLUPLOAD_LIBRARY_NEWS_GALLERY_LOADING');?> <span class="num">0</span>%</div>
			</div>
		</div>
		<div class="descrphoto col-xs-9">
			<label for="settings13"><?=GetMessage('PLUPLOAD_LIBRARY_NEWS_GALLERY_DESCRIPTION');?></label>
			<div class="field validate">
				<input type="hidden" name="GALLERY[][VALUE]" class="gal_val" value="<?=isset($files[0])?$files[0]['VALUE']:''?>">
				<textarea class="input gal_desc form-control" data-maxlength="1500" name="GALLERY[][DESCRIPTION]"><?=isset($files[0])?$files[0]['DESCRIPTION']:''?></textarea>
				<label class="checkbox">
					<input class="checkbox gal_del custom" type="checkbox" name="GALLERY[][del]" value="Y">
					<span class="lbl"><?=GetMessage('PLUPLOAD_LIBRARY_NEWS_GALLERY_DELETE');?></span>
				</label>
			</div>
		</div>
	</div>
	<? if ( !empty( $arParams['GALLERY'] ) ) { ?>
		<? foreach ( $arParams['GALLERY'] as $valID => $arPhoto ) { ?>
			<div class="b_photobl">
				<div class="b_photo col-xs-3">
					<img src="<?=$arPhoto['SRC']?>" alt="photo" style="max-width: 100%;">
				</div>
				<div class="descrphoto col-xs-9">
					<label for="settings13"><?=GetMessage('PLUPLOAD_LIBRARY_NEWS_GALLERY_DESCRIPTION');?></label>
					<div class="field validate">
						<input type="hidden" name="GALLERY_old[<?=$valID?>][VALUE]" value="<?=$arPhoto['ID']?>">
						<textarea class="input form-control" data-minlength="2" data-maxlength="1500" id="settings13" name="GALLERY_old[<?=$valID?>][DESCRIPTION]"><?=$arPhoto['DESCRIPTION']?></textarea>
						<label class="checkbox">
							<input class="gal_del checkbox" type="checkbox" name="GALLERY_old[<?=$valID?>][del]" value="Y">
							<span class="lbl"><?=GetMessage('PLUPLOAD_LIBRARY_NEWS_GALLERY_DELETE');?></span>
						</label>
					</div>
				</div>
			</div>
		<? } ?>
	<? } ?>
</div>

<div style="display: none;" class="pl_button"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>

<script type="text/javascript">
	$(function() {

		var newFilesCounter = 0;
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'setphoto_GALLERY',
				drop_element: "drag_container",
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'),
				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
						unique_names: true,
					<?
					}
				?>

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
					,
					mime_types: [
						{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
					]
					<?
					}
				?>
				},
				<? if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y'){?>
				resize: {
					width: <?=intval($arParams['RESIZE_WIDTH'])?>,
					height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
					<? if($arParams['RESIZE_CROP'] == 'Y') { ?>
						crop: true,
					<? } ?>
					quality: <?=intval($arParams['RESIZE_QUALITY'])?>
				},
				<? } ?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '100kb',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'

				},
				multi_selection: true,
				init: {
					FilesAdded: function(up, files) {
						plupload.each(files, function(file) {
							/* Берём болванку */
							var dummyClone = $('.b_photobl.dummy').clone(true);

							/* копируем болванку и вставляем */
							dummyClone.appendTo('.gallery_list').removeClass('dummy').attr('id', file.id).show();
							$('.gal_val', dummyClone).attr('name', 'GALLERY[n'+newFilesCounter+'][VALUE]');
							$('.gal_desc', dummyClone).attr('name', 'GALLERY[n'+newFilesCounter+'][DESCRIPTION]');
							$('.gal_del', dummyClone).attr('name', 'GALLERY[n'+newFilesCounter+'][del]').removeClass('custom');/*.replaceCheckBox();*/
							$('.progressbar', dummyClone).show();

							newFilesCounter++;
						});
						up.start();
					},
					UploadProgress: function(up, file) {
						var curProgress = $('#'+file.id+' .progressbar');
						curProgress.show(); /* На всякий, уже не нужо */
						$('.progress span', curProgress).css('width', file.percent+'%');
						$('.num', curProgress).html(file.percent);
					},
					FileUploaded: function(up, file, response) {
						var result = response.response;
						if (result) {
							var obResponse = JSON.parse(result);
							$('#'+file.id+' .gal_val').val(obResponse.file);
							$('#'+file.id+' .uploaded_img').attr('src', obResponse.file);

							/* Прогресс прячем */
							$('#'+file.id+' .progressbar').hide();
						}
					},
					Error: function(up, err) {
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
					}
				}
		});

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>


