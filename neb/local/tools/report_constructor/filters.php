<?
/**
 * Вкладка с фильтрами
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!TblEntities::checkAccess()) {
    die('Permissions denied!');
}

if ($_REQUEST['entities']){
$compare = TblEntities::getCompareType();
?>
    <div class="filters-wrap">
        <div class="row steps__row clearfix">
            <div class="form-group col-sm-3">
                <select id="" class="js-select filter-select form-control" name="filter[]" data-select-picker  data-input-name="value_compare[]">
                    <?foreach ($_REQUEST['entities'] as $item):
                    $res = TblEntities::getEntities($item);
                    $arFields = TblEntities::getEntityFields($item);?>
                    <?foreach ($arFields as $field):?>
                    <option value='<?=$field['Id']?>' type="<?=$field['FieldType']?>"><?=$res[$item];?> - <?=$field['Description']?></option>
                    <?endforeach;?>
                    <?endforeach;?>
                </select>
            </div>
            <div class="form-group col-sm-3">
                <select id="" class="js-select form-control" name="compare[]" data-select-picker>
                    <?foreach ($compare as $id=>$name):?>
                    <option value=<?=$id?>><?=$name?></option>
                    <?endforeach;?>
                </select>
            </div>

            <div class="form-group col-sm-3 input-cont">
            </div>

            <div class="form-group col-sm-1">
                <a class="btn btn-default add-next-row">+</a>
                <a class="btn btn-default del-this-row">−</a>
            </div>
        </div>

    </div>
<?
}
?>
<div class="filters-wrap-tmpl"></div>

<script src="/local/templates/adaptive/js/jquery.mask.min.js"></script>
<script>
	$(function () {
		window.rowTmpl = $('.filters-wrap').html();
	});
</script>