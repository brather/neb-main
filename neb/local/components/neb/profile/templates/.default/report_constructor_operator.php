﻿<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {

    die();
}

$APPLICATION->SetTitle('Отчеты');

/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
$APPLICATION->IncludeComponent(
    'neb:report.constructor.list',
    '',
    [],
    $component
);