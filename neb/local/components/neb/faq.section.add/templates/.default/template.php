<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;
Loc::loadMessages(__FILE__);
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();?>
<div class="container-fluid">
    <?if(!empty($arResult["ERROR"])) {?>
        <div class="row block-mess bg-danger">
            <h4 class="text-danger"><?= $arResult["ERROR"] ?></h4>
        </div>
        <div class="row"><hr></div>
    <?}?>
    <?if(!empty($arResult["SUCCESS"])) {?>
        <div class="row block-mess bg-success">
            <h4 class="text-success"><?= $arResult["SUCCESS"] ?></h4>
        </div>
        <div class="row"><hr></div>
    <?}?>
    <div class="row">
        <form action="<?= $arResult["FORM_URL"] ?>" method="POST" class="nrf">
            <input type="hidden" name="CONTROL_ADD_SECTION" value="Y" />
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="NAME_SECTION">
                        <?= Loc::getMessage("NAME_SECTION"); ?>
                    </label>
                    <div class="col-lg-9">
                        <input type="text"
                               class="form-control"
                               name="NAME_SECTION"
                               id="NAME_SECTION"
                               placeholder="<?= Loc::getMessage("INPUT_NAME_SECTION") ?>"
                               value="<?= $arRequest["NAME_SECTION"] ?>"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="SORT_SECTION">
                        <?= Loc::getMessage("SORT_SECTION"); ?>
                    </label>
                    <div class="col-lg-9">
                        <input type="text"
                               class="form-control"
                               name="SORT_SECTION"
                               id="SORT_SECTION"
                               placeholder="<?= Loc::getMessage("INPUT_SORT_SECTION") ?>"
                               value="<?= $arRequest["SORT_SECTION"] ?>"/>
                    </div>
                </div>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary">
                    <?= Loc::getMessage("ADD_SECTION") ?>
                </button>
            </div>
        </form>
    </div>
</div>