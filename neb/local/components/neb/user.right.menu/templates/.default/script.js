$(function(){
	var sideAddTagLink;
	$(document).on('click', '[data-tag-side-widget] [data-tag-edit]', function(e){
		e.preventDefault();
		var editButton = $(this),
			itemControl = editButton.closest('[data-tag-side-item]'),
			item = itemControl.find('[data-tag-title]'),
			itemTitle = item.text(),
			itemId = itemControl.data('tag-side-item'),
			input = $('<input/>').attr('type','text').addClass('form-control').val(itemTitle).attr('data-tag-input',''),
			sideWidget = item.closest('[data-tag-side-widget]'),
			saveChangesButton = itemControl.find('[data-tag-rename]'),
			removeURL = sideWidget.data('remove-url'),
			editURL = sideWidget.data('edit-url');

		editButton.attr('disabled','disabled');
		
		if ( $(e.target).is(editButton) ) {
			itemControl.prepend(input).toggleClass('edit-process');
			input
				.on('keydown',function(e){
					if (e.which == 13) {
				        saveChanges();
				    }
				    if (e.which == 27) {
				        cancelChanges();
				    }
				})
				.focus();
		}

		function saveChanges() {
			var value = input.val();
			if (itemTitle != value) {
				$.ajax({
					method: 'POST',
					url: editURL,
					data: {
						name: value,
						id: itemId
					},
					beforeSend: function(){
						input.attr('disabled','disabled');
						saveChangesButton.attr('disabled','disabled');
					},
					success: function(data){
						if (data == 'true') {
							item.text(value);
						}
					}
				})
				.always(function(){
					$(document).off('click.edittag'+itemId);
					input.closest('[data-tag-side-item]').toggleClass('edit-process',false).end().remove();
					saveChangesButton.removeAttr('disabled');
					editButton.removeAttr('disabled');
				});				
			} else {
				$(document).off('click.edittag'+itemId);
				input.closest('[data-tag-side-item]').toggleClass('edit-process',false).end().remove();
				editButton.removeAttr('disabled');
			}
		}

		function cancelChanges() {
			$(document).off('click.edittag'+itemId);
			input.closest('[data-tag-side-item]').toggleClass('edit-process',false).end().remove();
			editButton.removeAttr('disabled');
		}

		$(document).on('click.edittag'+itemId, function(e){
			var allChilds = $(itemControl).find('*');
			if ( $(e.target).is(itemControl) || $(e.target).is(allChilds) ) {//inside
				if ( $(e.target).is(saveChangesButton) ) {
					saveChanges();
				}
			} else {//outside
				cancelChanges();
			}
		});
	})
	.on('click','[data-tag-side-widget] [data-tag-delete]', function(e){
		e.preventDefault();
		var deleteButton = $(this),
			itemControl = deleteButton.closest('[data-tag-side-item]'),
			sideWidget = itemControl.closest('[data-tag-side-widget]'),
			removeURL = sideWidget.data('remove-url'),
			itemId = itemControl.data('tag-side-item'),
			item = itemControl.find('[data-tag-title]'),
			itemTitle = item.text(),
			itemSpan = $('<span/>').text(itemTitle),
			postData = {
				"right_menu_collections[]": itemId
			},
			message = {
				title: "Удалить подборку?",
				confirmTitle: "Удалить"
			};
		itemControl.addClass('delete-process');


		$.when( FRONT.confirm.handle(message,deleteButton) ).then(function(confirmed){
            if(confirmed){
					$.ajax({
						method: 'POST',
						url: removeURL,
						data: postData,
						success: function(data){
							if (data =='') {//success responce
								itemControl.remove();
							}
						}
					});
            } else {
            	itemControl.removeClass('delete-process');
            }
        });
	})
	.off('click','[data-tag-side-widget] [data-tag-edit][disabled]')
	.on('click','[data-add-tag-widget-placeholder] a', function(e){
		e.preventDefault();
		var wrapper = $(this).closest('[data-add-tag-widget-placeholder]');
		sideAddTagLink = $(this);//parent variable
		var addForm = FRONT.formutils.addTagWidget();
		$(wrapper).html( addForm );
		$('input[type="text"]', addForm).focus();
		addForm.on('submit',function(e){
			if (!e.result) {//форма не пуста
				addForm.fadeOut(function(){
					this.remove();
				});
			}
		});
	})
	.on('refreshed','[data-tag-side-widget]',function(){
		// console.log('triggered refreshed');
		$(this).next('[data-add-tag-widget-placeholder]').html(sideAddTagLink);
	});

});


// $(function() {
// 	$('.right_menu_collections .btremove').click(function() {
// 		var form = $(this).closest('.right_menu_collections').find('form');

// 		$.ajax({
// 			url: form.attr('action'),
// 			method: 'POST',
// 			data: form.serialize(),
// 			success: function(html){
// 				reloadRightMenuBlock();
// 			}
// 		}); 
// 	});

//     $('.edit-form form').submit(function(e) {
//         var that = $(this),
//             id = that.find('input.idValue').val();
//         e.preventDefault();
//         $.ajax({
//             url: that.attr('action'),
//             method: 'POST',
//             data: that.serialize(),
//             success: function(data){
//                 if('true' === data) {
//                     that
//                         .parent()
//                         .parent()
//                         .find('.list-collections-menu input.checkbox[value="' + id + '"]')
//                         .parents('.checkwrapper')
//                         .find('.b-libfilter_name')
//                         .text(that.find('input[name="name"]').val());
//                 }
//             }
//         });
//     });
// });

// function reloadRightMenuBlock(){
// 	$('.b-libfilter_remove:visible').hide();
// 	$.ajax({
// 		//url: '/local/components/neb/user.right.menu/templates/.default/reload.php',
// 		url: window.location.href,
// 		method: 'GET',
// 		data: {
// 		reloadRightCollection : 'Y'
// 		//, type: $('.right_menu_collections').data('type')
// 		},
// 		success: function(html){
// 			$('.right_menu_collections .list-collections-menu').html(html);
// 			$('input.checkbox:not(".custom")').replaceCheckBox();
// 			$('.b-libfilter').libFilter();
// 		}
// 	}); 
// }