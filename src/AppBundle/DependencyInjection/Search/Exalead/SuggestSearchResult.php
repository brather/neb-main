<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 20:01
 */

namespace AppBundle\DependencyInjection\Search\Exalead;


use AppBundle\Entity\SearchResultEntityInterface;
use AppBundle\Traits\ExaleadParseResultTrait;

class SuggestSearchResult extends SearchResult
{
    use ExaleadParseResultTrait;

    /**
     * @return SearchResultEntityInterface[]
     */
    public function buildEntities()
    {
        $entities = [];

        return $entities;
    }
}