<?php

use \Neb\Main\Helper\MainHelper,
    \Neb\Main\Helper\UtilsHelper;

/**
 * Класс для работы с Геокодером Google
 * Определяет географические координаты объекта по его адресу и наоборот
 * https://developers.google.com/maps/documentation/geocoding/intro?hl=ru
 */

class GeocodeGoogle
{
    /**
     * Получение кооординат объекта по адресу (прямое геокодирование)
     * 
     * @param $sAddress - адрес
     * @param string $sKey - ключ  API
     * @param string $sLanguage - язык, на котором выводятся результаты
     * @param string $sRegion - код региона, указываемый как значение ccTLD ("домен верхнего уровня") из двух символов.
     * @param string $sComponents - фильтры компонентов, разделяемые вертикальной чертой (|).
     * @param string $sFormat - формат запроса
     * @return array|null]
     */
    public static function getByAddress($sAddress, $sKey = '', $sLanguage = 'ru', $sRegion = 'ru', $sComponents = '', $sFormat = 'json') {

        if (empty($sAddress))
            return null;

        $sUrl = 'https://maps.google.com/maps/api/geocode/'.$sFormat.'?address='.urlencode($sAddress);

        // ключ к платной версии API
        if (!empty($sKey))
            $sUrl .= '&key='.urlencode($sKey);

        // язык
        if ($sLanguage)
            $sUrl .= '&language=' . urlencode($sLanguage);

        // регион
        if ($sRegion)
            $sUrl .= '&region=' . urlencode($sRegion);

        // компоненты
        if ($sComponents)
            $sUrl .= '&components=' . urlencode($sComponents);

        $sResponse  = self::getContentByCurl($sUrl);
        $arResponse = UtilsHelper::convertXmlJsonToArray($sResponse);
        $arResult   = self::parseResponse($arResponse, $sAddress);

        return $arResult;
    }

    /**
     * Получение объекта по координатам (обратное геокодирование)
     *
     * @param $sLat - широта (лангитуда)
     * @param $sLong - долгота (лонгитуда)
     * @param string $sPlaceId - идентификатор места
     * @param string $sKey - ключ  API
     * @param string $sLanguage - язык, на котором выводятся результаты
     * @param string $sLocationType - типы мест, разделенные вертикальной чертой (|)
     * @param string $sResultType - типы адресов, разделенные вертикальной чертой (|)
     * @param string $sFormat - формат запроса
     * @return array|null
     */
    public static function getByGeocode($sLat, $sLong, $sPlaceId = '', $sKey = '', $sLanguage = 'ru',
                                        $sLocationType = '', $sResultType = '', $sFormat = 'json') {
        if (empty($sLat) && empty($sLat))
            if (empty($sPlaceId))
                return null;

        $sLatLng = trim($sLat) . ',' . trim($sLong);

        $sUrl = 'https://maps.google.com/maps/api/geocode/'.$sFormat.'?latlng='.urlencode($sLatLng);

        // ключ к платной версии API
        if (!empty($sKey))
            $sUrl .= '&key='.urlencode($sKey);

        // язык
        if ($sLanguage)
            $sUrl .= '&language=' . urlencode($sLanguage);

        // location_type
        if ($sLocationType)
            $sUrl .= '&location_type=' . urlencode($sLocationType);

        // result_type
        if ($sResultType)
            $sUrl .= '&result_type=' . urlencode($sResultType);

        $sResponse  = self::getContentByCurl($sUrl);
        $arResponse = UtilsHelper::convertXmlJsonToArray($sResponse);
        $arResult   = self::parseResponse($arResponse, $sLatLng);

        return $arResult;
    }

    /**
     * Преобразование ответа от сервписа в удобный вид
     *
     * @param $arResponse
     * @param string $sRequest
     * @return array
     */
    private static function parseResponse($arResponse, $sRequest = '') {

        $arResult = [];

        // xml exception
        if (!empty($arResponse['result'])) {
            $arResponse['results'][] = $arResponse['result'];
            $arResponse['result'] = null;
            unset($arResponse['result']);
        }

        // сортировка для удобства чтения массива
        krsort($arResponse);

        // формирование результирующего массива
        foreach ($arResponse as $k => $v) {
            if (stripos($k, 'results') !== false) {
                foreach ($v as $k2 => $v2) {

                    foreach ($v2 as $k3 => $v3) {

                        if (stripos($k3, 'address_component') !== false) {

                            foreach ($v3 as $arItem) {
                                foreach ($arItem['types'] as $sType) {
                                    if ($sType == 'political')
                                        continue;
                                    $arResult['objects'][$k2]['short_address'][$sType] = $arItem['short_name'];
                                    $arResult['objects'][$k2]['long_address'][$sType]  = $arItem['long_name'];
                                }
                            }

                        } elseif (stripos($k3, 'geometry') !== false) {

                            $arResult['objects'][$k2]['location']      = self::getLocation($v3['location']);
                            $arResult['objects'][$k2]['location_info'] = self::getLocationType($v3['location_type']);
                            $arResult['objects'][$k2]['location_type'] = $v3['location_type'];
                            $arResult['objects'][$k2]['viewport']      = array(
                                'northeast' => self::getLocation($v3['viewport']['northeast']),
                                'southwest' => self::getLocation($v3['viewport']['southwest'])
                            );

                        } elseif (stripos($k3, 'type') !== false) {

                            if (is_array($v3))
                                $arResult['objects'][$k2]['types'] = implode(',', $v3);
                            else
                                $arResult['objects'][$k2]['types'] = $v3;
                        }
                        else {
                            $arResult['objects'][$k2][$k3] = $v3;
                        }
                    }
                }
            }
            elseif (stripos($k, 'status') !== false) {
                $arResult['request'] = $sRequest;
                $arResult[$k] = $v;
                $arResult['status_info'] = self::getStatusInfo($v);
                $arResult['found'] = count($arResponse['results']);
            }
            elseif (stripos($k, 'error') !== false) {
                $arResult['error'] = $v;
            }
            else {
                $arResult[$k] = $v;
            }
        }

        return $arResult;
    }

    /**
     * Формирование координат
     *
     * @param $arLocation
     * @return mixed
     */
    private static function getLocation($arLocation) {
        foreach ($arLocation as $k => $v)
            $arLocation[$k] = number_format($v, 6, '.', '');
        return $arLocation;
    }

    /**
     * Получение расшифровки кода статуса ответа
     *
     * @param $sStatus
     * @return string
     */
    private static function getStatusInfo($sStatus) {
        switch (strtoupper($sStatus)) {
            case 'OK':               $sRes = 'Ошибок нет';                            break;
            case 'ZERO_RESULTS':     $sRes = 'Результаты не найдены';                 break;
            case 'OVER_QUERY_LIMIT': $sRes = 'Превышение квоты запросов';             break;
            case 'REQUEST_DENIED':   $sRes = 'Запрос отклонен';                       break;
            case 'INVALID_REQUEST':  $sRes = 'В запросе отсуствуют необходимые поля'; break;
            case 'UNKNOWN_ERROR':    $sRes = 'Ошибка удаленного сервера';             break;
            default:                 $sRes = $sStatus;
        }
        return $sRes;
    }

    /**
     * Получение расшифровки точности результата
     *
     * @param $sLocationType
     * @return string
     */
    private static function getLocationType($sLocationType) {
        switch (strtoupper($sLocationType)) {
            case 'ROOFTOP':            $sRes = 'Точное соответствие';    break;
            case 'RANGE_INTERPOLATED': $sRes = 'Приближенное значение';  break;
            case 'GEOMETRIC_CENTER':   $sRes = 'Геометрический центр';   break;
            case 'APPROXIMATE':        $sRes = 'Приближенный результат'; break;
            default:                   $sRes = $sLocationType;
        }
        return $sRes;
    }

    /**
     * Выполнение HTTP-запроса через cURL
     *
     * @param $sUrl
     * @return mixed
     */
    private static function getContentByCurl($sUrl) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $sUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, MainHelper::checkSslVerification($sUrl));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    /**
     * Выполнение HTTP-запроса через file_get_contents
     *
     * @param $sUrl
     * @return mixed
     */
    private static function getContentByFileGetContents($sUrl) {
        return file_get_contents($sUrl);
    }
}