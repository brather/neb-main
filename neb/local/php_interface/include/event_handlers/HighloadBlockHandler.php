<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 29.03.2017
 * Time: 10:43
 *
 * http://g-rain-design.ru/blog/posts/highloadblock-events/
 */

use \Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler("", "NebLibsOnAfterAdd",    ["HighloadBlockHandler", "updateDateModify"]);
EventManager::getInstance()->addEventHandler("", "NebLibsOnAfterUpdate", ["HighloadBlockHandler", "updateDateModify"]);

class HighloadBlockHandler
{
    public static function updateDateModify(\Bitrix\Main\Entity\Event $event) {

        static $bHandlerStop;
        if($bHandlerStop===true)
            return;

        $ID = $event->getParameter("id");
        if (is_array($ID))
            $ID = $ID["ID"];
        if (!$ID)
            return;

        $entityDataClass = $event->getEntity()->GetDataClass();

        $bHandlerStop = true;
        $result = $entityDataClass::update($ID, ['UF_DATE_MODIFY' => new \Bitrix\Main\Type\DateTime]);
        $bHandlerStop = false;
    }
}