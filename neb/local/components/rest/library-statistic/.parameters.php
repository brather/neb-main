<?php
$fieldsAllow = array(
    'USERS',
    'ACTIVE_USERS',
    'PUBLICATIONS',
    'SEARCH_COUNT',
    'VIEWS',
    'DOWNLOADS',
    'VIEWS_BOOK',
    'PUBLICATIONS_DIGITIZED',
    'PUBLICATIONS_DIGITIZED_DD',
    'PUBLICATIONS_DD',
);
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'   => '/',
                'VARIABLES' => array('token', 'fieldName'),
                'TYPE'      => array('GET'),
            ),
        ),
    ),
);
foreach ($fieldsAllow as $fieldName) {
    $arComponentParameters['PARAMETERS']['SEF_MODE'][strtolower($fieldName)]
        = array(
        'VARIABLES' => array('token', 'exportType', 'dateFrom', 'dateTo'),
        'TYPE'      => array('GET'),
    );
}