<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$arRequest = $component->getRequest();
$APPLICATION->SetTitle(Loc::getMessage('FEEDBACK_APPOINT_TITLE') . $arRequest['id']);

$arRequest = $component->getRequest();

if ($arRequest['ajax'] == 'Y') {
    $component->restartBuffer();
}

if ($arResult['json'] == true) {
    $component->showJson($arResult);
}

?>

<section data-addressates>

    <? foreach($arResult['ERROR'] as $sError): ?>
        <?ShowError($sError);?>
    <? endforeach; ?>

    <form id="appointFormFeedback" method="post" action="<?=$arParams['FEEDBACK_APPOINT_URL']?:$_SERVER['REDIRECT_URL'];
        ?>?id=<?=$arRequest['id']?>&lib=<?=$arRequest['lib']?>&ajax=<?=$arRequest['ajax']?>">

        <input type="hidden" name="FEEDBACK[ACTION]" value="APPOINT" />

        <select id="responsibleLibSelect" class="form-control input-lg"
            data-location-wo-lib="<?=$arParams['FEEDBACK_APPOINT_URL']?:$_SERVER['REDIRECT_URL'];?>?id=<?=$arRequest['id']?>&lib="
            data-select-picker
            data-_select-two
            data-placeholder="Все библиотеки"
            name="FEEDBACK[LIB]"
        >
            <option value="-1" <? if(!isset($_REQUEST['lib'])) echo 'selected' ?>><?=Loc::getMessage('FEEDBACK_APPOINT_SELECT')?></option>
            <?foreach ($arResult['LIBS'] as $lib): ?>
                <option value="<?=$lib['ID']?>"<?= $lib['ID'] == $_REQUEST['lib'] ? 'selected' : ''?>><?=$lib['NAME']?></option>
            <? endforeach; ?>
        </select>
        <? if ($arRequest['ajax'] != 'Y'): ?>
            <script type="text/javascript">
                $(function(){ $(document).on('change','#responsibleLibSelect', function(){
                    location.href = '<?=$arParams['FEEDBACK_APPOINT_URL']?:$_SERVER['REDIRECT_URL'];
                            ?>?id=<?=$arRequest['id']?>&lib=' + $('#responsibleLibSelect').val();
                });});
            </script>
        <? endif; ?>

        <? if (!empty($arResult['USERS'])): ?>
            <dl class="responsive-table-layout biblio-admins responsive-hover">
                <dt>
                    <span>
                        <input type="checkbox" id="checkedAllUser" value="<?=$arItem['ID']?>" />
                         <span class="lbl"></span>
                    </span>
                    <span><a class="sort up"><?=Loc::getMessage('FEEDBACK_APPOINT_NAME')?></a></span>
                    <span><?=Loc::getMessage('FEEDBACK_APPOINT_EMAIL')?></span>
                    <span><?=Loc::getMessage('FEEDBACK_APPOINT_PHONE')?></span>
                </dt>
                <? foreach ($arResult['USERS'] as $arItem): ?>
                    <label>
                        <dd>
                            <span>
                                <input type="checkbox" name="FEEDBACK[USER][]" value="<?=$arItem['ID']?>" <?
                                if (in_array($arItem['ID'], $arRequest['FEEDBACK']['USER'])): ?> checked="checked"<? endif; ?> />
                                <span class="lbl"></span>
                            </span>
                            <span><?=$arItem['LAST_NAME']?> <?=$arItem['NAME']?> <?=$arItem['SECOND_NAME']?></span>
                            <span><a href="mailto:<?=$arItem['EMAIL']?>"><?=$arItem['EMAIL']?></a></span>
                            <span><?=$arItem['WORK_PHONE']?></span>
                        </dd>
                    </label>
                <? endforeach; ?>
            </dl>
        <? endif; ?>
        <br /><input class="btn btn-primary btn-lg" type="submit" value="Назначить" />
    </form>
</section>
    <script type="text/javascript">
        $(function () {
            $('#checkedAllUser').on('click', function () {
                var check = this.checked;
                $('#appointFormFeedback dd input[type=checkbox]').each(function(v,c){
                   if (c.checked !== check) $(c).trigger('click');
                });
            });
        });
    </script>
<?
if ($arRequest['ajax'] == 'Y')
    die();