<?php
CBitrixComponent::includeComponentClass("rest:rest.server");

use \Nota\UserData\Notes as UserNotes,
	\Bitrix\Main\UserTable,
	\Bitrix\Main\Loader;

/**
 * Компонент организует API по приёму, передаче, удалению заметок (комментариев) к страницам книг
 *
 * Class RestBookmarks
 */
class RestNotes extends RestServer
{
	protected $token, $bookID, $numPage, $noteID, $text, $uid, $withImage = true;

	/**
	* Т.к. через ЧПУ мы не обработаем GET, PUT, DELETE - обрабатываем их тут.
	* Но помним уже, что в параметрах у нас стоит ограничение, и другие REQUEST_METHOD не пройдут.
	*/
	public function processRequest($token = null, $bookID = null, $numPage = null, $text = null, $ID = null, $withImage = null)
	{
		if ( isset($token) && !empty($token) )
			$this->token 		= trim($token);

		if ( isset($bookID) && !empty($bookID) )
			$this->bookID 		= trim($bookID);

		if ( isset($numPage) && !empty($numPage) )
			$this->numPage 		= trim($numPage);

		if ( isset($text) && !empty($text) )
			$this->text 		= trim($text);

		if ( isset($ID) && !empty($ID) )
			$this->noteID 	= $ID;

        if (isset($withImage) && strlen($withImage) > 0) {
            $this->withImage = filter_var($withImage, FILTER_VALIDATE_BOOLEAN);
        }

		// Токен нам нужен везде - так что проверить его можно и тут.
		if ( strlen($this->token) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));

		// Теперь сразу найдём uid пользователя
		$arUser = UserTable::getList([
			'filter' => ['ACTIVE' => 'Y', 'UF_TOKEN' => $this->token],
			'select' => ['ID', 'LOGIN']
		])->fetch();

		if ( !$arUser || intval($arUser['ID']) <= 0 || $arUser['LOGIN'] == nebUser::ANONYMOUS_LOGIN )
			return $this->restError(404, GetMessage('USER_NOT_FOUND'));
		else
			$this->uid = $arUser['ID'];

		Loader::includeModule('nota.userdata');

		switch ( $_SERVER['REQUEST_METHOD'] )
		{
			case 'PUT':
				return $this->PUTNotes();
				break;
			case 'GET':
				return $this->GETNotes();
				break;
			case 'DELETE':
				return $this->DELETENotes();
				break;
		}
	}

	/**
	* Находит UID по token
	* Добавляет в избранное указанную книгу
	*/
	protected function PUTNotes()
	{
		if ( !$this->bookID || strlen($this->bookID) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'book_id')));

		if ( !$this->numPage || strlen($this->numPage) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'num_page')));
		if ( !is_numeric($this->numPage) )
			return $this->restError(500, GetMessage('NUM_PAGE_INT'));

		if ( strlen($this->text) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'text')));

		$resultID = UserNotes::add($this->bookID, $this->numPage, $this->text, $this->uid);

		$this->arResult = array('resultID' => (int)$resultID);

		return 200;
	}

	/**
	* Отдаёт список всех книг из избранного
	*/
	protected function GETNotes()
	{
        $result = UserNotes::getNotesList(
            $this->bookID,
            $this->numPage,
            $this->uid,
            $this->withImage
        );
		if($result === false)
			$result = (object) array();

		$this->arResult = $result;

		return 200;
	}

	/**
	* Удаляет из избранного по token
	*/
	protected function DELETENotes()
	{
		if ( !$this->noteID || strlen($this->noteID) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'ID')));

		// Удаляем с предварительным сопоставлением пользователю
		$result = UserNotes::deleteWithTest($this->noteID, $this->uid);

		if ( !$result )
			$this->restError(404, GetMessage('NOT_FOUND'));

		$this->arResult = true;
        
		return 200;
	}
}