<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 13:23
 */

namespace AssessorBundle\Controller\Api;

use AssessorBundle\Entity\Handler\QuerySortRequest;
use AssessorBundle\Entity\TblQueryFrequency;
use AssessorBundle\Entity\TblQueryRating;
use AssessorBundle\Form\QuerySortRequestType;
use AssessorBundle\Form\TblQueryRatingType;
use AssessorBundle\Traits\AssessorRatingTrait;
use AppBundle\DependencyInjection\NebSearcher;
use BitrixBundle\Annotations\IncludeBitrix;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\View\View;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ApiQueryRatingController
 *
 * @DI\Service("assessor.api.query_rating", scope="request")
 */
class ApiQueryRatingController implements RatingApiInterface
{
    use AssessorRatingTrait;

    /**
     * @DI\Inject("form.factory")
     * @var FormFactory
     */
    public $formFactory;

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'AssessorBundle:TblQueryRating';
    }

    /**
     * @return TblQueryRatingType
     */
    public function getFormType()
    {
        return new TblQueryRatingType();
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return void
     */
    public function handleAutoComplete($qb)
    {
        $query = $this->request->query->get('query');
        $qb->where('t.query LIKE :query');
        $qb->setParameter('query', $query . '%');
    }

    /**
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return array
     */
    public function editionsAction()
    {
        $editions = $this->request->query->get('editions');
        $entities = $this->getEntityManager()
            ->getRepository('AssessorBundle:TblQueryRating')
            ->findBy(['fullsymbolicid' => $editions], ['rating' => 'desc']);

        return ['entities' => $entities];
    }

    /**
     * @param Request $request
     *
     * @IncludeBitrix()
     *
     * @return View
     */
    public function applySortAction(Request $request)
    {
        $form = $this->formFactory->create(
            new QuerySortRequestType(),
            new QuerySortRequest(),
            ['method' => 'POST']
        );
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getEntityManager();
            /** @var QuerySortRequest $requestEntity */
            $requestEntity = $form->getData();
            $pageSize = NebSearcher::PAGE_SIZE;
            $editions = $requestEntity->getEditions();
            $total = $requestEntity->getTotal();
            $pageNumber = $requestEntity->getPageNumber();
            $queryId = $requestEntity->getQueryId();

            $queryEntity = $em->getRepository('AssessorBundle:TblQueryFrequency')->find($queryId);
            if (!$queryEntity instanceof TblQueryFrequency) {
                throw new NotFoundHttpException("Query($queryId) not found");
            }

            $entities = $em->getRepository('AssessorBundle:TblQueryRating')
                ->findBy(['idquery' => $queryId], ['rating' => 'desc']);
            $resultEntities = [];
            $editionsFlipped = array_flip($editions);
            foreach ($entities as $key => $entity) {
                unset($entities[$key]);
                $editionId = $entity->getFullsymbolicid();
                $entities[$editionId] = $entity;
                if (!isset($editionsFlipped[$editionId])) {
                    $editions[] = $editionId;
                }
            }
            foreach ($editions as $key => $editionId) {
                $itemIndex = $key + ($pageNumber - 1) * $pageSize;
                $rating = ($total - $itemIndex) * 10;
                if (isset($entities[$editionId])) {
                    $entity = $entities[$editionId];
                    $entity->setRating($rating);
                    $em->merge($entity);
                } else {
                    $entity = new TblQueryRating();
                    $entity->setIdquery($queryId);
                    $entity->setRating($rating);
                    $entity->setFullsymbolicid($editionId);
                    $em->persist($entity);
                }
                $resultEntities[] = $entity;
            }
            $em->flush();
            $em->clear();

            return View::create(['entities' => $resultEntities], 200);
        }

        return View::create($form, 400);
    }

}