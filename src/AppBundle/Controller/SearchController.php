<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 11:39
 */

namespace AppBundle\Controller;

use BitrixBundle\Annotations\Template as BitrixTemplate;

class SearchController
{
    /**
     * @BitrixTemplate("AppBundle:search:main.html.php")
     *
     * @return array
     */
    public function searchMainAction()
    {
        $result = [];

        return $result;
    }
}