<?php
$MESS['LIBRARY_STAT_EXPORT_EXCEL'] = "Экспорт в Excel";
$MESS['LIBRARY_STAT_REFRESH'] = "Обновить";
$MESS['LIBRARY_STAT_NUMBER'] = "Количество дедуплицированных карточек изданий библиотеки";
$MESS['LIBRARY_STAT_NUMBER_COPY'] = "Количество дедуплицированных электронных копий изданий библиотеки";
$MESS['LIBRARY_STAT_NUMBER_DOWNLOADED'] = "Количество скачанных изданий библиотеки через портал";
$MESS['LIBRARY_STAT_NUMBER_READ'] = "Количество просмотров изданий библиотеки";
$MESS['LIBRARY_STAT_DATE'] = "Дата";