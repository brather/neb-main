<?
$MESS["FOOTER_ADD_TO"] = "Добавить в";
$MESS["FOOTER_MY_COLLECTIONS"] = "мои подборки";
$MESS["FOOTER_CREATE_COLLECTION"] = "Cоздать подборку";
$MESS["FOOTER_REMOVE_BOOK"] = "Удалить книгу из Моей библиотеки";
$MESS["FOOTER_ACCEPT_REMOVE"] = "Удалить";
$MESS["FOOTER_KEEP_BOOK"] = "Оставить";
$MESS["FOOTER_CLOSE_WINDOW"] = "Закрыть окно";
//$MESS["LEARNING_TEMPLATE_COPYRIGHT"] = "&copy; 2002 Битрикс, 2007 1C-Битрикс";
?>
