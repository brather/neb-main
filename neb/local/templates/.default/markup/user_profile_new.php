
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb bbox b-text" id="" value="Станислав Лем">							
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>	
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>		

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
		<div class="b-searchresult noborder">
			<ul class="b-profile_nav">
				<li>
					<a href="#" class="b-profile_navlk js_profilemenu current">личный кабинет</a>
					<ul class="b-profile_subnav">
						<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
					</ul>
				</li>
				<li><a class="b-profile_nav_lb" href="#">моя библиотека</a></li>
				<li><a href="#" class="b-profile_nav_qt">цитаты</a></li>
				<li><a href="#" class="b-profile_nav_bm ">закладки</a></li>
				<li><a href="#" class="b-profile_nav_notes">заметки</a></li>
				<li><a href="user_profile_lk_searchresult.php" class="b-profile_nav_search">поисковые запросы</a></li>

			</ul> 
			<div class="b-razdel">
			<select class="js_select">
				<option value="-1">Выберите раздел</option>
				<option value="user_profile_new.php">Личный кабинет</option>
				<option value="user_profile_favorites.php">Моя библиотека</option>
				<option value="user_profile_lk_quo.php">Цитаты</option>
				<option value="user_profile_lk_bookmark.php">Закладки</option>
				<option value="user_profile_lk_notes.php">Заметки</option>
				<option value="user_profile_lk_searchresult.php">Поисковые запросы</option>
			</select>
		</div>
		</div><!-- /.b-searchresult-->
		<script type="text/javascript">
     
				
			$(function() {
					$('.b-message_ask').doAutoreg();
			});

			(function() { /*create closure*/
				$.fn.doAutoreg = function() { /* попап регистрации */

					this.each(function() {
						var popup = $(this).clone(true);
							popup.removeClass('hidden');
							popup.dialog({
								closeOnEscape: true,	                   
								modal: true,
								draggable: false,
								resizable: false,             
								width: ($(window).width() > 550)? 550: 300,
								dialogClass: 'autoreg_popup',
								position: "center",
								open: function(){
									$('.ui-widget-overlay').addClass('black');
									popup.find('.closepopup, .pclose').click(function() {				
                                        /*Close the dialog*/
                                        popup.dialog("close").remove();
                                        $('.ui-widget-overlay').remove();
                                        return false;
                                    });
								},
								close: function() {
									popup.dialog("close").remove();
								}
							});

							$('.ui-widget-overlay').click(function() {				
								/*Close the dialog*/
								popup.dialog("close");
							});
					});
				}
				/*end of closure*/
			})(jQuery);

		</script>
		<div class="b-side right mt10">
			<div class="b-profile_side">
				<div class="b-profile_photo">
					<img src="./i/user_photo.png" alt="user photo">
				</div>
				<div class="b-profile_name">Николай Петров</div>
				<div class="b-profile_reg">Зарегистрирован: 24.04.2014 <br/>ограниченный доступ</div>
				<ul class="b-profile_info">
					<li><a href="#">изменить фото</a></li>
					<li><a href="#" class="set_prof">настройки профиля</a></li>
					<li><a href="#" class="help">Помощь</a></li>
					<li class=" hidden"><a href="#">личные сообщения</a><span class="b-profile_msgnum">3</span></li>
				</ul>

				<a class="button_mode  hidden" href="#">получить полный доступ</a>			


			</div>
		</div><!-- /.b-side -->
		
	<div class="b-mainblock left mt10">
		
		<div class="b-profile_lk">
			<h2 class="mode">10 самых популярных изданий</h2>
			<table class="b-usertable tsize">
						<tr>
							<th><span>Автор</span></th>
							<th><span>Название</span></th>
							<th><span>Дата публикации</span></th>
							<th><span>Библиотека</span></th>
							<th><span>Читать</span></th>
						</tr>
						<tr>
							<td class="pl15">Станислав Лем</td>
							<td class="pl15"><a href="#">Сумма технологии</a></td>
							<td class="pl15">2011</td>
							<td>Рязанская областная библиотека</td>
							<td><button type="submit" value="1" class="formbutton">Читать</button>	</td>
						</tr>
						<tr>
							<td class="pl15">Станислав Лем</td>
							<td class="pl15"><a class="popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Сумма технологии</a></td>
							<td class="pl15">2011</td>
							<td>Рязанская областная библиотека</td>
							<td><button type="submit" value="1" class="formbutton">Читать</button>	</td>
						</tr>
						<tr>
							<td class="pl15">Станислав Лем</td>
							<td class="pl15"><a class="popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Сумма технологии</a></td>
							<td class="pl15">2011</td>
							<td>Рязанская областная библиотека</td>
							<td><button type="submit" value="1" class="formbutton">Читать</button>	</td>
						</tr>
					</table>
					<div class="b-paging">
					<div class="b-paging_cnt"><a class="b-paging_prev iblock" onclick="return false;" href=""></a><a class="b-paging_num current iblock" onclick="return false;" href="#" target="_parent">1</a><a href="/profile/plan_digitization/?by1=UF_DATE_ADD&amp;order1=desc&amp;PAGEN_1=2" class="b-paging_num iblock" target="_parent">2</a><a href="/profile/plan_digitization/?by1=UF_DATE_ADD&amp;order1=desc&amp;PAGEN_1=2" class="b-paging_next iblock" target="_parent"></a></div></div>
		</div><!-- /.b-profile_lk -->
		
	</div><!-- /.b-mainblock -->
	
</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>
<!--предупреждение-->
<div class="b-message_ask hidden">
	<a href="#" class="closepopup">Закрыть окно</a>
	<p>Доступ к закрытому изданию возможен только для пользователей НЭБ, прошедших полную регистрацию. Перейти к регистрации?</p>
	<p class="tcenter mt10">
		<a href="#" class="formbutton">Да</a>
		<a  href="#" class="formbutton pclose">Нет</a>
	</p>
</div>
<!--/предупреждение .-->

</body>
</html>