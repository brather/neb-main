<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application,
    \Bitrix\Main\UI\PageNavigation,
    \Bitrix\Highloadblock\HighloadBlockTable;

class PlanListComponent extends CBitrixComponent {

    private $page = 'template';                                 // шаблон компонента
    private $countItems = '';                                   // количество планов по фильтру
    private $countItemsOnPage = '10';                           // количество планов на одной странице
    private $listSort = array(                                  // сортировка планов на странице
        "by" => "UF_DATE_F_PLAN",
        "order" => "ASC"
    );
    private $arRequest = array();                               // массив с данными $_REQUEST
    private $usersLibAdmin = array();                           // массив с id пользователей администраторов библиотек
    private $arUser = array();                                  // массив с данными текущего администратора библиотеки
    private $countPages = '';                                   // количество страниц итоговое
    private $arStatus = array();                                // массив с данными по свойству "Статус", где ключом выступает id свойства список

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        if(!empty($arParams['COUNT_ON_PAGE']))
            $this->countItemsOnPage = $arParams['COUNT_ON_PAGE'];
        if(!empty($arParams['SORT']))
            $this->listSort = $arParams['SORT'];

        if ($arParams['ELEMENT_ID'] > 0)
            $this->page = 'detail';
        else
            $this->page = 'template';

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }

    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->_includeModule();            // Подключаем модули

        $this->_getRequest();               // Получаем и обрабатываем данные REQUEST
        $this->_getLibrary();               // Возвращаем id библиотеки текущего администратора
        $this->_getUsers();                 // Возвращаем массив id пользователей группы "администраторы библиотек" без id текущего администратора

        $this->_getStatusesList();          // Возвращаем массив значений свойства статус
        $this->_getListPlan();              // Возвращаем список планов

        $this->includeComponentTemplate($this->page);
    }

    /**
     * Подключаем модули
     */
    private function _includeModule() {
        Loader::includeModule('highloadblock');
        Loader::includeModule('nota.exalead');
    }

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    private function _getRequest() {
        return $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * Возвращаем данные текущего пользователя
     */
    private function _getLibrary() {
        if(UGROUP_LIB_CODE_ADMIN === nebUser::getCurrent()->getRole()) {
            $this->arUser = nebUser::getCurrent()->getUser();
            $this->arResult["UF_LIBRARY"] = $this->arUser["UF_LIBRARY"];
        }
    }

    /**
     * Возвращаем массив id пользователей группы "администраторы библиотек" без id текущего администратора
     */
    private function _getUsers() {
        $rsGroup    = CGroup::GetList($by = "ID", $order = "asc", ["STRING_ID" => UGROUP_LIB_CODE_ADMIN, "Y"])->Fetch();
        $arLibAdmin = CGroup::GetGroupUser($rsGroup['ID']);
        foreach($arLibAdmin as $idUser) {
            if($idUser != $this->arUser['ID']) {
                $this->usersLibAdmin[] = $idUser;
            }
        }
    }

    /**
     * Получение id highload-блока по его символьному коду
     */
    private function _getIdHLBlock($sCodeHL) {

        $sql = "SELECT ID, NAME FROM b_hlblock_entity WHERE NAME='" . $sCodeHL . "';";
        $arRecord = Application::getConnection()->query($sql)->fetch();

        return intval($arRecord['ID']);
    }

    /**
     * Возвращаем класс для работы с HL блоком
     *
     * @param $idHL
     * @return \Bitrix\Main\Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    function _getDataClass($idHL) {
        $hlblock    = HighloadBlockTable::getById($idHL)->fetch();
        $data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $data_class;
    }

    /**
     * Где план-лист
     *
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getListPlan() {

        // список планов оцифровки
        $this->arResult['HL_ID'] =  $this->_getIdHLBlock(HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
        $dataHLBlock = $this->_getDataClass($this->arResult['HL_ID']);

        if($this->arRequest["count_on_page"] != '')
            $this->countItemsOnPage = $this->arRequest["count_on_page"];

        $arFilter = array(
            "UF_LIBRARY" => $this->arUser["UF_LIBRARY"],
        );
        if ($this->arParams['ELEMENT_ID'] > 0) {
            $arFilter['ID'] = $this->arParams['ELEMENT_ID'];
        }

        if (!empty($this->arRequest['by'])) {
            $this->listSort["by"] = $this->arRequest["by"];
        }
        if (!empty($this->arRequest['order'])) {
            $this->listSort["order"] = $this->arRequest["order"];
        }

        // получение полного количества
        $this->countItems = $dataHLBlock::getCount($arFilter);

        // иницирование навигационной цепочки
        $obPageNavigation = new PageNavigation('page');
        $obPageNavigation->allowAllRecords(true)->initFromUri();
        $obPageNavigation->setRecordCount($this->countItems)->setPageSize($this->countItemsOnPage);
        $this->arResult['NAV'] = $obPageNavigation;

        // формирование параметров запроса
        $arQueryParams = [
            "select" => ['*'],
            "order"  => [$this->listSort["by"] => $this->listSort["order"]],
            "filter" => $arFilter
        ];
        if (strpos($_SERVER['REQUEST_URI'], 'page-all') === false) {
            $arQueryParams['limit']  = $obPageNavigation->getLimit();
            $arQueryParams['offset'] = $obPageNavigation->getOffset();
        }

        // основная выборка данных
        $rsData = $dataHLBlock::getList($arQueryParams);
        $iPlan = 0;
        while($arData = $rsData->fetch()) {
            $this->arResult["ITEMS_PLAN"][$iPlan] = $arData;
            // Получим значение сортировки статуса у плана
            $this->arResult["ITEMS_PLAN"][$iPlan]['UF_REQUEST_STATUS_SORT'] = $this->arStatus[$arData['UF_REQUEST_STATUS']]['SORT'];
            $iPlan++;
        }
    }

    /**
     * Возвращаем массив значений статусов планов оцифровки (UF_REQUEST_STATUS)
     *
     * @return array
     */
    private function _getStatusesList() {

        $iHLBlock = $this->_getIdHLBlock(HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
        $arEnum   = $this->getEnumPropertyList('HLBLOCK_'. $iHLBlock);

        foreach ($arEnum as $arItem) {
            $this->arResult["LIST_STATUS"][$arItem["XML_ID"]] = $arItem;
            $this->arStatus[$arItem["ID"]] = $arItem;
        }
    }

    /**
     * Получим значения списка свойства типа список для HL блока (если не задан код свойства, то по умолчанию ищем статус)
     *
     * @param $sEntityId
     * @param string $sPropertyCode
     * @return array
     */
    public function getEnumPropertyList($sEntityId, $sPropertyCode = 'UF_REQUEST_STATUS') {

        $arReturn = array();

        $arProp = CUserTypeEntity::GetList([], ['ENTITY_ID' => $sEntityId, 'FIELD_NAME' => $sPropertyCode])->Fetch();

        $cUserFieldEnum = new CUserFieldEnum();
        $rsEnum = $cUserFieldEnum->GetList([], ['USER_FIELD_ID' => $arProp['ID']]);
        while($arItem = $rsEnum->Fetch()) {
            $arReturn[$arItem['ID']] = $arItem;
        }

        return $arReturn;
    }
}