<?
/**
 * Добавление цитаты из открытого просмотровщика
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized())
	exit();

$book_id = htmlspecialcharsbx(trim($_REQUEST['book_id']));
if(empty($book_id))
	exit();

$noteID 	= isset($_REQUEST['noteid']) ? (int)$_REQUEST['noteid'] : null;
$notepage 	= intval($_REQUEST['notepage']);
$note 		= htmlspecialcharsbx(trim($_REQUEST['note']));
$notearea 	= htmlspecialcharsbx(trim($_REQUEST['notearea']));
$noteimg 	= trim($_REQUEST['noteimg']);
$notetop 	= intval($_REQUEST['notetop']);
$noteleft 	= intval($_REQUEST['noteleft']);
$notewidth 	= intval($_REQUEST['notewidth']);
$noteheight = intval($_REQUEST['noteheight']);

use \Bitrix\Main\Loader,
    Nota\UserData\Notes;

Loader::includeModule("nota.userdata");

$arFields = array(
	'BOOK_ID' 	=> $book_id,
	'NUM_PAGE' 	=> $notepage,
	'NOTE' 		=> $note,
	'NOTE_AREA' => $notearea,
	'IMG_DATA' 	=> $noteimg,
	'TOP' 		=> $notetop,
	'LEFT' 		=> $noteleft,
	'WIDTH' 	=> $notewidth,
	'HEIGHT' 	=> $noteheight,
);

if ( (bool)$noteID )
	$id = Notes::update($noteID, $arFields);
else
	$id = Notes::addFromArray($arFields);

// Определим тест ответа
$hintText = '';
if ( !(bool)$noteID && (bool)$id )
	$hintText = 'Заметка добавлена';
elseif ( !(bool)$noteID && !(bool)$id )
	$hintText = 'Ошибка добавления заметки';
elseif ( (bool)$noteID && (bool)$id )
	$hintText = 'Заметка обновлена';
elseif ( (bool)$noteID && !(bool)$id )
	$hintText = 'Ошибка обновления заметки';

if ( !(bool)$id )
	$id = rand(1000,100000);
?>

<div class="b-hintadded hidden" data-id="<?=$id?>"><?=$hintText?></div>