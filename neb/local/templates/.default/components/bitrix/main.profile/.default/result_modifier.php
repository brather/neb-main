<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$obNUser = new nebUser();

// если пользователь является сотрудником библиотеки необходимо выводить стоимость одного скана
if ($obNUser->isLibrary() === true){

    $arLibrary = $obNUser->getLibrary();

    // сохранение стоимости скана
    if ($_REQUEST["PROPERTY_SCAN_COST"]) CIBlockElement::SetPropertyValues($arLibrary['ID'], IBLOCK_ID_LIBRARY, $_REQUEST ["PROPERTY_SCAN_COST"], 'SCAN_COST');

    //для библиотекаря необходимо выводить стоимость скана
    $db_props = CIBlockElement::GetProperty(IBLOCK_ID_LIBRARY, $arLibrary['ID'], "sort", "asc", Array("CODE"=>"SCAN_COST"));

    if ($ob =  $db_props->GetNext()) $arResult["arLibrary"]['SCAN_COST'] = $ob['VALUE'];
}