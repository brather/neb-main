$(function(){
    var sessid = $('[data-list-layout]').data('sessionId');

    $(document).on('click','[data-verify-toggler]',function(e){
        e.preventDefault();
        var checkbox = $(this),
            postUrl = checkbox.data('url'),
            verifyText = checkbox.data('verify-text'),
            verifiedText = checkbox.data('verified-text'),
            userId= checkbox.data('user-id'),
            labelContainer = checkbox.siblings('.lbl'),
            params = {
                sessid: sessid,
                user_id: userId,
                verify: $(checkbox).attr('checked') ? 'false' : 'true',
                token: getUserToken()
            };

        $.ajax({
            method: 'PUT',
            url: postUrl,
            data: params,
            success: function(data) {
                if(data.hasOwnProperty('errors')) {
                    FRONT.infomodal({
                        title: "Ошибка",
                        text: data.errors.message,
                        button: "ОК"
                    });
                }

                if (data.message) {
                    var message = {
                        title: "Внимание",
                        text: data.message,
                        button: "ОК"
                    };
                    FRONT.infomodal(message);
                }


                if (data.hasOwnProperty('success') && true === data.success) {
                    if ($(checkbox).attr('checked')) {
                        $(checkbox).removeAttr('checked');
                        $(checkbox).prop('checked', false);
                        labelContainer.text(verifyText);
                    } else {
                        $(checkbox).attr('checked', true);
                        $(checkbox).prop('checked', true);
                        labelContainer.text(verifiedText);
                    }
                }

            },
            error: function(xhr){
                var message = neb.objectPathSearch(xhr, 'responseJSON.errors.message');
                if(message) {
                    FRONT.infomodal({
                        title: "Ошибка",
                        text: message,
                        button: "ОК"
                    });
                }
            }
        });
    });

    /*
    $(document).on('click','[data-unbind]',function(e){
        e.preventDefault();
        var toggler = $(this),
            postUrl = toggler.data('url'),
            readerId = toggler.data('reader-id'),
            readerBlock = toggler.closest('[data-reader-block]'),
            params = {
                'sessid': sessid
            },
            message = {
                text: 'Удалить читателя №'+readerId+' из библиотеки?',
                confirmTitle: "Удалить"
            };

        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed){
                $.ajax({
                    method: 'POST',
                    url: postUrl,
                    data: params,
                    success: function(data){
                        data = JSON.parse(data);
                        if(parseInt(data["error"]) == 0) {
                            $(readerBlock).remove();
                        } else {
                            var message = {
                                title: "Внимание",
                                text: data.message,
                                button: "ОК"
                            };
                            FRONT.infomodal(message);
                        }

                    }
                });
            }
        });
    });

    $('.action-block').click(function (e) {
        e.preventDefault();
        var params = {
            'sessid': sessid
        };
        console.log(params);
        $.post($(this).data('url'), params, function (data) {
            if ( data.error != 0 ) {
                console.log('error');
                return;
            }

            // блокировка была переделана на верификацию
            if (data.hasOwnProperty('block') && true === data.block) {
                $(this).parent().find('.checkbox').removeClass('checked');
            } else {
                $(this).parent().find('.checkbox').addClass('checked', true);
            }
        }.bind(this), 'json');
    });
    $('.checkbox-action input.checkbox').change(function() {
        $(this).parents('.checkbox-action').find('.action-block').click();
    });*/
});
