<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title></title>

	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width"/>
	<link rel="icon" href="favicon.ico" type="image/x-icon" />

	<script src="js/libs/modernizr.min.js"></script>

	<link rel="stylesheet" href="css/style.css">

	<script src="js/libs/jquery.min.js"></script>
	<script type="text/javascript" src="js/slick.min.js"></script>

	<script src="js/plugins.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	<script>window.jQuery.ui || document.write('<script src="js/libs/jquery.ui.min.js">\x3C/script>')</script>
	<script src="js/jquery.knob.js"></script>

	<script src="js/script.js"></script>
	<script>
	$(function() {
        $('html').addClass('mainhtml'); /* это для морды */
    }); /* DOM loaded */
	</script>
</head>
<body>
	<div class="b-citychangepopup" data-width="498">
		<a href="#" class="closepopup"></a>
		<form class="b-map_search_form " action="." method="get">
		<p>Введите название города</p>
		<input type="text" class="input ui-autocomplete-input" name="q" id="" autocomplete="off" placeholder="Москва">
		<div class="b-choosecity">
			<div class="iblock b-citylist">
				<p>Быстрый выбор</p>				
				<div class="b-handcity">
					<div class="b-handcityitem">
						<div class="b-handcityregion">Центр</div>
						<ul class="b-cityquisklist">
							<li><a class="b-city" href="#">Москва</a></li>
							<li><a class="b-city" href="#">Белгород</a></li>
							<li><a class="b-city" href="#">Брянск</a></li>
							<li><a class="b-city" href="#">Владимир</a></li>
							<li><a class="b-city" href="#">Орел</a></li>
							<li><a class="b-city" href="#">Рязань</a></li>
							<li><a class="b-city" href="#">Тверь</a></li>
							<li><a class="b-city" href="#">Тула</a></li>
						</ul>
					</div>
					
					<div class="b-handcityitem">
						<div class="b-handcityregion">Северо-Запад</div>
						<ul class="b-cityquisklist">
							<li><a class="b-city" href="#">Санкт-Петербург</a></li>
							<li><a class="b-city" href="#">Архангельск</a></li>
							<li><a class="b-city" href="#">Калининград</a></li>
							<li><a class="b-city" href="#">Мурманск</a></li>
							<li><a class="b-city" href="#">Петрозаводск</a></li>
							<li><a class="b-city" href="#">Псков</a></li>
						</ul>

					</div>
					<div class="b-handcityitem">
						<div class="b-handcityregion">Юг</div>
						<ul class="b-cityquisklist">
							<li><a class="b-city" href="#">Астрахань</a></li>
							<li><a class="b-city" href="#">Краснодар</a></li>
							<li><a class="b-city" href="#">Ростов-на-Дону</a></li>
							<li><a class="b-city" href="#">Сочи</a></li>
							<li><a class="b-city" href="#">Ставрополь</a></li>
						</ul>

					</div>
					<div class="b-handcityitem">
						<div class="b-handcityregion">Сибирь</div>
						<ul class="b-cityquisklist">
							<li><a class="b-city" href="#">Иркутск</a></li>
							<li><a class="b-city" href="#">Красноярск</a></li>
							<li><a class="b-city" href="#">Новосибирск</a></li>
							<li><a class="b-city" href="#">Кызыл</a></li>
							<li><a class="b-city" href="#">Улан -Удэ</a></li>
							<li><a class="b-city" href="#">Омск</a></li>
						</ul>

					</div>
					<div class="b-handcityitem">
						<div class="b-handcityregion">Поволжье</div>
						<ul class="b-cityquisklist">
							<li><a class="b-city" href="#">Пенза</a></li>
							<li><a class="b-city" href="#">Пермь</a></li>
							<li><a class="b-city" href="#">Салехард</a></li>
						</ul>

					</div>
					<div class="b-handcityitem">
						<div class="b-handcityregion">Урал</div>
						<ul class="b-cityquisklist">
							<li><a class="b-city" href="#">Екатеринбург</a></li>
							<li><a class="b-city" href="#">Тюмень</a></li>
							<li><a class="b-city" href="#">Челябинск</a></li>
						</ul>

					</div>
					<div class="b-handcityitem">
						<div class="b-handcityregion">Северный Кавказ</div>
						<ul class="b-cityquisklist">
							<li><a class="b-city" href="#">Грозный</a></li>
							<li><a class="b-city" href="#">Магас</a></li>
						</ul>

					</div>
					<div class="b-handcityitem">
						<div class="b-handcityregion">Дальний Восток</div>
						<ul class="b-cityquisklist">
							<li><a class="b-city" href="#">Петропавловск-Камчатский</a></li>
							<li><a class="b-city" href="#">Хабаровск</a></li>
							<li><a class="b-city" href="#">Южно-Сахалинск</a></li>
						</ul>

					</div>
					
				</div>
			</div>
			<div class="iblock b-cityquisk">
				<p>Быстрый выбор</p>
				<ul class="b-cityquisklist">
					<li><a class="b-city" href="#">Москва</a></li>
					<li><a class="b-city" href="#">Санкт- Петербург</a></li>
					<li><a class="b-city" href="#">Калуга</a></li>
					<li><a class="b-city" href="#">Самара</a></li>
					<li><a class="b-city" href="#">Новосибирск</a></li>
				</ul>
			</div>
			<div class="b-citycation">			
				<input type="submit" name="submit" value="Принять" class="formbutton">
			</div>
		</div>
	</form>
	</div><!-- .b-citychangepopup -->

	<div class="homepage">
		<div class="b-citypopup">
			<div class="wrapper">
				<a href="#" class="close"></a>
				<span class="b-citylabel">Ваш город
					<a href="#" class="b-citycheck setMsk">г. Санкт- <br/>Петербург</a> ? </span>
					<span class="b-citycheckbt">
						<a href="#" class="button_b setMsk">Да, верно</a>
						<a href="#" class="button_gray js_changecity">Изменить</a>
					</span>
				</div>
			</div><!-- .b-citypopup -->
			<header id="header" class="mainheader">		

				<div class="wrapper clearfix">
			
			<div class="b-headernav_login right">
				<a href="#" class="b-headernav_login_lnk js_register" data-width="255">Регистрация</a>
				<div class="b_register_popup b_login_popup" >
					<div class="b_login_popup_in bbox">
						<h4>Зарегистрироваться как</h4>
						<a href="#" class="formbutton">Читатель</a>
						<div class="b-rightholder">или <a href="#">правообладатель</a></div>
					</div>
				</div> 	<!-- /.b_login_popup -->	

				<a href="#" class="b-headernav_login_lnk current popup_opener" data-height="320"  data-width="550" >Войти</a> <!-- data-width="550"  --> 
				<div class="b_login_popup popup" >
					<div class="b_login_popup_in bbox">
						<div class="b-login_txt hidden"><a href="#">вход для библиотек</a> <a href="#">для правообладателей</a></div>
						
						<div class="b-loginform iblock rel">
							<h4>вход в личный кабинет</h4>
							<form action="" class="b-form b-formlogin">
								<div class="fieldrow nowrap">
									<div class="fieldcell iblock">
										<label for="settings11">Электронная почта</label>
										<div class="field validate">
											<input type="text" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="30" data-minlength="2" name="num" class="input">										
										</div>
									</div>

								</div>
								<div class="fieldrow nowrap">

									<div class="fieldcell iblock">
										<label for="settings10">Пароль</label>
										<div class="field validate">
											<input type="password" data-required="required" value="" id="settings10" data-maxlength="30" data-minlength="2" name="pass" class="input">										
										</div>
									</div>
								</div>
								<div class="fieldrow nowrap fieldrowaction">
									<div class="fieldcell ">
										<div class="field clearfix">
											<button class="formbutton left" value="1" type="submit">Войти</button>
											<a href="#" class="formlink right js_passrecovery">забыли пароль?</a>
										</div>
									</div>
								</div>
							</form>
							<div class="b-passrform">								

								<form action=""  class="b-passrecoveryform b-form hidden">
									<div class="fieldrow nowrap">
										<div class="fieldcell iblock">
											<label for="settings11">Введите электронную почту, указанную при регистрации</label>
											<div class="field validate">
												<input type="text" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="30" data-minlength="2" name="num" class="input">	
												<em class="error required">Поле обязательно для заполнения</em>									
											</div>
										</div>

										<div class="fieldrow nowrap fieldrowaction">
											<div class="fieldcell ">
												<div class="field clearfix">
													<button class="formbutton" value="1" type="submit">Восстановить</button>

												</div>
											</div>
										</div>
									</div>
								</form>
								<form action=""  class="b-passrecoveryform b-form">
									<div class="fieldrow nowrap">
										<div class="fieldcell iblock">
											<div class="b-warning">Адрес эл. почты не найден</div>
											<label for="settings11">Попробуйте ввести другой адрес</label>
											<div class="field validate">
												<input type="text" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="30" data-minlength="2" name="num" class="input">	
												<em class="error required">Поле обязательно для заполнения</em>									
											</div>
										</div>

										<div class="fieldrow nowrap fieldrowaction">
											<div class="fieldcell ">
												<div class="field clearfix">
													<button class="formbutton" value="1" type="submit">Восстановить</button>

												</div>
											</div>
										</div>
									</div>
								</form>
								<p class="b-passconfirm hidden">На почту выслано письмо с ссылкой на сброс пароля</p>
							</div>
						</div><!-- /.b-loginform -->
						<div class="b-login_social iblock">
							<h4>через социальные сети</h4>
							<a href="#" class="b-login_slnk vklink">вконтакте</a>
							<a href="#" class="b-login_slnk odnlink">одноклассники</a>
							<a href="#" class="b-login_slnk fblink">facebook</a>
						</div><!-- /.b-login_social -->

					</div>

				</div> 	<!-- /.b_login_popup -->
			</div>
			<div class="b-header_lang right ">
					<a href="#" class="en iblock hidden">Eng</a>
					<a href="#" class="ru iblock"></a>
				</div>
				<div class="b-headernav-apps right">
					<a href="#" class="b-headerapps_img"><span>3</span></a>						
					<a href="#" class="b-headerapps_lnk">Приложения для моб. устройств</a>
				</div>
				<nav class="b-headernav left">    
					<a href="#collections" class="b-headernav_lnk">Коллекции</a>
					<!--<a href="#" class="b-headernav_lnk current">Библиотеки</a>
					<a href="#" class="b-headernav_lnk">Форум</a>-->
				</nav>
		</div>	<!-- /.wrapper -->			
		
	</header>
	<section class="mainsection innerpage" >	
		<form action="" class="searchform">		
			<div class="b-portalinfo clearfix wrapper">
				<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
				</div> 
				<div class=" rightblock iblock">
					<div class="b-search_field">
						<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
						<div class="b_search_set clearfix">
							<div class="checkwrapper b-search_lib">
								<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в коллекциях</label>
							</div>
							<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>
							<div class="checkwrapper b-search_lib">
								<input class="checkbox js_btnsub" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
							</div>
						</div> <!-- /.b_search_set -->
					</div>
				</div>

			</div><!-- /.b-portalinfo -->


			<div class="b-search wrapper">

			</div> <!-- /.b-search -->
			<div class="b-search_ex">
				<div class="wrapper rel">
					<div class="b_search_row js_search_row hidden" >
						<select name="logic" disabled="disabled" id="" class="ddl_logic">
							<option value="">или</option>
							<option value="">и</option>
						</select>
						<select name="theme" disabled="disabled" id="" class="ddl_theme">
							<option value="">по всем полям</option>
							<option value="">по дате</option>
						</select>
						<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					</div>
					<div class="b_search_row visiblerow">
						<select name="logic[0]" id="" class="js_select ddl_logic ">
							<option value="">или</option>
							<option value="">и</option>
						</select>
						<select name="theme[0]" id="" class="js_select ddl_theme">
							<option value="">по всем полям</option>
							<option value="">по дате</option>
						</select>
						<input type="text" name="text[0]" class="b-text b_search_txt" id="">
						<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
					</div>						

					<div class="b_search_date">
						<div class="b_search_datelb iblock">Дата публикации</div>
						<div class="b_searchslider iblock js_searchslider"></div>
						<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
						<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

					</div>
					<input type="submit" class="formbutton" value="Принять">
				</div>
			</div><!-- /.b-search_ex -->
		</form>			

	</section>
	<div class="b-map rel mappage">
		<form method="get" action="." class="b-map_search_filtr">
			<div class="b-map_search">
				<div class="wrapper">
					<div class="b-cytyinfo">
						<span class="b-map_search_txt">Найти ближайшую библиотеку в </span>
						<a href="#" class="b-citycheck setMsk">г. Москва</a>
						<a href="#" class="b-changecity js_changecity">(изменить)</a>
					</div>
					<input type="text" class="b-map_tb ui-autocomplete-input searchonmap" name="q" id="" autocomplete="off" placeholder="Введите адрес в этом городе">		
					<input type="submit" class="button_b" value="Найти" name="submit">
				</div>
			</div><!-- /.b-map_search -->
		</form>
<!-- 
data-path="AJAX_fake_map_places.json" data-lat="55.76" data-lng="37.64" data-zoom="10" - начальные параметры карты: масштаб и координаты города, путь к файлу с точками
-->
<div class="ymap" id="ymap" data-path="AJAX_fake_map_places.json" data-lat="55.76" data-lng="37.64" data-zoom="10">

</div>
<span class="marker_lib hidden">
			<a href="#" class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</a><br />
			<span class="b-elar_status">Федеральная библиотека</span><br />
			<span class="b-map_elar_info">
				<span class="b-map_elar_infoitem addr"><span>Адрес:</span>Москва, ул. Череповецкая, 17</span><br />
				<span class="b-map_elar_infoitem graf"><span>График работы:</span>Ежедневно с 10.00 до 18.00</span>
			</span>
			<span class="b-mapcard_act clearfix">
				<span class="right neb b-mapcard_status">Участник</span>
				<a href="#" class="button_mode">перейти в библиотеку</a>
			</span>
		</span>
</div><!-- /.b-map -->

<section class="rel innerwrapper clearfix">
	<div class="b-elar_usrs">
		<h2>Библиотеки</h2>
		<ol class="b-elar_usrs_list">
			<li>
				<span class="num">1.</span>
				<div class="b-elar_name iblock">
					<span class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</span>
					<div class="b-elar_status neb">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, ул. Череповецкая, 17</div>
					<a href="#" class="b-elar_address_link" data-lat="55.8858831574" data-lng="37.6034869031" >Показать на карте</a>
				</div>
			</li>
			<li>	
				<span class="num">2.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Президентская библиотека им. Б. Н. Ельцина</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, ул. Маршала Бирюзова, 32</div>
					<a href="#" class="b-elar_address_link" data-lat="55.7993656804" data-lng="37.4820865371">Показать на карте</a>
				</div>
			</li>
			<li>
				<span class="num">3.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</a>
					<div class="b-elar_status neb">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, ул. Голубинская, 28</div>
					<a href="#" class="b-elar_address_link" data-lat="55.60165876" data-lng="37.55305003">Показать на карте</a>
				</div>
			</li>
			<li>
				<span class="num">4.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Президентская библиотека им. Б. Н. Ельцина</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, бул. Осенний, 12, корп.1</div>
					<a href="#" class="b-elar_address_link" data-lat="55.7579825666" data-lng="37.4093059305">Показать на карте</a>
				</div>
			</li>
			<li>
				<span class="num">5.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</a>
					<div class="b-elar_status neb">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, пл. Киевского Вокзала, 2</div>
					<a href="#" class="b-elar_address_link" data-lat="55.7438480368" data-lng="37.566648449">Показать на карте</a>
				</div>
			</li>
			<li>
				<span class="num">6.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Президентская библиотека им. Б. Н. Ельцина</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, ул. Раменки, 3</div>
					<a href="#" class="b-elar_address_link" data-lat="55.6937294292" data-lng="37.4949511508">Показать на карте</a>
				</div>
			</li>
			<li>
				<span class="num">7.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Российская государственная библиотека (РГБ), г. Москва</a>
					<div class="b-elar_status neb">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, Протопоповский пер., 3</div>
					<a href="#" class="b-elar_address_link" data-lat="55.7804654496" data-lng="37.634653289">Показать на карте</a>
				</div>
			</li>
			<li>
				<span class="num">8.</span>
				<div class="b-elar_name iblock">
					<a href="#" class="b-elar_name_txt">Президентская библиотека им. Б. Н. Ельцина</a>
					<div class="b-elar_status">Федеральная библиотека</div>
				</div>
				<div class="b-elar_address iblock">
					<div class="b-elar_address_txt">Москва, Столярный пер., 3, корп.6</div>
					<a href="#" class="b-elar_address_link" data-lat="55.7647144122" data-lng="37.5717483543">Показать на карте</a>
				</div>
			</li>

		</ol>
	</div><!-- /.b-elar_usrs -->


</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

<script src="//api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>

</body>
</html>