<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__); 

try
{
	if (!Main\Loader::includeModule('iblock'))
		throw new Main\LoaderException(Loc::getMessage('NEW_LIST_PARAMETERS_IBLOCK_MODULE_NOT_INSTALLED'));
		
	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(

			"PARENT_ID" => array(
                "PARENT" => "BASE",
                "NAME" => Loc::getMessage('SIMILAR_LIST_PARAMETRS_PARENT_ID'),
                "TYPE" => "STRING",
                "DEFAULT" => '',
			),
			
			'CACHE_TIME' => array(
				'DEFAULT' => 3600
			)
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e -> getMessage());
}
