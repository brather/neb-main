<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
Loc::loadMessages(__FILE__);?>
<div class="container-fluid">
    <?if(!empty($arResult["ERROR"])) {?>
        <div class="row block-mess bg-danger">
            <h4 class="text-danger"><?= $arResult["ERROR"] ?></h4>
        </div>
        <div class="row"><hr></div>
    <?}?>
    <?if(!empty($arResult["SUCCESS"])) {?>
        <div class="row block-mess bg-success">
            <h4 class="text-success"><?= $arResult["SUCCESS"] ?></h4>
        </div>
        <div class="row"><hr></div>
    <?}?>
    <div class="row">
        <form action="<?= $arResult["FORM_URL"] ?>" method="POST" class="nrf">
            <input type="hidden" name="CONTROL_ADD_ITEM" value="Y" />
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="NAME_ITEM">
                        <?= Loc::getMessage("NAME_ITEM"); ?>
                    </label>
                    <div class="col-lg-9">
                        <input type="text"
                               class="form-control"
                               name="NAME_ITEM"
                               id="NAME_ITEM"
                               placeholder="<?= Loc::getMessage("INPUT_NAME_ITEM") ?>"
                               value="<?= $arRequest["NAME_ITEM"] ?>"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="sect-name">
                        <?= Loc::getMessage("CHOISE_SECTION_FOR_ITEM") ?>
                    </label>
                    <div class="col-lg-9">
                        <select name="SECTION_NAME_FOR_ITEM" id="sect-name" class="form-control">
                            <option value="-">-</option>
                            <?foreach ($arResult["SECTIONS"] as $idSect => $nSect){
                                $selected = '';
                                if(!empty($arRequest["SECTION_NAME_FOR_ITEM"])
                                    && $arRequest["SECTION_NAME_FOR_ITEM"] == $idSect){
                                    $selected = ' selected';
                                }?>
                                <option value="<?= $idSect ?>"<?= $selected ?>><?= $nSect ?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="SORT_ITEM">
                        <?= Loc::getMessage("SORT_ITEM"); ?>
                    </label>
                    <div class="col-lg-9">
                        <input type="text"
                               class="form-control"
                               name="SORT_ITEM"
                               id="SORT_ITEM"
                               placeholder="<?= Loc::getMessage("SORT_ITEM_DEFAULT"); ?>" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="detail-text">
                        <?= Loc::getMessage("DETAIL_TEXT_ITEM"); ?>
                    </label>
                    <div class="col-lg-9">
                        <?$APPLICATION->IncludeComponent(
                            "neb:faq.editor",
                            "",
                            array(
                                "TYPE_EDITOR" => "TYPE_1",
                                "TEXT" => $arRequest["DETAIL_TEXT_ITEM"], // контент из запроса который нужно вставить
                                "TEXTAREA_NAME" => "DETAIL_TEXT_ITEM", // имя поля
                                "TEXTAREA_ID" => "content",         // id поля
                                "TEXTAREA_WIDTH" => "100%",  // ширина
                                "TEXTAREA_HEIGHT" => "300px",    // высота
                                "INIT_ID" => "detail-text", // ID самого редактора
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary">
                    <?= Loc::getMessage("ADD_ITEM") ?>
                </button>
            </div>
        </form>
    </div>
    <div class="row"><hr></div>
    <div class="row"><p><small><?= Loc::getMessage("REQUIRED_FIELD") ?></small></p></div>
</div>