<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

global $APPLICATION;
$APPLICATION->IncludeComponent(
    'rest:library-statistic',
    '',
    array(
        'SEF_MODE'          => 'Y',
        'SEF_FOLDER'        => '/rest_api/library-statistic/',
        'SERVICE_NAME'      => 'Library statistic',
        'SEF_URL_TEMPLATES' => array(
            'users'                     => 'export/USERS/',
            'active_users'              => 'export/ACTIVE_USERS/',
            'publications'              => 'export/PUBLICATIONS/',
            'search_count'              => 'export/SEARCH_COUNT/',
            'views'                     => 'export/VIEWS/',
            'downloads'                 => 'export/DOWNLOADS/',
            'views_book'                => 'export/VIEWS_BOOK/',
            'publications_digitized'    => 'export/PUBLICATIONS_DIGITIZED/',
            'publications_digitized_dd' => 'export/PUBLICATIONS_DIGITIZED_DD/',
            'publications_dd'           => 'export/PUBLICATIONS_DD/',
            'publictionsReport'         => 'export/publications-report/',
        ),
    ),
    false
);