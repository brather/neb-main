<?php

/**
 * User: agolodkov
 * Date: 05.12.2015
 * Time: 14:41
 */
class RegionsComponent extends \Neb\Main\ListComponent
{
    /**
     * @return $this
     */
    public function prepareParams()
    {
        return $this;
    }

    /**
     * @return $this
     */
    public function loadList()
    {
        // города в шапку
        $sQuery = "
        SELECT
          ID as ID,
          UF_CITY_NAME as NAME
        FROM
          neb_libs_city
        WHERE
          UF_CITY_NAME    LIKE '%москва%'
          OR UF_CITY_NAME LIKE '%санкт-петербург%'
          OR UF_CITY_NAME LIKE '%севастополь%'
        ORDER BY
          ID";
        $this->arResult['city'] = \Bitrix\Main\Application::getConnection()->query($sQuery)->fetchAll();

        // список регионов
        $sQuery = "SELECT DISTINCT UF_REGION FROM neb_libs ORDER BY UF_REGION;";
        $sQuery = sprintf($sQuery,  mysqli_real_escape_string($this->arParams['regionName']));
        $this->arResult['items'] = \Bitrix\Main\Application::getConnection()->query($sQuery)->fetchAll();

        //debugPre($this->arResult['city']);

        return $this;
    }
}