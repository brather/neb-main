<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Ответ на заявку
 */

$APPLICATION->IncludeComponent(
    "neb:feedback.reply",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        'FEEDBACK_URL'       => $arParams['SEF_FOLDER'],
        'FEEDBACK_REPLY_URL' => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['reply']
    ),
    $component
);