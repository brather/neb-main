<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$APPLICATION->SetTitle("Статистика"); ?>
<? //$APPLICATION->AddHeadScript('http://code.highcharts.com/highcharts.js');?>
<div class="iblock b-lib_fulldescr">
<div class='b-breadcrumb' style='margin-bottom:15px;'>Количество посещений страницы библиотеки в НЭБ - <?=intval($arResult['ITEM']['SHOW_COUNTER'])?></div>

<!--<div class="b-breadcrumb bot">
	<span>Разделы</span>
    <ul class="b-breadcrumblist">
        <li><a href="?form=read&from=<? //=$arParams['DATE_FROM']?>&to=<? //=$arParams['DATE_TO']?>&PAGEN_1=1">Количество прочитанных книг библиотеки</a></li>
        <li><a href="?form=view&from=<? //=$arParams['DATE_FROM']?>&to=<? //=$arParams['DATE_TO']?>&PAGEN_1=1">Количество просмотренных книг библиотеки</a></li>
                    
    </ul>
</div>-->

<div class='b-add_digital js_digital'>
<form action="<?=$APPLICATION->GetCurPage()?>?<?if ($arParams['FORMAT'] == 'view'):?>form=view<?endif;?>" method="GET" name="form1">
с <?echo CalendarDate("from", $arParams['DATE_FROM'], "form1", "10", "class=\"b-text\"")?>
по <?echo CalendarDate("to", $arParams['DATE_TO'], "form1", "10", "class=\"b-text\"")?>
<input type='hidden' value='<?=$arParams['FORMAT']?>' name='form'>
<input type="submit" class="b-search_bth bbox" value="Обновить" style='float:none;'>
</form>
<br>
<div><?if ($arParams['FORMAT'] != 'view'):?>Количество прочитанных книг библиотеки<?else:?>Количество просмотренных книг библиотеки<?endif;?></div>
<br>

<div id="container" style="width:100%; height:400px;"></div>
<? 
foreach ($arResult['ITEMS'] as $date=>$value):
	$arrData[] = '\''.$date.'\'';
endforeach;
$Data = implode(", ", $arrData);

foreach ($arResult['ITEMS'] as $date=>$value):
	//print_r($value);
	if(!$value['RD']) $value['RD'] = 0;
	$arrCount[] = $value['RD'];
endforeach;
$Count = implode(", ", $arrCount);
?>
<script>
$(function () {
    $('#container').highcharts({
        title: false,
        xAxis: {
            categories: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan']
			/*categories: [ <?#=$Data?>]*/
        },
        yAxis: {
            title: {
                text: 'Количество просмотров'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
		
        legend: false,
        series: [{
			name: 'Посещения',
            /*data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]*/
            data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9, 4]
			/*data: [ <?#=$Count?>]*/
        }]
    });
});
</script>
<?=htmlspecialcharsBack($arResult['STR_NAV'])?>
</div>
<br><br>
</div>

