<?php
$MESS['LIBRARY_LIST_FIND_CLOSEST'] = 'FIND THE NEAREST LIBRARY';
$MESS['LIBRARY_LIST_FIND_BUTTON'] = 'Find';
$MESS['LIBRARY_LIST_ENTER_ADDRESS'] = 'Enter address';
$MESS['LIBRARY_LIST_CHANGE_CITY'] = 'Change City';
$MESS['LIBRARY_LIST_LIBS'] = 'Libraries';
$MESS['LIBRARY_LIST_SHOW_ON_MAP'] = 'Show on the map';
$MESS['LIBRARY_LIST_YOUR_CITY'] = 'Your city';
$MESS['LIBRARY_LIST_YES'] = 'Yes, right';
$MESS['LIBRARY_LIST_EDIT'] = 'Change';
$MESS['LIBRARY_LIST_ENTER_CITY'] = 'Enter the name of the city';
$MESS['LIBRARY_LIST_CHOOSE'] = 'Quick selection';
$MESS['LIBRARY_LIST_FAST_CHOOSE'] = 'Quick selection';
$MESS['LIBRARY_LIST_ACCEPT'] = 'Choose';

//попап на карте
$MESS['LIBRARY_LIST_POPUP_ADDRESS'] = 'Address';
$MESS['LIBRARY_LIST_POPUP_WORK'] = 'Working schedule';
$MESS['LIBRARY_LIST_POPUP_USER'] = 'Member';
$MESS['LIBRARY_LIST_POPUP_URL'] = 'Go to the library';
$MESS['LIBRARY_LIST_ADDRESS'] = 'Address';
$MESS['LIBRARY_LIST_MEMBER_NEL'] = 'Member of NEB';