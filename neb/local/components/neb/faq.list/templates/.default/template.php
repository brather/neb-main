<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?if(!empty($arResult["PAGE_TITLE"]) && $arResult["SHOW_TITLE"] == "Y"){?>
    <h1><?= $arResult["PAGE_TITLE"] ?></h1>
<?}?>
<?if(!empty($arResult["PAGE_DESCRIPTION"])){?>
    <p>
        <?= $arResult["PAGE_DESCRIPTION"] ?>
        <?if(!empty($arResult["URL_FEEDBACK_PAGE"])){?>
            <?= $arResult["URL_FEEDBACK_PAGE"] ?>
        <?}?>
    </p>
<?}?>
<?foreach($arResult["SECTION_ITEMS"] as $sectionId => $sectionArr){?>
    <?if($sectionArr['ACTIVE'] == 'Y'){?>
        <h2><?= $sectionArr["SECTION_NAME"] ?></h2>
        <dl data-faq-list-expandable>
            <?foreach($sectionArr["ITEMS"] as $iItem => $arItem){?>
                <?if($arItem['ACTIVE'] == 'Y') {?>
                    <dt id="s<?= $sectionId ?>_q<?= $iItem ?>">
                        <a href="#s<?= $sectionId ?>_q<?= $iItem ?>"><?= $arItem["NAME"] ?></a>
                    </dt>
                    <dd style="display: none;"><?= $arItem["DETAIL_TEXT"] ?></dd>
                <?}?>
            <?}?>
        </dl>
    <?}?>
<?}?>