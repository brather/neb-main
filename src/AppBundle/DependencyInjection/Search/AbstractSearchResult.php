<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 13:24
 */

namespace AppBundle\DependencyInjection\Search;


class AbstractSearchResult
{
    private $_data;
    private $_xml;
    private $_json;

    /**
     * @param string $data
     */
    public function __construct($data)
    {
        $this->_data = $data;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * @return object
     */
    public function getJson()
    {
        if (null === $this->_json) {
            $this->_json = \GuzzleHttp\json_decode($this->_data);
        }

        return $this->_json;
    }

    /**
     * @return \SimpleXMLElement
     */
    public function getXml()
    {
        if (null === $this->_xml) {
            $this->_xml = new \SimpleXMLElement($this->_data);
        }

        return $this->_xml;
    }
}