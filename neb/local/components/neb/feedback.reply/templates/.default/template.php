<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$arRequest = $component->getRequest();
$APPLICATION->SetTitle(Loc::getMessage('FEEDBACK_REPLY_TITLE') . $arRequest['id']);

if ($arRequest['ajax'] == 'Y') {
    $component->restartBuffer();
}

if ($arResult['json'] == true) {
    $component->showJson($arResult);
}

$arFile = $arResult['TICKET']['FIELDS']['FB_REPLY_FILE']['FILE'];

?>

<div class="feedback-reply">

    <? if ($arResult['TICKET']['STATUS_DESCRIPTION'] === 'CLOSED'): ?>

        <?=Loc::getMessage('FEEDBACK_REPLY_DATE_CREATE')?>: <?=$arResult['TICKET']['DATE_CREATE'];?><br /><br />

        <?=Loc::getMessage('FEEDBACK_REPLY_DATE_CLOSE')?>: <?=$arResult['TICKET']['TIMESTAMP_X'];?><br /><br />

        <?=Loc::getMessage('FEEDBACK_REPLY_STATUS')?>: <?=$arResult['TICKET']['STATUS_TITLE'];?><br /><br />

        <?=Loc::getMessage('FEEDBACK_REPLY_THEME')?>: <?=$arResult['TICKET']['FIELDS']['FB_THEME']['ANSWER_TEXT'];?><br /><br />

        <? $arBook = $arResult['TICKET']['FIELDS']['FB_BOOK_ID']['BOOK'];
        if (!empty($arBook['id'])): ?>
            <?=Loc::getMessage('FEEDBACK_REPLY_BOOK')?>: <a href="/catalog/<?=$arBook['id'];?>/"><?
            echo trim($arBook['authorbook'] .': '.$arBook['title']); ?></a><br /><br />
        <? endif; ?>

        <?=Loc::getMessage('FEEDBACK_REPLY_MESSAGE')?>: <?=$arResult['TICKET']['FIELDS']['FB_TEXT']['USER_TEXT'];?><br /><br />

        <?=Loc::getMessage('FEEDBACK_REPLY_ANSWER')?>: <?=$arResult['TICKET']['FIELDS']['FB_REPLY_TEXT']['USER_TEXT'];?><br /><br />

        <? if (!empty($arFile)): ?>
            <?=Loc::getMessage('FEEDBACK_REPLY_FILE')?>: <a href="<?=$arFile['PATH']?>"><?=$arFile['FILE_NAME']?></a> [<?=$arFile['SIZE']?>]<br />
        <? endif; ?>

    <? else: ?>

        <?=Loc::getMessage('FEEDBACK_REPLY_DATE_CREATE')?>: <?=$arResult['TICKET']['DATE_CREATE'];?><br /><br />

        <?=Loc::getMessage('FEEDBACK_REPLY_THEME')?>: <?=$arResult['TICKET']['FIELDS']['FB_THEME']['ANSWER_TEXT'];?><br /><br />

        <? $arBook = $arResult['TICKET']['FIELDS']['FB_BOOK_ID']['BOOK'];
        if (!empty($arBook['id'])): ?>
            <?=Loc::getMessage('FEEDBACK_REPLY_BOOK')?>: <a href="/catalog/<?=$arBook['id'];?>/"><?
                echo trim($arBook['authorbook'] .': '.$arBook['title']); ?></a><br /><br />
        <? endif; ?>

        <?=Loc::getMessage('FEEDBACK_REPLY_MESSAGE')?>: <?=$arResult['TICKET']['FIELDS']['FB_TEXT']['USER_TEXT'];?><br /><br />

        <? foreach($arResult['ERROR'] as $sError): ?>
            <?ShowError($sError);?>
        <? endforeach; ?>

        <form id="replyFormFeedback" method="post" enctype="multipart/form-data" action="<?=$arParams['FEEDBACK_REPLY_URL']
                                    ?:$_SERVER['REDIRECT_URL'];?>?id=<?=$arRequest['id']?>&ajax=<?=$arRequest['ajax']?>">

            <input type="hidden" name="FEEDBACK[ACTION]" value="REPLY" />

            <textarea class="form-control" name="FEEDBACK[MESSAGE]" style="width:100%; height: 100px;"><?
                echo $arRequest['FEEDBACK']['MESSAGE'] ? : $arResult['TICKET']['FIELDS']['FB_REPLY_TEXT']['USER_TEXT'];
            ?></textarea><br />

            <? if (!empty($arFile)): ?>
                Файл: <a href="<?=$arFile['PATH']?>"><?=$arFile['FILE_NAME']?></a> [<?=$arFile['SIZE']?>]<br />
            <? endif; ?>

            <div class="form-group">
                <label>Прикрепить файлы</label>
                <div data-file-upload data-url="/api/files/upload/gallery/" data-inputs-name="posting_files[]">
            <span class="btn btn-default fileinput-button">
                <span>Выберите файлы</span>
                <input data-file-upload-input type="file"
                       name="FEEDBACK[FILE]"/>
                <div class="hidden">
                    <div data-file-item>
                        <div data-file-client-name></div>
                        <div data-file-item-progress>
                            <div class="file-progress"><div class="file-progress-bar"></div></div>
                        </div>
                        <div data-file-server-name></div>
                    </div>
                    <span data-uploaded-file-present>
                        <a target="_blank" data-present-link href="#"></a>
                        <input type="hidden" data-file-input name="upload_files[]"
                               value=""/>
                    </span>
                </div>
            </span>
                    <div data-present-files style="width: 70%;">

                    </div>
                </div>
            </div>

            <!--<input class="btn btn-default" type="file" id="FEEDBACK_FILE" name="FEEDBACK[FILE]" data-inputs-name="FEEDBACK[FILE]"
                data-file-upload data-url="/api/files/upload/gallery/" /><br />-->

            <div class="btn-group">
                <input type="submit" class="btn btn-primary btn-md" name="FEEDBACK[SEND]" value="<?=Loc::getMessage('FEEDBACK_REPLY_SUBMIT')?>"  onkeyup="this.disabled = true;"/>
                <input type="submit" class="btn btn-default btn-md" name="FEEDBACK[SAVE]" value="<?=Loc::getMessage('FEEDBACK_REPLY_SAVE')?>"  onkeyup="this.disabled = true;"/>
            </div>
        </form>

        <script type="text/javascript">
            $('#replyFormFeedback [data-file-upload]').uploaderInit();
        </script>

    <? endif; ?>
</div>

<?if ($arRequest['ajax'] == 'Y')
    die();