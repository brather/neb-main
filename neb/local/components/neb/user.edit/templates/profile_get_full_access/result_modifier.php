<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var array $arResult
 */
if (isset($arResult['userData'])) {
    $arResult['arUser'] = $arResult['userData'];
} else {
    $arResult['arUser'] = [];
}

$arResult['ERRORS'] = $arResult['errors'];
$arResult['USER_VERIFIED'] = (
    $arResult['arUser']['UF_STATUS'] == nebUser::USER_STATUS_VERIFIED
);
$arResult['DATA_SAVED'] = $arResult['success'] ? 'Y' : 'N';