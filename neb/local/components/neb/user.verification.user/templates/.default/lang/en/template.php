<?
$MESS["USER_VERIFICATION_ENTER_SITE"] = "Вход на сайт";
$MESS["USER_VERIFICATION_EMAIL"] = "Электронная почта";
$MESS["USER_VERIFICATION_PASSWORD_PROTECTION"] = "Пароль";
$MESS["USER_VERIFICATION_PASSWORD_MIN"] = "не менее 6 символов";
$MESS["USER_VERIFICATION_CONFIRM_PASSWORD"] = "Подтверждение пароля";

$MESS["USER_VERIFICATION_FIELD_FILL"] = "Заполните";
$MESS["USER_VERIFICATION_MORE_PLACEHOLDER_SYMBOLS"] = "Не более {0} символов";
$MESS["USER_VERIFICATION_LESS_PLACEHOLDER_SYMBOLS"] = "Не менее {0} символов";
$MESS["USER_VERIFICATION_PASSWORD_MATCH"] = "Пароль не совпадает с введенным ранее";

$MESS['USER_VERIFICATION_NUMBER'] = 'Номер читательского билета';
$MESS['USER_VERIFICATION_DATE']   = 'Дата окончания действия читательского билета';

$MESS["USER_VERIFICATION_SEND_DATA"] = "Отправить данные";
$MESS["USER_VERIFICATION_REGISTRY_ELP"] = "в реестр читательских билетов";