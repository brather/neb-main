<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;
$APPLICATION->IncludeComponent(
	"rest:notes", 
	"", 
	array(
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/rest_api/notes/",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"DOCUMENTATION" => "Y",
		"SERVICE_NAME" => "API Заметок",
		"SEF_URL_TEMPLATES" => array(
			"processRequest" => "",
		)
	),
	false
);