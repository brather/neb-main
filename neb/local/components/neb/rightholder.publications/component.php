<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $USER;
if (!$USER->IsAuthorized())
    $APPLICATION->AuthForm("У вас нет права доступа к данному файлу.");

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    Nota\Exalead\BooksAdd;

Loader::includeModule("nota.exalead");

$arParams = array_replace_recursive(
    array(
        'fullPeriod' => false,
    ), $arParams
);

if (
    $_SERVER['REQUEST_METHOD'] == 'POST'
    && isset($_POST['BOOK_NUM_ID'])
    && !empty($_POST['BOOK_NUM_ID'])
) {
    if (is_array($_POST['BOOK_NUM_ID'])) {
        $booksKeys = $_POST['BOOK_NUM_ID'];
    } else {
        $booksKeys = array($_POST['BOOK_NUM_ID']);
    }
    $bookFiles = array();
    foreach ($booksKeys as $key) {
        if (isset($_POST['BOOK_ID_' . $key])
            && isset($_POST['FILE_PDF_' . $key])
        ) {
            $bookFiles[$_POST['BOOK_ID_' . $key]] = $_POST['FILE_PDF_' . $key];
        }
    }
    $booksIds = array_filter(array_keys($bookFiles));
    if (!empty($booksIds)) {
        $biblioCards
            = \Nota\Exalead\LibraryBiblioCardTable::getByFullSymbolicId(
            $booksIds
        );
        foreach ($biblioCards as $item) {
            $item = array_intersect_key(
                $item,
                array(
                    'ALIS' => true,
                    'Author' => true,
                    'Name' => true,
                    'PublishYear' => true,
                    'BBKText' => true,
                    'ISBN' => true,
                    'FullSymbolicId' => true,
                )
            );
            if (isset($bookFiles[$item['FullSymbolicId']])) {
                $item['pdfLink'] = BooksAdd::getPdfLink(
                    $bookFiles[$item['FullSymbolicId']],
                    $item['FullSymbolicId']
                );
                $booksAdd = new BooksAdd();
                $idBook = $booksAdd->addPublication($item, LIB_ID_RIGHTHOLDER);
            }
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' and !empty($_REQUEST['add_book'])) {
    $libId = BooksAdd::setLibrary(LIB_ID_RIGHTHOLDER, 'Библиотека правообладателей');

    $arFields = array(
        'ALIS' => $libId,
        'Author' => htmlspecialcharsEx($_REQUEST['AUTHOR']),
        'Name' => htmlspecialcharsEx($_REQUEST['NAME']),
        'PublishYear' => htmlspecialcharsEx($_REQUEST['PUBLISH_YEAR']),
        'BBKText' => htmlspecialcharsEx($_REQUEST['BBK']),
        'ISBN' => htmlspecialcharsEx($_REQUEST['ISBN']),
        'pdfLink' => htmlspecialcharsEx($_REQUEST['FILE_PDF']),

    );
    BooksAdd::setIdFromALIS($arFields, 0);
    $arFields['pdfLink'] = BooksAdd::getPdfLink(
        $arFields['pdfLink'],
        $arFields['IdFromALIS']
    );
    if (isset($_REQUEST['ISBN'])) {
        $arFields['ISBN'] = $_REQUEST['ISBN'];
    }

    $idBook = BooksAdd::addPublication($arFields, LIB_ID_RIGHTHOLDER);
    if ($idBook > 0)
        LocalRedirect($APPLICATION->GetCurPage());
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' and !empty($_REQUEST['edit_book'])) {
    $libId = BooksAdd::setLibrary(LIB_ID_RIGHTHOLDER, 'Библиотека правообладателей');
    $book_id = (int)$_REQUEST['ID'];

    $arFields = array(
        'ALIS' => $libId,
        'Author' => htmlspecialcharsEx($_REQUEST['AUTHOR']),
        'Name' => htmlspecialcharsEx($_REQUEST['NAME']),
        'PublishYear' => htmlspecialcharsEx($_REQUEST['PUBLISH_YEAR']),
        'BBKText' => htmlspecialcharsEx($_REQUEST['BBK']),
        'pdfLink' => htmlspecialcharsEx($_REQUEST['FILE_PDF']),

    );

    BooksAdd::edit($book_id, $arFields);
    /*$idBook = BooksAdd::add($arFields, LIB_ID_RIGHTHOLDER);
    if($idBook > 0)
        LocalRedirect($APPLICATION->GetCurPage());*/
}

if ($_REQUEST['action'] == 'remove' and (int)$_REQUEST['id'] > 0) {
    BooksAdd::remove((int)$_REQUEST['id']);
    LocalRedirect($APPLICATION->GetCurPage());
}

if ($_REQUEST['action'] == 'todel' and (int)$_REQUEST['id'] > 0) {
    $arFields = array(
        'toDel' => htmlspecialcharsEx(1)
    );
    $bookId = (int)$_REQUEST['id'];
    if (!(intval($bookId) <= 0 || empty($arFields))) {
        global $DB;
        foreach ($arFields as &$Field) {
            $Field = '"' . $DB->ForSql($Field) . '"';
        }
        $DB->Update('tbl_common_biblio_card', $arFields, "WHERE ID='" . $bookId . "'", $err_mess . __LINE__);
        //BooksAdd::edit((int)$_REQUEST['id'], $arFields);
        LocalRedirect($APPLICATION->GetCurPage());
    }
}
if ($_REQUEST['action'] == 'undotodel' and (int)$_REQUEST['id'] > 0) {
    $arFields = array(
        'toDel' => htmlspecialcharsEx(0)
    );
    $bookId = (int)$_REQUEST['id'];
    if ((intval($bookId) <= 0 || empty($arFields))) {
        global $DB;
        foreach ($arFields as &$Field) {
            $Field = '"' . $DB->ForSql($Field) . '"';
        }
        $DB->Update('tbl_common_biblio_card', $arFields, "WHERE ID='" . $bookId . "'", $err_mess . __LINE__);
        //BooksAdd::edit((int)$_REQUEST['id'], $arFields);
        LocalRedirect($APPLICATION->GetCurPage());
    }
}

$arFilter = array(
    ' and request_type = "rightholder"'
);


if (false === $arParams['fullPeriod'] and isset($_REQUEST['date-from'])
    && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $_REQUEST['date-from'])
) {
    $arResult['dateFrom'] = $_REQUEST['date-from'];
}
if (false === $arParams['fullPeriod'] and empty($arResult['dateTo']) && isset($_REQUEST['date-to'])
    && preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $_REQUEST['date-to'])
) {
    $arResult['dateTo'] = $_REQUEST['date-to'];
}
if (false === $arParams['fullPeriod'] and empty($arResult['dateFrom'])) {
    $arResult['dateFrom'] = date('01.m.Y');
}
if (false === $arParams['fullPeriod'] and empty($arResult['dateTo'])) {
    $arResult['dateTo'] = date('d.m.Y');
}
$q = htmlspecialcharsbx($_REQUEST['qbook']);
$type = htmlspecialcharsbx($_REQUEST['qbooktype']);
if (!empty($q)) {
    if ($type == 'all')
        $arFilter[] = ' and (Name like "%' . $q . '%" or Author like "%' . $q . '%" or BBKText like "%' . $q . '%" or PublishYear like "%' . $q . '%")';
    elseif ($type == 'Name')
        $arFilter[] = ' and Name like "%' . $q . '%"';
    elseif ($type == 'Author')
        $arFilter[] = ' and Author like "%' . $q . '%"';
}
if (isset($arResult['dateFrom'])) {
    $arFilter[] = ' and BX_TIMESTAMP >= "' . ConvertDateTime(
            $arResult['dateFrom'],
            'YYYY-MM-DD'
        ) . '"';
}
if (isset($arResult['dateTo'])) {
    $arFilter[] = ' and BX_TIMESTAMP <= "' . ConvertDateTime(
            $arResult['dateTo'],
            'YYYY-MM-DD'
        ) . '"';
}

$by = htmlspecialcharsEx($_REQUEST['by']);
if (empty($by))
    $by = 'BX_TIMESTAMP';
$order = htmlspecialcharsEx($_REQUEST['order']);
if (empty($order))
    $order = 'desc';

$arResult['BOOKS'] = BooksAdd::getListBooksCurrentUser($arFilter, array(), $by, $order);
$booksIds = array();
foreach ($arResult['BOOKS'] as $key => &$book) {
    if (!empty($book['FullSymbolicId'])) {
        $booksIds[$book['FullSymbolicId']] = $key;
    } elseif (empty($book['status'])) {
        $book['status'] = 'new';
    }
    if (empty($book['status'])) {
        $book['status'] = 'modify';
    }
    $book['status'] = strtoupper($book['status']);
}
unset($book);
$booksInfoResult = \Nota\Exalead\BooksInfoTable::getList(array(
    'filter' => array(
        'BOOK_ID' => array_keys($booksIds),
    ),
));
foreach ($arResult['BOOKS'] as $key => $data) {
    $arResult['BOOKS'][$key]['VIEWS'] = 0;
}
while ($bookInfo = $booksInfoResult->fetch()) {
    if (!isset($booksIds[$bookInfo['BOOK_ID']])) {
        continue;
    }
    $bookKey = $booksIds[$bookInfo['BOOK_ID']];

    $arResult['BOOKS'][$bookKey]['VIEWS'] = $bookInfo['VIEWS'];

    if (empty($arResult['BOOKS'][$bookKey]['FullSymbolicId'])):
        $arResult['BOOKS'][$bookKey]['FullSymbolicId'] = $bookInfo['BOOK_ID'];
    endif;
}

if (isset($_REQUEST['export'])):

    MainHelper::clearBuffer();

    require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel/IOFactory.php");

    $objPHPExcel = new \PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setTitle('Список произведений');

    $row = 1;
    $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'ID')->getStyle('A' . $row)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'Название')->getStyle('B' . $row)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, 'Автор')->getStyle('C' . $row)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, 'Количество просмотров')->getStyle('D' . $row)->getFont()->setBold(true);

    $row = 2;
    foreach ($arResult['BOOKS'] as $ids => $value):
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $value['FullSymbolicId']);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $value['Name']);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $value['Author']);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $value['VIEWS']);
        $row++;
    endforeach;

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);

    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="Список произведений.xls"');
    header('Cache-Control: max-age=0');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: public, must-revalidate');

    $objWriter->save('php://output');
    exit;

endif;

$this->IncludeComponentTemplate();
