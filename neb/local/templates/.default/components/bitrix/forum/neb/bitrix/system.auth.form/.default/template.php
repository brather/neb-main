<? use Bitrix\Main\Localization\Loc;

Loc::loadMessages('/local/templates/.default/components/bitrix/forum/neb/bitrix/system.auth.form/.default/lang/' . LANGUAGE_ID .'/template.php');

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParamsToDelete = array(
	"login",
	"logout",
	"register",
	"forgot_password",
	"change_password",
	"confirm_registration",
	"confirm_code",
	"confirm_user_id",
);

$arResult["AUTH_URL"] = $APPLICATION->GetCurPageParam("login=yes", array_merge($arParamsToDelete, array("logout_butt", "backurl")), $get_index_page=false);

$GLOBALS['APPLICATION']->AddHeadScript("/bitrix/js/main/utils.js");
?><noindex><?
if ($arResult["FORM_TYPE"] == "login"):
?>
<div id="forum-login-form-window">

<a href="" onclick="return ForumCloseLoginForm()" rel="nofollow" style="float:right;"><?=GetMessage("AUTH_CLOSE_WINDOW")?></a>

<form method="post" target="_top" action="<?=POST_FORM_ACTION_URI?>">
	<?
	if (strlen($arResult["BACKURL"]) > 0)
	{
	?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?
	}
	?>
	<?
	foreach ($arResult["POST"] as $key => $value)
	{
	?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?
	}
	?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />

	<table width="100%" class="table table-unbordered" cellpadding="10">
			<tr>
				<td>
				<?=GetMessage("AUTH_LOGIN")?>:<br />
				<input class="form-control" type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" size="17" /></td>
			</tr>
			<tr>
				<td>
				<?=GetMessage("AUTH_PASSWORD")?>:<br />
				<input class="form-control" type="password" name="USER_PASSWORD" maxlength="50" size="17" /></td>
			</tr>
		<?
		if ($arResult["STORE_PASSWORD"] == "Y") 
		{
		?>
			<tr>
				<td valign="top">
					<input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" />
					<label for="USER_REMEMBER" class="lbl"><?=GetMessage("AUTH_REMEMBER_ME")?></label>
				</td>
			</tr>
		<?
		}
		if ($arResult["CAPTCHA_CODE"])
		{
		?>
			<tr>
				<td>
				<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
				<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
				<input type="text" name="captcha_word" maxlength="50" value="" /></td>
			</tr>
		<?
		}
		?>
			<tr>
				<td><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></td>
			</tr>

			<tr>
				<td><a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" data-toggle="modal" data-target="#forget" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></td>
			</tr>
		<?
		if($arResult["NEW_USER_REGISTRATION"] == "Y")
		{
		?>
			<tr>
				<td><a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a><br /></td>
			</tr>
		<?
		}
		?>
	</table>	
</form>
</div>
<a href="<?=$arResult["AUTH_URL"]?>" onclick="return ForumShowLoginForm(this);" target="_self" rel="nofollow"><span><?=GetMessage("AUTH_LOGIN_BUTTON")?></span></a>
<?
else:
?>
<a href="<?
	?><?=htmlspecialcharsbx($APPLICATION->GetCurPageParam("logout=yes",
	array("login", "logout", "register", "forgot_password", "change_password", BX_AJAX_PARAM_ID)))?><?
	?>" rel="nofollow"><span><?=GetMessage("AUTH_LOGOUT_BUTTON")?></span></a>
<? endif; ?>
    <? if (!$USER->IsAuthorized()):?>
        <? $GLOBALS['APPLICATION']->AddHeadScript("/local/templates/.default/components/bitrix/system.auth.form/adaptive/script.js"); ?>
        <div class="modal fade recovery-modal" tabindex="-1" role="dialog" id="forget" style="font-size: initial;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="/auth/forgot_password.php?forgot_password=yes" method="post" class="b-passrecoveryform b-form">
                        <input type="hidden" name="AUTH_FORM" value="Y">
                        <input type="hidden" name="TYPE" value="SEND_PWD">
                        <input type="hidden" name="backurl" value="/">
                        <input type="hidden" name="send" value="1">
                        <input type="hidden" name="IS_AJAX" value="Y">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            <h4 class="modal-title"><?=Loc::getMessage('AUTH_PASS_RECOVERY_TITLE')?></h4>
                        </div>
                        <div class="modal-body" style="text-align: center;">
                            <div class="icloser">
                                <div class="js-fieldrow">
                                    <div class="b-warning hidden"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND')?></div>
                                    <label for="settings11"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC')?></label>
                                    <div class="field validate">
                                        <input type="text" class="form-control input-lg input" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="50" data-minlength="2" name="USER_EMAIL">
                                    </div>
                                </div>
                                <p class="b-passconfirm hidden"><?=Loc::getMessage('AUTH_PASS_RECOVERY_SUCCESS')?></p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary js-recovery-submit btn-lg" value="1" type="submit"><?=Loc::getMessage('AUTH_PASS_RECOVERY_RECOVER')?></button>
                            <button type="button" class="btn btn-default btn-lg" data-dismiss="modal"><?=Loc::getMessage('AUTH_CLOSE')?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            $(function(){
                $('[data-target=#forget]').on('click',function(){
                    ForumCloseLoginForm();
                });
                $('#forget').on('shown.bs.modal',function(){

                    $('#settings11').focus();
                })
            });
        </script>
    <? endif;?>

</noindex>