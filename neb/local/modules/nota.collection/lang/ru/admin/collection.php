<?
$MESS["NOTA_COLLECTION_TITLE"] = 'Коллекции книг';
$MESS["NOTA_COLLECTION_ADD"] = 'Добавить коллекцию';
$MESS["NOTA_COLLECTION_NAME"] = 'Название';
$MESS["NOTA_COLLECTION_ID"] = 'ID';
$MESS["NOTA_COLLECTION_BOOK_ID"] = 'ID книги';
$MESS["NOTA_COLLECTION_COLLECTION_ID"] = 'ID коллекции';
$MESS["NOTA_COLLECTION_EDIT"] = 'Редактировать';
$MESS["NOTA_COLLECTION_DELETE"] = 'Удалить';
$MESS["NOTA_COLLECTION_DELETE_CONFIRM"] = 'Вы действительно хотите удалить коллекцию?';
$MESS["NOTA_COLLECTION_ACCESS_DENIED"] = 'Доступ закрыт';
$MESS["NOTA_COLLECTION_SORT"] = 'Сортировка';
$MESS["NOTA_COLLECTION_TIMESTAMP_X"] = 'Дата изм.';
$MESS["NOTA_COLLECTION_MAIN"] = 'Выводить на главную';
