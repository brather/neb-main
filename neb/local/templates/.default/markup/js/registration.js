/**
 * Created by agolodkov on 07.05.2015.
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else {
        factory(jQuery);
    }
}(function ($) {
    function _librarySelect(){
        var librarySelect = $('.library-list-select select');
        /*
        if (librarySelect.val()) {
            $('#full-register-form').slideDown();
        }
        */
        librarySelect.on('change', function (){
            /*
            var fullFormContainer = $('#full-register-form');
            if (librarySelect.val()) {
                fullFormContainer.slideDown();
            } else {
                fullFormContainer.slideUp();
            }
            */
            $('#UF_LIBRARY').val(librarySelect.val());
        });
    }

    $(function () {
        new _librarySelect();
    });
}));