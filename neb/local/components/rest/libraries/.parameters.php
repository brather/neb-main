<?php
$arComponentParameters = array(
    "PARAMETERS" => array(
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => Array("DEFAULT" => 3600),
        "DOCUMENTATION" => array(
            "NAME" => GetMessage("DOCUMENTATION"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
            "GROUPS" => array(
                "CONTROL" => GetMessage('GROUP_CONTROL'),
            ),
        ),
        "SERVICE_NAME" => array(
            "NAME" => GetMessage("SERVICE_NAME"),
            "TYPE" => "TEXT",
            "DEFAULT" => GetMessage('DEFAULT_NAME'),
        ),
        "SEF_MODE" => Array(
			'processRequest' => array(
				'NAME' 			=> GetMessage('PROCESS_REQUEST_NAME'),
				'DEFAULT' 		=> '/',
				'VARIABLES' 	=> array('date_start', 'date_end', 'limit', 'offset',),
				'TYPE' 			=> array('GET'),
				'DOCUMENTATION' => array(
					'GROUP' 		=> 'CONTROL',
					'DESCRIPTION' 	=> '',
					'PARAMETERS' 	=> array(
					)
				),
			),
        ),
    ),
); 