<?
	if (!$USER->IsAuthorized()) 
		return false;

	$obNUser = new nebUser();	
	$obNUser->getRole();

	/*if($obNUser->role != 'user')
	{
	$arLibrary =	$obNUser->getLibrary();
	} */	

	if($obNUser->role == 'user') 
	{
		/* массиы меню пользователя*/

		$aMenuLinksExt = array(
			array(' ', '/special/profile/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
			array('Заказы на сканирование', '/special/profile/scanning/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Настройки профиля', '/special/profile/edit/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Помощь', '/faq/', array(), 									array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			#array('Выйти', '/special/profile/?logout=yes', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),

			array('Личный кабинет', '/special/profile/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),

			array('моя библиотека', '/special/profile/my-library/', array(),			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_lb")),
			array('поисковые запросы', '/special/profile/searches/', array(),			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_search")),

            array('цитаты', '/special/profile/quote/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_qt")),
            array('закладки', '/special/profile/bookmark/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_bm")),
            array('заметки', '/special/profile/note/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_notes")),

		);
	}
	else if($obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR)
	{
		/* массиы меню библиотекаря*/

		$aMenuLinksExt = array(
			array(' ', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
			array('Настройки профиля', '/special/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Помощь', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'MSGNUM' => 0)),
			array('Кабинет', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			//array('Новости', '/special/profile/news/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			//array('Выйти', '/special/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
			array('Статистика', '/special/profile/statistics/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Фонды', '/special/profile/funds/manage/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Читатели', '/special/profile/readers/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			//array('Коллекции', '/special/profile/collection/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			//array('Новости', '/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=3&type=news&lang=ru', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Выставки', '/special/profile/collection/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Новости', '/special/profile/news/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Оцифровка', '/special/profile/plan_digitization/', array(), 	array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('ЕЭЧБ', '/special/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		);
	}
	else if($obNUser->role == UGROUP_LIB_CODE_CONTROLLER)
	{
		/* массиы меню библиотекаря-контролера*/

		$aMenuLinksExt = array(
			array(' ', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
			array('Настройки профиля', '/special/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Помощь', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'MSGNUM' => 0)),
			#array('Выйти', '/special/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
			array('Кабинет', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Статистика', '/special/profile/statistics/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('ЕЭЧБ', '/special/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		);
	}
	else if($obNUser->role == UGROUP_RIGHTHOLDER_CODE)
	{
		/* Массив меню правообладателя	*/
		$aMenuLinksExt = array(
			array(' ', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),

			array('Настройки профиля', '/special/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Помощь', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			#array('Выйти', '/special/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),

			array('Личный кабинет', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),

			array('Работа с фондами', '/special/profile/work-fonds/', array(),	array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Статистика', '/special/profile/statistics/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		);
	}
	$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>