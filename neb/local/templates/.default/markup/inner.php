
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right">
						<input class="checkbox js_btnsub" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		<nav class="b-commonnav">
			<a href="#" class="current">о проекте</a>
			<a href="#">право</a>
			<a href="#">участники</a>
		</nav>
		<div class="b-plaintext">
			<h2>Цели НЭБ</h2>
			<p>Национальная электронная библиотека призвана собирать, архивировать, описывать электронные документы, способствующие сохранению и развитию национальной науки и культуры, и организовывать их общественное использование. Таким образом, должно сформироваться <a href="#">единое национальное собрание</a> полных текстов электронных документов, свободный доступ к которому осуществляется через интернет-портал Национальной электронной библиотеки, что обеспечит основу для развития в России единого социально-культурного пространства. </p>


			<div class="cols threecols">
				<div class="iblock bbox">
					<img src="./i/ico_book_big.png" alt="">
					<h4>1 000 000</h4>
					<p>В настоящее время проект НЭБ поддерживается тремя участниками: Российская государственная библиотека, Российская национальная библиотека и Государственная публичная научно-техническая библиотека России.</p>
				</div>
				<div class="iblock bbox">
					<img src="./i/ico_book_big.png" alt="">
					<h4>1 000 000</h4>
					<p>В настоящее время проект НЭБ поддерживается тремя участниками: Российская государственная библиотека, Российская национальная библиотека и Государственная публичная научно-техническая библиотека России.</p>
				</div>
				<div class="iblock bbox">
					<img src="./i/ico_book_big.png" alt="">
					<h4>1 000 000</h4>
					<p>В настоящее время проект НЭБ поддерживается тремя участниками: Российская государственная библиотека, Российская национальная библиотека и Государственная публичная научно-техническая библиотека России.</p>
				</div>
			</div><!-- /cols -->
			<h3>Подзаголовок</h3>
			<p>Национальная электронная библиотека призвана собирать, архивировать, описывать электронные документы, способствующие сохранению и развитию национальной науки и культуры, и организовывать их общественное использование. Таким образом, должно сформироваться единое национальное собрание полных текстов электронных документов, свободный доступ к которому осуществляется через интернет-портал Национальной электронной библиотеки, что обеспечит основу для развития в России <a href="#">единого социально-культурного</a> пространства. </p>
			<ul class="b-commonlist">
				<li>интеграция библиотек России в единую информационную сеть;</li>
				<li>разработка четких схем взаимодействия библиотек в рамках действующего законодательства, в том числе об авторском праве;</li>
				<li>развитие технической базы, позволяющей обеспечить;</li>
				<li>создание электронных копий высокого качества и единого формата;</li>
				<li>формирование стандартных библиографических описаний и организацию единого </li>
				<li>поиска по всем каталогам, отражающим распределенные фонды;</li>
				<li>возможность вечного хранения электронных документов и удобство работы с .</li>
			</ul>
		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
	

</section>

</div><!-- /.homepage -->

<div class="b-mylibrary">
	<div class="wrapper">
		<div class="b-mylibrarytit">Моя библиотека</div>
		<ul class="b-mylibrarylist">
			<li>
				<a href="#">Сейчас  читаю</a> <span class="b-num">2</span>
			</li>
			<li><a href="#">В закладках</a><span class="b-num">14</span></li>
			<li><a href="#">Буду читать</a><span class="b-num">47</span></li>
		</ul>
	</div>
</div><!-- /.b-mylibrary -->
<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<!--<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu js_openmenu">мои подборки</a>
	
	<ul class="b-selectionlist">
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb8">	<label for="cb8" class="black">Любимые авторы</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb9">	<label for="cb9" class="black">Научно-популярная фантастика</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb10"><label for="cb10" class="black">Ракеты и люди</label>

		</li>	
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb14"><label for="cb14" class="lite">Отметить как прочитанное</label>

		</li>
		<li><a href="#" class="b-selection_add"><span>+</span>Cоздать подборку</a></li>
	</ul>
</div>--><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>