<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

$APPLICATION->IncludeComponent(
    'neb:user.edit',
    'create_user',
    [
        'SUCCESS_TEMPLATE' => 'success',
        'MAIL_EVENTS'      => array(
            'USER_REGISTRATION',
            'USER_PASS_CHANGED'
        ),
    ],
    $component
);