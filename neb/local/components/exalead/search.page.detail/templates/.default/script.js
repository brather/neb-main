var angle = 0;
var book_id, count_page, min_width_img, image_start_width, image_start_height, min_height_img, positionpart;
var FRONT = FRONT || {};

book_id = document.getElementById('book_id').value;

FRONT.user = [];
FRONT.events = (function(){
  var topics = {};

  return {
    subscribe: function(topic, listener) {
      // создаем объект topic, если еще не создан
      if(!topics[topic]) topics[topic] = { queue: [] };

      // добавляем listener в очередь
      var index = topics[topic].queue.push(listener) -1;

      // предоставляем возможность удаления темы
      return {
        remove: function() {
          delete topics[topic].queue[index];
        }
      };
    },
    publish: function(topic, info) {
      // если темы не существует или нет подписчиков, не делаем ничего
      if(!topics[topic] || !topics[topic].queue.length) return;

      // проходим по очереди и вызываем подписки
      var items = topics[topic].queue;
      items.forEach(function(item) {
          item(info || {});
      });
    }
  };
})();
FRONT.viewer = {};
FRONT.viewer.init = (function (){
    return {
        page: (function page() {
            var constInitPage = new Number(parseInt(document.getElementById('page-number').value));
            this.page = (function(){
                return constInitPage;
            });
            return constInitPage;
        }),
        docready: $(function(){
            FRONT.viewer.init.page();
            FRONT.viewer.previousCurrentPage = new Number(document.getElementById('page-number').value);
            FRONT.viewer.currentPage = FRONT.viewer.init.page();
            // FRONT.viewer.pageActualize();
            //FRONT.viewer.turnThePage(FRONT.viewer.previousCurrentPage);
        })
    };
})();
FRONT.viewer.UX = {
    pageNumberHistory: [],
    currentPage: 0
}
FRONT.viewer.UX.surfHistory = (function(set){
    this.set = set;
    if(typeof set === "undefined") {
        return FRONT.viewer.UX.pageNumberHistory[FRONT.viewer.UX.pageNumberHistory.length-1];
    }
    else if ( this.set != FRONT.viewer.UX.pageNumberHistory[FRONT.viewer.UX.pageNumberHistory.length-1] ) {
        FRONT.viewer.UX.pageNumberHistory.push(this.set);
        FRONT.viewer.UX.currentPage = this.set;
    }
})

FRONT.viewer.pageActualize = function(page,caller) {
    // запись в историю
    // FRONT.viewer.UX.surfHistory(page);
    // отображение активного пункта-ссылки в списке произведений 
    (function(){
        var list = $('[data-chapter-list]')[0],
            items = $(list).find('[data-chapter-item]');
        $(items).each(function(){
            var item = $(this),
                title = item.find('.title-anchor'),
                itemData = item.data('bitrix'),
                startPage = itemData.startPage,
                finishPage = itemData.finishPage,
                included = ( (startPage <= page) && (page <= finishPage) );
            $(item).toggleClass('active', included );
        });     
    })();
    // отображение активного пункта-ссылки в списке закладок
    (function(){
        var list = $('[data-right-bookmarks] + .bookmark-content'),
            items = list.find('a');
        $(items).each(function(){
            $(this).toggleClass('active', page == parseInt( $(this).attr('p')) );
        });     
    })();
}



FRONT.viewer.turnThePage = function(page,caller){

    // запись в историю
    FRONT.viewer.UX.surfHistory(page);
    /*адресная строка*/
    var page = page,
        pagenumber = page.num;
    var currentPage = parseInt($('#page-number').val()),
        previousCurrentPage = FRONT.viewer.previousCurrentPage,
        historyAPI = !!(window.history && history.pushState),
        locationSearch;

    $('.flaviusmatis').pagination({
        pages: count_page,
        currentPage: pagenumber,
        listStyle: 'pagination',
        onPageClick: function(pageNumber,event) {
            if (event) {
                event.preventDefault();
            }
            // getText(pageNumber);
            getImage(pageNumber);
        },
        prevText: "Назад",
        nextText: "Вперед"
    });


    if (page.num == 1) {
        locationSearch = './';
    } else {
        locationSearch = '?page=' + pagenumber.toString();
    }
    page.num = parseInt(page.num);
    if (page.go && historyAPI) {
        /*history.go(go); //зацикливается видимо генерится событие при переходе*/
        // getText(page.num);
        getImage(page,'FRONT.viewer.turnThePage'); 
    } 
    else if (page.num != currentPage && historyAPI && page.num != 0) {
        history.pushState({
            prechangedPage:currentPage,
            actualPage:page.num
        }, null, locationSearch);
    }
    else if (historyAPI && page.num != 0) { /*превая страница*/
        history.replaceState({
            prechangedPage:currentPage,
            actualPage:page.num
        }, null, locationSearch);
    } else {
        /*page.num == currentPage*/
        return
    }
    FRONT.viewer.pageActualize(page.num,'FRONT.viewer.turnThePage');
    
}

FRONT.viewer.store = function(){
    var textStore = {},
        editionDataStore = {};
    this.getTextStore = function(){
        return textStore;
    }
    this.getEditionDataStore = function(){
        return editionDataStore;
    }
    this.rewriteEditionDataStore = function(data){
        var dfd;
        if (data && typeof data == 'object') {
            editionDataStore = data;
        } else if (typeof data == 'string') {
        } else {
            dfd =  new jQuery.Deferred();
            $.when( this.getEditionData(true) ).then(function(newdata){
                //  this.getEditionData(true);
                dfd.resolve(newdata);
            });
            return dfd.promise();
        }
    } 
}
FRONT.viewer.store.prototype.getEditionData = function(reget) {
    var editionId = book_id,
        editionData = this.getEditionDataStore(),
        res = editionData,
        that = this,
        dfd = new jQuery.Deferred();

    if ( ($.isEmptyObject(editionData) || reget) && reget !== false ) {
        $.ajax({
            type: "GET",
            url: "/local/tools/viewer/quo_list.php",
            data: "book_id=" + book_id,
            success: function(data) {
                res = data;
                
                // editionData = data;
                that.rewriteEditionDataStore(data);
                
                dfd.resolve(res);
            }
        }).fail(function( jqXHR, textStatus, errorThrown ) {
            dfd.reject(textStatus);
        });

    } else {
        dfd.resolve(res);
    }
    return dfd.promise();
}
// FRONT.viewer.store.prototype.setEditionData = function(obj){
//     var editionData = this.getEditionDataStore(),
//         exist = store.hasOwnProperty(page) && store[page];
//     store[page] = text;
//     if (exist) {
//         return exist
//     }
// }
FRONT.viewer.store.prototype.getText = function(page) {
    var store = this.getTextStore(),
        res = store[page],
        that = this,
        dfd = new jQuery.Deferred();
    if (!res) {
        $.ajax({
            type: "GET",
            url: "/rest_api/exalead/book/page/",
            dataType: 'json',
            data: {
                'book_id' : document.getElementById('book_id').value,
                'dataType' : 'txt',
                'p' : page
                },
            success: function(msg){
                // var text = msg['content'];
                // $('.text-view').html('').html(text);
                if (msg['content']) {
                    res = msg['content'];
                }
                that.setText(page,res);
                dfd.resolve(res);
            }
        });
        // this.setText(page,res);
    } else {
        dfd.resolve(res);
    }
    return dfd.promise();
}
FRONT.viewer.store.prototype.setText = function(page,text) {
    var store = this.getTextStore(),
        exist = store.hasOwnProperty(page) && store[page];
    store[page] = text;
    if (exist) {
        return exist
    }
}

var pagesContent = new FRONT.viewer.store();

// Подписка на "событие", генерируемое FRONT.events.publish('имя события', {"объект":"передаваемый","в":"функцию","через":"подписчика"});
// Второй параметр у подписки - имя вызываемой функции
var pageSubscription = FRONT.events.subscribe('pagechange', FRONT.viewer.turnThePage);
//pageSubscription.remove();


window.onpopstate = function (e) {
    var pageNumber, go = -1;
    var page = {
        state: {},
        translate: false
    };
    if (e.state && e.state.prechangedPage) {
        page.state = e.state;
        page.num = e.state.actualPage;
        page.go = -1;
    } else {
        page.num = FRONT.viewer.init.page();
    }
    // go = ? надо записывать e.state.prechangedPage к себе (PRECHANGEDPAGE) и сравнивать с ним e.state.actualPage
    FRONT.viewer.turnThePage(page,'onpopstate');
    // FRONT.viewer.pageActualize(page.num,'onpopstate'); //как это сюда попало? ведь вызывается из FRONT.viewer.turnThePage
};

FRONT.spinner = {};
FRONT.spinner.opts = {
      lines: 17 // The number of lines to draw
    , length: 18 // The length of each line
    , width: 7 // The line thickness
    , radius: 19 // The radius of the inner circle
    , scale: 0.55 // Scales overall size of the spinner
    , corners: 0 // Corner roundness (0..1)
    , color: '#00708c' // #rgb or #rrggbb or array of colors
    , opacity: 0 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 99 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 12 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '50%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'absolute' // Element positioning
};
FRONT.spinner.btnOpts = {
      lines: 9 // The number of lines to draw
    , length: 3 // The length of each line
    , width: 2 // The line thickness
    , radius: 3 // The radius of the inner circle
    , scale: 1 // Scales overall size of the spinner
    , corners: 1 // Corner roundness (0..1)
    , color: '#fff' // #rgb or #rrggbb or array of colors
    , opacity: 0.25 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 60 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '-.7ex' // Top position relative to parent
    , left: '0' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'relative' // Element positioning
};
FRONT.spinner.mini = {
      lines: 7 // The number of lines to draw
    , length: 2 // The length of each line
    , width: 1 // The line thickness
    , radius: 1 // The radius of the inner circle
    , scale: 1 // Scales overall size of the spinner
    , corners: 1 // Corner roundness (0..1)
    , color: '#fff' // #rgb or #rrggbb or array of colors
    , opacity: 0.25 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1.6 // Rounds per second
    , trail: 60 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '-.7ex' // Top position relative to parent
    , left: '0' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'relative' // Element positioning
};

FRONT.spinner.graphOpts = {
    lines: 17 // The number of lines to draw
    , length: 30 // The length of each line
    , width: 7 // The line thickness
    , radius: 50 // The radius of the inner circle
    , scale: 0.55 // Scales overall size of the spinner
    , corners: 0 // Corner roundness (0..1)
    , color: '#00708c' // #rgb or #rrggbb or array of colors
    , opacity: 0 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 99 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 12 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '45%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'relative' // Element positioning
};

FRONT.spinner.scanLoad = {
    scale:.95, 
    className: 'spinner-neb-logo', 
    nebLogo: true,
    length: -10,
    width: 29,
    radius: 84,
    trail: 90
}


FRONT.spinner.execute = function(target,options){
    var opts = {};
    $.extend(opts,this.opts);
    if (options) {
        $.extend(opts,options);
    }
    // var options = options || this.opts;
    return new Spinner(opts).spin(target);
};

FRONT.viewer.getSingleContent = function(page) {
    // var page = page || {num:FRONT.viewer.UX.currentPage}
    var viewName,
        view;
        $('[data-view-port]').each(function(){
            var port;
            if ( $(this).data('viewPort') == 'switched-on' ) {
                view = $(this);
            }        
        });
    viewName = view.data('viewName');

    //снять выделенный блок (который зумируется и добавляет цитаты и заметки)
    $('#selectedrect').css({'display': 'none'});
    $('.layer_notes').css({'display': 'none'});
    $('#text_note').val('');
    $('.jcrop-holder>div:first-child').hide();


    if (viewName == 'simple-image') {
        //проверка в закладках ли страница
        isMark(page.num);
        isQuo(page.num);
        displayNormalImage();
    } else if (viewName == 'fullscreen-image') {
        displayFullscreeenImage()
    } else if (viewName == 'display-text') {
        displayText();
    }

    function displayText() {
        // var text = FRONT.viewer.store.getText()

        $('[data-view-name="display-text"]').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
        $('[data-view-name="display-text"]').find('.spinner-logo').fadeOut(function(){ $(this).detach() });
        $('[data-view-name="display-text"]').find('.milk-shadow').fadeOut(function(){ $(this).detach() });

        $('[data-view-name="display-text"]').prepend('<div class="milk-shadow draw"></div>');
        FRONT.spinner.execute( $('[data-view-name="display-text"]')[0], FRONT.spinner.scanLoad);

        $.when( pagesContent.getText(page.num) ).then(function(text){
            $('[data-view-name="display-text"]').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
            $('[data-view-name="display-text"]').find('.spinner-logo').fadeOut(function(){ $(this).detach() });
            $('[data-view-name="display-text"]').find('.milk-shadow').fadeOut(function(){ $(this).detach() });
            $('.text-view').html(text);
            // $('.text-view').html('').html(text);
        });

    }

    function displayFullscreeenImage() { 
        var p = page.num,
            search = $('.search_ div').length > 0 ? $('#search_text').val() : '',
            libraryId = $('meta[name=book-library-id]').attr('content'),
            pageSrc =  "/local/tools/exalead/getImages.php?book_id=" + book_id + "&p=" + p + "&search=" + search + "";
        if(libraryId) {
            pageSrc += ('&library_id='+libraryId);
        }

        $('.big_image').attr("src", pageSrc + "&width=2000");
        $('#big_image_holder').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
        $('#big_image_holder').find('.spinner-logo').fadeOut(function(){ $(this).detach() });
        $('#big_image_holder').find('.milk-shadow').fadeOut(function(){ $(this).detach() });
        
        $('#big_image_holder').prepend('<div class="milk-shadow draw"></div>');
        FRONT.spinner.execute( $('#big_image_holder')[0], FRONT.spinner.scanLoad);

        $('#big_image').on('load.bigimage',function() {
            $('#big_image_holder').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
            $('#big_image_holder').find('.spinner-logo').fadeOut(function(){ $(this).detach() });
            $('#big_image_holder').find('.milk-shadow').fadeOut(function(){ $(this).detach() });

            $('#big_image').off('load.loadsmallimage');
            $('#big_image').focus();
        }).on('error',function(){
            $('#big_image_holder').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
            $('#big_image_holder').find('.spinner-logo').fadeOut(function(){ $(this).detach() });
            $('#big_image_holder').find('.milk-shadow').fadeOut(function(){ $(this).detach() });
            $('#big_image').off('load.loadsmallimage');
        });;

    }

    function displayNormalImage(){
        var p = page.num,
            search = $('.search_ div').length > 0 ? $('#search_text').val() : '',
            libraryId = $('meta[name=book-library-id]').attr('content'),
            pageSrc =  "/local/tools/exalead/getImages.php?book_id=" + book_id + "&p=" + p + "&search=" + search + "";
        if(libraryId) {
            pageSrc += ('&library_id='+libraryId);
        }
        
        $('.zoomer-image').attr("src",pageSrc);
        $('.viewer_img').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
        $('.viewer_img').find('.spinner-logo').fadeOut(function(){ $(this).detach() });
        $('.viewer_img').prepend('<div class="milk-shadow draw"></div>');

        FRONT.spinner.execute( $('.viewer_img')[0], FRONT.spinner.scanLoad);
        $('.zoomer-image').on('load.loadsmallimage',function(){
            $('.viewer_img').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
            $('.viewer_img').find('.spinner-logo').fadeOut(function(){ $(this).detach() });
            $('.viewer_img').find('.milk-shadow').fadeOut(function(){ $(this).detach() });
            $('.zoomer-image').off('load.loadsmallimage');

            image_start_width = $('#image').width();
            image_start_height = $('#image').height();

        }).on('error',function(){
            $('.viewer_img').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
            $('.viewer_img').find('.spinner-logo').fadeOut(function(){ $(this).detach() });
            $('.viewer_img').find('.milk-shadow').fadeOut(function(){ $(this).detach() });
            $('.zoomer-image').off('load.loadsmallimage');
        });

    }
}
var singleContent = FRONT.events.subscribe('pagechange', FRONT.viewer.getSingleContent);
var singleContentOnSwitch = FRONT.events.subscribe('switchview', FRONT.viewer.getSingleContent);

FRONT.infomodal = function(message) {
    var message = message || {},
        options = message.options || {
            buttonBar: true
        },
        btnPrimaryTitle = message.button || "Ок";
    var btnGroup = $('<div/>').addClass('btn-bar'),
        btnAccept = $('<button/>').addClass('btn').addClass('btn-primary').attr('data-dismiss','modal').text(btnPrimaryTitle);
    if (options.buttonBar) {
        btnGroup.append(btnAccept);
    }
    // if (!message.title) {message.title = 'Внимание';}
    if (!message.textContent) {
        message.obj = $(message.obj) || 'Ошибка в объекте';
    }


    var modalMarkup = $('#universal-modal').clone(true).insertAfter('#universal-modal');
        modalMarkup.removeAttr('id');

    $(modalMarkup)
    .one('show.bs.modal', function(){
        var modal = $(this),
            title = message["title"],
            modalContent;
        if (message.textContent) {
            // modalContent = '<p>'+message.textContent+'</p>';
            modalContent = $('<p/>').html(message.textContent);
            modal.find('.modal-dialog').addClass('modal-sm');
        } else {
            modalContent = message.obj;
        }
        modalContent = modal.find('.modal-body').append(modalContent);

        modal.find('.modal-title').text(title).end()
            // .find('.modal-body').append(modalContent).end()
            .find('.modal-body').append(btnGroup).end()
            .find('.modal-footer').hide();
        if (message.callback) {
            message.callback(modalContent,modal,message.context);
        }
    })
    .one('hidden.bs.modal', function(e){
        $(this).detach();
    })
    .off('keydown')
    .on('keydown', function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            $(btnAccept).click();
        }
        if(event.keyCode == 27) {
            event.preventDefault();
            $(btnAccept).click();
        }
    })
    .modal('show');
}

FRONT.confirm = {}; //  конфирм с бутстрабовским модальным окном
FRONT.confirm.handle = function (message,toggler) {
    return this.modalDialog(message,toggler)
};
FRONT.confirm.modalDialog = function (message,toggler) {
    var dfd = new jQuery.Deferred();
    var result = false,
        message = message || {},
        toggler = toggler || {},
        modalSizeClass = message.modalSize || 'modal-sm',
        btnGroup = $('<div/>').addClass('btn-bar'),
        btnPrimaryTitle = message.confirmTitle || $(toggler).data('button-title') || 'Да',
        btnDefaultText = message.cancelTitle || $(toggler).data('cancel-title') || 'Отмена',
        btnAccept = $('<button/>').addClass('btn').addClass('btn-primary').attr('data-dismiss','modal').text(btnPrimaryTitle),
        btnDefault = $('<button/>').addClass('btn').addClass('btn-default').attr('data-dismiss','modal').text(btnDefaultText);

    btnGroup.append(btnAccept).append(btnDefault);
    $(btnAccept).on('click',function(){
        result = true;
    });

    var modalMarkup = $('#universal-modal').clone(true).insertAfter('#universal-modal');            
        modalMarkup.removeAttr('id');

    $(modalMarkup)
    .one('show.bs.modal', function(e){
        var modal = $(this),
            button = $(e.relatedTarget),
            title = message.title || 'Внимание',
            caption;
        if (message.text) {
            caption = '<p>'+message.text+'</p>';
        } else {
            caption = message.obj;
        }

        modal.find('.modal-dialog').addClass(modalSizeClass).end()
            .find('.modal-title').text(title);

        if (message.text || message.obj || message.callback) {
            modal.find('.modal-body').html(caption);
            modal.find('.modal-footer').append(btnGroup);
        } else {
            modal.find('.modal-body').append(btnGroup);
            modal.find('.modal-footer').hide();
        }

        if (message.callback) {
            message.callback(modal,message.context);
        }
    })
    .one('hidden.bs.modal', function(e){
        $(this).detach();
        dfd.resolve(result);
    })
    .off('keydown')
    .on('keydown', function(event){
        if(event.keyCode == 13) {
            event.preventDefault();            
            $(btnAccept).click();
        }
        if(event.keyCode == 27) {
            event.preventDefault();            
            $(btnDefault).click();
        }
    })
    .modal('show', toggler);
    
    return dfd.promise();
};

FRONT.viewer.addChangeCreation = function(options){
        var addForm = $('[data-add-change-form]').clone(false),
            data_edit = options.data_edit;

        FRONT.infomodal({
            title: options.modalTitle,
            obj: addForm,
            callback: function(markup,modal) {
                var form = markup.find('form'),
                    syncValues = {};

                if ( !options.isAdd ) {
                    jQuery.each(data_edit, function(i, val) {
                        if ('fullSymbolicId' == i) {
                            i = 'book_id';
                        }
                        $("[name='" + i + "']", form).attr('value', val); // для инпутов
                        $("[name='" + i + "']", form).val(val); // для селектов
                    });
                } else {

                    form.on('change',function(){
                        $.each(form.serializeArray(), function(i,key){
                            syncValues[key.name] = key.value;
                            $('[name="'+key.name+'"]', $(options.preForm) ).val(key.value);
                        });
                        $(options.preForm).data('prepared',syncValues);
                    });

                    jQuery.each(data_edit, function(i, val) {
                        $("[name='" + i + "']", form).val(val);
                    });
                    $(modal).one('shown.bs.modal',function(){
                        $('#title',form).focus();
                    });
                }

                var validator = form.validate({ 
                    rules: {
                        "title": {
                            required: true,
                            minlength: 3
                        },
                        "startPage": {
                            required: true,
                            digits: true,
                            lesserThan: "[name='finishPage']",
                            minPage: 1,
                            maxPage: count_page
                        },
                        "finishPage": {
                            required: true,
                            digits: true,
                            greaterThan: "[name='startPage']",
                            maxPage: count_page
                        }
                    },
                    messages: {
                        "title": {
                            required:  "Введите название",
                            minlength: "Не менее 3 символов"
                        },
                        "startPage": {
                            required: "Введите начальный номер электронного образа",
                            digits: "Только цифры",
                            lesserThan : "Начальный номер должен быть меньше конечного",
                            minPage: "Должно быть натуральное число",
                            maxPage: "Число не должно быть больше последней страницы издания"
                        },
                        "finishPage": {
                            required: "Введите конченый номер электронного образа",
                            digits: "Только цифры",
                            greaterThan: "Конечный номер должен быть больше начального",
                            maxPage: "Число не должно быть больше последней страницы издания"
                        }
                    },
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parent());
                        $(element).closest('.form-group').toggleClass('has-error', true);
                    },
                    submitHandler: function(form) {
                        var form = $(form),
                            preForm = $(options.preForm);
                        $(form).find('.btn-primary').attr('disabled','true');

                        FRONT.spinner.execute( $(form).find('.btn-primary').get(0), FRONT.spinner.btnOpts );

                        var m_method = $(form).attr('method');
                        var m_action = $(form).attr('action');
                        var m_data   = $(form).serialize();
                        
                        $.ajax({
                            type: m_method,
                            url:  m_action,
                            data: m_data,
                            success: function(result){
                                if (preForm.length) {
                                    preForm[0].reset();
                                    preForm.data('prepared','');
                                }
                                var rightToolsWidget = $('[data-right-tools-widget]'),
                                    rightChapters = $('.chapter-content', rightToolsWidget),
                                    resId = result.fullSymbolicId,
                                    resIdBook = result.fullSymbolicIdBook,
                                    anchor = result.startPage + '. ' + result.title,
                                    rightChaptersContainer,
                                    rightChaptersItem,
                                    deleteAllChapterButton;

                                var delAllInfo = {
                                        "book_id": resIdBook,
                                        "status": "delete"
                                    },
                                    delInfo = {
                                        "book_id": resId,
                                        "status_code": "delete"
                                    }

                                if ("added" == result.status) {

                                    $(modal).modal('hide');

                                    if ( options.isAdd ) {

                                        deleteAllChapterButton = $('<div class="right-tools-kind" data-button-delete-all="" style="display:none">'
                                            + '<span data-change-status-chapter=""'
                                            + 'class="right-tools-label delete-all-chapters"'
                                            + 'title="Удалить все произведения"'
                                            + 'data-confirm-title="Удалить"'
                                            + 'data-info="">Удалить все</span>'
                                        + '</div>');
                                        
                                        // rightChaptersItem = $('<div data-chapter-item="" data-bitrix="" style="display:none;">'
                                        //  + '<a title="">'
                                        //      + '<span p="" class="title-anchor"></span>'
                                        //      + '<span class="edit" data-edit-chapter="" title="Редактировать произведение" data-info=""></span>'
                                        //      + '<span class="change-status-btn" data-change-status-chapter="" title="Удалить произведение" data-info=""></span>'
                                        //  + '</a>'
                                        // + '</div>');
                                        rightChaptersItem = $('[data-markedup-edition-js-template] [data-chapter-item]').clone(false);

                                        rightChaptersContainer = $('<div class="right-tools-kind" data-dl="" style="display:none;">'
                                            + '<span class="right-tools-label chapter" data-define="" title="Содержание">Содержание</span>'
                                            + '<div class="chapter-content" data-describe="" data-chapter-list="" style="max-height: 386px; display: block;">'
                                                
                                            + '</div>'
                                        + '</div>');

                                        if ( !rightChapters.length ) {
                                            rightToolsWidget.find('.right-tools-kind').eq(3).after(rightChaptersContainer);
                                            rightChapters = $('.chapter-content', rightChaptersContainer);
                                            rightChaptersContainer.slideDown();
                                            
                                            rightToolsWidget.append( $('[data-confirmall-template]').clone('false') );
                                            rightToolsWidget.append( $('[data-declineall-template]').clone('false') );
                                            rightToolsWidget.append(deleteAllChapterButton);
                                            deleteAllChapterButton.find('[data-change-status-chapter]').attr('data-info', JSON.stringify(delAllInfo) );
                                            deleteAllChapterButton.slideDown();
                                        } else {
                                            
                                            if (rightToolsWidget.find('.all-approve-btn').length == 0 && $.inArray('markup_approver',FRONT.user) != -1 ) {
                                                $('[data-confirmall-template]').clone('false').insertBefore(rightToolsWidget.find('[data-button-delete-all]'));
                                            }
                                            if (rightToolsWidget.find('.all-decline-btn').length == 0 && $.inArray('markup_approver',FRONT.user) != -1 ) {
                                                $('[data-declineall-template]').clone('false').insertBefore(rightToolsWidget.find('[data-button-delete-all]'));
                                            }

                                        }

                                        rightChapters.prepend(rightChaptersItem);
                                        var xApprove = {},
                                            xDecline = {};
                                        for (var key in result) {
                                          xApprove[key] = result[key];
                                        }
                                        xApprove['status_code'] = 'confirm';
                                        for (var key in result) {
                                          xDecline[key] = result[key];
                                        }
                                        xDecline['status_code'] = 'decline';
                                        rightChaptersItem.attr('data-bitrix', JSON.stringify(result) );
                                        rightChaptersItem.attr('data-status', 'added' );
                                        rightChaptersItem.find('.approve').attr('data-info', JSON.stringify( xApprove ) );
                                        rightChaptersItem.find('.decline').attr('data-info', JSON.stringify( xDecline ) );
                                        rightChaptersItem.find('.edit').attr('data-info', JSON.stringify(result) );
                                        rightChaptersItem.find('.change-status-btn').attr('data-info', JSON.stringify(delInfo) );
                                        rightChaptersItem.find('.title-anchor').attr('p',JSON.stringify(result.startPage)).text(anchor);
                                        rightChaptersItem.slideDown();
                                    }
                                    // if ( options.isAdd ) {
                                    //     rightChapters.find('[data-chapter-item]').each(function(){
                                    //         var dataInfo = JSON.parse( $(this).find('[data-edit-chapter]').attr('data-info') );
                                    //         if ( dataInfo.fullSymbolicId == resId) {
                                    //             $(this).find('.title-anchor').text(anchor);
                                    //             $(this).find('.edit').attr('data-info', JSON.stringify(result) );
                                    //             $(this).find('.title-anchor').attr('p', JSON.stringify(result.startPage) );
                                    //         }
                                    //     });
                                    // }
                                } else if ("consideration" == result.status){
                                    rightChapters.find('[data-chapter-item]').each(function(){
                                        var dataInfo = JSON.parse( $(this).find('[data-edit-chapter]').attr('data-info') );
                                        if ( dataInfo.fullSymbolicId == resId) {
                                            $(this).find('.title-anchor').text(anchor);
                                            $(this).find('.edit').attr('data-info', JSON.stringify(result) );
                                            $(this).find('.title-anchor').attr('p', JSON.stringify(result.startPage) );
                                        }
                                    });
                                    $(modal).on('hidden.bs.modal',function(){
                                        $('#milk-shadow').toggleClass('draw', true);
                                        FRONT.spinner.execute( $('#milk-shadow')[0] );
                                        $('body').css('overflow','hidden');
                                        window.location.reload();
                                    });
                                    $(modal).modal('hide');
                                } else {
                                    $(form).find('.btn-primary').find('.spinner').detach();
                                    $(form).find('.btn-primary').closest('.button-bar').append('Произошла ошибка');

                                }
                            }
                        });

                        
                    },
                    // set this class to error-labels to indicate valid fields
                    success: function(label) {
                        // set &nbsp; as text for IE
                        //label.html("&nbsp;").addClass("resolved");
                        label.remove();
                    },
                    highlight: function(element, errorClass, validClass) {        
                        $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                        $(element).parent().find("." + errorClass).removeClass("resolved");
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
                    }
                });
                //validator.resetForm();
                form.on('keyup','[name="startPage"], [name="finishPage"]',function(){
                    form.find('[name="startPage"]').valid();
                    form.find('[name="finishPage"]').valid();
                });
            },
            options: {
                buttonBar: false
            }
        });
}

$(function(){

    if ($(document.head).find('[name="userfront"]').data('markup-approver') !== undefined) {
        FRONT.user.push('markup_approver');
    }
    if ($(document.head).find('[name="userfront"]').data('markup-editor') !== undefined) {
        FRONT.user.push('markup_editor');
    }

    jQuery.validator.addMethod('minPage', function (value, el, param) {
        return value >= param;
    });

    jQuery.validator.addMethod('maxPage', function (value, el, param) {
        return value <= param;
    });

    jQuery.validator.addMethod(
        "greaterThan", 
        function(value, element, params) {
            var form = $(element).closest('form');
            // if (!/Invalid|NaN/.test(new Date(value))) {
            //     return new Date(value) >= new Date($(params).val());
            // }
            return isNaN(value) && isNaN($(params, form).val()) 
                || (Number(value) >= Number($(params, form).val())); 
        },
        'Must be greater than {0}.'
    );
    jQuery.validator.addMethod(
        "lesserThan", 
        function(value, element, params) {
            var form = $(element).closest('form');
            // if (!/Invalid|NaN/.test(new Date(value))) {
            //     return new Date(value) <= new Date($(params).val());
            // }
            return isNaN(value) && isNaN($(params, form).val()) 
                || (Number(value) <= Number($(params, form).val())); 
        },
        'Must be greater than {0}.'
    );

    book_id = $('#book_id').val();
    count_page = parseInt($('#count_page').val());
    positionpart = parseInt($('#positionpart').val());

    var p = parseInt($('#page-number').val());

    var max_width_img = 800;
    var max_height_img = 1000;

    $('.toggle-view').click(function(e){
        e.preventDefault();
        if($(this).hasClass('image-now')){
            $('.rotate, .superscale-page, .save').parent().addClass('tool-disabled').slideUp('fast');
            $('.jcrop-holder>div:first-child').hide();
            $('#selectedrect').hide();
            $('.jcrop-holder').hide();

            $('[data-view-port]').each(function(){
                $(this).attr('data-view-port','');
                $(this).data('viewPort','')
            });
            $('[data-view-name="display-text"]').data('viewPort','switched-on');
            $('[data-view-name="display-text"]').attr('data-view-port','switched-on');

            FRONT.events.publish('switchview', FRONT.viewer.UX.currentPage);
            $('.text-view-wrapper').show();

            $(this).removeClass('image-now').addClass('text-now').text('Показывать как картинку');
        }
        else {
            $('.rotate, .superscale-page, .save').parent().addClass('tool-disabled').slideDown('fast');
            $('.jcrop-holder>div:first-child').hide();
            $('#selectedrect').hide();
            $('.jcrop-holder').show();

            $('[data-view-port]').each(function(){
                $(this).attr('data-view-port','');
                $(this).data('viewPort','')
            });
            $('[data-view-name="simple-image"]').data('viewPort','switched-on');
            $('[data-view-name="simple-image"]').attr('data-view-port','switched-on');

            FRONT.events.publish('switchview', FRONT.viewer.UX.currentPage);
            $('.text-view-wrapper').hide();

            $(this).removeClass('text-now').addClass('image-now').text('Показывать как текст');         
        }
    });

    if ( window.location.search.indexOf('isBlind') != -1 ) {
        setTimeout(function(){
            $('.toggle-view').trigger('click');
        },2000);
    }

    getImage(p,'docready');
    // getText(p);
    /* $(".viewer").zoomer({
        controls: {
            zoomIn: ".increase",
            zoomOut: ".reduce"
        }
    });  */

    /* toggle left tools */

    $('.toggle-left-tools').click(function() {
        $(this).toggleClass('close');
        $('.prev-page').toggleClass('left');

        if ($('.left-tools').is(':visible')) {
            $('.left-tools').slideUp(200);
        } else {
            $('.left-tools').slideDown(200);
        }
    });
    /* end toggle left tools */

    /* show submenu */
    $('.left-tools li.sub').hover(
        function() {
            if(!$(this).hasClass('.tool-disabled')){
                $(this).children('ul').show();
            }
        },
        function() {
            $(this).children('ul').hide();
        }
    );
    /* end show submenu */


    /* show right tools */

    $(document).on('click','[data-define]',function(e){
        var toggler = $(this),
            togglersDescription = toggler.next('[data-describe]'),
            widget = toggler.closest('[data-dl-accordion]'),
            otherDescriptions = $('[data-describe]', widget).not(togglersDescription),
            otherDescriptions = otherDescriptions.add('.properties-book-content'),
            animationInProgress = false;

        if ( togglersDescription.is(':visible') || togglersDescription.css('display') == 'block' ) {
            togglersDescription.stop().slideToggle(200,function(){
                toggler.toggleClass('active', togglersDescription.is(':visible'));
            });
        } else {
            togglersDescription.slideToggle(200,function(){
                toggler.toggleClass('active',true);
            });
            otherDescriptions.slideUp(200,function(){
                $(this).prev('[data-define]').toggleClass('active',false);
            });
        }
    });

    $(document).on('click','.properties-book',function(e){
        e.preventDefault();
        var toggler = $(this),
            descr = $('.properties-book-content'),
            others = $('[data-dl-accordion] [data-describe]');

        others.slideUp(200);
        
        if ( descr.is(':visible') || descr.css('display') == 'block' ) {
            descr.stop().slideToggle(200,function(){
                toggler.toggleClass('active', descr.is(':visible'));
                toggler.attr('aria-expanded', descr.is(':visible'));
                descr.attr('aria-hidden', false);
            });
        } else {
            descr.slideToggle(200,function(){
                toggler.toggleClass('active',true);
                toggler.attr('aria-expanded',true);
                descr.attr('aria-hidden', descr.is(':visible'));
            });
            others.slideUp(200,function(){
                $(this).prev('[data-define]').toggleClass('active',false);
            });
        }

    });

    /* end show right tools */

    /* footer */
    $('.toggle-page-navigation').click(function() {
        $(this).toggleClass('active');
        $('.page-navigation').toggleClass('active');
    });
    /* end footer */



    $(document).on('keyup',function(e) {
        if (e.which == 39 && !$(e.target).hasClass('crop-text') && $(e.target).prop('tagName') !== 'INPUT' && $(e.target).prop('tagName') !== 'TEXTAREA' ) {
            $('.main .next-page').focus().click();
        }
        if (e.which == 37 && !$(e.target).hasClass('crop-text') && $(e.target).prop('tagName') !== 'INPUT' && $(e.target).prop('tagName') !== 'TEXTAREA' ) {
            $('.main .prev-page').focus().click();
        }
        // поиск по Enter
        if (e.keyCode == 13 && $(".page-navigation input.page:focus").length) {
            if ($.trim($('.page-navigation input.page').val()) != '') {
                $('.go').click();
            }
        }
        if (e.keyCode == 13 && $("#search_text:focus").length) {
            if ($.trim($('#search_text').val()) != '') {
                $('#search_submit').click();
            }
        }

        if (e.keyCode == 27) {
            $(".b-headernav_login").fadeOut("slow");
            $('#selectedrect').css({
                'display': 'none'
            });
            $('.layer_notes').css({
                'display': 'none'
            });
            $('#text_note').val('');
        }
    });

    // смена страницы
    $('.next-page, .prev-page').on('click', function() {
        var p = parseInt($(this).attr('p'));
        // FRONT.viewer.turnThePage(p);
        var page = parseInt($('#page-number').val());
        if (p != page) {
            // getText(p);
            getImage(p,'nexpageonclick');
        }
    });

    $('.page-navigation .go').click(function() {
        var p = parseInt($('.page-navigation input.page').val())
            // получение страницы
        var page = parseInt($('#page-number').val());
        if (p != page) {
            // getText(p);
            getImage(p,'gopageclick');
        }
    });


// перевернуть страницу по часовой стрелки
(function($) { //create closure
    $.fn.rotateRight = function() {
        $(this).click(function() {
            angle += 90;
            if (angle == 360) angle = 0;
            $('.viewer').rotate(angle);
            $('#selectedrect').css({'display':'none'});
            $('.layer_notes').css({'display':'none'});
            $('#text_note').val('');
            $('.jcrop-holder>div:first-child').hide();
            if (angle == 0) {
                $('.jcrop-tracker').show();
                $("div.quote-book, div.note-book").css('display', 'block');
            } else {
                $('.jcrop-tracker').hide();
                $("div.quote-book, div.note-book").css('display', 'none');
            }
            // $('#image').rotate(angle);
        });

    };
    //end of closure
})(jQuery);

// перевернуть страницу против часовой стрелки
(function($) { //create closure
    $.fn.rotateLeft = function() {
        $(this).click(function() {
            angle -= 90;
            if (angle == -360) angle = 0;
            $('.viewer').rotate(angle);
            $('#selectedrect').css({'display':'none'});
            $('.layer_notes').css({'display':'none'});
            $('#text_note').val('');
            $('.jcrop-holder>div:first-child').hide();
            if (angle == 0) {
                $('.jcrop-tracker').show();
                $("div.quote-book, div.note-book").css('display', 'block');
            } else {
                $('.jcrop-tracker').hide();
                $("div.quote-book, div.note-book").css('display', 'none');
            }
            // $('#image').rotate(angle);
        });
    };
    //end of closure
})(jQuery);


    // сохранение страницы
    $('.left-tools .save').click(function(e) {
        e.preventDefault();
        var page = $('#page-number').val();
        var url = '/local/tools/exalead/savePage.php?book_id=' + book_id + '&p=' + page;

        window.location.href = url;
    });

    // подгрузка страниц в содержание
    $('.miniatures').click(function() {
        var page = $('#page-number').val();
        var count = 4;
        if (count_page < count) count = count_page;
        if ($('.miniatures-content div.miniatures-images').children('span').length == 0) {
            var i;
            for (i = 1; i <= count; i++) {
                $('.miniatures-content div.miniatures-images').append('<span  p=' + i + ' ><img src="/local/tools/exalead/thumbnails.php?book_id=' + book_id + '&p=' + i + '"></span>');
            }

            $(".miniatures-content div.miniatures-images span[p = " + page + "]").addClass('active');

            $('.miniatures-content div.miniatures-images span img').load(function() {
                $('.miniatures-content div.miniatures-images span[p=1] img, .miniatures-content div.miniatures-images span[p=2] img, .miniatures-content div.miniatures-images span[p=3] img, .miniatures-content div.miniatures-images span[p=4] img').css('display', 'block')
            });
        }
    });


    // содержание - следующий элемент
    $('.miniatures-content div.miniatures-top div,.miniatures-content div.miniatures-bottom div').on('click', function() {
        var p = parseInt($(this).attr('p'));

        var page = parseInt($('#page-number').val());

        if (p > 0 && p <= count_page) {
            $(".miniatures-content div.miniatures-images span").removeClass('active');
            //следующий элемент
            if ($(this).hasClass('next') == true) {
                $('.miniatures-content div.miniatures-images span:first').remove();
                $('.miniatures-content div.miniatures-images').append('<span p=' + p + '><img  src="/local/tools/exalead/thumbnails.php?book_id=' + book_id + '&p=' + p + '"></span>');
                $('.miniatures-content div.miniatures-bottom div.next').attr('p', p + 1);
                $('.miniatures-content div.miniatures-top div.prev').attr('p', p - 4);

            } else {
                $('.miniatures-content div.miniatures-images span:last-child').remove();
                $('.miniatures-content div.miniatures-images').prepend('<span p=' + p + '><img  src="/local/tools/exalead/thumbnails.php?book_id=' + book_id + '&p=' + p + '" p=' + p + '></span>');
                $('.miniatures-content div.miniatures-bottom div.next').attr('p', p + 4);
                $('.miniatures-content div.miniatures-top div.prev').attr('p', p - 1);
            }
            $(".miniatures-content div.miniatures-images span[p = " + page + "]").addClass('active');
            $('.miniatures-content div.miniatures-images span[p=' + p + '] img').load(function() {
                $('.miniatures-content div.miniatures-images span[p=' + p + '] img').css('display', 'block')
            });
        }
    });

    // смена страницы
    $(document).on('click','div.miniatures-images span, div.miniatures-images a, div.bookmark-content a, div.quote-content a, div.note-content a, div.search_result a', function() {
        var p = parseInt($(this).attr('p'));
        // получение страницы
        var page = parseInt($('#page-number').val());
        if (p != page){
            // getText(p);
            getImage(p,'notecontentwtf');
        }
    });

    $(document).on('click','[data-chapter-item]', function(e){
        var p = parseInt($(this).find('.title-anchor').attr('p'));
        if ( $(e.target).attr('class') == 'title-anchor' ) {
            getImage(p,'datachapteritemclick');
        }
    });

    // добавление/удаление в личный кабинет
    $('.favorite-book').click(function(event) {
        var toggler = $(this);
        if ($('form.b-formlogin')[0]) {
            event.preventDefault();
            $('.b_login_popup').show();
        } else {
            var url = '';
            if ($(this).hasClass("active") == true) {

                $.when( FRONT.confirm.handle({title:"Удалить книгу из избранного?", confirmTitle: "Удалить"},toggler) ).then(function(confirmed){
                    if(confirmed){
                        url = toggler.attr('data-url');
                        toggler.attr('title', 'Добавить в личный кабинет');
                        toggler.html('Добавить в избранное');
                        $.ajax({
                            type: "GET",
                            url: url,
                            success: function(data) {
                                $('.favorite-book').toggleClass("active");
                            }
                        });
                    }
                });

            } else {
                url = toggler.attr('data-collection');
                toggler.attr('title', 'В личном кабинете');
                toggler.html('Удалить из избранного');
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(data) {
                        FRONT.infomodal({title:"Книга добавлена в избранное"});
                        $('.favorite-book').toggleClass("active");
                    }
                });
            }
            
        }
    });

    /*$('.main *').click(function(){
        if ($(this).attr("class") != 'view-page'){
      $(".b-headernav_login").fadeOut("slow");
        }
        

    })
    */

    //добавление в закладки
    $('.bookmark-tools, .add-bookmark').click(function(event) {
        event.preventDefault();
        if ($('form.b-formlogin')[0]) {
            $('.b-headernav_login').slideDown();
            $('.b_login_popup').show();
        } 
        else {
            
            var page = parseInt($('#page-number').val());
            if ($(this).hasClass('active') != true) {

                if ($('.favorite-book').hasClass('active') == false) {
                    $('.favorite-book').click();
                }

                $.ajax({
                    type: "GET",
                    url: "/local/tools/viewer/bookmark_list.php",
                    data: "book_id=" + book_id + "&page=" + page,
                    success: function(msg) {

                        if (parseInt(msg) > 0) {                            
                            FRONT.infomodal({title:"Закладка добавлена"});
                            $.when ( pagesContent.rewriteEditionDataStore() ).then(function(data){
                                FRONT.viewer.buildRightBookmarks();
                                isMark(page);
                            });

                            // $('.bookmark-content').prepend('<div><a p="' + page + '"><span>' + page + '</span> страница</a><span id=' + msg + ' class="delete"></span></div>');
                        }
                    }
                });
            } else {
                $('.bookmark-content a[p=' + page + ']').next('span.delete').click(); //!!!

            }
        }
    });

    // удалить закладку
    $(document).on('click', '.bookmark-content span.delete', function() {
        //toggleBookmark

        var toggler = $(this),
            mark_id = this.id,
            url = '/local/tools/viewer/bookmark_list.php',
            page = parseInt($('#page-number').val());

        $.when( FRONT.confirm.handle({title:"Удалить закладку?", confirmTitle: "Удалить"},toggler) ).then(function(confirmed){
            if(confirmed){
                $.ajax({
                    type: "GET",
                    url: url + '?mark_id=' + mark_id,
                    success: function(data) {
                        // $('#' + mark_id).parent('div').remove();
                        // pagesContent.getEditionData(true);
                        // isMark(page);
                        $.when ( pagesContent.rewriteEditionDataStore() ).then(function(data){
                            FRONT.viewer.buildRightBookmarks();
                            isMark(page);
                        });
                    }
                });
            }
        });
    });

    // добавление в цитаты
    $('#b-textLayer_quotes').click(function(event) {
        event.preventDefault();
        var that = this;
        if ($('form.b-formlogin')[0]) {
            $('.b-headernav_login').slideDown();
            $('.b_login_popup').show();
        } 
        else {
            if(!$(this).hasClass('load-flag')){
                $(this).addClass('load-flag');

                if ($('.favorite-book').hasClass('active') == false) {
                    $('.favorite-book').click();
                }
                $('.layer_notes').css({
                    'display': 'none'
                });
                var url = $(this).attr('href');
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(data) {
                        $('.quote-content').append(data);
                        var p = parseInt($('#page-number').val());
                        getImage(p,'govnokodclick');
                        FRONT.infomodal({title:"Цитата добавлена"});
                        $('#selectedrect').css({
                            'display': 'none'
                        });
                        setTimeout(function(){
                            $(that).removeClass('load-flag');
                        }, 500);
                    }
                });
            }
        }

    });

    // добавление в заметки
    $('#b-textLayer_notes').click(function(event) {
        if ($('form.b-formlogin')[0]) {
            event.preventDefault();
            $('.b_login_popup').show();
        } else {
            $('.layer_notes').css('display', 'block');
            event.preventDefault();
        }
    });

    $('#add_note').click(function(event) {

        elem = $(this);
        if(!elem.hasClass('load-flag')){
            elem.addClass('load-flag');
            if ($('.favorite-book').hasClass('active') == false) {
                $('.favorite-book').click();
            }
            var url = $('#b-textLayer_notes').attr('href');
            var note = $('#text_note').val();
            $.ajax({
                type: "GET",
                url: url + '&note=' + note,
                success: function(data) {
                    FRONT.infomodal({title:"Заметка добавлена"});
                    var p = parseInt($('#page-number').val());
                    getImage(p,'add_note click');
                    $('#selectedrect').css({
                        'display': 'none'
                    });
                    $('.note-content').append(data);
                    $('.note').addClass('active');
                    elem.removeClass('load-flag');
                    setTimeout(function(){
                        $(this).removeClass('load-flag');
                    }, 500);
                }
            })
            event.preventDefault();
        }
    });

    // удалить цитату
    $('.quote-content').on('click', 'span.delete', function() {
        var toggler = $(this);
        var quo_id = this.id;
        var url = '/local/tools/exalead/saveImg.php';

        $.when( FRONT.confirm.handle({title:"Удалить цитату?", confirmTitle: "Удалить"},toggler) ).then(function(confirmed){
            if(confirmed){
                $.ajax({
                    type: "GET",
                    url: url + '?quo_id=' + quo_id,
                    success: function(data) {
                        var sibs = $('#' + quo_id).parent('div').siblings('div');
                        if (!sibs.length) {
                            $('#' + quo_id).closest('.quote-content').siblings('.quote').click();
                        }
                        $('#' + quo_id).parent('div').remove();
                        var p = parseInt($('#page-number').val());
                        getImage(p,'далили цитату');
                    }
                })
            }
        });

        
    });
    // удалить закладку
    $('.note-content').on('click', 'span.delete', function() {
        var toggler = $(this);
        var note_id = this.id;
        var url = '/local/tools/exalead/saveImg.php';

        $.when( FRONT.confirm.handle({title:"Удалить заметку?", confirmTitle: "Удалить"},toggler) ).then(function(confirmed){
            if(confirmed){
                $.ajax({
                    type: "GET",
                    url: url + '?note_id=' + note_id,
                    success: function(data) {
                        var sibs = $('#' + note_id).parent('div').siblings('div');
                        if (!sibs.length) {
                            $('#' + note_id).closest('.note-content').siblings('.note').click();
                        }
                        $('#' + note_id).parent('div').remove();
                        var p = parseInt($('#page-number').val());
                        getImage(p,'удалили закладку');
                    }
                })
            }
        });
                
    });

    //Запрет скачивания книги без авторизации
    $('.bookmark-tools, .add-bookmark').click(function(event) {
        event.preventDefault();
        if ($('form.b-formlogin')[0]) {
            $('.b-headernav_login').slideDown();
            $('.b_login_popup').show();
        }
    });

    // ================================================================================================================
    $('[data-prefill]').on('submit',function(e){
        e.preventDefault();
        var form = $(this),
            values = {},
            options = {};

        $.each(form.serializeArray(), function(i,key){
            if (values.hasOwnProperty(key.name)) {
                values[key.name] = $.makeArray(values[key.name]);
                values[key.name].push(key.value);
            }
            else {
                values[key.name] = key.value;
            }
        });
        options.isAdd = true;
        options.preForm = form;
        options.modalTitle = 'Добавить произведение';
        options.data_edit = JSON.parse(form.attr('data-info'));
        if (options.preForm.data('prepared')) {
            $.extend(options.data_edit,options.preForm.data('prepared'));
        }
        $.extend(options.data_edit,values);
        FRONT.viewer.addChangeCreation(options);
    });

    $(document).on('click','[-data-add-chapter],[data-edit-chapter]',function(e){
        e.preventDefault();
        var options = {},
            toggler = $(this);

        options.isAdd = toggler[0].hasAttribute("data-add-chapter");
        options.modalTitle = (options.isAdd) ? 'Добавить произведение' : 'Редактировать произведение';
        options.data_edit = JSON.parse(toggler.attr('data-info'));

        FRONT.viewer.addChangeCreation(options);
    });

    // $(document).on('click','[data-add-chapter],[data-edit-chapter]',function(e){
    //  e.preventDefault();
    //  var addForm = $('[data-add-change-form]').clone(false),
    //      toggler = $(this),
    //      modalTitle = (toggler[0].hasAttribute("data-add-chapter")) ? 'Добавить произведение' : 'Редактировать произведение',
    //      data_edit = JSON.parse(toggler.attr('data-info'));

    //  FRONT.infomodal({
    //      title: modalTitle,
    //      obj: addForm,
    //      callback: function(markup,modal) {
    //          var form = markup.find('form');

    //          if ( !toggler[0].hasAttribute("data-add-chapter") ) {
    //              jQuery.each(data_edit, function(i, val) {
    //                  if ('fullSymbolicId' == i) {
    //                      i = 'book_id';
    //                  }
    //                  $("[name='" + i + "']", form).attr('value', val); // для инпутов
    //                  $("[name='" + i + "']", form).val(val); // для селектов
    //              });
    //          } else {
    //              $('#startPage',form).val(FRONT.viewer.UX.currentPage);
    //              $(modal).one('shown.bs.modal',function(){
    //                  $('#title',form).focus();
    //              });
    //          }

    //          var validator = form.validate({ 
    //              rules: {
    //                  "title": {
    //                      required: true,
    //                      minlength: 3
    //                  },
    //                  "startPage": {
    //                      required: true,
    //                      digits: true,
    //                      lesserThan: "[name='finishPage']"
    //                  },
    //                  "finishPage": {
    //                      required: true,
    //                      digits: true,
    //                      greaterThan: "[name='startPage']"
    //                  }
    //              },
    //              messages: {
    //                  "title": {
    //                      required:  "Введите название",
    //                      minlength: "Не менее 3 символов"
    //                  },
    //                  "startPage": {
    //                      required: "Введите начальный номер электронного образа",
    //                      digits: "Только цифры",
    //                      lesserThan : "Начальный номер должен быть меньше конечного"
    //                  },
    //                  "finishPage": {
    //                      required: "Введите конченый номер электронного образа",
    //                      digits: "Только цифры",
    //                      greaterThan: "Конечный номер должен быть больше начального"
    //                  }
    //              },
    //              errorPlacement: function(error, element) {
    //                  error.appendTo(element.parent());
    //                  $(element).closest('.form-group').toggleClass('has-error', true);
    //              },
    //              submitHandler: function(form) {
    //                  var form = $(form);
    //                  $(form).find('.btn-primary').attr('disabled','true');

    //                  FRONT.spinner.execute( $(form).find('.btn-primary').get(0), FRONT.spinner.btnOpts );

    //                  var m_method = $(form).attr('method');
    //                  var m_action = $(form).attr('action');
    //                  var m_data   = $(form).serialize();
                        
    //                  $.ajax({
    //                      type: m_method,
    //                      url:  m_action,
    //                      data: m_data,
    //                      success: function(result){
    //                          var rightToolsWidget = $('[data-right-tools-widget]'),
    //                              rightChapters = $('.chapter-content', rightToolsWidget),
    //                              resId = result.fullSymbolicId,
    //                              resIdBook = result.fullSymbolicIdBook,
    //                              anchor = result.startPage + '. ' + result.title,
    //                              rightChaptersContainer,
    //                              rightChaptersItem,
    //                              deleteAllChapterButton;

    //                          var delAllInfo = {
    //                                  "book_id": resIdBook,
    //                                  "status": "delete"
    //                              },
    //                              delInfo = {
    //                                  "book_id": resId,
    //                                  "status_code": "delete"
    //                              }

    //                          if ("added" == result.status || "consideration" == result.status) {

    //                              $(modal).modal('hide');

    //                              if ( toggler[0].hasAttribute("data-add-chapter") ) {

    //                                  deleteAllChapterButton = $('<div class="right-tools-kind" data-button-delete-all="" style="display:none">'
    //                                      + '<span data-change-status-chapter=""'
    //                                      + 'class="right-tools-label delete-all-chapters"'
    //                                      + 'title="Удалить все произведения"'
    //                                      + 'data-info="">Удалить все</span>'
    //                                  + '</div>');
                                        
    //                                  rightChaptersItem = $('<div data-chapter-item="" data-bitrix="" style="display:none;">'
    //                                      + '<a title="">'
    //                                          + '<span p="" class="title-anchor"></span>'
    //                                          + '<span class="edit" data-edit-chapter="" title="Редактировать произведение" data-info=""></span>'
    //                                          + '<span class="change-status-btn" data-change-status-chapter="" title="Удалить произведение" data-info=""></span>'
    //                                      + '</a>'
    //                                  + '</div>');

    //                                  rightChaptersContainer = $('<div class="right-tools-kind" data-dl="" style="display:none;">'
    //                                      + '<span class="right-tools-label chapter" data-define="" title="Содержание">Содержание</span>'
    //                                      + '<div class="chapter-content" data-describe="" data-chapter-list="" style="max-height: 386px; display: block;">'
                                                
    //                                      + '</div>'
    //                                  + '</div>');

    //                                  if ( !rightChapters.length ) {
    //                                      rightToolsWidget.find('.right-tools-kind').eq(3).after(rightChaptersContainer);
    //                                      rightChapters = $('.chapter-content', rightChaptersContainer);
    //                                      rightChaptersContainer.slideDown();
                                            
    //                                      rightToolsWidget.append(deleteAllChapterButton);
    //                                      deleteAllChapterButton.find('[data-change-status-chapter]').attr('data-info', JSON.stringify(delAllInfo) );
    //                                      deleteAllChapterButton.slideDown();
    //                                  }

    //                                  rightChapters.prepend(rightChaptersItem);

    //                                  rightChaptersItem.attr('data-bitrix', JSON.stringify(result) );
    //                                  rightChaptersItem.find('.edit').attr('data-info', JSON.stringify(result) );
    //                                  rightChaptersItem.find('.change-status-btn').attr('data-info', JSON.stringify(delInfo) );
    //                                  rightChaptersItem.find('.title-anchor').attr('p',JSON.stringify(result.startPage)).text(anchor);
    //                                  rightChaptersItem.slideDown();
    //                              }
    //                              if ( toggler[0].hasAttribute("data-edit-chapter") ) {
    //                                  rightChapters.find('[data-chapter-item]').each(function(){
    //                                      var dataInfo = JSON.parse( $(this).find('[data-edit-chapter]').attr('data-info') );
    //                                      if ( dataInfo.fullSymbolicId == resId) {
    //                                          $(this).find('.title-anchor').text(anchor);
    //                                          $(this).find('.edit').attr('data-info', JSON.stringify(result) );
    //                                          $(this).find('.title-anchor').attr('p', JSON.stringify(result.startPage) );
    //                                      }
    //                                  });
    //                              }
    //                          } else {
    //                              $(form).find('.btn-primary').find('.spinner').detach();
    //                              $(form).find('.btn-primary').closest('.button-bar').append('Произошла ошибка');

    //                          }
    //                      }
    //                  });

                        
    //              },
    //              // set this class to error-labels to indicate valid fields
    //              success: function(label) {
    //                  // set &nbsp; as text for IE
    //                  //label.html("&nbsp;").addClass("resolved");
    //                  label.remove();
    //              },
    //              highlight: function(element, errorClass, validClass) {        
    //                  $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
    //                  $(element).parent().find("." + errorClass).removeClass("resolved");
    //              },
    //              unhighlight: function(element, errorClass, validClass) {
    //                  $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
    //              }
    //          });
    //          //validator.resetForm();
    //          form.on('keyup','[name="startPage"], [name="finishPage"]',function(){
    //              form.find('[name="startPage"]').valid();
    //              form.find('[name="finishPage"]').valid();
    //          });
    //      },
    //      options: {
    //          buttonBar: false
    //      }
    //  });
    // });


    // добавление / редактирование проиведения издания
    $('.b-formchapter').submit(function(e){
        e.preventDefault();
        var m_method = $(this).attr('method');
        var m_action = $(this).attr('action');
        var m_data   = $(this).serialize();
        
        $.ajax({
            type: m_method,
            url:  m_action,
            data: m_data,
            success: function(result){
                if ("added" == result.status || "consideration" == result.status) {
                    $('.b-headernav_markup').slideUp(300);
                }
            }
        });
    });

    // закрытие окна редактирования произведения
    $('.close-window').click(function() {
        $('.b-headernav_markup').slideUp(300);
    });

    // ================================================================================================================

    $(document).on('click','[data-change-status-chapter]', function(e){
        var toggler = $(e.target),
            data_edit = JSON.parse( toggler.attr('data-info') ),
            chapterItem = toggler.closest('[data-chapter-item]'),
            chapterList = $('[data-chapter-list]'),
            rightToolsWidget = $('[data-right-tools-widget]'),
            message = {
                title: toggler.attr('title') || "Удалить произведение",
                confirmTitle: toggler.attr('data-confirm-title') || "Подтвердить",
                modalSize: 'modal-st',
            },
            bookId = (data_edit.fullSymbolicId) ? data_edit.fullSymbolicId : data_edit.book_id,
            isAllDecline = $(toggler).hasClass('all-decline-btn'),
            isAllApprove = $(toggler).hasClass('all-approve-btn');
        var dd = {
                book_id: bookId
        };

        if ( isAllDecline ) {
            message.callback = allDeclinePreview
        }

        function allDeclinePreview(modal,context){
            var opts = {};
            for (var key in FRONT.spinner.btnOpts) {
                opts[key] = FRONT.spinner.btnOpts[key]
            }
            opts.color = '#00708c';
            opts.position = 'relative';
            $(modal).find('.modal-body').append('<p>Вы действительно хотите отклонить все последние изменения в произведениях?</p><center></center>');
            FRONT.spinner.execute( $(modal).find('.modal-body').find('center')[0], opts );
            $.ajax({
                url: '/local/tools/viewer/markup_prev.php',
                data: dd,
                method: 'GET',
                success: function(data){
                    var result = data;
                    $(modal).find('.modal-body').find('.spinner').detach();
                    
                    listObj = $('<dl class="data-dl">'),
                    preAnswerList = listObj.filter('dl');
                    if ($.isArray(result)) {
                        if ( result.length > 0 ) {
                            $(modal).find('.modal-body').append( $('<p>Ожидаются следующие изменения:</p>') );
                        }
                        $.each(data, function(i,val){
                            var resItemId = parseInt(val['id']),
                                resItemFullId = val['fullSymbolicId'],
                                resStatus = val['status'],
                                itemResKey = $('<dt>').text(val['title']),
                                itemResVal = $('<dd>'),
                                statusMap = {
                                    deleted: 'произведение удалено',
                                    confirmed: 'произведение подтверждено'
                                },
                                statusMapped = statusMap[ val['status'].toString() ];
                            if (statusMapped == 'undefined') {
                                statusMapped = 'неизвестный ответ сервера'
                            }
                            itemResVal.text(statusMapped);
                            preAnswerList.append(itemResKey);
                            preAnswerList.append(itemResVal);
                            preAnswerList.appendTo(listObj);
                        });
                    }
                    listObj.appendTo( $(modal).find('.modal-body') );

                }
            }).fail(function(){
                $(modal).find('.modal-body').find('.spinner').detach();
                // FRONT.infomodal({title:"Произошла ошибка"});
            });
        }


        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed){
                $.ajax({
                    url: '/local/tools/viewer/markup_status.php',
                    data: {
                        book_id: bookId,
                        status: data_edit.status_code
                    },
                    method: 'GET',
                    success: function(data){
                        var result = data,
                            resLength = 0,
                            displayRes = [],
                            messObj = $('<dl class="data-dl">'),
                            answerList = messObj.filter('dl');

                        if ($.isArray(result)) {
                            resLength = result.length;
                            $.each(data, function(i,val){
                                var resItemId = parseInt(val['id']),
                                    resItemFullId = val['fullSymbolicId'],
                                    resStatus = val['status'],
                                    itemResKey = $('<dt>').text(val['title']),
                                    itemResVal = $('<dd>'),
                                    statusMap = {
                                        deleted: 'произведение удалено',
                                        confirmed: 'произведение подтверждено'
                                    },
                                    statusMapped = statusMap[ val['status'].toString() ];
                                if (statusMapped == 'undefined') {
                                    statusMapped = 'неизвестный ответ сервера'
                                }
                                itemResVal.text(statusMapped);

                                if (isAllApprove) {
                                    $('[data-chapter-list] [data-chapter-item]').each(function(){
                                        var chapter = $(this),
                                            chapterStatus = chapter.data('status'),
                                            chapterData = JSON.parse( chapter.attr('data-bitrix') ),
                                            chapterId = parseInt(chapterData['id']);
                                        if (resItemId == chapterId && (chapterStatus == 'consideration' || chapterStatus == 'added') ) {
                                            answerList.append(itemResKey);
                                            answerList.append(itemResVal);
                                            answerList.appendTo(messObj);
                                        }
                                    });
                                } else {
                                    answerList.append(itemResKey);
                                    answerList.append(itemResVal);
                                    answerList.appendTo(messObj);
                                }

                                chapterList.find('[data-chapter-item]').each(function(){
                                    var item = $(this),
                                        itemData = JSON.parse( item.attr('data-bitrix') ),
                                        itemFullId = itemData['fullSymbolicId'];
                                        itemId = parseInt(itemData['id']);
                                    if ( itemFullId == resItemFullId && resStatus == 'deleted' ) {
                                        item.slideUp(function(){
                                            $(this).detach();
                                            if ( !(chapterList.find('[data-chapter-item]').length) ) {
                                                $(chapterList).closest('[data-dl]').slideUp(function(){
                                                    $(this).detach();                                               
                                                });
                                                rightToolsWidget.find('[data-button-delete-all], .all-approve-btn, .all-decline-btn').each(function(){
                                                    $(this).slideUp(function(){
                                                        $(this).detach();                                               
                                                    });
                                                });
                                            }
                                        });
                                    }
                                    else if ( itemFullId == resItemFullId ) {
                                        item.attr('data-status', resStatus);
                                    }
                                });
                            });
                            FRONT.infomodal({
                                title:"Ответ на запрос",
                                obj:messObj,
                                callback: function(modalContent,modal,context){
                                    if (isAllDecline || isAllApprove) {
                                        $(modal).on('hidden.bs.modal',function(){
                                            $('#milk-shadow').toggleClass('draw', true);
                                            FRONT.spinner.execute( $('#milk-shadow')[0] );
                                            $('body').css('overflow','hidden');
                                            window.location.reload();
                                        });
                                    }
                                }
                            });
                        } else {
                            FRONT.infomodal({title:"Неожиданный ответ сервера"});
                        }
                    }
                }).fail(function(){
                    FRONT.infomodal({title:"Произошла ошибка"});
                });
            }
        });
    });

    // открытие окна изменения статуса
    // $('.change-status-btn').click(function(event) {

    //  // заполнение формы
    //  var data_edit = JSON.parse($(this).attr('data-info'));
    //  $('#book_status_id').attr('value', data_edit.book_id);
    //  $('#book_status_code').attr('value', data_edit.status_code);
        
    //  $('#book_status_name').html(data_edit.change);
    //  $('#book_status_title').html(data_edit.info);

    //  // показ формы
    //  $('.b-headernav_markup.delete').slideDown();
    //  event.preventDefault();
    //  $('.b_markup_markup.delete').show();
    // });

    // отправка формы изменения статуса на сервер
    $('.b-headernav_markup.delete .b-form').submit(function(e){

        e.preventDefault();
        var m_method = $(this).attr('method');
        var m_action = $(this).attr('action');
        var m_data   = $(this).serialize();

        $.ajax({
            type: m_method,
            url:  m_action,
            data: m_data,
            success: function(result){
                $('.b-headernav_markup').slideUp(300);
            }
        });
    });

    // закрытие окна редактирования произведения
    $('.b-headernav_markup.delete .close-win').click(function() {
        $('.b-headernav_markup').slideUp(300);
    });
    
    // ================================================================================================================

    $('.move').click(function() {
        $(this).toggleClass("active");
        if ($(this).hasClass("active") == true) {
            $('.zoomer-image').css('cursor', 'move');
        } else {
            $('.zoomer-image').css('cursor', 'default');
        }
    });

    function getCookieExt(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    // поиск
    $('#search_submit').click(function() {
        var search_text = encodeURI($('#search_text').val());
        $('.search_preloader').show();
        $.ajax({
            type: "GET",
            url: "/local/tools/exalead/search.php?search_text=" + search_text + "&book_id=" + book_id,
            success: function(data) {
                $('.search_preloader').hide();
                $('.search_result div').remove();
                $('.search_result').append(data).css('display', 'block');
                if (getCookieExt('__qSearch') !== undefined && window.triggerOwnerLibrary === true ){
                    $($('.search_result div a')[0]).trigger('click');
                    delete window.triggerOwnerLibrary;
                }
            }
        })
    });


    function launchIntoFullscreen(element) {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }

    function exitFullscreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
    

    $(document).on('click','[data-view-fullscreen]',function(){
        $('body').toggleClass('fs-view',true);
        $('[data-view-port]').each(function(){
            $(this).attr('data-view-port','');
            $(this).data('viewPort','')
        });
        $('#big_image_holder').data('viewPort','switched-on');
        $('#big_image_holder').attr('data-view-port','switched-on');
        FRONT.events.publish('switchview', FRONT.viewer.UX.currentPage);
        launchIntoFullscreen( $('#big_image_holder').get(0) );
    });
    $(document).on('click','[data-contract-view]',function(){
        $('body').toggleClass('fs-view',false);
        $('[data-view-port]').each(function(){
            $(this).attr('data-view-port','');
            $(this).data('viewPort','')
        });
        $('#image').data('viewPort','switched-on');
        $('#image').attr('data-view-port','switched-on');
        FRONT.events.publish('switchview', FRONT.viewer.UX.currentPage);
        exitFullscreen();
    });

    document.addEventListener('webkitfullscreenchange', detectFS, false);
    document.addEventListener('mozfullscreenchange', detectFS, false);
    document.addEventListener('fullscreenchange', detectFS, false);
    document.addEventListener('MSFullscreenChange', detectFS, false);

    function detectFS() {
        if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement) {
             $('body').toggleClass('fs-view',true);
             $('[data-view-port]').each(function(){
                $(this).attr('data-view-port','');
                $(this).data('viewPort','')
            });
            $('#big_image_holder').data('viewPort','switched-on');
            $('#big_image_holder').attr('data-view-port','switched-on');
            FRONT.events.publish('switchview', FRONT.viewer.UX.currentPage);
        } else {
             $('body').toggleClass('fs-view',false);
                 $('[data-view-port]').each(function(){
                $(this).attr('data-view-port','');
                $(this).data('viewPort','')
            });
            $('#image').data('viewPort','switched-on');
            $('#image').attr('data-view-port','switched-on');
            FRONT.events.publish('switchview', FRONT.viewer.UX.currentPage);
        }        
    }

    $(document).on('click','[data-fs-scale-select]',function(e){
        $('#big_image')
            .toggleClass('original_image', e.target.hasAttribute('data-fs-normal') )
            .toggleClass('horizontal-scale', e.target.hasAttribute('data-fs-horizontal') )
            .toggleClass('vertical-scale', e.target.hasAttribute('data-fs-vertical') );
        $(e.target).toggleClass('active',true);
        $(e.target).siblings().toggleClass('active',false);

    });

    $('.scale-page').click(function() {
        $('#big_image, #big_image_holder').show();
        $('.turn-page ').addClass('active');
        launchIntoFullscreen( $('#big_image_holder').get(0) );

        

        /*
        $('.turn-page ').addClass('active');
        $('.left-tools').hide();
        $('.footer').addClass('close');
        $('.footer-small').addClass('open');
        $('.main').addClass('padding-bottom');
        $('.page-book .right').hide();
        $('.navigation').addClass('open');
        $('#selectedrect').css({'display':'none'});
        $('.layer_notes').css({'display':'none'});
        $('#text_note').val('');
        $("div.quote-book, div.note-book").css('display', 'none');
        $("#image").animate({"max-width": max_width_img+"px","max-height": max_height_img+"px" }, "normal");
        */
    });

    $('.turn-page').click(function() {
        $(this).removeClass('active');
        $('#big_image').hide();
        $('.fullscreen-zoom').hide();
        $('#big_image_holder').hide();
        exitFullscreen();

        $('body').toggleClass('fs-view',false);

        /*
        $('.left-tools').show();
        $('.footer').removeClass('close');
        $('.footer-small').removeClass('open');
        $('.main').removeClass('padding-bottom');
        $('.page-book .right').show();
        $('.navigation').removeClass('open');
        $("div.quote-book, div.note-book").css('display', 'block');
        $("#image").animate({"max-width": min_width_img+"px","max-height": min_height_img+"px" }, "normal");
        */
    });

    //масштабирование
    /*
        $('.increase').click(function(){

            var $image    = $("#image");
            var interval  = 20;
            if ($image.width() < 720 && $image.height() < 1000) {

                $image.animate({
                    width: $image.width() + interval + 'px'
                }, 'fast');

            }

        });

        // масштабирование колёсиком

        /*$(".view-page").mouseover(function() {

            var $image          = $("#image"),
                interval        = 10,
                initWidth       = $image.width(),
                getCurrentWidth = function() {
                    return $image.width();
                };

            $(this).on("mousewheel", function(event) {

                if (event.deltaY > 0) {
                    if ($image.width() < 720 && $image.height() < 1000) {
                        $image.width(getCurrentWidth() + interval);
                    }
                } else {
                    if ($image.width() > 400) {
                        $image.width(getCurrentWidth() - interval);
                    }
                }
                return false;

            });

        }).mouseout(function () {
            $(this).off("mousewheel");
        });
        
        $('.reduce').click(function(){

            var $image    = $("#image");
            var interval  = 20;
            if ($image.width() > 400) {

                $image.animate({
                    width: $image.width() - interval + 'px'
                }, 'fast');

            }
        });

        $('.scale-status').click(function(){

            $("div.quote-book, div.note-book").css('display', 'block');
            //$("#image").animate({"max-width": min_width_img+"px","max-height": min_height_img+"px" }, "normal");
            $("#image").animate({"width": image_start_width+"px","image_start_height": image_start_height+"px" }, "normal");

        });
    */
    function arrowsPosition(scroll) {
        $("#big_image_holder .prev-page, #big_image_holder .next-page").css({
            top: scroll + $(window).height() / 2 + 120 + "px"
        });
    }
    arrowsPosition(60);

    $(window).scroll(function() {

        $(".turn-page:not(fullscreen-zoom)").css({
            top: $(window).scrollTop() + "px"
        });
        $(".turn-page.fullscreen-zoom").css({
            top: $(window).scrollTop() + 61 + "px"
        });
        arrowsPosition($(window).scrollTop());

    });

    // полный экран масштабирование

    $('.scale-page').click(function() {
        $('.turn-page.active.fullscreen-zoom').remove();
        // $(document.body).css({
        //     overflow: 'scroll'
        // });
        $('.turn-page').clone().empty().prependTo('.page-book').addClass('fullscreen-zoom').css({
            position: 'absolute',
            right: 0,
            top: 60,
            'z-index': 1300,
            background: 'url("images/scale.png") top'
        }).html(
            'Масштаб<br /><div class="scale-button normal">1:1</div><div class="scale-button horizontal"></div><div class="scale-button vertical"></div>'
        ).hover(function() {
            $('.scale-button').show();
        }, function() {
            $('.scale-button').hide();
        }).click(function() {
            $('.scale-button').show();
        });
        $('#big_image').click(function() {
            $('.scale-button').hide();
        });

        function setInitImage() {
            $('#big_image').css({
                'width': '90%',
                'margin-left': '5%',
                'margin-top': 0,
                height: ''
            });
        }
        setInitImage();
        $(window).scrollTop(0);

        var maxWidth = $("#big_image").width();

        $('#big_image_holder').height($("#big_image").height());

        var $image = $("#big_image");
        var interval = 80;

        $('.scale-button.normal').click(function() {
            // $(document.body).css({
            //     overflow: 'scroll'
            // });
            setInitImage();
        });
        $('.scale-button.horizontal').click(function() {

            // $(document.body).css({
            //     overflow: 'scroll'
            // });
            $("#big_image").css('height', '');
            $("#big_image").width($(window).width()).css({
                'margin-left': 0,
                'margin-top': 0
            });

        });
        $('.scale-button.vertical').click(function() {

            $(window).scrollTop(0);
            var oldHeight = $('#big_image').height();
            var oldWidth = $('#big_image').width();

            $("#big_image").height($(window).height()).css({
                'margin-top': 0,
                'margin-left': ($(window).width() - $("#big_image").width()) / 2 + 'px'
            });

            var coef = (oldHeight / $('#big_image').height());
            $("#big_image").width(oldWidth / coef).css({
                'margin-left': ($(window).width() - $("#big_image").width()) / 2 + 'px'
            });
            // $(document.body).css({
            //     height: '100%',
            //     overflow: 'hidden'
            // });

        });

    });

    var jcrop_api;
    if (jQuery().Jcrop) {
        $('.viewer').Jcrop({
            onChange: showCoords_,
            onSelect: showCoords,
            onRelease: clearCoords
        }, function() {
            jcrop_api = this;
        });
        $('#coords').on('change', 'input', function(e) {
            jcrop_api.setSelect([x1, y1, x2, y2]);
        });
    }

    // var selectResourceTypeDialog = function (resources) {
    //  var modal = $('#exampleModal');
    //  modal.find('p').html('Выберите тип документа:');
    //  for (var key in resources) {
    //      if (resources.hasOwnProperty(key)) {
    //          var label = resources[key].ext;
    //          if (resources[key].hasOwnProperty('label')) {
    //              label = resources[key].label;
    //          }
    //          modal.find('p').append(
    //              '<li class="cursor-pointer document-type" data-doc-type="' + resources[key].ext + '">'
    //              + '<a>' + label + '</a>'
    //              + '</li>'
    //          );
    //      }
    //  }

    //  this.show = function (selectCallback) {
    //      modal.find('li.document-type').click(function () {
    //          var type = $(this).data('doc-type');
    //          selectCallback(type);
    //          modal.arcticmodal('close');
    //      });
    //      modal.arcticmodal();
    //  }
    // };

    // $('.save-book-link.allowed').click(function (e) {
    //  e.preventDefault();
    //  var downLoadUrl = $(this).attr('href');
    //  $.getJSON('/local/tools/exalead/resources.php?book_id=' + book_id,
    //      {},
    //      function (resources) {
    //          if (resources.length <= 1) {
    //              location.href = downLoadUrl + '&doc_type=' + resources[0].ext;
    //          } else {
    //              (new selectResourceTypeDialog(resources)).show(function (type) {
    //                  location.href = downLoadUrl + '&doc_type=' + type;
    //              });
    //          }
    //      }
    //  );
    // });
    $(document).on('click','[data-save-edition]',function(e){
        e.preventDefault();
        var that = this;
        this.attrHref = $(this).attr('href');

        function showPreview(modalContent,modal,context){
            var opts = {};
            for (var key in FRONT.spinner.btnOpts) {
                opts[key] = FRONT.spinner.btnOpts[key]
            }
            opts.color = '#00708c';
            opts.position = 'relative';
            FRONT.spinner.execute( $(modal).find('.modal-body').find('center')[0], opts );
            $.ajax({
                url: '/local/tools/exalead/resources.php',
                data: {
                    book_id: book_id
                },
                dataType: 'json',
                method: 'GET',
                success: function(data){
                    $(modal).find('.modal-body').find('.spinner').detach();
                    var result = data,
                    listObj = $('<ul>'),
                    preAnswerList = listObj.filter('ul');
                    if ( result.length == 1 ) {
                        $(modal).find('.modal-title').text( 'Начинается скачивание документа.' );
                        $(modal).find('.modal-body').html('<p>Формат документа: '+ data[0]['label'] ? data[0]['label'] : data[0]['ext'] +'</p>');
                        document.location.href = context.attrHref;
                    } else if ( result.length > 1 ) {
                        $.each(data, function(i,val){
                            var anchor = val['label'],
                                doc_type = val['ext'],
                                listItem = $('<li>'),
                                downloadLink = $('<a>').attr('href',context.attrHref + '&doc_type=' + doc_type).text( anchor ? anchor : doc_type ).appendTo(listItem);
                            preAnswerList.append(listItem);
                            preAnswerList.appendTo(listObj);
                        });
                        $(modal).find('.modal-body').html(listObj);
                    } else {
                        $(modal).find('.modal-title').text( 'Произошла ошибка.' );
                        $(modal).find('.modal-body').html('Обратитесь к оператору НЭБ или попробуйте позже.');
                    }
                }
            }).fail(function(){
                $(modal).find('.modal-body').find('.spinner').detach();
                FRONT.infomodal({title:"Произошла ошибка"});
            });
        }
        FRONT.infomodal({
            title:"Выберите формат документа",
            callback: showPreview,
            context: that,
            options: {buttonBar: false}
        });
    });
});

// Simple event handler, called from onChange and onSelect
// event handlers, as per the Jcrop invocation above
function showCoords_(c) {
    $('#selectedrect').css({
        'display': 'none'
    });
    $('.layer_notes').css({
        'display': 'none'
    });
    $('#text_note').val('');
    if (angle == 0) {
        $('.jcrop-keymgr').next().css('display', 'block');
    }

    $('.zoom-image').unbind('mouseover').unbind('mousewheel');
    $('.selected-increase').unbind('click').unbind('mousedown').unbind('mouseup');
    $('.selected-reduce').unbind('click').unbind('mousedown').unbind('mouseup');
}

function showCoords(c) {
    var page = FRONT.viewer.UX.currentPage.num;
    var height_img = parseInt($('#image').height());
    var width_img = parseInt($('#image').width());

    $('.jcrop-holder').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
    $('.jcrop-holder').find('.milk-shadow').fadeOut(function(){ $(this).detach() });
    $('.jcrop-holder').find('div').first().append('<div class="milk-shadow draw"></div>');
    FRONT.spinner.execute( $('.jcrop-holder').find('div').first()[0], FRONT.spinner.opts);

    function getZoomImage(){
        var dfd = new jQuery.Deferred();
        var p = FRONT.viewer.UX.currentPage.num,
            search = $('.search_ div').length > 0 ? $('#search_text').val() : '',
            libraryId = $('meta[name=book-library-id]').attr('content'),
            pageSrc =  "/local/tools/exalead/getImages.php?book_id=" + book_id + "&p=" + p + "&search=" + search + "",
            zoomImage = $('<img/>');
        if(libraryId) {
            pageSrc += ('&library_id='+libraryId);
        }
        zoomImage.attr("src", pageSrc + "&width=2000");
        zoomImage.addClass('zoom-image');
        zoomImage.on('load',function(){
            dfd.resolve(zoomImage);
        }).on('error',function(){
            dfd.reject('error pass');
        });
        $('.selectedrect-wrapper').find('.zoom-image').detach();
        $('.selectedrect-wrapper').prepend(zoomImage);
        return dfd.promise();
    }

    $.when( getZoomImage() ).then(function(data){
        var zoomImage = $(data);
        $('.jcrop-holder').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
        $('.jcrop-holder').find('.milk-shadow').fadeOut(function(){ $(this).detach() });

        $('.crop-text').val('');
        $('.crop-text-block').hide();


        if (angle == 0 && height_img > 0 && height_img == image_start_height) {
            $('.selected-increase').removeClass('selected-increase--disabled');
            $('.selected-reduce').addClass('selected-reduce--disabled');

            var max_width_img = 800;
            var left = (max_width_img - width_img) / 2;
            left = c.x - left;

            $('#selectedrect').css({
                'display': 'block',
                'width': c.w + "px",
                'height': c.h + "px",
                'top': c.y + "px",
                'left': c.x + "px"
            });

            var targetImage = $('.zoom-image');
            var topDefault = c.y+2;
            var leftDefault = left+2;
            var intervalSum = 0;


            targetImage.css({
                'top': -topDefault,
                'left': -leftDefault,
                'width': width_img+'px'
            });
            var $image          = targetImage,
                interval        = 15,
                initWidth       = $image.width(),
                topCoords       = +$image.css('top').split('px')[0],
                leftCoords      = +$image.css('left').split('px')[0],
                getCurrentWidth = function() {
                    return $image.width();
                };


            $(targetImage).on('mousewheel', function(event) {
                var currentHeight = $image.height();
                if (event.deltaY > 0) {
                    if($image.width() < 1500 && $image.height() < 1800) {
                        $('.selected-reduce').removeClass('selected-reduce--disabled');
                        $image.width(getCurrentWidth() + interval);
                        topCoords =  topCoords - 8;
                        leftCoords = leftCoords - interval/2;

                        $image.css({
                            'top': topCoords + 'px',
                            'left': leftCoords +'px'
                        });
                    }
                    else {
                        $('.selected-increase').addClass('selected-increase--disabled');
                    }
                }
                else {
                    if ($image.width() > width_img) {
                        $('.selected-increase').removeClass('selected-increase--disabled');
                        $image.width(getCurrentWidth() - interval);
                        topCoords = topCoords + 8;
                        leftCoords = leftCoords + interval/2;

                        $image.css({
                            'top': topCoords + 'px',
                            'left': leftCoords+'px'
                        });
                    }
                    else {
                        $('.selected-reduce').addClass('selected-reduce--disabled');
                    }
                }
                event.stopImmediatePropagation();
                return false;
            });
            $('.selected-increase').click(function(event){
                if($image.width() < 1500 && $image.height() < 1800) {
                    $image.width(getCurrentWidth() + interval);
                    $('.selected-reduce').removeClass('selected-reduce--disabled');
                    topCoords =  topCoords - 8;
                    leftCoords = leftCoords - interval/2;

                    $image.css({
                        'top': topCoords+'px',
                        'left': leftCoords+'px'
                    });
                }
                else {
                    $(this).addClass('selected-increase--disabled');
                }
                return false;
            });

            $('.selected-increase').mousedown(function(event){
                event.preventDefault();
                increaseTimer = setInterval(function(){
                    if($image.width() < 1500 && $image.height() < 1800) {
                        $image.width(getCurrentWidth() + interval);
                        $('.selected-reduce').removeClass('selected-reduce--disabled');
                        topCoords =  topCoords - 8;
                        leftCoords = leftCoords - interval/2;

                        $image.css({
                            'top': topCoords+'px',
                            'left': leftCoords+'px'
                        });
                    }
                    else {
                        $(this).addClass('selected-increase--disabled');
                    }
                    return false;
                }, 10);
            }).mouseup(function(){
                clearInterval(increaseTimer);
            });

            $('.selected-reduce').click(function(event){
                event.preventDefault();
                if ($image.width() > width_img) {
                    $image.width(getCurrentWidth() - interval);
                    if ($image.width()-interval > width_img) {
                        $('.selected-increase').removeClass('selected-increase--disabled');
                    }
                    topCoords = topCoords + 8;
                    leftCoords = leftCoords + interval/2;

                    $image.css({
                        'top': topCoords+'px',
                        'left': leftCoords+'px'
                    });
                }
                else {
                    $(this).addClass('selected-reduce--disabled');
                }
                return false;
            });

            $('.selected-reduce').mousedown(function(){
                reduceTimer = setInterval(function(){
                    if ($image.width() > width_img) {
                        $image.width(getCurrentWidth() - interval);
                        if ($image.width()-interval > width_img) {
                            $('.selected-increase').removeClass('selected-increase--disabled');
                        }
                        topCoords = topCoords + 8;
                        leftCoords = leftCoords + interval/2;

                        $image.css({
                            'top': topCoords+'px',
                            'left': leftCoords+'px'
                        });
                    }
                    else {
                        $(this).addClass('selected-reduce--disabled');
                    }
                    return false;
                }, 10);
            }).mouseup(function(){
                clearInterval(reduceTimer);
            });

            //Расчет координат в процентах для получения текста из выделенного куска
            var y1_percent = (c.y*100/(+$('#image').height())).toFixed(2);
            var y2_percent = ((c.h+c.y)*100/(+$('#image').height())).toFixed(2);
            var x1_percentPre = (left*100/(+$('#image').width())).toFixed(2);
            var x1_percent = (x1_percentPre > 0 ? x1_percentPre : 0);
            var x2_percentPre = ((left+c.w)*100/(+$('#image').width())).toFixed(2);
            var x2_percent = x2_percentPre > 100 ? 100 : x2_percentPre;

            // getTextInCrop(x1_percent,x2_percent,y1_percent,y2_percent).then(function(data){
            $.when( getTextInCrop(x1_percent,x2_percent,y1_percent,y2_percent) ).then(function(data){
                var result = data.replace(/\n/g, ' ').replace(/\s+/g, ' ');
                if(result.length > 0){
                    $('.crop-text-block').show();
                    $('.crop-text-block .search_preloader').hide();
                    $('.crop-text').val(result).show();
                }
            }, function(error){
            });

            $('#b-textLayer_quotes').attr('href', '/local/tools/exalead/saveImg.php?book_id=' + book_id + '&p=' + page + '&top=' + c.y + '&left=' + left + '&w=' + c.w + '&h=' + c.h + '&w_img=' + width_img);
            $('#b-textLayer_notes').attr('href', '/local/tools/exalead/saveImg.php?book_id=' + book_id + '&p=' + page + '&top=' + c.y + '&left=' + left + '&w=' + c.w + '&h=' + c.h + '&type=note&w_img=' + width_img);
        
        }
        $('.jcrop-keymgr').next().css('display', 'none');
    }, function(error){
        $('.jcrop-holder').find('[role="progressbar"]').fadeOut(function(){ $(this).detach() });
        $('.jcrop-holder').find('.milk-shadow').fadeOut(function(){ $(this).detach() });
    });

    // $('.big_image').attr("src", pageSrc + "&width=2000");



    // var clonImage = $('.big_image').clone().addClass('zoom-image').attr('id','big_image_clone').show();
    // $('.selectedrect-wrapper').prepend(clonImage);
    // $('.zoom-image').removeClass('.big_image');

    // var page = parseInt($('#page-number').val());
};

function clearCoords() {
    $('#coords input').val('');
};

function getTextInCrop(x1,x2,y1,y2){
    $('.crop-text-block .search_preloader').show();
    $('.crop-text').hide();
    var data = '';
    var coordinates = 
                {               
                    x1 : x1,
                    x2 : x2,
                    y1 : y1,
                    y2 : y2
                };
    return (function(){
        var dfd = new jQuery.Deferred();

        $.ajax({
            type: "GET",
            url: "/rest_api/rslviewer/",
            dataType: 'json',
            data: {
                    action : 'getTextByCoordinates',
                    book_id : book_id,
                    page : parseInt($('#page-number').val()),
                    coordinates : JSON.stringify(coordinates)
                },
            success: function(msg){
                dfd.resolve(msg);
            },
            error: function(jqXHR, textStatus, errorThrown){
                dfd.reject(jqXHR);
            }
        });

        return dfd.promise();
    })();
    // return new Promise(function(resolve, reject){
    //  $.ajax({
    //      type: "GET",
    //      url: "/rest_api/rslviewer/",
    //      dataType: 'json',
    //      data: {
    //              action : 'getTextByCoordinates',
    //              book_id : book_id,
    //              page : parseInt($('#page-number').val()),
    //              coordinates : JSON.stringify(coordinates)
    //          },
    //      success: function(msg){
    //          resolve(msg);
    //      },
    //      error: function(jqXHR, textStatus, errorThrown){
    //          reject(jqXHR);
    //      }
    //  });     
    // });
}

function getText(p){
    var res = false;
    if (p > 0 && p <= count_page) {
        $.ajax({
            type: "GET",
            url: "/rest_api/exalead/book/page/",
            dataType: 'json',
            data: {
                'book_id' : book_id,
                'dataType' : 'txt',
                'p' : p
                },
            success: function(msg){
                var text = msg['content'];
                $('.text-view').html('').html(text);
                // $('#pagetext').html(text);
                if (msg['content']) {
                    res = msg['content'];
                }
            }
        });
    }
    //$('.zoomer-image').attr("src", "/local/tools/exalead/getImages.php?book_id=" + book_id + "&p=" + p + "&search=" + search + "");
    //$('.big_image').attr("src", "/local/tools/exalead/getImages.php?book_id=" + book_id + "&p=" + p + "&search=" + search + "&width=2000");
    return res
}

function getImage(p,caller) {
    var p = p, 
        o = {};
    if (typeof p === 'number') {
        o.num = p
        if (o.num > 0 && o.num <= count_page) {
            FRONT.events.publish('pagechange', o);
        }
    } else if (typeof p === 'Number') {
        o.num = parseInt(p);
        if (o.num > 0 && o.num <= count_page) {
            FRONT.events.publish('pagechange', o);
        }
    } else if (typeof p === 'object') {
        o = p;
        p = parseInt(p.num);
        if (o.translate === false) {
            // console.log('подтянуть картинку но не переводить страницу');
        } else {
            if (o.num > 0 && o.num <= count_page) {
                FRONT.events.publish('pagechange', o);
            }
        }
    }
    
    // 
    if (p > 0 && p <= count_page) {
        // $('.flaviusmatis').pagination({
        //     pages: count_page,
        //     currentPage: p,
        //     listStyle: 'pagination',
        //     onPageClick: function(pageNumber,event) {
        //         if (event) {
        //             event.preventDefault();
        //         }
        //         // getText(pageNumber);
        //         getImage(pageNumber);
        //     },
        //     prevText: "Назад",
        //     nextText: "Вперед"
        // });

        image_start_height = 0;
        // $('#selectedrect').css({'display': 'none'});
        // $('.layer_notes').css({'display': 'none'});
        // $('#text_note').val('');
        // $("div.quote-book, div.note-book").remove();/* wtf удаление с вьюпорта цитат и заметок при перелистывании!*/
        $('.navigation span').removeClass('end');
        // var $preloader = $('.preloader');
        // $preloader.fadeIn();
        var search = '';
        // если был осуществлен поиск, то добавить
        if ($('.search_ div').length > 0) {
            search = $('#search_text').val();
        }

        //проверка в закладках ли страница
        // isMark(p);
        // var libraryId = $('meta[name=book-library-id]').attr('content');
        // var pageSrc =  "/local/tools/exalead/getImages.php?book_id=" + book_id + "&p=" + p + "&search=" + search + "";
        // if(libraryId) {
        //     pageSrc += ('&library_id='+libraryId);
        // }
        // // $('.zoomer-image').attr("src", pageSrc + "");
        // $('.big_image').attr("src", pageSrc + "&width=2000");
        // $('.big_image').load(function() {
        //     if(0 === positionpart) {
        //         $('html, body').animate({scrollTop: $(document).height()});
        //         positionpart = 1;
        //     }
        // });

        $('#page-number').val(p);

        if (p == count_page) {
            $('.navigation span.next').attr('p', p);
            $('.next-page').attr('p', p);
        } else {
            $('.navigation span.next').attr('p', p + 1);
            $('.next-page').attr('p', p + 1);
        }

        $('.navigation span.prev').attr('p', p - 1);
        $('.prev-page').attr('p', p - 1);
        $(".miniatures-content div.miniatures-images span").removeClass('active');
        $(".miniatures-content div.miniatures-images span[p = " + p + "]").addClass('active');

        // $('.zoomer-image').load(function() {
        //     $preloader.fadeOut('slow');
        //     image_start_width = $('#image').width();
        //     image_start_height = $('#image').height();
        // });

        // $('.jcrop-holder>div:first-child').hide();

        // isQuo(p);
    }

}
// проверка на наличие цитат, заметок
function isQuo(page) {
    if ( $('[data-right-cites]').hasClass('active') ) {
        FRONT.viewer.drawSheetQuoteItems();
    } else {
        $("div.quote-book").remove();
    }

    // заметки
    if ( $('[data-edition-notes]').hasClass('active') ) {
        FRONT.viewer.drawSheetNoteItems();
    } else {
        $("div.note-book").remove();
    }

    // $.ajax({
    //     type: "GET",
    //     url: "/local/tools/viewer/quo_list.php",
    //     data: "book_id=" + book_id,
    //     success: function(data) {
    //         var page = parseInt($('#page-number').val());
    //         //цитаты

            

    //     }
    // })
}

$(document).on('click','[data-right-bookmarks]',function(){
    var toggler = $(this),
        isActive = toggler.hasClass('active'),
        container = $(toggler).next('.bookmark-content'),
        containerIsEmpty = Boolean( $(toggler).next('.bookmark-content:empty').length ),
        template = $('<div>'+
                            '<a></a>'+
                            '<span class="delete"></span>'+
                        '</div>');
        if (!isActive && containerIsEmpty) {
            container.append('<div class="milk-shadow draw" style="height: 100px;"></div>'); 
            FRONT.spinner.execute( $('.milk-shadow', container)[0] );
            $.when( pagesContent.getEditionData() ).then(function(data){
                FRONT.viewer.buildRightBookmarks(data);
                // container.empty();
                // $(data.bookmarks).each(function(){
                //     var item = $(template).clone(),
                //         itemData = this;
                //     item.appendTo(container);
                //     item.find('a').attr('p',itemData.page).text(itemData.page + ' страница').end()
                //         .find('.delete').attr('id',itemData.id);
                // });
            });
        }
});
FRONT.viewer.toggleBookmark = function(markId){    

    if ( typeof markId == 'undefined' ) {
        //
    }
    //добавить
    // $.ajax({
    //     type: "GET",
    //     url: "/local/tools/viewer/bookmark_list.php",
    //     data: "book_id=" + book_id + "&page=" + page,
    //     success: function(msg) {

    //         if (parseInt(msg) > 0) {                            
    //             FRONT.infomodal({title:"Закладка добавлена"}); 

    //             FRONT.viewer.buildRightBookmarks();

    //             // $('.bookmark-content').prepend('<div><a p="' + page + '"><span>' + page + '</span> страница</a><span id=' + msg + ' class="delete"></span></div>');
    //             isMark(page);
    //         }
    //     }
    // });

    //удалить
    // var toggler = $(this),
    //         mark_id = this.id,
    //         url = '/local/tools/viewer/bookmark_list.php',
    //         page = parseInt($('#page-number').val());

    //     $.when( FRONT.confirm.handle({title:"Удалить закладку?", confirmTitle: "Удалить"},toggler) ).then(function(confirmed){
    //         if(confirmed){
    //             $.ajax({
    //                 type: "GET",
    //                 url: url + '?mark_id=' + mark_id,
    //                 success: function(data) {
    //                     // $('#' + mark_id).parent('div').remove();
    //                     // pagesContent.getEditionData(true);
    //                     // isMark(page);
    //                     $.when ( pagesContent.rewriteEditionDataStore() ).then(function(data){
    //                         FRONT.viewer.buildRightBookmarks();
    //                         isMark(page);
    //                     });
    //                 }
    //             });
    //         }
    //     });

}
FRONT.viewer.buildRightBookmarks = function(gettedData) {
    var obj = gettedData,
        container = $('.bookmark-content'),
        template = $('<div>'+
                        '<a></a>'+
                        '<span class="delete"></span>'+
                    '</div>');
    if (!obj || $.isEmptyObject(obj)) {
        $.when( pagesContent.getEditionData() ).then(function(json){
            build(json);
        });
    } else {
        build(obj);
    }
    function build(data) {
        container.empty();
        $(data.bookmarks).each(function(){
            var item = $(template).clone(),
                itemData = this;
            item.appendTo(container);
            item.find('a').attr('p',itemData.page).text(itemData.page + ' страница').end()
                .find('.delete').attr('id',itemData.id);
        });
    }
}

$(document).on('click','[data-right-cites]',function(){
    var toggler = $(this),
        isActive = toggler.hasClass('active'),
        container = $(toggler).next('.quote-content'),
        containerIsEmpty = Boolean( $(toggler).next('.quote-content:empty').length ),
        template = $('<div>'+
                            '<a><span></span>'+
                            '<img/>'+
                            '</a>'+
                            '<span class="delete"></span>'+
                        '</div>');

    
    if (!isActive && containerIsEmpty) {
        container.append('<div class="milk-shadow draw" style="height: 100px;"></div>'); 
        FRONT.spinner.execute( $('.milk-shadow', container)[0] );
        $.when( pagesContent.getEditionData() ).then(function(data){
            drawRightQuoteItems(data);
            FRONT.viewer.drawSheetQuoteItems(data);
        });
    } else if (!isActive) {
        FRONT.viewer.drawSheetQuoteItems();
    } else {
        $(".quote-book").detach();
    }

// {
//     citearea:null,
//     citeheight:"35",
//     citeimg:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAsMAAAAjCAAAAABUPFg1AAAAAmJLR0QA/4ePzL8AAAAJb0ZGcwAAAAgAAADgAGgHx08AAAAJcEhZcwAAAEgAAABIAEbJaz4AAAAJdnBBZwAAAtAAAAOlAEFFRvoAABeMSURBVHja7VwHeFXF1p2EFEISpKgkKCCiKE1QREGUoj6aEWlRVIooID0QpHcRKdKliYCPonSI8BSIKFIEVEoSeggllJB6++ll/XvOBQThJnny0J/vu5uPezPnnCnnnjVr1uzZZxh8mGma3m+VPs5tfL35oO3I2tquee8EGKoGqJABD6BpBv1tAA5+tWhCNWEHdB26CdmArAMSFLhBGegDc0f2b7BKoSsorwkTAv2XrZrgMek4FUX5NV6gQAcNq2AJpsZro7Nu6AK1yfBA0qlcuKh6B2W2gRevUrF0aNVbLzzxxFIFgmGdBV2o8ipNw5B5Albbqb3w2z1vzNeJ6xjmkICnX4kd9Oyl/hE/ygQi5IkcdwQ3kTBzRTX4hZoHbh0SB4hJ6CPceU3gqCT4UJEChEXjgc9D9mnQFcIphxVcdFynvkJVmXYovDACGC8Coow8Dk3qG4Q8yQKkyFvkJLgasLoArHI4JDWrOvqaFtaxa5/veTfQeTtlD3UF6G5IHLNnebmGyhOSF8l+u6fNJ4YNw7gKY9VNJDaIZXEsjQvZCY4fiyChyESiEr9IER0c0io/JxH7wp3Hc2rE1hajwvS46DwBvu2zBkzWjyPd4DjUNC8G+QfxMiSDaFjPIyi6CN3QOUW7ROti3ilcAhSHyLOYboMuId4n1Bs5/JQThqDzHnchhvcH0EGDvnNlL1BVPgrkUkECP0MlaKrshb3f7mljBV7Bn7IiD2YXCY3mxxEpnMrOpNFxt8W+/AIClylBljl1GiQhLP0h4bzGwWKRqsdh0ShyjTXtROSxj00n52CdyJejzdAki+5Nj8Elh6rnUAmmrFAm4lBeP8cwFeP2WPysKNoZm1WH2+JdcGaFJSaoi2nTj3jO51JexSnnZlptzFWlDA9svDlZXAfRZx5vgO2f/v39dueWL4aJiE3OYfTMBwWdIeoUprI9UE51++S5F9O4qpTOdL2/fc9G02zH2lUcZU+qUWscsKlB2/HvlO132tZiTN9Sa1Cv13ulB7SqLk59oNnXHEIEQc+a+36/WgMpa1PnWhfanrdLdRsytPFDY0icdJrwdtWlOhIefap7m077a73f74GY92tmK/a3JnQo9wMufhS1pW1I2HSkxpcfcNT+YuWhqVpi4/cHdS/Z57TVM7r91HNKg5hz1JaG8xs2ys3a2rR5Qtng+lnYXO/BNebSqJqJmF76td8PjI9+M+WffgB+u2MrgIc5S4qKoeOjoHXbdm7e9hE7kiO9lIrsik2AbGK4M8W2IiFgrWs6011oHQVdwtc/6P+OOKXMrZwndnsme5ieydbim+0o/RFnRCfXHfXmE0E7nVwQSwpxsap6NGTNL01MHFebxHPdJQKmR/+sCV3qA89Xn6hcLJ6Ib5L12mtEzAv53bOwTKeEw1XZMfwasApXNrPz1NJPT9lSQhINDUS8Lz+Vk6vWfgwoOgf20JXaxToPj0oaw1oBTevR/DPyIxoaonubyA1a6PqnH4Df7tgK1BI2zsKSMorNXLh80fJ32WFz5TOLFk6pGp5LkytH5oXANUhlG+XFYSr+XbWeRMP7pt3YwdKwaiJnbxreL7ElvKDiw+HyWKJ2V3+ukInGCbncYaBbsz9jMSN10iUa0hfhJjxSmWZAzDNAbPkrSGMbkHl+PRNheEo2wWGWAGNn5JDLFyJ24+SzzAbdOTVN2EeoJsXtsIc3J2HxXeTi3IFZaib7DFojgi/aFTmLVq9o5iclepLwqTBSQSZL/Kd/f7/dufnE8LBhw4YOGTJk8JjLGnedDS52hAvOccG/YnBMVrp2wS5bPoGDwYNn1OqRg/lBzgOffVCNrrmy5LTAMQxx4vAmpagHXAj5j4E8rULLBVsyLNk72qCpFVFh+icfjhw44OP+8dMvkyLGlGAF4pAaQL8wLm5fuh+u9yst7fzsZhipJVYR/GMf4T6L+mWUIyyJREj5JjjBEtO7LwvJlnRMOYQ9xXZr3A3hZl00kei/v4FdIycVnaCixUvUV6YRxN+si9kJNQeTtC/desU384p845/T3fvmE8NXrprTxidoGMwyuE93apEkJb6Gh0+8RH7cOMrm/vL+4yvweciRvuhWk7uN42GsKnYCh2ttkSZR8bYMttWkCV21TocnhY5V3dCXERlz77DozM3MyMzMzLIJOmX8rIgAV/yTCloHynYYrSvB077qr4trNM1DakAC7HpXq7HvPiAcYdtJllTqrqWxHR3VVYzkgzE3GXsC98NJU0qjVAvuAmHD5dHtbQieAHe7+lTf16EHEfvC+vmo3o0ER9neu8/tYKsNmhGqfjfxPW0F+yXAAYvRwWf4wP9J6AFMi/iejn1JfCcrOBWwVxZqRuFz9iayBlThrrQe8HzHzuud36BcVWwuONgyq6YxUGOKUT8Qs02OG1LHuqrIhq5Z6xJwzWYEvJGPIXsh20m83Ow1CW+0deIn9jky2Ho7MCN4NcnXF1siiR10a1KlsUZydI0M5y4m58gYddaTwnZwT5ygv/cwFZ4TsOAUOw2TzbqkdW1MxxdGwhEbNVtAzY4OaGUGQkwP/lbzcEcF/H7ie9jyW+OwTDNV0YSrR2AyPzqO/YiLZUov+GFeL8vZhd8CNwAvVBVnk1bFpAfyIG/+3o1t7KBR7Qmn+0WmHcPhEmuBPFT5RBOaRBEF/xzPXWDmnyt0fRFMaGr7mAQhsjWc7kq/QiN1rM4J32icC12GTEF4tA1Md8WT5hE2D9hfHNgbRlJ7ETuJLSd6mNq+qP2a5bhOCFohYutz5iK2EauLfpyGxmWhmrHjFcQ+kauhTh9cwsMfAFlhC4BT/TJ09z/9GPx2B5bfGodl1nwr5+tHWad1Ntuqx1m7ZVhfLji6IV/iMqWT3VmLbrWbpaQ+GzHEvis89Ouzs0vHxY2txwZdWBoe3WPyfR2RPpC13uDEZFYhrkF97srqw1KgaaZ5rQarqxjS3iZs8sXdtUpNh3yyRJt/t17oxMpSj/Xo8NwEOalv8DurKOd/noqZ2mSN23Uo/JW565/+VUgbxt49dTKGDT1TJSI+L6Uj63SCr2KI+CJi/IRXT0CpGVr58H2ltud1qNx+3dB2GpZHh27CTFZ5i2cGa7Tt8AjW4og2JWAd4HU/++2etEJoCV3jK7/W0i+c1hzIk7zLcjDI1sobnRU1KBIcJqUiN9GMzUxuuFjLcGlId2l8mqVaIQp8QUHPgTv96lKfRfa64WVkvmRmQHB6DC5PD+2yk3LJs2qx8RWJbJP0M8S0FL44co79fvwnvrhiuC0+d0HMJmGb513ezqYegl1ZbhgKjiHDzIT+WmPb0ZO8PqiaIXOntEfQOWxVBfpRKHbln34Ofvvr5hPD+lUTNCsAgtQvDbiCHVcgOSzUKUoWj87hZ0VLTfIAHqc0gwBHqvRcAo+/cRHGOeplKDSt8/oA8gwOe06/N9RGCYMOWsKYrnPmeBWq6Ya1Zs2POuE2rYU4Xfil6M86AV138krtVmyF5OETM9GjWytvulv1QDAVQ7OAipYvW1M3O+Hb22Ar5EjisRaSt3gJfrtXrRA8zB+vk3OxoXG0eLj3CnbvVN7OlS5BWHdKmtsKjLgCTQWRZbauW5FsuiTyRWlN5p64LIVfk8OXlal7WDC2hASZSkTL0a2LdqtUIkiBKFpDBq8lh0sXzeJk+jzDiIX5Gh8EibS6plrdwa1ZbTJtgheV1E6Rr5/QDbR8jh/ii9kuwq5Mx1xWrNzVFQ7dLyXuZStwTueGpitWkILKg8N0kzMkDyOT3F58cKNR27QcVIrJQ2p0jT4UTp2KYcVcGhwsuuYNt6S/TF2/Vo+liPm3bliSQyI8yXzBmABm8BhLHgVk1U5V8FwKdsc36pFkrZHwyDUvr3IfmVWGN/YSLi51eFCnQdmW1ntrgUmYdfFmyJzhOVtrfFwQvGGZOvx2z1qBPMzpygO+LKxoPPYWssIxV+DYayGTGPavN8009JsFx+2v8nYA/aY0bpOPByNZkRk+0j7y+7wP08i3bYZRiMb77X9hhdAS3shc0xsc6Q3R/Ru8qYUFwC3XcWly28zG1Tg7X+mC8t9ymXnb/H/pLvx2B1YghiXubNAcuDbwKhL/yyjEGq1pGHfwBPWbmC9fLPzpJJHybdnfIt580r7y3/4++DHjju7Qb/8jKwQPi5rFWAYMm9e1oBQmyMA7U/vrPHTNcVyImgzuP7mpah/t4dNHX2lf+X3dh1fC+G6eput+gP89ViCGrTn7ldVN2/XeCGxo1abHNj6/0wqMMPA+XvNOJktGIVFwE9TNm75uaY/hK+0r/1+9j8L1P7/9D6wwaxw0k8vpG/gbhFxMYVsk/vbF/x/7s27wNaczC0gXlP/WCwvTwUy/0+7uW4EY5jG+RELD2Dme+pSlaNaroIV4fr50aeFM0bxg0rT82dhbh/6HvDF1ynLbHNb72PmkfeS//X2YqsS7sk9+9iohPxX/DVYwD9OT8pgYwHIkGdqUkMMeorCcSwWXrEtut/TXpYTNxR2/uiLLipYfiA2rKucfL8YZsiDIt3OFKYJL0H2mfeb3cR+66LIc4r7a5XILsuaH8N9hPjGsaV4G5GSjifq4oJN8VXYa+w040X3ks/UuWCsLyYOj3+zbYCJ2tigzQVjf4NEv7Pip0Zv9PwwakpXVcuSw4uvMRp0/LP/OBzXSp1asu8mKpjcVk9PXtfL5aoZoLoxq06V5twsNeneLatuvSqZ28PUJrRsm49KQRza/w8K+wKVeFWPOZDQuP/rCd3XKLj87omjXlA11y250zS331Da4DsVMbtbwssTXDzXz7Nuje5X9Tcms2L/dA4PeaSpOL9Oq1zPx1OlGjxhQ9EtleaP4Zo/FtxoMdd4HUyuM0a+l97YtOahz443Q7KN7j6kyHtjSInpg56ZrZAx77cvHu9GvMLv+wI8al52bZnVrNX5oXNkf4Pq9eZde3Sv0TO1YpkUKMofWmVi9Rnz/tvHGuPYzq/SC6OD+dRX+N57uphXMw6abc01cQMLO9ev39gz4BfaaNiOvckNAJtQcDNir/8q+wiymOtD6cZ5h5c/GxjJHMKKOhrhy7o9xhv2ADb8hbABNBjMtHSoYN4z9Si4PgSgxwn65Us+huMh2u7f9lB3kVNGyZo66mrXdvr16QCJS2A4d81gmjLYNFeXkg+vgjn2a0F+tl6A6S54GWla0qxKPh6i6Ge4lAQcSd2AJc7jH6tKTsUDocPf86U5lIjs8wyN2qS1heObaZrCNjky6lsac4oY+IfAs+vaWlLSwyQa+CHIqk4pk7oxM0ZaGHxU9eFvVFoZqhirSTcRMdeHlCBqZBp5Sj7HNgvJII7qXiWv192tC1HseYIeFVSF7rMV43rP8C4F30fJZa+YuK4OH4RiChqHs05XLlq1rxA7jq2cXzlsQHpVn8dHxsA0OV5EF7jksG1sqV+Ibj6zcjeXsiuf7MXYjLgwOPT1opUHgffxjaz3YwrA1bb9ePvRclB4J3NfqAgF+LcScPtXoYAr7PPcc2wLC7xj9MtslHGvOHA68VRf2K2yjjveelx144UPgvQaQtR8D5/GRQV1c3OaCXKrheQ3fMAfS4SrxKbSHmuHpuCUzP2HjzsP4sJqJ8yibIEEdknst7Z7LZMwqkpbGfiaB0PgBj312sAsLA4+mvwOsLnacGt3HhXEh/KeB41yxi3AfnmHCPf0EdkQcAr4ocTYHrdLxxgvQBDX9I2AbOyJKfMjhmsM/t7t75hPDo0aNGM5t3B6LMoeFp3B8Toncawx89/h5eM5JVpjBETbsq1L9oK4Iwe+D+1bhG0/NyFITA48RwlfEvfagS9cuhfJ3Qu0hHeatyOABDKpyY/nDD/DHay/XYP6/XsmGfiXgayriiSb82bMP1d/Ck+BU7n8Fhx5c7+z6NTsFMbaSji3sR1VvU3nD8pX399bxUCx3XIc359tiqd1K2QiDLz5kwDU/KkeBLJfuPOlfDZyeIt/nXbyQpcChxtcycuBku2DnQulqGmMjFves9pW8uOQe6gkj6AbmBS4fHb1A07GmRzzbTo3uBGVmUS8aFwV49YGE2ccch9l+qFpUV23XQFN/o75KgsYuLxv4QYl9IhRTvBbo4be7ZD4xnJV5JeMyGd8TQtKNeIIPTdk/Y8no9wiQYzmJ6d/RsAV7x7C18mx2KE7rX4MH4XSFa2fgceyv8Q0mF6XLHGw1XxaJbn98csgIXnQ2Dzy+Vn6uFYrjqTAscU54f2gX2fcKlEdr01O3l+hs/s7+Q/U81wbH2KYutk3FTgK7/vXKe+NCtwLtaicf2vtYZ4iV6vJSi8XywDejc1FSoBkdKxJqFgfl8SDLR2L3TXo47nLkdr4vAN9SpVtV0kHJpLINiK5raXEW++Vgo6dSviqZQKP/InYUM9nBhCZPnvG8/LGRzO8eHYBJxb2uhi/ZGT5lcJOqTtVOsz0wlNkl0juchtb+Vb4hltJiOjaHH7w+5fPHJ99Fy08PW74hweu3GhB4nkf6zKQ53dzg7+jAaK4IdBwP/NalNq2kLSn2LsxeT/LwoDho37IrqN8X6icRGmyZbKtGuCo1RdSalYTHTfrQ2mzKW77Ez5kIpcF3VFiSnh2YQEhsVyqDHnvEVOdZdoBOB80VjhZ9xp63nel2GKR5z7MNEmLrEAhrE7Bee0ByKCi+hI/Y8iz2gwLjtRgSMZtIS8AlRg8FBoUcfKQWP7+QOLFDTY6p8LoeiOe3XEtL64soLjmkzRE2HjmYHmUYJIg0sJYTns4zdxFKBbybiSER4HftSmUD4MTqS9BHJeEXlgqXLpRpEyvDfPNpGlVyxlfSSQsl8m00+AsC8Hso7qLli2Hvl6GqpjGInYQhYTLba14uUeqz/Qsm0HMXDewj+ag9WU2ZwUjyxt8P5P38LbC++AHUrH3hYkyoPRXH2Y+KCneFPkClKPwydCkX0tof5dOALsjlhkhqHNOdp4PWEKcdYovhOFQxD3uLrRCFg6UcOBn0FbAiPNOKo5MPRq8jvqdR21m9Vw72sxkuJFbJstmI4qWoJtDEyOMmzKVF8vgLI/cNgdIxUIgv3m7D1nYXKfvbz/P+1zWkd/K+1s7r6bHMxDk2yOjw0CXSw7NgTKRBJCmkx8AgmzKc7T04J6ePgTlMgRVO+myx2Rlre2cmpHU25X1sP9/4ajTbrsLz5kt844HBzCb0CT2UjDHz+CaIXveO3+6O5f8+nXn1HYrLm4uxnmsN6ZvyQTGJ8r7SkexlB9eDalpv9vqHr7588nidkMHGtgdLzxYnlx/YJ64563VuMavefQl7GwcHBrTYhLyF7MVOz7+aoc9ij/BCjWvlc6aXjWmsZp/Xn9+A1P6s3Roo0uaAUdOaHjH0Y6zW6sUvHcCRD9g7Zw69zvpf4vsMHn+fvZW6NIqtVWaFPbpFwaLoQZPa7gSfLBraoerNF8QuJ1GzvRkbugPyNPZ0j/ZPfa9r7VnY/ZtUE6srBMy0UR9qGhD5UNL19G8vBHXvVLtvNpx1X5raZ4yAn19mXbo/3cuWXDyi5Y/BNU4ERXyrrqnDZp/n8ajyuSdZ2JMK6gTHY3uXkM5nqFdq5WW410VHEvyNpPvC6x8KLnfFvP8Z+p0M0T+nu4tWqHfzbzK+k86JNMHX6cjtEjH36aYrHHqmYebeKgQPtLhKw/nbgdP87aETgdvSk+z5LQvyktSsEweupYkKU1I8t/KeCvXs8Zwbj3hOn75NDCmpitSUrBvqE/PyBFX2iJKs3QhE5+lTHugOu4fUEH+PSld2jscNwveiDocD+kW36N+G5S7bf41h/tKQ6fIdAr/E+577/kSPtaX1LefV5FlaYdylBFvJRBLbqQuGd49tH+2xonj/8Deb3vdXbwE93+HQdQOc+IK5eBufl/an6yintRfGLXFogjUQ6daqM/1hKu+l0Wzgektlb4CyLFubM/t5+C7af8/DPIwlnzgAOz1XhR6gdzX61kf3SyJ/wVgqHDmZm4N/AF/Xzb9BN7VGv/5xk3kJ0rzeqbSbvv4w1cfx21dN/wW+9f2lseViht9cKf+drrfMH/tzF+3/AIeSdu7cXiDsAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE3LTAzLTAxVDExOjUxOjMzKzAzOjAwYUA7qAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNy0wMy0wMVQxMTo1MTozMyswMzowMBAdgxQAAAARdEVYdGpwZWc6Y29sb3JzcGFjZQAyLHVVnwAAACB0RVh0anBlZzpzYW1wbGluZy1mYWN0b3IAMngyLDF4MSwxeDFJ+qa0AAAAAElFTkSuQmCC",
//     citeleft:"8",
//     citepage:"3",
//     citetop:"224",
//     citewidth:"707",
//     id:"2055"
// }


    function drawRightQuoteItems(data){
        container.empty();
        $(data.cites).each(function(){
            var item = $(template).clone(),
                itemData = this;
            item.appendTo(container);
            item.find('a').attr('p',itemData.citepage).end()
                .find('a span').text(itemData.citepage + ' страница').end()
                .find('a img').attr('src',itemData.citeimg).end()
                .find('.delete').attr('id',itemData.id);
        });

    }
});

FRONT.viewer.drawSheetQuoteItems = function(gettedData) {
    var obj = gettedData;
    if (!obj || $.isEmptyObject(obj)) {
        $.when( pagesContent.getEditionData() ).then(function(json){
            build(json);
        });
    } else {
        build(obj);
    }
    function build(data) {
        $('.quote-book').detach();
        // var page = parseInt($('#page-number').val());
        var page = FRONT.viewer.UX.currentPage.num;
        for (var i in data['cites']) {
            if (data['cites'][i].citepage == page) {
                var h = parseInt(data['cites'][i].citeheight) + 24;
                $('.viewer_img').append('<div id="quo_' + data['cites'][i].id + '" class="quote-book"><div class="quote-icon"></div><div class="quote-book-content"></div></div>');
                $('#quo_' + data['cites'][i].id).css({
                    'width': data['cites'][i].citewidth + 'px',
                    'height': h + 'px',
                    'top': data['cites'][i].citetop + 'px',
                    'left': data['cites'][i].citeleft + 'px'
                });
            }
        }
    }
}


$(document).on('click','[data-edition-notes]',function(){
    var toggler = $(this),
        isActive = toggler.hasClass('active'),
        container = $(toggler).next('.note-content'),
        containerIsEmpty = Boolean( $(toggler).next('.note-content:empty').length ),
        template = $('<div>'+
                            '<a><span></span>'+
                            '<span></span>'+
                            '</a>'+
                            '<span class="delete"></span>'+
                        '</div>');

    
    if (!isActive && containerIsEmpty) {
        container.append('<div class="milk-shadow draw" style="height: 100px;"></div>'); 
        FRONT.spinner.execute( $('.milk-shadow', container)[0] );
        $.when( pagesContent.getEditionData() ).then(function(data){
            if (typeof data == 'object') {
                drawRightNoteItems(data);
                FRONT.viewer.drawSheetNoteItems(data);
            } else {
                container.empty();
            }
        });
    } else if (!isActive) {
        FRONT.viewer.drawSheetNoteItems();
    } else {
        $(".note-book").detach();
    }

    function drawRightNoteItems(data){
        container.empty();
        $(data.notes).each(function(){
            var item = $(template).clone(),
                itemData = this;
            item.appendTo(container);
            item.find('a').attr('p',itemData.notepage).end()
                .find('a span').eq(0).text(itemData.notepage + ' страница').end().end()
                .find('a span').eq(1).text(itemData.note).end().end()
                .find('.delete').attr('id',itemData.id);
        });

    }
});

FRONT.viewer.drawSheetNoteItems = function(gettedData) {
    var obj = gettedData;
    if ( (!obj || $.isEmptyObject(obj)) && typeof obj == 'object' ) {
        $.when( pagesContent.getEditionData() ).then(function(json){
            build(json);
        });
    } else if (typeof obj == 'object') {
        build(obj);
    }
    function build(data) {
        // var page = parseInt($('#page-number').val());
        $('.note-book').detach();
        var page = FRONT.viewer.UX.currentPage.num;
        for (var i in data['notes']) {

            if (data['notes'][i].notepage == page) {
                var h = parseInt(data['notes'][i].noteheight) + 24;

                //$('.viewer_img').append('<div id="note_'+data['notes'][i].id+'" class="note-book"><div class="note-icon"></div><textarea class="note-book-content" disabled>'+data['notes'][i].note+'</textarea><span class="note-edit"></span></div>');
                $('.viewer_img').append('<div id="note_' + data['notes'][i].id + '" class="note-book"><div class="note-icon"></div><textarea class="note-book-content" disabled>' + data['notes'][i].note + '</textarea><span class="note-edit" data-url="/local/tools/viewer/note_add.php?noteid=' + data['notes'][i].id + '&notepage=' + data['notes'][i].notepage + '&notetop=' + data['notes'][i].notetop + '&noteleft=' + data['notes'][i].noteleft + '&notewidth=' + data['notes'][i].notewidth + '&noteheight=' + data['notes'][i].noteheight + '"></span></div>');
                $('#note_' + data['notes'][i].id).css({
                    'width': data['notes'][i].notewidth + 'px',
                    'height': h + 'px',
                    'top': data['notes'][i].notetop + 'px',
                    'left': data['notes'][i].noteleft + 'px'
                });

            }
        }
    }
}

// isQuo from setTimeout
$(document).on('click','.note-icon', function() {
                    //$(this).parent('.note-book').toggleClass('hide');
    $(this).siblings('.note-book-content').slideToggle(300);
    $(this).siblings('.note-edit').toggleClass('visible');
});
$(document).on('click','.quote-icon', function() {
    //$(this).parent('.quote-book').toggleClass('hide');
    $(this).siblings('.quote-book-content').slideToggle(300);
});

$(document).on('click','.note-edit', function() {
    if ($(this).hasClass('save')) {
        $(this).siblings('.note-book-content').attr('disabled', true);
        $(this).parent('.note-book').removeClass('edit');
        $(this).removeClass('save');
        var text = $(this).prev().val();
        var url = $(this).attr('data-url') + '&note=' + text + '&book_id=' + book_id;
        $.ajax({
            type: "GET",
            url: url,
            success: function(data) {

            }
        })
    } else {
        $(this).siblings('.note-book-content').removeAttr('disabled');
        $(this).parent('.note-book').addClass('edit');
        $(this).addClass('save');
    }
});


// FRONT.viewer.checkIsMark = function(pageNum) {
//     var page = pageNum,
//         dfd;
//     if (typeof pageNum == 'undefined') {
//         page = FRONT.viewer.UX.currentPage.num;
//     }
// }




// проверка страницы на закладку
function isMark(page) { 
    var pageNum = FRONT.viewer.UX.currentPage.num,
        marked = false,
        markId;
    $.when( pagesContent.getEditionData(false) ).then(function(data){
        if ($.isEmptyObject(data)) {
            $.ajax({
                type: "GET",
                url: "/local/tools/viewer/bookmark_exist.php",
                data: "book_id=" + book_id + "&page=" + page,
                success: function(data) {
                    if (parseInt(data) > 0) {
                        $('.bookmark-tools, .add-bookmark').addClass('active').attr('title','Удалить страницу из закладок');
                    } else {
                        $('.bookmark-tools, .add-bookmark').removeClass('active').attr('title','Добавить страницу в закладки');
                    }
                }
            })            
        } else {
            $(data['bookmarks']).each(function(){
                var obj = this;
                if (obj.page == pageNum) {
                    marked = true;
                    markId = obj.id;
                }
            });
            if (marked) {
                $('.bookmark-tools, .add-bookmark').addClass('active').attr('title','Удалить страницу из закладок');
            } else {
                $('.bookmark-tools, .add-bookmark').removeClass('active').attr('title','Добавить страницу в закладки');
            }
        }

        // container.empty();
        // $(data.bookmarks).each(function(){
        //     var item = $(template).clone(),
        //         itemData = this;
        //     item.appendTo(container);
        //     item.find('a').attr('p',itemData.page).text(itemData.page + ' страница').end()
        //         .find('.delete').attr('id',itemData.id);
        // });
    });

}

$(function(){

    // var $preloader = $('.preloader');
    // $preloader.fadeOut('slow');

    $('.right-tools-kind > div').css({'max-height': ($(window).height() / 1.5), 'overflow-y':'auto'} );

    $('#search_submit').click(function() {
        $(".search_result").removeClass('empty');
    });

    var mappage = false;
    $('.link-nearest-library').click(function(e) {
        e.preventDefault();
        var mapContainer = $('.library-map');
        mapContainer.slideToggle({
            start: function() {
                if (false === mappage) {
                    mapContainer.mappage();
                    mappage = true;
                }
            }
        });
        e.stopImmediatePropagation();
    });

    // var clonImage = $('.big_image').clone().addClass('zoom-image').attr('id','big_image_clone').show();
    // $('.selectedrect-wrapper').prepend(clonImage);
    // $('.zoom-image').removeClass('.big_image');

});

$(function() {
    $('.rotate-left').rotateLeft();
    $('.rotate-right').rotateRight();

    var listsReady = {};
    $('.ajax-block.slide-down-active, .ajax-block .slide-down-active').click(function() {
        var that = $(this);
        if (!that.hasClass('ajax-block')) {
            that = that.parents('.ajax-block').get(0);
            that = $(that);
        }
        if (that.hasClass('empty-list')) {
            return;
        }
        if (that.find('.list-content').is(':visible')) {
            that.find('.b-addbook_popuptit').removeClass('opened');
        } else {
            that.find('.b-addbook_popuptit').addClass('opened');
        }
        var id = that.attr('id');
        if (listsReady.hasOwnProperty(id) && true === listsReady[id]) {
            that.find('.list-content').slideToggle();
            return;
        }
        var wait = BX.showWait(that.find('h2').get(0));
        $.ajax({
            url: that.data('ajax-url'),
            success: function(data) {
                if (!data) {
                    data = that.data('msg-empty');
                }
                data = $(data);
                BX.closeWait(that.find('h2').get(0), wait);
                that.find('.list-content').html(data);
                that.find('.list-content .b-addbook_popuptit').remove();
                that.find('.list-content').slideDown();
                listsReady[id] = true;
            }
        });
    });
    $('.read-width-btn').click(function() {
        var list = $('.read-with-wrapper');
        if (list.hasClass('open')) {
            list.removeClass('open');
            $(this).removeClass('opened');
            list.slideUp('slow');
        } else {
            list.addClass('open');
            $(this).addClass('opened');
            list.slideDown('slow');
        }
    });
});

/* legacy */
$(function(){
    
    //Кнопка закрыть для всплывающих окон
    $('.close-window').click(function(event){
        event.preventDefault();
        $(this).parent().hide();
    });

    $(document).on( "click", ".js_passrecovery",function(e) {
        e.preventDefault();
        var link =$(this);
        var passform = $('.b-passrform');
        passform.addClass('cont_fade_popup');
            if (link.hasClass('open')) {
                passform.fadeOut(200, function(){
                    link.removeClass('open');
                    $(document).off("click.qbqbqb").off("keyup.qbqbqb");
                });

            } else {
                passform.fadeIn(200, function(){
                    link.addClass('open');
                    $(document).off("click.qbqbqb").on("click.qbqbqb", function(e){
                        var targ = $(e.target);
                        if (!targ.hasClass('cont_fade_popup') && targ.parents('.cont_fade_popup').length < 1) {
                            passform.fadeOut(200, function(){
                                link.removeClass('open');
                            });
                            $(document).off("click.qbqbqb").off("keyup.qbqbqb");
                        }
                    });
                     $(document).off("keyup.qbqbqb").on('keyup.qbqbqb', function(e) {
                        if (e.keyCode == 27) {
                            passform.fadeOut(200, function() { //EA
                                link.removeClass('open');
                            });
                            $(document).off("click.qbqbqb").off("keyup.qbqbqb");
                        }   // esc
                     });

                    $(document).on("click",'.closepopup', function(e) {
                            passform.fadeOut(200, function() { //EA
                                link.removeClass('open');
                            });
                            $(document).off("click.qbqbqb").off("keyup.qbqbqb");
                     });
                });
            }
    });

    $(document).on('submit', '.b-passrecoveryform', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json'
        }).done(function(result) {
            if (result.error_message != '')
            {
                $('.b-passrecoveryform .b-warning').html(result.error_message);
                $('.b-passrecoveryform label').html(result.description);
                $('.b-passrecoveryform .b-warning').removeClass('hidden');
                $('.b-passrecoveryform .validate').removeClass('ok').addClass('error');
            }
            else
            {
                $('.b-passrecoveryform .b-warning').addClass('hidden');
                $('.b-passrecoveryform').addClass('hidden');
                $('.b-passconfirm').removeClass('hidden');
            }
        });
    });

});