<?php
namespace Nota\Library;

\CModule::IncludeModule('iblock');
\CModule::IncludeModule('highloadblock');
\CModule::IncludeModule('nota.exalead');
\CModule::IncludeModule('nota.userdata');

use Bitrix\Highloadblock as HL,
    Nota\Exalead\Stat\StatBookReadTable,
    Nota\Exalead\Stat\StatBookViewTable,
    \Bitrix\Iblock\SectionTable,
    \Bitrix\Iblock\IblockTable,
    \Bitrix\Main\Data\Cache;

class Collection {

    public static function add($name, $description, $background, $libraryID, $isShowInSlider, $isShowName, $parentSection){

        $name = trim($name);
        if (empty($name))
            return 'Обязательное поле "Название выставки" не заполнено.';

        if($parentSection != '-') $iblockSectId = $parentSection;
        else $iblockSectId = '';
        $iblockIdCollection = Collection::getIBlockId(IBLOCK_CODE_COLLECTIONS);
        $section = new \CIBlockSection();
        $id = $section->Add(Array(
            'IBLOCK_ID'         => $iblockIdCollection,
            'IBLOCK_SECTION_ID' => $iblockSectId,
            'NAME'              => $name,
            'CODE'              => \Cutil::translit($name, "ru"),
            'DETAIL_PICTURE'    => \CFile::MakeFileArray($background),
            'DESCRIPTION'       => $description,
            'UF_SLIDER'         => $isShowInSlider,
            'UF_SHOW_NAME'      => $isShowName,
            'UF_LIBRARY'        => $libraryID,
        ));

        return $id ? true : $section->LAST_ERROR;
    }

    public static function getByID($id){
        $iblockIdCollection = Collection::getIBlockId(IBLOCK_CODE_COLLECTIONS);
        $result = \CIBlockSection::GetList(Array(), Array('IBLOCK_ID' => $iblockIdCollection, 'ID' => $id), false, Array('UF_*'));
        $result = $result->Fetch();
        return $result ? $result : false;
    }

    public static function edit($id, $code, $name, $active, $description, $background, $libraryID, $isShowInSlider, $isShowName, $deleteImage, $parentSection){
        $iblockIdCollection = Collection::getIBlockId(IBLOCK_CODE_COLLECTIONS);
        $name = trim($name);
        if (empty($name))
            return 'Обязательное поле "Название выставки" не заполнено.';

        if($deleteImage == 'on') $detailPicture = array('del' => 'Y');
        else $detailPicture = \CFile::MakeFileArray($background);

        if($parentSection != '-') $iblockSectId = $parentSection;
        else $iblockSectId = '';

        $section = new \CIBlockSection();
        $id = $section->Update($id, Array(
            'IBLOCK_ID' => $iblockIdCollection,
            'IBLOCK_SECTION_ID' => $iblockSectId,
            'NAME' => $name,
            'ACTIVE' => $active,
            'CODE' => $code,
            'DETAIL_PICTURE' => $detailPicture,
            'DESCRIPTION' => $description,
            'UF_SLIDER' => $isShowInSlider,
            'UF_SHOW_NAME' => $isShowName,
            'UF_LIBRARY' => $libraryID,
        ));

        return $id ? true : $section->LAST_ERROR;
    }

    public static function remove($collectionID){
        \CIBlockSection::Delete($collectionID);
    }

    public static function sortBooks($data){
        $element = new \CIBlockElement();
        foreach($data as $d){
            $element->Update($d['id'], Array(
                'SORT' => $d['sort']
            ));
        }
    }

    public static function getIBlockId($iblockCode) {
        $res = IblockTable::getList(array(
            'filter' => array('CODE' => $iblockCode),
            'select' => array('ID')
        ))->Fetch();
        return $res['ID'];
    }

    public static function addBook($collectionID, $bookID, $bookTitle, $bookAuthor){
        $element = new \CIBlockElement();
        $iblockIdCollection = Collection::getIBlockId(IBLOCK_CODE_COLLECTIONS);
        if(empty($bookTitle)) {
            $bookName = 'Книга';
        } else {
            $bookName = $bookTitle;
        }
        $element->Add(Array(
            'IBLOCK_ID' => $iblockIdCollection,
            'IBLOCK_SECTION_ID' => $collectionID,
            'NAME' => $bookName,
            'SORT' => 1,
            'PROPERTY_VALUES' => Array(
                'BOOK_ID' => $bookID,
                'BOOK_AUTHOR' => $bookAuthor,
                'TYPE_IN_SLIDER' => COLLECTION_SLIDER_TYPE_BASIC_ENUM_ID
            )
        ));
    }

    public static function toggleBookCover($bookID, $isCover){
        \CIBlockElement::SetPropertyValuesEx($bookID, false, Array('TYPE_IN_SLIDER' => Array('VALUE' => $isCover ? COLLECTION_SLIDER_TYPE_BASIC_ENUM_ID : COLLECTION_SLIDER_TYPE_COVER_ENUM_ID)));
    }

    public static function removeBook($bookID){
        \CIBlockElement::Delete($bookID);
    }

    public static function getBookStat($bookID, $viewOnly = false){
        //кол-во просмотров
        $rsData = StatBookViewTable::getList(array(
            "runtime" => array('CNT' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
            "select" => array('CNT'),
            "filter" => array('=BOOK_ID' => $bookID),
        ));
        $arData = $rsData->Fetch();
        $return['VIEW_CNT'] = (int)$arData['CNT'];

        if(!$viewOnly){
            //кол-во прочтений
            $rsData = StatBookReadTable::getList(array(
                "runtime" => array('CNT' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
                "select" => array('CNT'),
                "filter" => array('=BOOK_ID' => $bookID),
            ));
            $arData = $rsData->Fetch();
            $return['READ_CNT'] = (int)$arData['CNT'];

            //кол-во добавлений в избранное
            $hlblock = HL\HighloadBlockTable::getById(HIBLOCK_BOOKS_DATA_USERS)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $rsData = $entity_data_class::getList(array(
                "runtime" => array('CNT' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
                "select" => array('CNT'),
                "filter" => array('UF_BOOK_ID' => $bookID),
            ));
            $arData = $rsData->Fetch();
            $return['FAV_CNT'] = (int)$arData['CNT'];
        }
        return $return;
    }

    public function getSubSection($sectionId) {
        $arSection = array();
        $iblockIdCollection = Collection::getIBlockId(IBLOCK_CODE_COLLECTIONS);
        $resSect = \CIBlockSection::GetList(
            array("SORT" => "asc"),
            array("IBLOCK_ID" => $iblockIdCollection, "SECTION_ID" => $sectionId),
            false,
            array("ID", "NAME", "DEPTH_LEVEL", "SECTION_PAGE_URL")
        );
        while($obSect = $resSect->GetNext()) {
            $arSection[] = $obSect;
        }
        return $arSection;
    }
}
