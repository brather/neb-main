
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
				background-color: #ee6334;
				color: #fff;
				margin: -25px 5px 0px 275px;
				padding: 3px;
				font-size: 12px;
				font-style: italic;
				display: block;
				width: 71px;
				">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
						
					</div>
					
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right">
						<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix ">
	
	
	<div class="b-searchresult">
		<ul class="b-profile_nav">
			<li>
				<a href="#" class="b-profile_navlk js_profilemenu ">личный кабинет</a>
				<ul class="b-profile_subnav">
					<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
					<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
				</ul>
			</li>
			<li><a href="#" class="">работа с фондами</a></li>
			<li><a class="current" href="#">статистика</a></li>			
		</ul>                 
	</div><!-- /.b-searchresult-->
	<div class="b-addsearch clearfix">
		<a class="button_blue btadd left popup_opener ajax_opener noclose" href="ajax_addbook.php" data-overlaybg="dark" data-width="631" data-posmy="left+120" data-posat="top left">Добавить книгу</a>
		<div class="b-search_field right">
			<input type="submit" class="b-search_bth bbox" value="Найти">
			<span class="b-search_fieldbox">
				<input type="text" name="" class="b-search_fieldtb b-text" id="asearch" value="" autocomplete="off" data-src="search.php?session=MY_SESSION&moreparams=MORE_PARAMS">
				<select name="searchopt" id=""class="js_select b_searchopt">
					<option value="1">по всем полям</option>
					<option value="2">по дате</option>

				</select>
			</span>
			

		</div><!-- /.b-search_field-->
	</div>

	<div class="b-fondtable">

		<table class="b-usertable b-fondtb b-fondtbedit">
			<tr>

				<th class="title_cell"><a class="sort up" href="#">Название</a></th>
				<th class="autor_cell"><a href="#" class="sort up">Автор</a></th>
				<th class="bbk_cell"><a href="#" class="sort up">ББК</a></th>
				<th class="year_cell"><a href="#" class="sort up">Год издания</a></th>
				<th class="exs_cell"><a href="#" class="sort up">Дата добавления </a></th>
				<th class="exs_cell"><a href="#" class="sort up">Ссылка<br> на pdf</a></th>
				<th class="recycle_cell"><span class="b-recyclebin">&nbsp;</span></th>

			</tr>
			<tr>
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">Эгоистичный ген</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
					
				</td>

				<td class="pl15">
					<div class="b-fieldeditable">
						
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">Доккинс Ричард</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_mid"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">140.1234.2341</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_size1"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">1323</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input  type="text" class="txt txt_small"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>		
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">12.12.2014</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_size2"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>
				<td class="pl15">
					<span class="nowrap"> <a href="#" class="b-viewpdf">Посмотреть</a> <a href="#" class="b-fieldeedit"></a></span>
				</td>		
				<td>
					<div class="rel plusico_wrap minus">
						<a href="#" class="plus_ico lib "></a>
						<div class="b-hint rel"><span>Удалить</span> из Моей библиотеки</div>
					</div>
				</td>	
			</tr>

			<tr>
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">Эгоистичный ген</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>

				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">Доккинс Ричард</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_mid"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#">Добавить ББК</a></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_size1"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#">Добавить</a></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_small"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>		
				<td class="pl15">
					<div class="b-fieldeditable">	
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">12.12.2014</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_size2"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>
				<td class="pl15">
					<span class="nowrap"><a href="#">Добавить pdf</a></span>
				</td>		
				<td>
					<div class="rel plusico_wrap minus">
						<a href="#" class="plus_ico lib "></a>
						<div class="b-hint rel"><span>Удалить</span> из Моей библиотеки</div>
					</div><!--b-fieldeditable -->
				</td>	
			</tr>
			<tr>
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">Эгоистичный ген</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>

				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">Доккинс Ричард</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_mid"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#">Добавить ББК</a></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_size1"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>
				<td class="pl15">
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><a href="#">Добавить</a></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_small"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>		
				<td class="pl15">
					<div class="b-fieldeditable">	
						<div class="b-fondtitle"><a href="#" class="b-fieldeedit"></a><span class="b-fieldtext">12.12.2014</span></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_size2"></span><a href="#" class="b-editablsubmit iblock"></a></div>
					</div><!--b-fieldeditable -->
				</td>
				<td class="pl15">
					<span class="nowrap"><a href="#">Добавить pdf</a></span>
				</td>		
				<td>
					<div class="rel plusico_wrap minus">
						<a href="#" class="plus_ico lib "></a>
						<div class="b-hint rel"><span>Удалить</span> из Моей библиотеки</div>
					</div><!--b-fieldeditable -->
				</td>	
			</tr>
		</table>
	</div><!-- /.b-stats-->
	
</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu">мои подборки</a>
	

	<form class="b-selectionadd" action="">
		<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
		<span class="b-selectionaddtxt">Cоздать подборку</span>
		<input type="text" class="input hidden">
	</form>
</div><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>