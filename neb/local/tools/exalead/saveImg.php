<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!$USER->IsAuthorized())
    exit();

CModule::IncludeModule('nota.exalead');
use Nota\Exalead\RslViewer;

CModule::IncludeModule("nota.userdata");
use Nota\UserData\Quotes;
use Nota\UserData\Notes;


$quo_id = intval($_REQUEST['quo_id']);
$note_id = intval($_REQUEST['note_id']);
// удаление цитат
if ($quo_id > 0) {
    Quotes::delete($quo_id);
}
// удаление заметок
if ($note_id > 0) {
    Notes::delete($note_id);
}

$book_id = trim($_REQUEST['book_id']);
if (empty($book_id))
    exit();


$page = intval($_REQUEST['p']);
//$cite = htmlspecialcharsbx(trim($_REQUEST['cite']));
$citepage = $page;
$citetop = intval($_REQUEST['top']);
$citeleft = intval($_REQUEST['left']);
$citewidth = intval($_REQUEST['w']);
$citeheight = intval($_REQUEST['h']);


$Viewer = new RslViewer($book_id);

//размер изображения
$width = round($_REQUEST['w_img']);
//var_dump($width);die;
$rwJpg = $Viewer->getDocumentPage($width, $page);

$thumbnailFolder = "/upload/tmp_quo/" . substr($book_id, -3) . '/';
$fileName = $thumbnailFolder . $url . "w.png";
RewriteFile($_SERVER["DOCUMENT_ROOT"] . $fileName, $rwJpg);

$im = new Imagick($_SERVER["DOCUMENT_ROOT"] . $fileName);

$im->cropImage($citewidth, $citeheight, $citeleft, $citetop);
$im->writeImage();

$img = $_SERVER["DOCUMENT_ROOT"] . $fileName;
$imageSize = getimagesize($img);
$imageData = base64_encode(file_get_contents($img));
$imageHTML = "data:{$imageSize['mime']};base64,{$imageData}";


if ($_REQUEST['type'] != 'note') {

    $arFields = array(
        'BOOK_ID' => $book_id,
        'TEXT' => '',
        'IMG_DATA' => $imageHTML,
        'PAGE' => $citepage,
        'TOP' => $citetop,
        'LEFT' => $citeleft,
        'WIDTH' => $citewidth,
        'HEIGHT' => $citeheight,
    );

    $id = Quotes::add($arFields);
    $quo = Quotes::getById($id);
    $html = '<div><a p=' . $quo["UF_PAGE"] . '>
<span>' . $quo["UF_PAGE"] . ' страница</span>
<img src=' . $quo['UF_IMG_DATA'] . '>
</a>
<span class="delete" id=' . $quo["ID"] . '></span>
	</div>';

} else {
    //заметки

    $note = trim($_REQUEST['note']);
    $arFields = array(
        'BOOK_ID' => $book_id,
        'NUM_PAGE' => $citepage,
        'NOTE' => $note,
        'NOTE_AREA' => '',
        'IMG_DATA' => $imageHTML,
        'TOP' => $citetop,
        'LEFT' => $citeleft,
        'WIDTH' => $citewidth,
        'HEIGHT' => $citeheight,
    );


    $id = Notes::addFromArray($arFields);
    $note = Notes::getById($id);

    $html = '<div><a p=' . $note["UF_NUM_PAGE"] . '> 
<span>' . $note["UF_NUM_PAGE"] . ' страница</span>
<span>' . $note['UF_TEXT'] . '</span>
</a>
<span class="delete" id=' . $note["ID"] . '></span>
	</div>';

}
echo $html;