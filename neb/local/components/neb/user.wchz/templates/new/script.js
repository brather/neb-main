$(function(){

    $('ul.lk-table__row .btn-primary').click(function () {

        var that = $(this);
        var user_id = $(this).attr('data-id');

        // запрос на активацию пользователя
        $.ajax({
            type: "PUT",
            url: "/rest_api/users/status/",
            data: "user_id=" + user_id + "&active=true&token=" + getUserToken(),
            success: function (data) {
                if(true === data.success) {
                    that.parents('.lk-table__row').remove();
                }

            }
        });

    })

})