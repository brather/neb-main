<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<!-- МЕНЮ В САЙДБАРЕ -->
<? $APPLICATION->IncludeComponent(
    'bitrix:menu', 'lk_left', Array(
    'ROOT_MENU_TYPE'        => 'left_' . LANGUAGE_ID,
    'MENU_CACHE_TYPE'       => 'N',
    'MENU_CACHE_TIME'       => '3600',
    'MENU_CACHE_USE_GROUPS' => 'N',
    'MENU_CACHE_GET_VARS'   => array(0 => '',),
    'MAX_LEVEL'             => '2',
    'CHILD_MENU_TYPE'       => '',
    'USE_EXT'               => 'Y',
    'DELAY'                 => 'N',
    'ALLOW_MULTI_SELECT'    => 'N',
),
    false
); ?>
<!-- /МЕНЮ В САЙДБАРЕ -->
<?
$APPLICATION->IncludeComponent(
    'neb:library.left_counter',
    '',
    Array(
        'IBLOCK_ID'  => Bitrix\NotaExt\Iblock\IblockTools::getIBlockId(
            IBLOCK_CODE_LIBRARY
        ),
        'LIBRARY_ID' => $arLibrary['ID'],
        'CACHE_TIME' => 3600,
    ),
    $component
);
?>
<!-- /КАРТОЧКА С ИНФОРМАЦИЕЙ -->


