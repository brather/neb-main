<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Highloadblock as HL;

/**
 * Компонент заявки на оцифровку
 * Class BooksDigitalQueryComponent
 */
class BooksDigitalQueryComponent extends \CBitrixComponent
{
    protected $user;
    protected $userLibrary;
    protected $modules = array('highloadblock');

    public function onIncludeComponentLang()
    {
        $this -> includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    public function onPrepareComponentParams($params)
    {
        $this->user = new nebUser();
        $this->userLibrary = $this->user->getLibrary();

        $params['PDF_AVAILABLE'] = (bool)$params['PDF_AVAILABLE'];
        $params['EXALEAD_BOOK_ID'] = trim($params['EXALEAD_BOOK_ID']);
        $params['LIBRARY_ID'] = $this->userLibrary['ID'];
        $params['CACHE_TIME'] = (intval($params['CACHE_TIME']) > 0 ? intval($params['CACHE_TIME']) : 3600);

        return $params;
    }

    /**
     * Проверяет подключение необходиимых модулей
     * @throws LoaderException
     */
    protected function checkModules()
    {
        foreach ($this->modules as $module)
        {
            if(!Main\Loader::includeModule($module))
            {
                throw new Main\LoaderException(Loc::getMessage("Не установлен модуль #MODULE#", array("#MODULE#" => $module)));
            }
        }
    }

    /**
     * Проверка на корректность обязательных параметров компонента
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        $result = true;

        if (empty($this -> arParams['EXALEAD_BOOK_ID']))
            $result = false;

        if (!isset($this -> arParams['PDF_AVAILABLE']))
            $result = false;

        if (empty($this -> arParams['LIBRARY_ID']))
            $result = false;

        return $result;
    }

    /**
     * Проверка прав доступа
     */
    protected function checkAccess()
    {
        if (PlanDigitalization::isAdd() || $this->userLibrary['ID'] == $this->arParams['LIBRARY_ID'])
            return true;
        else
            return false;
    }

    /**
     * Обработка действий
     */
    protected function checkActions()
    {
        if (!check_bitrix_sessid())
            return;

        $action = (isset($_REQUEST['action']) ? trim($_REQUEST['action']) : '');
		
		if ($action == 'addDigitizationQuery' && $_SERVER['REQUEST_METHOD'] == 'POST')
            $this->addDigitizationQuery();
		elseif ($action == 'addDigitizationQueryForm')
            $this->addDigitizationQueryForm();
    }

    /**
     * Добавление заявки на оцифровку
     * @return bool
     * @throws Exception
     */
    protected function addDigitizationQuery()
    {
        $hlblock = HL\HighloadBlockTable::getById(HIBLOCK_PLAN_DIGIT)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $dt = new \Bitrix\Main\Type\DateTime();
        $dtPlus = new \Bitrix\Main\Type\DateTime();
        $dtPlus->add('+'.PLAN_DIGIT_PERIOD_FINISH.' days');

        $user = new nebUser();

        $arFields = array(
            'UF_LIBRARY' 		=> $this->arParams['UF_LIBRARY'],
            'UF_EXALEAD_BOOK' 	=> $this->arParams['EXALEAD_BOOK_ID'],
            'UF_DATE_ADD' 		=> $dt,
            'UF_DATE_FINISH' 	=> $dtPlus,
            'UF_COMMENT'        => htmlspecialchars($_REQUEST['comment']),
            'UF_NEB_USER'       => $user->GetID(),
        );

        if ($id = $entity_data_class::add($arFields)->getId())
        {
            /**
             * @TODO: сбрасывать кеш на событиях
             */
            global $CACHE_MANAGER;
            $CACHE_MANAGER->ClearByTag("hlblock_id" . HIBLOCK_PLAN_DIGIT);
        }

        global $APPLICATION;
        LocalRedirect($APPLICATION->GetCurPage());
    }

	/**
	 * Форма добавления заявки на оцифровку
	 */
	protected function addDigitizationQueryForm()
	{
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
        
        $this->IncludeComponentTemplate('addForm');
		
		die;
	}

    /**
     * Проверка есть ли уже заявка на эту книгу от данной библиотеки
     * @return bool
     * @throws Exception
     */
    protected function isQueryExists()
    {
        $result = false;
        $hlblockId = HIBLOCK_PLAN_DIGIT;

        $hlblock = HL\HighloadBlockTable::getById($hlblockId)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $obCacheExt    	= new \Bitrix\NotaExt\NPHPCacheExt();
        $arCacheParams = array(
            'hlblock_id' => $hlblock,
            'UF_LIBRARY' => $this->arParams['LIBRARY_ID'],
            'UF_EXALEAD_BOOK' => $this->arParams['EXALEAD_BOOK_ID'],
        );
        $cacheTag = "hlblock_id" . HIBLOCK_PLAN_DIGIT;

        if (!$obCacheExt->InitCache(__function__, $arCacheParams, $cacheTag, $this->arParams['CACHE_TIME']))
        {
            $rsData = $entity_data_class::getList(array(
                "select" => array("ID"),
                "filter" => array('UF_LIBRARY' => $this->arParams['LIBRARY_ID'], 'UF_EXALEAD_BOOK' => $this->arParams['EXALEAD_BOOK_ID'])
            ));

            if ($arData = $rsData->Fetch())
            {
                $result = true;
            }

            $obCacheExt->StartDataCache($result);
        }
        else
        {
            $result = $obCacheExt->GetVars();
        }

        return $result;
    }

    public function executeComponent()
    {
        try
        {
            if ($this->checkParams() && $this->checkAccess())
            {
                if ($this->isQueryExists())
                {
                    $this->arResult['STATUS'] = 'QUERY_ALREADY_EXISTS';
                }
                else
                {
                    $this->checkActions();
                }

                $this->IncludeComponentTemplate();
            }
        }
        catch (Exception $e)
        {
            ShowError($e -> getMessage());
        }
    }
}