<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'   => '/',
                'VARIABLES' => array(
                    'token',
                    'active',
                ),
                'TYPE'      => array('GET'),
            ),
        ),
    ),
); 