<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

?>
<div class="b-form_header"><span
        class="iblock"><?= Loc::getMessage(
            'REGISTER_FIELD_PASSPORT'
        ); ?></span></div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="settings01"><?= Loc::getMessage(
                'REGISTER_FIELD_LAST_NAME'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <em class="error maxlength"><?= Loc::getMessage(
                    'REGISTER_FIELD_MORE_30_SYMBOLS'
                ); ?></em>
            <input type="text" data-validate="fio"
                   data-required="required"
                   value="<?= $arResult["VALUES"]['LAST_NAME'] ?>"
                   id="settings01" data-maxlength="30"
                   data-minlength="2" name="REGISTER[LAST_NAME]"
                   class="input">
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="settings02"><?= Loc::getMessage(
                'REGISTER_FIELD_FIRSTNAME'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <em class="error maxlength"><?= Loc::getMessage(
                    'REGISTER_FIELD_MORE_30_SYMBOLS'
                ); ?></em>
            <input type="text" data-required="required"
                   data-validate="fio"
                   value="<?= $arResult["VALUES"]['NAME'] ?>"
                   id="settings02" data-maxlength="30"
                   data-minlength="2" name="REGISTER[NAME]"
                   class="input">
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <label for="settings22"><?= Loc::getMessage(
                'REGISTER_FIELD_MIDNAME'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <em class="error maxlength"><?= Loc::getMessage(
                    'REGISTER_FIELD_MORE_30_SYMBOLS'
                ); ?></em>
            <input type="text" data-validate="fio"
                   value="<?= $arResult["VALUES"]['SECOND_NAME'] ?>"
                   id="settings22" data-maxlength="30"
                   data-minlength="2" data-required="false"
                   name="REGISTER[SECOND_NAME]" class="input">
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="settings06"><?= Loc::getMessage(
                'REGISTER_FIELD_PERSONAL_BIRTHDAY'
            ); ?></label>

        <div class="field validate iblock seldate"
             data-yearsrestrict="12">
            <em class="error dateformat"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT'
                ); ?></em>
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_FILL'
                ); ?></em>
            <em class="error yearsrestrict"><?= Loc::getMessage(
                    'REGISTER_FIELD_AGE'
                ); ?></em>
            <input type="hidden" class="realseldate"
                   data-required="true"
                   value="<?= $arResult["VALUES"]['PERSONAL_BIRTHDAY'] ?>"
                   id="settings06"
                   name="REGISTER[PERSONAL_BIRTHDAY]">
            <select name="birthday" id="PERSONAL_BIRTHDAY_D"
                    class="js_select w102 sel_day"
                    data-required="true">
                <option value="-1"><?= Loc::getMessage(
                        'MAIN_REGISTER_DAY'
                    ); ?></option>
                <?
                for ($i = 1; $i <= 31; $i++) {
                    ?>
                    <option value="<?= $i ?>"><?= $i ?></option>
                <?
                }
                ?>
            </select>

            <select name="birthmonth" id="PERSONAL_BIRTHDAY_M"
                    class="js_select w165 sel_month"
                    data-required="true">
                <option value="-1"><?= Loc::getMessage(
                        'MAIN_REGISTER_MONTH'
                    ); ?></option>
                <?
                for ($i = 1; $i <= 12; $i++) {
                    ?>
                    <option value="<?= $i ?>"><?= FormatDate(
                            'f', MakeTimeStamp(
                                '01.' . $i . '.' . date('Y')
                            )
                        ) ?></option>
                <?
                }
                ?>
            </select>

            <select name="birthyear" id="PERSONAL_BIRTHDAY_Y"
                    class="js_select w102 sel_year"
                    data-required="true">
                <option value="-1"><?= Loc::getMessage(
                        'MAIN_REGISTER_YEAR'
                    ); ?></option>
                <?
                for (
                    $i = (date('Y') - 14);
                    $i >= (date('Y') - 95); $i--
                ) {
                    ?>
                    <option value="<?= $i ?>"><?= $i ?></option>
                <?
                }
                ?>
            </select>
        </div>

    </div>
</div>
<div class="fieldcell iblock">
    <em class="hint">*</em>
    <label for="settings08"><?= Loc::getMessage(
            'REGISTER_FIELD_GENDER'
        ); ?></label>

    <div class="field validate iblock">
        <em class="error required"><?= Loc::getMessage(
                'REGISTER_FIELD_REQ'
            ); ?></em>
        <select name="REGISTER[PERSONAL_GENDER]" id="PERSONAL_GENDER"
                class="js_select w370" data-required="true">
            <option value="M">
                <?= Loc::getMessage('REGISTER_FIELD_GENDER_MALE'); ?>
            </option>
            <option value="F">
                <?= Loc::getMessage('REGISTER_FIELD_GENDER_FEMALE'); ?>
            </option>
        </select>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="settings011"><?= Loc::getMessage(
                'REGISTER_FIELD_PASSPORT_NUMBERS'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>

            <div class="field iblock"><input type="text"
                                             data-required="required"
                                             value="<?= $arResult["VALUES"]['UF_PASSPORT_SERIES'] ?>"
                                             id="settings011"
                                             name="UF_PASSPORT_SERIES"
                                             class="input w110">
            </div>
            <div class="field iblock"><input type="text"
                                             data-required="required"
                                             value="<?= $arResult["VALUES"]['UF_PASSPORT_NUMBER'] ?>"
                                             id="settings022"
                                             name="UF_PASSPORT_NUMBER"
                                             class="input w190">
            </div>
        </div>
    </div>
</div>
<div class="fieldcell iblock">
    <em class="hint">*</em>
    <label for="settings08"><?= Loc::getMessage(
            'REGISTER_FIELD_CITIZENSHIP'
        ); ?></label>

    <div class="field validate iblock">
        <em class="error required"><?= Loc::getMessage(
                'REGISTER_FIELD_REQ'
            ); ?></em>
        <select name="UF_CITIZENSHIP" id="UF_CITIZENSHIP"
                class="js_select w370" data-required="true">
            <?
            $arUF_CITIZENSHIP = nebUser::getFieldEnum(
                array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'),
                array("VALUE" => "ASC")
            );
            if (!empty($arUF_CITIZENSHIP)) {
                $last = count($arUF_CITIZENSHIP);
                foreach ($arUF_CITIZENSHIP as $arItem) {
                    if ($arItem['XML_ID'] == $last) {
                        $other = $arItem;
                        continue;
                    }
                    if ($arItem['DEF'] == "Y") {
                        $ruCitizenship = $arItem['ID'];
                    }
                    ?>
                    <option
                        value="<?= $arItem['ID'] ?>"<?= ($arItem['DEF']
                        == "Y") ? ' selected="selected"'
                        : ''; ?>><?= $arItem['VALUE'] ?></option>
                <?php
                }
                ?>
                <option
                    value="<?= $other['ID'] ?>"<?= ($other['DEF']
                    == "Y") ? ' selected="selected"'
                    : ''; ?>><?= $other['VALUE'] ?></option>

            <?
            }
            ?>
        </select>
    </div>
</div>