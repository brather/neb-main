<link href="/local/templates/adaptive/css/bootstrap.min.css" rel="stylesheet">
<link href="/local/templates/adaptive/css/style.css" rel="stylesheet">
<link href="/local/templates/adaptive/css/low_vision.css" rel="stylesheet">

<div id="panel-formYandexMoney">

<form action="<?php echo $arResult["PaymentType"]["ACTION"]?>" method="post" target="parent">
    <?php foreach($arResult["PaymentType"]["FIELDS"] as $name=>$field):?>
        <input type="hidden" value="<?php echo $field;?>" name="<?php echo $name;?>"/>
    <?php endforeach;?>
    <select name="paymentType" class="form-control">
        <?php foreach($arResult["PaymentType"]["TYPES"] as $name=>$field):?>
            <option value="<?php echo $name;?>"><?php echo $field;?></option>
        <?php endforeach;?>
    </select>
    <div style="margin-top: 1em;">
        <button href="#" class="btn btn-primary pull-right" data-parent-submit>Оплатить</button>
        <a href="#" class="btn btn-default payorderlinkcancel" data-parent-modal-close>Закрыть</a>
    </div>
</form>

<script>
    if (self != top && typeof $ !== 'function') {
        window.$ = window.parent.jQuery;
    }
    var iframeDOCUMENT = $(document),
        parentDocument = $(window.parent.document); /*если наследовать $ из родительского окна (мы в ифрейме)
        modal fade in*/

    iframeDOCUMENT.on('click', '[data-parent-submit]', function(e){
        window.parent.location.reload();
    });
    iframeDOCUMENT.on('click', '[data-parent-modal-close]', function(e){
        parentDocument.find('.modal.fade.in').modal('hide');
    });
    var bodyClasses = $(window.parent.document).find('body').attr('class');
    $(document).find('body').addClass(bodyClasses).removeClass('modal-open');
</script>

</div>