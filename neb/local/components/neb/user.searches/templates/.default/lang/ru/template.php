<?
$MESS['PROFILE_SEARCHES_SORT_DATE'] = 'По дате';
$MESS['PROFILE_SEARCHES_SHOW'] = 'Показать';
$MESS['PROFILE_SEARCHES_SETTINGS'] = 'Настройки';
$MESS['PROFILE_SEARCHES_REMOVE'] = 'Удалить';
$MESS['PROFILE_SEARCHES_FROM_SEARCHES'] = 'из Моих поисковых запросов';
$MESS['PROFILE_SEARCHES_SAVED_REQUEST'] = 'Дата';
$MESS['PROFILE_SEARCHES_YOU_SEARCHED'] = 'Вы искали';
$MESS['PROFILE_SEARCHES_FOUND'] = 'Найдено';
$MESS['PROFILE_SEARCHES_FOUND_1'] = 'Найдена';
$MESS['PROFILE_SEARCHES_FOUND_2'] = 'Найдены';
$MESS['PROFILE_SEARCHES_FOUND_5'] = 'Найдено';
$MESS['PROFILE_SEARCHES_REFINE'] = 'Уточнение поиска';
$MESS['PROFILE_SEARCHES_REDO'] = 'Повторить поиск';
$MESS['PROFILE_SEARCHES_MY_COLLETIONS'] = 'мои подборки';
$MESS['PROFILE_SEARCHES_CREATE_COLLECTION'] = 'cоздать подборку';
$MESS['PROFILE_SEARCHES_BOOKS_1'] = 'книга';
$MESS['PROFILE_SEARCHES_BOOKS_2'] = 'книги';
$MESS['PROFILE_SEARCHES_BOOKS_5'] = 'книг';
$MESS['PROFILE_MU_NUMBER_LC'] = 'Номер ЕЭЧБ';
$MESS['PROFILE_SEARCHES_EXPAND'] = 'Все параметры';
$MESS['PROFILE_SEARCHES_COLLAPSE'] = 'Свернуть';

?>
