<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="lk-sidebar__menu">
        <div class="lk-sidebar__menu-adaptive">
            <?foreach($arResult as $arItem):?>
            <a href="<?=$arItem["LINK"]?>"
                tabindex="-1"
                class="lk-sidebar__menu-link<?=($arItem['SELECTED'])?' lk-sidebar__menu-link--active':''?>"
            >
                <?=$arItem["TEXT"]?>
            </a>
            <?endforeach;?>
        </div>
    </div>
<?endif;?>