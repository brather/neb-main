<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<?
use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Type\DateTime;

$context   = Application::getInstance()->getContext();
$arServer  = $context->getServer();
$arRequest = $context->getRequest()->toArray();



?>
<div class="container-fluid"><hr></div>
<?if(!empty($arResult['ERROR'])) {?>
    <h3 class="text-danger"><?= $arResult['ERROR'] ?></h3>
<?} else {?>
    <?$arCurentPlan = $arResult['LIST_PLAN'][$arRequest['idplan']];
    $showDuplication = false;
    $classDuplication = '';
    $classBtnChangePlan = '';?>
    <div class="container-fluid plan-list" id="plan-<?= $arRequest['idplan'] ?>">
        <div class="col-xs-6">
            <p><small><?= Loc::getMessage('DIG_PLAN_EDIT_T_PLAN') ?></small><?= $arCurentPlan['UF_PLAN_NAME'] ?></p>
            <p><small><?= Loc::getMessage('DIG_PLAN_EDIT_T_LIBRARY') ?></small><?= $arResult['LIST_LIBRARY'][$arCurentPlan['UF_LIBRARY']]['NAME'] ?></p>
            <p><small><?= Loc::getMessage('DIG_PLAN_EDIT_T_DATE_PLANED') ?></small><?= $arCurentPlan['UF_DATE_F_PLAN'] ?></p>
        </div>
        <div class="col-xs-6">
            <p><?= Loc::getMessage('DIG_PLAN_EDIT_T_PLAN_STATUS') ?></p>
            <?if($arCurentPlan['LIST_STATUS']['ACTIVE_SELECT'] == 'Y') {?>
                <?foreach ($arResult['PROPERTY_STATUS_PLAN'] as $idStatus => $arStatus) {
                    foreach($arResult['PROPERTY_STATUS_BOOK'] as $idStatusBook => $arStatusBook) {
                        if($arStatusBook['VALUE'] == $arStatus['VALUE']) {?>
                            <input id="ps-<?= $idStatus ?>" type="hidden" name="plan-status-<?= $idStatus ?>" value="<?= $idStatusBook ?>" />
                            <?break;
                        }
                    }
                }?>
                <select name="pl-status" id="pl-select-<?= $arRequest['idplan'] ?>" class="form-control">
                    <?foreach($arCurentPlan['LIST_STATUS']['OPTIONS'] as $idStatus) {?>
                        <?$selected = '';
                        if($idStatus == $arCurentPlan['UF_REQUEST_STATUS']){
                            $selected = ' selected';
                        }?>
                        <option value="<?= $idStatus ?>"<?= $selected ?>><?= $arResult['PROPERTY_STATUS_PLAN'][$idStatus]['VALUE'] ?></option>
                    <?}?>
                </select>
            <?} else {?>
                <?$classBtnChangePlan = ' disabled';?>
                <p><?= $arResult['PROPERTY_STATUS_PLAN'][$arCurentPlan['LIST_STATUS']['OPTIONS'][0]]['VALUE'] ?></p>
            <?}?>
            <p><br></p>
            <p>
                <button class="btn btn-info js-update-plan<?= $classBtnChangePlan ?>"
                        data-confirm-title="<?= Loc::getMessage('DIG_PLAN_EDIT_T_CONFIRM_TITLE') ?>"
                        data-confirm-question="<?= Loc::getMessage('DIG_PLAN_EDIT_T_CONFIRM_QUESTION') ?>"
                        data-confirm-request="<?= Loc::getMessage('DIG_PLAN_EDIT_T_CONFIRM_REQUEST') ?>"
                        data-status-plan="#pl-select-<?= $arRequest['idplan'] ?>"
                        data-plan-id="<?= $arRequest['idplan'] ?>"
                        data-ajax-url="<?= $arParams['AJAX_PLAN_EDIT'] ?>">
                    <?= Loc::getMessage('DIG_PLAN_EDIT_T_CHANGE') ?>
                </button>
            </p>
        </div>
    </div>

    <div class="container-fluid"><hr></div>



    <div class="b-readersearch clearfix">
        <h3><?=Loc::getMessage("MARKUP_LOG_TITLE");?></h3>

        <form class="form nrf" method="get" action="?">

            <input type="hidden" name="idplan" value="<?= $arRequest['idplan'] ?>" />
            <input type="hidden" name="FILTER" value="Y" />

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="e_author">Автор</label>
                        <input type="text" name="AUTHOR" id="e_author" value="<?=$arRequest['AUTHOR']?>" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="e_title">Название</label>
                        <input type="text" name="TITLE" id="e_title" value="<?=$arRequest['TITLE']?>" class="form-control" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Статус</label>
                        <select class="form-control" name="STATUS">
                            <option value="">Все</option>
                            <? foreach ($arResult['PROPERTY_STATUS_BOOK'] as $arStatus): ?>
                                <option value="<?=$arStatus['ID']?>"<?=$arRequest['STATUS'] == $arStatus['ID']
                                    ? ' selected="selected"' : '';?>><?=$arStatus['VALUE']?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>&nbsp;</label><br />
                        <input type="submit" value="<?= Loc::getMessage('FIND') ?>" class="btn btn-primary" />
                        <input type="reset" value="Сбросить" class="btn btn-default" onClick="
                            document.location.href = '<?=$arParams['SEF_FOLDER']
                                . $arParams['SEF_URL_TEMPLATES']['template']?>?idplan=<?= $arRequest['idplan'] ?>';
                        "/>
                    </div>
                </div>
            </div>

        </form>

    </div>








    <div class="container-fluid blip" id="book-list-<?= $arRequest['idplan'] ?>">
        <div class="search-result-sort clearfix res-sort-wtf">
            <div class="sort_wrap">
                <?foreach($arResult['SORT_LIST'] as $cSort => $arSort){?>
                    <a href="<?= $arSort['URI'] ?>" class="<?= $arSort['CLASS'] ?>">
                        <?= Loc::getMessage('DIG_PLAN_EDIT_T_SORT_' . $cSort) ?>
                    </a>
                <?}?>
                <?if(!empty($arResult['DUPLICATION_URI'])){
                    $classDuplication = ' blip-duplication';?>
                    <a href="<?= $arResult['DUPLICATION_URI'] ?>">
                        <?if($arRequest['show-duplication'] == 'off' || empty($arRequest['show-duplication'])){?>
                            <?= Loc::getMessage('DIG_PLAN_EDIT_T_SORT_SHOW_DUPLICATION') ?>
                        <?} elseif($arRequest['show-duplication'] == 'on') {
                            $showDuplication = true;?>
                            <?= Loc::getMessage('DIG_PLAN_EDIT_T_SORT_HIDE_DUPLICATION') ?>
                        <?}?>
                    </a>
                <?}?>
            </div>
        </div>

        <div class="b-result-doc b-collection_booklist js_sortable ui-sortable blip-list<?= $classDuplication ?>"
             id="accordion-books"
             data-empty-mess="<?= Loc::getMessage('DIG_PLAN_EDIT_T_EMTY_BOOK_LIST') ?>">
            <?$indexBook = 0;
            foreach($arResult['LIST_BOOKS'] as $iBook => $arBook){?>
                <?$clsBook = '';
                if(count($arBook['DUPLICATE']) > 0) {
                    $clsBook = ' blip-duplication-book';
                    if($arResult['PROPERTY_STATUS_BOOK'][$arBook['UF_REQUEST_STATUS']]['VALUE'] != DIGITIZATION_STATUS_CANCELED) {
                        $clsBook .= ' blip-duplication-no-canceled';
                    }
                }
                if($showDuplication) {
                    if(count($arBook['DUPLICATE']) > 0) {
                        $indexBook = $indexBook + 1;
                    } elseif(count($arBook['DUPLICATE']) == 0) {
                        continue;
                    }
                } else {
                    $indexBook = $iBook + 1;
                }?>
                <div class="blip-item b-result-docitem container-fluid ui-sortable-handle<?= $clsBook ?>" id="<?= $arBook['ID'] ?>">
                    <div class="row" id="<?= $arBook['UF_EXALEAD_BOOK'] ?>">
                        <div class="col-xs-1">
                            <div class="row">
                                <h3><?= $arBook['INDEX'] ?><?= '.' ?></h3>
                            </div>
                        </div>
                        <div class="col-xs-10 iblock b-result-docinfo">
                            <div class="row">
                                <h3>
                                    <a target="_blank" href="/catalog/<?= $arBook['UF_EXALEAD_BOOK'] ?>">
                                        <?= $arBook['UF_BOOK_NAME'] ?>
                                    </a>
                                </h3>
                                <ul class="b-resultbook-info">
                                    <? if (!empty($arBook['UF_EXALEAD_BOOK'])): ?>
                                        <li>
                                            <?= Loc::getMessage('DIG_PLAN_EDIT_T_BOOK_EXALEAD')?>
                                            <?= $arBook['UF_EXALEAD_BOOK'] ?>
                                        </li>
                                    <? endif; ?>
                                    <? if (!empty($arBook['UF_BOOK_AUTHOR'])): ?>
                                        <li>
                                            <?= Loc::getMessage('DIG_PLAN_EDIT_T_BOOK_AUTHOR')?>
                                            <?= $arBook['UF_BOOK_AUTHOR'] ?>
                                        </li>
                                    <? endif; ?>
                                    <? if (!empty($arBook['UF_BOOK_YEAR_PUBLIC'])): ?>
                                        <li>
                                            <?= Loc::getMessage('DIG_PLAN_EDIT_T_BOOK_PUBLISH_YEAR')?>
                                            <?= $arBook['UF_BOOK_YEAR_PUBLIC'] ?>
                                        </li>
                                    <? endif; ?>
                                    <? if (!empty($arBook['UF_DATE_ADD'])): ?>
                                        <li>
                                            <?= Loc::getMessage('DIG_PLAN_EDIT_T_BOOK_DATE_ADD')?>
                                            <?= $arBook['UF_DATE_ADD'] ?>
                                        </li>
                                    <? endif; ?>
                                    <? if (!empty($arBook['UF_NUMBER_FROM_READE']) || !empty($arBook['UF_DATE_READER'])): ?>
                                        <li>
                                            <?= Loc::getMessage('DIG_PLAN_EDIT_T_BOOK_ORDER')?>
                                            <? if (!empty($arBook['UF_NUMBER_FROM_READE'])): ?>
                                                № <?= $arBook['UF_NUMBER_FROM_READE'] ?>
                                            <? endif; ?>
                                            <? if (!empty($arBook['UF_DATE_READER'])): ?>
                                                от <?= $arBook['UF_DATE_READER'] ?>
                                            <? endif; ?>
                                        </li>
                                    <? endif; ?>
                                    <? if (!empty($arBook['UF_COMMENT'])): ?>
                                        <li>
                                            <?= Loc::getMessage('DIG_PLAN_EDIT_T_BOOK_COMMENT')?>
                                            <?= $arBook['UF_COMMENT'] ?>
                                        </li>
                                    <? endif; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-6">
                                    <p class="text-right"><?= Loc::getMessage('DIG_PLAN_EDIT_T_BOOK_STATUS') ?></p>
                                </div>
                                <div class="col-xs-6">
                                    <?$selectDisabled = '';
                                    $arOptions = array();
                                    $arAllowed = array();
                                    $arStatusCurrent = $arResult['PROPERTY_STATUS_BOOK'][$arBook['UF_REQUEST_STATUS']];
                                    //region Если статус "На согласовании оператором НЭБ", "Согласовано", "Отменено"
                                    if($arStatusCurrent['VALUE'] == DIGITIZATION_STATUS_ON_AGREEMENT
                                        || $arStatusCurrent['VALUE'] == DIGITIZATION_STATUS_AGREED
                                        || $arStatusCurrent['VALUE'] == DIGITIZATION_STATUS_CANCELED) {
                                        $arAllowed = array(
                                            DIGITIZATION_STATUS_ON_AGREEMENT,
                                            DIGITIZATION_STATUS_AGREED,
                                            DIGITIZATION_STATUS_CANCELED);

                                    }
                                    //endregion
                                    //region Если статус "На оцифровке"
                                    elseif($arStatusCurrent['VALUE'] == DIGITIZATION_STATUS_ON) {
                                        $selectDisabled = ' disabled';
                                        $arAllowed = array(DIGITIZATION_STATUS_ON);
                                    }
                                    //endregion
                                    //region Если статус "Оцифровано" или "Выполнено"
                                    elseif($arStatusCurrent['VALUE'] == DIGITIZATION_STATUS_DIGITIZED
                                        || $arStatusCurrent['VALUE'] == DIGITIZATION_STATUS_DONE){
                                        $arAllowed = array(DIGITIZATION_STATUS_DIGITIZED, DIGITIZATION_STATUS_DONE);
                                    }
                                    //endregion
                                    foreach($arResult['PROPERTY_STATUS_BOOK'] as $idStatus => $arStatus) {
                                        if(in_array($arStatus['VALUE'], $arAllowed)) {
                                            $arOptions[$idStatus] = $arStatus;
                                        }
                                    }?>
                                    <select name="book-status"
                                            id="blip-status-<?= $arBook['ID'] ?>"
                                            class="book-status form-control"<?= $selectDisabled ?>>
                                        <?if(!$arBook['UF_REQUEST_STATUS']){?>
                                            <option value="-">-</option>
                                        <?}?>
                                        <?foreach($arOptions as $idStatus => $arStatus){?>
                                            <?$selected = '';
                                            $attrCanceled = '';
                                            if($arStatus['VALUE'] == DIGITIZATION_STATUS_CANCELED) {
                                                $attrCanceled = ' data-canceled="Y"';
                                            }
                                            if($arBook['UF_REQUEST_STATUS'] == $idStatus) {
                                                $selected = ' selected';
                                            }?>
                                            <option value="<?= $idStatus ?>"<?= $selected ?><?= $attrCanceled ?>>
                                                <?= $arStatus['VALUE'] ?>
                                            </option>
                                        <?}?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="text-right">
                                        <button class="btn btn-default js-update-book<?= $selectDisabled ?>"
                                                data-status-book="#blip-status-<?= $arBook['ID'] ?>"
                                                data-ajax-url="<?= $arParams['AJAX_BOOK_EDIT'] ?>"
                                                data-id-book="<?= $arBook['ID'] ?>"
                                                class="js-update-book js-tooltip">
                                            <?= Loc::getMessage('DIG_PLAN_EDIT_T_CHANGE_STATUS') ?>
                                        </button>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="text-right">
                                        <button class="btn btn-default btn-comment btn-comment-<?= $arBook['ID'] ?>"
                                                data-id-book="<?= $arBook['ID'] ?>">Комментарий</button>
                                        <form action="<?=$arParams['AJAX_COMMENT_BOOK']?>" method="get"
                                              class="comment-form" id="comment-form-<?= $arBook['ID'] ?>"
                                              data-id-book="<?= $arBook['ID'] ?>">
                                            <p>
                                                <input type="hidden" name="DATA_AJAX" value="Y" />
                                                <input type="hidden" name="ID" value="<?=$arBook['ID']?>" />
                                                <input type="text"   name="COMMENT" value="<?=$arBook['UF_COMMENT']?>" />
                                                <input type="submit" value="Сохранить" class="btn btn-default" />
                                            </p>
                                        </form>
                                    </p>
                                </div>
                            </div>

                            <?if(!empty($arBook['DATA_EXALEAD'])) {?>
                                <div class="row text-right">
                                    <div class="col-xs-12">
                                        <a class="btn btn-default js-download-book"
                                           target="_blank"
                                           href="<?= $arParams['DOWNLOAD_PATH'] ?><?= $arBook['DATA_EXALEAD']['pdfLink'] ?>">
                                            <i class="fa fa-download"></i><?= Loc::getMessage('DIG_PLAN_EDIT_T_DOWNLOAD') ?>
                                        </a>
                                    </div>
                                </div>
                            <?}?>
                            <?if(count($arBook['DUPLICATE']) > 0){?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <?= Loc::getMessage('DIG_PLAN_EDIT_T_BOOK_IN_DIG_LIBRARY') ?>
                                        <ol>
                                            <?foreach($arBook['DUPLICATE'] as $iDupBook => $arDupBook) {?>
                                                <li>
                                                    <a target="_blank" href="<?= $arParams['SEF_FOLDER'] ?>?idplan=<?= $arResult['LIST_PLAN'][$arDupBook['UF_PLAN_ID']]['ID'] ?>">
                                                        <?= $arResult['LIST_PLAN'][$arDupBook['UF_PLAN_ID']]['UF_PLAN_NAME'] ?>
                                                        &laquo;<?= $arResult['LIST_LIBRARY'][$arDupBook['UF_LIBRARY']]['NAME'] ?>&raquo;
                                                    </a>
                                                </li>
                                            <?}?>
                                        </ol>
                                    </div>
                                </div>
                            <?}?>
                        </div>
                        <div class="col-xs-12"><hr></div>
                    </div>
                </div>
            <?}?>
        </div>
    </div>
<?}?>
<?$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $arResult['NAV'],
        "SEF_MODE" => "N",
    ),
    false
);?>
<?if(count($arResult['LIST_BOOKS']) > $arParams['COUNT_ON_PAGE']) {?>
    <div class="container-fluid"><hr></div>
<?}?>