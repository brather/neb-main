<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serialization;
use LbcBundle\Entity\Traits\CategoryCodeTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LbcMaindivision
 *
 * @Gedmo\Tree(type="closure")
 * @Gedmo\TreeClosure(class="LbcBundle\Entity\LbcMaindivisionClosure")
 * @ORM\Table(name="lbc_maindivision")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\ClosureTreeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class LbcMaindivision extends AbstractLbcCategory
{
    use CategoryCodeTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="IsCommon", type="integer", nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    public $iscommon;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="LbcMaindivision", inversedBy="children")
     * @ORM\JoinColumn(name="ParentId", referencedColumnName="Id", onDelete="CASCADE")
     * @Serialization\Groups({"details"})
     */
    public $parent;

    /**
     * @ORM\OneToMany(targetEntity="LbcMaindivision", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    public $children;



    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Set iscommon
     *
     * @param integer $iscommon
     *
     * @return LbcMaindivision
     */
    public function setIscommon($iscommon)
    {
        $this->iscommon = $iscommon;

        return $this;
    }

    /**
     * Get iscommon
     *
     * @return integer
     */
    public function getIscommon()
    {
        return $this->iscommon;
    }
}
