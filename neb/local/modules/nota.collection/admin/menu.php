<?php
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

global $USER;
if (!$USER->IsAdmin())
    return false;


return $aMenu;
?>