<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//*************************************
//show confirmation form
//*************************************
?>
<form action="<?=$arResult["FORM_ACTION"]?>" method="get">
<h3>
	<?echo GetMessage("subscr_title_confirm")?>
</h3>
<p>Вам отправлен код подтверждения <?echo $arResult["SUBSCRIPTION"]["DATE_CONFIRM"];?></p>
<div class="row naf">
	<div class="col-sm-4">
		<div class="form-group">
			<label>
				<em class="hint">*</em>
				<?echo GetMessage("subscr_conf_code")?>
			</label>
			<input type="text" name="CONFIRM_CODE" value="<?echo $arResult["REQUEST"]["CONFIRM_CODE"];?>" size="20" class="form-control" />
		</div>
		<!--p><?echo GetMessage("subscr_conf_date")?></p>
		<p><?echo $arResult["SUBSCRIPTION"]["DATE_CONFIRM"];?></p-->
	</div>
	<div class="col-sm-4">
		<?echo GetMessage("subscr_conf_note1")?> <a title="<?echo GetMessage("adm_send_code")?>" href="<?echo $arResult["FORM_ACTION"]?>?ID=<?echo $arResult["ID"]?>&amp;action=sendcode&amp;<?echo bitrix_sessid_get()?>"><?echo GetMessage("subscr_conf_note2")?></a>.
	</div>
</div>
<input type="submit" name="confirm" class="btn btn-primary" value="<?echo GetMessage("subscr_conf_button")?>" />
<input type="hidden" name="ID" value="<?echo $arResult["ID"];?>" />
<?echo bitrix_sessid_post();?>
</form>
<br />
