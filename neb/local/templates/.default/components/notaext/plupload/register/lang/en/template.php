<?php

$MESS['BROWSER_NOT_SUPPORTED'] = 'Ваш браузер не поддерживает загрузку файлов :-(';
$MESS['NO_FILE_SELECTED'] = 'Файл не выбран';
$MESS['SELECT_FILE'] = 'Выбрать файл';
$MESS['SELECT_FILES'] = 'Выбрать файлы';
$MESS['UPLOAD'] = 'Загрузить';

$MESS["PLUPLOAD_REGISTER_SET"] = 'Add a photo';
$MESS["PLUPLOAD_REGISTER_DROP"] = 'or put an image here manually';