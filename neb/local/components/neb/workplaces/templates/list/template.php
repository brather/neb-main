<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 */
?>
<ul class="ul-city-list">
<?foreach ($arResult['items'] as $city=>$arLibrary):?>
	<li>
    	<a><?=$city?></a>
    	<ul class="submenu2">
		    <?foreach ($arLibrary as $library):?>
		    	<li>
		    		<h5><?=$library['lib']['NAME']?></h5>

					<table>
					    <colgroup><col width="30%"><col width="70%"></colgroup>
					    <tbody>
					        <tr>
					            <td>Адрес</td>
					            <td><?=$library['lib']['PROPERTY_ADDRESS_VALUE']?></td>
					        </tr>
					        <tr>
					            <td>Режим работы</td>
					            <td><?=$library['lib']['PROPERTY_SCHEDULE_VALUE']?></td>
					        </tr>
					        <tr>
					            <td>Номер телефона</td>
					            <td><?=$library['lib']['PROPERTY_PHONE_VALUE']?></td>
					        </tr>
					        <tr>
					            <td>Cайт</td>
					            <td>
						            <a href="<?=$library['lib']['PROPERTY_URL_VALUE']?>" target="_blank">
						            	<?=$library['lib']['PROPERTY_URL_VALUE']?>
						            </a>
					            </td>
					        </tr>
					    </tbody>
					</table>
		        </li>
		    <?endforeach;?>
    	</ul>

<?endforeach;?>
</ul>

