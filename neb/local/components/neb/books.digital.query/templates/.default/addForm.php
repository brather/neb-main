<?	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<div class="b-addcomment">
	<form method="post" action="<?=$APPLICATION->GetCurPage();?>" class="b-form b-form_common b-formcomment" >
		<?=bitrix_sessid_post()?>
		<input type="hidden" name="action" value="addDigitizationQuery" />
		<h3><?=GetMessage('BOOKS_DIGITAL_QUERY_ADD_FORM_COMMENT');?></h3>
		<p><?=GetMessage('BOOKS_DIGITAL_QUERY_ADD_FORM_DESRIBE');?></p>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock ">

				<div class="field validate">
					<textarea   data-required="required" class="textarea" name="comment" id="settings10" data-minlength="2" data-maxlength="800"></textarea>
					<em class="error required"><?=GetMessage('BOOKS_DIGITAL_QUERY_ADD_FORM_REQ');?></em>
				</div>

			</div>
		</div>

		<p><?=GetMessage('BOOKS_DIGITAL_QUERY_ADD_FORM_FASTER');?></p>
		<div class="clearfix">
            <input type="hidden" name="UF_LIBRARY" value="<?php echo $arParams['UF_LIBRARY']?>">
			<button type="submit" value="1" class="formbutton small"><?=GetMessage('BOOKS_DIGITAL_QUERY_ADD_FORM_ADD');?></button>
			<a class="formbutton gray btrefuse small" href="#"><?=GetMessage('BOOKS_DIGITAL_QUERY_ADD_FORM_CANCEL');?></a>
		</div>
	</form>
</div>
