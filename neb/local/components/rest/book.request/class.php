<?php
CBitrixComponent::includeComponentClass('rest:rest.server');
CModule::IncludeModule('iblock');

/**
 * Class RestLibraryNebParty
 */
class RestBookRequest extends RestServer
{
    /**
     * @var array
     */
    protected $_params
        = array(
            'request_id'   => null,
            'confirm'      => null,
            'refuse'       => null,
            'view_price'   => null,
            'request_type' => null,
            'book_id'      => null,
            'limit'        => DEFAULT_PAGE_SIZE,
            'offset'       => 0,
        );

    /**
     * @var array
     */
    protected $_methodAllowGroups
        = array(
            'putAction' => array(
                13 => true,
                9  => true,
            ),
        );

    /**
     * @return $this
     */
    protected function _prepareParams()
    {
        $this->_params['confirm'] = filter_var(
            $this->_params['confirm'],
            FILTER_VALIDATE_BOOLEAN
        );
        $this->_params['refuse'] = filter_var(
            $this->_params['refuse'],
            FILTER_VALIDATE_BOOLEAN
        );
        $this->_params['request_id'] = (integer)$this->_params['request_id'];
        $this->_params['limit'] = (integer)$this->_params['limit'];
        $this->_params['offset'] = (integer)$this->_params['offset'];
        if (is_array($this->_params['book_id'])) {
            $this->_params['book_id'] = array_filter($this->_params['book_id']);
        }

        return $this;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function putAction()
    {
        if (empty($this->_params['request_id'])) {
            throw new Exception('Param "request_id" is empty!');
        }
        NebMainHelper::includeModule('nota.exalead');
        if (true === $this->_params['confirm']) {
            $recordId = \Nota\Exalead\LibraryBiblioCardTable::confirmRequest(
                $this->_params['request_id']
            );
            if (!$recordId) {
                throw new Exception('Request not confirmed!');
            }
            if ($recordId > 0) {
                $this->arResult['success'] = true;
            }
            $this->arResult['recordId'] = $recordId;
        } elseif (true === $this->_params['refuse']) {
            if (\Nota\Exalead\BiblioCardTable::refuseRequest(
                $this->_params['request_id']
            )
            ) {
                $this->arResult['success'] = true;
            }
        } else {
            $data = array_intersect_key(
                $this->_params,
                \Nota\Exalead\BiblioCardTable::getMap()
            );
            if (empty($data)) {
                throw new Exception('Empty data');
            }
            $updateResult = \Nota\Exalead\BiblioCardTable::update(
                $this->_params['request_id'],
                $data
            );
            if ($updateResult->getAffectedRowsCount() < 1) {
                throw new Exception('Cannot update request!');
            }
            $this->arResult['success'] = true;
        }

        return 200;
    }


    /**
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     */
    public function getAction()
    {
        NebMainHelper::includeModule('nota.exalead');
        $params = array(
            'filter' => array(),
            'limit'  => $this->_params['limit'],
            'offset' => $this->_params['offset'],
        );
        if ($this->_params['request_id'] > 0) {
            $params['filter']['Id'] = $this->_params['request_id'];
        }
        if (!empty($this->_params['request_type'])) {
            $params['filter']['request_type'] = $this->_params['request_type'];
        }
        if (!empty($this->_params['book_id'])) {
            $params['filter']['FullSymbolicId'] = $this->_params['book_id'];
        }
        $this->arResult['data']
            = \Nota\Exalead\BiblioCardTable::getList($params)
            ->fetchAll();

        return 200;
    }
}