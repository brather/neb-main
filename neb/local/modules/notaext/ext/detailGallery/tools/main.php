<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/prolog.php");

use Bitrix\Main\Loader;

Loader::includeModule('notaext');

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

use Bitrix\NotaExt\Ext\DetailGallery\Params as DGParams;

if (!check_bitrix_sessid() or !Bitrix\NotaExt\Ext\DetailGallery\Access::check())
    die(GetMessage("ACCESS_DENIED"));

$EDITOR_ID = htmlspecialcharsEx($_REQUEST['EDITOR_ID']);

?>
<script type="text/javascript">
    $(function () {
        if ($("#tabs-detail-gallery").tabs('instance'))
            $("#tabs-detail-gallery").tabs('destroy');

        $("#tabs-detail-gallery").tabs({active: 0});
        $("#tabs-detail-gallery").tabs("disable", "#tabs-edit");
    });
</script>
<div id="tabs-detail-gallery">
    <ul>
        <li><a href="#tabs-upload"><?= Loc::getMessage('EDBG_TAB_UPLOAD_TITLE'); ?></a></li>
        <li><a href="#tabs-search"><?= Loc::getMessage('EDBG_TAB_SEARCH_TITLE'); ?></a></li>
        <li><a href="#tabs-edit"><?= Loc::getMessage('EDBG_TAB_EDIT_TITLE'); ?></a></li>
    </ul>
    <div id="tabs-upload">
        <?
        $arParamsComponent = array(
            'FILE_TYPES' => DGParams::getParam('upload_ext'),
            'MAX_FILE_SIZE' => DGParams::getParam('upload_max_size_file'),
            'FILES_FIELD_NAME' => 'detail_galleryplupload_files',
            'CLEANUP_DIR' => true,
            'THUMBNAIL_HEIGHT' => 140,
            'THUMBNAIL_WIDTH' => 200,
            'IMG_QUALITY' => DGParams::getParam('upload_quality_file'),
        );

        $APPLICATION->IncludeComponent('notaext:plupload', 'adminDetailGallery', $arParamsComponent);
        ?>
    </div>
    <div id="tabs-search">
        <script type="text/javascript">
            $(function () {
                /* autocomplete */
                $(".search-form .field input").autocomplete({
                    source: "/local/tools/notaext/detailGallery/tags.php?search=y",
                    minLength: 2,
                    autoFocus: true,
                    select: function (event, ui) {
                        this.value = ui.item.text;
                        $(".search-form form").submit();
                        return false;
                    }
                })
                    .autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li>").append("<a>" + item.text + "</a>").appendTo(ul);
                }
            });
        </script>

        <div class="search-form">
            <form action="/local/tools/notaext/detailGallery/search.php">
                <span class="field"><input type="text" name="qsearch" value=""/></span>
                <span class="button"><input type="submit" name="s"
                                            value="<?= Loc::getMessage('EDBG_SEARCH_BUTTON'); ?>"/></span>
                <span class="selection hidden"><input type="button" name="select"
                                                      rel="<?= Loc::getMessage('EDBG_CHOOSE_BUTTON'); ?>"
                                                      value=""/></span>
            </form>
        </div>
        <div class="search-result"></div>
    </div>
    <div id="tabs-edit">
        <script type="text/javascript">

            $(function () {
                $('#tabs-edit #dgInsert').click(function () {
                    var param = $('#tabs-edit select#show_template').val();
                    var strParam = param;
                    param = eval('(' + param + ')');
                    var n = $('.edit-items-list li.item').length;
                    if (n <= 0) {
                        alert('<?=Loc::getMessage('EDBG_MESS_ADD_PHOTO');?>');
                        return false;
                    }

                    if (param.one_photo == true) {
                        if (n > 1) {
                            if (!confirm('<?=Loc::getMessage('EDBG_MESS_ONE_PHOTO');?>')) {
                                return false;
                            }
                        }
                    }

                    var arInsert = [];
                    var firstImgId = 0;
                    $('.edit-items-list li.item').each(function () {
                        if (firstImgId <= 0)
                            firstImgId = $(this).attr('id');
                        arInsert.push({ID: $(this).attr('id'), PARAM: $(this).attr('rel')});
                    });

                    var arInsertB64 = btoa('(' + JSON.stringify(arInsert) + ')');

                    var paramsImg = {};

                    if (arInsert[0].PARAM.length > 0) {
                        var firstImgParams = eval(atob(arInsert[0].PARAM));
                        paramsImg = firstImgParams.Data;
                    }

                    paramsImg.wResize = param.width;
                    paramsImg.hResize = param.height;
                    paramsImg.exact_size = param.exact;

                    paramsImg.id = firstImgId;
                    paramsImg.sessid = BX.bitrix_sessid();

                    $.getJSON("/local/tools/notaext/detailGallery/img_crop.php?prev=y", paramsImg, function (json) {
                        srcImg = json.img;

                        var text = '<img class="gallery-detail-object-v2" id="dg' + (Math.floor(Math.random() * 1000) + 1) + '" src="' + srcImg + '" align_rem="' + param.align + '" style="width:' + param.width + 'px;" width="' + param.width + '" rel="' + btoa('(' + JSON.stringify(param) + ')') + '" alt="' + arInsertB64 + '" editor="<?=$EDITOR_ID?>"/>';

                        <?
                        if(COption::GetOptionString("fileman", "use_editor_3") == 'Y')
                        {
                        ?>
                        /* new editor */
                        window.BXHtmlEditor.Get('<?=$EDITOR_ID?>').selection.GetSelection().deleteFromDocument();
                        window.BXHtmlEditor.Get('<?=$EDITOR_ID?>').selection.InsertHTML(text);
                        <?
                        }else{
                        ?>
                        GLOBAL_pMainObj['<?=$EDITOR_ID?>'].insertHTML(text);
                        <?
                        }
                        ?>

                        $('body').trigger('WindowDetailGalleryClose');
                    });


                });
                <?
                # редактирование галереи
                $arRelParam['template'] = '';
                if (!empty($_REQUEST['targetRel']))
                    $arRelParam = DGParams::decode($_REQUEST['targetRel']);

                if(!empty($_REQUEST['targetAlt']))
                {
                $arItem = DGParams::decode($_REQUEST['targetAlt']);

                if(!empty($arItem))
                {
                foreach ($arItem as &$item) {
                    $arFilter = array(
                        'IBLOCK_ID' => Bitrix\NotaExt\Ext\DetailGallery\Params::getParam('save_ib'),
                        'ID' => $item['ID'],
                        '!DETAIL_PICTURE' => false
                    );

                    $arSelect = array('ID', 'DETAIL_PICTURE', 'skip_other');
                    $resResult = Bitrix\NotaExt\Iblock\Element::getList($arFilter, 1, $arSelect);

                    $arCropParams = array();
                    if (!empty($item['PARAM'])) {
                        $arParam = DGParams::decode($item['PARAM']);
                        $arCropParams = $arParam['Data'];

                    }
                    $arCropParams['wResize'] = 200;
                    $arCropParams['hResize'] = 140;

                    $img = Bitrix\NotaExt\ImgConvert::Crop($resResult['ITEM']['DETAIL_PICTURE'], $arCropParams, '/upload/resize_cache_dg');
                    $item['PREVIEW_PICTURE'] = $img;
                }
                ?>
                initEditTab(<?=Cutil::PhpToJSObject($arItem)?>);
                $("#tabs-detail-gallery").tabs({active: 2});
                $("#tabs-detail-gallery").tabs("enable", "#tabs-edit");
                $('a[href="#tabs-edit"]').click();
                <?
                }
                }
                ?>
            });
        </script>
        <div class="show-template">
            <div class="title"><?= Loc::getMessage('EDBG_INSERT_TEMPLATE'); ?></div>
            <div class="field">
                <?
                $show_component = DGParams::getParam('show_component');
                if (!empty($show_component)) {
                    $arComponentTemplates = CComponentUtil::GetTemplatesList($show_component);

                    if (!empty($arComponentTemplates)) {
                        foreach ($arComponentTemplates as &$template) {
                            if (!empty($template['DESCRIPTION'])) {
                                $arDesc = Cutil::JsObjectToPhp($template['DESCRIPTION']);
                                $template['SORT'] = intval($arDesc['SORT']);
                            } else
                                $template['SORT'] = 0;
                        }
                        unset($template);
                        $arComponentTemplates = Bitrix\NotaExt\Sort::SortFieldTypeNum($arComponentTemplates, 'SORT', 'asc');
                        ?>
                        <select name="show_template" id="show_template">
                            <?
                            foreach ($arComponentTemplates as $template) {
                                ?>
                                <option value="<?= $template['DESCRIPTION'] ?>" <?= $arRelParam['template'] == $template['NAME'] ? 'selected="selected"' : '' ?>><?= $template['TITLE'] ?></option>
                                <?
                            }
                            ?>
                        </select>
                        <input type="button" id="dgInsert" name="dgInsert"
                               value="<?= Loc::getMessage('EDBG_INSERT_BUTTON'); ?>"/>
                        <?
                    }
                }
                ?>
            </div>
        </div>
        <div class="head-title"><?= Loc::getMessage('EDBG_DRAG_PHOTO'); ?></div>
        <ul class="edit-items-list">
        </ul>
    </div>
</div>