<?php

/**
 * Импорт координат библиотек
 */
class ImportGeocode
{
    // импорт координат библиотек
    public static function importLibsGeocode($iMaxExecTime = 0, $sYandexKey = null, $sGoogleKey = null, $bClearLog = false) {

        if (!$iMaxExecTime) {
            $iMaxExecTime = ini_get('max_execution_time');
            if ($iMaxExecTime > 30)
                $iMaxExecTime = $iMaxExecTime - 10;
            elseif ($iMaxExecTime > 5)
                $iMaxExecTime = $iMaxExecTime - 1;
        }

        self::importLibsModules();
        self::checkLogTable();

        // очистка таблицы логов
        if ($bClearLog)
            self::clearLogTable();

        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        // выборка всех библиотек без координат
        $libraryData = $libraryEntity::getList(array(
            "filter" => array('UF_POS' => ''),
            "order"  => array("ID"),
            "select" => array("ID", 'UF_RGB_ID', 'UF_FULL_NAME', 'UF_ADRESS'),
        ));
        while ($arItem = $libraryData->Fetch())
            $arNetLibs[$arItem['ID']] = $arItem;
        unset($libraryData, $arItem);
        if (empty($arNetLibs))
            return false;

        $iStartTime = microtime(true);

        // обновление библиотек
        foreach ($arNetLibs as $arNetLib) {

            $arYandex   = GeocodeYandex::getByAddress($arNetLib['UF_ADRESS'], $sYandexKey);
            $arGoogle   = GeocodeGoogle::getByAddress($arNetLib['UF_ADRESS'], $sGoogleKey);
            $arLocation = self::getLocation($arYandex, $arGoogle);

            if (!empty($arLocation['SEARCHER']) && !empty($arLocation['LOCATION'])) {
                $obResult = $libraryEntity::update($arNetLib['ID'], array('UF_POS' => $arLocation['LOCATION']));
                if ($obResult->isSuccess()) {
                    $sStatus = 'SUCCESS';
                    $sInfo   = $arLocation['SEARCHER'];
                } else {
                    $sStatus = 'ERROR';
                    $sInfo = implode(', ', $obResult->getErrorMessages());
                }
            } else {
                $sStatus = 'ERROR';
                $sInfo   = $arLocation['ERROR'];
            }
            
            $arYandexObject = $arYandex['objects'][0];
            $arGoogleObject = $arGoogle['objects'][0];

            $arStatLog = array(
                'ID'         => 0,
                'DATE'       => 'now()',
                'RGB_ID'     => $arNetLib['UF_RGB_ID'],
                'NAME'       => $arNetLib['UF_FULL_NAME'],
                'ADDRESS'    => $arNetLib['UF_ADRESS'],
                'STATUS'     => $sStatus,
                'INFO'       => $sInfo,
                'Y_LOCATION' => self::getLocationString($arYandexObject['location']),
                'Y_STATUS'   => $arYandexObject['precision_info'],
                'Y_TYPE'     => $arYandexObject['kind_info'],
                'G_LOCATION' => self::getLocationString($arGoogleObject['location']),
                'G_STATUS'   => strtolower($arGoogle['status']) != 'ok'
                    ? $arGoogle['status_info'] : $arGoogleObject['location_info'],
                'G_TYPE'     => $arGoogleObject['types']
            );
            $strSql = self::buildInsertQuery('neb_log_import_libs_geocode', $arStatLog);
            \Bitrix\Main\Application::getConnection()->query($strSql);

            // ограничение времени выполнения скрипта
            if ($iMaxExecTime && round(microtime(true) - $iStartTime, 2) >= $iMaxExecTime) {
                LocalRedirect($_SERVER['SCRIPT_NAME'].'?'
                    .DeleteParam(array("step")).'&step='.(intval($_REQUEST['step']) + 1));
                die();
            }
        }

        unset($libraryHLBlock, $libraryEntity);
    }

    // импорт координат городов
    public static function importCityGeocode($iMaxExecTime = 0, $sYandexKey = null, $sGoogleKey = null) {

        if (!$iMaxExecTime) {
            $iMaxExecTime = ini_get('max_execution_time');
            if ($iMaxExecTime > 30)
                $iMaxExecTime = $iMaxExecTime - 10;
            elseif ($iMaxExecTime > 5)
                $iMaxExecTime = $iMaxExecTime - 1;
        }

        self::importLibsModules();
        self::checkLogTable();

        $libraryCityHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY_CITY)->fetch();
        $libraryCityEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryCityHLBlock)->getDataClass();

        // выборка всех городов библиотек без координат
        $libraryData = $libraryCityEntity::getList(array(
            "filter" => array('UF_POS' => ''),
            "order"  => array("ID"),
            "select" => array("ID", 'UF_CITY_NAME'),
        ));
        while ($arItem = $libraryData->Fetch())
            $arNetLibsCity[$arItem['ID']] = $arItem;
        unset($libraryData, $arItem);
        if (empty($arNetLibsCity))
            return false;

        $iStartTime = microtime(true);

        // обновление библиотек
        foreach ($arNetLibsCity as $arCity) {

            $arYandex   = GeocodeYandex::getByAddress($arCity['UF_CITY_NAME'], $sYandexKey);
            $arGoogle   = GeocodeGoogle::getByAddress($arCity['UF_CITY_NAME'], $sGoogleKey);
            $arLocation = self::getLocation($arYandex, $arGoogle);

            if (!empty($arLocation['SEARCHER']) && !empty($arLocation['LOCATION']))
                $libraryCityEntity::update($arCity['ID'], array('UF_POS' => $arLocation['LOCATION']));

            // ограничение времени выполнения скрипта
            if ($iMaxExecTime && round(microtime(true) - $iStartTime, 2) >= $iMaxExecTime) {
                LocalRedirect($_SERVER['SCRIPT_NAME'].'?'
                    .DeleteParam(array("step")).'&step='.(intval($_REQUEST['step']) + 1));
                die();
            }
        }

        unset($libraryHLBlock, $libraryEntity);
    }

    // получает расшифровку кода статуса ответа
    private static function getLocation($arYandex, $arGoogle) {

        $arYandexObject = $arYandex['objects'][0];
        $arGoogleObject = $arGoogle['objects'][0];

        // утановка веса точности адреса Яндекс
        switch (strtolower($arYandexObject['precision'])) {
            case 'exact':  $iYandex = 1; break;
            case 'number': $iYandex = 3; break;
            case 'near':   $iYandex = 4; break;
            case 'range':  $iYandex = 5; break;
            case 'street': $iYandex = 7; break;
            case 'other':  $iYandex = 9; break;
            default:       $iYandex = 5;
        }

        // утановка веса точности адреса Google
        switch (strtoupper($arGoogleObject['location_type'])) {
            case 'ROOFTOP':            $iGoogle = 2;  break;
            case 'RANGE_INTERPOLATED': $iGoogle = 6;  break;
            case 'GEOMETRIC_CENTER':   $iGoogle = 8;  break;
            case 'APPROXIMATE':        $iGoogle = 10; break;
            default:                   $iGoogle = 5;
        }

        // если Google сообщил что результаты не найдены
        if (strtolower($arGoogle['status']) != 'ok')
            $iGoogle = 400;

        // проверка на принадлежность адреса России
        if (!empty($arYandexObject['full_address']['CountryNameCode'])
            && strtolower($arYandexObject['full_address']['CountryNameCode']) != 'ru')
            $iYandex = 100;
        if (!empty($arGoogleObject['short_address']['country'])
            && strtolower($arGoogleObject['short_address']['country']) != 'ru')
            $iGoogle = 100;

        // вычисление результата
        if ($iYandex == $iGoogle && $iYandex == 100)
            $arResult = array('ERROR' => 'Координаты объекта не соответсвтуют России!');
        elseif ($iYandex <= $iGoogle)
            $arResult = array('SEARCHER' => 'YANDEX', 'LOCATION' => self::getLocationString($arYandexObject['location']));
        else
            $arResult = array('SEARCHER' => 'GOOGLE', 'LOCATION' => self::getLocationString($arGoogleObject['location']));

        return $arResult;
    }

    // подключает библиотеки и модули
    private static function importLibsModules()
    {
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/helpers/geo/GeocodeYandex.php");
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/helpers/geo/GeocodeGoogle.php");

        \Bitrix\Main\Loader::IncludeModule("highloadblock");
    }

    // проверяет наличие таблицы логов
    private static function checkLogTable() {

        //\Bitrix\Main\Application::getConnection()->query('DROP TABLE IF EXISTS neb_log_import_libs_geocode;');
        $sQuery = '
            CREATE TABLE IF NOT EXISTS neb_log_import_libs_geocode (
              ID int NOT NULL AUTO_INCREMENT,
              DATE timestamp NOT NULL,
              RGB_ID int NOT NULL,
              NAME varchar(5000) NOT NULL,
              ADDRESS varchar(255) NOT NULL,
              STATUS varchar(255) DEFAULT NULL,
              INFO  varchar(5000) DEFAULT NULL,
              Y_LOCATION varchar(255) DEFAULT NULL,
              Y_STATUS varchar(255) DEFAULT NULL,
              Y_TYPE varchar(255) DEFAULT NULL,
              G_LOCATION varchar(255) DEFAULT NULL,
              G_STATUS varchar(255) DEFAULT NULL,
              G_TYPE varchar(255) DEFAULT NULL,
              PRIMARY KEY (ID)
            )
            ENGINE = INNODB;
        ';
        \Bitrix\Main\Application::getConnection()->query($sQuery);
    }

    // очистка старых логов
    private static function clearLogTable($iMaxTime = 30) {
        $sQuery = 'DELETE FROM neb_log_import_libs_geocode';
        if ($iMaxTime)
            $sQuery .= ' WHERE DATE <=' . (strtotime('now') - $iMaxTime * 60);
        \Bitrix\Main\Application::getConnection()->query($sQuery);
    }

    // ========================================== СЛУЖЕБНЫЕ МЕТОДЫ ===================================================

    // собирает insert-запрос
    private static function buildInsertQuery($sTable, $arFields) {

        $sQuery = "INSERT INTO `".$sTable."` (";
        foreach ($arFields as $k => $v)
            if ($k == 'ID')
                $sQuery .= $k;
            else
                $sQuery .= ', '.$k;
        $sQuery .= ") VALUES (";
        foreach ($arFields as $k => $v)
            if ($k == 'ID')
                $sQuery .= "'0'";
            elseif ($k == 'DATE')
                $sQuery .= ", ".$v;
            else
                $sQuery .= ", '".$v."'";
        $sQuery .= ")";

        return $sQuery;
    }

    // преобразует массив координат в строку для сайта
    private static function getLocationString($arLocation) {
        return trim($arLocation['lng'].' '.$arLocation['lat']);
    }
}