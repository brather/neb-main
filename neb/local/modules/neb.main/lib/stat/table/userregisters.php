<?php
/**
 * User: agolodkov
 * Date: 18.01.2016
 * Time: 12:23
 */

namespace Neb\Main\Stat\Table;


use Bitrix\Main\Entity\DataManager;

class UserRegistersTable extends DataManager
{
    /**
     * @return string
     */
    public static function getConnectionName()
    {
        return 'stat';
    }


    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'user_registers';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'user_id'   => array(
                'data_type' => 'integer',
                'primary'   => true,
            ),
            'ip'        => array(
                'data_type' => 'integer'
            ),
            'timestamp' => array(
                'data_type' => 'datetime',
            ),
        );
    }
}