<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aMenu = array();
if (\Bitrix\Main\Loader::includeModule("intervolga.remoteuserclient"))
{
	if (\Intervolga\RemoteUserClient\Rights::canWrite())
	{
		/**
		 * @var array $aMenu
		 */
		$sModuleName = basename(dirname(dirname(__FILE__)));

		$aMenu = array(
			"parent_menu" => "global_menu_settings",
			"section" => $sModuleName,
			"sort" => 50,
			"text" => Loc::getMessage("intervolga.remoteuserclient.MENU_TITLE"),
			"title" => "",
			"icon" => "fileman_sticker_icon",
			"page_icon" => "",
			"items_id" => $sModuleName . "_items",
			"more_url" => array(
				"/bitrix/admin/" . $sModuleName . "_upload.php",
				"/bitrix/admin/" . $sModuleName . "_upload_fields.php",
			),
			"items" => array(
				array(
					'text' => Loc::getMessage("intervolga.remoteuserclient.MENU_UPLOAD_FIELDS"),
					'url' => "/bitrix/admin/" . $sModuleName . "_upload_fields.php",
					'module_id' => $sModuleName,
				),
				array(
					'text' => Loc::getMessage("intervolga.remoteuserclient.MENU_UPLOAD"),
					'url' => "/bitrix/admin/" . $sModuleName . "_upload.php",
					'module_id' => $sModuleName,
				),
			),
		);
	}
}

return $aMenu;