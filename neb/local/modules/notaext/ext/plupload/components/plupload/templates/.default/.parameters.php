<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
#pre($arCurrentValues,1);
$arTemplateParameters["UPLOAD_AUTO_START"] = array(
    "NAME" => 'Автоматический старт заугрзки файла',
    "TYPE" => "CHECKBOX",
    "MULTIPLE" => "N",
    "DEFAULT" => "N",
);

$arTemplateParameters["UNIQUE_NAMES"] = array(
    "NAME" => 'Задавать уникальное имя для файла',
    "TYPE" => "CHECKBOX",
    "MULTIPLE" => "N",
    "DEFAULT" => "N",
);
$arTemplateParameters["RESIZE_IMAGES"] = array(
    "NAME" => 'Уменшать графический файл',
    "TYPE" => "CHECKBOX",
    "MULTIPLE" => "N",
    "DEFAULT" => "N",
    "REFRESH" => "Y"
);


if ($arCurrentValues['RESIZE_IMAGES'] == 'Y') {
    $arTemplateParameters["RESIZE_WIDTH"] = array(
        "NAME" => 'Ширина',
        "TYPE" => "STRING",
        "DEFAULT" => "800",
    );
    $arTemplateParameters["RESIZE_HEIGHT"] = array(
        "NAME" => 'Высота',
        "TYPE" => "STRING",
        "DEFAULT" => "600",
    );
    $arTemplateParameters["RESIZE_CROP"] = array(
        "NAME" => 'Обрезать',
        "TYPE" => "CHECKBOX",
        "MULTIPLE" => "N",
        "DEFAULT" => "N",
    );


    $arTemplateParameters["THUMBNAIL_WIDTH"] = array(
        "NAME" => 'Ширина превью',
        "TYPE" => "STRING",
        "DEFAULT" => "200",
    );
    $arTemplateParameters["THUMBNAIL_HEIGHT"] = array(
        "NAME" => 'Высота превью',
        "TYPE" => "STRING",
        "DEFAULT" => "200",
    );

    $arQuality = array();
    for ($i = 10; $i <= 100; $i = $i + 10) $arQuality[$i] = $i . '%';

    $arTemplateParameters["RESIZE_QUALITY"] = array(
        "NAME" => 'Качество',
        "TYPE" => "LIST",
        "DEFAULT" => "80",
        "VALUES" => $arQuality
    );
}
