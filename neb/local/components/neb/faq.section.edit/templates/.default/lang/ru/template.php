<?php
$MESS["NAME_SECTION"] = "Название раздела: ";
$MESS["INPUT_NAME_SECTION"] = "название раздела...";
$MESS["ADD_SECTION"] = "Добавить";
$MESS["UPDATE_SECTION"] = "Сохранить";
$MESS["DELETE_SECTION"] = "Удалить раздел";
$MESS["BACK_TO_LIST"] = "К списку разделов";
$MESS["ACTIVE_SECTION"] = "Раздел активен";
$MESS["FAQ_QUESTION_DELETE_SECTION"] = "Удалить раздел?";
$MESS["SORT_SECTION"] = "Индекс сортировки: ";