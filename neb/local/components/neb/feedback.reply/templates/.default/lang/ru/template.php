<?php

$MESS['FEEDBACK_REPLY_DATE_CREATE'] = 'Дата обращения';
$MESS['FEEDBACK_REPLY_DATE_CLOSE']  = 'Дата ответа';
$MESS['FEEDBACK_REPLY_STATUS']      = 'Статус';
$MESS['FEEDBACK_REPLY_THEME']       = 'Тема';
$MESS['FEEDBACK_REPLY_BOOK']        = 'Книга';
$MESS['FEEDBACK_REPLY_MESSAGE']     = 'Сообщение';
$MESS['FEEDBACK_REPLY_ANSWER']      = 'Ответ';
$MESS['FEEDBACK_REPLY_FILE']        = 'Файл';

$MESS['FEEDBACK_REPLY_TITLE']       = 'Ответ на заявку №';
$MESS['FEEDBACK_REPLY_SUBMIT']      = 'Сохранить и отправить';
$MESS['FEEDBACK_REPLY_SAVE']        = 'Сохранить';