<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<section class="innersection innerwrapper clearfix">
    <div class="b-registration rel">

        <h2 class="mode">Вход</h2>
        <div class="b-form b-form_common b-regform">
            <hr/>
            <div class="fieldrow nowrap radiolist">
                <div class="fieldcell iblock mt10">
                    <div class="field validate">
                        <div class="b-radio">
                            <a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['simple']?>" title="<?=Loc::getMessage('SPECIAL_REG_SIMPLE');?>"><?=Loc::getMessage('SPECIAL_REG_SIMPLE');?></a>
                        </div>
                        <div class="b-radio">
                            <a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['full']?>" title="<?=Loc::getMessage('SPECIAL_REG_STANDART');?>"><?=Loc::getMessage('SPECIAL_REG_STANDART');?></a>
                        </div>
                        <div class="b-radio">
                            <a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['rgb']?>" title="<?=Loc::getMessage('SPECIAL_REG_RGB');?>"><?=Loc::getMessage('SPECIAL_REG_RGB');?></a>
                        </div>
                        <!--<div class="b-radio">
                            <a href="/special/registration/esia/" title="<?/*=Loc::getMessage('SPECIAL_REG_ESIA');*/?>"><?/*=Loc::getMessage('SPECIAL_REG_ESIA');*/?></a>
                        </div>-->
                    </div>
                </div>
            </div>
            <hr/>
        </div>
    </div><!-- /.b-registration-->
</section>
