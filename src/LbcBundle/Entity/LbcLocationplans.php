<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serialization;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LbcLocationplans
 *
 * @Gedmo\Tree(type="closure")
 * @Gedmo\TreeClosure(class="LbcBundle\Entity\LbcLocationplansClosure")
 * @ORM\Table(name="lbc_locationplans")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\ClosureTreeRepository")
 */
class LbcLocationplans extends AbstractLbcCategory
{

    /**
     * @var integer
     *
     * @ORM\Column(name="MainDivisionId", type="integer", nullable=false)
     */
    public $maindivisionid;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="LbcLocationplans", inversedBy="children")
     * @ORM\JoinColumn(name="parentid", referencedColumnName="Id", onDelete="CASCADE")
     */
    public $parent;

    /**
     * @ORM\OneToMany(targetEntity="LbcLocationplans", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    public $children;

    /**
     * Set maindivisionid
     *
     * @param integer $maindivisionid
     *
     * @return LbcLocationplans
     */
    public function setMaindivisionid($maindivisionid)
    {
        $this->maindivisionid = $maindivisionid;

        return $this;
    }

    /**
     * Get maindivisionid
     *
     * @return integer
     */
    public function getMaindivisionid()
    {
        return $this->maindivisionid;
    }
}
