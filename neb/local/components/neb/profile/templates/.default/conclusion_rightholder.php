<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->SetTitle('Заявка на заключение договора правообладателя');
?>
<section class="innersection innerwrapper clearfix ">
    <?$APPLICATION->IncludeComponent(
        "neb:rightholder.contracts.conclusion",
        "",
        array(
            'LIST_URL' => $arParams['SEF_FOLDER']
        ),
        $component
    );?>
</section>