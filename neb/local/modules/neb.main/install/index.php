<?

class neb_main extends CModule
{
    var $MODULE_ID = 'neb.main';
    var $MODULE_VERSION = '0.0.1';
    var $MODULE_VERSION_DATE = '2016-01-01 00:00:00';
    var $MODULE_NAME = 'Neb Base';
    var $MODULE_DESCRIPTION = 'Базовый модуль НЭБ';
    var $MODULE_GROUP_RIGHTS = 'N';
    var $MODULE_HOLDER;

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . '/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = 'Base Neb';
        $this->MODULE_DESCRIPTION = 'Базовый модуль НЭБ.РФ';
        $this->PARTNER_NAME = 'NEB development team';
        $this->PARTNER_URI = 'http://НЭБ.РФ';
        $moduleHolder = 'local';
        $this->MODULE_HOLDER = $moduleHolder;
    }

    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);

        return true;
    }

    function DoUninstall()
    {
        UnRegisterModule($this->MODULE_ID);

        return true;
    }
}
