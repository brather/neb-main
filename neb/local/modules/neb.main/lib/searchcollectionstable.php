<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 05.05.2017
 * Time: 15:37
 */

namespace Neb\Main;

use \Bitrix\Main\Entity\DataManager;

/**
 * Class SearchCollectionsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> NAME string optional
 * <li> NAME_EN string optional
 * <li> COUNT int optional
 * <li> SORT int optional default 500
 * </ul>
 *
 * @package Bitrix\Search
 **/

class SearchCollectionsTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'neb_search_collections';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'NAME' => array(
                'data_type' => 'text',
            ),
            'NAME_EN' => array(
                'data_type' => 'text',
            ),
            'COUNT' => array(
                'data_type' => 'integer',
            ),
            'SORT' => array(
                'data_type' => 'integer',
            ),
            'ACTIVE' => array(
                'data_type' => 'boolean',
                'values' => array('N', 'Y'),
            ),
            'SELECT' => array(
                'data_type' => 'boolean',
                'values' => array('N', 'Y'),
            ),
        );
    }
}