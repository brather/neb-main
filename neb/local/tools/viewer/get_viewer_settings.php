<?php

/**
 * Получение настроек для нового просмотровщика
 */

define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Config\Option,
    \Neb\Main\Helper\MainHelper;

$arViewerOptionsEx = [
    'viewer_version'                      => '',
    'viewer_link'                         => '',
    'viewer_ex_warning_show'              => '',
    'viewer_ex_warning_text'              => '',
    'viewer_ex_book_percent'              => '',
    'viewer_ex_library_window_show'       => '',
    'viewer_ex_library_window_text'       => '',
    'viewer_ex_library_window_period'     => '',
    'viewer_ex_library_window_interval'   => '',
    'viewer_ex_library_window_grow'       => '',
    'viewer_ex_library_window_grow_speed' => '',
    'viewer_ex_books_show'                => '',
    'viewer_ex_message'                   => '',

    'viewer_ex_analysis_period'           => '',
    'viewer_ex_max_open_page_count'       => '',
    'viewer_ex_identification'            => '',
    'viewer_ex_block_user'                => '',
    'viewer_ex_block_computer'            => '',
    'viewer_ex_block_workplace'           => '',

    'viewer_ex_watermark_email'           => '',
    'viewer_ex_watermark_date'            => '',
    'viewer_ex_watermark_workplace'       => '',
    'viewer_ex_watermark_position'        => '',
    'viewer_ex_watermark_color'           => '',
];

/**
 * Силище-костылище для просмотровщика: там ограничение на количество в 13 параметров,
 * но для сервиса выдачи нужно отдавать абсолютно все параметры
 */
$i = 0;
foreach ($arViewerOptionsEx as $k => &$v) {

    if ($i < 13 || $_REQUEST['full'] == 'Y') {
        $v = Option::get('nota.exalead', $k);
    } else  {
        unset($arViewerOptionsEx[$k]);
    }

    $i++;
}

MainHelper::showJson($arViewerOptionsEx);