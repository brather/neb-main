<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Neb\Main\Helper\DbHelper,
    \Neb\Main\Helper\MainHelper;

Loc::loadLanguageFile(__FILE__);
$context = Application::getInstance()->getContext();
$arRequest = $context->getRequest()->toArray();
$arServer = $context->getServer()->toArray();
$documentRoot = $arServer['DOCUMENT_ROOT'];
$arData = array();

if($arRequest['file']) {
    $file = $arRequest['file'];
    $arPathInfo = pathinfo($file);
    if(file_exists($file)) {
        $connection = DbHelper::getLibraryConnection();
        $sql = "UPDATE tbl_common_biblio_card SET pdfLink='', UpdatingDateTime='" . date('Y-m-d H:i:s') . "' WHERE FullSymbolicId='" . $arRequest['addParamOriginalName'] . "';";
        if($connection->query($sql)){
            if(unlink($file)) {
                $arData['MESS'] = 'success';
            }
        } else {
            $arData['MESS'] = Loc::getMessage('DELETE_FILE_AJAX_ERROR_NOT_CONNECTION_TO_DB');;
        }
    } else {
        $arData['MESS'] = Loc::getMessage('DELETE_FILE_AJAX_FILE_NOT_FOUND_1') . $arPathInfo['basename'] . Loc::getMessage('DELETE_FILE_AJAX_FILE_NOT_FOUND_2');
    }
}

MainHelper::showJson($arData);