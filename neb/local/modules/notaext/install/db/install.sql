CREATE TABLE IF NOT EXISTS `n_ext_notaext` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EXT_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `INSTALL` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='В таблице хранится информация об установленных расширениях' AUTO_INCREMENT=1 ;
