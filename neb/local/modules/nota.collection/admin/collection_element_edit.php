<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!CModule::IncludeModule('nota.collection')) {
    echo CAdminMessage::ShowMessage(Loc::getMessage('NOTA_COLLECTION_ACCESS_DENIED'));
    return false;
}

use Nota\Collection\ElementsTable;

$aTabs = array();
$aTabs[] = array(
    "DIV" => "edit1",
    'TAB' => Loc::getMessage('NOTA_COLLECTION_EDIT'),
    'TITLE' => Loc::getMessage('NOTA_COLLECTION_EDIT')

);

$tabControl = new CAdminTabControl('tabControl', $aTabs);

$arErrors = array();

if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && check_bitrix_sessid()) {
    $arFields = Array();
    $arFields["BOOK_ID"] = $UF_EXALEAD_LINK;
    $arFields["SORT"] = $SORT;
    $arFields["COLLECTION_ID"] = $COLLECTION_ID;

    if (strlen($ID) > 0) {
        $res = ElementsTable::update($ID, $arFields);
    } else {
        $res = ElementsTable::add($arFields);
        $ID = $res->getId();
    }

    if (!$res->isSuccess())
        $errors = $res->getErrorMessages();
    else {
        if (strlen($apply) <= 0)
            LocalRedirect('collection_elements.php?COLLECTION_ID=' . $COLLECTION_ID);
        else
            LocalRedirect($APPLICATION->GetCurPage() . "?lang=" . $lang . "&ID=" . UrlEncode($ID) . "&COLLECTION_ID=" . $COLLECTION_ID . "&" . $tabControl->ActiveTabParam());
    }

}

if ($COLLECTION_ID > 0) {
    $rsSect = Nota\Collection\CollectionTable::getById($COLLECTION_ID);
    if ($arSect = $rsSect->Fetch()) {
        $GLOBALS['adminChain']->AddItem(array(
            "TEXT" => htmlspecialcharsex($arSect['NAME']),
            "LINK" => htmlspecialcharsbx('collection_elements.php?COLLECTION_ID=' . $COLLECTION_ID),
        ));
    }
}

if (strlen($ID) > 0)
    $APPLICATION->SetTitle(GetMessage("NOTA_COLLECTION_EDIT"));
else
    $APPLICATION->SetTitle(GetMessage("NOTA_COLLECTION_NEW"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

ClearVars("str_");

if (intval($ID) > 0) {
    $result = ElementsTable::getById($ID);
    if ($arRow = $result->fetch())
        extract($arRow, EXTR_PREFIX_ALL, 'str');
} else {
    $ID = '';
    $str_SORT = 100;
}


$aMenu = array(
    array(
        'TEXT' => Loc::getMessage('NOTA_COLLECTION_ELEMENTS'),
        'TITLE' => Loc::getMessage('NOTA_COLLECTION_ELEMENTS'),
        'LINK' => 'collection_elements.php?COLLECTION_ID=' . $COLLECTION_ID . '&lang=' . LANGUAGE_ID,
        'ICON' => 'btn_list',
    )
);

if (strlen($ID) > 0) {
    $aMenu[] = array("SEPARATOR" => "Y");
    $aMenu[] = array(
        "TEXT" => GetMessage("NOTA_COLLECTION_ADD"),
        "TITLE" => GetMessage("NOTA_COLLECTION_ADD"),
        "LINK" => "collection_element_edit.php?COLLECTION_ID=" . $COLLECTION_ID . "&lang=" . LANGUAGE_ID,
        "ICON" => "btn_new"
    );
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

if (!empty($errors))
    CAdminMessage::ShowMessage(implode("\n", $errors));

$map = ElementsTable::getMap();

?>
    <form method="POST" id="form" name="form" action="<?= $APPLICATION->GetCurUri() ?>">
        <?= bitrix_sessid_post() ?>
        <input type="hidden" name="Update" value="Y">
        <input type="hidden" name="ID" value="<?= $ID ?>">
        <?
        $tabControl->Begin();
        $tabControl->BeginNextTab();

        if (strlen($ID) > 0) {
            ?>
            <tr>
                <td><?= $map['ID']['title'] ?></td>
                <td><?= $str_ID ?></td>
            </tr>
            <?
        }
        ?>
        <tr>
            <td colspan="2">
                <?
                global $USER_FIELD_MANAGER;
                $entity_id = 'BOOK_COLLECTION';
                $arUserFields = $USER_FIELD_MANAGER->GetUserFields($entity_id, $ID, LANGUAGE_ID);
                $arUserFields['UF_EXALEAD_LINK']['VALUE'] = $str_BOOK_ID;
                echo $USER_FIELD_MANAGER->GetEditFormHTML(false, $GLOBALS['UF_EXALEAD_LINK'], $arUserFields['UF_EXALEAD_LINK']);
                ?>
            </td>
        </tr>
        <tr>
            <td><?= $map['SORT']['title'] ?></td>
            <td><input type="text" name="SORT" size="5" maxlength="5" value="<?= intval($str_SORT) ?>"></td>
        </tr>
        <?
        $tabControl->Buttons(array("disabled" => false));
        $tabControl->End();
        ?>
    </form>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>