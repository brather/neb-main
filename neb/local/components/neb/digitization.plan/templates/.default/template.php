<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>

<div class="reader-search clearfix">
    <form>
        <? if (true === $arResult['ALLOWED_BLOCKS']['libraryFilter']): ?>
            <?= Loc::getMessage('DIGITIZATION_PLAN_T_LIBRARY') ?>
            <select name="library" class="form-control">
                <option value="0"><?= Loc::getMessage('DIGITIZATION_PLAN_T_ALL') ?></option>
                <? foreach ($arResult['LIBRARIES'] as $library): ?>
                    <option value="<?= $library['ID'] ?>"
                        <? if ($arParams['LIBRARY_ID'] === intval($library['ID'])) {
                            echo 'selected';
                        } ?>><?= $library['NAME'] ?></option>
                <? endforeach; ?>
            </select>
        <? endif; ?>

        <div class="form-inline">
            <div style="margin: 1em 0;">
                <?= Loc::getMessage('DIGITIZATION_PLAN_T_DATE_APPLICATION') ?>
                <?= Loc::getMessage('DIGITIZATION_PLAN_T_FROM') ?>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="UF_DATE_ADD_from" size="10"
                               value="<?= $arResult['str_UF_DATE_ADD_from'] ?>"
                               id="dateZayaFrom"
                               class="form-control">
                        <span class="input-group-btn">
                            <a id="calendarlabel" class="btn btn-default"
                               onclick="BX.calendar({node: 'dateZayaFrom', field: 'dateZayaFrom', form: '', bTime: false, value: ''});">
                                <span
                                    class="glyphicon glyphicon-calendar"></span>
                            </a>
                        </span>
                    </div>
                </div>
                <?= Loc::getMessage('DIGITIZATION_PLAN_T_TO') ?>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="UF_DATE_ADD_to" size="10"
                               value="<?= $arResult['str_UF_DATE_ADD_to'] ?>"
                               id="dateZayaTo"
                               class="form-control">
                        <span class="input-group-btn">
                            <a id="calendarlabel" class="btn btn-default"
                               onclick="BX.calendar({node: 'dateZayaTo', field: 'dateZayaTo', form: '', bTime: false, value: ''});">
                                <span
                                    class="glyphicon glyphicon-calendar"></span>
                            </a>
                        </span>
                    </div>
                </div>
            </div>

            <div style="margin: 1em 0;">
                <?= Loc::getMessage('DIGITIZATION_PLAN_T_DATE_PLANE') ?>
                <?= Loc::getMessage('DIGITIZATION_PLAN_T_FROM') ?>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="UF_DATE_FINISH_from"
                               size="10"
                               value="<?= $arResult['str_UF_DATE_FINISH_from'] ?>"
                               id="datePlanFrom"
                               class="form-control">
                        <span class="input-group-btn">
                            <a id="calendarlabel" class="btn btn-default"
                               onclick="BX.calendar({node: 'datePlanFrom', field: 'datePlanFrom', form: '', bTime: false, value: ''});">
                                <span
                                    class="glyphicon glyphicon-calendar"></span>
                            </a>
                        </span>
                    </div>
                </div>
                <?= Loc::getMessage('DIGITIZATION_PLAN_T_TO') ?>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="UF_DATE_FINISH_to" size="10"
                               value="<?= $arResult['str_UF_DATE_FINISH_to'] ?>"
                               id="datePlanTo"
                               class="form-control">
                        <span class="input-group-btn">
                            <a id="calendarlabel" class="btn btn-default"
                               onclick="BX.calendar({node: 'datePlanTo', field: 'datePlanTo', form: '', bTime: false, value: ''});">
                                <span
                                    class="glyphicon glyphicon-calendar"></span>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <?= Loc::getMessage('DIGITIZATION_PLAN_T_STATUS') ?>
        <select name="request_status" class="form-control">
            <option value="0"><?= Loc::getMessage('DIGITIZATION_PLAN_T_ALL') ?></option>
            <? foreach ($arResult['STATUS_LIST'] as $statusKey => $statusName) { ?>
                <option value="<?= $statusKey ?>"
                    <? if ($arParams['REQUEST_STATUS'] === $statusKey) {
                        echo 'selected';
                    } ?>><?= $statusName ?></option>
            <? } ?>
        </select>
        <br>
        <?= Loc::getMessage('DIGITIZATION_PLAN_T_AUTHOR') ?>
        <input type="text" name="UF_BOOK_AUTHOR" class="form-control" value="<?= $arParams['UF_BOOK_AUTHOR'] ?>">
        <br>
        <?= Loc::getMessage('DIGITIZATION_PLAN_T_TITLE') ?>
        <input type="text" name="UF_BOOK_NAME" class="form-control" value="<?= $arParams['UF_BOOK_NAME'] ?>">
        <br>

        <? if ('user' !== $arResult['USER_ROLE']) { ?>
            <br>
            <?= Loc::getMessage('DIGITIZATION_PLAN_T_SHOW_TICKET') ?>
            <select name="UF_ADD_TYPE" class="form-control">
                <option value=""><?= Loc::getMessage('DIGITIZATION_PLAN_T_ALL') ?></option>
                <option value="user" <?= ('user' === $arParams['UF_ADD_TYPE']
                    ? 'selected' : '') ?>><?= Loc::getMessage('DIGITIZATION_PLAN_T_FROM_USERS') ?>
                </option>
                <option value="library"<?= ('library'
                === $arParams['UF_ADD_TYPE']
                    ? 'selected' : '') ?>><?= Loc::getMessage('DIGITIZATION_PLAN_T_FROM_LIBRARY') ?>
                </option>
            </select>
            <br>
        <? } ?>

        <button class="btn btn-primary"
                type="submit"
                value="<?= Loc::getMessage('FIND') ?>">
            <?= Loc::getMessage('DIGITIZATION_PLAN_T_FIND') ?>
        </button>
        <br>
        <br>

        <div class="b-digitizing_actions">
            <a class="btn btn-primary" target="_blank"
               href="<?= $arParams['EXPORT_URL'] ?>?print=Y"><?= Loc::getMessage('DIGITIZATION_PLAN_T_PRINT') ?></a>
            <a class="btn btn-primary" target="_blank"
               href="<?= $arParams['EXPORT_URL'] ?>?export=Y"><?= Loc::getMessage('DIGITIZATION_PLAN_T_EXPORT_TO_EXCEL') ?></a>
        </div>
    </form>
</div>

<h3><?= Loc::getMessage('DIGITIZATION_PLAN_T_LIST_OF_TICKETS') ?></h3>
<table class="table">
    <thead>
    <tr>
        <th><?= Loc::getMessage('DIGITIZATION_PLAN_T_EDITION') ?></th>
        <th>
            <a <?= SortingExalead('COUNT'); ?>>
                <nobr><?= Loc::getMessage('DIGITIZATION_PLAN_T_COUNT_SHORT') ?></nobr>
            </a>
        </th>
        <th>
            <a <?= SortingExalead('UF_DATE_ADD'); ?>><?= Loc::getMessage('DIGITIZATION_PLAN_T_DATE_APPLICATION_TITLE') ?></a>
        </th>
        <th>
            <a <?= SortingExalead('UF_DATE_FINISH'); ?>><?= Loc::getMessage('DIGITIZATION_PLAN_T_DATE_PLANE_TITLE') ?></a>
        </th>
        <th>
            <?= Loc::getMessage('DIGITIZATION_PLAN_T_DATA') ?>
        </th>
        <th>
            <?= Loc::getMessage('DIGITIZATION_PLAN_T_ACTIONS') ?>
        </th>
    </tr>
    </thead>
    <tbody>
    <? foreach ($arResult['items'] as $book) { ?>
        <tr>
            <td>
                <dl>
                    <?
                    if (!isset($book['exalead_data']) || empty($book['exalead_data'])) {
                        echo Loc::getMessage('DIGITIZATION_PLAN_T_BOOK_NOT_FOUND');
                    } else { ?>
                        <a href="/catalog/<?= $book['UF_EXALEAD_BOOK'] ?>/">
                            <?
                            echo $book['exalead_data']['title']
                                . '.-'
                                . $book['exalead_data']['publishplace']
                                . ','
                                . $book['exalead_data']['publishyear']
                            ?>
                        </a>
                    <? } ?>

                    <?
                    if (!empty($book['UF_BOOK_AUTHOR'])) {
                        echo "<dt>" . Loc::getMessage('DIGITIZATION_PLAN_T_AUTHOR') . "</dt> <dd>{$book['UF_BOOK_AUTHOR']}</dd>";
                    }
                    ?>
                </dl>
            </td>
            <td align="center">
                <?= $book['COUNT'] ?>
            </td>
            <td>
                <?= $book['UF_DATE_ADD'] ?>
            </td>
            <td>
                <?= $book['UF_DATE_FINISH'] ?>
            </td>
            <td>
                <?
                $ids = implode(',', $book['ORDERS']);
                echo "<b>" . Loc::getMessage('DIGITIZATION_PLAN_T_NUMBER') . "</b> $ids<br>";
                echo "<b>" . Loc::getMessage('DIGITIZATION_PLAN_T_STATUS') . "</b> {$book['UF_REQUEST_STATUS_NAME']}<br>";
                if (!empty($book['BOOK_FILE_PATH'])) {
                    echo "<b>" . Loc::getMessage('DIGITIZATION_PLAN_T_FILE') . "</b> <a target='_blank' href='{$book['BOOK_FILE_PATH']}'>" . Loc::getMessage('DIGITIZATION_PLAN_T_LOOK') . "</a><br>";
                }
                ?>
            </td>
            <td>
                <? if (!empty($book['ALLOWED_ACTIONS'])) {
                    foreach ($book['ALLOWED_ACTIONS'] as $action => $actionName) {
                        ?><a  href="#" class="digitiztion-action"
                              data-action="<?= $action ?>"
                              data-order-id="<?= $book['ID'] ?>"
                              data-exalead-id="<?= $book['UF_EXALEAD_BOOK'] ?>"
                              data-library-id="<?= $book['UF_IBLOCK_LIBRARY'] ?>"
                              data-date-finish="<?= $book['UF_DATE_FINISH'] ?>"
                              data-replated-libraries='<?= json_encode(
                                  $book['relatedLibraries']
                              ) ?>'
                        ><?= $actionName ?></a>
                        <?
                        /** @todo выпилить это, надо сделать аплоадилку на JavaScript */
                        if ('done' === $action) { ?>
                            <div class="upload-dpf-template hidden">
                                <? $APPLICATION->IncludeComponent(
                                    "notaext:plupload",
                                    "upload_pdf",
                                    [
                                        "MAX_FILE_SIZE"     => "24",
                                        "FILE_TYPES"        => "pdf",
                                        "DIR"               => "tmp_register",
                                        "FILES_FIELD_NAME"  => "profile_file",
                                        'FIELD_NAME'        => 'book_order_pdf',
                                        "MULTI_SELECTION"   => "N",
                                        "CLEANUP_DIR"       => "Y",
                                        "UPLOAD_AUTO_START" => "Y",
                                        "RESIZE_IMAGES"     => "Y",
                                        "RESIZE_WIDTH"      => "110",
                                        "RESIZE_HEIGHT"     => "110",
                                        "RESIZE_CROP"       => "Y",
                                        "RESIZE_QUALITY"    => "98",
                                        "UNIQUE_NAMES"      => "Y",
                                        "PERSONAL_PHOTO"    => $sImage,
                                        "FULL_NAME"         => $arResult['arUser']['NAME']
                                            . ' '
                                            . $arResult['arUser']['LAST_NAME'],
                                        "USER_EMAIL"        => $arResult['arUser']['EMAIL'],
                                        "DATE_REGISTER"     => $arResult['arUser']['DATE_REGISTER'],
                                        "UF_STATUS"         => $arResult['arUser']['UF_STATUS'],
                                        "USER_ID"           => $arParams["USER_ID"],
                                        "FOR_LIBRARY_USER"  => "Y",
                                    ],
                                    false
                                ); ?>
                            </div>
                            <?
                        } ?>
                        <br><?
                    }
                } ?>
            </td>
        </tr>
        <? if (is_array($book['history_logs'])) {
            foreach ($book['history_logs'] as $logItem) { ?>
                <tr>
                    <td colspan="6" style="padding-bottom: 1em;">
                        <?
                        $user = (!empty($logItem['user_data']['NAME'])
                            || $logItem['user_data']['LAST_NAME'])
                            ? $logItem['user_data']['NAME']
                            . ' ' . $logItem['user_data']['LAST_NAME']
                            : $logItem['user_data']['LOGIN'];
                        echo "<b>" . Loc::getMessage('DIGITIZATION_PLAN_T_TICKET') . "</b> №{$logItem['ORDER_ID']}<br>";
                        echo "<b>" . Loc::getMessage('DIGITIZATION_PLAN_T_DATE') . "</b> {$logItem['TIMESTAMP']}<br>";
                        echo "<b>" . Loc::getMessage('DIGITIZATION_PLAN_T_USER') . "</b> {$user}<br>";
                        echo "<b>" . Loc::getMessage('DIGITIZATION_PLAN_T_STATUS') . "</b> {$logItem['STATUS_NAME']}<br>";
                        echo !empty($logItem['COMMENT'])
                            ? "<b>" . Loc::getMessage('DIGITIZATION_PLAN_T_COMMENT') . "</b> {$logItem['COMMENT']}<br>"
                            : '';
                        ?>
                    </td>
                </tr>
            <? }
        } ?>
    <? } ?>
    </tbody>
</table>

<div id="libraries-list" class="hidden">
    <select class="form-control">
        <option value="0"><?= Loc::getMessage('DIGITIZATION_PLAN_T_NOTHING') ?></option>
        <? foreach ($arResult['SELECT_LIBRARIES'] as $key => $library) { ?>
            <option
                data-idlibrary="<?= $key ?>"
                value="<?= $library['ID'] ?>"><?= $library['NAME'] ?></option>
        <? } ?>
    </select>
</div>
<script type="application/javascript">
$(function(){
    var _execDigitizationAction = function (request, popup) {
        $.ajax({
            url: '/rest_api/book/plan-digitization/',
            method: 'PUT',
            dataType: 'json',
            data: request,
            cache: false,
            success: function () {
                popup.popup.modal('hide');
                location.reload();
            },
            error: function (xhr) {
                var message = window.neb.objectPathSearch(xhr, 'responseJSON.errors.message');
                popup.errorElement.text(
                    message
                        ? message
                        : '<?= Loc::getMessage('DIGITIZATION_PLAN_T_FAILED_ADD_PLAN') ?>'
                );
                popup.errorElement.show();
            }
        });
    };
    var _buildPupUp = function (options) {
        for (var key in options) {
            this[key] = options[key];
        }
    };
    _buildPupUp.prototype = $.extend(_buildPupUp.prototype, {
        popup: null,
        message: null,
        okButtonName: '<?= Loc::getMessage('DIGITIZATION_PLAN_T_OK') ?>',
        cancelButtonName: '<?= Loc::getMessage('DIGITIZATION_PLAN_T_CANCELED') ?>',
        okButton: null,
        cancelButton: null,
        textarea: null,
        iniPopUp: function () {
            this.popup.applyHeader();
            this.popup.applyFooter();
            this.setButtons();
            this.setErrorBlock();
            this.setMessage();
        },
        setErrorBlock: function () {
            this.popup.errorElement = $('<p class="popup-error alert alert-danger"></p>');
            this.popup.errorElement.hide();
            this.popup.popupBody.prepend(this.popup.errorElement);
        },
        setButtons: function () {
            this.okButton = $('<button type="button" class="btn btn-primary">' + this.okButtonName + '</button>');
            this.cancelButton = $('<button type="button" class="btn btn-primary">' + this.cancelButtonName + '</button>');

            this.popup.popupFooter.append(this.okButton);
            this.popup.popupFooter.append(this.cancelButton);
            this.cancelButton.click(function () {
                this.popup.popup.modal('hide');
            }.bind(this));
        },
        setMessage: function () {
            var message = $('<p>' + this.message + '</p>');
            this.popup.popupBody.append(message);
        },
        setTextArea: function () {
            this.textarea = $('<textarea class="form-control" rows="5"></textarea>');
            this.popup.popupBody.append(this.textarea);
        }
    });

    var simpleTexareaPopup = function (message, requestParams, status) {
        FRONT.simplePopup(function () {
            var builder = new _buildPupUp({
                popup: this,
                message: message
            });
            builder.iniPopUp();
            builder.setTextArea();

            var request = $.extend({}, requestParams);
            request.UF_REQUEST_STATUS = status;
            builder.okButton.click(function () {
                request.comment = builder.textarea.val();
                _execDigitizationAction(request, this);
            }.bind(this));
        });
    };
    $(function () {
        $('a.digitiztion-action').click(function (e) {
            e.preventDefault();
            var action = $(this).data('action'),
                libraryId = $(this).data('library-id'),
                dateFinish = $(this).data('date-finish'),
                replatedLibraries = $(this).data('replated-libraries'),
                bookExaleadId = $(this).data('exalead-id');
            if ('string' === typeof  replatedLibraries) {
                try {
                    replatedLibraries = $.parseJSON(replatedLibraries);
                } catch (e) {
                    replatedLibraries = {};
                }
            }
            var requestParams = {
                token: getUserToken(),
                actionType: action,
                ID: $(this).data('order-id')
            };
            switch (action) {
                case 'agree':
                    FRONT.simplePopup(function () {
                        var builder = new _buildPupUp({
                            popup: this,
                            message: '<?= Loc::getMessage('DIGITIZATION_PLAN_T_CONFIRM_APPROVAL') ?>'
                        });
                        builder.iniPopUp();
                        builder.setTextArea();

                        var finishDate = $('<input type="text" size="10" class="form-control" onclick="BX.calendar({node: this, field: this, form: history, bTime: false, value: \'\'});">');
                        var finishBlock = $('<p style="margin-top: 1em;"><?= Loc::getMessage('DIGITIZATION_PLAN_T_SPECIFY_DATE') ?></p>');
                        var librarySelect = $('#libraries-list').find('select').clone();
                        librarySelect.find('option').each(function () {
                            var idlibrary = $(this).data('idlibrary');
                            if (idlibrary && !replatedLibraries.hasOwnProperty(idlibrary)) {
                                $(this).remove();
                            }
                        });
                        var librarySelectBlock = $('<p><?= Loc::getMessage('DIGITIZATION_PLAN_T_CHOOSE_LIBRARY') ?></p>');

                        finishBlock.append(finishDate);
                        librarySelectBlock.append(librarySelect);

                        finishDate.val(dateFinish);
                        librarySelect.val(libraryId);

                        this.popupBody.append(finishBlock);
                        this.popupBody.append(librarySelectBlock);

                        var request = $.extend({}, requestParams);
                        builder.okButton.click(function (e) {
                            request.book_id = bookExaleadId;
                            request.comment = builder.textarea.val();
                            request.UF_REQUEST_STATUS = <?=PlanDigitalization::STATUS_AGREED?>;
                            request.UF_DATE_FINISH = finishDate.val();
                            request.UF_LIBRARY = librarySelect.val();
                            _execDigitizationAction(request, this);
                        }.bind(this));
                    });
                    break;
                case 'cancel':
                    simpleTexareaPopup('<?= Loc::getMessage('DIGITIZATION_PLAN_T_CHOOSE_REASON_CANCELED') ?>', requestParams, <?=PlanDigitalization::STATUS_CANCELED?>);
                    break;
                case 'back_in_progress':
                    simpleTexareaPopup('<?= Loc::getMessage('DIGITIZATION_PLAN_T_CHOOSE_REASON_TO_WORK') ?>', requestParams, <?=PlanDigitalization::STATUS_IN_PROCESS?>);
                    break;
                case 'refuse':
                    simpleTexareaPopup('<?= Loc::getMessage('DIGITIZATION_PLAN_T_CHOOSE_REASON_FAILURE') ?>', requestParams, <?=PlanDigitalization::STATUS_ON_AGREEMENT?>);
                    break;
                case 'close':
                    FRONT.simplePopup(function () {
                        var builder = new _buildPupUp({
                            message: '<?= Loc::getMessage('DIGITIZATION_PLAN_T_CONFIRN_QUESTION') ?>',
                            popup: this
                        });
                        builder.iniPopUp();
                        var request = $.extend({}, requestParams);
                        request.UF_REQUEST_STATUS = <?=PlanDigitalization::STATUS_CLOSED?>;
                        builder.okButton.click(function () {
                            _execDigitizationAction(request, this);
                        }.bind(this));
                    });
                    break;
                case 'in_progress':
                    FRONT.simplePopup(function () {
                        var builder = new _buildPupUp({
                            message: '<?= Loc::getMessage('DIGITIZATION_PLAN_T_CONFIRM_ACCEPTANCE') ?>',
                            popup: this
                        });
                        builder.iniPopUp();
                        var request = $.extend({}, requestParams);
                        request.UF_REQUEST_STATUS = <?=PlanDigitalization::STATUS_IN_PROCESS?>;
                        builder.okButton.click(function () {
                            _execDigitizationAction(request, this);
                        }.bind(this));
                    });
                    break;
                case 'done':
                    FRONT.simplePopup(function () {
                        var builder = new _buildPupUp({
                            message: '<?= Loc::getMessage('DIGITIZATION_PLAN_T__CONFIRM_ACCEPTANCE_TICKET') ?>',
                            popup: this
                        });
                        builder.iniPopUp();

                        var pdfPlupload = $('div.upload-dpf-template');
                        var pdfInput = pdfPlupload.find('input[name="book_order_pdf"]');
                        var pdfBlock = $('<div><?= Loc::getMessage('DIGITIZATION_PLAN_T_UPLOAD_PDF') ?></div>');
                        pdfPlupload.removeClass('hidden');
                        pdfBlock.append(pdfPlupload);
                        this.popupBody.append(pdfBlock);

                        var request = $.extend({}, requestParams);
                        request.UF_REQUEST_STATUS = <?= PlanDigitalization::STATUS_DONE ?>;
                        builder.okButton.click(function () {
                            if (pdfInput.val()) {
                                request.UF_BOOK_FILE_path = pdfInput.val();
                                /*console.log(pdfInput.val());
                                console.log(request);
                                console.log(request.UF_BOOK_FILE_path);*/
                                _execDigitizationAction(request, this);
                            } else {
                                this.errorElement.text(
                                   '<?= Loc::getMessage('DIGITIZATION_PLAN_T_CHOOSE_FILE') ?>'
                                );
                                this.errorElement.show();
                            }
                        }.bind(this));
                    });
                    break;
            }
        });
    });
});
</script>

<?= $arResult['nav'] ?>
