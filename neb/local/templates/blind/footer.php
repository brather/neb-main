<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION;
Use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>

<?
$APPLICATION->SetAdditionalCSS("/local/templates/adaptive/css/bootstrap.min.css");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/delayed-styles.css");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/checkboxes.css");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery-ui.autocompletenotheme.min.css");

//$APPLICATION->AddHeadScript("/local/templates/adaptive/js/script.js?bust=1");
/*$APPLICATION->AddHeadScript("/local/templates/adaptive/js/jquery-1.9.1.min.js");
$APPLICATION->AddHeadScript("/local/templates/adaptive/js/bootstrap.min.js");
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/template.js");*/



?>
<script src="/local/templates/adaptive/js/jquery-1.9.1.min.js"></script>
<!-- <script src="/local/templates/adaptive/js/jquery/jquery-ui-1.10.4.custom.min.js"></script> -->
<!-- <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->
<script type="text/javascript" src="/local/templates/adaptive/js/jquery/postjquery.js"></script>
<script src="/local/templates/adaptive/js/bootstrap.min.js"></script>
<script src="/local/templates/adaptive/js/protocolcheck.js"></script>
<script src="/local/templates/adaptive/js/neb/book-read-check.js"></script>
<!--script>defer$()</script-->

<?$APPLICATION->IncludeFile("/local/include_areas/counters.php", Array(), Array("MODE" => "php", "NAME" => "Счетчики"));?>

</body>
</html>