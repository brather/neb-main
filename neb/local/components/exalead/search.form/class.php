<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

require_once ($_SERVER["DOCUMENT_ROOT"]. '/local/tools/suggest/ReflectionTypeHint.php');
require_once ($_SERVER["DOCUMENT_ROOT"]. '/local/tools/suggest/Text/LangCorrect.php');
require_once ($_SERVER["DOCUMENT_ROOT"]. '/local/tools/suggest/UTF8.php');
require_once ('filter.php');

class SearchFormComponent extends \CBitrixComponent
{
	/**
	 * Шаблоны путей по умолчанию
	 *
	 * @var array
	 */
	protected $defaultUrlTemplates404 = array();
	
	/**
	 * Переменные шаблонов путей
	 *
	 * @var array
	 */
	protected $componentVariables = array();

	/**
	 * Страница шаблона
	 *
	 * @var string
	 */
	protected $page = '';

	/**
     * Подготавливает входные параметры
	 *
     * @param array $params
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
    	$parentResult = parent::onPrepareComponentParams($params);

		$arParams = array('PAGE' => trim($params['PAGE']));

		$arParams = array_merge($parentResult, $arParams);

		return $arParams;
	}

	/**
	 * Выполняет логику работы компонента
	 */
	public function executeComponent()
	{
		try
		{
			/*if (strpos($_SERVER['REQUEST_URI'], 'collections/') === false) {
				if (isset($_REQUEST['q'])) {
					$this->storeRequest();
				} else {
					$this->restoreRequest();
				}
			}*/
			$this->arParams['s_in_closed'] = true;
			if(isset($_REQUEST['q']) && !isset($_REQUEST['s_in_closed'])) {
				$this->arParams['s_in_closed'] = false;
			}

			$this -> setSefDefaultParams();
			$this -> getResult();
			$this -> getFormType();
			$this -> getExtendedLine();
			$this -> includeComponentTemplate($this -> page);
		}
		catch (Exception $e)
		{
			ShowError($e -> getMessage());
		}
	}

	/**
	 * Определяет переменные шаблонов и шаблоны путей
	 */
	protected function setSefDefaultParams()
	{
		$this -> defaultUrlTemplates404 = array(
		    'index'  => 'index.php',
		    'detail' => 'detail/#ELEMENT_ID#/'
		);
		$this -> componentVariables = array('ELEMENT_ID');
	}

	/**
	 * Получение результатов
	 */
	protected function getResult()
	{
		$urlTemplates = array();

		$variables = array();
		$urlTemplates = \CComponentEngine::makeComponentUrlTemplates(
			$this -> defaultUrlTemplates404,
			$this -> arParams['SEF_URL_TEMPLATES']
		);
		$variableAliases = \CComponentEngine::makeComponentVariableAliases(
			$this -> defaultUrlTemplates404,
			$this -> arParams['VARIABLE_ALIASES']
		);
		$this -> page = \CComponentEngine::parseComponentPath(
			$this -> arParams['SEF_FOLDER'],
			$urlTemplates,
			$variables
		);

		\CComponentEngine::initComponentVariables(
			$this -> page,
			$this -> componentVariables, $variableAliases,
			$variables
		);

		$this->page = 'template';

        // проверка на раскладку клавииатуры
        if (isset($_REQUEST['notransform']) && $_REQUEST['notransform'] ) {

			$_REQUEST['q'] = $q = trim(htmlspecialcharsEx($_REQUEST['q']));
			$_REQUEST["QUERY"] = $q;

		} else {
			$corrector = new Text_LangCorrect();
			$q = trim(htmlspecialcharsEx($_REQUEST['q']));
			$q = $corrector->parse($q, $corrector::KEYBOARD_LAYOUT);

			$_REQUEST['q'] = trim(htmlspecialcharsEx($_REQUEST['q']));
			$_REQUEST["QUERY"] = $q;

			if ($_REQUEST['q'] !== $q) {
				$_REQUEST["ORIGINAL_QUERY"] = $_REQUEST["q"];
			}
		}

		$this->arResult = array(
		   'FOLDER' => $this -> arParams['SEF_FOLDER'],
		   'URL_TEMPLATES' => $urlTemplates,
		   'VARIABLES' => $variables,
		   'ALIASES' => $variableAliases,
		);

        if (isset($this->arParams['SHOW_URLS'])) {
			$this->arResult['SHOW_URLS'] = $this->arParams['SHOW_URLS'];
		}

        $this->arResult['searchFields']  = NebFilterStructure::getSearchFields();
        $this->arResult['newcollection'] = NebFilterStructure::getSearchCollections();
	}

	/**
	 * Получение типа формы (простая, двойная или расширенная)
	 */
	public function getFormType() {

		if (isset($_REQUEST['librarytype'])) {
            $this->arResult['formType'] = 'E';
        } else if (isset($_REQUEST['q_name']) || isset($_REQUEST['q_author'])) {
            $this->arResult['formType'] = 'D';
        } else {
            $this->arResult['formType'] = 'S';
        }
	}

	/**
	 * Массив для формирования линии свернутого расширенного поиска
	 * @return array
	 */
	public function getExtendedLine() {

		$arResult = [];

		if ($this->arResult['formType'] != 'E')
			return $arResult;

		foreach ($this->arResult['searchFields'] as $arFields) {
			$arItems[$arFields['name']] = $arFields;
		}

		foreach ($_REQUEST as $k => $v) {
			if (empty($v))
				continue;

			if (!empty($arItems[$k]['label'])) {

				$v = htmlspecialcharsEx($v);

				if ($arItems[$k]['id'] == 'bbkfull') {
                    $v = implode(', ', $v);
                }
				elseif (!empty($arItems[$k]['value'][$v]['name']))
					$v = $arItems[$k]['value'][$v]['name'];
				elseif (!empty($arItems[$k]['value'][$v]))
					$v = $arItems[$k]['value'][$v];

				$arResult[$arItems[$k]['label']] = $v;
			}
		}

		$this->arResult['extendedLine'] = $arResult;
	}

	/**
	 * Записывает в сессию параметры REQUEST
	 * @return $this
	 */
	public function storeRequest(){

		$_SESSION['storeRequest'] = $_REQUEST;

		if (isset($_SESSION['storeRequest']['debug']))
			unset($_SESSION['storeRequest']['debug']);

		return $this;
	}

	/**
	 * Удаляет из сесии и куков параметры REQUEST
	 * @return $this
	 */
    public function restoreRequest(){
		if (isset($_COOKIE['cleanSearchAll'])
			&& isset($_SESSION['storeRequest'])
		) {
			$q = isset($_SESSION['storeRequest']['q'])
				? $_SESSION['storeRequest']['q']
				: null;
			$_SESSION['storeRequest'] = [];
			if ($q) {
				$_SESSION['storeRequest']['q'] = $q;
			}
			setcookie('cleanSearchAll', '', time() - 3600);
		}
		if (isset($_COOKIE['cleanSearchString'])
			&& isset($_SESSION['storeRequest']['q'])
		) {
			unset($_SESSION['storeRequest']['q']);
			setcookie('cleanSearchString', '', time() - 3600);
		}
        if (!empty($_SESSION['storeRequest'])) {
			$_REQUEST = array_merge($_SESSION['storeRequest'], $_REQUEST);
		}

        return $this;
    }

	/**
	 * Валидирует дату начала (используется в шаблонах)
	 * @param $sPrev
	 * @param $sNext
	 * @return bool
	 */
	public static function validatePrevDate($sPrev, $sNext) {
		return !empty($sPrev) && strcmp(intval($sPrev), $sPrev) == 0
			&& $sPrev >= SEARCH_BEGIN_YEAR && $sPrev <= date("Y") && $sPrev <= $sNext;
	}

	/**
	 * Валидирует дату окончания (используется в шаблонах)
	 * @param $sPrev
	 * @param $sNext
	 * @return bool
	 */
	public static function validateNextDate($sPrev, $sNext) {
		return !empty($sNext) && strcmp(intval($sNext), $sNext) == 0
			&& $sNext >= SEARCH_BEGIN_YEAR && $sNext <= date("Y") && $sNext >= $sPrev;
	}
}