<?php
/**
 * User: agolodkov
 * Date: 12.07.2016
 * Time: 10:52
 */

namespace AppBundle\Command;


use LbcBundle\Traits\LbcMigrationsTrait;
use Neb\Main\Stat\Tracker;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PrepareLbcMigrationsSql extends ContainerAwareCommand
{
    use LbcMigrationsTrait;

    protected function configure()
    {
        $this
            ->setName('lbc:prepare-sql')
            ->setDescription('Prepare SQL inserts');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);

        $this->getContainer()
            ->get('bitrix')
            ->requireProlog(
                $this->getContainer()
                    ->get('kernel')
                    ->getRootDir()
            );
        while (ob_get_level()) {
            ob_end_clean();
        }
        $sqlDir = $this->getContainer()
                ->get('kernel')
                ->getRootDir() . '/DoctrineMigrations/sql/';
        $files = [
            'bbk_full.sql',
//            'bbk_supply_2_dump.sql',
//            'bbk_supply_3_dump.sql',
//            'bbk_supply_4_dump.sql',
//            'bbk_supply_5_dump.sql',
//            'bbk_supply_6_dump.sql',
//            'bbk_supply_7_dump.sql',
//            'bbk_supply_8_dump.sql',
        ];

//        preg_match_all(
//            '/(INSERT INTO `lbc_maindivision` \(`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`\) VALUES \([\d]+, [\dNULL]+, [\d]+, \')([^0-9\(]+)([0-9]{1}[^0-9]{1})/',
//            "INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (201376, 201308, 5, 'Ж.с5', 'Геодезические, геофизические и геологические методы', '', '', '', 0, 2947, 0, '');",
//            $matches
//        );
//        var_dump($matches);
//
//
//        $string="INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (200873, 200022, 2, 'А07/08', 'НАУЧНЫЙ КОММУНИЗМ', '', '', 'См. также: Ф Коммунистические и рабочие партии. Общественно-политические организации трудящихся Альтернатива. См. Ф Коммунистические и рабочие партии', 0, 186, 0, '');
//INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (200874, 200873, 3, 'А7:', 'Диалектика развития социализма и коммунизма. Закономерности становления и развития коммунистической формации', '', '', '', 0, 186, 0, '');
//INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (200887, 200873, 3, 'А7а/я', 'ОБЩИЙ РАЗДЕЛ', 'Раздел А7а/я строится в соответствии с Таблицей общих типовых делений*, аналогично общему разделу к Марксизму-ленинизму (А.а/я). Ниже приводятся деления, имеющие специфические формулировки, детализацию или методические указания. *Библиотечно-библиографическая классификация. Таблицы для научных библиотек. Вып. XXV. Таблицы типовых делений. М., 1961, ч. 1. Таблицы общих типовых делений, стр. 7-36', '', '', 1, 186, 0, '');
//INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (300166, 300103, 3, 'С6,0', 'Общие работы', 'Под индексом С6,0 собираются также учебники для высшей школы', '', '', 0, 268, 0, '');
//INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (201375, 201373, 8, 'Ж.с454.2', 'Ионообменная хроматография', '', '', '', 0, 2947, 0, '');
//INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (201376, 201308, 5, 'Ж.с5', 'Геодезические, геофизические и геологические методы', '', '', '', 0, 2947, 0, '');
//INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (201377, 201308, 5, 'Ж.с7', 'Специальные методы', '', '', '', 0, 2947, 0, '');
//INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (201378, 201377, 6, 'Ж.с78', 'Применение фотографии, кино, телевидения', '', '', '', 0, 2947, 0, '');
//INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (201379, 201308, 5, 'Ж.с8', 'Обработка результатов исследования, испытания, анализа', '', '', '', 0, 2947, 0, '');
//INSERT INTO `lbc_maindivision` (`Id`, `ParentId`, `Level`, `Code`, `Name`, `Comments`, `Keywords`, `Links`, `IsCommon`, `Rec_id`, `Gr`, `GrErr`) VALUES (202367, 202263, 5, '(2Р35)', 'Поволжье', '', '', '', 1, 4740, 0, '');
//";
//        $string = explode(PHP_EOL, $string);
//        foreach ($string as $item) {
//            $item = $this->prepareInsertString($item);
//            echo $item, PHP_EOL;
//        }
//        die;


        foreach ($files as $file) {
            $count = 0;
            if (file_exists($sqlDir . $file)) {
                echo "File $sqlDir$file\n";
                $replacedFile = $sqlDir . 'replaced_' . $file;
                if(file_exists($replacedFile)) {
                    unlink($replacedFile);
                }
                $handle = fopen($sqlDir . $file, 'r');
                while (!feof($handle)) {
                    $line = fgets($handle);
                    if ($this->validInsertString($line)) {
                        $line = $this->prepareInsertString($line);
                        file_put_contents($replacedFile, $line, FILE_APPEND);
                        $count++;
                        echo "Insert lines: {$count}\r";
                    }
                }
                fclose($handle);
                echo PHP_EOL;
            }
        }


        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}