<?php

/**
 * Class PlanDigitizationComponent
 */
class PlanDigitizationComponent extends \Neb\Main\ListComponent
{
    /**
     * @var array
     */
    protected $_defaultParams
        = array(
            'NAV_NUM'    => 1,
            'PAGE_SIZE'  => 15,
            'LIBRARY_ID' => 0,
            'listParams' => array(
                'filter' => array(),
            )
        );

    /**
     * @return $this
     */
    public function prepareParams()
    {
        $dateFilter = ['UF_DATE_ADD', 'UF_DATE_FINISH'];
        foreach ($dateFilter as $dateField) {
            $this->arParams[$dateField . '_from'] = 0;
            $this->arParams[$dateField . '_to'] = 0;
        }

        foreach ($dateFilter as $dateField) {
            if (isset($_REQUEST[$dateField . '_from'])) {
                $this->arParams[$dateField . '_from'] = strtotime(
                    $_REQUEST[$dateField . '_from']
                );
            }
            if (isset($_REQUEST[$dateField . '_to'])) {
                $this->arParams[$dateField . '_to'] = strtotime(
                    $_REQUEST[$dateField . '_to']
                );
            }
        }
        if (empty($this->arParams['UF_DATE_ADD_from'])) {
            $this->arParams['UF_DATE_ADD_from'] = time() - 30 * 86400;
        }
        if (empty($this->arParams['UF_DATE_ADD_to'])) {
            $this->arParams['UF_DATE_ADD_to'] = time();
        }
        if (empty($this->arParams['UF_DATE_FINISH_from'])) {
            $this->arParams['UF_DATE_FINISH_from'] = time() - 30 * 86400;
        }
        if (empty($this->arParams['UF_DATE_FINISH_to'])) {
            $this->arParams['UF_DATE_FINISH_to'] = time() + 6 * 30 * 86400;
        }
        foreach ($dateFilter as $dateField) {
            $this->arResult['str_' . $dateField . '_from'] = ConvertTimeStamp(
                $this->arParams[$dateField . '_from'], 'SHORT'
            );
            $this->arResult['str_' . $dateField . '_to'] = ConvertTimeStamp(
                $this->arParams[$dateField . '_to'], 'SHORT'
            );
        }

        $this->arParams['LIBRARY_ID'] = (integer)$this->arParams['LIBRARY_ID'];
        $this->arParams['REQUEST_STATUS']
            = (integer)$this->arParams['REQUEST_STATUS'];

        foreach (
            [
                'UF_BOOK_AUTHOR',
                'UF_BOOK_NAME',
                'UF_ADD_TYPE'
            ] as $filterField
        ) {
            $this->arParams[$filterField] = '';
            if (isset($_REQUEST[$filterField])) {
                $this->arParams[$filterField] = trim($_REQUEST[$filterField]);
            }
        }

        $this->_applyListFilter();
        $this->_prepareNavigation();

        return $this;
    }

    protected function _applyListFilter()
    {
        $this->arParams['listParams']['filter']['>=UF_DATE_ADD']
            = $this->arResult['str_UF_DATE_ADD_from'];
        $this->arParams['listParams']['filter']['<=UF_DATE_ADD']
            = $this->arResult['str_UF_DATE_ADD_to'] . ' 23:59:59';
        $this->arParams['listParams']['filter']['>=UF_DATE_FINISH']
            = $this->arResult['str_UF_DATE_FINISH_from'];
        $this->arParams['listParams']['filter']['<=UF_DATE_FINISH']
            = $this->arResult['str_UF_DATE_FINISH_to'] . ' 23:59:59';

        if (!empty($this->arParams['LIBRARY_ID'])) {
            $libLink = CIBlockElement::GetProperty(
                IBLOCK_ID_LIBRARY,
                $this->arParams['LIBRARY_ID'],
                [],
                ['CODE' => 'LIBRARY_LINK']
            )->Fetch();
            if (!empty($libLink)) {
                $this->arParams['listParams']['filter']['UF_LIBRARY'] = $this->arParams['LIBRARY_ID'];
            }
        }

        $allowedStatues = array_intersect_key(
            PlanDigitalization::getStatusEnums(),
            PlanDigitalization::getAllowedRoleStatuses(
                nebUser::getCurrent()->getRole()
            )
        );
        foreach ($allowedStatues as $xmlId => $statusEnum) {
            $allowedStatues[$xmlId] = $statusEnum['ID'];
        }
        $this->arParams['listParams']['filter']['UF_REQUEST_STATUS']
            = $allowedStatues;
        if (isset($allowedStatues[PlanDigitalization::STATUS_ON_AGREEMENT])) {
            $this->arParams['listParams']['filter']['UF_REQUEST_STATUS'][]
                = null;
        }
        $this->arParams['listParams']['filter'][999] = [
            'LOGIC' => 'OR',
            ['UF_REQUEST_STATUS' => null],
            ['!=UF_REQUEST_STATUS' => PlanDigitalization::getStatusValueId(
                PlanDigitalization::STATUS_CANCELED
            )
            ],
        ];
        if ($this->arParams['REQUEST_STATUS'] > 0) {
            if (!isset($allowedStatues[$this->arParams['REQUEST_STATUS']])) {
                throw new Exception('Статус не доступен!');
            }
            unset($this->arParams['listParams']['filter'][999]);
            if (!$statusValueId = PlanDigitalization::getStatusValueId(
                $this->arParams['REQUEST_STATUS']
            )
            ) {
                throw new Exception(
                    "Статус '{$this->arParams['REQUEST_STATUS']}' не найден!"
                );
            }
            if (PlanDigitalization::STATUS_ON_AGREEMENT
                === $this->arParams['REQUEST_STATUS']
            ) {
                $this->arParams['listParams']['filter'][] = [
                    'LOGIC' => 'OR',
                    ['UF_REQUEST_STATUS' => null],
                    ['UF_REQUEST_STATUS' => $statusValueId],
                ];
            } else {
                $this->arParams['listParams']['filter']['UF_REQUEST_STATUS']
                    = $statusValueId;
            }
        }

        if (mb_strlen($this->arParams['UF_BOOK_AUTHOR']) > 0)
            $this->arParams['listParams']['filter']['%=UF_BOOK_AUTHOR'] = '%' . $this->arParams['UF_BOOK_AUTHOR'] . '%';

        if (mb_strlen($this->arParams['UF_BOOK_NAME']) > 0)
            $this->arParams['listParams']['filter']['%=UF_BOOK_NAME'] = '%' . $this->arParams['UF_BOOK_NAME'] . '%';

        if (mb_strlen($this->arParams['UF_ADD_TYPE']) > 0)
            $this->arParams['listParams']['filter']['=UF_ADD_TYPE'] = $this->arParams['UF_ADD_TYPE'];

        $this->arParams['listParams']['select'][] = 'UF_EXALEAD_PARENT';
        $this->arParams['listParams']['select'][] = 'ORDERS';
        $this->arParams['listParams']['select'][] = 'COUNT';
        $this->arParams['listParams']['runtime'] = array(
            new \Bitrix\Main\Entity\ExpressionField(
                'ORDERS',
                'GROUP_CONCAT(ID)'
            ),
            new \Bitrix\Main\Entity\ExpressionField(
                'COUNT',
                'COUNT(ID)'
            ),
            new \Bitrix\Main\Entity\ExpressionField(
                'UF_EXALEAD_PARENT_UNIQUE',
                'IF(UF_EXALEAD_PARENT IS NULL OR UF_EXALEAD_PARENT = 0,UUID(),UF_EXALEAD_PARENT)'
            ),
        );
        $this->arParams['listParams']['group'] = 'UF_EXALEAD_PARENT_UNIQUE';

        $sortFields = [
            'UF_BOOK_AUTHOR' => true,
            'UF_BOOK_NAME'   => true,
            'COUNT'          => true,
            'UF_DATE_ADD'    => true,
            'UF_DATE_FINISH' => true,
        ];
        if (isset($_REQUEST['by'])) {
            $by = trim($_REQUEST['by']);
            $order = isset($_REQUEST['order']) ? $_REQUEST['order'] : 'asc';
            $order = strtoupper($order);
            if (isset($sortFields[$by])) {
                $this->arParams['listParams']['order'] = [
                    $by => $order,
                ];
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function loadList()
    {
        NebMainHelper::includeModule('nota.exalead');
        $tableClass = NebMainHelper::getHIblockDataClassName(
            HIBLOCK_PLAN_DIGIT
        );
        $this->_loadTable(new $tableClass());
        $this->_loadOrdersData();
        $this->_applyRoleBlocks();
        $this->_loadBooksFromExalead();
        $this->_loadUsers();
        $this->_loadLibraries();
        $this->_setStatuses();
        $this->_setAllowedActions();
        $this->_loadHistoryLog();
        $this->_loadBookFiles();

        return $this;
    }


    protected function _loadOrdersData()
    {
        $planDigitization = NebMainHelper::getHIblockDataClassName(
            HIBLOCK_PLAN_DIGIT
        );
        $ordersIds = [];
        foreach ($this->arResult['items'] as $key => &$item) {
            $item['ORDERS'] = explode(',', $item['ORDERS']);
            foreach ($item['ORDERS'] as $orderId) {
                $ordersIds[$orderId] = $key;
            }
        }
        $this->arResult['ORDERS_IDS'] = $ordersIds;
        unset($item);
        $ordersRs = $planDigitization::getList(
            [
                'filter' => [
                    'ID' => array_keys($ordersIds),
                ],
            ]
        );
        while ($order = $ordersRs->fetch()) {
            if (isset($ordersIds[$order['ID']])) {
                $key = $ordersIds[$order['ID']];
                $this->arResult['items'][$key] = array_merge(
                    $order,
                    $this->arResult['items'][$key]
                );
                if ($this->arResult['items'][$key]['UF_LIBRARY']
                    = (integer)$this->arResult['items'][$key]['UF_LIBRARY']
                ) {
                    $this->arResult['items'][$key]['UF_IBLOCK_LIBRARY']
                        = nebLibrary::getIblockIdByExaleadId(
                        $this->arResult['items'][$key]['UF_LIBRARY']
                    );
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _applyRoleBlocks()
    {
        $blocks = [
            'libraryFilter' => false,
        ];
        $roleAllowedBlocks = [
            'operator' => [
                'libraryFilter' => true,
            ],
        ];
        $userRole = nebUser::getCurrent()->getRole();
        $this->arResult['ALLOWED_BLOCKS'] = isset($roleAllowedBlocks[$userRole])
            ? array_replace($blocks, $roleAllowedBlocks[$userRole])
            : [];

        return $this;
    }

    /**
     * @return $this
     */
    protected function _loadBooksFromExalead()
    {
        $bookIds = [];
        foreach ($this->arResult['items'] as $key => $item) {
            $bookIds[$item['UF_EXALEAD_BOOK']] = $key;
        }
        $query = new \Nota\Exalead\SearchQuery();
        $query->getByArIds(array_keys($bookIds));
        $query->setPageLimit(10000);
        $client = new \Nota\Exalead\SearchClient();
        $exaleadResult = $client->getResult($query);
        if (isset($exaleadResult['ITEMS'])) {
            foreach ($exaleadResult['ITEMS'] as $item) {
                if (isset($bookIds[$item['id']])) {
                    $this->arResult['items'][$bookIds[$item['id']]]['exalead_data']
                        = $item;
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _loadUsers()
    {
        $userIds = [];
        foreach ($this->arResult['items'] as $key => $item) {
            $item['UF_NEB_USER'] = (integer)$item['UF_NEB_USER'];
            if ($item['UF_NEB_USER'] < 0) {
                continue;
            }
            if (!isset($userIds[$item['UF_NEB_USER']])) {
                $userIds[$item['UF_NEB_USER']] = [];
            }
            $userIds[$item['UF_NEB_USER']][] = $key;
        }
        if (!empty($userIds)) {
            $user = new CUser();
            $rsUsers = $user->GetList(
                $by,
                $order,
                [
                    'ID' => implode('|', array_keys($userIds)),
                ]
            );
            while ($userData = $rsUsers->Fetch()) {
                if (isset($userIds[$userData['ID']])) {
                    foreach ($userIds[$userData['ID']] as $key) {
                        $this->arResult['items'][$key]  ['user_data']
                            = $userData;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @todo отптимизировать, 2 раза выбираются библиотеки
     * @return $this
     */
    protected function _loadLibraries()
    {
        $ordersFilter = array_intersect_key(
            $this->arParams['listParams']['filter'], [
                'UF_NEB_USER'       => true,
                '>=UF_DATE_ADD'     => true,
                '<=UF_DATE_ADD'     => true,
                '>=UF_DATE_FINISH'  => true,
                '<=UF_DATE_FINISH'  => true,
                'UF_REQUEST_STATUS' => true,
            ]
        );
        $planDigitization = NebMainHelper::getHIblockDataClassName(
            HIBLOCK_PLAN_DIGIT
        );
        $ordersRs = $planDigitization::getList(
            [
                'filter' => $ordersFilter,
                'group'  => ['UF_LIBRARY'],
                'select' => ['UF_LIBRARY']
            ]
        );
        $libIds = [];
        while ($orderItem = $ordersRs->fetch()) {
            if (isset($orderItem['UF_LIBRARY'])) {
                $libIds[] = (integer)$orderItem['UF_LIBRARY'];
            }
        }
        $libIds = array_filter($libIds);
        $arResult['LIBRARIES'] = array();
        if (!empty($libIds)) {
            $arLibs = \Bitrix\NotaExt\Iblock\Element::getList(
                array(
                    'IBLOCK_ID'             => IBLOCK_ID_LIBRARY,
                    'ACTIVE'                => 'Y',
                    'PROPERTY_LIBRARY_LINK' => $libIds,
                ),
                0,
                array('ID', 'NAME'),
                array('NAME' => 'ASC')
            );

            foreach ($arLibs['ITEMS'] as $lib) {
                $this->arResult['LIBRARIES'][] = $lib;
            }
        }

        $idParemtOrderMap = [];
        $libraryIds = [];
        foreach ($this->arResult['items'] as $key => &$orderItem) {
            if (!isset($orderItem['relatedLibraries'])) {
                $orderItem['relatedLibraries'] = [];
            }
            $orderItem['relatedLibraries'][$orderItem['exalead_data']['idlibrary']]
                = true;
            $libraryIds[$orderItem['exalead_data']['idlibrary']] = true;
            if (!empty($orderItem['exalead_data']['idparent2'])) {
                $idParemtOrderMap[$orderItem['exalead_data']['idparent2']]
                    = $key;
            }
        }
        unset($orderItem);
        if (!empty($idParemtOrderMap)) {
            $idParentQuery = [];
            foreach ($idParemtOrderMap as $idParent => $true) {
                $idParentQuery[] = "idparent2:($idParent)";
            }
            $idParentQuery = implode(' OR ', $idParentQuery);
            $query = new \Nota\Exalead\SearchQuery($idParentQuery);
            $query->setPageLimit(5000);
            $query->setParam('sl', 'sl_nofuzzy');
            $client = new \Nota\Exalead\SearchClient();
            $result = $client->getResult($query);
            if (!empty($result['ITEMS'])) {
                foreach ($result['ITEMS'] as $book) {
                    if (isset($idParemtOrderMap[$book['idparent2']])) {
                        $key = $idParemtOrderMap[$book['idparent2']];
                        $this->arResult['items'][$key]['relatedLibraries'][$book['idlibrary']]
                            = true;
                        $libraryIds[$book['idlibrary']] = true;
                    }
                }
            }
        }
        $libraryIds = array_keys($libraryIds);
        $libraryIds = array_values($libraryIds);
        if (!empty($libraryIds)) {
            $arLibs = \Bitrix\NotaExt\Iblock\Element::getList(
                array(
                    'IBLOCK_ID'             => IBLOCK_ID_LIBRARY,
                    'ACTIVE'                => 'Y',
                    'PROPERTY_LIBRARY_LINK' => $libraryIds,
                ),
                0,
                array('ID', 'NAME', 'PROPERTY_LIBRARY_LINK'),
                array('NAME' => 'ASC')
            );
            $this->arResult['SELECT_LIBRARIES'] = [];
            foreach ($arLibs['ITEMS'] as $lib) {
                $this->arResult['SELECT_LIBRARIES'][$lib['PROPERTY_LIBRARY_LINK_VALUE']]
                    = $lib;
            }
        }

        return $this;
    }

    /**
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     */
    protected function _loadHistoryLog()
    {
        $orderIds = [];
        if (isset($this->arParams['listParams']['filter']['UF_NEB_USER'])) {
            $orderRelationsMap = [];
            foreach ($this->arResult['items'] as $key => $item) {
                $orderIds[$item['ID']] = $key;
                $relationKey = empty($item['UF_EXALEAD_PARENT'])
                    ? $item['UF_EXALEAD_BOOK']
                    : $item['UF_EXALEAD_PARENT'];
                $orderRelationsMap[$relationKey] = $key;
            }
            $relatedOrders = PlanDigitalization::getRelatedOrdersByIdsList(
                array_keys($orderIds)
            );
            foreach ($relatedOrders as $item) {
                $relationKey = empty($item['UF_EXALEAD_PARENT'])
                    ? $item['UF_EXALEAD_BOOK']
                    : $item['UF_EXALEAD_PARENT'];
                if (isset($orderRelationsMap[$relationKey])) {
                    $orderIds[$item['ID']] = $orderRelationsMap[$relationKey];
                }
            }
        } else {
            foreach ($this->arResult['items'] as $key => $item) {
                foreach ($item['ORDERS'] as $orderId) {
                    $orderIds[$orderId] = $key;
                }
            }
        }
        if (!empty($orderIds)) {
            $logs = \Neb\Main\PlanDigitizationLogTable::getList(
                [
                    'filter' => [
                        'ORDER_ID' => array_keys($orderIds),
                    ]
                ]
            );
            $logItems = [];
            $userIds = [];

            /** @var Bitrix\Main\Type\DateTime $logItem [TIMESTAMP] */
            while ($logItem = $logs->fetch()) {
                $logItem['STATUS_NAME'] = PlanDigitalization::getStatusName(
                    $logItem['STATUS_ID']
                );
                $logItems[$logItem['ID']] = $logItem;
                if (!$userIds[$logItem['USER_ID']]) {
                    $userIds[$logItem['USER_ID']] = [];
                }
                $userIds[$logItem['USER_ID']][] = $logItem['ID'];
            }
            if (!empty($userIds)) {
                $user = new CUser();
                $rsUsers = $user->GetList(
                    $by,
                    $order,
                    [
                        'ID' => implode('|', array_keys($userIds)),
                    ]
                );
                while ($userData = $rsUsers->Fetch()) {
                    if (isset($userIds[$userData['ID']])) {
                        foreach ($userIds[$userData['ID']] as $itemId) {
                            $logItems[$itemId]['user_data']
                                = $userData;
                        }
                    }
                }
            }
            foreach ($logItems as $logItem) {
                if (isset($orderIds[$logItem['ORDER_ID']])) {
                    $this->arResult['items']
                    [$orderIds[$logItem['ORDER_ID']]]
                    ['history_logs'][]
                        = $logItem;
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _loadBookFiles()
    {
        $file = new CFile();
        foreach ($this->arResult['items'] as $key => &$item) {
            $item['BOOK_FILE_PATH'] = $file->GetPath($item['UF_BOOK_FILE']);
        }
        unset($item);

        return $this;
    }

    /**
     * @return $this
     */
    protected function _setStatuses()
    {
        foreach ($this->arResult['items'] as $key => &$item) {
            $item['UF_REQUEST_STATUS_NAME']
                = PlanDigitalization::getStatusNameByValueId(
                $item['UF_REQUEST_STATUS']
            );
        }
        unset($item);
        $this->arResult['STATUS_LIST'] = array_intersect_key(
            PlanDigitalization::getStatusList(),
            PlanDigitalization::getAllowedRoleStatuses(
                nebUser::getCurrent()->getRole()
            )
        );

        return $this;
    }

    /**
     * @return $this
     */
    protected function _setAllowedActions()
    {
        $role = nebUser::getCurrent()->getRole();
        $actionNames = [
            'cancel'           => 'Отменить',
            'agree'            => 'Согласовать',
            'close'            => 'Закрыть',
            'done'             => 'Выполнить',
            'in_progress'      => 'Взять в работу',
            'back_in_progress' => 'Вернуть на доработку',
            'refuse'           => 'Отказаться',
        ];
        foreach ($this->arResult['items'] as $key => &$item) {
            $item['ALLOWED_ACTIONS'] = array_intersect_key(
                $actionNames,
                array_flip(
                    PlanDigitalization::getAllowedActionsRS(
                        $role,
                        PlanDigitalization::getStatusByValueId(
                            $item['UF_REQUEST_STATUS']
                        )
                    )
                )
            );
        }
        unset($item);

        return $this;
    }
}