<?
define('STOP_STATISTICS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\RslViewer,
    \Nota\Exalead\LibraryBiblioCardTable;

Loader::includeModule('nota.exalead');

$error   = true;
$result  = $arJson = [];

/**
 * Проверка доступа к книге по её ID
 */
if ('GET' == $_SERVER['REQUEST_METHOD'] && !empty($_GET['book_id'])) {

    $sTicket = '';

    $nebUser = new \nebUser();
    if ($nebUser->IsAuthorized()) {
        $nebUser->checkTemporaryVerification();
        $arUser = $nebUser->getUser();
    } else {
        $arUser = $nebUser->getAnonymousUser();
    }

    $sBookId   = $_GET['book_id'];
    $idLibrary = intval(substr($sBookId, 0, strpos($sBookId, '_')));

    LibraryBiblioCardTable::applyViewBook($sBookId, session_id());

    $obRslViewer = new RslViewer($_GET['book_id']);
    $sAvailableType = $obRslViewer->isAvailableType();
    $iHttpStatus    = $obRslViewer->getHttpStatus();

    // вид доступа не определен (ошибка запроса, например)
    if (false == $sAvailableType) {

        $result['result'] = 'error';
        $result['statusCode'] = $iHttpStatus;
    }
    // книга в открытом доступе РГБ/РНБ
    elseif ('open' == $sAvailableType) {

        $result['result'] = $sAvailableType;
    }
    // книга в заблокирована до особых указаний правообладателя
    elseif ('blocked' == $sAvailableType) {

        $result['result'] = 'copyright';
        $result['accesscomment'] = $obRslViewer->getAccessComment();
    }
    // книга в заблокирована до особых указаний правообладателя
    elseif ('close' == $sAvailableType) {

        $result['result'] = 'closed';

        // проверка режим доступа для анонимных пользователей
        if ($nebUser->checkAuthAccessMode()) {
            $arTicket = $nebUser->getWorkPlaceTicket();
            $sTicket = !empty($arTicket['UF_NUMBER']) ? $arTicket['UF_NUMBER'] : 'required';
        }

        // для пользователей модуля разметки разрешен доступ из открытого просмотровщика
        $arUsrGr = array_intersect($nebUser->getUserGroups(), [UGROUP_MARKUP_EDITOR, UGROUP_MARKUP_APPROVER]); // PHP 5.4 fuck...
        if ($nebUser->IsAuthorized() && !empty($arUsrGr)) {
            $result['result'] = 'open';
        }
    }
    // книга имеет иной тип доступа
    else {
        // проверка на принадлежности книги РГБ и нахождение пользователя в институском ВЧЗ
        if (199 === $idLibrary
            && NebWorkplaces::checkAccess($_SERVER['REMOTE_ADDR'], ['FILTER' => ['PROPERTY_TYPE_VALUE' => 'institute']]))
        {
            $result['redirectUrl'] = "https://viewer.rsl.ru/01" . end(explode('_', $sBookId));
            unset($result['result']);
        }
    }
    $result['isWorkplace']  = NebWorkplaces::checkAccess();
    $result['isAuthorized'] = (boolean)$nebUser->IsAuthorized();
    $result['ticket']       = $sTicket;
    $result['token']        = $arUser['UF_TOKEN'];
}

MainHelper::showJson($result);