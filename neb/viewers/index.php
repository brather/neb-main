<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Программное обеспечение");
?>

<h2>Мобильное приложение НЭБ</h2>

<div class="row">
	<div class="col-sm-1 text-center app-icon-cell">
		<a class="neb-app-icon android-icon" href="https://play.google.com/store/apps/details?id=ru.elar.neb.viewer" title="Android">
			<img alt="mob-android.png" src="/local/templates/adaptive/img/app-icons/android-blue.png">
		</a>
	</div>
	<div class="col-sm-11 app-desc-cell"><a href="https://play.google.com/store/apps/details?id=ru.elar.neb.viewer">Android</a></div>
</div>
<div class="row">
	<div class="col-sm-1 text-center app-icon-cell"><a class="neb-app-icon ios-icon" href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=944682831&mt=8"><img alt="mob-apple.png" src="/local/templates/adaptive/img/app-icons/ios-blue.png" title="mob-apple.png"></a></div>
	<div class="col-sm-11 app-desc-cell"><a href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=944682831&mt=8">iPhone</a></div>
</div>

<h2>Приложение НЭБ для стационарных компьютеров</h2>
<p>
	Приложение просмотра изданий, охраняемых авторским правом, для установки на терминалах&nbsp;доступа к НЭБ в электронных читальных залах&nbsp;
</p>
<p>
	<em>Обращаем ваше&nbsp;внимание, что просмотр изданий, охраняемых авторским правом, осуществляется&nbsp;только из <a href="/workplaces/">электронных читальных залов НЭБ.</a></em>
</p>
 
<div class="row">
	<div class="col-sm-1 text-center app-icon-cell">
		<span class="neb-app-icon win-icon"><img src="/local/templates/adaptive/img/app-icons/win-color.png" class="viewers__logo-img" alt=""></span>
	</div>
	<div class="col-sm-11 app-desc-cell">
		<p>
			<a href="/upload/distribs/nebviewer-windows.exe">nebviewer-windows.exe</a> &mdash;&nbsp;программа просмотра&nbsp;для Windows</b>
		</p>
		
	</div>
</div>
<div class="row">
	<div class="col-sm-1 text-center app-icon-cell">
		<span class="neb-app-icon osx-icon"><img src="/local/templates/adaptive/img/app-icons/osx-blue.png" class="viewers__logo-img" alt=""></span>
	</div>
	<div class="col-sm-11 app-desc-cell"><a href="/upload/distribs/nebviewer-macos-x86_64.dmg">nebviewer-macos-x86_64.dmg</a> &mdash;&nbsp;программа просмотра&nbsp;для <b>32</b> и <b>64</b> битных версий MacOS SnowLeopard, DirtyPanther</div>
</div>
<div class="row">
	<div class="col-sm-1 text-center app-icon-cell">
		<span class="neb-app-icon linux-icon"><img src="/local/templates/adaptive/img/app-icons/linux-blue.png" class="viewers__logo-img" alt=""></span>
	</div>
	<div class="col-sm-11 app-desc-cell">
		<p>
			<a href="/upload/distribs/nebviewer-linux-i686.run">nebviewer-linux-i686.run</a> &mdash;&nbsp;программа просмотра&nbsp;для <b>32</b> битных Linux
		</p>
		<p>
			<a href="/upload/distribs/nebviewer-linux-x86_64.run">nebviewer-linux-x86_64.run</a> &mdash;&nbsp;программа просмотра&nbsp;для <b>64</b> битных Linux
		</p>
	</div>
</div>

<style>
.neb-app-icon {display: inline-block; width: 65px; height: 65px; background-repeat: no-repeat; background-position: center center;}
@media screen {.neb-app-icon img {display: none;}}
.android-icon {background-image: url("/local/templates/adaptive/img/app-icons/android-blue.png");}
.ios-icon {background-image: url("/local/templates/adaptive/img/app-icons/ios-blue.png");}
.osx-icon {background-image: url("/local/templates/adaptive/img/app-icons/osx-blue.png");}
.linux-icon {background-image: url("/local/templates/adaptive/img/app-icons/linux-blue.png");}
.win-icon {background-image: url("/local/templates/adaptive/img/app-icons/win-color.png");}

.color2 .android-icon,
.color3 .android-icon,
.color5 .android-icon {background-image: url("/local/templates/adaptive/img/app-icons/android.png");}
.color2 .ios-icon,
.color3 .ios-icon,
.color5 .ios-icon {background-image: url("/local/templates/adaptive/img/app-icons/ios.png");}
.color2 .osx-icon,
.color3 .osx-icon,
.color5 .osx-icon {background-image: url("/local/templates/adaptive/img/app-icons/osx.png");}
.color2 .linux-icon,
.color3 .linux-icon,
.color5 .linux-icon {background-image: url("/local/templates/adaptive/img/app-icons/linux.png");}
.color2 .win-icon,
.color3 .win-icon,
.color5 .win-icon {background-image: url("/local/templates/adaptive/img/app-icons/win.png");}

.app-desc-cell {padding-top: 1em;}
</style>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>