<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="subscribe-form" id="subscribe-form">
    <?
    $frame = $this->createFrame("subscribe-form", false)->begin();
    ?>
    <form action="<?= $arResult["FORM_ACTION"] ?>">

        <? foreach ($arResult["RUBRICS"] as $itemID => $itemValue): ?>
            <?if(null !== $arParams['RUBRIC_ID']
                && $itemValue["ID"] != $arParams['RUBRIC_ID']) {
                continue;
            } ?>
            <div class="checkbox <?= ($itemValue['ID'] == $arParams['RUBRIC_ID'])?'hidden':''?>">
                <label for="sf_RUB_ID_<?= $itemID ?>">
                    <input type="checkbox"
                           id="sf_RUB_ID_<?= $itemID ?>"
                           name="sf_RUB_ID[]"
                           value="<?= $itemValue["ID"] ?>"
                           checked>
                                <span
                                    class="lbl"><?= $itemValue["NAME"] ?></span>
                </label>
            </div>
        <? endforeach; ?>
        <div class="row">
            <div class="form-group col-xs-4">
                <div class="input-group">
                    <input type="text" name="sf_EMAIL" size="20"
                        value="<?= $arResult["EMAIL"] ?>"
                        title="<?= GetMessage("subscr_form_email_title") ?>"
                        class="form-control"
                        />
                    <span class="input-group-btn">
                        <button name="OK" class="btn btn-primary">
                            <?= GetMessage("subscr_form_button") ?>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </form>
    <?
    $frame->beginStub();
    ?>
    <form action="<?= $arResult["FORM_ACTION"] ?>">

        <? foreach ($arResult["RUBRICS"] as $itemID => $itemValue): ?>
            <label for="sf_RUB_ID_<?= $itemValue["ID"] ?>">
                <input type="checkbox" name="sf_RUB_ID[]"
                       id="sf_RUB_ID_<?= $itemValue["ID"] ?>"
                       value="<?= $itemValue["ID"] ?>"/> <?= $itemValue["NAME"] ?>
            </label><br/>
        <? endforeach; ?>

        <table border="0" cellspacing="0" cellpadding="2" align="center">
            <tr>
                <td><input type="text" name="sf_EMAIL" size="20" value=""
                           title="<?= GetMessage(
                               "subscr_form_email_title"
                           ) ?>"/></td>
            </tr>
            <tr>
                <td align="right"><input type="submit" name="OK"
                                         value="<?= GetMessage(
                                             "subscr_form_button"
                                         ) ?>"/></td>
            </tr>
        </table>
    </form>
    <?
    $frame->end();
    ?>
</div>
