<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<?if ($arParams['PDF_AVAILABLE'] and $arResult['STATUS'] != 'QUERY_ALREADY_EXISTS') {?>
	<div class="b-lineinfo">
		<h5><?=GetMessage('BOOKS_DIGITAL_QUERY_DIG')?></h5>

		<?if ($arResult['STATUS'] == 'QUERY_ALREADY_EXISTS') {?>
			<a href="javascript:void();" data-posat="left top" data-posmy="left+288 top-270" data-width="375"  class="formbtn grad noclose"><?=GetMessage('BOOKS_DIGITAL_QUERY_IN_PLAN')?></a>
		<?} else {?>
			<a href="<?=$APPLICATION->GetCurPageParam('action=addDigitizationQueryForm&'.bitrix_sessid_get(), array('action'))?>" data-posat="left top" data-posmy="left+288 top-48" data-width="375"  class="formbtn grad popup_opener ajax_opener noclose"><?=GetMessage('BOOKS_DIGITAL_QUERY_ADD_QUERY')?></a>
		<?}?>
	</div>
<?} else {/*?>
	<div class="clearfix b-digitalaction">
		<?if ($arResult['STATUS'] == 'QUERY_ALREADY_EXISTS') {?>
			<a href="javascript:void();" data-posat="left top" data-posmy="left+288 top-270" data-width="375"  class="formbtn grad noclose">В плане на оцифровку</a>
		<?} else {?>
			<a href="<?=$APPLICATION->GetCurPageParam('action=addDigitizationQueryForm&'.bitrix_sessid_get(), array('action'))?>" data-posat="left top" data-posmy="left+288 top-48" data-width="375"  class="formbtn grad popup_opener ajax_opener noclose">Заявка на оцифровку</a>
		<?}?>
	</div>
<?*/}?>
