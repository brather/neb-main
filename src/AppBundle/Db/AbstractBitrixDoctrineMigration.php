<?php
namespace AppBundle\Db;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\DBAL\Migrations\Version;

/**
 * User: agolodkov
 * Date: 20.06.2016
 * Time: 18:06
 */
abstract class AbstractBitrixDoctrineMigration extends AbstractMigration
    implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(Version $version)
    {
        parent::__construct($version);

    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    protected function _includeBitrix()
    {
        $this->container
            ->get('bitrix')
            ->requireProlog(
                $this->container
                    ->get('kernel')
                    ->getRootDir()
            );

        return $this;
    }
}