<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151214140654 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            <<<SQL
            ALTER TABLE tbl_common_biblio_card
ADD COLUMN `FullSymbolicId` VARCHAR(100)
COLLATE utf8_unicode_ci DEFAULT NULL;
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql(
            <<<SQL
            ALTER TABLE tbl_common_biblio_card
DROP COLUMN `FullSymbolicId`;
SQL
        );
    }
}
