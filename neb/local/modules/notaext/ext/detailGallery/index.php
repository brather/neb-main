<?

class detailGallery extends CExt
{
    var $EXT_ID = __CLASS__;
    var $EXT_VERSION;
    var $EXT_VERSION_DATE;
    var $EXT_NAME;
    var $EXT_DESCRIPTION;
    var $EXT_SORT;

    public function __construct()
    {
        parent::__construct();

        $this->EXT_VERSION = '1.0';
        $this->EXT_VERSION_DATE = '06.05.2014';

        $this->EXT_NAME = 'Detail Gallery';
        $this->EXT_DESCRIPTION = 'Свойство для вставки галереи в детальное описание элемента (<b>Требует установки расширения <i>"Загрузка больших файлов"</i></b>)';
        $this->EXT_SORT = 10000;

    }

    function onBeforeInstall()
    {

    }

    function onBeforeUninstall()
    {

    }

    function onAfterInstall()
    {
        RegisterModuleDependences("main", "OnEndBufferContent", 'notaext', 'Bitrix\NotaExt\Ext\DetailGallery\setDetailGallery', "OnEndBufferContent");
        RegisterModuleDependences("fileman", "OnBeforeHTMLEditorScriptRuns", 'notaext', 'Bitrix\NotaExt\Ext\DetailGallery\EditorButton', "OnBeforeHTMLEditorScriptRuns");
    }

    function onAfterUninstall()
    {
        UnRegisterModuleDependences("main", "OnEndBufferContent", 'notaext', 'Bitrix\NotaExt\Ext\DetailGallery\setDetailGallery', "OnEndBufferContent");
        UnRegisterModuleDependences("fileman", "OnBeforeHTMLEditorScriptRuns", 'notaext', 'Bitrix\NotaExt\Ext\DetailGallery\EditorButton', "OnBeforeHTMLEditorScriptRuns");
    }
}