<?
$MESS["LEARNING_TEMPLATE_COPYRIGHT"] = "&copy; 2013 Bitrix, Inc.";
$MESS["FOOTER_ADD_TO"] = "Add to";
$MESS["FOOTER_MY_COLLECTIONS"] = "My collections";
$MESS["FOOTER_CREATE_COLLECTION"] = "Make a collection";
$MESS["FOOTER_REMOVE_BOOK"] = "Remove the book from My Library";
$MESS["FOOTER_ACCEPT_REMOVE"] = "Remove";
$MESS["FOOTER_KEEP_BOOK"] = "Cancel";
$MESS["FOOTER_CLOSE_WINDOW"] = "Close window";
// -------
$MESS['COPYRIGHT_AUTORS'] = 'Supported by "ELAR JSC" on order of Ministry of Culture of the Russian Federation'; // Разработка АО ЭЛАР по заказу Министерства Культуры РФ
$MESS['COPYRIGHT_TEXT'] = 'All rights reserved. It is forbidden to copy whole/part of the materials. In case of consistent use a reference to the website is needed. It is forbidden to copy whole/part of the works. The permission agreement with their authors is needed.'; 
/*Все права защищены. Полное или частичное копирование материалов запрещено, при согласованном 
 * использовании материалов необходима ссылка на ресурс. 
 * Полное или частичное копирование произведений запрещено, 
 * согласование использования произведений производится с их авторами.';*/
$MESS['CLOSE'] = 'close'; // закрыть
