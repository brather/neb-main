<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->IncludeComponent(
    'neb:digitization.plan',
    '',
    [
        'DATE_FROM'      => $_REQUEST['dateFrom'],
        'DATE_TO'        => $_REQUEST['dateTo'],
        'LIBRARY_ID'     => $_REQUEST['library'],
        'REQUEST_STATUS' => $_REQUEST['request_status'],
        'listParams'     => [
            'filter' => [
                'UF_NEB_USER' => nebUser::getCurrent()
                    ->GetID()
            ]
        ],
        'EXPORT_URL'     => '/profile/digitization-export/',
    ],
    $component
);
