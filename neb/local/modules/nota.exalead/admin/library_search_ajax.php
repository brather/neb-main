<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!Main\Loader::includeModule('highloadblock'))
	throw new Main\LoaderException('Модуль highloadblock не установлен!');

// Идентификатор Highload блока библиотек
$libraryHlblockId = 6;

$hlblock = HL\HighloadBlockTable::getById($libraryHlblockId)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

if($_REQUEST['id'])
{
    $result = $entity_data_class::getById($_REQUEST['id'])->fetch();

	if(count($result))
	{	
		$arLibrary = array(
			'TITLE' => $result['UF_NAME'],
            'ADDRESS' => $result['UF_ADRESS'],
            'PHONE' => $result['UF_PHONE'],
		);
	}	
}?>

<?if(count($arLibrary)){?>
	<?if(!empty($arLibrary['TITLE'])){?>
		<p><?=$arLibrary['TITLE']?></p>
	<?}?>
    <?if(!empty($arLibrary['ADDRESS'])){?>
        <p><?=$arLibrary['ADDRESS']?></p>
    <?}?>
    <?if(!empty($arLibrary['PHONE'])){?>
        <p><?=$arLibrary['PHONE']?></p>
    <?}?>
<?}?>
<?die();?>