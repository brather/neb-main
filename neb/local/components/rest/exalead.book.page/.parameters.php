<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'       => '/',
                    'VARIABLES' => array(
                        'token',
                        'book_id',
                        'p',
                        'dataType',
                        'width',
                        'search',
                        'pageWidth',
                        'isAvailable',
                    ),
                'TYPE'          => array('GET'),
                'DOCUMENTATION' => array(
                    'GROUP'       => 'CONTROL',
                    'DESCRIPTION' => '',
                    'PARAMETERS'  => array()
                ),
            ),
        ),
    ),
); 