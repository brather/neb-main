<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//use \Neb\Main\Stat\PiwikStats;
/**
 * @var CMain $APPLICATION
 * @var array $arResult
 * @var array $arParams
 */
?>
<? $APPLICATION->SetTitle("Статистика"); ?>
<div class="iblock b-lib_fulldescr">

    <div class='b-add_digital js_digital'>
        <form class="stats-period-form"
            action="<?= $APPLICATION->GetCurPage() ?>?<? if ($arParams['FORMAT']
                == 'view'
            ): ?>form=view<? endif; ?>" method="GET" name="form1">
            C <input type="text" id="date-from" name="from" size="10" value="" class="b-text datepicker datepicker-from">
            по <input type="text" name="to"  id="date-to" size="10" value="" class="b-text datepicker datepicker-current">
            <input type='hidden' value='<?= $arParams['FORMAT'] ?>' name='form'>

            <div class="divchekblok">
                <span>Количество изданий</span>
                <input class="checkbox" id="check"
                       type="checkbox"
                       name="count_items"
                       value="Y"
                       checked="checked"><br>
                <span>Количество изданий c электронной копией</span> <input
                    class="checkbox" id="check" type="checkbox"
                    name="count_digitalitems" value="Y" checked="checked"><br>
                <span>Количество прочитанных книг библиотеки</span> <input
                    class="checkbox" id="check" type="checkbox"
                    name="count_readbook" value="Y" checked="checked"><br>
            </div>
            <input type="submit" class="b-search_bth bbox stats-period-submit" value="<?=GetMessage("LIBRARY_STAT_REFRESH");?>"
                   style='float:none;'>
        </form>
        <div class="stats-period-message">
            Задан не верный период дат
        </div>
        <br>
        <div class="b-digitizing_actions stat-buttons-block">
            <a class="button_mode" target="_blank" href="<?=$APPLICATION->GetCurPageParam('export=Y', array('export'));?>"><?=GetMessage('LIBRARY_STAT_EXPORT_EXCEL')?></a>
        </div>

        <?php if (isset($arResult['grphicData'])
            && !empty($arResult['grphicData'])
        ) { ?>
            <div style="width:100%; overflow:hidden;">
                <ul class="uleditstat">
                    <?php foreach ($arResult['grphicData'] as $dataItem) { ?>
                        <li>
                            <a href=''><?php echo $dataItem['name'] ?></a>
                            <ul class="submenu2">
                                <li>
                                    <div>
                                        <iframe
                                            width="100%"
                                            height="350"
                                            src="<?//= PiwikStats::DEFAULT_HOST . $arResult['widgetUrl'] . '&' . http_build_query(  $dataItem['widgetParams']) ?>"
                                            scrolling="no"
                                            frameborder="0"
                                            marginheight="0"
                                            marginwidth="0"></iframe>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <? } ?>

        <?php echo htmlspecialcharsBack($arResult['STR_NAV']) ?>
    </div>
    <br><br>
</div>