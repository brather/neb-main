<?
mb_internal_encoding('ISO-8859-1'); # без этого не работает АПИ Экзалиды связано с mbstring.func.overload = 2

error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", 1);

require_once('config.php');
require_once('functions.php');

/*
Скрипт сохраняет атрибуты книги в таблице tbl_common_biblio_card
*/

/*
Доступ к MySQL
*/
$DBHost = $_REQUEST['HOST'];
$DBLogin = $_REQUEST['LOGIN'];
$DBPassword = $_REQUEST['PASSWORD'];
$DBName = $_REQUEST['DBNAME'];

/*
Путь для хранения PDF файлов
*/

$SAVE_DIR = EXALED_INDEX_DIR_FILES;

/************************************************************************************/
/************************************START*******************************************/


$LIB_ID = trim($_REQUEST['LIB_ID']);
$Id = trim($_REQUEST['Id']);
$PDF_LINK = trim($_REQUEST['PDF_LINK']);

if(empty($LIB_ID)){
    exaleadLog("Не указаны данные библиотеки.");
    die('Empty LIB data');
}

require_once('database.php');

$db = new DBConnect($DBHost, $DBLogin, $DBPassword, $DBName, 3306);


$alis_id_lib = false;

$db->query('select * from tbl_libraries_alis where Type = 80 AND Library = '.intval($LIB_ID));
if($arRes = $db->fetch())
{
    $alis_id_lib = $arRes['Id'];
}
else
{
    $db->query('INSERT INTO tbl_libraries_alis SET Type = 80, Name = "'.intval($LIB_ID).'", Library = "'.intval($LIB_ID).'"');
    $alis_id_lib = $db->insert_id();
}

if(empty($alis_id_lib))
{
    exaleadLog("Не указан alis_id_lib");
    die('Error alis ID');
}

$arFieldsBookCard = array(
    'ALIS',
    'IdFromALIS',
    'Author',
    'Name',
    //'Title',
    'SubName',
    'PublishYear',
    'PublishPlace',
    'Publisher',
    'ISBN',
    'Language',
    'CountPages',
    'Format',
    'Series',
    'VolumeNumber',
    'VolumeName',
    'Collection',
    'AccessType',
    'isLicenseAgreement',
    'Responsibility',
    'Annotation',
    'ContentRemark',
    'CommonRemark',
    'PublicationInformation',
    'pdfLink',
    'BBKText',
    'UDKText',
    'LanguageText',
);

$arFields = array();
foreach($arFieldsBookCard as $field)
{
    if(!empty($_POST[$field]))
        $arFields[$field] = htmlspecialchars(trim($_POST[$field]));
}


$arFields['UpdatingDateTime'] = 'now()';

if($PDF_LINK){
    // загрузка файла
    $arFields['pdfLink'] = $PDF_LINK;
}else{

    $arFields['ALIS'] = $alis_id_lib;
    $arFields['Name'] 	= implode(' ', Array($arFields['Title'], $arFields['SubName'], $arFields['VolumeName'], $arFields['VolumeNumber']));
    //$arFields['PublishYearNorm'] = substr($arFields['PublishYear'],0,4);
    if (preg_match('/(\S*)\s(\S*)\s(\S*)/', $arFields['Author'], $tmp) == true )
        $arFields['Author2'] = $tmp[1]." ".substr($tmp[2],0,2).".".substr($tmp[3],0,2).".";
    else
        $arFields['Author2'] = $arFields['Author'];

    $arFields['Responsibility2'] = $arFields['Author2'];
    $arFields['PublishPlace'] = str_replace(array('М.','Л.','Спб'),array('Москва','Ленинград','Санкт-Петербург'),$arFields['PublishPlace']);
}

// проверить есть ли книга в таблице
$strSql = '
	SELECT * FROM `tbl_common_biblio_card` WHERE IdFromALIS = "'.trim($_REQUEST['IdFromALIS']).'" OR FullSymbolicId="'.$Id.'" limit 1;
	';
$db->query($strSql);
$arRes = $db->fetch();

if(!empty($arRes)) {
    /* Обноавляем данные */
    $res = $db->update('tbl_common_biblio_card', 'Id = "'.$arRes['Id'].'"', $arFields);

} else {
    /*	Добавляем */
    $arFields['CreationDateTime'] = 'now()';
    $cardId = $db->insert('tbl_common_biblio_card', $arFields);
}

?>