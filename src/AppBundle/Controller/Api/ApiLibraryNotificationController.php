<?php
/**
 * User: agolodkov
 * Date: 05.09.2016
 * Time: 17:02
 */

namespace AppBundle\Controller\Api;


use AppBundle\DependencyInjection\LibraryPostingHelper;
use BitrixBundle\DependencyInjection\BitrixHelper;
use BitrixBundle\Entity\Posting\BPosting;
use BitrixBundle\Form\Posting\BPostingType;
use FOS\RestBundle\View\View;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use BitrixBundle\Annotations\IncludeBitrix;

/**
 * Class LbcHistoryController
 *
 * @DI\Service("app.api.library_notification", scope="request")
 */
class ApiLibraryNotificationController
{
    /**
     * @DI\Inject("form.factory")
     * @var FormFactory
     */
    public $formFactory;

    /**
     * @DI\Inject("neb.library_posting")
     * @var LibraryPostingHelper
     */
    public $libraryPosting;

    /**
     * @DI\Inject("bitrix_helper")
     * @var BitrixHelper
     */
    public $bitrix;

    /**
     * @DI\Inject("service_container")
     * @var ContainerInterface
     */
    public $container;


    /**
     * @param Request $request
     *
     * @Method("POST")
     * @Rest\View(serializerGroups={"details"})
     * @IncludeBitrix()
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createAction(Request $request)
    {
        $entity = new BPosting();
        $form = $this->formFactory->create(
            new BPostingType(),
            $entity,
            ['method' => 'POST']
        );

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (!$id = $this->libraryPosting->createPosting($form)) {
                throw new HttpException(500, 'Posting create error');
            }
            $entity->setId($id);

            return ['entity' => $entity];
        }

        return $form;
    }

    /**
     * @param int $id
     *
     * @Method("POST")
     * @Rest\View()
     * @IncludeBitrix()
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    public function sendAction($id)
    {
        $result = [
            'step'    => $this->libraryPosting->getStepEmailsCount(),
            'remains' => 0,
        ];
        $posting = $this->libraryPosting->forgeCPosting();
        $posting->SendMessage($id, 0, $result['step']);
        $statuses = $posting->GetEmailStatuses($id);
        if (isset($statuses['Y']) && $statuses['Y'] > 0) {
            $result['remains'] = (integer)$statuses['Y'];
        }

        return $result;
    }

    /**
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return array
     */
    public function historyAction()
    {
        $this->libraryPosting->search();

        return [
            'entities' => $this->libraryPosting->getEntities(),
        ];
    }

    /**
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return BinaryFileResponse
     * @throws \Exception
     */
    public function historyExportAction()
    {
        $uploadDir = realpath($this->container->getParameter('dir.upload'));
        $uploadDir .= '/';
        if (!file_exists($uploadDir)) {
            throw new \Exception("Not found upload directory '$uploadDir'");
        }
        $uploadDir .= 'tmp/library-notification/';
        $this->bitrix->checkIncludeThrowException();
        $excel = new \PHPExcel();
        $excel->getProperties()
            ->setTitle('Оповещения')
            ->setSubject('Оповещения');
        $sheet = $excel->setActiveSheetIndex(0);
        $sheet
            ->setCellValue('A1', '№')
            ->setCellValue('B1', 'Дата отправки')
            ->setCellValue('C1', 'Текст письма')
            ->setCellValue('D1', 'Отправитель')
            ->setCellValue('E1', 'Получатели');

        $entities = $this->libraryPosting->search()->getEntities();
        $row = 2;
        foreach ($entities as $entity) {
            $reseivers = [];
            foreach ($entity->getPostingEmails() as $postingEmail) {
                $reseivers[] = $postingEmail->getEmail();
            }
            $sheet
                ->setCellValue('A' . $row, $entity->getId())
                ->setCellValue('B' . $row, $entity->getDateSent())
                ->setCellValue('C' . $row, strip_tags(html_entity_decode($entity->getBody())))
                ->setCellValue('D' . $row, $entity->getFromField())
                ->setCellValue('E' . $row, implode(',', $reseivers));
            $row++;
        }
        $sheet->calculateColumnWidths();
        $excel->getActiveSheet()->setTitle('Statistics');
        $excel->setActiveSheetIndex(0);
        if (!file_exists($uploadDir)) {
            mkdir($uploadDir, 0777, true);
        }
        $fileName = 'history-export-' . microtime(true) . '.xlsx';
        $filePath = $uploadDir . $fileName;
        $excelWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $excelWriter->save($filePath);
        $response = new BinaryFileResponse($filePath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    public function detailAction($id)
    {

    }
}