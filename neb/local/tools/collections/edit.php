<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized())
    exit();

$name = htmlspecialcharsbx(trim($_REQUEST['name']));
$id = htmlspecialcharsbx(trim($_REQUEST['id']));

$affectedRows = 0;
if(!empty($id) && !empty($name)) {
    CModule::IncludeModule("nota.userdata");
    $result = Nota\UserData\Collections::update($id, array('UF_NAME' => $name));
    $errors = $result->getErrors();
    if(empty($errors)) {
        echo 'true';
        die;
    }
}
echo 'true';