<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Loader,
    \Bitrix\Main\Data\Cache,
    \Bitrix\Main\Localization\Loc,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient,
    \Neb\Main\SearchCollectionsTable,
    \Neb\Main\SearchLanguagesTable;

Loc::loadMessages(__FILE__);

class NebFilterStructure
{
    /**
     * Возвращает поля поиска
     * @return array
     */
    public static function getSearchFields(){

        $arFields = [

            /** ============================================= ОБЩАЯ ЧАСТЬ ========================================== */

            array( // ВИД ПОИСКА (ВСЕ)
                'type'        => 'tabs',
                'name'        => 'librarytype',
                'id'          => 'searchrub',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ALL_SECTION_SEARCH'),
                'tabindex'    => 8,
                'access'      => ['always'],
                'aria-hidden' => true,
                'value' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_ALL_SECTION_LIBS'),
                    'museum'  => Loc::getMessage('EXTENDED_SEARCH_ALL_SECTION_MUSEUMS'),
                    'archive' => Loc::getMessage('EXTENDED_SEARCH_ALL_SECTION_ARCHIVES'),
                ),
            ),
            array( // ТИП ПОИСКА (ВСЕ)
                'type'        => 'select',
                'name'        => 'searchtype',
                'id'          => 'searchtype',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ALL_SEARCH_TYPE'),
                'tabindex'    => 9,
                'access'      => ['always'],
                'aria-hidden' => true,
                'value' => array(
                    'simple'   => Loc::getMessage('EXTENDED_SEARCH_ALL_SEARCH_SIMPLE'),
                    'extended' => Loc::getMessage('EXTENDED_SEARCH_ALL_SEARCH_EXENDED'),
                ),
            ),
            array( // ОБЩИЙ ЗАПРОС (ПРОСТОЙ ПОИСК)
                'type'        => 'text',
                'name'        => 'q',
                'id'          => 'requeststring',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ALL_FULLQUERY'),
                'tabindex'    => 1,
                'access'      => ['simple', 'library', 'museum', 'archive'],
            ),
            array( // АВТОР (БИБЛИОТЕКИ, МУЗЕИ)
                'type'        => 'text',
                'name'        => 'q_author',
                'id'          => 'author',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ALL_AUTHOR'),
                'tabindex'    => 2,
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_ALL_AUTHOR_PH_LIBRARY'),
                    'museum'  => Loc::getMessage('EXTENDED_SEARCH_ALL_AUTHOR_PH_MUSEUM'),
                ),
                'access'      => ['extended', 'library', 'museum'],
                'attributes'  => 'data-autocomplete-item data-query-field="authorbook" data-entity-field="title"',
            ),
            array( // НАЗВАНИЕ (БИБЛИОТЕКИ, МУЗЕИ, АРХИВЫ)
                'type'        => 'text',
                'name'        => 'q_name',
                'id'          => 'title',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ALL_TITLE'),
                'tabindex'    => 1,
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_ALL_TITLE_PH_LIBRARY'),
                    'museum'  => Loc::getMessage('EXTENDED_SEARCH_ALL_TITLE_PH_MUSEUM'),
                    'archive' => Loc::getMessage('EXTENDED_SEARCH_ALL_TITLE_PH_ARCHIVE'),
                ),
                'access'      => ['extended', 'library', 'museum', 'archive'],
                'attributes'  => 'data-autocomplete-item data-query-field="title" data-entity-field="title" data-filtered-by="authorbook"',
            ),
            array( // ТЕКСТ
                'type'        => 'text',
                'name'        => 'text',
                'id'          => 'text',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_LIB_TEXT'),
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_ALL_TEXT_PH_LIBRARY'),
                    'museum'  => '',
                    'archive' => '',
                ),
                'tabindex'    => 21,
                'access'      => ['extended', 'library', 'museum', 'archive'],
            ),
            array( // ДАТА (БИБЛИОТЕКИ, МУЗЕИ, АРХИВЫ)
                'type'        => 'date',
                'name'        => 'publishyear',
                'id'          => 'publishyear',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ALL_DATE'),
                'tabindex'    => 12,
                'access'      => ['extended', 'library', 'museum', 'archive'],
                'aria-hidden' => true,
                'value' => array(
                    '_prev'  => ['name' => 'от', 'value' => SEARCH_BEGIN_YEAR],
                    '_next'  => ['name' => 'до', 'value' => date('Y')],
                ),
            ),

            /** ============================================== БИБЛИОТЕКИ ========================================== */

            array( // ДОСТУП К ИЗДАНИЯМ
                'type'        => 'radio',
                'name'        => 'access',
                'id'          => 'allres',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_LIB_ACCESS'),
                'tabindex'    => 10,
                'access'      => ['extended', 'library'],
                'value' => array(
                    'all'    => [
                        'name'        => Loc::getMessage('EXTENDED_SEARCH_LIB_ACCESS_ALL_NAME'),
                        'screen'      => Loc::getMessage('EXTENDED_SEARCH_LIB_ACCESS_ALL_SCREEN'),
                        'aria-hidden' => true,
                        'default'     => true
                    ],
                    'open'   => [
                        'name'        => Loc::getMessage('EXTENDED_SEARCH_LIB_ACCESS_OPEN_NAME'),
                        'screen'      => Loc::getMessage('EXTENDED_SEARCH_LIB_ACCESS_OPEN_SCREEN'),
                        'aria-hidden' => true,
                    ],
                    'closed' => [
                        'name'        => Loc::getMessage('EXTENDED_SEARCH_LIB_ACCESS_CLOSED_NAME'),
                        'screen'      => Loc::getMessage('EXTENDED_SEARCH_LIB_ACCESS_CLOSED_SCREEN'),
                        'aria-hidden' => true,
                    ],
                ),
            ) ,
            array( // ОБЛАСТЬ ПОИСКА
                'type'        => 'radio',
                'name'        => 'searcharea',
                'id'          => 'searcharea',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_LIB_SEARCH_AREA'),
                'tabindex'    => 11,
                'access'      => ['extended', 'library'],
                'value' => array(
                    'books'  => [
                        'name'   => Loc::getMessage('EXTENDED_SEARCH_LIB_SEARCH_AREA_BOOK_NAME'),
                        'screen' => Loc::getMessage('EXTENDED_SEARCH_LIB_SEARCH_AREA_BOOK_SCREEN'),
                    ],
                    'ebooks' => [
                        'name'   => Loc::getMessage('EXTENDED_SEARCH_LIB_SEARCH_AREA_EBOOK_NAME'),
                        'screen' => Loc::getMessage('EXTENDED_SEARCH_LIB_SEARCH_AREA_EBOOK_SCREEN'),
                        'default' => true
                    ],
                ),
            ),

            array( // ПО ПОДБОРКАМ
                'type'     => 'select',
                'name'     => 'newcollection[]',
                'id'       => 'collection',
                'label'    =>  Loc::getMessage('EXTENDED_SEARCH_COLLECTIONS'),
                'tabindex' => 20,
                'access'   => ['extended', 'library'],
                'value'    => self::getSearchCollections(),
                'multiple' => true,
                'langcollection' => Loc::getMessage('EXTENDED_SEARCH_CHOISE_COLLECTION'),
            ),

            array( // ИЗДАТЕЛЬСТВО
                'type'        => 'text',
                'name'        => 'publisher',
                'id'          => 'pubhouse',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_LIB_PUBLISHING'),
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_LIB_PUBLISHING_PH'),
                ),
                'tabindex'    => 21,
                'access'      => ['extended', 'library'],
            ),
            array( // МЕСТО ИЗДАНИЯ
                'type'        => 'text',
                'name'        => 'publishplace',
                'id'          => 'publishplace',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_LIB_PUBPLACE'),
                'placeholder' => array(
                    'library' =>  Loc::getMessage('EXTENDED_SEARCH_LIB_PUBPLACE_PH'),
                ),
                'tabindex'    => 22,
                'access'      => ['extended', 'library'],
            ),
            array( // БИБЛИОТЕКА
                'type'        => 'text',
                'name'        => 'library',
                'id'          => 'library',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_LIB_LIBRARY'),
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_LIB_LIBRARY_PH'),
                ),
                'tabindex'    => 23,
                'access'      => ['extended', 'library'],
            ),
            array( // ISBN
                'type'        => 'text',
                'name'        => 'isbn',
                'id'          => 'isbn',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_LIB_ISBN'),
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_LIB_ISBN_PH'),
                ),
                'tabindex'    => 24,
                'access'      => ['extended', 'library'],
            ),
            array( // КОД ББК
                'type'        => 'text',
                'name'        => 'bbk',
                'id'          => 'bbk',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_LIB_BBK'),
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_LIB_BBK_PH'),
                ),
                'tabindex'    => 25,
                'access'      => ['extended', 'library'],
            ),
            array( // РАЗДЕЛ ББК
                'type'        => 'text',
                'name'        => 'sectionbbk',
                'id'          => 'sectionbbk',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_SECTION_BBK'),
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_SECTION_BBK_PH'),
                ),
                'tabindex'    => 25,
                'access'      => ['extended', 'library'],
            ),
            array( // КОД УД
                'type'        => 'text',
                'name'        => 'ud',
                'id'          => 'ud',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_LIB_UD'),
                'placeholder' => array(
                    'library' =>  Loc::getMessage('EXTENDED_SEARCH_LIB_UD_PH'),
                ),
                'tabindex'    => 26,
                'access'      => ['extended', 'library'],
            ),
            array( // РАЗДЕЛ (ББК)
                'type'     => 'select-checkbox',
                'name'     => 'bbkfull',
                'id'       => 'bbkfull',
                'label'    => Loc::getMessage('EXTENDED_SEARCH_BBK_SECTIONS'),
                'tabindex' => 27,
                'access'   => ['extended', 'library'],
                'value'    => self::getBbk()
            ),

            /** ================================================ МУЗЕИ ============================================ */

            array( // МУЗЕЙ
                'type'        => 'text',
                'name'        => 'museum',
                'id'          => 'museum',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_MUSEUM_MUSEUM'),
                'placeholder' => array(
                    'museum' =>  Loc::getMessage('EXTENDED_SEARCH_MUSEUM_MUSEUM_PH'),
                ),
                'tabindex'    => 30,
                'access'      => ['extended', 'museum'],
            ),
            array( // МАТЕРИАЛ, ТЕХНИКА ИСПОЛНЕНИЯ
                'type'        => 'text',
                'name'        => 'material',
                'id'          => 'material',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_MUSEUM_MATERIALS'),
                'placeholder' => array(
                    'museum' =>  Loc::getMessage('EXTENDED_SEARCH_MUSEUM_MATERIALS_PH'),
                ),
                'tabindex'    => 31,
                'access'      => ['extended', 'museum'],
            ),
            array( // ИНВЕНТАРНЫЙ НОМЕР
                'type'        => 'text',
                'name'        => 'inventorynumber',
                'id'          => 'inventorynumber',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_MUSEUM_INVERNUMBER'),
                'placeholder' => array(
                    'museum' =>  Loc::getMessage('EXTENDED_SEARCH_MUSEUM_INVERNUMBER_PH'),
                ),
                'tabindex'    => 32,
                'access'      => ['extended', 'museum'],
            ),

            /** ================================================ АРХИВЫ ============================================ */

            array( // АРХИВ
                'type'        => 'text',
                'name'        => 'archive',
                'id'          => 'archive',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_ARCHIVE'),
                'placeholder' => array(
                    'archive' =>  Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_ARCHIVE_PH'),
                ),
                'tabindex'    => 41,
                'access'      => ['extended', 'archive'],
            ),
            array( // ФОНД
                'type'        => 'text_extended',
                'name'        => 'fund',
                'id'          => 'fund',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_FOUND'),
                'tabindex'    => 42,
                'access'      => ['extended', 'archive'],
                'input'       => array(
                    '_number'   => [
                        'name'        => Loc::getMessage('EXTENDED_SEARCH_NUMBER_FOUND'),
                        'placeholder' => array(
                            'archive' =>  Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_FOUNDNUMBER_PH'),
                        ),
                    ],
                    '_name' => [
                        'name'        => Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_NAME'),
                        'placeholder' => array(
                            'archive' =>  Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_NAME_PH'),
                        ),
                    ],
                ),
            ),
            array( // ОПИСЬ
                'type'        => 'text_extended',
                'name'        => 'inventory',
                'id'          => 'inventory',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_INVENTORY'),
                'tabindex'    => 43,
                'access'      => ['extended', 'archive'],
                'input'       => array(
                    '_number'   => [
                        'name' => Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_NUMBER'),
                        'placeholder' => []
                    ],
                    '_name' => [
                        'name'        => Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_NAME'),
                        'placeholder' => array(
                            'archive' =>  Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_INVNAME_PH'),
                        ),
                    ],
                ),
            ),
            array( // ДЕЛО
                'type'        => 'text_extended',
                'name'        => 'unit',
                'id'          => 'unit',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_CASE'),
                'tabindex'    => 44,
                'access'      => ['extended', 'archive'],
                'input'       => array(
                    '_number'   => [
                        'name' => Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_NUMBER'),
                        'placeholder' => []
                    ],
                    '_name' => [
                        'name' => Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_NAME'),
                        'placeholder' => array(
                            'archive' =>  Loc::getMessage('EXTENDED_SEARCH_ARCHIVE_UNITNAME_PH'),
                        ),
                    ],
                ),
            ),

            array( // ЯЗЫК (ВСЕ)
                'type'        => 'select',
                'name'        => 'lang',
                'id'          => 'lang',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ALL_LANGUAGE'),
                'tabindex'    => 50,
                'access'      => ['always'],
                'aria-hidden' => true,
                'value'       => self::getSearchLanguages(),
                'attributes'  => ''
            ),

            /** ============================================ ПРАВООБЛАДАТЕЛЬ ======================================= */


            array( // Заявитель
                'type'        => 'text',
                'name'        => 'declarant',
                'id'          => 'declarant',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ALL_DECLARANT'),
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_ALL_DECLARANT_PH_LIBRARY'),
                ),
                'tabindex'    => 21,
                'access'      => ['extended', 'library'],
            ),

            array( // Патентообладатель
                'type'        => 'text',
                'name'        => 'patentholder',
                'id'          => 'patentholder',
                'label'       => Loc::getMessage('EXTENDED_SEARCH_ALL_PATENTHPLDER'),
                'placeholder' => array(
                    'library' => Loc::getMessage('EXTENDED_SEARCH_ALL_PATENTHPLDER_PH_LIBRARY'),
                ),
                'tabindex'    => 21,
                'access'      => ['extended', 'library'],
            ),
        ];

        if ((new CUser())->IsAdmin() || nebUser::checkElar() || EXALEAD_DEBUG_MODE) {
            $arFields[] = array(
                'type'     => 'checkbox',
                'name'     => 'debug',
                'id'       => 'debug',
                'label'    => Loc::getMessage('EXTENDED_SEARCH_DEBUG_MODE'),
                'tabindex' => 100,
                'access'   => ['always'],
                'value'    => 1,
            );
        }

        if (false === mb_stripos($_SERVER['REQUEST_URI'], '/search/extended/')) {
            foreach ($arFields as &$arField) {
                $arField['placeholder'] = is_array($arField['placeholder']) ? [] : '';
            }
        }

        return $arFields;
    }

    /**
     * Получение массива ББК из Exalead
     *
     * @return array|bool|mixed|null|string
     */
    private static function getBbk() {

        $arResult = [];
        $cacheTime = 86400;
        $cacheDir  = CACHE_DIRECTORY . '/' . __CLASS__ . '/'. __FUNCTION__;
        $cacheId   = md5( $cacheDir );

        $obCache = Cache::createInstance();
        if ($obCache->initCache($cacheTime, $cacheId, $cacheDir)) {

            $arResult = $obCache->getVars();

        } elseif ($obCache->startDataCache()) {

            if (!Loader::includeModule('nota.exalead'))
                return $arResult;

            // формирование запроса
            $query = new SearchQuery('bbk_level:1');
            $query->setParam('st', 'st_bbk');
            $query->setParam('hf', '100');
            $query->setParam('of', 'json');
            $query->setParam('sl', 'sl_bbk');
            $query->setQueryType('json');

            // отправка запроса
            $client = new SearchClient();
            $res = $client->getResult($query);

            $arResult = [];
            foreach ($res['hits'] as $arItem) {
                $iId = 0;  $sName = '';
                foreach ($arItem['metas'] as $arItem2) {
                    if ($arItem2['name'] == 'bbk_id') {
                        $iId = $arItem2['value'];
                    }
                    elseif ($arItem2['name'] == 'bbk_fullname') {
                        $sName = $arItem2['value'];
                    }
                }
                if ($iId > 0) {
                    $arResult[$iId] = $sName;
                }
            }

            $obCache->endDataCache($arResult);
        }

        return $arResult;
    }

    /**
     * Список коллекций
     *
     * @return array
     */
    public static function getSearchCollections(){

        $arResult = [];
        $cacheTime = 86400;
        $cacheDir  = CACHE_DIRECTORY . '/' . __CLASS__ . '/'. __FUNCTION__;
        $cacheId   = md5( $cacheDir );

        $obCache = Cache::createInstance();
        if ($obCache->initCache($cacheTime, $cacheId, $cacheDir)) {

            $arResult = $obCache->getVars();

        } elseif ($obCache->startDataCache()) {

            $rsCollections = SearchCollectionsTable::getList([
                'order'  => ['SORT' => 'ASC', 'ID' => 'ASC'],
                'filter' => ['ACTIVE' => 'Y'],
                'select' => ['ID', 'NAME', 'NAME_EN', 'SELECT']
            ]);
            while ($arFields = $rsCollections->fetch()) {
                $arResult[$arFields['ID']] = $arFields;
            }
            unset($rsCollections, $arFields);

            $obCache->endDataCache($arResult);
        }

        return $arResult;
    }

    /**
     * Получение списка языков поиска
     *
     * @return array
     */
    public static function getSearchLanguages(){

        $arResult  = [];
        $cacheTime = 86400;
        $cacheDir  = CACHE_DIRECTORY . '/' . __CLASS__ . '/'. __FUNCTION__;
        $cacheId   = md5( $cacheDir . LANGUAGE_ID );

        $obCache = Cache::createInstance();
        if ($obCache->initCache($cacheTime, $cacheId, $cacheDir)) {

            $arResult = $obCache->getVars();

        } elseif ($obCache->startDataCache()) {

            $rsCollections = SearchLanguagesTable::getList([
                'order'  => [(LANGUAGE_ID == 'en' ? 'NAME_EN' : 'NAME') => 'ASC'],
                'filter' => ['ACTIVE' => 'Y'],
                'select' => ['ID', 'NAME', 'NAME_EN']
            ]);
            while ($arFields = $rsCollections->fetch()) {
                $arResult[$arFields['ID']] = $arFields;
            }
            unset($rsCollections, $arFields);

            $obCache->endDataCache($arResult);
        }

        return $arResult;
    }
}