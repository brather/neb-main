$(function(){
    var $formUpdate = $('#faq-form-update-item'),
        $inpDel = $formUpdate.find('input[name=DELETE_ITEM]'),
        textConfirm = $inpDel.data('confirm');
    $formUpdate.on('submit', function(e){
        if($inpDel.is(':checked')) {
            var isDelete = confirm(textConfirm);
            if(!isDelete) e.preventDefault();
        }
    });
});