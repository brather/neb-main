<?php
/**
 * User: agolodkov
 * Date: 11.03.2016
 * Time: 18:00
 */

namespace Neb\Main;


class UserFormRequest
{

    protected $_fields = [];

    public static function buildRequest($type = null)
    {
        $data = [];
        if ('POST' === $_SERVER['REQUEST_METHOD']) {
            $data = $_POST;
        }
        if (null !== $type) {
            $data['form-type'] = $type;
        }
        if (isset($data['EMAIL'])) {
            $data['LOGIN'] = $data['EMAIL'];
        }

        return new static($data);
    }

    public function __construct($data)
    {
        switch (isset($data['form-type']) ? $data['form-type'] : 'simple') {
            case 'simple':
                $this->applyFields($data);
                break;
            case 'register.form':
                $this->applyFields($this->extractRegisterForm($data));
                break;
        }
    }

    public function getAction()
    {
        return isset($this->_fields['action'])
            ? $this->_fields['action']
            : null;
    }

    public function extractProfileFields($data)
    {
        return $data;
    }

    public function extractRegisterForm($data)
    {
        if (isset($data['scan1'])
            && file_exists(
                $_SERVER['DOCUMENT_ROOT'] . $data['scan1']
            )
        ) {
            $data['UF_SCAN_PASSPORT1'] = \CFile::MakeFileArray($data['scan1']);
        }
        if (isset($data['scan2'])
            && file_exists(
                $_SERVER['DOCUMENT_ROOT'] . $data['scan2']
            )
        ) {
            $data['UF_SCAN_PASSPORT2'] = \CFile::MakeFileArray($data['scan2']);
        }
        $data = array_merge($data, $data['REGISTER']);

        return $data;
    }

    public function applyFields($fields)
    {
        if (isset($fields['EMAIL'])) {
            $fields['LOGIN'] = $fields['EMAIL'];
        }
        $this->_fields = $fields;
    }

    /**
     * @param array $group
     *
     * @return array
     */
    public function getRequestFieldsGroup($group)
    {
        return array_intersect_key(
            $this->_fields,
            \nebUser::getFieldGroupKeys($group)
        );
    }


    public function getFields()
    {
        return $this->_fields;
    }

    public function getField($name)
    {
        return isset($this->_fields[$name])
            ? $this->_fields[$name]
            : null;
    }
}