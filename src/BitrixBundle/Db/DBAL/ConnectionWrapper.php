<?php
/**
 * User: agolodkov
 * Date: 03.10.2016
 * Time: 17:32
 */

namespace BitrixBundle\Db\DBAL;


use BitrixBundle\Db\DBAL\Driver\Mysqli\MysqliConnection;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Event\ConnectionEventArgs;

class ConnectionWrapper extends Connection
{
    const EVENT_PRE_CONNECT = 'preConnect';

    /**
     * @var bool
     */
    protected $_isConnected = false;

    /**
     * @return bool
     */
    public function connect()
    {
        if ($this->isConnected() || $this->_isConnected) {
            return false;
        }

        if ($this->_eventManager->hasListeners(static::EVENT_PRE_CONNECT)) {
            $eventArgs = new ConnectionEventArgs($this);
            $this->_eventManager->dispatchEvent(static::EVENT_PRE_CONNECT, $eventArgs);
        }

        if ($this->_isConnected) {
            return false;
        }

        return parent::connect();
    }

    /**
     * @param MysqliConnection $conn
     */
    public function setConn(MysqliConnection $conn)
    {
        $this->_conn = $conn;
    }

    /**
     * @param boolean $isConnected
     */
    public function setIsConnected($isConnected)
    {
        $this->_isConnected = $isConnected;
    }

    /**
     * @return bool
     */
    public function isConnected()
    {
        return $this->_isConnected;
    }

    /**
     * Closes the connection.
     *
     * @return void
     */
    public function close()
    {
        unset($this->_conn);

        $this->_isConnected = false;
    }
}