<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
\Bitrix\Main\Page\Asset::getInstance()->addJs('/local/templates/adaptive/js/accessors.js');
\Bitrix\Main\Page\Asset::getInstance()->addCss('/local/templates/adaptive/css/accessors.css');
echo \Neb\Main\Helper\SymfonyHelper::getInstance()
	->forwardController('AssessorBundle:AuthorRating:list')
	->getContent();
