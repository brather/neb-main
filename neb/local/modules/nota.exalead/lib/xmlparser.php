<?
namespace Nota\Exalead;

/**
 * Class XmlParser - Распарсиваем XML ответ
 * @package Nota\Exalead
 */
class XmlParser
{
    private $xml = '';
    public $arResult = Array();

    public function __construct($strXml)
    {
        if (empty($strXml))
            return false;
        $this->xml = simplexml_load_string($strXml);
    }

    public function getXml()
    {
        return $this->xml;
    }

    /**
     * @return bool
     */
    public function search()
    {
        if (empty($this->xml))
            return false;

        if (isset($this->xml->spellcheckSuggestions)):

            $string = (string)$this->xml->spellcheckSuggestions->SpellCheckSuggestions->attributes()->origStr;

            foreach ($this->xml->spellcheckSuggestions->SpellCheckSuggestions->SpellCheckSuggestion->SpellCheckSubStringSuggestion as $str):
                $this->arResult['spellcheckSuggestion'][] = (string)$str->attributes()->str;
                $offset = (int)$str->attributes()->offset;
                $this->arResult['spellcheckSuggestionOrig'][] = mb_substr($string, $offset, (string)$str->attributes()->length, 'UTF-8');

            endforeach;

        else:
            $this->arResult['spellcheckSuggestion'] = '';
        endif;

        $this->arResult['COUNT'] = $this->xml->attributes();
        $this->arResult['COUNT'] = (int)$this->arResult['COUNT']['nmatches'];

        $this->arResult['NHITS'] = $this->xml->attributes();
        $this->arResult['NHITS'] = (int)$this->arResult['NHITS']['nhits'];

        $this->arResult['START_ITEM'] = $this->xml->attributes();
        $this->arResult['START_ITEM'] = (int)$this->arResult['START_ITEM']['start'];

        if (!empty($this->xml->hits->Hit))
        {
            /* Книги */
            $arItems = array();
            $number = 0;

            foreach ($this->xml->hits->Hit as $hit) {

                $number++;
                $arItem = array();

                $arItem['NUM_ITEM'] = $this->arResult['START_ITEM'] + $number;

                foreach ($hit->groups->AnswerGroup as $agroup) {
                    /* @var $agroup \SimpleXMLElement */
                    $agroupAtr = $agroup->attributes();

                    switch ( $agroupAtr['id'] )
                    {
                        case 'publisher':
                            $arItem['r_publisher'] = $agroup->categories->Category->attributes();
                            $arItem['r_publisher'] = (string)$arItem['r_publisher']['id'];
                            break;
                        case 'authorbook':
                            foreach ($agroup->categories->Category as $category):
                                /* @var $category \SimpleXMLElement */
                                $authorbook = $category->attributes();
                                $arItem['r_authorbook'][str_replace(' ', '', (string)$authorbook['title'])] = (string)$authorbook['id'];
                            endforeach;
                            break;
                        case 'library':
                            $arItem['r_library'] = $agroup->categories->Category->attributes();
                            $arItem['r_library'] = (string)$arItem['r_library']['id'];
                            break;
                        case 'bbkfull':
                            foreach ($agroup->categories->Category as $category):

                                $bbk = array('id' => (string)$category['id'],
                                             'title' => (string)$category['title']);
                                $category_ = $category->Category;

                                while ($category_) {

                                    $bbk['category'][] = array('id' => (string)$category_['id'],
                                                               'title' => (string)$category_['title']);
                                    $category_ = $category_->Category;
                                }
                                //$arItem['bbkfull'][] = $bbk;
                            endforeach;
                            break;
                        case 'bbk_bbkfull':
                            foreach ($agroup->categories->Category as $category):

                                $bbk = array('id' => (string)$category['id'],
                                             'title' => (string)$category['title']);
                                $category_ = $category->Category;

                                while ($category_) {

                                    $bbk = array('id' => (string)$category_['id'],
                                                 'title' => (string)$category_['title']);
                                    $category_ = $category_->Category;
                                }
                                $arItem['bbkfull'] = $bbk;
                            endforeach;
                            break;
                        case 'udkfull':
                            foreach ($agroup->categories->Category as $category):

                                $udk = array('id' => (string)$category['id'],
                                             'title' => (string)$category['title']);
                                $category_ = $category->Category;

                                while ($category_) {

                                    $udk['category'][] = array('id' => (string)$category_['id'],
                                                               'title' => (string)$category_['title']);
                                    $category_ = $category_->Category;
                                }
                                //$arItem['udkfull'][] = $udk;
                            endforeach;
                            break;
                    }
                }

                foreach ($hit->metas->Meta as $meta)
                {
                    /* @var $meta \SimpleXMLElement */
                    $metaAtr = $meta->attributes();

                    $metaAtrName = strval($metaAtr['name']);

                    if (empty($arItem['filename']) && $metaAtrName == 'file_size' && (int)$meta->MetaString > 0) {
                        /**
                         * Заглушка, чтобы отображалась ссылка на PDF
                         */
                        $arItem['fileExt'] = array('pdf');
                    }

                    switch ( $metaAtrName )
                    {
                        case 'text':
                            foreach ($meta->MetaText->TextSeg as $text)
                            {
                                /* @var $text \SimpleXMLElement */
                                $highlighted = $text->attributes();
                                $highlighted = (string)$highlighted['highlighted'];

                                // убрать лишние символы. Доработка для РГБ
                                $text = preg_replace_callback(
                                    "/[^A-zА-яЁёÄäÖöÜüß\(\.,;:!?\-\)іѲѳѢѣѵѴѣ\d\s]|[\]\[\^]/u",
                                    function ($matches) { for ($i = 0; $i < count($matches); $i++) return ''; },
                                    $text
                                );

                                if (!is_array($text))
                                {
                                    if ($highlighted == 'true')
                                    {
                                        $arItem['text'] .= ' <strong>' . (string)$text . '</strong> ';
                                        $arItem['text_strong'] = true;
                                    }
                                    else
                                        $arItem['text'] .= ' ' . (string)$text . ' ';
                                }
                            }
                            break;
                        case 'title':
                            foreach ($meta->MetaText->TextSeg as $text)
                            {
                                /* @var $title \SimpleXMLElement */
                                $highlighted = $text->attributes();
                                $highlighted = (string)$highlighted['highlighted'];

                                if (!is_array($text))
                                {
                                    if ($highlighted == 'true')
                                    {
                                        $arItem['title'] .= ' <strong>' . (string)$text . '</strong> ';
                                        $arItem['title_strong'] = true;
                                        $this->arResult['title_strong'] = true;
                                    }
                                    else
                                        $arItem['title'] .= ' ' . (string)$text . ' ';
                                }
                            }
                            break;
                        case 'bbk_bbkfull':
                            foreach ($meta->MetaText->TextSeg as $text)
                            {
                                /* @var $text \SimpleXMLElement */
                                $highlighted = $text->attributes();
                                $highlighted = (string)$highlighted['highlighted'];

                                if (!is_array($text))
                                {
                                    if ($highlighted == 'true')
                                    {
                                        $arItem['bbk_bbkfull'] .= ' <strong>' . (string)$text . '</strong> ';
                                        $arItem['bbkfull_strong'] = true;
                                        $this->arResult['bbkfull_strong'] = true;
                                    }
                                    else
                                        $arItem['bbk_bbkfull'] .= ' ' . (string)$text . ' ';
                                }
                            }
                            break;
                        case 'bbkfull':
                            $i = 0;

                            foreach ($meta->MetaText as $textSeg)
                            {
                                foreach ($textSeg->TextSeg as $text)
                                {
                                    $highlighted = $text->attributes();
                                    $highlighted = (string)$highlighted['highlighted'];

                                    if (!is_array($text->TextSeg))
                                    {
                                        $text = str_replace('/', '|', (string)$text);
                                        $arItem['r_bbkfull'][$i] .= mb_strtolower($text);

                                        if ($highlighted == 'true')
                                        {
                                            $arItem['bbkfull'][$i] .= ' <strong>' . $text . '</strong> ';
                                            $this->arResult['bbkfull_strong'] = true;
                                        }
                                        else
                                        {
                                            $arItem['bbkfull'][$i] .= ' ' . $text . ' ';
                                        }

                                    }
                                }
                                $arItem['r_bbkfull'][$i] = explode('|', $arItem['r_bbkfull'][$i]);
                                $arItem['bbkfull'][$i] = explode('|', $arItem['bbkfull'][$i]);
                                $i++;
                            }
                            break;
                        case 'udkfull':
                            $i = 0;

                            foreach ($meta->MetaText as $textSeg)
                            {
                                foreach ($textSeg->TextSeg as $text)
                                {
                                    if (!is_array($text->TextSeg))
                                    {
                                        $arItem['r_udkfull'][$i] .= mb_strtolower($text);
                                        $arItem['udkfull'][$i] .= ' ' . $text . ' ';
                                    }
                                }

                                $arItem['r_udkfull'][$i] = explode('/', $arItem['r_udkfull'][$i]);
                                $arItem['udkfull'][$i] = explode('/', $arItem['udkfull'][$i]);
                                $i++;
                            }
                            break;
                        case 'librarytype':
                            $arItem['librarytype'] = mb_strtolower(strval($meta->MetaString), 'UTF-8');
                            break;
                        case 'authorbook':
                            if ($meta->MetaString) {
                                $arItem['authorbook'] = (string)$meta->MetaString;
                            }
                            elseif ($meta->MetaText->TextSeg)
                            {
                                foreach ($meta->MetaText as $metaText):
                                    $authorbook = '';
                                    foreach ($metaText->TextSeg as $text):
                                        $highlighted = $text->attributes();
                                        $highlighted = (string)$highlighted['highlighted'];

                                        if (!is_array($text))
                                        {
                                            // логика соединения слов через пробелы
                                            if (strlen($authorbook) > 0)
                                            {
                                                $sLastChar = substr($authorbook, strlen($authorbook) - 1, 1);
                                                $sSecondChar = substr((string)$text, 0, 1);
                                                $sAlphabet = '[а-яА-Яa-zA-Z]'; // Ru, En, De alphabet

                                                // если предыдущий символ не буква и текущий буква - то соединяем через пробел
                                                if (!preg_match($sAlphabet, $sLastChar) && preg_match($sAlphabet, $sSecondChar))
                                                    $authorbook .= ' ';
                                            }

                                            if ($highlighted == 'true')
                                            {
                                                $authorbook .= '<strong>' . (string)$text . '</strong>';
                                                $arItem['authorbook_strong'] = true;
                                                $this->arResult['authorbook_strong'] = true;
                                            }
                                            else
                                                $authorbook .= (string)$text;
                                        }
                                    endforeach;

                                    $arItem['authorbook'][] = $authorbook;
                                    $arItem['authorbook_for_link'][] = $authorbook;

                                endforeach;

                                if (count($arItem['authorbook']) > 0)
                                    $arItem['authorbook'] = implode(', ', $arItem['authorbook']);

                            }
                            break;
                        case 'filename':        /****************************/
                        case 'viewurl':         /*          Logic OR        */
                        case 'file_name':       /****************************/

                            $arItem['filename'] = (string)$meta->MetaString;

                            if ( !empty( $arItem['filename'] ) )
                            {
                                $arTmpFile = explode( ' ', $arItem['filename'] );
                                TrimArr( $arTmpFile );

                                if ( !empty( $arTmpFile ) )
                                {
                                    foreach ( $arTmpFile as $file )
                                    {
                                        $ext = GetFileExtension( $file );

                                        if ( !empty( $ext ) )
                                            $arItem['fileExt'][] = $ext;
                                    }

                                    if ( !empty( $arItem['fileExt'] ) )
                                        $arItem['fileExt'] = array_unique( $arItem['fileExt'] );
                                }
                            }
                            break;
                        case 'id':
                            $arItem['id'] = (string)$meta->MetaString;
                            $arItem['~id'] = urlencode(str_replace('/', '!|', $arItem['id']));
                            break;
                        case 'collection':
                        case 'mpk':
                        case 'declarant':

                            $arMeta = (array)$meta;
                            $MetaString = $arMeta['MetaString'];

                            if (!is_array($MetaString))
                                $MetaString = array($MetaString);
                            foreach ($MetaString as $k => $coll) {
                                if (is_array($coll))
                                    unset($MetaString[$k]);
                            }

                            if (in_array($metaAtrName, ['mpk', 'declarant'])) {
                                $MetaString = implode(', ', $MetaString);
                            }

                            $arItem[$metaAtrName] = $MetaString;

                            break;
                        case 'file_extension':
                            $_tmpArrVideo   = array ('flv', 'avi', 'mp4', 'webm');
                            $_tmpArrAudio   = array ('mp3', 'ogg');

                            $_isAudio = $_isVideo = false;

                            if ( ($_isAudio = in_array( (string)$meta->MetaString, $_tmpArrAudio )) || ($_isVideo = in_array( (string)$meta->MetaString, $_tmpArrVideo )) )
                            {
                                $arItem['fileExt'] = array ((string)$meta->MetaString);

                                $arItem['IS_READ'] = "N";
                                $arItem['IS_MEDIA'] = "Y";
                                $arItem['IS_MEDIA_AUDIO'] = $_isAudio;
                                $arItem['IS_MEDIA_VIDEO'] = $_isVideo;
                            }

                            unset($_tmpArrAudio, $_tmpArrVideo);
                            break;
                        case 'filesize':
                            $arItem['filesize'] = (int)$meta->MetaString;
                            $arItem['file_size'] = $arItem['filesize'];
                            break;
                        case 'keywords':
                            $arMeta = (array)$meta;
                            foreach ($arMeta['MetaText'] as $textSeg)
                                $arItem['keywords'][] = trim((string)$textSeg->TextSeg);
                            break;
                        case 'publish_date':
                            $arItem[$metaAtrName] = (string)$meta->MetaString;
                            if (count(explode('/', $arItem[$metaAtrName])) == 3) {
                                $arItem[$metaAtrName] = date('d.m.Y', MakeTimeStamp($arItem[$metaAtrName], "YYYY/MM/DD"));;
                            }
                            break;
                        default:
                            $arItem[$metaAtrName] = (string)$meta->MetaString;
                            break;
                    }
                }


            {// todo: Пока издания формата EPub не имеют указания на себя (file_extension) будет этот костыль
               /* if ( isset( $arItem["externallink"] ) && !empty( $arItem["externallink"] ) )
                {
                    $externalLinkExtension = substr( $arItem["externallink"], strripos( $arItem["externallink"], '.' ) + 1 );

                    if ( mb_strtoupper($externalLinkExtension) === "EBUP" )
                    {
                        $arItem["fileExt"] = array( $externalLinkExtension );
                    }
                }*/
            }

                $arItem['URL'] = $hit->attributes();
                $arItem['URL'] = (string)$arItem['URL']['url'];
                $arItem['source'] = $hit->attributes();
                $arItem['source'] = (string)$arItem['source']['source'];

                # ссылка на закрытый просматровщик
                /*if($arItem['isprotected'] == 1 and $arItem['filesize'] > 0){
                    $arItem['PROTECTED_URL'] = CLOSED_VIEWER_LINK.$arItem['~id'];
                }*/

                /* Проверка на чтение книги - показывать ли кнопку ЧИТАТЬ*/
                #if($arItem['filesize'] > 0 and $arItem['filesize'] < 100000000)
                if ($arItem['filesize'] > 0 && !isset($arItem['IS_READ'])) {
                    $arItem['IS_READ'] = 'Y';
                }

                /*if(!empty($arItem['collection'])){

                    if(is_array($arItem['collection']))
                        $strCollection = implode(',', $arItem['collection']);
                    else
                        $strCollection = $arItem['collection'];

                    if($arItem['idlibrary'] == 199  and strpos('text'.$strCollection, 'open') === false){
                        $arItem['isprotected'] = 1;
                        $arItem['IS_READ'] = 'N';
                    }
                    if($arItem['idlibrary'] == 200  and strpos('text'.$strCollection, 'защищенные авторским правом') !== false){
                        $arItem['isprotected'] = 1;
                        $arItem['IS_READ'] = 'N';
                    }
                }*/

                if ($arItem['library'] == 'РГБ') {
                    $arItem['source'] = 'RGB';
                }

                if ($arItem['source'] == 'VideoData') {
                    $arItem['VIDEO_URL'] = '/catalog/' . $arItem['~id'] . '/video/';
                }

                $arItem['DETAIL_PAGE_URL'] = '/catalog/' . $arItem['~id'] . '/';

                #if($arItem['idlibrary'] == 200)
                #$arItem['VIEWER_URL'] = 'http://electronic-library.ru/?id='.urlencode($arItem['id']);
                #else
                $arItem['VIEWER_URL'] = '/catalog/' . $arItem['~id'] . '/viewer/';
                $arItem['PROTECTED_URL'] = CLOSED_VIEWER_LINK . $arItem['~id'];

                /*
                если $arItem['idlibrary'] == 199 и $arItem['filesize'] > 0 то тянем картинку с сервиса доступа
                иначе из Экзалиды
                */
                if ($arItem['idlibrary'] == 199 and $arItem['filesize'] > 0) {
                    $arItem['IMAGE_URL'] = '/local/tools/exalead/thumbnail.php?url=' . $arItem['URL']
                        . '&source=OUR_SERVICE';
                } else {
                    $arItem['IMAGE_URL'] = '/local/tools/exalead/thumbnail.php?url=' . $arItem['URL'] . '&source='
                        . $arItem['source'];
                }
                /*
                обрабатываем логику, - если нет поискового запроса, то нет выделенного текста. И если есть аннотация то показываем ее вместо текста
                ну и если нет текста, а есть аннотация то показываем ее
                */
                if (empty($arItem['text']) and !empty($arItem['annotation'])) {
                    $pos = strpos($arItem['annotation'], ' ', 200);
                    $arItem['text'] = substr($arItem['annotation'], 0, $pos);
                }

                $arItem['readBookId'] = $arItem['id'];
                $arItem['viewerOptions'] = array(
                    'title'  => trim(strip_tags($arItem['title'])),
                    'author' => trim($arItem['authorbook']),
                );
                if (!empty($arItem['idbook'])) {
                    $arItem['readBookId'] = $arItem['idbook'];
                    $arItem['viewerOptions']['page'] = $arItem['startpageparts'];
                    $arItem['viewerOptions']['positionpart'] = $arItem['positionpart'];
                }

                $arItems[] = $arItem;
            }

            $this->arResult['ITEMS'] = $arItems;
        }

        /*
        Группы для фильтрации
        */
        $arGroupsKeyVal = array();
        $arGroups = array();
        foreach ($this->xml->groups->AnswerGroup as $group) {
            $attributes = $group->attributes();
            $id = strtoupper((string)$attributes['id']);

            $arGroup = array();

            if ($id == 'YEARRANGE')
                $selector = $group->categories->Category;
            else
                $selector = $group->categories->Category;

            $i = 0;

            foreach ($selector as $cat) {

                $cat_attributes = $cat->attributes();

                $title = (string)$cat_attributes['title'];

                if ($title == 'Книга')
                    $title = 'Книг';

                $idGroup = $cat->attributes();
                $idGroup = (string)$idGroup['id'];
                $countGroup = $cat->attributes();
                $countGroup = (string)$countGroup['count'];

                $data = array();

                if ('COLLECTION_NEW' == $id):
                    foreach ($cat as $key => $ccc):
                        $data[] = array(
                            'title' => (string)$ccc->attributes()->title,
                            'data' => (string)$ccc->attributes()->count,
                        );
                    endforeach;
                endif;
                if (('TERMS' == $id) or ('YEARRANGE' == $id)):

                    foreach ($cat as $key => $ccc):
                        $data[] = array(
                            'title' => (string)$ccc->attributes()->title,
                            'data' => (string)$ccc->attributes()->count,
                            'id' => (string)$ccc->attributes()->id,
                        );
                    endforeach;

                endif;


                $arGroup[] = array(
                    'id' => $idGroup,
                    'count' => $countGroup,
                    'title' => $title,
                    'data' => $data,
                );
                $arGroupsKeyVal[$idGroup] = $i;

                if ('BBKFULL' == $id || 'UDKFULL' == $id):

                    foreach ($cat as $key => $ccc):
                        $i++;
                        $arGroup[] = array(
                            'id' => $idGroup,
                            'count' => $countGroup,
                            'title' => (string)$ccc->attributes()->title
                        );

                        $arGroupsKeyVal[(string)$ccc->attributes()->id] = $i;

                        $category_ = $ccc->Category;
                        while ($category_) {
                            $i++;
                            $arGroup[] = array(
                                'id' => $idGroup,
                                'count' => $countGroup,
                                'title' => (string)$category_->attributes()->title
                            );
                            $arGroupsKeyVal[(string)$category_->attributes()->id] = $i;
                            $category_ = $category_->Category;
                        }

                    endforeach;

                endif;
                $i++;
            }


            if ('BBKFULL' === $id)
            {
                if( count( $arGroups['BBKFULL_STAT'] ) <= 0 && $group->categories instanceof \SimpleXMLElement )
                {
                    $arGroups['BBKFULL_STAT'] = array();

                    self::getTreeBBKfull( $group->categories, $arGroups['BBKFULL_STAT'] );
                }
            }

            $arGroups[$id] = $arGroup;
        }


        $this->arResult['GROUPS_KEY_VAL'] = $arGroupsKeyVal;
        $this->arResult['GROUPS'] = $arGroups;

    }

    /* @param $categories \SimpleXMLElement */
    private static function getTreeBBKfull ( \SimpleXMLElement $categories, &$arr = array() )
    {
        /* @var $category \SimpleXMLElement */

        foreach ( $categories->Category as $category )
        {
            $arr[] = array(
                'title'     => (string)$category->attributes()->title,
                'count'     => (int)$category->attributes()->count,
                'path'      => (string)$category->attributes()->path,
                'pathId'    => (string)$category->attributes()->id,
                'fullPath'  => (string)$category->attributes()->fullPath
            );

            if ($category->Category instanceof \SimpleXMLElement)
            {
                self::getTreeBBKfull($category, $arr);
            }
        }
    }

    /**
     * Может быть вы имели в виду
     *
     * @return bool
     */
    public function spellcheck()
    {
        if (empty($this->xml->SpellCheckSuggestion))
            return false;
        $this->arResult['REFINEMENT'] = $this->xml->SpellCheckSuggestion->attributes();
        $this->arResult['REFINEMENT'] = (string)$this->arResult['REFINEMENT']['newStr'];
    }

    /**
     * Автокомплит
     *
     * @return bool
     */
    public function autocomplete()
    {
        if (count($this->xml->SuggestXMLEntry) <= 0)
            return false;

        foreach ($this->xml->SuggestXMLEntry as $v) {
            $entry = $v->attributes();
            $entry = (string)$entry['entry'];
            $this->arResult[] = $entry;
        }
    }
}