<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$this->SetViewTarget('lib_menu', $pos);
include_once('menu.php');
$this->EndViewTarget();

?>

<div class="row">
    <div class="col-md-3 col-sm-3 lk-sidebar">
        <?$APPLICATION->ShowViewContent('lib_menu')?>
        <?
        $APPLICATION->IncludeComponent(
            "neb:library.right_counter",
            "",
            Array(
                "IBLOCK_ID" => IBLOCK_ID_LIBRARY,
                "LIBRARY_ID" => $arResult["VARIABLES"]["ID"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "COLLECTION_URL" => $arParams['COLLECTION_URL'],
            ),
            $component
        );
        ?>
    </div>
    <div class="col-md-9 col-sm-9 lk-content">
        <?
        global $arrFilterLibraryNews;
        $arrFilterLibraryNews = array('PROPERTY_LIBRARY' => $arResult['VARIABLES']['ID'], '!PROPERTY_LIBRARY' => false);

        $APPLICATION->IncludeComponent("bitrix:news.list", "library_news", Array(
            "IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
            "IBLOCK_ID" => "3",	// Код информационного блока
            "NEWS_COUNT" => "20",	// Количество новостей на странице
            "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
            "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
            "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
            "FILTER_NAME" => "arrFilterLibraryNews",	// Фильтр
            "FIELD_CODE" => array(	// Поля
                0 => "NAME",
                1 => "PREVIEW_TEXT",
            ),
            "PROPERTY_CODE" => array(	// Свойства
            ),
            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => str_replace('#CODE#', $arResult['VARIABLES']["CODE"], $arResult["FOLDER"].$arResult["URL_TEMPLATES"]['news_detail']),	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CACHE_TIME" => $arParams["CACHE_TIME"],	// Время кеширования (сек.)
            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N",	// Учитывать права доступа
            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
            "ACTIVE_DATE_FORMAT" => "j F",	// Формат показа даты
            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
            "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
            "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
            "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
            "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "PARENT_SECTION" => "",	// ID раздела
            "PARENT_SECTION_CODE" => "",	// Код раздела
            "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
            "DISPLAY_DATE" => "Y",	// Выводить дату элемента
            "DISPLAY_NAME" => "Y",	// Выводить название элемента
            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
            "PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
            "PAGER_TITLE" => "Новости",	// Название категорий
            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
        ),
            $component
        );?>
    </div>
</div>
