<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init(['popup']);

if(!empty($arResult['AUTH_SERVICES']))
{
	foreach ($arResult['AUTH_SERVICES'] as $serviceName => $service)
	{
		switch($serviceName)
		{
			case 'Facebook':
				$cssClass = 'fblink';
				break;
			case 'VKontakte':
				$cssClass = 'vklink';
				break;
			case 'Odnoklassniki':
				$cssClass = 'odnlink';
				break;
			default:
				$cssClass = '';
				break;
		}

		$arResult['AUTH_SERVICES'][$serviceName]['CSS_CLASS'] = $cssClass;
		$service['FORM_HTML'] = str_replace('660, 425', '730, 580', $service['FORM_HTML']);

		preg_match_all('/onclick="(.+?)"/i', $service['FORM_HTML'], $onclickLink, PREG_SET_ORDER);
		$arResult['AUTH_SERVICES'][$serviceName]['AUTH_LINK'] = $onclickLink[0][1];

	}
	$arResult['AUTH_SERVICES']['VKontakte']['FORM_HTML'] = str_replace(array(',notify', ',email'), '', $arResult['AUTH_SERVICES']['VKontakte']['FORM_HTML']);
	$arResult['AUTH_SERVICES']['VKontakte']['AUTH_LINK'] = str_replace(array(',notify', ',email'), '', $arResult['AUTH_SERVICES']['VKontakte']['AUTH_LINK']);
}

if(isset($_SESSION["EXTERNAL_AUTH_ERROR"]) && !empty($_SESSION["EXTERNAL_AUTH_ERROR"]))
{
	$arResult['ERROR_MESSAGE'] = $_SESSION["EXTERNAL_AUTH_ERROR"];
	unset($_SESSION["EXTERNAL_AUTH_ERROR"]);
}
