<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader,
    Bitrix\NotaExt\Iblock,
    Bitrix\NotaExt\Ext\DetailGallery\Params as DGParams,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('notaext');
Loader::includeModule('iblock');

$error = false;
$arResult = array();

if ($_SERVER['REQUEST_METHOD'] != 'POST' and !check_bitrix_sessid() and !empty($_REQUEST['bgitems']))
    $error = true;
if (!Bitrix\NotaExt\Ext\DetailGallery\Access::check())
    $error = true;

if (!$error) {
    /*
    ID Инфоблока для сохранения
    */
    $IBLOCK_ID = DGParams::getParam('save_ib');
    if (intval($IBLOCK_ID) <= 0)
        die('set iblock to save file');

    /*
    Получаем все базовые теги
    */
    $arBasicTags = array();
    $arFilter = array('IBLOCK_ID' => Iblock\IblockTools::getIBlockId('basic_tags'));
    $arSelect = array('ID', 'NAME', 'IBLOCK_ID');

    $arResBasicTags = Iblock\Element::getList($arFilter, false, $arSelect);
    if (!empty($arResBasicTags)) {
        foreach ($arResBasicTags['ITEMS'] as $arItem)
            $arBasicTags[$arItem['ID']] = $arItem['~NAME'];
    }
    /*
    Дополнительные теги
    */
    $arAdditionalTags = array();
    $arAdditionalTagsName = array();
    $arFilter = array('IBLOCK_ID' => Iblock\IblockTools::getIBlockId('additional_tags'));
    $arSelect = array('ID', 'NAME', 'IBLOCK_ID');

    $arResAdditionalTags = Iblock\Element::getList($arFilter, false, $arSelect);
    if (!empty($arResAdditionalTags)) {
        foreach ($arResAdditionalTags['ITEMS'] as $arItem) {
            $arAdditionalTags[$arItem['ID']] = $arItem['~NAME'];
            $arAdditionalTagsName[$arItem['~NAME']] = $arItem['ID'];
        }
    }

    /*
    Обрабатываем общие теги
    */
    $arBasicTagsAll = array();
    $arAdditionalTagsAll = array();
    if (!empty($_REQUEST['basic-tags-all']) and is_array($_REQUEST['basic-tags-all'])) {
        foreach ($_REQUEST['basic-tags-all'] as $tag) {
            $tag = trim($tag);
            /*	   Если задан ID тега 		*/
            if (intval($tag) > 0 and strlen($tag) == strlen(intval($tag))) {
                if (!empty($arBasicTags[$tag]))
                    $arBasicTagsAll[] = $tag;
                elseif ($arAdditionalTags[$tag])
                    $arAdditionalTagsAll[] = $tag;
            } else # Если задан новый дополнительный тег
            {
                # Если такой тег уже добавлен
                if (empty($arAdditionalTagsName[$tag])) {
                    $el = new CIBlockElement;
                    $arLoadArray = array
                    (
                        "MODIFIED_BY" => $USER->GetID(),
                        "IBLOCK_SECTION_ID" => false,
                        "IBLOCK_ID" => Iblock\IblockTools::getIBlockId('additional_tags'),
                        "NAME" => $tag,
                        "ACTIVE" => "Y",
                    );
                    $elementId = $el->Add($arLoadArray);
                    $arAdditionalTagsAll[] = $elementId;
                } else
                    $arAdditionalTagsAll[] = $arAdditionalTagsName[$tag];
            }

        }
    }

    /*
    Проверяем есть ли свойство для хранения оригинала
    */

    $properties = CIBlockProperty::GetList(Array(), Array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID, 'CODE' => 'REAL_PICTURE'));
    if ($properties->SelectedRowsCount() <= 0) {
        $arFields = Array(
            "NAME" => "Original file",
            "ACTIVE" => "Y",
            "SORT" => "1000",
            "CODE" => "REAL_PICTURE",
            "PROPERTY_TYPE" => "F",
            "IBLOCK_ID" => $IBLOCK_ID
        );
        $ibp = new CIBlockProperty;
        $PropID = $ibp->Add($arFields);
    }

    $arAddedItem = array();
    foreach ($_REQUEST['bgitems'] as $itemId) {
        /*
        Добавляем фото в ИБ
        */
        $el = new CIBlockElement;
        $PROP = array(
            'REAL_PICTURE' => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $_REQUEST['originalfile'][$itemId]),
        );
        if (!empty($_REQUEST['LICENCE_EXPIRATION'][$itemId]))
            $PROP['LICENCE_EXPIRATION'] = ConvertTimeStamp(strtotime($_REQUEST['LICENCE_EXPIRATION'][$itemId]), 'SHORT');

        if (!empty($_REQUEST['LICENCE'][$itemId]))
            $PROP['LICENCE'] = $_REQUEST['LICENCE'][$itemId];

        if (!empty($_REQUEST['CREDIT'][$itemId]))
            $PROP['CREDIT'] = $_REQUEST['CREDIT'][$itemId];

        /*			 ОБрабатываем теги отдельной фото	*/

        $arBasicTagsItem = array();
        $arAdditionalTagsItem = array();

        if (!empty($_REQUEST['DG_TAGS'][$itemId])) {
            foreach ($_REQUEST['DG_TAGS'][$itemId] as $tag) {
                $tag = trim($tag);
                /*	   Если задан ID тега 		*/
                if (intval($tag) > 0 and strlen($tag) == strlen(intval($tag))) {
                    if (!empty($arBasicTags[$tag]))
                        $arBasicTagsItem[] = $tag;
                    elseif ($arAdditionalTags[$tag])
                        $arAdditionalTagsItem[] = $tag;
                } else # Если задан новый дополнительный тег
                {
                    # Если такой тег уже добавлен
                    if (empty($arAdditionalTagsName[$tag])) {
                        $el = new CIBlockElement;
                        $arLoadArray = array
                        (
                            "MODIFIED_BY" => $USER->GetID(),
                            "IBLOCK_SECTION_ID" => false,
                            "IBLOCK_ID" => Iblock\IblockTools::getIBlockId('additional_tags'),
                            "NAME" => $tag,
                            "ACTIVE" => "Y",
                        );
                        $elementId = $el->Add($arLoadArray);
                        $arAdditionalTagsItem[] = $elementId;
                    } else
                        $arAdditionalTagsItem[] = $arAdditionalTagsName[$tag];
                }
            }
            if (!empty($arBasicTagsItem) or !empty($arBasicTagsAll))
                $PROP['BASIC_TAGS'] = array_merge($arBasicTagsItem, $arBasicTagsAll);

            if (!empty($arAdditionalTagsItem) or !empty($arAdditionalTagsAll))
                $PROP['ADDITIONAL_TAGS'] = array_merge($arAdditionalTagsItem, $arAdditionalTagsAll);

        }


        $arResizePicture = Bitrix\NotaExt\ImgConvert::Resize($_REQUEST['originalfile'][$itemId], DGParams::getParam('default_w'), DGParams::getParam('default_h'), false, 98, '/upload/resize_cache_dg/');

        $arLoadArray = array
        (
            "MODIFIED_BY" => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $IBLOCK_ID,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $_REQUEST['bgitemsname'][$itemId],
            "ACTIVE" => "Y",
            "PREVIEW_TEXT" => $_REQUEST['PREVIEW_TEXT'][$itemId],
            "PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $_REQUEST['thumbnailfile'][$itemId]),
            "DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $arResizePicture['src'])
        );
        if (!empty($_REQUEST['DATE_ACTIVE_TO'][$itemId]))
            $arLoadArray['DATE_ACTIVE_TO'] = ConvertTimeStamp(strtotime($_REQUEST['DATE_ACTIVE_TO'][$itemId]), 'SHORT');

        if ($elementId = $el->Add($arLoadArray)) {
            $arAddedItem[$elementId] = array(
                'ID' => $elementId,
                'NAME' => $arLoadArray['NAME'],
                'PREVIEW_TEXT' => $arLoadArray['PREVIEW_TEXT'],
                'PREVIEW_PICTURE' => $_REQUEST['thumbnailfile'][$itemId],
            );
        } else {
            //echo "Error: ".$el->LAST_ERROR;
        }
    }

    $arResult = $arAddedItem;
}

MainHelper::showJson(array('error' => $error, 'result' => $arResult));