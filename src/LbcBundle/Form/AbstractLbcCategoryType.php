<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 17:18
 */

namespace LbcBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

abstract class AbstractLbcCategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parentid')
            ->add('level')
            ->add('name')
            ->add('comments')
            ->add('keywords')
            ->add('links')
            ->add('gr')
            ->add('grerr')
            ->add('recId')
            ->add('parent')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'      => $this->getEntityName(),
                'csrf_protection' => false,
            )
        );
    }

    abstract  public function getEntityName();

}