<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);?>

<div class="container">
	<form action="<?= $arParams['FORM_ACTION'] ?>"
		  target="_top"
		  method="post"
		  class="b-form b-form_common b-addbookform"
		  data-select-items-widget>
		<input type="hidden" name="action" value="addBook">
		<?if(!empty($arParams['COLLECTION_ID'])){?>
			<input type="hidden" name="COLLECTION_ID" value="<?= $arParams['COLLECTION_ID'] ?>">
		<?}?>		
		<?= bitrix_sessid_post() ?>
		<div class="fieldrow">
			<table class="modal-results-table">
				<?foreach($arResult["ITEMS"] as $item) {?>
					<tr>
						<td>
							<div class="b-radio">
								<?if(empty($arParams["ARR_EXALEAD_BOOKS"])
									|| !in_array($item['id'], $arParams["ARR_EXALEAD_BOOKS"])) {?>
									<label class="checkbox">
										<input type="checkbox"
											   name="BOOK_NUM_ID[]"
											   value="<?=$item['NUM_ITEM']?>"
											   class="checkbox custom"
											   id="libraryCollectionBooks"
											   data-select-item-control>
										<span class="lbl"></span>
									</label>
									<input type="hidden"
										   name="BOOK_ID_<?= $item['NUM_ITEM'] ?>"
										   value="<?=$item['id']?>">
									<input type="hidden"
										   name="BOOK_TITLE_<?= $item['NUM_ITEM'] ?>"
										   value="<?= strip_tags($item['title']) ?>">
									<input type="hidden"
										   name="BOOK_AUTHOR_<?= $item['NUM_ITEM'] ?>"
										   value="<?= strip_tags($item['authorbook']) ?>">
									<!-- <input type="hidden" name="BOOK_TITLE_<?=$item['NUM_ITEM']?>" value="<?=$item['title']?>">
									<input type="hidden" name="BOOK_AUTHOR_<?=$item['NUM_ITEM']?>" value="<?=$item['authorbook']?>"> -->
									<input type="hidden"
										   name="BOOK_YEAR_<?= $item['NUM_ITEM'] ?>"
										   value="<?= $item['publishyear'] ?>">
								<?}?>
							</div>
						</td>
						<td>
							<a class="b_bookpopular_photo" href="<?= $item['DETAIL_PAGE_URL'] ?>" target="_blank">
								<img data-title="<?= strip_tags($item['title']) ?>"
									 data-autor="<?= strip_tags($item['authorbook']) ?>"
									 alt=""
									 src="<?= $item['IMAGE_URL'] ?>">
							</a>
						</td>
						<td>
							<h2>
								<span class="num"><?= $item['NUM_ITEM'] ?><?= '.' ?></span>
								<a href="<?= $item['DETAIL_PAGE_URL'] ?>" target="_blank">
									<?= ltrim($item['title']) ?>
									<?if(!empty($arParams["ARR_EXALEAD_BOOKS"])
										&& in_array($item['id'], $arParams["ARR_EXALEAD_BOOKS"])){?>
										<small class="text-info">
											<?= Loc::getMessage("LIB_SEARCH_PAGE_BOOKS_EXIST_IN_COLLECTION") ?>
										</small>
									<?}?>
								</a>
							</h2>
							<ul class="b-resultbook-info">
								<?if(!empty($item['authorbook'])) {?>
									<li>
										<span><?= Loc::getMessage('LIB_SEARCH_AUTHOR') ?></span>
										<?= $item['authorbook'] ?>
									</li>
								<?}?>
								<?if(!empty($item['publishyear'])) {?>
									<li>
										<span><?= Loc::getMessage('LIB_SEARCH_PUBLIC_YEAR') ?></span>
										<?= $item['publishyear'] ?>
									</li>
								<?}?>
								<?if(!empty($item['countpages'])) {?>
									<li>
										<span><?= Loc::getMessage('LIB_SEARCH_COUNT_OF_PAGES') ?></span>
										<?= $item['countpages'] ?>
									</li>
								<?}?>
								<?if(!empty($item['publisher'])) {?>
									<li>
										<span><?= Loc::getMessage('LIB_SEARCH_PUBLIC_HOUSE') ?></span>
										<?= $item['publisher'] ?>
									</li>
								<?}?>
								<?if(!empty($item['library'])) {?>
									<li>
										<span><em><?= Loc::getMessage('LIB_SEARCH_SOURCE') ?></em></span>
										<?= $item['library'] ?>
									</li>
								<?}?>
							</ul>
						</td>
					</tr>
				<?}?>
			</table><!-- /.b-result-doc -->
		</div>
		<?= $arResult['STR_NAV'] ?>
		<div class="fieldrow nowrap fieldrowaction container b-add-actionbar">
			<div class="fieldcell">
				<div class="field clearfix">
					<a href="#" class="btn btn-default pull-right" data-cancel-outer-modal>
						<?= Loc::getMessage('LIB_SEARCH_CANCEL_SELECT') ?>
					</a>
					<button disabled class="btn btn-primary" value="1" type="submit">
						<?= Loc::getMessage('LIB_SEARCH_ADD_SELECT') ?>
					</button>
				</div>
			</div>
		</div>
	</form>
</div>
<script>
    $(document).on('change', '[data-select-item-control]', function(){
    	var toggler = $(this),
    		widget = toggler.closest('[data-select-items-widget]'),
    		editionsToSelect = $('[data-select-item-control]', widget),
    		submitButton = $('[type="submit"]', widget),
    		hasChecked = false;
        
		editionsToSelect.each(function(){
			if ( $(this).prop('checked') ) {
				hasChecked = true;
			}
		});
		if (hasChecked) {
			submitButton.removeAttr('disabled');
		} else {
			submitButton.attr('disabled','');
		}
		/* console.log('hasChecked = ' + hasChecked); */
    });
	$('[data-cancel-outer-modal]').on('click', function(e){
		e.preventDefault();
		window.parent.$('[data-dismiss="modal"]').trigger('click.modal');
	});
</script>