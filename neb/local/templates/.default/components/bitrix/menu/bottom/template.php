<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <ul class="main-footer__footer-nav">
        <? foreach($arResult as $arItem):    ?>
            <li class="main-footer__footer-nav-kind">
                <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
            </li>
        <?endforeach?>
    </ul>
<?endif?>