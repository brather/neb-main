<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<div class="b-addsearch clearfix">
	<a class="button_blue btadd left popup_opener ajax_opener noclose" href="<?=$this->__folder.'/addBook.php'?>" data-width="631" data-posmy="left" data-posat="top left"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_BOOK');?></a>
	<div class="b-search_field right">
		<form method="get">
			<input type="text" name="qbook" class="b-search_fieldtb b-text" id="asearch" value="<?=htmlspecialcharsbx($_REQUEST['qbook'])?>" autocomplete="off">
			<select name="qbooktype" class="js_select b_searchopt b_searchopt-rightholder">
				<option <?=$_REQUEST['qbooktype'] == 'all' ? 'selected="selected"' : ''?> value="all"><?=GetMessage('RIGHTHOLDER_BOOKS_SEARCH_ALL');?></option>
				<option <?=$_REQUEST['qbooktype'] == 'Name' ? 'selected="selected"' : ''?> value="Name"><?=GetMessage('RIGHTHOLDER_BOOKS_SEARCH_TITLE');?></option>
				<option <?=$_REQUEST['qbooktype'] == 'Author' ? 'selected="selected"' : ''?> value="Author"><?=GetMessage('RIGHTHOLDER_BOOKS_SEARCH_AUTHOR');?></option>
			</select>
			<input type="submit" class="b-search_bth bbox" value="<?=GetMessage('RIGHTHOLDER_BOOKS_SEARCH');?>">
		</form>
	</div><!-- /.b-search_field-->
</div>

<div class="b-fondtable">
	<table class="b-usertable b-fondtb b-fondtbedit">
		<tr>
			<th class="title_cell"><a <?=SortingExalead("Name")?>><?=GetMessage('RIGHTHOLDER_BOOKS_TITLE');?></a></th>
			<th class="autor_cell"><a <?=SortingExalead("Author")?>><?=GetMessage('RIGHTHOLDER_BOOKS_AUTHOR');?></a></th>
			<th class="bbk_cell"><a <?=SortingExalead("BBKText")?>><?=GetMessage('RIGHTHOLDER_BOOKS_BBK');?></a></th>
			<th class="year_cell"><a <?=SortingExalead("PublishYear")?>><?=GetMessage('RIGHTHOLDER_BOOKS_PUBLISH_YEAR');?></a></th>
			<th class="exs_cell"><a <?=SortingExalead("BX_TIMESTAMP")?>><?=GetMessage('RIGHTHOLDER_BOOKS_DATE_ADD');?> </a></th>
			<th class="exs_cell"><a ><?=GetMessage('RIGHTHOLDER_BOOKS_PDF_LINK');?></a></th>
			<th class="exs_cell"><a ><?=GetMessage('RIGHTHOLDER_BOOKS_DESCRIPTION');?></a></th>
			<th class="recycle_cell"><span style="width:auto"><?=GetMessage('RIGHTHOLDER_BOOKS_DEL_REQUEST');?></span></th>
		</tr>
		<?
			if(!empty($arResult['BOOKS']))
			{
				//print_r($arResult['BOOKS']);
				foreach($arResult['BOOKS'] as $arBook)
				{
				?>
				<tr>
					<td class="pl15">
						<div class="b-fondtitle"><span class="b-fieldtext"><?=$arBook['Name']?></span></div>
					</td>
					<td class="pl15">
						<div class="b-fondtitle"><span class="b-fieldtext"><?=$arBook['Author']?></span></div>
					</td>
					<td class="pl15">
						<div class="b-fondtitle"><span class="b-fieldtext"><?=$arBook['BBKText']?></span></div>
					</td>
					<td class="pl15">
						<div class="b-fondtitle"><span class="b-fieldtext"><?=$arBook['PublishYear']?></span></div>
					</td>
					<td class="pl15">
						<div class="b-fondtitle"><span class="b-fieldtext"><?=$arBook['BX_TIMESTAMP_']?></span></div>
					</td>
					<td class="pl15">
						<span class="nowrap"> <a href="<?=$arBook['pdfLink']?>" target="_blank" class="b-viewpdf"><?=GetMessage('RIGHTHOLDER_BOOKS_VIEW');?></a></span>
					</td>
                    <td class="pl15">
                        <span class="nowrap"> <a class="popup_opener ajax_opener closein" href="<?=$this->__folder?>/addBook.php?BOOK_ID=<?=$arBook['Id']?>" data-popupsize="high" data-overlaybg="dark" data-width="632"><?=GetMessage('RIGHTHOLDER_BOOKS_EDIT');?></a></span>
                    </td>
					<td><?
                    	if (!isset($arBook['toDel']) || $arBook['toDel'] == 0):?>
                        <div class="rel plusico_wrap minus">
                            <a class="plus_ico lib " href="<?=$APPLICATION->GetCurPage().'?id='.$arBook['Id'].'&action=todel'?>"></a>
                            <div class="b-hint rel"><span><?=GetMessage('RIGHTHOLDER_BOOKS_REQUEST_TO_DEL');?></span></div>
						</div><?
						else:?>	
                        <div class="rel plusico_wrap minus" style="width:auto; white-space:nowrap; padding-bottom:10px">
                            <a class="lib " href="<?=$APPLICATION->GetCurPage().'?id='.$arBook['Id'].'&action=undotodel'?>"><?=GetMessage('RIGHTHOLDER_BOOKS_CANCEL_DEL');?></a>
                            <div class="b-hint rel"><span><?=GetMessage('RIGHTHOLDER_BOOKS_WITHDRAW');?></span></div>
                        </div><?
                        endif?> 
					</td>
				</tr>
				<?
				}
			}
		?>	  
	</table>
</div><!-- /.b-stats-->		
<script>
/*$(".plus_ico").click(function (e) {
	e.preventDefault()
	$(this).hide().after("<div style='margin-left:-30px;'>Отправлено</div>");;
	$(".b-hint").hide();
})*/
</script>															
