
<?
$pagename='';
?>
<? include("header_main_auth.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb bbox b-text" id="" value="Станислав Лем">							
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>	
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
	<div class="b-side right mt10">
			<div class="b-profile_side">
				<div class="b-profile_photo">
					<img src="./i/user_photo.png" alt="user photo">
				</div>
				<div class="b-profile_name">Николай Петров</div>
				<div class="b-profile_reg">Зарегистрирован: 24.04.2014 <br/>ограниченный доступ</div>
				<ul class="b-profile_info">
					<li><a href="#">изменить фото</a></li>
					<li><a href="user_profile_settings.php" class="set_prof act">настройки профиля</a></li>
					<li><a href="#" class="help">Помощь</a></li>
					<li class=" hidden"><a href="#">личные сообщения</a><span class="b-profile_msgnum">3</span></li>
				</ul>

				<a class="button_mode  hidden" href="#">получить полный доступ</a>			


			</div>
		</div><!-- /.b-side -->
	<div class="b-razdel">
			<select class="js_select">
				<option value="-1">Выберите раздел</option>
				<option value="user_profile_new.php">Личный кабинет</option>
				<option value="user_profile_favorites.php" >Моя библиотека</option>
				<option value="user_profile_lk_quo.php">Цитаты</option>
				<option value="user_profile_lk_bookmark.php">Закладки</option>
				<option value="user_profile_lk_notes.php" >Заметки</option>
				<option value="user_profile_lk_searchresult.php">Поисковые запросы</option>
			</select>
		</div>
	<div class="b-searchresult noborder hidden">
		<ul class="b-profile_nav">
			<li>
				<a href="#" class="b-profile_navlk js_profilemenu current">личный кабинет</a>
				<ul class="b-profile_subnav">
					<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
					<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
					<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
				</ul>
			</li>
			<li><a class="b-profile_nav_lb" href="#">моя библиотека</a></li>
			<li><a href="#" class="b-profile_nav_qt">цитаты</a></li>
			<li><a href="#" class="b-profile_nav_bm ">закладки</a></li>
			<li><a href="#" class="b-profile_nav_notes">заметки</a></li>
			<li><a href="#" class="b-profile_nav_search">поисковые запросы</a></li>
		</ul>                 
	</div><!-- /.b-searchresult-->
	<div class="b-mainblock left">

		<div class="b-profile_set b-profile_brd rel">
			<div class="b-media_export hidden">
				<div class="b-media_exportlb">Экспортировать
					прочитанные книги в социальные сети</div>
					<form action="" class="b-form">
						<div class="fieldrow nowrap">
							<div class="fieldcell iblock">
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb29"><label for="cb29" class="black">Вконтакте</label>
								</div>
								<div class="field validate">
									<input type="text" value="" id="settings21" data-maxlength="30" data-minlength="2" name="vk" class="input">										
								</div>
								<div class="formsample">например: http://vk.com/my</div>
							</div>
						</div>
						<div class="fieldrow nowrap">
							<div class="fieldcell iblock">
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb39"><label for="cb39" class="black">Одноклассники</label>
								</div>
								<div class="field validate">
									<input type="text" value="" id="settings21" data-maxlength="30" data-minlength="2" name="vk" class="input">										
								</div>
								<div class="formsample">например: http://odnoklassniki.ru/my</div>
							</div>
						</div>
						<div class="fieldrow nowrap">
							<div class="fieldcell iblock">
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb49"><label for="cb49" class="black">Facebook</label>
								</div>
								<div class="field validate">
									<input type="text" value="" id="settings21" data-maxlength="30" data-minlength="2" name="vk" class="input">										
								</div>
								<div class="formsample">например: http://www.facebook.com/my</div>
							</div>
						</div>
						<div class="fieldrow nowrap">
							<div class="fieldcell iblock">
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb59"><label for="cb59" class="black">Twitter</label>
								</div>
								<div class="field validate">
									<input type="text" value="" id="settings21" data-maxlength="30" data-minlength="2" name="vk" class="input">										
								</div>
								<div class="formsample">например: http://twitter.com/my</div>
							</div>
						</div>
						<div class="fieldrow nowrap fieldrowaction">
							<div class="fieldcell ">
								<div class="field clearfix">
									<button class="formbtn" value="1" type="submit">Подтвердить</button>

								</div>
							</div>
						</div>	
					</form>
				</div>
				<h2>настройки профиля</h2>
				<form action="" class="b-form b-form_common b-profile_passform">
					<div class="b-form_header">Смена пароля</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<em class="hint">*</em>
							<label for="settings087">Текущий пароль</label>
							<div class="field validate">
								<input type="password" data-validate="password" data-required="true" value="" id="settings087" data-maxlength="30" data-minlength="2" name="pass" class="input">
								<em class="error validate">Неверный пароль</em>
								<em class="error required">Заполните</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<em class="hint">*</em>
							<label for="settings01">Новый пароль</label>
							<div class="field validate">
								<input type="password" data-validate="password" data-required="true" value="" id="settings01" data-maxlength="30" data-minlength="6" name="pass" class="input">	
								<em class="error validate">Неверный пароль</em>
								<em class="error required">Заполните</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<em class="hint">*</em>
							<label for="settings02">Подтвердить новый пароль</label>
							<div class="field validate">
								<input data-identity="#settings01" type="password" data-required="true" value="" id="settings02" data-maxlength="30" data-minlength="6" name="pass" class="input" data-validate="password">	
								<em class="error validate">Неверный пароль</em>
								<em class="error required">Заполните</em>
								<em class="error identity">Пароли не совпадают</em>
							</div>
						</div>
					</div>


					<div class="fieldrow nowrap fieldrowaction">
						<div class="fieldcell ">
							<div class="field clearfix">
								<button class="formbtn" value="1" type="submit">Подтвердить</button>
								<div class="messege_ok hidden">
									<a href="#" class="closepopup">Закрыть окно</a>
									<p>На Вашу электронную почту, указанную при регистрации, отправлено письмо. <br/>Для подтверждения нового пароля воспользуйтесь ссылкой из письма. <br/>Срок действия ссылки - 24 часа.</p>
								</div>
							</div>
						</div>
					</div>
				</form>
				<form action="" class="b-form b-form_common b-profile_setform">
					<div class="b-form_header">основные настройки</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings03">Отображаемое имя</label>
							<div class="field validate">
								<input type="text" data-required="true" value="" id="settings03" data-maxlength="30" data-validate="alpha" data-minlength="2" name="pass" class="input">										
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings04">Адрес электронной почты</label>
							<div class="field validate">
								<input type="text" class="input" name="num" data-minlength="2" data-maxlength="30" id="settings04" data-validate="email" value="">									
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings05">Пол</label>
							<div class="field validate">
								<select name="" id="" class="js_select w270"  data-required="true">									
									<option value="1">мужской</option>
									<option value="2">женский</option>
								</select>									
							</div>
						</div>
					</div>
					
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">

							<label for="settings06">Дата рождения</label>
							<div class="field validate iblock seldate">
								<em class="error dateformat">Неверный формат даты</em>
								<em class="error required">Заполните</em>
								<em class="error yearsrestrict">Вам должно быть<br>не меньше 12 лет</em>
								<input type="hidden" class="realseldate" data-required="true" value="" id="settings06" name="REGISTER[PERSONAL_BIRTHDAY]">
								<select name="birthday" id=""  class="js_select w102 sel_day"  data-required="true">
									<option value="">день</option>
									<option value="01">1</option>
									<option value="02">2</option>
								</select>
								
								<select name="birthmonth" id=""class="js_select w165 sel_month"  data-required="true" >
									<option value="">месяц</option>
									<option value="01">январь</option>
									<option value="02">февраль</option>
								</select>
								
								<select name="birthyear" id=""  class="js_select w102 sel_year"  data-required="true">
									<option value="">год</option>
									<option value="1980">1980</option>
									<option value="1981">1981</option>
								</select>
							</div>									

						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings07">Немного о себе</label>
							<div class="field validate">
								<textarea class="textarea" name="about" id="settings07" data-minlength="2" data-maxlength="800" ></textarea>							
							</div>
						</div>
					</div>
					<div class="fieldcell nowrap photorow">
						<label>Загрузить скан разворота паспорта с фотографией </label>
						<div class="field validate">
							<div class="setscan iblock">
								<input type="file" class="photofile" name="" id="" >
								<a href="#">Загрузить изображение</a>
								<div class="setphoto_lb">или перетащите его на это поле</div>
								<div class="progressbar"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружается <span class="num">33</span>%</div></div>
							</div>
							
							<div class="b-scan_photo iblock">
								<img src="./pic/scan.jpg" alt="">
							</div>
							<em class="error required">Загрузите скан паспорта</em>
						</div>
					</div>
					<div class="fieldcell nowrap photorow">
						<label>Загрузить скан разворота паспорта с  пропиской </label>
						<div class="field validate">
							<div class="setscan iblock">
								
								<input type="file" class="photofile" name="" id="" >
								<a href="#">Загрузить изображение</a>
								<div class="setphoto_lb">или перетащите его на это поле</div>
								<div class="progressbar"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружается <span class="num">33</span>%</div></div>
							</div>
							<div class="b-scan_photo iblock">
								<img src="./pic/scan.jpg" alt="">
							</div>
							<em class="error required">Загрузите скан паспорта</em>
						</div>
					</div>

					<div class="b-form_header">настройки поиска</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock b-srch_numset">
							<label for="settings08">Количество <br>результатов поиска</label>
							<div class="field validate searchnum_set">
								<input type="hidden" class="" value="" id="settings08" name="SEARCH[RESULT_NUMBER]">
								
								<a href="#" class="b-search_numset current iblock">10</a>	
								<a href="#" class="b-search_numset iblock">25</a>	
								<a href="#" class="b-search_numset iblock">50</a>								
							</div>
						</div>
						<div class="fieldcell iblock">
							<label for="settings09">Максимальное количество <br>документов по запросу</label>
							<div class="field validate">
								<input type="text" value="" id="settings09" data-maxlength="300" data-minlength="2" data-validate="number" name="pass" class="input docnum">										
							</div>
						</div>

					</div>
					<div class="fieldrow nowrap fr_search_set hidden">
						<div class="fieldcell iblock fieldcheck">
							<label for="settings17">Список полей, выводимых в результате поиска</label>
							<div class="b-checksettings">
								<div class="checkwrapper w212">
									<input class="checkbox" type="checkbox" name="" id="cb4"><label for="cb4" class="black">Обложка</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb5"><label for="cb5" class="black">Анотация</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb6"><label for="cb6" class="black">Год публикации</label>
								</div>
								<div class="checkwrapper w212">
									<input class="checkbox" type="checkbox" name="" id="cb7"><label for="cb7" class="black">Количество страниц</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb8"><label for="cb8" class="black">Издательство</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb9"><label for="cb9" class="black">Библиотека</label>
								</div>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap hidden">
						<div class="fieldcell iblock fieldcheck">
							<label for="settings17">Настройка полей фильтров</label>
							<div class="b-checksettings">
								<div class="checkwrapper w104">
									<input class="checkbox" type="checkbox" name="" id="cb14"><label for="cb14" class="black">Авторы</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb15"><label for="cb15" class="black">Дата публикации</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb16"><label for="cb16" class="black">Формат издания</label>
								</div>
								<div class="checkwrapper w120">
									<input class="checkbox" type="checkbox" name="" id="cb17"><label for="cb17" class="black">Колекции</label>
								</div>
								<div class="checkwrapper w104">
									<input class="checkbox" type="checkbox" name="" id="cb27"><label for="cb27" class="black">Тематики</label>
								</div>
								<div class="checkwrapper w104">
									<input class="checkbox" type="checkbox" name="" id="cb18"><label for="cb18" class="black">Язык</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb19"><label for="cb19" class="black">Библиотеки</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb20"><label for="cb20" class="black">Издательство</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb21"><label for="cb21" class="black">Место издания</label>
								</div>
								<div class="checkwrapper">
									<input class="checkbox" type="checkbox" name="" id="cb22"><label for="cb22" class="black">Объем изадния</label>
								</div>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap fieldrowaction">
						<div class="fieldcell ">
							<div class="field clearfix">
								<button class="formbtn" value="1" type="submit">Сохранить</button>

							</div>
						</div>
					</div>
				</form>
			</div><!-- /.b-profilesettings-->


		</div><!-- /.b-mainblock -->
	</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>