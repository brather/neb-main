<?
namespace Bitrix\NotaExt\Ext\DetailGallery;
class Params
{
    /*
    @method getParam - возвращает параметр из настроек расширения
    */
    static public function getParam($name)
    {
        $value = \COption::GetOptionString('notaext', 'detailGallery_' . $name);

        return $value;
    }

    /*
    @method decode декодирует строку параметра из base64
    */
    static public function decode($strParam)
    {
        $param = base64_decode($strParam);
        $param = ltrim($param, '(');
        $param = rtrim($param, ')');
        $param = \Cutil::JsObjectToPhp($param);
        return $param;
    }
}