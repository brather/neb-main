<?php
namespace Nota\Journal;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);


class NoteTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'neb_journal.note';
    }

    public static function getConnectionName()
    {
        return 'journal';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            new Entity\EnumField('EVENT', array(
                'values' => array('ADD', 'EDIT', 'DELETE', 'VIEW')
            )),
            'USER' => array(
                'data_type' => 'integer',
            ),
            'USER_DATA' => array(
                'data_type' => 'text',
            ),
            'TIMESTAMP' => array(
                'data_type' => 'datetime',
            ),
            'BOOK_ID' => array(
                'data_type' => 'string',
            ),
            'DATE' => array(
                'data_type' => 'datetime',
            ),
            'TEXT' => array(
                'data_type' => 'text',
            ),
            'USER_COLLECTION' => array(
                'data_type' => 'text',
            ),
            'INCLUDED' => array(
                'data_type' => 'text',
            ),
        );
    }
}