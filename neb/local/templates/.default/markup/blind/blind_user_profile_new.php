 
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php"  accesskey="z"  class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
		<div class="b-searchresult noborder">
			<ul class="b-profile_nav">
				<li><a href="#" class="b-profile_navlk js_profilemenu current" title="личный кабинет">личный кабинет</a></li>
				<li><a class="b-profile_nav_lb" href="#" title="моя библиотека">моя библиотека</a></li>
				<li><a href="#" class="b-profile_nav_qt" title="цитаты">цитаты</a></li>
				<li><a href="#" class="b-profile_nav_bm" title="закладки">закладки</a></li>
				<li><a href="#" class="b-profile_nav_notes" title="заметки">заметки</a></li>
				<li><a href="user_profile_lk_searchresult.php" title="поисковые запросы" class="b-profile_nav_search">поисковые запросы</a></li>

			</ul>                 
		</div><!-- /.b-searchresult-->
			<div class="b-profile_side">
				<div class="b-profile_name">Николай Петров</div>
				<div class="b-profile_reg">Зарегистрирован: 24.04.2014</div>
				<div class="b-profile_reg">ограниченный доступ</div>
				<ul class="b-profile_info">
					<li><a href="blind_user_profile_settings.php" class="set_prof">настройки профиля</a></li>
					<li><a href="#" class="help" title="Помощь">Помощь</a></li>
					<li><a href="#" title="Выйти">Выйти</a></li>	
				</ul>
			</div>
		
		
		<div class="b-profile_lk">
			<h2 class="mode">10 самых популярных изданий</h2>

				<div class="b-filter_list_wrapper">
			<div class="b-result-doc">
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_4.jpg"></span>
						<div class="b-result-doclabel">
							Требуется авторизация
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">1.</span>
						<h2><a href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_27.jpg"></span>

					</div>
					<div class="iblock b-result-docinfo">
					<span class="num">2.</span>
						<h2><a  href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> Межпоселенческая централизованная библиотечная система Никольского муниципального района</div>
							
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_4.jpg"></span>
						<div class="b-result-doclabel">
							Требуется авторизация
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">1.</span>
						<h2><a href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_27.jpg"></span>

					</div>
					<div class="iblock b-result-docinfo">
					<span class="num">2.</span>
						<h2><a  href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> Межпоселенческая централизованная библиотечная система Никольского муниципального района</div>
							
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_4.jpg"></span>
						<div class="b-result-doclabel">
							Требуется авторизация
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">1.</span>
						<h2><a href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_27.jpg"></span>

					</div>
					<div class="iblock b-result-docinfo">
					<span class="num">2.</span>
						<h2>Сумма технологии</h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> Межпоселенческая централизованная библиотечная система Никольского муниципального района</div>
							
						</div>
					</div>


				</div>
				
				
			</div><!-- /.b-result-doc -->
		</div><!-- /.b-filter_list_wrapper -->

		</div><!-- /.b-profile_lk -->
		
	
</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>