<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if (!empty($arResult['ELEMENTS'])) {
    $arItem = current($arResult['ELEMENTS']);
    $arResult = array_merge($arResult, $arItem);
}

$this->setFrameMode(true);
$APPLICATION->SetPageProperty('hide-title', true);

?>
<div class="news-detail">

    <a href="<?=$arParams['SEF_FOLDER']?>" class="news-detail__link">Информация для участников НЭБ</a>

    <h1><?=$arResult["NAME"]?></h1>
    <div class="news-item">
        <?if (!empty($arResult['DETAIL_PICTURE']) || !empty($arResult['PREVIEW_PICTURE'])):?>
            <img width=500px src="<?=$arResult['DETAIL_PICTURE']['PATH']?:$arResult['PREVIEW_PICTURE']['PATH']?>"
                 class="img_right" alt="<?=$arResult["NAME"]?>" />
        <?endif;?>
        <?=$arResult['DETAIL_TEXT']?>
    </div>

    <?
    $APPLICATION->IncludeComponent(
        "neb:main.share",
        ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
            "DATA_TITLE"         => $arResult['NAME'],
            "DATA_DESCRIPTION"   => $arResult['DETAIL_TEXT'],
            "THEME_SERVICES" => array(
                0 => "vkontakte",
                1 => "facebook",
                2 => "odnoklassniki",
            )
        ),
        false
    );
    ?>
</div>