<?php

use \Neb\Main\Helper\MainHelper,
    \Neb\Main\Helper\UtilsHelper;

/**
 * Класс для работы с Геокодером Яндекса
 * Определяет географические координаты объекта по его адресу и наоборот
 * https://tech.yandex.ru/maps/doc/geocoder/desc/concepts/About-docpage/
 */

class GeocodeYandex
{
    /**
     * Получение кооординат объекта по адресу (прямое геокодирование)
     *
     * @param $sAddress - адрес объекта
     * @param string $sKey - ключ
     * @param int $iMax - максимальное количество возвращаемых объектов
     * @param string $sLanguage - локаль
     * @param string $sFormat - формат запроса
     * @return array|null
     */
    public static function getByAddress($sAddress, $sKey = '', $iMax = 0, $sLanguage = 'ru_RU', $sFormat = 'json') {

        if (empty($sAddress))
            return null;

        $sUrl = 'https://geocode-maps.yandex.ru/1.x/?format='.$sFormat.'&geocode='.urlencode($sAddress);

        // ключ к платной версии API
        if (!empty($sKey))
            $sUrl .= '&apikey=' . urlencode($sKey);

        // максимальное количество объектов
        if ($iMax)
            $sUrl .= '&results=' . $iMax;

        // язык
        if ($sLanguage)
            $sUrl .= '&lang=' . urlencode($sLanguage);

        $sResponse  = self::getContentByCurl($sUrl);
        $arResponse = UtilsHelper::convertXmlJsonToArray($sResponse);
        $arResult   = self::parseResponse($arResponse);
        
        return $arResult;
    }

    /**
     * Получение объекта по координатам (обратное геокодирование)
     *
     * @param $sLat - широта (лангитуда)
     * @param $sLong - долгота (лонгитуда)
     * @param string $sKey - ключ  API
     * @param int $iMax - максимальное количество возвращаемых объектов
     * @param string $sLanguage - локаль
     * @param string $sKind - вид топонима: house, street, metro, district, locality
     * @param string $sFormat - формат запроса
     * @return array|null
     */
    public static function getByGeocode($sLat, $sLong, $sKey = '', $iMax = 0, $sLanguage = 'ru_RU', $sKind = '', $sFormat = 'json') {

        if (empty($sLat) && empty($sLat))
            return null;

        $sLatLng = trim($sLat) . ',' . trim($sLong);

        $sUrl = 'https://geocode-maps.yandex.ru/1.x/?sco=latlong&format='.$sFormat.'&geocode='.urlencode($sLatLng);

        // максимальное количество объектов
        if ($iMax)
            $sUrl .= '&results=' . $iMax;
        
        // язык
        if ($sLanguage)
            $sUrl .= '&lang=' . urlencode($sLanguage);

        // вид топонима
        if (!empty($sKind))
            $sUrl .= '&kind=' . urlencode($sKind);

        // ключ к платной версии API
        if (!empty($sKey))
            $sUrl .= '&apiKey=' . urlencode($sKey);

        $sResponse  = self::getContentByCurl($sUrl);
        $arResponse = UtilsHelper::convertXmlJsonToArray($sResponse);
        $arResult   = self::parseResponse($arResponse);

        return $arResult;
    }

    /**
     * Преобразование ответа от сервписа в удобный вид
     *
     * @param $arResponse
     * @return array
     */
    private static function parseResponse($arResponse) {

        $arResult = array();

        if (empty($arResponse))
            return $arResult;

        // json: response element
        if (!empty($arResponse['response']))
            $arResponse = $arResponse['response'];

        // xml: if single address
        if (!empty($arResponse['GeoObjectCollection']['featureMember']['GeoObject']))
            $arResponse['GeoObjectCollection']['featureMember'][0] = $arResponse['GeoObjectCollection']['featureMember'];

        $arObjects = [];
        foreach ($arResponse['GeoObjectCollection']['featureMember'] as $key => $val) {
            foreach ($val['GeoObject'] as $key2 => $val2) {
                if (stripos($key2, 'metaDataProperty') !== false) {
                    foreach ($val2['GeocoderMetaData'] as $key3 => $val3) {
                        if (is_array($val3))
                            $arObjects[$key]['address'] = self::getAddress($val3);
                        else
                            $arObjects[$key][$key3] = $val3;

                        if (stripos($key3, 'precision') !== false)
                            $arObjects[$key][$key3.'_info'] = self::getPrecision($val3);
                        elseif (stripos($key3, 'kind') !== false)
                            $arObjects[$key][$key3.'_info'] = self::getKind($val3);
                    }
                } elseif (stripos($key2, 'boundedBy') !== false) {
                    $arObjects[$key]['viewport'] = array(
                        'northeast' => self::getLocation($val2['Envelope']['upperCorner']),
                        'southwest' => self::getLocation($val2['Envelope']['lowerCorner'])
                    );
                } elseif (stripos($key2, 'Point') !== false) {
                    $arObjects[$key]['location'] = self::getLocation($val2['pos']);
                }
                else {
                    $arObjects[$key][$key2] = $val2;
                }
            }
        }
        
        // формирование результата
        foreach ($arResponse['GeoObjectCollection']['metaDataProperty']['GeocoderResponseMetaData'] as $k => $v)
            if (stripos($k, 'point') !== false)
                $arResult['location'] = self::getLocation($v['pos']);
            else
                $arResult[$k] = $v;
        if (!empty($arObjects))
            $arResult['objects'] = $arObjects;

        return $arResult;
    }

    /**
     * Формирование координат
     *
     * @param $sLocation
     * @return array
     */
    private static function getLocation($sLocation) {

        $arLocation = explode(' ', $sLocation);
        if (count($arLocation) == 2)
            return array(
                'lat' => number_format($arLocation[1], 6, '.', ''),
                'lng' => number_format($arLocation[0], 6, '.', '')
            );

        return array();
    }

    /**
     * Рекурсивный обход дерева адреса и формирование одномерного массива
     *
     * @param $arData
     * @return array
     */
    private static function getAddress($arData) {

        $arResult = array();

        foreach ($arData as $k => $v)
            if (is_array($v))
                $arResult += self::getAddress($v);
            else
                $arResult[$k] = $v;

        return $arResult;
    }

    /**
     * Получение языковой расшифровки типа точности
     *
     * @param $sPrecision
     * @return string
     */
    public static function getPrecision($sPrecision) {
        switch (strtolower($sPrecision)) {
            case 'exact':  $sRes = 'Точное соответствие'; break;
            case 'number': $sRes = 'Совпал номер дома, но не совпало строение или корпус'; break;
            case 'near':   $sRes = 'Найден дом с номером, близким к запрошенному'; break;
            case 'range':  $sRes = 'Ответ содержит приблизительные координаты запрашиваемого дома'; break;
            case 'street': $sRes = 'Найдена только улица'; break;
            case 'other':  $sRes = 'Улица не найдена, но найден, например, посёлок, район и т. п.'; break;
            default:       $sRes = $sPrecision;
        }
        return $sRes;
    }

    /**
     * Получение языковой расшифровки вида найденного топонима
     *
     * @param $sKind
     * @return string
     */
    public static function getKind($sKind) {
        switch (strtolower($sKind)) {
            case 'house':      $sRes = 'Отдельный дом'; break;
            case 'street':     $sRes = 'Улица';         break;
            case 'metro':      $sRes = 'Станция метро'; break;
            case 'district':   $sRes = 'Район города';  break;
            case 'locality':   $sRes = 'Населённый пункт: город / поселок / деревня / село и т. п.'; break;
            case 'area':       $sRes = 'Район области'; break;
            case 'province':   $sRes = 'Область';       break;
            case 'country':    $sRes = 'Страна';        break;
            case 'hydro':      $sRes = 'Река / озеро / ручей / водохранилище и т. п.'; break;
            case 'railway':    $sRes = 'Ж.д. станция';  break;
            case 'route':      $sRes = 'Линия метро / шоссе / ж.д. линия'; break;
            case 'vegetation': $sRes = 'Лес / парк / сад и т. п.'; break;
            case 'airport':    $sRes = 'Аэропорт';      break;
            case 'other':      $sRes = 'Прочее';        break;
            default:           $sRes = $sKind;
        }
        return $sRes;
    }

    /**
     * Выполнение HTTP-запроса через cURL
     *
     * @param $sUrl
     * @return mixed
     */
    private static function getContentByCurl($sUrl) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $sUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, MainHelper::checkSslVerification($sUrl));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    /**
     * Выполнение HTTP-запроса через file_get_contents
     *
     * @param $sUrl
     * @return mixed
     */
    private static function getContentByFileGetContents($sUrl) {
        return file_get_contents($sUrl);
    }
}