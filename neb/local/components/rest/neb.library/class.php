<?php

CBitrixComponent::includeComponentClass('rest:rest.server');
CModule::IncludeModule('iblock');

use \Neb\Main\Library\LibsTable;

/**
 * Class RestLibraryNebParty
 */
class RestNebLibrary extends RestServer
{
    /**
     * @var array
     */
    protected $_params
        = array(
            'limit'       => DEFAULT_PAGE_SIZE,
            'offset'      => 0,
            'UF_LOCALITY' => null,
        );

    /**
     * @return $this
     */
    protected function _prepareParams()
    {
        $this->_params['limit'] = (integer)$this->_params['limit'];
        $this->_params['offset'] = (integer)$this->_params['offset'];
        if (is_array($this->_params['UF_LOCALITY'])) {
            $this->_params['UF_LOCALITY'] = array_filter(
                $this->_params['UF_LOCALITY']
            );
        }

        return $this;
    }

    /**
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     */
    public function getAction()
    {
        $params = array(
            'filter' => array(),
            'limit'  => $this->_params['limit'],
            'offset' => $this->_params['offset'],
        );
        $params['filter'] = array_diff_key(
            $this->_params,
            array(
                'token'  => true,
                'limit'  => true,
                'offset' => true
            )
        );

        $this->arResult['data'] = LibsTable::getList($params)->fetchAll();

        return 200;
    }
}