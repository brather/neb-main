<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;

$APPLICATION->IncludeComponent(
    "neb:library.plan_digital_add.reader",
    "",
    Array(
        "HL_IBLOCK_ID"   => 1,
        "COUNT_OF_PLANS" => 15,
        "SEF_FOLDER"     => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['reader'],
        "USER_GROUPS"    => array("3", "4", "11")
    ),
    false
);