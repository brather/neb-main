<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151211162223 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('TRUNCATE TABLE neb_libs;');
        $this->addSql('LOCK TABLES `neb_libs` WRITE;');
        $this->addSql(
            file_get_contents(__DIR__ . '/sql/neb_libs20151211162223_up.sql')
        );
        $this->addSql('UNLOCK TABLES;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('TRUNCATE TABLE neb_libs;');
        $this->addSql('LOCK TABLES `neb_libs` WRITE;');
        $this->addSql(
            file_get_contents(__DIR__ . '/sql/neb_libs20151211162223_down.sql')
        );
        $this->addSql('UNLOCK TABLES;');
    }
}
