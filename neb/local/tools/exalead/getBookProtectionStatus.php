<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

use \Bitrix\Main\Config\Option;
use \Neb\Main\Helper\MainHelper;

if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') die();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$server_ip = Option::get("nota.exalead", "viewer_ip");
$server_port = Option::get("nota.exalead", "viewer_port");

MainHelper::clearBuffer();
echo file_get_contents("http://$server_ip:$server_port/{$_REQUEST['BOOK_ID']}/document/type");