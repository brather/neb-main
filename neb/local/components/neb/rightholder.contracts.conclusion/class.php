<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * Class RightholderContractsConclusion
 */
class RightholderContractsConclusion extends CBitrixComponent
{
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->_getForm();
        $this->includeComponentTemplate();
    }

    private function _getForm() {

        global $APPLICATION;

        $iContractId = 0;
        $arBooks = $arContracts = [];

        if (intval($_REQUEST['edit'])) {
            $iContractId = intval($_REQUEST['edit']);
            $APPLICATION->SetTitle('Редактирование договора #'.$iContractId);
        }

        $obRightHolder = new \RightHolder($iContractId);
        if ($iContractId) {
            $arContracts = $obRightHolder->getContracts();
            $arEntities  = $obRightHolder->getContractsEntities($arContracts);
            $arBooks     = $obRightHolder->getBooksWithCopyright($arEntities['BOOKS']);
        }
        $arError         = $obRightHolder->saveContract();

        $this->arResult = array(
            'GROUPS'   => $obRightHolder->arUserGroups,
            'ENUMS'    => $obRightHolder->arEnum,
            'STATUS'   => $obRightHolder->arStatus,
            'CONTRACT' => $obRightHolder->getFirst($arContracts),
            'BOOKS'    => $arBooks,
            'ERROR'    => $arError
        );
    }
}