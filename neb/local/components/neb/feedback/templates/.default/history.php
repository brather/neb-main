<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * Показ истории заявки
 */

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->IncludeComponent(
    "neb:feedback.history",
    ".default",
    array(
        "COMPONENT_TEMPLATE"   => ".default",
        'FEEDBACK_URL'         => $arParams['SEF_FOLDER'],
        'FEEDBACK_APPOINT_URL' => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['history']
    ),
    $component
);