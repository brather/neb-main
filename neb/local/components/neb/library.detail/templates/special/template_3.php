<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__DIR__.'/template.php');
?>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock">

		<?$APPLICATION->ShowViewContent('lib_menu')?>

		<div class="b-lib_descr">
			<div class="iblock b-lib_fulldescr">
				<div class="b-libstatus"><?=$arResult['ITEM']['PROPERTY_STATUS_VALUE']?></div>
				<h2><?=$arResult['ITEM']['NAME']?></h2>
				<a href="<?=$arResult['ITEM']['PROPERTY_URL_VALUE']?>" class="b-libsite"
                   target="_blank"><?=$component->getLinkView($arResult['ITEM']['PROPERTY_URL_VALUE'])?></a>
				<div class="b-rolledtext" data-height="300">
					<div class="b-rolled_in ani_all_500_in">
						<p><?=htmlspecialcharsBack(TxtToHTML($arResult['ITEM']['~PREVIEW_TEXT']))?></p>
					</div>
					<a href="#" class="slidetext">Читать далее</a>
				</div>
			</div>

			<!--<a href="#" class="b-lib_photoslink">Фотогалерея</a>-->

			<div class="b-lib_map">

				<ul class="iblock b-lib_contact">
					<? if(!empty($arResult['ITEM']['PROPERTY_ADDRESS_VALUE'])){ ?>
						<li>
							<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_ADDRESS');?>:</span>
							<span class="value"><?=$arResult['ITEM']['PROPERTY_ADDRESS_VALUE']?></span>
						</li>
					<? } ?>
					<? if(!empty($arResult['ITEM']['PROPERTY_SCHEDULE_VALUE'])) { ?>
						<li>
							<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_WORK');?>:</span>
							<span class="value"><?=$arResult['ITEM']['PROPERTY_SCHEDULE_VALUE']?></span>
						</li>
					<? } ?>
					<? if(!empty($arResult['ITEM']['PROPERTY_PHONE_VALUE'])) { ?>
						<li>
							<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_PHONE');?></span>
							<span class="value">
                                <? foreach ($arResult['ITEM']['PROPERTY_PHONE_VALUE'] as $sPhone): ?>
                                    <span><?=trim($sPhone)?></span><br />
                                <? endforeach; ?>
                            </span>
						</li>
					<? } ?>
					<? if(!empty($arResult['ITEM']['PROPERTY_EMAIL_VALUE'])) { ?>
						<li>
							<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_EMAIL');?></span>
							<span class="value"><a href="mailto:<?=$arResult['ITEM']['PROPERTY_EMAIL_VALUE']?>"><?=$arResult['ITEM']['PROPERTY_EMAIL_VALUE']?></a></span>
						</li>
					<? } ?>
					<? if(!empty($arResult['ITEM']['PROPERTY_SKYPE_VALUE'])) { ?>
						<li>
							<span class="title">Skype</span>
							<span class="value"><?=$arResult['ITEM']['PROPERTY_SKYPE_VALUE']?></span>
						</li>
					<? } ?>
					<? if(!empty($arResult['ITEM']['PROPERTY_CONTACTS_VALUE'])) { ?>
						<li>
							<span class="title"><?=Loc::getMessage('LIBRARY_DETAIL_CONTACT');?></span>
							<span class="value"><?=implode('<br />', $arResult['ITEM']['PROPERTY_CONTACTS_VALUE'])?></span>
						</li>
					<? } ?>
				</ul><!-- /.b-lib_contact -->

				<div class="b-lib_mapwrapper iblock">
					<span class="marker_lib hidden">
						<a href="#" onclick="return false;" class="b-elar_name_txt"><?=$arResult['ITEM']['NAME']?></a><br />
						<span class="b-elar_status"><?=$arResult['ITEM']['PROPERTY_STATUS_VALUE']?></span><br />
						<span class="b-map_elar_info">
							<?
							if(!empty($arResult['ITEM']['PROPERTY_ADDRESS_VALUE']))
							{
								?>
								<span class="b-map_elar_infoitem"><span><?=Loc::getMessage('LIBRARY_DETAIL_ADDRESS_MAP');?>:</span><?=$arResult['ITEM']['PROPERTY_ADDRESS_VALUE']?></span><br />
							<?
							}
							if(!empty($arResult['ITEM']['PROPERTY_SCHEDULE_VALUE']))
							{
								?>
								<span class="b-map_elar_infoitem"><span><?=Loc::getMessage('LIBRARY_DETAIL_WORK');?>:</span><?=$arResult['ITEM']['PROPERTY_SCHEDULE_VALUE']?></span>
							<?
							}
							?>
						</span>
						<span class="b-mapcard_act clearfix">
							<span class="right neb b-mapcard_status"><?=Loc::getMessage('LIBRARY_DETAIL_USER');?></span>
						</span>
					</span>
				</div>

			</div>

		</div><!-- /.b-lib_descr -->

	</div><!-- /.b-mainblock -->

	<div class="b-side">
		<?
		$APPLICATION->IncludeComponent(
			"neb:library.right_counter",
			"",
			Array(
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"LIBRARY_ID" => $arResult['ITEM']["ID"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"COLLECTION_URL" => $arParams['COLLECTION_URL'],
			),
			$component
		);
		?>
		<?$APPLICATION->IncludeComponent("bitrix:voting.current", "right", Array(
				"CHANNEL_SID" => "LIBRARY_".$arResult['ITEM']['ID'],	// Группа опросов
				"VOTE_ID" => "",	// ID опроса
				"VOTE_ALL_RESULTS" => "N",	// Показывать варианты ответов для полей типа Text и Textarea
				"AJAX_MODE" => "Y",	// Включить режим AJAX
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
			),
			false
		);?>
		<?
		$APPLICATION->IncludeComponent(
			"neb:books.popular",
			"library_detail_side_three",
			Array(
				"LIBRARY_ID" => $arResult['ITEM']["ID"],
				"ITEM_COUNT" => 12,
				"CACHE_TIME" => $arParams["CACHE_TIME"],
			),
			$component
		);
		?>
	</div><!-- /.b-side -->
	<?
	global $arrFilterLibraryNews;
	$arrFilterLibraryNews = array('PROPERTY_LIBRARY' => $arResult['ITEM']['ID'], '!PROPERTY_LIBRARY' => false);

	$APPLICATION->IncludeComponent("bitrix:news.list", "library_news", Array(
			"IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
			"IBLOCK_ID" => "3",	// Код информационного блока
			"NEWS_COUNT" => "20",	// Количество новостей на странице
			"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
			"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
			"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
			"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
			"FILTER_NAME" => "arrFilterLibraryNews",	// Фильтр
			"FIELD_CODE" => array(	// Поля
				0 => "NAME",
				1 => "PREVIEW_TEXT",
			),
			"PROPERTY_CODE" => array(	// Свойства
			),
			"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
			"DETAIL_URL" => str_replace('#CODE#', $arResult['ITEM']["CODE"], $arParams['NEWS_DETAIL_URL']),	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
			"AJAX_MODE" => "N",	// Включить режим AJAX
			"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
			"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
			"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			"CACHE_TYPE" => "A",	// Тип кеширования
			"CACHE_TIME" => $arParams["CACHE_TIME"],	// Время кеширования (сек.)
			"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
			"CACHE_GROUPS" => "N",	// Учитывать права доступа
			"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
			"ACTIVE_DATE_FORMAT" => "j F",	// Формат показа даты
			"SET_TITLE" => "N",	// Устанавливать заголовок страницы
			"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
			"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
			"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
			"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
			"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
			"PARENT_SECTION" => "",	// ID раздела
			"PARENT_SECTION_CODE" => "",	// Код раздела
			"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
			"DISPLAY_DATE" => "Y",	// Выводить дату элемента
			"DISPLAY_NAME" => "Y",	// Выводить название элемента
			"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
			"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
			"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
			"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
			"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
			"PAGER_TITLE" => "Новости",	// Название категорий
			"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
			"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
			"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
			"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		),
		$component
	);?>
</section>