<?php
$MESS = array(
    'LIBRARY_STAT_REFRESH'        => 'Refresh',
    'LIBRARY_STAT_EXPORT'         => 'Export by libraries',
    'LIBRARY_PUBLICATIONS_EXPORT' => 'Complex publications report',
    'STAT_COMPLEX_DATE_FROM'      => 'Report date',
    'STAT_COMPLEX_DATE_TO'        => 'Last report date',
);