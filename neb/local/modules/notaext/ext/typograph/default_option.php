<?
$typograph_default_option = array(
    "typograph_paragraphs" => "Y",
    "typograph_breakline" => "N",
    "typograph_oa_oquote" => "N",
    "typograph_oa_oquote_extra" => "N",
    "typograph_oa_obracket_coma" => "N",
    "typograph_spaces_nobr_in_surname_abbr" => "N",
);
