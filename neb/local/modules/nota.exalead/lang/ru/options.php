<?php
$MESS['EXALEAD_SETTINGS'] = 'Подключение к Exalead';
$MESS['RSL_SETTINGS'] = 'Подключение к сервису выдачи';
$MESS['EXALEAD_IP'] = 'IP адрес сервера Exalead';
$MESS['EXALEAD_PORT'] = 'Номер порта сервера Exalead';
$MESS['VIEWER_IP'] = 'IP адрес сервера API';
$MESS['VIEWER_PORT'] = 'Номер порта сервера API';
$MESS['VIEWER_PROTOCOL'] = 'Протокол доступа сервера API';
$MESS['VIEWER_KEY'] = 'Ключ сервера API rsl';
$MESS['VIEWER_DIRECTORY_FOR_PDF_FILES'] = 'Директория для книг, оцифрованных администратором библиотеки';

$MESS['BOOKS_ADD_SETTINGS'] = 'Добавление книги';
$MESS['BOOK_ADD_EXALEAD_IP'] = 'IP адрес сервера Exalead для добавления книги';
$MESS['BOOK_ADD_FILE_PATH'] = 'Путь к папке с файлами(для сохранения)';
$MESS['BOOK_ADD_FILE_PATH_2'] = 'Путь для записи в БД';
$MESS['BOOK_ADD_URL'] = 'Адрес куда будет отправляться запрос на добавление книги';
$MESS['BOOK_ADD_LOGIN'] = 'Логин';
$MESS['BOOK_ADD_PASSWORD'] = 'Пароль';
$MESS['BOOK_ADD_DB'] = 'База данных';

$MESS['VIEWER_SETTINGS'] = 'Просмотровщик';
$MESS['VIEWER_VERSION'] = 'Актуальная версия закрытого просмотровщика Windows';
$MESS['VIEWER_LINK'] = 'Адрес скачивания новой версии просмотровщика';
$MESS['VIEWER_LINK_MAC'] = 'Адрес скачивания новой версии просмотровщика для OS X';
$MESS['VIEWER_WARNING_SHOW'] = 'Показывать окно «Предупреждение об ограниченном доступе»';
$MESS['VIEWER_WARNING_TEXT'] = 'Текст «Предупреждение об ограниченном доступе»';
$MESS['VIEWER_BOOK_PERCENT'] = 'Показывать какой процент книг';
$MESS['VIEWER_LIBRARY_WINDOW_SHOW'] = 'Показывать окно «подтверждение нахождения в библиотеке»';
$MESS['VIEWER_LIBRARY_WINDOW_TEXT'] = 'Текст «подтверждение нахождения в библиотеке»';
$MESS['VIEWER_LIBRARY_WINDOW_PERIOD'] = 'Периодичность показа окна «подтверждение нахождения в библиотеке»';
$MESS['VIEWER_LIBRARY_WINDOW_INTERVAL'] = 'Интервал между показами в секундах (не менее 5)/страницах';
$MESS['VIEWER_LIBRARY_WINDOW_GROW'] = 'Нарастание количества показов';
$MESS['VIEWER_LIBRARY_WINDOW_GROW_SPEED'] = 'Скорость нарастания в секундах/страницах';
$MESS['VIEWER_BOOK_SHOW'] = 'Показывать книги вне библиотеки';
$MESS['VIEWER_MESSAGE'] = 'Сообщение, которое выводится после достижения заданного процента';

$MESS['VIEWER_BLOCK_SETTINGS'] = 'Настройки блокировки изданий';
$MESS['VIEWER_ANALYSIS_PERIOD'] = 'Период анализа (минут)';
$MESS['VIEWER_MAX_OPEN_PAGE_COUNT'] = 'Максимальное количество открытых страниц за период анализа';
$MESS['VIEWER_IDENTIFICATION'] = 'Идентификация';
$MESS['VIEWER_BLOCK_USER'] = 'Блокировать пользователя';
$MESS['VIEWER_BLOCK_COMPUTER'] = 'Блокировать машину';
$MESS['VIEWER_BLOCK_WORKPLACE'] = 'Блокировать ВЧЗ';

$MESS['VIEWER_WATERMARK_SETTINGS'] = 'Настройки водяных знаков';
$MESS['VIEWER_WATERMARK_EMAIL'] = 'Тест: email';
$MESS['VIEWER_WATERMARK_DATE'] = 'Тест: дата';
$MESS['VIEWER_WATERMARK_WORKPALCE'] = 'Текст: ЭЧЗ';
$MESS['VIEWER_WATERMARK_POSITION'] = 'Положение на картинке';
$MESS['VIEWER_WATERMARK_COLOR'] = 'Цвет шрифта';

$MESS['OTHER_SETTINGS'] = 'Прочие';
$MESS['SEARCH_QUERY_TRANSLATE'] = 'Включить перевод поискового запроса';