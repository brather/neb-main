<?
$APPLICATION->RestartBuffer();

echo '
	<html>
	<head>
	<title>' . $APPLICATION->GetTitle() . '</title>
	<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET . '">
	<style>
	td {mso-number-format:\@;}
	.number0 {mso-number-format:0;}
	.number2 {mso-number-format:Fixed;}
	table, td {border:black solid 1px;}
	td{padding-left:5px;}
	</style>
	<script>
	window.print() ;
	</script>
	</head>
	<body>';

echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
echo "<tr>";
echo '<td>Автор</td>';
echo '<td>Название</td>';
echo '<td>Дата оцифровки</td>';
echo '<td>Год издания</td>';
echo '<td>Библиотека</td>';
echo '<td>В плане на оцифровке</td>';
echo "</tr>";

if (!empty($arResult['ITEMS'])) {
    foreach ($arResult['ITEMS'] as $arItem) {

        if (empty($arResult['BOOKS'][$arItem['id']]))
            continue;

        echo "<tr>";
        echo '<td>' . $arItem['authorbook'] . '&nbsp;</td>';
        echo '<td>' . $arItem['title'] . '&nbsp;</td>';
        echo '<td>' . $arResult['BOOKS'][$arItem['id']]['DATE_FINISH'] . '&nbsp;</td>';
        echo '<td>' . $arItem['publishyear'] . '&nbsp;</td>';
        echo '<td>' . $arItem['library'] . '&nbsp;</td>';
        echo '<td>';
        if (!empty($arResult['BOOKS_OTHER'][$arItem['id']])) {
            foreach ($arResult['BOOKS_OTHER'][$arItem['id']] as $arOther) {
                echo $arOther['NAME'] . ' до ' . $arOther['DATE'] . '<br />';
            }
        }
        echo '&nbsp;</td>';
        echo "</tr>";
    }
}

echo "</table>";
echo '</body></html>';

exit();