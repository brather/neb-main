<?php
/**
 * User: agolodkov
 * Date: 07.05.2015
 * Time: 11:04
 */

$arResult['LIBRARIES'] = array();
$field = $arResult['USER_PROPERTIES']['DATA']['UF_LIBRARY'];
if ($field && CModule::IncludeModule('iblock')) {
    $filter = array(
        'IBLOCK_ID' => $field['SETTINGS']['IBLOCK_ID'],
    );
    if ('Y' === $field['SETTINGS']['ACTIVE_FILTER']) {
        $filter['ACTIVE'] = 'Y';
    }
    $rsElements = CIBlockElement::GetList(array('NAME' => 'ASC'), $filter);
    while ($element = $rsElements->Fetch()) {
        $arResult['LIBRARIES'][] = array(
            'ID'   => $element['ID'],
            'NAME' => $element['NAME'],
        );
    }
}