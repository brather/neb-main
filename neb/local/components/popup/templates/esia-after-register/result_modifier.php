<?php
/**
 * @var array $arResult
 * @var CUser $USER
 */

if (!$USER->IsAuthorized()) {
    LocalRedirect('/');
}
$nebUser = new nebUser();
if ($nebUser->checkIntegrityGroup('full')) {
    LocalRedirect('/');
}