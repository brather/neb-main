<?
namespace Bitrix\NotaExt\Ext\Page404;

class Handlers
{
	function Redirect404()
	{
	
		if (
			!defined('ADMIN_SECTION') &&
			defined("ERROR_404") &&
			defined("PATH_TO_404") &&
			file_exists($_SERVER["DOCUMENT_ROOT"] . PATH_TO_404)
		) {
			global $APPLICATION;
			$APPLICATION->RestartBuffer();
            \CHTTP::SetStatus("404 Not Found");
			
			$strTemplatePatch = defined('TEMPLATE_PATH_404') ? TEMPLATE_PATH_404 : SITE_TEMPLATE_PATH; 
			
			include($_SERVER["DOCUMENT_ROOT"] . $strTemplatePatch . "/header.php");
			include($_SERVER["DOCUMENT_ROOT"] . PATH_TO_404);
			include($_SERVER["DOCUMENT_ROOT"] . $strTemplatePatch . "/footer.php");
		}
	}

	function onStart()
	{
		define ('PATH_TO_404','/404.php');
	}
}