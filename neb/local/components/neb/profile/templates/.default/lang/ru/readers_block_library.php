<?php
include dirname(__FILE__) . 'common.php';
$MESS['READER_BLOCK_NO_ALLOWED_STATUS'] = 'Невозможно заблокировать читателя с текущим статутом.';
$MESS['READER_VERIFY_NO_ALLOWED_STATUS'] = 'Невозможно верифицировать читателя с текущим статутом.';
$MESS['READER_BLOCK_SUCCESS'] = 'Верификация успешно снаята.';
$MESS['READER_UNBLOCK_SUCCESS'] = 'Читатель успешно верифицирован.';
$MESS['READER_BLOCK_ERROR'] = 'Ошибка блокировки читателя.';
$MESS['READER_VERIFY_ERROR'] = 'Ошибка верификации читателя.';
$MESS['READER_BLOCK_ACCESS_ERROR'] = 'Невозможно заблокировать указанного читателя, так как он не приписен к вашей библиотеке.';
$MESS['READER_ACCESS_ERROR'] = 'Указанного читателя не приписен к вашей библиотеке.';
$MESS['READER_BLOCK_RGB_ERROR'] = 'Ошибка добавление пользователя в РГБ.';