<?php
$MESS['REMOVE_BOOK_AJAX_DELETE_SUCCESS'] = 'Издание было успешно удалено из плана.';
$MESS['REMOVE_BOOK_AJAX_DELETE_ERROR'] = 'Ошибка удаления издания из плана. Попробуйте позже.';
$MESS['REMOVE_BOOK_AJAX_UPDATE_SUCCESS'] = 'Издание было успешно обновлено.';
$MESS['REMOVE_BOOK_AJAX_UPDATE_ERROR'] = 'Ошибка обновления издания. Попробуйте обновить позже.';