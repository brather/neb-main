<?php
/**
 * User: agolodkov
 * Date: 18.01.2016
 * Time: 12:56
 */

namespace Neb\Main\Stat\Report;


use Bitrix\Main\DB\Connection;
use Bitrix\Main\Entity\DataManager;
use Neb\Main\Stat\Report;
use Neb\Main\Stat\StatException;

class SimpleTbl extends Report
{
    protected $_table;

    /**
     * @var array
     */
    protected $_ips = [];

    /**
     * @return array
     */
    public function getData()
    {
        $addingCondition = [];
        if (!empty($this->_ips)) {
            $ips = [];
            foreach ($this->_ips as $ip) {
                $ips[] = 'ip = ' . (integer)$ip;
            }
            $addingCondition[] = '(' . implode(' OR ', $ips) . ')';
        }
        $entiry = static::getTableEntity($this->_table);
        $sql
            = "
SELECT
  DATE_FORMAT(timestamp, '%%Y-%%m-%%d') date,
  COUNT(user_id)                        cnt
FROM %s
WHERE
  %s
  AND timestamp >= '%s'
  AND timestamp <= '%s'
GROUP BY date;";
        $sql = sprintf(
            $sql,
            $entiry::getTableName(),
            implode(' AND ', $addingCondition),
            date('Y-m-d 00:00:00', $this->_dateFrom),
            date('Y-m-d 23:59:59', $this->_dateTo)
        );
        $connection = $entiry->getEntity()->getConnection();
        $result = [];
        /** @todo write log if !Connection */
        if ($connection instanceof Connection) {
            $result = $connection->query($sql);
            $result = $result->fetchAll();
        }

        return $result;
    }

    /**
     * @param $table
     *
     * @return DataManager
     * @throws StatException
     */
    public static function getTableEntity($table)
    {
        $class = 'Neb\Main\Stat\Table\\'
            . str_replace(' ', '', ucwords(str_replace('_', ' ', $table)))
            . 'Table';
        if (!class_exists($class)) {
            throw new StatException("Table entiry $table not found.");
        }

        return new $class();
    }
}