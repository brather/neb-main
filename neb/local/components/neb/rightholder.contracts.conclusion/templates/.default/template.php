<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

foreach ($arResult['ERROR'] as $sError)
	ShowError($sError);

?>
<form method="post" action="" enctype="multipart/form-data">

	<input type="hidden" name="saveContract" value="Y" />

	Вид договора*<br />
	<select name="type">
		<? foreach ($arResult['ENUMS']['UF_TYPE'] as $arFields): ?>
			<option <?=($_REQUEST['type'] ? : $arResult['CONTRACT']['UF_TYPE']) == $arFields['ID']
				? 'selected="selected"' : ''?> value="<?=$arFields['ID']?>"><?=$arFields['VALUE']?></option>
		<? endforeach; ?>
	</select>

	<br /><br />

	Правоустанавливающие документы
	<div class="legal">
		<input type="file" name="legal[]" />
	</div>
	<button id="addFile">Добавить файл</button>

	<br /><br />

	Идентификаторы книг
	<div class="books">
		<? if (!empty($_REQUEST['book'])): ?>
			<? foreach ($_REQUEST['book'] as $k => $v):
				if (empty($v)) continue; ?>
				<input type="input" name="book[<?=$k?>]" value="<?=$v?>" style="width:400px;" /><br />
			<? endforeach; ?>
		<? else: ?>
			<? foreach ($arResult['CONTRACT']['UF_BOOK'] as $k => $v):
				if (empty($v)) continue; ?>
				<input type="input" name="book[<?=$k?>]" value="<?=$v?>" style="width:400px;" /><br />
			<? endforeach; ?>
		<? endif; ?>
		<input type="input" name="book[]" value="" style="width:400px;" /><br />
	</div>
	<button id="addBook">Добавить книгу</button><br /><br />

	Комментарий:<br />
	<textarea name="comment"><?=htmlspecialcharsBX($_REQUEST['comment']?:$arResult['CONTRACT']['UF_COMMENT'])?></textarea>

	<br /><br />
	<input type="submit" class="b-search_bth bbox" name="action" value="Сохранить черновик" />
	<input type="submit" class="b-search_bth bbox" name="action" value="Отправить на согласование" />
</form>

<script type="text/javascript">
	$(function(){
		$('#addFile').click(function(event) {
			event.preventDefault();
			$('<input type="file" name="legal[]" /><br />').fadeIn('slow').appendTo('.legal');
		});
		$('#addBook').click(function(event) {
			event.preventDefault();
			$('<input type="input" name="book[]" style="width:400px;" /><br />').fadeIn('slow').appendTo('.books');
		});
		$('#remove').click(function() {
			/*$('.field:last').remove();*/
		});
	});
</script>