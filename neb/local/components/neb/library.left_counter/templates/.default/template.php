<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
//$arResult['PROPERTY_COLLECTIONS_VALUE'] = 2;
?>

<!-- КАРТОЧКА С ИНФОРМАЦИЕЙ -->
<div class="lk-sidebar__card">
    <div class="lk-sidebar__card-info">
		  <?
        if(!empty($arResult['PROPERTY_STATUS_VALUE'])):
        ?>
        <p class="lk-sidebar__card-text lk-sidebar__card-text--small"><?=$arResult['PROPERTY_STATUS_VALUE']?></p>
		<?endif;?>
        <h5 class="lk-sidebar__card-title">
            <?=$arResult['NAME']?>
        </h5>
        <?
        if(!empty($arResult['PREVIEW_PICTURE'])):
        ?>
        <img class="lk-sidebar__card-avatar" src="<?=$arResult['PREVIEW_PICTURE']['src']?>" alt="<?=$arResult['NAME']?>">
        <?endif;?>
        <?
        if(!empty($arResult['PROPERTY_PUBLICATIONS_VALUE'])):
        ?>
        <p class="lk-sidebar__card-text">
            <?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_FUND');?>
        </p>
        <p class="lk-sidebar__card-text lk-sidebar__card-text--count">
            <?=getNumberNeb($arResult['PROPERTY_PUBLICATIONS_VALUE']);?>
        </p>
        <p class="lk-sidebar__card-text">
            <?=GetEnding($arResult['PROPERTY_PUBLICATIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_2'))?>
        </p>
        <?endif;?>
    </div>
    <div class="lk-sidebar__card-divider"></div>
    <?
    if(!empty($arResult['PROPERTY_COLLECTIONS_VALUE'])):
    ?>
    <div class="lk-sidebar__card-collections">
        <p class="lk-sidebar__card-text">
            <?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_INC');?>
        </p>
        <p class="lk-sidebar__card-text lk-sidebar__card-text--count">
            <?=getNumberNeb($arResult['PROPERTY_COLLECTIONS_VALUE']);?>
        </p>
        <p class="lk-sidebar__card-text">
            <?=GetEnding($arResult['PROPERTY_COLLECTIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_2'))?>
        </p>
    </div>
    <div class="lk-sidebar__card-divider"></div>
    <?endif;?>
    <div class="lk-sidebar__card-readers">
        <?
        if(!empty($arResult['PROPERTY_USERS_VALUE'])):
        ?>
        <p>
            <?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_READER');?>
        </p>
        <p>
            <span class="fa fa-user"></span> x <?=number_format($arResult['PROPERTY_USERS_VALUE'], 0, '', ' ')?>
        </p>
        <?endif;?>
        <?
        if(!empty($arResult['PROPERTY_VIEWS_VALUE'])):
        ?>
        <p>
            <?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_VIEWERS');?>
        </p>
        <p>
            <span  class="fa fa-eye"></span> x <?=number_format($arResult['PROPERTY_VIEWS_VALUE'], 0, '', ' ')?>
        </p>
        <?endif;?>
    </div>
    <!-- <div class="lk-sidebar__card-divider"></div> -->
</div>
<!-- /КАРТОЧКА С ИНФОРМАЦИЕЙ -->