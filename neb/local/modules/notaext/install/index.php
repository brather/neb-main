<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang . "/lang/", "/install/index.php"));

/**
 * Class notaext
 * Notaext - это общий модуль, позволяющий устаналивать и автоматически подгружать "расширения" для сайтов.
 * Оные расширения находятся в папке ext/ и подчиняются простым правилам.
 * Создание расширения значительно проще, чем создание отдельностоятщего модуля благодаря автоинсталляторам.
 */
Class notaext extends CModule
{
    var $MODULE_ID = __CLASS__;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_FOLDER;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $MODULE_GROUP_RIGHTS = "Y";

    const BITRIX_HOLDER = "bitrix";
    const LOCAL_HOLDER = "local";
    var $MODULE_HOLDER = '';

    function __construct()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $this->MODULE_FOLDER = substr($path, 0, strlen($path) - strlen("/install/index.php"));
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));

        include($path . "/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->MODULE_NAME = GetMessage("SCOM_INSTALL_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("SCOM_INSTALL_DESCRIPTION");
        $this->PARTNER_NAME = GetMessage("SPER_PARTNER");
        $this->PARTNER_URI = GetMessage("PARTNER_URI");


        $moduleHolder = self::LOCAL_HOLDER;
        $pathToInclude = $_SERVER['DOCUMENT_ROOT'] . "/" . $moduleHolder . "/modules/" . $this->MODULE_ID . "/include.php";
        if (!file_exists($pathToInclude))
            $moduleHolder = self::BITRIX_HOLDER;

        $this->MODULE_HOLDER = $moduleHolder;

    }

    function InstallDB($install_wizard = true)
    {
        global $DB, $DBType, $DBName, $APPLICATION;
        $this->errors = false;

        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/" . $this->MODULE_HOLDER . "/modules/" . $this->MODULE_ID . "/install/db/install.sql");

        if ($this->errors !== false) {
            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }

        /*
        Фиксим длинну поля TO_CLASS в таблице b_module_to_module. Для того что бы можно было регистрировать события с длинными именами классов
        */
        $db_res = $DB->Query("SELECT DATA_TYPE, CHARACTER_MAXIMUM_LENGTH FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='" . $DBName . "' AND TABLE_NAME='b_module_to_module' AND COLUMN_NAME='TO_CLASS'");
        if ($arRes = $db_res->Fetch()) {
            if ($arRes['DATA_TYPE'] == 'varchar' and intval($arRes['CHARACTER_MAXIMUM_LENGTH']) < 100)
                $DB->Query("ALTER TABLE `b_module_to_module` CHANGE `TO_CLASS` `TO_CLASS` VARCHAR( 130 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ;");
        }

        RegisterModule($this->MODULE_ID);
        //RegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", $this->MODULE_ID, "GalleryDetailUserProperty", "GetUserTypeDescription");

        #IblockTools Events add
        RegisterModuleDependences("iblock", "OnBeforeIBlockAdd", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "AddIBlocksEvent");
        RegisterModuleDependences("iblock", "OnAfterIBlockAdd", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "AddIBlocksEvent");

        RegisterModuleDependences("iblock", "OnBeforeIBlockUpdate", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "UpdateIBlocksEventBefore");
        RegisterModuleDependences("iblock", "OnAfterIBlockUpdate", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "UpdateIBlocksEventAfter");

        RegisterModuleDependences("iblock", "OnBeforeIBlockDelete", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "DeleteIBlocksEvent");

        #menu
        RegisterModuleDependences("main", "OnBuildGlobalMenu", $this->MODULE_ID, 'CMenuExt', "OnBuildGlobalMenu");

        return true;
    }

    function UnInstallDB($arParams = Array())
    {
        global $DB, $DBType, $APPLICATION;

        $this->errors = false;

        /*
        Деинсталируем все установленные расширения
        */
        \Bitrix\Main\Loader::IncludeModule($this->MODULE_ID);
        $strFolder = substr(__DIR__, 0, strrpos(__DIR__, '/')) . '/ext';
        $dirExts = CExt::getDir($strFolder);
        foreach ($dirExts as $ext) {
            $ext_dir = $strFolder . "/" . $ext;
            $ext_dir_info = $ext_dir . '/index.php';
            if (file_exists($ext_dir_info)) {
                include_once($ext_dir_info);
                if (class_exists($ext)) {
                    $info = new $ext;
                    $status = $info->IsInstalled();
                    if ($status === true)
                        $info->Uninstall();
                }
            }
        }

        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/" . $this->MODULE_HOLDER . "/modules/" . $this->MODULE_ID . "/install/db/uninstall.sql");

        if ($this->errors !== false) {
            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }

        //UnRegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", $this->MODULE_ID, "GalleryDetailUserProperty", "GetUserTypeDescription");

        #IblockTools Events remove
        UnRegisterModuleDependences("iblock", "OnBeforeIBlockAdd", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "AddIBlocksEvent");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockAdd", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "AddIBlocksEvent");

        UnRegisterModuleDependences("iblock", "OnBeforeIBlockUpdate", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "UpdateIBlocksEventBefore");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockUpdate", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "UpdateIBlocksEventAfter");

        UnRegisterModuleDependences("iblock", "OnBeforeIBlockDelete", $this->MODULE_ID, 'Bitrix\NotaExt\Iblock\IblockTools', "DeleteIBlocksEvent");

        #menu
        UnRegisterModuleDependences("main", "OnBuildGlobalMenu", $this->MODULE_ID, 'CMenuExt', "OnBuildGlobalMenu");

        UnRegisterModule($this->MODULE_ID);

        return true;
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . "/" . $this->MODULE_HOLDER . "/modules/" . $this->MODULE_ID . "/install/admin", $_SERVER['DOCUMENT_ROOT'] . "/bitrix/admin");

        //CopyDirFiles($this->MODULE_FOLDER."/install/components", $_SERVER["DOCUMENT_ROOT"]."/local/components/nota", true, true);

        return true;
    }

    function UnInstallFiles()
    {

        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'] . "/" . $this->MODULE_HOLDER . "/modules/" . $this->MODULE_ID . "/install/admin", $_SERVER['DOCUMENT_ROOT'] . "/bitrix/admin");

        //DeleteDirFiles($this->MODULE_FOLDER."/install/components", $_SERVER["DOCUMENT_ROOT"]."/local/components/nota");
        #DeleteDirFilesEx("/bitrix/js/advertising/");
        return true;
    }

    function DoInstall()
    {
        global $APPLICATION, $step;

        $this->InstallFiles();
        $this->InstallDB(false);
        $this->InstallEvents();
        return true;
    }

    function DoUninstall()
    {
        global $APPLICATION, $step;

        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnInstallEvents();
        return true;
    }
}
