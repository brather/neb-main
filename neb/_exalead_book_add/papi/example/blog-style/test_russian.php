<?php
	require('../../PushAPI.inc');
	require('tick.inc');

	try {
		echo "<pre>\n";
		echo "Create\n";

		$uri = '000000111144444DDDFF5555';

		$papi = PushAPIFactory::createHttp("213.208.168.8", 10002, "test");

		echo "Ping\n";
		$papi->ping();

		$myPart1 = new Part(array('content' =>
			'<html><body>Проверка</body></html>',
			#'<html><body>test en text</body></html>',
			'directives' =>
			array('mimeHint' => 'text/html',
				'encoding' => 'UTF-8')
			)
		);
		$myDoc1 = new Document(array('uri' => $uri,
			'parts' => $myPart1,
			'metas' => array('path' => 'Path/To/Example/1')
			)
		);

		tickSet('serialization');

		echo "Checking if document exists\n";
		$stamp = $papi->getDocumentStatus($uri);
		if ($stamp !== false) {
			echo "Yes, its stamp is " . $stamp . "\n";
		} else {
			echo "No, it does not yet exist\n";
		}
		echo "Pushing documents\n";

		$resp = $papi->addDocument(array($myDoc1));

		tickSet('pushing');

		echo "Set checkpoint\n";
		$serial = $papi->setCheckpoint(0, true);

		$cp = $papi->enumerateCheckpointInfo();
		foreach($cp as $k => $v) {
			echo "Checkpoint: name='" . $k . "' value='" . $v . "'\n";
		}

		$se = $papi->enumerateSyncedEntries('http://example.com/',
			PushAPI::RECURSIVE_DOCUMENTS);
		foreach($se as $url => $stamp) {
			echo "Entry: url='" . $url . "' stamp='" . $stamp . "'\n";
		}

		echo "Waiting for checkpoint serial " . $serial . "\n";

		echo "Triggering indexing job\n";
		$papi->triggerIndexingJob();
		tickSet('checkpoint+trigger');

		echo "Waiting for documents to be searchable ..\n";
		while(!$papi->areDocumentsSearchable($serial)) {
			echo "Waiting .. current  checkpoint: " . $papi->getCheckpoint()
			. "        " . "\r";
			sleep(3);
		}
		tickSet('indexing');

		/* Update a document */
		$docUpdate = new Document(array('uri' => 'http://example.com/doc1.html',
			'metas' => array('title' => 'Bye bye!')
			)
		);
		$resp = $papi->updateDocument($docUpdate, array('title'));
		$serial = $papi->setCheckpoint(0, true);
		$papi->triggerIndexingJob();
		while(!$papi->areDocumentsSearchable($serial)) {
			echo "Waiting .. current  checkpoint: " . $papi->getCheckpoint()
			. "        " . "\r";
			sleep(3);
		}
		tickSet('updating');

		$papi->ping();
		$papi->close();
		echo "All done!\n";

		tickShow();
		echo "</pre>\n";
	} catch(PushAPIFactory $e)  {
		echo "Error: " . $e->getMessage() . "\n";
	}

?>
