<?
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 20.05.2016
 * Time: 18:33
 */

use \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\Main\Application,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\FileTable,
    \Bitrix\Main\Mail\Event,
    \Bitrix\Main\UserTable,
    \Bitrix\Main\UserGroupTable,
    \Bitrix\Main\GroupTable,
    \Neb\Main\Helper\MainHelper;

/**
 * Класс для работы с правообладателями
 */
class RightHolder
{
    const RIGHTHOLDER_HBLOCK_CONTACTS = 'RightHoldersContracts';
    const RIGHTHOLDER_HBLOCK_HISTORY  = 'RightHoldersContractsHistory';

    private $iContractId, $iUserId, $arHBlock, $arHBlockCodes;
    public $arUserGroups, $arEnum, $arStatus;

    function __construct($iContractId = null) {

        $this->iContractId  = intval($iContractId);

        $this->includeModules();
        $this->getUser();
        $this->getHIblocks();
        $this->getHIblocksCodes();
        $this->getUserGroups();
        $this->getUserFieldsEnum();
        $this->getStatuses();
    }

    /** =========================================== PRIVATE METHODS ================================================ */

    /**
     * Подключает модули
     */
    private static function includeModules() {
        Loader::includeModule('highloadblock');
    }

    /**
     * Получение идентификатора текущего пользователя
     */
    private function getUser() {
        $obUser         = new \CUser();
        $this->iUserId  = intval($obUser->GetID());
    }

    /**
     * Получает идентификаторы highload-инфоблоков
     * @return array
     */
    private function getHIblocks() {

        $arResult = [];

        // при первом запуске скрипта должен запуститься скрипт миграции и потом опять выбораны данные по h-блокам
        for ($i = 0; $i < 2; $i++) {

            $rsHIblocks = HighloadBlockTable::getList(['filter' => [
                'NAME' => [self::RIGHTHOLDER_HBLOCK_CONTACTS, self::RIGHTHOLDER_HBLOCK_HISTORY]]
            ]);
            while ($arItem = $rsHIblocks->fetch())
                $arResult[$arItem['ID']] = $arItem;

            if (count($arResult) == 2)
                break;

            /** ================================================ TODO: УДАЛИТЬ ПОСЛЕ РЕЛИЗА НЭБ-3 (ПОСЛЕ ИСПОЛНЕНИЯ НА ПРОДЕ) ==============================================================*/
            require_once 'RightholderMigration.php';
            RightholderMigration::migrationMethod();
        }

        $this->arHBlock  = $arResult;
    }

    /**
     * Возвращает коды h-блоков из массива
     *
     * @return array
     */
    private function getHIblocksCodes() {

        $arResult = [];

        foreach ($this->arHBlock as $arItem)
            $arResult[$arItem['NAME']] = $arItem['ID'];

        $this->arHBlockCodes = $arResult;
    }

    /**
     * Получает информацию о пользователе
     *
     * @param $iUserId
     * @return array
     */
    private static function getUserInfo($iUserId) {

        $arResult = [];

        if (intval($iUserId) < 1)
            return $arResult;

        $arResult = UserTable::getById($iUserId)->fetch();
        if ($arResult['ID']) {
            $arResult['FULL_NAME'] = trim($arResult['LAST_NAME'] . ' ' . $arResult['NAME']
                    . ' ' . $arResult['SECOND_NAME']) . ' [' . $arResult['ID'] . ']';
        }

        return $arResult;
    }

    /**
     * Получает группы пользователя
     *
     * @return array
     */
    private function getUserGroups()
    {
        $arUserGroups = [];

        if (intval($this->iUserId) < 1)
            return $arUserGroups;

        // получение идентификаторов групп текущего пользователя
        $rsUserGroups = UserGroupTable::getList(array(
            'filter' => array('USER_ID' => $this->iUserId),
            'select' => array('GROUP_ID')
        ));
        while ($arFields = $rsUserGroups->fetch())
            $arUserGroups[$arFields['GROUP_ID']] = $arFields['GROUP_ID'];

        // получение символьных кодов групп текущего пользователя
        $rsUserGroups = GroupTable::getList(array(
            'filter' => array(
                'ID'         => array_keys($arUserGroups),
                '!STRING_ID' => false,
                'ACTIVE'     => 'Y',
            ),
            'select' => array('ID', 'STRING_ID')
        ));
        while ($arFields = $rsUserGroups->fetch())
            if ($arUserGroups[$arFields['ID']])
                $arUserGroups[$arFields['ID']] = $arFields['STRING_ID'];

        $this->arUserGroups = $arUserGroups;
    }

    /**
     * Получает все пользовательские поля типа "список" и их значения highload-инфоблоков
     *
     * @return array
     */
    private function getUserFieldsEnum()
    {
        $arResult = [];

        $iHIblock = $this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_CONTACTS];

        if (!$iHIblock)
            return $arResult;

        // поиск ID свойств типа "список"
        $rsUserTypeEntityEnum = \CUserTypeEntity::GetList(
            array('ID' => 'ASC'),
            array('ENTITY_ID' => 'HLBLOCK_'.$iHIblock, 'USER_TYPE_ID' => 'enumeration')
        );
        while ($arFields = $rsUserTypeEntityEnum->Fetch())
            $arUserTypeEntityEnum[$arFields['ID']] = $arFields['FIELD_NAME'];

        // выборка значений свойств типа "список"
        if (!empty($arUserTypeEntityEnum)) {
            $obUserFieldEnum  = new \CUserFieldEnum();
            $rsUserFieldEnum = $obUserFieldEnum->GetList(
                array('SORT' => 'ASC'),
                array("USER_FIELD_ID" => array_keys($arUserTypeEntityEnum))
            );
            while ($arFields = $rsUserFieldEnum->Fetch())
                $arResult[$arUserTypeEntityEnum[$arFields['USER_FIELD_ID']]][$arFields['ID']] = $arFields;
        }

        $this->arEnum = $arResult;
    }

    /**
     * Преобразовывает массив статусов
     *
     * @return array
     */
    private function getStatuses() {

        $arResult = array();

        foreach ($this->arEnum['UF_STATUS'] as $arItem)
            $arResult[$arItem['XML_ID']] = $arItem['ID'];

        $this->arStatus = $arResult;
    }

    /** ============================================ PUBLIC METHODS ================================================ */

    /**
     * Получает договора
     *
     * @return array
     */
    public function getContracts() {

        $arResult = [];

        $obRequest = Application::getInstance()->getContext()->getRequest();
        $sBy       = htmlspecialcharsBX(strtoupper($obRequest->getQuery("by")));
        $sOrder    = htmlspecialcharsBX(strtoupper($obRequest->getQuery("order")));
        $sDateFrom = htmlspecialcharsBX($obRequest->getQuery("dateFrom"));
        $sDateTo   = htmlspecialcharsBX($obRequest->getQuery("dateTo"));
        $iStatus   = intval($obRequest->getQuery("status"));
        $arDateFrom = explode('.', $sDateFrom);
        $arDateTo   = explode('.', $sDateTo);

        $arFilter = $arOrder = array();

        if (!empty($sBy) && !empty($sOrder))
            $arOrder[$sBy] = $sOrder;
        else
            $arOrder['ID'] = 'DESC';

        if (!empty($sDateFrom) && checkdate($arDateFrom[1], $arDateFrom[0], $arDateFrom[2]))
            $arFilter['>=UF_DATE_FROM'] = $sDateFrom;

        if (!empty($sDateTo) && checkdate($arDateTo[1], $arDateTo[0], $arDateTo[2]))
            $arFilter['<=UF_DATE_FROM'] = $sDateTo;

        if ($iStatus)
            $arFilter['UF_STATUS'] = $iStatus;

        // правообладатель видит только свои договора
        if ($this->isRightholder()) {
            $arFilter['UF_USER_ID'] = $this->iUserId;
        }
        // оператору не видны черновики правообладателей
        else if ($this->isOperator() && empty($arFilter['UF_STATUS'])) {
            $arFilter['!UF_STATUS'] = $this->arStatus['draft'];
        }

        if ($this->iContractId)
            $arFilter['ID'] = $this->iContractId;

        $obContractsIBlock = HighloadBlockTable::compileEntity(
            $this->arHBlock[$this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_CONTACTS]])->getDataClass();
        $rsContracts = $obContractsIBlock::getList(array(
            "order"  => $arOrder,
            "filter" => $arFilter,
        ));
        while ($arFields = $rsContracts->fetch()) {
            $arFields['STATUS'] = $this->arEnum['UF_STATUS'][$arFields['UF_STATUS']];
            $arResult[$arFields['ID']] = $arFields;
        }

        return $arResult;
    }

    /**
     * Получает активные договора (для статистики)
     *
     * @return array
     */
    public function getContractsActive() {

        $arResult = [];

        $arFilter = array( // верно
            '<=UF_DATE_FROM' => (new DateTime()),
            '>=UF_DATE_TO'   => (new DateTime()),
            'UF_STATUS'      => $this->arStatus['signed']
        );

        // правообладатель видит только свои договора
        if ($this->isRightholder())
            $arFilter['UF_USER_ID'] = $this->iUserId;

        $obContractsIBlock = HighloadBlockTable::compileEntity(
            $this->arHBlock[$this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_CONTACTS]])->getDataClass();
        $rsContracts = $obContractsIBlock::getList(array(
            'select' => array('ID', 'UF_BOOK'),
            "order"  => array("ID" => "DESC"),
            "filter" => $arFilter,
        ));
        while ($arFields = $rsContracts->fetch())
            $arResult[$arFields['ID']] = $arFields;

        return $arResult;
    }

    /**
     * Получает историю статусов договоров
     *
     * @param $arContracts
     * @return array
     */
    public function getHistory($arContracts) {

        $arResult = [];

        if (empty($arContracts))
            return $arResult;

        $obHistoryIBlock = HighloadBlockTable::compileEntity(
            $this->arHBlock[$this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_HISTORY]])->getDataClass();
        $rsStatuses = $obHistoryIBlock::getList(array(
            "select" => array(
                "ID",
                "UF_CONTRACT",
                "UF_DATE",
                "UF_STATUS",
                "UF_COMMENT"
            ),
            "order"  => array("ID" => "DESC"),
            "filter" => array('UF_CONTRACT'=> array_keys($arContracts)),
        ));
        while ($arFields = $rsStatuses->fetch())
            $arResult[$arFields['UF_CONTRACT']][$arFields['ID']] = $arFields;

        return $arResult;
    }

    /**
     * Получает договора для отправки напоминания о их продлении
     *
     * @param int $iDays
     * @return array
     */
    public function getRemindContracts($iDays = 30) {

        $obContractsIBlock = HighloadBlockTable::compileEntity(
            $this->arHBlock[$this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_CONTACTS]])->getDataClass();
        
        $arResult = $obContractsIBlock::getList(array(
            "order"  => array("ID" => "ASC"),
            "filter" => array(
                'UF_STATUS'      => $this->arStatus['signed'],                   // статус - подисан
                '<=UF_DATE_FROM' => (new DateTime()),                            // старше даты начала договора
                'UF_DATE_TO'     => (new DateTime)->add('+' . $iDays . ' days')  // прибавка даты
            ),
        ))->fetchAll();

        return $arResult;
    }

    /**
     * Получает пользователей, документы и книги из договора
     * @param array $arContracts
     * @return array
     */
    public static function getContractsEntities($arContracts) {

        $arUsers = $arFiles = $arBooks = [];

        foreach ($arContracts as $arItem) {
            if ($arItem['UF_USER_ID'])
                $arUsers[] = $arItem['UF_USER_ID'];
            foreach ($arItem['UF_LEGAL_DOCUMENTS'] as $iItem)
                $arFiles[] = $iItem;
            foreach ($arItem['UF_BOOK'] as $iItem)
                $arBooks[] = $iItem;
        }

        $arResult = [
            'USERS' => $arUsers,
            'FILES' => $arFiles,
            'BOOKS' => $arBooks
        ];

        return $arResult;
    }

    /**
     * Получает пользователей из БД по массиву идентификаторов
     * @param array $arUsers
     * @return array
     */
    public static function getUsers($arUsers) {

        // проверять принадлежность группе правообладателей?

        $arResult = [];

        if (empty($arUsers))
            return $arResult;

        $rsUsers = UserTable::getList(array(
            'order'  => array('ID' => 'ASC'),
            'filter' => array("ID" => $arUsers, 'ACTIVE' => 'Y'),
            'select' => array('ID', 'LAST_NAME', 'NAME', 'SECOND_NAME', 'EMAIL')
        ));
        while ($arItem = $rsUsers->fetch())
            $arResult[$arItem['ID']] = $arItem;

        return $arResult;
    }

    /**
     * Получает книги из Exalead по массиву идентификаторов
     * @param $arBook
     * @return array
     */
    public static function getBooksFromExalead($arBook) {

        $arResult = [];

        if (empty($arBook))
            return $arResult;

        // получение книг из Exalead
        Loader::includeModule('nota.exalead');

        $query  = new \Nota\Exalead\SearchQuery();
        $client = new \Nota\Exalead\SearchClient();

        foreach ($arBook as $sBook) {
            $sBook = trim($sBook);
            if (empty($sBook))
                continue;

            $query->getById($sBook);
            $ar = $client->getResult($query);
            if (!empty($ar))
                $arResult[$sBook] = $ar;
        }
        unset($query, $client);

        return $arResult;
    }

    /**
     * Проверяет список книг на участие в договорах.
     * @param $arContractBooks
     * @return array
     */
    public function getBooksWithCopyright($arContractBooks) {

        $arResult = self::getBooksFromExalead($arContractBooks);

        if (!empty($arResult)) {

            $obContractsIBlock = HighloadBlockTable::compileEntity(
                $this->arHBlock[$this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_CONTACTS]])->getDataClass();
            $rsContracts = $obContractsIBlock::getList(array(
                "select" => array(
                    "ID",
                    "UF_NUMBER",
                    "UF_BOOK"
                ),
                "order" => array("ID" => "DESC"),
                "filter" => array(
                    'UF_BOOK'        => array_keys($arResult),              // массив проверяемых книг
                    'UF_STATUS'      => $this->arStatus['signed'],          // статус - подписан
                    '!ID'            => intval($this->iContractId),         // исключение id текущего договора
                    '<=UF_DATE_FROM' => (new DateTime()),                   // активный по дате
                    '>=UF_DATE_TO'   => (new DateTime()),
                ),
            ));
            while ($arFields = $rsContracts->fetch()) {
                foreach ($arFields['UF_BOOK'] as $sBook)
                    if (!empty($arResult[$sBook]))
                        $arCopyright[$sBook][] = $arFields['ID'];
            }
        }

        // проверка нахождения в БД, открытости доступа
        foreach ($arContractBooks as $sBook) {

            if (empty($sBook)) continue;

            if (empty($arResult[$sBook])) {
                $arResult[$sBook]['ERROR'] = 1;
                $arResult[$sBook]['STATUS'] = 'Ошибка! Книга не найдена в БД!';
            } elseif ($arResult[$sBook]['isprotected'] == 0) {
                $arResult[$sBook]['ERROR'] = 1;
                $arResult[$sBook]['STATUS'] = 'Ошибка! Данная книга находится в открытом доступе и не может быть защищена!';
            }
            else {
                $arResult[$sBook]['ERROR'] = 0;
                $arResult[$sBook]['STATUS'] = 'Допущена к защите';
            }
        }
        // проверка копирайтов
        foreach ($arResult as $sBook => $arItem)
            if (!empty($arCopyright[$sBook])) {
                $arResult[$sBook]['ERROR']  = 1;
                $arResult[$sBook]['STATUS'] = 'Ошибка! Данная книга уже защищена в договоре #' . implode(', #', $arCopyright[$sBook]);
            }

        return $arResult;
    }

    /**
     * Получает файлы из БД по массиву идентификаторов
     * @param $arFiles
     * @return array
     */
    public static function getFiles($arFiles) {

        $arResult = [];

        if (empty($arFiles))
            return $arResult;

        $rsFile = FileTable::getList(array('filter' => array("ID" => $arFiles)));
        while($arFields = $rsFile->fetch()) {
            $arFields['PATH']         = '/upload/' . $arFields["SUBDIR"] . "/" . $arFields["FILE_NAME"];
            $arFields['FORMAT_SIZE']  = \CFile::FormatSize($arFields['FILE_SIZE']);
            $arResult[$arFields['ID']] = $arFields;
        }

        return $arResult;
    }

    /**
     * Получет массив доступных действий с договором на детальной странице
     * @param $arContracts
     * @param $arBooks
     * @return array
     */
    public function getActions($arContracts, $arBooks)
    {
        $arResult = [];

        if (empty($arContracts))
            return $arResult;

        $arContract = self::getFirst($arContracts);

        if ($this->isRightholder() && $arContract['STATUS']['XML_ID'] == 'draft')

            $arResult = array('PROCESSED' => true);

        elseif (
            ($this->isOperator() || $this->isAdmin())         // пользователь - оператор ВЧЗ или админ
            && $arContract['STATUS']['XML_ID'] == 'processed' // статус - на согласовании
        ) {
            $bDraft = $bSign = true;

            if (empty($arBooks)) {
                $bSign = false;
            } else {
                foreach ($arBooks as $arBook)
                    if ($arBook['ERROR'])
                        $bSign = false;
            }
            $arResult = array('DRAFT' => $bDraft, 'SIGN' => $bSign);
        }

        return $arResult;
    }


    /**
     * Сохраняет договор
     */
    public function saveContract() {

        $arContract = $arBook = $arError = [];

        // компиляция сущностей
        $obContractsIBlock = HighloadBlockTable::compileEntity(
            $this->arHBlock[$this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_CONTACTS]])->getDataClass();
        $obHistoryIBlock   = HighloadBlockTable::compileEntity(
            $this->arHBlock[$this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_HISTORY]])->getDataClass();

        // получение массивов $_POST и $_FILES
        $obRequest = Application::getInstance()->getContext()->getRequest();
        $arPost  = $obRequest->getPostList()->toArray();
        $arFiles = $obRequest->getFileList()->toArray();
        $arFiles = self::getFilesArray($arFiles);

        // метод может работать только при сохранении формы
        if ($arPost["saveContract"] != 'Y')
            return [];

        // получение информации о контракте
        if ($this->iContractId) {

            $arContract = $obContractsIBlock::getList(["filter" => ['ID' => $this->iContractId]])->fetch();

            if ($this->arEnum['UF_STATUS'][$arContract['UF_STATUS']]['XML_ID'] != 'draft')
                $arError[] = 'Редактирование договора возможно только на этапе черновика!';

            if ($arContract['UF_USER_ID'] != $this->iUserId)
                $arError[] = 'Вы не можете редактировать договор другого правообладателя!';
        }

        // проверка прав доступа к скрипту
        if (!($this->isRightholder() || $this->isAdmin()))
            $arError[] = 'Вы не принадлежите группе правообладателей!';

        // выбор книг из Exalead по POST-массиву c проверкой Copyright
        $arBook = self::getBooksWithCopyright($arPost['book']);

        // получение массива ошибок из книг
        if (empty($arBook))
            $arError[] = 'Укажите хотябы одну книгу!';
        else
            foreach ($arBook as $sBook => $arItem)
                if ($arItem['ERROR'])
                    $arError[] = $arItem['STATUS'];

        // определение статуса события
        if (stripos($arPost["action"], 'чернов') !== false)
            $iStatus = $this->arStatus['draft'];
        elseif (stripos($arPost["action"], 'соглас') !== false)
            $iStatus = $this->arStatus['processed'];

        // распознание типа договора
        $iType = null;
        if (!empty($this->arEnum['UF_TYPE'][$arPost["type"]]))
            $iType = $arPost["type"];

        if ($iStatus < 1)
            $arError[] = 'Не удалось распознать тип статуса!';

        // работа скрипта может быть продолжена только при отсутствии ошибок
        if (!empty($arError))
            return $arError;



        // комментарий
        $sComment   = htmlspecialcharsBX($arPost["comment"]);

        // сохранение файлов в БД и получение их идентификаторов
        $arFileToDb = [];
        foreach ($arFiles['legal'] as $arFile) {
            $arFields['file'] = $arFile;
            if (CFile::SaveForDB($arFields, "file", "highloadblock"))
                $arFileToDb[] = $arFields['file'];
        }

        //  ================================== РЕДАКТИРОВАНИЕ СОХРАНЕННОГО ДОГОВОРА ===================================
        if ($this->iContractId) {

            $arUpdate = [
                'UF_TYPE'    => $iType,
                'UF_STATUS'  => $iStatus,
                'UF_BOOK'    => array_keys($arBook),
                'UF_COMMENT' => $sComment
            ];
            if (!empty($arFileToDb))
                $arUpdate['UF_LEGAL_DOCUMENTS'] = $arFileToDb;

            if (!empty($arUpdate)) {

                // сохранение договора
                $result = $obContractsIBlock::update($this->iContractId, $arUpdate);

                if ($result->isSuccess()) {

                    // сохранение операции в историю
                    $arHistory = array(
                        "UF_CONTRACT" => $this->iContractId,
                        "UF_DATE"     => (new DateTime()),
                        "UF_STATUS"   => $this->arEnum['UF_STATUS'][$iStatus]['VALUE'],
                        "UF_COMMENT"  => $this->iUserId
                    );
                    $obHistoryIBlock::add($arHistory);

                    // отправка сообщения оператору при смене статуса на "На согласовании"
                    if ($this->arEnum['UF_STATUS'][$iStatus]['XML_ID'] == 'processed')
                        self::sendOperatorEmail($this->iContractId, $this->iUserId, $arBook, self::getFiles($arFileToDb),
                            $this->arEnum['UF_TYPE'][$iType]['VALUE'], $this->arEnum['UF_STATUS'][$iStatus]['VALUE'], $sComment);

                    // перенаправление на список договоров
                    LocalRedirect('/profile/'); die();
                }
            }
        }
        //  ====================================== СОЗДАНИЕ НОВОГО ДОГОВОРА ==========================================
        else {

            $arAddData = array(
                "UF_USER_ID"         => $this->iUserId,
                "UF_NUMBER"          => null,
                "UF_DATE_FROM"       => (new DateTime()),
                "UF_DATE_TO"         => null,
                "UF_STATUS"          => $iStatus,
                "UF_TYPE"            => $iType,
                "UF_LEGAL_DOCUMENTS" => $arFileToDb,
                "UF_BOOK"            => array_keys($arBook)
            );
            $result = $obContractsIBlock::add($arAddData);

            if ($result->isSuccess()) {
                // запись в историю
                $arHistory = array(
                    "UF_CONTRACT" => $result->getId(),
                    "UF_DATE"     => (new DateTime()),
                    "UF_STATUS"   => $this->arEnum['UF_STATUS'][$iStatus]['VALUE'],
                    "UF_COMMENT"  => $this->iUserId
                );
                $obHistoryIBlock::add($arHistory);

                // отправка сообщения оператору при смене статуса на "На согласовании"
                if ($this->arEnum['UF_STATUS'][$iStatus]['XML_ID'] == 'processed')
                    self::sendOperatorEmail($result->getId(), $this->iUserId, $arBook, self::getFiles($arFileToDb),
                        $this->arEnum['UF_TYPE'][$iType]['VALUE'], $this->arEnum['UF_STATUS'][$iStatus]['VALUE'], $sComment);

                // редирект на страницу списка договоров
                LocalRedirect('/profile/'); die();
            }
        }
    }

    /**
     * Обновляет договор и его историю
     * @param $arContracts
     * @param null $arFiles
     * @return null
     */
    public function updateContract($arContracts, $arFiles = null) {

        if (empty($arContracts))
            return null;

        $obRequest  = Application::getInstance()->getContext()->getRequest();

        //$iContract  = intval($obRequest->getQuery("contract"));
        $sJson      = htmlspecialcharsBX($obRequest->getQuery("json"));
        $sAction    = htmlspecialcharsBX($obRequest->getQuery("action"));
        $sComment   = htmlspecialcharsBX($obRequest->getQuery("comment"));

        // обновление договора
        if ($sJson == 'Y') {

            $arContract = $arContracts[$this->iContractId];

            $obContractsIBlock = HighloadBlockTable::compileEntity(
                $this->arHBlock[$this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_CONTACTS]])->getDataClass();
            $obHistoryIBlock   = HighloadBlockTable::compileEntity(
                $this->arHBlock[$this->arHBlockCodes[self::RIGHTHOLDER_HBLOCK_HISTORY]])->getDataClass();

            $arJson = array(
                'success' => false
            );

            // правообладатель
            if ($this->isRightholder()) {

                if ($sAction == 'processed' && $arContract['STATUS']['XML_ID'] == 'draft') {

                    $iStatus = $this->arStatus['processed'];
                    $sStatus = $this->arEnum['UF_STATUS'][$iStatus]['VALUE'];

                    // обновление договора
                    $result = $obContractsIBlock::update($this->iContractId, array('UF_STATUS' => $iStatus));
                    if ($arJson['success'] = $result->isSuccess())
                        $arJson['message'] = 'Договор успешно отправлен на согласование!';
                    else
                        $arJson['error'][] = implode(', ',$result->getErrorMessages());

                    // добавление записи в журнал истории договоров
                    $result2 = $obHistoryIBlock::add(array(
                        'UF_CONTRACT' => $this->iContractId,
                        'UF_DATE'     => date('d.m.Y H:i:s'),
                        'UF_STATUS'   => $sStatus,
                        'UF_COMMENT'  => $sComment
                    ));
                    if (!$arJson['success'] = $result2->isSuccess())
                        $arJson['error'][] = implode(', ',$result2->getErrorMessages());

                    // оповещение правообладателя
                    if ($arJson['success']) {
                        self::sendOperatorEmail($this->iContractId, $this->iUserId,
                            self::getBooksWithCopyright($arContract['UF_BOOK']),
                            self::getFiles($arContract['UF_LEGAL_DOCUMENTS']),
                            $this->arEnum['UF_TYPE'][$arContract['UF_TYPE']]['VALUE'],
                            $this->arEnum['UF_STATUS'][$iStatus]['VALUE'],
                            $arContract['UF_COMMENT'] . ' / ' . $sComment
                        );
                    }
                }
            }
            // оператор НЭБ или главный админ
            elseif ($this->isOperator() || $this->isAdmin()) {

                // вернуть на доработку
                if ($sAction == 'draft' && $arContract['STATUS']['XML_ID'] == 'processed') {

                    $iStatus = $this->arStatus['draft'];
                    $sStatus = $this->arEnum['UF_STATUS'][$iStatus]['VALUE'];

                    // обновление договора
                    $result = $obContractsIBlock::update($this->iContractId, array('UF_STATUS' => $iStatus));
                    if ($arJson['success'] = $result->isSuccess()) {
                        $arJson['message'] = 'Договор успешно отправлен на доработку!';
                        $arJson['redirect'] = true;
                    } else
                        $arJson['error'][] = implode(', ',$result->getErrorMessages());

                    // добавление записи в журнал истории договоров
                    $result2 = $obHistoryIBlock::add(array(
                        'UF_CONTRACT' => $this->iContractId,
                        'UF_DATE'     => date('d.m.Y H:i:s'),
                        'UF_STATUS'   => $sStatus,
                        'UF_COMMENT'  => $sComment
                    ));
                    if (!$arJson['success'] = $result2->isSuccess())
                        $arJson['error'][] = implode(', ',$result2->getErrorMessages());

                    // оповещение правообладателя
                    if ($arJson['success'])
                        self::sendRightholderEmail($arContract, $sStatus, $sComment);
                }
                // подписать
                elseif ($sAction == 'signed' && $arContract['STATUS']['XML_ID'] == 'processed') {

                    $sNumber    = htmlspecialcharsBX($obRequest->getQuery("number"));
                    $sDateFrom  = htmlspecialcharsBX($obRequest->getQuery("dateFrom"));
                    $sDateTo    = htmlspecialcharsBX($obRequest->getQuery("dateTo"));

                    if (empty($sNumber))
                        $arJson['error'][] = 'Поле "Номер договора" должно быть заполнено!';

                    if (empty($sDateFrom))
                        $arJson['error'][] = 'Поле "Дата заключения договора" должно быть заполнено!';

                    if (empty($sDateTo))
                        $arJson['error'][] = 'Поле "Срок действия договора" должно быть заполнено!';

                    if (!empty($sDateFrom) && (!empty($sDateTo) && strtotime($sDateFrom) >= strtotime($sDateTo)))
                        $arJson['error'][] = 'Срок действия должен быть больше даты заключения договора!';

                    if (empty($arJson['error'])) {

                        $iStatus = $this->arStatus['signed'];
                        $sStatus = $this->arEnum['UF_STATUS'][$iStatus]['VALUE'];

                        $arUpdate = array(
                            'UF_NUMBER'    => $sNumber,
                            'UF_DATE_FROM' => $sDateFrom,
                            'UF_DATE_TO'   => $sDateTo,
                            'UF_STATUS'    => $iStatus,
                        );

                        // обновление договора
                        $result = $obContractsIBlock::update($this->iContractId, $arUpdate);
                        if ($arJson['success'] = $result->isSuccess())
                            $arJson['message'] = 'Договор успешно подписан!';
                        else
                            $arJson['error'][] = implode(', ',$result->getErrorMessages());

                        // добавление записи в журнал истории договоров
                        $result = $obHistoryIBlock::add(array(
                            'UF_CONTRACT' => $this->iContractId,
                            'UF_DATE'     => date('d.m.Y H:i:s'),
                            'UF_STATUS'   => $sStatus,
                            'UF_COMMENT'  => $sComment
                        ));
                        if (!$result->isSuccess())
                            $arJson['error'][] = implode(', ',$result->getErrorMessages());

                        // оповещение правообладателя
                        if ($arJson['success'])
                            self::sendRightholderEmail($arContract, $sStatus, $sComment);
                    }
                }
            }

            MainHelper::showJson($arJson);
        }
    }

    // ============================================= ПОЧТОВЫЕ СОБЫТИЯ ================================================

    /**
     * Получает массив e-mail адресов операторов НЭБ
     * @return array
     */
    public static function getOperatorsEmails() {

        $arResult = array();

        // получаение идентификатора группы "Оператор НЭБ"
        $rsUserGroups = GroupTable::getList(array(
            'filter' => array('ACTIVE' => 'Y', 'STRING_ID' => UGROUP_OPERATOR_CODE),
            'select' => array('ID')
        ));
        if ($arFields = $rsUserGroups->fetch()) {

            // выполучение всех пользователей из данной группы
            $rsUsers = UserTable::getList(array(
                'filter' => array("Bitrix\Main\UserGroupTable:USER.GROUP_ID" => $arFields['ID'], 'ACTIVE' => 'Y'),
                'select' => array('ID', 'EMAIL')
            ));
            while ($arFields = $rsUsers->fetch())
                $arResult[] = $arFields['EMAIL'];
        }

        return $arResult;
    }

    /**
     * Отправка оператору НЭБ электронного сообщения о новом договоре
     *
     * @param $iContract
     * @param $iUser
     * @param $arBooks
     * @param $arFiles
     * @param $sType
     * @param $sStatus
     * @param $sComment
     */
    private static function sendOperatorEmail($iContract, $iUser, $arBooks, $arFiles, $sType, $sStatus, $sComment) {

        $sFiles = $sBooks = '';

        // e-mail адреса получателей писем
        $arRecipients = self::getOperatorsEmails();

        if (empty($arRecipients))
            return;

        // информация о пользователе
        $arUser = self::getUserInfo($iUser);

        // правоустанавливающие документы
        foreach ($arFiles as $arItem)
            $sFiles .= "\n" . 'http://' . $_SERVER['HTTP_HOST'] . $arItem['PATH'] . ' (' . $arItem['FORMAT_SIZE'] . ')';

        // книги
        foreach ($arBooks as $sBook => $arBook)
            $sBooks .= "\n" . $sBook . ' - ' . trim($arBook['title']) . ' (' . trim($arBook['authorbook']) . ')';

        $arEventFields = array(
            "ID"      => $iContract,
            "EMAIL"   => implode(', ', $arRecipients),
            "USER"    => $arUser['FULL_NAME'],
            "DATE"    => (new DateTime)->toString(),
            "TYPE"    => $sType,
            "STATUS"  => $sStatus,
            "COMMENT" => $sComment,
            "LEGAL"   => $sFiles,
            "BOOK"    => $sBooks,
        );

        // отправка почтового сообщения
        Event::send(array(
            "EVENT_NAME" => "NEB_RIGHTHOLDER_CHANGE",
            "LID"        => SITE_ID,
            "C_FIELDS"   => $arEventFields
        ));
    }

    /**
     * Отправка правообладателю электронного сообщения о смене статуса договора
     *
     * @param $arContract
     * @param $sStatus
     * @param null $sComment
     */
    private static function sendRightholderEmail($arContract, $sStatus, $sComment = null) {

        // информация о пользователе
        $arUser = UserTable::getById($arContract['UF_USER_ID'])->fetch();

        $arEventFields = array(
            "NAME"        => trim($arUser['NAME'] . ' ' . $arUser['LAST_NAME']),
            "EMAIL"       => $arUser['EMAIL'],
            "ID"          => $arContract['ID'],
            "DATE_FROM"   => (new DateTime)->format($arContract['UF_DATE_FROM']),
            "STATUS"      => $sStatus,
            "INFORMATION" => !empty($sComment) ? 'Комментарий: ' . $sComment : ''
        );
        Event::send(array(
            "EVENT_NAME" => "NEB_RIGHTHOLDER_CHANGE",
            "LID"        => SITE_ID,
            "C_FIELDS"   => $arEventFields
        ));
    }

    /**
     * Отправка правообладателю электронного напоминания об окончании срока действия договора
     *
     * @param $arContracts
     * @param $arUsers
     * @param $arFiles
     * @param $arBooks
     * @return bool
     */
    public function sendRightholderNotify($arContracts, $arUsers, $arFiles, $arBooks) {

        foreach ($arContracts as $arContract) {

            // имя пользователя
            $sUserName = trim($arUsers[$arContract['UF_USER_ID']]['NAME'] . ' ' . $arUsers[$arContract['UF_USER_ID']]['LAST_NAME']);  // TODO: добавить название юрика

            // правоустанавливающие документы
            $sFiles = $sBooks = '';
            foreach ($arContract['UF_LEGAL_DOCUMENTS'] as $iDoc)
                if (!empty($arFiles[$iDoc]))
                    $sFiles .= "\n" . 'http://' . $_SERVER['HTTP_HOST'] . $arFiles[$iDoc]['PATH'] . ' (' . $arFiles[$iDoc]['FORMAT_SIZE'] . ')';

            // книги
            foreach ($arBooks as $sBook => $arBook)
                $sBooks .= "\n" . $sBook . ' - ' . trim($arBook['title']) . ' (' . trim($arBook['authorbook']) . ')';

            $arEventFields = array(
                "NAME"      => $sUserName,
                "EMAIL"     => $arUsers[$arContract['UF_USER_ID']]['EMAIL'],
                "ID"        => $arContract['ID'],
                "NUMBER"    => $arContract['UF_NUMBER'],
                "DATE_FROM" => (new DateTime())->format($arContract['UF_DATE_FROM']),
                "DATE_TO"   => (new DateTime())->format($arContract['UF_DATE_TO']),
                "TYPE"      => $this->arEnum['UF_TYPE'][$arContract['UF_TYPE']]['VALUE'],
                "STATUS"    => $this->arEnum['UF_STATUS'][$arContract['UF_STATUS']]['VALUE'],
                "DOCUMENT"  => $sFiles,
                "BOOK"      => $sBooks,
                "COMMENT"   => $arContract['UF_COMMENT']
            );
            Event::send(array(
                "EVENT_NAME" => "NEB_RIGHTHOLDER_CHANGE",
                "LID"        => SITE_ID,
                "C_FIELDS"   => $arEventFields
            ));
        }

        return true;
    }


    // ============================================= СЛУЖЕБНЫЕ МЕТОДЫ ================================================

    /**
     * Получает первый элемент массива
     * @param $arItems
     * @return mixed
     */
    public static function getFirst($arItems) {
        reset($arItems);
        return current($arItems);
    }

    /**
     * Проверка пользователя на принадлежность группе "Правообладатель"
     * @return bool
     */
    private function isRightholder() {
        return in_array(UGROUP_RIGHTHOLDER_CODE, $this->arUserGroups);
    }

    /**
     * Проверка пользователя на принадлежность группе "Оператор НЭБ"
     * @return bool
     */
    private function isOperator() {
        return in_array(UGROUP_OPERATOR_CODE, $this->arUserGroups);
    }

    /**
     * Проверка пользователя на принадлежность группе "Администратор"
     * @return bool
     */
    private function isAdmin() {
        return in_array(UGROUP_ADMIN, $this->arUserGroups);
    }

    /**
     * Преобразовывает массив $_FILES в пригодный вид
     * 
     * @param $arFiles
     * @return array
     */
    private static function getFilesArray($arFiles) {

        $arResult = array();

        foreach ($arFiles as $sFilesName => $arProp) {
            $arFile = array();
            if (is_array($arProp['size'])) {
                foreach ($arProp['size'] as $k => $v)
                    if ($arProp['size'][$k] > 0)
                        $arFile[] = $k;
            }

            foreach ($arProp as $sPropName => $arPropVal)
                if (is_array($arPropVal)) {
                    foreach ($arFile as $v)
                        $arResult[$sFilesName][$v][$sPropName] = $arPropVal[$v];
                    if (!empty($arResult[$sFilesName][$v]))
                        $arResult[$sFilesName][$v]['MODULE_ID'] = 'highloadblock';
                } else {
                    $arResult[$sFilesName][$sPropName] = $arPropVal;
                    if (!empty($arResult[$sFilesName][$sPropName]))
                        $arResult[$sFilesName]['MODULE_ID'] = 'highloadblock';
                }
        }

        return $arResult;
    }

    /** ===================================== WARNING! LEGACY! OLD METHODS! ========================================= */

    /**
     *Получаем ID группы правообладателя
     */
    public static function getUserGroupRoleID() {

        $arRihthoderGroup = GroupTable::getList(array(
            'filter' => array(
                "STRING_ID" => UGROUP_RIGHTHOLDER_CODE,
                'ACTIVE'    => 'Y',
            ),
            'select' => array('ID', 'STRING_ID')
        ))->fetchRaw();

        if ($arRihthoderGroup['ID'])
            return $arRihthoderGroup['ID'];

        return false;
    }

    /**
     * Добавляет пользователя в группу
     */
    public static function setUserGroup($UID) {

        if (intval($UID) <= 0)
            return false;

        $arGroups = CUser::GetUserGroup($UID);
        $idGroup = self::getUserGroupRoleID();

        if (!in_array($idGroup, $arGroups)) {
            $arGroups[] = $idGroup;
            CUser::SetUserGroup($UID, $arGroups);
        }

        return true;
    }
}