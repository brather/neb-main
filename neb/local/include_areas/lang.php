<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="b-header_lang right">
	<?
	$sLangUrl = $_SERVER['REQUEST_URI'];
	if (strpos($sLangUrl, '?') === false)
		$sLangUrl = $sLangUrl . '?';
	elseif (strpos($sLangUrl, 'setlang=') !== false)
		$sLangUrl = str_replace(['&setlang=' . LANGUAGE_ID, 'setlang=' . LANGUAGE_ID,], '', $sLangUrl);
	$sParams = end(explode('?', $sLangUrl));
	if (!empty($sParams))
		$sLangUrl = $sLangUrl . '&';
    ?>
    <noindex>
        <?if('ru' == NebLang::get()):
            ?><a href="<?=$sLangUrl?>setlang=en" class="en iblock" title="Английская версия" rel="nofollow">Eng</a><?
        else:
            ?><a href="<?=$sLangUrl?>setlang=ru" class="en iblock" title="Russian version" rel="nofollow">Рус</a><?
        endif; ?>
    </noindex>
	<?php if(stripos($GLOBALS["APPLICATION"]->GetCurDir(),"/special/") !== 0):?>
		<a href="/special/" class="ru iblock" title="Версия сайта для слабовидящих">Aa</a>
	<?php endif;?>
</div>