$(function(){
    var cls = {
        arrow   : '.slick-arrow',
        prev    : 'clsp__editions-list-prev',
        next    : 'clsp__editions-list-next',
        contain : '.clsp__details',
        list    : '.clsp__editions-list',
        item    : '.clsp__editions-list-item',
        slide   : '.slick-slide',
        cloned  : '.slick-cloned',
        active  : 'slick-active'
    };

    $(cls.item).find('a > img').tooltip();

    $(cls.list).on('afterChange', function(slick, currentSlide){
        var $this = $(this),
            contain = $this.closest('li'),
            send = {
                maxSlickIndex   : currentSlide.slideCount - 1,
                countAll        : $this.data('count-all'),
                countInSlide    : $this.data('count-in-slide'),
                sectionId       : $this.data('section-id'),
                iblockId        : $this.data('iblock-id'),
                items           : []
            },
            indexItem = 0;

        $this.find(cls.item).not(cls.cloned).each(function(i,y){
            send.items[indexItem] = $(y).data('book-id');
            indexItem++;
        });
        if(send.countAll > currentSlide.slideCount) {
            startLoader(contain);
            $.ajax({
                type : 'post',
                data : send,
                url : '/local/tools/collections/loadBooks.php',
                success : function(data){
                    $this.slick('slickAdd', data);
                    stopLoader();
                }
            });
        }
    });
});
//region Функция запуска лоадера
function startLoader(block) {
    var blkLoader = '<div id="blk-loader" class="blk-milk-shadow"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
    $(block).append(blkLoader);
}
//endregion
//region Функция остановки лоадера
function stopLoader() {
    $('#blk-loader').animate({ 'opacity' : 0 }, 300, function(){
        $(this).remove();
    });
}
//endregion