<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
// $APPLICATION->IncludeComponent(
//     'neb:element.text',
//     '',
//     [
//         'XML_ID'    => 'how-to-search',
//         'SET_TITLE' => 'Y',
//     ],
//     false
// );
?>

<style>
@media screen and (max-width: 992px) {
	.hts-row .col-md-9 {}
	.hts-row .col-md-3 {display: none;}
}
</style>
<div class="row hts-row">
	<div class="col-md-9">
		<div id="q1">
			<h2>КАК ИСКАТЬ</h2>
			<p>Национальная электронная библиотека объединяет фонды публичных библиотек России федерального, регионального и муниципального уровней, библиотек научных и образовательных учреждений, музеев, архивов.</p>
			<p>На Портале НЭБ доступны следующие режимы поиска изданий:</p>
			<ul>
				<li><a href="#simplesearch">Простой поиск;</a></li>
				<li><a href="#titleauthorsearch">Поиск изданий по автору и/или названию;</a></li>
				<li><a href="#extsearch">Расширенный поиск.</a></li>
			</ul>
			<p>При заполнении критериев поиска используется автоматическое определение раскладки клавиатуры. При вводе значения Портал НЭБ определяет верную раскладку для вводимых слов.</p>
			<p>Для введения значения в поисковые поля можно использовать виртуальную клавиатуру, значок которой появляется при наведении курсора мыши на поисковое поле, планируемое к заполнению.</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/virtkeyboard.png"/></p></div>


			<ul class="info-tabs nav-tabs" data-info-tabs="" role="tablist">
				<li class="active"><a href="#simplesearch" role="tab" data-toggle="tab">Простой поиск</a></li>
				<li><a href="#titleauthorsearch" role="tab" data-toggle="tab">Поиск изданий по автору и/или названию</a></li>
				<li><a href="#extsearch" role="tab" data-toggle="tab">Расширенный поиск</a></li>
			</ul>

			<div class="info-tab-content tab-content contrast" data-tabs-content>
				<div class="tab-pane active" id="simplesearch" role="tabpanel">
					<h3 id="q1_1">Простой поиск</h3>
					<p>На странице Портала НЭБ предусмотрена область для работы в режиме простого поиска.</p>
					<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/simpleform.png"/></p></div>

					<p>Для выполнения простого поиска изданий выполните следующие шаги:</p>
					<h4>Шаг 1</h4>
					<p>По умолчанию поиск осуществляется по всем коллекциям Портала НЭБ, кроме коллекции «Авторефераты и диссертации».</p>
					<p>Для поиска изданий в конкретных коллекциях выберите их в области «Искать по коллекциям».</p>
					<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/simpleformcollections1.png"/></p></div>

					<p>Для выбора дополнительных коллекций, в которых будет выполняться поиск изданий, нажмите кнопку «Еще» и из открывшегося списка выберите нужные.</p>
					<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/simpleformcollections2.png"/></p></div>

					<h4>Шаг 2</h4>
					<p>В поисковой строке введите искомое значение.</p>
					<p>При вводе значения в поисковую строку отображается список значений, в которых присутствуют введенные символы.</p>
					<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/simpleformautocomplete.png"/></p></div>

					<p>Нужное значение можно выбрать из отображаемого списка или ввести его с помощью клавиатуры.</p>
					 
					<h4>Шаг 3</h4>
					<p>Нажмите кнопку <!--&lquo;-->«Найти»<!--&raquo-->; или клавишу «Enter» на клавиатуре.</p>
					Поиск производится в три этапа:</p>
					<ul>
						<li>Первый этап поиска выполняется только по библиографическому описанию издания; </li>
						<li>Второй этап поиска выполняется по совпадению поисковой фразы в текстах изданий (слова поискового запроса в текстах изданий находятся рядом); </li>
						<li>Результаты третьего этапа включают в себя издания, в которых слова поискового запроса встречаются в тексте раздельно друг от друга. </li>
					</ul>
					<p>Каждый последующий этап включает в себя результаты предыдущего.</p>
				</div>
				<div class="tab-pane" id="titleauthorsearch" role="tabpanel">
					<h3 id="q1_2">Поиск изданий по автору и/или названию</h3>
					<p>На странице Портала НЭБ предусмотрена область для поиска изданий по автору и/или названию.</p>
					<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/authortitleresultform.png"/></p></div>
					 
					<p>Для выполнения поиска изданий по автору и/или названию выполните следующие шаги:</p>
					<h4>Шаг 1</h4>
					<p>Введите искомое значение в поле «Автор» и/или «Название».</p>
					<p>При вводе значения в любом из полей отображается список значений, в которых присутствуют вводимые символы.</p>
					<p>Нужное значение можно выбрать из отображаемого списка или ввести его с помощью клавиатуры.</p>
					<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/authortitleautocomplete.png"/></p></div>
					 
					<h4>Шаг 2</h4>
					<p>Нажмите кнопку   или клавишу «Enter» на клавиатуре.</p>
					<p>Незаполненное поле при поиске не используется.</p>
				</div>
				<div class="tab-pane" id="extsearch" role="tabpanel">
					<h3 id="q1_3">Расширенный поиск</h3>
					<p>Для более точного поиска изданий предусмотрена возможность расширенного поиска.</p>
					<p>Переход в режим расширенного поиска возможен по ссылке «Расширенный поиск»:<//p>

					<ul>
						<li>из области поиска изданий по автору и/или названию главной страницы Портала НЭБ,
					<div class="illustr">		<p><img src="/local/templates/adaptive/img/howtosearch/simpleformextendlink.png"/></p></div>
						</li>
						<li>со страницы результатов 
							<p>простого поиска.</p>
					<div class="illustr">		<p><img src="/local/templates/adaptive/img/howtosearch/simpleresultinfo3.png"/></p></div>
							<p>или поиска изданий по автору и/или названию</p>
					<div class="illustr">		<p><img src="/local/templates/adaptive/img/howtosearch/authortitleresultinfo3.png"/></p></div>
						</li> 
					</ul>

					<p>Для выполнения расширенного поиска изданий выполните следующие шаги:</p>

					<h4>Шаг 1</h4>
					<p>Выберите раздел для поиска. По умолчанию выбран раздел «Библиотеки».</p>
					<p>Поиск будет осуществляться только в выбранном разделе.</p>
					<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/librarytype.png"/></p></div>
					 
					<p>Вид страницы расширенного поиска зависит от выбранного раздела (ниже показаны примеры заполнения поисковых полей разных видов страниц):</p>

					<ul>
						<li>Библиотеки;
					<div class="illustr">		<p><img src="/local/templates/adaptive/img/howtosearch/extbiblio.png"/></p></div>
						</li>
					 
						<li>Музеи;
					<div class="illustr">		<p><img src="/local/templates/adaptive/img/howtosearch/extmuseum.png"/></p></div>
						</li>
					 
						<li>Архивы.
					<div class="illustr">		<p><img src="/local/templates/adaptive/img/howtosearch/extarch.png"/></p></div>
						</li>
					</ul>
					 
					<h4>Шаг 2</h4>
					<p>При необходимости задайте интервал даты публикации издания. Для установки интервала воспользуйтесь ползунком времени или введите необходимый интервал лет по бокам ползунка.</p>
					<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/typerange.png"/></p></div>

					<h4>Шаг 3</h4>
					<p>Заполните остальные поля страницы расширенного поиска, значения которых будут учитываться в процессе поиска.</p>
					<p>Незаполненные поля при поиске не используются.</p>
					<p>Для удаления всех заданных значений поисковых полей используйте кнопку  .</p>
					<h4>Шаг 4</h4>
					<p>Нажмите кнопку   или клавишу «Enter» на клавиатуре.</p>
				</div>
			</div>
		</div>





		<div id="q2">
			<h2>РАБОТА С РЕЗУЛЬТАТАМИ ПОИСКА</h2>
			<p>По окончании поиска на экране отобразится страница с результатами поиска.</p>
			<p>В верхней части страницы с результатами поиска будут показаны условия поиска и коллекции, в которых выполнялся:</p>

			<ul>
				<li>простой поиск;</li>
				 
				<li>поиск по автору и/или названию;</li>
				 
				<li>расширенный поиск;</li>
			</ul>
			 
			<p>Для поиска изданий во всех коллекциях Портала НЭБ по заданным в поисковой строке критериям нажмите кнопку  <img src="/local/templates/adaptive/img/howtosearch/clearfilter.png"/>.</p>
			<p>Для удаления конкретного уточняющего фильтра воспользуйтесь кнопкой-крестиком, расположенной справа от планируемого к удалению фильтра.</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/cancelsinglefilter.png"/></p></div>
			 
			<p>В средней части страницы результатов поиска в табличном виде будет показан список найденных изданий, которые удовлетворяют заданным критериям поиска, с указанием количества найденных изданий.</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/found.png"/></p></div>
			<p>В зависимости заданных критериев поиска в списке результатов могут присутствовать несколько типов записей (для каждой записи предусмотрено уникальное цветовое выделение и графическое изображение):</p>

			<ul>
				<li><img src="/local/templates/adaptive/img/howtosearch/type1.png"/> – открытое издание;</li>
				<li><img src="/local/templates/adaptive/img/howtosearch/type2.png"/> – издание, охраняемое авторским правом;</li>
				<li><img src="/local/templates/adaptive/img/howtosearch/type3.png"/> – запись каталога;</li>
				<li><img src="/local/templates/adaptive/img/howtosearch/type4.png"/> – запись каталога ЭКБСОН.</li>
			</ul>

			<p>На Портале НЭБ можно выполнить следующие действия при работе с результатами поиска:</p>

			<ul>
				<li>Отображение большего количества результатов поиска.</li>
				<li>Поиск точных совпадений.</li>
				<li>Исправление заданного запроса.</li>
				<li>Уточнение результатов поиска.</li>
				<li>Сортировка результатов поиска.</li>
				<li>Просмотр библиографической карточки издания.</li>
				<li>Просмотр текста издания.</li>
			</ul>

			<h3 id="q2_1">Отображение большего количества результатов поиска</h3>
			<p>Для отображения большего количества результатов поиска по текстам изданий нажмите на ссылку «показать еще».</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/more.png"/></p></div>
			 
			<h3 id="q2_2">Поиск точных совпадений</h3>
			<p>Для поиска точных совпадений в текстах изданий нажмите на ссылку «по точному совпадению в текстах изданий».</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/match.png"/></p></div>
			 
			<h3 id="q2_3">Исправление заданного запроса</h3>
			<p>При выполнении поиска анализируется корректность заданного поискового запроса, и на странице результатов поиска в поле «Возможно Вы имели в виду» предлагается вариант его замены. Для исправления запроса и отображения результатов поиска по исправленному запросу нажмите на предлагаемом значении. </p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/typo.png"/></p></div>
			 
			<h3 id="q2_4">Уточнение результатов поиска</h3>
			<p>На Портале НЭБ имеется возможность уточнения параметров поиска для сокращения слишком большого списка найденных по запросу изданий.</p>
			<p>В правой части страницы результатов поиска расположена панель с параметрами, по которым можно уточнить результаты поиска:</p>
				
			<ul>
				<li>автор;</li>
				<li>дата;</li>
				<li>издательство;</li>
				<li>библиотека;</li>
				<li>место издания;</li>
				<li>термины;</li>
				<li>коллекции.</li>
			</ul>

			<p>Для уточнения результатов поиска выполните следующие шаги:</p>
			<h4>Шаг 1</h4>
			<p>Выберите кретерий, по которому хотите уточнить поиск.</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/filtergrouptitle.png"/></p></div>
			 
			<h4>Шаг 2</h4>
			<p>В открывшемся списке отобразятся все значения, по которым можно отфильтровать результаты поиска, и количество записей, в которых встречается данное значение. Значения в списке отсортированы по количеству вхождений в результаты поиска, в случае одинакового количества вхождений – по алфавиту.</p>
			<p>Из открывшегося списка выберите нужное значение.</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/filtertitle.png"/></p></div>
			<p>Список результатов поиска будет обновлен. В параметрах поиска появится уточнение.</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/filterdisplay.png"/></p></div>
			<p>Одновременно можно выполнить уточнение сразу по нескольким параметрам, например, по автору и месту издания.</p>

			<h3 id="q2_5">Сортировка результатов поиска</h3>
			<p>На Портале НЭБ предусмотрена возможность сортировки результатов поиска:</p>
				
			<ul>
				<li>по релевантности;</li>
				<li>по автору;</li>
				<li>по названию;</li>
				<li>по дате.</li>
			</ul>

			<p>Ссылки, при нажатии на которые выполняется сортировка, расположены в заголовке таблицы результатов поиска. По умолчанию результаты поиска сортируются по релевантности.</p>
			<p>Чтобы выполнить сортировку по одному из параметров, необходимо нажать на соответствующей ссылке левой кнопкой мыши. </p>
			<p>Результаты поиска будут отсортированы по выбранному параметру. Справа от наименования параметра отобразится стрелка, обозначающая порядок сортировки (  – прямой порядок сортировки,   – обратный порядок сортировки).</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/sort.png"/></p></div>

			<h3 id="q2_6">Просмотр библиографической карточки издания</h3>
			<p>Для просмотра библиографической карточки издания в списке результатов поиска нажмите левой кнопкой мыши на названии издания.</p>
			 
			<p>Отобразится страница просмотра библиографической карточки выбранного издания, в которой можно выполнить следующие действия (нажав левой кнопкой мыши на соответствующей ссылке):</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/clitcktitle.png"/></p></div>

			<ul>
				<li>уточнить результаты поиска изданий по автору, наименованию издательства или году издания;</li>
				<li>просмотреть информацию о библиотеке, в фондах которой хранится бумажная копия данного издания;</li>
				<li>просмотреть информацию о произведении, в состав которого входит просматриваемое издание (при наличии такового);</li>
				<li>просмотреть на карте местоположение ближайшей библиотеки, в фондах которой имеется бумажная версия данного издания.</li>
			</ul>

			<p>С помощью кнопки «Читать» можно вызвать к просмотру текст электронной версии издания.</p>
			<div class="illustr"><p><img src="/local/templates/adaptive/img/howtosearch/editioncard.png"/></p></div>
			 
			<h3 id="q2_7">Просмотр текста издания</h3>
			<p>Для изданий, представленных на Портале НЭБ в виде электронных копий, предусмотрена возможность чтения текста изданий. </p>
			<p>Для просмотра текста издания в списке результатов поиска или в библиографической карточке выбранного издания нажмите кнопку &laquo;Читать&raquo; или кнопку &laquo;Смотреть&raquo;, если поиск выполнялся в разделе «Музеи».</p>
		</div>
	</div>
	<div class="col-md-3 relative">
		<div id="helpnav">
			<ul class="nav">
				<li>
					<a href="#q1">КАК ИСКАТЬ</a>
					<ul class="nav">
						<li><a href="#simplesearch">Простой поиск</a></li>
						<li><a href="#titleauthorsearch">Поиск изданий по автору и/или названию</a></li>
						<li><a href="#extsearch">Расширенный поиск</a></li>
					</ul>
				</li>
				<li>
					<a href="#q2">РАБОТА С РЕЗУЛЬТАТАМИ ПОИСКА</a>
					<ul class="nav">
						<li><a href="#q2_1">Отображение большего количества результатов поиска</a></li>
						<li><a href="#q2_2">Поиск точных совпадений</a></li>
						<li><a href="#q2_3">Исправление заданного запроса</a></li>
						<li><a href="#q2_4">Уточнение результатов поиска</a></li>
						<li><a href="#q2_5">Сортировка результатов поиска</a></li>
						<li><a href="#q2_6">Просмотр библиографической карточки издания</a></li>
						<li><a href="#q2_7">Просмотр текста издания</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>

<script>
	$(function(){
		var script = $('<script>');
		script.attr('src','/local/templates/adaptive/js/jquery/jquery.sticky.js');
		$('body').append( script );
	});
	$(function(){
        var hash = window.location.hash,
            linkhash = hash.substr(1),
            currentPageHash = linkhash,
            autoclick = false,
            historyAPI = !!(window.history && history.pushState);

        $(document).on('click','[data-info-tabs] a, [href="#simplesearch"], [href="#titleauthorsearch"], [href="#extsearch"]',function(e){
        	if (!autoclick) {
	            var link = $(this),
	                target;
	            if ($(e.target).attr("href")) {
	                linkhash = $(e.target).attr("href").substr(1);
	            }
	            if(historyAPI) {
	                history.pushState({
	                	prechangedPage:currentPageHash,
						actualPage:linkhash
	                }, null, '#'+linkhash);
	            }
	            link.parent().siblings().toggleClass('active',false);
	            link.parent().toggleClass('active',true);
	            target = $('[data-tabs-content]').find('.tab-pane#'+linkhash);
	            // if (autoclick &&  $(target).offset() ) {
	            // 	$("html, body").delay(300).animate({scrollTop: $(target).offset().top }, 500);
	            // }
	            target.siblings().toggleClass('active',false);
	            target.toggleClass('active',true);
        	}
            autoclick = false;        		
        });

        window.onpopstate = function (e) {
        	if (e.state) {
            	autoclick = true;
        	}
            var hash = window.location.hash,
                linkhash = hash.substr(1);
            $('[data-info-tabs] a[href="'+hash+'"]').click();
        };

        if (hash) {
            autoclick = true;
            $('[data-info-tabs] a[href="'+hash+'"]').click();
        }
        
    	$('body').css('position','relative');
    	$('body').scrollspy({ target: '#helpnav' });
    	$('#helpnav').sticky({
    		topSpacing: 60,
    		bottomSpacing: $('.container.main-footer').height() + 100
    	});


    	

    });
</script>




<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>