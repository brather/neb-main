<?
$MESS ['AUTH_NEW_PASSWORD'] = "Новый пароль:";
$MESS ['AUTH_NEW_PASSWORD'] = "Новый пароль";
$MESS ['AUTH_NEW_PASSWORD_CONFIRM'] = "Подтверждение пароля";
$MESS ['RECOVERY'] = "Изменение пароля";
$MESS ['DEFENSE'] = "Защищенность пароля";
$MESS ['REQUIRED'] = "Поле обязательно для заполнения";
$MESS ['SEND'] = "Отправить";
$MESS ['SIX'] = "не менее 6 символов";
$MESS ['NATIONAL_ELECTRONIC_LIBRARY'] = "Национальная электронная библиотека";
$MESS ['MIN_LEN'] = "Должно быть {0} и более символов";
$MESS ['MAX_LEN'] = "Должно быть {0} или менее символов";
?>