<?php
$MESS = array(
    'AUTHOR_TITLE'       => 'Автор',
    'IS_NEW'             => 'Новая',
    'ADD_BOOK'           => 'Добавить библиотеку',
    'PUBLISH_YEAR_TITLE' => 'Год публикации',
    'ISBN_TITLE'         => 'ISBN',
    'PDF_TYTLE'          => 'PDF',
    'VIEWS'              => 'Просмотров'
);