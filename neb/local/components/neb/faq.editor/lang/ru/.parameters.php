<?php
$MESS["EDITOR_MODE"] = "Режим редактора";
$MESS["EDITOR_SIMPLIFIED"] = "Упрощенные редактор";
$MESS["EDITOR_FULL"] = "Полной редактор";
$MESS["EDITOR_ID"] = "ID редатора (уникальный)";
$MESS["EDITOR_CONTENT_DEFAULT"] = "Контент который нужно вставить в редактор";
$MESS["EDITOR_TEXTAREA_NAME"] = "Имя поля TEXTAREA";
$MESS["EDITOR_TEXTAREA_ID"] = "ID поля TEXTAREA";
$MESS["EDITOR_WIDTH"] = "Ширина редактора";
$MESS["EDITOR_HEIGHT"] = "Высота редактора";