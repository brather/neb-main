<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150924165458 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery(<<<SQL
ALTER TABLE tbl_common_biblio_card
ADD COLUMN request_type VARCHAR(20) DEFAULT NULL;
CREATE INDEX request_type ON tbl_common_biblio_card (request_type);
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->executeQuery(<<<SQL
DROP INDEX request_type ON tbl_common_biblio_card;
ALTER TABLE tbl_common_biblio_card
DROP COLUMN request_type;
SQL
        );
    }
}
