<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 13:00
 */

namespace LbcBundle\Controller\Api;

use Doctrine\Bundle\DoctrineBundle\Registry;
use LbcBundle\Controller\Traits\DoctrineInjectTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class LbcMaindivisionimagesController
 *
 * @Route("/api/bbk")
 * @DI\Service("app.api.maindivision.images", scope="request")
 */
class LbcMaindivisionimagesController extends AbstractImagesController
{
    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcMaindivisionimages';
    }

    /**
     * @return string
     */
    public function getOwnFieldName()
    {
        return 'maindivisionid';
    }
}