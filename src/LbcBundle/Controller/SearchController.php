<?php
/**
 * User: agolodkov
 * Date: 01.09.2016
 * Time: 15:11
 */

namespace LbcBundle\Controller;


use Doctrine\Bundle\DoctrineBundle\Registry;
use Gedmo\Tree\Entity\Repository\ClosureTreeRepository;
use Knp\Component\Pager\Paginator;
use LbcBundle\DependencyInjection\ExaleadSearchHelper;
use LbcBundle\DependencyInjection\LbcCategory;
use LbcBundle\Entity\AbstractLbcCategory;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use BitrixBundle\Annotations\Template as BitrixTemplate;
use BitrixBundle\Annotations\IncludeBitrix;

class SearchController
{
    /**
     * @DI\Inject("request")
     * @var Request
     */
    public $request;

    /**
     * @DI\Inject("doctrine")
     * @var Registry
     */
    public $doctrine;

    /**
     * @DI\Inject("lbc.exalead_helper")
     * @var ExaleadSearchHelper
     */
    public $exalead;

    /**
     * @DI\Inject("knp_paginator")
     * @var Paginator
     */
    public $paginator;

    /**
     * @DI\Inject("lbc.category")
     * @var LbcCategory
     */
    public $categoryHelper;

    /**
     *
     * @Method("GET")
     * @BitrixTemplate("LbcBundle:category:category.html.php")
     * @IncludeBitrix()
     *
     * @return array
     */
    public function searchAction()
    {
        $collection = $this->exalead->search()->getCollection();
        $entities = $collection->getValues();
        $entities = array_filter($entities);
        $entities = array_values($entities);
        $categorySort = ['m' => 10, 'l' => 20, 's' => 30];
        $that = $this;
        usort(
            $entities,
            function ($a, $b) use ($categorySort, $that) {
                $a = $that->categoryHelper->getEntityCategoryName($a);
                $a = substr($a, 0, 1);
                $a = isset($categorySort[$a]) ? $categorySort[$a] : 0;
                $b = $that->categoryHelper->getEntityCategoryName($b);
                $b = substr($b, 0, 1);
                $b = isset($categorySort[$b]) ? $categorySort[$b] : 0;

                return ($a === $b ? 0 : ($a > $b ? 1 : -1));
            }
        );

        $categoryGroups = [
            'm' => 'Основные деления',
            'l' => 'Планы расположения',
            's' => 'Специальные деления',
        ];
        $lastType = null;
        foreach ($entities as $entity) {
            $type = $this->categoryHelper->getEntityCategoryName($entity);
            $type = substr($type, 0, 1);
            if ($type !== $lastType && isset($categoryGroups[$type])) {
                $entity->categoryGroup = $categoryGroups[$type];
            }
            $lastType = $type;
        }

        /** @var AbstractLbcCategory $entity */
        foreach ($entities as $entity) {
            /** @var ClosureTreeRepository $repository */
            $repository = $this->doctrine->getManager()->getRepository(get_class($entity));
            $entity->setChildCount($repository->childCount($entity));
            if ($entity->getLevel() > 1) {
                $path = $repository->getPath($entity);
                foreach ($path as $pathEntity) {
                    $this->categoryHelper->prepareEntity($pathEntity);
                }
                $entity->setPath($path);
            }
            $this->categoryHelper->prepareEntity($entity);
        }


        $this->paginator->setDefaultPaginatorOptions(
            [
                'pageParameterName' => $this->exalead->getPageParameterName(),
            ]
        );
        if (!$page = $this->request->query->get('page')) {
            $page = 1;
        }
        $paginaton = $this->paginator->paginate(
            $collection,
            $page,
            $this->exalead->getPageSize()
        );

        return [
            'entities'   => $entities,
            'pagination' => $paginaton
        ];
    }
}