<?
define('STOP_STATISTICS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;

$error   = true;
$result  = $arJson = [];

$login    = trim($_POST['login']);
$password = trim($_POST['password']);

if ($_SERVER['REQUEST_METHOD'] == 'POST' and !empty($password) and !empty($login) and check_bitrix_sessid()) {

    // Делаем по просьбе клиента проверку по мылу и паролю
    $arUser = array();

    $arFilter = array(
        '=EMAIL' => $login,
        'ACTIVE' => 'Y'
    );

    $arUser = CUser::GetList(
        ($by = "id"),
        ($order = "desc"),
        $arFilter,
        array(
            'FIELDS' => array('ID', 'PASSWORD', 'NAME', 'LAST_NAME', 'EMAIL', 'DATE_REGISTER', 'SECOND_NAME'),
            'SELECT' => array('UF_SCAN_PASSPORT1', 'UF_RGB_USER_ID', 'UF_REGISTER_TYPE', 'UF_STATUS'))
    )->Fetch();

    if (empty($arUser)) {

        $arFilter = array(
            'LOGIN_EQUAL' => $login,
            'ACTIVE' => 'Y'
        );

        $arUser = CUser::GetList(
            ($by = "id"),
            ($order = "desc"),
            $arFilter,
            array(
                'FIELDS' => array('ID', 'PASSWORD', 'NAME', 'LAST_NAME', 'EMAIL', 'DATE_REGISTER', 'SECOND_NAME'),
                'SELECT' => array('UF_SCAN_PASSPORT1', 'UF_RGB_USER_ID', 'UF_REGISTER_TYPE', 'UF_STATUS'))
        )->Fetch();
    }

    if (!empty($arUser)) {

        // Проверяем валидность пароля
        if (strlen($arUser["PASSWORD"]) > 32) {
            $salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
            $db_password = substr($arUser["PASSWORD"], -32);
        } else {
            $salt = "";
            $db_password = $arUser["PASSWORD"];
        }

        $user_password = md5($salt . $password);
        if ($db_password === $user_password) {
            $USER->Authorize($arUser['ID']);
            /* Если статус не верифицирован, не пускаем пользователя */
            if (nebUser::USER_STATUS_VERIFIED
                === (integer)$arUser['UF_STATUS']
                ||
                nebUser::USER_STATUS_VIP === (integer)$arUser['UF_STATUS']
            ) {
                if (!empty($_SERVER['HTTP_REFERER']))
                    $redirect = $_SERVER['HTTP_REFERER'];

                $error = false;
            } //пользователь на упрощенной регистрации?
            elseif (nebUser::REGISTER_TYPE_SIMPLE
                === (integer)$arUser['UF_REGISTER_TYPE']
            ) {
                $USER->Authorize($arUser['ID']);
                $redirect = '/profile/update_select/';
                $result['code'] = 'access_deny';
                $error = true;
            } else {
                $result['code'] = 'access_deny';
                $error = true;
            }
        } else {
            $error = true;
        }
    }
}

$arJson['error'] = $error;
$arJson['redirect'] = isset($redirect) ? $redirect : false;
$arJson['result'] = $result;

\Neb\Main\Helper\MainHelper::showJson($arJson);
