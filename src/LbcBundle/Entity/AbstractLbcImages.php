<?php
/**
 * User: agolodkov
 * Date: 17.08.2016
 * Time: 17:23
 */

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;

abstract class AbstractLbcImages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ImagePath", type="text", nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    protected $imagepath;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imagepath
     *
     * @param string $imagepath
     *
     * @return $this
     */
    public function setImagepath($imagepath)
    {
        $this->imagepath = $imagepath;

        return $this;
    }

    /**
     * Get imagepath
     *
     * @return string
     */
    public function getImagepath()
    {
        return $this->imagepath;
    }
}