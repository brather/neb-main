<?
$MESS["intervolga.remoteuserclient.NO_ACCESS"] = "У вас нет доступа к этой странице";
$MESS["intervolga.remoteuserclient.PAGE_TITLE"] = "Выгрузить поля пользователей на удаленный сервер";
$MESS["intervolga.remoteuserclient.NO_UF"] = "На сайте не созданы пользовательские поля пользователей";
$MESS["intervolga.remoteuserclient.USER_USER_FIELDS_UPLOADED"] = "Поле \"#THIS_SITE_USER_USER_FIELDS#\" выгружено на удаленный сайт как [#REMOTE_SITE_USER_USER_FIELDS#]";
$MESS["intervolga.remoteuserclient.USER_USER_FIELDS_NOT_UPLOADED"] = "Поле \"#THIS_SITE_USER_USER_FIELDS#\" не выгружено на удаленный сайт";
$MESS["intervolga.remoteuserclient.USER_USER_FIELDS_UPLOADED_TOTAL"] = "Всего полей пользователей выгружено: #TOTAL#";
$MESS["intervolga.remoteuserclient.USER_USER_FIELDS_NOT_UPLOADED_TOTAL"] = "Всего полей пользователей не выгружено: #TOTAL#";
$MESS["intervolga.remoteuserclient.ALL"] = "Все";
$MESS["intervolga.remoteuserclient.NONE"] = "Ни одно";
$MESS["intervolga.remoteuserclient.NONE_UPLOADED"] = "Ни одно поле не было выгружено";
$MESS["intervolga.remoteuserclient.AGAIN"] = "Новая выгрузка полей";
$MESS["intervolga.remoteuserclient.UPLOAD_USERS_NOW"] = "Перейти к выгрузке пользователей";