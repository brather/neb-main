<?php
$MESS['WORKPLACES_TYPES_XML_ID'] = 'Символьный код на англ.языке в нижнем регистре';
$MESS['WORKPLACES_TYPES_VALUE']  = 'Название';
$MESS['WORKPLACES_TYPES_SORT']   = 'Сортировка';
$MESS['WORKPLACES_TYPES_SAVE']   = 'Сохранить';
$MESS['WORKPLACES_TYPES_DELETE'] = 'Удалить';
$MESS['WORKPLACES_TYPES_DELMES'] = 'Для того, чтобы удалить данное свойство, уберите привязки к нему в ЭЧЗ ';