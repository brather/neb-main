<?
/*
Работа с цитатами
*/
namespace Nota\UserData;

use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock\HighloadBlockTable;
use \Bitrix\Main\Type\DateTime;
use \Nota\Exalead\SearchQuery;
use \Nota\Exalead\SearchClient;
use \Nota\Journal\J;

Loader::includeModule("highloadblock");
Loader::includeModule('nota.exalead');
Loader::includeModule('nota.journal');

class Quotes
{
    private static function getEntity_data_class($HIBLOCK = HIBLOCK_QUO_DATA_USERS)
    {
        $hlblock           = HighloadBlockTable::getById($HIBLOCK)->fetch();
        $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $entity_data_class;
    }

    /**
    @method add добавление цитаты
    @param $arFields
    @param $arFields['BOOK_ID'] - ID книги в Экзалиде
    @param $arFields['TEXT'] - текст цитаты
    @param $arFields['IMG_DATA'] - data image base64
    @param $arFields['PAGE'] - номер страницы
    @param $arFields['TOP'] - левый верхний угол: Y
    @param $arFields['LEFT'] - левый верхний угол: X
    @param $arFields['WIDTH'] - ширина блока выделения/картинки
    @param $arFields['HEIGHT'] - высота блока выделения/картинки
    */
    public static function add(array $arFields, $uid = false)
    {
        global $USER;
        if (!$uid)
            $uid = $USER->GetID();

        if (empty($arFields))
            return false;

        $query = new SearchQuery();
        $query->getById($arFields['BOOK_ID']);

        $client = new SearchClient();
        $resExalead = $client->getResult($query);

        $entity_data_class = self::getEntity_data_class();
        $dt = new DateTime();

        $arFieldsAdd = array(
            'UF_DATE_ADD' => $dt,
            'UF_BOOK_ID' => $arFields['BOOK_ID'],
            'UF_UID' => $uid,
            'UF_TEXT' => $arFields['TEXT'],
            'UF_IMG_DATA' => $arFields['IMG_DATA'],
            'UF_PAGE' => $arFields['PAGE'],
            'UF_TOP' => $arFields['TOP'],
            'UF_LEFT' => $arFields['LEFT'],
            'UF_WIDTH' => $arFields['WIDTH'],
            'UF_HEIGHT' => $arFields['HEIGHT'],
            'UF_BOOK_NAME' => $resExalead['title'],
            'UF_BOOK_AUTHOR' => $resExalead['authorbook'],
        );

        J::add('quote', 'add', Array('BOOK_ID' => $arFields['BOOK_ID'], 'DATE' => $dt, 'TEXT' => $arFields['TEXT'], 'BOOK_LINK' => '/catalog/' . $arFields['BOOK_ID'] . '/'));

        $result = $entity_data_class::add($arFieldsAdd);
        $saveID = $result->getId();

        return $saveID;
    }

    /**
    @method getListBook - получить список цитат для книги
    @param $BOOK_ID string
    */
    public static function getListBook($bookID = false, $uid = false)
    {
        global $USER;
        if (!$uid)
            $uid = $USER->GetID();

        if (!empty($BOOK_ID))
            return false;

        $entity_data_class = self::getEntity_data_class();

        $rsData = $entity_data_class::getList(array(
            "select" => array('ID', 'UF_IMG_DATA', 'UF_PAGE', 'UF_TOP', 'UF_LEFT', 'UF_WIDTH', 'UF_HEIGHT', 'UF_DATE_ADD'),
            "filter" => array(
                'UF_UID' => $uid,
                'UF_BOOK_ID' => $bookID
            ),
        ));

        $arResult = array();

        while ($arData = $rsData->Fetch()) {
            $arResult[] = $arData;
        }

        return $arResult;
    }

    public static function getById($ID)
    {
        global $USER;
        if (empty($ID))
            return false;

        $entity_data_class = self::getEntity_data_class();

        $rsData = $entity_data_class::getList(array(
            "select" => array('*'),
            "filter" => array(
                #'UF_UID' 		=> $USER->GetID(),
                'ID' => $ID
            ),
        ));

        return $rsData->Fetch();
    }

    /**
     * Удаляет по паре UID + ID, заведомо проверив, реально ли эта запись принадлежит пользователю
     */
    public static function deleteWithTest($id, $uid)
    {
        $entity_data_class = self::getEntity_data_class();
        $toDelete = $entity_data_class::getList(array(
            "select" => array("ID"),
            "filter" => array(
                'ID' => $id,
                'UF_UID' => $uid,
            ),
        ))->Fetch();

        if (!$toDelete)
            return false;

        self::delete($toDelete['ID']);

        return true;
    }
    
    public static function delete($id)
    {
        $id = intval($id);
        if ($id <= 0)
            return false;

        $quote = self::getById($id);

        if (empty($quote))
            return false;

        J::add('quote', 'delete', Array('BOOK_ID' => $quote['BOOK_ID'], 'DATE' => new DateTime(), 'TEXT' => $quote['TEXT'], 'BOOK_LINK' => '/catalog/' . $quote['BOOK_ID'] . '/'));

        $entity_data_class = self::getEntity_data_class();
        $entity_data_class::Delete($quote['ID']);

        Collections::removeLinkObject($quote['ID'], false, 'quotes');
    }

    public static function update($id, $text)
    {
        $id = intval($id);
        $text = strip_tags(trim($text));

        if (empty($id) or empty($text))
            return false;

        $quote = self::getById($id);
        if (empty($quote))
            return false;

        J::add('quote', 'edit', Array('BOOK_ID' => $quote['BOOK_ID'], 'DATE' => new DateTime(), 'TEXT' => $text, 'BOOK_LINK' => '/catalog/' . $quote['BOOK_ID'] . '/'));

        $entity_data_class = self::getEntity_data_class();

        $arFields = array(
            'UF_TEXT' => $text
        );
        $entity_data_class::update($quote['ID'], $arFields);

        return true;
    }
}