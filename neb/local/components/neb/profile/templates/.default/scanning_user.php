<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix">
	<div>
		<?php if('ru' == LANGUAGE_ID):?>
			<p>Данный раздел предназначен для управления, хранения и оплаты материалов, полученных при работе с библиотечным комплексом самостоятельного сканирования.</p>
			<p>О наличии данной услуги в нужной Вам библиотеке,  Вы можете узнать у администратора конкретной <a href="/library" target="_blank">библиотеки</a>.</p>
		<?php elseif('en' == LANGUAGE_ID):?>
			<p>This section is intended for the management, storage and payment of materials obtained when using self-scanning library complex.</p>
			<p>For availability of these services in the order you want the library, you can ask the administrator a specific library.</p>
		<?php endif?>
	</div>
	
	<?$APPLICATION->IncludeComponent("neb:user.scanning", "", Array(),false	);?>
	
</section>