<?php
	require('../../PushAPI.inc');

	// Push a document with different stamps
	try {
		$papi = PushAPIFactory::createHttp("localhost", 42002, "default");
		$papi->ping();

		for($i = 0 ; $i < 10 ; $i++) {
			$url = 'http://example.com/stamp/doc.html';
			$myDoc1 = new Document(array('uri' => $url,
				'metas' =>
				array('path' => 'Path/To/Example/1'),
				'stamp' => 42 + $i
				)
			);

			$stamp = $papi->getDocumentStatus($url);
			if ($stamp !== false) {
				echo "Current stamp == " . $stamp . "\n";
			}
			echo "Pushing stamp == " . $myDoc1->metas['stamp'] . "\n";
			$resp = $papi->addDocument(array($myDoc1));

			// Set checkpoint and wait for documents to be indexed
			$serial = $papi->setCheckpoint(0, true);
			$papi->triggerIndexingJob();
			while(!$papi->areDocumentsSearchable($serial)) {
				sleep(3);
			}
		}

		$papi->ping();
		$papi->close();
		echo "All done!\n";

	} catch(PushAPIFactory $e)  {
		echo "Error: " . $e->getMessage() . "\n";
	}

?>
