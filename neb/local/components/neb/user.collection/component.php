<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

use \Bitrix\Main\Application;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock\HighloadBlockTable;
use \Nota\Exalead\SearchQuery;
use \Nota\Exalead\SearchClient;

Loc::loadMessages(__FILE__);
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

Loader::includeModule('iblock');
Loader::includeModule("highloadblock");
Loader::includeModule("nota.userdata");
Loader::includeModule('nota.exalead');

// highload quo
$hlblock_quo = HighloadBlockTable::getById(HIBLOCK_QUO_DATA_USERS)->fetch();
$entity_data_class_quo = HighloadBlockTable::compileEntity($hlblock_quo)->getDataClass();

// highload note
$hlblock_note = HighloadBlockTable::getById(HIBLOCK_NOTES_DATA_USERS)->fetch();
$entity_data_class_note = HighloadBlockTable::compileEntity($hlblock_note)->getDataClass();

// highload bookmark
$hlblock_bookmark = HighloadBlockTable::getById(HIBLOCK_BOOKMARKS_DATA_USERS)->fetch();
$entity_data_class_bookmark = HighloadBlockTable::compileEntity($hlblock_bookmark)->getDataClass();

$hlblock = HighloadBlockTable::getById(HIBLOCK_BOOKS_DATA_USERS)->fetch();
$entity_data_class_books = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

$hlblock = HighloadBlockTable::getById(HIBLOCK_COLLECTIONS_USERS)->fetch();
$entity_data_class_collection = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

$arResult = array();

$arResult['iNumPage'] = !empty($_REQUEST['PAGEN_1']) ? intval($_REQUEST['PAGEN_1']) : 1;

$cid = !empty($arParams['COLLECTION_ID']) ? intval($arParams['COLLECTION_ID']) : 'null';
$arResult['idCollection'] = $cid;

$collection = $entity_data_class_collection::getList(array('filter' => array('ID' => $cid)))->fetch();
$limit = ($arResult['iNumPage'] - 1) * 15;

// Псевдосортировка по б***ким идэшникам.
//    if(!empty($_REQUEST['by']) && ('document_publishyearsort' == $_REQUEST['by']) && !empty($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))):
//        $by = ' `ID` ' . $_REQUEST['order'];
//    else:
//        $by = ' `ID` DESC' ;
//    endif;

//    $strSql = "SELECT * FROM `neb_users_collections_links` WHERE `UF_COLLECTION_ID` = $cid ORDER BY $by  ";
$strSql = "SELECT * FROM `neb_users_collections_links` WHERE `UF_COLLECTION_ID` = $cid ";
$objData = Application::getConnection()->query($strSql);


$strSql = "SELECT count(*) as CCC FROM `neb_users_collections_links` WHERE `UF_COLLECTION_ID` = $cid";
$arResult['ITEM_COUNT'] = Application::getConnection()->query($strSql)->fetch();
$arResult['ITEM_COUNT'] = intval($arResult['ITEM_COUNT']['CCC']);

$arResult['ITEMS'] = array();
//Массивы для сортировки со вспомогательным массивом
$authors = $titles = $dates = array();
$index = 0;

$arFilter = array(
    "select" => array("*"),
    "filter" => array(),
    //"order"	 => array('ID' => 'DESC') //Нет смысла в сортировке, если выбирается только 1 запись.
);

$query = new SearchQuery();
$client = new SearchClient();

while ($row = $objData->fetch()) {

    $arFilter['filter'] = array('ID' => $row['UF_OBJECT_ID']);

    $res = array();

    if ('quotes' == $row['UF_TYPE']):
        $data = $entity_data_class_quo::getList($arFilter)->fetch();

    elseif ('books' == $row['UF_TYPE']):
        $data = $entity_data_class_books::getList($arFilter)->fetch();

    elseif ('notes' == $row['UF_TYPE']):
        $data = $entity_data_class_note::getList($arFilter)->fetch();

    elseif ('bookmarks' == $row['UF_TYPE']):
        $data = $entity_data_class_bookmark::getList($arFilter)->fetch();

    else:
        continue;

    endif;
    $query->getByArIds(array($data['UF_BOOK_ID']));
    $res = $client->getResult($query);
    $res = $res['ITEMS'][0];
    $res = array_merge($res, $data);

    $res['UF_TYPE'] = $row['UF_TYPE'];

    $arResult['ITEMS'][$index] = $res; //Основной массив данных
    $authors[$index] = $res['authorbook'];//Вспомогательный массив с авторами
    $titles[$index] = $res['title'];//Вспомогательный массив с названиями
    $dates[$index] = (int)$res['year'];//Вспомогательный массив с датами публикаций.
    ++$index; //Общий индекс всех массивов.
}

global $order;
$order = 'asc';
if (!empty($_REQUEST['order']) && is_set($_REQUEST['order'])) {
    switch (toupper($_REQUEST['order'])) {
        case 'ASC':
            $sort_order = SORT_ASC;
            break;
        case 'DESC':
            $sort_order = SORT_DESC;
            $order = 'desc';
            break;
    }
} else {
    $sort_order = SORT_ASC;
}

global $by;
$by = 'BOOK_AUTHOR';
if (!empty($_REQUEST['by']) && is_set($_REQUEST['by'])) {
    switch (toupper($_REQUEST['by'])) {
        case 'BOOK_AUTHOR':
            array_multisort($authors, $sort_order, SORT_LOCALE_STRING, $arResult['ITEMS']);
            break;
        case 'BOOK_NAME':
            array_multisort($titles, $sort_order, SORT_LOCALE_STRING, $arResult['ITEMS']);
            $by = 'BOOK_NAME';
            break;
        case 'PUBLISH_DATE':
            array_multisort($dates, $sort_order, SORT_NUMERIC, $arResult['ITEMS']);
            $by = 'PUBLISH_DATE';
            break;
    }
}

$rs = new CDBResult;
$rs->InitFromArray($arResult['ITEMS']);
$rs->NavStart(15, true, $arResult['iNumPage']);
$arResult["STR_NAV"] = $rs->GetPageNavStringEx($navComponentObject, "", "");
$arResult['NAV_COUNT'] = (int)$rs->NavRecordCount;

$arResult['ITEMS'] = array_slice($arResult['ITEMS'], $limit, 15);

$APPLICATION->SetTitle($collection['UF_NAME']);

$this->IncludeComponentTemplate();

return array('arParams' => $arParams, 'arResult' => $arResult);