<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

class FileUploadClass extends CBitrixComponent {
    //region Задание свойств для класса
    private $page = 'template';                     // Шаблон компонента
    private $arExtensions = array();                // Массив с разрешенными расширениями
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        $this->includeComponentLang(basename(__FILE__));
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->_getSettingsServer();                    // Возвращаем параметры массива $_SERVER
        $this->_checkFolder();                          // Проверяем и устанавливаем значение директории для хранения файлов
        $this->_setExtentions();                        // Устанавливаем массив расширений
        $this->_checkFilesInDirectory();                // Проверяем на существованеи файлы и заполняем массив с файлами
        $this->includeComponentTemplate($this->page);   // Подключаем шаблон
    }
    //endregion
    //region Определение корневой директории
    /**
     * Определение корневой директории
     */
    private function _getSettingsServer() {
        $arServer = Application::getInstance()->getContext()->getServer()->toArray();
        $this->arResult['HTTP_HOST'] = $arServer['HTTP_HOST'];
        $this->arResult['DOCUMENT_ROOT'] = $arServer['DOCUMENT_ROOT'];
    }
    //endregion
    //region Проверка папки для сохранения
    /**
     * Проверка папки для сохранения
     */
    private function _checkFolder() {
        if(!empty($this->arParams['FOLDER_FOR_FILES'])) {
            if(!is_dir($this->arParams['FOLDER_FOR_FILES'])) {
                mkdir($this->arParams['FOLDER_FOR_FILES'], 0777);
            }
            if(!empty($this->arParams['SUB_FOLDER'])) {
                $newDir = $this->arParams['FOLDER_FOR_FILES'] . $this->arParams['SUB_FOLDER'] . '/';
                if(!is_dir($newDir)) {
                    mkdir($newDir, 0777);
                }
                $uploadDirAbs = $newDir;
                $uploadDirRel = $this->arParams['FOLDER_FOR_FILES'] . $this->arParams['SUB_FOLDER'] . '/';
            } else {
                $uploadDirAbs = $this->arParams['FOLDER_FOR_FILES'];
                $uploadDirRel = $this->arParams['FOLDER_FOR_FILES'];
            }
            $this->arResult['UPLOAD_DIR'] = array(
                'PATH_ABSOLUTE' => $uploadDirAbs,
                'PATH_RELATIVE' => $uploadDirRel
            );
        } else {
            $this->arResult['ERROR'] = Loc::getMessage('FILE_UPLOAD_C_ERROR_FOLDER_NOT_FOUND');
        }        
    }
    //endregion
    //region Возвращаем массив расширений
    private function _setExtentions() {
        $this->arExtensions = explode(',', $this->arParams['TYPE_FILE']);
    }
    //endregion
    //region Получить размер файла
    /** Получить размер файла
     * @param $file - файл, размер которого необходимо получить
     * @return float|int|string
     */
    private function getSizeFile($file) {
        $fileSize = filesize($file);
        // Размер больше 1 Кб
        if($fileSize > 1000) {
            $fileSize = ($fileSize/1000);
            // Размер больше 1 Мегабайта
            if($fileSize > 1000) {
                $fileSize = ($fileSize/1000);
                // Размер больше 1 Гигабайта
                if($fileSize > 1000) {
                    $fileSize = ($fileSize/1000);
                    $fileSize = round($fileSize, 1) . Loc::getMessage('FILE_UPLOAD_C_SIZE_GB');
                } else {
                    $fileSize = round($fileSize, 1) . Loc::getMessage('FILE_UPLOAD_C_SIZE_MB');
                }
            } else {
                $fileSize = round($fileSize, 1) . Loc::getMessage('FILE_UPLOAD_C_SIZE_KB');
            }
        } else {
            $fileSize = round($fileSize, 1) . Loc::getMessage('FILE_UPLOAD_C_SIZE_B');
        }
        return $fileSize;
    }
    //endregion
    //region Проверка файлов в директории
    /**
     * Проверка файлов в директории
     */
    private function _checkFilesInDirectory(){
        $files = scandir($this->arResult['UPLOAD_DIR']['PATH_ABSOLUTE']);
        foreach($files as $iFile => $nFile) {
            $extFile = '';
            if($nFile != '.' && $nFile != '..') {
                $arPathInfoFile = pathinfo($this->arResult['UPLOAD_DIR']['PATH_ABSOLUTE'] . '/' . $nFile);
                $extFile = $arPathInfoFile['extension'];
                if(in_array($extFile, $this->arExtensions)) {
                    //region Если указано имя файла то возвращаем только лишь с тем именем
                    if(!empty($this->arParams['FILE_NAME'])) {
                        if(checkStringForSetAttr($this->arParams['FILE_NAME']) == $arPathInfoFile['filename']) {
                            $this->arResult['FILES'][] = array(
                                'ALIAS' => $this->arParams['FILE_ALIAS'],
                                'NAME_SHORT' => $arPathInfoFile['filename'],
                                'NAME_FULL' => $arPathInfoFile['basename'],
                                'EXTENSION' => $arPathInfoFile['extension'],
                                'FILE_SIZE' => self::getSizeFile($this->arResult['UPLOAD_DIR']['PATH_ABSOLUTE'] . '/' . $nFile),
                                'URL_DOWNLOAD' => $this->arResult['UPLOAD_DIR']['PATH_RELATIVE'] . $nFile
                            );
                        }
                    }
                    //endregion
                    //region Если имя файла не указано, то возвращаем весь список директории
                    else {
                        $this->arResult['FILES'][] = array(
                            'NAME_SHORT' => $arPathInfoFile['filename'],
                            'NAME_FULL' => $arPathInfoFile['basename'],
                            'EXTENSION' => $arPathInfoFile['extension'],
                            'FILE_SIZE' => self::getSizeFile($this->arResult['UPLOAD_DIR']['PATH_ABSOLUTE'] . '/' . $nFile),
                            'URL_DOWNLOAD' => $this->arResult['UPLOAD_DIR']['PATH_RELATIVE'] . $nFile
                        );
                    }
                    //endregion
                }
            }
        }
        if(!empty($this->arParams['COUNT_FILES_IN_DIRECTORY']) && count($this->arResult['FILES']) >= $this->arParams['COUNT_FILES_IN_DIRECTORY']) {
            $this->arResult['FULL_DIRECTORY'] = true;
        }
    }
    //endregion
}