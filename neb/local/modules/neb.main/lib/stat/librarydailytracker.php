<?php
/**
 * User: agolodkov
 * Date: 12.07.2016
 * Time: 17:23
 */

namespace Neb\Main\Stat;

use \Bitrix\Main\DB\Exception,
    \Bitrix\Main\Type\Date,
    \Bitrix\Main\Config\Option,
    \Neb\Main\Helper\Debug,
    \Neb\Main\Helper\MainHelper,
    \Neb\Main\Helper\DateHelper,
    \Nota\Exalead\BiblioAlisTable,
    \Nota\Exalead\LibraryBiblioCardTable,
    \CUtil;

/**
 * Class LibraryDailyTracker
 *
 * @package Neb\Main\Stat
 */
class LibraryDailyTracker
{
    /**
     * @var
     */
    private $_libraryId;

    /**
     * @var
     */
    private $_exaleadLibraryId;

    /**
     * @var
     */
    private $_date;

    /**
     * LibraryDailyTracker constructor.
     *
     * @param int $bitrixLibraryId
     * @param null $date
     * @param int $exaleadLibraryId
     *
     * @throws \Exception
     */
    public function __construct($bitrixLibraryId = 0, $date = null, $exaleadLibraryId = 0)
    {
        $this->_libraryId = (integer)$bitrixLibraryId;
        $this->_exaleadLibraryId = (int)$exaleadLibraryId;
        if (null === $date) {
            $this->_date = date('Y-m-d');
        } elseif (!$this->_date = strtotime($date)
            || !DateHelper::validateFormat($date, 'Y-m-d')
        ) {
            throw new \Exception('Not valid track date');
        } else {
            $this->_date = date('Y-m-d', strtotime($date));
        }
    }

    /**
     * @param $stat
     * @param string $date
     * @param array $params
     * @param $mParam
     *
     * @throws \Exception
     */
    public static function trackAllLibrariesStat($stat, $date = null, $params = [], $mParam)
    {
        $instance = new static(0, $date);
        $method = 'track' . $stat;
        if (!method_exists($instance, $method)) {
            throw new \Exception("Stat $stat not defined");
        }
        $rsLibraries = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ACTIVE' => 'Y', '!PROPERTY_LIBRARY_LINK' => false],
            false,
            false,
            ['ID', 'PROPERTY_LIBRARY_LINK']
        );
        while ($library = $rsLibraries->Fetch()) {

            $libraryInstance = new static((int)$library['ID'], $date, (int)$library['PROPERTY_LIBRARY_LINK_VALUE']);

            if (isset($mParam) && $mParam !== null)
                $libraryInstance->$method($mParam);
            else
                $libraryInstance->$method();
        }

        if (isset($mParam) && $mParam !== null)
            $instance->$method($mParam);
        else
            $instance->$method();

        if (isset($params['onTick']) && is_callable($params['onTick']))
        {
            $params['onTick']();
        }
    }

    /**
     * @return StatisticAggregator
     * @throws \Exception
     */
    private function _getAggregator()
    {
        if ($this->_libraryId > 0) {
            $libId = \nebLibrary::getLibraryIdByIblockId($this->_libraryId);
            $statAggregator = new StatisticAggregator(
                ['libraryExaleadIdsArray' => [$libId]]
            );
        } else {
            $statAggregator = new StatisticAggregator();
        }
        $statAggregator->prepareLibraryStatDates();

        return $statAggregator;
    }

    /**
     * @return array
     */
    private function _getTrackParams()
    {
        $dataParams = [
            'date' => $this->_date,
        ];
        if ($this->_libraryId > 0) {
            $dataParams['library_id'] = $this->_libraryId;
        }

        return $dataParams;
    }

    /**
     * @throws \Exception
     */
    public function trackLibraryBooks()
    {
        Debug::info("Books count library (id: {$this->_libraryId}) from exalead by date {$this->_date}");
        $statAggregator = $this->_getAggregator();
        $statAggregator->loadExaleadData();
        if (!$exaleadResult = $statAggregator->getExaleadResult()) {
            throw new \Exception('Cannot load data from exalead!');
        }
        $booksCount = $exaleadResult['COUNT'];
        Debug::info("Books count: $booksCount");

        $dataParams = $this->_getTrackParams();
        $dataParams['value_num'] = $booksCount;
        \Neb\Main\Stat\Tracker::periodDataTrack('library_books', $dataParams);
        $this->_saveInNebStatEdition('COUNT_EDITION', $dataParams);
    }

    /**
     *
     */
    public function trackLibraryBooksDigitized()
    {
        Debug::info("Books digitized count library (id: {$this->_libraryId}) from exalead by date {$this->_date}");
        $statAggregator = $this->_getAggregator();
        $booksDigitCount = $statAggregator->countPublicationsDigitized();
        if (!$statAggregator->getExaleadResult()) {
            throw new \Exception('Cannot load data from exalead!');
        }
        $booksDigitCount = $booksDigitCount[ConvertTimeStamp(time())];
        Debug::info("Books count: $booksDigitCount");

        $dataParams = $this->_getTrackParams();
        $dataParams['value_num'] = $booksDigitCount;

        \Neb\Main\Stat\Tracker::periodDataTrack('library_books_digitized', $dataParams);
        $this->_saveInNebStatEdition('COUNT_EDITION_DIG', $dataParams);
    }

    /**
     *
     */
    public function trackLibraryUsersRegisters()
    {
        Debug::info("Users count library (id: {$this->_libraryId}) by date {$this->_date}");
        $filter = [
            'DATE_REGISTER_1' => ConvertTimeStamp(strtotime($this->_date))
        ];
        if ($this->_libraryId > 0) {
            $filter = ['UF_LIBRARY' => $this->_libraryId];
        }
        $rsUsers = \CUser::GetList($order, $by, $filter);
        $rsUsers->NavStart();
        Debug::info("Users count: {$rsUsers->NavRecordCount}");

        $dataParams = $this->_getTrackParams();
        $dataParams['value_num'] = $rsUsers->NavRecordCount;

        \Neb\Main\Stat\Tracker::periodDataTrack('library_user_registers',$dataParams);
    }

    /**
     * Треки статистики скачивания и чтения изданий уникальных и не уникальных
     */
    public function trackLibraryStatLog()
    {
        Debug::info("Track stat log library (id: {$this->_libraryId}) by date {$this->_date}");
        $libraryId = 0;
        if ($this->_libraryId > 0) {
            $libraryId = \nebLibrary::getLibraryIdByIblockId($this->_libraryId);
        }
        if ($libraryId > 0) {
            $connection = NebStatLogTable::getEntity()->getConnection();
            $dataParams = $this->_getTrackParams();

            $query
                = "
SELECT
  SUM(CNT_READ) SUM_READ,
  SUM(CNT_VIEW) SUM_VIEW
FROM neb_stat_log
WHERE ID_LIB = $libraryId AND DT='{$this->_date}';
        ";
            $result = $connection->query($query)->fetch();

            $dataParams['value_num'] = $result['SUM_READ'];
            Tracker::periodDataTrack(
                'library_service_cnt_book_downloads',
                $dataParams
            );
            Debug::info("Sum book downloads count: {$dataParams['value_num']}");
            $dataParams['value_num'] = $result['SUM_VIEW'];
            Tracker::periodDataTrack(
                'library_service_cnt_book_views',
                $dataParams
            );
            Debug::info("Sum book views count: {$dataParams['value_num']}");


            $query
                = "
SELECT
  count(CNT_READ) CNT_READ,
  count(CNT_VIEW) CNT_VIEW
FROM neb_stat_log
WHERE ID_LIB = $libraryId AND DT='{$this->_date}';
        ";
            $result = $connection->query($query)->fetch();

            $dataParams['value_num'] = $result['CNT_READ'];
            Tracker::periodDataTrack(
                'library_service_cnt_book_downloads_unique',
                $dataParams
            );
            Debug::info("Count book downloads count: {$dataParams['value_num']}");
            $dataParams['value_num'] = $result['CNT_VIEW'];
            Tracker::periodDataTrack(
                'library_service_cnt_book_views_unique',
                $dataParams
            );
            Debug::info("Count book views count: {$dataParams['value_num']}");
        }
    }

    /**
     * @param array $params
     *
     * @throws \Exception
     */
    public function trackLibraryBookStatLog($params)
    {
        Debug::info('Start tracking library book stat log');
        MainHelper::includeModule('nota.exalead');

        $arAnonymousUser = \nebUser::getAnonymousUser();

        if (intval($arAnonymousUser['ID']) <= 0)
            throw new \Exception('Anonynouse user not found!');
        
        $batchSize = 1000;
        $arrayPost = [
            'token'  => $arAnonymousUser['UF_TOKEN'],
            'from'   => $this->_date,
            'to'     => $this->_date,
            'offset' => 0,
            'len'    => $batchSize,
        ];

        /** На всякий случай, чтобы не потереть всю статистику */
        if (!DateHelper::validateFormat($this->_date, 'Y-m-d')) {
            throw new \Exception('Date is not valid');
        }
        $connection = NebStatLogTable::getEntity()->getConnection();
        $connection->query('START TRANSACTION;');
        $connection->query("DELETE from neb_stat_log WHERE DT = '{$this->_date}';");

        $process = [
            'offset'   => 0,
            'finded'   => 0,
            'to-save'  => 0,
            'updated'  => 0,
            'inserted' => 0,
        ];
        while ($result = $this->_applyStatsBatch($arrayPost)) {
            $arrayPost['offset'] += $batchSize;
            $process['offset'] = $arrayPost['offset'];
            $msg = [];
            foreach ($process as $key => $value) {
                if (isset($result[$key])) {
                    $process[$key] += $result[$key];
                }
                $msg[] = "$key: {$process[$key]}";
            }
            $msg = implode(', ', $msg);
            Debug::reline($msg, 'info');
            if(isset($params['onTick']) && is_callable($params['onTick'])) {
                $params['onTick']();
            }
        }
        Debug::clearReline();

        $connection->query('COMMIT;');
    }

    /**
     * @param int $isProtected
     */
    public function trackPublicationProtected( $isProtected = 0 )
    {
        switch ($isProtected)
        {
            case 0:
                $debugInfo = "Not protected publication, count by date: " . $this->_date;
                break;
            default:
                $debugInfo = "Protected publication, count by date: " . $this->_date;
        }
        Debug::info($debugInfo);

        $q = [
            // Default AND
            "q" => ["#all", "filesize>0", "isprotected:" . intval($isProtected), " NOT ispart:1"],
            "sl" => "sl_statistic3",
            "hf" => 0
        ];

        if ( $this->_exaleadLibraryId !== 0 ) {
            $q['q'][] = "idlibrary:" . $this->_exaleadLibraryId;
        }

        // Получить данные по всем PROTECTED & NO PROTECTED & COPYRIGHT публикациям
        $aggResult = $this->_aggregateReturnResult( $q, false );
        $dataParams = $this->_getTrackParams();
        $dataParams['value_num'] = intval($aggResult['COUNT']);

        if ( !isset($dataParams['library_id']) && $this->_libraryId === 0 ) {
            $dataParams['library_id'] = $this->_libraryId;
        }

        switch ($isProtected) {
            case  2: $trackType = 'publication_copyright';     break;
            case  1: $trackType = 'publication_is_protected';  break;
            default: $trackType = 'publication_not_protected';
        }

        \Neb\Main\Stat\Tracker::periodDataTrack($trackType, $dataParams);
    }

    /**
     * Треки разметки изданий
     */
    public function trackPublicationMarkup()
    {
        $q = [
            "q" => ["#all", "filesize>0", "ispart:1"],
            "sl" => "sl_statistic3",
            "hf" => 0
        ];

        if ( $this->_exaleadLibraryId !== 0 ) {
            $q['q'][] = "idlibrary:" . $this->_exaleadLibraryId;
        }

        $aggResult = $this->_aggregateReturnResult( $q, false );
        $dataParams = $this->_getTrackParams();
        $dataParams['value_num'] = intval($aggResult['COUNT']);

        if ( !isset($dataParams['library_id']) && $this->_libraryId === 0 ) {
            $dataParams['library_id'] = $this->_libraryId;
        }

        \Neb\Main\Stat\Tracker::periodDataTrack('publication_markup', $dataParams);
    }

    public function trackPublicationLanguage( )
    {
        $q = array(
            "q"     => ($this->_exaleadLibraryId === 0)? array("#all", "filesize>0") : array("#all", "filesize>0", "idlibrary:\"" . $this->_exaleadLibraryId . "\""),
            "sl"    => "sl_statistic3",
            "hf"    => "0"
        );

        $aggResult  = $this->_aggregateReturnResult( $q, false );

        if ( isset( $aggResult['GROUPS']['LANG'] ) )
        {
            /* Перебор данных по LANG для конкреной LIBRARY */
            foreach ( $aggResult['GROUPS']['LANG'] as $lang )
            {
                $dataParams = $this->_getTrackParams();
                $dataParams['value_num'] =  $lang['count'];
                $dataParams['tr_type'] = CUtil::translit( mb_convert_encoding( $lang['title'], "UTF-8" ), 'ru' );
                $dataParams['cr_type'] =  $lang['title'];

                $trackType = "language";

                if ( !isset($dataParams['library_id']) && $this->_libraryId === 0 )
                    $dataParams['library_id'] = $this->_libraryId;

                // Запись данных в таблицу статистики
                \Neb\Main\Stat\Tracker::periodDataTrack($trackType, $dataParams);
            }
        }

        return 0;
    }

    public function trackFileExtension ( )
    {
        $paramPrepare = array(
            "q"     => ($this->_exaleadLibraryId === 0)? array("#all", "filesize>0") : array("#all", "filesize>0", "idlibrary:\"" . $this->_exaleadLibraryId . "\""),
            "sl"    => "sl_statistic3",
            "hf"    => "0"
        );

        $aggResult = $this->_aggregateReturnResult( $paramPrepare, false );

        if ( isset( $aggResult['GROUPS']['FILE_EXTENSION'] ) )
        {
            foreach ( $aggResult['GROUPS']['FILE_EXTENSION'] as $file_extension )
            {
                $dataParams = $this->_getTrackParams();
                $dataParams['value_num'] = $file_extension['count'];
                $dataParams['extension'] = $file_extension['title'];
                $trackType = "file_extension";

                if ( !isset($dataParams['library_id']) && $this->_libraryId === 0 )
                    $dataParams['library_id'] = $this->_libraryId;

                if ( !isset($dataParams['neb_library_id']) && $this->_exaleadLibraryId !== 0 )
                    $dataParams['neb_library_id'] = $this->_exaleadLibraryId;

                // Запись данных в таблицу статистики
                \Neb\Main\Stat\Tracker::periodDataTrack($trackType, $dataParams);
            }
        }

        return 0;
    }

    /**
     * Recursion!!!
     *
     * @return int
     * //todo Пересмотреть.
     */
    public function trackLibraryCollection( )
    {
        $q = array(
            /* Default AND */
            "q" => ($this->_exaleadLibraryId === 0)? array("#all", "filesize>0") : array( "#all", "filesize>0", "idlibrary:" . $this->_exaleadLibraryId ),
            "sl" => "sl_statistic3",
            "hf" => "0",
        );

        /* Запрос данных для конкретного ID LIBRARY */
        $aggResult = $this->_aggregateReturnResult( $q, false );

        if ( isset( $aggResult['GROUPS']['COLLECTION_NEW'] ) )
        {
            /* Перебор данных по COLLECTION для конкреной LIBRARY */
            foreach ( $aggResult['GROUPS']['COLLECTION_NEW'] as $collection )
            {
                $dataParams = $this->_getTrackParams();
                $dataParams['value_num'] = $collection['count'];
                $dataParams['title'] = $collection['title'];
                $dataParams['code'] = CUtil::translit( mb_convert_encoding($collection['title'], 'UTF-8'), 'ru' );
                $dataParams['ext_id'] = 0;
                $trackType = "collection";

                if ( !isset($dataParams['library_id']) && $this->_libraryId === 0 )
                    $dataParams['library_id'] = $this->_libraryId;

                // Запись данных в таблицу статистики
                \Neb\Main\Stat\Tracker::periodDataTrack($trackType, $dataParams);
            }
        }

        return 0;
    }

    public function trackBBKFull ( )
    {
        $q = array (
            "q"     => ( $this->_exaleadLibraryId === 0 ) ? array("#all", "filesize>0"): array("#all", "filesize>0", "idlibrary:" . $this->_exaleadLibraryId),
            "sl"    => "sl_statistic3",
            "hf"    => "0"
        );

        $aggResult = $this->_aggregateReturnResult( $q, false);

        if ( isset( $aggResult['GROUPS']['BBKFULL_STAT'] ))
        {
            foreach ($aggResult['GROUPS']['BBKFULL_STAT'] as $bbkfull )
            {
                $dataParams = $this->_getTrackParams();
                $dataParams['value_num'] = $bbkfull['count'];
                $dataParams['title'] = $bbkfull['title'];
                $dataParams['pathId'] = $bbkfull['pathId'];
                $dataParams['path'] = $bbkfull['path'];
                $dataParams['fullPath'] = $bbkfull['fullPath'];
                $dataParams['depth'] = substr_count($bbkfull['pathId'], "/") - 1;

                $trackType = "bbkfull";

                if ( !isset($dataParams['library_id']) && $this->_libraryId === 0 )
                    $dataParams['library_id'] = $this->_libraryId;

                if ( !isset($dataParams['neb_library_id']) && $this->_exaleadLibraryId !== 0 )
                    $dataParams['neb_library_id'] = $this->_exaleadLibraryId;

                // Запись данных в таблицу статистики
                \Neb\Main\Stat\Tracker::periodDataTrack($trackType, $dataParams);
            }
        }

        return 0;
    }

    private function _aggregateReturnResult ( $param = null, $cache = true )
    {
        /* From default requests */
        if ( is_null($param) )
        {
            $param = array(
                "q"     => "#all",
                "sl"    => "sl_statistic3",
                "hf"    => "0"
            );
        }
        try {
            if ( !isset($param["q"]) || count($param["q"]) == 0 )
                throw new Exception("Параметр запроса 'q' не передан, пустой!");
        }
        catch (Exception $e)
        {
            die($e->getMessage() . "\t" . $e->getFile() . " : " . $e->getLine());
        }

        $agg = $this->_getAggregator();

        return $agg->prepareExaleadParamsAndGetResult( $param, $cache );
    }

    /**
     * @param array $arrayPost
     *
     * @return bool|array
     * @throws \Exception
     */
    private function _applyStatsBatch($arrayPost)
    {
        $protocol = Option::get('nota.exalead', 'viewer_protocol');
        $ip       = Option::get('nota.exalead', 'viewer_ip');
        $port     = Option::get('nota.exalead', 'viewer_port');
        $key      = Option::get("nota.exalead", "viewer_key");

        $statResultJson = $this->_viewerCurl($protocol . '://' . $ip . '/stat/', $arrayPost, $port, $key);
        $statResultJson = iconv('ISO-8859-1', 'UTF-8', $statResultJson);
        if (null === ($statResult = \json_decode($statResultJson, true))) {
            if (JSON_ERROR_NONE !== \json_last_error()) {
                throw new \Exception(
                    'Parse json error: ' . \json_last_error_msg() . PHP_EOL . 'Json content: ' . $statResultJson
                );
            }
        }

        return empty($statResult) ? false : $this->_saveStatsBatch($statResult);
    }

    /**
     * @param array $statBatch
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Exception
     */
    private function _saveStatsBatch(&$statBatch)
    {
        $result = [];
        $books = [];
        foreach ($statBatch as $item) {
            $read = 0;
            $view = 0;
            $continue = false;
            switch ($item['method']) {
                case 'GetResource':
                    $read = 1;
                    break;
                case 'RenderPageCustomSize':
                case 'PageSearchRender':
                    $view = 1;
                    break;
                default:
                    $continue = true;
                    break;
            }
            if (true === $continue) {
                continue;
            }
            $bookId = urldecode($item['document']);
            $bookId = trim($bookId);
            if (empty($bookId)) {
                continue;
            }
            if (!isset($books[$bookId])) {
                $books[$bookId] = [
                    'CNT_READ' => 0,
                    'CNT_VIEW' => 0,
                ];
            }
            $books[$bookId]['CNT_READ'] += $read;
            $books[$bookId]['CNT_VIEW'] += $view;
        }
        $result['finded'] = count($books);

        $alisBookMap = [];
        if (!empty($books)) {
            $bookData = LibraryBiblioCardTable::getByFullSymbolicId(array_keys($books));

            foreach ($bookData as $item) {
                if ($item['ALIS'] && $item['FullSymbolicId']) {
                    if (!isset($alisBookMap[$item['ALIS']])) {
                        $alisBookMap[$item['ALIS']] = [];
                    }
                    $alisBookMap[$item['ALIS']][] = $item['FullSymbolicId'];
                }
            }
        }

        if (!empty($alisBookMap)) {
            $alisData = BiblioAlisTable::getList(
                [
                    'filter' => [
                        'Id' => array_keys($alisBookMap)
                    ]
                ]
            );
            while ($item = $alisData->fetch()) {
                if (isset($alisBookMap[$item['Id']]) && $item['Library']) {
                    foreach ($alisBookMap[$item['Id']] as $bookId) {
                        $books[$bookId]['ID_LIB'] = $item['Library'];
                    }
                }
            }
        }

        foreach ($books as $key => $item) {
            if (!$item['CNT_READ'] && !$item['CNT_VIEW']) {
                unset($books[$key]);
            }
            if (!isset($item['ID_LIB'])) {
                unset($books[$key]);
            }
        }

        $result['to-save'] = count($books);
        if (!empty($books)) {
            $statLogResult = NebStatLogTable::getList(
                [
                    'filter' => [
                        'DT'      => ConvertTimeStamp(strtotime($this->_date)),
                        'ID_BOOK' => array_keys($books),
                    ],
                ]
            );
            while ($item = $statLogResult->fetch()) {
                if (isset($books[$item['ID_BOOK']])) {
                    $books[$item['ID_BOOK']]['ID'] = $item['ID'];
                    $books[$item['ID_BOOK']]['CNT_READ'] += (integer)$item['CNT_READ'];
                    $books[$item['ID_BOOK']]['CNT_VIEW'] += (integer)$item['CNT_VIEW'];
                }
            }

            $insertValues = [];
            $updated = 0;
            foreach ($books as $bookId => $item) {
                if (isset($item['ID'])) {
                    $updateResult = NebStatLogTable::update(
                        $item['ID'], [
                            'CNT_READ' => $item['CNT_READ'] > 0 ? $item['CNT_READ'] : null,
                            'CNT_VIEW' => $item['CNT_VIEW'] > 0 ? $item['CNT_VIEW'] : null,
                        ]
                    );
                    $updated += $updateResult->getAffectedRowsCount();
                } else {
                    $insertValues[] = implode(
                        ',',
                        [
                            'DT'       => '"' . $this->_date . '"',
                            'ID_BOOK'  => '"' . $bookId . '"',
                            'ID_LIB'   => $item['ID_LIB'],
                            'CNT_READ' => $item['CNT_READ'] > 0 ? ('"' . $item['CNT_READ'] . '"') : 'NULL',
                            'CNT_VIEW' => $item['CNT_VIEW'] > 0 ? ('"' . $item['CNT_VIEW'] . '"') : 'NULL',
                        ]
                    );
                }
            }
            $result['updated'] = $updated;
            $result['inserted'] = 0;

            if (!empty($insertValues)) {
                $insertValues = '(' . implode("),\n(", $insertValues) . ')';
                $connection = NebStatLogTable::getEntity()->getConnection();
                $connection->query(
                    "
INSERT INTO neb_stat_log(
  DT,
  ID_BOOK,
  ID_LIB,
  CNT_READ,
  CNT_VIEW)
VALUES $insertValues
"
                );
                $result['inserted'] = $connection->getAffectedRowsCount();
            }
        }

        return $result;
    }

    /**
     * @param        $url
     * @param        $array_post
     * @param null   $port
     * @param string $key
     *
     * @return mixed
     */
    private function _viewerCurl($url, $array_post, $port = null, $key = '')
    {
        $ch = curl_init($url);
        curl_setopt(
            $ch, CURLOPT_USERAGENT,
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.17) Gecko/2009122116 Firefox/3.0.17"
        );
        $arHeader = [];
        if (!empty($key)) {
            $arHeader[] = 'DLIB-KEY: ' . $key;
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $arHeader);
        if (!empty($port)) {
            curl_setopt($ch, CURLOPT_PORT, $port);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, MainHelper::checkSslVerification($url));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLINFO_HEADER_OUT, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array_post));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


    /**
     * @param $field
     * @param $dataParams
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Exception
     */
    private function _saveInNebStatEdition($field, $dataParams)
    {
        $result = false;
        if (!empty($dataParams['library_id']) && $timestamp = strtotime($dataParams['date'])) {
            $statItem = NebStatEditionTable::getList([
                'filter' => [
                    'ID_BITRIXDB' => $dataParams['library_id'],
                    '>=DATE_STAT' => Date::createFromTimestamp($timestamp),
                    '<=DATE_STAT' => Date::createFromTimestamp($timestamp + 86399),
                ]
            ])->fetch();

            $fields = [
                'ID_BITRIXDB'       => $dataParams['library_id'],
                'DATE_STAT'         => Date::createFromTimestamp($timestamp),
                'ACTIVE_IN_BITRIX'  => 'Y',
                'COUNT_EDITION'     => 0,
                'COUNT_EDITION_DIG' => 0,
            ];
            if ($statItem) {
                $fields = array_merge($fields, $statItem);
                $fields[$field] = $dataParams['value_num'];
                unset($fields['ID']);
                $updateResult = NebStatEditionTable::update($statItem['ID'], $fields);
                $result = $updateResult->getAffectedRowsCount() > 0 ? true : false;
            } else {
                $library = \CIBlockElement::GetList(
                    array(),
                    array('ID' => $dataParams['library_id']),
                    array('PROPERTY_LIBRARY_LINK', 'NAME')
                )->Fetch();
                if ($library && !empty($library['PROPERTY_LIBRARY_LINK_VALUE'])) {
                    $fields['ID_EXALED'] = $library['PROPERTY_LIBRARY_LINK_VALUE'];
                    $fields['TITLE_LIB'] = $library['NAME'];
                    $fields[$field] = $dataParams['value_num'];
                    $addResult = NebStatEditionTable::add($fields);
                    $result = $addResult->getId() ? true : false;
                }
            }
        }

        return $result;
    }
}