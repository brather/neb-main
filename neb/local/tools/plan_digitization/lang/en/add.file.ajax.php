<?php
$MESS['ADD_FILE_AJAX_DELETE'] = ' Delete';
$MESS['ADD_FILE_AJAX_ERROR_NOT_SAVE'] = 'Error saving the file, try again later';
$MESS['ADD_FILE_AJAX_ERROR_FILE_SIZE'] = 'It exceeds the maximum file size';
$MESS['ADD_FILE_AJAX_ERROR_NOT_CONNECTION_TO_DB'] = 'Error connecting to database';
$MESS['ADD_FILE_AJAX_ERROR_EXTENSION'] = 'Invalid file type';
$MESS['ADD_FILE_AJAX_ERROR_EMPTY_DIRECTORY'] = 'No root folder for saving files';
$MESS['ADD_FILE_AJAX_BYTE'] = 'Byte';
$MESS['ADD_FILE_AJAX_KB'] = 'Kb';
$MESS['ADD_FILE_AJAX_MB'] = 'Mb';
$MESS['ADD_FILE_AJAX_GB'] = 'Gb';
$MESS['ADD_FILE_AJAX_CONFIRM_TITLE'] = 'Delete?';
$MESS['ADD_FILE_AJAX_CONFIRM_TEXT'] = 'Delete';
$MESS['ADD_FILE_AJAX_CONFIRM_YES'] = 'Are you sure you want to delete this pdf?';