<?
namespace Bitrix\NotaExt\Ext\colorPickerProp\Prop;

/*
Свойство инфоблока для вывода поля с выбором цвета
*/
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class PropertyColorPicker
{
    public function GetUserTypeDescription()
    {
        return array(
            'PROPERTY_TYPE' => 'S',
            'USER_TYPE' => 'prop_color_picker',
            'DESCRIPTION' => 'ColorPicker',
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
            'PrepareSettings' => array(__CLASS__, 'PrepareSettings'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            //'GetAdminFilterHTML' 	=> array(__CLASS__, 'GetAdminFilterHTML'),
        );
    }

    public function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
    {
        global $APPLICATION;
        \CUtil::InitJSCore(array("jquery"));
        $APPLICATION->AddHeadString('<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.minicolors/2.1.2/jquery.minicolors.css">', true);
        $APPLICATION->AddHeadString('<script src="//cdn.jsdelivr.net/jquery.minicolors/2.1.2/jquery.minicolors.min.js"></script>', true);
        $APPLICATION->AddHeadString("<script>
				function initMyInput(inputID){
					this[inputID+'_interval'] = setInterval(function() {
						if ( $('#'+inputID).length > 0 )
						{
							$('#'+inputID).minicolors({
								control: $(this).attr('data-control') || 'hue',
								defaultValue: $(this).attr('data-defaultValue') || '',
								inline: $(this).attr('data-inline') === 'true',
								letterCase: $(this).attr('data-letterCase') || 'lowercase',
								opacity: $(this).attr('data-opacity'),
								position: $(this).attr('data-position') || 'bottom right',
								/*change: function(hex, opacity) {
									var log;
									try {
										log = hex ? hex : 'transparent';
										if( opacity ) log += ', ' + opacity;
										console.log(log);
									} catch(e) {}
								},*/
								theme: 'default'
							});

							clearInterval(this[inputID+'_interval']);
						}
					}, 500);
				}
			</script>", true);


        $result = strlen($value['VALUE']) > 0 ? "
			
				<div>
					<div style='display:inline-block;width:20px;height:20px;background-color:{$value['VALUE']};'></div>
					<span>{$value['VALUE']}</span>
				</div>
			
			" : 'Цвет не выбран.';

        return $result;
    }

    public function GetPropertyFieldHtml($arProperty, $arValue, $strHTMLControlName)
    {
        global $APPLICATION;
        $arSettings = self::PrepareSettings($arProperty);

        \CUtil::InitJSCore(array("jquery"));
        $APPLICATION->AddHeadString('<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.minicolors/2.1.2/jquery.minicolors.css">', true);
        $APPLICATION->AddHeadString('<script src="//cdn.jsdelivr.net/jquery.minicolors/2.1.2/jquery.minicolors.min.js"></script>', true);

        //if(empty($arValue['VALUE']))
        //	$arValue['VALUE'] = $arProperty['USER_TYPE_SETTINGS']['DEFAULT_VALUE'];
        $strResult = '';
        ob_start();
        ?>

        <input type="text" name="<?= $strHTMLControlName["VALUE"] ?>"
               id="color_<?= md5($strHTMLControlName["VALUE"]) ?>" value="<?= $arValue['VALUE'] ?>"
               style="padding-left: 26px !important;"/>

        <?
        // Определим, на какой странице мы находимся, и в зависимости от того, детальная это страница или список - выдадим нужный js
        $curPage = $APPLICATION->GetCurPage();
        ?>
        <? if (strpos($curPage, 'iblock_element_admin') !== false) { ?>
        <script type="text/javascript">
            top.BX.evalGlobal('initMyInput("color_<?=md5($strHTMLControlName["VALUE"])?>");');
        </script>
    <? } elseif (strpos($curPage, 'iblock_element_edit') !== false) { ?>
        <script type="text/javascript">
            $(function () {
                $('#color_<?=md5($strHTMLControlName["VALUE"])?>').minicolors({
                    control: $(this).attr('data-control') || 'hue',
                    defaultValue: $(this).attr('data-defaultValue') || '',
                    inline: $(this).attr('data-inline') === 'true',
                    letterCase: $(this).attr('data-letterCase') || 'lowercase',
                    opacity: $(this).attr('data-opacity'),
                    position: $(this).attr('data-position') || 'bottom left',
                    /*change: function(hex, opacity) {
                     var log;
                     try {
                     log = hex ? hex : 'transparent';
                     if( opacity ) log += ', ' + opacity;
                     console.log(log);
                     } catch(e) {}
                     },*/
                    theme: 'default'
                });
            });
        </script>
    <? } ?>
        <?

        $strResult = ob_get_contents();
        ob_end_clean();

        return $strResult;
    }

    public function PrepareSettings($arFields)
    {
        $strDefaultValue = trim(isset($arFields['USER_TYPE_SETTINGS']['DEFAULT_VALUE']) ? $arFields['USER_TYPE_SETTINGS']['DEFAULT_VALUE'] : '#70c24a');

        return array(
            'DEFAULT_VALUE' => $strDefaultValue,
        );
    }

    public function GetSettingsHTML($arFields, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            /*Скрываем не нужные настройки*/
            "HIDE" => array("ROW_COUNT", "COL_COUNT", "MULTIPLE_CNT", "DEFAULT_VALUE", "WITH_DESCRIPTION", "MULTIPLE", "SEARCHABLE", "FILTRABLE", "SMART_FILTER"),
        );

        $arSettings = self::PrepareSettings($arFields);

        return '<tr>
			<td>' . Loc::getMessage('COLOR_PICKER_SETTING_DEFAULT_VALUE') . '</td>
			<td><input type="text" name="' . $strHTMLControlName["NAME"] . '[DEFAULT_VALUE]" value="' . htmlspecialcharsbx($arSettings['DEFAULT_VALUE']) . '"></td>
			</tr>
			';
    }
}

?>