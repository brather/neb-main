<?

$MESS['LIB_SEARCH_PAGE_DETAIL_AUTHOR'] = 'Author';
$MESS['LIB_SEARCH_PAGE_DETAIL_TITLE'] = 'Title';
$MESS['LIB_SEARCH_PAGE_DETAIL_SUBTITLE'] = 'Subtitle';
$MESS['LIB_SEARCH_PAGE_DETAIL_RESPONSIBILITY'] = 'Responsibility';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE'] = 'Publish place';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH'] = 'Publisher';
$MESS['LIB_SEARCH_PAGE_DETAIL_YEAR'] = 'Year of publication';
$MESS['LIB_SEARCH_PAGE_DETAIL_PAGES'] = 'Pages';
$MESS['LIB_SEARCH_PAGE_DETAIL_SERIE'] = 'Chapter';
$MESS['LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT'] = 'Note content';
$MESS['LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE'] = 'Note';
$MESS['LIB_SEARCH_PAGE_DETAIL_ANNOTATION'] = 'Annotation';
$MESS['LIB_SEARCH_PAGE_DETAIL_BBK'] = 'BBK';
$MESS['LIB_SEARCH_PAGE_DETAIL_UDC'] = 'Universal Decimal Classification (UDC)';
$MESS['LIB_SEARCH_PAGE_DETAIL_EDITION'] = 'Edition';
$MESS['LIB_SEARCH_PAGE_DETAIL_LIB'] = 'Library';
$MESS['LIB_SEARCH_PAGE_DETAIL_LANGUAGE'] = 'Language';

$MESS['LIB_SEARCH_PAGE_DETAIL_FUND_NUMBER'] = 'Fund number'; //Номер фонда
$MESS['LIB_SEARCH_PAGE_DETAIL_FUND_NAME'] = 'Fund name'; //Название фонда
$MESS['LIB_SEARCH_PAGE_DETAIL_INVENTORY_NUMBER'] = 'Inventory number'; // Номер описи
$MESS['LIB_SEARCH_PAGE_DETAIL_INVENTORY_NAME'] = 'Inventory name'; //Заголовок описи
$MESS['LIB_SEARCH_PAGE_DETAIL_UNIT_NUMBER'] = 'Unit number'; //Номер дела
$MESS['LIB_SEARCH_PAGE_DETAIL_UNIT_NAME'] = 'Unit name';//Заголовок дела';
$MESS['LIB_SEARCH_PAGE_DETAIL_DATES'] = 'Dates'; //Крайние даты

$MESS['LIB_SEARCH_PAGE_DETAIL_DESCRIPTION'] = 'Description'; //Описание
$MESS['LIB_SEARCH_PAGE_DETAIL_MATERIAL'] = 'Material, technique'; //Материал, техника исполнения
$MESS['LIB_SEARCH_PAGE_DETAIL_SIZE'] = 'Size'; //Размеры

$MESS['LIB_SEARCH_PAGE_DETAIL_MPK'] = 'MPK'; //МПК
$MESS['LIB_SEARCH_PAGE_DETAIL_DECLARANT'] = 'Declarant'; //Заявитель
$MESS['LIB_SEARCH_PAGE_DETAIL_PATENTHOLDER'] = 'Patentholder'; //Патентообладатель
$MESS['LIB_SEARCH_PAGE_DETAIL_REPRESENTATIVE'] = 'Representative';//Представитель
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH_DATE'] = 'Publication date'; //Дата публикации
$MESS['LIB_SEARCH_PAGE_DETAIL_PATENTCODE'] = 'Patent code'; //Дата публикации
