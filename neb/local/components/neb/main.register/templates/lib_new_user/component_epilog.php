<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Nota\UserData\Rgb;
if (isset($arResult['VALUES']['UF_PLACE_REGISTR']) && $arResult['VALUES']['UF_PLACE_REGISTR'] == 'on') //Запоним поля для РГБ
{ 
    $arResult['VALUES']['WORK_CITY'] = $arResult['VALUES']['PERSONAL_CITY'];
    $arResult['VALUES']['WORK_ZIP'] = $arResult['VALUES']['PERSONAL_ZIP'];
    $arResult['VALUES']['WORK_STREET'] = $arResult['VALUES']['PERSONAL_STREET'];
    $arResult['VALUES']['UF_HOUSE2'] = $arResult['VALUES']['UF_CORPUS'];
//    $arResult['VALUES'][] = $arResult['VALUES'][];
}
if (intval($arResult['VALUES']['USER_ID']) > 0) {
    $arFields = array();
    $arFields['ACTIVE'] = 'Y';
    
    if ( !empty($_REQUEST['scan1']) )
    {
        $arFile = CFile::MakeFileArray( $_SERVER["DOCUMENT_ROOT"] . $_REQUEST['scan1'] );
        $arFile["MODULE_ID"] = "main";
        $arFields['UF_SCAN_PASSPORT1'] =  $arFile;
    }
    
    if ( !empty($_REQUEST['scan2']) )
    {
        $arFile2 = CFile::MakeFileArray( $_SERVER["DOCUMENT_ROOT"] . $_REQUEST['scan2'] );
        $arFile2["MODULE_ID"] = "main";
        $arFields['UF_SCAN_PASSPORT2'] =  $arFile2;
    }

//    $arFields['UF_STATUS'] = nebUser::USER_STATUS_REGISTERED;
    $arFields['UF_STATUS'] = nebUser::USER_STATUS_VERIFIED; //Сразу верифицируем.
    $arFields['UF_REGISTER_TYPE'] = nebUser::REGISTER_TYPE_SIMPLE;

    $user = new CUser;
    $user->Update($arResult['VALUES']['USER_ID'], $arFields);
    nebUser::SendUserInfo(
        $arResult['VALUES']['USER_ID'], SITE_ID, $arParams['MESSAGE']
    );
    
    if ($arParams['verifyUser']) //Отправим пользователя на верификацию.
    {
        $rgb = new rgb();
        $res = $rgb->pushUserIntoRGB($arResult['VALUES']['USER_ID']);
    }
}
?>
