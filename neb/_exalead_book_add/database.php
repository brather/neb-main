<?
	class DBConnect
	{
		private $db = null;
		private $result = null; 

		/*Конструктору передаем адрес, имя пользователя, пароль, имя базы данных, порт, а также кодировку для соединения.
		По умолчанию используется utf8*/ 

		public function __construct($host, $user, $password, $base, $port = null, $charset = 'utf8')
		{
			$this->db = new mysqli($host, $user, $password, $base, $port);
			$this->db->set_charset($charset);
		} 

		public function __destruct() {
			$this->db->close();
		}

		/*основная и единственная функция, которая выполняет запрос и возвращает результат его работы*/ 

		public function query($query)
		{
			if(!$this->db)
				return false; 

			/*очищаем предыдущий результат*/ 

			if(is_object($this->result))
				$this->result->free(); 

			/*выполняем запрос*/ 

			$this->result = $this->db->query($query); 

			/*если есть ошибки - выводим их*/ 

			if($this->db->errno)
				die("mysqli error #".$this->db->errno.": ".$this->db->error); 

			/*если результат отрицательный - возвращаем false*/ 

			if($this->result == FALSE)
				return false;

			return $this->result;
		}

		public function fetch()
		{
			if(is_object($this->result))
				return $this->result->fetch_assoc();
			else
				return false;
		}
		public function fetch_array()
		{
			if(is_object($this->result))
				return $this->result->fetch_array();
			else
				return false;
		}

		public function insert_id()
		{
			return $this->db->insert_id;
		}

		public function insert($table, $arFields)
		{
			if(empty($arFields) or !is_array($arFields) or empty($table))
				return false;

			$strSet = self::getFieldsSet($arFields);

			$strSql = 'INSERT INTO '.$table.' SET '.$strSet;
			self::query($strSql);
			return self::insert_id();
		}

		public function update($table, $where, $arFields)
		{
			if(empty($arFields) or !is_array($arFields) or empty($table) or empty($where) )
				return false;

			$strSet = self::getFieldsSet($arFields);	

			$strSql = 'UPDATE '.$table.' SET '.$strSet.' where '.$where;
			return self::query($strSql);
		}
 
		private function getFieldsSet($arFields)
		{
			$strSet = '';	
			$i=0;
			foreach($arFields as $field => $val)
			{
				$i++;
				if($val == 'CURRENT_DATE' or $val == 'now()')
					$strSet .= $field.' = '.$val.(count($arFields) > $i ? ', ' : '');
				else
					$strSet .= $field.' = "'.$this->db->real_escape_string($val).'"'.(count($arFields) > $i ? ', ' : '');
			}

			return $strSet;
		}
	}
?>