<?php
$MESS["AJAX_AGREEMENT_READ_TERMS"] = 'Read the Terms of Use';
$MESS["AJAX_AGREEMENT_AGREE_WITH_TERMS"] = 'I agree with the terms';
$MESS["AJAX_AGREEMENT_AGREE_WITH_PROCESSING_DATA"] = 'I agree to the processing of personal data';
$MESS["AJAX_AGREEMENT_CLOSE_POPUP"] = 'Close';
