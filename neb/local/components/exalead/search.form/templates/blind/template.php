<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<div class="container v-padding">
    <div class="button-bar" role="toolbar">
        <a href="../" class="btn btn-default" tabindex="6">
            Вернуться на главную
        </a>
        <a href="/" class="btn btn-default" tabindex="7" style="float:right;" aria-hidden="true">
            Переход в режим для зрячих
        </a>
    </div>
    <form action="../results/" class="extendedSearch">
        <dl class="form-dl">
            <? foreach ($arResult['searchFields'] as $arItem):
                $bAccess = true;
                if (!in_array('always', $arItem['access']) && !(in_array('simple', $arItem['access'])
                        && ($_REQUEST['searchtype'] == 'simple' || empty($_REQUEST['searchtype'])))
                    && (
                        !empty($_REQUEST['librarytype']) && !in_array($_REQUEST['librarytype'], $arItem['access'])
                        || !empty($_REQUEST['librarytype']) && !in_array($_REQUEST['searchtype'], $arItem['access'])
                        || empty($_REQUEST['librarytype']) && empty($_REQUEST['searchtype'])
                )){
                    $bAccess = false;
                }
                ?>
                <dt class="<?=implode(' ', $arItem['access'])?>"
                    <? if (!$bAccess): ?> style="display: none;"<? endif; ?>
                >
                    <? if ($arItem['type'] == 'text_extended'): ?>
                        <label class="for-lg one-line-height" <? if ($arItem['aria-hidden'] == true):
                            ?> aria-hidden="true"<? endif; ?>>
                            <?=$arItem['label']?><br />

                            <small>(<? $i=0; foreach ($arItem['input'] as $post => $arInput): ?>
                                <label id="<?=$arItem['id'].$post?>label" for="<?=$arItem['id'].$post?>">
                                    <span class="screen-hide"></span><?=$arInput['name']?>
                                </label><?=!$i?',':''?>
                             <? $i++; endforeach; ?>)</small>

                        </label>
                    <? elseif ($arItem['type'] == 'radio'):?>
                        <label><?=$arItem['label']?></label>
                    <? elseif ($arItem['name'] == 'newcollection[]'):?>
                        <label><?=$arItem['label']?></label>
                    <? else: ?>
                        <label class="for-lg"<? if ($arItem['aria-hidden'] == true): ?> aria-hidden="true"<? endif;
                            ?> for="<?=$arItem['id']?>" id="<?=$arItem['id']?>label"><?=$arItem['label']?></label>
                    <? endif; ?>
                </dt>
                <dd <? if (!$bAccess):
                    ?> style="display: none;"<?
                endif; ?>>

                    <? if ($arItem['name'] == 'newcollection[]'): ?>

                        <? foreach ($arItem['value'] as $arCollection): ?>
                            <label for="<?=$arItem['id'] . $arCollection['ID']?>">
                                <input type="checkbox" name="<?=$bAccess?$arItem['name']:''?>" data-name="<?=$arItem['name']?>"
                                    value="<?=$arCollection['NAME']?>"
                                    class="form-control" 
                                    id="<?=$arItem['id'] . $arCollection['ID']?>"
                                    <? if (
                                        empty($_REQUEST['newcollection']) && 'Y' == $arCollection['SELECT']
                                        || is_array($_REQUEST['newcollection']) && in_array($arCollection['NAME'], $_REQUEST['newcollection'])
                                        || $arCollection['NAME'] == $_REQUEST['newcollection']
                                    ): ?> checked="checked"<? endif; ?>
                                    aria-labelledby="<?=$arItem['id'] . $arCollection['ID']?>label"
                                />
                                <span class="lbl" id="<?=$arItem['id'] . $arCollection['ID']?>label"><?echo
                                    LANGUAGE_ID == 'en' ? $arCollection['NAME_EN'] : $arCollection['NAME']?></span>
                            </label>
                        <?  endforeach; ?>

                    <? elseif ($arItem['name'] == 'lang'): ?>

                        <select name="<?=$bAccess?$arItem['name']:''?>" data-name="<?=$arItem['name']?>"
                                class="form-control input-lg" id="<?=$arItem['id']?>"
                                tabindex="<?=$arItem['tabindex']?>" aria-labelledby="<?=$arItem['id']?>label">

                            <option value="">Все</option>
                            <? foreach ($arItem['value'] as $arLang): ?>
                                <option value="<?=$arLang['NAME']?>" <?
                                if ($_REQUEST[$arItem['name']] == $arLang['NAME']): ?> selected="selected"<?
                                endif; ?>><?=$arLang['NAME']?></option>
                            <? endforeach; ?>
                        </select>

                    <? elseif (in_array($arItem['type'], ['tabs', 'select', 'select-checkbox'])): ?>

                        <select name="<?=$bAccess?$arItem['name']:''?>" data-name="<?=$arItem['name']?>"
                                class="form-control input-lg" id="<?=$arItem['id']?>"
                                tabindex="<?=$arItem['tabindex']?>" aria-labelledby="<?=$arItem['id']?>label">
                            
                            <? if ($arItem['type'] == 'select-checkbox'): ?>
                                <option value="">Выберите раздел</option>
                            <? endif; ?>
                            
                            <? foreach ($arItem['value'] as $k => $v): ?>
                                <option value="<?=$k?>" <? if ($_REQUEST[$arItem['name']] == $k):
                                    ?> selected="selected"<? endif; ?>><?=$v?></option>
                            <? endforeach; ?>
                        </select>

                    <? elseif ($arItem['type'] == 'radio'): ?>

                        <? foreach ($arItem['value'] as $k => $v): ?>
                            <label class="for" for="<?=$k?>">
                                <input tabindex="<?=$arItem['tabindex']?>" type="radio" class="form-control"
                                    name="<?=$bAccess?$arItem['name']:''?>" data-name="<?=$arItem['name']?>"
                                    value="<?=$k?>" id="<?=$k?>" aria-labelledby="<?=$k?>label"
                                    <? if ($_REQUEST[$arItem['name']] == $k || empty($_REQUEST[$arItem['name']])
                                        && $v['default'] == true): ?> checked="true"<? endif; ?>
                                />
                                <span class="lbl">
                                    <span <? if ($v['aria-hidden'] == true):
                                        ?> aria-hidden="true"<? endif; ?>><?=$v['name']?></span>
                                    <span class="screen-hide" id="<?=$k?>label"><?=$v['screen']?></span>
                                </span>
                            </label>
                        <? endforeach; ?>

                    <? elseif ($arItem['type'] == 'checkbox'): ?>

                        <input type="checkbox" class="form-control input-lg" id="<?=$arItem['id']?>"
                               name="<?=$bAccess?$arItem['name']:''?>" data-name="<?=$arItem['name']?>"
                               value="<?=$arItem['value']?>"
                               aria-labelledby="<?=$arItem['id']?>label" style="opacity: 1;"
                                <? if ($_REQUEST[$arItem['name']] == 1): ?> checked="true"<? endif; ?>
                        />

                    <? elseif ($arItem['type'] == 'date'): ?>

                        <? foreach ($arItem['value'] as $k => $v): ?>
                            <label class="for-lg" for="<?=$arItem['id'].$k?>" id="<?=$arItem['id'].$k?>label">
                                <span class="screen-hide"><?=$arItem['label']?>, </span><?=$v['name']?><span class="screen-hide">,</span>
                            </label>
                            <input tabindex="<?=$arItem['tabindex']+1?>" type="text" class="form-control input-lg"
                                   name="<?=$bAccess?$arItem['name'].$k:''?>" data-name="<?=$arItem['name'].$k?>"
                                   id="<?=$arItem['id'].$k?>"  aria-labelledby="<?=$arItem['id'].$k?>label"
                                   value="<?=htmlspecialchars($_REQUEST[$arItem['name'].$k]) ? : $v['value']?>"
                                   style="display: inline-block; width: 5em;"
                            />
                        <? endforeach; ?>
                        <span class="instead-input-lg" style="margin-left: 1ex;" aria-hidden="true">года</span>
                    
                    <? elseif ($arItem['type'] == 'text_extended'): ?>

                        <div class="row">
                            <? $i=0; foreach ($arItem['input'] as $post => $arInput): ?>
                                <div class="col-xs-<?=!$i?'3':'9'?>">
                                    <input type="text" class="form-control input-lg" id="<?=$arItem['id'].$post?>"
                                        name="<?=$bAccess?$arInput['name'].$post:''?>" data-name="<?=$arInput['name'].$post?>"
                                        data-placeholder="<?=$arInput['placeholder']?>"
                                        value="<?=htmlspecialchars($_REQUEST[$arInput['name'].$post])?>"
                                        aria-labelledby="<?=$arItem['id'].$post?>label" />
                                </div>
                            <? $i++; endforeach; ?>
                        </div>

                    <? elseif ($arItem['type'] == 'text'): ?>

                        <input type="text" class="form-control input-lg" id="<?=$arItem['id']?>" tabindex="<?=$arItem['tabindex']?>"
                               name="<?=$bAccess?$arItem['name']:''?>" data-name="<?=$arItem['name']?>"
                               value="<?=htmlspecialchars($_REQUEST[$arItem['name']])?>" aria-labelledby="<?=$arItem['id']?>label"
                               data-placeholder="<?=$arItem['placeholder']?>" />

                    <? endif; ?>
                </dd>
            <? endforeach; ?>
        </dl>
        <div class="button-bar" role="toolbar">
            <button class="btn btn-primary" tabindex="4"><?=Loc::getMessage('EXTENDED_SEARCH_FORM_SEARCH')?></button>
            <br />
            <button class="btn btn-default" type="reset" tabindex="5"><?=Loc::getMessage('EXTENDED_SEARCH_FORM_CLEAN')?></button>
        </div>
    </form>
</div>