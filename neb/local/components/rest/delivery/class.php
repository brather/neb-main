<?php

CBitrixComponent::includeComponentClass('rest:rest.server');

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Main\UserTable;

/**
 * Class RestDelivery
 */
class RestDelivery extends RestServer
{
    /**
     * @var array
     */
    protected $_params         = ['token' => null, 'action' => null];
    protected $_arAccessGroups = ['admin'];

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    protected static function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams()
    {
        $arRequest = static::getRequest();

        if (empty($arRequest['token']))
            throw new Exception('Parameter "token" is required!', 403);

        if (empty($arRequest['action']))
            throw new Exception('Parameter "action" is required!', 403);

        $this->_params['token']    = $arRequest['token'];
        $this->_params['action']   = $arRequest['action'];
        $this->_params['form_sid'] = 'SIMPLE_FORM_1';

        return $this;
    }

    /**
     * GET Action
     *
     * @return int
     * @throws Exception
     */
    public function getAction()
    {
        $this->ckeckToken();
        $this->switchAction();
        return 200;
    }

    /**
     * Выбор действия
     *
     * @throws Exception
     */
    private function switchAction() {
        $arRequest = static::getRequest();
        switch ($arRequest['action']) {
            case 'getlist':
                $this->getDelivery();
                break;
            default:
                throw new Exception('Parameter "action" incorrect!', 403);
        }
    }

    /**
     * Проверка актуальности токена пользователя
     *
     * @throws Exception
     */
    private function ckeckToken() {

        $arUser = UserTable::getList([
            'filter' => ['ACTIVE' => 'Y', 'UF_TOKEN' => $this->_params['token']],
            'select' => ['ID']
        ])->fetch();

        if (intval($arUser['ID']) <= 0)
            throw new Exception(GetMessage('USER_NOT_FOUND'), 404);

        $obUser   = new nebUser($arUser['ID']);
        $arGroups = $obUser->getUserGroups();
        $arGroups = array_intersect($arGroups, $this->_arAccessGroups);

        if (empty($arGroups))
            $this->restError(403, 'Role access error!');
    }

    /**
     * Получение списка e-mail адресатов для формы обратной связи
     *
     * @return array
     * @throws Exception
     * @throws \Bitrix\Main\LoaderException
     */
    private function getDelivery() {

        if (!Loader::IncludeModule("form"))
            throw new Exception('Module "form" not installed', 500);

        // получение идентификатора почтового шаблона из формы обратной связи
        $obForm = new CForm();
        $arForm = $obForm->GetBySID($this->_params['form_sid'])->Fetch();

        if (empty($arForm))
            throw new Exception('Feedback form not found!', 500);

        if (empty($arForm['MAIL_EVENT_TYPE']))
            throw new Exception('Feedback event type not found!', 500);

        // получение e-mail адресов из шаблонов писем обратной связи
        $arEmail = $arResult = [];
        $rsEvents = CEventMessage::GetList(
            $by="id",
            $order="asc",
            ["TYPE_ID" => $arForm['MAIL_EVENT_TYPE'], "ACTIVE" => "Y"]
        );
        while ($arEvent = $rsEvents->Fetch()) {
            $arEmail[] = $arEvent['EMAIL_TO'];
            $arEmail[] = $arEvent['BCC'];
        }

        // формирование результирующего массива
        foreach($arEmail as $arItems)
            foreach(explode(',', $arItems) as $sItem)
                if (strpos($sItem, '@') !== false && !in_array(trim($sItem), $arResult))
                    $arResult[] = trim($sItem);

        $this->arResult['delivery'] = $arResult;

        return $arResult;
    }
}