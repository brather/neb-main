<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

$libraryId = intval($arResult['VARIABLES']['ID']);
if ($libraryId < 1) return;

$COLLECTIONS_IBLOCK_ID = Bitrix\NotaExt\Iblock\IblockTools::getIBlockId('kollektsii_6');

?>
<div class="row">
    <div class="col-md-3 col-sm-3 lk-sidebar">
	    <?
		include_once('menu.php');
	    ?>
	    <?
	    $APPLICATION->IncludeComponent(
	        "neb:library.right_counter",
	        "",
	        Array(
	            "IBLOCK_ID" => $arParams['IBLOCK_ID'],
	            "LIBRARY_ID" => $libraryId,
	            "CACHE_TIME" => $arParams["CACHE_TIME"],
	            "CLASS" => 'nomargin',
	        ),
	        $component
	    );
	    ?>
    </div>
    <div class="col-md-9 col-sm-9 lk-content">
	    <?/*
		$APPLICATION->IncludeComponent(
			"collection:main.slider", 
			"collections", 
			array(
				"PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['seaction'],
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"IBLOCK_ID" => $COLLECTIONS_IBLOCK_ID,
				"LIBRARY_ID" => $libraryId
			),
			$component
		);
		*/?>

		<!-- Сортировка не нужна -->
		<?
		$APPLICATION->IncludeComponent(
			"neb:collections.list", 
			"library", 
			array(
				"PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['seaction'],
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"IBLOCK_ID" => $COLLECTIONS_IBLOCK_ID,
				"LIBRARY_ID" => $libraryId,
				'TITLE' => 'N',
			),
			$component
		);
		?>
	</div>
</div>
