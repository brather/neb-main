<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arCategory = array_pop($arResult['CATEGORY']);

//$APPLICATION->SetPageProperty('category_section', $arCategory);
$APPLICATION->SetPageProperty('hide-title', true);
$APPLICATION->SetTitle($arCategory['bbk_name']);

?>
<main>
    <section class="category container">

        <? if(!empty($arResult['CATEGORY'])):?>
            <h1><?= $arCategory['bbk_name'] ?></h1>
        <? else: ?>
            <h1>Все рубрики</h1>
        <? endif; ?>

        <? if(!empty($arResult['CATEGORY'])):?>
            <header>
                <ul class="category-breadcrumbs">
                    <li class="category-breadcrumbs__item">
                        <a href="/category/">Все рубрики</a>
                    </li>
                    <? foreach ($arResult['CATEGORY'] as $key => $arItem):?>
                        <li class="category-breadcrumbs__item">
                            <a href="/category/?category_id=<?= $arItem['bbk_fullid'];
                                ?>&category_name=<?=$arItem['bbk_fullname'];?>"><?=$arItem['bbk_name']?></a>
                        </li>
                    <? endforeach;?>
                </ul>
            </header>
        <? endif; ?>

        <main class="row">
            <div class="col-md-3">
                <h2 data-target-toggle-class=".lk-sidebar__menu-adaptive">Подрубрики</h2>
                <br />
                <div class="lk-sidebar__menu">
                    <div class="lk-sidebar__menu-adaptive">
                        <? foreach ($arResult['res'] as $key => $arItem):?>
                            <a class="lk-sidebar__menu-link" href="/category/?category_id=<?=
                                $arCategory['bbk_fullid'] . ($arCategory['bbk_fullid'] ? '/' : '') . $arItem['bbk_id'];
                                ?>&category_name=<?=$arItem['bbk_fullname']?>"><?=$arItem['bbk_name']?></a>
                        <? endforeach;?>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <h2>Популярные издания рубрики</h2>
                <br />
                <ul class="search-result__content-list clearfix">
                    <? foreach($arResult['books']['ITEMS'] as $index => $arItem): ?>
                        <? $APPLICATION->IncludeFile(
                            '/local/components/exalead/search.page/templates/.default/search_row.php',
                            array('arParams' => $arParams,'arResult' => $arResult, 'index' => $index,'arItem' => $arItem)
                        ); ?>
                    <? endforeach; ?>
                </ul>
            </div>
        </main>

    </section>

    <?= !empty($arResult['STR_NAV']) ? htmlspecialcharsBack($arResult['STR_NAV']) : ''; ?>
</main>
