<?php
$MESS["T_LIB_PLAN_ADD_EDITION_TITLE"] = "Добавление изданий в план оцифровки";
$MESS["T_LIB_PLAN_ADD_EDITION_DESCR"] = "Выберите способ загрузки издания: ";
$MESS["T_LIB_PLAN_ADD_EDITION_MODE_EXALEAD"] = "Из каталога НЭБ";
$MESS["T_LIB_PLAN_ADD_EDITION_MODE_EXCEL"] = "Импорт плана оцифровки (только xlsx)";
$MESS["T_LIB_PLAN_ADD_EDITION_MODE_FROM_USER"] = "По заявкам от читателей";
$MESS["T_LIB_PLAN_ADD_EDITION_MODE_CHOISE_FILE"] = "Выберите файл";
$MESS["T_LIB_PLAN_ADD_EDITION_MODE_CHOISE_FILE_PDF"] = "Выберите файл pdf";
$MESS["T_LIB_PLAN_ADD_EDITION_MODE_FILE_EMPTY"] = "Файл не выбран";
$MESS["T_LIB_PLAN_ADD_EDITION_MODE_BOOKS"] = "Добавить издания";
$MESS["T_LIB_PLAN_ADD_EDITION_MODE_BOOK"] = "Добавить книгу";

$MESS["T_LIB_TABLE_EDITION_TITLE"] = "Название";
$MESS["T_LIB_TABLE_EDITION_AUTHOR"] = "Автор";
$MESS["T_LIB_TABLE_EDITION_YEAR"] = "Год издания";
$MESS["T_LIB_TABLE_EDITION_DATE_CREATE"] = "Дата заявки";
$MESS["T_LIB_TABLE_EDITION_DATE_F_PLAN"] = "Дата оцифровки";
$MESS["T_LIB_TABLE_EDITION_STATUS"] = "Статус";
$MESS["T_LIB_TABLE_EDITION_DELETE"] = "Удалить";
$MESS["T_LIB_TABLE_EDITION_TITLE_D"] = "Заглавие: ";
$MESS["T_LIB_TABLE_EDITION_AUTHOR_D"] = "Автор: ";
$MESS["T_LIB_TABLE_EDITION_YEAR_D"] = "Год издания: ";
$MESS["T_LIB_TABLE_EDITION_DATE_CREATE_D"] = "Дата заявки: ";
$MESS["T_LIB_TABLE_EDITION_DESC_D"] = "Описание: ";
$MESS["T_LIB_TABLE_EDITION_LIBRARY_D"] = "Бибилиотека: ";
$MESS["T_LIB_TABLE_EDITION_STATUS_D"] = "Статус: ";
$MESS["T_LIB_TABLE_EDITION_WHO_DIGITIZATION_D"] = "Оцифровывает: ";
$MESS["T_LIB_TABLE_EDITION_DESCRIPTION_TITLE"] = "Дополнительное информация ";
$MESS["T_LIB_TABLE_EDITION_COMMENT_D"] = "Комментарий: ";

$MESS["T_LIB_TO"] = "до: ";
$MESS["T_LIB_FROM"] = "с: ";
$MESS["T_LIB_POSTFIX_YEAR"] = " г.";

$MESS["T_LIB_CONFIRM_DELETE_EDITION_TITLE"] = "Удалить?";
$MESS["T_LIB_CONFIRM_DELETE_EDITION_CONFIRM_TITLE"] = "Удалить";
$MESS["T_LIB_CONFIRM_DELETE_EDITION_CONFIRM_TEXT"] = "Вы действительно хотите удалить данное издание?";

$MESS["T_LIB_FILTER_TITLE"] = "Фильтр списка изданий плана";
$MESS["T_LIB_FILTER_COUNT_BOOKS"] = "Книг в плане на оцифровку: ";
$MESS["T_LIB_FILTER_PERIOD_STATISTICS"] = "Статистика за период: ";
$MESS["T_LIB_FILTER_USER_ADD"] = "Добавление издания произведено: ";
$MESS["T_LIB_FILTER_USER_ADD_USER"] = "читателем";
$MESS["T_LIB_FILTER_USER_ADD_ADMIN"] = "администратором";
$MESS["T_LIB_FILTER_USER_ADD_ALL"] = "не важно";
$MESS["T_LIB_FILTER_BTN_SUBMIT"] = "Применить";
$MESS["T_LIB_FILTER_BTN_RESET"] = "Сбросить";

$MESS['LIBRARY_PLAN_LOAD_ERROR_TRY'] = "Ошибка загрузки, повторите еще раз";
$MESS['LIBRARY_PLAN_LOAD_SUCCESS'] = "Успешно загружено ";
$MESS['LIBRARY_PLAN_LOAD_RECORDS'] = " записей";
$MESS['LIBRARY_PLAN_ERRORS'] = "Ошибок ";
$MESS['LIBRARY_PLAN_ALREADY_INCLUDED'] = "Уже внесены в план оцифровки другой библиотеки ";
$MESS['LIBRARY_PLAN_NO_PUBLICATION_NEL'] = " изданий нет в фонде НЭБ";
$MESS['LIBRARY_PLAN_EDITION_DATE_CREATE'] = "Дата создания";

$MESS['LIBRARY_PLAN_DIGITAL_PRINT'] = "Печать";
$MESS['LIBRARY_PLAN_DIGITAL_EXPORT_EXCEL'] = "Экспорт в Excel";
$MESS['T_LIB_PLAN_ADD_EDITION_MODE_PROTOCOL_EXPORT'] = "Экспорт протокола загрузки";
$MESS['LIBRARY_PLAN_UPLOAD_PDF'] = " Загрузить";
$MESS['T_LIB_EMPTY_PDF_FILE'] = "На данный момент не загружено ни одного файла";