<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arReturn = $APPLICATION->IncludeComponent(
    "neb:markup.logs",
    "",
    array(
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => $arParams['SEF_FOLDER'],
        "SEF_URL_TEMPLATES" => array(
            "template" => "markup/",
            "detail"   => "markup/#ID#/",
        )
    ),
    $component
);