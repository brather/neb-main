<?php
/**
 * Refactoring by D-Efremov
 * Date: 27.10.2016
 * Time: 15:00
 */

session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . '/local/tools/esia/psaml/_toolkit_loader.php';
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Loader;
use \Bitrix\Main\UserTable;

/**
 * Class EsiaWebService - авторизация по протоколу ЕСИА через ГосУслуги
 */
class EsiaWebService {

	private $auth;

	public function __construct()
	{
		if (!Loader::includeModule('socialservices')) {
			SocialServicesAuth::JSRedirect(); // просто закрытие окна
		}

		global $settingsInfo;
		$this->auth = new OneLogin_Saml2_Auth($settingsInfo);
	}

	/**
	 * Роутинг действий
	 */
	public function main() {
		switch ($_GET['act']) {
			case 'do_login':  $this->doLigin();   break;
			case 'login':     $this->login();     break;
			case 'do_logout': $this->do_logout(); break;
			case 'logout':    $this->logout();    break;
		}
	}

	/**
	 * Авторизоваться
	 */
	private function doLigin() {
		//$this->auth->login('https://demo.neb.elar.ru');
		$this->auth->login();
	}

	/**
	 * Ответ с данными об авторизации
	 */
	private function login() {

		$obUser = new \CUser();

		// если уже авторизованы в другой вкладке, закрываем окошко и обновляем страничку
		if ($obUser->IsAuthorized())
		{
			SocialServicesAuth::JSRedirect('/');
		}
		// логин или создание аккаунта в битрикс
		else
		{
			$this->auth->processResponse();
			$errors = $this->auth->getErrors();

			//есть ошибки?
			if (!empty($errors)) {
				//print_r('<p>'.implode(', ', $errors).'</p>');
				file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/WebServices/log.txt', print_r($errors, true), FILE_APPEND);
			}

			$_SESSION['samlUserdata']     = $this->auth->getAttributes();
			$_SESSION['samlNameId']       = $this->auth->getNameId();
			$_SESSION['samlSessionIndex'] = $this->auth->getSessionIndex();

			if ($this->auth->isAuthenticated()) {

				$attributes = $_SESSION['samlUserdata'];

				$passportSeries = '';
				$passportNumber = '';
				if (isset($attributes['urn:esia:principalDocuments'][0])) {
					$principalDocuments = new SimpleXMLElement($attributes['urn:esia:principalDocuments'][0]);
					foreach ($principalDocuments->document as $document) {
						if ('01' === (string)$document->documentType) {
							$passportSeries = strval($document->series);
							$passportNumber = strval($document->number);
						}
						if (($document->documentType == '01') || ($document->documentType == '02')) {
							$issuedBy = $document->issuedBy;
							$posdate = strpos($document->issueDate, 'T');
							$issueDate = substr($document->issueDate, 0, $posdate);
						}
					}
				}

				$arFields = array(
					'EXTERNAL_AUTH_ID'   => 'Esia',
					'XML_ID'             => $attributes['urn:mace:dir:attribute:userId'][0],
					'LOGIN'              => $attributes['urn:esia:personEMail'][0],
					'EMAIL'              => $attributes['urn:esia:personEMail'][0],
					'NAME'               => $attributes['urn:mace:dir:attribute:firstName'][0],
					'LAST_NAME'          => $attributes['urn:mace:dir:attribute:lastName'][0],
					'SECOND_NAME'        => $attributes['urn:mace:dir:attribute:middleName'][0],
					'PERSONAL_BIRTHDAY'  => date('d.m.Y', strtotime($attributes['urn:esia:birthDate'][0])),
					'PERSONAL_GENDER'    => $attributes['urn:esia:gender'][0] == 'FEMALE' ? 'F' : 'M',
					//'PERSONAL_PHONE'     => $attributes['urn:esia:personMobilePhone'][0],
					//'PERSONAL_MOBILE'    => $attributes['urn:esia:personMobilePhone'][0],
					'UF_REGISTER_TYPE'   => '40',
					'UF_STATUS'          => '42',
					//'UF_PASSPORT_SERIES' => $passportSeries,
					//'UF_PASSPORT_NUMBER' => $passportNumber,
					//'UF_ISSUED'          => 'Выдан: '.$issuedBy.', дата выдачи: '.$issueDate
				);
				$uf_esia_fields = '';
				foreach ($arFields as $key=>$field)
					if (!empty($field))
						$uf_esia_fields .= $key.'/';

				$arFields['UF_ESIA_FIELDS'] = $uf_esia_fields;

				// защитное требование
				if (empty($arFields['EXTERNAL_AUTH_ID']) && empty($arFields['XML_ID'])) {
					SocialServicesAuth::JSRedirect(); // просто закрытие окна
				}

				//debugLog($arFields, '/WebServices/log.txt');
//$arFields['EMAIL'] = '';
				// формирование фильтра по параметрам социалки или e-mail пользователя
				$arFilter = [];
				$arFilter['LOGIC'] = 'OR';
				$arFilter[] = ['=EXTERNAL_AUTH_ID' => $arFields['EXTERNAL_AUTH_ID'], '=XML_ID' => $arFields['XML_ID']];
				if (!empty($arFields['EMAIL']))
					$arFilter[] = ['EMAIL' => $arFields['EMAIL']];

				// выборка пользователя из БД, если был уже зарегистрирован
				$arUser = UserTable::getList([
					'filter' => [$arFilter],
					'select' => ['ID', 'LOGIN', 'EMAIL', 'ACTIVE', 'NAME', 'LAST_NAME', 'CONFIRM_CODE'],
				])->fetch();

				// пользователь есть в БД
				if ($arUser['ID']) {
					// пользователь зарегистрирован и активен
					if ($arUser['ACTIVE'] == 'Y') {
						$obUser->Authorize($arUser['ID']);
						SocialServicesAuth::JSRedirect('/');
					}
					// пользователь зарегистрирован и не подтвердил e-mail
					elseif (!empty($arUser['CONFIRM_CODE'])) {
						$obEvent = new CEvent;
						$obEvent->SendImmediate('NEW_USER_CONFIRM', SITE_ID, $arUser);
						SocialServicesAuth::JSRedirect('/auth/reg.php?success=Y');
					}
					// закроем окно
					else {
						SocialServicesAuth::JSRedirect();
					}
				}
				// новый пользователь
				else {
					// в случае отсутствия email перенаправляем на страницу для его заполнения
					if (empty($arFields['EMAIL'])) {
						$_SESSION['ESIA_REGISTER'] = $arFields;
						SocialServicesAuth::JSRedirect('/auth/reg.php');
					}
					// зарегистировать и авторизовать
					else {
						$arFields['ID'] = $obUser->Add($arFields);
						if ($arFields['ID'] > 0) {
							//$nebUser->setStatus(nebUser::USER_STATUS_VERIFIED);
							$obUser->SendUserInfo($arFields['ID'], SITE_ID, 'Приветствуем Вас как нового пользователя НЭБ!');
							$obUser->Authorize($arFields['ID']);

							SocialServicesAuth::JSRedirect('/profile/');
						} else{
							die($obUser->LAST_ERROR);
						}
					}
				}
			}
		}
	}

	/**
	 * вызыв логаута
	 */
	private function do_logout() {

		$returnTo = null;
		$params = array();
		$nameId = null;
		$sessionIndex = null;
		if (isset($_SESSION['samlNameId'])) {
			$nameId = $_SESSION['samlNameId'];
		}
		if (isset($_SESSION['samlSessionIndex'])) {
			$sessionIndex = $_SESSION['samlSessionIndex'];
		}

		$this->auth->logout($returnTo, $params, $nameId, $sessionIndex);
	}

	private function logout() {
		/*echo $this->auth->getSSOurl();
        print_r($this->auth->getAttributes());*/
	}
}

$obEsiaWebService = new EsiaWebService();
$obEsiaWebService->main();