<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

$context = Application::getInstance()->getContext();
$arServer = $context->getServer()->toArray();
$arRequest = $context->getRequest()->toArray();

$arPlan = $arResult['ITEMS_PLAN'][0];

CJSCore::Init(array('date'));

?>

<hr />

<div>
    <dd>
        <?// Блок с данными плана ?>
        <div class="container-fluid">
            <div class="row">
                <hr>
            </div>
        </div>
        <div class="container-fluid">
            <div class="col-xs-6">
                <dl>
                    <dt><?= Loc::getMessage("T_PLAN_LIST_DATE_CREATE") ?></dt>
                    <dd><?= ConvertDateTime($arPlan["UF_DATE_CREATE"], "DD.MM.YYYY HH:MI", SITE_ID) ?></dd>
                    <? if ($arPlan["UF_REQUEST_STATUS_SORT"] >= $arResult["LIST_STATUS"]["AGREED"]["SORT"]): ?>
                        <dt><?= Loc::getMessage("T_PLAN_LIST_DATE_AGREEMENT") ?></dt>
                        <dd><?= ConvertDateTime($arPlan["UF_DATE_AGREEMENT"], "DD.MM.YYYY", SITE_ID) ?></dd>
                    <? elseif ($arPlan["UF_REQUEST_STATUS_SORT"] >= $arResult["LIST_STATUS"]["DONE"]["SORT"]): ?>
                        <dt><?= Loc::getMessage("T_PLAN_LIST_DONE") ?></dt>
                        <dd><?= ConvertDateTime($arPlan["UF_DATE_F_ACTUAL"], "DD.MM.YYYY", SITE_ID) ?></dd>
                    <? endif; ?>
                </dl>
            </div>
            <div class="col-xs-6">
                <form action="<?= $arParams["SEF_FOLDER"] ?>" method="post"
                      data-ajax="/local/tools/plan_digitization/plan.update.ajax.php">
                    <input type="hidden" name="plan" value="<?= $arPlan["UF_PLAN_NAME"] ?>" />
                    <input type="hidden" name="<?= $preffixPlan ?>DATA_EDIT_AJAX" value="Y" />
                    <div class="form-group">
                        <div class="row">
                            <label for="<?= $preffixPlan ?>DATA_EDIT_UF_DATE_F_PLAN" class="control-label col-xs-12">
                                <?= Loc::getMessage("T_PLAN_LIST_TITLE_DATE_F_PLAN") ?>
                            </label>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <? $clsDisabled = '';
                                    if ($arPlan['UF_REQUEST_STATUS_SORT'] >= $arResult['LIST_STATUS']['AGREED']['SORT']) {
                                        $clsDisabled = ' disabled';
                                    } ?>
                                    <input type="text"
                                           name="<?= $preffixPlan ?>DATA_EDIT_UF_DATE_F_PLAN"
                                           class="form-control"
                                           id="<?= $preffixPlan ?>DATA_EDIT_UF_DATE_F_PLAN"
                                           value="<?= ConvertDateTime($arPlan["UF_DATE_F_PLAN"], "DD.MM.YYYY", SITE_ID) ?>"
                                           data-masked="99.99.9999"<?= $clsDisabled ?>/>
                                                <span class="input-group-btn">
                                                    <a class="btn btn-default<?= $clsDisabled ?>"
                                                       id="<?= $preffixPlan ?>CALENDAR_DATE_F_PLAN"
                                                       onclick="BX.calendar({node: '<?= $preffixPlan ?>DATA_EDIT_UF_DATE_F_PLAN', field: '<?= $preffixPlan ?>DATA_EDIT_UF_DATE_F_PLAN',  form: '', bTime: false, value: ''});">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </a>
                                                </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="container-fluid">
                                <div class="btn-group">

                                    <? if ($arPlan['UF_REQUEST_STATUS'] == $arResult['LIST_STATUS']['AGREED']['ID']): ?>
                                        <button
                                                class="btn btn-primary js-plan-change"
                                                type="submit"
                                                data-preffix="<?= $preffixPlan ?>"
                                                data-lang-date-f-plan="<?= Loc::getMessage("T_PLAN_LIST_TITLE_DATE_F_PLAN"); ?>"
                                                data-plan-year="<?= ConvertDateTime($arPlan["UF_DATE_F_PLAN"], "YYYY", SITE_ID) ?>"
                                                data-plan-id="<?= $arPlan["ID"] ?>"
                                                data-plan-library="<?= $arPlan["UF_LIBRARY"] ?>"
                                                data-plan-name="<?= $arPlan["UF_PLAN_NAME"] ?>"
                                                data-plan-status="<?= $arResult['LIST_STATUS']['DIGITIZED']['ID'] ?>">
                                            Выполнено
                                        </button>
                                    <? elseif ($arPlan["UF_REQUEST_STATUS_SORT"] < $arResult["LIST_STATUS"]["AGREED"]["SORT"]): ?>
                                        <button
                                                class="btn js-plan-change"
                                                type="submit"
                                            <? if ($arPlan['UF_REQUEST_STATUS']): ?>
                                                disabled="disabled"
                                            <? endif; ?>
                                                data-preffix="<?= $preffixPlan ?>"
                                                data-lang-date-f-plan="<?= Loc::getMessage("T_PLAN_LIST_TITLE_DATE_F_PLAN"); ?>"
                                                data-plan-year="<?= ConvertDateTime($arPlan["UF_DATE_F_PLAN"], "YYYY", SITE_ID) ?>"
                                                data-plan-id="<?= $arPlan["ID"] ?>"
                                                data-plan-library="<?= $arPlan["UF_LIBRARY"] ?>"
                                                data-plan-name="<?= $arPlan["UF_PLAN_NAME"] ?>"
                                                data-plan-status="0">
                                            <?= Loc::getMessage("T_PLAN_LIST_CHANGE") ?>
                                        </button>
                                        <button
                                                class="btn btn-primary js-plan-change"
                                                type="submit"
                                            <? if ($arPlan['UF_REQUEST_STATUS']): ?>
                                                disabled="disabled"
                                            <? endif; ?>
                                                data-preffix="<?= $preffixPlan ?>"
                                                data-lang-date-f-plan="<?= Loc::getMessage("T_PLAN_LIST_TITLE_DATE_F_PLAN"); ?>"
                                                data-plan-year="<?= ConvertDateTime($arPlan["UF_DATE_F_PLAN"], "YYYY", SITE_ID) ?>"
                                                data-plan-id="<?= $arPlan["ID"] ?>"
                                                data-plan-library="<?= $arPlan["UF_LIBRARY"] ?>"
                                                data-plan-name="<?= $arPlan["UF_PLAN_NAME"] ?>"
                                                data-plan-status="<?= $arResult['LIST_STATUS']['IN_AGREEING_NEL_OPERATOR']['ID'] ?>">
                                            На согласование
                                        </button>
                                    <? endif; ?>

                                    <button class="btn btn-default js-plan-delete"
                                            data-plan-id="<?= $arPlan["ID"] ?>"
                                            data-mess-confirm="<?= Loc::getMessage("T_PLAN_LIST_DELETE_CONFIRM") ?>"
                                            type="submit">
                                        <?= Loc::getMessage("T_PLAN_LIST_DELETE") ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <hr>
            </div>
        </div>
        <? // Блок с просмотром, добавлением и редактированием изданий плана ?>
        <div class="container-fluid">
            <div class="row">
                <? $addEdition = "Y";
                if ($arPlan["UF_REQUEST_STATUS"] >= $arResult["LIST_STATUS"]["AGREED"]["ID"]) {
                    $addEdition = "N";
                }
                $APPLICATION->IncludeComponent(
                    "neb:library.plan_digital",
                    "libadmin",
                    //"",
                    Array(
                        "ADD_EDITION" => $addEdition,
                        "DATE_FROM" => $arRequest['from'],
                        "DATE_TO" => $arRequest['to'],
                        "LIBRARY_ID" => $arResult["UF_LIBRARY"],
                        "DETAIL_PAGE_URL" => $arParams['DETAIL_PAGE_URL'],
                        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
                        "ADD_PAGE_URL" => $arParams["ADD_PAGE_URL"],
                        "ADD_PAGE_URL_FROM_READER" => $arParams["ADD_PAGE_URL_FROM_READER"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "DATA_PLAN" => array(
                            "ID" => $arPlan["ID"],
                            "NAME" => $arPlan["UF_PLAN_NAME"],
                            "STATUS" => $arPlan["UF_REQUEST_STATUS"],
                            "LIST_STATUS_PLAN" => $arResult["LIST_STATUS"],
                            "DATA_F_PLAN" => $arPlan["UF_DATE_F_PLAN"]
                        )
                    ),
                    false
                ); ?>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <hr>
            </div>
        </div>
    </dd>
</div>