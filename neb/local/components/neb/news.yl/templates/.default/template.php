<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<?php if('ru' == LANGUAGE_ID):?>
    <section class="main-news container">
        <div class="row">
            <div class="main-news__title clearfix">
                <div class="main-news__title-icon col-md-1">
                    <img src="/local/templates/adaptive/img/news-lg.png" alt="" />
                </div>
                <h3 class="col-md-11 main-news__title-header">
                    <a href="/year_literature/"><?=GetMessage("LITGOD_BLOCK_TITLE")?></a>
                </h3>
            </div>
            <div class="main-news__gallery main-news__gallery--year">
                <? foreach ($arResult['item'] as $k => $arItem): ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 main-news-item">
                        <a href="<?= $arItem['link']; ?>"
                           class="main-news__gallery-link main-news__gallery-link--year" title="<?= $arItem['title']; ?>"<?
                        if (!empty($arItem['enclosure']['local'])) { ?> style="background-image: url('<?= $arItem['enclosure']['local'] ?>')"<? } ?>>
                            <img src="<?= $arItem['enclosure']['local'] ?>" alt="<?= $arItem['title']; ?>" class="main-news__gallery-image" /><br />
                        </a>
                        <a href="<?= $arItem['link']; ?>"><?= $arItem['title']; ?></a>
                    </div>
                <? endforeach ?>
            </div>
        </div>
    </section>
<?php endif;?>