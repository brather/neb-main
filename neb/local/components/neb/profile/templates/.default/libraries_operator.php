<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Библиотеки участники НЭБ');
/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
require(dirname(__FILE__) . '/includes/header.php');
$APPLICATION->IncludeComponent(
    'neb:library.party',
    '',
    [
        'LIBRARY_NAME' => $_REQUEST['LIBRARY_NAME'],
    ],
    $component
);
require(dirname(__FILE__) . '/includes/footer.php');
