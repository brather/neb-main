<?php
/** @var \Symfony\Bundle\FrameworkBundle\Templating\PhpEngine $view */
/** @var \BitrixBundle\DependencyInjection\BitrixViewHelper $bitrixViewHelper */
/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$bitrixViewHelper = $view['bitrix_view_helper'];
$slots = $view['slots'];
$bitrixViewHelper->addJs('/local/templates/adaptive/vendor/mustache.min.js');
$bitrixViewHelper->addJs('/local/templates/adaptive/js/bbk.js');
$bitrixViewHelper->addCss('/local/templates/adaptive/css/bbk.css');
$bitrixViewHelper->showHeader();
$slots->output('body');
$slots->output('javascripts');
$bitrixViewHelper->showFooter();