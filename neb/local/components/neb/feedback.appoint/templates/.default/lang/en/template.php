<?php
$MESS['FEEDBACK_APPOINT_TITLE']  = 'Assigning user to the order №';
$MESS['FEEDBACK_APPOINT_SELECT'] = '-- Select the library -- ';
$MESS['FEEDBACK_APPOINT_NAME']   = 'Full name';
$MESS['FEEDBACK_APPOINT_EMAIL']  = 'E-mail';
$MESS['FEEDBACK_APPOINT_PHONE']  = 'Phone';