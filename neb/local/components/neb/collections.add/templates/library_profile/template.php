<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Page\Asset,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

Asset::getInstance()->addJs('/local/tools/ckeditor/ckeditor.js');
Loc::loadMessages(__FILE__);

$edit = isset($arResult['COLLECTION']);
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

$depthLevel = $arResult['COLLECTION']['DEPTH_LEVEL'];
?>
<div class="b-collectionformpage">
    <!--<?=$edit ? Loc::getMessage('COLLECTION_DETAIL_EDITING') : Loc::getMessage('COLLECTION_DETAIL_CREATING');?>-->
    <form action="<?=$APPLICATION->GetCurPage()?>"
          method="POST"
          class="b-form b-form_common b-collectform naf"
          id="collection-edit">
        <?if($arResult['COLLECTION']):?>
            <input type="hidden" name="action" value="editCollection"/>
            <input type="hidden" name="id" value="<?= $arResult['COLLECTION']['ID'] ?>"/>
            <input type="hidden" name="code" value="<?= $arResult['COLLECTION']['CODE'] ?>"/>
        <?else:?>
            <input type="hidden" name="action" value="addCollection"/>
        <?endif;?>
        <?=bitrix_sessid_post()?>
        <div class="form-group">
            <div class="row">
                <label for="inputNAME" class="col-xs-4">
                    <div class="container-fluid">
                        <em class="hint">*</em><?= Loc::getMessage('COLLECTION_DETAIL_COLLECTION_NAME') ?>
                    </div>
                </label>
                <div class="col-xs-8">
                    <input type="text"
                           data-required="required"
                           value="<?= $arResult['COLLECTION']['NAME'] ?>"
                           id="inputNAME"
                           data-maxlength="225"
                           data-minlength="2"
                           name="name"
                           class="form-control" >
                </div>
            </div>
        </div>
        <?$checkActive = '';
        if($arResult["COLLECTION"]["ACTIVE"] == "Y") {
            $checkActive = ' checked';
        }
        $checkShowInSlider = '';
        if($arResult["COLLECTION"]["UF_SLIDER"]) {
            $checkShowInSlider = ' checked';
        }
        $checkShowTitleInSlider = '';
        if($arResult["COLLECTION"]["UF_SHOW_NAME"]) {
            $checkShowTitleInSlider = ' checked';
        }?>
        <div class="form-group">
            <div class="row">
                <div class="col-xs-4">
                    <label class="pointer naf-label">
                        <input type="checkbox" name="is_active"<?= $checkActive ?>>
                        <span class="lbl"><?= Loc::getMessage("COLLECTION_DETAIL_ACTIVE") ?></span>
                    </label>
                </div>
            </div>
        </div>
        <?$hideSlider = '';
        if(!empty($arResult['COLLECTION']['IBLOCK_SECTION_ID']) || !empty($arRequest['PARENT_SECT_ID'])){
            $hideSlider = ' style="display: none;"';
        }?>
        <div class="form-group js-slider-show"<?= $hideSlider ?>>
            <div class="row">
                <div class="col-xs-4">
                    <label class="pointer naf-label">
                        <input type="checkbox" name="is_show_in_slider"<?= $checkShowInSlider ?>>
                        <span class="lbl"><?= Loc::getMessage("COLLECTION_DETAIL_SHOW_IN_SLIDER") ?></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group js-slider-show"<?= $hideSlider ?>>
            <div class="row">
                <div class="col-xs-4">
                    <label class="pointer naf-label">
                        <input type="checkbox" name="is_show_name"<?= $checkShowTitleInSlider ?>>
                        <span class="lbl"><?= Loc::getMessage("COLLECTION_DETAIL_SHOW_TITLE_IN_SLIDER") ?></span>
                    </label>
                </div>
            </div>
        </div>
        <?if(count($arResult['SUB_SECTIONS']) > 0){?>
            <hr>
            <div class="panel-group subsections" id="collection">
                <div class="panl panel-info">
                    <div class="panel-heading">
                        <h4>
                            <a data-toggle="collapse" data-parent="#accordion" href="#subcollection-<?= $arResult['COLLECTION']['ID'] ?>">
                                <?= Loc::getMessage("COLLECTION_SUB_SECTION_LIST"); ?> <i class="fa fa-chevron-down"></i>
                            </a>
                        </h4>
                    </div>
                    <div class="panel-collapse collapse" id="subcollection-<?= $arResult['COLLECTION']['ID'] ?>">
                        <div class="panel-body">
                            <?$APPLICATION->IncludeComponent(
                                "neb:collections.list",
                                "library_profile",
                                array(
                                    "PAGE_URL" => $arParams['PAGE_URL'],
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "3600",
                                    "IBLOCK_ID" => IBLOCK_ID_COLLECTION,
                                    "LIBRARY_ID" => $arParams['LIBRARY_ID'],
                                    "SORT" => 'created',
                                    "ORDER" => 'DESC',
                                    "PARENT_COLLECTION" => $arResult['COLLECTION']['ID']
                                ),
                                $component
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        <?}
        else {?>
            <div class="form-group">
                <div class="row">
                    <label for="parent_section" class="col-xs-4">
                        <?= Loc::getMessage('COLLECTION_PARENT_SECTION'); ?>
                    </label>
                    <div class="col-xs-8">
                        <select name="parent_section" id="parentSection" class="form-control">
                            <option value="-">-</option>
                            <?foreach($arResult['PARENT_SECTIONS'] as $iSection => $arSection){?>
                                <?$selected = '';
                                if($arSection['ID'] == $arResult['COLLECTION']['IBLOCK_SECTION_ID']
                                    || $arRequest['PARENT_SECT_ID'] == $arSection['ID']){
                                    $selected = ' selected';
                                }?>
                                <option value="<?= $arSection['ID'] ?>"<?= $selected ?>><?= $arSection['NAME'] ?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>
        <?}?>
        <?$APPLICATION->IncludeComponent(
            "notaext:plupload",
            "library_collection_add",
            array(
                "MAX_FILE_SIZE" => "24",
                "FILE_TYPES" => "jpg,jpeg,png",
                "DIR" => "tmp_collection",
                "FILES_FIELD_NAME" => "profile_file",
                "MULTI_SELECTION" => "N",
                "CLEANUP_DIR" => "Y",
                "UPLOAD_AUTO_START" => "Y",
                "RESIZE_IMAGES" => "Y",
                "RESIZE_WIDTH" => "4000",
                "RESIZE_HEIGHT" => "4000",
                "RESIZE_CROP" => "Y",
                "RESIZE_QUALITY" => "98",
                "UNIQUE_NAMES" => "Y",
                "FILE_PATH" => CFile::GetPath($arResult['COLLECTION']['DETAIL_PICTURE'])
            ),
            false
        );?>
        <div class="form-group">
            <div class="row"><div class="col-xs-12"><hr></div></div>
            <div class="row">
                <div class="col-xs-12">
                    <label class="pointer naf-label">
                        <input type="checkbox" name="delete_image" data-confirm="<?= Loc::getMessage("COLLECTION_DELETE_IMAGE_SECCTION_CONFIRM") ?>">
                        <span class="lbl"><?= Loc::getMessage("COLLECTION_DELETE_IMAGE_SECCTION") ?></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label for="iDETAIL_TEXT" class="col-xs-12"><?= Loc::getMessage('COLLECTION_DETAIL_DESCRIPTION') ?></label>
                <div class="col-xs-12">
                    <textarea
                        data-required="required"
                        data-maxlength="1500"
                        data-minlength="2"
                        name="description"
                        class="form-control"
                        id="iDETAIL_TEXT"
                    >
						<?=$arResult['COLLECTION']['DESCRIPTION']?>
					</textarea>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row" style="margin-top: 2em;">
            <div class="col-xs-3">
                <button class="btn btn-primary btn-lg" value="1" type="submit" data-put-editor-data data-onclick="putEditorData(); return true;"><?=Loc::getMessage('COLLECTION_DETAIL_SAVE');?></button>
            </div>
            <div class="col-xs-3 col-xs-offset-6">
                <a href="/profile/collection/" class="btn btn-default btn-lg pull-right">Отменить</a>
            </div>
        </div>
    </form>
    <p style="margin-top: 2em;"><?=Loc::getMessage('COLLECTION_DETAIL_REQ');?></p>
</div><!-- /.b-newsform-->

</div><!-- /.b-mainblock -->
<?/*
<div class="b-side right b-sidesmall">
    <a href="#" class="b-btlibpage"><?=Loc::getMessage('PROFILE_ML_PAGE');?></a>

</div><!-- /.b-side -->
*/?>

<script>
    $(function(){
        CKEDITOR.replace('iDETAIL_TEXT', {
            'language': 'ru'
        });
        $(document).on('click','[data-put-editor-data]',function(){
            putEditorData();
        });
        function putEditorData() {
            $('#iDETAIL_TEXT').val(CKEDITOR.instances['iDETAIL_TEXT'].getData());
        }
    });
</script>
