<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

//debugPre($arItem);

?>

<div class="search-result-item sri">
    <h2><a class="h5" href="../<?=str_replace('/catalog', 'catalog', $arItem['DETAIL_PAGE_URL'])?>" class="btn btn-default"><?=$arItem['title']?></a></h2>
    <dl class="sri-characters">
        <?php if(!empty($arItem['authorbook'])):?>
            <dt>Автор издания</dt>
            <dd><?=$arItem['authorbook']?></dd>
        <?php endif; ?>
        <?php if(!empty($arItem['publishyear'])):?>
            <dt>Год издания</dt>
            <dd><?=$arItem['publishyear']?></dd>
        <?php endif; ?>

        <?php if(!empty($arItem['countpages'])):?>
            <dt>Количество страниц</dt>
            <dd><?=$arItem['countpages']?></dd>
        <?php endif; ?>

        <dt>Доступ</dt>
        <dd>
            <?php if($arItem['isprotected'] == 2): ?>
                Заблокирован по требованию правообладателя
            <?php elseif($arItem['isprotected'] == 1 && !$arParams['IS_SHOW_PROTECTED']): ?>
                Закрытый
            <?php else: ?>
                Открытый
            <?php endif; ?>
        </dd>
    </dl>
    <div class="button-bar">
        <?/*a href="../<?=str_replace('/catalog', 'catalog', $arItem['DETAIL_PAGE_URL'])?>" class="btn btn-default">Карточка издания</a><br/*/?>
        <a 
            href="../<?=str_replace('/catalog', 'catalog', $arItem['VIEWER_URL'])?>" 
            class="btn btn-primary"
            onclick="readBook(event, this); return false;"
            data-link="<?= $arItem['readBookId'] ?>"
            data-options="<?= htmlspecialchars(json_encode($arItem['viewerOptions']),ENT_QUOTES,'UTF-8') ?>"
            data-strict-search="<?= (int)$arItem["STRICT_QUERY_SEARCH"] ?>"
            data-full-search="<?= (int)$arItem["FULL_QUERY_SEARCH"] ?>"
            data-reader-blind
            target="_blank"
        >
            Читать
        </a>
    </div>
</div>