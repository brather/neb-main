<?php

require_once(__DIR__
    . '/../../../php_interface/include/PHPExcel.php');
CBitrixComponent::includeComponentClass("rest:rest.server");

/**
 * Class RestLibraryStatistic
 */
class RestLibraryStatistic extends RestServer
{
    /**
     * Поле в таблице статистики по котороему получаем данные
     *
     * @var string
     */
    protected $_fieldName;

    /**
     * @var array
     */
    protected $_methodAllowGroups
        = array(
            'getAction' => array(
                13 => true,
            )
        );

    protected $_params
        = array(
            'exportType' => null,
            'dateFrom'   => null,
            'dateTo'     => null,
        );

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams()
    {
        if (!CModule::IncludeModule('nota.collection')) {
            throw new Exception('Module "nota.collection" not installed');
        }
        if (!isset($this->_params['dateFrom'])
            || empty($this->_params['dateFrom'])
        ) {
            $this->_params['dateFrom'] = ConvertTimeStamp(
                time() - 86400
                * \Nota\Collection\LibraryStatTable::DEFAULT_STAT_DAYS
            );
        }
        if (!isset($this->_params['dateTo'])
            || empty($this->_params['dateTo'])
        ) {
            $this->_params['dateTo'] = ConvertTimeStamp();
        }

        return $this;
    }

    /**
     * @param $function
     * @param $args
     *
     * @return bool
     * @throws Exception
     */
    public function __call($function, $args)
    {
        if (isset($this->componentParameters['PARAMETERS']['SEF_MODE'][$function])) {
            $this->_fieldName = strtoupper($function);

            return call_user_func_array(array($this, 'processRequest'), $args);
        }
        throw new Exception('field not exists');
    }

    /**
     * @return int
     * @throws Exception
     */
    public function getAction()
    {
        $this->loadStatistics();

        switch ($this->_params['exportType']) {
            case 'excel':
                $this->exortExcel();
                exit;
                break;
        }

        return 200;
    }

    /**
     * @throws Exception
     * @throws \Bitrix\Main\ArgumentException
     */
    public function loadStatistics()
    {
        $dbResult = \Nota\Collection\LibraryStatTable::getList(
            $this->_buildLibraryStatParams()
        );
        $this->arResult['statData'] = array();
        while ($library = $dbResult->fetch()) {
            $this->arResult['statData'][$library['LIBRARY_ID']] = $library;
        }
        if (!CModule::IncludeModule('iblock')) {
            throw new Exception('Module iblock not installed');
        }
        /**
         * Достаем название библиотек
         */
        $dbResult = \Bitrix\Iblock\ElementTable::getList(
            array(
                'filter' => array(
                    'ID' => array_keys($this->arResult['statData']),
                ),
                'select' => array(
                    'ID',
                    'NAME',
                ),
            )
        );
        while ($library = $dbResult->fetch()) {
            $this->arResult['statData'][$library['ID']]['NAME']
                = $library['NAME'];
        }

        return $this;
    }

    /**
     * Отдает в браузер excel файло сгенеренный из $this->arResult['statData']
     *
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     */
    public function exortExcel()
    {
        $title = 'Export statistics - ' . $this->_fieldName
            . ' ' . $this->_params['dateFrom']
            . ' - ' . $this->_params['dateTo'];
        $excel = new PHPExcel();
        $excel->getProperties()
            ->setTitle($title)
            ->setSubject($title);
        $sheet = $excel->setActiveSheetIndex(0);
        $sheet
            ->setCellValue('A1', 'Library')
            ->setCellValue('B1', 'Sum');
        $sheet->getColumnDimension('A')
            ->setAutoSize(true);
        $sheet->getColumnDimension('B')
            ->setAutoSize(true);

        $data = $this->arResult['statData'];
        if (isset($data[0])) {
            $data[0]['NAME'] = 'Total';
        }
        $index = 2;
        foreach ($data as $statData) {
            $sheet
                ->setCellValue('A' . $index, $statData['NAME'])
                ->setCellValue('B' . $index, $statData[$this->_fieldName]);
            $index++;
        }


        $excel->getActiveSheet()->setTitle('Statistics');
        $excel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header(
            'Content-Disposition: attachment;filename="' . $title . '.xlsx"'
        );
        header(
            'Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'
        );
        $excelWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $excelWriter->save('php://output');
    }

    /**
     * @return array
     */
    private function _buildLibraryStatParams()
    {
        $params = array();
        switch ($this->_fieldName) {
            case 'PUBLICATIONS':
                /**
                 * Не нашел как в битриксе сделать фильтр по подзапросу
                 */
                $dbResult = \Nota\Collection\LibraryStatTable::getList(
                    array(
                        'select'  => array(
                            'MAX_DATE',
                        ),
                        'runtime' => array(
                            new \Bitrix\Main\Entity\ExpressionField(
                                'MAX_DATE', 'MAX(DATE)'
                            )
                        )
                    )
                );
                $item = $dbResult->fetch();
                $params['filter'] = array(
                    'DATE' => $item['MAX_DATE'],
                );
                break;
            default:
                $params['filter'] = array(
                    '>DATE' => new \Bitrix\Main\Type\Date(
                        $this->_params['dateFrom']
                    )
                );
                $params['filter']['<DATE'] = $this->_params['dateTo'];
                $params['select'] = array(
                    'LIBRARY_ID',
                    new \Bitrix\Main\Entity\ExpressionField(
                        $this->_fieldName, 'SUM(' . $this->_fieldName . ')'
                    )
                );
                $params['group'] = array('LIBRARY_ID');
                break;
        }

        return $params;
    }
}