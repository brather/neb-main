<?
$MESS['LIB_SEARCH_FORM_TEMPLATE_ALL'] = 'all items';
$MESS['LIB_SEARCH_FORM_TEMPLATE_NAME'] = 'main title';
$MESS['LIB_SEARCH_FORM_TEMPLATE_MPUBLISHPLACE'] = 'place of publication';
$MESS['LIB_SEARCH_FORM_TEMPLATE_MPUBLISHER'] = 'publisher';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AUTHOR'] = 'author';
$MESS['LIB_SEARCH_FORM_TEMPLATE_ISBN'] = 'ISBN';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TEXT'] = 'library';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SEARCH'] = 'Advanced search';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PORTAL'] = 'Search only in scanned editions';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TEXT'] = 'Libraries';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FORACCESS'] = 'access';
$MESS['LIB_SEARCH_FORM_TEMPLATE_OPEN_ACCESS'] = 'Free access';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CLOSE_ACCESS'] = 'Limited access';
$MESS['LIB_SEARCH_FORM_TEMPLATE_EXAMPLES'] = 'Example: Gagarin’s biography';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FIND'] = 'Find';

$MESS['LIB_SEARCH_FORM_TEMPLATE_OR'] = 'or';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AND'] = 'and';
$MESS['LIB_SEARCH_FORM_TEMPLATE_NO'] = 'not';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AUTHORBOOK'] = 'author';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TITLE'] = 'main title';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PUBLISHER'] = 'publishers';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PUBLISHPLACE'] = 'place of publication';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CATEGORY'] = 'by category';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE'] = 'date';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE_PUBLIC'] = 'Date of publication';

$MESS['LIB_SEARCH_FORM_TEMPLATE_ADD'] = 'add a search condition';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DEL'] = 'remove condition';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE_SUBMIT'] = 'accept';

$MESS['LIB_SEARCH_FORM_TEMPLATE_ABOUT'] = 'About the Project';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FULLTEXT_SEARCH_ONLY'] = 'Search in full-text publications only';

$MESS['LIB_SEARCH_FORM_TEMPLATE_FIND_IN'] = 'Search in results';

$MESS['LIB_SEARCH_FORM_TEMPLATE_STRICT_SEARCH'] = 'Exact phrase';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FOR_FULL_TEXT'] = 'Search on full text of publications';
$MESS['LIB_SEARCH_FORM_TEMPLATE_IN_DISERTATIONS'] = 'Search in theses and dissertations';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CLEAN'] = 'Clean search';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SCANNED_BOOKS'] = 'Scanned books Russian libraries';
$MESS['LIB_SEARCH_FORM_TEMPLATE_BETA'] = 'beta-version';
