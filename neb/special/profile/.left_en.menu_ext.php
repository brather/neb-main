<?
	if (!$USER->IsAuthorized()) 
		return false;

	$obNUser = new nebUser();	
	$obNUser->getRole();

	/*if($obNUser->role != 'user')
	{
	$arLibrary =	$obNUser->getLibrary();
	} */	

	if($obNUser->role == 'user') 
	{
		/* массиы меню пользователя*/

		$aMenuLinksExt = array(
			array(' ', '/special/profile/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
			array('Orders for scanning', '/special/profile/scanning/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Profile settings', '/special/profile/edit/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Help', '/faq/en/', array(), 									array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			#array('Выйти', '/special/profile/?logout=yes', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),

			array('Personal account', '/special/profile/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),

			array('my library', '/special/profile/my-library/', array(),			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_lb")),
			array('search requests', '/special/profile/searches/', array(),			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_search")),

            array('quotes', '/special/profile/quote/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_qt")),
            array('bookmarks', '/special/profile/bookmark/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_bm")),
            array('notes', '/special/profile/note/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_notes")),

		);
	}
	else if($obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR)
	{
		/* массиы меню библиотекаря*/

		$aMenuLinksExt = array(
			array(' ', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
			array('Profile settings', '/special/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Help', '/faq/en/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'MSGNUM' => 0)),
			array('Personal account', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			//array('Новости', '/special/profile/news/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			//array('Выйти', '/special/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
			array('Statistics', '/special/profile/statistics/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Funds', '/special/profile/funds/manage/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Readers', '/special/profile/readers/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			//array('Коллекции', '/special/profile/collection/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			//array('Новости', '/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=3&type=news&lang=ru', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Collections', '/special/profile/collection/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('News', '/special/profile/news/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Digitization', '/special/profile/plan_digitization/', array(), 	array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('ELP', '/special/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		);
	}
	else if($obNUser->role == UGROUP_LIB_CODE_CONTROLLER)
	{
		/* массиы меню библиотекаря-контролера*/

		$aMenuLinksExt = array(
			array(' ', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
			array('Profile settings', '/special/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Help', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'MSGNUM' => 0)),
			#array('Выйти', '/special/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
			array('Personal account', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Statistics', '/special/profile/statistics/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('ELP', '/special/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		);
	}
	else if($obNUser->role == UGROUP_RIGHTHOLDER_CODE)
	{
		/* Массив меню правообладателя	*/
		$aMenuLinksExt = array(
			array(' ', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),

			array('Profile settings', '/special/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			array('Help', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
			#array('Выйти', '/special/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),

			array('Personal account', '/special/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),

			array('Funds', '/special/profile/work-fonds/', array(),	array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
			array('Statistics', '/special/profile/statistics/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
		);
	}
	$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>
