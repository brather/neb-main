<?php
/**
 * User: agolodkov
 * Date: 03.08.2016
 * Time: 15:28
 */

namespace BitrixBundle\Annotations;

/**
 * Class Template
 *
 * @Annotation
 */
class Template extends \Sensio\Bundle\FrameworkExtraBundle\Configuration\Template
{

    public function __construct(array $values)
    {
        parent::__construct($values);
    }
}