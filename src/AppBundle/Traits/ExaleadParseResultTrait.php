<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 14:32
 */

namespace AppBundle\Traits;


trait ExaleadParseResultTrait
{
    use ObjectHelperTrait;

    protected static $_exaleadMetaTypes
        = [
            'file_size'       => 'integer',
            'ispart'          => 'string',
            'idbook'          => 'string',
            'namebook'        => 'string',
            'startpageparts'  => 'string',
            'finishpageparts' => 'string',
            'rangepageparts'  => 'string',
            'subtitle'        => 'string',
            'dateindex'       => 'string',
            'year'            => 'string',
            'idlibrary'       => 'string',
            'publishyear'     => 'string',
            'librarytype'     => 'string',
            'library'         => 'string',
            'publisher'       => 'string',
            'idparent'        => 'string',
            'idparent2'       => 'string',
            'responsibility'  => 'string',
            'publishplace'    => 'string',
            'countpages'      => 'string',
            'isprotected'     => 'string',
            'filename'        => 'string',
            'viewurl'         => 'string',
            'file_name'       => 'string',
            'id'              => 'string',
            'id_abis'         => 'string',
            'is_skbr'         => 'string',
            'id_skbr'         => 'string',
            'series'          => 'string',
            'annotation'      => 'string',
            'publicurl'       => 'string',
            'bbk'             => 'string',
            'contentnotes'    => 'string',
            'edition'         => 'string',
            'isbn'            => 'string',
            'issn'            => 'string',
            'notes'           => 'string',
            'placepublish'    => 'string',
            'udk'             => 'string',
            'isschool'        => 'string',
            'dates'           => 'string',
            'fundnumber'      => 'string',
            'fundname'        => 'string',
            'inventorynumber' => 'string',
            'inventoryname'   => 'string',
            'unitnumber'      => 'string',
            'unitname'        => 'string',
            'description'     => 'string',
            'material'        => 'string',
            'size'            => 'string',
            'positionpart'    => 'string',
            'text'            => 'segments',
            'title'           => 'segments',
            'bbk_bbkfull'     => 'segments',
            'bbkfull'         => 'segments',
            'udkfull'         => 'segments',
            'authorbook'      => 'segments',
            'collection'      => 'arrayString',
        ];

    /**
     * @param \SimpleXMLElement $xml
     *
     * @return array
     */
    public function parseBookData(\SimpleXMLElement $xml)
    {
        $result = ['items' => []];
        if ($hits = $this->getObjectPathData($xml, ['hits', 'Hit'])) {
            foreach ($hits as $hit) {
                $result['items'][] = $this->parseHitMetas($hit);
            }
        }

        return $result;
    }

    /**
     * @param \SimpleXMLElement $hit
     *
     * @return array
     */
    public function parseHitMetas(\SimpleXMLElement $hit)
    {
        $metasData = [];
        if ($metas = $this->getObjectPathData($hit, ['metas', 'Meta'])) {
            /** @var \SimpleXMLElement $meta */
            foreach ($metas as $meta) {
                $metaAttributes = $meta->attributes();
                $metasData[(string)$metaAttributes['name']] = $meta;
            }
        }
        /**
         * @var string            $name
         * @var \SimpleXMLElement $meta
         */
        foreach ($metasData as $name => $meta) {
            if (isset(static::$_exaleadMetaTypes[$name])) {
                switch (static::$_exaleadMetaTypes[$name]) {
                    case 'integer':
                        $metasData[$name] = (int)$meta->MetaString;
                        break;
                    case 'string':
                        $metasData[$name] = (string)$meta->MetaString;
                        break;
                    case 'segments':
                        $item = ['text' => '', 'segments' => []];
                        foreach ($meta->MetaText as $metaText) {
                            /** @var \SimpleXMLElement $text */
                            foreach ($metaText as $text) {
                                $segment = [];
                                $attributes = $text->attributes();
                                $segment['highlighted'] = (string)$attributes['highlighted'];
                                $segment['highlighted'] = filter_var(
                                    $attributes['highlighted'],
                                    FILTER_VALIDATE_BOOLEAN
                                );
                                if (!is_array($text)) {
                                    $text = (string)$text;
                                    $segment['text'] = $text;
                                    $item['text'] .= $text;
                                }
                                $item['segments'][] = $segment;
                            }
                        }
                        $metasData[$name] = $item;
                        break;
                    case 'arrayString':
                        $item = [];
                        $arMeta = (array)$meta;
                        $metaString = $arMeta['MetaString'];
                        if (!is_array($metaString)) {
                            $metaString = array($metaString);
                        }
                        foreach ($metaString as $coll) {
                            if (!is_array($coll)) {
                                $item[] = $coll;
                            }
                        }
                        $metasData[$name] = $item;
                        break;
                }
            }
        }

        return $metasData;
    }


    /**
     * @param \SimpleXMLElement $xml
     *
     * @return array
     */
    public function parseAnswerGroups(\SimpleXMLElement $xml)
    {
        $result = [];
        if ($groups = $this->getObjectPathData($xml, ['groups', 'AnswerGroup'])) {
            /** @var \SimpleXMLElement $group */
            foreach ($groups as $group) {
                $attributes = $group->attributes();
                $categoryId = (string)$attributes['id'];
                $result[$categoryId] = $this->parseGroupCategories($group);
            }
        }

        return $result;
    }

    /**
     * @param \SimpleXMLElement $group
     *
     * @return array
     */
    public function parseGroupCategories(\SimpleXMLElement $group)
    {
        $result = [];
        if ($categories = $this->getObjectPathData($group, ['categories', 'Category'])) {
            /** @var \SimpleXMLElement $category */
            foreach ($categories as $category) {
                $item = [];
                foreach ($category->attributes() as $attribute => $value) {
                    $item[$attribute] = (string)$value;
                }
                $result[] = $item;
            }
        }

        return $result;
    }
}