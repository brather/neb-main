<?php
/**
 * User: agolodkov
 * Date: 21.10.2016
 * Time: 16:45
 */

namespace AppBundle\DependencyInjection\Search\Exalead;


use AppBundle\DependencyInjection\Search\ConditionInterface;
use AppBundle\DependencyInjection\Search\QueryBuilderInterface;

class QueryBuilder implements QueryBuilderInterface
{
    const DEFAULT_LIMIT = 15;
    /**
     * @var ConditionInterface
     */
    private $_condition;

    private $_params = [];

    private $_method = '/search-api/search';

    /**
     * @param ConditionInterface $condition
     */
    public function __construct(ConditionInterface $condition)
    {
        $this->_condition = $condition;
        $this->_params['hf'] = static::DEFAULT_LIMIT;

    }

    /**
     * @return string
     */
    public function buildQuery()
    {
        $this->_params['q'] = $this->_condition->buildCondition();

        return $this->_method . '?' . http_build_query($this->_params);
    }

    /**
     * @param ConditionInterface $condition
     *
     * @return mixed
     */
    public function setCondition(ConditionInterface $condition)
    {
        $this->_condition = $condition;
    }

    public function addSort($order, $by)
    {
        // TODO: Implement addSort() method.
    }

    /**
     * @param int $value
     */
    public function setLimit($value)
    {
        $this->_params['hf'] = $value;
    }

    /**
     * @param int $value
     */
    public function setOffset($value)
    {
        $this->_params['b'] = $value;
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function addParam($name, $value)
    {
        $this->_params[$name] = $value;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->_method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->_method = $method;
    }
}