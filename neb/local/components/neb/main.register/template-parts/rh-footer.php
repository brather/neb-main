<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

?>
<hr>
<div class="fieldrow nowrap fieldrowaction">
    <div class="fieldcell ">
        <div class="field clearfix">
            <button class="formbutton" value="1"
                    type="submit"><?= Loc::getMessage(
                    "MAIN_REGISTER_REGISTER"
                ); ?></button>
            <input type="hidden" name="register_submit_button"
                   value="<?= Loc::getMessage("AUTH_REGISTER") ?>"/>
        </div>
    </div>
</div>
</form>

</div><!-- /.b-registration-->
</section>