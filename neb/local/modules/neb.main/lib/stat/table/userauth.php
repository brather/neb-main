<?php
/**
 * User: agolodkov
 * Date: 18.01.2016
 * Time: 12:23
 */

namespace Neb\Main\Stat\Table;


use Bitrix\Main\Entity\DataManager;

class UserAuthTable extends DataManager
{
    /**
     * @return string
     */
    public static function getConnectionName()
    {
        return 'stat';
    }


    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'user_auth';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id'        => array(
                'data_type' => 'integer',
                'primary'   => true,
            ),
            'user_id'   => array(
                'data_type' => 'integer',
            ),
            'ip'        => array(
                'data_type' => 'integer'
            ),
            'timestamp' => array(
                'data_type' => 'datetime',
            ),
        );
    }
}