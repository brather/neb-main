
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class="rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper searchempty clearfix">
		<h1>Преимущества НЭБ</h1>
			<div class="b-mainslider">
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock"><img class="loadingimg" src="../pic/pic_14.jpg" alt=""></div>
						<div class="b-mainslider_descr iblock">
							<h3>1. Единый читательский билет</h3>
							<p>Авторизуйтесь на сайте с помощью Единого читательского билета либо через портал госуслуг, чтобы получить доступ к условно-свободным документам, а так же возможность пользования личным кабинетом с удобными сервисами. </p>
						</div>
					</div>
				</div>
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock"><img class="loadingimg" src="../pic/pic_14.jpg" alt=""></div>
						<div class="b-mainslider_descr iblock">
							<h3>2. С 31 января 2014 года вступают в силу изменения в пордяке регистрации Единых читательских билетов</h3>
							<p> В соответствии с указом Президента Российской Федерации от 08.01.2014 регистрация единого читательского билета для граждан РФ будет возможна через портал госуслуги. Данная мера должна сильно упростить процесс регистрации читателя в системе, а также сократить сроки получения им Единого читательского билета.</p>
						</div>
					</div>
				</div>
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock"><img class="loadingimg"  src="../pic/pic_14.jpg" alt=""></div>
						<div class="b-mainslider_descr iblock">
							<h3>3. Единая система поиска по полным текстам всех фондов</h3>
							<p>Вводите любой текст в поисковую строку и получайте наиболее релевантные выдачи по полнотекстовому индексу с учетом симантики запроса.</p>
						</div>
					</div>
				</div>
			</div> <!-- /.b-mainslider -->
	

</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>
</body>
</html>