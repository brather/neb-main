<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 13:24
 */

namespace AppBundle\DependencyInjection\Search\Exalead;


use AppBundle\DependencyInjection\Search\AbstractSearchResult;
use AppBundle\Entity\SearchResultEntityInterface;

abstract class SearchResult extends AbstractSearchResult
{

    /**
     * @return SearchResultEntityInterface[]
     */
    abstract public function buildEntities();
}