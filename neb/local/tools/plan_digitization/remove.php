<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('highloadblock');
Loader::includeModule('nota.journal');

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
$arBookId = $arRequest['BOOK_ID'];

$result = array('error'  => null, 'status' => 200);
try {
    if ($_SERVER['REQUEST_METHOD'] and check_bitrix_sessid() and !empty($arBookId)) {
        $obUser = new nebUser();
        $Library = $obUser->getLibrary();
        if (!$obUser->isLibrary() && !$obUser->isOperator()) {
            throw new Exception('Access deny', 403);
        } elseif ($obUser->isLibrary() && empty($Library['ID'])) {
            throw new Exception('Empty library id', 500);
        }
        //region Вернем id блока с планами оцифровки
        $idHLBlockDigitization = '';
        $connection = Application::getConnection();
        $sql = "SELECT ID,NAME FROM b_hlblock_entity WHERE NAME='" . HIBLOCK_CODE_DIGITIZATION_PLAN . "';";
        $recordset = $connection->query($sql);
        while($record = $recordset->fetch()) {
            $idHLBlockDigitization = $record['ID'];
        }
        //endregion
        $idNewStatus = '';
        if(empty($arRequest['STATUS'])) {
            //region Получим данные по свойству "Статус"
            //region Возвращаем id самого свойства
            $rsData = CUserTypeEntity::GetList(
                array("SORT" => "ASC"),
                array("ENTITY_ID" => "HLBLOCK_" . $idHLBlockDigitization, "FIELD_NAME" => "UF_REQUEST_STATUS")
            );
            $idPropStatus = '';
            if($arRes = $rsData->Fetch()) {
                $idPropStatus = $arRes["ID"];
            }
            //endregion
            //region Возвращаем массив значений свойства с id
            $arPropStatus = array();
            if($idPropStatus != '') {
                $obUserFieldEnum = new CUserFieldEnum();
                $rsStatus = $obUserFieldEnum->GetList(['SORT' => 'ASC'], ['USER_FIELD_ID' => $idPropStatus]);
                while ($arStatus = $rsStatus->Fetch()) {
                    if($arStatus['VALUE'] == DIGITIZATION_STATUS_CANCELED) {
                        $idNewStatus = $arStatus['ID'];
                    }
                    $arPropStatus[$arStatus['ID']] = $arStatus;
                }
            }
            //endregion
            //endregion
        } else {
            $idNewStatus = $arRequest['STATUS'];
        }

        //region Вернем список id администраторов библиотек (если издание добавлено администратором в план, то оно удаляется, если клиентом сайта, то обновляются свойства)
        $rsGroup = CGroup::GetList($by = "ID",$order = "asc", array("STRING_ID" => UGROUP_LIB_CODE_ADMIN, "Y"))->Fetch();
        $arLibAdmin = CGroup::GetGroupUser($rsGroup['ID']);
        //endregion

        $hlblock = HighloadBlockTable::getById($idHLBlockDigitization)->fetch();
        $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

        $filter = array('UF_EXALEAD_BOOK' => $arBookId);
        if (!$obUser->isOperator()) {
            $filter['=UF_LIBRARY'] = $Library['ID'];
        }
        $rsData = $entity_data_class::getList(
            array(
                "select" => array("ID", "UF_NEB_USER"),
                "filter" => $filter
            )
        );
/** @todo http://84.47.187.69:4480/redmine/issues/11175 */
//        J::add('plandigital', 'delete', Array('BOOK_ID' => $book_id));

        while ($arData = $rsData->fetch()) {
            if(in_array($arData['UF_NEB_USER'], $arLibAdmin)) {
                $entity_data_class::delete($arData['ID']);
            } else {
                $entity_data_class::update($arData['ID'], array('UF_REQUEST_STATUS' => ''));
                $entity_data_class::update($arData['ID'], array('UF_PLAN_ID' => ''));
            }
        }

        if ($entity_data_class::getEntity()
                ->getConnection()
                ->getAffectedRowsCount() < 1
        ) {
            throw new Exception('Cannot delete plan', 500);
        }

        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag("hlblock_id" . HIBLOCK_PLAN_DIGIT);
    }
} catch (Exception $e) {
    $result['error'] = $e->getMessage();
    if (empty($result['error'])) {
        $result['error'] = 'Unknown error';
    }
    $result['status'] = $e->getCode();
}

MainHelper::showJson($result);