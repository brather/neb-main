<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<div class="b-graphpopup">
<div id="container" style="width:100%; height:400px;"></div>
<? if ($_GET["ACTION"] == "VIEW"):?>
    <script>
    $(function () {
        $('#container').highcharts({
            title: false,
            xAxis: {
                categories: [ 'Март','Апр','Май','Июнь','Июль','Авг','Сент','Окт','Ноя','Дек','Янв' ]
                /*categories: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                categories: [ <?#=$Data?>] */
            },
            yAxis: {
                title: {
                    text: 'Количество просмотров'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },

            legend: false,
            series: [{
                name: 'Посещения',
                data: [234, 345, 323, 234, 477, 566, 505, 422, 399, 366, 388]
                /*data: [ <?#=$Count?>] */
            }]
        });
    });
    </script>
<? else:?>
    <script>
    $(function () {
        $('#container').highcharts({
            title: false,
            xAxis: {
                categories: [ 'Март','Апр','Май','Июнь','Июль','Авг','Сент','Окт','Ноя','Дек','Янв' ]
            },
            yAxis: {
                title: {
                    text: 'Количество читателей'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },

            legend: false,
            series: [{
                name: 'Читатели',
                data: [202, 224, 258, 305, 360, 369, 380, 399, 420, 433, 445]
                /*data: [ <?#=$Count?>] */
            }]
        });
    });
    </script>
<? endif?>
    <div id="highcharts" data-sorce="<?=$_REQUEST["PATH"]?>/graph_json.php?BOOK_ID=<?=$_REQUEST["BOOK_ID"]?>&TITLE=<?=$_REQUEST["TITLE"]?>&ACTION=<?=$_REQUEST["ACTION"]?>" style="min-width: 310px; height: 485px; margin: 0 auto"></div>
</div>