<?php

$MESS['FEEDBACK_REPLY_DATE_CREATE'] = 'Date create';
$MESS['FEEDBACK_REPLY_DATE_CLOSE']  = 'Date close';
$MESS['FEEDBACK_REPLY_STATUS']      = 'Status';
$MESS['FEEDBACK_REPLY_THEME']       = 'Theme';
$MESS['FEEDBACK_REPLY_BOOK']        = 'Book';
$MESS['FEEDBACK_REPLY_MESSAGE']     = 'Message';
$MESS['FEEDBACK_REPLY_ANSWER']      = 'Answer';
$MESS['FEEDBACK_REPLY_FILE']        = 'File';

$MESS['FEEDBACK_REPLY_TITLE']       = 'Reply on order №';
$MESS['FEEDBACK_REPLY_WRITE_CAUSE'] = 'Answer';
$MESS['FEEDBACK_REPLY_SUBMIT']      = 'Save and send';
$MESS['FEEDBACK_REPLY_SAVE']        = 'Save';