<?namespace Intervolga\RemoteUserClient\Tools;

use Intervolga\RemoteUserClient\User;

class Integrator
{
	/**
	 * Returns fields, that must not be loaded from remote server.
	 *
	 * @return array
	 */
	public static function getSkipMergeFieldsArray()
	{
		return array(
			"EMAIL",
			"LAST_NAME",
			"NAME",
			"SECOND_NAME",
			"TIMESTAMP_X",
			"PERSONAL_PHOTO_INPUT",
			"WORK_LOGO_INPUT",
			"PASSWORD",
			"CHECKWORD",
			"IS_ONLINE",
			"ID",
			"LOGIN",
			"ACTIVE",
			"LAST_LOGIN",
			"DATE_REGISTER",
			"LID",
			"EXTERNAL_AUTH_ID",
			"CHECKWORD_TIME",
			"CONFIRM_CODE",
			"LOGIN_ATTEMPTS",
			"LAST_ACTIVITY_DATE",
			"AUTO_TIME_ZONE",
			"TIME_ZONE",
			"TIME_ZONE_OFFSET",
			"STORED_HASH",
			"ADMIN_NOTES",
		);
	}

	/**
	 * Returns local user with its remote copy.
	 *
	 * @param int $iUser local user id
	 * @param array $arUser local user fields
	 *
	 * @return mixed
	 */
	public static function mergeLocalUserWithRemote($iUser, $arUser)
	{
		if ($iUser)
		{
			$arParams = array(
				"SELECT" => array(),
				"FIELDS" => array(),
			);
			foreach (array_keys($arUser) as $sKey)
			{
				if (strpos($sKey, "~") === 0)
				{
					continue;
				}
				if (in_array($sKey, static::getSkipMergeFieldsArray()))
				{
					continue;
				}
				if (strpos($sKey, "UF_") === 0)
				{
					$arParams["SELECT"][] = $sKey;
				}
				else
				{
					$arParams["FIELDS"][] = $sKey;
				}
			}
			$arUsers = User::getList("ID", "ASC", array("ID" => $iUser), $arParams);
			if ($arUsers[$iUser])
			{
				foreach ($arUsers[$iUser] as $sKey => $value)
				{
					if (in_array($sKey, $arParams["FIELDS"]) || in_array($sKey, $arParams["SELECT"]))
					{
						if (array_key_exists("~" . $sKey, $arUser))
						{
							$arUser["~" . $sKey] = $value;
							$arUser[$sKey] = htmlspecialcharsEx($value);
						}
						else
						{
							$arUser[$sKey] = $value;
						}
					}
				}
			}
		}

		return $arUser;
	}
}