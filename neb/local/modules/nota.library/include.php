<?
CModule::AddAutoloadClasses(
    'nota.library',
    array(
        'Nota\Library\Collection' => 'lib/collection.php',
    )
);

use Bitrix\NotaExt\Iblock\Element as IB;

class CNotaLibrary
{
	public static function getGroupsArrayByCode($groups){
		$groups = implode("|", $groups);
		$groupsDB = CGroup::GetList($by = "c_sort", $order = "asc", array("STRING_ID" => $groups));
		if(intval($groupsDB->SelectedRowsCount()) > 0)
		{
			while($g = $groupsDB->Fetch())
			{
				$result[] = $g["ID"];
			}
		}
		return $result;//IBLOCK_CODE_LIBRARY
	}

	public static function isAdmin(){
		return CSite::InGroup(CNotaLibrary::getGroupsArrayByCode(array(UGROUP_LIB_CODE_ADMIN)));
	}

	public static function isEditor(){
		return CSite::InGroup(CNotaLibrary::getGroupsArrayByCode(array(UGROUP_LIB_CODE_EDITOR)));
	}

	public static function isController(){
		return CSite::InGroup(CNotaLibrary::getGroupsArrayByCode(array(UGROUP_LIB_CODE_CONTROLLER)));
	}

	public static function isStaff(){
		return CSite::InGroup(CNotaLibrary::getGroupsArrayByCode(array(UGROUP_LIB_CODE_ADMIN, UGROUP_LIB_CODE_CONTROLLER, UGROUP_LIB_CODE_EDITOR)));
	}

	public static function getLibraryID(){
		global $USER;

		$arFilter = array("ID" => $USER->GetID());
		$arParams["SELECT"] = array("UF_LIBRARY");


			$arRes = CUser::GetList($by, $desc, $arFilter, $arParams)->Fetch();


		return $arRes["UF_LIBRARY"] > 0 ? $arRes["UF_LIBRARY"] : false;
	}

	function OnAdminListDisplay(&$list){
		$userLibraryID = self::getLibraryID();
		//новости библиотеки
		if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/iblock_list_admin.php" && $_REQUEST["IBLOCK_ID"] == 3 && self::isStaff()){
			//скроем привязку к библиотеке
			unset($list->aVisibleHeaders["PROPERTY_3"]);

			foreach($list->aRows as $k => $row){
				$libraryProp = CIBlockElement::GetProperty(3, $row->arRes["ID"], array("sort" => "asc"), Array("CODE" => "LIBRARY"));
				$libraryID = $libraryProp->Fetch();
				$libraryID = $libraryID["VALUE"];

				if($libraryID != $userLibraryID) unset($list->aRows[$k]);
			}
		}

		//список коллекций (разделы)
		if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/iblock_section_admin.php" && $_REQUEST["IBLOCK_ID"] == 6 && self::isStaff()){
			//скроем привязку к библиотеке
			unset($list->aVisibleHeaders["UF_LIBRARY"]);

			if(!isset($_REQUEST["find_section_section"])){
				//скроем меню пока не выбрана коллекция
				$list->context->items = Array($list->context->items[1]);
			}

			foreach($list->aRows as $k => $row){
				$libraryID = $row->arRes["UF_LIBRARY"];
				if($libraryID != $userLibraryID) unset($list->aRows[$k]);
			}
		}

		//список книг в коллекции
		if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/iblock_list_admin.php" && $_REQUEST["IBLOCK_ID"] == 6 && isset($_REQUEST["find_section_section"]) && self::isStaff()){
			//скроем привязку к библиотеке
			unset($list->aVisibleHeaders["UF_LIBRARY"]);
			unset($list->context->items[2]);
		}
	}

	//функция скроет все пункты меню, которые открылись при предоставлении доступа к инфоблокам
	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
		if(self::isStaff()){
			foreach($aModuleMenu as $k => $menu){
				if($menu["module_id"] != "nota.library") unset($aModuleMenu[$k]);
			}
		}
	}

	private function replaceFormData($file, $iblock, $replaceData, &$form){
		if($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/" . $file && $_REQUEST["IBLOCK_ID"] == $iblock && self::isStaff()){
			if(count($replaceData) > 0){
				foreach($form->tabs as $k => $v){
					foreach($form->tabs[$k]["FIELDS"] as $field_index => $field){
						if(array_key_exists($field["id"], $replaceData)){
							if($_REQUEST["ID"] == 0 && isset($replaceData[$field["id"]])){
								$form->tabs[$k]["CONTENT"] = str_replace($v["FIELDS"][$field_index]["custom_html"], $replaceData[$field["id"]], $form->tabs[$k]["CONTENT"]);
							} else {
								$form->tabs[$k]["CONTENT"] = str_replace($v["FIELDS"][$field_index]["custom_html"], $v["FIELDS"][$field_index]["hidden"], $form->tabs[$k]["CONTENT"]);
							}
						}
					}
				}
			}
		}
	}

	//скрываем свойства от пользователя
	function OnAdminTabControlBegin(&$form){
		$userLibraryID = self::getLibraryID();

		$newsReplaceArray = Array(
			"PROPERTY_3" => '<input type="hidden" name="PROP[3][n0][VALUE]" value="'.$userLibraryID.'">',
		);
		self::replaceFormData("iblock_element_edit.php", 3, $newsReplaceArray, $form);

		$bookReplaceArray = Array(
			"SECTIONS" => '<input type="hidden" name="IBLOCK_SECTION[]" value="'.$userLibraryID.'">',
		);
		self::replaceFormData("iblock_element_edit.php", 6, $bookReplaceArray, $form);

		$collectionReplaceArray = Array(
			"UF_LIBRARY" => true,
			"UF_ADDED_FAVORITES" => true,
			"UF_VIEWS" => true
		);
		self::replaceFormData("iblock_section_edit.php", 6, $collectionReplaceArray, $form);
	}

	//проверка на редактирование новостей
	function OnBeforeIBlockElementUpdate(&$arFields){
	
		/*$userLibraryID = self::getLibraryID();

		$tmpLibraryID = $arFields["PROPERTY_VALUES"]["3"];
		reset($tmpLibraryID);
		$tmpLibraryID = $tmpLibraryID[key($tmpLibraryID)];

		if($tmpLibraryID != $userLibraryID){
			global $APPLICATION;
			$APPLICATION->throwException("У вас нет прав на редактирование этой записи.");
			return false;
		}*/
		
	}
}
?>