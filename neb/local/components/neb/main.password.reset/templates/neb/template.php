<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?
if (isset($arResult["SUCCESS_MESSAGE"])) {
    ShowNote($arResult["SUCCESS_MESSAGE"]);
    die();
}
if (isset($arResult["ERROR_MESSAGE"])) {
    ShowError($arResult["ERROR_MESSAGE"]);
    die();
}
?>
<form method="post" name="readerselfpass" action="" class="reader-self-password nrf" id='regform'
      enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="action" value="changePasswordRequest"/>

    <div class="nrf-fieldset-title">
        <h2><?= GetMessage('CHANGE_PASSW'); ?></h2>
    </div>

    <div class="row form-group">
        <div class="col-md-7">
            <label for="CURRENT_PASSWORD"><?= GetMessage('CHANGE_PASS_CURRENT_PASS'); ?></label>
        </div>
        <div class="col-md-5">
            <input type="password"
                   data-required="true" value=""
                   data-maxlength="30" data-minlength="2"
                   name="CURRENT_PASSWORD"
                   class="form-control" id="CURRENT_PASSWORD">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-7">
            <label for="readerselfrepassword"><?= GetMessage('NEW_PASS'); ?></label>
        </div>
        <div class="col-md-5">
            <input type="password"
                   data-required="true" value=""
                   data-maxlength="30" data-minlength="2"
                   name="NEW_PASSWORD"
                   class="form-control" id="readerselfrepassword">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-7">
            <label for="NEW_PASSWORD_CONFIRM"><?= GetMessage('PASS_TWO'); ?></label>
        </div>
        <div class="col-md-5">
            <input type="password"
                   data-required="true" value=""
                   data-maxlength="30" data-minlength="2"
                   name="NEW_PASSWORD_CONFIRM"
                   class="form-control" id="NEW_PASSWORD_CONFIRM">
        </div>
    </div>
    <div class="fieldrow nowrap fieldrowaction">
        <div class="fieldcell ">
            <div class="field clearfix">
                <button name="save" class="formbtn btn btn-default" value="1"
                        type="submit"><?= GetMessage('CONFIRM'); ?></button>
                <input class="btn btn-default" type="reset" value="Отмена" data-cancel/>
                <div class="messege_ok hidden">
                    На Вашу электронную почту, указанную при регистрации,
                    отправлено письмо. <br/>Для подтверждения нового пароля
                    воспользуйтесь ссылкой из письма. <br/>Срок действия
                    ссылки - 24 часа.
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    $(function () {
        jQuery.validator.addMethod("latin", function (value, element) {
            return this.optional(element) || /^[A-Za-z0-9\w]{6,20}/.test(value);
        }, "Используйте только латинские буквы и цифры");

        var validator = $('form[name="readerselfpass"]').validate({
            rules: {
                "CURRENT_PASSWORD": {
                    required: true
                },
                "NEW_PASSWORD": {
                    required: true,
                    minlength: 6,
                    /* latin: true */
                },
                "NEW_PASSWORD_CONFIRM": {
                    required: true,
                    minlength: 6,
                    /* latin: true, */
                    equalTo: "#readerselfrepassword"
                }
            },
            messages: {
                "CURRENT_PASSWORD": {
                    required: "<?=GetMessage('REQUIRED_PASS');?>"
                },
                "NEW_PASSWORD": {
                    required: "<?=GetMessage('REQUIRED_PASS');?>",
                    minlength: "<?=GetMessage('PASS_MIN_PLACEHOLDER');?>"
                },
                "NEW_PASSWORD_CONFIRM": {
                    required: "<?=GetMessage('PASS_REPEAT');?>",
                    minlength: "<?=GetMessage('PASS_MIN_PLACEHOLDER');?>",
                    equalTo: "<?=GetMessage('PASS_MUST_EQUAL');?>"
                }
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.parent());
                $(element).closest('.form-group').toggleClass('has-error', true);
            },
            /* specifying a submitHandler prevents the default submit, good for the demo */
            submitHandler: function (form) {
                /* console.log("submitted!"); */
                var postData = {},
                    form = $(form);
                postData.sessid = $('[name="sessid"]', form).val();
                postData["action"] = $('[name="action"]', form).val();
                postData.CURRENT_PASSWORD = $('[name="CURRENT_PASSWORD"]', form).val();
                postData.NEW_PASSWORD = $('[name="NEW_PASSWORD"]', form).val();
                postData.NEW_PASSWORD_CONFIRM = $('[name="NEW_PASSWORD_CONFIRM"]', form).val();
                $.ajax({
                    type: "POST",
                    data: postData,
                    url: window.location.href,
                    success: function (data) {
                        /*console.log('success');
                        console.log(data); */
                        var message = {
                            title: "Внимание",
                            text: data,
                            button: "ОК"
                        };

                        /*validator.resetForm(); */
                        FRONT.infomodal(message);
                        $('[name="CURRENT_PASSWORD"]', form).val('');
                        $('[name="NEW_PASSWORD"]', form).val('');
                        $('[name="NEW_PASSWORD_CONFIRM"]', form).val('');

                    },
                    error: function (xhr, textStatus, errorThrown) {
                    }
                });
            },
            /* set this class to error-labels to indicate valid fields */
            success: function (label) {
                /* set &nbsp; as text for IE
                label.html("&nbsp;").addClass("resolved"); */
                label.remove();
            },
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                $(element).parent().find("." + errorClass).removeClass("resolved");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
            }
        });

        $(document).on('click', '[data-cancel]', function (e) {
            e.preventDefault();
            validator.resetForm();
        });
    });
</script>