<?php
/**
 * User: agolodkov
 * Date: 12.07.2016
 * Time: 10:52
 */

namespace AppBundle\Command;


use Neb\Main\Stat\Tracker;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateLibraryStatistics extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stats:migrate:library-books')
            ->setDescription('Date to migrate')
            ->addOption(
                'to-date',
                null,
                InputOption::VALUE_REQUIRED
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);

        $this->getContainer()
            ->get('bitrix')
            ->requireProlog(
                $this->getContainer()
                    ->get('kernel')
                    ->getRootDir()
            );
        while (ob_get_level()) {
            ob_end_clean();
        }

        global $DB;
        $dateTo = $input->getOption('to-date');

        $query
            = "
        SELECT
  ID_BITRIXDB,
  COUNT_EDITION,
  COUNT_EDITION_DIG,
  DATE_FORMAT(DATE_STAT, '%Y-%m-%d') DATE_STAT
FROM neb_stat_edition
WHERE DATE_STAT < '$dateTo'
      AND ID_EXALED > 0
ORDER BY DATE_STAT desc;
    ";
        $dbResult = $DB->Query($query);
        $lastItem = [
            'COUNT_EDITION'     => 0,
            'COUNT_EDITION_DIG' => 0,
        ];
        $result = ['success' => 0, 'error' => 0];
        while ($item = $dbResult->Fetch()) {
            $item['ID_BITRIXDB'] = (integer)$item['ID_BITRIXDB'];
            if ($item['ID_BITRIXDB'] < 1) {
                continue;
            }
            if ((integer)$item['COUNT_EDITION'] > 0) {
                $lastItem['COUNT_EDITION'] = (integer)$item['COUNT_EDITION'];
            }
            if ((integer)$item['COUNT_EDITION_DIG'] > 0) {
                $lastItem['COUNT_EDITION_DIG']
                    = (integer)$item['COUNT_EDITION_DIG'];
            }
            $trackParams = [
                'date'       => $item['DATE_STAT'],
                'library_id' => $item['ID_BITRIXDB'],
                'value_num'  => $lastItem['COUNT_EDITION'],
            ];
            if (Tracker::periodDataTrack(
                'library_books',
                $trackParams
            )
            ) {
                $result['success']++;
            } else {
                $result['error']++;
            }
            $trackParams['value_num'] = $lastItem['COUNT_EDITION_DIG'];
            if (Tracker::periodDataTrack(
                'library_books_digitized',
                $trackParams
            )
            ) {
                $result['success']++;
            } else {
                $result['error']++;
            }
            echo "success: {$result['success']}, errors: {$result['error']}\r";
        }
        $query
            = "
SELECT
  SUM(COUNT_EDITION)                 COUNT_EDITION,
  SUM(COUNT_EDITION_DIG)             COUNT_EDITION_DIG,
  DATE_FORMAT(DATE_STAT, '%Y-%m-%d') DATE_STAT
FROM neb_stat_edition
WHERE DATE_STAT < '$dateTo'
      AND ID_EXALED > 0
GROUP BY DATE_STAT
ORDER BY DATE_STAT desc;
        ";
        $dbResult = $DB->Query($query);
        $lastItem = [
            'COUNT_EDITION'     => 0,
            'COUNT_EDITION_DIG' => 0,
        ];
        while ($item = $dbResult->Fetch()) {
            if ((integer)$item['COUNT_EDITION'] > 0) {
                $lastItem['COUNT_EDITION'] = (integer)$item['COUNT_EDITION'];
            }
            if ((integer)$item['COUNT_EDITION_DIG'] > 0) {
                $lastItem['COUNT_EDITION_DIG']
                    = (integer)$item['COUNT_EDITION_DIG'];
            }
            $trackParams = [
                'date'      => $item['DATE_STAT'],
                'value_num' => $lastItem['COUNT_EDITION'],
            ];
            if (Tracker::periodDataTrack(
                'library_books',
                $trackParams
            )
            ) {
                $result['success']++;
            } else {
                $result['error']++;
            }
            $trackParams['value_num'] = $lastItem['COUNT_EDITION_DIG'];
            if (Tracker::periodDataTrack(
                'library_books_digitized',
                $trackParams
            )
            ) {
                $result['success']++;
            } else {
                $result['error']++;
            }
            echo "success: {$result['success']}, errors: {$result['error']}\r";
        }
        echo PHP_EOL;
        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}