<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 16.02.2017
 * Time: 18:01
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult['BOOK'])) {

    global $APPLICATION;

    $APPLICATION->SetTitle($arResult['BOOK']['META']['TITLE']);
    $APPLICATION->SetPageProperty("description", $arResult['BOOK']['META']['DESCRIPTION']);
    $APPLICATION->SetPageProperty("keywords", $arResult['BOOK']['META']['KEYWORDS']);
}
