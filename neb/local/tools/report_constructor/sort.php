<?
/**
 * Вкладка с сортировками
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!TblEntities::checkAccess()) {
    die('Permissions denied!');
}

if ($_REQUEST['entities']){

    $massFields = array();
    $aggregate= TblEntities::getAgregateType();// направление сортировки
    $orderType = TblEntities::getOrderType();
    foreach ($_REQUEST['entities'] as $item) {
        $res = TblEntities::getEntities($item);
        $massEntities[$item] = TblEntities::getEntities($item);
        $arFields = TblEntities::getEntityFields($item);
        $massFields += $arFields ;
        ?>
        <?foreach ($arFields as $field):?>
        <div class="steps__row row clearfix">
            <div class="form-group col-sm-1" style="width: 45px;padding-right: 0;">
                <a href="" class="sort-arrow sort-up">↑</a>
                <a href="" class="sort-arrow sort-down">↓</a>
            </div>
            <div class="form-group col-sm-3" style="padding-left: 0;">
                <label for="sort<?=$field['Id']?>">
                    <input type="checkbox" id="sort<?=$field['Id']?>" name="sort[]" value="<?=$field['Id']?>">
                    <span class="lbl"><?=$res[$item];?> - <?=$field['Description']?></span>
                </label>
            </div>
            <div class="form-group col-sm-2">
                <select id="" class="js-select form-control" name="order<?=$field['Id']?>" data-select-picker>
                    <?foreach ($orderType as $id=>$name):?>
                    <option value=<?=$id?>><?=$name?></option>
                    <?endforeach;?>
                </select>
            </div>
        </div>
        <?endforeach;?>
    <?
    }
    //выбор полей из агрегаций
    foreach ($_REQUEST['group'] as $field):
        ?>
        <input type='hidden' name='sort_agr<?=$field?>' value=<?=$_REQUEST['agr'.$field]?>>
        <div class="steps__row row clearfix">
            <div class="form-group col-sm-1" style="width: 45px;padding-right: 0;">
                <a href="" class="sort-arrow sort-up">↑</a>
                <a href="" class="sort-arrow sort-down">↓</a>
            </div>
            <div class="form-group col-sm-3" style="padding-left: 0;">
                <label for="group_sort<?=$field?>">
                    <input type="checkbox" id="group_sort<?=$field?>" name="sort[]" value="agr<?=$field?>">
                    <span class="lbl"><?=$aggregate[$_REQUEST['agr'.$field]]?>(<?=$massEntities[$massFields[$field]['IdEntity']][$massFields[$field]['IdEntity']];?> - <?=$massFields[$field]['Description']?>)</span>
                </label>
            </div>
            <div class="form-group col-sm-2">
                <select id="" class="js-select form-control" name="order_agr<?=$field?>" data-select-picker>
                    <?foreach ($orderType as $id=>$name):?>
                    <option value=<?=$id?>><?=$name?></option>
                    <?endforeach;?>
                </select>
            </div>
        </div>
        <?
    endforeach;
}
?>
