<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/prolog.php");
#IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/iblock/admin/iblock_list_admin.php');
IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/iblock/admin/iblock_section_search.php');
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/interface/admin_lib.php");

global $USER;

$bExcel = isset($_REQUEST["mode"]) && ($_REQUEST["mode"] == "excel");
$dsc_cookie_name = COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM') . "_DSC";


//Init variables
$IBLOCK_ID = IntVal($_GET["IBLOCK_ID"]);
$n = preg_replace("/[^a-zA-Z0-9_\\[\\]]/", "", $_GET["n"]);
$k = preg_replace("/[^a-zA-Z0-9_:]/", "", $_GET["k"]);
$lookup = preg_replace("/[^a-zA-Z0-9_:]/", "", $_GET["lookup"]);
$m = $_GET["m"] === "y";
$get_xml_id = $_GET["get_xml_id"] === "Y";
$strWarning = "";


$bSearch = false;
$bCurrency = false;
$arCurrencyList = array();
$minImageSize = array("W" => 1, "H" => 1);
$maxImageSize = array(
    "W" => COption::GetOptionString("iblock", "list_image_size"),
    "H" => COption::GetOptionString("iblock", "list_image_size"),
);

if (isset($_REQUEST['mode']) && ($_REQUEST['mode'] == 'list' || $_REQUEST['mode'] == 'frame')) {
    CFile::DisableJSFunction(true);
}

if ($IBLOCK_ID > 0)
    $res = CIBlock::GetByID($IBLOCK_ID);
if ($ar_res = $res->GetNext())
    $type = $ar_res['IBLOCK_TYPE_ID'];

$arIBTYPE = CIBlockType::GetByIDLang($type, LANGUAGE_ID);
if ($arIBTYPE === false)
    $APPLICATION->AuthForm(GetMessage("IBLIST_A_BAD_BLOCK_TYPE_ID"));

$IBLOCK_ID = IntVal($IBLOCK_ID);
$arIBlock = CIBlock::GetArrayByID($IBLOCK_ID);

if ($arIBlock)
    $bBadBlock = !CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "iblock_admin_display");
else
    $bBadBlock = true;

if ($bBadBlock) {
    $APPLICATION->SetTitle($arIBTYPE["NAME"]);
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    if ($bBadBlock):
        ?>
        <? echo ShowError(GetMessage("IBLIST_A_BAD_IBLOCK")); ?>
        <a href="<? echo htmlspecialcharsbx("iblock_admin.php?lang=" . LANGUAGE_ID . "&type=" . urlencode($type)) ?>"><? echo GetMessage("IBLOCK_BACK_TO_ADMIN") ?></a>
        <?
    endif;
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
    die();
}

if (!$arIBlock["SECTIONS_NAME"])
    $arIBlock["SECTIONS_NAME"] = $arIBTYPE["SECTION_NAME"] ? $arIBTYPE["SECTION_NAME"] : GetMessage("IBLIST_A_SECTIONS");
if (!$arIBlock["ELEMENTS_NAME"])
    $arIBlock["ELEMENTS_NAME"] = $arIBTYPE["ELEMENT_NAME"] ? $arIBTYPE["ELEMENT_NAME"] : GetMessage("IBLIST_A_ELEMENTS");

$arIBlock["SITE_ID"] = array();
$rsSites = CIBlock::GetSite($IBLOCK_ID);
while ($arSite = $rsSites->Fetch())
    $arIBlock["SITE_ID"][] = $arSite["LID"];


define("MODULE_ID", "iblock");
define("ENTITY", "CIBlockDocument");
define("DOCUMENT_TYPE", "iblock_" . $IBLOCK_ID);

$dbrFProps = CIBlockProperty::GetList(
    array(
        "SORT" => "ASC",
        "NAME" => "ASC"
    ),
    array(
        "IBLOCK_ID" => $IBLOCK_ID,
        "CHECK_PERMISSIONS" => "N",
    )
);

$arFileProps = array();
$arProps = array();
while ($arProp = $dbrFProps->GetNext()) {
    if ($arProp["ACTIVE"] == "Y") {
        $arProp["PROPERTY_USER_TYPE"] = ('' != $arProp["USER_TYPE"] ? CIBlockProperty::GetUserType($arProp["USER_TYPE"]) : array());
        $arProps[] = $arProp;
    }

    if ($arProp["PROPERTY_TYPE"] == "F") {
        $arFileProps[$arProp["ID"]] = $arProp;
    }
}


$sTableID = (defined("CATALOG_PRODUCT") ? "tbl_product_list_" : "tbl_iblock_list_") . md5($type . ".popup." . $IBLOCK_ID);
$oSort = new CAdminSorting($sTableID, "timestamp_x", "desc");
$arOrder = (strtoupper($by) === "ID" ? array($by => $order) : array($by => $order, "ID" => "ASC"));
$lAdmin = new CAdminList($sTableID, $oSort);
$lAdmin->bMultipart = true;
$arFilterFields = Array(
    "find_name",
    "find_section_section",
    "find_id_1", "find_id_2",
    "find_timestamp_1", "find_timestamp_2",
    "find_code",
    "find_external_id",
    "find_modified_by", "find_modified_user_id",
    "find_created_from", "find_created_to",
    "find_created_by", "find_created_user_id",
    "find_date_active_from_from", "find_date_active_from_to",
    "find_date_active_to_from", "find_date_active_to_to",
    "find_active",
    "find_intext",
    "find_status", "find_status_id",
    "find_tags",
);
foreach ($arProps as $arProp) {
    if ($arProp["FILTRABLE"] == "Y" && $arProp["PROPERTY_TYPE"] != "F")
        $arFilterFields[] = "find_el_property_" . $arProp["ID"];
}

//We have to handle current section in a special way
$section_id = intval($find_section_section);
$lAdmin->InitFilter($arFilterFields);
$find_section_section = $section_id;
//This is all parameters needed for proper navigation
$sThisSectionUrl = '&type=' . urlencode($type) . '&lang=' . LANGUAGE_ID . '&IBLOCK_ID=' . $IBLOCK_ID . '&find_section_section=' . intval($find_section_section);

$arFilter = Array(
    "IBLOCK_ID" => $IBLOCK_ID,
    "NAME" => $find_name,
    "SECTION_ID" => $find_section_section,
    "ID_1" => $find_id_1,
    "ID_2" => $find_id_2,
    "TIMESTAMP_X_1" => $find_timestamp_1,
    "CODE" => $find_code,
    "EXTERNAL_ID" => $find_external_id,
    "MODIFIED_BY" => $find_modified_by,
    "MODIFIED_USER_ID" => $find_modified_user_id,
    "DATE_CREATE_1" => $find_created_from,
    "CREATED_BY" => $find_created_by,
    "CREATED_USER_ID" => $find_created_user_id,
    "DATE_ACTIVE_FROM_1" => $find_date_active_from_from,
    "DATE_ACTIVE_FROM_2" => $find_date_active_from_to,
    "DATE_ACTIVE_TO_1" => $find_date_active_to_from,
    "DATE_ACTIVE_TO_2" => $find_date_active_to_to,
    "ACTIVE" => $find_active,
    "DESCRIPTION" => $find_intext,
    "WF_STATUS" => $find_status == "" ? $find_status_id : $find_status,
    "?TAGS" => $find_tags,
    "CHECK_PERMISSIONS" => "Y",
    "MIN_PERMISSION" => "R",
);
if (!empty($find_timestamp_2))
    $arFilter["TIMESTAMP_X_2"] = CIBlock::isShortDate($find_timestamp_2) ? ConvertTimeStamp(AddTime(MakeTimeStamp($find_timestamp_2), 1, "D"), "FULL") : $find_timestamp_2;
if (!empty($find_created_to))
    $arFilter["DATE_CREATE_2"] = CIBlock::isShortDate($find_created_to) ? ConvertTimeStamp(AddTime(MakeTimeStamp($find_created_to), 1, "D"), "FULL") : $find_created_to;

foreach ($arProps as $arProp) {
    if ($arProp["FILTRABLE"] == "Y" && $arProp["PROPERTY_TYPE"] != "F") {
        $value = ${"find_el_property_" . $arProp["ID"]};

        if (array_key_exists("AddFilterFields", $arProp["PROPERTY_USER_TYPE"])) {
            call_user_func_array($arProp["PROPERTY_USER_TYPE"]["AddFilterFields"], array(
                $arProp,
                array("VALUE" => "find_el_property_" . $arProp["ID"]),
                &$arFilter,
                &$filtered,
            ));
        } elseif (is_array($value) || strlen($value)) {
            if ($value === "NOT_REF")
                $value = false;
            $arFilter["?PROPERTY_" . $arProp["ID"]] = $value;
        }
    }
}

$arSubQuery = array();


if (IntVal($find_section_section) < 0 || strlen($find_section_section) <= 0)
    unset($arFilter["SECTION_ID"]);

// List header
$arHeader = array();
//Common
$arHeader[] = array(
    "id" => "NAME",
    "content" => GetMessage("IBLIST_A_NAME"),
    "sort" => "name",
    "default" => true,
);
$arHeader[] = array(
    "id" => "ACTIVE",
    "content" => GetMessage("IBLIST_A_ACTIVE"),
    "sort" => "active",
    "default" => true,
    "align" => "center",
);
$arHeader[] = array(
    "id" => "SORT",
    "content" => GetMessage("IBLIST_A_SORT"),
    "sort" => "sort",
    "default" => true,
    "align" => "right",
);
$arHeader[] = array(
    "id" => "CODE",
    "content" => GetMessage("IBLIST_A_CODE"),
    "sort" => "code",
);
$arHeader[] = array(
    "id" => "EXTERNAL_ID",
    "content" => GetMessage("IBLIST_A_EXTCODE"),
    "sort" => "external_id",
);
$arHeader[] = array(
    "id" => "TIMESTAMP_X",
    "content" => GetMessage("IBLIST_A_TIMESTAMP"),
    "sort" => "timestamp_x",
    "default" => true,
);
$arHeader[] = array(
    "id" => "USER_NAME",
    "content" => GetMessage("IBLIST_A_MODIFIED_BY"),
    "sort" => "modified_by",
);
$arHeader[] = array(
    "id" => "DATE_CREATE",
    "content" => GetMessage("IBLIST_A_DATE_CREATE"),
    "sort" => "created",
);
$arHeader[] = array(
    "id" => "CREATED_USER_NAME",
    "content" => GetMessage("IBLIST_A_CREATED_USER_NAME"),
    "sort" => "created_by",
);
$arHeader[] = array(
    "id" => "ID",
    "content" => GetMessage("IBLIST_A_ID"),
    "sort" => "id",
    "default" => true,
    "align" => "right",
);
//Section specific
$arHeader[] = array(
    "id" => "ELEMENT_CNT",
    "content" => GetMessage("IBLIST_A_ELS"),
    "sort" => "element_cnt",
    "align" => "right",
);
$arHeader[] = array(
    "id" => "SECTION_CNT",
    "content" => GetMessage("IBLIST_A_SECS"),
    "align" => "right",
);
//Element specific
$arHeader[] = array(
    "id" => "DATE_ACTIVE_FROM",
    "content" => GetMessage("IBLIST_A_DATE_ACTIVE_FROM"),
    "sort" => "date_active_from",
);
$arHeader[] = array(
    "id" => "DATE_ACTIVE_TO",
    "content" => GetMessage("IBLIST_A_DATE_ACTIVE_TO"),
    "sort" => "date_active_to",
);
$arHeader[] = array(
    "id" => "SHOW_COUNTER",
    "content" => GetMessage("IBLIST_A_SHOW_COUNTER"),
    "sort" => "show_counter",
    "align" => "right",
);
$arHeader[] = array(
    "id" => "SHOW_COUNTER_START",
    "content" => GetMessage("IBLIST_A_SHOW_COUNTER_START"),
    "sort" => "show_counter_start",
    "align" => "right",
);
$arHeader[] = array(
    "id" => "PREVIEW_PICTURE",
    "content" => GetMessage("IBLIST_A_PREVIEW_PICTURE"),
    "align" => "right",
    "sort" => "has_preview_picture"
);
$arHeader[] = array(
    "id" => "PREVIEW_TEXT",
    "content" => GetMessage("IBLIST_A_PREVIEW_TEXT"),
);
$arHeader[] = array(
    "id" => "DETAIL_PICTURE",
    "content" => GetMessage("IBLIST_A_DETAIL_PICTURE"),
    "align" => "right",
    "sort" => "has_detail_picture"
);
$arHeader[] = array(
    "id" => "DETAIL_TEXT",
    "content" => GetMessage("IBLIST_A_DETAIL_TEXT"),
);
$arHeader[] = array(
    "id" => "TAGS",
    "content" => GetMessage("IBLIST_A_TAGS"),
    "sort" => "tags",
);


foreach ($arProps as $arFProps) {
    $arHeader[] = array(
        "id" => "PROPERTY_" . $arFProps['ID'],
        "content" => $arFProps['NAME'],
        "align" => ($arFProps["PROPERTY_TYPE"] == 'N' ? "right" : "left"),
        "sort" => ($arFProps["MULTIPLE"] != 'Y' ? "PROPERTY_" . $arFProps['ID'] : ""),
    );
}

$lAdmin->AddHeaders($arHeader);

$arSelectedFields = $lAdmin->GetVisibleHeaderColumns();
$arSelectedProps = Array();
foreach ($arProps as $i => $arProperty) {
    $k = array_search("PROPERTY_" . $arProperty['ID'], $arSelectedFields);
    if ($k !== false) {
        $arSelectedProps[] = $arProperty;
        if ($arProperty["PROPERTY_TYPE"] == "L") {
            $arSelect[$arProperty['ID']] = Array();
            $rs = CIBlockProperty::GetPropertyEnum($arProperty['ID']);
            while ($ar = $rs->GetNext())
                $arSelect[$arProperty['ID']][$ar["ID"]] = $ar["VALUE"];
        } elseif ($arProperty["PROPERTY_TYPE"] == "G") {
            $arSelect[$arProperty['ID']] = Array();
            $rs = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $arProperty["LINK_IBLOCK_ID"]), array("ID", "NAME", "DEPTH_LEVEL"));
            while ($ar = $rs->GetNext())
                $arSelect[$arProperty['ID']][$ar["ID"]] = str_repeat(" . ", $ar["DEPTH_LEVEL"]) . $ar["NAME"];
        }
        unset($arSelectedFields[$k]);
    }
}

$arSelectedFields[] = "ID";
$arSelectedFields[] = "CREATED_BY";
$arSelectedFields[] = "LANG_DIR";
$arSelectedFields[] = "LID";
$arSelectedFields[] = "ACTIVE";

if (in_array("LOCKED_USER_NAME", $arSelectedFields))
    $arSelectedFields[] = "WF_LOCKED_BY";
if (in_array("USER_NAME", $arSelectedFields))
    $arSelectedFields[] = "MODIFIED_BY";
if (in_array("PREVIEW_TEXT", $arSelectedFields))
    $arSelectedFields[] = "PREVIEW_TEXT_TYPE";
if (in_array("DETAIL_TEXT", $arSelectedFields))
    $arSelectedFields[] = "DETAIL_TEXT_TYPE";

$arSelectedFields[] = "LOCK_STATUS";
$arSelectedFields[] = "DETAIL_PAGE_URL";
$arSelectedFields[] = "SITE_ID";
$arSelectedFields[] = "CODE";
$arSelectedFields[] = "EXTERNAL_ID";

$arVisibleColumnsMap = array();
foreach ($arSelectedFields as $value)
    $arVisibleColumnsMap[$value] = true;

// Getting list data
if (array_key_exists("ELEMENT_CNT", $arVisibleColumnsMap)) {
    $arFilter["CNT_ALL"] = "Y";
    $arFilter["ELEMENT_SUBSECTIONS"] = "N";
    $rsData = CIBlockSection::GetMixedList($arOrder, $arFilter, true, $arSelectedFields);
} else {
    $rsData = CIBlockSection::GetMixedList($arOrder, $arFilter, false, $arSelectedFields);
}

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

// Navigation setup
$lAdmin->NavText($rsData->GetNavPrint(htmlspecialcharsbx($arIBlock["SECTIONS_NAME"])));

$bSearch = CModule::IncludeModule('search');

function GetElementName($ID)
{
    $ID = intval($ID);
    static $cache = array();
    if (!isset($cache[$ID])) {
        $rsElement = CIBlockElement::GetList(array(), array("ID" => $ID, "SHOW_HISTORY" => "Y"), false, false, array("ID", "IBLOCK_ID", "NAME"));
        $cache[$ID] = $rsElement->GetNext();
    }
    return $cache[$ID];
}

function GetIBlockTypeID($IBLOCK_ID)
{
    $IBLOCK_ID = IntVal($IBLOCK_ID);
    if (0 > $IBLOCK_ID)
        $IBLOCK_ID = 0;
    static $cache = array();
    if (!isset($cache[$IBLOCK_ID])) {
        $rsIBlock = CIBlock::GetByID($IBLOCK_ID);
        if (!($cache[$IBLOCK_ID] = $rsIBlock->GetNext()))
            $cache[$IBLOCK_ID] = array("IBLOCK_TYPE_ID" => "");
    }
    return $cache[$IBLOCK_ID]["IBLOCK_TYPE_ID"];
}

$arUsersCache = array();

$boolIBlockElementAdd = CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $find_section_section, "section_element_bind");

$arRows = array();
$arElemID = array();

$arProductIDs = array();
$arProductGroupIDs = array();
$arMeasureList = array();
$arMeasureIDs = array();
$arCatalogRights = array();

// List build
while ($arRes = $rsData->NavNext(true, "f_")) {
    #$sec_list_url = htmlspecialcharsbx(CIBlock::GetAdminSectionListLink($IBLOCK_ID, array('find_section_section'=>$f_ID)));
    $sec_list_url = $APPLICATION->GetCurPageParam("find_section_section=" . $f_ID, array("find_section_section", "mode", "table_id", "find_name"));

    $el_edit_url = htmlspecialcharsbx(CIBlock::GetAdminElementEditLink($IBLOCK_ID, $f_ID, array('find_section_section' => intval($find_section_section), "WF" => "Y")));;
    $sec_edit_url = htmlspecialcharsbx(CIBlock::GetAdminSectionEditLink($IBLOCK_ID, $f_ID));

    $arRes_orig = $arRes;

    if ($f_TYPE == "S") {
        $bReadOnly = !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $f_ID, "section_edit");
    } else {
        $bReadOnly = !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $f_ID, "element_edit");
    }

    if ($f_TYPE == "S") // double click moves deeper
    {
        $arRes["PREVIEW_PICTURE"] = $arRes["PICTURE"];
        $row = $lAdmin->AddRow($f_TYPE . $f_ID, $arRes, $sec_list_url, GetMessage("IBLIST_A_LIST"));
    } else // in case of element take his action
    {
        $row = $lAdmin->AddRow($f_TYPE . $f_ID, $arRes);
        $arElemID[] = $f_ID;
    }
    $arRows[$f_TYPE . $f_ID] = $row;

    if ($f_TYPE == "S")
        $row->AddViewField("NAME", '<input type=hidden name="n' . $arRes["ID"] . '" id="name_' . $f_TYPE . $arRes["ID"] . '" value="' . CUtil::JSEscape(htmlspecialcharsbx($arRes["NAME"])) . '"/><a href="' . $sec_list_url . '" class="adm-list-table-icon-link" title="' . GetMessage("IBLIST_A_LIST") . '"><span class="adm-submenu-item-link-icon adm-list-table-icon iblock-section-icon"></span><span class="adm-list-table-link">' . $f_NAME . '</span></a>');
    else
        $row->AddViewField("NAME", '<input type=hidden name="n' . $arRes["ID"] . '" id="name_' . $f_TYPE . $arRes["ID"] . '" value="' . CUtil::JSEscape(htmlspecialcharsbx($arRes["NAME"])) . '"/>' . $f_NAME);

    $row->AddInputField("NAME", false);
    $row->AddCheckField("ACTIVE", false);
    $row->AddInputField("SORT", false);
    $row->AddInputField("CODE", false);
    $row->AddInputField("EXTERNAL_ID", false);


    if (array_key_exists("MODIFIED_BY", $arVisibleColumnsMap) && intval($f_MODIFIED_BY) > 0) {
        if (!array_key_exists($f_MODIFIED_BY, $arUsersCache)) {
            $rsUser = CUser::GetByID($f_MODIFIED_BY);
            $arUsersCache[$f_MODIFIED_BY] = $rsUser->Fetch();
        }
        if ($arUser = $arUsersCache[$f_MODIFIED_BY])
            $row->AddViewField("USER_NAME", '[<a href="user_edit.php?lang=' . LANGUAGE_ID . '&ID=' . $f_MODIFIED_BY . '" title="' . GetMessage("IBLIST_A_USERINFO") . '">' . $f_MODIFIED_BY . "</a>]&nbsp;(" . $arUser["LOGIN"] . ") " . $arUser["NAME"] . " " . $arUser["LAST_NAME"]);
    }

    if (array_key_exists("CREATED_BY", $arVisibleColumnsMap) && intval($f_CREATED_BY) > 0) {
        if (!array_key_exists($f_CREATED_BY, $arUsersCache)) {
            $rsUser = CUser::GetByID($f_CREATED_BY);
            $arUsersCache[$f_CREATED_BY] = $rsUser->Fetch();
        }
        if ($arUser = $arUsersCache[$f_CREATED_BY])
            $row->AddViewField("CREATED_USER_NAME", '[<a href="user_edit.php?lang=' . LANGUAGE_ID . '&ID=' . $f_CREATED_BY . '" title="' . GetMessage("IBLIST_A_USERINFO") . '">' . $f_CREATED_BY . "</a>]&nbsp;(" . $arUser["LOGIN"] . ") " . $arUser["NAME"] . " " . $arUser["LAST_NAME"]);
    }

    if (array_key_exists("PREVIEW_PICTURE", $arVisibleColumnsMap)) {
        $row->AddViewFileField("PREVIEW_PICTURE", array(
                "IMAGE" => "Y",
                "PATH" => "Y",
                "FILE_SIZE" => "Y",
                "DIMENSIONS" => "Y",
                "IMAGE_POPUP" => "Y",
                "MAX_SIZE" => $maxImageSize,
                "MIN_SIZE" => $minImageSize,
            )
        );
    }

    if (array_key_exists("DETAIL_PICTURE", $arVisibleColumnsMap)) {
        $row->AddViewFileField("DETAIL_PICTURE", array(
                "IMAGE" => "Y",
                "PATH" => "Y",
                "FILE_SIZE" => "Y",
                "DIMENSIONS" => "Y",
                "IMAGE_POPUP" => "Y",
                "MAX_SIZE" => $maxImageSize,
                "MIN_SIZE" => $minImageSize,
            )
        );
    }

    if ($f_TYPE == "S") {
        if (array_key_exists("ELEMENT_CNT", $arVisibleColumnsMap)) {
            $row->AddViewField("ELEMENT_CNT", $f_ELEMENT_CNT . '(' . IntVal(CIBlockSection::GetSectionElementsCount($f_ID, Array("CNT_ALL" => "Y"))) . ')');
        }

        if (array_key_exists("SECTION_CNT", $arVisibleColumnsMap)) {
            $arFilter = Array("IBLOCK_ID" => $IBLOCK_ID, "SECTION_ID" => $f_ID);
            $row->AddViewField("SECTION_CNT", " " . IntVal(CIBlockSection::GetCount($arFilter)));
        }
    }

    if ($f_TYPE == "E") {
        if (array_key_exists("PREVIEW_TEXT", $arVisibleColumnsMap))
            $row->AddViewField("PREVIEW_TEXT", ($arRes["PREVIEW_TEXT_TYPE"] == "text" ? htmlspecialcharsex($arRes["PREVIEW_TEXT"]) : HTMLToTxt($arRes["PREVIEW_TEXT"])));
        if (array_key_exists("DETAIL_TEXT", $arVisibleColumnsMap))
            $row->AddViewField("DETAIL_TEXT", ($arRes["DETAIL_TEXT_TYPE"] == "text" ? htmlspecialcharsex($arRes["DETAIL_TEXT"]) : HTMLToTxt($arRes["DETAIL_TEXT"])));


        $row->AddCalendarField("DATE_ACTIVE_FROM", false);
        $row->AddCalendarField("DATE_ACTIVE_TO", false);
        if (array_key_exists("TAGS", $arVisibleColumnsMap))
            $row->AddViewField("TAGS", $f_TAGS);

    }

    $row->AddViewField("ID", '<a href="' . ($f_TYPE == "S" ? $sec_edit_url : $el_edit_url) . '" title="' . GetMessage("IBLIST_A_EDIT") . '">' . $f_ID . '</a>');

    $arProperties = array();
    if ($f_TYPE == "E" && count($arSelectedProps) > 0) {
        $rsProperties = CIBlockElement::GetProperty($IBLOCK_ID, $arRes["ID"]);
        while ($ar = $rsProperties->GetNext()) {
            if (!array_key_exists($ar["ID"], $arProperties))
                $arProperties[$ar["ID"]] = array();
            $arProperties[$ar["ID"]][$ar["PROPERTY_VALUE_ID"]] = $ar;
        }

        foreach ($arSelectedProps as $aProp) {
            $arViewHTML = array();
            $arEditHTML = array();
            if (strlen($aProp["USER_TYPE"]) > 0)
                $arUserType = CIBlockProperty::GetUserType($aProp["USER_TYPE"]);
            else
                $arUserType = array();
            $max_file_size_show = 100000;

            $last_property_id = false;
            foreach ($arProperties[$aProp["ID"]] as $prop_id => $prop) {
                $prop['PROPERTY_VALUE_ID'] = intval($prop['PROPERTY_VALUE_ID']);
                $VALUE_NAME = 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][' . $prop['PROPERTY_VALUE_ID'] . '][VALUE]';
                $DESCR_NAME = 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][' . $prop['PROPERTY_VALUE_ID'] . '][DESCRIPTION]';
                //View part
                if (array_key_exists("GetAdminListViewHTML", $arUserType)) {
                    $arViewHTML[] = call_user_func_array($arUserType["GetAdminListViewHTML"],
                        array(
                            $prop,
                            array(
                                "VALUE" => $prop["~VALUE"],
                                "DESCRIPTION" => $prop["~DESCRIPTION"]
                            ),
                            array(
                                "VALUE" => $VALUE_NAME,
                                "DESCRIPTION" => $DESCR_NAME,
                                "MODE" => "iblock_element_admin",
                                "FORM_NAME" => "form_" . $sTableID,
                            ),
                        ));
                } elseif ($prop['PROPERTY_TYPE'] == 'N')
                    $arViewHTML[] = $bExcel && isset($_COOKIE[$dsc_cookie_name]) ? number_format($prop["VALUE"], 4, chr($_COOKIE[$dsc_cookie_name]), '') : $prop["VALUE"];
                elseif ($prop['PROPERTY_TYPE'] == 'S')
                    $arViewHTML[] = $prop["VALUE"];
                elseif ($prop['PROPERTY_TYPE'] == 'L')
                    $arViewHTML[] = $prop["VALUE_ENUM"];
                elseif ($prop['PROPERTY_TYPE'] == 'F') {
                    $arViewHTML[] = CFileInput::Show('NO_FIELDS[' . $prop['PROPERTY_VALUE_ID'] . ']', $prop["VALUE"], array(
                        "IMAGE" => "Y",
                        "PATH" => "Y",
                        "FILE_SIZE" => "Y",
                        "DIMENSIONS" => "Y",
                        "IMAGE_POPUP" => "Y",
                        "MAX_SIZE" => $maxImageSize,
                        "MIN_SIZE" => $minImageSize,
                    ), array(
                            'upload' => false,
                            'medialib' => false,
                            'file_dialog' => false,
                            'cloud' => false,
                            'del' => false,
                            'description' => false,
                        )
                    );
                } elseif ($prop['PROPERTY_TYPE'] == 'G') {
                    if (intval($prop["VALUE"]) > 0) {
                        $rsSection = CIBlockSection::GetList(
                            array(),
                            array("ID" => $prop["VALUE"]),
                            false,
                            array('ID', 'NAME', 'IBLOCK_ID')
                        );
                        if ($arSection = $rsSection->GetNext()) {
                            $arViewHTML[] = $arSection['NAME'] .
                                ' [<a href="' .
                                htmlspecialcharsbx(CIBlock::GetAdminSectionEditLink($arSection['IBLOCK_ID'], $arSection['ID'])) .
                                '" title="' . GetMessage("IBEL_A_SEC_EDIT") . '">' . $arSection['ID'] . '</a>]';
                        }
                    }
                } elseif ($prop['PROPERTY_TYPE'] == 'E') {
                    if ($t = GetElementName($prop["VALUE"])) {
                        $arViewHTML[] = $t['NAME'] .
                            ' [<a href="' . htmlspecialcharsbx(CIBlock::GetAdminElementEditLink($t['IBLOCK_ID'], $t['ID'], array(
                                "find_section_section" => $find_section_section,
                                'WF' => 'Y',
                            ))) . '" title="' . GetMessage("IBEL_A_EL_EDIT") . '">' . $t['ID'] . '</a>]';
                    }
                }
                //Edit Part
                $bUserMultiple = $prop["MULTIPLE"] == "Y" && array_key_exists("GetPropertyFieldHtmlMulty", $arUserType);
                if ($bUserMultiple) {
                    if ($last_property_id != $prop["ID"]) {
                        $VALUE_NAME = 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . ']';
                        $arEditHTML[] = call_user_func_array($arUserType["GetPropertyFieldHtmlMulty"], array(
                            $prop,
                            $arProperties[$prop["ID"]],
                            array(
                                "VALUE" => $VALUE_NAME,
                                "DESCRIPTION" => $VALUE_NAME,
                                "MODE" => "iblock_element_admin",
                                "FORM_NAME" => "form_" . $sTableID,
                            )
                        ));
                    }
                } elseif (array_key_exists("GetPropertyFieldHtml", $arUserType)) {
                    $arEditHTML[] = call_user_func_array($arUserType["GetPropertyFieldHtml"],
                        array(
                            $prop,
                            array(
                                "VALUE" => $prop["VALUE"],
                                "DESCRIPTION" => $prop["DESCRIPTION"],
                            ),
                            array(
                                "VALUE" => $VALUE_NAME,
                                "DESCRIPTION" => $DESCR_NAME,
                                "MODE" => "iblock_element_admin",
                                "FORM_NAME" => "form_" . $sTableID,
                            ),
                        ));
                } elseif ($prop['PROPERTY_TYPE'] == 'N' || $prop['PROPERTY_TYPE'] == 'S') {
                    if ($prop["ROW_COUNT"] > 1)
                        $html = '<textarea name="' . $VALUE_NAME . '" cols="' . $prop["COL_COUNT"] . '" rows="' . $prop["ROW_COUNT"] . '">' . $prop["VALUE"] . '</textarea>';
                    else
                        $html = '<input type="text" name="' . $VALUE_NAME . '" value="' . $prop["VALUE"] . '" size="' . $prop["COL_COUNT"] . '">';
                    if ($prop["WITH_DESCRIPTION"] == "Y")
                        $html .= ' <span title="' . GetMessage("IBLIST_A_PROP_DESC_TITLE") . '">' . GetMessage("IBLIST_A_PROP_DESC") .
                            '<input type="text" name="' . $DESCR_NAME . '" value="' . $prop["DESCRIPTION"] . '" size="18"></span>';
                    $arEditHTML[] = $html;
                } elseif ($prop['PROPERTY_TYPE'] == 'L' && ($last_property_id != $prop["ID"])) {
                    $VALUE_NAME = 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][]';
                    $arValues = array();
                    foreach ($arProperties[$prop["ID"]] as $g_prop) {
                        $g_prop = intval($g_prop["VALUE"]);
                        if ($g_prop > 0)
                            $arValues[$g_prop] = $g_prop;
                    }
                    if ($prop['LIST_TYPE'] == 'C') {
                        if ($prop['MULTIPLE'] == "Y" || count($arSelect[$prop['ID']]) == 1) {
                            $html = '<input type="hidden" name="' . $VALUE_NAME . '" value="">';
                            foreach ($arSelect[$prop['ID']] as $value => $display) {
                                $html .= '<input type="checkbox" name="' . $VALUE_NAME . '" id="id' . $uniq_id . '" value="' . $value . '"';
                                if (array_key_exists($value, $arValues))
                                    $html .= ' checked';
                                $html .= '>&nbsp;<label for="id' . $uniq_id . '">' . $display . '</label><br>';
                                $uniq_id++;
                            }
                        } else {
                            $html = '<input type="radio" name="' . $VALUE_NAME . '" id="id' . $uniq_id . '" value=""';
                            if (count($arValues) < 1)
                                $html .= ' checked';
                            $html .= '>&nbsp;<label for="id' . $uniq_id . '">' . GetMessage("IBLIST_A_PROP_NOT_SET") . '</label><br>';
                            $uniq_id++;
                            foreach ($arSelect[$prop['ID']] as $value => $display) {
                                $html .= '<input type="radio" name="' . $VALUE_NAME . '" id="id' . $uniq_id . '" value="' . $value . '"';
                                if (array_key_exists($value, $arValues))
                                    $html .= ' checked';
                                $html .= '>&nbsp;<label for="id' . $uniq_id . '">' . $display . '</label><br>';
                                $uniq_id++;
                            }
                        }
                    } else {
                        $html = '<select name="' . $VALUE_NAME . '" size="' . $prop["MULTIPLE_CNT"] . '" ' . ($prop["MULTIPLE"] == "Y" ? "multiple" : "") . '>';
                        $html .= '<option value=""' . (count($arValues) < 1 ? ' selected' : '') . '>' . GetMessage("IBLIST_A_PROP_NOT_SET") . '</option>';
                        foreach ($arSelect[$prop['ID']] as $value => $display) {
                            $html .= '<option value="' . $value . '"';
                            if (array_key_exists($value, $arValues))
                                $html .= ' selected';
                            $html .= '>' . $display . '</option>' . "\n";
                        }
                        $html .= "</select>\n";
                    }
                    $arEditHTML[] = $html;
                } elseif ($prop['PROPERTY_TYPE'] == 'F' && ($last_property_id != $prop["ID"])) {
                    if ($prop['MULTIPLE'] == "Y") {
                        $inputName = array();
                        foreach ($arProperties[$prop["ID"]] as $g_prop) {
                            $inputName['FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][' . $g_prop['PROPERTY_VALUE_ID'] . '][VALUE]'] = $g_prop["VALUE"];
                        }

                        $arEditHTML[] = CFileInput::ShowMultiple($inputName, 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][n#IND#]', array(
                            "IMAGE" => "Y",
                            "PATH" => "Y",
                            "FILE_SIZE" => "Y",
                            "DIMENSIONS" => "Y",
                            "IMAGE_POPUP" => "Y",
                            "MAX_SIZE" => $maxImageSize,
                            "MIN_SIZE" => $minImageSize,
                        ), false, array(
                                'upload' => true,
                                'medialib' => false,
                                'file_dialog' => false,
                                'cloud' => false,
                                'del' => true,
                                'description' => $prop["WITH_DESCRIPTION"] == "Y",
                            )
                        );
                    } else {
                        $arEditHTML[] = CFileInput::Show($VALUE_NAME, $prop["VALUE"], array(
                            "IMAGE" => "Y",
                            "PATH" => "Y",
                            "FILE_SIZE" => "Y",
                            "DIMENSIONS" => "Y",
                            "IMAGE_POPUP" => "Y",
                            "MAX_SIZE" => $maxImageSize,
                            "MIN_SIZE" => $minImageSize,
                        ), array(
                                'upload' => true,
                                'medialib' => false,
                                'file_dialog' => false,
                                'cloud' => false,
                                'del' => true,
                                'description' => $prop["WITH_DESCRIPTION"] == "Y",
                            )
                        );
                    }
                } elseif (($prop['PROPERTY_TYPE'] == 'G') && ($last_property_id != $prop["ID"])) {
                    $VALUE_NAME = 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][]';
                    $arValues = array();
                    foreach ($arProperties[$prop["ID"]] as $g_prop) {
                        $g_prop = intval($g_prop["VALUE"]);
                        if ($g_prop > 0)
                            $arValues[$g_prop] = $g_prop;
                    }
                    $html = '<select name="' . $VALUE_NAME . '" size="' . $prop["MULTIPLE_CNT"] . '" ' . ($prop["MULTIPLE"] == "Y" ? "multiple" : "") . '>';
                    $html .= '<option value=""' . (count($arValues) < 1 ? ' selected' : '') . '>' . GetMessage("IBLIST_A_PROP_NOT_SET") . '</option>';
                    foreach ($arSelect[$prop['ID']] as $value => $display) {
                        $html .= '<option value="' . $value . '"';
                        if (array_key_exists($value, $arValues))
                            $html .= ' selected';
                        $html .= '>' . $display . '</option>' . "\n";
                    }
                    $html .= "</select>\n";
                    $arEditHTML[] = $html;
                } elseif ($prop['PROPERTY_TYPE'] == 'E') {
                    $VALUE_NAME = 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][' . $prop['PROPERTY_VALUE_ID'] . ']';
                    if ($t = GetElementName($prop["VALUE"])) {
                        $arEditHTML[] = '<input type="text" name="' . $VALUE_NAME . '" id="' . $VALUE_NAME . '" value="' . $prop["VALUE"] . '" size="5">' .
                            '<input type="button" value="..." onClick="jsUtils.OpenWindow(\'iblock_element_search.php?lang=' . LANGUAGE_ID . '&amp;IBLOCK_ID=' . $prop["LINK_IBLOCK_ID"] . '&amp;n=' . urlencode($VALUE_NAME) . '\', 600, 500);">' .
                            '&nbsp;<span id="sp_' . $VALUE_NAME . '" >' . $t['NAME'] . '</span>';
                    } else {
                        $arEditHTML[] = '<input type="text" name="' . $VALUE_NAME . '" id="' . $VALUE_NAME . '" value="" size="5">' .
                            '<input type="button" value="..." onClick="jsUtils.OpenWindow(\'iblock_element_search.php?lang=' . LANGUAGE_ID . '&amp;IBLOCK_ID=' . $prop["LINK_IBLOCK_ID"] . '&amp;n=' . urlencode($VALUE_NAME) . '\', 600, 500);">' .
                            '&nbsp;<span id="sp_' . $VALUE_NAME . '" ></span>';
                    }
                }
                $last_property_id = $prop['ID'];
            }
            $table_id = md5($f_TYPE . $f_ID . ':' . $aProp['ID']);
            if ($aProp["MULTIPLE"] == "Y") {
                $VALUE_NAME = 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][n0][VALUE]';
                $DESCR_NAME = 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][n0][DESCRIPTION]';
                if (array_key_exists("GetPropertyFieldHtmlMulty", $arUserType)) {
                } elseif (array_key_exists("GetPropertyFieldHtml", $arUserType)) {
                    $arEditHTML[] = call_user_func_array($arUserType["GetPropertyFieldHtml"],
                        array(
                            $prop,
                            array(
                                "VALUE" => "",
                                "DESCRIPTION" => "",
                            ),
                            array(
                                "VALUE" => $VALUE_NAME,
                                "DESCRIPTION" => $DESCR_NAME,
                                "MODE" => "iblock_element_admin",
                                "FORM_NAME" => "form_" . $sTableID,
                            ),
                        ));
                } elseif ($prop['PROPERTY_TYPE'] == 'N' || $prop['PROPERTY_TYPE'] == 'S') {
                    if ($prop["ROW_COUNT"] > 1)
                        $html = '<textarea name="' . $VALUE_NAME . '" cols="' . $prop["COL_COUNT"] . '" rows="' . $prop["ROW_COUNT"] . '"></textarea>';
                    else
                        $html = '<input type="text" name="' . $VALUE_NAME . '" value="" size="' . $prop["COL_COUNT"] . '">';
                    if ($prop["WITH_DESCRIPTION"] == "Y")
                        $html .= ' <span title="' . GetMessage("IBLIST_A_PROP_DESC_TITLE") . '">' . GetMessage("IBLIST_A_PROP_DESC") . '<input type="text" name="' . $DESCR_NAME . '" value="" size="18"></span>';
                    $arEditHTML[] = $html;
                } elseif ($prop['PROPERTY_TYPE'] == 'F') {
                } elseif ($prop['PROPERTY_TYPE'] == 'E') {
                    $VALUE_NAME = 'FIELDS[' . $f_TYPE . $f_ID . '][PROPERTY_' . $prop['ID'] . '][n0]';
                    $arEditHTML[] = '<input type="text" name="' . $VALUE_NAME . '" id="' . $VALUE_NAME . '" value="" size="5">' .
                        '<input type="button" value="..." onClick="jsUtils.OpenWindow(\'iblock_element_search.php?lang=' . LANGUAGE_ID . '&amp;IBLOCK_ID=' . $prop["LINK_IBLOCK_ID"] . '&amp;n=' . urlencode($VALUE_NAME) . '\', 600, 500);">' .
                        '&nbsp;<span id="sp_' . $VALUE_NAME . '" ></span>';
                }

                if (
                    $prop["PROPERTY_TYPE"] !== "G"
                    && $prop["PROPERTY_TYPE"] !== "L"
                    && $prop["PROPERTY_TYPE"] !== "F"
                    && !$bUserMultiple
                )
                    $arEditHTML[] = '<input type="button" value="' . GetMessage("IBLIST_A_PROP_ADD") . '" onClick="addNewRow(\'tb' . $table_id . '\')">';
            }
            if (count($arViewHTML) > 0) {
                if ($prop["PROPERTY_TYPE"] == "F")
                    $row->AddViewField("PROPERTY_" . $aProp['ID'], implode("", $arViewHTML));
                else
                    $row->AddViewField("PROPERTY_" . $aProp['ID'], implode(" / ", $arViewHTML));
            }
            if (!$bReadOnly && count($arEditHTML) > 0)
                $row->AddEditField("PROPERTY_" . $aProp['ID'], '<table id="tb' . $table_id . '" border=0 cellpadding=0 cellspacing=0><tr><td nowrap>' . implode("</td></tr><tr><td nowrap>", $arEditHTML) . '</td></tr></table>');
        }
    }

    $arActions = Array();
    if ($f_TYPE == "E") {
        $arActions[] = array(
            "DEFAULT" => "Y",
            "TEXT" => GetMessage("IBLOCK_SECSEARCH_SELECT"),
            "ACTION" => "javascript:SelEl('" . ($f_ID) . "', '" . htmlspecialcharsbx($jsPath . htmlspecialcharsbx(CUtil::JSEscape($arRes["NAME"]), ENT_QUOTES)) . "&nbsp;/&nbsp;" . "')",
        );
    }
    $row->AddActions($arActions);
}

// List footer
$lAdmin->AddFooter(
    array(
        array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()),
        array("counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"),
    )
);

/*	if($m)
 {
     $lAdmin->AddGroupActionTable(array(
         array(
             "action" => "SelAll()",
             "value" => "select",
             "type" => "button",
             "name" => GetMessage("IBLOCK_SECSEARCH_SELECT"),
         )
         ), array("disable_action_target"=>true));
 }*/

$lAdmin->AddAdminContextMenu(array(), false);

if ($IBLOCK_ID > 0) {
    $chain = $lAdmin->CreateChain();
    if (intval($find_section_section) > 0) {
        $nav = CIBlockSection::GetNavChain($IBLOCK_ID, $find_section_section);
        while ($ar_nav = $nav->GetNext()) {
            if ($find_section_section == $ar_nav["ID"]) {
                $chain->AddItem(array(
                    "TEXT" => $ar_nav["NAME"],
                ));
            } else {
                $chain->AddItem(array(
                    "TEXT" => $ar_nav["NAME"],
                    "LINK" => 'nota_iblock_sections_element_search.php?lang=' . LANG . '&amp;IBLOCK_ID=' . $IBLOCK_ID . '&amp;find_section_section=-1' . '&amp;n=' . urlencode($n) . '&amp;lookup=' . urlencode($lookup) . '&amp;k=' . urlencode($k) . ($m ? "&amp;m=y" : ""),
                    "ONCLICK" => $lAdmin->ActionAjaxReload('nota_iblock_sections_element_search.php?lang=' . LANG . '&IBLOCK_ID=' . $IBLOCK_ID . '&find_section_section=' . $ar_nav["ID"] . '&n=' . urlencode($n) . '&amp;lookup=' . urlencode($lookup) . '&k=' . urlencode($k) . ($m ? "&m=y" : "")) . ';return false;',
                ));
            }
        }
    }
    $lAdmin->ShowChain($chain);
} else {
    $lAdmin->BeginPrologContent();
    $message = new CAdminMessage(array("MESSAGE" => GetMessage("IBLOCK_SECSEARCH_CHOOSE_IBLOCK"), "TYPE" => "OK"));
    echo $message->Show();
    $lAdmin->EndPrologContent();
}


$lAdmin->CheckListMode();

$APPLICATION->SetTitle($arIBlock["NAME"] . ": " . ($arIBTYPE["SECTIONS"] == "Y" ? $arIBlock["SECTIONS_NAME"] : $arIBlock["ELEMENTS_NAME"]));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");

$chain = new CAdminChain("main_navchain");
$chain->AddItem(array(
    "TEXT" => htmlspecialcharsex($arIBlock["NAME"]),
    "LINK" => 'nota_iblock_sections_element_search.php?lang=' . LANG . '&amp;IBLOCK_ID=' . $IBLOCK_ID . '&amp;find_section_section=0' . '&amp;n=' . urlencode($n) . '&amp;lookup=' . urlencode($lookup) . '&amp;k=' . urlencode($k) . ($m ? "&amp;m=y" : ""),
    "ONCLICK" => $lAdmin->ActionAjaxReload('nota_iblock_sections_element_search.php?lang=' . LANG . '&IBLOCK_ID=' . $IBLOCK_ID . '&find_section_section=0' . '&n=' . urlencode($n) . '&amp;lookup=' . urlencode($lookup) . '&k=' . urlencode($k) . ($m ? "&m=y" : "")) . ';return false;',
));
$chain->Show();
?><br/>
    <form method="GET" name="find_form" id="find_form" action="<?= $APPLICATION->GetCurPage() ?>">
        <?
        $arFindFields = Array();
        $arFindFields["IBLIST_A_PARENT"] = GetMessage("IBLIST_A_PARENT");
        $arFindFields["IBLIST_A_ID"] = GetMessage("IBLIST_A_ID");
        $arFindFields["IBLIST_A_TS"] = GetMessage("IBLIST_A_TS");
        $arFindFields["IBLIST_A_CODE"] = GetMessage("IBLIST_A_CODE");
        $arFindFields["IBLIST_A_EXTCODE"] = GetMessage("IBLIST_A_EXTCODE");
        $arFindFields["IBLIST_A_F_MODIFIED_BY"] = GetMessage("IBLIST_A_F_MODIFIED_BY");
        $arFindFields["IBLIST_A_F_CREATED_WHEN"] = GetMessage("IBLIST_A_F_CREATED_WHEN");
        $arFindFields["IBLIST_A_F_CREATED_BY"] = GetMessage("IBLIST_A_F_CREATED_BY");
        $arFindFields["IBLIST_A_F_ACTIVE_FROM"] = GetMessage("IBLIST_A_DATE_ACTIVE_FROM");
        $arFindFields["IBLIST_A_F_ACTIVE_TO"] = GetMessage("IBLIST_A_DATE_ACTIVE_TO");
        $arFindFields["IBLIST_A_ACT"] = GetMessage("IBLIST_A_ACTIVE");
        $arFindFields["IBLIST_A_F_DESC"] = GetMessage("IBLIST_A_F_DESC");
        $arFindFields["IBLIST_A_TAGS"] = GetMessage("IBLIST_A_TAGS");

        foreach ($arProps as $arProp)
            if ($arProp["FILTRABLE"] == "Y" && $arProp["PROPERTY_TYPE"] != "F")
                $arFindFields["IBLIST_A_PROP_" . $arProp["ID"]] = $arProp["NAME"];


        $filterUrl = $APPLICATION->GetCurPageParam(); //$APPLICATION->GetCurPage().'?type='.urlencode($type).'&IBLOCK_ID='.urlencode($IBLOCK_ID).'&lang='.urlencode(LANG);
        $oFilter = new CAdminFilter($sTableID . "_filter", $arFindFields, array("table_id" => $sTableID, "url" => $filterUrl));
        ?>
        <script type="text/javascript">
            var arClearHiddenFields = new Array();
            function applyFilter(el) {
                BX.adminPanel.showWait(el);
                <?=$sTableID . "_filter";?>.
                OnSet('<?=CUtil::JSEscape($sTableID)?>', '<?=CUtil::JSEscape($filterUrl)?>');
                return false;
            }

            function deleteFilter(el) {
                BX.adminPanel.showWait(el);
                if (0 < arClearHiddenFields.length) {
                    for (var index = 0; index < arClearHiddenFields.length; index++) {
                        if (undefined != window[arClearHiddenFields[index]]) {
                            if ('ClearForm' in window[arClearHiddenFields[index]]) {
                                window[arClearHiddenFields[index]].ClearForm();
                            }
                        }
                    }
                }
                <?=$sTableID . "_filter"?>.
                OnClear('<?=CUtil::JSEscape($sTableID)?>', '<?=CUtil::JSEscape($APPLICATION->GetCurPage() . '?type=' . urlencode($type) . '&IBLOCK_ID=' . $IBLOCK_ID . '&lang=' . LANGUAGE_ID . '&')?>');
                return false;
            }

            try {
                var DecimalSeparator = Number("1.2").toLocaleString().charCodeAt(1);
                document.cookie = '<?echo $dsc_cookie_name?>=' + DecimalSeparator + '; path=/;';
            } catch (e) {
            }
        </script><?
        $oFilter->Begin();
        ?>

        <script type="text/javascript">
            function SelEl(id, name) {
                <?
                    if ('' != $lookup)
                    {
                        if ('' != $m)
                        {
                        ?>window.opener.<? echo $lookup; ?>.AddValue(id);<?
                }
                else
                {
                ?>
                window.opener.<? echo $lookup; ?>.AddValue(id);
                window.close();<?
                }
                }
                else
                {
                ?><?if($m):?>
                window.opener.InS<? echo md5($n)?>(id, name);
                <?else:?>
                el = window.opener.document.getElementById('<?echo $n?>[<?echo $k?>]');
                if (!el)
                    el = window.opener.document.getElementById('<?echo $n?>');
                if (el) {
                    el.value = id;
                    if (window.opener.BX)
                        window.opener.BX.fireEvent(el, 'change');
                }
                el = window.opener.document.getElementById('sp_<?echo md5($n)?>_<?echo $k?>');
                if (!el)
                    el = window.opener.document.getElementById('sp_<?echo $n?>');
                if (!el)
                    el = window.opener.document.getElementById('<?echo $n?>_link');
                if (el)
                    el.innerHTML = name;
                window.close();
                <?endif;?><?
                }
                ?>
            }

            function SelAll() {
                var frm = document.getElementById('form_<?echo $sTableID?>');
                if (frm) {
                    var e = frm.elements['ID[]'];
                    if (e && e.nodeName) {
                        var v = e.value;
                        var n = document.getElementById('name_' + v).value;
                        SelEl(v, n);
                    }
                    else if (e) {

                        var l = e.length;
                        for (i = 0; i < l; i++) {
                            var a = e[i].checked;
                            if (a == true) {
                                var v = e[i].value;
                                /*console.log(v);*/
                                var n = document.getElementById('name_' + v).value;
                                SelEl(v, n);
                            }
                        }
                    }
                    window.close();
                }
            }
        </script>

        <tr>
            <td><b><? echo GetMessage("IBLIST_A_NAME") ?></b></td>
            <td><input type="text" name="find_name" value="<? echo htmlspecialcharsex($find_name) ?>"
                       size="47">&nbsp;<?= ShowFilterLogicHelp() ?></td>
        </tr>
        <tr>
            <td><? echo GetMessage("IBLIST_A_F_SECTION") ?></td>
            <td>
                <select name="find_section_section">
                    <option value="-1"><? echo GetMessage("IBLOCK_ALL") ?></option>
                    <option
                        value="0"<? if ($find_section_section == "0") echo " selected" ?>><? echo GetMessage("IBLOCK_UPPER_LEVEL") ?></option>
                    <?
                    $bsections = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));
                    while ($arSection = $bsections->GetNext()):
                        ?>
                        <option value="<? echo $arSection["ID"] ?>"<? if ($arSection["ID"] == $find_section_section) echo " selected" ?>><? echo str_repeat("&nbsp;.&nbsp;", $arSection["DEPTH_LEVEL"]) ?><? echo $arSection["NAME"] ?></option><?
                    endwhile;
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><? echo GetMessage("IBLIST_A_F_FROM_TO_ID") ?></td>
            <td nowrap>
                <input type="text" name="find_id_1" size="10" value="<? echo htmlspecialcharsex($find_id_1) ?>">
                ...
                <input type="text" name="find_id_2" size="10" value="<? echo htmlspecialcharsex($find_id_2) ?>">
            </td>
        </tr>
        <tr>
            <td><? echo GetMessage("IBLOCK_FIELD_TIMESTAMP_X") ?>:</td>
            <td><? echo CalendarPeriod("find_timestamp_1", htmlspecialcharsbx($find_timestamp_1), "find_timestamp_2", htmlspecialcharsbx($find_timestamp_2), "find_form", "Y") ?></td>
        </tr>
        <tr>
            <td><? echo GetMessage("IBLIST_A_CODE") ?>:</td>
            <td><input type="text" name="find_code" size="47" value="<? echo htmlspecialcharsbx($find_code) ?>"></td>
        </tr>
        <tr>
            <td><? echo GetMessage("IBLIST_A_EXTCODE") ?>:</td>
            <td><input type="text" name="find_external_id" size="47"
                       value="<? echo htmlspecialcharsbx($find_external_id) ?>"></td>
        </tr>
        <tr>
            <td><?= GetMessage("IBLIST_A_F_MODIFIED_BY") ?>:</td>
            <td>
                <? echo FindUserID(
                    "find_modified_user_id",
                    $find_modified_user_id,
                    "",
                    "find_form",
                    "5",
                    "",
                    " ... ",
                    "",
                    ""
                ); ?>
            </td>
        </tr>

        <tr>
            <td><? echo GetMessage("IBLIST_A_DATE_CREATE") ?>:</td>
            <td><? echo CalendarPeriod("find_created_from", htmlspecialcharsex($find_created_from), "find_created_to", htmlspecialcharsex($find_created_to), "find_element_form", "Y") ?></td>
        </tr>

        <tr>
            <td><? echo GetMessage("IBLIST_A_F_CREATED_BY") ?>:</td>
            <td>
                <? echo FindUserID(
                    "find_created_user_id",
                    $find_created_user_id,
                    "",
                    "find_form",
                    "5",
                    "",
                    " ... ",
                    "",
                    ""
                ); ?>
            </td>
        </tr>


        <tr>
            <td><? echo GetMessage("IBLIST_A_DATE_ACTIVE_FROM") ?>:</td>
            <td><? echo CalendarPeriod("find_date_active_from_from", htmlspecialcharsex($find_date_active_from_from), "find_date_active_from_to", htmlspecialcharsex($find_date_active_from_to), "find_form") ?></td>
        </tr>
        <tr>
            <td><? echo GetMessage("IBLIST_A_DATE_ACTIVE_TO") ?>:</td>
            <td><? echo CalendarPeriod("find_date_active_to_from", htmlspecialcharsex($find_date_active_to_from), "find_date_active_to_to", htmlspecialcharsex($find_date_active_to_to), "find_form") ?></td>
        </tr>
        <tr>
            <td><? echo GetMessage("IBLIST_A_ACTIVE") ?>:</td>
            <td>
                <select name="find_active">
                    <option value=""><?= htmlspecialcharsex(GetMessage('IBLOCK_VALUE_ANY')) ?></option>
                    <option
                        value="Y"<? if ($find_active == "Y") echo " selected" ?>><?= htmlspecialcharsex(GetMessage("IBLOCK_YES")) ?></option>
                    <option
                        value="N"<? if ($find_active == "N") echo " selected" ?>><?= htmlspecialcharsex(GetMessage("IBLOCK_NO")) ?></option>
                </select>
            </td>
        </tr>
        <tr>
            <td><? echo GetMessage("IBLIST_A_F_DESC") ?>:</td>
            <td><input type="text" name="find_intext" value="<? echo htmlspecialcharsex($find_intext) ?>" size="30">&nbsp;<?= ShowFilterLogicHelp() ?>
            </td>
        </tr>
        <tr>
            <td><?= GetMessage("IBLIST_A_TAGS") ?>:</td>
            <td>
                <?
                if ($bSearch):
                    echo InputTags("find_tags", $find_tags, $arIBlock["SITE_ID"]);
                else:
                    ?>
                    <input type="text" name="find_tags" value="<? echo htmlspecialcharsex($find_tags) ?>" size="30">
                <? endif ?>
            </td>
        </tr>
        <?

        function _ShowGroupPropertyFieldList($name, $property_fields, $values)
        {
            if (!is_array($values)) $values = Array();

            $res = "";
            $result = "";
            $bWas = false;
            $sections = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $property_fields["LINK_IBLOCK_ID"]), array("ID", "NAME", "DEPTH_LEVEL"));
            while ($ar = $sections->GetNext()) {
                $res .= '<option value="' . $ar["ID"] . '"';
                if (in_array($ar["ID"], $values)) {
                    $bWas = true;
                    $res .= ' selected';
                }
                $res .= '>' . str_repeat(" . ", $ar["DEPTH_LEVEL"]) . $ar["NAME"] . '</option>';
            }
            $result .= '<select name="' . $name . '[]" size="' . ($property_fields["MULTIPLE"] == "Y" ? "5" : "1") . '" ' . ($property_fields["MULTIPLE"] == "Y" ? "multiple" : "") . '>';
            $result .= '<option value=""' . (!$bWas ? ' selected' : '') . '>' . GetMessage("IBLIST_A_PROP_NOT_SET") . '</option>';
            $result .= $res;
            $result .= '</select>';
            return $result;
        }

        foreach ($arProps as $arProp):
            if ($arProp["FILTRABLE"] == "Y" && $arProp["PROPERTY_TYPE"] != "F"):
                ?>
                <tr>
                    <td><?= $arProp["NAME"] ?>:</td>
                    <td>
                        <? if (array_key_exists("GetAdminFilterHTML", $arProp["PROPERTY_USER_TYPE"])):
                            echo call_user_func_array($arProp["PROPERTY_USER_TYPE"]["GetAdminFilterHTML"], array(
                                $arProp,
                                array(
                                    "VALUE" => "find_el_property_" . $arProp["ID"],
                                    "TABLE_ID" => $sTableID,
                                ),
                            ));
                        elseif ($arProp["PROPERTY_TYPE"] == 'S'):?>
                            <input type="text" name="find_el_property_<?= $arProp["ID"] ?>"
                                   value="<? echo htmlspecialcharsex(${"find_el_property_" . $arProp["ID"]}) ?>"
                                   size="30">&nbsp;<?= ShowFilterLogicHelp() ?>
                        <?
                        elseif ($arProp["PROPERTY_TYPE"] == 'N' || $arProp["PROPERTY_TYPE"] == 'E'):?>
                            <input type="text" name="find_el_property_<?= $arProp["ID"] ?>"
                                   value="<? echo htmlspecialcharsex(${"find_el_property_" . $arProp["ID"]}) ?>"
                                   size="30">
                        <?
                        elseif ($arProp["PROPERTY_TYPE"] == 'L'):?>
                            <select name="find_el_property_<?= $arProp["ID"] ?>">
                                <option value=""><? echo GetMessage("IBLOCK_VALUE_ANY") ?></option>
                                <option value="NOT_REF"><? echo GetMessage("IBLIST_A_PROP_NOT_SET") ?></option><?
                                $dbrPEnum = CIBlockPropertyEnum::GetList(Array("SORT" => "ASC", "NAME" => "ASC"), Array("PROPERTY_ID" => $arProp["ID"]));
                                while ($arPEnum = $dbrPEnum->GetNext()):
                                    ?>
                                    <option
                                        value="<?= $arPEnum["ID"] ?>"<? if (${"find_el_property_" . $arProp["ID"]} == $arPEnum["ID"]) echo " selected" ?>><?= $arPEnum["VALUE"] ?></option>
                                    <?
                                endwhile;
                                ?></select>
                            <?
                        elseif ($arProp["PROPERTY_TYPE"] == 'G'):
                            echo _ShowGroupPropertyFieldList('find_el_property_' . $arProp["ID"], $arProp, ${'find_el_property_' . $arProp["ID"]});
                        endif;
                        ?>
                    </td>
                </tr>
                <?
            endif;
        endforeach; ?>
        <?

        $oFilter->Buttons();
        ?><input class="adm-btn" type="submit" name="set_filter"
                 value="<? echo GetMessage("admin_lib_filter_set_butt"); ?>"
                 title="<? echo GetMessage("admin_lib_filter_set_butt_title"); ?>" onClick="return applyFilter(this);">
        <input class="adm-btn" type="submit" name="del_filter"
               value="<? echo GetMessage("admin_lib_filter_clear_butt"); ?>"
               title="<? echo GetMessage("admin_lib_filter_clear_butt_title"); ?>"
               onClick="deleteFilter(this); return false;">
        <?
        $oFilter->End();
        ?>
    </form>
<?
$lAdmin->DisplayList();

echo ShowError($strWarning);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
?>