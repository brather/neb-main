<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

require_once 'utils.php';
require_once 'meta.php';

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Config\Option,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Application,

    \Neb\Main\Helper\DbHelper,
    \Neb\Main\Helper\UtilsHelper,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient,
    \Nota\Exalead\RslViewer,
    \Nota\UserData\Books;

/**
 * Class SearchPageDetailComponent - страница издания
 *
 * TODO: Перенести в метод self::getBookParts так же и функционал компонента exalead:book.parts (двойной запрос)
 * TODO: Учесть, что getBookParts() получает всю разметку для мироформатов, а exalead:book.parts постранично !!!
 */
class SearchPageDetailComponent extends CBitrixComponent
{
    private $sBookId = '', $obUser;

    public function __construct($component)
    {
        Loc::loadMessages(__FILE__);
        $this->obUser = new nebUser();
        parent::__construct($component);
    }

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->includeModules(['nota.exalead', 'nota.userdata', 'highloadblock']);
        $this->prepareParams();
        $this->setBookId();
        $this->setUser();
        $this->executeQuery();
        $this->showTemplate();
    }

    /**
     * Подключение модулей
     *
     * @param $arModules - массив модулей
     * @throws \Bitrix\Main\LoaderException
     */
    private static function includeModules($arModules) {
        foreach ($arModules as $sModule) {
            if (!Loader::includeModule($sModule))
                die('Module ' . $sModule . 'not installed!');
        }
    }

    /**
     * Формирование параметров запроса к Exalead
     */
    private function prepareParams() {

        $arParams = $this->arParams;

        $arDefaultUrlTemplates404 = array(
            "list"   => "/",
            "detail" => "#ELEMENT_ID#/"
        );

        $arDefaultVariableAliases404 = $arDefaultVariableAliases = $arUrlTemplates = $arUrlPublication = array();

        $arComponentVariables = array("ELEMENT_ID");

        $SEF_FOLDER = "";

        if ($arParams["SEF_MODE"] == "Y") {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

            $componentPage = CComponentEngine::parseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

            if (mb_strlen($componentPage) <= 0)
                $componentPage = "list";

            CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

            $SEF_FOLDER = $arParams["SEF_FOLDER"];

        } else {

            $arVariables = array();

            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
            CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

            if (!empty($arVariables["ELEMENT_ID"]) > 0) {
                $componentPage = "detail";
            } else {
                $componentPage = "list";
            }
        }

        $arResult = [
            "FOLDER"          => $SEF_FOLDER,
            "URL_TEMPLATES"   => $arUrlTemplates,
            "VARIABLES"       => $arVariables,
            "ALIASES"         => $arVariableAliases
        ];

        if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' || $componentPage == 'viewer') {
            $arParams['RestartBuffer'] = true;
        }

        $arParams['COMPONENT_PAGE'] = $componentPage;

        $this->arParams = $arParams;
        $this->arResult = $arResult;
    }

    /**
     * Устанавливает идентификатор книги из URL в поле класса
     */
    private function setBookId() {

        $sBookId = $this->arResult['VARIABLES']['BOOK_ID'];
        $sBookId = urldecode(trim($sBookId));
        $sBookId = str_replace('!|', '/', $sBookId);

        $this->sBookId = $sBookId;
    }

    /**
     * Установка текущего пользователя
     */
    private function setUser() {

        if (!$this->obUser->IsAuthorized())
            return;

        $this->arResult['isAuthorized']  = $this->obUser->IsAuthorized();
        $this->arResult['isInWorkplace'] = $this->obUser->isInWorkplace() && $this->obUser->IsAuthorized();
        $this->arResult['nebUser']       = $this->obUser->getUser();
        $this->arResult['USER_GROUPS']   = $this->obUser->getUserGroups();
        $this->arResult['USER_STATUS']   = $this->obUser->getStatus();
    }

    /**
     * Исполнение запроса к Exalead
     */
    public function executeQuery()
    {
        $arParams = $this->arParams;
        $arResult = $this->arResult;

        $arResult['BOOK']           = self::getBookById($this->sBookId);
        $arResult['BOOK']['parent'] = self::getBookParentId($this->sBookId);
        $arResult['BOOK']['META']   = BookMeta::getInstance()->get($arResult['BOOK'], $arParams['COMPONENT_PAGE'] == 'viewer');

        if (empty($arResult['BOOK']['id']))
            return false;

        // получаем книгу из сервиса выдачи
        $obRslViewer = new RslViewer($this->sBookId);
        $arResult['BOOK']['AccessType'] = $obRslViewer->isAvailableType();

        // перезапись более актуального признака доступа из сервиса выдачи
        switch ($arResult['BOOK']['AccessType']) {
            case 'open':    $arResult['BOOK']['isprotected'] = 0; break;
            case 'close':   $arResult['BOOK']['isprotected'] = 1; break;
            case 'blocked': $arResult['BOOK']['isprotected'] = 2; break;
        }

        // проверяем доступна ли книга для просмотра в пдф
        if ($arResult['BOOK']['isprotected'] <= 0 || $this->obUser->isShowProtectedContent()) {

            $isAvailable = ($arResult['BOOK']['filesize'] > 0 ? true : false);
            #$isAvailable = $arResult['BOOK']['IS_READ'] == 'Y';

            #if($isAvailable === true)
            # $arResult['VIEWER_URL'] = str_replace('#BOOK_ID#', urlencode($this->sBookId), $arResult['FOLDER'].$arResult['URL_TEMPLATES']['viewer']);
            $arResult['VIEWER_URL'] = $arResult['BOOK']['VIEWER_URL'];
            $arResult['PDF_AVAILABLE'] = $isAvailable;
        }

        $arParams['IS_SHOW_PROTECTED'] = $this->obUser->isShowProtectedContent();

        // получение битриксовой библиотеки
        $arLibrary = self::getBxLib($arResult['BOOK']['idlibrary']);
        $arResult['BOOK']['UF_LIBRARY'] = $arLibrary['ID'];

        // =========================================== режим дет. страницы ===========================================
        if ($arParams['COMPONENT_PAGE'] == 'detail') {

            $arResult['BOOK']['parts']  = self::getBookParts($arResult['BOOK']['id']);

            // формирование ссылки на медиа-файл
            // TODO: закрыть через /local/tools/exalead/getFiles.php
            // TODO: также закрыть в /local/components/exalead/search.page/templates/.default/template.php
            if ("Y" === $arResult['BOOK']['IS_MEDIA']) {
                $arResult['BOOK']['MEDIA_URL'] = Option::get("nota.exalead", "viewer_protocol")
                    . '://'. Option::get("nota.exalead", "viewer_ip") . ':' . Option::get("nota.exalead", "viewer_port")
                    . '/' . $arResult['BOOK']['id'] . '/resources/' . $arResult['BOOK']['fileExt'][0];
            }

            // получаем ссылку на страницу библиотеки к которой привязана книга
            $arResult['BOOK']['LIBRARY_DETAIL_PAGE_URL'] = $arLibrary['DETAIL_PAGE_URL'];

            // ссылка на библиотеку
            $librariesUrlParams = array();
            $arResult['BOOK']['NEAREST_LIBRARY_URL'] = '';
            if (isset($arResult['BOOK']['idparent2']) && !empty($arResult['BOOK']['idparent2'])) {
                $librariesUrlParams['book_idparent2'] = $arResult['BOOK']['idparent2'];
            } elseif (isset($arResult['BOOK']['idlibrary']) && !empty($arResult['BOOK']['idlibrary'])) {
                $librariesUrlParams['idlibrary'] = $arResult['BOOK']['idlibrary'];
            }
            if (!empty($librariesUrlParams)) {
                $librariesUrlParams['json'] = 'Y';
                $arResult['BOOK']['NEAREST_LIBRARY_URL'] = '/library/?' . http_build_query($librariesUrlParams);
            }

            if (!UtilsHelper::checkSearcherBot()) {

                \Nota\Exalead\Stat\BookView::set(
                    $this->sBookId,
                    isset($arResult['BOOK']['idlibrary']) ? $arResult['BOOK']['idlibrary'] : 0
                );

                $arResult['SEARCH_QUERY'] = \Neb\Main\Stat\StatBookOpenTable::extractSearchQuery(
                    $_SERVER['HTTP_REFERER']
                );
                \Neb\Main\Stat\StatBookOpenTable::add(
                    array(
                        'BOOK_ID' => $this->sBookId,
                        'REFERER' => $_SERVER['HTTP_REFERER'],
                        'SEARCH_QUERY' => $arResult['SEARCH_QUERY'],
                    )
                );
                \Neb\Main\Stat\Tracker::track(
                    'neb_book_card_view',
                    [
                        'book_id' => $this->sBookId,
                        'session_id' => session_id(),
                        'closed' => 'open' !== $arResult['BOOK']['AccessType'],
                        'user_id' => intval($this->arResult['nebUser']['ID']),
                    ]
                );
            }
        }
        // =========================================== режим просмотровщика ===========================================
        elseif ($arParams['COMPONENT_PAGE'] == 'viewer') {

            // если книга не оцифрована, либо закрыта, то перенаправляем на её карточку
            if ('Y' !== $arResult['BOOK']['IS_READ'] || $arResult['BOOK']['isprotected'] > 0) {
                LocalRedirect($arResult['BOOK']['DETAIL_PAGE_URL']);
            }

            $arResult['BOOK']['LIBRARY_ID'] = $arLibrary['ID'];

            // количество страниц
            $arResult['BOOK']['pageCount'] = $obRslViewer->getPagesCount();
            list($arResult['BOOK']['width']) = explode("x", $obRslViewer->getGeometry(1));

            // список произведений издания из сервиса выдачи
            $arChParams = !empty($this->arResult['nebUser']['UF_TOKEN'])
                ? ['token' => $this->arResult['nebUser']['UF_TOKEN']] : [];
            $arResult['BOOK']['markup'] = $obRslViewer->getMarkup($arChParams);

            // список доступных действий формы
            if (in_array(UGROUP_MARKUP_EDITOR, $this->arResult['USER_GROUPS'])
                || in_array(UGROUP_MARKUP_APPROVER, $this->arResult['USER_GROUPS'])) {
                $arResult['BOOK']['actions'][] = 'add';
                $arResult['BOOK']['actions'][] = 'delete';
            }
            if (in_array(UGROUP_MARKUP_APPROVER, $this->arResult['USER_GROUPS'])) {
                foreach ($arResult['BOOK']['markup'] as $arItem) {
                    if ($arItem['status'] == 'added' || $arItem['status'] == 'consideration') {
                        $arResult['BOOK']['actions'][] = 'approve';
                        $arResult['BOOK']['actions'][] = 'decline';
                    }
                }
            }

            setcookie("pageWidth", intval($arResult['BOOK']['width']), time() + 3600, '/');

            if (!UtilsHelper::checkSearcherBot()) {
                $arResult['USER_BOOKS'] = Books::getListCurrentUser($this->sBookId);
                \Nota\Exalead\Stat\BookRead::set($this->sBookId);
            }
        }

        // пока исключено из кода, т.к. запрос долго отрабатывает
        //$books = self::getBookStat($this->sBookId);
        //$arResult['readWith'] = self::getBooksByIds($books);

        $this->arParams = $arParams;
        $this->arResult = $arResult;
    }

    /** =========================================== СЛУЖЕБНЫЕ МЕТОДЫ =============================================== */

    /**
     * Получение издания по его идентификатору
     *
     * @param $sBookId
     * @param array $arParams
     * @return array|bool|mixed|null|string
     */
    private static function getBookById($sBookId, $arParams = []) {

        $arResult = [];

        if (empty($sBookId))
            return $arResult;

        $query = new SearchQuery();
        $query->getById($sBookId);
        foreach ($arParams as $k => $v) {
            $query->setParam($k, $v);
        }

        $client = new SearchClient();
        $arResult = $client->getResult($query);

        return $arResult;
    }

    /**
     * Получение книг по массиву идентификаторов
     *
     * @param $arBook
     * @param int $iLimit
     * @return array|bool|mixed|null|string
     */
    private static function getBooksByIds($arBook, $iLimit = 10) {

        $arResult = [];

        if (empty($arBook))
            return $arResult;

        $query = new SearchQuery();
        $query->getByArIds($arBook);
        $query->setPageLimit($iLimit);

        $client = new SearchClient();
        $arResult = $client->getResult($query);

        return $arResult;
    }

    /**
     * Получение произведений книги по её идентификатору
     *
     * @param $sBookId
     * @return array|bool|mixed|null|string
     */
    private static function getBookParts($sBookId) {

        $arResult = [];

        if (empty($sBookId))
            return $arResult;

        $query = new SearchQuery();
        $query->setQuery('idbook:"' . $sBookId . '"');
        $query->setPageLimit(10000);
        $query->setParam('s', 'asc(document_startpageparts)');

        $client = new SearchClient();
        $arResult = $client->getResult($query);

        if (isset($arResult['ITEMS']) && is_array($arResult['ITEMS'])) {
            usort(
                $arResult['ITEMS'],
                function ($a, $b) {
                    return intval($a['startpageparts']) < intval($b['startpageparts']) ? -1 : 1;
                }
            );
        }

        return $arResult;
    }

    /**
     * Получает родительскую книгу по FullSymbolicId из БД Библиотеки
     * Используется для задания canonical url для исключения дубликаций книг
     * TODO: переделать на сервис выдачи
     *
     * @param $sBookId
     * @return string
     */
    private static function getBookParentId($sBookId) {

        if (empty($sBookId))
            return "";

        $sQuery = "SELECT
          parent.FullSymbolicId
        FROM
          tbl_common_biblio_card AS book
            JOIN tbl_common_biblio_card AS parent ON parent.Id = book.IdParent
        WHERE
          book.FullSymbolicId = '$sBookId';";

        $connection   = DbHelper::getLibraryConnection();
        $arParentBook = $connection->query($sQuery)->fetchRaw();
        $sResult      = strval($arParentBook['FullSymbolicId']);

        return $sResult;
    }

    /**
     * Получение библиотеки в Битрикс по идентификатору Exalead
     *
     * @param $idLibrary
     * @return mixed
     */
    private static function getBxLib($idLibrary) {

        $idLibrary = intval($idLibrary);

        if ($idLibrary <= 0)
            return '';

        $obNebLibrary = new nebLibrary();
        $arLibrary    = $obNebLibrary->getBitrixID('=PROPERTY_LIBRARY_LINK', $idLibrary);

        return $arLibrary['ITEM'];
    }

    /**
     * Получение статистики по книге
     *
     * @param $sBookId
     * @return array
     */
    private static function getBookStat($sBookId) {

        $arResult = [];

        if (empty($sBookId))
            return $arResult;

        $sQuery = <<<SQL
SELECT
  r.BOOK_ID,
  COUNT(*) AS c
FROM
  (
    SELECT
      UID_GUEST,
      BOOK_ID
    FROM neb_stat_book_read
    WHERE BOOK_ID = '#BOOK_ID#'
  ) uid_session
INNER JOIN
  neb_stat_book_read r ON uid_session.UID_GUEST = r.UID_GUEST
WHERE
  r.BOOK_ID != uid_session.BOOK_ID
GROUP BY
  r.BOOK_ID
ORDER BY
  c DESC
LIMIT 10
SQL;
        $sQuery = str_replace('#BOOK_ID#', DbHelper::mysqlEscapeString($sBookId), $sQuery);
        $rsData = DbHelper::getBitrixConnection()->query($sQuery);
        while ($arGroup = $rsData->fetch())
            $arResult[] = $arGroup['BOOK_ID'];

        return $arResult;
    }

    /**
     * Подключает шаблон
     */
    private function showTemplate() {

        global $APPLICATION;

        // 404-я ошибка
        if (empty($this->arResult['BOOK']['id'])) {

            @define("ERROR_404", "Y");
            CHTTP::SetStatus("404 Not Found");

            if (false !== stripos($APPLICATION->GetCurDir(), '/blind/')) {
                $this->includeComponentTemplate($this->arParams['COMPONENT_PAGE']);
            } else {
                echo '<h1>'.Loc::getMessage('PUBLICATION_NOT_FOUND').'</h1>';
                require_once $_SERVER['DOCUMENT_ROOT'] . '/404.php';
            }
        }
        // показ шаблона
        else {
            if ($this->arParams['RestartBuffer'] === true) {
                $APPLICATION->RestartBuffer();
            }

            $this->includeComponentTemplate($this->arParams['COMPONENT_PAGE']);

            if ($this->arParams['RestartBuffer'] === true) {
                exit();
            }
        }
    }
}