<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode"><?=GetMessage("REG_RGB_TITLE");?></h2>
		<form action="" class="b-form b-form_common b-regform" method="post" id="regform">
			<?
			if($arResult['CHECK_STATUS'] === false)
			{
			?>
			<div class="nookresult">
				<p class="error"><?if(!empty($arResult['ERROR_MESSAGE'])){echo $arResult['ERROR_MESSAGE'];}else echo GetMessage("REG_RGB_ERROR_RGB_DATA_NOT_FOUND");?></p>
				<p><a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['full']?>" class="black"><?=GetMessage("REG_RGB_ERROR_COMPLETE_FULL_REG");?></a></p>
			</div>
			<?
			}
			?>
			<?=bitrix_sessid_post()?>
			<hr/>


	</div><!-- /.b-registration-->
</section>