<?php
$MESS['DIG_PLAN_EDIT_C_NO_AUTHORIZED'] = 'Error! Login to access the functionality of editing digitization plan.';
$MESS['DIG_PLAN_EDIT_C_NO_ROLE'] = 'Error! You do not have enough access rights to the functionality of editing digitization plan.';
$MESS['DIG_PLAN_EDIT_C_EMPTY_BOOK_ID'] = 'Error! ID of plan is not obtained.';