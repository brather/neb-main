<?php
$MESS['NAME'] = 'Имя';
$MESS['NAME_TITLE'] = 'Название';
$MESS['FULL_NAME'] = 'ФИО';
$MESS['FIND'] = 'Найти';
$MESS['EDIT'] = 'Редактировать';
$MESS['DELETE'] = 'Удалить';
$MESS['ACTION'] = 'Действие';
$MESS['CONFIRM'] = 'Подтвердить';
$MESS['REFUSE'] = 'Отказать';
$MESS['CONFIRMED'] = 'Подтверждено';
$MESS['REFUSED'] = 'Отказано';