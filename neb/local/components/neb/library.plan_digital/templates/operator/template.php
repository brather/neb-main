<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));
?>
<style>
    .plus_ico {
        display: inline-block;
        width: 18px;
        height: 18px;
        background: url("/local/templates/adaptive/img/icons.png") no-repeat 0 -533px;
        margin-left: 34px;
        margin-top: 3px;
        cursor: pointer;
    }
    .minus .plus_ico { background-position: 0 -563px; }
</style>
<div class="b-mainblock left">
    <div class="b-searchresult clearfix">
        <? $APPLICATION->ShowViewContent('menu_top') ?>
    </div>
    <!-- /.b-searchresult-->
    <form action="<?= $APPLICATION->GetCurPage() ?>" method="GET" name="form1">
        <div>
            Библиотека
            <select name="library" class="form-control">
                <option value="0">Все</option>
                <? foreach ($arResult['LIBS'] as $id => $lib_name): ?>
                    <option value="<?= $id ?>" <? if ($_REQUEST['library']
                    == $id): ?>selected<? endif; ?>><?= $lib_name ?></option>
                <? endforeach; ?>
            </select>
        </div>
        <br>

        <div>
            C <input type="text" id="date-from" name="from" size="10"
                     value="<?= $_GET['from'] ?>"
                     class="b-text datepicker datepicker-from">
            по <input type="text" name="to" id="date-to" size="10"
                      value="<?= $_GET['to'] ?>"
                      class="b-text datepicker datepicker-current">

            <input type="submit" class="btn btn-primary" value="Обновить"
                   style='float:none;'>
        </div>
    </form>
    <br>

    <div class="b-digitizing_actions">
        <a class="btn btn-primary" target="_blank"
           href="<?= $APPLICATION->GetCurPageParam(
               'print=Y', array('print')
           ); ?>"><?= GetMessage('LIBRARY_PLAN_DIGITAL_PRINT'); ?></a>
        <a class="btn btn-primary" target="_blank"
           href="<?= $APPLICATION->GetCurPageParam(
               'export=Y', array('export')
           ); ?>"><?= GetMessage('LIBRARY_PLAN_DIGITAL_EXPORT_EXCEL'); ?></a>
    </div>
    <br>

    <div class="b-add_digital js_digital">
        <?
        if ($arParams['ajax']) {
            $APPLICATION->RestartBuffer();
        }
        ?>

        <div class="lk-table">
            <div class="lk-table__column" style="width:55%"></div>
            <div class="lk-table__column" style="width:15%"></div>
            <div class="lk-table__column" style="width:15%"></div>
            <div class="lk-table__column" style="width:15%"></div>
            <div class="lk-table__column" style="width:15%"></div>
            <div class="lk-table__column" style="width:15%"></div>
            <ul class="lk-table__header">
                <li class="lk-table__header-kind">
                    Библиотека
                </li>
                <li class="lk-table__header-kind">
                    <?= GetMessage('LIBRARY_PLAN_DIGITAL_AUTHOR'); ?>
                </li>
                <li class="lk-table__header-kind">
                    <?= GetMessage('LIBRARY_PLAN_DIGITAL_NAMEDIG'); ?>
                </li>
                <li class="lk-table__header-kind">
                    <a <?= SortingExalead(
                        "UF_DATE_ADD", 'by1', 'order1', 'nav_start',
                        array('by', 'order')
                    ) ?>><?= GetMessage('LIBRARY_PLAN_DIGITAL_DATE_ADD'); ?></a>
                </li>
                <li class="lk-table__header-kind">
                    <?= GetMessage('LIBRARY_PLAN_DIGITAL_DATEDIG'); ?>
                </li>
                <li class="lk-table__header-kind">
                    <?= GetMessage('LIBRARY_PLAN_DIGITAL_COMMENT'); ?>
                </li>
                <li class="lk-table__header-kind">
                    <?= GetMessage('LIBRARY_PLAN_DIGITAL_REMOVE_FROM_PLAN'); ?>
                </li>
            </ul>
            <section class="lk-table__body">
                <? if (!empty($arResult['BOOKS'])) {
                    foreach ($arResult['BOOKS'] as $arItem) {
                        ?>
                        <ul class="lk-table__row item-container" id="<?=$arItem['BOOK_ID']?>">
                            <li class="lk-table__col">
                                <?= $arResult['LIBS'][$arItem['LIBRARY_ID']]; ?>
                            </li>
                            <li class="lk-table__col">
                                <?= $arResult['ITEMS'][$arItem['BOOK_ID']]['authorbook'] ?>
                            </li>
                            <li class="lk-table__col">
                                <? if (isset($arResult['ITEMS'][$arItem['BOOK_ID']])): ?>
                                    <?= $arResult['ITEMS'][$arItem['BOOK_ID']]['title'] ?>
                                <? else:?>
                                    Книга с идентификатором <?= $arItem['BOOK_ID'] ?> не найдена в НЭБ
                                <? endif; ?>
                            </li>
                            <li class="lk-table__col">
                                <?= $arItem['DATE_ADD'] ?>
                            </li>
                            <li class="lk-table__col">
                                <?= $arItem['DATE_FINISH'] ?>
                            </li>
                            <li class="lk-table__col">
                                <?= $arItem['COMMENT'] ?>
                            </li>
                            <li class="lk-table__col">
                                <div
                                    class="rel plusico_wrap plan-digitalization minus">
                                    <div class="plus_ico"></div>
                                    <div class="b-hint del"><a
                                            href="#"><?= GetMessage(
                                                'LIBRARY_PLAN_DIGITAL_REMOVE'
                                            ); ?></a> <?= GetMessage(
                                            'LIBRARY_PLAN_DIGITAL_FROM_PLAN'
                                        ); ?></div>
                                </div>
                            </li>
                        </ul>
                        <div class="collapsed" style="display:none;">
                            <div class="b-infobox rel b-infoboxdescr"
                                 data-link="descr">
                                <a href="#" class="close"></a>
                                <?
                                if (!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['authorbook'])) {
                                    ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= GetMessage(
                                                'LIBRARY_PLAN_DIGITAL_AUTHOR'
                                            ); ?>: </span>
                                        <span
                                            class="iblock val"><?= $arResult['ITEMS'][$arItem['BOOK_ID']]['authorbook'] ?></span>
                                    </div>
                                    <?
                                }
                                ?>
                                <div class="b-infoboxitem">
                                    <span class="tit iblock"><?= GetMessage(
                                            'LIBRARY_PLAN_DIGITAL_HEADING'
                                        ); ?>: </span>
                                    <span
                                        class="iblock val"><?= $arResult['ITEMS'][$arItem['BOOK_ID']]['title'] ?></span>
                                </div>
                                <?
                                if (!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['year'])) {
                                    ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= GetMessage(
                                                'LIBRARY_PLAN_DIGITAL_OUTPUT'
                                            ); ?>: </span>
                                        <span
                                            class="iblock val"><?= $arResult['ITEMS'][$arItem['BOOK_ID']]['year'] ?>
                                            г.</span>
                                    </div>
                                    <?
                                }
                                if (!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['countpages'])) {
                                    ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= GetMessage(
                                                'LIBRARY_PLAN_DIGITAL_PHYS_DESC'
                                            ); ?>: </span>
                                        <span
                                            class="iblock val"><?= $arResult['ITEMS'][$arItem['BOOK_ID']]['countpages'] ?>
                                            с.</span>
                                    </div>
                                    <?
                                }
                                if (!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['library'])) {
                                    ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?= GetMessage(
                                                'LIBRARY_PLAN_DIGITAL_LIB'
                                            ); ?>: </span>
                                        <span
                                            class="iblock val"><?= $arResult['ITEMS'][$arItem['BOOK_ID']]['library'] ?></span>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                            <!-- /b-infobox -->
                            <?
                            if (!empty($arResult['BOOKS_OTHER'][$arItem['BOOK_ID']])) {
                                ?>
                                <div class="b-infobox rel" data-link="digital">
                                    <a href="#" class="close"></a>
                                    <?
                                    foreach (
                                        $arResult['BOOKS_OTHER'][$arItem['BOOK_ID']]
                                        as $arOther
                                    ) {
                                        ?>
                                        <div
                                            class="b-infoboxitem b-infoboxmain">
                                            <span
                                                class="tit iblock"><?= GetMessage(
                                                    'LIBRARY_PLAN_DIGITAL_WHO_DIG'
                                                ); ?></span>
                                            <span
                                                class="iblock val"><strong><?= $arOther['NAME'] ?></strong><span
                                                    class="date">до <?= $arOther['DATE'] ?></span></span>
                                        </div>

                                        <?
                                    }
                                    ?>

                                </div><!-- /b-infobox -->
                                <?
                            }
                            ?>
                        </div>
                    <? }
                } ?>
            </section>
        </div>
        <?= htmlspecialcharsBack($arResult['STR_NAV']) ?>
    </div>

</div><!-- /.b-mainblock -->


