<?php
$MESS = array(
    'LIBRARY_STAT_REFRESH'        => 'Обновить',
    'LIBRARY_STAT_EXPORT'         => 'Выгрузить по всем библиотекам',
    'LIBRARY_PUBLICATIONS_EXPORT' => 'Комплексный отчет по ИР',
    'STAT_COMPLEX_DATE_FROM'      => 'Дата отчета',
    'STAT_COMPLEX_DATE_TO'        => 'Дата предыдущего отчета',
    'STAT_QUERY_EXPORT'           => 'Отчёт по запросам и изданиям',
    'STAT_QUERY_EXPORT_EXCEL'     => 'Экспорт статистики в формат Excel',
);