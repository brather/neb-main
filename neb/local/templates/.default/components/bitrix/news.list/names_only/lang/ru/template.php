<?php
$MESS['FIND_CLOSEST_LIBRARY'] = 'Выбранный город ';
$MESS['CHANGE'] = '(изменить)';
$MESS['ADDRESS_OR_NAME'] = 'Введите адрес или название библиотеки';
$MESS['SEARCH_BTN'] = 'Найти';
$MESS['MEMBER_OF_NEL'] = 'Участник проекта НЭБ';
$MESS['VRR'] = 'ЭЧЗ';
$MESS['ADDRESS'] = 'Адрес';
$MESS['SHOW_ON_THE_MAP'] = 'Показать на карте';

