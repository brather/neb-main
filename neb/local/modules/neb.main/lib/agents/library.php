<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 23.05.2017
 * Time: 18:07
 */

namespace Neb\Main\Agents;

use \Bitrix\Main\Loader,
    \Neb\Main\Stat\NebStatEditionTable;

/**
 * Class Library
 *
 * @package Neb\Main\Agents
 */
class Library
{
    /**
     * @var Library
     */
    private static $_instance;
    private $arLibs = [];

    /**
     * Обновляет ключ у анонимного читателя электронного читального зала
     *
     * @return string
     */
    public static function updateData() {

        $sResult = "\\" . __METHOD__ . '();';

        try {

            $ob = self::getInstance();
            $ob->getLibraries();
            $ob->getPublicationsStat();
            $ob->getCollections();
            $ob->updateStat();

        } catch (\Exception $e) {
            $log = new \CEventLog();
            $log->Log('ERROR', 'library', 'neb.main', __FUNCTION__, strval($e));
        }

        return $sResult;
    }

    /**
     * @return Library
     */
    private static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Получение библиотек
     *
     * @return array
     */
    private function getLibraries() {

        $arResult = [];

        if (!Loader::includeModule('iblock')) {
            return $arResult;
        }

        $rsLibraries = \CIBlockElement::GetList(
            ['ID' => 'ASC'],
            ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ACTIVE' => 'Y'],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'PROPERTY_PUBLICATIONS', 'PROPERTY_PUBLICATIONS_DIGITIZED', 'PROPERTY_COLLECTIONS']
        );
        while ($arFields = $rsLibraries->Fetch()) {
            $arResult[$arFields['ID']]['PROPS'] = [
                'PUBLICATIONS'           => intval($arFields['PROPERTY_PUBLICATIONS_VALUE']),
                'PUBLICATIONS_DIGITIZED' => intval($arFields['PROPERTY_PUBLICATIONS_DIGITIZED_VALUE']),
                'COLLECTIONS'            => intval($arFields['PROPERTY_COLLECTIONS_VALUE'])
            ];
        }

        $this->arLibs = $arResult;
    }

    /**
     * Получение статистики изданий
     */
    private function getPublicationsStat() {

        // получить последнюю дату
        $arItem = NebStatEditionTable::getList([
            'order'  => ['ID' => 'DESC'],
            'select' => ['DATE_STAT'],
            'limit'  => 1
        ])->fetch();

        if (empty($this->arLibs) || empty($arItem['DATE_STAT'])) {
            return [];
        }

        $rsStat = NebStatEditionTable::getList([
            'select' => ['ID_BITRIXDB', 'COUNT_EDITION', 'COUNT_EDITION_DIG'],
            'filter' => ['ID_BITRIXDB' => array_keys($this->arLibs), '>=DATE_STAT' => $arItem['DATE_STAT']],
            'order'  => ['ID_BITRIXDB' => 'ASC'],
            'limit'  => count($this->arLibs)
        ]);
        while ($arItem = $rsStat->fetch()) {
            $this->arLibs[$arItem['ID_BITRIXDB']]['STAT'] = [
                'PUBLICATIONS'           => $arItem['COUNT_EDITION'],
                'PUBLICATIONS_DIGITIZED' => $arItem['COUNT_EDITION_DIG']
            ];
        }
    }

    /**
     * Получение коллекций библиотек
     */
    private function getCollections() {

        $arResult = [];

        if (!Loader::includeModule('iblock'))
            return $arResult;

        $rsSection = \CIBlockSection::GetList(
            ['LEFT_MARGIN' => 'ASC'],
            ['IBLOCK_ID' => IBLOCK_ID_COLLECTION, 'ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1, '!UF_LIBRARY' => false],
            false,
            ['ID',  'UF_LIBRARY']
        );
        while ($arSection = $rsSection->Fetch()) {
            if (!empty($this->arLibs[$arSection['UF_LIBRARY']])) {
                $this->arLibs[$arSection['UF_LIBRARY']]['STAT']['COLLECTIONS']++;
            }
        }
    }

    /**
     * Обновление статистических данных
     */
    private function updateStat() {

        if (empty($this->arLibs) || !Loader::includeModule('iblock')) {
            return;
        }

        $obIBlockElement = new \CIBlockElement();

        // специально обновления сделал через свойства, чтобы дата изменения не писалась
        foreach ($this->arLibs as $iLibId => $arLibrary) {

            // статистика изданий
            if ($arLibrary['STAT']['PUBLICATIONS'] > 0 // специально так
                && $arLibrary['STAT']['PUBLICATIONS'] != $arLibrary['PROPS']['PUBLICATIONS']
            ){
                $obIBlockElement->SetPropertyValueCode(
                    $iLibId,
                    'PUBLICATIONS',
                    $arLibrary['STAT']['PUBLICATIONS']
                );
            }

            // статистика изданий оцифрованных
            if ($arLibrary['STAT']['PUBLICATIONS_DIGITIZED'] > 0 // специально так
                && $arLibrary['STAT']['PUBLICATIONS_DIGITIZED'] != $arLibrary['PROPS']['PUBLICATIONS_DIGITIZED']
            ){
                $obIBlockElement->SetPropertyValueCode(
                    $iLibId,
                    'PUBLICATIONS_DIGITIZED',
                    $arLibrary['STAT']['PUBLICATIONS_DIGITIZED']
                );
            }

            // коллекции
            if ($arLibrary['STAT']['COLLECTIONS'] != $arLibrary['PROPS']['COLLECTIONS']) {
                $obIBlockElement->SetPropertyValueCode(
                    $iLibId,
                    'COLLECTIONS',
                    intval($arLibrary['STAT']['COLLECTIONS'])
                );
            }
        }
    }
}
