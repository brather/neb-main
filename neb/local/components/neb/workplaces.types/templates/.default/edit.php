<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 * @var array $arParams
 */

$arType = $arResult['ITEMS'][$arParams['ID']];

?>
<div class="profile-container-readers-profile" data-component="profile/readers/edit">

    <?foreach($arResult['ERRORS'] as $sError) ShowError($sError); ?>

    <form method="post" action="" class="nrf">

        <input type="hidden" name="TYPE[ID]" value=<?=intval($arType["ID"])?> />

        <div class="form-group">
            <label for="typexmlid">
                <em class="hint">*</em>
                <?=Loc::getMessage('WORKPLACES_TYPES_XML_ID');?>
            </label>
            <input type="text"
                   value="<?=$_REQUEST["TYPE"]['XML_ID']?:$arType['XML_ID']?>"
                   id="typexmlid"
                   name="TYPE[XML_ID]"
                   class="form-control" />
        </div>

        <div class="form-group">
            <label for="typevalue">
                <em class="hint">*</em>
                <?=Loc::getMessage('WORKPLACES_TYPES_VALUE');?>
            </label>
            <input type="text"
                   value="<?=$_REQUEST["TYPE"]['VALUE']?:$arType['VALUE']?>"
                   id="typevalue"
                   name="TYPE[VALUE]"
                   class="form-control" />
        </div>

        <div class="form-group">
            <label for="typesort">
                <em class="hint">*</em>
                <?=Loc::getMessage('WORKPLACES_TYPES_SORT');?>
            </label>
            <input type="text"
                   value="<?=$_REQUEST["TYPE"]['SORT']?:($arType['SORT']?:'500')?>"
                   id="typesort"
                   name="TYPE[SORT]"
                   class="form-control" />
        </div>

        <div class="fieldrow nowrap fieldrowaction">
            <div class="fieldcell ">
                <div class="field clearfix">
                    <button name="TYPE[ACTION]" class="btn btn-primary" value="save" type="submit">
                        <?=Loc::getMessage('WORKPLACES_TYPES_SAVE');?>
                    </button>
                    <? if (intval($arType["ID"]) > 0): ?>
                        <? if (intval($arType["COUNT"]) > 0): ?>
                            <br /><br /><label>
                            <a href="/bitrix/admin/iblock_list_admin.php?PAGEN_1=1&SIZEN_1=20&type=library&IBLOCK_ID=<?=$arType['IBLOCK_ID'];
                                ?>&lang=<?=LANGUAGE_ID?>&set_filter=Y&adm_filter_applied=0&find_el_property_<?=$arType['PROPERTY_ID'];
                                ?>=<?=$arType['ID']?>" target="_blank">
                                <?=Loc::getMessage('WORKPLACES_TYPES_DELMES');?> [<?=$arType["COUNT"]?>]
                            </a></label>
                        <? else: ?>
                            <button name="TYPE[ACTION]" class="btn btn-delete" value="delete" type="submit" style="float:right;">
                                <?=Loc::getMessage('WORKPLACES_TYPES_DELETE');?>
                            </button>
                        <? endif; ?>
                    <? endif; ?>
                </div>
            </div>
        </div>

    </form>
</div>