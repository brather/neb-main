<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>
<div class="edition-detailed-description__all-editions">
    <? if ($_REQUEST['Ajax_instances'] != 'Y'
        && 'same' !== $_REQUEST['listType']
    ) {

    } else {
        $APPLICATION->RestartBuffer();
    } ?>
    <h3>Экземпляры</h3>

    <div class="text-radio-horizontal-two">
        <label class="same-selector same-selector-online"
               data-href="/catalog/<?= $arResult['CURRENT_ID'] ?>/"
               data-type="online">
            <input type="radio" name="somefilter"
                <?= ('all' !== $_REQUEST['loadType'] ? 'checked' : '') ?>>
            <span>с онлайн-версией</span>
        </label>
        <label class="same-selector same-selector-all"
               data-href="/catalog/<?= $arResult['CURRENT_ID'] ?>/"
               data-type="all">
            <input type="radio" name="somefilter"
                <?= ('all' === $_REQUEST['loadType'] ? 'checked' : '') ?>>
            <span>по всем записям</span>
        </label>
    </div>

    <ul class="editions-list list-unstyled">
        <?
        foreach ($arResult['ITEMS'] as $arElement) {
            ?>
            <li class="<?= (
                    ($arResult['CURRENT_ID'] == $arElement['ID'] ? ' viewing' : '')
                . ((!empty($arElement['VIEWER_URL']) && $arElement['IS_READ'] == 'Y') ? ' viewable' : '')
            ) ?>">
                <a
                    class=""
                    data-width="955"
                    data-page="<?= $arResult['CURRENT_PAGE'] ?>"
                    href="<?= $arElement['LINK'] . ($arResult['CURRENT_PAGE'] > 1 ? '?PAGEN_1='. $arResult['CURRENT_PAGE'] : '') ?>">
                    <? if ($arElement['PUBLISHYEAR'] != ''): ?>
                        Издание <?= $arElement['PUBLISHYEAR'] ?> года
                    <? else:?>
                        Год издания не известен
                    <? endif?>
                </a>
                <? if (!empty($arElement['VIEWER_URL']) && $arElement['IS_READ'] == 'Y') { ?>
                    <a href="#"
                       class="b-samelist-read"
                       target="_blank"
                       title="Читать книгу"
                       onclick="readBook(event, this); return false;"
                       data-link="<?= $arElement['ID'] ?>"
                       data-id="<?= $arResult['BOOK']['id'] ?>"
                       data-options="<?php echo htmlspecialchars(
                           json_encode(
                               array(
                                   'title'  => trim($arElement['AUTHOR']),
                                   'author' => trim(
                                       strip_tags($arElement['TITLE'])
                                   )
                               )
                           ),
                           ENT_QUOTES,
                           'UTF-8'
                       ) ?>">
                    </a>
                <? } ?>
                <div class="same-book-lib">
                    <?php if (empty($arElement['DETAIL_PAGE_URL'])) { ?>
                        <?= $arElement['LIBRARY'] ?>
                    <?php } else { ?>
                        <a target="_blank"
                           href="<?= $arElement['DETAIL_PAGE_URL'] ?>"><?= $arElement['LIBRARY'] ?></a>
                    <?php } ?>
                </div>
            </li>
        <? } ?>
    </ul>

    <div class="pagination-table" data-pagination-exemplars>
        <?= $arResult['STR_NAV'] ?>
    </div>
    <?
    if ($_REQUEST['Ajax_instances'] == 'Y'
        && 'same' === $_REQUEST['listType']
    ) {
        exit();
    }
    ?>

</div>

<script type="text/javascript">
    $(function () {
        var container = $('.edition-detailed-description__all-editions'),
            inprogress = false;
        function disableButtons() {
          $(document).off('click', 'a, .same-selector');
        }
        $(document).on('click', '.same-selector', function (e) {
            e.preventDefault();
            e.stopPropagation();
            disableButtons();

            var isactive = $(this)[0].tagName == 'LABEL' && $(this).find('input[type="radio"]').prop('checked');

            if ( $(this)[0].tagName == 'LABEL' && !inprogress && !isactive ) {
              inprogress = true;
              $(this).find('input[type="radio"]').prop('checked',true);
              var containerHeight = $(container).height();
              container
                .css({'position':'relative', 'height': containerHeight + 'px' })                
                .append('<div class="milk-shadow draw"/>');
              FRONT.spinner.execute( $(container)[0] );
              $.get('', {
                  Ajax_instances: "Y",
                  listType: 'same',
                  loadType: $(this).data('type')
              }, function (data) {
                  $(container).removeAttr('style');
                  $(container).html(data);
              })
              .always(function (data) {
                inprogress = false;
              });
            }
        });

        $(document).on('click', '[data-pagination-exemplars] a:not(.pagination__active)', function(e){
          e.preventDefault();
          disableButtons();
          var href = $(this).attr('href'),
              containerHeight = $(container).height();
          if (href != '#' || href != '' && !inprogress) {
            inprogress = true;
            container.css({'position':'relative', 'height': containerHeight + 'px' })
              .append('<div class="milk-shadow draw"/>');
            FRONT.spinner.execute( $(container)[0] );
            $.get(href, {}, function (data) {
                $(container).removeAttr('style');
                container.html(data);
            })
            .always(function (data) {
              inprogress = false;
            });
          }
                
        });
    });
</script>