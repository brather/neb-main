<?php
/**
 * User: agolodkov
 * Date: 21.10.2016
 * Time: 17:21
 */

namespace AppBundle\DependencyInjection\Search\Exalead;


use AppBundle\DependencyInjection\Search\ConditionInterface;
use AppBundle\DependencyInjection\Search\SearchException;

class Condition implements ConditionInterface
{
    const ALL_QUERY = '#all';
    /**
     * @var ConditionInterface[]
     */
    private $_conditions = [];
    private $_logic = [];
    private $_allowLogic
        = [
            'OR'  => true,
            'AND' => true,
        ];
    protected $_conditionTemplate = '%s';

    /**
     * @param $condition
     */
    public function __construct($condition)
    {
        $condition = $this->_prepareCondition($condition);
        $this->_conditions[] = $condition;
    }

    /**
     * @todo по возможности отрефакторить этот метод
     * @return string
     */
    public function buildCondition()
    {
        $condString = '';
        foreach ($this->_conditions as $key => $condition) {
            if ($condition instanceof ConditionInterface) {
                $condition = '(' . $condition->buildCondition() . ')';
            }
            $prepare = true;
            if (is_array($condition)) {
                foreach ($condition as $field => $value) {
                    $condition[$field] = $field . ':' . $this->_prepareValue($value);
                }
                $condition = implode(' AND ', $condition);
                $prepare = false;
            }
            if (isset($this->_logic[$key])) {
                if (true === $prepare) {
                    $condition = $this->_prepareValue($condition);
                }
                $condition = ' ' . $this->_logic[$key] . ' ' . $condition;
                $prepare = false;
            }

            if (true === $prepare) {
                $condString .= $this->_prepareValue($condition);
            } else {
                $condString .= $condition;
            }
        }

        return $condString;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    protected function _prepareValue($value)
    {
        return sprintf($this->_conditionTemplate, $value);
    }

    /**
     * @param string|ConditionInterface $condition
     */
    public function andCondition($condition)
    {
        $this->addCondition('AND', $condition);
    }

    /**
     * @param string|ConditionInterface $condition
     */
    public function orCondition($condition)
    {
        $this->addCondition('OR', $condition);
    }

    /**
     * @param string                    $logic
     * @param string|ConditionInterface $condition
     *
     * @throws SearchException
     */
    public function addCondition($logic, $condition)
    {
        if (!isset($this->_allowLogic[$logic])) {
            throw new SearchException("Logic $logic not allowed!");
        }
        $condition = $this->_prepareCondition($condition);
        $this->_conditions[] = $condition;
        end($this->_conditions);
        $this->_logic[key($this->_conditions)] = $logic;
    }

    /**
     * @return string
     */
    public function getConditionTemplate()
    {
        return $this->_conditionTemplate;
    }

    /**
     * @param string $conditionTemplate
     */
    public function setConditionTemplate($conditionTemplate)
    {
        $this->_conditionTemplate = $conditionTemplate;
    }

    /**
     * @param string|array $condition
     *
     * @return array|string
     */
    protected function _prepareCondition($condition)
    {
        if (empty($condition)) {
            $condition = static::ALL_QUERY;
        }
        if (is_array($condition)) {
            foreach ($condition as $field => $value) {
                $value = trim($value);
                if (empty($value)) {
                    $condition[$field] = static::ALL_QUERY;
                }
            }
        }

        return $condition;
    }
}