<?php
if (file_exists(__DIR__ . '/../../../neb/bitrix/.settings_extra.php')) {
    $bitrixConfig = require(__DIR__ . '/../../../neb/bitrix/.settings_extra.php');
} else {
    throw new Exception('Database error');
}
self::$config['modules']['enabled'][] = 'Db';
self::$config['modules']['config']['Db'] = [
    'dsn'      => 'mysql:host=' . $bitrixConfig['connections']['value']['default']['host']
        . ';dbname=neb_test_database',
    'user'     => $bitrixConfig['connections']['value']['default']['login'],
    'password' => $bitrixConfig['connections']['value']['default']['password'],
    'dump'     => 'Tests/_data/dump.sql',
    'populate' => false,
    'cleanup'  => false,
];