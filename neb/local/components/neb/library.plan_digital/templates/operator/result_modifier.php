<?
use Bitrix\NotaExt\Iblock\Element;

$from = date('Y-m-d', $arParams['dateFrom']);
$to = date('Y-m-d', $arParams['dateTo'] + 86400);
$rsPlanLibs = $DB->Query(
    "
SELECT UF_LIBRARY
FROM plan_digitization
WHERE UF_DATE_ADD >= '$from' AND UF_DATE_ADD <= '$to'
GROUP BY UF_LIBRARY;
"
);
$libIds = [];
while ($lib = $rsPlanLibs->Fetch()) {
    if (isset($lib['UF_LIBRARY'])) {
        $libIds[] = (integer)$lib['UF_LIBRARY'];
    }
}
$libIds = array_filter($libIds);
$arResult['LIBS'] = array();

if (!empty($libIds)) {
    $arLibs = Element::getList(
        array(
            'IBLOCK_ID' => IBLOCK_ID_LIBRARY,
            'ACTIVE'    => 'Y',
            'ID'        => $libIds,
        ),
        0,
        array('ID', 'NAME'),
        array('NAME' => 'ASC')

    );

    foreach ($arLibs['ITEMS'] as $lib):
        $arResult['LIBS'][$lib['ID']] = $lib['NAME'];
    endforeach;
}