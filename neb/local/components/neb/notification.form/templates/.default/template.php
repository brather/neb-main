<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Created by PhpStorm.
 * User: achechel
 * Date: 23.01.2017
 * Time: 17:11
 */
/** @var array $arResult */

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);

global $USER;

Asset::getInstance()->addJs('/local/templates/adaptive/vendor/tinymce/tinymce.min.js');
?>
<p>
    <span data-href="<?= $_SERVER['REQUEST_URI']?>">Отправка</span> | <a href="<?= $arResult['URLS']['history']?>">История</a>
</p>

<form class="postingForm form-form" method="POST" data-notification-form >
    <div class="form-group form-form">
        <label for="bposting-postingEmails">Кому</label>
        <select id="bposting-postingEmails" name="bposting[postingEmails][]" data-select-two multiple="true" class="form-control"
                data-placeholder="Всем библиотекам">
            <? foreach ( $arResult['LIBRARIES'] as $library): ?>
            <option value="<?= $library['email']?>" <?= ($library['selected']) ? "selected": ""?>><?= $library['name']?></option>
            <? endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label>Тема письма</label>
        <input type="text" name="subject" class="form-control" value="">
    </div>
    <div class="form-group">
        <label>Прикрепить файлы</label>
        <div data-file-upload data-url="<?= $arResult['URLS']['fileUpload']?>" data-inputs-name="posting_files[]">
            <span class="btn btn-default fileinput-button">
                <span>Выберите файлы</span>
                <input data-file-upload-input type="file"
                       name="files[]" multiple/>
                <div class="hidden">
                    <div data-file-item>
                        <div data-file-client-name></div>
                        <div data-file-item-progress>
                            <div class="file-progress"><div class="file-progress-bar"></div></div>
                        </div>
                        <div data-file-server-name></div>
                    </div>
                    <span data-uploaded-file-present>
                        <a target="_blank" data-present-link href="#"></a>
                        <input type="hidden" data-file-input name="upload_files[]"
                               value=""/>
                    </span>
                </div>
            </span>
            <div data-present-files>

            </div>
        </div>
    </div>
    <div class="notification-draw">
        <textarea id="postingText"></textarea>
    </div>
    <input type="hidden" name="fromField" value="<?=$USER->GetEmail()?>">

    <div class="form-buttons">
        <input type="submit" class="btn btn-primary" data-submit-notification disabled onclick="this.disabled = true;">
    </div>
</form>