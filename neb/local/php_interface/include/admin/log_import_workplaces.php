<?
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';

global $DB, $USER;
$sLogTableName = 'neb_log_import_workplaces';

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

// ======================================= проверка критических данных =====================================

if (!$USER->IsAdmin())
    $arFatalError[] = 'Admin permissions denied!';

// выборка возможных статусов
$query = "SHOW TABLES LIKE '".$sLogTableName."'";
$iFind = $DB->Query($query)->SelectedRowsCount();
if (!$iFind)
    $arFatalError[] = 'Log table does not exist!';

if (!empty($arFatalError)) {
    require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';
    foreach ($arFatalError as $sError)
        echo $sError.'<br />';
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
    die();
}

// =========================================== выборка данных ============================================

// выборка возможных статусов
$arStatus[] = 'Все';
$query = "SELECT STATUS FROM ".$sLogTableName." GROUP BY STATUS ORDER BY STATUS DESC";
$rsStatus = $DB->Query($query, false, $err_mess . __LINE__);
while ($arItem = $rsStatus->Fetch())
    $arStatus[$arItem['STATUS']] = $arItem['STATUS'];
unset($rsStatus, $arItem);

// фильтры
$arFilter = array();
if ($_REQUEST['set_filter'] == 'Y') {
    if ($_REQUEST['find_date'])
        $arFilter[] = "DATE >= '".date('Y-m-d H:i:s', strtotime($_REQUEST['find_date']))."'";
    if ($_REQUEST['find_date_to'])
        $arFilter[] = "DATE <= '".date('Y-m-d H:i:s', strtotime($_REQUEST['find_date_to']))."'";
    if (!empty($_REQUEST['find_name']))
        $arFilter[] = "NAME LIKE '%".$_REQUEST['find_name']."%'";
    if ($arStatus[$_REQUEST['find_status']])
        $arFilter[] = "STATUS = '".$arStatus[$_REQUEST['find_status']]."'";
}
$sFilter = implode(' AND ', $arFilter);
if (!empty($sFilter))
    $sFilter = 'WHERE '.$sFilter;

$query = "
SELECT
  ID,
  DATE,
  RGB_ID,
  LIB_ID,
  NAME,
  STATUS,
  INFO
FROM
  ".$sLogTableName."
".$sFilter;
$rsLog = $DB->Query($query, false, $err_mess . __LINE__);
//while ($arItem = $rsLog->Fetch())
//    $arResult[$arItem['ID']] = $arItem;

// =========================================== формирование результата ============================================

$sTableID = 'tbl_'.$sLogTableName;
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$arOrder = (strtoupper($by) === "ID"? array($by => $order): array($by => $order, "ID" => "ASC"));
$lAdmin = new CAdminList($sTableID, $oSort);
$arFilterFields = Array(
    "find_status",
    "find_date",
    "find_date_to",
    "find_name",
);

$lAdmin->InitFilter($arFilterFields);

$arHeader = array(
    array(
        'id'      => 'ID',
        'content' => 'ID',
        'sort'    => 'ID',
        'default' => true
    ),
    array(
        'id'      => 'DATE',
        'content' => 'Дата и время',
        'default' => true,
        'sort'    => 'DATE',
    ),
    array(
        'id'      => 'RGB_ID',
        'content' => 'RGB_ID',
        'sort'    => 'RGB_ID',
        'default' => true
    ),
    array(
        'id'      => 'LIB_ID',
        'content' => 'LIB_ID',
        'sort'    => 'LIB_ID',
        'default' => true
    ),
    array(
        'id'      => 'NAME',
        'content' => 'Название',
        'default' => true,
        'sort'    => 'NAME',
    ),
    array(
        'id'      => 'STATUS',
        'content' => 'Статус',
        'default' => true,
        'sort'    => 'STATUS',
    ),
    array(
        'id'      => 'INFO',
        'content' => 'Описание',
        'default' => true,
        'sort'    => 'INFO',
    )
);

$lAdmin->AddHeaders($arHeader);
$arSelect = $lAdmin->GetVisibleHeaderColumns();

// заполнение результирующей таблицы
$rsData = new CAdminResult($rsLog, $sTableID);
$rsData->NavStart(50);
$lAdmin->NavText($rsData->GetNavPrint('ЭЧЗ'));
while ($arRes = $rsData->NavNext())
    $row = $lAdmin->AddRow($str_ID, $arRes);

$lAdmin->AddAdminContextMenu(array());

$lAdmin->CheckListMode();
$APPLICATION->SetTitle('Лог импорта ЭЧЗ');

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

?>
    <form method="GET" action="" name="find_form">
        <input type="hidden" name="filter" value="Y">
        <?
        $oFilter = new CAdminFilter(
            $sTableID."_filter",
            array()
        );
        $oFilter->Begin();
        ?>
        <tr>
            <td><b>Название</b></td>
            <td><input type="text" name="find_name" value="<?echo htmlspecialcharsbx($find_name)?>" size="40"></td>
        </tr>
        <tr>
            <td>Дата:</td>
            <td><?=CalendarDate("find_date", date('d.m.Y'), "find_form", "15", "class=\"my_input\"")?><span class="adm-calendar-separate" style="display: inline-block;"></span>
                <?=CalendarDate("find_date_to", $find_date_to, "find_form", "15", "class=\"my_input\"")?></td>
        </tr>
        <tr>
            <td><b>Статус</b></td>
            <td>
                <select name="find_status" style="width:100%;">
                    <? foreach ($arStatus as $sStatus): ?>
                        <option value="<?=$sStatus?>" <?if ($_REQUEST['find_status'] == $sStatus):?>selected="selected"<?endif;?>><?=$sStatus?></option>
                    <? endforeach; ?>
                </select>
            </td>
        </tr>
        <?
        $oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPageParam(), "form"=>"find_form"));
        $oFilter->End();
        ?>
    </form>
<?
$lAdmin->DisplayList();
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
?>