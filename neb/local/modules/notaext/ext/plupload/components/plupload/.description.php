<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)	die();

$arComponentDescription = array(
	'NAME' 			=> GetMessage('PLUPLOAD_NAME'),
	'DESCRIPTION' 	=> GetMessage('PLUPLOAD_DESC'),
	'PATH' 			=> array(
		'ID' 			=> 'utility',
	)
);

?>