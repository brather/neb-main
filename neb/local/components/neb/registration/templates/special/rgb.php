<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode">регистрация на портале</h2>
		<form action="" class="b-form b-form_common b-regform" method="post" id="regform">
			<?=bitrix_sessid_post()?>
			<hr/>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings01">Фамилия</label>
					<div class="field validate">
						<input type="text" data-validate="fio" data-required="required" value="<?=htmlspecialcharsbx($_REQUEST['LAST_NAME'])?>" id="settings01" data-maxlength="30" data-minlength="2" name="LAST_NAME" class="input">
						<em class="error required">Заполните</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>							
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings02">Имя</label>
					<div class="field validate">
						<input type="text" data-required="required" data-validate="fio" value="<?=htmlspecialcharsbx($_REQUEST['NAME'])?>" id="settings02" data-maxlength="30" data-minlength="2" name="NAME" class="input" >	
						<em class="error required">Заполните</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>												
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings22">Отчество</label>
					<div class="field validate">
						<input type="text" data-validate="fio" value="<?=htmlspecialcharsbx($_REQUEST['SECOND_NAME'])?>" id="settings22" data-maxlength="30" data-minlength="2" name="SECOND_NAME" class="input" >
						<em class="error required">Заполните</em>		
						<em class="error validate">Неверный формат</em>	
						<em class="error maxlength">Более 30 символов</em>													
					</div>
				</div>
			</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings21" class="disable">Номер читательского билета РГБ (12 цифр, включая номер зала)</label>
					<div class="field validate">
						<input type="text" data-validate="number" value="<?=htmlspecialcharsbx($_REQUEST['RGB_CARD_NUMBER'])?>" id="settings21" data-required="required" data-maxlength="30" data-minlength="2" name="RGB_CARD_NUMBER" class="input">	
						<em class="error required">Заполните</em>
						<em class="error validate">Неверный формат</em>	
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings05">Пароль <a href="https://passport.rsl.ru/forgot_password">Забыли пароль?</a> </label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="required" value="" id="settings05" data-maxlength="30" name="PASSWORD" class="pass_status input">										
						<em class="error required">Заполните</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings55">Подтвердить пароль</label>
					<div class="field validate">
						<input data-identity="#settings05" type="password" data-required="required" value="" id="settings55" data-maxlength="30" name="CONFIRM_PASSWORD" class="input" data-validate="password">										
						<em class="error identity ">Пароль не совпадает с введенным ранее</em>
						<em class="error required">Заполните</em>
						<em class="error validate">Пароль менее 6 символов</em>
						<em class="error maxlength">Пароль более 30 символов</em>
					</div>
				</div>
			</div>
			<hr>

			<!--div class="checkwrapper ">
				<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb3"><label for="cb3" class="black">Согласен на обработку персональных данных, с</label> <a href="/special/user-agreement/" target="_blank">правилами использования</a> <label for="cb3" class="black">ознакомлен</label>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix">
						<button class="formbutton btdisable" value="1" type="submit" disabled="disabled">Зарегистрироваться</button>
					</div>
				</div>
			</div-->
			<div class="checkwrapper">
				<label class="defautl_cursor"><?=GetMessage("REG_RGB_AGREE_FOR_POPUP");?> </label> <a href="/local/components/neb/registration/templates/.default/ajax_agreement.php" target="_blank" class="popup_opener ajax_opener closein" data-width="955" data-height="auto"><?=GetMessage("REG_RGB_AGREE_FOR_POPUP_TERMS");?></a>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix divdisable">
						<button class="formbutton btdisable" value="1" type="submit" disabled="disabled"><?=GetMessage("REG_RGB_SIGN_UP");?></button>
                        <div class="b-hint"><?=GetMessage("REG_RGB_READ_TERMS");?></div>
					</div>
				</div>
			</div>
			<?
				if($arResult['CHECK_STATUS'] === false)
				{
				?>
				<div class="nookresult">
					<p class="error"><?
                        if(!empty($arResult['ERROR_MESSAGE'])){

                            echo $arResult['ERROR_MESSAGE'];
                        }
                        else{
                            echo GetMessage("REG_RGB_ERROR_RGB_DATA_NOT_FOUND");
                        }?>
                    </p>
					<p><a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['full']?>" class="black">Пройдите процедуру полной регистрации</a></p>
				</div>
				<?
				}

			?>
		</form>
	</div><!-- /.b-registration-->
</section>