<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {die();}

if(empty($arResult['BOOK'])) {
    return false;
}

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init();

$openOptions = array(
    'title'  => trim(strip_tags($arResult['BOOK']['title'])),
    'author' => trim($arResult['BOOK']['authorbook']),
);
?>
<link rel="stylesheet" href="/local/templates/.default/markup/css/card.css" type="text/css" />
<?
	if($arParams['RestartBuffer'] === true)
	{
	?>
    <script type="text/javascript" src="<?php echo $templateFolder ?>/script.js"></script>
    <script>
		$(function() {
		    $('.boockard-read-button').click(function(event){
		        event.preventDefault();
		        $('.boockard-error-msg').hide();
		        readBook(event, this);
		        setTimeout(function(){
		            if($('.boockard-read-button').data('load') != 'load') $('.boockard-error-msg').text('Ошибка сервера. Пожалуйста, повторите запрос').show();
		        }, 5000);
		    });
			$('.exclamation-opener').exclamationOpener();
		});   
	</script>
	<div class="b-bookpopup popup" >
		<a href="#" class="closepopup"><?=Loc::getMessage('LIB_SEARCH_PAGE_CLOSE_WINDOW')?></a>
		<?
		}
		else
		{
		
			include_once($_SERVER['DOCUMENT_ROOT'].'/local/include_areas/books_closed.php');

		?>
		<div class="b-book_bg">
			<section class="innersection innerwrapper clearfix">
			<div class="b-bookpopup b-bookpage" >
				<?
				}
			?>
			<?
				/* Start Тело карточки книги*/
			?>
			<div class="b-bookpopup_in bbox">
				<?/*?><div class="b-bookframe iblock">
					<img alt="" class="b-bookframe_img" src="<?=$arResult['BOOK']['IMAGE_URL']?>&width=262&height=408">
				</div>
				<?*/?>
                <?if($arParams['RestartBuffer'] !== true):?>
                    <div class="rel clearfix"><a href="javascript:;" class="bookclose right bookclose-blank"><?=Loc::getMessage('LIB_SEARCH_PAGE_CLOSE_WINDOW')?></a></div>
                <?endif;?>
				<div class="b-onebookinfo iblock">
					<div class="b-bookhover b-booktitle-block clearfix">
						<?
							if(collectionUser::isAdd())
							{
								if(!empty($arResult['USER_BOOKS']))
								{
								?>
								<div class="meta minus">
									<div class="b-hint rel"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_REMOVE_FROM_LIB')?></div>
									<a href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arResult['BOOK']['id'])?>" data-collection="<?=ADD_COLLECTION_URL?>list.php?id=<?=urlencode($arResult['BOOK']['id'])?>" class="b-bookadd fav"></a>
								</div>
								<?
								}
								else
								{
								?>
								<div class="meta">
									<div class="b-hint rel"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ADD_TO_LIB')?></div>
									<a href="#" data-normitem="Y" data-collection="<?=ADD_COLLECTION_URL?>list.php?id=<?=urlencode($arResult['BOOK']['id'])?>" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arResult['BOOK']['id'])?>" class="b-bookadd"></a>
								</div>
								<?
								}
							}
						?>
						<div class="bookcard-col-left bookcard-col-left--nonfloat">
							<?
							if(!empty($arResult['BOOK']['authorbook'])){
							?>
							<span class="b-autor">
                                 <?foreach ($arResult['BOOK']['authorbook_for_link'] as $key => $author):?>
                                     <a href="/search/?f_field[authorbook]=f/authorbook/<?
                                     echo urlencode(mb_strtolower(strip_tags(trim($author))))?>"
                                     class="lite"><?=$author?></a>
                                     <?if ($key+1 < count($arResult['BOOK']['authorbook_for_link'])):?>, <?endif;?>
                                 <?endforeach;?>
                            </span>
							<?
							}
							?>
							<div class="b-bookhover_tit black"><?=$arResult['BOOK']['title']?></div>
	                        <?php
	                        /*
	                        $show = false;
	                            if($arResult['BOOK']['isprotected'] > 0 and !empty($arResult['BOOK']['PROTECTED_URL'])):
	                                $show = true;
	                            elseif(!empty($arResult['VIEWER_URL']) && $arResult['PDF_AVAILABLE']):
	                                $show = true;
	                            endif;
	                        */
	                        ?>
	                        <?php if ($arResult['BOOK']['IS_READ']): ?>
								<div class="clearfix read-button-block">
		                            <a class="boockard-read-button clearfix <?php if(!$arResult['isInWorkplace'] && ($arResult['nebUser']['UF_STATUS'] !== nebUser::USER_STATUS_VIP )): ?> exclamation-opener boockard-read-button--disabled <?php endif;?>"
		                               href="javascript:void(0);"
		                               data-link="<?= $arResult['BOOK']['id'] ?>"
		                               data-options="<?php echo htmlspecialchars(json_encode($openOptions), ENT_QUOTES, 'UTF-8')?>">
		                                <button type="button" value="1" class="formbutton left">
		                                    <span class="read-button-text"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_VIEW')?></span>
		                                    <span class="read-button-preloader"></span>
		                                </button>
		                            </a>
			                        <?
				                        if(!$arResult['isInWorkplace'] && ($arResult['nebUser']['UF_STATUS'] !== nebUser::USER_STATUS_VIP )) {
				                    ?>
                                        <div class="popup-window popup-window-closedbook popup-hover">
                                            <p>
                                                Доступ к изданиям, защищённым авторским правом возможен только из виртуальных читальных залов НЭБ. <br>
		                                        <a class="popup-window-closedbook-link" href="#link-nearest-library">Найти ближайший</a>.
                                            </p>
                                        </div>
                                    <?
                                    }
                                    else {
				                        if($arResult['BOOK']['isprotected'] > 0 and !$arParams['IS_SHOW_PROTECTED']) {
				                        ?>
			                                    <img class="copyright-bnt exclamation-opener" src="/local/templates/.default/markup/i/lock.png" alt="">
				                                <div class="popup-window popup-window-copyright popup-hover">
				                                    <p>Издание охраняется<br>авторским правом.<br>Для чтения требуется<br>полная регистрация.</p>
				                                </div>
										<?
										}
										else {
										?>
											<img class="copyright-bnt exclamation-opener" src="/local/templates/.default/markup/i/unlock.png" alt="">
											<div class="popup-window popup-window-copyright popup-hover">
										    	<p>Произведение, перешедшее<br>в общественное достояние.<br>Регистрация для чтения не требуется.</p>
											</div>
										<?
                                        }
                                    }
                                    ?>
		                            <div class="boockard-error-msg"></div>

		                    	</div>
	                        <?php endif; ?>
						</div>
						<h2 class="b-samelist-title">Экземпляры</h2>
					</div>

					<div class="rel clearfix">
						<?
						?>
						<div class="bookcard-col-left">
							<div itemscope itemtype="http://schema.org/Book" class="b-infobox b-descrinfo"  data-link="descr">
								<?
								if(!empty($arResult['BOOK']['authorbook'])){
								?>						
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR')?>: </span>
										<span class="iblock val">
                                              <?foreach ($arResult['BOOK']['authorbook_for_link'] as $key=>$author):?>
                                                  <a itemprop="author" href="/search/?f_field[authorbook]=f/authorbook/<?
                                                  echo urlencode(mb_strtolower(strip_tags(trim($author))))?>"><?=$author?></a>
                                                  <?if ($key+1 < count($arResult['BOOK']['authorbook_for_link'])):?>, <?endif;?>
                                              <?endforeach;?>
	                                    </span>
									</div>
								<?
								}
								?>
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE')?>: </span>
									<span itemprop="name" class="iblock val"><?=$arResult['BOOK']['title']?></span>
								</div>
								<?
								if(!empty($arResult['BOOK']['subtitle'])){
								?>						
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SUBTITLE')?>: </span>
										<span class="iblock val"><?=$arResult['BOOK']['subtitle']?></span>
									</div>
								<?
								}
								?>
								<?
								if(!empty($arResult['BOOK']['responsibility'])){
								?>						
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_RESPONSIBILITY')?>: </span>
										<span class="iblock val"><?=$arResult['BOOK']['responsibility']?></span>
									</div>
								<?
								}
								?>
								<?
								if(!empty($arResult['BOOK']['publishplace'])){
								?>	
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>: </span>
										<span class="iblock val"><?=$arResult['BOOK']['publishplace']?></span>
									</div>
								<?
								}
								?>								
								<?
								if(!empty($arResult['BOOK']['publisher'])){
								?>
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH')?>: </span>
										<span class="iblock val">
											<a itemprop="publisher" target="_parent"
											href="/search/?f_field[publisher]=f/publisher/<?=urlencode(mb_strtolower(trim($arResult['BOOK']['publisher'])))?>"><?=$arResult['BOOK']['publisher']?></a>
										</span>
									</div>
								<?
								}
								?>								
								<?
								if(!empty($arResult['BOOK']['publishyear'])){
								?>
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR')?>: </span>
										<span class="iblock val">
											<a itemprop="datePublished" target="_parent" href="/search/?f_publishyear=<?=$arResult['BOOK']['publishyear']?>"><?=$arResult['BOOK']['publishyear']?></a>
										</span>
									</div>
								<?
								}
								?>								
								<?
								if(!empty($arResult['BOOK']['countpages'])){
								?>
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES')?>: </span>
										<span itemprop="numberOfPages" class="iblock val"><?=$arResult['BOOK']['countpages']?></span>
									</div>
								<?
								}
								?>
								
								<?
								if(!empty($arResult['BOOK']['series'])){
								?>	
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE')?>: </span>
										<span class="iblock val"><?=$arResult['BOOK']['series']?></span>
									</div>
								<?
								}
								?>
								
								<?
								if(!empty($arResult['BOOK']['isbn'])){
								?>	
									<div class="b-infoboxitem">
										<span class="tit iblock">ISBN: </span>
										<span itemprop="isbn" class="iblock val"><?=$arResult['BOOK']['isbn']?></span>
									</div>
								<?
								}
								?>
								
								<?
								if(!empty($arResult['BOOK']['issn'])){
								?>	
									<div class="b-infoboxitem">
										<span class="tit iblock">ISSN: </span>
										<span class="iblock val"><?=$arResult['BOOK']['issn']?></span>
									</div>
								<?
								}
								?>
								
								<?
								if(!empty($arResult['BOOK']['contentnotes'])){
								?>	
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT')?>: </span>
										<span class="iblock val"><?=$arResult['BOOK']['contentnotes']?></span>
									</div>
								<?
								}
								?>
								
								<?
								if(!empty($arResult['BOOK']['notes'])){
								?>	
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE')?>: </span>
										<span class="iblock val"><?=$arResult['BOOK']['notes']?></span>
									</div>
								<?
								}
								?>
								
								<?
								if(!empty($arResult['BOOK']['annotation'])){
								?>	
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ANNOTATION')?>: </span>
										<span itemprop="description" class="iblock val"><?=$arResult['BOOK']['annotation']?></span>
									</div>
								<?
								}
								?>
								
								<?
                                if(!empty($arResult['BOOK']['bbk'])){
                                    ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BBK')?>: </span>
                                        <span class="iblock val"><?=$arResult['BOOK']['bbk']?></span>
                                    </div>
                                <?
                                }
                                ?>

                                <?
                                if(!empty($arResult['BOOK']['bbkfull'])){
                                    ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BBKFULL')?>: </span>
                                        <span class="iblock val">
                                             <?foreach($arResult['BOOK']['bbkfull'] as $key=>$bbkfull):?>
                                                 <p>
                                                     <?unset($r_bbkfull);?>
                                                     <?foreach($bbkfull as $k=>$bbk):?>
                                                         <?$r_bbkfull .= '/'.urlencode(trim($arResult['BOOK']['r_bbkfull'][$key][$k]));?>
                                                         <?if ($k != 0):?> > <?endif;?> <a href="/search/?f_field[bbkfull]=f/bbkfull<?=$r_bbkfull?>"><?=$bbk?></a>
                                                     <?endforeach;?>
                                                 </p>
                                             <?endforeach;?>
                                        </span>
                                    </div>
                                <?
                                }
                                ?>
								
								<?
								if(!empty($arResult['BOOK']['udk'])){
								?>	
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UDC')?>: </span>
										<span class="iblock val"><?=$arResult['BOOK']['udk']?></span>
									</div>
								<?
								}
								?>

                                <?
                                if(!empty($arResult['BOOK']['udkfull'])){
                                    ?>
                                    <div class="b-infoboxitem">
                                        <span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UDCFULL')?>: </span>
                                        <span class="iblock val">
                                            <?foreach($arResult['BOOK']['udkfull'] as $key=>$udkfull):?>
                                                <p>
                                                    <?unset($r_udkfull);?>
                                                    <?foreach($udkfull as $k=>$udk):?>
                                                        <?$r_udkfull .= '/'.urlencode(trim($arResult['BOOK']['r_udkfull'][$key][$k]));?>
                                                        <?if ($k != 0):?> > <?endif;?> <a href="/search/?f_field[udkfull]=f/udkfull<?=$r_udkfull?>"><?=$udk?></a>
                                                    <?endforeach;?>
                                                </p>
                                            <?endforeach;?>
                                        </span>
                                    </div>
                                <?
                                }
                                ?>

								<?
								if(!empty($arResult['BOOK']['edition'])){
								?>	
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_EDITION')?>: </span>
										<span class="iblock val"><?=$arResult['BOOK']['edition']?></span>
									</div>
								<?
								}
								?>
								
								<?
								if(!empty($arResult['BOOK']['library'])){
								?>
									<div class="b-infoboxitem">
										<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB')?>: </span>
										<span class="iblock val">
											<?
												if(!empty($arResult['BOOK']['LIBRARY_DETAIL_PAGE_URL']))
												{
												?>
												<a target="_blank" href="<?=$arResult['BOOK']['LIBRARY_DETAIL_PAGE_URL']?>"><?=$arResult['BOOK']['library']?></a>
												<?
												}
												else
												{
												?>
												<?=$arResult['BOOK']['library']?>
												<?
												}
											?>
										</span>
									</div>
								<?
								}
								?>
							</div><!-- /b-infobox -->
						</div>
						
						<div class="bookcard-col-right">
							<div class="b-samelist">
			                <?php if (!empty($arResult['BOOK']['idparent2'])) {?>
								<div class="same-selector-block clearfix">
									<a data-type="online" href="/catalog/<?=$arResult['BOOK']['id']?>/" class="same-selector same-selector-online same-selector--left same-selector--active">с онлайн-версией</a>
									<a data-type="all" href="/catalog/<?=$arResult['BOOK']['id']?>/" class="same-selector same-selector-all same-selector--right">по всем записям</a>
								</div>
				                <?
									$APPLICATION->IncludeComponent(
									    'exalead:same.list.short',
									    '',
									    Array(
									        'PARENT_ID' => $arResult['BOOK']['idparent2'],
									        'CURRENT_ID' => $arResult['BOOK']['id']
									    )
									);
								?>
								</div>
							<?	
			                 } 
			                 else {?>
								<p>Похожие издания не найдены</p>
							<?
			                 }
			                 ?>
						</div>
                    </div>
				</div>


				<div class="nearest-map rel clearfix">
                    <?php if (!empty($arResult['BOOK']['NEAREST_LIBRARY_URL'])) { ?>
                        <a class="link-nearest-library" href="#" id="link-nearest-library">
                            <?php echo Loc::getMessage(
                                'LIB_SEARCH_PAGE_DETAIL_FIND_NEARLY'
                            ) ?>
                        </a>
                    <?php } ?>
                    <script src="//api-maps.yandex.ru/2.0-stable/?load=package.full&amp;lang=ru-RU" type="text/javascript"></script>
                    <div class="library-map">
                        <div class="ymap" id="ymap"
                             data-path="<?php echo $arResult['BOOK']['NEAREST_LIBRARY_URL'] ?>"
                             data-show-nearly="1">
                        </div>
                        <span class="marker_lib hidden">
                            <a href="#" class="b-elar_name_txt"></a><br />
                            <span class="b-elar_status"></span><br />
                            <span class="b-map_elar_info">
                                <span class="b-map_elar_infoitem addr"><span><?=Loc::getMessage('LIBRARY_LIST_POPUP_ADDRESS');?>:</span></span><br />
                                <span class="b-map_elar_infoitem graf"><span><?=Loc::getMessage('LIBRARY_LIST_POPUP_WORK');?>:</span></span>
                            </span>
                            <span class="b-mapcard_act clearfix">
                                <span class="right neb b-mapcard_status"><?=Loc::getMessage('LIBRARY_LIST_POPUP_USER');?></span>
                                <a href="#" class="button_mode"><?=Loc::getMessage('LIBRARY_LIST_POPUP_URL');?></a>
                            </span>
                        </span>
                    </div>
				</div>
				<div class="b-line_social">
					<h5><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SHARE')?></h5>
				</div>
				<?
					if (!$arResult['PDF_AVAILABLE'] && $arResult['nebUser']['UF_NUM_ECHB'] && !$arResult['isBookAdded'] ) :
					$APPLICATION->IncludeComponent("neb:books.digital.query", "",
						array(
							'EXALEAD_BOOK_ID' => $arResult['BOOK']['id'],
                            'UF_LIBRARY'      => $arResult['BOOK']['UF_LIBRARY'],
							'PDF_AVAILABLE'   => true,
						),
						false
					);
					endif;
				?>
				<?
					$APPLICATION->IncludeComponent(
						"collection:in.collection",
						"",
						Array(
							"IBLOCK_TYPE" => "collections",
							"IBLOCK_ID" => "6",
							"BOOK_ID" => $arResult['BOOK']['id']
						)
					);
				?>

                <?php if (!empty($arResult['BOOK']['idparent'])) { ?>
                    <div id="publication-similar-items"
                         class="ajax-block slide-down<?php echo empty($arResult['BOOK']['idparent'])
                             ? ' empty-list' : ''; ?>"
                         data-ajax-url="/catalog/similar.php?idparent=<?php echo $arResult['BOOK']['idparent'] ?>"
                         data-msg-empty="<?php echo Loc::getMessage(
                             'LIST_EMPTY'
                         ); ?>">
                        <div
                            class="b-addbook_popuptit similar clearfix slide-down-active">
                            <h2><?= Loc::getMessage(
                                    'LIST_SIMILAR_ITEMS'
                                ) ?></h2>
                        </div>
                        <div class="list-content">
                            <?php echo Loc::getMessage('LIST_EMPTY'); ?>
                        </div>
                    </div>
                <? } ?>

                <?php if (!empty($arResult['readWith']['ITEMS'])): ?>
                	<div class="slide-down clearfix">
                        <div class="b-addbook_popuptit slide-down-active read-width-btn slide-down">
                        	<h2> C этим изданием также искали </h2>
                        </div>
                    </div>

					<span class="read-with-wrapper">
                        <table id="readWith" class="b-usertable tsize">
                            <tr>
                                <th class=""><a>Автор</a></th>
                                <th class=""><a>Название </a></th>
                                <th class=""><a>Дата<br />публикации</a></th>
                                <th class=""><a>Библиотека</a></th>
                                <th class=""><a>Читать</a></th>
                            </tr>
                            <?php foreach ($arResult['readWith']['ITEMS'] as $book): ?>
                                <tr>
                                    <td class=""><?php if (isset($book['authorbook']) && $book['authorbook']): echo $book['authorbook']; endif; ?> &nbsp; </td>
                                    <td class=""><?php if (isset($book['title']) && $book['title']): echo $book['title']; endif;?> &nbsp; </td>
                                    <td class=""><?php if (isset($book['publishyear']) && $book['publishyear']): echo $book['publishyear']; endif; ?> &nbsp; </td>
                                    <td class=""><?php if (isset($book['library']) && $book['library']): echo $book['library']; endif; ?> &nbsp;</td>

                                    <td class=""><?if(!empty($book['VIEWER_URL']) and $book['IS_READ'] == 'Y'):?>
                                            <a href="javascript:void(0);" target="_blank"
                                               onclick="readBook(event, this); return false;"
                                               data-link="<?= $book['id'] ?>"
                                               data-options="<?php echo htmlspecialchars(
                                                   json_encode(array(
                                                           'title'  => trim($book['AUTHOR']),
                                                           'author' => trim(strip_tags($book['TITLE']))

                                                            )), ENT_QUOTES,'UTF-8' )?>">

                                                <button type="submit" value="1" class="formbutton"><?=Loc::getMessage('SAME_LIST_READ')?></button>
                                            </a>
                                        <?endif;?> </td>

                                </tr>
                            <?php endforeach; ?>
                        </table>
                	</span>
                <?php endif; ?>

			</div>

			<?
				/* End Тело карточки книги*/
			?>

			<?
				if($arParams['RestartBuffer'] === true)
				{
				?>		
			</div> 	<!-- /.bookpopup -->
			<?
			}
			else
			{
			?>
		</div> 	<!-- /.bookpopup -->
		</section>
	</div>
	<?
	}
?>
