<?php

CBitrixComponent::includeComponentClass('rest:rest.server');

use \Bitrix\Main\Loader;

/**
 * Class RestWchz
 */
class RestWchz extends RestServer
{
    /**
     * @var array
     */
    protected $_params = [];
    protected $_methodAllowGroups = [];

    /**
     * @return int
     */
    public function getAction()
    {
        $arResult = [];

        if (!Loader::includeModule('iblock'))
            return false;

        $arFilter = [
            'IBLOCK_ID' => IBLOCK_ID_WORKPLACES,
        ];
        if (empty($_REQUEST['active'])) {
            $arFilter['ACTIVE'] = 'Y';
        }
        elseif (in_array(strtoupper($_REQUEST['active']), ['Y', 'N'])) {
            $arFilter['ACTIVE'] = strtoupper($_REQUEST['active']);
        }

        $obIBlockElement = new CIBlockElement();
        $rsWchz = $obIBlockElement->GetList(
            [],
            $arFilter,
            false,
            false,
            [
                'ID',
                'IBLOCK_ID',
                'ACTIVE',
                'NAME',
                'PROPERTY_LIBRARY',
                'PROPERTY_IP',
            ]
        );
        while ($arItem = $rsWchz->Fetch()) {
            $arResult[] = $arItem;
        }

        $this->arResult['ITEMS'] = $arResult;

        return 200;
    }
}