<?php
$MESS["NAME_ITEM"] = "Вопрос*: ";
$MESS["INPUT_NAME_ITEM"] = "вопрос...";
$MESS["ADD_ITEM"] = "Добавить";
$MESS["DETAIL_TEXT_ITEM"] = "Детальное описание ответа на вопрос*: ";
$MESS["CHOISE_SECTION_FOR_ITEM"] = "Выберите раздел в который поместите вопрос*: ";
$MESS["SORT_ITEM_DEFAULT"] = "500";
$MESS["SORT_ITEM"] = "Индекс сортировки: ";
$MESS['REQUIRED_FIELD'] = "* - Обязательно к заполнению.";