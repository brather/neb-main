<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 30.05.2017
 * Time: 18:45
 */

namespace Neb\Main\Agents;

use \Neb\Main\Library\LibsTable;
use \Neb\Main\Helper\DbHelper;
use \Nota\Exalead\SearchQuery;
use \Nota\Exalead\SearchClient;

/**
 * Class PopularBooks
 *
 * @package Neb\Main\Agents
 */
class PopularBooks
{
    /**
     * Сохранение статистики популярных изданий
     * Alternate neb/_statistics/build-most-popular-books.php
     *
     * @return string
     * @throws \Exception
     */
    public static function save ()
    {
        $sResult = "\\" . __METHOD__ . '();';

        try {

            $arLibs = self::getLibraries();

            foreach ($arLibs as $iLib) {
                $bAdded  = Service::addCheckDate($sResult, 86400); // очень долгое исполнение агента
                $arBooks = self::getBooksStat($iLib);
                $arQuery = self::getQueries($iLib, $arBooks);
                self::execQueries($arQuery);
            }

        } catch (\Exception $e) {
            $log = new \CEventLog();
            $log->Log('ERROR', 'popularbooks', 'neb.main', __FUNCTION__, strval($e));
        }

        return $sResult;
    }

    /**
     * Получение идентификаторов библиотек
     */
    private static function getLibraries() {

        \NebMainHelper::includeModule('iblock');

        $arLibsIds = $arLibs = [];

        $rsLibs = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ACTIVE' => 'Y', '!PROPERTY_LIBRARY_LINK' => false],
            false,
            false,
            ['PROPERTY_LIBRARY_LINK']
        );
        while ($arLib = $rsLibs->Fetch()) {
            $arLibsIds[] = $arLib['PROPERTY_LIBRARY_LINK_VALUE'];
        }

        $arLibs[0] = 0;
        if (!empty($arLibsIds)) {
            $rsLibs = LibsTable::getList(['filter' => ['ID' => $arLibsIds], 'select' => ['ID']]);
            while ($arLib = $rsLibs->Fetch()) {
                $arLibs[$arLib['ID']] = $arLib['ID'];
            }
        }

        return $arLibs;
    }

    /**
     * Получение статистики самых популярных изданий

     * @param int $iLibraryId - Фильтр по библиотеке
     * @param int $iTopCount - Количество сверху
     * @return array
     */
    private static function getBooksStat($iLibraryId = 0, $iTopCount = 1000) {

        $arResult = [];

        if ($iLibraryId) {
            $sSelect = "LIB_ID,";
            $sWhere = "AND LIB_ID = $iLibraryId";
        }

        $sSql = <<<SQL
SELECT
  BOOK_ID,
  $sSelect
  COUNT(BOOK_ID) AS VIEWS
FROM
  neb_stat_book_view
WHERE
  1=1
$sWhere
GROUP BY
  BOOK_ID
ORDER BY
  VIEWS DESC
LIMIT
  0, $iTopCount;
SQL;
        $connection = DbHelper::getBitrixConnection();
        $rsRows = $connection->query($sSql);
        while ($arItem = $rsRows->fetch()) {
            $arResult[$arItem['BOOK_ID']] = $arItem;
        }

        $digitizedBooks = self::getFiles(array_keys($arResult));

        foreach ($arResult as $bookId => &$arItem) {
            $arItem['BOOK_ID'] = '"' . $arItem['BOOK_ID'] . '"';
            $arItem['VIEWS'] = intval($arItem['VIEWS']);
            if (isset($arItem['LIB_ID'])) {
                $arItem['LIB_ID'] = intval($arItem['LIB_ID']);
            }
            if (!$iLibraryId) {
                unset($arItem['LIB_ID']);
            }
            $arItem['HAS_FILE'] = isset($digitizedBooks[$bookId]) ? 1 : 0; // да, и никак иначе
        }

        return $arResult;
    }

    private static function getFiles($bookIds) {

        \NebMainHelper::includeModule('nota.exalead');

        $digitizedBooks = [];
        $offset = 0;
        $iExaleadSearchBatch = 50;
        do {
            $ids = array_slice($bookIds, $offset, $iExaleadSearchBatch);
            if (!empty($ids)) {

                $query = new SearchQuery();
                $query->getByArIds($ids);
                $query->setQuery('filesize>0 AND (' . $query->getParameter('q') . ')');
                $query->setPageLimit($iExaleadSearchBatch);
                $query->setNextPagePosition($offset);

                $client = new SearchClient();
                $result = $client->getResult($query);
                if (!empty($result['ITEMS'])) {
                    foreach ($result['ITEMS'] as $arItem) {
                        if (!empty($arItem['id'])) {
                            $digitizedBooks[$arItem['id']] = true;
                        }
                    }
                }
            }
            $offset += $iExaleadSearchBatch;
        }
        while ($offset < count($bookIds));

        return $digitizedBooks;
    }

    private static function getQueries(&$iLibrary, &$arBooksStat) {

        $arQuery = $sReplace = [];
        foreach ($arBooksStat as $arItem) {
            $sReplace[] = '(' . implode(',', $arItem) . ')';
        }
        $sReplace = implode(",\n", $sReplace);

        if (empty($sReplace)) {
            return [];
        }

        if ($iLibrary > 0) {
            $arQuery['library ' . $iLibrary . ' start transaction'] = <<<SQL
START TRANSACTION;
SQL;
            $arQuery['library ' . $iLibrary . ' delete'] = <<<SQL
DELETE FROM neb_stat_book_view_most_popular
WHERE LIB_ID = $iLibrary;
SQL;

            $arQuery['library ' . $iLibrary . ' fill table'] = <<<SQL
REPLACE INTO neb_stat_book_view_most_popular (BOOK_ID, LIB_ID, VIEWS, HAS_FILE)
  VALUES $sReplace;
SQL;

            $arQuery['library ' . $iLibrary . ' transaction commit'] = <<<SQL
COMMIT;
SQL;
        }
        // Общая выборка самых популярных
        else {
            $arQuery['common transaction start'] = <<<SQL
START TRANSACTION;
SQL;

            $arQuery['common delete'] = <<<SQL
DELETE FROM neb_stat_book_view_most_popular
WHERE LIB_ID IS NULL;
SQL;
            $arQuery['common fill table'] = <<<SQL
REPLACE INTO neb_stat_book_view_most_popular(BOOK_ID, VIEWS, HAS_FILE)
  VALUES $sReplace;
SQL;
            $arQuery['common transaction commit'] = <<<SQL
COMMIT;
SQL;
        }

        return $arQuery;
    }

    /**
     * Исполнение запросов
     * @param $arQuery
     */
    private function execQueries(&$arQuery) {
        $connection = DbHelper::getBitrixConnection();
        foreach ($arQuery as $sQuery) {
            $connection->query($sQuery);
        }
    }
}