<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient;

Loc::loadMessages(__FILE__);

/**
 * Компонент выбирает из экзалиды новые книжки и показывать инфу о них пользователю
 *
 * Class NewListComponent
 */
class NewListComponent extends CBitrixComponent
{
    private $bCache = false;
    private $iCacheTime = 3600;

    public function onPrepareComponentParams($arParams)
    {
        $this->arParams['DATE'] = intval($arParams['DATE']);
        $this->arParams['COUNT'] = intval($arParams['COUNT']);
        $this->arParams['PAGE_URL'] = trim($arParams['PAGE_URL']);
        $this->arParams['NEXT_BOOK'] = intval($arParams['NEXT_BOOK']);

        $this->bCache = in_array($this->arParams['CACHE_TYPE'], ['A', 'Y']);
        $this->iCacheTime = intval($this->arParams['CACHE_TIME']);

        return $arParams;
    }

    public function executeComponent()
    {
        Loader::includeModule('nota.exalead');

        if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
            $this->arResult['AJAX'] = true;
        }

        $this->getCountPublications();
        $this->getNewPublications();

        $this->includeComponentTemplate();
    }

    /**
     * Количество изданий за последние n дней
     */
    private function getCountPublications()
    {
        $time_stamp = AddToTimeStamp(['DD' => -$this->arParams['DATE']]);
        $date = date('Y/m/y', $time_stamp);

        $query = new SearchQuery('dateindex>=' . $date . ' AND filesize>0');
        $query->setPageLimit($this->arParams['COUNT']);
        $query->setNextPagePosition($this->arParams['NEXT_BOOK']);

        $client = new SearchClient();
        $client->setCacheEnabled($this->bCache, $this->iCacheTime);
        $result = $client->getResult($query);

        $this->arResult['COUNT'] = $result['COUNT'];
        $this->arResult['DATE_1'] = $date;
        $this->arResult['MONTH'] = FormatDate("f", $time_stamp);
    }

    /**
     * Список новых изданий в библиотеке
     */
    private function getNewPublications() {

        $query = new SearchQuery('filesize>0');
        $query->setParam('s', 'desc(document_dateindex)');
        $query->setParam('sl', 'sl_statistic3');
        $query->setPageLimit($this->arParams['COUNT']);

        $client = new SearchClient();
        $client->setCacheEnabled(true, 86400);
        $result = $client->getResult($query);

        foreach($result['ITEMS'] as $arElement) {
            $this->arResult['ITEMS'][] = [
                'ID' => $arElement['id'],
                'URL' => $arElement['URL'],
                'SOURCE' => $arElement['source'],
                'AUTHOR' => $arElement['authorbook'],
                'AUTHOR_FOR_LINK' => $arElement['authorbook_for_link'],
                'AUTHOR_LINK' => '/search/?f_field[authorbook]=f/authorbook/'.urlencode(mb_strtolower(strip_tags(trim($arElement['authorbook'])))),
                'TITLE' => MbTruncateText($arElement['title'], 30),
                'LINK' => $arElement['DETAIL_PAGE_URL'],
                'IMAGE_URL' => $arElement['IMAGE_URL'],
                'VIEWER_URL' => $arElement['VIEWER_URL'],
                'LIBRARY' => $arElement['library'],
                'LIB_LINK' => '/search/?f_field[library]=' . $arElement['library'],
            ];
        }
    }
}