<?php
namespace AppBundle\Traits\Entity;

/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 18:37
 */
trait ExaleadEntityTrait
{
    /**
     * @param array $value
     *
     * @return string
     */
    public function buildSegmentsText($value)
    {
        $result = '';
        if (isset($value['segments'])) {
            foreach ($value['segments'] as $segment) {
                $result .= $segment['text'];
            }
        }

        return $result;
    }

    /**
     * @param array $value
     *
     * @return string
     */
    public function buildSegmentsHtml($value)
    {
        $result = '';
        if (isset($value['segments'])) {
            foreach ($value['segments'] as $segment) {
                if (true === $segment['highlighted']) {
                    $result .= '<strong>' . $segment['text'] . '</strong>';
                } else {
                    $result .= $segment['text'];
                }
            }
        }

        return $result;
    }

    /**
     * @param string $attribute
     * @param array  $value
     */
    public function applySegmentsAttribute($attribute, $value)
    {

        if (property_exists($this, $attribute)) {
            $this->$attribute = $this->buildSegmentsText($value);
        }
        if (property_exists($this, $attribute . 'Source')) {
            $prop = $attribute . 'Source';
            $this->$prop = $value;
        }
        if (property_exists($this, $attribute . 'Html')) {
            $prop = $attribute . 'Html';
            $this->$prop = $this->buildSegmentsHtml($value);
        }
    }
}