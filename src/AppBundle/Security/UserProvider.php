<?php
/**
 * User: agolodkov
 * Date: 16.08.2016
 * Time: 16:50
 */

namespace AppBundle\Security;


use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider extends \BitrixBundle\Security\UserProvider
{
    /**
     * @return User
     */
    public function forgeUser()
    {
        return new User();
    }

    /**
     * @param array $user
     *
     * @return User
     */
    protected function _forgeBitrixUser(array $user)
    {
        $arResGroups = [];

        $nebUser = new \nebUser($user['ID']);
        $arGroups = $nebUser->getUserGroups();
        foreach ($arGroups as $sCode)
            $arResGroups[] = 'ROLE_' . strtoupper($sCode);

        /** @var User $symfonyUser */
        $symfonyUser = parent::_forgeBitrixUser($user);
        $symfonyUser->setUsername($user['EMAIL']);
        $symfonyUser->setRoles($arResGroups);
        $symfonyUser->setToken($user['UF_TOKEN']);
        $symfonyUser->setNebUser($nebUser);

        return $symfonyUser;

    }

    /**
     * @return User
     * @throws \Exception
     */
    public function getCurrentUser()
    {
        $this->_bitrix->checkIncludeThrowException();
        if ($user = \nebUser::getCurrent()->getUser()) {
            return $this->_forgeBitrixUser(\nebUser::getCurrent()->getUser());
        }

        return $this->forgeUser();
    }
}