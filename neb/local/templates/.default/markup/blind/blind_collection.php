
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>
						
					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
<section class="innersection innerwrapper clearfix searchempty ">
		<nav class="b-commonnav noborder">
			<a href="#" title="новые" class="current">новые</a>
			<a href="#" title="популярные" >популярные</a>
			<a href="#" title="рекомендованные" >рекомендованные</a>
		</nav>
		<div class="b-collection_descr">
			<h1>Серебряный век: быт, нравы, моды, знаменитости</h1>
			<div><span class="button_mode b-bookincoll">15 книг в коллекции</span></div>
			<div class="b-result_sorce_info"><em>Автор:</em> Рязанская областная библиотека</div>
			<div class="b-collupdate"><em>Обновлено:</em> 12.07. 2014</div>
			<p>Хочу собрать книги, в которых пожилой герой (героиня) вспоминает свою молодость, или какие-то яркие события из жизни, или полностью рассказывает свой жизненный путь. Книги только художественные.</p>
			<p>Подборка коллективная, так что добавляйте — не стесняйтесь! Буду только благодарна за помощь!</p>
		</div>
		<div class="b-filter_list_wrapper">
			<div class="b-result-doc">
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_4.jpg"></span>
						<div class="b-result-doclabel">
							Требуется авторизация
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">1.</span>
						<h2><a href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_27.jpg"></span>

					</div>
					<div class="iblock b-result-docinfo">
					<span class="num">2.</span>
						<h2><a  href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> Межпоселенческая централизованная библиотечная система Никольского муниципального района</div>
							
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_4.jpg"></span>
						<div class="b-result-doclabel">
							Требуется авторизация
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">1.</span>
						<h2><a href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_27.jpg"></span>

					</div>
					<div class="iblock b-result-docinfo">
					<span class="num">2.</span>
						<h2><a  href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> Межпоселенческая централизованная библиотечная система Никольского муниципального района</div>
							
						</div>
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_4.jpg"></span>
						<div class="b-result-doclabel">
							Требуется авторизация
						</div>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num">1.</span>
						<h2><a href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>					
					</div>

				</div>
				<div class="b-result-docitem">

					<div class="iblock b-result-docphoto">
						<span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="../pic/pic_27.jpg"></span>

					</div>
					<div class="iblock b-result-docinfo">
					<span class="num">2.</span>
						<h2><a  href="blind_card.php" title="Сумма технологии">Сумма технологии</a></h2>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> Станислав Лем</li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Количество страниц:</span> 427</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em>Источник:</em> Межпоселенческая централизованная библиотечная система Никольского муниципального района</div>
							
						</div>
					</div>


				</div>
				
				<div class="b-paging">
					<div class="b-paging_cnt">
						<a href="" title="предыдущая" class="b-paging_prev iblock">предыдущая</a>
						<a href="" title="1" class="b-paging_num current iblock">1</a>
						<a href="" title="2"  class="b-paging_num iblock">2</a>
						<a href="" title="3"  class="b-paging_num iblock">3</a>
						<a href="" title="4"  class="b-paging_num iblock">4</a>
						<a href="" title="следующая"  class="b-paging_next iblock">следующая</a>
					</div>
				</div><!-- /.b-paging -->
			</div><!-- /.b-result-doc -->
		</div><!-- /.b-filter_list_wrapper -->

</div><!-- /.homepage -->


<? include("footer.php"); ?>

</body>
</html>