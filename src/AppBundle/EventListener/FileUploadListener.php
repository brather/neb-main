<?php
namespace AppBundle\EventListener;

use Oneup\UploaderBundle\Event\PostPersistEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * User: agolodkov
 * Date: 22.09.2016
 * Time: 16:02
 */
class FileUploadListener
{

    /**
     * @var ContainerInterface
     */
    private $_container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->_container = $container;
    }

    /**
     * @param PostPersistEvent $event
     */
    public function onUpload(PostPersistEvent $event)
    {
        /** @var File $file */
        $file = $event->getFile();
        $response = $event->getResponse();
        $documentRoot = $this->_container->getParameter('dir.neb');
        $documentRoot = realpath($documentRoot);

        $files = [];
        $files[] = [
            'url'  => str_replace($documentRoot, '', $file->getRealPath()),
            'name' => $file->getBaseName(),
            'type' => $file->getType(),
            'size' => $file->getSize(),
        ];
        $response['files'] = $files;

    }
}