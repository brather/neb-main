/* удаление закладки */
$(function(){
    $(document).on('click','[data-delete-favorite-edition]',function(e){
        var toggler = $(e.target),
            editionItem = toggler.closest('[data-favorite-item]'),
            editionId = toggler.data('id');
            itemType = toggler.data('item-type');
            message = {
                title: 'Удаление элемента из подборки.',
                confirmTitle: "Удалить",
                text: toggler.data('modal-title')
            };
        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed){
                $.ajax({
                    url: '/local/tools/collections/list.php',
                    data: {
                        t: itemType,
                        id: editionId,
                        idCollection: $('#idCollection').val()
                    },
                    method: 'GET',
                    beforeSend: function(){
                        editionItem.addClass('deletion-progress');
                    },
                    success: function(data){
                        $(editionItem).slideUp(function(){
                            this.remove();
                        });
                        FRONT.reloadSideTags();
                    }
                });
            }
        });
    });
});