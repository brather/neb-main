<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Config\Option,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,

    \Neb\Main\Helper\UtilsHelper,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient,
    \Nota\Exalead\RslViewer;

/**
 * Class SearchPageComponent - страница поиска изданий
 */
class SearchPageComponent extends CBitrixComponent
{
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->includeModules(['nota.exalead']);
        $this->includeLibs();
        $this->prepareParams();

        $this->executeQuery();
    }

    /**
     * Сборка запроса
     * @return string
     */
    private function buildQuery() {

        $arQuery = [];

        $_REQUEST['q'] = SearchQuery::sanitizeQuery($_REQUEST['q']);

        // обычный поиск
        if (!empty($_REQUEST['q'])) {

            $_REQUEST['q'] = trim(htmlspecialcharsEx($_REQUEST['q']));

            // проверка на раскладку клавииатуры
            if (isset($_REQUEST['notransform']) && $_REQUEST['notransform']) {
                $_REQUEST["QUERY"] = $_REQUEST['q'];
            } else {
                $corrector = new Text_LangCorrect();
                $q = $corrector->parse($_REQUEST['q'], $corrector::KEYBOARD_LAYOUT);

                $this->arResult["REQUEST"]['QUERY'] = $q;
                if ($_REQUEST['q'] !== $q) {
                    $this->arResult["REQUEST"]['ORIGINAL_QUERY'] = $_REQUEST["q"];
                    $_REQUEST['q'] = $q;
                }
            }

            // выбор вида простого запроса
            if (true === $this->arResult['FULL_QUERY_SEARCH'])
                $arQuery[] = '(' . $_REQUEST['q'] . ')';
            elseif (true === $this->arResult['STRICT_QUERY_SEARCH'])
                $arQuery[] = '("' . $_REQUEST['q'] . '")';
            else
                $arQuery[] = 'simple:(' . $_REQUEST['q'] . ')';
        }
        // расширенный поиск
        else {

            $arExtendFields = [
                'q_author'        => 'authorbook',
                'q_name'          => 'title',
                'text'            => 'text',
                'publisher'       => 'publisher',
                'publishplace'    => 'publishplace',
                'library'         => 'library',
                'isbn'            => 'isbn',
                'bbk'             => 'bbk',
                'sectionbbk'      => 'bbkfull',
                'ud'              => 'ud',
                //'bbkfull'       => 'bbkfull',

                'museum'          => 'library',
                'material'        => 'material',
                'inventorynumber' => 'inventorynumber',

                'archive'          => 'library',
                'fund_number'      => 'fundnumber',
                'fund_name'        => 'fundname',
                'inventory_number' => 'inventorynumber',
                'inventory_name'   => 'inventoryname',
                'unit_number'      => 'unitnumber',
                'unit_name'        => 'unitname',

                'lang'             => 'lang',

                'declarant'        => 'declarant',
                'patentholder'     => 'patentholder'
            ];
            foreach ($arExtendFields as $k => $v) {
                if (!empty($_REQUEST[$k])) {
                    $arQuery[] = $v . ':(' . htmlspecialcharsbx($_REQUEST[$k]) . ')';
                }
            }

            // заглушка при пустом поиске
            if (empty($arQuery)) {
                $arQuery[] = '(#all)';
            }

            // признак доступа
            if ($_REQUEST['access'] == 'open') {
                $arQuery[] = 'isprotected:0';
            } elseif ($_REQUEST['access'] == 'closed') {
                $arQuery[] = 'isprotected>0';
            }

            // даты
            $sDateStart  = intval($_REQUEST['publishyear_prev']);
            $sDateFinish = intval($_REQUEST['publishyear_next']);

            if (!empty($sDateStart) && $sDateStart <= $sDateFinish
                && $sDateStart > SEARCH_BEGIN_YEAR && $sDateStart <= SEARCH_END_YEAR
            ) {
                $arQuery[] = 'publishyearint>=' . $sDateStart;
            }

            if (!empty($sDateStart) && $sDateStart <= $sDateFinish
                && $sDateFinish >= SEARCH_BEGIN_YEAR && $sDateFinish < SEARCH_END_YEAR
            ) {
                $arQuery[] = 'publishyearint<=' . $sDateFinish;
            }

            // ББК
            if (!empty($_REQUEST['bbkfull'])) {

                if (!is_array($_REQUEST['bbkfull']))
                    $_REQUEST['bbkfull'] = [$_REQUEST['bbkfull']];

                $arBbk = [];
                foreach ($_REQUEST['bbkfull'] as $sBbk)
                    $arBbk[] = 'bbkfull:("' . htmlspecialcharsbx($sBbk) . '")';
                $arQuery[] = '('. implode(' OR ', $arBbk) .')';
            }
        }

        // тип библиотеки
        if (isset($_REQUEST['librarytype']) && !empty($_REQUEST['librarytype'])) {
            $arQuery[] = 'librarytype:' . $_REQUEST['librarytype'];
        } else {
            $arQuery[] = 'librarytype:library';
        }

        // фильтрация по библиотеке пользователя (используется в планах оцифровки)
        if (intval($this->arParams['ID_LIBRARY']) > 0 && !$this->arParams['ID_COLLECTION']) {
            $arQuery[] = 'idlibrary:' . $this->arParams['ID_LIBRARY'];
        }

        // область поиска (в планах оцифровки исключаются издания с электрокопией)
        if (empty($_REQUEST['searcharea']) || $_REQUEST['searcharea'] == 'ebooks') {
            $arQuery[] = (intval($this->arParams['ID_LIBRARY']) > 0 && !$this->arParams['ID_COLLECTION']
                            ? 'NOT ' : '') . 'filesize>0';
        }

        // коллекции
        $arNewCollections = $this->getNewCollections();
        if (!empty($arNewCollections)) {
            $arQuery[] = $arNewCollections;
        }

        $sResult = implode(' AND ', $arQuery);

        return $sResult;
    }

    /**
     * Коллекции
     */
    private function getNewCollections() {

        $sResult = '';
        $arNewCollection = [];

        require_once ( $_SERVER["DOCUMENT_ROOT"] .  $this->GetPath() . '/../search.form/filter.php');
        $arCollections = NebFilterStructure::getSearchCollections();

        $bDefault = true;
        foreach ($arCollections as $arCollection) {
            if (
                $arCollection['SELECT'] == 'Y' && !in_array($arCollection['NAME'], $_REQUEST['newcollection'])
                || $arCollection['SELECT'] == 'N' && in_array($arCollection['NAME'], $_REQUEST['newcollection'])
            ) {
                $bDefault = false;
            }
        }

        if (empty($_REQUEST['newcollection']) || $bDefault) {
            foreach ($arCollections as $arCollection) {
                if ($arCollection['SELECT'] == 'N') {
                    $arNewCollection[] = 'collection_new:-("' . htmlspecialcharsbx($arCollection['NAME']) . '")';
                }
            }
            $sOperator = ' AND ';
        } else {
            foreach ($_REQUEST['newcollection'] as $sCollection)
                $arNewCollection[] = 'collection_new:("' . htmlspecialcharsbx($sCollection) . '")';
            $sOperator = ' OR ';
        }

        if (!empty($arNewCollection)) {
            $sResult = '(' . implode($sOperator, $arNewCollection) . ')';
        }

        return $sResult;
    }

    /**
     * Формирование параметров запроса к Exalead
     */
    private function prepareParams() {

        $arParams = $this->arParams;

        global $APPLICATION;

        /*$arParams['IS_ADD_PLAN_DIGIT'] = PlanDigitalization::isAdd();
        if ($arParams['IS_ADD_PLAN_DIGIT'])
            $arParams['BOOK_IN_PLAM'] = PlanDigitalization::getBookInPlan();*/

        $arParams['IS_SAVE_QUERY']     = saveQuery::isSave();
        $arParams['IS_SHOW_PROTECTED'] = nebUser::isShowProtectedContent();

        if ($arParams['BLIND'] == 'Y') {
            $arParams['ITEM_COUNT'] = 100;
        }
        else {
            $arParams['ITEM_COUNT'] = intval($_REQUEST['pagen']) ? : static::getDefaultPagen();
            if ($arParams['ITEM_COUNT'] > 60)
                $arParams['ITEM_COUNT'] = 60;
        }

        $arParams['SEARCH_QUERY_TRANSLATE'] = Option::get('nota.exalead', 'search_query_translate') == 'Y';

        $arParams['SHOW_LONG_PAGE'] = true;
        $arParams['LONG_PAGE'] = !empty($_REQUEST['longpage']);
        $arParams['NEXT_ITEM'] = intval($_REQUEST['next']);
        $arParams['MODE'] = empty($_REQUEST['mode']) ? 'list' : trim($_REQUEST['mode']);
        $arParams['cache'] = isset($arParams['cache']) ? (boolean)$arParams['cache'] : true;

        $this->arResult = [
            'STRICT_QUERY_SEARCH'     => $_REQUEST['s_strict'] == 'on',
            'FULL_QUERY_SEARCH'       => isset($_REQUEST['full_query_search']),

            'STRICT_QUERY_SEARCH_URL' => $APPLICATION->GetCurUri('s_strict=on'),
            'FULL_QUERY_SEARCH_URL'   => str_replace('s_strict=on', '', $APPLICATION->GetCurUri('full_query_search=1'))
        ];

        $this->arResult['REFINEMENT'] = '';
        $this->arResult['librarytype'] = $_REQUEST['librarytype'];

        // сортировка
        $arParams['BY']    = trim(htmlspecialcharsEx($_REQUEST['by']));
        $arParams['ORDER'] = trim(htmlspecialcharsEx($_REQUEST['order']));

        $arParams['q'] = $this->buildQuery();
        $arParams['source'] = $_REQUEST['q'];
        $arParams['QUERY_EXECUTE'] = true;

        $this->arParams = $arParams;
    }

    /**
     * Подключение модулей
     *
     * @param $arModules - массив модулей
     * @throws \Bitrix\Main\LoaderException
     */
    private static function includeModules($arModules) {
        foreach ($arModules as $sModule)
            Loader::includeModule($sModule);
    }

    /**
     * Подключение библиотек
     */
    private function includeLibs() {
        require_once ($_SERVER["DOCUMENT_ROOT"] . '/local/tools/suggest/ReflectionTypeHint.php');
        require_once ($_SERVER["DOCUMENT_ROOT"] . '/local/tools/suggest/Text/LangCorrect.php');
        require_once ($_SERVER["DOCUMENT_ROOT"] . '/local/tools/suggest/UTF8.php');
        //require_once ($_SERVER["DOCUMENT_ROOT"] . $this->getPath() . '/../search.form/filter.php');
    }

    /**
     * Исполнение запроса к Exalead
     */
    public function executeQuery()
    {
        $arParams = $this->arParams;
        $arResult = $this->arResult;

        global $APPLICATION;

        Loc::loadMessages(__FILE__);
        CPageOption::SetOptionString("main", "nav_page_in_session", "N");

        if ($arParams['QUERY_EXECUTE'])
        {
            $query = new SearchQuery();
            $query->setQuery($arParams['q']);
            $query->setPageLimit($arParams['ITEM_COUNT']);
            $query->setNavParams();

            // рубрика
            $sBbkFull = htmlspecialcharsbx(str_replace('/', ' ', $_REQUEST['category_name']));
            if (!empty($sBbkFull)) {

                $sBbkFull = ' AND ((bbkfull:"' . $sBbkFull . '" AND filesize>0){b =50000000} OR (bbkfull:"' . $sBbkFull
                    . '" AND NOT filesize>0){b =40000000} OR ("'.$sBbkFull.'" AND filesize>0){b =30000000} OR '.$sBbkFull . ')';

                $query->setParam('q', $query->getParameter('q') . $sBbkFull);
            }

            if (!empty($_REQUEST['slparam']) and $_REQUEST['slparam'] == 'sl_nofuzzy')
                $query->setParam('sl', 'sl_nofuzzy');

            if (!empty($_REQUEST['f_field'])) {
                $f_field = $_REQUEST['f_field'];
                if (!is_array($f_field))
                    $f_field = array($f_field);

                $terms = array();
                foreach ($f_field as $k => $v) {
                    if (!is_array($v))
                        $v = array($v);

                    foreach ($v as $fieldValue) {
                        if ('terms' === $k) {
                            $terms = '+' . htmlspecialcharsbx($fieldValue);
                            $query->setParam('r', $terms, true);
                        } else {
                            //$v = urldecode($v); WTF???
                            $v = htmlspecialcharsbx($fieldValue);
                            $query->setParam('r', '+' . $v, true);
                        }
                    }

                    // условие для поиска по библиотеке
                    if ('library' === $k && $_REQUEST['q'] == '' && !$_REQUEST['logic']) {
                        $query->setParam('sl', 'sl_statistic3');
                    }
                }
            }

            /**
             * Фильтрация по типу контента
             */
            if (!empty($_REQUEST['f_ft'])) {
                $client_ft = new SearchClient();
                $client_ft->setCacheEnabled($arParams['cache']);
                $result_ft = $client_ft->getResult($query);
                unset($result_ft['ITEMS'], $result_ft['GROUPS_KEY_VAL']);
                foreach ($result_ft['GROUPS'] as $k => $v)
                    if ($k != 'TYPEBOOK')
                        unset($result_ft['GROUPS'][$k]);
                $query->setParam('r', '+' . htmlspecialcharsbx(trim(urldecode($_REQUEST['f_ft']))), true);
            }

            if (!empty($arParams['BY']) && !empty($arParams['ORDER']))
                $query->setParam('s', $arParams['ORDER'] . '(' . $arParams['BY'] . ')');

            // Запрос к Exalead (по умолчанию первый этап поиска)
            $client = new SearchClient();
            $client->setCacheEnabled($arParams['cache']);

            for ($i=1; $i<=3; $i++) {

                $result = $client->getResult($query);

                if ($result['COUNT'] > 0 || $result['FULL_QUERY_SEARCH']) {
                    break;
                }
                else {
                    // повторный поиск (переход от первого к второму этапу) - без simple и в кавычках
                    if (false === $arResult['STRICT_QUERY_SEARCH']) {

                        $q = str_replace('simple:', '', $query->getParameter('q'));
                        $q = str_replace('(' . $arParams['source'] . ')',  '("' . $arParams['source'] . '")', $q);
                        $query->setQuery($q);

                        $arResult['STRICT_QUERY_SEARCH'] = true;
                    }
                    // повторный поиск (переход от второго к третьему этапу) - без simple и кавычкек
                    elseif (false === $arResult['FULL_QUERY_SEARCH']) {

                        $q = str_replace('("' . $arParams['source'] . '")',  '(' . $arParams['source'] . ')', $query->getParameter('q'));
                        $query->setQuery($q);

                        $arResult['STRICT_QUERY_SEARCH'] = false;
                        $arResult['FULL_QUERY_SEARCH'] = true;
                    }
                }
            }

            if (isset($result) && is_array($result) && count($result['ITEMS'])) {
                foreach ($result['ITEMS'] as &$item) {
                    $item["STRICT_QUERY_SEARCH"]    = $arResult['STRICT_QUERY_SEARCH'];
                    $item["FULL_QUERY_SEARCH"]      = $arResult['FULL_QUERY_SEARCH'];
                }
            }
            self::trackStat($arResult['librarytype'], intval($result['ITEMS'][0]['idlibrary']));

            $arResult['query'] = $query;
            $arResult['ADDITIONAL_SEARCH'] = false;

            // поиск по текстам изданий , если не найдено с simple
            $page = (isset($_REQUEST['PAGEN_1']) ? $_REQUEST['PAGEN_1'] : 1);
            if (empty($_REQUEST['s_in_results'])
                && ($_REQUEST['is_full_search'] == 'on')
                && $result['NHITS'] < 1
                //&& false === $arResult['SIMPLE_QUERY_SEARCH']
            ) {
                if ($result['NHITS'] > $arParams['ITEM_COUNT'] && $page >= intval(round($result['NHITS']) / $arParams['ITEM_COUNT']))
                    $arResult['FULL_SEARCH'] = true;

                $queryString = $query->getParameter('q');

                if ($arResult['STRICT_QUERY_SEARCH']) {
                    $query->setParam('q', str_replace('simple:(' . $arParams['source'] . ')', $arParams['source'], $queryString));
                } else {
                    $query->setParam('q', str_replace('simple:(' . $arParams['source'] . ')', '(' . $arParams['source'] . ')', $queryString));
                }
                $arResult['expand_query'] = 1;
                $client = new SearchClient();
                $client->setCacheEnabled($arParams['cache']);
                $result = $client->getResult($query);

                if ($result['NHITS'] > 0)
                    $arResult['ADDITIONAL_SEARCH'] = true;
            }

            if (!empty($result_ft['GROUPS']['TYPEBOOK'])) {
                $result['GROUPS']['TYPEBOOK'] = $result_ft['GROUPS']['TYPEBOOK'];
                $result['COUNT'] = $result_ft['COUNT'];
            }

            if (isset($result['spellcheckSuggestion'][0]) && !empty($result['spellcheckSuggestion'][0]))
                $arResult["REQUEST"]['QUERY_SUGGEST'] = $result['spellcheckSuggestion'][0];

            if (intval($result['COUNT']) > 0) {

                // получение постраничной навигации
                $client->getDBResult($result);
                if (!$arParams['LONG_PAGE']) {
                    $arResult['STR_NAV'] = $client->strNav;
                    $arResult['NavNum'] = $client->arNavParam['NavNum'];
                }

                $arResult += $result;

                if ($arParams['LONG_PAGE']) {
                    $nextPage = $client->arNavParam['NavPageNomer'] + 1;
                    if ($nextPage <= $client->arNavParam['NavPageCount']) {
                        $arResult['NEXT_URL_PARAMS'] = parse_url(
                            $APPLICATION->GetCurPageParam(
                                'PAGEN_' . $client->arNavParam['NavNum'] . '='
                                . $nextPage,
                                array('PAGEN_' . $client->arNavParam['NavNum'], 'dop_filter')
                            ), PHP_URL_QUERY
                        );
                    }
                }

                //список рубрик по поисковой фразе
                $query_bbk = new SearchQuery('bbk_bbkfull:' . $_REQUEST['q']);
                $query_bbk->setParam('sl', 'sl0');
                $query_bbk->setParam('st', 'st_bbk');
                $query_bbk->setPageLimit(3);

                $client = new SearchClient();
                $client->setCacheEnabled($arParams['cache']);
                $result_bbk = $client->getResult($query_bbk);
                $arResult['bbkfull_strong'] = $result_bbk["bbkfull_strong"];
                if ($result_bbk['COUNT'] > 0)
                    $arResult['BBK'] = $result_bbk['ITEMS'];

            } else {

                // вы имели ввиду
                $query = new SearchQuery();
                $query->getSpellcheck($_REQUEST['q']);

                $client = new SearchClient();
                $client->setCacheEnabled($arParams['cache']);
                $result = $client->getResult($query);
                $arResult['REFINEMENT'] = $result['REFINEMENT'];
            }

            $this->arParams = $arParams;
            $this->arResult = $arResult;

            // получает актуальные признаки доступа для книг из сервиса выдачи
            $this->getAccessType();

            // вы искали на портале
            $this->youSearch($result);
        }

        if ($_REQUEST['dop_filter'] == 'on' && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
            $APPLICATION->RestartBuffer();

        if (empty($_REQUEST['dop_filter']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
            $this->arParams['ajax'] = true;

        // cookie для автоперехода на страницу с подстветкой в просмотровщике #18534
        if (isset($_GET['q']) && !empty($_GET['q']) // обосновать
            && empty($this->getTemplateName()) || in_array($this->getTemplateName(), ['.default', 'blind']) // обосновать
        ){
            setcookie('__qSearch', base64_encode($_GET['q']), time() + 3600, '/', $_SERVER['HTTP_HOST']);
        }

        $this->includeComponentTemplate();

        if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
            exit();
    }

    /**
     * Получение из настроек пользователя количетсва результатов на страницу
     *
     * @param int $iResult
     * @return int
     */
    private static function getDefaultPagen($iResult = 15) {

        $obUser = new CUser();
        if ($obUser->IsAuthorized()) {
            $arUser = CUser::GetByID($obUser->GetID())->Fetch();
            if (intval($arUser['UF_SEARCH_PAGE_COUNT']) > 0)
                $iResult = $arUser['UF_SEARCH_PAGE_COUNT'];
        }

        return $iResult;
    }

    /**
     * Получает актуальные признаки доступа для книг из сервиса выдачи
     */
    private function getAccessType() {

        $arId = [];
        foreach ($this->arResult['ITEMS'] as $arItem) {
            $arId[] = $arItem['id'];
        }

        $arAccessBooks = (new RslViewer())->isAvailableBooks($arId);

        foreach ($this->arResult['ITEMS'] as &$arItem) {
            if (isset($arAccessBooks[$arItem['id']])) {
                $arItem['isprotected'] = $arAccessBooks[$arItem['id']];
            }
        }
    }

    /**
     * Вы искали на портале
     */
    private function youSearch($result) {

        global $APPLICATION;

        $arSearchResultParams = $arSearchSuggestion = [];

        if (!empty($_REQUEST['theme']) && is_array($_REQUEST['theme'])) {

            foreach ($_REQUEST['theme'] as $k => $v) {

                $arTParams = array();
                $arTParams['logic'] = $_REQUEST['logic'];
                $arTParams['theme'] = $_REQUEST['theme'];
                $arTParams['text'] = $_REQUEST['text'];
                $arTParams['isprotected'] = $_REQUEST['isprotected'];

                unset($arTParams['logic'][$k]);
                unset($arTParams['theme'][$k]);
                unset($arTParams['text'][$k]);
                unset($arTParams['isprotected'][$k]);

                $logic = htmlspecialcharsEx($_REQUEST['logic'][$k]);
                $notValue = '';
                if ('NOT' === $logic) {
                    $notValue = Loc::getMessage('SEARCH_PARAM_LOGIC_NOT') . ' ';
                    $logic = '';
                }

                if ($_REQUEST['theme'][$k] == 'foraccess') {
                    $arSearchResultParams[] = array(
                        'text'  => Loc::getMessage(
                            'SEARCH_PARAM_' . $v,
                            array('#VALUE#' => $notValue . Loc::getMessage(
                                    'SEARCH_PARAM_ACCESS_'
                                    . intval($_REQUEST['isprotected'][$k])
                                ))
                        ),
                        'logic' => Loc::getMessage('SEARCH_PARAM_LOGIC_' . $logic),
                        'url'   => $APPLICATION->GetCurPageParam(
                            http_build_query($arTParams),
                            array('logic', 'theme', 'text', 'isprotected')
                        )
                    );
                } else {
                    if ($v != 'foraccess' and empty($_REQUEST['text'][$k]))
                        continue;

                    $val = htmlspecialcharsEx($_REQUEST['text'][$k]);

                    $arSearchResultParams[] = array(
                        'text'  => Loc::getMessage(
                            'SEARCH_PARAM_' . $v, array('#VALUE#' => $notValue . $val)
                        ),
                        'logic' => Loc::getMessage('SEARCH_PARAM_LOGIC_' . $logic),
                        'url'   => $APPLICATION->GetCurPageParam(
                            http_build_query($arTParams),
                            array('logic', 'theme', 'text', 'isprotected')
                        )
                    );

                    $arSearchSuggestion[] = array(
                        'text'  => Loc::getMessage(
                            'SEARCH_PARAM_' . $v, array('#VALUE#' => str_replace(
                                $result['spellcheckSuggestionOrig'],
                                $result['spellcheckSuggestion'], $notValue . $val
                            ))
                        ),
                        'logic' => Loc::getMessage('SEARCH_PARAM_LOGIC_' . $logic)
                    );
                }
            }
        }

        if (intval($this->arParams['date_start']) > SEARCH_BEGIN_YEAR
            || (!empty($this->arParams['date_finish']) && $this->arParams['date_finish'] < SEARCH_END_YEAR)) {
            $arSearchResultParams[] = array(
                'text'  => Loc::getMessage(
                    'SEARCH_PARAM_PUBLISHYEAR', array('#VALUE_1#' => $this->arParams['date_start'],
                        '#VALUE_2#' => $this->arParams['date_finish'])
                ),
                'logic' => Loc::getMessage('SEARCH_PARAM_LOGIC_AND'),
                'url'   => $APPLICATION->GetCurPageParam('', ['publishyear_prev', 'publishyear_next'])
            );
        }

        /**
         * Поиск по тегам
         */
        if (!empty($_REQUEST['f_field'])) {
            foreach ($_REQUEST['f_field'] as $k => $v) {
                $v = urldecode($v);

                if (isset($arResult['GROUPS_KEY_VAL'][$v])) {

                    $arFFParams = $_REQUEST['f_field'];
                    unset($arFFParams[$k]);
                    $k = strtoupper($k);
                    $arSearchResultParams[] = array(
                        'text'  => Loc::getMessage(
                            'SEARCH_PARAM_' . $k,
                            array('#VALUE#' => $arResult['GROUPS'][$k][$arResult['GROUPS_KEY_VAL'][$v]]['title'])
                        ),
                        'logic' => Loc::getMessage('SEARCH_PARAM_LOGIC_AND'),
                        'url'   => $APPLICATION->GetCurPageParam(
                            http_build_query(array('f_field' => $arFFParams)),
                            array("f_field")
                        )
                    );
                }
            }
        }

        $this->arResult['RESULT_PARAMS']  = $arSearchResultParams;
        $this->arResult['SUGGEST_PARAMS'] = $arSearchSuggestion;
    }

    /**
     * Трек статистики
     *
     * @param $sLibraryType
     * @param $iIdLibrary
     */
    private static function trackStat($sLibraryType, $iIdLibrary) {

        if (UtilsHelper::checkSearcherBot()) {
            return;
        }

        $trackParams = [
            'query'      => $_REQUEST['q'],
            'session_id' => session_id(),
            'user_id'    => intval(nebUser::getCurrent()->GetID()),
        ];
        if ('library' === $sLibraryType && $iIdLibrary > 0 && isset($_REQUEST['f_field']['library'])) {
            self::includeModules(['notaext']);
            $arLib = \Bitrix\NotaExt\Iblock\Element::getList(
                array(
                    'IBLOCK_ID'             => \Bitrix\NotaExt\Iblock\IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY),
                    'PROPERTY_LIBRARY_LINK' => $iIdLibrary
                ),
                1,
                array('ID', 'skip_other' => true)
            );
            if (!empty($arLib['ITEM'])) {
                $trackParams['library_id'] = $arLib['ITEM']['ID'];
            }
        }

        \Neb\Main\Stat\Tracker::track('neb_search_queries', $trackParams);
    }
}