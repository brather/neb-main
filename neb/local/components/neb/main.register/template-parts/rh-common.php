<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

?>
<div class="b-form_header"><?=GetMessage("MAIN_REGISTER_LOGIN");?></div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="settings04"><?=GetMessage("REGISTER_FIELD_EMAIL");?></label>
        <div class="field validate">
            <input type="text" class="input" name="REGISTER[EMAIL]" data-minlength="5" data-maxlength="50" id="settings04" data-validate="email" value="<?=$arResult["VALUES"]['EMAIL']?>" data-required="required">
            <em class="error validate"><?=GetMessage("REGISTER_FIELD_EMAIL_FORMAT");?></em>
            <em class="error required"><?=GetMessage("REGISTER_FIELD_REQ");?></em>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>

        <label for="settings05"><?=GetMessage("REGISTER_FIELD_PASSWORD");?></label>
        <div class="field validate">
            <input type="password" data-validate="password" data-required="required" value="<?=$arResult["VALUES"]['PASSWORD']?>" id="settings05" data-maxlength="30" data-minlength="6" name="REGISTER[PASSWORD]" class="pass_status input">
            <em class="error required"><?=GetMessage("REGISTER_FIELD_REQ");?></em>
            <em class="error validate"><?=GetMessage("MAIN_REGISTER_PASSWORD_LENGTH_MIN");?></em>
            <em class="error maxlength"><?=GetMessage("MAIN_REGISTER_PASSWORD_LENGTH_MAX");?></em>
        </div>
        <div class="passecurity">
            <div class="passecurity_lb"><?=GetMessage("MAIN_REGISTER_PASSWORD_SECURE")?></div>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="settings55"><?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD");?></label>
        <div class="field validate">
            <input data-identity="#settings05" type="password" data-required="required" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" id="settings55" data-maxlength="30" data-minlength="6" name="REGISTER[CONFIRM_PASSWORD]" class="input" data-validate="password">
            <em class="error identity "><?=GetMessage("MAIN_REGISTER_PASSWORD_MISMATCH");?></em>
            <em class="error required"><?=GetMessage("REGISTER_FIELD_REQ");?></em>
            <em class="error validate"><?=GetMessage("MAIN_REGISTER_PASSWORD_LENGTH_MIN");?></em>
            <em class="error maxlength"><?=GetMessage("MAIN_REGISTER_PASSWORD_LENGTH_MAX");?></em>
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <label for="settings11"><?= Loc::getMessage(
                'REGISTER_FIELD_PERSONAL_MOBILE'
            ); ?></label>

        <div class="field validate">
            <input type="text" class="input maskphone"
                   name="REGISTER[PERSONAL_MOBILE]" id="settings11"
                   value="<?= $arResult["VALUES"]['PERSONAL_MOBILE'] ?>">
                        <span class="phone_note"><?= Loc::getMessage(
                                'REGISTER_FIELD_PERSONAL_MOBILE_NOTE'
                            ); ?></span>
        </div>
    </div>
</div>
<div class="wrapfield">
    <div class="b-form_header"><span
            class="iblock"><?= Loc::getMessage(
                'REGISTER_GROUP_ADDRESS_REGISTERED'
            ); ?></span></div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings03"><?= Loc::getMessage(
                    'REGISTER_FIELD_PERSONAL_ZIP'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_REG_NOTE'
                    ); ?></span></label>

            <div class="field validate w140">
                <em class="error required"><?= Loc::getMessage(
                        'MAIN_REGISTER_REQ'
                    ); ?></em>
                <em class="error number"><?= Loc::getMessage(
                        'REGISTER_FIELD_PERSONAL_ZIP_INCORRECT'
                    ); ?></em>
                <input type="text" data-validate="number"
                       data-required="required"
                       value="<?= $arResult["VALUES"]['PERSONAL_ZIP'] ?>"
                       id="settings03" name="REGISTER[PERSONAL_ZIP]"
                       class="input ">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings033"><?= Loc::getMessage(
                    'MAIN_REGISTER_REGION'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_REG_NOTE'
                    ); ?></span></label>

            <div class="field validate">
                <em class="error required"><?= Loc::getMessage(
                        'MAIN_REGISTER_REQ'
                    ); ?></em>
                <input type="text" data-required="required"
                       value="<?= $arResult["VALUES"]['UF_REGION'] ?>"
                       id="settings033" data-maxlength="30"
                       data-minlength="2" name="UF_REGION"
                       class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <label for="settings013"><?= Loc::getMessage(
                    'MAIN_REGISTER_AREA'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_REG_NOTE'
                    ); ?></span></label>

            <div class="field validate">
                <input type="text"
                       value="<?= $arResult["VALUES"]['UF_AREA'] ?>"
                       id="settings013" data-maxlength="30"
                       data-minlength="2" name="UF_AREA"
                       class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings055"><?= Loc::getMessage(
                    'REGISTER_FIELD_PERSONAL_CITY'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_REG_NOTE'
                    ); ?></span></label>

            <div class="field validate">
                <em class="error required"><?= Loc::getMessage(
                        'MAIN_REGISTER_REQ'
                    ); ?></em>
                <input type="text" data-required="required"
                       value="<?= $arResult["VALUES"]['PERSONAL_CITY'] ?>"
                       id="settings055" data-maxlength="30"
                       data-minlength="2"
                       name="REGISTER[PERSONAL_CITY]" class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings07"><?= Loc::getMessage(
                    'REGISTER_FIELD_PERSONAL_STREET'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_REG_NOTE'
                    ); ?></span></label>

            <div class="field validate">
                <em class="error required"><?= Loc::getMessage(
                        'MAIN_REGISTER_REQ'
                    ); ?></em>
                <input type="text"
                       value="<?= $arResult["VALUES"]['PERSONAL_STREET'] ?>"
                       id="settings07" data-maxlength="30"
                       data-minlength="2" data-required="true"
                       name="REGISTER[PERSONAL_STREET]"
                       class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>

            <div class="field iblock fieldthree ">
                <label for="settings09"><?= Loc::getMessage(
                        'REGISTER_FIELD_PERSONAL_HOUSE'
                    ); ?>:</label>

                <div class="field validate">
                    <input type="text" class="input"
                           name="UF_CORPUS" data-maxlength="20"
                           id="settings09"
                           value="<?= $arResult["VALUES"]['UF_CORPUS'] ?>"
                           data-required="true"/>
                    <em class="error required"><?= Loc::getMessage(
                            'MAIN_REGISTER_REQ'
                        ); ?></em>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings10"><?= Loc::getMessage(
                        'REGISTER_FIELD_PERSONAL_BUILDING'
                    ); ?>: </label>

                <div class="field">
                    <input type="text" class="input"
                           name="UF_STRUCTURE" data-maxlength="20"
                           maxlength="4" id="settings10"
                           value="<?= $arResult["VALUES"]['UF_STRUCTURE'] ?>"/>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings11"><?= Loc::getMessage(
                        'REGISTER_FIELD_PERSONAL_APPARTMENT'
                    ); ?>: </label>

                <div class="field">
                    <input type="text" class="input" name="UF_FLAT"
                           data-maxlength="20" maxlength="4"
                           id="settings11"
                           value="<?= $arResult["VALUES"]['UF_FLAT'] ?>"/>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapfield">
    <div class="b-form_header">
                    <span class="iblock"><?= Loc::getMessage(
                            'REGISTER_GROUP_ADDRESS_RESIDENCE'
                        ); ?></span>

        <div class="checkwrapper">
            <input class="checkbox addrindently" type="addr"
                   name="UF_PLACE_REGISTR" id="cb3r"><label
                for="cb3r" class="black"><?= Loc::getMessage(
                    'REGISTER_GROUP_ADDRESS_CHECKBOX_NOTE'
                ); ?></label>
            <?
            if (!empty($arResult["VALUES"]['UF_PLACE_REGISTR'])) {
                ?>
                <script type="text/javascript">
                    $(function () {
                        $('input[name="UF_PLACE_REGISTR"]').click();
                    });
                </script>
            <?
            }
            ?>
            <script type="text/javascript">
                $(function () {
                    $('#cb3r').change(function () {
                        $(this).attr('value', +$(this)[0].checked);
                        $(".hidden-fieldgroup").each(function () {
                            $(this).attr('value', $("input[name='" + $(this).attr('name') + "'][type != 'hidden']").attr('value'));
                        })
                    });
                });
            </script>
        </div>
    </div>
    <input type="hidden" class="hidden-fieldgroup"
           data-validate="number" value=""
           name="REGISTER[WORK_ZIP]">

    <div class="fieldrow nowrap">
        <div class="fieldcell iblock inly">
            <label for="settings044"><?= Loc::getMessage(
                    'REGISTER_FIELD_PERSONAL_ZIP'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_RES_NOTE'
                    ); ?></span></label>

            <div class="field validate w140">
                <input type="text" data-validate="number"
                       value="<?= $arResult["VALUES"]['WORK_ZIP'] ?>"
                       id="settings044" name="REGISTER[WORK_ZIP]"
                       class="input">
            </div>
        </div>
    </div>
    <input type="hidden" class="hidden-fieldgroup" value=""
           name="UF_REGION2">

    <div class="fieldrow nowrap">
        <div class="fieldcell iblock inly">
            <label for="settings033r"><?= Loc::getMessage(
                    'MAIN_REGISTER_REGION'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_RES_NOTE'
                    ); ?></span></label>

            <div class="field validate">
                <input type="text"
                       value="<?= $arResult["VALUES"]['UF_REGION2'] ?>"
                       id="settings033r" data-maxlength="30"
                       data-minlength="2" name="UF_REGION2"
                       class="input">
            </div>
        </div>
    </div>
    <input type="hidden" class="hidden-fieldgroup" value=""
           name="UF_AREA2">

    <div class="fieldrow nowrap">
        <div class="fieldcell iblock inly">
            <label for="settings013r"><?= Loc::getMessage(
                    'MAIN_REGISTER_AREA'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_RES_NOTE'
                    ); ?></span></label>

            <div class="field validate">
                <input type="text"
                       value="<?= $arResult["VALUES"]['UF_AREA2'] ?>"
                       id="settings013r" data-maxlength="30"
                       data-minlength="2" name="UF_AREA2"
                       class="input">
            </div>
        </div>
    </div>
    <input type="hidden" class="hidden-fieldgroup" value=""
           name="REGISTER[WORK_CITY]">

    <div class="fieldrow nowrap">
        <div class="fieldcell iblock inly">
            <label for="settings066"><?= Loc::getMessage(
                    'REGISTER_FIELD_PERSONAL_CITY'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_RES_NOTE'
                    ); ?></span></label>

            <div class="field validate">
                <input type="text"
                       value="<?= $arResult["VALUES"]['WORK_CITY'] ?>"
                       id="settings066" data-maxlength="30"
                       data-minlength="2" name="REGISTER[WORK_CITY]"
                       class="input">
            </div>
        </div>
    </div>
    <input type="hidden" class="hidden-fieldgroup" value=""
           name="REGISTER[WORK_STREET]">

    <div class="fieldrow nowrap">
        <div class="fieldcell iblock inly">
            <label for="settings08r"><?= Loc::getMessage(
                    'REGISTER_FIELD_PERSONAL_STREET'
                ); ?> <span class="minscreen"><?= Loc::getMessage(
                        'REGISTER_GROUP_ADDRESS_RES_NOTE'
                    ); ?></span></label>

            <div class="field validate">
                <input type="text"
                       value="<?= $arResult["VALUES"]['WORK_STREET'] ?>"
                       id="settings08r" data-maxlength="30"
                       data-minlength="2"
                       name="REGISTER[WORK_STREET]" class="input">
            </div>
        </div>
    </div>
    <input type="hidden" class="hidden-fieldgroup" value=""
           name="UF_HOUSE2">
    <input type="hidden" class="hidden-fieldgroup" value=""
           name="UF_STRUCTURE2">
    <input type="hidden" class="hidden-fieldgroup" value=""
           name="UF_FLAT2">

    <div class="fieldrow nowrap">
        <div class="fieldcell iblock inly">
            <div class="field iblock fieldthree ">
                <label for="settings09r"><?= Loc::getMessage(
                        'REGISTER_FIELD_PERSONAL_HOUSE'
                    ); ?>:</label>

                <div class="field validate">
                    <input type="text" class="input"
                           name="UF_HOUSE2" data-maxlength="20"
                           id="settings09r"
                           value="<?= $arResult["VALUES"]['UF_HOUSE2'] ?>"/>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings10r"><?= Loc::getMessage(
                        'REGISTER_FIELD_PERSONAL_BUILDING'
                    ); ?>: </label>

                <div class="field">
                    <input type="text" class="input"
                           name="UF_STRUCTURE2" data-maxlength="20"
                           maxlength="4" id="settings10r"
                           value="<?= $arResult["VALUES"]['UF_STRUCTURE2'] ?>"/>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings11r"><?= Loc::getMessage(
                        'REGISTER_FIELD_PERSONAL_APPARTMENT'
                    ); ?>: </label>

                <div class="field">
                    <input type="text" class="input" name="UF_FLAT2"
                           data-maxlength="20" maxlength="4"
                           id="settings11r"
                           value="<?= $arResult["VALUES"]['UF_FLAT2'] ?>"/>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="b-form_header"><?=Loc::getMessage("MAIN_REGISTER_PAYMENT");?></div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_BANK_NAME"><?=Loc::getMessage("REGISTER_FIELD_BANK_NAME");?></label>
        <div class="field validate">
            <input type="text" class="input" name="UF_BANK_NAME"
                   id="UF_BANK_NAME" value="<?=$arResult['VALUES']['UF_BANK_INN']?>" data-required="required">
            <em class="error required"><?=Loc::getMessage("REGISTER_FIELD_REQ");?></em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_BANK_INN"><?=Loc::getMessage("REGISTER_FIELD_BANK_INN");?></label>
        <div class="field validate">
            <input type="text" class="input" name="UF_BANK_INN"
                   id="UF_BANK_INN"
                   value="<?=$arResult['VALUES']['UF_BANK_INN']?>" data-required="required">
            <em class="error required"><?=Loc::getMessage("REGISTER_FIELD_REQ");?></em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_BANK_KPP"><?=Loc::getMessage("REGISTER_FIELD_BANK_KPP");?></label>
        <div class="field validate">
            <input type="text" class="input" name="UF_BANK_KPP"
                   id="UF_BANK_KPP" value="<?=$arResult['VALUES']['UF_BANK_KPP']?>" data-required="required">
            <em class="error required"><?=Loc::getMessage("REGISTER_FIELD_REQ");?></em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_BANK_BIC"><?=Loc::getMessage("REGISTER_FIELD_BANK_BIC");?></label>
        <div class="field validate">
            <input type="text" class="input" name="UF_BANK_BIC"
                   id="UF_BANK_BIC" value="<?=$arResult['VALUES']['UF_BANK_BIC']?>" data-required="required">
            <em class="error required"><?=Loc::getMessage("REGISTER_FIELD_REQ");?></em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_COR_ACCOUNT"><?=Loc::getMessage("REGISTER_FIELD_COR_ACCOUNT");?></label>
        <div class="field validate">
            <input type="text" class="input" name="UF_COR_ACCOUNT"
                   id="UF_COR_ACCOUNT"
                   value="<?=$arResult['VALUES']['UF_COR_ACCOUNT']?>"
                   data-required="required">
            <em class="error required"><?=Loc::getMessage("REGISTER_FIELD_REQ");?></em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_PERSONAL_ACCOUNT"><?=Loc::getMessage("REGISTER_FIELD_PERSONAL_ACCOUNT");?></label>
        <div class="field validate">
            <input type="text" class="input" name="UF_PERSONAL_ACCOUNT"
                   id="UF_PERSONAL_ACCOUNT" value="<?=$arResult['VALUES']['UF_PERSONAL_ACCOUNT']?>"
                   data-required="required">
            <em class="error required"><?=Loc::getMessage("REGISTER_FIELD_REQ");?></em>
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_FOUNDATION"><?= Loc::getMessage(
                'REGISTER_FIELD_FUNDATION'
            ); ?></label>

        <div class="field validate iblock">
            <em class="error required"><?= Loc::getMessage(
                    'MAIN_REGISTER_REQ'
                ); ?></em>
            <select name="UF_FOUNDATION" id="UF_FOUNDATION"
                    class="js_select w370" data-required="true">
                <option value=""><?= Loc::getMessage(
                        'MAIN_REGISTER_SELECT'
                    ); ?></option>
                <?php
                foreach (
                    $arResult['FIELD_VALUES']['UF_FOUNDATION'] as
                    $field
                ) {
                    ?>
                    <option <?=
                    $arResult['VALUES']['UF_FOUNDATION']
                    == $field['ID'] ? 'selected="selected"'
                        : '' ?>
                        value="<?= $field['ID'] ?>">
                        <?= $field['VALUE'] ?>
                    </option>
                <?
                }
                ?>
            </select>
        </div>
    </div>
</div>

<?$APPLICATION->IncludeComponent(
    "notaext:plupload",
    "rightholder_documents",
    array(
        "MAX_FILE_SIZE" => "10",
        "FILE_TYPES" => "jpg,png,gif,jpeg,pdf,doc,docx",
        "DIR" => "tmp_register",
        "FILES_FIELD_NAME" => "register_file",
        "MULTI_SELECTION" => "N",
        "CLEANUP_DIR" => "Y",
        "UPLOAD_AUTO_START" => "Y",
        "RESIZE_IMAGES" => "Y",
        "RESIZE_WIDTH" => "110",
        "RESIZE_HEIGHT" => "110",
        "RESIZE_CROP" => "Y",
        "RESIZE_QUALITY" => "98",
        "UNIQUE_NAMES" => "Y"
    ),
    $component
);?>
