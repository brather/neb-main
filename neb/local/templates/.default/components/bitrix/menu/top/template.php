<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<nav class="main-navigation clearfix">
    <a href="#" class="main-navigation__hamburger-btn">
        <img src="/local/templates/adaptive/img/h-menu.png" alt="">
    </a>

    <?php if($APPLICATION->GetCurPage(false) != SITE_DIR):?>
        <a class="main-navigation__logo" href="/">
            <img alt="На главную" src="/local/templates/adaptive/img/logo-nav.png">
        </a>
    <?php endif; ?>

    <?if (!empty($arResult)):?>
        <ul class="main-navigation__general clearfix">
            <?php foreach($arResult as $arItem): ?>

                <li class="main-navigation__general-kind"><a title="<?=$arItem["TEXT"]?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>

            <?php endforeach; ?>
            <li class="adaptive-genlink main-navigation__general-kind">
                <a href="/blind/" class="genlink blind-version-link" title="Раздел для слепых">Раздел для слепых</a>
            </li>
            <li class="adaptive-genlink main-navigation__general-kind">
                <a href="javascript:void(0);" class="genlink main-navigation__additional-link--low-vision" title="Настройки для слабовидящих" onclick="setBlind()">
                    <?php if(!empty($_COOKIE['blind_version'])) {?>Свернуть настройки для слабовидящих<?php } else {?>Показать настройки для слабовидящих<?php } ?>
                </a>
            </li>
        </ul>
    <?php endif; ?>

    <ul class="main-navigation__additional clearfix">
        
        <li class="adaptive-addlink main-navigation__additional-kind">
            <a href="/blind/" class="main-navigation__additional-link blind-version-link" title="Раздел для слепых">&nbsp;</a>
        </li>
        <li class="adaptive-addlink main-navigation__additional-kind">
            <a href="javascript:void(0);" class="main-navigation__additional-link main-navigation__additional-link--low-vision" title="Настройки для слабовидящих" onclick="setBlind()">
                <img 
                    class="main-navigation__additional-low-vision-img" 
                    src="/local/templates/adaptive/img/glass.png" 
                    alt="Настройки для слабовидящих"
                    title="<?php if(!empty($_COOKIE['blind_version'])) {?>Свернуть настройки для слабовидящих<?php } else {?>Показать настройки для слабовидящих<?php } ?>"
                    >
            </a>
        </li>
        <?php // Смена языка
        $sRequestPath = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?') ? : 10000) . '?' . DeleteParam(['setlang']);
        ?>
        <?php if('en' == LANGUAGE_ID):?>
            <li class="main-navigation__additional-kind">
                <noindex><a href="<?=$sRequestPath?>&setlang=ru" class="main-navigation__additional-link" title="Russian version" rel="nofollow">Ru</a></noindex>
            </li>
        <?php else:?>
            <li class="main-navigation__additional-kind">
                <noindex><a href="<?=$sRequestPath?>&setlang=en" class="main-navigation__additional-link" title="Английская версия" rel="nofollow">En</a></noindex>
            </li>
        <?php endif;?>
        <?php if (!$USER->IsAuthorized()):?>
            <li class="main-navigation__additional-kind">
                <a href="/auth/" class="main-navigation__additional-link neb-auth-link" title="<?=Loc::getMessage('TOP_MENU_LOGIN'); ?>"><?=Loc::getMessage('TOP_MENU_LOGIN'); ?></a>
            </li>
            <li class="main-navigation__additional-kind">
                <a href="/auth/reg.php" class="main-navigation__additional-link" title="<?=Loc::getMessage('TOP_MENU_REG'); ?>"><?=Loc::getMessage('TOP_MENU_REG'); ?></a>
            </li>
        <?php endif;?>
        <?php if ($USER->IsAuthorized()):?>
            <li class="main-navigation__additional-kind">
                <a title="Личный кабинет" class="main-navigation__additional-link main-navigation__additional-link--lk" href="/profile/">
                    <img alt="Личный кабинет" src="/local/templates/adaptive/img/lk-icon.png" class="main-navigation__additional-lk-img">
                </a>
            </li>

            <li class="main-navigation__additional-kind">
                <a title="Выйти" class="main-navigation__additional-link main-navigation__additional-link--exit" href="/?logout=yes">
                    <img alt="Выйти" src="/local/templates/adaptive/img/exit-icon.png" class="main-navigation__additional-lk-img">
                </a>
            </li>

        <?php endif;?>
    </ul>
</nav>

