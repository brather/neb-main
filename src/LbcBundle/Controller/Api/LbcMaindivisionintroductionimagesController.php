<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 11:00
 */

namespace LbcBundle\Controller\Api;

use Doctrine\Bundle\DoctrineBundle\Registry;
use LbcBundle\Entity\LbcMaindivisionintroduction;
use LbcBundle\Entity\LbcMaindivisionintroductionimages;
use LbcBundle\Entity\Repository\LbcMaindivisionintroductionimagesRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class LbcMaindivisionintroductionimagesController
 *
 * @Route("/api/bbk")
 * @DI\Service("app.api.maindivision.introduction.images", scope="request")
 */
class LbcMaindivisionintroductionimagesController extends AbstractImagesController
{

    /**
     * @param int $id
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     *
     * @return array
     */
    public function imagesAction($id)
    {
        /** @var LbcMaindivisionintroductionimagesRepository $repository */
        $repository = $this->getRepository();
        $id = (integer)$id;
        if ($id < 1) {
            throw new BadRequestHttpException('Empty ' . $this->getOwnFieldName());
        }
        /** @var LbcMaindivisionintroductionimages[] $entities */
        $entities = $repository->findAllByMaindivision($id);
        $this->prepareImagePath($entities);

        return [
            'entities' => $entities,
        ];
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcMaindivisionintroductionimages';
    }

    /**
     * @return string
     */
    public function getOwnFieldName()
    {
        return 'maindivisionintroductionid';
    }
}