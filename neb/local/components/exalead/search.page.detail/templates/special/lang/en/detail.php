<?
$MESS['LIB_SEARCH_PAGE_DETAIL_ADD_TO_LIB'] = '<span>Add</span> to My library';
$MESS['LIB_SEARCH_PAGE_DETAIL_REMOVE_FROM_LIB'] = '<span>Remove the book</span> from My library';
$MESS['LIB_SEARCH_PAGE_DETAIL_FIND_NEARLY'] = 'Find a book in a nearby library';

$MESS['LIB_SEARCH_PAGE_DETAIL_PAGES'] = 'Pages';
$MESS['LIB_SEARCH_PAGE_DETAIL_YEAR'] = 'Year of publication';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH'] = 'Publisher';
$MESS['LIB_SEARCH_PAGE_DETAIL_AUTHOR'] = 'Author';
$MESS['LIB_SEARCH_PAGE_DETAIL_TITLE'] = 'Title';
$MESS['LIB_SEARCH_PAGE_DETAIL_SUBTITLE'] = 'Subtitle';
$MESS['LIB_SEARCH_PAGE_DETAIL_LIB'] = 'Library';
$MESS['LIB_SEARCH_PAGE_DETAIL_SHARE'] = 'Share with friends';
$MESS['LIB_SEARCH_PAGE_DETAIL_RESPONSIBILITY'] = 'Responsibility';

$MESS['LIB_SEARCH_PAGE_DETAIL_SERIE'] = 'Chapter';
$MESS['LIB_SEARCH_PAGE_DETAIL_EDITION'] = 'Edition';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE'] = 'Publish place';
$MESS['LIB_SEARCH_PAGE_DETAIL_ANNOTATION'] = 'Annotation';
$MESS['LIB_SEARCH_PAGE_DETAIL_VIEW'] = 'View';
$MESS['LIB_SEARCH_PAGE_DETAIL_BBK'] = 'BBK';
$MESS['LIB_SEARCH_PAGE_DETAIL_UDC'] = 'Universal Decimal Classification (UDC)';
$MESS['LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT'] = 'Note content';

$MESS['LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE'] = 'Note';

$MESS['LIB_SEARCH_PAGE_CLOSE_WINDOW'] = 'Close window';

$MESS['LIBRARY_LIST_POPUP_ADDRESS'] = 'Address';
$MESS['LIBRARY_LIST_POPUP_WORK'] = 'Working schedule';
$MESS['LIBRARY_LIST_POPUP_USER'] = 'Member';
$MESS['LIBRARY_LIST_POPUP_URL'] = 'Go to the library';

$MESS['LIST_SIMILAR_ITEMS'] = 'Similar publications';
$MESS['LIST_SAME_ITEMS'] = 'Copies';
$MESS['LIST_EMPTY'] = 'Empty';

$MESS['LIB_SEARCH_PAGE_DETAIL_VIEWER_WARNING'] = 'Warning! To read publications, copyright restrictions, you must install <a target="_blank" href="/viewers/">the program on the computer viewing</a>.';

