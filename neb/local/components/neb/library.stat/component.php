<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var array            $arResult
 * @var array            $arParams
 * @var CBitrixComponent $this
 * @var CDatabase        $DB
 */
use \Bitrix\NotaExt\Iblock\Element;

$maxDates = 15;
$arResult = array(
    'library'     => array(),
    'booksStat'   => array(),
    'libraryStat' => array(),
    'daysCount'   => 0,
    'interval'    => 0,
);

CModule::IncludeModule('iblock');

$arUser = CUser::GetByID($USER->GetID())->Fetch();

$library = Element::getList(
    array('IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ID' => $arUser['UF_LIBRARY']),
    1,
    array(
        'ID',
        'PROPERTY_EXALEAD_FILTER',
        'skip_other',
        'SHOW_COUNTER',
        'PROPERTY_LIBRARY_LINK'
    )
);
$arResult['library'] = $library['ITEM'];


// По умолчанию ищем за неделю
$arParams['dateFrom'] = strtotime($arParams['DATE_FROM']);
$arParams['dateTo'] = strtotime($arParams['DATE_TO']);
if ($arParams['dateFrom'] > strtotime('midnight', time())) {
    $arParams['dateFrom'] = false;
}
if ($arParams['dateFrom'] > $arParams['dateTo']) {
    $arParams['dateTo'] = $arParams['dateFrom'] + 86400 * 7;
}
if (false === $arParams['dateFrom']) {
    $arParams['dateFrom'] = time() - 691200;
}
if (false === $arParams['dateTo']) {
    $arParams['dateTo'] = time() - 86400;
}

// Количество дней и диапазон дата(интервал)
$arResult['daysCount'] = ($arParams['dateTo'] - $arParams['dateFrom']) / 86400;
$arResult['daysCount']++;
if (!isset($_REQUEST['export']) && $arResult['daysCount'] > $maxDates) {
    $arResult['interval'] = ceil($arResult['daysCount'] / $maxDates);
}

// Генерирует фильтр по датам
$buildDateFilter = function ($field) use ($arParams, $DB) {
    if (empty($arParams['dateFrom'])) {
        return '';
    }
    $where = array();
    $where[] = $field . ' >= \'' . date('Y-m-d', $arParams['dateFrom']) . '\'';
    $where[] = $field . ' <= \'' . date('Y-m-d', $arParams['dateTo']) . '\'';

    return implode(' AND ', $where);
};


/** Статистика просмотров */
$libraryId = (integer)$arResult['library']['PROPERTY_LIBRARY_LINK_VALUE'];
$statQueryString
    = "
SELECT
  `DT`            `date`,
  count(`CNT_READ`) `reads`,
  count(`CNT_VIEW`) `views`
FROM neb_stat_log
WHERE ID_LIB = $libraryId
  AND {$buildDateFilter('DT')}
GROUP BY `date`;";
$res = $DB->Query($statQueryString, false, $err_mess . __LINE__);
while ($bookStatItem = $res->Fetch()) {
    $arResult['booksStat'][] = $bookStatItem;
}

/** Статистика по изданиям */
$libraryId = (integer)$arUser['UF_LIBRARY'];
$query
    = "
SELECT
  DATE_FORMAT(`nebstat`.`DATE_STAT`, '%Y-%m-%d')  `date`,
  `nebstat`.`COUNT_EDITION`                       `editions`,
  `nebstat`.`COUNT_EDITION_DIG`                   `digitized_editions`
FROM `neb_stat_edition` `nebstat`
WHERE `nebstat`.`ID_BITRIXDB` = {$libraryId}
      AND {$buildDateFilter('DATE_STAT')}
      ";
$res = $DB->Query($query, false, $err_mess . __LINE__);
while ($libraryStatItem = $res->Fetch()) {
    $arResult['libraryStat'][] = $libraryStatItem;
}

$template = '';
if (isset($_REQUEST['export']) && 'Y' === $_REQUEST['export']) {
    $template = 'export';
}

$this->IncludeComponentTemplate($template);