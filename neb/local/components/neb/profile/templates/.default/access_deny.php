<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

require(dirname(__FILE__) . '/includes/header.php');
ShowError('Доступ к разделу запрещен');
require(dirname(__FILE__) . '/includes/footer.php');
