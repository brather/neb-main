<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$sTableID = 'tbl_stat_user';
$lAdmin_tab1 = new CAdminList($sTableID, $oSort_tab1);
$arFilterFields = Array(
		"find_date"
);

if (!$find_date){
	$find_date = date("d.m.Y");
}
$arrRes = array();

// общее количество пользователей 
$filter = Array
(
    "ACTIVE"              => "Y",
	"DATE_REGISTER_2" => $find_date." 23:59:59"
);

$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter);
$user_all = $rsUsers->SelectedRowsCount();
$arrRes[] = array('NAME' => 'Общее количество пользователей(тип регистрации: упрощенная, полная, неизвестная)', 'CNT' => $user_all);

// количество пользователей с упрощенной регистрацией
$filter = Array
(
	"ACTIVE"              => "Y",
	"DATE_REGISTER_2" => $find_date." 23:59:59",
	"UF_REGISTER_TYPE" => [37]
);
$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter,array("SELECT"=>array("UF_*")));
$user_reg = $rsUsers->SelectedRowsCount();
$arrRes[] = array('NAME' => 'Количество пользователей на упрощенной регистрации', 'CNT' => $user_reg);

// Количество пользователей с полной регистрацией
$filter = Array
(
	"ACTIVE"              => "Y",
	"DATE_REGISTER_2" => $find_date." 23:59:59",
	"UF_REGISTER_TYPE" => array(39,38,40)
);
$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter,array("SELECT"=>array("UF_*")));
$user_full_reg = $rsUsers->SelectedRowsCount();
$arrRes[] = array('NAME' => 'Количество пользователей с полной регистрацией', 'CNT' => $user_full_reg);

// Количество пользователей с неизвестной регистрацией
$filter = Array
(
	"ACTIVE"              => "Y",
	"DATE_REGISTER_2" => $find_date." 23:59:59",
	[
		'LOGIC' => 'OR',
		"UF_REGISTER_TYPE" => false,
		"!UF_REGISTER_TYPE" => array(37,38,39,40),
	],

);
$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter,array("SELECT"=>array("UF_*")));
$user_full_reg = $rsUsers->SelectedRowsCount();
$arrRes[] = array('NAME' => 'Количество пользователей с неизвестной регистрацией', 'CNT' => $user_full_reg);

$filter = Array
(
	"ACTIVE"              => "Y",
	"DATE_REGISTER_2" => $find_date." 23:59:59",
	array('LOGIC' => 'AND', "!UF_LIBRARY" => false, "!UF_LIBRARY" => 55)
);
$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter,array("SELECT"=>array("UF_*")));
$user_bibl = $rsUsers->SelectedRowsCount();
$arrRes[] = array('NAME' => 'Количество пользователей, зарегистрированных региональными библиотеками', 'CNT' => $user_bibl);

// количество читателей РГБ
$filter = Array
(
	"ACTIVE"              => "Y",
	"DATE_REGISTER_2" => $find_date." 23:59:59",
	"UF_REGISTER_TYPE" => 39
);
$rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter,array("SELECT"=>array("UF_*")));
$user_rgb = $rsUsers->SelectedRowsCount();
$arrRes[] = array('NAME' => 'Количество читателей РГБ', 'CNT' => $user_rgb);

$arHeader = array(
	array(
		'id'      => 'NAME',
		'content' => '',
		'sort'    => 'NAME',
		'default' => true
	),
	array(
		'id'      => 'CNT',
		'content' => 'Количество',
		'default' => true,
		'sort'    => 'CNT',
	),
);

$lAdmin_tab1->AddHeaders($arHeader);
$arSelect = $lAdmin_tab1->GetVisibleHeaderColumns();

$rsData = new CAdminResult($arrRes, $sTableID);

$rsData->NavStart();
$lAdmin_tab1->NavText($rsData->GetNavPrint(''));


foreach($arrRes as $str)
{
	$row = $lAdmin_tab1->AddRow($str_ID, $str);
}

$lAdmin_tab1->AddAdminContextMenu(array());
$lAdmin_tab1->CheckListMode();
$APPLICATION->SetTitle('Статистика пользователей');
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>
<form method="GET" action="" name="find_form">
	<input type="hidden" name="filter" value="Y">
	<?
	$oFilter = new CAdminFilter(
		$sTableID."_filter",
		array(
			"ID",
		)
	);

	$oFilter->Begin();
	?>
	<tr>
		<td>Дата:</td>
		<td><?=CalendarDate("find_date", $find_date, "find_form", "15", "class=\"my_input\"")?></td>
	</tr>
	<?
	$oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPageParam(), "form"=>"find_form"));
	$oFilter->End();
	?>
</form>
<div class="adm-detail-content-wrap">
	<div class="adm-detail-content">
		<?$lAdmin_tab1->DisplayList();?>
	</div>
	<br>
</div>
<?require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>
