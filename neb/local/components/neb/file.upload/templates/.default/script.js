$(function(){
    //region Определение используемых классов
    var speedToggle = 300,
        speedDelay = 2000,
        cls = {
            form : '.form-file-pdf',
            file : '.file-upload-input',
            files : '.files',
            group : '.form-group',
            fileName : '.fileinput-filename',
            blockFiles : '.upload-files',
            row : '.template-download',
            disabled : 'load-disabled',
            delete : '.btn-delete-file',
            progressBar : '.progress-bar',
            emptyRow : '.empty-files'
        };
    //endregion
    //region Загрузка файлов
    var UserAgentString = navigator.userAgent,
        IE = false;
    if(UserAgentString.indexOf('Trident/7.0') + 1){
        IE = true;
    }


    $(cls.file).on('change', function(){
        var $this = $(this),
            form = $this.closest(cls.form),
            files = this.files,
            dataFile = new FormData(),
            ajaxUrl = $this.data('url'),
            inputDataSend = {
                folder : $this.data('folder'),
                subFolder : $this.data('sub-folder'),
                fileName : $this.data('file-name'),
                urlDownload : $this.data('url-download'),
                aliasName : $this.data('alias'),
                fileMaxSize : $this.data('file-maxsize'),
                fileType : $this.data('file-type'),
                ajaxDeleteFile : $this.data('ajax-delete'),
                countFiles : $this.data('max-files')
            },
            dataLangIe = {
                title: $this.data('ie-confirm-title'),
                text: $this.data('ie-confirm-text'),
                confirmTitle: $this.data('ie-confirm-yes')
            },
            idSec = '-' + inputDataSend.subFolder,
            idSubSec = '-' + inputDataSend.fileName,
            postfix = idSec + idSubSec,
            $blkForm = $('#pdf-upload' + postfix),
            $blkContainFiles = $('#upload-files' + postfix),
            $progressBar = $('#progressbar' + postfix).find(cls.progressBar),
            $blkToSave = $blkContainFiles.find(cls.files),
            countFiles = inputDataSend.countFiles,
            getOptions = '',
            $indexData = 0;
        $.each($this.data(), function(i,y){
            if(i.search('addParam') !== -1) {
                inputDataSend[i] = y;
            }
        });
        //region Скрытие блока
        function hideBlock() {
            $progressBar.parent().hide(speedToggle);
        }
        //endregion
        function setSuccess(data) {
            setTimeout(hideBlock, speedDelay);
            if(!data.ERROR) {
                if($blkToSave.find(cls.emptyRow).is(':visible')) {
                    $blkToSave.find(cls.emptyRow).hide(speedToggle);
                }
                $blkToSave.append(data.html);
                if(countFiles) {
                    if($blkToSave.find(cls.row).not(cls.emptyRow).length >= countFiles) {
                        $blkForm.find('.row').addClass(cls.disabled);
                    }
                }
            } else {
                alert(data.ERROR);
            }
        }
        if($progressBar.is(':hidden')) {
            $progressBar.parent().show(speedToggle);
        }
        $progressBar.attr('aria-valuenow', 0).css({ 'width' : '0%' }).text('');
        $.each(inputDataSend, function(i,y){
            if($indexData == 0) {
                getOptions = '?';
            } else {
                getOptions += '&';
            }
            getOptions += i + '=' + y;
            $indexData = $indexData + 1;
        });

        dataFile.append(0, files[0]);
        $.ajax({
            url : ajaxUrl + getOptions,
            type : 'POST',
            data : dataFile,
            cache : false,
            dataType : 'json',
            processData : false,
            contentType : false,
            xhr: function(){
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.addEventListener('progress', function(e){
                    if(e.lengthComputable) {
                        var percentComplete = Math.ceil(e.loaded / e.total * 100);
                        $progressBar.attr('aria-valuenow', percentComplete).css({ 'width' : percentComplete + '%' }).text(percentComplete + '%');
                    }
                    if(IE) {
                        startLoader('body');
                        if(e.loaded == e.total) {
                            var hrefOnDel = '<a data-form="#pdf-upload-' + inputDataSend.fileName + '"' +
                                ' data-container="#upload-files-' + inputDataSend.fileName + '"' +
                                ' data-lang-confirm-title="' + dataLangIe.title + '"' +
                                ' data-lang-confirm-text="' + dataLangIe.text + '"' +
                                ' data-lang-confirm-yes="' + dataLangIe.confirmTitle + '"' +
                                ' data-ajax-delete="' + inputDataSend.ajaxDeleteFile + '"' +
                                ' data-delete="' + inputDataSend.folder + inputDataSend.subFolder +
                                '/' + inputDataSend.fileName + '.' + inputDataSend.fileType + '"' +
                                ' href="#" class="btn btn-danger btn-delete-file">' +
                                '<i class="glyphicon glyphicon-trash"></i><span> ' + dataLangIe.confirmTitle + '</span></a>',
                                data = {
                                html : '<tr class="template-download fade in">' +
                                '<td><p class="name"><span class="file-fa">' + inputDataSend.aliasName + '</span></p></td>' +
                                '<td><span class="size">' + getSizeFile(e.total) + '</span></td>' +
                                '<td>' + hrefOnDel + '</td>' +
                                '</tr>'
                            };
                            setSuccess(data);
                            function reloadPage() {
                                window.location.reload();
                                stopLoader();
                            }
                            setTimeout(reloadPage, 2 * speedDelay);
                        }
                    }
                }, false);

                return xhr;
            },
            success : function(data){
                setSuccess(data);
            }
        });
    });
    //endregion

    //region Удаление файлов
    $(document).on('click', cls.delete, function(e){
        e.preventDefault();
        var $this = $(this),
            rowOnDel = $this.closest(cls.row),
            toggler = $(e.target),
            $blkForm = $($this.data('form')),
            $blkContain = $($this.data('container')),
            countFiles = $this.closest(cls.blockFiles).data('max-files'),
            dataDelete = {
                file : $this.data('delete')
            },
            message = {
                title: $this.data('lang-confirm-title'),
                text: $this.data('lang-confirm-text'),
                confirmTitle: $this.data('lang-confirm-yes')
            },
            urlAjax = $this.data('ajax-delete');
        $.each($this.data(), function(i,y){
            if(i.search('addParam') !== -1) {
                dataDelete[i] = y;
            }
        });
        $.when( FRONT.confirm.handle(message,toggler) ).then(function(confirmed){
            if(confirmed) {
                $.post(urlAjax, dataDelete).done(function(data){
                    if(data.MESS == 'success') {
                        rowOnDel.hide(speedToggle, function(){
                            rowOnDel.remove();
                            if(countFiles) {
                                if($blkContain.find(cls.row).not(cls.emptyRow).length < countFiles) {
                                    $blkForm.find('.row').removeClass(cls.disabled);
                                }
                            }
                            if($blkContain.find(cls.row).not(cls.emptyRow).length == 0) {
                                $blkContain.find(cls.emptyRow).show(speedToggle);
                            }
                        });
                    } else {
                        alert(data.MESS);
                    }
                });
            }
        });
    });
    //endregion
});
function getSizeFile(size) {
    // Размер больше 1 Кб
    if(size > 1000) {
        size = (size/1000);
        // Размер больше 1 Мегабайта
        if(size > 1000) {
            size = (size/1000);
            // Размер больше 1 Гигабайта
            if(size > 1000) {
                size = (size/1000);
                size = Math.round(size, 1) + 'Гб';
            } else {
                size = Math.round(size, 1) + 'Мб';
            }
        } else {
            size = Math.round(size, 1) + 'Кб';
        }
    } else {
        size = Math.round(size, 1) + 'байт';
    }
    return size;
}
//region Функция запуска лоадера
function startLoader(block) {
    stopLoader();
    var blkLoader = '<div id="blk-loader" class="blk-milk-shadow"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
    $(block).append(blkLoader);
}
//endregion
//region Функция остановки лоадера
function stopLoader() {
    $('#blk-loader').animate({ 'opacity' : 0 }, 300, function(){
        $(this).remove();
    });
}
//endregion