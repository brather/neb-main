<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="row">
    <div class="col-md-3 col-sm-3 lk-sidebar">
	<?$APPLICATION->ShowViewContent('lib_menu')?>
    <?
    $APPLICATION->IncludeComponent(
        "neb:library.right_counter",
        "",
        Array(
            "IBLOCK_ID" => $arParams['IBLOCK_ID'],
            "LIBRARY_ID" => $arResult['ITEM']["ID"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "COLLECTION_URL" => $arParams['COLLECTION_URL'],
        ),
        $component
    );
    ?>
    </div>
    <div class="col-md-9 col-sm-9 lk-content">
        <h2>Фонды</h2>

        <form name="show_all_checkbox" class="hidden">
            <label for="show_all_checkbox">
                <input type="checkbox" id="show_all_checkbox" name="all_funds"
                       value="on" <?php if (!empty($_REQUEST['all_funds'])): ?> checked="checked" <?php endif; ?>>
                <span class="lbl"><?= Loc::getMessage(
                        'LIBRARY_FUNDS_SHOW_ALL_FUNDS'
                    ) ?></span>
            </label>
        </form>
        <script type="application/javascript">
            $(function () {
                $('form[name="show_all_checkbox"]').removeClass('hidden');
                $('#show_all_checkbox').change(function () {
                    $(this).parents('form[name="show_all_checkbox"]').submit();
                });
            });
        </script>
        <br>
	<?
		$APPLICATION->IncludeComponent(
			"exalead:search.page.viewer.left",
			"",
			Array(
				"PARAMS" => array_merge($arParams, array('TITLE_MODE_BLOCK' => 'Отобразить книги плиткой', 'TITLE_MODE_LIST' => 'Отобразить книги списком',
                                                         "HIDE_LIST_TEXT" => 'Y')),
				"RESULT" => $arResult,
			),
			$component
		);
	?>
    </div>
</div>
