<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<?if ($APPLICATION->GetCurPage(false) != SITE_DIR):?>
    </div>
<?endif;?>

<?php if ('/auth/' !== substr($APPLICATION->GetCurPage(), 0, 6)):?>
<!-- Футер НАЧАЛО -->
<footer class="container main-footer">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-6">
            <a href="#" title="НЭБ" alt="НЭБ">
                <img src="/local/templates/adaptive/img/logo-footer.png" alt="НЭБ" class="main-footer__logo">
            </a>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "bottom",
                array(
                    "ROOT_MENU_TYPE" => "bottom1_".LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left_".LANGUAGE_ID,
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            );?>

        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "bottom",
                array(
                    "ROOT_MENU_TYPE" => "bottom2_".LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left_".LANGUAGE_ID,
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            );?>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "bottom",
                array(
                    "ROOT_MENU_TYPE" => "bottom3_".LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left_".LANGUAGE_ID,
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            );?>
        </div>
    </div>
    <div class="row main-footer__copyright">
        <div class="col-md-3 col-sm-3 col-xs-12 <?php if('en' == LANGUAGE_ID):?>enminlogo<?php else:?>ruminlogo<?php endif;?>">
            <?php if('en' == LANGUAGE_ID):?>
                <img src="/local/templates/adaptive/img/ministr-hires-en.png" alt="" class="main-footer__copyright-ministr-logo">
            <?php else:?>
                <img src="/local/templates/adaptive/img/ministr-hires-ru.png" alt="" class="main-footer__copyright-ministr-logo">
            <?php endif;?>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <p class="main-footer__copyright-autors">
                <?=getMessage('COPYRIGHT_AUTORS')?>
            </p>
        </div>
    </div>
    <div class="row">
        <p class="main-footer__copyright-text col-md-12">
            <?=getMessage('COPYRIGHT_TEXT')?>
        </p>
    </div>
</footer>
<div id="milk-shadow"></div>
<?php endif; ?>
<!-- /Футер КОНЕЦ -->
<!-- СКРИПТЫ НАЧАЛО-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?if (!MINIFY_JS_CSS_MODE || MARKUP_DEBUG_MODE) { ?>
    <!-- СКРИПТЫ НАЧАЛО-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="/local/templates/adaptive/js/jquery-1.11.3.min.js"></script>

    <script type="text/javascript" src="/local/templates/adaptive/vendor/select2-4.0.3/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="/local/templates/adaptive/vendor/custom-select/bootstrap-select.custom.min.js"></script>

    <?php echo \Neb\Main\Helper\TemplateHelper::getInstance()->buildJs(['footer', 'beforeDefer']);?>
    <script type="text/javascript" src="/local/templates/adaptive/js/jquery/postjquery.js"></script>
    <!-- script>defer$()</script -->

    <!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
    <!-- <script src="/local/templates/adaptive/js/jquery/jquery-ui-1.10.4.custom.min.js"></script> -->
    <script src="/local/templates/adaptive/js/jquery/jquery-ui-1.12.1.custom.min.js"></script>
    <?php echo \Neb\Main\Helper\TemplateHelper::getInstance()->buildJs(['footer', 'afterDefer']);?>
    <script type="text/javascript" src="/local/templates/adaptive/vendor/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="/local/templates/adaptive/vendor/dc-accordeon/jquery.cookie.js"></script>
    <script type="text/javascript" src="/local/templates/adaptive/vendor/dc-accordeon/jquery.dcjqaccordion.2.7.min.js"></script>
    <!--script type="text/javascript" src="/local/templates/adaptive/vendor/jquery-ui/jquery.ui.datepicker-ru.js"></script-->
    <script type="text/javascript" src="/local/templates/adaptive/js/jquery/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="/local/templates/adaptive/js/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/local/templates/adaptive/js/jquery/jquery.tmpl.js"></script>
    <script type="text/javascript" src="/local/templates/adaptive/js/jquery/additional-methods.validate.min.js"></script>
    <script type="text/javascript" src="/local/templates/adaptive/vendor/charts.js/Chart.min.js"></script>
    <script type="text/javascript" src="/local/templates/adaptive/js/neb/charts.js"></script>
    <script type="text/javascript" src="/local/templates/adaptive/vendor/bootstrap-contextmenu/jquery.contextMenu.min.js"></script>
    <!--script type="text/javascript" src="/local/templates/adaptive/vendor/bootstrap-contextmenu.js"></script-->
    <?
    $arUsrGr = array_intersect(['admin', 'operator', 'operator_access2publications'], nebUser::getCurrent()->getUserGroups()); // PHP 5.4 fuck...
    if (!empty($arUsrGr)): // PHP 5.4 fuck ... ?>
        <script type="text/javascript" src="/local/templates/adaptive/js/neb/operator-neb.js?12"></script>
    <? endif; ?>


    <!-- script src="vendor/owl-carousel/owl.carousel.min.js"></script -->
    <script type="text/javascript" src="/local/templates/adaptive/vendor/slick/slick.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/local/templates/adaptive/js/bootstrap.min.js"></script>
    <script src="/local/templates/adaptive/js/protocolcheck.js"></script>
    <script src="/local/templates/adaptive/js/neb/jq-select-replace.js"></script>
    <script src="/local/templates/adaptive/js/neb/library-map.js"></script>
    <script src="/local/templates/adaptive/js/neb/book-read-check.js"></script>
    <!-- аплоадилка для формы регистрации -->
    <script src="/local/templates/adaptive/js/jquery/jquery.fileupload.js"></script>
    <!-- Версия для слабовидящих -->
    <script src="/local/templates/adaptive/js/blind.js"></script>
    <!-- часики-вертушка ожидания -->
    <script type="text/javascript" src="/local/templates/adaptive/js/spin.js"></script>
    <script type="text/javascript" src="/local/templates/.default/markup/js/libs/jquery.maskedinput.min.js"></script>

    <script type="text/javascript" src="/local/templates/adaptive/vendor/keyboard/keyboard.js"></script>


    <!-- Основные скрипты -->
    <script src="/local/templates/adaptive/js/script.js?bust=1"></script>
    <script src="/local/templates/adaptive/js/neb/auth.js"></script>

    <script src="/bitrix/js/phpsolutions.backtotop/backtotop.js"></script>
<? } else { ?>
    <script type="text/javascript" src="/local/templates/adaptive/js/site.min.js"></script>
    <?php echo \Neb\Main\Helper\TemplateHelper::getInstance()->buildJs(['footer', 'beforeDefer']);//иначе сыпятся ошибки /profile/feedback/  ?>
    <?php echo \Neb\Main\Helper\TemplateHelper::getInstance()->buildJs(['footer', 'afterDefer']);?>
<? };?>
<!-- /СКРИПТЫ КОНЕЦ -->
<div class="modal fade" tabindex="-1" role="dialog" id="universal-modal">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?=getMessage('CLOSE')?></button>
          </div>
        </div>
    </div>
</div>
<?/*!--link rel="stylesheet" href="/local/templates/adaptive/vendor/bootstrap-contextmenu/jquery.contextMenu.min.css" /--*/?>

<?$APPLICATION->IncludeFile("/local/include_areas/counters.php", Array(), Array("MODE" => "php", "NAME" => "Счетчики"));?>

</body>
</html>