<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<div class="share">
    <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
    <div class="ya-share2" id="ya-share2" data-services="<?=$arParams['THEME_SERVICES']?>"></div>

    <script type="text/javascript">
        var share = Ya.share2('ya-share2', {
            content: {
                url: '<?=$arParams['DATA_URL']?>',
                <? if (!empty($arParams['DATA_TITLE'])): ?>
                    title: '<?=trim($arParams['DATA_TITLE'])?>',
                <? endif; ?>
                <? if (!empty($arParams['DATA_DESCRIPTION'])): ?>
                    description: '<?=trim($arParams['DATA_DESCRIPTION'])?>',
                <? endif; ?>
                <? if (!empty($arParams['DATA_IMAGE'])): ?>
                    image: '<?=$arParams['DATA_IMAGE']?>',
                <? endif; ?>
            },
            theme: {
                /*services: '<?=$arParams['THEME_SERVICES']?>',*/
                bare: <?=$arParams['THEME_BARE']?'true':'false'?>,
                counter: <?=$arParams['THEME_COUNTER']?'true':'false'?>,
                <? if (!empty($arParams['THEME_COPY'])): ?>
                    copy: '<?=$arParams['THEME_COPY']?>',
                <? endif; ?>
                <? if (!empty($arParams['THEME_DIRECTION'])): ?>
                    direction: '<?=$arParams['THEME_DIRECTION']?>',
                <? endif; ?>
                <? if (!empty($arParams['THEME_LANG'])): ?>
                    lang: '<?=$arParams['THEME_LANG']?>',
                <? endif; ?>
                <? if (!empty($arParams['THEME_LIMIT'])): ?>
                    limit: <?=$arParams['THEME_LIMIT']?>,
                <? endif; ?>
                <? if (!empty($arParams['THEME_POPUP_DIRECTION'])): ?>
                    popupDirection: '<?=$arParams['THEME_POPUP_DIRECTION']?>',
                <? endif; ?>
                <? if (!empty($arParams['THEME_POPUP_POSITION'])): ?>
                    popupPosition: '<?=$arParams['THEME_POPUP_POSITION']?>',
                <? endif; ?>
                <? if (!empty($arParams['THEME_SIZE'])): ?>
                    size: '<?=$arParams['THEME_SIZE']?>',
                <? endif; ?>
            }
        });
    </script>
</div>