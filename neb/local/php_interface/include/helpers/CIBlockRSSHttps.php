<?

use \Bitrix\Main\Loader;
use \Bitrix\Main\Text\Encoding;

Loader::includeModule('iblock');

class CIBlockRSSHttps extends CIBlockRSS{

    function GetNewsEx($SITE, $PORT, $PATH, $QUERY_STR, $bOutChannel = False)
    {
        $cacheKey = md5($SITE.$PORT.$PATH.$QUERY_STR);

        $bValid = False;
        $bUpdate = False;
        if ($db_res_arr = CIBlockRSS::GetCache($cacheKey))
        {
            $bUpdate = True;
            if (strlen($db_res_arr["CACHE"])>0)
            {
                if ($db_res_arr["VALID"]=="Y")
                {
                    $bValid = True;
                    $text = $db_res_arr["CACHE"];
                }
            }
        }

        if (!$bValid)
        {
            $FP = fsockopen("ssl://".$SITE, 443, $errno, $errstr, 120);

            if ($FP)
            {
                $strVars = $QUERY_STR;
                $strRequest = "GET ".$PATH.(strlen($strVars) > 0? "?".$strVars: "")." HTTP/1.0\r\n";
                $strRequest.= "User-Agent: BitrixSMRSS\r\n";
                $strRequest.= "Accept: */*\r\n";
                $strRequest.= "Host: $SITE\r\n";
                $strRequest.= "Accept-Language: en\r\n";
                $strRequest.= "\r\n";
                fputs($FP, $strRequest);

                $headers = "";
                while(!feof($FP))
                {
                    $line = fgets($FP, 4096);
                    if($line == "\r\n")
                        break;
                    $headers .= $line;
                }

                $text = "";
                while(!feof($FP))
                    $text .= fread($FP, 4096);

                $rss_charset = "windows-1251";
                if (preg_match("/<"."\?XML[^>]{1,}encoding=[\"']([^>\"']{1,})[\"'][^>]{0,}\?".">/i", $text, $matches))
                {
                    $rss_charset = Trim($matches[1]);
                }
                elseif($headers)
                {
                    if(preg_match("#^Content-Type:.*?charset=([a-zA-Z0-9-]+)#m", $headers, $match))
                        $rss_charset = $match[1];
                }

                $text = preg_replace("/<!DOCTYPE.*?>/i", "", $text);
                $text = preg_replace("/<"."\\?XML.*?\\?".">/i", "", $text);
                $text = Encoding::convertEncoding($text, $rss_charset, SITE_CHARSET);

                fclose($FP);
            }
            else
            {
                $text = "";
            }
        }

        if (strlen($text) > 0)
        {
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");
            $objXML = new CDataXML();
            $res = $objXML->LoadString($text);
            if($res !== false)
            {
                $ar = $objXML->GetArray();
                if (!$bOutChannel)
                {
                    if (
                        is_array($ar) && isset($ar["rss"])
                        && is_array($ar["rss"]) && isset($ar["rss"]["#"])
                        && is_array($ar["rss"]["#"]) && isset($ar["rss"]["#"]["channel"])
                        && is_array($ar["rss"]["#"]["channel"]) && isset($ar["rss"]["#"]["channel"][0])
                        && is_array($ar["rss"]["#"]["channel"][0]) && isset($ar["rss"]["#"]["channel"][0]["#"])
                    )
                        $arRes = $ar["rss"]["#"]["channel"][0]["#"];
                    else
                        $arRes = array();
                }
                else
                {
                    if (
                        is_array($ar) && isset($ar["rss"])
                        && is_array($ar["rss"]) && isset($ar["rss"]["#"])
                    )
                        $arRes = $ar["rss"]["#"];
                    else
                        $arRes = array();
                }

                $arRes["rss_charset"] = strtolower(SITE_CHARSET);

                if (!$bValid)
                {
                    $ttl = (strlen($arRes["ttl"][0]["#"]) > 0)? IntVal($arRes["ttl"][0]["#"]): 60;
                    CIBlockRSS::UpdateCache($cacheKey, $text, array("minutes" => $ttl), $bUpdate);
                }
            }
            return $arRes;
        }
        else
        {
            return array();
        }
    }
}
