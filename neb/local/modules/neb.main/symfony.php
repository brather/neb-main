<?php

if (false !== stripos($_SERVER['REQUEST_URI'], '/api/')) {
    define('STOP_STATISTICS', true);
}

# удаление порта из переменной HTTP_HOST вследствие некорректных настроек сервера
$iPort = mb_strpos($_SERVER['HTTP_HOST'], ':');
if (false !== $iPort) {
    $_SERVER['HTTP_HOST'] = mb_substr($_SERVER['HTTP_HOST'], 0, $iPort);
}

$settings = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/.settings_extra.php';
if (file_exists($settings)) {
    $config = require($settings);
} else {
    throw new Exception('File /bitrix/.settings_extra.php not found');
}

$debug = isset($config['exception_handling']['value']['debug'])
    ? $config['exception_handling']['value']['debug']
    : false;

require_once $_SERVER['DOCUMENT_ROOT'] . '/../app/bootstrap.php.cache';
if($debug) {
    \Symfony\Component\Debug\Debug::enable();
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/../app/AppKernel.php';
$kernel = new AppKernel(true === $debug?'dev':'prod', $debug);
$kernel->loadClassCache();
$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);