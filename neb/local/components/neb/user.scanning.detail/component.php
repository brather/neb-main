<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$ID = (int)$_REQUEST['id'];

$arFilter = array(
    'IBLOCK_ID' => IBLOCK_PCC_ORDER,
    'ID' => $ID,
);


$arScan = Bitrix\NotaExt\Iblock\Element::getList(
    $arFilter,
    false,
    array(
        'PROPERTY_PRICE',
        'PROPERTY_PAID',
    ),
    $arOrder
);


$file = explode(",",$_REQUEST['file']);

$db_props = CIBlockElement::GetProperty(IBLOCK_PCC_ORDER, $ID, "sort", "asc", Array("CODE"=>"FILES_"));
while($ar_props = $db_props->Fetch())
{
    preg_match('/\/([0-9]*).jpeg/',$ar_props["VALUE"],$tmp);

    if((!in_array($tmp[1], $file) || !$_REQUEST['listType']) &&  $ar_props["VALUE"] != NULL ) {

        $srFiles[] = array(
            'id' => $tmp[1],
            'src' => $ar_props["VALUE"],
            'link' => $ar_props["VALUE"],
            'size' => filesize($ar_props["VALUE"])
        );
        $PROP['FILES'][] = array('VALUE' => $ar_props["VALUE"]) ;
        $all_size += filesize($ar_props["VALUE"]);
    }	
}

if (isset($_REQUEST['file']) && $_REQUEST['listType'] == 'delete'){
    CModule::IncludeModule('catalog');
    CIBlockElement::SetPropertyValues($ID, IBLOCK_PCC_ORDER, $PROP['FILES'], 'FILES_');

    /*
	Изменяем цену в торговом каталоге
    */
    $PRICE = count($srFiles) * $arScan['ITEMS'][0]['PROPERTY_PRICE_VALUE'];

    $arFields = array('PURCHASING_PRICE' => $PRICE);// зарезервированное количество
    CCatalogProduct::Update($ID, $arFields);


    $db_res = CPrice::GetList(array(),array( "PRODUCT_ID" => $ID));
    if ($ar_res = $db_res->Fetch()){
        $arFields = array('PRICE' => $PRICE);
        CPrice::Update($ar_res['ID'], $arFields);
    }
}

// Постраничная навигация для вывода списка сканов
// Задаем количество элементов на странице
$countOnPage = 4;

// Подготовка параметров для пагинатора
$navResult = new CDBResult();
$navResult->NavPageCount = ceil(count($srFiles) / $countOnPage);
// Получаем номер текущей страницы из реквеста
$page = ($_GET['PAGEN_1'] ? (intval($_GET['PAGEN_1']) <= $navResult->NavPageCount ? intval($_GET['PAGEN_1']) : $navResult->NavPageCount): 1);
// Отбираем элементы текущей страницы
$elementsPage = array_slice($srFiles, ($page-1) * $countOnPage, $countOnPage);
// Вывод страницы
$navResult->NavPageNomer = $page;
$navResult->NavNum = 1;
$navResult->NavPageSize = $countOnPage;
$navResult->NavRecordCount = count($srFiles);

$arResult['ID'] = $ID;
$arResult['PAID'] = $arScan['ITEMS'][0]['PROPERTY_PAID_VALUE'];
$arResult['PRICE'] = $arScan['ITEMS'][0]['PROPERTY_PRICE_VALUE'];
$arResult['IMAGECOUNT'] = count($srFiles);
$arResult['FILES'] = $elementsPage;
$arResult['NAV_RESULT'] = $navResult;
$arResult['FILES_SIZE'] = $all_size;


$this->IncludeComponentTemplate($arParams['TEMPLATE']);
?>