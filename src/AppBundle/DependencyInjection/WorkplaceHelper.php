<?php
/**
 * User: agolodkov
 * Date: 20.10.2016
 * Time: 16:05
 */

namespace AppBundle\DependencyInjection;


use AppBundle\Entity\Workplace;
use BitrixBundle\DependencyInjection\BitrixHelper;
use \Neb\Main\Helper\IpHelper;

class WorkplaceHelper
{
    /**
     * @var BitrixHelper
     */
    public $bitrix;

    /**
     * @param BitrixHelper $bitrixHelper
     */
    public function __construct(BitrixHelper $bitrixHelper)
    {
        $this->bitrix = $bitrixHelper;
    }

    /**
     * @param array $params
     *
     * @return array
     * @throws \Exception
     */
    public function findWorkplaces($params)
    {
        $this->bitrix->checkIncludeThrowException();
        $this->bitrix->includeModule('iblock');

        $element = new \CIBlockElement();
        $entities = [];
        $rsElements = $element->GetList(
            [],
            ['IBLOCK_ID' => IBLOCK_ID_WORKPLACES],
            null,
            null,
            [
                'ID',
                'NAME',
                'ACTIVE',
                'PROPERTY_IP',
                'PROPERTY_LIBRARY',
                'PROPERTY_IS_CHECK_IP',
                'PROPERTY_IS_CHECK_FINGERPRINT'
            ]
        );
        while ($item = $rsElements->Fetch()) {
            $workplace = new Workplace();
            $workplace->setId($item['ID']);
            $workplace->setName($item['NAME']);
            $workplace->setActive(($item['ACTIVE'] === "Y")? 1 : 0);
            $workplace->setIsCheckFingerprint(($item['PROPERTY_IS_CHECK_FINGERPRINT_VALUE'] === "Y")? 1 : 0);
            $workplace->setIsCheckIp(($item['PROPERTY_IS_CHECK_IP_VALUE'] === "Y")? 1 : 0);
            $workplace->setIps(array_filter($item['PROPERTY_IP_VALUE']));
            $workplace->setLibraryId($item['PROPERTY_LIBRARY_VALUE']);
            $entities[] = $workplace;
        }
        if (isset($params['ip']))
            $this->_filterByIp($params['ip'], $entities);

        if (isset($params['idlibrary']) && !empty($params['idlibrary']))
            $this->_filterByLibraryID((int)$params['idlibrary'], $entities);

        return array_values($entities);
    }

    /**
     * @param string      $ip
     * @param Workplace[] &$array
     */
    protected function _filterByIp($ip, &$array)
    {
        $this->bitrix->includeModule('neb.main');

        foreach ($array as $key => $item) {
            $ips = $item->getIps();
            $allow = false;
            foreach ($ips as $workplaceIp) {
                if (IpHelper::validateIpInRange($ip, $workplaceIp)) {
                    $allow = true;
                }
            }
            if (false === $allow) {
                unset($array[$key]);
            }
        }
    }

    /**
     * @param int           $libId
     * @param Workplace[]   &$array
     */
    protected function _filterByLibraryID($libId, &$array)
    {
        foreach ($array as $key => $workplace)
        {
            if ($workplace instanceof Workplace)
            {
                if ((int)$libId !== $workplace->getLibraryId())
                {
                    unset($array[$key]);
                }
            }
        }
    }
}