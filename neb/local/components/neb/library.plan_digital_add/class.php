<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Entity;
use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock\HighloadBlockTable;

class PlanDigitalAddComponent extends CBitrixComponent {

	/**
	 * Подготовка входных параметров компонента
	 * @param $arParams
	 * @return array
	 */
	public function onPrepareComponentParams($arParams) {
		return parent::onPrepareComponentParams($arParams);
	}

	/**
	 * Выполнение компонента
	 */
	public function executeComponent() {

		global $APPLICATION;
		$APPLICATION->RestartBuffer();

		$nebUser = new nebUser();
		$arLibrary = $nebUser->getLibrary();

		Loader::includeModule('highloadblock');

		if (!empty($arLibrary['PROPERTY_LIBRARY_LINK_VALUE'])) {

			$arHlBlock         = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
			$entity_data_class = HighloadBlockTable::compileEntity($arHlBlock)->getDataClass();
			$arData            = $entity_data_class::getById($arLibrary['PROPERTY_LIBRARY_LINK_VALUE'])->fetch();

			$this->arParams['UF_LIBRARY'] = $arLibrary;
			$this->arParams['LIBRARY_ID'] = $arData['UF_ID'];
		}

		$this->includeComponentTemplate();

		exit();
	}

}