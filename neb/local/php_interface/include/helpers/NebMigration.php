<?

/**
 * Класс миграции структур данных (в будущем будет дополнено):
 *  - Инфоблоки;
 *  - Highload-инфоблоки;
 *  - Пользовательские свойства;
 *  - Значения пользовательских свойств типа "список";
 *  - Типы почтовых событий;
 *  - Почтовые шаблоны;
 *  - Агенты.
 *
 * В целях оптимизации совместил операции добавления, обновления и редактирования сущностей в одном методе.
 *
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 06.06.2016
 * Time: 10:30
 */
class NebMigration
{
    private $iVersion = 1;  // версия скрипта миграции // TODO: прикрутить проверку
    private $arReport = [];

    public function __construct() {
        $this->setReportTime();
        CModule::IncludeModule('iblock');
        CModule::IncludeModule('highloadblock');
    }

    public function __destruct() {
        $this->setReportTime(true);
    }

    public function setData($arData) {

        $this->setIBlock($arData['IBLOCK']);
        $this->setHBlock($arData['HBLOCK']);
        $this->setUserFields($arData['USER_FIELDS']);
        $this->setUserFieldEnum($arData['USER_FIELD_ENUM']);
        $this->setEventType($arData['EVENT_TYPE']);
        $this->setEventMessage($arData['EVENT_MESSAGE']);
        $this->setAgent($arData['AGENT']);
    }

    public function deleteData($arData) {

        // добавить удаление инфоблоков - аккуратно  $this->setIBlock($arData['IBLOCK'], true);
        
        $this->setUserFieldEnum($arData['USER_FIELD_ENUM'], true);
        $this->setUserFields($arData['USER_FIELDS'], true);
        $this->setHBlock($arData['HBLOCK'], true);
        $this->setEventMessage($arData['EVENT_MESSAGE'], true);
        $this->setEventType($arData['EVENT_TYPE'], true);
        $this->setAgent($arData['AGENT'], true);
    }

    /**
     * Получаение массива отчетов о работе
     * @return array
     */
    public function getReport() {
        $this->setReportTime(true);
        return $this->arReport;
    }

    /*
    private static function addEventLog($arFields) {
        CEventLog::Add($arFields);
    }
    */

    /**
     * Инфоблоки
     *
     * @param $arEntity
     * @param bool $bDelete
     * @return array
     */
    private function setIBlock ($arEntity, $bDelete = false) {

        $arReport = $arHBlockName = [];

        if (empty($arEntity))
            return $arReport;

        $obIBlock = new CIBlock;

        // выбор данных инфоблоков из БД
        $arDBIBlocks = $this->getIBlockList($arHBlockName);

        foreach ($arEntity as $arItem) {

            $arDBIBlock = $arDBIBlocks[$arItem['IBLOCK_TYPE_ID']][$arItem['CODE']];

            // удаление
            if ($bDelete) {
                if (empty($arDBIBlock)) {
                    $arReport[] = 'Ошибка удаления: инфоблок не найден в БД - '
                        . $arItem['IBLOCK_TYPE_ID'] . ' - ' . $arItem['CODE'];
                }
                else {
                    $res = $obIBlock->Delete($arDBIBlock['ID']);
                    if (!$res)
                        $arReport[] = 'Ошибка: инфоблок не удален - ' . $arItem['IBLOCK_TYPE_ID']
                            . ' - ' . $arItem['CODE'] . ' - ' . $obIBlock->LAST_ERROR;
                    else
                        $arReport[] = 'Успех: инфоблок удален - '
                            . $arItem['IBLOCK_TYPE_ID'] . ' - ' . $arItem['CODE'];
                }
            }
            // обновление
            elseif ($arDBIBlock['ID'] > 0) {

                $arUpdate  = [];
                foreach ($arItem as $k => $v)
                    if ($arDBIBlock[$k] != $v && !in_array($k, ['ID']))
                        $arUpdate[$k] = $v;

                if (empty($arUpdate))
                    continue;

                $res = $obIBlock->Update($arDBIBlock['ID'], $arItem);
                if (!$res)
                    $arReport[] = 'Ошибка: инфоблок не обновлен - ' . $arItem['IBLOCK_TYPE_ID']
                        . ' - ' . $arItem['CODE'] . ' - ' . $obIBlock->LAST_ERROR;
                else
                    $arReport[] = 'Успех: инфоблок обновлен - ' . $arItem['IBLOCK_TYPE_ID']
                        . ' - ' . $arItem['CODE'] . ' - ' . $arDBIBlock['ID'];
            }
            // добавление
            else {
                $res = $obIBlock->Add($arItem);
                if ($res < 1)
                    $arReport[] = 'Ошибка: инфоблок не добавлен - ' . $arItem['IBLOCK_TYPE_ID'] . ' - '
                        . $arItem['CODE'] . ' - ' . $obIBlock->LAST_ERROR;
                else
                    $arReport[] = 'Успех: инфоблок добавлен - ' . $arItem['IBLOCK_TYPE_ID']
                        . ' - ' . $arItem['CODE'] . ' - ' . $res;
            }
        }

        if (!empty($arReport))
            $this->arReport = array_merge($this->arReport, $arReport);

        return $arReport;
    }

    /**
     * Highload-инфоблоки
     *
     * @param $arEntity
     * @param bool $bDelete
     * @return array
     */
    private function setHBlock ($arEntity, $bDelete = false) {

        $arReport = $arHBlockName = [];

        if (empty($arEntity))
            return $arReport;

        foreach ($arEntity as $arItem)
            $arHBlockName[] = $arItem['NAME'];

        // выбор данных инфоблоков из БД
        $arDBHBlocks = $this->getHBlockList($arHBlockName);

        foreach ($arEntity as $arItem) {

            $arDBHBlock = $arDBHBlocks[$arItem['NAME']];

            // удаление
            if ($bDelete) {
                if (empty($arDBHBlock)) {
                    $arReport[] = 'Ошибка удаления: hightload-инфоблок не найден в БД - ' . $arItem['NAME'];
                }
                else {
                    $res = \Bitrix\Highloadblock\HighloadBlockTable::delete($arDBHBlock['ID']);
                    if (!$res->isSuccess())
                        $arReport[] = 'Ошибка: hightload-инфоблок не удален - '
                            . $arDBHBlock['NAME'] . ' - ' . implode(', ', $res->getErrorMessages());
                    else
                        $arReport[] = 'Успех: hightload-инфоблок удален - '
                            . $arDBHBlock['NAME'] . ' - ' . $arDBHBlock['ID'];
                }
            }
            // обновление
            elseif ($arDBHBlock['ID'] > 0) {

                $arUpdate  = [];
                foreach ($arItem as $k => $v)
                    if ($arDBHBlock[$k] != $v && !in_array($k, ['NAME']))
                        $arUpdate[$k] = $v;

                if (empty($arUpdate))
                    continue;

                $res = \Bitrix\Highloadblock\HighloadBlockTable::update($arDBHBlock['ID'], $arUpdate);
                if (!$res->isSuccess())
                    $arReport[] = 'Ошибка: hightload-инфоблок не обновлен - '
                        . $arItem['NAME'] .' - '. implode(', ', $res->getErrorMessages());
                else
                    $arReport[] = 'Успех: hightload-инфоблок обновлен - ' . $arItem['NAME'] . ' - ' . $res->getId();
            }
            // добавление
            else {
                $res = \Bitrix\Highloadblock\HighloadBlockTable::add($arItem);
                if (!$res->isSuccess())
                    $arReport[] = 'Ошибка: hightload-инфоблок не добавлен - '
                        . $arItem['NAME'] .' - '. implode(', ', $res->getErrorMessages());
                else
                    $arReport[] = 'Успех: hightload-инфоблок добавлен - ' . $arItem['NAME'] . ' - ' . $res->getId();
            }
        }

        if (!empty($arReport))
            $this->arReport = array_merge($this->arReport, $arReport);

        return $arReport;
    }

    /**
     * Пользовательские свойства
     *
     * @param $arEntity
     * @param bool $bDelete
     * @return array
     */
    private function setUserFields($arEntity, $bDelete = false) {

        $obUserTypeEntity = new \CUserTypeEntity();

        $arReport = $arHBlocksCode = $arDBEntityId = $arDBUserFields = [];

        if (empty($arEntity))
            return $arReport;

        $arDBEntityId = $this->getUserFieldsId($arEntity);

        // получение групп свойств по EntityId
        if  (!empty($arDBEntityId))
            $arDBUserFields = $this->getUserFieldsList(array_values($arDBEntityId));

        // обход добавляемых сущностей
        foreach ($arEntity as $arItem) {

            $arItem['ENTITY_ID'] = $arDBEntityId[$arItem['ENTITY_ID']] ? : $arItem['ENTITY_ID'];
            $arDBUserField       = $arDBUserFields[$arItem['ENTITY_ID']][$arItem['FIELD_NAME']];

            // удаление
            if ($bDelete) {
                if (empty($arDBUserField['ID'])) {
                    $arReport[] = 'Ошибка удаления: пользовательское свойство не найдено в БД - '
                        . $arItem['ENTITY_ID'] .' - ' . $arItem['FIELD_NAME'];
                }
                else {
                    if (false === $obUserTypeEntity->Delete($arDBUserField['ID']))
                        $arReport[] = 'Ошибка: пользовательское свойство не удалено - '
                            . $arItem['ENTITY_ID'] . ' - ' . $arItem['FIELD_NAME'] . ' - ' . $arDBUserField['ID'];
                    else
                        $arReport[] = 'Успех: пользовательское свойство удалено - '
                            . $arItem['ENTITY_ID'] . ' - ' . $arItem['FIELD_NAME'] . ' - ' . $arDBUserField['ID'];
                }
            }
            // обновление
            elseif ($arDBUserField['ID'] > 0) {

                $arUpdate  = [];
                foreach ($arItem as $k => $v)
                    if ($arDBUserField[$k] != $v && !is_array($v) // вложенные массивы не проверяю пока что
                            && !in_array($k, ['ID', 'USER_TYPE_ID', 'MULTIPLE', 'ENTITY_ID', 'FIELD_NAME']))
                        $arUpdate[$k] = $v;

                if (empty($arUpdate))
                    continue;

                if (!$obUserTypeEntity->Update($arDBUserField['ID'], $arUpdate))
                    $arReport[] = 'Ошибка: пользовательское свойство не обновлено - '
                        . $arItem['ENTITY_ID'] .' - ' . $arItem['FIELD_NAME'] .' - ' . $arDBUserField['ID'];
                else
                    $arReport[] = 'Успех: пользовательское свойство обновлено - '
                        . $arItem['ENTITY_ID'] .' - ' . $arItem['FIELD_NAME'] .' - ' . $arDBUserField['ID'];
            }
            // добавление
            else {
                $iUserEntity = intval($obUserTypeEntity->Add($arItem));
                if ($iUserEntity <= 0)
                    $arReport[] = 'Ошибка: пользовательское свойство не добавлено - '
                        . $arItem['ENTITY_ID'] .' - ' . $arItem['FIELD_NAME'];
                else
                    $arReport[] = 'Успех: пользовательское свойство добавлено - '
                        . $arItem['ENTITY_ID'] .' - ' . $arItem['FIELD_NAME'] .' - ' . $iUserEntity;
            }
        }

        if (!empty($arReport))
            $this->arReport = array_merge($this->arReport, $arReport);

        return $arReport;
    }

    /**
     * Значения пользовательских свойств типа "Список"
     *
     * @param $arEntity
     * @param bool $bDelete
     * @return array
     */
    private function setUserFieldEnum($arEntity, $bDelete = false)
    {
        $obUserFieldEnum  = new \CUserFieldEnum();

        $arReport = $arEnumProps = $arDBUserFields = $arUserFieldEnum = $arUserFieldEnumId = [];

        if (empty($arEntity))
            return $arReport;

        $arDBEntityId = $this->getUserFieldsId($arEntity);
        foreach ($arEntity as $arItem)
            $arEnumProps[$arItem['USER_FIELD_ID']] = $arItem['USER_FIELD_ID'];

        // получение свойств по EntityId
        if  (!empty($arEnumProps))
            $arDBUserFields = $this->getUserFieldsList(array_values($arDBEntityId));

        // собираем ID пользовательских свойств типа список
        foreach ($arEntity as $arItem) {
            $arUserField = $arDBUserFields[$arDBEntityId[$arItem['ENTITY_ID']]][$arItem['USER_FIELD_ID']];

            if (empty($arUserField) || $arUserField['USER_TYPE_ID'] !== 'enumeration')
                continue;

            $arUserFieldEnum[$arUserField['ENTITY_ID']][$arUserField['FIELD_NAME']] = $arUserField['ID'];
            $arUserFieldEnumId[$arUserField['ID']] = $arUserField['ID'];
        }

        // значения имеющихся в базе пользовательских свойств
        $arDBUserFieldEnum = self::getUserFieldEnum(array_values($arUserFieldEnumId));

        // сравнение, добавление и обновление
        foreach ($arEntity as $arItem) {
            $arUserField = $arDBUserFields[$arDBEntityId[$arItem['ENTITY_ID']]][$arItem['USER_FIELD_ID']]; // свойство в БД
            $arUserFieldValue = $arDBUserFieldEnum[$arUserField['ID']][$arItem['XML_ID']]; // значение свойства в БД

            if (empty($arUserField) || $arUserField['USER_TYPE_ID'] !== 'enumeration')
                continue;

            // удаление
            if ($bDelete) {
                if (empty($arUserFieldValue)) {
                    $arReport[] = 'Ошибка: значение пользовательского свойства не найдено - '
                       . $arItem['ENTITY_ID'] .' - ' .$arItem['USER_FIELD_ID'] .' - ' . $arItem['VALUE'];
                } else {
                    $obUserFieldEnum->DeleteFieldEnum($arUserField['ID']);
                    $arReport[] = 'Успех: значение пользовательского свойства удалено - '
                        . $arUserField['ENTITY_ID'] . ' - ' . $arItem['USER_FIELD_ID'] . ' - ' . $arItem['VALUE'];
                }
            }
            // обновление
            elseif (!empty($arUserFieldValue)) {
                // пока не писал и не факт что нужно
            }
            // добавление
            else {
                unset($arItem['ENTITY_ID'], $arItem['USER_FIELD_ID']);
                $bUserFieldEnum = $obUserFieldEnum->SetEnumValues($arUserField['ID'], ['n0' => $arItem]);
                if (!$bUserFieldEnum)
                    $arReport[] = 'Ошибка: значение пользовательского свойства не добавлено - '
                        . $arUserField['ENTITY_ID'] .' - ' . $arUserField['FIELD_NAME'] .' - ' . $arItem['VALUE'];
                else
                    $arReport[] = 'Успех: значение пользовательского свойства добавлено - '
                        . $arUserField['ENTITY_ID'] .' - ' . $arUserField['FIELD_NAME'] .' - ' . $arItem['VALUE'];
            }
        }

        if (!empty($arReport))
            $this->arReport = array_merge($this->arReport, $arReport);

        return $arReport;
    }

    /**
     * Типы почтовых событий
     *
     * @param $arEntity
     * @param bool $bDelete
     * @return array
     */
    private function setEventType($arEntity, $bDelete = false) {

        $arReport = $arDBEventTypes = [];

        if (empty($arEntity))
            return $arReport;

        // выбор всех почтовых типов из БД
        $arDBEventTypes = $this->getEventTypeList();

        // добавление отсутствующих типов в БД
        foreach ($arEntity as $arEvent) {

            $arDBEventType = $arDBEventTypes[$arEvent['LID']][$arEvent['EVENT_NAME']];

            // удаление
            if ($bDelete) {
                if (empty($arDBEventType)) {
                    $arReport[] = 'Ошибка удаления: тип почтового события не найден в БД - '
                        . $arEvent['EVENT_NAME'] . ' - '.$arEvent['LID'];
                }
                else {
                    $res = \Bitrix\Main\Mail\Internal\EventTypeTable::delete($arDBEventType['ID']);
                    if (!$res->isSuccess())
                        $arReport[] = 'Ошибка: тип почтового события не удален - '. $arDBEventType['EVENT_NAME']
                            .' - '.$arDBEventType['LID'] . ' - '. implode(', ', $res->getErrorMessages());
                    else
                        $arReport[] = 'Успех: тип почтового события удален - '
                            . $arDBEventType['EVENT_NAME'] . ' - '.$arDBEventType['LID'];
                }
            }
            // обновление
            elseif ($arDBEventType['ID'] > 0) {
                $arUpdate  = [];
                foreach ($arEvent as $k => $v)
                    if ($arDBEventType[$k] != $v && !in_array($k, ['ID']))
                        $arUpdate[$k] = $v;

                if (empty($arUpdate))
                    continue;

                if (!$res = \Bitrix\Main\Mail\Internal\EventTypeTable::update($arDBEventType['ID'], $arUpdate))
                    $arReport[] = 'Ошибка: тип почтового события не обновлен - ' . $arEvent['EVENT_NAME']
                        . ' - '.$arEvent['LID'] . implode(', ', $res->getErrorMessages());
                else
                    $arReport[] = 'Успех: тип почтового события обновлен - '
                        . $arEvent['EVENT_NAME'] . ' - '.$arEvent['LID'];
            }
            // добавление
            else {
                if (!$iAgent = \Bitrix\Main\Mail\Internal\EventTypeTable::add($arEvent))
                    $arReport[] = 'Ошибка: тип почтового события не добавлен - '
                        . $arEvent['EVENT_NAME'].' - ' . $arEvent['LID'];
                else
                    $arReport[] = 'Успех: тип почтового события добавлен - '
                        . $arEvent['EVENT_NAME'].' - ' . $arEvent['LID'];
            }
        }

        if (!empty($arReport))
            $this->arReport = array_merge($this->arReport, $arReport);

        return $arReport;
    }

    /**
     * Почтовые шаблоны
     *
     * @param $arEntity
     * @param bool $bDelete
     * @return array
     */
    private function setEventMessage($arEntity, $bDelete = false) {

        $arReport = $arDBAgents = $arEventType = [];

        if (empty($arEntity))
            return $arReport;

        foreach ($arEntity as $arEventMessage)
            $arEventType[] = $arEventMessage['EVENT_NAME'];

        $arDBEventMessages = self::getEventMessageList($arEventType);

        foreach ($arEntity as $arEventMessage) {

            $arDBEventMessage = [];
            foreach ($arDBEventMessages[$arEventMessage['EVENT_NAME']] as $arItem) {
                if ($arEventMessage['SUBJECT'] == $arItem['SUBJECT']) {
                    $arDBEventMessage = $arItem;
                    break;
                }
            }

            // удаление
            if ($bDelete) {
                if (!$arDBEventMessage['ID'])
                    continue;

                $res = \Bitrix\Main\Mail\Internal\EventMessageTable::delete($arDBEventMessage['ID']);
                if (!$res->isSuccess())
                    $arReport[] = 'Ошибка: почтовый шаблон не удален - ' . $arEventMessage['EVENT_NAME']
                        . ' - ' . $arEventMessage['SUBJECT'] . ' - ' . implode(', ', $res->getErrorMessages());
                else
                    $arReport[] = 'Успех: почтовый шаблон удален - '
                        . $arEventMessage['EVENT_NAME'] . ' - ' . $arEventMessage['SUBJECT'];
            }
            // обновление
            elseif ($arDBEventMessage['ID'] > 0) {
                $arUpdate  = [];
                foreach ($arEventMessage as $k => $v)
                    if ($arDBEventMessage[$k] != $v && !in_array($k, ['ID', 'EVENT_NAME', 'SUBJECT']))
                        $arUpdate[$k] = $v;

                if (empty($arUpdate))
                    continue;

                $res = \Bitrix\Main\Mail\Internal\EventMessageTable::update($arDBEventMessage['ID'], $arUpdate);
                if (!$res->isSuccess())
                    $arReport[] = 'Ошибка: почтовый шаблон не обновлен - '. $arEventMessage['EVENT_NAME']
                        .' - '. $arEventMessage['SUBJECT'] .' - '. implode(', ', $res->getErrorMessages());
                else
                    $arReport[] = 'Успех: почтовый шаблон обновлен - '
                        . $arEventMessage['EVENT_NAME'] . ' - '.$arEventMessage['SUBJECT'];
            }
            // добавление
            else {
                $res = \Bitrix\Main\Mail\Internal\EventMessageTable::add($arEventMessage);
                if (!$res->isSuccess())
                    $arReport[] = 'Ошибка: почтовый шаблон не добавлен - '. $arEventMessage['EVENT_NAME']
                        .' - '.$arEventMessage['SUBJECT'] .' - '. implode(', ', $res->getErrorMessages());
                else
                    $arReport[] = 'Успех: почтовый шаблон добавлен - '
                        . $arEventMessage['EVENT_NAME'] . ' - '.$arEventMessage['SUBJECT'];
            }
        }

        if (!empty($arReport))
            $this->arReport = array_merge($this->arReport, $arReport);

        return $arReport;
    }

    /**
     * Агенты
     *
     * @param $arEntity
     * @param bool $bDelete
     * @return array
     */
    private function setAgent($arEntity, $bDelete = false) {

        $arReport = $arDBAgents = [];

        if (empty($arEntity))
            return $arReport;

        $arDBAgents = self::getAgentList();

        foreach ($arEntity as $arAgent) {

            $arDBAgent = $arDBAgents[$arAgent['NAME']];

            // удаление
            if ($bDelete) {
                if (empty($arDBAgent['ID'])) {
                    $arReport[] = 'Ошибка: агент не найден в БД - ' . $arAgent['NAME'];
                }
                else {
                    if (!CAgent::Delete($arDBAgent['ID']))
                        $arReport[] = 'Ошибка: агент не удален - ' . $arAgent['NAME'];
                    else
                        $arReport[] = 'Успех: агент удален - ' . $arAgent['NAME'];
                }
            }
            // обновление
            elseif ($arDBAgent['ID'] > 0) {
                $arUpdate  = [];
                foreach ($arAgent as $k => $v)
                    if ($arDBAgent[$k] != $v && !in_array($k, ['ID', 'LAST_EXEC', 'NEXT_EXEC']))
                        $arUpdate[$k] = $v;

                if (empty($arUpdate))
                    continue;

                if (!$ob = CAgent::Update($arDBAgent['ID'], $arUpdate))
                    $arReport[] = 'Ошибка: агент не обновлен - '.$arAgent['NAME'];
                else
                    $arReport[] = 'Успех: агент обновлен - '.$arAgent['NAME'];

            }
            // добавление
            else {
                if (!$iAgent = CAgent::Add($arAgent))
                    $arReport[] = 'Ошибка: агент не добавлен - '.$arAgent['NAME'];
                else
                    $arReport[] = 'Успех: агент добавлен - '.$arAgent['NAME'];
            }
        }

        if (!empty($arReport))
            $this->arReport = array_merge($this->arReport, $arReport);

        return $arReport;
    }

    /**
     * Получает список сайтов
     *
     * @return array
     */
    private static function getSites() {

        $arResult = [];

        $rsSites = CSite::GetList($by="sort", $order="asc", []);
        while ($arItem = $rsSites->Fetch())
            $arResult[$arItem['LID']] = $arItem;

        return $arResult;
    }

    /**
     * Получает список типов инфоблоков
     *
     * @return array
     */
    private static function getIBlockType () {

        $arResult = [];

        $rsIBlockType = CIBlockType::GetList();
        while($arItem = $rsIBlockType->Fetch())
            $arResult[$arItem['ID']] = $arItem;

        return $arResult;
    }

    /**
     * Получает список инфоблоков
     *
     * @return array
     */
    private static function getIBlockList() {

        $arResult = [];

        $rsIBlock = CIBlock::GetList([], []);
        while($arItem = $rsIBlock->Fetch())
            $arResult[$arItem['IBLOCK_TYPE_ID']][$arItem['CODE']] = $arItem;

        return $arResult;
    }

    /**
     * Получает список H-блоков из БД
     *
     * @param array $arHBlockName
     * @return array
     */
    private static function getHBlockList($arHBlockName = []) {

        $arResult = $arFilter = [];

        if (!empty($arHBlockName))
            $arFilter = ['NAME' => $arHBlockName];

        // выбор таблиц из БД
        $rsHIBlocks = \Bitrix\Highloadblock\HighloadBlockTable::getList(['filter' => $arFilter]);
        while ($arItem = $rsHIBlocks->fetch())
            $arResult[$arItem['NAME']] = $arItem;

        return $arResult;
    }

    /**
     * Получает список пользовательских свойств по фильтру ENTITY_ID
     *
     * @param array $arUserFieldsId
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    private static function getUserFieldsList($arUserFieldsId = []) {

        $arResult = $arFilter = [];

        if (!empty($arUserFieldsId))
            $arFilter = ['ENTITY_ID' => $arUserFieldsId];

        $rsFields = \Bitrix\Main\UserFieldTable::getList(['filter' => $arFilter]);
        while ($arItem = $rsFields->fetch())
            $arResult[$arItem['ENTITY_ID']][$arItem['FIELD_NAME']] = $arItem;

        return $arResult;
    }

    /**
     * Выборка значений пользовательских свойств типа "список"
     *
     * @param array $arUserFieldEnum
     * @return array
     */
    private static function getUserFieldEnum($arUserFieldEnum = []) {

        $arResult = $arFilter = [];

        if (!empty($arUserFieldEnum))
            $arFilter = ['USER_FIELD_ID' => $arUserFieldEnum];

        $obUserFieldEnum = new \CUserFieldEnum();
        $rsUserFieldEnum = $obUserFieldEnum->GetList(['SORT' => 'ASC'], $arFilter);
        while ($arItem = $rsUserFieldEnum->Fetch())
            $arResult[$arItem['USER_FIELD_ID']][$arItem['XML_ID']] = $arItem;

        return $arResult;
    }

    /**
     * Получает коды сущностей в БД по кодам из массива пользовательских свойств
     * @param $arEntity
     * @return array
     */
    private function getUserFieldsId($arEntity) {

        $arResult = $arEntityId = [];

        // все сущности
        foreach ($arEntity as $arItem)
            $arEntityId[$arItem['ENTITY_ID']] = $arItem['ENTITY_ID'];

        // поиск кодов конкретных сущностей
        foreach ($arEntityId as $sCode)
            if (strpos($sCode, 'HLBLOCK_') !== false)
                $arHBlocksCode[$sCode] = str_replace('HLBLOCK_', '', $sCode);

        // запрос к highload-инфоблокам
        if (!empty($arHBlocksCode))
            foreach ($this->getHBlockList(array_values($arHBlocksCode)) as $arItem)
                $arResult['HLBLOCK_' . $arItem['NAME']] = 'HLBLOCK_' . $arItem['ID'];

        return $arResult;
    }

    /**
     * Получает все типы почтовых событий из БД
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     */
    private static function getEventTypeList($arType = []) {

        $arResult = $arFilter = [];

        if (!empty($arType))
            $arFilter = ['EVENT_NAME' => $arType];

        $rsEventType = \Bitrix\Main\Mail\Internal\EventTypeTable::getList(['filter' => $arFilter]);
        while ($arItem = $rsEventType->Fetch())
            $arResult[$arItem['LID']][$arItem['EVENT_NAME']] = $arItem;

        return $arResult;
    }

    private static function getEventMessageList($arType = []) {

        $arResult = $arFilter = [];

        if (!empty($arType))
            $arFilter = ['EVENT_NAME' => $arType];

        $rsEventMessage = \Bitrix\Main\Mail\Internal\EventMessageTable::getList(['filter' => $arFilter]);
        while ($arItem = $rsEventMessage->Fetch()) {
            unset($arItem['MESSAGE_PHP']);
            $arResult[$arItem['EVENT_NAME']][$arItem['ID']] = $arItem;
        }

        return $arResult;
    }

    private static function getAgentList() {

        $arResult = [];

        $rsAgents = CAgent::GetList();
        while ($arItem = $rsAgents->Fetch())
            $arResult[$arItem['NAME']] = $arItem;

        return $arResult;
    }

    /**
     * Установка времени работы скрипта
     * @param bool $bEnd
     */
    private function setReportTime($bEnd = false) {
        if (!$bEnd)
            $this->arReport[] = 'Время начала работы скрипта: '.self::getDateTime();
        else
            $this->arReport[] = 'Время окончания работы скрипта: '.self::getDateTime();
    }

    /**
     * Получение текущего времени в заданном формате
     * @return bool|string
     */
    private static function getDateTime() {
        return date('d.m.Y H:i:s');
    }
}