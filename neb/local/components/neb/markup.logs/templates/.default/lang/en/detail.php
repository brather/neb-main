<?php

$MESS['MARKUP_LOG_TITLE']       = 'Status change log';
$MESS['MARKUP_LOG_DESCRIPTION'] = 'Description';

$MESS['MARKUP_LOG_AUTHOR']      = 'Author';
$MESS['MARKUP_LOG_NAME']        = 'Title';
$MESS['MARKUP_LOG_STARTPAGE']   = 'Begin';
$MESS['MARKUP_LOG_ENDPAGE']     = 'End';
$MESS['MARKUP_LOG_POSITION']    = 'Position';
$MESS['MARKUP_LOG_RANGEPAGE']   = 'Range page';
$MESS['MARKUP_LOG_STATUS']      = 'Status';
$MESS['MARKUP_LOG_MODERATION']  = 'Moderation status';
$MESS['MARKUP_LOG_DATE']        = 'Date';
$MESS['MARKUP_LOG_LOGIN']       = 'Login';

$MESS['MARKUP_LOG_STATUS_ADDED']         = 'Added';
$MESS['MARKUP_LOG_STATUS_CONSIDERATION'] = 'Consideration';
$MESS['MARKUP_LOG_STATUS_CONFIRMED']     = 'Confirmed';
$MESS['MARKUP_LOG_STATUS_DELETED']       = 'Deleted';