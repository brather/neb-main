<?

$MESS['LIB_SEARCH_PAGE_DETAIL_AUTHOR'] = 'Author';
$MESS['LIB_SEARCH_PAGE_DETAIL_TITLE'] = 'Title';
$MESS['LIB_SEARCH_PAGE_DETAIL_SUBTITLE'] = 'Subtitle';
$MESS['LIB_SEARCH_PAGE_DETAIL_RESPONSIBILITY'] = 'Responsibility';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE'] = 'Publish place';
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH'] = 'Publisher';
$MESS['LIB_SEARCH_PAGE_DETAIL_YEAR'] = 'Year of publication';
$MESS['LIB_SEARCH_PAGE_DETAIL_PAGES'] = 'Pages';
$MESS['LIB_SEARCH_PAGE_DETAIL_SERIE'] = 'Chapter';
$MESS['LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT'] = 'Note content';
$MESS['LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE'] = 'Note';
$MESS['LIB_SEARCH_PAGE_DETAIL_ANNOTATION'] = 'Annotation';
$MESS['LIB_SEARCH_PAGE_DETAIL_BBK'] = 'BBK';
$MESS['LIB_SEARCH_PAGE_DETAIL_UDC'] = 'Universal Decimal Classification (UDC)';
$MESS['LIB_SEARCH_PAGE_DETAIL_UDCFULL'] = 'UDC section';
$MESS['LIB_SEARCH_PAGE_DETAIL_EDITION'] = 'Edition';
$MESS['LIB_SEARCH_PAGE_DETAIL_LIB'] = 'Library';
$MESS['LIB_SEARCH_PAGE_DETAIL_LANGUAGE'] = 'Language';
$MESS['LIB_SEARCH_PAGE_DETAIL_INCLUDED_IN'] = 'Includen in'; //Входит в состав

$MESS['LIB_SEARCH_PAGE_DETAIL_FUND_NUMBER'] = 'Fund number'; //Номер фонда
$MESS['LIB_SEARCH_PAGE_DETAIL_FUND_NAME'] = 'Fund name'; //Название фонда
$MESS['LIB_SEARCH_PAGE_DETAIL_INVENTORY_NUMBER'] = 'Inventory number'; // Номер описи
$MESS['LIB_SEARCH_PAGE_DETAIL_INVENTORY_NAME'] = 'Inventory name'; //Заголовок описи
$MESS['LIB_SEARCH_PAGE_DETAIL_UNIT_NUMBER'] = 'Unit number'; //Номер дела
$MESS['LIB_SEARCH_PAGE_DETAIL_UNIT_NAME'] = 'Unit name';//Заголовок дела';
$MESS['LIB_SEARCH_PAGE_DETAIL_DATES'] = 'Dates'; //Крайние даты

$MESS['LIB_SEARCH_PAGE_DETAIL_DESCRIPTION'] = 'Description'; //Описание
$MESS['LIB_SEARCH_PAGE_DETAIL_MATERIAL'] = 'Material, technique'; //Материал, техника исполнения
$MESS['LIB_SEARCH_PAGE_DETAIL_SIZE'] = 'Size'; //Размеры

$MESS['LIB_SEARCH_PAGE_DETAIL_MPK'] = 'MPK'; //МПК
$MESS['LIB_SEARCH_PAGE_DETAIL_DECLARANT'] = 'Declarant'; //Заявитель
$MESS['LIB_SEARCH_PAGE_DETAIL_PATENTHOLDER'] = 'Patentholder'; //Патентообладатель
$MESS['LIB_SEARCH_PAGE_DETAIL_REPRESENTATIVE'] = 'Representative';//Представитель
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH_DATE'] = 'Publication date'; //Дата публикации
$MESS['LIB_SEARCH_PAGE_DETAIL_PATENTCODE'] = 'Patent code'; //Дата публикации

$MESS['LIB_SEARCH_PAGE_DETAIL_CATEGORIES'] = 'Categories'; //Рубрики

// ---- libs
$MESS['LIB_SEARCH_PAGE_DETAIL_ADDRESS'] = 'Address';
$MESS['LIB_SEARCH_PAGE_DETAIL_WORK'] = 'Working schedule';
$MESS['LIB_SEARCH_PAGE_DETAIL_USER'] = 'Member';
$MESS['LIB_SEARCH_PAGE_DETAIL_URL'] = 'Go to the library';

// ------
$MESS['LIB_SEARCH_PAGE_DETAIL_ABOUT']     = 'About'; //О произведении
$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLIC']    = 'Public publication'; //Открытое издание
$MESS['LIB_SEARCH_PAGE_DETAIL_PROTECTED'] = 'Copyright publication'; // Защищенное
$MESS['LIB_SEARCH_PAGE_DETAIL_COPYRIGHT'] = 'Suspended by the rightholder';

$MESS['LIB_SEARCH_PAGE_DETAIL_EKBSON'] = 'Directory entry EKBSON'; //Запись каталога ЭКБСОН
$MESS['LIB_SEARCH_PAGE_DETAIL_CATALOG'] = 'Directory entry'; // Запись каталога

$MESS['LIB_SEARCH_PAGE_DETAIL_NEAREST_BIBLIO_PAPER'] = 'The nearest library with a paper copy of the edition'; //Ближайшая библиотека с бумажным экземпляром издания
$MESS['LIB_SEARCH_PAGE_DETAIL_NEAREST_BIBLIO_I'] = 'The nearest electronic reading room'; //Ближайший электронный читальный зал
$MESS['LIB_SEARCH_PAGE_DETAIL_SERVER_ERROR'] = 'Internal server error'; //Ошибка сервера. Пожалуйста, повторите запрос
$MESS['LIB_SEARCH_PAGE_DETAIL_RELEVANT_BOOKS'] = ' With this edition also searched'; // C этим изданием также искали
$MESS['LIB_SEARCH_PAGE_DETAIL_PAPER_TO_EBOOK'] = 'You can order for translation of this work into an electronic form';

// ---- actions
$MESS['LIB_SEARCH_PAGE_DETAIL_BACK'] = 'Back'; //Вернуться
$MESS['LIB_SEARCH_PAGE_DETAIL_READ'] = 'Read';// Читать
$MESS['LIB_SEARCH_PAGE_DETAIL_VIEW'] = 'View'; //Смотреть
$MESS['LIB_SEARCH_PAGE_DETAIL_LISTEN'] = 'Listen';
$MESS['LIB_SEARCH_PAGE_DETAIL_REPORT_AN_ERROR'] = 'Report an error';
$MESS['LIB_SEARCH_PAGE_DETAIL_OPEN_THIS_BOOK'] = 'Open this book'; //Открыть эту книгу
$MESS['LIB_SEARCH_PAGE_DETAIL_CLOSE_THIS_BOOK'] = 'Close this book'; //Закрыть эту книгу
$MESS['LIB_SEARCH_PAGE_DETAIL_ORDER'] = 'Order';

$MESS[''] = '';