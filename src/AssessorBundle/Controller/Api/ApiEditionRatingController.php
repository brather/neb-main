<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 13:23
 */

namespace AssessorBundle\Controller\Api;

use AssessorBundle\Form\TblBookRatingType;
use AssessorBundle\Traits\AssessorRatingTrait;
use Doctrine\ORM\QueryBuilder;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ApiEntityRatingController
 *
 * @DI\Service("assessor.api.edition_rating", scope="request")
 */
class ApiEditionRatingController implements RatingApiInterface
{
    use AssessorRatingTrait;

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'AssessorBundle:TblBookRating';
    }

    /**
     * @return TblBookRatingType
     */
    public function getFormType()
    {
        return new TblBookRatingType();
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return void
     */
    public function handleAutoComplete($qb)
    {
        $query = $this->request->query->get('query');
        if ('title' === $this->request->query->get('entityField')) {
            $qb->where("t.title LIKE :query");
        } else {
            $qb->where("t.author LIKE :query");
        }
        $qb->setParameter('query', $query . '%');
    }
}