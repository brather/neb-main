<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('nota.userdata');
use Nota\UserData\Rgb;

try{

    if('POST' ==  $_SERVER['REQUEST_METHOD']):
        $email = $_POST['email'];


        if(check_email($email)):
            $nebUser = new nebUser();
            $arFieldsUser = array(
                'EMAIL' => $email,
            );

            $user = $nebUser->GetList($b, $o, array("=EMAIL" => $email));
            if ($user->Fetch()):
                throw new \Exception('Пользователь с таким e-mail уже зарегистрирован');
            endif;

            if($nebUser->Update($nebUser->USER_ID, $arFieldsUser)):

                $rgb = new rgb();
                $data = $nebUser->getUser();
                //$res = $rgb->userUpdate(array('uid' => $data['UF_RGB_USER_ID'], 'email' => $email));

                LocalRedirect('/profile/', false);
            else:

                throw new \Exception('Неверный пользователь!');
            endif;
        else:
            throw new \Exception('Неправильный email!');
        endif;

    endif;

}catch (\Exception $e ){
    $arResult['ERROR'] = $e->getMessage();
}

$this->IncludeComponentTemplate();