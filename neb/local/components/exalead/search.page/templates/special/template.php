<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<section class="innersection innerwrapper clearfix <?=empty($arResult['ITEMS'])? ' searchempty' : ''?>">
    <div class="clearfix">
		<?
			if($arResult['COUNT'] >= 300000)
			{
				echo Loc::getMessage('LIB_SEARCH_PAGE_FOUND_MORE_300000_RESULT');
			}
			else
			{
			?>
			<?=GetEnding($arResult['COUNT'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'))?> 
			<?=number_format($arResult['COUNT'], 0, '', ' ')?> 
			<?=GetEnding($arResult['COUNT'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'))?>

			<?
				if(empty($_REQUEST['slparam']))
				{
				?>
				(<?=getmessage('LIB_SEARCH_PAGE_INCLUDING');?> <a href="<?=$APPLICATION->GetCurPageParam('slparam=sl_nofuzzy', array('slparam'))?>"><?=number_format($arResult['COUNT']-$arResult['NHITS'], 0, '', ' ')?></a> <?=getmessage('LIB_SEARCH_PAGE_DUPLICATED');?> )
				<?
				}
			}
		?>

<div class="b-filter">
	<div class="b-filter_wrapper">
		<a href="#" class="sort sort_opener"><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SORT');?></a>
		<span class="sort_wrap" align="center">
			<a <?=SortingExalead("document_authorsort")?>><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SORT_BY_AUTHOR');?></a>
			<a <?=SortingExalead("document_titlesort")?>><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SORT_BY_NAME');?></a>
		</span>
		<span class="b-filter_act" align="center">
			<span class="b-filter_show"><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SORT_BY_SHOW');?></span>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=15", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 15 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">15</a>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 30 ? ' current' : ''?>">30</a>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=45", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 45 ? ' current' : ''?>">45</a>
			<?
				if(false && !empty($arParams['SHOW_LONG_PAGE'])){
				?>
				<a title="<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SHOW_ALL');?>" href="<?=$APPLICATION->GetCurPageParam("pagen=15&longpage=y", array("pagen", "dop_filter"));?>" class="b-filter_num <?=$arParams['LONG_PAGE'] ? 'current' : ''?>">&hellip;</a>
				<?
				}
			?>
		</span>
	</div>
</div><!-- .b-filter -->
		<div class="b-filter_list_wrapper">
            <div class="b-result-doc">
                <?foreach($arResult['ITEMS'] as $book):?>
                    <div class="b-result-docitem">

                        <div class="iblock b-result-docphoto">
                            <span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="<?=$book['IMAGE_URL']?>"></span>

                        </div>
                        <div class="iblock b-result-docinfo">
                            <span class="num"><?=$book['NUM_ITEM']?>.</span>
                            <h2><a  href="/special<?=$book['DETAIL_PAGE_URL']?>" title="<?=$book['title']?>"><?=$book['title']?></a></h2>
                            <ul class="b-resultbook-info">
                                <?if($book['author']):?><li><span>Автор:</span> <?=$book['author']?></li><?endif;?>
                                <?if($book['publishyear']):?><li><span>Год публикации:</span> <?=$book['publishyear']?></li><?endif;?>
                                <?if($book['countpages']):?><li><span>Количество страниц:</span> <?=$book['countpages']?></li><?endif;?>
                                <?if($book['publisher']):?><li><span>Издательство:</span> <?=$book['publisher']?></li><?endif;?>
                            </ul>
                            <div class="b-result_sorce clearfix">
                                <div class="b-result_sorce_info left"><em>Источник:</em> <?=$book['library']?></div>
                            </div>
                        </div>

                    </div>
                <?endforeach;?>

                <?=$arResult['STR_NAV']?>

            </div><!-- /.b-result-doc -->
        </div><!-- /.b-filter_list_wrapper -->


</section>