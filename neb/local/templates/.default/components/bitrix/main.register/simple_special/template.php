<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

	if ( (int)$arResult['VALUES']['USER_ID'] > 0 && $arResult["USE_EMAIL_CONFIRMATION"] === "Y" )
	{ 
	?>
	<section class="innersection innerwrapper clearfix">
		<div class="b-formsubmit">
			<span class="b-successlink"><?=GetMessage("REGISTER_SUCCESSFUL")?></span>
			<p><?=Loc::getMessage('REGISTER_EMAIL_WILL_BE_SENT');?></p>
		</div>
	</section>
	<?
		return;
	}
?>
<script type="text/javascript">
	function setlogin(){
		$('input#LOGIN').val($('input#settings04').val());
	}
</script>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode"><?=Loc::getMessage('MAIN_REGISTER_TITLE');?></h2>
		<?
			if (count($arResult["ERRORS"]) > 0){
				foreach ($arResult["ERRORS"] as $key => $error){
					if (intval($key) == 0 && $key !== 0) {
						$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
					}
				}

				ShowError(implode("<br />", $arResult["ERRORS"]).'<br /><br />');

			}elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && (int)$arResult['VALUES']['USER_ID'] > 0 ){
			?>
			<p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
			<?
			}
		?>
		<form action="<?=POST_FORM_ACTION_URI?>" class="b-form b-form_common b-regform" method="post" name="regform" enctype="multipart/form-data" onsubmit="setlogin()" id="regform">
			<?
				if($arResult["BACKURL"] <> ''){
				?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?
				}
			?>
			<input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value="" />

			<p class="note"><?=Loc::getMessage('MAIN_REGISTER_IMPORTANT');?></p>
			<!--<div class="b-form_header"><span class="iblock"><?/*=Loc::getMessage('MAIN_REGISTER_PASSPORT_DATA');*/?></span></div>-->
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings01"><?=Loc::getMessage('MAIN_REGISTER_LASTNAME');?></label>
					<div class="field validate">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>	
						<input type="text" data-validate="fio" data-required="required" value="<?=$arResult["VALUES"]['LAST_NAME']?>" id="settings01" data-maxlength="30" data-minlength="2" name="REGISTER[LAST_NAME]" class="input">										
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings02"><?=Loc::getMessage('MAIN_REGISTER_FIRSTNAME');?></label>
					<div class="field validate">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>		
						<input type="text" data-required="required" data-validate="fio" value="<?=$arResult["VALUES"]['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="REGISTER[NAME]" class="input" >										
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings22"><?=Loc::getMessage('MAIN_REGISTER_MIDNAME');?></label>
					<div class="field validate">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
						<input type="text" data-validate="fio" value="<?=$arResult["VALUES"]['SECOND_NAME']?>" id="settings22" data-maxlength="30" data-minlength="2"data-required="false"  name="REGISTER[SECOND_NAME]" class="input" >										
					</div>
				</div>
			</div>

			<!--<div class="b-form_header"><?/*=Loc::getMessage('MAIN_REGISTER_ENTER');*/?></div>-->

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings04"><?=Loc::getMessage('MAIN_REGISTER_EMAIL');?></label>
					<div class="field validate">
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_EMAIL_FORMAT');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<input type="text" class="input" name="REGISTER[EMAIL]" data-minlength="2" data-maxlength="50" id="settings04" data-validate="email" value="<?=$arResult["VALUES"]['EMAIL']?>" data-required="true">
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<div class="passecurity">
						<div class="passecurity_lb"><?=Loc::getMessage('MAIN_REGISTER_PROTECTED_PASSWORD');?></div>
					</div>
					<label for="settings05"><span><?=Loc::getMessage('MAIN_REGISTER_MIN_6S');?></span><?=Loc::getMessage('REGISTER_FIELD_PASSWORD');?></label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="true" value="<?=$arResult["VALUES"]['PASSWORD']?>" id="settings05" data-maxlength="30" data-minlength="6" name="REGISTER[PASSWORD]" class="pass_status input">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings55"><?=Loc::getMessage('MAIN_REGISTER_CONFIRM_PASSWORD');?></label>
					<div class="field validate">
						<em class="error identity "><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_MISMATCH');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
						<input data-identity="#settings05" type="password" data-required="true" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" id="settings55" data-maxlength="30" data-minlength="2" name="REGISTER[CONFIRM_PASSWORD]" class="input" data-validate="password">								
					</div>
				</div>
			</div>
			<hr>
			<!--div class="checkwrapper ">
				<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb33"><label for="cb33" class="black"><?=Loc::getMessage('MAIN_REGISTER_AGRE1');?> </label><a href="/special/user-agreement/" target="_blank"><?=Loc::getMessage('MAIN_REGISTER_AGRE2');?></a> <label for="cb3" class="black"><?=Loc::getMessage('MAIN_REGISTER_AGRE3');?></label>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix">
						<button disabled="disabled" type="submit" value="1" class="formbutton btdisable"><?=Loc::getMessage('MAIN_REGISTER_REGISTER');?></button>
						<input type="hidden" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
					</div>
				</div>
			</div-->
			<div class="checkwrapper">
				<label class="defautl_cursor"><?=Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP');?> </label> <a href="/local/components/neb/registration/templates/special/ajax_agreement.php" target="_blank" class="popup_opener ajax_opener closein" data-width="955" data-height="auto"><?=Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP_TERMS');?></a>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix divdisable">
						<button disabled="disabled" type="submit" value="1" class="formbutton btdisable"><?=Loc::getMessage('MAIN_REGISTER_REGISTER');?></button>
						<div class="b-hint"><?=Loc::getMessage('MAIN_REGISTER_READ_TERMS');?></div>
						<input type="hidden" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->
</section>