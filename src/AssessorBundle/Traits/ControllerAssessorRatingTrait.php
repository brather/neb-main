<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 14:44
 */

namespace AssessorBundle\Traits;


use AssessorBundle\Controller\Api\RatingApiInterface;
use AssessorBundle\Entity\AssessorRatingEntityInterface;
use AppBundle\DependencyInjection\BitrixMainPageNavigation;
use JMS\DiExtraBundle\Annotation as DI;


trait ControllerAssessorRatingTrait
{
    use AssessorRatingTrait,
        AssessorTabRoutingTrait;

    /**
     * @DI\Inject("neb.bitrix_main_pagenavigation")
     * @var BitrixMainPageNavigation
     */
    public $bitrixPager;


    /**
     * @param string $entityName
     * @param array  $order
     * @param array  $condition
     *
     * @return array
     */
    public function getListData($entityName, $order = [], $condition = [])
    {
        $pagerName = 'assessor';
        $pager = $this->bitrixPager;
        $pager->initPageNavigation($pagerName);

        $currentPage = $pager->getPageNavigation($pagerName)->getCurrentPage();
        if (!empty($order)) {
            $this->applyOrderBy($entityName, $order);
        }
        $result = $this->_entities(
            $entityName,
            $pager->getPageSize(),
            $pager->getPageSize() * ($currentPage - 1),
            [],
            $condition
        );

        $qb = $this->_getListQueryBuilder($entityName);
        $qb->select($qb->expr()->count('t.id'));
        $qb->setFirstResult(null);
        $qb->setMaxResults(null);
        $count = $qb->getQuery()->getSingleScalarResult();
        $pager->getPageNavigation($pagerName)->setRecordCount($count);

        $result['paginationHtml'] = $pager->buildBitrixMainPageNavigation($pagerName);

        $this->_prepareEntities($result['entities'], $this->getEntityCode($entityName));
        $this->_prepareResult($result);
        $result['url']['queryAutoComplete'] = $this->router->generate(
            'api_assessor_autocomplete',
            ['entity' => $this->getEntityCode($entityName)]
        );

        $result['order'] = [];
        $fields = $this->getEntityManager()->getClassMetadata($entityName)->getFieldNames();
        foreach ($fields as $field) {
            $result['order'][$field] = [
                'url' => $this->router->generate(
                    $this->getCurrentRoute(),
                    ['order' => [$field => 'desc']]
                ),
                'up'  => false
            ];
        }
        foreach ($order as $field => $direction) {
            $result['order'][$field] = [
                'active' => true,
                'url'    => $this->router->generate(
                    $this->getCurrentRoute(),
                    ['order' => [$field => ('asc' === $direction ? 'desc' : 'asc')]]
                ),
                'up'     => ('desc' === $direction) ? true : false
            ];
        }

        return $result;
    }

    /**
     * @param string $entityName
     * @param array  $order
     */
    public function applyOrderBy($entityName, $order)
    {
        $fields = $this->getEntityManager()->getClassMetadata($entityName)->getFieldNames();
        $fields = array_flip($fields);
        $qb = $this->_getListQueryBuilder($entityName);
        foreach ($order as $field => $direction) {
            if (isset($fields[$field])) {
                $qb->addOrderBy('t.' . $field, $direction);
            }
        }
    }

    /**
     * @return array
     */
    public function getCurrentRoute()
    {
        try {
            $currentRoute = $this->router->match($this->getCurrentPath());
            $currentRoute = $currentRoute['_route'];
        } catch (\Exception $e) {
            $currentRoute = RatingApiInterface::DEFAULT_ROUTE;
        }

        return $currentRoute;
    }

    /**
     * @return string
     */
    public function getCurrentPath()
    {
        $uri = $this->request->getRequestUri();
        $uri = parse_url($uri);

        return $uri['path'];
    }

    /**
     * @param array &$result
     */
    protected function _prepareResult(&$result)
    {
        $result['url'] = $this->getAssessorTabsUrls();
        $result['current'] = [$this->getCurrentName($this->getCurrentPath()) => true];
        $result['query'] = $this->request->query->all();
    }


    /**
     * @param AssessorRatingEntityInterface[] $entities
     * @param string                          $entityCode
     */
    protected function _prepareEntities($entities, $entityCode)
    {
        foreach ($entities as $entity) {
            if ($entity instanceof AssessorRatingEntityInterface) {
                $entity->setUpdateUrl(
                    $this->router->generate(
                        'api_assessor_entity_update',
                        ['entity' => $entityCode, 'id' => $entity->getId()]
                    )
                );
            }
        }
    }

}