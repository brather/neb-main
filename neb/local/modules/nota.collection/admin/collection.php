<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!CModule::IncludeModule('nota.collection')) {
    echo CAdminMessage::ShowMessage(Loc::getMessage('NOTA_COLLECTION_ACCESS_DENIED'));
    return false;
}

use Nota\Collection\CollectionTable;

$sTableID = 'tbl_collection_list';

$oSort = new CAdminSorting($sTableID, "NAME", "desc");
$arOrder = (strtoupper($by) === "ID" ? array($by => $order) : array($by => $order, "ID" => "ASC"));
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = Array(
    "find_id",
    "find_name",
    "find_library_id",
    "find_main",
);

$lAdmin->InitFilter($arFilterFields);

$arFilter = array(
    "ID" => $find_id,
    "?NAME" => $find_name,
    "LIBRARY_ID" => $find_library_id,
    "MAIN" => $find_main,
);

TrimArr($arFilter);

if ($lAdmin->EditAction()) {
    foreach ($FIELDS as $ID => $arFields) {
        $DB->StartTransaction();
        $ID = IntVal($ID);

        if (!$lAdmin->IsUpdated($ID))
            continue;

        if (!CollectionTable::Update($ID, $arFields)) {
            $lAdmin->AddUpdateError('Udate error', $ID);
            $DB->Rollback();
        }
        $DB->Commit();
    }
}

if ($arID = $lAdmin->GroupAction()) {
    if ($_REQUEST['action_target'] == 'selected') {
        $rsData = CollectionTable::getList(array('select' => array('ID'), 'filter' => $arFilter));
        while ($arRes = $rsData->fetch())
            $arID[] = $arRes['ID'];
    }

    foreach ($arID as $ID) {
        if (strlen($ID) <= 0)
            continue;

        switch ($_REQUEST['action']) {
            case "delete":
                $DB->StartTransaction();
                $result = CollectionTable::delete($ID);
                if (!$result->isSuccess()) {
                    $DB->Rollback();
                    $lAdmin->AddGroupError(
                        'Ошибка' .
                        ' (ID = ' . $ID . ': ' . implode('<br>', $result->getErrorMessages()) . ')',
                        $ID
                    );
                }

                $DB->Commit();
                break;
        }
    }
}

$arHeader = array(
    array(
        'id' => 'ID',
        'content' => Loc::getMessage('NOTA_COLLECTION_ID'),
        'sort' => 'ID',
        'default' => true
    ),
    array(
        'id' => 'NAME',
        'content' => Loc::getMessage('NOTA_COLLECTION_NAME'),
        'sort' => 'NAME',
        'default' => true
    ),
    array(
        'id' => 'SORT',
        'content' => Loc::getMessage('NOTA_COLLECTION_SORT'),
        'sort' => 'SORT',
        'default' => true
    ),
    array(
        'id' => 'MAIN',
        'content' => Loc::getMessage('NOTA_COLLECTION_MAIN'),
        'sort' => 'MAIN',
        'default' => true
    ),
    array(
        'id' => 'TIMESTAMP_X',
        'content' => Loc::getMessage('NOTA_COLLECTION_TIMESTAMP_X'),
        'sort' => 'TIMESTAMP_X',
        'default' => true
    ),
);


$lAdmin->AddHeaders($arHeader);

$arSelect = $lAdmin->GetVisibleHeaderColumns();
if (!in_array('ID', $arSelect))
    $arSelect[] = 'ID';

$rsData = CollectionTable::getList(array(
    'select' => $arSelect,
    'filter' => $arFilter,
    'order' => $arOrder
));

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(''));

while ($arRes = $rsData->NavNext()) {
    $row = $lAdmin->AddRow($arRes['ID'], $arRes);

    $row->AddInputField('NAME', array('size' => 50));
    $row->AddInputField('SORT', array('size' => 10));
    $row->AddCheckField("MAIN");

    $row->AddViewField('NAME', '<a href="/bitrix/admin/collection_elements.php?COLLECTION_ID=' . $arRes['ID'] . '&lang=' . LANGUAGE_ID . '"><span class="adm-submenu-item-link-icon adm-list-table-icon iblock-section-icon"></span><span class="adm-list-table-link">' . $arRes['NAME'] . '</span></a>');
    $row->AddViewField('MAIN', $arRes['MAIN'] == 'Y' ? 'Да' : 'Нет');

    $row->AddActions(
        array(
            array(
                'ICON' => 'edit',
                'TEXT' => GetMessage('NOTA_COLLECTION_EDIT'),
                'ACTION' => $lAdmin->ActionRedirect('collection_edit.php?ID=' . $arRes['ID'] . '&lang=' . LANGUAGE_ID)
            ),
            array(
                'ICON' => 'delete',
                'TEXT' => GetMessage('NOTA_COLLECTION_DELETE'),
                "ACTION" => "if(confirm('" . GetMessageJS("NOTA_COLLECTION_DELETE_CONFIRM") . "')) " . $lAdmin->ActionDoGroup($arRes['ID'], "delete"),
            )
        )
    );
}


$lAdmin->AddAdminContextMenu(
    array(
        array(
            'TEXT' => Loc::getMessage('NOTA_COLLECTION_ADD'),
            'LINK' => 'collection_edit.php' . '?lang=' . LANGUAGE_ID,
            'TITLE' => Loc::getMessage('NOTA_COLLECTION_ADD'),
            'ICON' => 'btn_new',
        )
    )
);

$lAdmin->AddGroupActionTable(array('delete' => Loc::getMessage('NOTA_COLLECTION_DELETE')));

$lAdmin->CheckListMode();
$APPLICATION->SetTitle(Loc::getMessage('NOTA_COLLECTION_TITLE'));

$map = CollectionTable::getMap();

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';
?>
<form method="GET" action="" name="find_form">
    <input type="hidden" name="filter" value="Y">
    <?
    $oFilter = new CAdminFilter(
        $sTableID . "_filter",
        array(
            "ID",
            $map['LIBRARY_ID']['title'],
            $map['MAIN']['title']
        )
    );

    $oFilter->Begin();
    ?>
    <tr>
        <td><b><? echo GetMessage("NOTA_COLLECTION_NAME") ?></b></td>
        <td><input type="text" name="find_name" value="<?= htmlspecialcharsbx($find_name) ?>" size="40"></td>
    </tr>
    <tr>
        <td>ID:</td>
        <td><input type="text" name="find_id" value="<?= htmlspecialcharsbx($find_id) ?>" size="15"></td>
    </tr>
    <tr>
        <td><?= $map['LIBRARY_ID']['title'] ?>:</td>
        <td><input type="text" name="find_library_id" value="<?= htmlspecialcharsbx($find_library_id) ?>" size="15">
        </td>
    </tr>
    <tr>
        <td><?= $map['MAIN']['title'] ?>:</td>
        <td><input type="checkbox" name="find_main" value="Y" <?= $find_main == 'Y' ? 'checked="checked"' : '' ?>></td>
    </tr>
    <?
    $oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPageParam(), "form" => "find_form"));
    $oFilter->End();
    ?>
</form>
<?
$lAdmin->DisplayList();
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
?>
