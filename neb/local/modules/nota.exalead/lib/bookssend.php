<?
namespace Nota\Exalead;

use \Bitrix\Main\Config\Option,
    Nota\Exalead\BiblioCardTable,
    Nota\Exalead\BiblioAlisTable,
    Nota\Exalead\DateConverter;

/**
 * Отправка книги в Экзалиду
 * 
 * Class BooksSend
 * @package Nota\Exalead
 */
class BooksSend
{
    public static function getList()
    {
        $result = BiblioCardTable::getList(array(
            'select' => array('*', 'LIB_NAME' => 'LIB.Name', 'LIB_ID' => 'LIB.Library'),
            'filter' => array(
                'BX_IS_UPLOAD' => '0'
            )
        ));

        while ($res = $result->Fetch(new DateConverter)) {
            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $res['pdfLink']))
                continue;
            $arResult[] = $res;
        }

        return $arResult;
    }

    public static function send($arFields)
    {
        if (empty($arFields))
            return false;

        trimArr($arFields);

        $url = Option::get("nota.exalead", "book_add_url");
        $login = Option::get("nota.exalead", "book_add_login");
        $password = Option::get("nota.exalead", "book_add_password");
        $key = Option::get("nota.exalead", "book_add_key");

        $upload = $_SERVER['DOCUMENT_ROOT'] . $arFields['pdfLink'];
        if (!file_exists($upload))
            return false;

        $arFields['upload'] = "@" . $upload;
        $arFields['KEY'] = $key;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        if (!empty($login) and !empty($password)) {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $login . ":" . $password);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $arFields);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $result = curl_exec($ch);
        curl_close($ch);

        if (empty($result))
            return false;

        $arResult = json_decode($result, true);
        if (empty($arResult))
            return false;

        return $arResult;
    }

    public static function setInfo($id, $arFields)
    {
        $id = intval($id);

        if (empty($arFields) or empty($id))
            return false;
        if (empty($arFields['IdFromALIS']) or empty($arFields['IdBxcard']))
            return false;

        $arFueldsUpdate = array(
            'EX_IdFromALIS' => $arFields['IdFromALIS'],
            'EX_IdCard' => $arFields['IdExaleadCard'],
            'EX_pdfLink' => $arFields['pdfLink'],
            'BX_IS_UPLOAD' => 1
        );
        return BiblioCardTable::update($id, $arFueldsUpdate);
    }
}