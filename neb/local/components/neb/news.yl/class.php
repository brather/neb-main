<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Web\HttpClient,
    \Bitrix\Main\Text\Encoding,
    \Neb\Main\Helper\MainHelper,
    \Neb\Main\Helper\FileHelper;

class YearLiteratureComponent extends CBitrixComponent
{
    const RSS_FEED_URL = 'https://godliteratury.ru/wp-content/themes/glit-v1/rss.php';

    /**
     * Подготовка параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams["OUT_CHANNEL"] = $arParams["OUT_CHANNEL"] == 'Y';
        $arParams["COUNT"] = intval($arParams["COUNT"]);

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     * Пришлось откзазаться от запроса через CIBlockRSS::GetNewsEx, т.к. сохраняет кеш в БД.
     */
    public function executeComponent()
    {
        if ($this->startResultCache()) {

            $sResult        = $this->getRssFeed();
            $sResult        = $this->getCDataXml($sResult);
            $sResult        = $this->getRss($sResult);
            $this->arResult = $this->elementsLimit($sResult);

            $this->endResultCache();
        }

        $this->includeComponentTemplate();
    }

    /**
     * Получение RSS-фида
     *
     * @return array|bool|mixed|SplFixedArray|string
     */
    private function getRssFeed() {

        $http = new HttpClient(["socketTimeout" => 5]);
        if (!MainHelper::checkSslVerification(self::RSS_FEED_URL)) {
            $http->disableSslVerification();
        }
        $http->setHeader("User-Agent", "BitrixSMRSS");
        $text = $http->get(self::RSS_FEED_URL);

        if ($text)
        {
            $rss_charset = "windows-1251";
            if (preg_match("/<"."\?XML[^>]{1,}encoding=[\"']([^>\"']{1,})[\"'][^>]{0,}\?".">/i", $text, $matches))
            {
                $rss_charset = trim($matches[1]);
            }
            else
            {
                $headers = $http->getHeaders();
                $ct = $headers->get("Content-Type");
                if (preg_match("#charset=([a-zA-Z0-9-]+)#m", $ct, $match))
                    $rss_charset = $match[1];
            }

            $text = preg_replace("/<!DOCTYPE.*?>/i", "", $text);
            $text = preg_replace("/<"."\\?XML.*?\\?".">/i", "", $text);
            $text = Encoding::convertEncoding($text, $rss_charset, SITE_CHARSET);
            $text = self::trimBadTags($text); // экранирование лишних тегов
        }

        return $text;
    }

    /**
     * Преобразует XML RSS-фид в массив CDataXML
     *
     * @param $text
     * @return array
     */
    private function getCDataXml($text) {

        $arRes = [];

        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");
        $objXML = new CDataXML();
        $res = $objXML->LoadString($text);
        if($res !== false) {
            $ar = $objXML->GetArray();
            if (!$this->arParams["OUT_CHANNEL"]) {
                if (
                    is_array($ar) && isset($ar["rss"])
                    && is_array($ar["rss"]) && isset($ar["rss"]["#"])
                    && is_array($ar["rss"]["#"]) && isset($ar["rss"]["#"]["channel"])
                    && is_array($ar["rss"]["#"]["channel"]) && isset($ar["rss"]["#"]["channel"][0])
                    && is_array($ar["rss"]["#"]["channel"][0]) && isset($ar["rss"]["#"]["channel"][0]["#"])
                )
                    $arRes = $ar["rss"]["#"]["channel"][0]["#"];
                else
                    $arRes = array();
            } else {
                if (
                    is_array($ar) && isset($ar["rss"])
                    && is_array($ar["rss"]) && isset($ar["rss"]["#"])
                )
                    $arRes = $ar["rss"]["#"];
                else
                    $arRes = array();
            }

            $arRes["rss_charset"] = strtolower(SITE_CHARSET);
        }

        return $arRes;
    }

    /**
     * Преобразует массив CDataXml в нативный rss-массив
     * Также записывает на диск кеш оптимизированных картинок
     *
     * @param $arCDataXmlRss
     * @return array
     */
    private function getRss($arCDataXmlRss) {

        $obIBlockRSS = new CIBlockRSS();
        $arResult    = $obIBlockRSS->FormatArray($arCDataXmlRss, $this->arParams["OUT_CHANNEL"]);
        foreach ($arResult['item'] as $k => &$arItem) {
            $arItem['enclosure']['local'] = FileHelper::saveTempImage($arItem['enclosure']['url'], '/tmp_year_literature');
        }

        return $arResult;
    }

    /**
     * Ограничивает количество выдаваемых элементов
     *
     * @param $arResult
     * @return mixed
     */
    private function elementsLimit($arResult) {

        if ($this->arParams["COUNT"] > 0)
            while (count($arResult["item"]) > $this->arParams["COUNT"])
                array_pop($arResult["item"]);

        return $arResult;
    }

    /**
     * Получает все теги из строки RSS
     *
     * @param $sRssFeed
     * @return array
     */
    private static function getAllTags($sRssFeed) {

        $arTags = [];

        if (empty($sRssFeed))
            return $arTags;

        preg_match_all("/<[^>]+>/", $sRssFeed, $matches);
        foreach ($matches[0] as $str) {
            $str  = str_replace(['<', '>', '/'], '', $str);
            $sPos = strpos($str, ' ');
            if (false !== $sPos)
                $str = substr($str, 0, $sPos);
            $arTags[$str] = $str;
        }

        return $arTags;
    }

    /**
     * Вырезает все недопустимые теги в строке RSS
     *
     * @param $sRssFeed
     * @return mixed
     */
    private static function trimBadTags($sRssFeed) {

        $arAllowed = ['rss', 'channel', 'title', 'description', 'link', 'language', 'item', 'enclosure', 'guid',
            'pubDate', 'copyright', 'managingEditor', 'webMaster', 'lastBuildDate', 'category', 'generator', 'docs',
            'cloud', 'ttl', 'image', 'rating', 'textInput', 'skipHours', 'skipDays', 'author', 'comments', 'source'];

        $arTags = self::getAllTags($sRssFeed);
        foreach ($arTags as $s) {
            if (!in_array($s, $arAllowed)) {
                $sRssFeed = str_replace(['<'.$s.'>', '</'.$s.'>', '<'.$s.'/>', '<'.$s.' />'], '', $sRssFeed);
            }
        }

        return $sRssFeed;
    }
}