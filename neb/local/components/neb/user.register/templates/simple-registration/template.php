<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $USER;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (intval($arResult['VALUES']['USER_ID']) > 0) {
    $USER->Authorize($arResult['VALUES']['USER_ID']);
    LocalRedirect('/profile/');
}

// сюда перенаправляется Esia в случае отсуствия в ГосУслугах электропочты пользователя
$arEsia = $_SESSION['ESIA_REGISTER'];

?>
<header>
    <div class="container">
        <div class="row">
            <div class="neb-logo-mid">
                <a href="/" title="<?= Loc::getMessage(
                    'NATIONAL_ELECTRONIC_LIBRARY'
                ) ?>">
                    <img src="/local/templates/adaptive/img/logo-mid.png"
                         alt="<?= Loc::getMessage('NATIONAL_ELECTRONIC_LIBRARY') ?>" class="" />
                </a>
            </div>
        </div>
    </div>
</header>

<main>
    <div class="container">
        <div class="login-form">
            <h2 class="login-form__title"><?= Loc::getMessage('AUTH_REGISTER'); ?></h2>

            <p class="text-center"
               style="display: none;"><?= Loc::getMessage('FULL_ACCESS_REGISTER') ?></p>

               <?/*details>
                    <summary>$arResult[]</summary>
                    <pre>
                        <?php print_r($arResult);?>
                    </pre>
                </details*/?>

            <? if (!empty($arEsia)): ?>
                <div class="social-auth">
                    <h3 class="text-centered">
                        <?= Loc::getMessage('SOCIAL_REGISTER_ESIA') ?>
                    </h3>
                </div>
            <? else: ?>
                <div class="social-auth">
                    <h3 class="text-centered"><?= Loc::getMessage('SOCIAL_REGISTER') ?></h3>
                    <ul class="social-auth__list clearfix">
                        <? if (!empty($arResult['AUTH_SERVICES']['Esia'])): ?>
                            <li class="social-auth__list-kind">
                                <a href="#"
                                   class="social-auth__list-link social-auth__list-link--gu socservice-auth-with-alert"
                                   data-service-popup="/WebServices/Esia.php?act=do_login"
                                   title="<?= Loc::getMessage('AUTH_GOSUSLUGI') ?>">
                                </a>
                            </li>
                        <? endif; ?>
                        <li class="social-auth__list-kind">
                            <a href="#"
                               class="social-auth__list-link social-auth__list-link--rgb socservice-auth-with-alert"
                               data-service-popup="/auth/rgb.php?redirect=1&show_errors=1"
                               title="<?= Loc::getMessage('AUTH_RGB') ?>">
                            </a>
                        </li>
                        <? if (!empty($arResult['AUTH_SERVICES']['VKontakte'])): ?>
                            <li class="social-auth__list-kind">
                                <a href="#"
                                    data-sm-confirm
                                    data-onclick="<?= $arResult['AUTH_SERVICES']['VKontakte']['ONCLICK'] ?>"
                                    class="social-auth__list-link social-auth__list-link--vk"
                                    title="<?= Loc::getMessage('AUTH_VKONTAKTE') ?>">
                                </a>
                            </li>
                        <? endif; ?>
                        <? if (!empty($arResult['AUTH_SERVICES']['Odnoklassniki'])): ?>
                            <li class="social-auth__list-kind">
                                <a href="#"
                                    data-sm-confirm
                                    data-onclick="<?= $arResult['AUTH_SERVICES']['Odnoklassniki']['ONCLICK'] ?>"
                                    class="social-auth__list-link social-auth__list-link--ok"
                                    title="<?= Loc::getMessage('AUTH_ODNOKLASSINIKI') ?>">
                                </a>
                            </li>
                        <? endif; ?>
                        <? if (!empty($arResult['AUTH_SERVICES']['Facebook'])): ?>
                            <li class="social-auth__list-kind">
                                <a href="#"
                                    data-sm-confirm
                                    data-onclick="<?= $arResult['AUTH_SERVICES']['Facebook']['ONCLICK'] ?>"
                                    class="social-auth__list-link social-auth__list-link--fb"
                                    title="<?= Loc::getMessage('AUTH_FACEBOOK') ?>">
                                </a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
            <? endif; ?>

            <div class="error_container">
                <?
                if (count($arResult["ERRORS"]) > 0) {
                    foreach ($arResult["ERRORS"] as $key => $error) {
                        if (intval($key) == 0 && $key !== 0) {
                            $arResult["ERRORS"][$key] = str_replace(
                                "#FIELD_NAME#", "&quot;" . GetMessage(
                                    "REGISTER_FIELD_" . $key
                                ) . "&quot;", $error
                            );
                        }
                    }

                    ShowError(
                        implode("<br />", $arResult["ERRORS"])
                        . '<br /><br />'
                    );

                } elseif ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"
                    && (int)$arResult['VALUES']['USER_ID'] > 0
                ) {
                    ?>
                    <p><?= GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['ERROR_MESSAGE']['MESSAGE'])
                    || (is_string($arResult['ERROR_MESSAGE'])
                        && !empty($arResult['ERROR_MESSAGE']))
                ) {
                    if (is_array($arResult['ERROR_MESSAGE'])
                        && !empty($arResult['ERROR_MESSAGE']['MESSAGE'])
                    ) {
                        $arResult['ERROR_MESSAGE']
                            = $arResult['ERROR_MESSAGE']['MESSAGE'];
                    }
                    ShowError($arResult['ERROR_MESSAGE']);
                }
                ?>
            </div>

            <form id="shortregform" action="<?= POST_FORM_ACTION_URI ?>"
                  method="post" enctype="application/x-www-form-urlencoded">
                <? if ($arResult["BACKURL"] <> ''): ?>
                    <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
                <? endif; ?>

                <input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value="" />

                <div class="form-group">
                    <label for="name"
                           class="login-form__label"><?= Loc::getMessage('NAME');
                        ?></label>
                    <div class="relative">
                        <input type="text"
                           class="form-control login-form__input" id="name"
                           name="REGISTER[NAME]" data-minlength="2"
                           data-maxlength="50"
                           data-virtual-keyboard
                           <? if (!empty($arEsia['NAME'])): ?>
                               value="<?= $arEsia['NAME'] ?>" disabled="disabled"
                           <? else: ?>
                               value="<?= $arResult["VALUES"]['NAME'] ?>"
                           <? endif; ?>
                        />
                    </div>
                </div>
                <div class="form-group">
                    <label for="email"
                           class="login-form__label">E-mail</label>
                    <div class="relative">
                        <input type="email"
                           class="form-control login-form__input" id="email"
                           value="<?= $arResult["VALUES"]['EMAIL'] ?>"
                           name="REGISTER[EMAIL]" data-minlength="2"
                           data-virtual-keyboard
                           data-maxlength="50" required />
                    </div>
                </div>
                <div class="form-group">
                    <label for="enterpassword"
                           class="login-form__label"><?= Loc::getMessage('PASSWORD');
                        ?></label>
                    <div class="relative">
                        <input type="password"
                           class="form-control login-form__input"
                           id="enterpassword" data-required="true"
                           value="<?= $arResult["VALUES"]['PASSWORD'] ?>"
                           data-virtual-keyboard
                           name="REGISTER[PASSWORD]" required />
                    </div>
                    <!-- pattern="[A-Za-z0-9]{6,}" -->
                </div>
                <div class="form-group">
                    <label for="confirmpassword"
                           class="login-form__label"><?= Loc::getMessage('CONFIRM_PASSWORD');
                        ?></label>
                    <div class="relative">
                        <input type="password"
                           class="form-control login-form__input"
                           id="confirmpassword" data-required="true"
                           value="<?= $arResult["VALUES"]['CONFIRM_PASSWORD'] ?>"
                           data-virtual-keyboard
                           name="REGISTER[CONFIRM_PASSWORD]" required />
                    </div>
                </div>
                <br />

                <div class="row">
                    <div
                        class="col-md-6 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-12">
                        <button type="submit"
                                class="btn btn-primary btn-lg col-md-12 col-sm-12 col-xs-12 login-form-btn"
                                name="register_submit_button"
                                value="register"><?= Loc::getMessage('MAIN_REGISTER_REGISTER');
                            ?></button>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <button type="button"
                                class="btn btn-default btn-lg col-md-12 col-sm-12 col-xs-12 login-form-btn"
                                onclick="document.location.href= '<?=$arParams['LINK_REFERER']?>'"><?= Loc::getMessage('AUTH_CANCEL');
                            ?></button>
                    </div>
                </div>
                <input type="hidden" name="form-type" value="register.form" />
                <input type="hidden" name="action" value="save" />
            </form>
            <script>
                $(function () {
                    jQuery.validator.addMethod("latin", function (value, element) {
                        return this.optional(element) || /^[A-Za-z0-9\w]{6,20}/.test(value);
                    }, "Используйте только латинские буквы и цифры");

                    $('#shortregform').validate({
                        rules: {
                            "REGISTER[EMAIL]": {
                                required: true,
                                email: true
                            },
                            "REGISTER[PASSWORD]": {
                                required: true,
                                minlength: 6,
                                latin: true
                            },
                            "REGISTER[CONFIRM_PASSWORD]": {
                                required: true,
                                minlength: 6,
                                latin: true,
                                equalTo: "#enterpassword"
                            }
                        },
                        messages: {
                            "REGISTER[EMAIL]": {
                                required: "Введите адрес электронной почты",
                                email: "В адресе электронной почты обнаружена ошибка"
                            },
                            "REGISTER[PASSWORD]": {
                                required: "Введите пароль",
                                minlength: jQuery.validator.format("не менее {0} символов")
                            },
                            "REGISTER[CONFIRM_PASSWORD]": {
                                required: "Повторите пароль",
                                minlength: jQuery.validator.format("не менее {0} символов"),
                                equalTo: "Пароль должен совпасть с паролем, введённым выше"

                            }
                        },
                        errorPlacement: function (error, element) {
                            error.appendTo(element.parent());
                            $(element).closest('.form-group').toggleClass('has-error', true);
                        },
                        /* specifying a submitHandler prevents the default submit, good for the demo
                         submitHandler: function() {
                             alert("submitted!");
                         },
                         закомментировал, т.к. вызывает глюк, требующий повторного нажатия кнопки "Зарегистрироваться"
                         set this class to error-labels to indicate valid fields
                        success: function (label) {
                            label.html("&nbsp;").addClass("resolved");
                        },*/
                        highlight: function (element, errorClass, validClass) {
                            $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                            $(element).parent().find("." + errorClass).removeClass("resolved");
                        },
                        unhighlight: function (element, errorClass, validClass) {
                            $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
                        }
                    });
                });
            </script>
        </div>
    </div>
</main>