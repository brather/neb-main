<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?= $arResult['BACK_TO_LIST'] ?>
<?$APPLICATION->IncludeComponent(
    "neb:faq.item.add",
    "",
    array(
        "IBLOCK_ID" => $arResult['IBLOCK_ID'],
        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
        "MIN_COUNT_SYMBOL" => $arParams['MIN_COUNT_SYMBOL'],
        "URL_PAGE" => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['add_item']
    )
);?>