<?php

$MESS['CONVERTED_TO_ALL'] = 'Вы оставили строку поиска пустой, и мы вывели все произведения, удовлетворяющие прочим фильтрам поиска.';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'Найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'Найден';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'Найдено';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'] = 'наиболее подходящих условиям поиска результатов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'] = 'наиболее подходящих условиям поиска результата';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'] = 'наиболее подходящий условиям поиска результат';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_RESULT_5'] = 'результатов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_RESULT_1'] = 'результата';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_RESULT_2'] = 'результат';

$MESS['LIB_SEARCH_PAGE_FULL_SEARCH'] = "Поиск дополнительно произведен по текстам изданий";
$MESS['LIB_SEARCH_LINK_SIMPLE_SEARCH'] = "искать в каталогах печатных книг";
$MESS['MORE_RESULTS'] = 'Больше результатов по вашему запросу можно найти на сайте';
$MESS['ARCHIVE_OF_THE_RUSSIAN_FEDERATION'] = 'Государственного архива Российской Федерации';
$MESS['LAYOUT_CHANGED'] = 'Исправлена раскладка клавиатуры в';

$MESS['SEARCH_STRICT'] = 'Показать результаты поиска <a href="#LINK#">по точному совпадению в текстах изданий</a>';
$MESS['SEARCH_STRICT_RESULTS'] = 'поиска по точному совпадению';

$MESS['SEARCH_BY_ORIGINAL_QUERY'] = 'Искать по оригинальному
                        запросу'; //Искать по оригинальному запросу
$MESS['SEARCH_IN_RUSSIAN'] = 'Искать на русском';
$MESS['SEARCH_IN_ENGLISH'] = 'Искать на английском';
$MESS['SEARCH_IN_DEUTSCH'] = 'Искать на немецком';
$MESS['SAVE_QUERY'] = 'Сохранить поисковый запрос';
$MESS['SAVED'] = 'Сохранено';
$MESS['SHOW_MORE'] = '(показать еще)';