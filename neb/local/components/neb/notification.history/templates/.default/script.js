$(function () {
    $('#libraries').selectpicker({liveSearch: true, size:10, dropupAuto: false});
    $('#senders').selectpicker({liveSearch: true, size:10, dropupAuto: false});
    
    $(document).on('click', 'input.send-subscribe', function(){
        this.disabled = true;
        $.ajax({
            url: '/local/components/neb/notification.form/ajax.php',
            method: "POST",
            dataType: "json",
            data: {
                action: 'send',
                id: $(this).attr('data-id')
            },
            success: function(res){
                if (res.success == true ){
                    window.location.reload();
                }
            },
            error: function(st, msg){
                //console.error(st, msg);
                alert('Error!\n' + st + '\n' + msg);
            }
        });
    });
});