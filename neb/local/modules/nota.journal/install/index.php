<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (class_exists('nota_journal')) {
    return;
}

class nota_journal extends CModule
{
    public $MODULE_ID = 'nota.journal';
    public $MODULE_VERSION = '0.0.1';
    public $MODULE_VERSION_DATE = '2014-12-01 00:00:00';
    public $MODULE_NAME = 'Journal';
    public $MODULE_DESCRIPTION = 'Модуль для работы с журналом НЭБ';
    public $MODULE_GROUP_RIGHTS = 'N';
    public $PARTNER_NAME = "Notamedia";
    public $PARTNER_URI = "http://www.notamedia.ru";

    public function DoInstall()
    {
        global $APPLICATION, $DB;

        /*$errors = $DB->RunSQLBatch(__DIR__ . '/db/' . strtolower($DB->type) . '/install.sql');
        if ($errors) {
            $APPLICATION->ThrowException(implode('<br>', (array)$errors));
            return false;
        }*/

        RegisterModule($this->MODULE_ID);

        RegisterModuleDependences("forum", "onAfterMessageAdd", "nota.journal", 'Nota\Journal\Events\Forum', "onAdd");
        RegisterModuleDependences("forum", "onAfterMessageUpdate", "nota.journal", 'Nota\Journal\Events\Forum', "onEdit");
        RegisterModuleDependences("forum", "onAfterMessageDelete", "nota.journal", 'Nota\Journal\Events\Forum', "onDelete");
        //новости библиотеки
        RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "nota.journal", 'Nota\Journal\Events\Libnews', "onAdd");
        RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "nota.journal", 'Nota\Journal\Events\Libnews', "onEdit");
        RegisterModuleDependences("iblock", "OnAfterIBlockElementDelete", "nota.journal", 'Nota\Journal\Events\Libnews', "onDelete");
        //описание библиотеки
        RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "nota.journal", 'Nota\Journal\Events\Libcontent', "onAdd");
        RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "nota.journal", 'Nota\Journal\Events\Libcontent', "onEdit");
        RegisterModuleDependences("iblock", "OnAfterIBlockElementDelete", "nota.journal", 'Nota\Journal\Events\Libcontent', "onDelete");
        //коллекции
        RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "nota.journal", 'Nota\Journal\Events\Collection', "onAdd");
        RegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "nota.journal", 'Nota\Journal\Events\Collection', "onEdit");
        RegisterModuleDependences("iblock", "OnAfterIBlockElementDelete", "nota.journal", 'Nota\Journal\Events\Collection', "onDelete");
    }

    public function DoUninstall()
    {
        global $APPLICATION, $DB;

        /*$errors = $DB->RunSQLBatch(__DIR__ . '/db/' . strtolower($DB->type) . '/uninstall.sql');
        if ($errors) {
            $APPLICATION->ThrowException(implode('<br>', (array)$errors));
            return false;
        }*/

        UnRegisterModuleDependences("forum", "onAfterMessageAdd", "nota.journal", 'Nota\Journal\Events\Forum', "onAdd");
        UnRegisterModuleDependences("forum", "onAfterMessageUpdate", "nota.journal", 'Nota\Journal\Events\Forum', "onEdit");
        UnRegisterModuleDependences("forum", "onAfterMessageDelete", "nota.journal", 'Nota\Journal\Events\Forum', "onDelete");
        //новости библиотеки
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "nota.journal", 'Nota\Journal\Events\Libnews', "onAdd");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "nota.journal", 'Nota\Journal\Events\Libnews', "onEdit");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementDelete", "nota.journal", 'Nota\Journal\Events\Libnews', "onDelete");
        //описание библиотеки
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "nota.journal", 'Nota\Journal\Events\Libcontent', "onAdd");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "nota.journal", 'Nota\Journal\Events\Libcontent', "onEdit");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementDelete", "nota.journal", 'Nota\Journal\Events\Libcontent', "onDelete");
        //коллекции
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "nota.journal", 'Nota\Journal\Events\Collection', "onAdd");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementUpdate", "nota.journal", 'Nota\Journal\Events\Collection', "onEdit");
        UnRegisterModuleDependences("iblock", "OnAfterIBlockElementDelete", "nota.journal", 'Nota\Journal\Events\Collection', "onDelete");

        UnRegisterModule($this->MODULE_ID);
    }
}

?>