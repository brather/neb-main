<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

\Bitrix\Main\Loader::IncludeModule("notaext");

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$APPLICATION->SetTitle(GetMessage("TITLE"));
require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

echo __FILE__;
?>
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="list-table">
    <tr class="heading">
        <td width="60%"><b><?= GetMessage("MOD_NAME") ?></b></td>
        <td><b><?= GetMessage("MOD_VERSION") ?></b></td>
        <td><b><?= GetMessage("MOD_DATE_UPDATE") ?></b></td>
        <td><b><?= GetMessage("MOD_SETUP") ?></b></td>
        <td><b><?= GetMessage("MOD_ACTION") ?></b></td>
    </tr>

</table>

<?
require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin.php");
?>
