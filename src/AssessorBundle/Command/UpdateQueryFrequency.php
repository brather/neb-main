<?php
/**
 * User: agolodkov
 * Date: 12.07.2016
 * Time: 10:52
 */

namespace AssessorBundle\Command;


use AssessorBundle\Entity\TblQueryFrequency;
use GuzzleHttp\Client;
use LbcBundle\Traits\LbcMigrationsTrait;
use Neb\Main\Stat\Tracker;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateQueryFrequency extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('assessor:update-query-frequency')
            ->setDescription('Update query frequency');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);

        $this->getContainer()->get('bitrix_helper')->requireProlog();
        while (ob_get_level()) {
            ob_end_clean();
        }
        error_reporting(E_ERROR);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '1024M');

        $em = $this->getContainer()->get('doctrine')->getManager('neb.library');
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $count = 0;
        $saveBatch = 1000;
        $offset = 0;
        do {
            $report = $this->getReport(
                'neb_search_queries_frequency_all',
                null,
                null,
                [
                    'limit'  => $saveBatch,
                    'offset' => $offset
                ]
            );
            $offset += $saveBatch;
            $queries = [];
            $queryMap = [];
            foreach ($report['result'] as $item) {
                $item['data'] = trim($item['data']);
                $item['data'] = mb_strtolower($item['data']);
                $item['data'] = mb_substr($item['data'], 0, 250);
                if (empty($item['data'])) {
                    continue;
                }
                $md5 = md5($item['data']);
                $queries[$md5] = $item['data'];
                $queryMap[$md5] = (integer)$item['value'];
            }
            $count += count($queries);
            $entities = $em
                ->getRepository('AssessorBundle:TblQueryFrequency')
                ->findBy(['md5' => array_keys($queryMap)]);
            foreach ($entities as $entity) {
                $md5 = $entity->getMd5();
                if ($queryMap[$md5]) {
                    $entity->setFrequency($queryMap[$md5]);
                    $em->merge($entity);
                    unset($queries[$md5]);
                }
            }
            $em->flush();
            $em->clear();
            foreach ($queries as $md5 => $query) {
                $entity = new TblQueryFrequency();
                $entity->setQuery($query);
                $entity->setFrequency($queryMap[$md5]);
                $em->persist($entity);
            }
            $em->flush();
            $em->clear();
            echo "Save $count queries", ' Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb', "\r";
        } while (!empty($report['result']));
        echo PHP_EOL;


        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }

    public function getReport($name, $dateFrom = null, $dateTo = null, $query = [])
    {
        $query['report'] = $name;
        if ($dateFrom) {
            $query['dateFrom'] = date('Y-m-d', $dateFrom);
        }
        if ($dateTo) {
            $query['dateTo'] = date('Y-m-d', $dateTo);
        }
        $query = http_build_query($query);
        $bitrix = $this->getContainer()->get('bitrix_helper');
        $token = $bitrix->getOption('neb.main', 'stats_reader_token');
        $host = $bitrix->getOption('neb.main', 'stats_host');
        $url = $host . 'reports?access-token=' . $token . '&' . $query;
        $client = new Client();
        $response = $client->get($url);
        gc_collect_cycles();

        return json_decode($response->getBody(), true);
    }
}
