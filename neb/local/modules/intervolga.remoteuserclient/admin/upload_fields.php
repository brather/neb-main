<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use \Bitrix\Main\Localization\Loc;
global $APPLICATION;

Loc::loadMessages(__FILE__);
\Bitrix\Main\Loader::includeModule("intervolga.remoteuserclient");
$APPLICATION->SetTitle(Loc::getMessage("intervolga.remoteuserclient.PAGE_TITLE"));

CJSCore::Init();

$arResult = array();
if (\Intervolga\RemoteUserClient\Rights::canWrite())
{
	global $USER_FIELD_MANAGER;

	$arResult["USER_FIELDS"] = $USER_FIELD_MANAGER->GetUserFields("USER", 0, "ru");
	foreach ($arResult["USER_FIELDS"] as $i => $arUserField)
	{
		if (in_array($arUserField["FIELD_NAME"], array("UF_IV_UPLOAD_DATE", "UF_IV_REMOTE_ID", "UF_IV_UPLOAD_PREVENT")))
		{
			unset($arResult["USER_FIELDS"][$i]);
		}
	}

	if (check_bitrix_sessid() && $_POST["submit"])
	{
		$iOk = 0;
		$iFail = 0;
		foreach ($arResult["USER_FIELDS"] as $arUserUserField)
		{
			if (in_array($arUserUserField["FIELD_NAME"], $_POST["FIELDS"]))
			{
				unset($arUserUserField["ID"]);
				$_SESSION["arUploadedUserUserFields"][$arUserUserField["FIELD_NAME"]] = \Intervolga\RemoteUserClient\User::uploadUserUserField($arUserUserField);
				if ($_SESSION["arUploadedUserUserFields"][$arUserUserField["FIELD_NAME"]])
				{
					$iOk++;
				}
				else
				{
					$iFail++;
				}
			}
		}
		LocalRedirect($APPLICATION->getCurPageParam(
			http_build_query(array(
				"OK" => "Y",
				"ERROR" => $iFail,
				"SUCCESS" => $iOk,
			)),
			array("OK", "ERROR", "SUCCESS")
		));
	}
	if ($_GET["OK"] == "Y" && $_SESSION["arUploadedUserUserFields"])
	{
		foreach ($_SESSION["arUploadedUserUserFields"] as $sUserFieldCode => $iRemoteUserUserFieldId)
		{
			if ($iRemoteUserUserFieldId)
			{
				$arResult["UPLOADED"]["OK"][] = Loc::getMessage("intervolga.remoteuserclient.USER_USER_FIELDS_UPLOADED", array(
					"#THIS_SITE_USER_USER_FIELDS#" => $sUserFieldCode,
					"#REMOTE_SITE_USER_USER_FIELDS#" => $iRemoteUserUserFieldId,
				));
			}
			else
			{
				$arResult["UPLOADED"]["ERROR"][] = Loc::getMessage("intervolga.remoteuserclient.USER_USER_FIELDS_NOT_UPLOADED", array(
					"#THIS_SITE_USER_USER_FIELDS#" => $sUserFieldCode,
				));
			}
		}
		if ($arResult["UPLOADED"]["OK"])
		{
			$iTotal = count($arResult["UPLOADED"]["OK"]);
			array_unshift($arResult["UPLOADED"]["OK"], "");
			array_unshift($arResult["UPLOADED"]["OK"], Loc::getMessage("intervolga.remoteuserclient.USER_USER_FIELDS_UPLOADED_TOTAL", array("#TOTAL#" => $iTotal)));
		}
		if ($arResult["UPLOADED"]["ERROR"])
		{
			$iTotal = count($arResult["UPLOADED"]["ERROR"]);
			array_unshift($arResult["UPLOADED"]["ERROR"], "");
			array_unshift($arResult["UPLOADED"]["ERROR"], Loc::getMessage("intervolga.remoteuserclient.USER_USER_FIELDS_NOT_UPLOADED_TOTAL", array("#TOTAL#" => $iTotal)));
		}
		unset($_SESSION["arUploadedUserUserFields"]);
	}
	elseif ($_GET["OK"] == "Y" && !$_SESSION["arUploadedUserUserFields"])
	{
		if ($_GET["SUCCESS"])
		{
			$arResult["UPLOADED"]["OK"][] = Loc::getMessage("intervolga.remoteuserclient.USER_USER_FIELDS_UPLOADED_TOTAL", array("#TOTAL#" => intval($_GET["SUCCESS"])));
		}
		if ($_GET["ERROR"])
		{
			$arResult["UPLOADED"]["ERROR"][] = Loc::getMessage("intervolga.remoteuserclient.USER_USER_FIELDS_NOT_UPLOADED_TOTAL", array("#TOTAL#" => intval($_GET["ERROR"])));
		}
		if (!$_GET["SUCCESS"])
		{
			$arResult["UPLOADED"]["ERROR"][] = Loc::getMessage("intervolga.remoteuserclient.NONE_UPLOADED");
		}
	}
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<? if (\Intervolga\RemoteUserClient\Rights::canWrite()): ?>
	<? if ($arResult["USER_FIELDS"]): ?>
		<? if ($_GET["OK"] == "Y"): ?>
			<? if ($arResult["ERRORS"]): ?>
				<?CAdminMessage::ShowMessage(implode("\n", $arResult["ERRORS"])) ?>
			<? elseif ($arResult["UPLOADED"]): ?>
				<? if ($arResult["UPLOADED"]["OK"]): ?>
					<?CAdminMessage::ShowNote(implode("\n", $arResult["UPLOADED"]["OK"])) ?>
				<? endif ?>
				<? if ($arResult["UPLOADED"]["ERROR"]): ?>
					<?CAdminMessage::ShowMessage(implode("\n", $arResult["UPLOADED"]["ERROR"])) ?>
				<? endif ?>
			<? endif ?>
			<a href="/bitrix/admin/intervolga.remoteuserclient_upload_fields.php"><?=Loc::getMessage("intervolga.remoteuserclient.AGAIN")?></a>
			<? if (!$arResult["UPLOADED"]["ERROR"]): ?>
				<br/><a href="/bitrix/admin/intervolga.remoteuserclient_upload.php"><?=Loc::getMessage("intervolga.remoteuserclient.UPLOAD_USERS_NOW")?></a>
			<? endif ?>
		<? else: ?>
			<form method="post" action="<?=$APPLICATION->GetCurPageParam("", array("OK", "ERROR", "SUCCESS"))?>">
				<?=bitrix_sessid_post()?>
				<button class="adm-btn" type="button" style="padding: 5px 15px !important;" id="checkAll"><?=Loc::getMessage("intervolga.remoteuserclient.ALL")?></button>
				<button class="adm-btn" type="button" style="padding: 5px 15px !important;" id="checkNone"><?=Loc::getMessage("intervolga.remoteuserclient.NONE")?></button>
				<script type="text/javascript">
					BX(function() {
						function changeChecked(bChecked) {
							var arCheckboxes = BX.findChildren(document.body, {
								className: "uploadUserField"
							}, true);
							for (var i in arCheckboxes) {
								BX.adjust(BX(arCheckboxes[i]), {props: {checked: bChecked}});
							}
						}
						BX.bind(BX("checkAll"), "click", function() {
							changeChecked(true);
						});
						BX.bind(BX("checkNone"), "click", function() {
							changeChecked(false);
						});
					});
				</script>
				<ol>
					<? foreach ($arResult["USER_FIELDS"] as $arUserUserField): ?>
						<li>
							<label>
								<input class="uploadUserField" type="checkbox" name="FIELDS[]" value="<?=$arUserUserField["FIELD_NAME"]?>" <? if (!isset($_POST["FIELDS"]) || in_array($arUserUserField["FIELD_NAME"], $_POST["FIELDS"])): ?>checked="checked" <? endif ?>/>
								<? if ($arUserUserField["EDIT_FORM_LABEL"]): ?>
									[<?=$arUserUserField["FIELD_NAME"]?>] <b><?=$arUserUserField["EDIT_FORM_LABEL"]?></b>
								<? else: ?>
									<?=$arUserUserField["FIELD_NAME"]?>
								<? endif ?>
							</label>
						</li>
					<? endforeach ?>
				</ol>
				<div>
					<input type="submit" class="adm-btn-save" name="submit" value="Выгрузить"/>
				</div>
			</form>
		<? endif ?>
	<? else: ?>
		<?CAdminMessage::ShowNote(Loc::getMessage("intervolga.remoteuserclient.NO_UF"))?>
	<? endif ?>
<? else: ?>
	<?CAdminMessage::ShowMessage(Loc::getMessage("intervolga.remoteuserclient.NO_ACCESS"))?>
<? endif ?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");