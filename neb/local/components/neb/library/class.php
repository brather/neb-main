<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\NotaExt\Iblock\Element;

/**
 * Class LibraryComponent
 */
class LibraryComponent extends CBitrixComponent
{
    private $componentPage;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arDefaultUrlTemplates404 = array(
            "list" => "/",
            "detail" => "#CODE#/"
        );

        $arDefaultVariableAliases404 = array();

        $arDefaultVariableAliases = array();

        $arComponentVariables = array("CODE");

        $SEF_FOLDER = "";
        $arUrlTemplates = array();

        if ($arParams["SEF_MODE"] == "Y") {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

            $componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

            if (StrLen($componentPage) <= 0) $componentPage = "list";

            CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

            $SEF_FOLDER = $arParams["SEF_FOLDER"];

        } else {

            $arVariables = array();

            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
            CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

            $componentPage = "";
            if (IntVal($arVariables["ELEMENT_ID"]) > 0) {
                $componentPage = "detail";
            } else {
                $componentPage = "list";
            }
        }

        $this->componentPage = $componentPage;

        $this->arResult = array(
            "FOLDER" => $SEF_FOLDER,
            "URL_TEMPLATES" => $arUrlTemplates,
            "VARIABLES" => $arVariables,
            "ALIASES" => $arVariableAliases
        );

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        //идентификатор библиотеки по коду
        $arLibrary = Element::getList(
            array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'CODE' => $this->arResult['VARIABLES']['CODE']),
            1,
            array('ID'),
            array(),
            array(),
            false,
            true
        );

        if ($arLibrary['ITEM']["ID"] > 0) {

            $this->arResult['VARIABLES']['ID'] = $arLibrary['ITEM']['ID'];
            $this->arResult['LIBRARY'] = $arLibrary['ITEM'];

            $this->includeComponentTemplate($this->componentPage);
        }
        // Библиотека не найдена
        else {
            @define("ERROR_404", "Y");
            CHTTP::SetStatus("404 Not Found");
            require_once $_SERVER['DOCUMENT_ROOT'] . '/404.php';
        }
    }
}