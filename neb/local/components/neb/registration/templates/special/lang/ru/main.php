<?php
$MESS['SPECIAL_REG_TITLE'] = 'регистрация на портале';
$MESS['SPECIAL_REG_SELECT'] = 'Выберите способ регистрации';
$MESS['SPECIAL_REG_SIMPLE'] = 'Упрощенная регистрация';
$MESS['SPECIAL_REG_FULL'] = 'Полная регистрация';
$MESS['SPECIAL_REG_RGB'] = 'Полная регистрация для читателей РГБ';
$MESS['SPECIAL_REG_STANDART'] = 'Полная регистрация для обычных пользователей';
$MESS['SPECIAL_REG_ESIA'] = 'Регистрация через ЕСИА';
$MESS['SPECIAL_REG_NEXT'] = 'Далее';
