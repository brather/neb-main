<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? $APPLICATION->SetTitle(""); ?>

<div class="container v-padding">
    <div class="button-bar" role="toolbar">
    	<a href="<?=$APPLICATION->GetCurDir()?>" class="btn btn-default">Вернуться на главную</a>
    </div>
    <form>
	    <dl class="form-dl">
	    	<dt><label class="for-lg" for="searchrub" aria-hidden="true" id="searchrublabel">Раздел для поиска</label></dt>
	    	<dd>
	    		<select class="form-control input-lg" aria-labelledby="searchrublabel" id="searchrub">
	    			<option>Библиотеки</option>
	    			<option selected>Музеи</option>
	    			<option>Архивы</option>
	    		</select>
	    	</dd>
	    	<dt><label class="for-lg" for="searchtype" aria-hidden="true" id="searchtypelabel">Вид поиска</label></dt>
	    	<dd>
	    		<select class="form-control input-lg" id="searchtype" aria-labelledby="searchtypelabel">
	    			<option>Одной строкой</option>
	    			<option selected>Расширенный поиск</option>
	    		</select>
	    	</dd>
	    	<dt><label for="author" id="authorlabel" class="for-lg">Автор</label></dt>
	    	<dd>
	    		<input tabindex="2" type="text" class="form-control input-lg" id="author" aria-labelledby="authorlabel" data-placeholder="Дмитриев Николай"/>
	    	</dd>
	    	<dt><label for="title" class="for-lg" id="titlelabel">Название</label></dt>
	    	<dd>
	    		<input tabindex="1" type="text" class="form-control input-lg" id="title" aria-labelledby="titlelabel" data-placeholder="Портрет императора Александра III"/>
	    	</dd>
	    	<dt><label class="for-lg" aria-hidden="true">Дата публикации</label></dt>
	    	<dd>
	    		<label class="for-lg" for="pubfrom" id="pubfromlabel">
	    			<span class="screen-hide">Дата публикации, </span>от<span class="screen-hide">,</span>
	    		</label>
	    		<input tabindex="3" type="text" class="form-control input-lg" id="pubfrom" style="display: inline-block; width: 5em;" value="900" aria-labelledby="pubfromlabel"/>
	    		<label class="for-lg" for="pubto" id="pubtolabel">
	    			<span class="screen-hide">Дата публикации, </span>до<span class="screen-hide">,</span>
	    		</label>
	    		<input tabindex="4" type="text" class="form-control input-lg" id="pubto" style="display: inline-block; width: 5em;" value="2016" aria-labelledby="pubtolabel"/>
	    		<span class="instead-input-lg" style="margin-left: 1ex;">года</span>
	    	</dd>
	    	<dt><label for="museumfor" id="museumforlabel" class="for-lg" aria-hidden="true">Музей</label></dt>
	    	<dd>
	    		<input tabindex="5" type="text" class="form-control input-lg" id="museumfor" data-labelledby="museumforlabel" data-placeholder="Государственный исторический музей"/>
	    	</dd>
	    	<dt><label for="details" id="detailslabel" aria-hidden="true" class="for-lg one-line-height">Материал, техника исполнения</label></dt>
	    	<dd>
	    		<input tabindex="6" type="text" class="form-control input-lg" id="details" data-labelledby="detailslabel" data-placeholder="Холста, масло"/>
	    	</dd>
	    	<dt><label for="invernumber" id="invernumberlabel" aria-hidden="true" class="for-lg">Инвентарный номер</label></dt>
	    	<dd>
	    		<input tabindex="7" type="text" class="form-control input-lg" id="invernumber" aria-labelledby="invernumberlabel" data-placeholder="И I 5964"/>
	    	</dd>
	    </dl>
	    <div class="button-bar" role="toolbar">
	    	<button class="btn btn-primary">Найти</button><br/>
	    	<button class="btn btn-default" type="reset">Очистить поиск</button>
	    </div>
	</form>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>