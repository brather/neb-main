
<?
$pagename='';
?>
<? include("header_main_auth.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			
		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<div class="b-book_bg">
	<section class="innersection innerwrapper clearfix">
		<div class="b-bookpopup b-bookpage" >
			<div class="b-bookpopup_in bbox">

				<div class="b-onebookinfo iblock">
					<div class="b-bookhover">
						<div class="meta"><div class="b-hint rel">Добавить в Мою библиотеку</div>
						<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
						<span class="b-autor"><a href="#" class="lite">Станислав Лем</a></span>
						<div class="b-bookhover_tit black">Сумма технологии</div>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>
					</div>		
					<div class="clearfix b-digitalaction">
						<a href="ajax_addcomment.php" data-posat="left top" data-posmy="left+288 top-270" data-width="375"  class="formbtn grad popup_opener ajax_opener noclose">Заявка на оцифровку</a>	
					</div>				
					<div class="rel js_description">
						<ul class="b-resultbook-info">
							<li><span>Количество страниц: </span> 427 </li>
							<li><span>Год публикации:</span> <a href="#">1964</a></li>
							<li><span>Издательство:</span> <a href="#">Азбука</a></li>
							<li><a href="#" class="b-fullinfo js_descr_link">полная информация о книге</a></li>
						</ul>
						<div class="b-infobox rel b-infoboxdescr"  data-link="descr">
							<a href="#" class="close"></a>							
							<div class="b-infoboxitem">
								<span class="tit iblock">Автор: </span>
								<span class="iblock val">Горшков, Евгений Андреевич</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Неконтролируемое имя: </span>
								<span class="iblock val">Институт психологии Российской академии наук</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие: </span>
								<span class="iblock val">Становление социальной психологии США : автореферат дис. ... кандидата психологических наук : 19.00.01</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Выходные данные: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Физическое описание: </span>
								<span class="iblock val">23 с.</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">Общая психология, психология личности, история психологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">История психологии -- История психологии в Соединенных Штатах Америки (США) -- История психологии в новейшее время -- Психологические направления</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Ключевые слова: </span>
								<span class="iblock val">социальная психология</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Хранение:</span>
								<span class="iblock val">9 11-3/12;</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Электронный адрес:</span>
								<span class="iblock val">Электронный ресурс</span>
							</div>							
						</div><!-- /b-infobox -->
					</div>
					<div class="b-line_social">
						<h5>Поделиться с друзьями</h5>
						<a href="#" class="b-login_slnk vklink iblock">вконтакте</a>
						<a href="#" class="b-login_slnk odnlink iblock">одноклассники</a>
						<a href="#" class="b-login_slnk fblink iblock">facebook</a>
					</div><!-- /.b-login_social -->
					<div class="b-lineinfo">
						<h5>найти похожие книги по тематике</h5>
						<ul class="b-lineinfolist">
							<li><a href="#">Общая психология, психология личности, история психологии</a></li>
							<li><a href="#">История психологии</a></li>
							<li><a href="#">История психологии в Соединенных Штатах Америки (США)</a></li>
							<li><a href="#">История психологии в новейшее время</a></li>
							<li><a href="#">Психологические направления</a></li>
						</ul>
					</div>
					
				</div>
				<div class="b-bookframe iblock">
					<img alt="" class="b-bookframe_img" src="./pic/pic_15.jpg">
					
				</div>
			
				<div class="b-addbook_popuptit clearfix">					
					<h2>похожие издания</h2>

				</div>
				<div class="book_slider">
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_35.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_36.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_37.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_38.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_35.jpg"></a>
					<a href="#" class="slide"><img alt="" class="loadingimg" src="./pic/pic_40.jpg"></a>
				</div>
			</div>

		</div> 	<!-- /.bookpopup -->

		

	</section>
</div>
</div><!-- /.homepage -->

<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu">мои подборки</a>
	

	<form class="b-selectionadd" action="">
		<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
		<span class="b-selectionaddtxt">Cоздать подборку</span>
		<input type="text" class="input hidden">
	</form>
</div><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>