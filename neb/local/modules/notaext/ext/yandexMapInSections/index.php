<?

class yandexMapInSections extends CExt
{
    var $EXT_ID = __CLASS__;
    var $EXT_VERSION;
    var $EXT_VERSION_DATE;
    var $EXT_NAME;
    var $EXT_DESCRIPTION;
    var $EXT_SORT;

    public function __construct()
    {
        parent::__construct();

        $this->EXT_VERSION = '1.0';
        $this->EXT_VERSION_DATE = '06.05.2014';

        $this->EXT_NAME = 'Привязка к Yandex карте в разделе инфоблока';
        $this->EXT_DESCRIPTION = 'Пользовательское свойство для привязки к Yandex карте в разделе инфоблока';
        $this->EXT_SORT = 100;

    }

    function onBeforeInstall()
    {

    }

    function onBeforeUninstall()
    {

    }

    function onAfterInstall()
    {

    }

    function onAfterUninstall()
    {

    }
}
