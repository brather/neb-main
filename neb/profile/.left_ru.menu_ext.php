<?

$obNUser = new nebUser();

if (!$obNUser->IsAuthorized())
    return false;

$obNUser->getRole();
$obNUser->getUserGroups();

/*
 * if($obNUser->role != 'user')
 *      $arLibrary = $obNUser->getLibrary();
 */

if($obNUser->role == 'user')
{
    /* меню пользователя*/
    $aMenuLinksExt = array(
        //array('Выйти', '/profile/?logout=yes', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
        array('Моя библиотека', '/profile/index.php', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
        array('Отсканированные материалы', '/profile/scanning/', array(), 	array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
        array('Поисковые запросы', '/profile/searches/', array(),			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_search")),
        array('Цитаты', '/profile/quote/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_qt")),
        array('Закладки', '/profile/bookmark/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_bm")),
        array('Заметки', '/profile/note/', array(), 						array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_nav_notes")),
    );
}
else if($obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR)
{
	/* меню библиотекаря */
    $aMenuLinksExt = [
        'profile'          => ['Новости',              '/profile/index.php',          [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        'statistics'        => ['Статистика',          '/profile/statistics/',        [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        'collection'        => ['Подборки',            '/profile/collection/',        [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        'check_eechb'       => ['ЕЭЧБ',                '/profile/check_eechb/',       [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        'plan_digitization' => ['Планы на оцифровку',  '/profile/plan_digitization/', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        'digitization'      => ['Заявки от читателей', '/profile/digitization/',      [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        'feedback'          => ['Обратная связь',      '/profile/feedback/',          [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        'markup'            => ['Разметка изданий',    '/profile/markup/',            [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => '']],
        'notification'      => ['Оповещения',          '/profile/notification/history/',      [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
    ];
    // доступ к разметке изданий только при дополнительной группе пользователя "Администратор аналитической росписи"
    if (!in_array(UGROUP_MARKUP_APPROVER, $obNUser->arUserGroups)) {
        unset($aMenuLinksExt['markup']);
    }
}
else if($obNUser->role == UGROUP_LIB_CODE_CONTORLLER)
{
    /* меню библиотекаря-контролера*/
    $aMenuLinksExt = array(
        array(' ', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"b-profile_navlk js_profilemenu")),
        array('Настройки профиля', '/profile/edit/', array(), 		array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
        array('Помощь', '/faq/', array(), 							array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'MSGNUM' => 0)),
        //array('Выйти', '/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
        array('Кабинет', '/profile/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
        array('Статистика', '/profile/statistics/', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
        array('ЕЭЧБ', '/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
    );
}
else if($obNUser->role == UGROUP_RIGHTHOLDER_CODE)
{
    /* меню правообладателя	*/
    $aMenuLinksExt = array(
        array('Список произведений', '/profile/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => "")),
        //array('Мои договоры', '/profile/contracts/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => "")),
        array('Заключение договора', '/profile/conclusion/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT'   => false, 'DEPTH_LEVEL' => 1, "CLASS" => "")),
        array('Обратная связь', '/profile/feedback/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1,"CLASS" => "")),
        array('Справочная информация', '/profile/reference/', array(), array('FROM_IBLOCK' => true, 'IS_PARENT'   => false, 'DEPTH_LEVEL' => 1, "CLASS" => "")),
    );
} elseif ($obNUser->role == UGROUP_OPERATOR_CODE) {

    /* меню оператора НЭБ */
    $sectionRights = $obNUser->getSectionsRights();

    $sectionItems = [
        'libraries'          => ['Библиотеки',      '/profile/libraries/',        [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'users'              => ['Пользователи',    '/profile/users/',            [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'report_constructor' => ['Конструктор отчетов','/profile/report_constructor/', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
		'workplaces'         => ['ЭЧЗ',             '/profile/workplaces/',       [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'workplaces_types'   => ['Типы ЭЧЗ',        '/profile/workplaces_types/', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'statistics'         => ['Статистика',      '/profile/statistics/',       [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'collection'         => ['Подборки',        '/profile/collection/',       [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        'funds'              => ['Фонды',           '/profile/funds/',            [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'digitization_list'  => ['Планы оцифровки', '/profile/digitization_list/',[], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'digitization'       => ['Заявки на оцифровку от читателей',  '/profile/digitization/',     [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'digitization_stat'  => ['Статистика оцифровки', '/profile/digitization_stat/',       [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'news'               => ['Новости',         '/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=11&type=newsNEB&lang=ru&find_section_section=0', [], ['FROM_IBLOCK' => true, 'IS_PARENT'   => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'forum'              => ['Форум',           '/forum/',                    [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'polls'              => ['Опросы',          '/bitrix/admin/vote_user_votes.php?lang=ru', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'rightholders'       => ['Правообладатели', '/profile/rightholders/',     [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'feedback'           => ['Обратная связь',  '/profile/feedback/',         [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'notification'       => ['Оповещения',      '/profile/notification/',     [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, 'CLASS' => '']],
        'markup'             => ['Разметка изданий','/profile/markup/',           [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => '']],
        'faq'			     => ['Часто задаваемые вопросы', '/profile/faq/',	 [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        'assessor_workplace' => ['Рабочее место асессора', '/profile/assessor-workplace/',	 [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
    ];

    // доступ к разметке изданий только при дополнительной группе пользователя "Администратор аналитической росписи"
    if (!in_array(UGROUP_MARKUP_APPROVER, $obNUser->arUserGroups)) {
        unset($sectionItems['markup']);
    }

    // если глобавльный оператор - всегда давать доступ
    if (in_array(UGROUP_OPERATOR_CODE, $obNUser->arUserGroups)) {
        $aMenuLinksExt = $sectionItems;
    }
    // иначе - проверять доп.группы
    else {
        $aMenuLinksExt = array_values(array_intersect_key($sectionItems, array_flip($sectionRights)));
    }
}
elseif($obNUser->role == UGROUP_OPERATOR_WCHZ){
    /* меню оператора ЭЧЗ */
    $aMenuLinksExt = [
        ['Информация об ЭЧЗ',     '/profile/index.php',     [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        ['Статистика',            '/profile/statistics/',   [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        ['Верификация',           '/profile/verification/', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        //['Читатели',              '/profile/readers/',      [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        //['Активные пользователи', '/profile/users/',        [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
        //['Новые пользователи',    '/profile/users-new/',    [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS" => ""]],
    ];
}
elseif($obNUser->role == UGROUP_MARKUP_APPROVER){
    $aMenuLinksExt = [
        ['Разметка изданий', '/profile/', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>""]],
    ];
}
elseif($obNUser->role == UGROUP_ASSESSOR){
    $aMenuLinksExt = [
        ['Рабочее место асессора', '/profile/assessor-workplace/', [], ['FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>""]],
    ];
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
//var_dump($aMenuLinks);