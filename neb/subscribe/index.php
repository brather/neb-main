<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Рассылки');
if (!$USER->IsAuthorized()) {
    echo 'Подписка доступна только авторизованным пользователям. <a href="/auth/">Авторизоваться.</a>';
} else {
    $APPLICATION->IncludeComponent(
        'bitrix:subscribe.form',
        'neb',
        Array(
            'COMPONENT_TEMPLATE'  => '.default',
            'USE_PERSONALIZATION' => 'Y',
            'SHOW_HIDDEN'         => 'N',
            'PAGE'                => 'subscr_edit.php',
            'CACHE_TYPE'          => 'N',
            'CACHE_TIME'          => '3600',
            'RUBRIC_ID'           => isset($_GET['RUBRIC_ID'])
                ? $_GET['RUBRIC_ID']
                : null,
        )
    );
}
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');