<?php
/**
 * User: agolodkov
 * Date: 13.05.2015
 * Time: 10:50
 */

namespace helpers\user;


use Nota\UserData\rgb;

/**
 * Class nebReader
 *
 * @package helpers\user
 */
class nebReader extends \nebUser
{
    /**
     * @param $status
     *
     * @return bool
     * @throws \Exception
     */
    public function setStatus($status)
    {
        if ($status !== static::USER_STATUS_VERIFIED) {
            return parent::setStatus($status);
        }
        $userData = $this->getUser();
        if (!empty($userData['UF_RGB_USER_ID'])) {
            return parent::setStatus($status);
        }
        $rgb = new rgb();
        try {
            $rgb->userCreateBx($this->USER_ID);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }

        return parent::setStatus($status);
    }
}