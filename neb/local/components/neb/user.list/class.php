<?php
/**
 * User: agolodkov
 * Date: 31.07.2015
 * Time: 10:37
 */

/**
 * Class NebLibraryPartyComponent
 */
class UserListComponent extends \Neb\Main\ListComponent
{

    /**
     * @var array
     */
    protected $_defaultParams
        = array(
            'NAV_NUM'    => 1,
            'PAGE_SIZE'  => 15,
            'listParams' => array(
                'filter' => array(),
                'select' => array(
                    'ID',
                    'NAME',
                    'LAST_NAME',
                    'SECOND_NAME',
                    'DATE_REGISTER',
                    'EMAIL',
                ),
            )
        );

    /**
     * @return $this
     */
    public function prepareParams()
    {
        $this->_prepareNavigation();

        $obUserTypeEntity = new \CUserTypeEntity();
        $oCUserFieldEnum = new \CUserFieldEnum();

        // получение идентификатора свойства статусов пользователя
        $arStatusField = $obUserTypeEntity->GetList(
            array('ID' => 'ASC'),
            array('ENTITY_ID' => 'USER', 'FIELD_NAME' => 'UF_STATUS')
        )->Fetch();

        // список статусов пользователей
        $arVerifyStatus = $oCUserFieldEnum->GetList(
            array('SORT' => 'ASC'),
            array("USER_FIELD_ID" => $arStatusField['ID'], "XML_ID" => 'user_status_verified')
        )->Fetch();

        unset($obUserTypeEntity, $oCUserFieldEnum);

        
        // ФИЛЬТРАЦИЯ ПОЛЬЗОВАТЕЛЕЙ ПО ФИО

        // Шаг 1: Собираем массив из слов
        $arWords = trim($this->arParams['FIO']);
        $arWords = explode(' ', $arWords);
        foreach ($arWords as $key => $word) {
            $arWords[$key] = trim($word);
            if (mb_strlen($arWords[$key]) < 1)
                unset($arWords[$key]);
        }

        // Шаг 2: Получаем все возможные комбинации полей поиска
        $arSearch = [];
        $iCount = count($arWords);
        if ($iCount) {
            $searchFields = ['LAST_NAME', 'NAME', 'SECOND_NAME'];
            foreach ($searchFields as $v1) {
                if ($iCount < 2) {
                    $arSearch[] = [$v1];
                    continue;
                }
                foreach ($searchFields as $v2) {
                    if ($iCount < 3 && $v1 !== $v2) {
                        $arSearch[] = [$v1, $v2];
                        continue;
                    }
                    foreach ($searchFields as $v3)
                        if ($v1 !== $v2 && $v2 !== $v3 && $v1 !== $v3)
                            $arSearch[] = [$v1, $v2, $v3];
                }
            }
        }

        // Шаг 3: Формируем массив поиска по полям
        $logicSearch = ['LOGIC' => 'OR'];
        foreach ($arSearch as $arFields) {
            $orLogic = ['LOGIC' => 'AND',];
            foreach ($arFields as $iNumber => $sFields) {
                $orLogic[] = [[
                    'LOGIC'        => 'AND',
                    $sFields       => '%' . $arWords[$iNumber] . '%',
                    '!' . $sFields => null,
                ],];
            }
            $logicSearch[] = $orLogic;
        }

        if ($this->arParams['WORK_COMPANY'])
            $logicSearch['WORK_COMPANY'] = $this->arParams['WORK_COMPANY'];

        if ($this->arParams['UF_STATUS'])
            $logicSearch['UF_STATUS'] = $this->arParams['WORK_COMPANY'];

        if (!empty($this->arParams['STATUS'])) {
            if ($this->arParams['STATUS'] == 'VERIFIED')
                $logicSearch['UF_STATUS'] = $arVerifyStatus['ID'];
            elseif ($this->arParams['STATUS'] == 'REGISTERED')
                $logicSearch['!UF_STATUS'] = $arVerifyStatus['ID'];
        }

        $this->arParams['listParams']['filter'][] = $logicSearch;

        // фильтрация пользователей по группам
        if (isset($this->arParams['GROUP_ID'])) {
            $this->arParams['listParams']['runtime'] = array(
                new \Bitrix\Main\Entity\ExpressionField(
                    'GROUP_EXIXTS',
                    'EXISTS(SELECT TRUE FROM b_user_group bug WHERE bug.USER_ID=%s AND bug.GROUP_ID IN ('
                    . implode(
                        ',',
                        (
                        is_array($this->arParams['GROUP_ID'])
                            ? $this->arParams['GROUP_ID']
                            : array($this->arParams['GROUP_ID'])
                        )
                    )
                    . '))',
                    array('ID')
                )
            );
            $this->arParams['listParams']['filter']['GROUP_EXIXTS'] = true;
            $this->arParams['listParams']['select'][] = 'GROUP_EXIXTS';
        }
        if (isset($this->arParams['SUBORDINATE_GROUP'])) {
            $this->arParams['listParams']['runtime'] = array(
                new \Bitrix\Main\Entity\ExpressionField(
                    'SUBORDINATE',
                    'NOT EXISTS(SELECT TRUE FROM b_user_group bug WHERE bug.USER_ID=%s AND bug.GROUP_ID NOT IN ('
                    . implode(
                        ',',
                        nebUser::getGroupSubordinate(
                            $this->arParams['SUBORDINATE_GROUP']
                        )
                    )
                    . '))',
                    array('ID')
                )
            );
            $this->arParams['listParams']['filter']['SUBORDINATE'] = true;
            $this->arParams['listParams']['select'][] = 'SUBORDINATE';
        }

        $this->arParams['listParams']['select'][] = 'UF_STATUS';
        $this->arParams['listParams']['select'][] = 'UF_FIO_HEAD';
        $this->arParams['listParams']['select'][] = 'UF_POSITION_HEAD';

        // сортировка
        $order = [];

        $orderDirection = 'ASC';
        if ($this->arParams['ORDER'] && 'desc' === $this->arParams['ORDER']) {
            $orderDirection = 'DESC';
        }
        if ($this->arParams['BY']) {
            $order[$this->arParams['BY']] = $orderDirection;
        }
        if (empty($order)) {
            $order = ['DATE_REGISTER' => 'DESC'];
        }
        $this->arParams['listParams']['order'] = $order;
        
        return $this;
    }

    /**
     * @return $this
     */
    public function loadList()
    {
        $this->_loadTable(new \Bitrix\Main\UserTable());

        $obUserTypeEntity = new \CUserTypeEntity();
        $obUserFieldEnum = new \CUserFieldEnum();

        $arStatusField = $obUserTypeEntity->GetList(
            array('ID' => 'ASC'),
            array('ENTITY_ID' => 'USER', 'FIELD_NAME' => 'UF_STATUS')
        )->Fetch();

        // список статусов пользователей
        $rsStatus = $obUserFieldEnum->GetList(array('SORT' => 'ASC'), array("USER_FIELD_ID" => $arStatusField['ID']));
        while ($arFields = $rsStatus->Fetch())
            $this->arResult['statuses'][$arFields['ID']] = $arFields;

        unset($obUserTypeEntity, $obUserFieldEnum, $rsStatus, $arFields);

        return $this;
    }
}