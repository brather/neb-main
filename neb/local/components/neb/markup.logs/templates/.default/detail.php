<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('MARKUP_LOG_TITLE'));

?>
<div class="properties-book-content">
    <h2><?=Loc::getMessage('MARKUP_LOG_DESCRIPTION')?>:</h2>

    <?if($arResult['BOOK']['title']):?>
        <div>
            <h3><?=Loc::getMessage('MARKUP_LOG_NAME')?>:</h3>
            <span><a href="/catalog/<?=$arResult['BOOK']['id']?>/viewer/"><?=$arResult['BOOK']['title']?></a></span>
        </div>
    <?endif;?>
    <?if($arResult['BOOK']['authorbook']):?>
        <div>
            <h3><?=Loc::getMessage('MARKUP_LOG_AUTHOR')?>:</h3>
            <span><?=$arResult['BOOK']['authorbook']?></span>
        </div>
    <?endif;?>
</div>

<? if (!empty($arResult['ITEMS'])): ?>
    <div class="table-responsive p">
        <table  class="table table-hover table-bordered table-condensed">
            <tr>
                <th><?=Loc::getMessage('MARKUP_LOG_AUTHOR')?></th>
                <th><?=Loc::getMessage('MARKUP_LOG_NAME')?></th>
                <th><?=Loc::getMessage('MARKUP_LOG_STARTPAGE')?></th>
                <th><?=Loc::getMessage('MARKUP_LOG_ENDPAGE')?></th>
                <th><?=Loc::getMessage('MARKUP_LOG_POSITION')?></th>
                <th><?=Loc::getMessage('MARKUP_LOG_RANGEPAGE')?></th>
                <th><?=Loc::getMessage('MARKUP_LOG_STATUS')?></th>
                <th><?=Loc::getMessage('MARKUP_LOG_MODERATION')?></th>
                <th><?=Loc::getMessage('MARKUP_LOG_DATE')?></th>
                <th><?=Loc::getMessage('MARKUP_LOG_LOGIN')?></th>
            </tr>
            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                <tr>
                    <td><?=$arItem['author']?></td>
                    <td><?=$arItem['title']?></td>
                    <td><?=$arItem['startPage']?></td>
                    <td><?=$arItem['finishPage']?></td>
                    <td><?=$arItem['position']?></td>
                    <td><?=$arItem['rangePage']?></td>
                    <td><?=Loc::getMessage('MARKUP_LOG_STATUS_' . strtoupper($arItem['status']))?></td>
                    <td><?=Loc::getMessage('MARKUP_LOG_STATUS_' . strtoupper($arItem['statusModeration']))?></td>
                    <td><?=(new Datetime($arItem['updatingDateTime']))->format('d.m.Y H:i:s');?></td>
                    <td><?=str_replace('"', '', $arItem['login'])?></td>
                </tr>
            <? endforeach; ?>
        </table>
    </div>
<? endif; ?>