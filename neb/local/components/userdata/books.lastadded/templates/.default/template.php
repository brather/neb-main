<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<div class="b-mylib_add">
<h2><?=Loc::getMessage('BOOKS_LAST_ADDED_TEMPLATE_ADDED'); ?></h2>
<div class="clearfix b-mylib_addinner">
	<div class="b-mylib_books iblock">
		<ul class="b-bookboard_list">
			<li>
				<a href="#" class="b-bookboardphoto iblock"><img src="<?=MARKUP?>pic/pic_1.jpg" alt=""></a>						
			</li>
			<li>
				<a href="#" class="b-bookboardphoto iblock"><img src="<?=MARKUP?>pic/pic_2.jpg" alt=""></a>
			</li>
			<li>
				<a href="#" class="b-bookboardphoto iblock"><img src="<?=MARKUP?>pic/pic_3.jpg" alt=""></a>
			</li>
			<li>
				<a href="#" class="b-bookboardphoto iblock"><img src="<?=MARKUP?>pic/pic_11.jpg" alt=""></a>
			</li>	
		</ul>
		<a href="#" class="button_mode"><?=Loc::getMessage('BOOKS_LAST_ADDED_TEMPLATE_GOTO'); ?></a>
	</div>
	
	<div class="b-mylib_fav iblock">
		<div class="b-mylib_favbooks rel">
			<a href="#" class="b-bookboardphoto iblock b-fav_one"><img src="<?=MARKUP?>pic/pic_1.jpg" alt=""></a>
			<a href="#" class="b-bookboardphoto iblock b-fav_two"><img src="<?=MARKUP?>pic/pic_2.jpg" alt=""></a>
			<a href="#" class="b-bookboardphoto iblock b-fav_three"><img src="<?=MARKUP?>pic/pic_3.jpg" alt=""></a>
		</div>
		<h4 class="black"><?=Loc::getMessage('BOOKS_LAST_ADDED_TEMPLATE_FAVORITE'); ?></h4>
		<span class="litelabel"><?=Loc::getMessage('BOOKS_LAST_ADDED_TEMPLATE_SELECTION'); ?></span>
		
	</div><!-- /.b-mylib_fav -->

</div>

</div><!-- /.b-mylib_add -->