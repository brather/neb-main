<?php
/**
 * User: agolodkov
 * Date: 31.07.2015
 * Time: 10:37
 */

/**
 * Class NebLibraryPartyComponent
 */
class RightHolderPublicationsListComponent extends \Neb\Main\ListComponent
{

    /**
     * @var array
     */
    protected $_defaultParams
        = array(
            'listParams' => array(
                'filter' => array(
                    'request_type' => array('rightholder'),
                ),
            )
        );

    /**
     * @return $this
     */
    public function prepareParams()
    {
        if (isset($this->arParams['UID'])
            && (integer)$this->arParams['UID'] > 0
        ) {
            $this->arParams['listParams']['filter']['BX_UID']
                = (integer)$this->arParams['UID'];
        }
        $this->_prepareNavigation();

        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     * @throws \Bitrix\Main\LoaderException
     */
    public function loadList()
    {
        if (!\Bitrix\Main\Loader::includeModule('nota.exalead')) {
            throw new Exception('Module "nota.exalead" not installed');
        }
        $this->_loadTable(new \Nota\Exalead\BiblioCardTable());

        $booksIds = array();
        foreach ($this->arResult['items'] as $key => &$item) {
            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $item['pdfLink'])) {
                unset($item['pdfLink']);
            }
            $item['new'] = true;
            if (!empty($item['FullSymbolicId'])) {
                $booksIds[$item['FullSymbolicId']] = $key;
                $item['new'] = false;
                $item['DETAIL_URL'] = '/catalog/'
                    . $item['FullSymbolicId'] . '/';
            }
            $requestRestUrl = '/rest_api/book/request/?request_id='
                . $item['Id'];
            $item['confirmUrl'] = $requestRestUrl . '&confirm=1';
            $item['refuseUrl'] = $requestRestUrl . '&refuse=1';
            $item['views'] = 0;
        }
        if (!empty($booksIds)) {
            $booksInfoResult = \Nota\Exalead\BooksInfoTable::getList(
                array(
                    'filter' => array(
                        'BOOK_ID' => array_keys($booksIds),
                    ),
                )
            );
            while ($bookInfo = $booksInfoResult->fetch()) {
                if (!isset($booksIds[$bookInfo['BOOK_ID']])) {
                    continue;
                }
                $bookKey = $booksIds[$bookInfo['BOOK_ID']];
                $this->arResult['items'][$bookKey]['views']
                    = (integer)$bookInfo['VIEWS'];
            }
        }
        unset($item);

        return $this;
    }

}