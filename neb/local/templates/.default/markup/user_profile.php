
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb bbox b-text" id="" value="Станислав Лем">							
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>	
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>		

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix">
<div class="b-searchresult b-searchresult_mode">
			<ul class="b-profile_nav">
				<li>
					<a href="#" class="b-profile_navlk js_profilemenu current">личный кабинет</a>
					<ul class="b-profile_subnav">
						<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
						<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
					</ul>
				</li>
				<li><a class="b-profile_nav_lb" href="#">моя библиотека</a></li>
				<li><a href="#" class="b-profile_nav_qt">цитаты</a></li>
				<li><a href="#" class="b-profile_nav_bm ">закладки</a></li>
				<li><a href="#" class="b-profile_nav_notes">заметки</a></li>
			</ul>                 
		</div><!-- /.b-searchresult-->
	<div class="b-mainblock left">
		
		<h2 class="bold">сейчас читаю</h2>

		<ul class="b-bookboard_list b-readnow">
			<li>
				<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_1.jpg" alt=""></a>
				<div class="b-readprogress"><div class="b-loadprogress">
					<div class="b-loadlabel"></div>
					<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="38">
				</div></div>
			</li>
			<li>
				<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_2.jpg" alt=""></a>
				<div class="b-readprogress"><div class="b-loadprogress"><div class="b-loadlabel"></div><input type="text"  data-height="37" class="knob" data-displayInput="false" data-width="37" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="68"></div></div>
			</li>
			<li>
				<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_3.jpg" alt=""></a>
				<div class="b-readprogress"><div class="b-loadprogress"><div class="b-loadlabel"></div><input type="text"  data-height="37" class="knob" data-displayInput="false" data-width="37" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="75"></div></div>
			</li>
			<li>
				<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_11.jpg" alt=""></a>
				<div class="b-readprogress"><div class="b-loadprogress"><div class="b-loadlabel"></div><input type="text"  data-height="37" class="knob" data-displayInput="false" data-width="37" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="95"></div></div>
			</li>	
		</ul> <!-- /.b-bookboard_list-->
		
		<div class="b-mylib_add">
			<h2>добавленные в мою библиотеку</h2>
			<div class="clearfix b-mylib_addinner">
				<div class="b-mylib_books iblock">
					<ul class="b-bookboard_list">
						<li>
							<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_1.jpg" alt=""></a>						
						</li>
						<li>
							<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_2.jpg" alt=""></a>
						</li>
						<li>
							<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_3.jpg" alt=""></a>
						</li>
						<li>
							<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_11.jpg" alt=""></a>
						</li>	
					</ul>
					<a href="#" class="button_mode">перейти в мою библиотеку</a>
				</div>
				
				<div class="b-mylib_fav iblock">
					<div class="b-mylib_favbooks rel">
						<a href="#" class="b-bookboardphoto iblock b-fav_one"><img class="" src="./pic/pic_1.jpg" alt=""></a>
						<a href="#" class="b-bookboardphoto iblock b-fav_two"><img class="" src="./pic/pic_2.jpg" alt=""></a>
						<a href="#" class="b-bookboardphoto iblock b-fav_three"><img class="" src="./pic/pic_3.jpg" alt=""></a>
					</div>
					<h4 class="black">Любимые авторы</h4>
					<span class="litelabel">подборка</span>
					
				</div><!-- /.b-mylib_fav -->

			</div>

		</div><!-- /.b-mylib_add -->
		<div class="b-random_quote">
			<h2>Случайная цитата</h2>
			<div class="b-quote_item">
			<div class="b-quote">
				<a href="#" class="b-bookboardphoto iblock"><img class="loadingimg" src="./pic/pic_28.jpg" alt=""></a>
				<div class="iblock b-quote_txt"><p>Театры, концерты, книги – я почти утратил вкус ко всем этим буржуазным привычкам. Они не были в духе времен. Политика была сама по себе в достаточной мере театром, ежевечерняя стрельба заменяла концерты, а огромная книга людской нужды убеждала больше целых библиотек.</p>
				</div>
			</div><!-- /.b-quote -->
			<div class="clearfix rel b-quote_act">
				<div class="b-quote_copy ">
					<a href="#" class="b-quote_copy_label">Скопировать</a>
					<div class="iblock ">
						<a href="#" class="b-openermenu js_openmenu">подпись ГОСТ</a>
						<ul class="b-gostlist">
							<li><span>Исаев И.А. История государства и права России : учеб. пособие для студ. вузов / И.А. Исаев. - М. : Проспект, 2009. - 336 с.</span></li>
							<li><span>Писахов С.Г. Сказки Сени Малины : сказки / С. Г. Писахов. - Архангельск : ИПП "Правда Севера", 2009. - 272 с. ил.</span></li>
							<li><span>Холодная война в Арктике / сост. М. Н. Супрун. - Архангельск : [б. и.], 2009. - 379 с.</span></li>		
						</ul>
					</div>
					
				</div>
				<ul class="b-resultbook-info">
					<li><span>Автор:</span> <a href="#">Эрих Мария Ремарк</a></li>
					<li><span>Книга:</span> <a href="#">Три товарища</a></li>

				</ul>
			</div>
			</div>
			<a href="#" class="button_mode">перейти в цитаты</a>
		</div><!-- /.b-random_quote -->
		<div class="b-viewhistory">
			<h2>История просмотров</h2>

			<div class="b-boookslider js_multipleslider">
				<div>
					<div class="rel">
						<a href="#" class="b-bookboardphoto"><img class="loadingimg" src="./pic/pic_12.jpg" alt=""></a>
						<div class="b-bhistiry_date">12 июня</div>
					</div>					
				</div>
				<div>
					<div class="rel">
						<a href="#" class="b-bookboardphoto"><img class="loadingimg" src="./pic/pic_13.jpg" alt=""></a>
						<div class="b-bhistiry_date">11 июня</div>
					</div>					
				</div>
				<div>
					<div class="rel">
						<a href="#" class="b-bookboardphoto"><img class="loadingimg" src="./pic/pic_5.jpg" alt=""></a>
						<div class="b-bhistiry_date">15 мая</div>
					</div>					
				</div>
				<div>
					<div class="rel">
						<a href="#" class="b-bookboardphoto"><img class="loadingimg" src="./pic/pic_6.jpg" alt=""></a>
						<div class="b-bhistiry_date">2 марта</div>
					</div>					
				</div>
				<div>
					<div class="rel">
						<a href="#" class="b-bookboardphoto"><img class="loadingimg" src="./pic/pic_12.jpg" alt=""></a>
						<div class="b-bhistiry_date">12 июня</div>
					</div>					
				</div>
				<div>
					<div class="rel">
						<a href="#" class="b-bookboardphoto"><img class="loadingimg" src="./pic/pic_13.jpg" alt=""></a>
						<div class="b-bhistiry_date">11 июня</div>
					</div>					
				</div>
				<div>
					<div class="rel">
						<a href="#" class="b-bookboardphoto"><img class="loadingimg" src="./pic/pic_5.jpg" alt=""></a>
						<div class="b-bhistiry_date">15 мая</div>
					</div>					
				</div>
				<div>
					<div class="rel">
						<a href="#" class="b-bookboardphoto"><img class="loadingimg" src="./pic/pic_6.jpg" alt=""></a>
						<div class="b-bhistiry_date">2 марта</div>
					</div>					
				</div>
			</div> <!-- /.boardslider -->
			<a href="#" class="button_mode">перейти в историю</a>
		</div>
		
		
	</div><!-- /.b-mainblock -->
	<div class="b-side right mt">
		<div class="b-profile_side">
			<div class="b-profile_photo">
				<img src="./i/user_photo.png" alt="user photo">
			</div>
			<ul class="b-profile_info">
				<li>
					<a href="#" id="setphoto_profile" style="position: relative; z-index: 1;">изменить фото</a>
					<div class="pl_button" id="container_zPwdWGY" style="position: relative; overflow:hidden;"><div id="html5_197h08hg010bqqq91g2l1gnjltr3_container" class="moxie-shim moxie-shim-html5" style="position: absolute; top: 496px; left: 1271px; width: 103px; height: 20px; overflow: hidden; z-index: 0;"><input type="file" accept="image/jpeg,image/png" style="font-size: 999px; opacity: 0; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;" id="html5_197h08hg010bqqq91g2l1gnjltr3"></div></div>
					<pre id="plupload_console_ayv8OaV" style="display: none;"></pre>
					<script type="text/javascript">
	$(function() {
		var uploader_zPwdWGY = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'setphoto_profile',
				container: document.getElementById('container_zPwdWGY'), 
									unique_names: true,
					
				url : '/local/components/notaext/plupload/ajax.php',
				filters : {
					max_file_size : '24mb',
					prevent_duplicates: true
											,
						mime_types: [
							{title : "Mine files", extensions : 'jpg,jpeg,png'}
						]
										},
									resize: {
						width: 110,
						height: 110,
													crop: true,
													quality: 98					},
									max_file_size : '24mb',
				chunk_size: '1mb',
				flash_swf_url : '/local/components/notaext/plupload/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '894c947aa7fcd253fa4f34381bda2888',
					aFILE_TYPES : 'jpg,jpeg,png',
					aDIR : '/upload/tmp_register/',
					aMAX_FILE_SIZE : '24',
					aMAX_FILE_AGE : '10800',
					aFILES_FIELD_NAME : 'profile_file',
					aMULTI_SELECTION : 'N',
					aCLEANUP_DIR : '1',
					aRAND_STR : 'zPwdWGY'
				},
				multi_selection: false,
				init: {

					FilesAdded: function(up, files) {

						uploader_zPwdWGY.start();
					},
					FileUploaded: function(up, file, response) {
						var result = response.response;
						if (result) {
							var obResponse = JSON.parse(result);
							$('#user_photo').attr('src', obResponse.file);

							$.post( "/local/templates/.default/components/notaext/plupload/profile/set_photo.php", { file: obResponse.file, sessid: '894c947aa7fcd253fa4f34381bda2888' });
						}
					},
					Error: function(up, err) {
						document.getElementById('plupload_console_zPwdWGY').innerHTML += err.message + '&lt;br&gt;';
					}
				}
			}
		);

		uploader_zPwdWGY.init();
	});
</script>
				</li>
				<li><a href="#">настройки профиля</a></li>
				<li><a href="#">личные сообщения</a></li>
			</ul>
			<div class="b-profile_messages"><span>3</span></div>			
			
			
		</div>
	</div><!-- /.b-side -->
	
</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>