<?php
namespace Bitrix\Posting;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class EmailTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> POSTING_ID int mandatory
 * <li> STATUS string(1) mandatory
 * <li> EMAIL string(255) mandatory
 * <li> SUBSCRIPTION_ID int optional
 * <li> USER_ID int optional
 * </ul>
 *
 * @package Bitrix\Posting
 **/

class EmailTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_posting_email';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('EMAIL_ENTITY_ID_FIELD'),
            ),
            'POSTING_ID' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => Loc::getMessage('EMAIL_ENTITY_POSTING_ID_FIELD'),
            ),
            'STATUS' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateStatus'),
                'title' => Loc::getMessage('EMAIL_ENTITY_STATUS_FIELD'),
            ),
            'EMAIL' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateEmail'),
                'title' => Loc::getMessage('EMAIL_ENTITY_EMAIL_FIELD'),
            ),
            'SUBSCRIPTION_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('EMAIL_ENTITY_SUBSCRIPTION_ID_FIELD'),
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('EMAIL_ENTITY_USER_ID_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for STATUS field.
     *
     * @return array
     */
    public static function validateStatus()
    {
        return array(
            new Main\Entity\Validator\Length(null, 1),
        );
    }
    /**
     * Returns validators for EMAIL field.
     *
     * @return array
     */
    public static function validateEmail()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}