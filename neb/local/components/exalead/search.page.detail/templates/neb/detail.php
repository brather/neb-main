<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * Карточка книги
 *
 * Используется микроформат http://schema.org/Book
 * https://yandex.ru/support/webmaster/supported-schemas/other-content.html
 * https://webmaster.yandex.ru/tools/microtest/
 * https://search.google.com/structured-data/testing-tool
 */

global $APPLICATION;

if (empty($arResult['BOOK'])) {
    return false;
}

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);
Asset::getInstance()->addCss('/local/templates/adaptive/css/library-map.css');

CJSCore::Init();

?>
<link rel="stylesheet" href="/local/templates/.default/markup/css/card.css" type="text/css" />

<a class="linkback-overall" data-link-back>&larr; <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BACK') ?></a>

<div class="edition-title">
    <h2 class="edition-title__author"><?
        foreach ($arResult['BOOK']['authorbook_for_link'] as $key => $sAuthor ):
            if ($key): ?>, <?endif; ?><a href="/search/?f_field[authorbook]=f/authorbook/<?
            echo urlencode(mb_strtolower(trim($sAuthor)))?>"><?= trim($sAuthor) ?></a><?
        endforeach; ?>
    </h2>
        <?
        if ( isset( $arResult['BOOK']['fileExt'] ) && in_array( 'epub', $arResult['BOOK']['fileExt'] ) ) {

            $lr = new NebLinkLitres();
            $lr->setUserID( $_COOKIE['NEB_SM_GUEST_ID'] );
            $lr->setEPubID( $arResult['BOOK']['id'] ); ?>
            <a class="btn btn-primary btn-lg pull-right"
                href="<?= $lr->generateLink() ?>"
                target="_blank" >
                    <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_READ') ?>
            </a>
        <? } elseif ('Y' === $arResult['BOOK']['IS_READ']) { ?>
            <a
                class="btn btn-primary btn-lg pull-right boockard-read-button clearfix<?php if (
                    !$arResult['isInWorkplace'] && $arResult['nebUser']['UF_STATUS'] !== nebUser::USER_STATUS_VIP
                ): ?> boockard-read-button--disabled <?php endif; ?>"
                href="javascript:void(0);"
                data-link="<?= $arResult['BOOK']['readBookId'] ?>"
                data-options="<?php echo htmlspecialchars(
                    json_encode($arResult['BOOK']['viewerOptions']), ENT_QUOTES,
                    'UTF-8'
                ) ?>"
                data-server-error="<?= Loc::getMessage(
                    'LIB_SEARCH_PAGE_DETAIL_SERVER_ERROR'
                ) ?>">
                <?php if ('museum' == $arResult['BOOK']['librarytype']): ?>
                    <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_VIEW') ?>
                <?php else: ?>
                    <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_READ') ?>
                <?php endif; ?>
            </a>
        <? } ?>
        <? if ( "Y" === $arResult['BOOK']['IS_MEDIA'] ): ?>
            <? $mediaText = $arResult['BOOK']['IS_MEDIA_AUDIO']
                ? Loc::getMessage("LIB_SEARCH_PAGE_DETAIL_LISTEN") : Loc::getMessage("LIB_SEARCH_PAGE_DETAIL_VIEW"); ?>
            <a href="#modal-media-player" data-toggle="modal" class="btn btn-primary btn-lg pull-right clearfix"><?= $mediaText; ?></a>
            <div class="modal fade" id="modal-media-player" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><?= $arResult['BOOK']['title'] ?></h4>
                        </div>
                        <div class="modal-body">
                            <? if ( $arResult['BOOK']['IS_MEDIA_AUDIO'] === true ):?>
                                <div class="text-center">
                                    <audio controls src="<?=$arResult['BOOK']['MEDIA_URL']?>"></audio>
                                </div>
                            <? endif;

                            if ( $arResult['BOOK']['IS_MEDIA_VIDEO'] === true && in_array('flv', $arResult['BOOK']['fileExt']) ): ?>
                                <object id="media_player" type="application/x-shockwave-flash"
                                        data="/local/components/exalead/search.page.detail/templates/neb/js/uppod.swf" width="567" height="320">
                                    <param name="bgcolor" value="#ffffff" />
                                    <param name="allowFullScreen" value="true" />
                                    <param name="allowScriptAccess" value="always" />
                                    <param name="wmode" value="window" />
                                    <param name="movie" value="/local/components/exalead/search.page.detail/templates/neb/js/uppod.swf" />
                                    <param name="flashvars" value="comment=<?=$arResult['BOOK']['title']?>&amp;m=video&amp;file=<?=$arResult['BOOK']['MEDIA_URL']?>" />
                                </object>
                                <?/*$APPLICATION->IncludeComponent(
                                    "bitrix:player",
                                    ".default",
                                    Array(
                                        "PLAYER_TYPE" => "auto",
                                        "USE_PLAYLIST" => "N",
                                        "PATH" => $arResult['publicurl'],
                                        "PROVIDER" => "http",
                                        "WIDTH" => "640",
                                        "HEIGHT" => "480",
                                        "SKIN_PATH" => "/bitrix/components/bitrix/player/mediaplayer/skins",
                                        "CONTROLBAR" => "bottom",
                                        "WMODE" => "opaque",
                                        "LOGO_POSITION" => "none",
                                        "PLUGINS" => array(0=>"",1=>"",),
                                        "WMODE_WMV" => "window",
                                        "SHOW_CONTROLS" => "Y",
                                        "SHOW_DIGITS" => "Y",
                                        "CONTROLS_BGCOLOR" => "FFFFFF",
                                        "CONTROLS_COLOR" => "000000",
                                        "CONTROLS_OVER_COLOR" => "000000",
                                        "SCREEN_COLOR" => "000000",
                                        "AUTOSTART" => "N",
                                        "REPEAT" => "none",
                                        "VOLUME" => "90",
                                        "MUTE" => "N",
                                        "ADVANCED_MODE_SETTINGS" => "Y",
                                        "BUFFER_LENGTH" => "10",
                                        "DOWNLOAD_LINK_TARGET" => "_self",
                                        "ALLOW_SWF" => "N",
                                        "STREAMER" => "",
                                        "PREVIEW" => "",
                                        "FILE_TITLE" => "",
                                        "FILE_DURATION" => "",
                                        "FILE_AUTHOR" => "",
                                        "FILE_DATE" => "",
                                        "FILE_DESCRIPTION" => "",
                                        "PLAYER_ID" => "",
                                        "DOWNLOAD_LINK" => "",
                                        "ADDITIONAL_WMVVARS" => "",
                                        "SKIN" => "glow.zip",
                                        "LOGO" => "",
                                        "LOGO_LINK" => "",
                                        "ADDITIONAL_FLASHVARS" => ""
                                    )
                                );*/?>
                            <? endif; ?>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        <? endif;?>
        <h1
            <?php if ($arResult['BOOK']['fileExt'] && $arResult['BOOK']['IS_READ'] == 'Y'
                    && (!empty(array_intersect(['pdf', 'epub'], $arResult['BOOK']['fileExt'])))
            ): ?>
                <?php if ($arResult['BOOK']['isprotected'] == 2): ?>
                    class="edition-title__book-header edition-title__book-status edition-title__book-status--close search-result__content-link-title--close"
                    title="<?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_COPYRIGHT')?>"
                <?php elseif ($arResult['BOOK']['isprotected'] == 1): ?>
                    class="edition-title__book-header edition-title__book-status edition-title__book-status--close search-result__content-link-title--close"
                    title="<?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PROTECTED') ?>"
                <?php else: ?>
                    class="edition-title__book-header edition-title__book-status edition-title__book-status--open"
                    title="<?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLIC') ?>"
                <?php endif; ?>
            <?php elseif ( isset($arResult['BOOK']['fileExt']) && "Y" === $arResult['BOOK']['IS_MEDIA']): ?>
                <? if (true === $arResult['BOOK']['IS_MEDIA_VIDEO']) { ?>
                    class="edition-title__video-header edition-title__video-status edition-title__video-status--open" title="<?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_VIEW')?>"
                <? } elseif (true === $arResult['BOOK']['IS_MEDIA_AUDIO']) { ?>
                    class="edition-title__audio-header edition-title__audio-status edition-title__audio-status--open" title="<?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LISTEN')?>"
                <? } ?>
            <?php elseif ('10003' == $arResult['BOOK']['idlibrary']): ?>
                class="edition-title__book-header edition-title__book-status edition-title__book-status--print-elar"
                title="<?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_EKBSON') ?>"
            <?php else: ?>
                class="edition-title__book-header edition-title__book-status edition-title__book-status--cata"
                title="<?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_CATALOG') ?>"
            <?php endif; ?>
        >
            <?= $arResult['BOOK']['title'] ?>
        </h1>
    </div>
    <div class="row edition-detailed-description">
        <div class="edition-detailed-description__about">

            <h3><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ABOUT') ?></h3>

            <dl itemscope itemtype="http://schema.org/Book" data-link="descr">

                <?
                if (!empty($arResult['BOOK']['authorbook'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR') ?>:</dt>
                    <dd>
                        <? foreach ($arResult['BOOK']['authorbook_for_link'] as $key => $sAuthor):
                            if ($key): ?>, <?endif; ?>
                            <a href="/search/?f_field[authorbook]=f/authorbook/<?= urlencode(mb_strtolower(trim($sAuthor)));
                            ?>"><span itemprop="author"><?= trim($sAuthor) ?></span></a><?
                        endforeach; ?>
                    </dd>
                    <?
                }
                ?>

                <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE') ?>:</dt>
                <dd itemprop="name"><?= $arResult['BOOK']['title'] ?></dd>

                <?
                if (!empty($arResult['BOOK']['subtitle'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SUBTITLE') ?>:</dt>
                    <dd><?= $arResult['BOOK']['subtitle'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['responsibility'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_RESPONSIBILITY') ?>:</dt>
                    <dd><?= $arResult['BOOK']['responsibility'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['publishplace'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>:</dt>
                    <dd itemprop="locationCreated"><?= $arResult['BOOK']['publishplace'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['publisher'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH') ?>:</dt>
                    <dd>
                        <a target="_parent" href="/search/?f_field[publisher]=f/publisher/<?
                            echo mb_strtolower($arResult['BOOK']['publisher']) ?>">
                            <span itemprop="publisher"><?= $arResult['BOOK']['publisher'] ?></span>
                        </a>
                    </dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['publishyear'])) {
                    $sYear = BookUtils::getYear($arResult['BOOK']['publishyear']);
                    $sYear = $sYear <= 0 ? $arResult['BOOK']['publishyear'] : str_replace(
                        $sYear, '<span itemprop="datePublished">'.$sYear.'</span>', $arResult['BOOK']['publishyear']
                    );
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR') ?>:</dt>
                    <dd>
                        <a target="_parent"
                           href="/search/?f_publishyear=<?= $arResult['BOOK']['publishyear'] ?>"><?=$sYear?></a>
                    </dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['countpages'])) {
                    $sPages = BookUtils::getCountPages($arResult['BOOK']['countpages']);
                    $sPages = $sPages <= 0 ? $arResult['BOOK']['countpages'] : str_replace(
                        $sPages, '<span itemprop="numberOfPages">'.$sPages.'</span>', $arResult['BOOK']['countpages']
                    );
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES') ?>:</dt>
                    <dd><?= $sPages; ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['series'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE') ?>:</dt>
                    <dd><?= $arResult['BOOK']['series'] ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['isbn'])) {
                    $sIsbn = BookUtils::getIsbn($arResult['BOOK']['isbn']);
                    $sIsbn = empty($sIsbn) ? $arResult['BOOK']['isbn'] : str_replace(
                        $sIsbn, '<span itemprop="isbn">'.$sIsbn.'</span>', $arResult['BOOK']['isbn']
                    );
                    ?>
                    <dt>ISBN:</dt>
                    <dd><?= $sIsbn ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['issn'])) {
                    ?>
                    <dt>ISSN:</dt>
                    <dd><?= $arResult['BOOK']['issn'] ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['contentnotes'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT') ?>:</dt>
                    <dd><?= $arResult['BOOK']['contentnotes'] ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['notes'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE') ?>:</dt>
                    <dd><?= $arResult['BOOK']['notes'] ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['annotation'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ANNOTATION') ?>:</dt>
                    <dd><?= $arResult['BOOK']['annotation'] ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['bbk'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BBK') ?>:</dt>
                    <dd><?= $arResult['BOOK']['bbk'] ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['udk'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UDC') ?>:</dt>
                    <dd><?= $arResult['BOOK']['udk'] ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['udkfull'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UDCFULL') ?>:</dt>
                    <dd>
                        <? foreach ($arResult['BOOK']['udkfull'] as $key => $udkfull): ?>
                            <p>
                                <? unset($r_udkfull); ?>
                                <? foreach ($udkfull as $k => $udk): ?>
                                    <? $r_udkfull .= '/' . urlencode(trim($arResult['BOOK']['r_udkfull'][$key][$k])); ?>
                                    <? if ($k != 0): ?> &gt; <? endif; ?> <a
                                        href="/search/?f_field[udkfull]=f/udkfull<?= $r_udkfull ?>"><?= $udk ?></a>
                                <? endforeach; ?>
                            </p>
                        <? endforeach; ?>
                    </dd>
                <? } ?>

                <?
                if (!empty($arResult['BOOK']['edition'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_EDITION') ?>:</dt>
                    <dd itemprop="bookEdition"><?= $arResult['BOOK']['edition'] ?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['library'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB') ?>:</dt>
                    <dd>
                        <?
                        if (!empty($arResult['BOOK']['LIBRARY_DETAIL_PAGE_URL'])) {
                            ?>
                            <a target="_blank"
                               href="<?= $arResult['BOOK']['LIBRARY_DETAIL_PAGE_URL'] ?>"><?= $arResult['BOOK']['library'] ?></a>
                            <?
                        } else {
                            ?>
                            <?= $arResult['BOOK']['library'] ?>
                            <?
                        }
                        ?>
                    </dd>
                    <?
                }

                if (!empty( $arResult['BOOK']['lang'])) {
                    ?>
                    <dt><?= Loc::getMessage("LIB_SEARCH_PAGE_DETAIL_LANGUAGE")?>:</dt>
                    <dd itemprop="inLanguage"><?= $arResult['BOOK']['lang'];?></dd>
                    <?
                }
                ?>

                <?
                if (!empty($arResult['BOOK']['idbook'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_INCLUDED_IN') ?>:</dt>
                    <dd>
                        <a itemprop="isPartOf" target="_blank" href="/catalog/<?= $arResult['BOOK']['idbook'] ?>/">
                            <?= $arResult['BOOK']['namebook'] ?>
                        </a>
                    </dd>
                    <?
                }
                ?>
                <? /** ======================================= Архивы =========================================== */ ?>
                <?
                if (!empty($arResult['BOOK']['fundnumber'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_FUND_NUMBER') ?>:</dt>
                    <dd><?= $arResult['BOOK']['fundnumber'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['fundname'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_FUND_NAME') ?>:</dt>
                    <dd><?= $arResult['BOOK']['fundname'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['inventorynumber'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_INVENTORY_NUMBER') ?>:</dt>
                    <dd><?= $arResult['BOOK']['inventorynumber'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['inventoryname'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_INVENTORY_NAME') ?>:</dt>
                    <dd><?= $arResult['BOOK']['inventoryname'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['unitnumber'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UNIT_NUMBER') ?>:</dt>
                    <dd><?= $arResult['BOOK']['unitnumber'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['unitname'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UNIT_NAME') ?>:</dt>
                    <dd><?= $arResult['BOOK']['unitname'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['dates'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_DATES') ?>:</dt>
                    <dd><?= $arResult['BOOK']['dates'] ?></dd>
                    <?
                }
                ?>
                <? /** ========================================== Музеи ========================================= */ ?>

                <?
                if (!empty($arResult['BOOK']['description'])) {
                    $arResult['BOOK']['META']['SCHEMA_DESCRIPTION'] = $arResult['BOOK']['description'];
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_DESCRIPTION') ?>:</dt>
                    <dd itemprop="description"><?= $arResult['BOOK']['description'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['material'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_MATERIAL') ?>:</dt>
                    <dd itemprop="material"><?= $arResult['BOOK']['material'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['size'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SIZE') ?>:</dt>
                    <dd><?= $arResult['BOOK']['size'] ?></dd>
                    <?
                }
                ?>
                <? /** ===================================== Правообладатель ===================================== */ ?>

                <?
                if (!empty($arResult['BOOK']['mpk'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_MPK') ?>:</dt>
                    <dd><?= $arResult['BOOK']['mpk'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['declarant'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_DECLARANT') ?>:</dt>
                    <dd><?= $arResult['BOOK']['declarant'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['patentholder'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PATENTHOLDER') ?>:</dt>
                    <dd itemprop="copyrightHolder"><?= $arResult['BOOK']['patentholder'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['representative'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_REPRESENTATIVE') ?>:</dt>
                    <dd><?= $arResult['BOOK']['representative'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['publish_date'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_DATE') ?>:</dt>
                    <dd><?= $arResult['BOOK']['publish_date'] ?></dd>
                    <?
                }
                ?>
                <?
                if (!empty($arResult['BOOK']['patentcode'])) {
                    ?>
                    <dt><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PATENTCODE') ?>:</dt>
                    <dd><?= $arResult['BOOK']['patentcode'] ?></dd>
                    <?
                }
                ?>

                <? if (!empty($arResult['BOOK']['META']['SCHEMA_DESCRIPTION'])): // Yandex required field ?>
                    <meta itemprop="description" content="<?=$arResult['BOOK']['META']['SCHEMA_DESCRIPTION']?>" />
                <? endif; ?>

                <? foreach ($arResult['BOOK']['parts']['ITEMS'] as $arPart): // Parts of book fields ?>
                    <link itemprop="hasPart" href="/catalog/<?=$arPart['url']?>/" />
                <? endforeach; ?>
            </dl>
        </div>

        <?= $APPLICATION->IncludeComponent(
            'exalead:same.list.short',
            'neb',
            Array(
                'PARENT_ID'  => $arResult['BOOK']['idparent2'],
                'CURRENT_ID' => $arResult['BOOK']['id']
            )
        ); ?>
    </div>

    <?php if (!empty($arResult['BOOK']['bbkfull'])): ?>
        <h3>
            <?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_CATEGORIES')?>
        </h3>
        <p>
            <?php foreach ($arResult['BOOK']['bbkfull'] as $key => $bbkfull): ?>
                <p>
                    <?php $r_bbkfull = ''; ?>

                    <?php foreach ($bbkfull as $k => $bbk): ?>
                        <?php $r_bbkfull .= '/' . urlencode(
                                trim($arResult['BOOK']['r_bbkfull'][$key][$k])
                            ); ?>

                        <? if ($k != 0): ?> &gt; <?php endif; ?>
                        <a href="/search/?f_field[bbkfull]=f/bbkfull<?= $r_bbkfull
                        . '&' . nfDeleteParam(
                            array('bbkfull', 'dop_filter',
                                  'PAGEN_' . $arResult['NavNum'])
                        ) ?>">
                            <?php echo $bbk; ?>
                        </a>
                    <?php endforeach; ?>
                </p>
            <?php endforeach; ?>
        </p>
    <?php endif; ?>

    <script
        src="//api-maps.yandex.ru/2.0-stable/?load=package.full&amp;lang=ru-RU"
        type="text/javascript"></script>
    <dl class="additional-expandable" data-list-expandable>
        <?= $APPLICATION->IncludeComponent(
            'exalead:book.parts',
            'expandable-item',
            Array(
                'BOOK_ID' => $arResult['BOOK']['id'],
            )
        ); ?>
        <?php if ('library' === $arResult['BOOK']['librarytype']): ?>

            <?
                /*if ($USER->IsAdmin() || nebUser::checkElar() || EXALEAD_DEBUG_MODE) {
            ?>
                <details>
                    <summary>$arResult[]</summary>
                    <pre>
                        <div style="border: 1px solid #999; padding: 1ex;">
                            <?
                                if ('Y' === $arResult['BOOK']['IS_READ'] && $arResult['BOOK']['isprotected'] == 0) {?>
                                    2) Карточка открытого издания с электронной версией
                                    (как сейчас - карта, заголовок блока "Ближайшая библиотека с бумажным экземпляром издания")
                                <?}
                                elseif ('Y' === $arResult['BOOK']['IS_READ'] && $arResult['BOOK']['isprotected'] == 1) {?>
                                    3) Карточка закрытого издания с электронной версией
                                    (карта со всеми библиотеками как на странице "электронные читальные залы",
                                    заголовок блока "Ближайший электронный читальный зал")
                                <?}
                                elseif ($arResult['BOOK']['IS_MEDIA_VIDEO'] !== true && $arResult['BOOK']['IS_MEDIA_AUDIO'] !== true) {?>
                                    1) Карточка издания без электронной версии?
                                    (как сейчас - карта, заголовок блока "Ближайшая библиотека с бумажным экземпляром издания")
                                <?}
                            ?>
                        </div>
                        <div style="border: 1px solid #0f0; padding: 1ex;">
                            <?php if ($arResult['BOOK']['fileExt'] && in_array('pdf', $arResult['BOOK']['fileExt']) && $arResult['BOOK']['IS_READ'] == 'Y'): ?>
                                if-1
                                <?php if ($arResult['BOOK']['isprotected'] == 2): ?>
                                    -if edition-title__book-status--close
                                <?php elseif ($arResult['BOOK']['isprotected'] == 1): ?>
                                    -elseif edition-title__book-status--close
                                <?php else: ?>
                                    -else edition-title__book-status--open
                                <?php endif; ?>
                            <?php elseif ( isset($arResult['BOOK']['fileExt']) && "Y" === $arResult['BOOK']['IS_MEDIA']): ?>
                                elseif-1
                                <? if ($arResult['BOOK']['IS_MEDIA_VIDEO'] === true) { ?>
                                    -if edition-title__video-status--open
                                <? } elseif ($arResult['BOOK']['IS_MEDIA_AUDIO'] === true) { ?>
                                    -elseif edition-title__audio-status--open
                                <? } ?>
                            <?php elseif ('10003' == $arResult['BOOK']['idlibrary']): ?>
                                elseif-2 edition-title__book-status--print-elar
                            <?php else: ?>
                                else
                            <?php endif; ?>
                        </div>
                        <!--
                            ['BOOK']['isprotected'] = 1

                        -->
                        <?php print_r($arResult);?>
                    </pre>
                </details>
            <?
                }*/
            ?>

             <?
                if ('Y' === $arResult['BOOK']['IS_READ'] && $arResult['BOOK']['isprotected'] == 1) {?>
                    <dt class="link-nearest-library">
                        <span><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_NEAREST_BIBLIO_I') ?></span>
                    </dt>
                    <dd>
                        <div class="library-map">
                            <div class="ymap" id="ymap" style="width:100%;height:400px;"
                                 data-zoom="10"
                                 data-original-path="<?php echo $arResult['BOOK']['NEAREST_LIBRARY_URL'] ?>"
                                 data-path="/local/tools/map/workplaces.php"
                                 data-show-nearly="1">
                            </div>
                                    <span class="marker_lib hidden">
                                        <a href="#" class="b-elar_name_txt"></a><br/>
                                        <span class="b-elar_status"></span><br/>
                                        <span class="b-map_elar_info">
                                            <span
                                                class="b-map_elar_infoitem addr"><span><?= Loc::getMessage(
                                                        'LIB_SEARCH_PAGE_DETAIL_ADDRESS'
                                                    ); ?>:</span></span><br/>
                                            <span
                                                class="b-map_elar_infoitem graf"><span><?= Loc::getMessage(
                                                        'LIB_SEARCH_PAGE_DETAIL_WORK'
                                                    ); ?>:</span></span>
                                        </span>
                                        <span class="b-mapcard_act clearfix">
                                            <span
                                                class="right neb b-mapcard_status"><?= Loc::getMessage(
                                                    'LIB_SEARCH_PAGE_DETAIL_USER'
                                                ); ?></span>
                                            <a href="#"
                                               class="button_mode"><?= Loc::getMessage(
                                                    'LIB_SEARCH_PAGE_DETAIL_URL'
                                                ); ?></a>
                                        </span>
                                    </span>
                        </div>
                    </dd>
                <? }
                elseif ("Y" !== $arResult['BOOK']['IS_MEDIA']) { ?>
                    <dt class="link-nearest-library">
                        <span><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_NEAREST_BIBLIO_PAPER') ?></span>
                    </dt>
                    <dd>
                        <div class="library-map">
                            <div class="ymap" id="ymap" style="width:100%;height:400px;"
                                 data-zoom="10"
                                 data-path="<?php echo $arResult['BOOK']['NEAREST_LIBRARY_URL'] ?>"
                                 data-show-nearly="1">
                            </div>
                                    <span class="marker_lib hidden">
                                        <a href="#" class="b-elar_name_txt"></a><br/>
                                        <span class="b-elar_status"></span><br/>
                                        <span class="b-map_elar_info">
                                            <span
                                                class="b-map_elar_infoitem addr"><span><?= Loc::getMessage(
                                                        'LIB_SEARCH_PAGE_DETAIL_ADDRESS'
                                                    ); ?>:</span></span><br/>
                                            <span
                                                class="b-map_elar_infoitem graf"><span><?= Loc::getMessage(
                                                        'LIB_SEARCH_PAGE_DETAIL_WORK'
                                                    ); ?>:</span></span>
                                        </span>
                                        <span class="b-mapcard_act clearfix">
                                            <span
                                                class="right neb b-mapcard_status"><?= Loc::getMessage(
                                                    'LIB_SEARCH_PAGE_DETAIL_USER'
                                                ); ?></span>
                                            <a href="#"
                                               class="button_mode"><?= Loc::getMessage(
                                                    'LIB_SEARCH_PAGE_DETAIL_URL'
                                                ); ?></a>
                                        </span>
                                    </span>
                        </div>
                    </dd>

             <? } ?>

        <?php endif; ?>

        <?php if (!empty($arResult['readWith']['ITEMS'])): // вместе с этим изданием таже ищут ?>
            <dt><span><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_RELEVANT_BOOKS') ?></span></dt>
            <dd>
                <div class="edition-parts-container">
                    <table class="neb-table">
                        <tr>
                            <th><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR') ?></th>
                            <th><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE') ?></th>
                            <th><?= str_replace(' ', '<br />', Loc::getMessage(
                                    'LIB_SEARCH_PAGE_DETAIL_PUBLISH_DATE'
                                )) ?></th>
                            <th><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB') ?></th>
                            <th><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_READ') ?></th>
                        </tr>
                        <?php foreach ($arResult['readWith']['ITEMS'] as $book): ?>
                            <tr>
                                <td>
                                    <?php if (isset($book['authorbook'])
                                        && $book['authorbook']
                                    ): echo $book['authorbook']; endif; ?>
                                    &nbsp;
                                </td>
                                <td>
                                    <?php if (isset($book['title'])
                                        && $book['title']
                                    ): echo $book['title']; endif; ?>
                                </td>
                                <td>
                                    <?php if (isset($book['publishyear'])
                                        && $book['publishyear']
                                    ): echo $book['publishyear']; endif; ?>
                                </td>
                                <td>
                                    <?php if (isset($book['library'])
                                        && $book['library']
                                    ): echo $book['library']; endif; ?>
                                </td>
                                <td>
                                    <? if (!empty($book['VIEWER_URL']) && $book['IS_READ'] == 'Y'): ?>
                                        <a
                                            class="btn btn-primary btn-lg pull-right boockard-read-button clearfix"
                                            href="javascript:void(0);"
                                            data-link="<?= $book['readBookId'] ?>"
                                            data-options="<?php echo htmlspecialchars(
                                                json_encode($book['viewerOptions']
                                                ), ENT_QUOTES, 'UTF-8'
                                            ) ?>"><?= Loc::getMessage(
                                                'LIB_SEARCH_PAGE_DETAIL_READ'
                                            ) ?></a>
                                    <? endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </dd>
        <?php endif; ?>
    </dl>

    <div class="row">
        <div class="col-md-6 social-section">
            <?$APPLICATION->IncludeComponent(
                "neb:main.share",
                ".default",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "DATA_TITLE"         => $arResult['BOOK']['META']['TITLE'],
                    "DATA_DESCRIPTION"   => $arResult['BOOK']['META']['DESCRIPTION'],
                    "THEME_SERVICES" => array(
                        0 => "vkontakte",
                        1 => "facebook",
                        2 => "odnoklassniki",
                    )
                ),
                false
            );
            ?>
        </div>
        <div class="col-md-6 book-control-section">
            <? $arUsrGr = array_intersect(['admin', 'operator', 'operator_access2publications'], $arResult['USER_GROUPS']);
            if (!empty($arResult['BOOK']['fileExt']) && !empty($arUsrGr)) { ?>
                <a class="btn btn-default btn-sm book-close-button"
                   data-close="<?= $arResult['BOOK']['AccessType'] == 'open' ? '1' : '0'?>"
                   data-book-id="<?= $arResult['BOOK']['readBookId'] ?>"
                   href="#">
                    <? if ($arResult['BOOK']['AccessType'] == 'open'):?>
                        <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_CLOSE_THIS_BOOK') ?>
                    <? else: ?>
                        <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_OPEN_THIS_BOOK') ?>
                    <? endif; ?>
                </a>
            <? } ?>
            <? if ($arResult['nebUser']['ID'] > 0) { ?>
                <a class="btn btn-default btn-sm book-error-button" data-is-authorized="<?= $arResult['isAuthorized'] ? "1" : "0"; ?>" data-book-id="<?= $arResult['BOOK']['readBookId'] ?>" href="#">
                    <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_REPORT_AN_ERROR') ?>
                </a>
            <? } ?>
        </div>
    </div>

    <?if (empty($arResult['BOOK']['idbook']) && 'Y' !== $arResult['BOOK']['IS_READ'] && $arResult['nebUser']['ID'] > 0) { ?>
        <p>
            <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAPER_TO_EBOOK') ?>
        </p>
        <a href="#" class="digitize-order-button btn btn-primary"
           data-book-id="<?= $arResult['BOOK']['id'] ?>"
           data-parent-id="<?= $arResult['BOOK']['idparent2'] ?>"><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ORDER') ?></a>

        <script type="application/javascript">
            $(function () {
                $('.digitize-order-button').click(function (e) {
                    e.preventDefault();
                    var requestParams = {
                        book_id: $(this).data('book-id'),
                        idparent: $(this).data('parent-id'),
                        sessid: BX.message('bitrix_sessid'),
                        token: getUserToken(),
                        comment: ''
                    };
                    var addDigitizationPlan = function (request, popup) {
                        $.ajax({
                            url: '/rest_api/book/plan-digitization/',
                            method: 'POST',
                            dataType: 'json',
                            data: request,
                            success: function (data) {
                                popup.popup.modal('hide');
                                var orderIsDone = window.neb.objectPathSearch(data, 'data.orderIsDone');
                                if(true === orderIsDone) {
                                    FRONT.simplePopup(function () {
                                        var ok = $('<button type="button" class="btn btn-primary">Ок</button>');
                                        var message = $('<p class="text-info">' +
                                            'Данная книга уже оцифрована библиотекой, вы можете <a href="/profile/digitization/">посмотреть результат в профиле</a>' +
                                            '</p>');

                                        this.applyHeader();
                                        this.applyFooter();
                                        this.popupBody.append(message);
                                        ok.click(function () {
                                            this.popup.modal('hide');
                                        }.bind(this));
                                        this.popupFooter.append(ok);
                                    });
                                }
                            },
                            error: function (xhr) {
                                var message = window.neb.objectPathSearch(xhr, 'responseJSON.errors.message');
                                if (message == "Не передан обязательный параметр 'comment'") {
                                    message = "Комментарий обязателен для заполнения"
                                }
                                popup.errorElement.text(
                                    message
                                        ? message
                                        : 'Ошибка добавления плана на оцифровку, попробуйте позднее'
                                );
                                popup.errorElement.show();
                            }
                        });
                    };
                    FRONT.simplePopup(function () {
                        var textarea = $('<textarea class="form-control" rows="5"></textarea>');
                        var ok = $('<button type="button" class="btn btn-primary">Добавить</button>');
                        var cancel = $('<button type="button" class="btn btn-primary">Отмена</button>');
                        var message = $('<p class="text-info">' +
                            'Напишите, для чего Вам необходимо данное издание.<br>' +
                            'Ваш комментарий может ускорить процесс перевода издания в электронный вид.' +
                            '</p>');
                        this.errorElement = $('<p class="popup-error alert alert-danger"></p>');
                        this.errorElement.hide();

                        var request = $.extend({}, requestParams);

                        this.applyHeader();
                        this.applyFooter();
                        this.popupBody.append(message);
                        this.popupBody.append(textarea);
                        this.popupBody.prepend(this.errorElement);
                        ok.click(function () {
                            request.comment = textarea.val();
                            addDigitizationPlan(request, this);
                        }.bind(this));
                        cancel.click(function () {
                            this.popup.modal('hide');
                        }.bind(this));
                        this.popupFooter.append(ok);
                        this.popupFooter.append(cancel);
                    });
                });
            });
        </script>
    <? } ?>
