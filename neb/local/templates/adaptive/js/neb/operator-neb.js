$(function () {
    var messagePoupUp = function (message) {
        FRONT.simplePopup(function() {
            var ok = $('<button type="button" class="btn btn-primary">Ок</button>');
            this.applyHeader();
            this.applyFooter();
            this.popupBody.append('<p class="text-info">' + message + '</p>');
            ok.click(function () {
                this.popup.modal('hide');
            }.bind(this));
            this.popupFooter.append(ok);
        });
    };
    var closeBook = function (params, simplePoupUp) {
        params.token = getUserToken();
        $.ajax({
            method: 'PUT',
            url: '/rest_api/book/close/',
            dataType: 'json',
            data: params,
            success: function (result) {
                var data = result.data;
                simplePoupUp.popup.modal('hide');
                if (!result.success) {
                    messagePoupUp('Не удалось выполнить действие');
                } else if (data && data.result) {
                    if ('saved_to_db' === data.result) {
                        messagePoupUp('Заявка сохранена, обновление статуса книги на сайте НЭБ.РФ произойдет в течении 24 часов!');
                    } else if ('sent_to_library' === data.result) {
                        messagePoupUp('Отправлено сообщение администратору библиотеки!');
                    }
                } else {
                    messagePoupUp('Не удалось выполнить действие');
                }
            },
            error: function (result) {
                simplePoupUp.popup.modal('hide');
                var message = neb.objectPathSearch(result, 'responseJSON.errors.message');
                if(message) {
                    messagePoupUp(message);
                } else {
                    messagePoupUp('Не удалось выполнить действие');
                }
            }
        });
    };
    $('.book-close-button').click(function (e) {
        var requestParams = {
            "book_id": $(this).data('book-id'),
            "close": $(this).data('close')
        };
        e.preventDefault();
        $.ajax({
            url: '/rest_api/book/close/',
            dataType: 'json',
            data: requestParams,
            success: function (result) {
                var data = result.data;
                if (data && data.hasOwnProperty('externalLibrary')) {
                    if (data.externalLibrary) {
                        FRONT.simplePopup(function () {
                            var textarea = $('<textarea class="form-control" rows="5"></textarea>');
                            var ok = $('<button type="button" class="btn btn-primary">Отправить</button>');
                            var cancel = $('<button type="button" class="btn btn-primary">Отмена</button>');
                            var request = $.extend({}, requestParams);

                            this.applyHeader();
                            this.applyFooter();
                            this.popupBody.append('<p class="text-info">Введите комментарий для администратора библиотеки:</p>');
                            this.popupBody.append(textarea);
                            ok.click(function () {
                                request.comment = textarea.val();
                                closeBook(request, this);
                            }.bind(this));
                            cancel.click(function () {
                                this.popup.modal('hide');
                            }.bind(this));
                            this.popupFooter.append(ok);
                            this.popupFooter.append(cancel);
                        });
                    } else {
                        FRONT.simplePopup(function () {
                            this.applyHeader();
                            this.applyFooter();
                            this.popupBody.append('<p class="text-info">Вы уверены что хотите '
                                + (requestParams.close ? 'закрыть' : 'открыть') + ' данную книгу?</p>');
                            var ok = $('<button type="button" class="btn btn-primary">Да</button>');
                            ok.click(function () {
                                closeBook(requestParams, this);
                            }.bind(this));
                            var cancel = $('<button type="button" class="btn btn-primary">Нет</button>');
                            cancel.click(function () {
                                this.popup.modal('hide');
                            }.bind(this));
                            this.popupFooter.append(ok);
                            this.popupFooter.append(cancel);
                        });
                    }
                }
            },
            error: function () {

            }
        });
    });
});