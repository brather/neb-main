<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__ . '/template.php');
if (empty($arResult['ITEMS']))
    return false;

if (!empty($arResult['ITEMS'])) {

    ?>
    <a name="lk_popular"></a>
    <?
    $APPLICATION->IncludeComponent(
        "exalead:search.page.viewer.left",
        "popular.book",
        Array(
            "PARAMS" => array_merge($arParams, array('TITLE_MODE_BLOCK' => 'Отобразить книги плиткой', 'TITLE_MODE_LIST' => 'Отобразить книги списком')),
            "RESULT" => $arResult,
        ),
        $component
    );
    ?>
    <?
}
