<?
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_COMMENT"] = "Добавить комментарий";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_DESRIBE"] = "Обязательно опишите почему вам нужна данная книга";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_REQ"] = "Поле обязательно для заполнения";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_FASTER"] = "Ваш комментарий может ускорить процесс оцифровки";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_ADD"] = "Добавить";
$MESS["BOOKS_DIGITAL_QUERY_ADD_FORM_CANCEL"] = "Отмена";
?>
