<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151006170431 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
ALTER TABLE tbl_common_biblio_card
ADD COLUMN view_price DOUBLE DEFAULT 0;
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->connection->executeQuery(
            <<<SQL
ALTER TABLE tbl_common_biblio_card
DROP COLUMN view_price;
SQL
        );
    }
}
