<?php

$MESS['MARKUP_LOG_TITLE']       = 'Лог изменения статусов издания';
$MESS['MARKUP_LOG_DESCRIPTION'] = 'Описание издания';

$MESS['MARKUP_LOG_AUTHOR']      = 'Автор';
$MESS['MARKUP_LOG_NAME']        = 'Название';
$MESS['MARKUP_LOG_STARTPAGE']   = 'Начало';
$MESS['MARKUP_LOG_ENDPAGE']     = 'Конец';
$MESS['MARKUP_LOG_POSITION']    = 'Позиция';
$MESS['MARKUP_LOG_RANGEPAGE']   = 'Расположение';
$MESS['MARKUP_LOG_STATUS']      = 'Статус';
$MESS['MARKUP_LOG_MODERATION']  = 'Статус модерации';
$MESS['MARKUP_LOG_DATE']        = 'Дата';
$MESS['MARKUP_LOG_LOGIN']       = 'Логин';

$MESS['MARKUP_LOG_STATUS_ADDED']         = 'Добавлено';
$MESS['MARKUP_LOG_STATUS_CONSIDERATION'] = 'Утверждение';
$MESS['MARKUP_LOG_STATUS_CONFIRMED']     = 'Утверждено';
$MESS['MARKUP_LOG_STATUS_DELETED']       = 'Удалено';