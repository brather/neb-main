<?

/**
 * Трек полного количества изданий библиотек из Exalead в Битрикс
 * Перенесен в \Neb\Main\Agents\StatsSaver::saveLibraryBooksCount();
 */

if ('cli' !== php_sapi_name()) {
	die('Only CLI');
}
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

$_SERVER["DOCUMENT_ROOT"] = str_replace('_statistics', '', __DIR__);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\NotaExt\Iblock\Element as IB;
CModule::IncludeModule("nota.exalead");
use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;

// создание таблицы для статистики
$query="
CREATE TABLE IF NOT EXISTS `neb_stat_edition` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `ID_BITRIXDB` int(10) NOT NULL,
  `ID_EXALED` int(10) NOT NULL,
  `TITLE_LIB` text NOT NULL,
  `COUNT_EDITION` int(8) NOT NULL,
  `COUNT_EDITION_DIG` int(8) NOT NULL,
  `DATE_STAT` datetime NOT NULL,
  `ACTIVE_IN_BITRIX` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`)
)";
$DB->Query($query, false, $err_mess.__LINE__);

$query = "SELECT `DATE_STAT` FROM	`neb_stat_edition` WHERE 	`DATE_STAT`= CURDATE()";
$sql = $DB->Query($query, false, $err_mess.__LINE__);

if (intval($sql->SelectedRowsCount())>0)
exit;

// все зарегистрированные в НЭБ библиотеки
$result = IB::getList(array(), 0, array("PROPERTY_SCHEDULE", "PROPERTY_LIBRARY_LINK", "PROPERTY_STATUS"));

$count_all = 0;
$count_dig_all = 0;

foreach($result['ITEMS'] as $lib):
	$lib_exalead_id = $lib['PROPERTY_LIBRARY_LINK_VALUE'];
	
	/* записей каталогов */
	$arParams['q'] = '#all AND idlibrary:'.$lib_exalead_id;
	$query = new \Nota\Exalead\SearchQuery($arParams['q']);	
	$query->setPageLimit(1);
	$query->setParam('sl', 'sl_statistic3');
				
	$client = new \Nota\Exalead\SearchClient();
	
	$result = $client->getResult($query);
	$count = $result['COUNT'];
	if ($count > 0) $count_all += $count;
	
	$arParams['q'] = '#all AND idlibrary:'.$lib_exalead_id.' AND filesize>0';
	$query = new \Nota\Exalead\SearchQuery($arParams['q']);
	$query->setPageLimit(1);
	$query->setParam('sl', 'sl_statistic3');

	$client = new \Nota\Exalead\SearchClient();
	$result = $client->getResult($query);
	$count_dig = $result['COUNT'];
	if ($count_dig > 0) $count_dig_all += $count_dig;
	
	$arFields = array(
					'DATE_STAT' => date("d.m.Y", time()),
					'ID_EXALED' => $lib_exalead_id,
					'ID_BITRIXDB' => $lib['ID'],
					'COUNT_EDITION' => $count,
					'COUNT_EDITION_DIG' => $count_dig,
					'TITLE_LIB' => $lib['NAME'],
					'ACTIVE_IN_BITRIX' => 'Y'
				);
				
	$arInsert = $DB->PrepareInsert("neb_stat_edition", $arFields, "form");
	$strSql = "INSERT INTO neb_stat_edition (".$arInsert[0].") VALUES (".$arInsert[1].")";
	$DB->Query($strSql, false, $err_mess.__LINE__);
	
endforeach;

echo "Записей каталогов: ".$count_all;
echo "Электронных книг: ".$count_dig_all;

/* записей каталогов */
/*
$query = new \Nota\Exalead\SearchQuery('#all');	
$query->setPageLimit(1);
$query->setParam('sl', 'sl_statistic3');
				
$client = new \Nota\Exalead\SearchClient();
$result = $client->getResult($query);	
	
if(intval($result['COUNT']) > 0)
	\COption::SetOptionString('nota.exalead','book_count_catalog', intval($result['COUNT']));
*/
/* электронных книг */
/*
$query = new \Nota\Exalead\SearchQuery('filesize>0');
$query->setPageLimit(1);
$query->setParam('sl', 'sl_statistic3');

$client = new \Nota\Exalead\SearchClient();
$result = $client->getResult($query);
			
if(intval($result['COUNT']) > 0)
	\COption::SetOptionString('nota.exalead','book_count_total', intval($result['COUNT']));
*/
