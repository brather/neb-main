<?php
$MESS['DOWNLOAD_BOOK_NOT_AUTHORIZED'] = 'Please log in to access the file';
$MESS['DOWNLOAD_BOOK_NOT_PERMISSION'] = 'You do not have enough permissions for the file';
$MESS['DOWNLOAD_BOOK_NOT_PATH'] = 'Do not specify the path to the file download';