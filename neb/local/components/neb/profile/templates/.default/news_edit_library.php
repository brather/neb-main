<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__ . '/main_library.php'); // Для того, что б не копировать один и тот же текст
Loc::loadMessages(__FILE__);

$APPLICATION->IncludeComponent(
    "neb:library.news.edit",
    ".default",
    array(
        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => "3",
        "NEWS_ID" => $arResult['VARIABLES']['ID'],
        "URL_LIST" => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['news'],
        "URL_DETAIL" => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['news']
    ),
    false
);