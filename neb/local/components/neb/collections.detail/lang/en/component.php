<?php
$MESS['BOOK_UPDATE_MESS_SUCCESS'] = 'Update sort book was success!';
$MESS['BOOK_UPDATE_MESS_ERROR'] = 'Error! Book has not been updated.';
$MESS['BOOK_UPDATE_IBLOCK_ID_NOT_DEFINED'] = 'Error! IBLOCK_ID is not defined';