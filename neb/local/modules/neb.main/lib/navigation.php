<?php
/**
 * User: agolodkov
 * Date: 05.08.2015
 * Time: 10:53
 */

namespace Neb\Main;

/**
 * Class Navigation
 *
 * @package Neb\Main
 */
class Navigation
{
    protected $_navParams
        = array(
            'totalCount'       => 0,
            'navPageNumber'    => 1,
            'navNum'           => 1,
            'pageElementCount' => 15,
        );

    protected $_componentName = 'bitrix:system.pagenavigation';
    protected $_templateName = '';

    /**
     * @var \CDBResult
     */
    protected $_resultObject;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->buildString();
    }

    /**
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        foreach ($options as $key => $value) {
            switch ($key) {
                case 'nav':
                    $this->_navParams = is_array($value)
                        ? array_merge($this->_navParams, $value)
                        : array();
                    break;
                case 'component':
                    $this->_componentName = $value;
                    break;
                case 'template':
                    $this->_templateName = $value;
                    break;
            }
        }
        if ($pageNum = $_REQUEST['PAGEN_' . $this->_navParams['navNum']]) {
            $this->_navParams['navPageNumber'] = $pageNum;
        }
    }


    /**
     * @return \CDBResult
     */
    public function buildResult()
    {
        $this->_resultObject = new \CDBResult();
        $this->_resultObject->NavPageCount = ceil(
            $this->_navParams['totalCount']
            / $this->_navParams['pageElementCount']
        );
        $this->_resultObject->NavPageNomer = $this->_navParams['navPageNumber'];
        $this->_resultObject->NavNum = $this->_navParams['navNum'];
        $this->_resultObject->NavPageSize
            = $this->_navParams['pageElementCount'];
        $this->_resultObject->NavRecordCount = $this->_navParams['totalCount'];

        return $this->_resultObject;
    }


    /**
     * @return string
     */
    public function buildString()
    {
        ob_start();
        global $APPLICATION;
        /** @var \CMain $APPLICATION */
        $APPLICATION->IncludeComponent(
            'bitrix:system.pagenavigation', '', array(
                'NAV_RESULT' => $this->buildResult(),
            )
        );

        return @ob_get_clean();
    }

    /**
     * @param array $parameters
     */
    public function setParameters($parameters)
    {
        $this->_navParams = $parameters;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->_navParams;
    }
}