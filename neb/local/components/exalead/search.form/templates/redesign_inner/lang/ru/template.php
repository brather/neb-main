<?php

$MESS["SEARCH_FORM_DEBUG"] = 'Режим отладки';
$MESS["SEARCH_FORM_BUTTON"] = "Найти";

$MESS["SEARCH_FORM_SIMPLE_SEARCH"] = 'Простой поиск';
$MESS["SEARCH_FORM_MORE_SIMPLE_PLACEHOLDER"] = ''; //Пушкин, сказка о царе Салтане
$MESS["SEARCH_FORM_COLLECTIONS"] = "Искать по коллекциям";
$MESS["SEARCH_FORM_CHOISE_COLLECTION"] = 'выберите коллекцию';
$MESS["SEARCH_FORM_MORE"] = 'Еще';
$MESS["SEARCH_FORM_MORE_MOBILE"] = 'Выбрать коллекции';

$MESS["SEARCH_FORM_DOUBLE_SEARCH"] = 'Поиск изданий по автору и/или названию';
$MESS["SEARCH_FORM_FAQ_ANCHOR"] = '?';
$MESS["SEARCH_FORM_FAQ_TITLE"] = 'Рекомендации по поиску';
$MESS['SEARCH_FORM_AUTHOR'] = 'автор';
$MESS["SEARCH_FORM_AUTHOR_PLACEHOLDER"] = ''; //Пушкин
$MESS['SEARCH_FORM_TITLE'] = 'название';
$MESS["SEARCH_FORM_TITLE_PLACEHOLDER"] = ''; //Сказка о царе Салтане
$MESS["SEARCH_FORM_EXTENDED_SEARCH"] = "Расширенный поиск";
$MESS["SEARCH_FORM_EXTENDED_SEARCH_BY"] = "Поиск осуществлен по";

$MESS["SEARCH_FORM_CUSTOMIZE"] = 'Изменить';