<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 */
?>

<a class="btn btn-primary" target="_blank"
   href="/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=4&type=library&lang=ru">
    <?= Loc::getMessage('ADD_BOOK') ?>
</a>

<div class="reader-search clearfix">
    <h3>Поиск библиотек</h3>
    <form>
        <input type="text" name="LIBRARY_NAME" class="b-text reader-search__input"
               value="<?= $arParams['LIBRARY_NAME'] ?>" placeholder="Название библиотеки" />
        <button class="btn btn-primary" type="submit"  value="<?= Loc::getMessage('FIND') ?>"><?= Loc::getMessage('FIND') ?></button>
    </form>
</div>

<h3>Список библиотек</h3>

<div class="lk-table">
    <div class="lk-table__column" style="width:80%"></div>
    <div class="lk-table__column" style="width:20%"></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind"><?= Loc::getMessage('NAME_TITLE') ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('ACTION') ?></li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['items'] as $library) { ?>
            <ul class="lk-table__row">
                <li class="lk-table__col">
                    <a target="_blank" href="<?= $library['DETAIL_URL'] ?>">
                        <?= $library['NAME'] ?>
                    </a>
                </li>
                <li class="lk-table__col">
                    <a href="/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=4&type=library&lang=ru&ID=<?= $library['ID'] ?>"
                       target="_blank">
                        <?= Loc::getMessage('EDIT') ?>
                    </a>
                </li>
            </ul>
        <? } ?>
    </section>
    <?= $arResult['nav'] ?>
</div>