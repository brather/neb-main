
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			
</section>
<section class="innersection innerwrapper clearfix searchempty">
	<nav class="b-commonnav noborder">
		<a href="#" title="новые" class="current">новые</a>
		<a href="#" title="популярные" >популярные</a>
		<a href="#" title="рекомендованные" >рекомендованные</a>
	</nav>
	<div class="b-collectionpage rel">
			<h1>коллекции</h1>
			<div class="b-collection_list mode">
				<div class="b-collection-item rel renumber">
					<span class="num">1.</span>
					<h2><a href="blind_collection.php" title="Серебряный век: быт, нравы, моды, знаменитости">Серебряный век: быт, нравы, моды, знаменитости</a></h2>
					<div><span class="button_mode b-bookincoll">15 книг в подборке</span></div>
					<div class="b-result_sorce_info"><em>Автор:</em> Рязанская областная библиотека</div>
				</div><!-- /.b-collection-item -->
				<div class="b-collection-item rel renumber">
					<span class="num">2.</span>
					<h2><a href="blind_collection.php">Серебряный век: быт, нравы, моды, знаменитости</a></h2>
					<div><span class="button_mode b-bookincoll">15 книг в подборке</span></div>
					<div class="b-result_sorce_info"><em>Автор:</em> Рязанская областная библиотека</div>
				</div><!-- /.b-collection-item -->

			</div><!-- /.b-collection_list -->
	</section>

</div><!-- /.homepage -->


<? include("footer.php"); ?>

</html>