<?php

namespace Nota\Exalead;

class PrepThumbnail
{
    public static function crop($file)
    {
        if (empty($file))
            return false;

        if (!file_exists($file))
            return false;

        // Убираем белые поля
        $image = new \Imagick($file);
        $geo = $image->getImageGeometry();

        // Варианты цвета рамки
        $arSearchColor = array('000000', 'fefefe', 'ffffff', 'e5e4e4');
        $whiteColor = 'ffffff';

        if (!empty($geo)) {

            $centerW = $geo['width'] / 2;
            $centerH = $geo['height'] / 2;

            // вычисляем границы рамки
            $top = false;
            $i = 0;
            $w = 0;
            while ($top === false) {
                $pixel = $image->getImagePixelColor($centerW, $i);
                $color = self::getHexByRGB($pixel->getColor());
                if (!in_array($color, $arSearchColor) or $w > 2)
                    $top = $i;

                if ($color == $whiteColor)
                    $w++;

                $i++;
                if ($i >= $centerW)
                    $top = 0;
            }

            $left = false;
            $i = 0;
            $w = 0;
            while ($left === false) {
                $pixel = $image->getImagePixelColor($i, $centerH);
                $color = self::getHexByRGB($pixel->getColor());
                if (!in_array($color, $arSearchColor) or $w > 2)
                    $left = $i;

                if ($color == $whiteColor)
                    $w++;

                $i++;
                if ($i >= $centerH)
                    $left = 0;
            }


            $right = false;
            $i = $geo['width'];
            $w = 0;
            while ($right === false) {
                $pixel = $image->getImagePixelColor($i, $centerH);
                $color = self::getHexByRGB($pixel->getColor());
                if (!in_array($color, $arSearchColor) or $w > 2)
                    $right = $i;

                if ($color == $whiteColor)
                    $w++;

                $i--;
                if ($i <= $centerW)
                    $right = $geo['width'];
            }

            $bottom = false;
            $i = $geo['height'];
            $w = 0;
            while ($bottom === false) {
                $pixel = $image->getImagePixelColor($centerW, $i);
                $color = self::getHexByRGB($pixel->getColor());
                if (!in_array($color, $arSearchColor) or $w > 2)
                    $bottom = $i;

                if ($color == $whiteColor)
                    $w++;

                $i--;
                if ($i <= $centerH)
                    $bottom = $geo['height'];
            }

            #if($top > 0)
            $top += 3;
            #if($left > 0)
            $left += 3;

            $w = $right - $left - 2;
            $h = $bottom - $top;

            if ($w < ($geo['width'] / 2)) {
                $w = $geo['width'];
                $left = 0;
            }

            /*print_r($top); echo '<br />';
            print_r($left); echo '<br />';
            print_r($right); echo '<br />';
            print_r($bottom); echo '<br />';
            pre($geo,1);
            pre($w,1);
            exit();*/

            if (intval($w) > 0 and intval($h) > 0) {
                $image->cropImage($w, $h, $left, $top);
                $image->writeImage($file);
                $image->clear();
            }
        }

    }

    private static function getHexByRGB($rgb)
    {
        if (!is_array($rgb))
            return false;
        return sprintf('%02x%02x%02x', $rgb['r'], $rgb['g'], $rgb['b']);
    }
}