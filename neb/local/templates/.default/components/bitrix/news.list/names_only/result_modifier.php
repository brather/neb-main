<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\CModule::IncludeModule("highloadblock");
\CModule::IncludeModule("iblock");

use \Bitrix\Highloadblock\HighloadBlockTable;
use \Neb\Main\Library\NebLibsCityTable;

$libraryHLBlock = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
$libraryEntity = HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

if (!$_REQUEST['city_id']) {$_REQUEST['city_id'] = 592;}
if ($_REQUEST['city_id']) {
    $cityData = NebLibsCityTable::getByID($_REQUEST['city_id']);
    $cityData = $cityData->Fetch();
    $cityData["UF_POS"] = explode(" ", $cityData["UF_POS"]);
    $arResult["CITY"] = $cityData;
}
$mass = $arResult["ITEMS"];
unset($arResult["ITEMS"]);
foreach($mass as $key=>$arItem)
{
    $arFilter['UF_ID'] = $arItem['PROPERTY_25'];
    if ($_REQUEST['city_id'])  $arFilter['UF_CITY'] = $_REQUEST['city_id'];
    $libraryData = $libraryEntity::getList(array(
        "select" => array("ID", "UF_NAME", "UF_ADRESS", "UF_POS"),
        "order" => array("ID"),
        "filter" => $arFilter,
    ));

    if($l = $libraryData->fetch()){

        $res["WCHZ"] = false;

        // есть ли ВЧЗ
        $arSelect = Array("ID");
        $arrFilter = Array("IBLOCK_ID"=>IBLOCK_ID_WORKPLACES, "PROPERTY_LIBRARY" =>  $arItem['PROPERTY_25'],  "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $ress = CIBlockElement::GetList(Array(), $arrFilter, false, Array("nPageSize"=>50), $arSelect);
        if ($ob = $ress->GetNextElement()) $res["WCHZ"] = true;

        $res['NAME'] = $l['UF_NAME'];
        $res['ADRESS'] = $l['UF_ADRESS'];
        $res['POS'] = $l['UF_POS'];
        $res['DETAIL_PAGE_URL'] = $arItem['DETAIL_PAGE_URL'];
        $arResult["ITEMS"][] = $res;
    }


}
if(SITE_TEMPLATE_ID == "special" && isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"]))
{
    foreach($arResult["ITEMS"] as $key=>$arItem)
    {
        $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = "/special".$arItem["DETAIL_PAGE_URL"];
    }
}