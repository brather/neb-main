<?php
/**
 * User: agolodkov
 * Date: 09.09.2016
 * Time: 11:25
 */

namespace BitrixBundle\Traits;


/**
 * Class IblockHelperTrait
 *
 * @package BitrixBundle\Traits
 */
trait IblockHelperTrait
{
    use BitrixHelperPropertyTrait;

    /**
     * @return \CIBlockElement
     * @throws \Exception
     */
    public function forgeCIBlockElement()
    {
        $this->_bitrix->checkIncludeThrowException();
        $this->_bitrix->includeModule('iblock');

        return new \CIBlockElement();
    }

}