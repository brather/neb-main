<?php
/**
 * User: agolodkov
 * Date: 05.10.2016
 * Time: 17:54
 */

namespace AssessorBundle\Controller;


use AssessorBundle\AssessorBundle;
use AssessorBundle\Controller\Api\ApiQueryFrequencyController;
use AssessorBundle\Controller\Api\RatingApiInterface;
use AssessorBundle\Entity\Handler\QuerySortRequest;
use AssessorBundle\Entity\TblQueryFrequency;
use AssessorBundle\Entity\TblQueryRating;
use AssessorBundle\Form\QuerySortRequestType;
use AssessorBundle\Traits\ControllerAssessorRatingTrait;
use AppBundle\DependencyInjection\BitrixMainPageNavigation;
use AppBundle\DependencyInjection\NebSearcher;
use BitrixBundle\Annotations\IncludeBitrix;
use JMS\DiExtraBundle\Annotation as DI;
use BitrixBundle\Annotations\Template as BitrixTemplate;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QueryRatingController
{
    use ControllerAssessorRatingTrait;

    /**
     * @DI\Inject("assessor.api.query_frequency")
     * @var ApiQueryFrequencyController
     */
    public $api;

    /**
     * @DI\Inject("neb.searcher")
     * @var NebSearcher
     */
    public $searcher;

    /**
     * @BitrixTemplate("AssessorBundle:assessor:queries.html.php")
     * @IncludeBitrix()
     *
     * @return array
     */
    public function listAction()
    {
        if ($query = $this->request->query->get('query')) {
            $qb = $this->_getListQueryBuilder($this->api->getEntityName());
            $qb->where($qb->expr()->like('t.query', ':query'));
            $qb->setParameter('query', "%$query%");
        }
        if(!$order = $this->request->query->get('order')) {
            $order = ['frequency' => 'desc'];
        }
        $result = $this->getListData($this->api->getEntityName(), $order);
        /** @var TblQueryFrequency[] $queriesMap */
        $queriesMap = [];
        /** @var TblQueryFrequency $entity */
        foreach ($result['entities'] as $entity) {
            $queriesMap[$entity->getId()] = $entity;
        }
        if (!empty($queriesMap)) {
            $qb = $this->getEntityManager()->getRepository('AssessorBundle:TblQueryRating')->createQueryBuilder('q');
            $qb->addSelect('q.idquery');
            $qb->where('q.idquery IN(:queriesId)');
            $qb->setParameter('queriesId', array_keys($queriesMap));
            $qb->groupBy('q.idquery');
            $ratingQuery = $qb->getQuery()->getResult();
            foreach ($ratingQuery as $entity) {
                if (isset($queriesMap[$entity['idquery']])) {
                    $queriesMap[$entity['idquery']]->setHasSorting(true);
                }
            }
        }

        return $result;
    }


    /**
     * @BitrixTemplate("AssessorBundle:assessor:search-query.html.php")
     * @IncludeBitrix()
     *
     * @return array
     */
    public function searchQueryAction()
    {
        $params = $this->request->query->all();
        $queryEntity = null;
        if (isset($params['queryId']) && (integer)$params['queryId'] > 0) {
            $queryEntity = $this
                ->getEntityManager()
                ->getRepository('AssessorBundle:TblQueryFrequency')
                ->find($params['queryId']);
        }
        if (!$queryEntity instanceof TblQueryFrequency) {
            throw new NotFoundHttpException('Query not found!');
        }
        $params['pagen'] = NebSearcher::PAGE_SIZE;
        $params['q'] = $queryEntity->getQuery();
        $params['full_query_search'] = true;
        $params['s_in_closed'] = true;

        $result = [];
        $result['searchListHtml'] = $this->searcher->buildNebSearchComponent(
            $params,
            [
                'queryId' => $params['queryId'],
                'cache'   => false,
            ],
            'assessor_search'
        );
        $result['current'] = ['queries' => true];
        $result['url'] = $this->getAssessorTabsUrls();
        $result['url']['currentQuery'] = $this->router->generate(
            'assessor_queries',
            ['query' => $queryEntity->getQuery()]
        );
        $result['url']['editionsUrl'] = $this->router->generate('api_assessor_query_rating_editions');

        return $result;
    }

}