<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/socialservices/classes/general/authmanager.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/socialservices/classes/general/openid.php');

use \Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler('socialservices', 'OnAuthServicesBuildList', ['SocServEventHandler', 'GetDescription']);

class SocServEventHandler {
    public static function GetDescription() {
        return array(array(
            'ID' => 'Esia',
            'CLASS' => 'CSocServEsia',
            'CSS_CLASS' => 'esialink',
            'NAME' => 'Esia Auth',
            'ICON' => 'esia',
        ));
    }
}

class CSocServEsia extends CSocServAuth {//extends CSocServOpenID

    const ID = 'Esia';

    public function GetSettings() {
        return array(
        );
    }

    public function GetFormHtml($arParams) {
        $url = URL_SCHEME . '://' . SITE_SERVER_NAME . '/WebServices/Esia.php?act=do_login';
        return '<a href="javascript:void(0)" onclick="BX.util.popup(\''.htmlspecialcharsbx(CUtil::JSEscape($url)).'\', 800, 600)">Esia</a>';
    }

    // по-тихоньку выпиливать надо
    public function Authorize($arFields) {

        global $USER, $APPLICATION;

        $userData = array();
        if (!empty($arFields['EMAIL']))
            $userData = $USER->GetList($by = 'ID', $ord = 'ASC', array('EMAIL' => $arFields['EMAIL'], 'ACTIVE' => 'Y'))->Fetch();

        // авторизация существующего пользователя
        if ($userData['ID'])
            $USER->Authorize($userData['ID']);
        // регистрация и авторизация нового пользователя
        else
            $this->AuthorizeUser($arFields);

        // вывод результата
        $APPLICATION->RestartBuffer();

        $url = '/';  //$url = SITE_SERVER_NAME;
        //if ($USER->IsAuthorized() && !$userData['ID'])
        //    $url .= 'popup/index.php?action=esia-after-register';
        echo '<script type="text/javascript">
				if (window.opener)
					window.opener.location = \''.CUtil::JSEscape($url).'\';
				window.close();
			</script>';

        die();
    }
}