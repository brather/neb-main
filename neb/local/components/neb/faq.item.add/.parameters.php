<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(!CModule::IncludeModule("iblock")) return;
use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;
Loc::loadMessages(__FILE__);
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-" => " "));
$arIBlocks = array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID" => $arRequest["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch()) $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("FAQ_ITEM_ADD_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("FAQ_ITEM_ADD_IBLOCK_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => '={$arRequest["ID"]}',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
        ),
        "MIN_COUNT_SYMBOL" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => Loc::getMessage("FAQ_ITEM_MIN_COUNT_SYMBOL"),
            "TYPE" => "TEXT",
            "DEFAULT" => "5"
        ),
        "URL_PAGE" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => Loc::getMessage("FAQ_ITEM_URL_PAGE"),
            "TYPE" => "TEXT",
            "DEFAULT" => ""
        ),
    ),
);