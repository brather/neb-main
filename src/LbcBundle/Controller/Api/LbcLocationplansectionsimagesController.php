<?php
/**
 * User: agolodkov
 * Date: 17.08.2016
 * Time: 17:19
 */

namespace LbcBundle\Controller\Api;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("app.api.locationplansections.images")
 */
class LbcLocationplansectionsimagesController extends AbstractImagesController
{
    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcLocationplansectionimages';
    }

    /**
     * @return string
     */
    public function getOwnFieldName()
    {
        return 'locationplansectionid';
    }
}