<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arResult
 * @var array $arParams
 */
if (empty($arResult['BOOK'])) {
    return false;
}
global $APPLICATION;

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>

<div class="container v-padding">
    <div class="button-bar" role="toolbar">
        <a href="../../" class="btn btn-default">Вернуться на главную</a><br />
        <a href="../../results/?<?=$_SESSION['BLIND_QUERY']?>" class="btn btn-default">К результатам поиска</a>
        <a href="/" class="btn btn-default" tabindex="7" style="float:right;">Переход в режим для зрячих</a>
    </div>

    <h1><?=$arResult['BOOK']['title']?></h1>

    <? if (!empty($arResult['BOOK']['id'])): ?>
        <p class="h4">О произведении</p>
    <? endif; ?>

    <dl class="data-dl" id="itemidlabel">

        <? if(!empty($arResult['BOOK']['authorbook'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR')?></dt>
            <dd><?=$arResult['BOOK']['authorbook']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['title']) && !empty($arResult['BOOK']['id'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE')?></dt>
            <dd><?=$arResult['BOOK']['title']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['subtitle'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SUBTITLE')?></dt>
            <dd><?=$arResult['BOOK']['subtitle']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['responsibility'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_RESPONSIBILITY')?></dt>
            <dd><?=$arResult['BOOK']['responsibility']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['publishplace'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?></dt>
            <dd><?=$arResult['BOOK']['publishplace']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['publisher'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH')?></dt>
            <dd><?=$arResult['BOOK']['publisher']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['publishyear'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR')?></dt>
            <dd><?=$arResult['BOOK']['publishyear']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['countpages'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES')?></dt>
            <dd><?=$arResult['BOOK']['countpages']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['series'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE')?></dt>
            <dd><?=$arResult['BOOK']['series']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['isbn'])): ?>
            <dt>ISBN</dt>
            <dd><?=$arResult['BOOK']['isbn']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['issn'])): ?>
            <dt>ISSN</dt>
            <dd><?=$arResult['BOOK']['issn']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['contentnotes'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT')?></dt>
            <dd><?=$arResult['BOOK']['contentnotes']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['notes'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE')?></dt>
            <dd><?=$arResult['BOOK']['notes']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['annotation'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ANNOTATION')?></dt>
            <dd><?=$arResult['BOOK']['annotation']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['bbk'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BBK')?></dt>
            <dd><?=$arResult['BOOK']['bbk']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['udk'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UDC')?></dt>
            <dd><?=$arResult['BOOK']['udk']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['edition'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_EDITION')?></dt>
            <dd><?=$arResult['BOOK']['edition']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['library'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB')?></dt>
            <dd><?=$arResult['BOOK']['library']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['lang'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LANGUAGE')?></dt>
            <dd><?=$arResult['BOOK']['lang']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['namebook'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_INCLUDED_IN')?></dt>
            <dd><?=$arResult['BOOK']['namebook']?></dd>
        <? endif; ?>



        <? if(!empty($arResult['BOOK']['fundnumber'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_FUND_NUMBER')?></dt>
            <dd><?=$arResult['BOOK']['fundnumber']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['fundname'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_FUND_NAME')?></dt>
            <dd><?=$arResult['BOOK']['fundname']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['inventorynumber'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_INVENTORY_NUMBER')?></dt>
            <dd><?=$arResult['BOOK']['inventorynumber']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['inventoryname'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_INVENTORY_NAME')?></dt>
            <dd><?=$arResult['BOOK']['inventoryname']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['unitnumber'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UNIT_NUMBER')?></dt>
            <dd><?=$arResult['BOOK']['unitnumber']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['unitname'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UNIT_NAME')?></dt>
            <dd><?=$arResult['BOOK']['unitname']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['dates'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_DATES')?></dt>
            <dd><?=$arResult['BOOK']['dates']?></dd>
        <? endif; ?>





        <? if(!empty($arResult['BOOK']['description'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_DESCRIPTION')?></dt>
            <dd><?=$arResult['BOOK']['description']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['material'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_MATERIAL')?></dt>
            <dd><?=$arResult['BOOK']['material']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['size'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SIZE')?></dt>
            <dd><?=$arResult['BOOK']['size']?></dd>
        <? endif; ?>



        <? if(!empty($arResult['BOOK']['mpk'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_MPK')?></dt>
            <dd><?=$arResult['BOOK']['mpk']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['declarant'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_DECLARANT')?></dt>
            <dd><?=$arResult['BOOK']['declarant']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['patentholder'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PATENTHOLDER')?></dt>
            <dd><?=$arResult['BOOK']['patentholder']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['representative'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_REPRESENTATIVE')?></dt>
            <dd><?=$arResult['BOOK']['representative']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['publish_date'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_DATE')?></dt>
            <dd><?=$arResult['BOOK']['publish_date']?></dd>
        <? endif; ?>

        <? if(!empty($arResult['BOOK']['patentcode'])): ?>
            <dt><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PATENTCODE')?></dt>
            <dd><?=$arResult['BOOK']['patentcode']?></dd>
        <? endif; ?>
    </dl>
    <div class="button-bar">
        <? if ('Y' === $arResult['BOOK']['IS_READ']) { ?>
            <button
                class="btn btn-primary boockard-read-button clearfix<? if (
                    !$arResult['isInWorkplace'] && $arResult['nebUser']['UF_STATUS'] !== nebUser::USER_STATUS_VIP
                ): ?> boockard-read-button--disabled <? endif; ?>"
                href="javascript:void(0);"
                data-link="<?= $arResult['BOOK']['readBookId'] ?>"
                data-options="<?= htmlspecialchars(json_encode($arResult['BOOK']['viewerOptions']), ENT_QUOTES, 'UTF-8') ?>"
                data-server-error="<?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERVER_ERROR'); ?>"
                aria-describedby="itemidlabel"
                data-reader-blind
            >
                <? if ('museum' == $arResult['BOOK']['librarytype']): ?>
                    <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_VIEW') ?>
                <? else: ?>
                    <?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_READ') ?>
                <? endif; ?>
            </button>
        <? } ?>
    </div>

</div>
