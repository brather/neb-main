<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Редактирование типа ЭЧЗ');

/**
 * @var array            $arResult
 * @var CMain            $APPLICATION
 * @var CBitrixComponent $component
 */
$APPLICATION->IncludeComponent(
    'neb:workplaces.types',
    '',
    [
        "ID" => intval($arResult['VARIABLES']['ID']),
        "LIST_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['workplaces_types']
    ],
    $component
);