<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader;
use \Bitrix\Main\UI\PageNavigation;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Entity\ReferenceField;

use \Bitrix\Iblock\IblockTable;
use \Bitrix\Iblock\ElementTable;


/**
 * Class ForLibrariesComponent
 */
class ForLibrariesComponent extends CBitrixComponent
{
    private
        $iIblock = 0,
        $sComponentPage = 'list',
        $sElementCode = '',
        $iPageLimit = 21;
    
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        // ЧПУ
        $arDefaultUrlTemplates404 = array(
            'list'    => '',
            'detail'  => '#ELEMENT_CODE#/'
        );

        $arDefaultVariableAliases404 = array();
        $arDefaultVariableAliases = array();
        $arComponentVariables = array('ID');

        $SEF_FOLDER = '';
        $arUrlTemplates = array();

        if ($arParams['SEF_MODE'] == 'Y')
        {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams['SEF_URL_TEMPLATES']);
            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams['VARIABLE_ALIASES']);

            $sComponentPage = CComponentEngine::parseComponentPath($arParams['SEF_FOLDER'], $arUrlTemplates, $arVariables);

            if (strlen($sComponentPage) <= 0) $sComponentPage = 'list';

            CComponentEngine::initComponentVariables($sComponentPage, $arComponentVariables, $arVariableAliases, $arVariables);

            $SEF_FOLDER = $arParams['SEF_FOLDER'];

        } else {

            $arVariables = array();

            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams['VARIABLE_ALIASES']);
            CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

            $sComponentPage = '';
            if (IntVal($arVariables['ELEMENT_ID']) > 0) {
                $sComponentPage = 'detail';
            } else {
                $sComponentPage = 'list';
            }
        }

        $this->sComponentPage = $sComponentPage;
        $this->sElementCode   = $arVariables['ELEMENT_CODE'];

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        Loader::includeModule('iblock');

        $this->_getIblock();
        $this->_getElements();

        // определение текущего раздела и выборка его элементов

        $this->includeComponentTemplate($this->sComponentPage);
    }

    /**
     * Получение идентификатора инфоблока
     *
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getIblock() {

        $arIblock = IblockTable::getList([
            'filter' => ['CODE' => 'for-libraries', 'ACTIVE' => 'Y'],
            'select' => ['ID'],
        ])->fetchRaw();

        $this->iIblock = $arIblock['ID'];
    }

    /**
     * Выборка списка элементов
     * 
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getElements() {

        global $APPLICATION;

        $sUrl = $this->arParams['SEF_FOLDER'] . $this->arParams['SEF_URL_TEMPLATES']['element'];

        $arQueryParams = [
            'filter' => [
                'IBLOCK_ID' => $this->iIblock,
                'ACTIVE'    => 'Y'
            ],
            'order'  => ['SORT' => 'ASC', 'ID' => 'ASC'],
            'select' => ['ID', 'NAME', 'CODE', 'IBLOCK_SECTION_ID', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_PICTURE'],
        ];

        if (!empty($this->sElementCode)) {
            $arQueryParams['filter']['CODE'] = $this->sElementCode;
            $arQueryParams['select'][] = 'DETAIL_TEXT';
        }

        // получение количества элементов на странице
        $iCount = ElementTable::getList($arQueryParams)->getSelectedRowsCount();

        // иницирование навигационной цепочки
        $obPageNavigation = new PageNavigation('page');
        $obPageNavigation->allowAllRecords(true)->initFromUri();
        $obPageNavigation->setRecordCount($iCount)->setPageSize($this->iPageLimit);
        $this->arResult['NAV'] = $obPageNavigation;

        if (strpos($_SERVER['REQUEST_URI'], 'page-all') === false) {
            $arQueryParams['limit']  = $obPageNavigation->getLimit();
            $arQueryParams['offset'] = $obPageNavigation->getOffset();
        }

        // выборка списка новостей
        $arQueryParams['select'] += [
            'PREVIEW_SUBDIR' => 'PREVIEW.SUBDIR', 'PREVIEW_FILE_NAME' => 'PREVIEW.FILE_NAME',
            'DETAIL_SUBDIR'  => 'DETAIL.SUBDIR',  'DETAIL_FILE_NAME'  => 'DETAIL.FILE_NAME'
        ];
        $arQueryParams['runtime'] = [
            new ReferenceField(
                'PREVIEW',
                '\Bitrix\Main\FileTable',
                ['=this.PREVIEW_PICTURE' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new ReferenceField(
                'DETAIL',
                '\Bitrix\Main\FileTable',
                ['=this.DETAIL_PICTURE' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        ];
        $rsElements = ElementTable::getList($arQueryParams);
        while ($arItem = $rsElements->fetch()) {

            if ($arItem['PREVIEW_PICTURE']) {
                $arItem['PREVIEW_PICTURE'] = ['PATH' => '/upload/' . $arItem['PREVIEW_SUBDIR'] . '/' . $arItem['PREVIEW_FILE_NAME']];
            }
            if ($arItem['DETAIL_PICTURE']) {
                $arItem['DETAIL_PICTURE'] = ['PATH' => '/upload/' . $arItem['DETAIL_SUBDIR'] . '/' . $arItem['DETAIL_FILE_NAME']];
            }

            $arItem['DETAIL_PAGE_URL'] = str_replace(
                ['#SECTION_CODE#', '#ELEMENT_CODE#'],
                [$this->arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['CODE'], $arItem['CODE']],
                $sUrl
            );

            $this->arResult['ELEMENTS'][$arItem['ID']] = $arItem;

            if ($arQueryParams['filter']['CODE'] == $arItem['CODE']) {
                $APPLICATION->SetTitle($arItem['NAME']);
            }
        }
    }
}