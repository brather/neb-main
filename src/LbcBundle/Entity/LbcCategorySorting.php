<?php
/**
 * User: agolodkov
 * Date: 29.08.2016
 * Time: 12:20
 */

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;

/**
 * LbcCategorySorting
 *
 * @ORM\Table(
 *      name="lbc_category_sorting",
 *      indexes={
 *          @ORM\Index(name="category_type_id_idx", columns={"category", "category_id"}),
 *          @ORM\Index(name="category_id_idx", columns={"category_id"})
 *      }
 *  )
 * @ORM\Entity
 */
class LbcCategorySorting
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serialization\Groups({"list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", nullable=false, length=100)
     */
    private $category;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=false, options={"unsigned":true})
     */
    private $categoryId;

    /**
     * @ORM\Column(name="sort", type="integer")
     * @Serialization\Groups({"list"})
     */
    private $sort;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }
}