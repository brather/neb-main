08.09.14
******** BD *******
ALTER TABLE `neb_collection_collection` ADD `SORT` INT NOT NULL ,
ADD `TIMESTAMP_X` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;
+++++++++++
ALTER TABLE `neb_collection_elements` ADD `SORT` INT NOT NULL ,
ADD `TIMESTAMP_X` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;

15.09.14

ALTER TABLE `neb_collection_library` ADD `POSTAL_ADDRESS` VARCHAR( 255 ) NULL ;
ALTER TABLE `neb_collection_library` ADD `SCHEDULE` VARCHAR( 255 ) NULL ;
ALTER TABLE `neb_collection_library` ADD `PHONE` VARCHAR( 255 ) NULL ;
ALTER TABLE `neb_collection_library` ADD `EMAIL` VARCHAR( 100 ) NULL ;
ALTER TABLE `neb_collection_library` ADD `SKYPE` VARCHAR( 100 ) NULL ;
ALTER TABLE `neb_collection_library` ADD `URL` VARCHAR( 150 ) NULL ;
ALTER TABLE `neb_collection_library` ADD `DESCRIPTION` TEXT NULL ;
ALTER TABLE `neb_collection_library` ADD `MAP` VARCHAR( 255 ) NULL ;
ALTER TABLE `neb_collection_library` ADD `TIMESTAMP_X` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ;
ALTER TABLE `neb_collection_library` ADD `SORT` INT( 11 ) NOT NULL DEFAULT '100';
ALTER TABLE `neb_collection_library` ADD `CODE` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `neb_collection_library` ADD `SIGNATURE` VARCHAR( 255 ) NULL AFTER `NAME` ;
ALTER TABLE `neb_collection_library` ADD `LOGO` INT NULL ;