<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper,
    \Neb\Main\Library\NebLibsCityTable;

Loader::includeModule("highloadblock");

$libraryHLBlock = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
$libraryEntity  = HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

if ($_REQUEST['city_id']) {
    $cityData = NebLibsCityTable::getByID($_REQUEST['city_id']);
    $cityData = $cityData->Fetch();
    $cityData["UF_POS"] = explode(" ", $cityData["UF_POS"]);
    $arResult["CITY"] = $cityData;
}

$arFilter = Array("IBLOCK_ID" => IBLOCK_ID_LIBRARY, "ACTIVE" => "Y");
$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_LIBRARY_LINK", "PROPERTY_MAP", "DETAIL_PAGE_URL");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();

    // находим библиотеку в HL и проверяем соответствует ли она городу
    $Filter = array('UF_ID' => $arFields['PROPERTY_LIBRARY_LINK_VALUE']);
    //if ($_REQUEST['city_id'])  $Filter['UF_CITY'] = $_REQUEST['city_id']; //отфильтровывает все точки, кроме выбранного города
    $libraryData = $libraryEntity::getList(array(
        "select" => array("ID", "UF_NAME", "UF_ADRESS", "UF_POS"),
        "order" => array("ID"),
        "filter" => $Filter,
    ));
    //print_r($arFields);

    if ($l = $libraryData->fetch()) {

        $map = explode(' ', $l["UF_POS"]);

        $arPoints[] = array(
            'id' => $arLibsFields['ID'],
            'address' => $l['UF_ADRESS'],
            'name' => $l['UF_NAME'],
            //'status' => $arLibsFields['PROPERTY_STATUS_VALUE'],
            //'parten' => $arItem["IS_NEB"],
            'link' => $arFields['DETAIL_PAGE_URL'],
            //'available' => $arItem['PROPERTY_SCHEDULE_VALUE'],
            'coordinates' => array($map[1], $map[0]),
        );

    }
}

$arJson = array();
$arJson['libs'][] = array(
    'id' => 1,
    'placemarksize' => array(34, 48),
    'placemarkicon' => '/local/templates/.default/markup/i/pin.png',
    'regions' => array(
        array(
            'current' => true,
            'city' => $arResult["CITY"]["UF_CITY_NAME"],
            'id' => ($arResult['CITY'] ? $arResult["CITY"]["ID"] : 592),
            'coordinates' => ($arResult['CITY'] ? array($arResult["CITY"]['UF_POS'][1], $arResult["CITY"]['UF_POS'][0]) : array('55.753676', '37.619899')),
            //'coordinates'	=> array('55.753676', '37.619899'),
            'metro' =>
                array(
                    array(
                        'station' => '',
                        'id' => 1,
                        'points' => $arPoints,
                    )
                )
        )
    )
);

MainHelper::showJson($arJson);