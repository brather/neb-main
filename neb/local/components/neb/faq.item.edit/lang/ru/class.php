<?php
$MESS["ERROR_MIN_COUNT_SYMBOL"] = "Ошибка! Вы ввели слишком мало символов.";
$MESS["ERROR_EMPTY_STRING"] = "Ошибка! Вы не заполнили одно из полей.";
$MESS["ERROR"] = "Ошибка!";
$MESS["ERROR_ITEM_EXIST"] = "Ошибка! Элемент с таким именем уже существует. Пожалуйста введите другое название элемента.";
$MESS["EDIT_SUCCESS_1"] = "Изменение элемента с id=<b>";
$MESS["EDIT_SUCCESS_2"] = "</b> произведено успешно!";
$MESS["ITEM_DELETE_SUCCESS_1"] = "Удаление элемента c id=<b>";
$MESS["ITEM_DELETE_SUCCESS_2"] = "</b> произведено успешно!";
$MESS["ITEM_DELETE_ERROR_1"] = "Ошибка! Удаления элемента c id=<b>";
$MESS["ITEM_DELETE_ERROR_2"] = "</b> не произведено.";
$MESS["ITEM_DELETE_ERROR_NOT_PERMISSION"] = "Ошибка! Удаления элемента не произведено, поскольку Вам не хватает прав доступа.";
$MESS["ITEM_NOT_FOUND_1"] = "Элемент с id=<b>";
$MESS["ITEM_NOT_FOUND_2"] = "</b> не был найден.";
$MESS["ERROR_DETAIL_TEXT_EMPTY"] = "Ошибка! Введите детальный текст.";