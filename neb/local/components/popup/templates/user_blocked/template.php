<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section class="innerwrapper clearfix">
    <div class="b-registration b-registration-popup rel">
        <form id="regform"  enctype="application/x-www-form-urlencoded" name="regform" class="b-form b-form_common b-regform" action="/popup/" method="post">

            <p class="note" style="margin: 0 auto;">
                Уважаемый читатель!<br> Ваш читательский билет заблокирован за нарушение правил библиотеки.
            </p>
            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell " style="margin: 0 auto;">
                    <div class="field clearfix divdisable">
                        <button class="formbutton" value="1" type="button" onclick="document.location.href = '/profile/';">Ок</button>
                        <button class="formbutton" value="1" type="button" onclick="document.location.href = '/feedback/';">Обратная связь</button>
                    </div>
                </div>
            </div>

        </form>
    </div><!-- /.b-registration-->
</section>
