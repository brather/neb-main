<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Iblock\SectionTable,
    \Bitrix\Iblock\ElementTable;

class FaqGetListItems extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $iblockId = '';
    private $urlSection = '';
    private $urlItem = '';
    private $arSection = array();
    private $arItems = array();
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        if(!empty($arParams["IBLOCK_ID"])) {
            $this->iblockId = $arParams["IBLOCK_ID"];
        }
        if(!empty($arParams["URL_PAGE_SECTION"])) {
            $this->urlSection = $arParams["URL_PAGE_SECTION"];
        }
        if(!empty($arParams["URL_PAGE_ITEM"])) {
            $this->urlItem = $arParams["URL_PAGE_ITEM"];
        }
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->_setSectionInArrResult();
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Возвращаем список всех разделов
    /** Возвращаем список всех разделов
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getListSections() {
        $resSections = SectionTable::GetList(array(
            'filter' => array('IBLOCK_ID' => $this->iblockId),
            'order' => array('SORT' => 'ASC', 'DATE_CREATE' => 'ASC'),
            'select' => array('ID', 'NAME', 'DATE_CREATE')
        ));
        while($obSection = $resSections->Fetch()) {
            $this->arSection[$obSection['ID']] = array(
                'ID' => $obSection['ID'],
                'NAME' => $obSection['NAME'],
                'DATE_CREATE' => $obSection['DATE_CREATE'],
                'URL' => $this->arParams['SEF_FOLDER'] . 'section/?sect_id=' . $obSection["ID"]
            );
        }
    }
    //endregion
    //region Возвращаем список всех элементов
    /** Возвращаем список всех элементов
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getListItems() {
        $resItems = ElementTable::GetList(array(
            'filter' => array('IBLOCK_ID' => $this->iblockId),
            'order' => array('SORT' => 'ASC', 'DATE_CREATE' => 'ASC'),
            'select' => array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'DATE_CREATE')
        ));
        while($obItems = $resItems->Fetch()) {
            $this->arItems[$obItems['ID']] = array(
                'ID' => $obItems['ID'],
                'NAME' => $obItems['NAME'],
                'IBLOCK_SECTION_ID' => $obItems['IBLOCK_SECTION_ID'],
                'DATE_CREATE' => $obItems['DATE_CREATE']
            );
        }
    }
    //endregion
    //region Заполняем результирующий массив
    /**
     * Заполняем результирующий массив
     */
    private function _setSectionInArrResult() {
        $this->_getListSections();
        $this->_getListItems();
        $this->arResult["SECTION_ITEMS"] = $this->arSection;
        foreach ($this->arItems as $idItem => $arItem) {
            $iblockSectId = $arItem["IBLOCK_SECTION_ID"];
            $this->arResult["SECTION_ITEMS"][$iblockSectId]["ITEMS"][$idItem] = $arItem;
            $this->arResult["SECTION_ITEMS"][$iblockSectId]["ITEMS"][$idItem]["URL"] = $this->arParams['SEF_FOLDER'] . 'item/?id=' . $arItem["ID"];
        }
    }
    //endregion
}