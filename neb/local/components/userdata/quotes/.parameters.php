<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Nota\Bookcatalog\SectionTable;
use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__); 
CModule::IncludeModule('nota.bookcatalog');
try
{
	
	$rsSection = SectionTable::getList(array(
        'select' => array_keys(SectionTable::getMap()),
        'filter' => array('PROMO' => 'Y'),
        'order' => array()
    ));
    
    while ($arSection = $rsSection->Fetch()) 
    {
        $arSect[$arSection['ID']] = $arSection['NAME'];
    }
	
	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			'IBLOCK_SECTIONS' => array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('LIB_SEARCH_FORM_PARAMETERS_IBLOCK_SECTIONS'),
				'TYPE' => 'LIST',
				'VALUES' => $arSect,
				"MULTIPLE" => "Y",
			),
		"PAGE_URL" => array(
		"PARENT" => "BASE",
		"NAME" => Loc::getMessage('LIB_SEARCH_FORM_PARAMETERS_PAGE_URL'),
		"TYPE" => "STRING",
		"DEFAULT" => '',
		),
			'CACHE_TIME' => array(
				'DEFAULT' => 3600
			)
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e -> getMessage());
}
