<?php use LbcBundle\AcceptanceTester;

$I = new AcceptanceTester($scenario);
$I->wantTo('Search categories by codes');
$query = 'Т3(4/9)';
$I->amOnPage('/bbk/search/?query=' . $query);
$I->see($query);
$query = 'А35е(2)л612-9';
$I->amOnPage('/bbk/search/?query=' . $query);
$I->see('Филиалы Центрального музея В. И. Ленина');
$query = ',014.2';
$I->amOnPage('/bbk/search/?query=' . $query);
$I->see('Публицистические произведения древних и средневековых авторов');
