
<?
$pagename='';
?>
<? include("header_main_auth.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black">Искать в моей библиотеке</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>		

		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<input type="submit" class="formbutton" value="Принять">
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		
		<div class="b-plaintext">
			<h2>Обратная связь</h2>

			<form action="" class="b-form b-form_common b-feedbackform">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="fb_email">Электронная почта</label>
						<div class="field validate">
							<input type="text" class="input" name="email" data-minlength="2" data-maxlength="30" id="fb_email" data-validate="email" value="" data-required="required">									
						</div>
					</div>
				</div>

				<div class="fieldrow nowrap ptop">
					<div class="fieldcell iblock">
						<label for="fb_theme">Тема:</label>
						<div class="field">
							<select name="theme" id="fb_theme" data-required="required" class="js_select w370">
								<option value="1">Вопрос</option>
								<option value="2">Жалоба</option>
								<option value="3">Предложение</option>
								<option value="2">Другое</option>
							</select>
						</div>
					</div>
				</div>

				<div class="fieldrow nowrap">
					<div class="fieldcell iblock mt10">
						<em class="hint">*</em>
						<label for="fb_message">Ваше сообщение:</label>
						<div class="field validate txtarea">
							<textarea id="fb_message" data-maxlength="1500" data-required="required" data-minlength="2" name="message" class="input" ></textarea>											
						</div>
					</div>
				</div>

				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="bt_captcha">Код с картинки:</label>
						<div class="field validate captcha">
							<input type="text" class="input" name="captcha" id="bt_captcha" value="" data-required="required">
							<img src="./i/captcha.jpg" alt="изображение с кодом" >
						</div>
					</div>
				</div>

				<div class="fieldrow nowrap fieldrowaction">
					<div class="fieldcell ">
						<div class="field clearfix">
							<button class="formbutton" value="1" type="submit">Отправить письмо!</button>

						</div>
					</div>
				</div>

			</form>

		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>

</body>
</html>