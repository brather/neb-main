<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$arTemplateParams = array(
		'template'  => 'left',
		'width'  	=> 250,
		'height' 	=> 500,
		'exact' 	=> false,
		'one_photo'	=> true,
		'align'		=> 'left',
		'SORT'		=> 20,
	);

	$arTemplateDescription = array(
		"NAME" => 'Gallery in the left',
		"DESCRIPTION" => Cutil::PhpToJSObject($arTemplateParams),
	);
?>