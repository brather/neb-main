$(function(){
	$('[data-slide-togglable]').each(function(){
		var widget = $(this),
			filterListHeight = widget.find('dl').height();
		if (filterListHeight > 75) {
			widget.toggleClass('interactable',true);
		}

	});
});