<?php
/**
 * User: agolodkov
 * Date: 11.08.2015
 * Time: 13:08
 */

namespace Neb\Main\Stat;

use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\DataManager;
use \Bitrix\Main\Entity\ExpressionField;
use \Bitrix\Main\Type\Date;
use \Bitrix\Main\Type\DateTime;

use \Neb\Main\Bitrix\FormResultTable;
use \Nota\Collection\LibraryStatTable;
use \Nota\Exalead\SearchClient;
use \Nota\Exalead\SearchQuery;


/**
 * Class StatisticAggregator
 *
 * @package Neb\Main
 */
class StatisticAggregator
{
    /**
     * @var array
     */
    protected $_options = array();

    /**
     * @var string
     */
    protected $_dateFrom;

    /**
     * @var string
     */
    protected $_dateTo;

    /**
     * @var int
     */
    protected $_libraryId = 0;

    /**
     * @var array
     */
    protected $_libraryExaleadIdsArray = array();

    /**
     * @var bool
     */
    protected $_activeUsers = false;

    /**
     * @var array
     */
    protected $_exaleadResult;

    /**
     * @var array
     */
    protected $_librariesStatData = array();

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        foreach ($options as $optionName => $optionValue) {
            if (property_exists($this, $optionName)) {
                $this->$optionName = $optionValue;
                continue;
            }
            $optionName = '_' . $optionName;
            if (property_exists($this, $optionName)) {
                $this->$optionName = $optionValue;
            }
        }
        if (null === $this->_dateFrom) {
            $this->_dateFrom = time() - 86400;
        }
        if (null === $this->dateTo) {
            $this->dateTo = time();
        }
        if (is_string($this->_dateFrom)) {
            $this->_dateFrom = MakeTimeStamp($this->_dateFrom);
        }
        if (is_string($this->dateTo)) {
            $this->dateTo = MakeTimeStamp($this->dateTo);
        }

        Loader::includeModule("nota.exalead");
    }

    /**
     * Для сбора статистики по расписанию
     *
     * @param array $options
     *
     * @throws \Exception
     */
    public static function loadLibrariesStatistics($options = array())
    {
        // При импорте большого периода может нехватать памяти
        if (isset($options['dateFrom'])) {
            ini_set('memory_limit', '1024M');
        }
        if (!Loader::includeModule('iblock')) {
            throw new \Exception('module iblock not installed');
        }
        $result = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ACTIVE' => 'Y'],
            false,
            false,
            ['ID']
        );

        /** Сбор статистики по библиотекам */
        while ($library = $result->Fetch()) {
            $libraryOptions = $options;
            $libraryOptions['libraryId'] = $library['ID'];
            $instance = new static($libraryOptions);
            $instance->loadFullLibraryStats();
        }

        /** Сбор статистики по всем библиотекам */
        $instance = new static($options);
        $instance->loadFullLibraryStats();

        if (!Loader::includeModule('nota.collection')) {
            throw new \Exception('Module "nota.collection" not installed');
        }
        /**
         * Сбор статитики количества изданий из экзалида
         * по федеральным библиотекам
         */
        $ids = \nebLibrary::getLibraryIblockIdListByExalead(
            \nebLibrary::$federalLibraries
        );
        $instance = new static(
            array(
                'libraryIdsArray' => $ids,
                'libraryId'       => LibraryStatTable::FEDERAL_LIBRARIES_ID,
            )
        );
        $instance->prepareLibraryStatDates();
        $instance->countLibraryFields(
            array(
                'PUBLICATIONS_DD',
                'PUBLICATIONS_DIGITIZED',
                'PUBLICATIONS_DIGITIZED_DD',
            )
        );
        $instance->insertLibraryStatData();
    }

    /**
     * @return $this
     */
    public function prepareLibraryStatDates()
    {
        $date = $this->_dateFrom;
        while ($date <= $this->dateTo) {
            $this->_librariesStatData[ConvertTimeStamp($date)] = array();
            $date += 86400;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function loadFullLibraryStats()
    {
        $this->prepareLibraryStatDates();
        if ($this->_libraryId > 0) {
            $this->_libraryExaleadIdsArray[] = \nebLibrary::getLibraryIdByIblockId($this->_libraryId);
        }
        $this->countLibraryFields([
            'USERS',
            'ACTIVE_USERS',
            'PUBLICATIONS',
            'SEARCH_COUNT',
            'VIEWS',
            'DOWNLOADS',
            'FEEDBACK_COUNT',
            'VIEWS_BOOK',
            'PUBLICATIONS_DD',
            'PUBLICATIONS_DIGITIZED',
            'PUBLICATIONS_DIGITIZED_DD',
        ]);
        $this->insertLibraryStatData();

        return $this;
    }

    /**
     * @param $fields
     */
    public function countLibraryFields($fields)
    {
        $dailyData = [];
        foreach ($fields as $fieldName) {
            switch ($fieldName) {
                case'USERS':
                    $dailyData[$fieldName] = $this->countRegisterUsersDaily();
                    break;
                case'ACTIVE_USERS':
                    $dailyData[$fieldName] = $this->countActiveUsersDaily();
                    break;
                case'PUBLICATIONS':
                    $dailyData[$fieldName] = $this->countEditionsDaily();
                    break;
                case'SEARCH_COUNT':
                    $dailyData[$fieldName] = $this->countSearchQueriesDaily();
                    break;
                case'VIEWS':
                    $dailyData[$fieldName] = $this->countBookViewsDaily();
                    break;
                case'DOWNLOADS':
                    $dailyData[$fieldName] = $this->countBookReadDaily();
                    break;
                case'FEEDBACK_COUNT':
                    $dailyData[$fieldName] = $this->countFeedBacksDaily();
                    break;
                case'VIEWS_BOOK':
                    $dailyData[$fieldName] = $this->countBookViewsCountDaily();
                    break;
                case'PUBLICATIONS_DD':
                    $dailyData[$fieldName] = $this->countPublicationsDd();
                    break;
                case'PUBLICATIONS_DIGITIZED':
                    $dailyData[$fieldName] = $this->countPublicationsDigitized(
                    );
                    break;
                case'PUBLICATIONS_DIGITIZED_DD':
                    $dailyData[$fieldName]
                        = $this->countPublicationsDigitizedDd();
                    break;
            }
        }
        foreach ($dailyData as $fieldName => $data) {
            foreach ($data as $date => $count) {
                $this->_librariesStatData[$date][$fieldName] = $count;
            }
        }
    }

    /**
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Exception
     */
    public function insertLibraryStatData()
    {
        $libraryStatTabel = new LibraryStatTable();
        $rsLibraryStat = $libraryStatTabel->getList(
            array(
                'select' => array(
                    'ID',
                    'DATE',
                ),
                'filter' => array(
                    'DATE'       => array_keys($this->_librariesStatData),
                    'LIBRARY_ID' => $this->_libraryId
                ),
            )
        );
        $existsDates = array();
        while ($statData = $rsLibraryStat->fetch()) {
            /** @var DateTime $date */
            $date = $statData['DATE'];
            if (!$date instanceof DateTime) {
                continue;
            }
            $date = date(
                $date->convertFormatToPhp(FORMAT_DATE),
                $date->getTimestamp()
            );
            /** @var string $date */
            $existsDates[$date] = $statData['ID'];
        }

        foreach ($this->_librariesStatData as $date => $data) {
            $data['DATE'] = new Date($date);
            if ($this->_libraryId > 0) {
                $data['LIBRARY_ID'] = $this->_libraryId;
            }
            if ($existsDates[$date]) {
                $libraryStatTabel->update($existsDates[$date], $data);
            } else {
                $libraryStatTabel->add($data);
            }
        }

        return $this;
    }

    /**
     * @param $dateField
     *
     * @return array
     */
    public function countUsersDaily($dateField)
    {
        $rsUsers = $this->getUsers($dateField);
        $days = array_flip(array_keys($this->_librariesStatData));
        foreach ($days as &$cnt) {
            $cnt = 0;
        }
        $users = array();
        while ($userData = $rsUsers->Fetch()) {
            $users[] = $userData;
        }

        return $this->buildLibraryDailyCount($users, $dateField);
    }


    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public function countEditionsDaily()
    {
        return $this->countDataLibraryDaily(
            new NebStatEditionTable(),
            'DATE_STAT',
            'COUNT_EDITION',
            'ID_BITRIXDB',
            $this->_libraryId
        );
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public function countBookViewsDaily()
    {
        return $this->countDataLibraryDaily(
            new NebStatLogTable(),
            'DT',
            'CNT_VIEW',
            'ID_LIB',
            \nebLibrary::getLibraryIdByIblockId($this->_libraryId)
        );
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public function countBookReadDaily()
    {
        return $this->countDataLibraryDaily(
            new NebStatLogTable(),
            'DT',
            'CNT_READ',
            'ID_LIB',
            \nebLibrary::getLibraryIdByIblockId($this->_libraryId)
        );
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public function countFeedBacksDaily()
    {
        if (0 === $this->_libraryId) {
            return $this->countDataLibraryDaily(
                new FormResultTable(),
                'DATE_CREATE',
                // Считаем сумму ID формы, так как оно = 1 - получаем количество
                'FORM_ID',
                // ID формы вместо библиотеки
                'FORM_ID',
                1
            );
        }

        return array();
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public function countBookViewsCountDaily()
    {
        return $this->countDataLibraryDaily(
            new NebStatLogTable(),
            'DT',
            'CNT_VIEW',
            'ID_LIB',
            \nebLibrary::getLibraryIdByIblockId($this->_libraryId),
            'COUNT'
        );
    }

    /**
     * @param DataManager $dataManager
     * @param string      $dataField
     * @param string      $cntField
     * @param string      $libraryField
     * @param int         $libraryId
     * @param string      $function
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public function countDataLibraryDaily(
        DataManager $dataManager,
        $dataField,
        $cntField,
        $libraryField,
        $libraryId,
        $function = 'SUM'
    ) {
        $result = $dataManager->getList(
            $this->_buildTableFilterLibrary(
                $dataField,
                $cntField,
                $libraryField,
                $libraryId,
                $function
            )
        );
        $data = array();
        while ($item = $result->fetch()) {
            $data[] = $item;
        }

        return $this->buildLibraryDailyCount(
            $data, $dataField, $cntField
        );
    }

    /**
     * @param string   $dateField
     * @param string   $countField
     * @param string   $libraryField
     * @param null|int $libId
     * @param string   $function
     *
     * @return array
     */
    private function _buildTableFilterLibrary(
        $dateField,
        $countField,
        $libraryField,
        $libId = null,
        $function = 'SUM'
    ) {
        if (null === $libId) {
            $libId = $this->_libraryId;
        }
        $params = array(
            'filter' => array(
                '>=' . $dateField => new Date(
                    ConvertTimeStamp($this->_dateFrom, 'SHORT')
                ),
                '<=' . $dateField => new Date(
                    ConvertTimeStamp($this->dateTo, 'SHORT')
                ),
            ),
            'order'  => array(
                $dateField => 'desc'
            ),
            'select' => array(
                $dateField,
            ),
        );
        $params['group'] = array($dateField);
        $params['select'][] = new ExpressionField(
            $countField, $function . '(' . $countField . ')'
        );
        if (0 !== $libId) {
            $params['filter'][$libraryField] = $libId;
        }

        return $params;
    }


    /**
     * @return mixed
     */
    public function countSearchQueriesDaily()
    {
        $data = array();
        if (!is_dir(
            realpath(
                $_SERVER['DOCUMENT_ROOT']
                . '/../tmp/exalead-search-logs'
            )
        )
        ) {
            return $data;
        }
        foreach ($this->_librariesStatData as $date => $value) {
            $report = new ExaleadQueryReport(
                array(
                    'date'     => $date,
                    'filePath' => realpath(
                        $_SERVER['DOCUMENT_ROOT']
                        . '/../tmp/exalead-search-logs'
                    ),
                )
            );
            if ($this->_libraryId > 0) {
                $libId = \nebLibrary::getLibraryIdByIblockId($this->_libraryId);
            } else {
                $libId = $this->_libraryId;
            }
            if (null === $libId) {
                continue;
            }
            $result = $report->countByLibrary($libId);
            if ($libId != $this->_libraryId
                && isset($result[$this->_libraryId])
            ) {
                $result = $result[$this->_libraryId];
            } else {
                $result = $result[$libId];
            }
            foreach ($result as $resultDate => $queriesCount) {
                if (!isset($data[$resultDate])) {
                    $data[$resultDate] = 0;
                }
                $data[$resultDate] += $queriesCount;
            }
        }
        uksort(
            $data,
            function ($a, $b) {
                $a = MakeTimeStamp($a);
                $b = MakeTimeStamp($b);

                return $a - $b;
            }
        );

        return $data;
    }


    /**
     * @param array       &$data
     * @param string      $field
     * @param string|null $sumField
     *
     * @return array
     */
    public function buildLibraryDailyCount(&$data, $field, $sumField = null)
    {
        $days = array_flip(array_keys($this->_librariesStatData));
        foreach ($days as &$cnt) {
            $cnt = 0;
        }
        foreach ($data as $dataItem) {
            $date = ConvertTimeStamp(
                MakeTimeStamp($dataItem[$field]), 'SHORT'
            );
            if (!isset($days[$date])) {
                continue;
            }
            if (isset($dataItem[$sumField])) {
                $days[$date] += $dataItem[$sumField];
            } else {
                $days[$date]++;
            }
        }
        uksort(
            $days, function ($a, $b) {
            $a = MakeTimeStamp($a);
            $b = MakeTimeStamp($b);

            return $a - $b;
        }
        );

        return $days;
    }

    /**
     * @param $date
     *
     * @return bool|int
     */
    public function countRegisterUsers($date)
    {
        if (is_int($date)) {
            $date = ConvertTimeStamp($date, 'SHORT');
        }
        $nebUser = new \nebUser();
        $rsUsers = $nebUser->GetList(
            $by, $order, array(
                'DATE_REGISTER_2' => $date,
            )
        );

        return $rsUsers->SelectedRowsCount();
    }

    /**
     * @return array
     */
    public function countRegisterUsersDaily()
    {
        $days = $this->countUsersDaily('DATE_REGISTER');
        foreach ($days as $date => $count) {
            $days[$date] = $count;
        }

        return $days;
    }

    /**
     * @return array
     */
    public function countActiveUsersDaily()
    {
        return $this->countUsersDaily('LAST_LOGIN');
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function countPublicationsDigitized()
    {
        $result = array();
        if (!empty($this->_libraryExaleadIdsArray)) {
            $queryLibs = array();
            foreach ($this->_libraryExaleadIdsArray as $libId) {
                $queryLibs[] = 'idlibrary:' . $libId;
            }
            $q = implode(' OR ', $queryLibs);
        } else {
            $q = '#all';
        }
        $q .= ' AND filesize>0';
        $this->loadExaleadData(array('q' => $q));
        if (isset($this->_exaleadResult['COUNT'])) {
            $result[ConvertTimeStamp(time())] = $this->_exaleadResult['COUNT'];
        }

        return $result;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function countPublicationsDigitizedDd()
    {
        $result = array();
        if (!empty($this->_libraryExaleadIdsArray)) {
            $queryLibs = array();
            foreach ($this->_libraryExaleadIdsArray as $libId) {
                $queryLibs[] = 'idlibrary:' . $libId;
            }
            $q = implode(' OR ', $queryLibs);
        } else {
            $q = '#all';
        }
        $q .= ' AND filesize>0';
        $this->loadExaleadData(array('q' => $q));
        if (isset($this->_exaleadResult['NHITS'])) {
            $result[ConvertTimeStamp(time())] = $this->_exaleadResult['NHITS'];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function countPublicationsDd()
    {
        $result = array();
        $this->loadExaleadData();
        if (isset($this->_exaleadResult['NHITS'])) {
            $result[ConvertTimeStamp(time())] = $this->_exaleadResult['NHITS'];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function countPublicationsAll()
    {
        $result = array();
        $this->loadExaleadData();
        if (isset($this->_exaleadResult['COUNT'])) {
            $result[ConvertTimeStamp(time())] = $this->_exaleadResult['COUNT'];
        }

        return $result;
    }

    /**
     * @param array $params
     *
     * @throws \Exception
     */
    public function loadExaleadData(array $params = array())
    {
        if (isset($this->_librariesStatData[ConvertTimeStamp(time())])) {
            if (!Loader::includeModule('nota.exalead')) {
                throw new \Exception('Module "nota.exalead" mot installed');
            }
            $localParams = array(
                'q'  => null,
                'l'  => 'ru',
                'sl' => 'sl_statistic3',
            );
            if (!empty($this->_libraryExaleadIdsArray)) {
                $queryLibs = array();
                foreach ($this->_libraryExaleadIdsArray as $libId) {
                    $queryLibs[] = 'idlibrary:' . $libId;
                }
                $localParams['q'] = implode(' OR ', $queryLibs);
            } else {
                $localParams['q'] = '#all';
            }
            $params = array_merge(
                $localParams, $params
            );
            $query = new SearchQuery();
            $query->setTimeout(3600);
            foreach ($params as $paramName => $value) {
                $query->setParam($paramName, $value);
            }
            $client = new SearchClient();
            $this->_exaleadResult = $client->getResult($query);
        }
    }

    public function prepareExaleadParamsAndGetResult($params, $cached = true)
    {
        if ( !is_array($params) && count($params) <=0 )
            throw new \Exception("Exalead \$params is not array or empty");

        if ( !isset($params['q']) || empty($params['q']) )
            throw new \Exception("Exalead param \"q\" is not set or empty");

        $sParam = array();

        if ( is_array( $params['q'] ) )
        {

            if ( is_string( $params['q'][0] ) && $params['q'][0] == "OR" )
            {
                unset( $params['q'][0] );

                $sParam['q'] = implode( ' OR ', $params['q'] );
            }
            elseif ( is_array( $params['q'][0] ) || count( $params['q'] ) > 1 )
            {
                $sParam['q'] = implode( ' AND ', $params['q'] );
            }
            unset($params['q']);
        }
        elseif ( is_string( $params['q'] ) )
        {
            $sParam['q'] = $params['q'];

            unset($params['q']);
        }

        $sParam = array_merge_recursive($sParam, $params);


        $q = new SearchQuery();
        $q->setTimeout(3600);

        foreach ( $sParam as $paramName => $value )
        {
            $q->setParam( $paramName, $value );
        }

        $client = new SearchClient();
        $client->setCacheEnabled($cached);

        return $client->getResult( $q );
    }

     /**
     * @return array
     */
    public function getExaleadResult()
    {
        return $this->_exaleadResult;
    }

    /**
     * @param $dateField
     *
     * @return bool|\CDBResult|\mysqli_result|null
     */
    public function getUsers($dateField)
    {
        $filter = array(
            $dateField . '_1' => ConvertTimeStamp($this->_dateFrom, 'SHORT'),
            $dateField . '_2' => ConvertTimeStamp($this->dateTo, 'SHORT'),
        );
        if (0 !== $this->_libraryId) {
            $filter['UF_LIBRARIES'] = $this->_libraryId;
        }
        $nebUser = new \nebUser();

        return $nebUser->GetList($by, $order, $filter);
    }
}