<?php
/**
 * User: agolodkov
 * Date: 12.05.2016
 * Time: 11:10
 */

namespace BitrixBundle\DependencyInjection;

use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class BitrixHelper
 *
 * @DI\Service("bitrix_helper")
 */
class BitrixHelper
{
    /**
     * @var KernelInterface
     */
    public $kernel;


    /**
     * @var string
     */
    private $_appPath;

    /**
     * @var string
     */
    private $_documentRoot;

    /**
     * @var \CMain
     */
    private $_application;

    /**
     * @var \CUser
     */
    private $_user;

    /**
     * @var bool
     */
    private $_headerIncluded = false;

    /**
     * @DI\InjectParams({
     *     "kernel" = @DI\Inject("kernel")
     * })
     * @param KernelInterface $kernel
     *
     * @throws \Exception
     */
    public function __construct(KernelInterface $kernel)
    {
        define('BITRIX_DB_ERROR_EXCEPTION', true);
        $this->kernel = $kernel;
        $this->_appPath = $this->kernel->getRootDir();
        if (empty($this->_appPath)) {
            throw new \Exception('Application path is empty');
        }

        if (!isset($_SERVER['DOCUMENT_ROOT']) || empty($_SERVER['DOCUMENT_ROOT'])) {
            $this->_documentRoot = realpath(
                $this->_appPath . '/../neb/'
            );
        } else {
            $this->_documentRoot = $_SERVER['DOCUMENT_ROOT'];
        }
    }

    private function _initGlobals()
    {
        $this->checkIncludeThrowException();
        global $APPLICATION, $USER;
        $this->_application = $APPLICATION;
        $this->_user = $USER;
    }

    /**
     * @return string
     */
    public function getDocumentRoot()
    {
        return $this->_documentRoot;
    }

    public function requireProlog()
    {
        if (!isset($_SERVER['DOCUMENT_ROOT']) || empty($_SERVER['DOCUMENT_ROOT'])) {
            $_SERVER['DOCUMENT_ROOT'] = $this->_documentRoot;
        }

        require_once($this->getDocumentRoot() . '/bitrix/modules/main/include/prolog_before.php');
        $this->_initGlobals();
    }

    public function requireEpilog()
    {
        require_once($this->getDocumentRoot() . '/bitrix/modules/main/include/epilog_after.php');
    }

    public function requireHeader()
    {
        if (!isset($_SERVER['DOCUMENT_ROOT'])) {
            $_SERVER['DOCUMENT_ROOT'] = $this->_documentRoot;
        }
        if (!defined('START_EXEC_PROLOG_AFTER_1')) {
            $this->_headerIncluded = true;
            require_once($this->getDocumentRoot() . '/bitrix/header.php');
        }
        $this->_initGlobals();
    }

    public function requireFooter()
    {
        if (true === $this->_headerIncluded) {
            require_once($this->getDocumentRoot() . '/bitrix/footer.php');
        }
    }

    /**
     * @return Asset
     */
    public function getAsset()
    {
        return Asset::getInstance();
    }

    public function getUser()
    {
        $this->_initGlobals();

        return $this->_user;
    }

    public function includeModule($module)
    {
        if (!\CModule::IncludeModule($module)) {
            throw new \Exception("Bitrix module '$module' not installed");
        }
    }

    /**
     * @return bool
     */
    public function checkIncluded()
    {
        return defined('B_PROLOG_INCLUDED') && true === B_PROLOG_INCLUDED;
    }

    /**
     * @throws \Exception
     */
    public function checkIncludeThrowException()
    {
        if (false === $this->checkIncluded()) {
            throw new \Exception('Bitrix not included');
        }
    }

    /**
     * @return \CMain
     */
    public function getApplication()
    {
        if (!$this->_application instanceof \CMain) {
            $this->_initGlobals();
        }

        return $this->_application;
    }

    /**
     * @param string $name
     *
     * @return \Bitrix\Main\DB\Connection
     */
    public function getDbConnection($name = '')
    {
        return Application::getConnection($name);
    }

    /**
     * @param string $module
     * @param string $name
     *
     * @return bool|null|string
     */
    public function getOption($module, $name)
    {
        return \COption::GetOptionString($module, $name);
    }
}