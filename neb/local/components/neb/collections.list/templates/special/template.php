<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?
	if(empty($arResult['SECTIONS']))
		return false;
?>


<section class="innersection innerwrapper clearfix searchempty">
    <div class="b-collectionpage rel">
		<div class="b-filter b-filternums">
			<div class="b-filter_wrapper">
				<a href="#" class="sort sort_opener"><?=Loc::getMessage('COLLECTION_LIST_SORT'); ?></a>
				<span class="sort_wrap">
					<a <?=SortingExalead("element_cnt")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_COUNT'); ?></a>
					<a <?=SortingExalead("name")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_TITLE'); ?></a>
					<a <?=SortingExalead("created")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_DATE'); ?></a>
				</span>
				<span class="b-filter_act">					
					<span class="b-filter_show"><?=Loc::getMessage('COLLECTION_LIST_SHOW'); ?></span>
					<a href="<?=$APPLICATION->GetCurPageParam("pagen=10", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 10 ? ' current' : ''?>">10</a>
					<a href="<?=$APPLICATION->GetCurPageParam("pagen=25", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 25 ? ' current' : ''?>">25</a>
					<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 30 ? ' current' : ''?>">30</a>
				</span>
			</div>
		</div><!-- /.b-filter -->
        <div class="b-collection_list mode">
            <?
			if($arParams['PAGEN'] > 1)
				$n = ($arParams['PAGEN'] - 1) * $arParams['SIZEN'];
			else
				$n = 0;
			foreach($arResult['SECTIONS'] as $k => $collection):
				$n++;?>
                <div class="b-collection-item rel renumber">
                    <span class="num"><?=$n?>.</span>
                    <h2><a href="/special<?=$collection['SECTION_PAGE_URL']?>" title="<?=$collection['NAME']?>"><?=$collection['NAME']?></a></h2>
                    <div><span class="button_mode b-bookincoll"><?=$collection['ELEMENT_CNT']?> <?=GetEnding($collection['ELEMENT_CNT'], 'изданий', 'издание', 'издания')?> в подборке</span></div>
                    <div class="b-result_sorce_info"><em>Автор:</em> <?=$arResult['LIBRARY'][$collection['UF_LIBRARY']]['NAME']?></div>
                </div><!-- /.b-collection-item -->
            <?endforeach;?>
        </div><!-- /.b-collection_list -->
</section>

<?=$arResult['NAV_STRING']?>

