<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST' and check_bitrix_sessid()) {
    $arParams['eechb'] = trim(htmlspecialcharsEx($_REQUEST['eechb']));

    $rsUsers = CUser::GetList(
        ($by = "personal_country"),
        ($order = "desc"),
        array('UF_NUM_ECHB' => $arParams['eechb']),
        array(
            'FIELDS' => array('NAME', 'LAST_NAME', 'SECOND_NAME', 'DATE_REGISTER', 'ID'),
            'SELECT' => array('UF_NUM_ECHB', 'UF_STATUS')
        )
    );
    while ($arUser = $rsUsers->Fetch())
        $arResult['ITEMS'][] = $arUser;
}
$this->IncludeComponentTemplate();

$APPLICATION->setTitle('Проверка ЕЭЧБ');