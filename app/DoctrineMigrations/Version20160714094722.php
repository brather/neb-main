<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160714094722 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $startDate = date('Y-m-d', time() + 86400);
        $this->addSql(
            "
INSERT INTO b_agent
(MODULE_ID, NAME, ACTIVE, NEXT_EXEC, AGENT_INTERVAL, IS_PERIOD, RUNNING)
VALUES (
  'neb.main',
  '\\\Neb\\\Main\\\Agents\\\StatsSaver::saveLibraryBooksCount();',
  'Y',
  '$startDate 05:00:00',
  86400,
  'Y',
  'N'
);
"
        );
        $this->addSql(
            "
INSERT INTO b_agent
(MODULE_ID, NAME, ACTIVE, NEXT_EXEC, AGENT_INTERVAL, IS_PERIOD, RUNNING)
VALUES (
  'neb.main',
  '\\\Neb\\\Main\\\Agents\\\StatsSaver::saveLibraryUsersRegisters();',
  'Y',
  '$startDate 05:30:00',
  86400,
  'Y',
  'N'
);
"
        );
        $this->addSql(
            "
INSERT INTO b_agent
(MODULE_ID, NAME, ACTIVE, NEXT_EXEC, AGENT_INTERVAL, IS_PERIOD, RUNNING)
VALUES (
  'neb.main',
  '\\\Neb\\\Main\\\Agents\\\StatsSaver::saveLibraryBooksDigitizedCount();',
  'Y',
  '$startDate 06:00:00',
  86400,
  'Y',
  'N'
);
"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql(
            "DELETE FROM b_agent WHERE NAME = '\\\Neb\\\Main\\\Agents\\\StatsSaver::saveLibraryBooksCount();';"
        );
        $this->addSql(
            "DELETE FROM b_agent WHERE NAME = '\\\Neb\\\Main\\\Agents\\\StatsSaver::saveLibraryUsersRegisters();';"
        );
        $this->addSql(
            "DELETE FROM b_agent WHERE NAME = '\\\Neb\\\Main\\\Agents\\\StatsSaver::saveLibraryBooksDigitizedCount();';"
        );
    }
}
