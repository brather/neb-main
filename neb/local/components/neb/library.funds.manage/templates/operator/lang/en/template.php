<?
$MESS['LIBRARY_FUNDS_MANAGE_ADD_BOOK'] = "Add book";
$MESS['LIBRARY_FUNDS_MANAGE_SEARCH'] = "Find";
$MESS['LIBRARY_FUNDS_MANAGE_AUTHOR'] = "Author";
$MESS['LIBRARY_FUNDS_MANAGE_TITLE'] = "Title";
$MESS['LIBRARY_FUNDS_MANAGE_DATE_ADDED'] = "Date <br>of adding";
$MESS['LIBRARY_FUNDS_MANAGE_REQUEST_DEL'] = 'Removing <br> <span class="nowrap">request</span>';
$MESS['LIBRARY_FUNDS_MANAGE_DO_REQUEST_DEL'] = "<span>Request about deleting</span> from My library";
$MESS['LIBRARY_FUNDS_DELETE_CONFIRM'] = "You sure?";
$MESS['LIBRARY_FUNDS_DELETED'] = "Deleted";
