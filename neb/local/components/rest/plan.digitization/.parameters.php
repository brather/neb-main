<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'       => '/',
                'VARIABLES'     => array(
                    'token',
                    'book_id',
                    'idparent',
                    'comment',
                    'actionType',
                    'ID',
                    'UF_DATE_FINISH',
                    'UF_LIBRARY',
                    'UF_REQUEST_STATUS',
                    'UF_BOOK_FILE_path',
                ),
                'TYPE'          => array('POST', 'PUT'),
                'DOCUMENTATION' => array(
                    'GROUP'       => 'CONTROL',
                    'DESCRIPTION' => '',
                    'PARAMETERS'  => array()
                ),
            ),
        ),
    ),
); 