<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Iblock\IblockTable,
    \Bitrix\Main\Loader,
    \Bitrix\NotaExt\Iblock\Section,
    \Bitrix\NotaExt\Iblock\Element,
    \Nota\Library\Collection,
    \Nota\UserData\UsersBooksCollectionDataTable,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient;

Loader::includeModule('iblock');
Loader::includeModule('nota.library');
Loader::includeModule('nota.userdata');
Loader::includeModule('nota.exalead');

// Определение дополнительных параметров и необходимых в работе массивов
$documentRoot = Application::getDocumentRoot();
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
$context = Application::getInstance()->getContext();
CPageOption::SetOptionString("main", "nav_page_in_session", "N");
$arParams = array_replace(['PAGE_SIZE' => 15,], $arParams);
$arMess = array();
$arResult = array();

// Определим адрес страницы для редиректа
$arServ = $context->getServer();
$redirectUri = $arServ['REDIRECT_URL'];
$rParam = false;
$arRequestForSet = array('book_title', 'by', 'order', 'PAGEN_1');
foreach($arRequestForSet as $cRequest) {
    if(!empty($arRequest[$cRequest])) {
        if($rParam) {
            $redirectUri .= '&';
        } else {
            $redirectUri .= '?';
            $rParam = true;
        }
        $redirectUri .= $cRequest . '=' . $arRequest[$cRequest];
    }
}
if(!empty($arRequest['anchor'])) {
    $redirectUri .= '#' . $arRequest['anchor'];
}

// Добавление книги
if ($arRequest['action'] == 'addBook' && isset($arRequest['COLLECTION_ID'])
    && $arRequest['BOOK_NUM_ID'] > 0
    && check_bitrix_sessid()
) {
    if(is_array($arRequest['BOOK_NUM_ID'])) {
        foreach ($arRequest['BOOK_NUM_ID'] as $num_id) {
            Collection::addBook(
                $arRequest['COLLECTION_ID'],
                $arRequest['BOOK_ID_' . $num_id],
                $arRequest['BOOK_TITLE_' . $num_id],
                $arRequest['BOOK_AUTHOR_' . $num_id]
            );
        }
    } else {
        Collection::addBook(
            $arRequest['COLLECTION_ID'],
            $arRequest['BOOK_ID_' . $arRequest['BOOK_NUM_ID']],
            $arRequest['BOOK_TITLE_' . $arRequest['BOOK_NUM_ID']],
            $arRequest['BOOK_AUTHOR_' . $arRequest['BOOK_NUM_ID']]
        );
    }
    LocalRedirect($APPLICATION->GetCurPage());
}

// Удаление книги
if ($arRequest['action'] == 'removeBook' && isset($arRequest['COLLECTION_ID'])
    && isset($arRequest['BOOK_ID'])
    && check_bitrix_sessid()
) {
    Collection::removeBook((int)$arRequest['BOOK_ID']);
    //LocalRedirect($APPLICATION->GetCurPage());
    LocalRedirect($redirectUri);
}

// Сделать книгу обложкой
if ($arRequest['action'] == 'toggleBookCover' && isset($arRequest['BOOK_ID'])
    && check_bitrix_sessid()
) {
    Collection::toggleBookCover((int)$arRequest['BOOK_ID'], (bool)$arRequest['IS_COVER']);
    //LocalRedirect($APPLICATION->GetCurPage());
    LocalRedirect($redirectUri);
}

// Обновить сортировку книг
if($arRequest['BOOK_UPDATE_SORT'] == 'Y' && !empty($arRequest['BOOK_ID'])) {
    $newBook = new CIBlockElement;
    $arFields = array('SORT' => $arRequest['BOOK_VALUE_SORT']);
    $res = $newBook->Update($arRequest['BOOK_ID'], $arFields);
    if($res) {
        $arMess[$arRequest['BOOK_ID']]['SUCCESS'] = Loc::getMessage('BOOK_UPDATE_MESS_SUCCESS');
    } else {
        $arMess[$arRequest['BOOK_ID']]['ERROR'] = Loc::getMessage('BOOK_UPDATE_MESS_ERROR');
    }
    LocalRedirect($redirectUri);
}

// Определим параметры сортировки изданий в подборке
$by = trim(htmlspecialcharsEx($arRequest['by']));
$order = trim(htmlspecialcharsEx($arRequest['order']));
if(empty($by)) {
    $by = 'sort';
}
if(empty($order)) {
    $order = 'ASC';
}
$arDataSort = array(
    'sort' => 'SORT',
    'document_titlesort' => 'NAME',
    'document_authorsort' => 'PROPERTY_BOOK_AUTHOR',
    'document_publishyearsort' => 'DATE_CREATE',
);

// Определяем массив с параметрами
if(intval($arRequest['pagen']) > 0) {
    $arParams['ITEM_COUNT'] = intval($arRequest['pagen']);
} else {
    $arParams['ITEM_COUNT'] = $arParams['PAGE_SIZE'];
}
if ($arParams['ITEM_COUNT'] > 60) {
    $arParams['ITEM_COUNT'] = 60;
}
$arParams['NEXT_ITEM'] = intval($arRequest['next']);
$arParams['MODE'] = empty($arRequest['mode']) ? 'list' : trim($arRequest['mode']);
if (!empty($arRequest['dop_filter']) and $arRequest['dop_filter'] == 'Y'
    and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    $APPLICATION->RestartBuffer();
}
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'
    and empty($arRequest['dop_filter'])) {
    $arParams['ajax'] = true;
}

// Вернем id инфоблока с подборками
$rsBlock = IblockTable::getList(array(
    'filter' => array('CODE' => IBLOCK_CODE_COLLECTIONS),
    'select' => array('NAME', 'ID')
))->fetch();
$iblockId = $rsBlock['ID'];

// Возвращаем id подборки
$arFilterSection = array('IBLOCK_ID' => $iblockId, 'ID' => $arParams["SECTION_ID"]);
$arSelectSection = array(
    'ACTIVE',
    'ID',
    'IBLOCK_ID',
    'DEPTH_LEVEL',
    'SECTION_PAGE_URL',
    'NAME',
    'DESCRIPTION',
    'TIMESTAMP_X',
    'DETAIL_PICTURE',
    'UF_LIBRARY',
    'UF_SLIDER',
    'UF_SHOW_NAME',
    'UF_ADDED_FAVORITES',
    'UF_VIEWS',
    'UF_BG_POSITION',
);
$arResult['SECTION'] = CIBlockSection::GetList(array(), $arFilterSection, false, $arSelectSection)->GetNext();
//$arResult['SECTION'] = CIBlockSection::GetByID($arParams["SECTION_ID"])->GetNext();

if ($arParams["SECTION_ID"] > 0 && empty($arResult['SECTION'])) {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
    require_once $_SERVER['DOCUMENT_ROOT'] . '/404.php';
    return;
}

// Возвращаем параметры библиотеки (url библиотеки)
if (!empty($arResult['SECTION'])) {
    $APPLICATION->SetTitle($arResult['SECTION']['NAME']);
    $arResult['LIBRARY'] = Element::getByID($arResult['SECTION']['UF_LIBRARY'], array('DETAIL_PAGE_URL'));
}

// Вернем все подразделы подборки
$arSectionsId = array($arResult['SECTION']['ID']);                  // Массив с id подборки и его подразделами
$arAllSections[$arResult['SECTION']['ID']] = $arResult['SECTION'];  // Массив с информацией по подборке и его подразделам
$arSortSubSection = array('SORT' => 'asc');
$arFilterSubSection = array(
    'IBLOCK_ID' => $iblockId,
    'SECTION_ID' => $arResult['SECTION']['ID']
);
$arSelectSubSection = array(
    'ID',
    'CODE',
    'NAME',
    'DEPTH_LEVEL',
    'SECTION_PAGE_URL',
    'TIMESTAMP_X',
    'UF_LIBRARY',
    'UF_ADDED_FAVORITES',
    'UF_VIEWS'
);
$arSubSections = Section::getList($arFilterSubSection, $arSelectSubSection, $arSortSubSection);
$arResult["AR_SUB_SECTIONS"] = $arSubSections;
foreach($arSubSections as $arSubSection) {
    $arSectionsId[] = $arSubSection['ID'];
    $arAllSections[$arSubSection['ID']] = $arSubSection;
}

// Вернем все элементы подборки вместе с подразделами
$arFilterBookItems = array(
    'IBLOCK_ID' => $iblockId,
    //'SECTION_ID' => $arResult['SECTION']['ID'],
    'SECTION_ID' => $arSectionsId,
    '!PROPERTY_BOOK_ID_VALUE'  => false
);
// Если есть фильтр по названию книги (в лк оператора библиотеки)
if(!empty($arRequest['book_title'])) {
    $arFilterBookItems['%NAME'] = $arRequest['book_title'];
}

$arNavParams = array("nPageSize" => $arParams["PAGE_SIZE"], "bShowAll" => false);
$arSelectBookItems = array(
    'ID',
    'NAME',
    'SORT',
    'DATE_CREATE',
    'DETAIL_PAGE_URL',
    'PROPERTY_BOOK_ID',
    'PROPERTY_LIBRARY_SOURCE',
    'PROPERTY_BOOK_AUTHOR',
    'SECTION_ID'
);
$cIBlockElement = new CIBlockElement();
$rsBookFavItems = $cIBlockElement->GetList(array($arDataSort[$by] => $order), $arFilterBookItems, false, $arNavParams, $arSelectBookItems);
$arResult["STR_NAV"] = $rsBookFavItems->GetPageNavStringEx($navComponentObject, "", '');
$arResult['NAV_NAME_NUMBER'] = (integer)$rsBookFavItems->NavPageNomer;
$arResult['SECTION']['CNT'] = $rsBookFavItems->NavRecordCount;
$arResult['START_ITEM'] = ($arResult['NAV_NAME_NUMBER']-1) * $arParams["PAGE_SIZE"];
$arBookFavoriteSection = array();
$arBookSlider = array();
$arBitrixBooks = array();
while($obBookItem = $rsBookFavItems->GetNext()) {
    $typeInSlider = CIBlockElement::GetProperty($iblockId, $obBookItem['ID'], 'SORT', 'ASC', array('CODE' => 'TYPE_IN_SLIDER'))->Fetch();
    $arBookFavoriteSection[$obBookItem['ID']] = array(
        'ID'                        => $obBookItem['ID'],
        'NAME'                      => $obBookItem['NAME'],
        'SECTION_ID'                => $obBookItem['SECTION_ID'],
        'SORT'                      => $obBookItem['SORT'],
        'IBLOCK_ID'                 => $obBookItem['IBLOCK_ID'],
        'DATE_CREATE'               => $obBookItem['DATE_CREATE'],
        'DETAIL_PAGE_URL'           => $obBookItem['DETAIL_PAGE_URL'],
        'BOOK_ID'                   => trim($obBookItem['PROPERTY_BOOK_ID_VALUE']),
        'BOOK_AUTHOR'               => $obBookItem['PROPERTY_BOOK_AUTHOR_VALUE'],
        'LIBRARY_SOURCE'            => $obBookItem['PROPERTY_LIBRARY_SOURCE_VALUE'],
        'TYPE_IN_SLIDER' => array(
            'PROPERTY_VALUE_ID'     => $typeInSlider['PROPERTY_VALUE_ID'],
            'VALUE'                 => $typeInSlider['VALUE'],
            'VALUE_ENUM'            => $typeInSlider['VALUE_ENUM'],
            'VALUE_XML_ID'          => $typeInSlider['VALUE_XML_ID']
        )
    );
    if(!empty($typeInSlider['VALUE'])) {
        $arBooksSlider[] = $arBookFavoriteSection[$obBookItem['ID']];
    }
    $arBitrixBooks[$obBookItem['PROPERTY_BOOK_ID_VALUE']] = Array(
        'ID'       => $obBookItem['ID'],
        'SORT'     => $obBookItem['SORT'],
        'IS_COVER' => $typeInSlider["VALUE_XML_ID"] == COLLECTION_SLIDER_TYPE_COVER_XML_ID,
    );
}

// Заполняем массив $arResult['ITEMS'] из exalead
$query = new SearchQuery();
$client = new SearchClient();
$countBookShowInSlider = 0;
$iItem = 0;
foreach($arBookFavoriteSection as $idBook => $arBook) {

    // Запрос на получение информации с exalead
    $result = array();
    $query->setParam('sl', 'sl_nofuzzy');
    $query->getById($arBook['BOOK_ID']);
    $result = $client->getResult($query);

    if(empty($arBook['BOOK_AUTHOR']) || is_null($arBook['BOOK_AUTHOR'])) {
        if(!empty($result['authorbook'])) {
            // Если у текущей книги поле автора пустое и при запросе к exalead поле автора существует, обновим элемент инфоблока
            CIBlockElement::SetPropertyValuesEx($arBook['ID'], $iblockId, array('BOOK_AUTHOR' => $result['authorbook']));
            $arBook['BOOK_AUTHOR'] = $result['authorbook'];
        }
    }

    // Возвращаем изображение издания
    $bookPicture = '/upload/tmp_thumbnail/' . substr($arBook['BOOK_ID'], -3) . '/' . $arBook['BOOK_ID'] . 'h125w80.png';
    if(!file_exists($documentRoot . $bookPicture)) {
        $bookPicture = $result['IMAGE_URL'];
    } else {
        $arBookFavoriteSection[$idBook]['IMAGE_URL'] = $bookPicture;
    }

    // Возвращаем источник издания
    if(!empty($arBook['LIBRARY_SOURCE']) || is_null($arBook['LIBRARY_SOURCE'])) {
        if(!empty($result['library'])) {
            CIBlockElement::SetPropertyValuesEx($arBook['ID'], $iblockId, array('LIBRARY_SOURCE' => $result['library']));
            $arBook['LIBRARY_SOURCE'] = $result['library'];
        }
    }

    // Показ издания в слайдере
    if(!empty($arBook['TYPE_IN_SLIDER']['VALUE']) && $countBookShowInSlider == 0) {
        $arResult['COVER_IMAGES'] = $result['IMAGE_URL'];
        $countBookShowInSlider++;
    }

    // Вернем статистику по просмотрам
    $arStatisticsView = array();
    if ($this->__templateName == "library_profile") {
        $arStatisticsView = Collection::getBookStat($arBook['BOOK_ID']);
    }

    if($arBitrixBooks[$arBook['BOOK_ID']]['ID'] == $arRequest['BOOK_IBLOCK_ID']) {
        $messForItem = $arMess[$arRequest['BOOK_IBLOCK_ID']];
    } else {
        $messForItem = '';
    }
    // Ошибка кроется при передаче одного из параметров
    $arResult['ITEMS'][$iItem] = $result;
    $arResult['ITEMS'][$iItem]['SORT'] = $arBook['SORT'];
    $arResult['ITEMS'][$iItem]['BOOK_IBLOCK_ID'] = $arBook['ID'];
    $arResult['ITEMS'][$iItem]['SECTION_ID'] = $arBook['SECTION_ID'];
    $arResult['ITEMS'][$iItem]['SECTION_NAME'] = $arAllSections[$arBook['SECTION_ID']]['NAME'];
    $arResult['ITEMS'][$iItem]['BITRIX'] = $arBitrixBooks[$arBook['BOOK_ID']];
    $arResult['ITEMS'][$iItem]['STAT'] = $arStatisticsView;
    $arResult['ITEMS'][$iItem]['UPDATE_SORT'] = array(
        'BOOK_ID' => $arBitrixBooks[$arBook['BOOK_ID']]['ID'],
        'SORT' => $arBitrixBooks[$arBook['BOOK_ID']]['SORT'],
        'MESS' => $messForItem
    );

    $iItem++;
}

// Определяем параметр подборки подборки по принадлежности пользователю
$arResult['IN_MY_COLLECTIONS'] = false;
if ($USER->IsAuthorized()) {
    $collection = UsersBooksCollectionDataTable::getList(
        array(
            'filter' => array(
                'UF_UID'           => $USER->GetID(),
                'UF_COLLECTION_ID' => $arParams['SECTION_ID'],
            )
        )
    );
    if ($collection->getSelectedRowsCount() > 0) {
        $arResult['IN_MY_COLLECTIONS'] = true;
    }
}

// Обрабатываем изображенеи подборки
if ($arResult['SECTION']['DETAIL_PICTURE'] > 0) {
    $File = CFile::ResizeImageGet(
        $arResult['SECTION']['DETAIL_PICTURE'],
        array('width' => 300, 'height' => 300), BX_RESIZE_IMAGE_PROPORTIONAL,
        true
    );
    $arResult['SECTION']['DETAIL_PICTURE'] = $File['src'];
} else {
    $arResult['SECTION']['DETAIL_PICTURE'] = '/local/templates/.default/markup/pic/pic_32.png';
}

// Обрабатываем время
$arResult['SECTION']['TIMESTAMP_X'] = FormatDateFromDB(
    $arResult['SECTION']['TIMESTAMP_X'], 'SHORT'
);

// Определяем тип запроса (ajax или нет)
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    exit();
}

// Подключаем шаблон
$this->IncludeComponentTemplate();
