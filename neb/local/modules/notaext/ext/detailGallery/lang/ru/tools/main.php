<?
$MESS ['EDBG_TAB_UPLOAD_TITLE'] = "Загрузка";
$MESS ['EDBG_TAB_SEARCH_TITLE'] = "Поиск";
$MESS ['EDBG_TAB_EDIT_TITLE'] = "Редактирование";
$MESS ['EDBG_DRAG_FILES'] = "Перетащите файл сюда";
$MESS ['EDBG_UPLOAD_BUTTON'] = "Загрузить с компьютера";
$MESS ['EDBG_DRAG_PHOTO'] = "Перетащите фотографии, чтобы изменить порядок";
$MESS ['EDBG_PHOTO_HIDE'] = "Скрыть";
$MESS ['EDBG_PHOTO_HIDE_TITLE'] = "Скрыть это фото";
$MESS ['EDBG_PHOTO_EDIT'] = "Редактировать";
$MESS ['EDBG_PHOTO_EDIT_TITLE'] = "Редактировать это фото";

$MESS ['EDBG_INSERT_TEMPLATE'] = "Шаблон для вставки";
$MESS ['EDBG_INSERT_BUTTON'] = "Вставить";
$MESS ['EDBG_MESS_ONE_PHOTO'] = "В выбранном шаблоне можно отобразить только одино фото. Продолжить?";
$MESS ['EDBG_MESS_ADD_PHOTO'] = "Добавить фото пожалуйста";

$MESS ['EDBG_SEARCH_BUTTON'] = "Искать";
$MESS ['EDBG_CHOOSE_BUTTON'] = "Выбрать # файл(ов)";
