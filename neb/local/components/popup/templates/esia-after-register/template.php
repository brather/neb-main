<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;

?>
<section class="innerwrapper clearfix">
    <div class="b-registration b-registration-popup rel">
        <form id="regform" enctype="application/x-www-form-urlencoded"
              name="regform" class="b-form b-form_common b-regform"
              method="post">

            <p class="note" style="margin: 0 auto;">
                <?= Loc::getMessage('ESIA_ACCESS_QUESTION') ?>
            </p>

            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell " style="margin: 0 auto;">
                    <div class="field clearfix divdisable">
                        <button class="formbutton" value="1" type="button"
                                onclick="document.location.href = '/profile/update_select/'; return false;">
                            <?= Loc::getMessage('ESIA_ACCESS_QUESTION_YES') ?>
                        </button>
                        <button class="formbutton" value="1" type="button"
                                onclick="document.location.href = '/';">
                            <?= Loc::getMessage('ESIA_ACCESS_QUESTION_NO') ?>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- /.b-registration-->
</section>