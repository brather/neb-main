<?

use \Bitrix\Main;
use \Bitrix\NotaExt\NPHPCacheExt;
use \Bitrix\NotaExt\Iblock\IblockTools;
use \Neb\Main\Helper\IpHelper;

/**
 * Рабочие места НЭБ в читальных залах библиотек
 */
class NebWorkplaces
{
    /**
     * Код инфоблока "Рабочие места"
     */
    const IBLOCK_CODE = 'workplaces';

    /**
     * Определяет находится ли пользователь в читальном зале одной из библиотек (ИБ "Рабочие места")
     *
     * @param null $userIp
     * @param array $listParams
     * @param bool $excludeElar
     * @return bool
     */
    public static function checkAccess($userIp = null, $listParams = [], $excludeElar = false)
    {
        if (empty($userIp))
            $userIp = $_SERVER['REMOTE_ADDR'];

        $libraryIps = static::getIpList($listParams);
        foreach ($libraryIps as $key => $libraryIp) {
            if (IpHelper::validateIpInRange($userIp, $libraryIp)) {
                return true;
            }
        }

        $bLocalDev = self::checkLocalDevAccess($userIp);

        if (empty($listParams) && !$excludeElar && !empty($bLocalDev))
            return true;

        return false;
    }

    /**
     * Определяет находится ли IP в читальном зале одной из библиотек (ИБ "Рабочие места")
     *
     * @param $ip
     * @return bool
     */
    public static function checkAccessEx($ip)
    {
        $libraryIps = static::getIpList();
        foreach ($libraryIps as $key => $libraryIp)
            if (IpHelper::validateIpInRange($ip, $libraryIp))
                return true;

        return false;
    }

    /**
     * Возвращает закешированный массив ip адресов и диапазонов ip адресов библиотек
     *
     * @param array $parameters
     * @return array|mixed
     * @throws Main\LoaderException
     */
    public static function getIpList($parameters = [])
    {
        $arResult = array();

        if ($iIblockId = IblockTools::getIBlockId(static::IBLOCK_CODE)) {
            $obCacheExt = new NPHPCacheExt();
            $arCacheParams = func_get_args();
            $arCacheParams['iblock_id']  = $iIblockId;
            $arCacheParams['parameters'] = $parameters;
            $cacheTag = "iblock_id_" . $iIblockId;

            if (!$obCacheExt->InitCache(__function__, $arCacheParams, $cacheTag)) {
                if (!Main\Loader::includeModule('iblock'))
                    throw new Main\LoaderException('iblock module not installed!');

                $sort = array('ID' => 'ASC');
                $filter = array('IBLOCK_ID' => $iIblockId, 'ACTIVE' => 'Y');
                $select = array('ID', 'PROPERTY_IP');

                if (isset($parameters['ORDER'])) {
                    $sort = array_replace_recursive(
                        ['ID' => 'ASC'],
                        $parameters['ORDER']
                    );
                }
                if (isset($parameters['FILTER'])) {
                    $filter = array_replace_recursive(
                        ['IBLOCK_ID' => $iIblockId, 'ACTIVE' => 'Y'],
                        $parameters['FILTER']
                    );
                }

                $obIBlockElement = new \CIBlockElement();
                $rsWorkplaces = $obIBlockElement->GetList($sort, $filter, false, false, $select);
                while ($arItem = $rsWorkplaces->Fetch()) {
                    if (is_array($arItem['PROPERTY_IP_VALUE']))
                        foreach ($arItem['PROPERTY_IP_VALUE'] as $key => $val)
                            $arResult[] = $val;
                    else
                        $arResult[] = $arItem['PROPERTY_IP_VALUE'];
                }

                $arResult = array_unique($arResult);
                sort($arResult);

                $obCacheExt->StartDataCache($arResult);
            } else {
                $arResult = $obCacheExt->GetVars();
            }
        }

        return $arResult;
    }

    /**
     * Получает ЭЧЗ по IP адресу
     *
     * @param null $userIp - IP адрес
     * @param string $sActive - флаг активности
     * @return array|bool
     * @throws Main\LoaderException
     */
    public static function getWorkplacesByIp($userIp = null, $sActive = 'Y')
    {
        if (empty($userIp))
            $userIp = $_SERVER['REMOTE_ADDR'];

        $arResult = [];

        // получение ЭЧЗ Элар для локальной разработки
        /*$arElar = self::checkLocalDevAccess($userIp, $sActive);
        if ($arElar['ID']) {
            $arResult = [$arElar['ID'] => $arElar];
        }*/

        if (empty($arResult) && $iIblockId = IblockTools::getIBlockId(static::IBLOCK_CODE)) {

            if (!Main\Loader::includeModule('iblock'))
                throw new Main\LoaderException('iblock module not installed!');

            $obIBlockElement = new \CIBlockElement();

            $arFilter = ['IBLOCK_ID' => $iIblockId];
            if (in_array($sActive, ['Y', 'N']))
                $arFilter['ACTIVE'] = $sActive;

            $rsWorkplaces = $obIBlockElement->GetList(
                ['ID' => 'ASC'],
                $arFilter,
                false,
                false,
                ['ID', 'ACTIVE', 'PROPERTY_IP', 'PROPERTY_LIBRARY']
            );
            while ($arItem = $rsWorkplaces->Fetch()) {
                if (is_array($arItem['PROPERTY_IP_VALUE'])) {
                    foreach ($arItem['PROPERTY_IP_VALUE'] as $key => $val) {
                        if (IpHelper::validateIpInRange($userIp, $val) !== false) {
                            $arResult[$arItem['ID']] = $arItem;
                            break;
                        }
                    }
                } else {
                    if (IpHelper::validateIpInRange($userIp, $arItem['PROPERTY_IP_VALUE']) !== false) {
                        $arResult[$arItem['ID']] = $arItem;
                    }
                }
            }
        }

        return $arResult;
    }

    /**
     * Служебный метод: Метод подменяет локальный IP на IP Элар при проверке ЭЧЗ
     *
     * @param $userIp
     * @param string $sActive
     * @return array|false
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     */
    private static function checkLocalDevAccess($userIp, $sActive = 'Y') {

        if (nebUser::checkElar($userIp) && $iIblockId = IblockTools::getIBlockId(static::IBLOCK_CODE)) {

            if (!Main\Loader::includeModule('iblock'))
                throw new Main\LoaderException('iblock module not installed!');

            $arFilter = ['IBLOCK_ID' => $iIblockId, '?NAME' => 'ЭЛАР'];

            if (in_array($sActive, ['Y', 'N']))
                $arFilter['ACTIVE'] = $sActive;

            $obIBlockElement = new \CIBlockElement();
            $arElarWorkPlace = $obIBlockElement->GetList(
                [],
                $arFilter,
                false,
                false,
                ['ID', 'ACTIVE', 'PROPERTY_IP', 'PROPERTY_LIBRARY']
            )->Fetch();

            return $arElarWorkPlace;
        }

        return [];
    }
}
