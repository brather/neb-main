#!/bin/bash
# Database credentials
user="$2"
password="$3"
host="$4"
db_name="$1"
basedir=$(dirname "$0")
# Other options
backup_path="$basedir/../backups/db"
find $backup_path/* -mtime +10 -exec rm {} \;
date=$(date +"%d-%b-%Y")
# Set default file permissions
umask 177
# Dump database into SQL file
mysqldump --user=$user --password=$password --host=$host $db_name | gzip > $backup_path/$db_name-$date.sql.gz
