<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Loader,
    \Nota\UserData\Rgb;

global $USER;

Loader::includeModule('nota.userdata');

if (!$USER->IsAuthorized())
    LocalRedirect('/');

CJSCore::Init(array('popup', 'date')); // в будущем проверить используемость данных скриптов в ЛК

$arDefaultUrlTemplates404 = array(
    "main" => "/",
    "profile_edit" => "edit/"
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array();

$SEF_FOLDER = "";
$arUrlTemplates = array();

if ($arParams["SEF_MODE"] == "Y") {
    $arVariables = array();

    $arUrlTemplates    = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
    $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);
    $componentPage     = CComponentEngine::parseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

    $obNUser = new nebUser();
    $obNUser->getRole();

    if (in_array($obNUser->role, [UGROUP_LIB_CODE_ADMIN, UGROUP_LIB_CODE_EDITOR, UGROUP_LIB_CODE_CONTROLLER])) {
        $arLibrary = $obNUser->getLibrary();
        if ($arLibrary === false) {
            ?>
            <center>
                <?
                ShowError("Ошибка! Отсутствует привязка к библиотеке в профиле текущего пользователя.");
                ?>
            </center>
            <?
            return false;
        }
    }

    $arUser = $obNUser->getUser();

    $sourceComponentPage = $componentPage;


    if (empty($componentPage))
        $componentPage = "main";

    if ($obNUser->role == 'user')
        $componentPage .= "_user";
    elseif ($obNUser->role == UGROUP_LIB_CODE_ADMIN || $obNUser->role == UGROUP_LIB_CODE_EDITOR
        || $obNUser->role == UGROUP_LIB_CODE_CONTROLLER)
        $componentPage .= "_library";
    elseif ($obNUser->role == OPERATOR_WCHZ)
        $componentPage .= "_operatorWCHZ";
    elseif ($obNUser->role == UGROUP_RIGHTHOLDER_CODE)
        $componentPage .= "_" . UGROUP_RIGHTHOLDER_CODE;
    elseif ($obNUser->role == UGROUP_MARKUP_APPROVER)
        $componentPage .= "_" . UGROUP_MARKUP_APPROVER;
    else {
        $componentPage .= "_" . $obNUser->role;
    }

    /** Пока только для оператора */
    if (UGROUP_OPERATOR_CODE === $obNUser->role && !empty($sourceComponentPage)
        && !$obNUser->checkSectionAccess($sourceComponentPage)
        && !in_array(UGROUP_OPERATOR_CODE, $obNUser->arUserGroups)
    ) {
        $componentPage = 'access_deny';
    }

    if ($componentPage == 'profile_update_select_user' && $_POST['action'] == 'update_profile') {
        if (SITE_TEMPLATE_ID == "special") {
            LocalRedirect('/special/profile/' . htmlspecialchars($_POST['type']) . '/');
        } else {
            LocalRedirect('/profile/' . htmlspecialchars($_POST['type']) . '/');
        }
    }

    if ($componentPage == 'profile_update_rgb_user') {
        try {
            require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/CAS-1.3.3' . '/CAS.php';
            //phpCAS::setDebug();
            phpCAS::client(CAS_VERSION_2_0, 'passport.rsl.ru/auth', 443, '');
            phpCAS::setNoCasServerValidation();
            phpCAS::forceAuthentication();


            if (!phpCAS::hasAttribute('readerNumber') || !phpCAS::getAttribute('readerNumber')):
                throw new \Exception('Ошибка получения данных!');
            endif;

            $RGB_CARD_NUMBER = phpCAS::getAttribute('readerNumber');


            $rgb = new Rgb();
            $res = $rgb->usersFind('reader_number', $RGB_CARD_NUMBER);

            if (isset($res['users'][0])):
                $res = $res['users'][0];
            else:
                throw new \Exception('Пользователь с данным номером читательского билета не найден в РГБ!');
            endif;

            $arFieldsUser = array(
                'UF_REGISTER_TYPE' => nebUser::REGISTER_TYPE_RGB,
                'ACTIVE' => 'Y',
                'UF_RGB_CARD_NUMBER' => $RGB_CARD_NUMBER,
                'UF_RGB_USER_ID' => $res['uid'],
                'UF_LIBRARY' => RGB_LIB_ID,
                'UF_STATUS' => nebUser::USER_STATUS_VERIFIED,
            );

            $u = new CUser;
            $u->Update($USER->GetID(), $arFieldsUser);

            $nebUser = new nebUser();
            $nebUser->setEICHB();
            $arResult['RGB_CARD_NUMBER'] = $RGB_CARD_NUMBER;
            $arResult['EICHB'] = $nebUser->EICHB;
            $componentPage .= '_ok';
        } catch (\Exception $e) {
            $componentPage = 'profile_update_select_user';
            ?><br/>
            <center>
                <?
                ShowError($e->getMessage());
                ?>
            </center>
            <br/>
            <?

        }//}catch( \Exception $e){

    }// if($componentPage == 'profile_update_rgb_user'){
    CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

    $SEF_FOLDER = $arParams["SEF_FOLDER"];
}

$arResult = array(
    "FOLDER" => $SEF_FOLDER,
    "URL_TEMPLATES" => $arUrlTemplates,
    "VARIABLES" => $arVariables,
    "ALIASES" => $arVariableAliases,
    "LIBRARY" => $arLibrary,
    "USER" => $arUser,
    "CHECK_STATUS" => $arResult['CHECK_STATUS'],
    "ERROR_MESSAGE" => $arResult['ERROR_MESSAGE'],
    "USER_ROLE" => $obNUser->role,
);
$arResult['USER_ROLE'] = $obNUser->role;
#pre($componentPage,1);

if ($componentPage == "collections_user")	//Если у пользователя нет доступа до библиотеки, то не даем зайти на страницу коллекций библиотеки
{
    if(SITE_TEMPLATE_ID == "special")
    {
        LocalRedirect('/special/profile/');
    }
    else
    {
        LocalRedirect('/profile/');
    }
}

// страница подтверждения регистрации единая для всех пользователей
if (strpos($componentPage, 'confirm_')!== false)
    $componentPage = 'confirm';

$arResult['verifyUser'] = false;
if (isset($_REQUEST['USER_VERIFY']) && $_REQUEST['USER_VERIFY'] == 'Y')
{
    $arResult['verifyUser'] = true;
}

$arResult['placeRegisterOn'] = false;
if (isset($_REQUEST['REGISTER']['UF_PLACE_REGISTR']) && $_REQUEST['REGISTER']['UF_PLACE_REGISTR'] == 'on')
{
    $arResult['placeRegisterOn'] = true;
}

$arResult['componentPage'] = $componentPage;
$this->IncludeComponentTemplate('layout/main');