<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper;

$IBLOCK_GALLERY_ID = intval($_REQUEST['IBLOCK_GALLERY_ID']);
if ($IBLOCK_GALLERY_ID <= 0)
    exit();

global $USER;
if (!$USER->IsAuthorized())
    exit();

Loader::includeModule("iblock");

$iblock_permission = (new CIBlock())->GetPermission($IBLOCK_GALLERY_ID);
if ($iblock_permission < "R")
    exit();

$l = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => $IBLOCK_GALLERY_ID), array("ID", "NAME", "DEPTH_LEVEL", "EXTERNAL_ID"));

$arResult = array();

while ($a = $l->Fetch()) {
    $arResult[$a["ID"]] = array("NAME" => str_repeat(".", $a["DEPTH_LEVEL"]) . htmlspecialcharsbx($a["NAME"]), "XML_ID" => $a["EXTERNAL_ID"]);
}

MainHelper::showJson($arResult);
