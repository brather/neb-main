<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Loader,
    \Bitrix\Main\Data\Cache,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient;

/**
 * Class BbkListComponent
 */
class BbkListComponent extends \CBitrixComponent
{
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams['COLUMNS'] = 3;

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $res = self::getBbk();

        foreach ($res['hits'] as $i => $arHit) {
            foreach ($arHit['metas'] as $arItem) {
                if ($arItem['name'] == 'bbk_id') {
                    $this->arResult['ITEMS'][$i]['id'] = $arItem['value'];
                    $this->arResult['ITEMS'][$i]['priority'] = self::getPriority($arItem['value'], $i);
                } elseif ($arItem['name'] == 'bbk_fullname') {
                    $this->arResult['ITEMS'][$i]['fullname'] = $arItem['value'];
                }
            }
        }

        // сортировка по полю массива priority
        usort($this->arResult['ITEMS'], function ($a, $b) {
            return ($a['priority'] - $b['priority']);
        });

        $this->includeComponentTemplate();
    }

    /**
     * Получение массива ББК из Exalead
     *
     * @return array|bool|mixed|null|string
     */
    private static function getBbk() {

        $arResult = [];
        $cacheTime = 86400;
        $cacheDir  = CACHE_DIRECTORY . '/' . __CLASS__ . '/'. __FUNCTION__;
        $cacheId   = md5( $cacheDir );

        $obCache = Cache::createInstance();
        if ($obCache->initCache($cacheTime, $cacheId, $cacheDir)) {

            $arResult = $obCache->getVars();

        } elseif ($obCache->startDataCache()) {

            if (!Loader::includeModule('nota.exalead')) {
                return false;
            }

            // формирование запроса
            $query = new SearchQuery('bbk_level:1');
            $query->setParam('st', 'st_bbk');
            $query->setParam('hf', '100');
            $query->setParam('of', 'json');
            $query->setParam('sl', 'sl_bbk');
            $query->setQueryType('json');

            // отправка запроса
            $client = new SearchClient();
            $arResult = $client->getResult($query);

            $obCache->endDataCache($arResult);
        }

        return $arResult;
    }

    /**
     * Хардкод с приоритетами категорий для ровного вывода категорий
     *
     * @param $iSection - id раздела
     * @param $iStep    - шаг для сортировки по умолчанию
     * @return int
     */
    private static function getPriority($iSection, $iStep) {
        switch ($iSection) {
            case 1012:  $iRes = 1;  break;
            case 1110:  $iRes = 2;  break;
            case 10248: $iRes = 3;  break;
            case 12871: $iRes = 4;  break;
            case 19485: $iRes = 5;  break;
            case 25699: $iRes = 6;  break;
            case 26580: $iRes = 7;  break;
            case 30958: $iRes = 8;  break;
            case 37274: $iRes = 9;  break;
            case 34613: $iRes = 10; break;
            case 31944: $iRes = 11; break;
            default:    $iRes = 1000 + $iStep;
        }

        return $iRes;
    }
}