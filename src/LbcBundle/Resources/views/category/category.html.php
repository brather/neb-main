<?php
/** @var \Symfony\Bundle\FrameworkBundle\Templating\PhpEngine $view */
/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
/** @var \LbcBundle\DependencyInjection\MustacheEngine $mustache */
/** @var \Symfony\Bundle\FrameworkBundle\Templating\GlobalVariables $app */
/** @var array $data */
$slots = $view['slots'];
$mustache = $view['mustache_engine'];
$view->extend('LbcBundle::base.html.php');
foreach ($data['crumbs'] as $key => &$crumb) {
    if (is_array($crumb)) {
        $crumb = (object)$crumb;
    }
    $options = $crumb->options;
    $url = $app->getRequest()->getPathInfo();
    if ($crumb->options['urlCategory'] === $app->getRequest()->getPathInfo()) {
        $crumb->currentUrl = true;
    }
}
unset($crumb);

/* Показывать дополнительную опцию в поиске для оператора в группе CODE (operator) */
$arGroup = (new nebUser)->getUserGroups();

$slots->set(
    'body',
    $mustache->render(
        'bbk/search-form',
        [
            'query' => $app->getRequest()->query->all(),
            'operator' => (bool)array_search('operator', $arGroup)
        ]
    )
    . $mustache->render('bbk/category', $data)
    . (
    $data['pagination'] instanceof \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination
        ? $view['knp_pagination']->render($data['pagination'])
        : ''
    )
);
