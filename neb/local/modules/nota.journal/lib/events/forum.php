<?php

namespace Nota\Journal\Events;

use Nota\Journal\J;
use Bitrix\Main\Type;

class Forum
{
    function onAdd(&$ID, $arMessage, $messageTopicInfo, $messageForumInfo, $arFields){
        J::add('forum', 'add', Array('TEXT' => $arMessage['POST_MESSAGE'], 'DATE' => new Type\DateTime('', 'Y-m-d')));
    }

    function onEdit(&$ID, &$arFields, $arMessage_prev){
        J::add('forum', 'edit', Array('TEXT' => $arFields["POST_MESSAGE"], 'DATE' => new Type\DateTime('', 'Y-m-d')));
    }

    function onDelete($ID, $arMessage){
        J::add('forum', 'delete', Array('TEXT' => $arMessage['POST_MESSAGE'], 'DATE' => new Type\DateTime('', 'Y-m-d')));
    }
}