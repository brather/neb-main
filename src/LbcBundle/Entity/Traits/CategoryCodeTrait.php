<?php
/**
 * User: agolodkov
 * Date: 16.08.2016
 * Time: 15:53
 */

namespace LbcBundle\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;


trait CategoryCodeTrait
{

    /**
     * @var string
     *
     * @ORM\Column(name="Code", type="string", length=255, nullable=false)
     * @Serialization\Groups({"list", "details"})
     * @Assert\NotBlank()
     */
    public $code;

    /**
     * @var string
     *
     * @ORM\Column(name="Code_sort", type="string", length=255, nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    public $codeSort;

    /**
     * @ORM\PrePersist
     */
    public function beforePersist()
    {
        $this->prepareSortCode();
    }

    /**
     * @ORM\PreUpdate
     */
    public function beforeUpdate()
    {
        $this->prepareSortCode();
    }


    public function prepareSortCode()
    {
        $code = $this->getCode();
        $patterns = [
            "/([^0-9\\(\\/]+)([0-9]{1}($|[^0-9]{1}))/",
            "/([^0-9\\(\\/]+)([0-9]{2}($|[^0-9]{1}))/",
            "/([^0-9^\\(\\/]+)([0-9]+\\/)([0-9]{1}($|[^0-9]{1}))/",
            "/([^0-9^\\(\\/]+)([0-9]+\\/)([0-9]{2}($|[^0-9]{1}))/",
        ];
        $replacements = [
            '${1}00${2}',
            '${1}0${2}',
            '${1}${2}00${3}',
            '${1}${2}0${3}',
        ];
        $code = preg_replace($patterns, $replacements, $code);
        $this->setCodeSort($code);
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getCodeSort()
    {
        return $this->codeSort;
    }

    /**
     * @param string $codeSort
     */
    public function setCodeSort($codeSort)
    {
        $this->codeSort = $codeSort;
    }
}