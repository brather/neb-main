<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Loader;

Loader::includeModule('notaext');

use \Bitrix\Main\Localization\Loc;
use \Bitrix\NotaExt\Ext\DetailGallery\Params as DGParams;

class StandardElementListComponent extends CBitrixComponent
{
    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        if (empty($params['PARAMS_ITEMS']))
            return false;

        $arItems = DGParams::decode($params['PARAMS_ITEMS']);

        if (empty($arItems))
            return false;

        foreach ($arItems as &$item) {
            if (!empty($item['PARAM']))
                $item['Ar_PARAM'] = DGParams::decode($item['PARAM']);
            unset($item['PARAM']);
        }

        $params['PARAMS_ITEMS'] = $arItems;

        return $params;
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {

        $arId = array();
        foreach ($this->arParams['PARAMS_ITEMS'] as $item) {
            $arId[] = $item['ID'];
        }

        $arFilter = array(
            'IBLOCK_ID' => DGParams::getParam('save_ib'),
            'ID' => $arId,
        );

        $arSelect = array('ID', 'NAME', 'IBLOCK_ID', 'DETAIL_PICTURE', 'PREVIEW_TEXT', 'skip_other');
        $arSort = array();

        $resResult = Bitrix\NotaExt\Iblock\Element::getList($arFilter, false, $arSelect, $arSort);

        if (empty($resResult['ITEMS']))
            return false;

        $arItems = array();
        foreach ($resResult['ITEMS'] as $item)
            $arItems[$item['ID']] = array('PICTURE' => $item['DETAIL_PICTURE'], 'TEXT' => $item['PREVIEW_TEXT']);

        $this->arResult['ITEMS'] = $arItems;
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try {
            if (!empty($this->arParams['PARAMS_ITEMS'])) {
                $this->getResult();
                $this->includeComponentTemplate();
            }
        } catch (Exception $e) {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }
    }
}
