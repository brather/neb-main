<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if(empty($arResult['ITEMS']))
    return false;

CJSCore::Init();

if($_REQUEST['Ajax_instances'] != 'Y' && 'similar' !== $_REQUEST['listType']){
	?>
	<div class="b-addbook_popuptit similar clearfix">
		<h2><?=Loc::getMessage('SAME_LIST_SIMILAR')?></h2>
	</div>
	<span class="content-instances similar">
		<?
		}
		else
			$APPLICATION->RestartBuffer();
	?>
	<table class="b-usertable tsize">
		<tr>
			<th><span><?=Loc::getMessage('SAME_LIST_AUTHOR')?></span></th>
			<th><span><?=Loc::getMessage('SAME_LIST_NAME')?></span></th>
			<th><span><?=Loc::getMessage('SAME_LIST_DATE')?></span></th>
			<th><span><?=Loc::getMessage('SAME_LIST_LIB')?></span></th>
			<th><span><?=Loc::getMessage('SAME_LIST_READ')?></span></th>
		</tr>
		<?
			foreach($arResult['ITEMS'] as $arElement)
			{
			?>
			<tr>
				<td class="pl15"><?=$arElement['AUTHOR']?></td>
				<td class="pl15"><a class="popup_opener ajax_opener coverlay" data-width="955" href="<?=$arElement['LINK']?>"><?=$arElement['TITLE']?></a></td>
				<td class="pl15"><?=$arElement['PUBLISHYEAR']?></td>
				<td>
                    <?php if(empty($arElement['DETAIL_PAGE_URL'])) { ?>
                        <?=$arElement['LIBRARY']?>
                    <?php } else {?>
                        <a target="_blank" href="<?=$arElement['DETAIL_PAGE_URL']?>"><?=$arElement['LIBRARY']?></a>
                    <?php } ?>
                </td>
				<td><?if(!empty($arElement['VIEWER_URL']) and $arElement['IS_READ'] == 'Y'){?>
                        <a href="javascript:void(0);" target="_blank"
                           onclick="readBook(event, this); return false;"
                           data-link="<?= $arElement['ID'] ?>"
                           data-options="<?php echo htmlspecialchars(
                               json_encode(
                                   array(
                                       'title'  => trim(strip_tags($arElement['AUTHOR'])),
                                       'author' => trim($arElement['TITLE'])
                                   )
                               ),
                               ENT_QUOTES,
                               'UTF-8'
                           )?>">
                        <button type="submit" value="1" class="formbutton"><?=Loc::getMessage('SAME_LIST_READ')?></button>
                    </a>
                    <?}?></td>
			</tr>
			<?
			}
		?>
	</table>
	<span class="nav-instances similar">
		<?=$arResult['STR_NAV']?>
	</span>
	<?
		if($_REQUEST['Ajax_instances'] == 'Y' && 'similar' === $_REQUEST['listType'])
			exit();
	?>

</span>
<script type="text/javascript">
	$(function() {
		DOCUMENT.on('click','.nav-instances.similar a', function(e){
			var href = $(this).attr('href');
			if(href == '#')
				return false;

			BX.showWait(this);

			$.get(href, { Ajax_instances: "Y", listType: 'similar' }, function( data ) {
				$('.content-instances.similar').html(data);
				BX.closeWait();
				$('.iblock').cleanWS();

				var destination = $('.b-addbook_popuptit.similar').offset().top;
				if ($.browser.safari) {
					$('body').animate({ scrollTop: destination }, 1100);
				} else {
					$('html').animate({ scrollTop: destination }, 1100);
				}
			});
			return false;
		});
	});
</script>