<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "Если вы забыли пароль, введите E-Mail.<br />Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по E-Mail.";

$MESS ['AUTH_EMAIL'] = "E-Mail:";
$MESS ['RECOVERY'] = "Восстановление пароля";
$MESS ['REQUIRED'] = "Поле обязательно для заполнения";
$MESS ['SEND'] = "Отправить";

$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND'] = 'Адрес эл. почты не найден';
$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC'] = 'Введите электронную почту, указанную при регистрации';
$MESS['AUTH_PASS_RECOVERY_TRY_AGAIN'] = 'Попробуйте ввести другой адрес';
?>