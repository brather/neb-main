<?php
/**
 * User: agolodkov
 * Date: 14.09.2016
 * Time: 18:02
 */

namespace LbcBundle\Controller\Api;

use BitrixBundle\DependencyInjection\BitrixHelper;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use FOS\RestBundle\View\View;
use Gedmo\Tree\Entity\MappedSuperclass\AbstractClosure;
use LbcBundle\DependencyInjection\LbcCategory;
use LbcBundle\DependencyInjection\LbcCategorySortingHelper;
use LbcBundle\Entity\AbstractLbcCategory;
use LbcBundle\Entity\Traits\CategoryCodeTrait;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use BitrixBundle\Annotations\IncludeBitrix;


/**
 * Class LbcHistoryController
 *
 * @DI\Service("app.api.lbc.common_category", scope="request", public=true)
 */
class CommonLbcCategoryController
{
    /**
     * @DI\Inject("lbc.category")
     * @var LbcCategory
     */
    public $categoryHelper;

    /**
     * @DI\Inject("lbc.category_sorting")
     * @var LbcCategorySortingHelper
     */
    public $categorySorting;

    /**
     * @DI\Inject("request")
     * @var Request
     */
    public $request;

    /**
     * @DI\Inject("bitrix_helper")
     * @var BitrixHelper
     */
    public $bitrix;

    /**
     * @DI\Inject("service_container")
     * @var Container
     */
    public $container;

    /**
     * Lists entities.
     *
     * @param string $category
     * @param int    $id
     *
     * @Method("GET")
     * @Rest\View()
     * @IncludeBitrix()
     *
     * @return array
     */
    public function actionsCheckAction($category, $id)
    {
        $categoryService = $this->categoryHelper->getCategoryApiControllerService($category);
        if (null === $categoryService) {
            throw new NotFoundHttpException("Category $category not found");
        }
        /** @var AbstractLbcCategory $entity */
        if (!$entity = $categoryService->getRepository()->find($id)) {
            throw new NotFoundHttpException("Category $category:$id not found");
        }
        $this->categoryHelper->prepareEntity($entity);
        $this->categoryHelper->getEntityControllerService($entity)->checkEntityActions($entity);
        $actions = $entity->getActions();

        return [
            'actions' => $actions,
        ];
    }

    /**
     * @param string $category
     * @param int    $id
     *
     * @Method("PATCH")
     * @Rest\View()
     * @IncludeBitrix()
     *
     * @return static
     */
    public function sortCategoryUp($category, $id)
    {
        $this->categorySorting->moveCategory($category, $id, 'up');

        return View::create(null, 204);
    }

    /**
     * @param string $category
     * @param int    $id
     *
     * @Method("PATCH")
     * @Rest\View()
     * @IncludeBitrix()
     *
     * @return static
     */
    public function sortCategoryDown($category, $id)
    {
        $this->categorySorting->moveCategory($category, $id, 'down');

        return View::create(null, 204);
    }


    /**
     * @param         $category
     * @param Request $request
     *
     * @Method("POST")
     * @Rest\View()
     * @IncludeBitrix()
     *
     * @return static
     */
    public function categoryPostAction($category, Request $request)
    {
        $controller = $this->categoryHelper->getCategoryApiControllerService($category);

        return $controller->categoryPostAction($request);
    }

    /**
     * @param string $category
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return array
     */
    public function autoCompleteAction($category)
    {
        $allowFields = [
            'code' => true,
            'name' => true,
        ];
        $api = $this->categoryHelper->getCategoryApiControllerService($category);
        $query = $this->request->query->get('query');
        $field = $this->request->query->get('entityField');
        if (!isset($allowFields[$field])) {
            throw new BadRequestHttpException("Field $field not allowed!");
        }
        $entities = [];
        if (!empty($query)) {
            /** @var EntityRepository $repository */
            $repository = $api->getRepository();
            $qb = $repository->createQueryBuilder('t');
            $qb->setMaxResults(10);
            $qb->where("t.$field LIKE :query");
            $qb->setParameter('query', $query . '%');
            $entities = $qb->getQuery()->getResult();
        }

        return [
            'entities' => $entities
        ];
    }

    /**
     * @param string  $category
     * @param integer $id
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return BinaryFileResponse
     * @throws \Exception
     */
    public function exportCategoryAction($category, $id)
    {
        /** @todo придумать что нибудь с memory_limit, не должна такая штука в папблик смотреть */
        ini_set('memory_limit', '1024M');
        $columns = range('A', 'Z');
        $getColumn = function ($level) use ($columns) {
            $col = '';
            $nested = 0;
            while ($level > count($columns)) {
                $level -= count($columns);
                $nested++;
            }
            if ($nested > 0) {
                $col .= $columns[$nested];
            }
            $col .= $columns[$level];

            return $col;
        };
        $uploadDir = realpath($this->container->getParameter('dir.upload'));
        $uploadDir .= '/';
        if (!file_exists($uploadDir)) {
            throw new \Exception("Not found upload directory '$uploadDir'");
        }
        $uploadDir .= 'tmp/lbc-category/';

        $categoryService = $this->categoryHelper->getCategoryApiControllerService($category);
        if (null === $categoryService) {
            throw new NotFoundHttpException("Category $category not found");
        }
        /** @var AbstractLbcCategory $entity */
        if (!$entity = $categoryService->getRepository()->find($id)) {
            throw new NotFoundHttpException("Category $category:$id not found");
        }


        $excel = new \PHPExcel();
        $excel->getProperties()
            ->setTitle('ББК - ' . $entity->getName())
            ->setSubject('ББК - ' . $entity->getName());
        $sheet = $excel->setActiveSheetIndex(0);
        $sheet->setCellValue('A1', $entity->getName());

        $limit = 100;
        $offset = 0;
        $row = 2;
        $entityName = 'LbcBundle\Entity\\' . $this->categoryHelper->getCategoryEntityName($category);
        $qb = $categoryService->getRepository()->getChildrenQueryBuilder($entity, false, 'codeSort');
        $qb->setMaxResults($limit);
        $qb->innerJoin(
            $entityName . 'Closure',
            'bread',
            Join::WITH,
            $qb->expr()->eq('c.descendant', 'bread.descendant')
        );
        $qb->innerJoin(
            $entityName,
            'cat',
            Join::WITH,
            $qb->expr()->eq('cat.id', 'bread.ancestor')
        );
        $qb->groupBy('c.descendant');
        $qb->addSelect('GROUP_CONCAT(cat.codeSort ORDER BY bread.depth DESC) AS breadcrumbs');
        $qb->orderBy('breadcrumbs');
        $level = $entity->getLevel();
        do {
            $qb->setFirstResult($offset);
            /** @var AbstractClosure[] $entities */
            $entities = $qb->getQuery()->getResult();
            $offset += $limit;
            if ($entities) {
                foreach ($entities as $childrenEntity) {
                    if($childrenEntity[0] instanceof AbstractClosure) {
                        $childrenEntity = $childrenEntity[0];
                    }
                    /** @var AbstractLbcCategory $descendant */
                    $descendant = $childrenEntity->getDescendant();
                    if (!$col = $getColumn($descendant->getLevel() - $level)) {
                        $col = 'A';
                    }
                    $name = $descendant->getName();
                    if (property_exists($descendant, 'code')) {
                        /** @var CategoryCodeTrait $descendant */
                        $name = $descendant->getCode() . ' - ' . $name;
                    }
                    try {
                        $sheet->setCellValue(
                            $col . $row,
                            $name
                        );
                    } catch (\Exception $e) {
                        continue;
                    }
                    $row++;
                }
            }
        } while ($entities);

        $sheet->calculateColumnWidths();
        $excel->getActiveSheet()->setTitle('Statistics');
        $excel->setActiveSheetIndex(0);
        if (!file_exists($uploadDir)) {
            mkdir($uploadDir, 0777, true);
        }
        $fileName = $category . '-export-' . microtime(true) . '.xlsx';
        $filePath = $uploadDir . $fileName;
        $excelWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $excelWriter->save($filePath);
        $response = new BinaryFileResponse($filePath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }
}