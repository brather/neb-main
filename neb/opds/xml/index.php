<?php
require_once($_SERVER['DOCUMENT_ROOT']
    . '/bitrix/modules/main/include/prolog_before.php');
if (CModule::IncludeModule('nota.exalead')) {
    header('Content-Type:application/xml;charset=utf-8', true);
    echo file_get_contents(
        COption::GetOptionString('nota.exalead', 'viewer_protocol') . '://'
        . COption::GetOptionString('nota.exalead', 'viewer_ip')
        . ':' . COption::GetOptionString('nota.exalead', 'viewer_port')
        . '/opds/xml'
    );
}
