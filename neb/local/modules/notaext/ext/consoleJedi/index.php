<?

/**
 * Class example
 * Базовый класс для расширения.
 * Его название должно соответствовать названию расширения
 *
 * Если вам нужно сделать какие-то дополнительные действия (добавить какие-то ещё файлы, удалить, переместить и т.п.)
 * то вы можете воспользоваться коллбэками и прописать в них создание на сайте всех необходимых папок-файлов.
 */
class consoleJedi extends CExt
{
    var $EXT_ID = __CLASS__;
    var $EXT_VERSION;
    var $EXT_VERSION_DATE;
    var $EXT_NAME;
    var $EXT_DESCRIPTION;
    var $EXT_SORT;

    public function __construct()
    {
        parent::__construct();

        $this->EXT_VERSION = '1.0';
        $this->EXT_VERSION_DATE = '06.05.2014';

        $this->EXT_NAME = 'Console Jedi';
        $this->EXT_DESCRIPTION = 'Консольные утилиты и "визарды" для разворачивания сайтов.';
        $this->EXT_SORT = 10000;

    }

    /**
     * Копирование файлов происходит автоматически,
     * регистрация хэндлеров (для пользовательских свойств и т.п.) происходит автоматически
     * но если Ваше расширение требует дополнительных действий - Вы можете воспользоваться ниже представленными коллбэками.
     */

    function onBeforeInstall()
    {

    }

    function onBeforeUninstall()
    {

    }

    function onAfterInstall()
    {
        // Копируем файлы на уровень выше DOCUMENT_ROOT!
        $target_path = realpath($_SERVER['DOCUMENT_ROOT']."/../");
        CopyDirFiles(
            $_SERVER['DOCUMENT_ROOT']."/".$this->MODULE_HOLDER."/modules/notaext/ext/consoleJedi/shell/",
            $target_path,
            true,
            true
        );
        // И подменяем в демо-визарде путь к сайту, чтобы оный демо-визард мог найти Битрикс
        $lid = array_pop(explode('/',$_SERVER["DOCUMENT_ROOT"]));
        passthru("sed -i -e 's/s1/$lid/g' $target_path/_wizards/2014-05-27-t10000/run.php");

        // @todo: Также должен информировать разработчика о том, во что он вляпался и как с этим работать
        // @todo: На какой-то ман вести, что ли...

    }

    function onAfterUninstall()
    {

    }
}

?>