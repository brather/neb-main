<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$APPLICATION->IncludeComponent(
    'neb:user.edit',
    'edit',
    [
        'USER_ID' => nebUser::getCurrent()->GetId(),
    ],
    $component
);