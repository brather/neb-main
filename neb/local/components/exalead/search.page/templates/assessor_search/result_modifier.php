<?php

$arResult['sortingData'] = [
    'queryId'    => (integer)$arParams['queryId'],
    'editions'   => [],
    'total'      => $arParams['ITEM_COUNT'],
    'pageNumber' => (integer)($arResult['query'] instanceof \Nota\Exalead\SearchQuery
        ? $arResult['query']->nav['PAGEN']
        : 1),
];