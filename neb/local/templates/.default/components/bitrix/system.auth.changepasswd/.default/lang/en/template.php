<?
$MESS ['AUTH_NEW_PASSWORD'] = "New password";
$MESS ['AUTH_NEW_PASSWORD_CONFIRM'] = "Confirm password";
$MESS ['RECOVERY'] = "Recovery password";
$MESS ['DEFENSE'] = "Defense password";
$MESS ['REQUIRED'] = "Required";
$MESS ['SEND'] = "Send";
$MESS ['SIX'] = "six leter";
$MESS ['NATIONAL_ELECTRONIC_LIBRARY'] = "Национальная электронная библиотека";
$MESS ['MIN_LEN'] = "Must be at least {0} characters";
$MESS ['MAX_LEN'] = "Must be less or equal {0} characters long";
?>