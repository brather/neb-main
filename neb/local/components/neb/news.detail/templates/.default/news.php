<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$APPLICATION->SetPageProperty('hide-title', true);
?>
<div class="news-detail">
    <a href="/news/" class="news-detail__link">Cписок новостей НЭБ</a>
    <div class="news-detail__nav">
        <?if ($arResult["TOLEFT"]):?><i class="fa fa-long-arrow-left"></i><a href="<?=$arResult["TOLEFT"]["URL"]?>" class="news-detail__nav-link news-detail__nav-link--prev">Предыдущая</a><?endif;?>
        <?if ($arResult["TORIGHT"]):?><a href="<?=$arResult["TORIGHT"]["URL"]?>" class="news-detail__nav-link">Следующая</a><i class="fa fa-long-arrow-right"></i><?endif;?>
    </div>
    <br />
    <h1><?=$arResult["NAME"]?></h1>
    <?if ($arResult['PROPERTIES']['type']['VALUE']):
        ?><p>метка новости: <?=$arResult['PROPERTIES']['type']['VALUE']?></p><?
    endif;?>
    <div class="news-item">
        <?if($arResult['PREVIEW_PICTURE']):?>
            <div class="detailed-pic">
                <img src="<?=$arResult['PREVIEW_PICTURE']['SRC']?>" title="<?echo
                $arResult['PREVIEW_PICTURE']['DESCRIPTION']?:$arResult["NAME"]?>" class="detailed-pic__image" />
                <?if($arResult['PREVIEW_PICTURE']['DESCRIPTION']):?>
                    <div class="detailed-pic__title"><?=$arResult['PREVIEW_PICTURE']['DESCRIPTION']?></div>
                <?endif;?>
            </div>
        <?endif;?>
        <?=$arResult['DETAIL_TEXT']?>
    </div>

    <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
    <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki"></div>
</div>