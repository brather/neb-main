<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "ITEM_COUNT" => array(
            "PARENT" => "BASE",
            "NAME" => 'ITEM_COUNT',
            "TYPE" => "STRING",
            "DEFAULT" => "12",
        ),
        "LIBRARY_ID" => array(
            "PARENT" => "BASE",
            "NAME" => 'LIBRARY_ID',
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
    ),
);
