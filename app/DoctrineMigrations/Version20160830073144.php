<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160830073144 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('CREATE TABLE lbc_locationplans_closure (id INT AUTO_INCREMENT NOT NULL, ancestor INT NOT NULL, descendant INT NOT NULL, depth INT NOT NULL, INDEX IDX_C6B54A58B4465BB (ancestor), INDEX IDX_C6B54A589A8FAD16 (descendant), INDEX IDX_6A471B17E4AC3B1C (depth), UNIQUE INDEX IDX_C18D18893C6320F4 (ancestor, descendant), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;');
        $this->addSql('CREATE TABLE lbc_locationplansections_closure (id INT AUTO_INCREMENT NOT NULL, ancestor INT NOT NULL, descendant INT NOT NULL, depth INT NOT NULL, INDEX IDX_AFBA4EC0B4465BB (ancestor), INDEX IDX_AFBA4EC09A8FAD16 (descendant), INDEX IDX_8460E6654814221F (depth), UNIQUE INDEX IDX_37FE6ECBBE04FC20 (ancestor, descendant), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;');
        $this->addSql('CREATE TABLE lbc_maindivision_closure (id INT AUTO_INCREMENT NOT NULL, ancestor INT NOT NULL, descendant INT NOT NULL, depth INT NOT NULL, INDEX IDX_7D6803B7B4465BB (ancestor), INDEX IDX_7D6803B79A8FAD16 (descendant), INDEX IDX_10A12DBFF20DAB27 (depth), UNIQUE INDEX IDX_5F69D0BA16E61838 (ancestor, descendant), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;');
        $this->addSql('CREATE TABLE lbc_specialdivision_closure (id INT AUTO_INCREMENT NOT NULL, depth INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;');
        $this->addSql('CREATE TABLE lbc_specialdivisionsections_closure (id INT AUTO_INCREMENT NOT NULL, depth INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;');

        $this->addSql('ALTER TABLE lbc_category_history CHANGE category_id category_id INT UNSIGNED NOT NULL, CHANGE timestamp timestamp TIMESTAMP DEFAULT NOW();');
        $this->addSql('ALTER TABLE lbc_locationplanimages CHANGE Id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('ALTER TABLE lbc_locationplans CHANGE id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('ALTER TABLE lbc_locationplans ADD CONSTRAINT FK_541D2DFF856A684C FOREIGN KEY (parentid) REFERENCES lbc_locationplans (Id) ON DELETE CASCADE;');
        $this->addSql('CREATE INDEX IDX_541D2DFF856A684C ON lbc_locationplans (parentid);');
        $this->addSql('ALTER TABLE lbc_locationplansectionimages CHANGE Id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('ALTER TABLE lbc_locationplansections CHANGE Id Id INT AUTO_INCREMENT NOT NULL, CHANGE Code Code VARCHAR(255) NOT NULL;');
        $this->addSql('ALTER TABLE lbc_locationplansections ADD CONSTRAINT FK_41C42E2856A684C FOREIGN KEY (parentid) REFERENCES lbc_locationplansections (Id) ON DELETE CASCADE;');
        $this->addSql('CREATE INDEX IDX_41C42E2856A684C ON lbc_locationplansections (parentid);');
        $this->addSql('ALTER TABLE lbc_maindivision CHANGE Id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('CREATE INDEX IDX_ADE76031E9982EB8 ON lbc_maindivision (ParentId);');
        $this->addSql('ALTER TABLE lbc_maindivisionimages CHANGE Id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('ALTER TABLE lbc_maindivisionintroduction CHANGE Id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('ALTER TABLE lbc_maindivisionintroductionimages CHANGE Id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('ALTER TABLE lbc_maindivisionintroductionimages ADD CONSTRAINT FK_4FE5009250DDFB1B FOREIGN KEY (maindivisionintroductionid) REFERENCES lbc_maindivisionintroduction (Id);');
        $this->addSql('CREATE INDEX IDX_4FE5009250DDFB1B ON lbc_maindivisionintroductionimages (maindivisionintroductionid);');
        $this->addSql('ALTER TABLE lbc_specialdivision CHANGE Id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('ALTER TABLE lbc_specialdivision ADD CONSTRAINT FK_BE1EBA28856A684C FOREIGN KEY (parentid) REFERENCES lbc_specialdivision (Id) ON DELETE CASCADE;');
        $this->addSql('CREATE INDEX IDX_BE1EBA28856A684C ON lbc_specialdivision (parentid);');
        $this->addSql('ALTER TABLE lbc_specialdivisionimages CHANGE Id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('ALTER TABLE lbc_specialdivisionsectionimages CHANGE Id Id INT AUTO_INCREMENT NOT NULL;');
        $this->addSql('ALTER TABLE lbc_specialdivisionsections CHANGE Id Id INT AUTO_INCREMENT NOT NULL, CHANGE Code Code VARCHAR(255) NOT NULL;');
        $this->addSql('ALTER TABLE lbc_specialdivisionsections ADD CONSTRAINT FK_132BFF0B856A684C FOREIGN KEY (parentid) REFERENCES lbc_specialdivisionsections (Id) ON DELETE CASCADE;');
        $this->addSql('CREATE INDEX IDX_132BFF0B856A684C ON lbc_specialdivisionsections (parentid);');

        $this->addSql('ALTER TABLE lbc_locationplans_closure ADD CONSTRAINT FK_C6B54A58B4465BB FOREIGN KEY (ancestor) REFERENCES lbc_locationplans (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE lbc_locationplans_closure ADD CONSTRAINT FK_C6B54A589A8FAD16 FOREIGN KEY (descendant) REFERENCES lbc_locationplans (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE lbc_locationplansections_closure ADD CONSTRAINT FK_AFBA4EC0B4465BB FOREIGN KEY (ancestor) REFERENCES lbc_locationplansections (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE lbc_locationplansections_closure ADD CONSTRAINT FK_AFBA4EC09A8FAD16 FOREIGN KEY (descendant) REFERENCES lbc_locationplansections (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE lbc_maindivision_closure ADD CONSTRAINT FK_7D6803B7B4465BB FOREIGN KEY (ancestor) REFERENCES lbc_maindivision (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE lbc_maindivision_closure ADD CONSTRAINT FK_7D6803B79A8FAD16 FOREIGN KEY (descendant) REFERENCES lbc_maindivision (id) ON DELETE CASCADE;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
