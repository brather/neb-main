<?php
/**
 * User: agolodkov
 * Date: 05.09.2016
 * Time: 11:23
 */

namespace LbcBundle\DependencyInjection\Templating;


use Symfony\Bundle\FrameworkBundle\Templating\TimedPhpEngine;

class DebugPhpEngine extends TimedPhpEngine
{
    public function render($name, array $parameters = [])
    {
        $parameters = [
            'data' => $parameters
        ];

        return parent::render($name, $parameters);
    }
}