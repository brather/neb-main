<?

/**
 * ДАННЫЙ ФАЙЛ БОЛЕЕ НЕ ИСПОЛЬЗУЕТСЯ НА ПОРТАЛЕ
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
session_write_close();

$book_id = urldecode(trim($_REQUEST['book_id']));
if (empty($book_id))
    return false;

\Bitrix\Main\Loader::includeModule('nota.exalead');

$isAvailable = false;
$obRslViewer = new \Nota\Exalead\RslViewer();
$res = $obRslViewer->getDocumnetInfo();
if ($res !== false && $res['type'] == 'open' && intval($res['pageCount']) > 0) {
    $isAvailable = true;
}

\Neb\Main\Helper\MainHelper::showJson(array('isAvailable' => $isAvailable));