<?

class sectionElementSearch extends CExt
{
    var $EXT_ID = __CLASS__;
    var $EXT_VERSION;
    var $EXT_VERSION_DATE;
    var $EXT_NAME;
    var $EXT_DESCRIPTION;
    var $EXT_SORT;

    public function __construct()
    {
        parent::__construct();

        $this->EXT_VERSION = '1.0';
        $this->EXT_VERSION_DATE = '06.05.2014';

        $this->EXT_NAME = 'Диалог выбора элемента с навигацией по разделам';
        $this->EXT_DESCRIPTION = 'Подменяем обычный диалог поиска\выбора элемента в свойтве "привязка к элементу с автозаполнением" - диалогом с навигацией по разделам';
        $this->EXT_SORT = 1000;
    }

    function onBeforeInstall()
    {

    }

    function onBeforeUninstall()
    {

    }

    function onAfterInstall()
    {
        RegisterModuleDependences("main", "OnEndBufferContent", 'notaext', 'Bitrix\NotaExt\Ext\sectionElementSearch\IblockSectionElementSearch', "OnEndBufferContent");
    }

    function onAfterUninstall()
    {
        UnRegisterModuleDependences("main", "OnEndBufferContent", 'notaext', 'Bitrix\NotaExt\Ext\sectionElementSearch\IblockSectionElementSearch', "OnEndBufferContent");
    }
}