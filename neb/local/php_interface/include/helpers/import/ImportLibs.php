<?php

/**
 * Импорт библиотек НЭБ из Excel
 */

class ImportLibs
{
    // импорт библиотек из excel-файла
    public static function import($sFileAddress, $bClearLog = false) {

        self::importLibsModules();
        self::checkLogTable();

        $arImportData = self::getDataFromExcel($sFileAddress);

        self::setNetLibs($arImportData, $bClearLog);
        self::setNetLibsCity();
    }

    // сравнение библиотек - со временем исключить
    public static function compareLibs($sFileAddress, $bClearLog = false) {

        self::importLibsModules();
        self::checkLogTable();

        $arImportData = self::getDataFromExcel($sFileAddress);

        //debugPre($arImportData);

        // ================================================================================================

        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        // выборка всех библиотек без координат
        $libraryData = $libraryEntity::getList(array(
            "filter" => array(),
            "order"  => array("ID"),
            "select" => array("ID", "UF_NAME", 'UF_ADRESS'),
        ));
        while ($arItem = $libraryData->Fetch())
            $arNetLibs[$arItem['ID']] = $arItem;
        unset($libraryHLBlock, $libraryEntity, $libraryData, $arItem);

        // ================================================================================================

        $obIBlockElement = new \CIBlockElement();
        $rsElements = $obIBlockElement->GetList(
            array(),
            array(
                "IBLOCK_ID"              => IBLOCK_ID_LIBRARY,
                "!PROPERTY_LIBRARY_LINK" => false,
                //"ACTIVE"               => "Y",
            ),
            false,
            false,
            array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_LIBRARY_LINK')
        );
        while ($arItem = $rsElements->Fetch()) {
            if (!empty($arNetLibs[$arItem['PROPERTY_LIBRARY_LINK_VALUE']])) {
                $arItem['LIB'] = $arNetLibs[$arItem['PROPERTY_LIBRARY_LINK_VALUE']];
                $arItem['LIB']['SEARCH_NAME']    = self::prepareStr4Search($arItem['LIB']['UF_NAME']);
                $arItem['LIB']['SEARCH_ADDRESS'] = self::prepareStr4Search($arItem['LIB']['UF_ADRESS']);
            }
            $arItem['SEARCH_NAME'] = self::prepareStr4Search($arItem['NAME']);
            $arLibsInNeb[$arItem['ID']] = $arItem;
        }
        unset($obIBlockElement, $rsElements, $arItem);

        // ================================================================================================

        //debugPre($arImportData);
        //debugPre($arNetLibs);
        //debugPre($arLibsInNeb);

        foreach ($arImportData as $arItem) {

            $sName    = self::prepareStr4Search($arItem['long_name']);
            $sAddress = self::prepareStr4Search($arItem['address']);

            foreach ($arLibsInNeb as $arItem2) {
                if ($arItem2['SEARCH_NAME'] == $sName
                    || $arItem2['LIB']['SEARCH_NAME'] == $sName
                    || $arItem2['LIB']['SEARCH_ADDRESS'] == $sAddress)
                {
                    $arFind[$arItem['id']] = $arItem;
                    break;
                }
            }
        }
        debugPre($arFind);
    }

    // удаление координат у всех библиотек
    public static function deleteLibsGeocode() {

        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        // выборка всех библиотек без координат
        $libraryData = $libraryEntity::getList(array(
            "filter" => array('!UF_POS' => false),
            "order"  => array("ID"),
            "select" => array("ID"),
        ));
        while ($arItem = $libraryData->Fetch())
            $libraryEntity::update($arItem['ID'], array('UF_POS' => null));
        unset($libraryHLBlock, $libraryEntity, $libraryData, $arItem);
    }

    // удаление всех библиотек с пустыми координатами
    public static function deleteLibsEmptyPos() {

        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        // выборка всех библиотек без координат
        $libraryData = $libraryEntity::getList(array(
            "filter" => array('UF_POS' => ''),
            "order"  => array("ID"),
            "select" => array("ID"),
        ));
        while ($arItem = $libraryData->Fetch())
            $libraryEntity::delete($arItem['ID']);
        unset($libraryHLBlock, $libraryEntity, $libraryData, $arItem);
    }

    // подготовка результата Excel-файла
    public static function getDataFromExcel($filename)
    {
        $arExcel = self::readExcel($filename);

        $arResult = array();

        $arMapName = array(
            'ID'          => 'id',
            'Nazvanie_1'  => 'long_name',
            'Nazvanie_2'  => 'short_name',
            'Adress'      => 'address',
            'Region'      => 'region',
            'Rayon'       => 'district',
            'Nas_punkt'   => 'locality',
            'Phone'       => 'phone',
            'Головная_ID' => 'head_id',
            'Договор_N'   => 'contract'
        );

        foreach ($arExcel as $k => $arItem) {
            if (!$k) {
                foreach ($arItem as $key => $val)
                    if ($arMapName[$val])
                        $arNewMapName[$key] = $arMapName[$val];
            } else {
                $arRes = array();
                foreach ($arItem as $key => $val)
                    if (!empty($arNewMapName[$key]))
                        $arRes[$arNewMapName[$key]] = self::trimSpaces($val);
                    else
                        $arRes[$key] = self::trimSpaces($val);

                $arResult[$k - 1] = $arRes;
            }
        }

        return $arResult;
    }

    /**
     * Добавление библиотек в h-инфоблок из файла.
     *
     * @param $arImportData
     * @param bool $bClearLog
     */
    private static function setNetLibs($arImportData, $bClearLog = false) {

        $arLibAdd = $arLibUpdate = $arLibDelete = array();

        $arLibsInNeb = self::getLibsInNeb();
        $arNetLibs   = self::getNebLibs();

        foreach ($arNetLibs as $arItem) {
            if ($arItem['UF_RGB_ID'])
                $arNetLibsRGB[$arItem['UF_RGB_ID']] = $arItem['ID'];
            else
                $arNetLibsOther[] = $arItem['ID'];
        }
        ksort($arNetLibsRGB);

        foreach ($arImportData as $arItem) {
            // найдена - обновить
            if ($arNetLibsRGB[$arItem['id']])
                $arLibUpdate[$arNetLibsRGB[$arItem['id']]] = $arItem;
            // не найдена - добавить
            else
                $arLibAdd[] = $arItem;
        }
        // поиск библиотек на удаление
        foreach ($arNetLibsOther as $iItem)
            if (!$arLibsInNeb[$iItem])
                $arLibDelete[] = $iItem;

        // очистка таблицы логов
        if ($bClearLog)
            self::clearLogTable();

        // обновление библиотек
        self::addNetLibs($arLibAdd);       // добавление отсутствующих
        self::updateNetLibs($arLibUpdate); // обновление имеющихся
        self::deleteNetLibs($arLibDelete); // удаление только инфоблочных
        self::addNetLibsId();              // идиотизмъ, да
    }

    /**
     * Обновление городов в отдельном h-блоке из таблицы библиотек
     */
    private static function setNetLibsCity() {

        $arLocation = $arLibs = $arSearchCity = [];

        // Шаг 1: Обновление списка городов в отдельном инфоблоке
        $arNetLibs = self::getNebLibs();
        foreach ($arNetLibs as $arLib) {
            if (!empty($arLib['UF_TOWN']))
                $arLocation[$arLib['UF_TOWN']] = $arLib['UF_TOWN'];
            if ($arLib['UF_CITY'] == 0 && !empty($arLib['UF_TOWN']))
                $arLibs[$arLib['ID']] = $arLib['UF_TOWN'];
        }
        ImportCities::updateLocality($arLocation);

        // Шаг 2: Обновление идентификаторов годоов у самих Библиотек

        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        $arNetLibsCity = ImportCities::getNebLibsCity();
        foreach ($arNetLibsCity as $iCity => $arCity)
            $arSearchCity[$arCity['SEARCH']] = $arCity['ID'];

        foreach ($arLibs as $iLib => $sCity) {
            $sCity = ImportCities::prepareLocality4Search($sCity);
            if ($arSearchCity[$sCity])
                $libraryEntity::update($iLib, array('UF_CITY' => $arSearchCity[$sCity]));
        }
    }

    /**
     * Обновление имеющихся Библиотек
     */
    private static function updateNetLibs($arLibUpdate) {

        if (empty($arLibUpdate))
            return null;

        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        $arNetLibs       = self::getNebLibs();

        foreach ($arLibUpdate as $iLib => $arLib) {

            $arUpdate = array();

            if ($arNetLibs[$iLib]['UF_ID'] != $arNetLibs[$iLib]['ID']) // да, это срань-наследие от предыдущих разрабов
                $arUpdate['UF_ID'] = $arNetLibs[$iLib]['ID'];

            if ($arNetLibs[$iLib]['UF_NAME'] != $arLib['short_name'])
                $arUpdate['UF_NAME'] = $arLib['short_name'];

            if ($arNetLibs[$iLib]['UF_FULL_NAME'] != $arLib['long_name'])
                $arUpdate['UF_FULL_NAME'] = $arLib['long_name'];

            if ($arNetLibs[$iLib]['UF_ADRESS'] != $arLib['address']) {
                $arUpdate['UF_ADRESS'] = $arLib['address'];
                $arUpdate['UF_POS']    = '';
            }

            if ($arNetLibs[$iLib]['UF_REGION'] != $arLib['region'])
                $arUpdate['UF_REGION'] = $arLib['region'];

            if ($arNetLibs[$iLib]['UF_AREA'] != $arLib['region'])
                $arUpdate['UF_AREA'] = $arLib['region'];

            if ($arNetLibs[$iLib]['UF_DISTRICT'] != $arLib['district'])
                $arUpdate['UF_DISTRICT'] = $arLib['district'];

            if ($arNetLibs[$iLib]['UF_TOWN'] != $arLib['locality'])
                $arUpdate['UF_TOWN'] = $arLib['locality'];

            if ($arNetLibs[$iLib]['UF_AREA2'] != $arLib['locality'])
                $arUpdate['UF_AREA2'] = $arLib['locality'];

            if ($arNetLibs[$iLib]['UF_LOCALITY'] != $arLib['locality'])
                $arUpdate['UF_LOCALITY'] = $arLib['locality'];

            if ($arNetLibs[$iLib]['UF_PHONE'] != $arLib['phone'])
                $arUpdate['UF_PHONE'] = $arLib['phone'];

            if ($arNetLibs[$iLib]['UF_CONTRACT'] != $arLib['contract'])
                $arUpdate['UF_CONTRACT'] = $arLib['contract'];

            if (!empty($arUpdate)) {

                $obResult = $libraryEntity::update($iLib, $arUpdate);
                if ($obResult->isSuccess()) {
                    $sStatus = 'SUCCESS';
                    $sInfo   = 'Обновлена: ' . $iLib;
                } else {
                    $sStatus = 'ERROR';
                    $sInfo   = implode(', ', $obResult->getErrorMessages());
                }
            }
            else {
                $sStatus = 'SUCCESS';
                $sInfo   = 'Обновление не требутеся: ' . $iLib;
            }

            self::writeLog($arLib['id'], $arLib['long_name'], $arLib['address'], $sStatus, $sInfo);
        }
        unset($libraryHLBlock, $libraryEntity);
    }

    /**
     * Метод удаляет библиотеки по списку идентификаторов
     */
    private function deleteNetLibs($arNetLibs) {

        if (empty($arNetLibs))
            return null;

        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        if (is_integer($arNetLibs)) {

            $libraryEntity::delete($arNetLibs);

        } elseif (is_array($arNetLibs)) {

            foreach ($arNetLibs as $iNetLib)
                $libraryEntity::delete($iNetLib);
        }
    }

    // добавление библиотек
    private static function addNetLibs($arAddNetLibs) {

        if (empty($arAddNetLibs))
            return false;

        // H-блок NetLibs
        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        foreach ($arAddNetLibs as $arLib) {
            $arFields = array(
                'UF_ID'                => 0,
                'UF_NAME'              => $arLib['short_name'],
                'UF_FULL_NAME'         => $arLib['long_name'],
                'UF_ADRESS'            => $arLib['address'],
                'UF_ADMINISTRATIVEARE' => null,
                'UF_REGION'            => $arLib['region'],
                'UF_AREA'              => $arLib['region'],
                'UF_DISTRICT'          => $arLib['district'],
                'UF_TOWN'              => $arLib['locality'],
                'UF_CITY'              => 0,
                'UF_AREA2'             => $arLib['locality'],
                'UF_LOCALITY'          => $arLib['locality'],
                'UF_PHONE'             => $arLib['phone'],
                'UF_POS'               => null,
                'UF_CONTRACT'          => $arLib['contract'],
                'UF_RGB_ID'            => $arLib['id'],
                'UF_DATE_JOIN'         => date("d.m.Y H:i:s")
            );
            $obResult = $libraryEntity::add($arFields);
            if ($obResult->isSuccess()) {
                $sStatus = 'SUCCESS';
                $sInfo   = 'Добавлена библиотека: '.$obResult->getId();
            }
            else {
                $sStatus = 'ERROR';
                $sInfo   = implode(', ', $obResult->getErrorMessages());
            }

            self::writeLog($arLib['id'], $arLib['long_name'], $arLib['address'], $sStatus, $sInfo);
        }
        unset($libraryHLBlock, $libraryEntity);
    }


    // добавление идентификаторов библиотек (идиотизмЪ, да)
    private static function addNetLibsId() {

        $arNetLibs = self::getNebLibs(true);

        if (empty($arNetLibs))
            return false;

        // H-блок NetLibs
        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        foreach ($arNetLibs as $arNetLib)
            $libraryEntity::update($arNetLib['ID'], array('UF_ID' => $arNetLib['ID']));

        unset($libraryHLBlock, $libraryEntity);
    }

    // Получает список библиотек-участниц НЭБ
    public static function getNebLibs($bWithOutId = false) {

        $arResult = $arFilter = array();

        if ($bWithOutId)
            $arFilter['UF_ID'] = '';

        $libraryHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();
        $libraryData = $libraryEntity::getList(array(
            "filter" => $arFilter,
            "order"  => array("ID"),
            "select" => array("ID", 'UF_ID', "UF_NAME", 'UF_FULL_NAME', 'UF_TOWN', "UF_AREA", "UF_AREA2", "UF_LOCALITY",
                "UF_REGION", "UF_CITY", "UF_ADRESS", "UF_PHONE", "UF_CONTRACT", "UF_RGB_ID"),
        ));
        while ($arItem = $libraryData->Fetch())
            $arResult[$arItem['ID']] = $arItem;
        unset($libraryHLBlock, $libraryEntity, $libraryData, $arItem);

        return $arResult;
    }
    
    // Получает список  идетификаторов привязанных библиотек-участниц НЭБ
    public static function getLibsInNeb() {

        $arResult = array();

        $obIBlockElement = new \CIBlockElement();

        $rsElements = $obIBlockElement->GetList(
            array(),
            array(
                "IBLOCK_ID"              => IBLOCK_ID_LIBRARY,
                //"ACTIVE"                 => "Y",
                "!PROPERTY_LIBRARY_LINK" => false
            ),
            false,
            false,
            array('ID', 'IBLOCK_ID', 'PROPERTY_LIBRARY_LINK')
        );
        while ($arItem = $rsElements->Fetch())
            $arResult[$arItem['PROPERTY_LIBRARY_LINK_VALUE']] = $arItem['PROPERTY_LIBRARY_LINK_VALUE'];

        unset($obIBlockElement, $rsElements, $arItem);

        return $arResult;
    }

    /**
     * Запись в лог
     *
     * @param $sRgbId
     * @param $sName
     * @param $sAddress
     * @param $sStatus
     * @param $sInfo
     */
    private static function writeLog($sRgbId, $sName, $sAddress, $sStatus, $sInfo) {

        $arLog = array(
            'ID'      => 0,
            'DATE'    => 'now()',
            'RGB_ID'  => $sRgbId,
            'NAME'    => $sName,
            'ADDRESS' => $sAddress,
            'STATUS'  => $sStatus,
            'INFO'    => $sInfo,
        );
        $sQuery = self::buildInsertQuery('neb_log_import_libs', $arLog);
        \Bitrix\Main\Application::getConnection()->query($sQuery);
    }

    // ================================================= НАЧАЛО ======================================================

    // подключаем модули
    private static function importLibsModules()
    {
        require_once($_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/PHPExcel.php");
        require_once $_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/include/helpers/import/ImportCities.php';

        \Bitrix\Main\Loader::IncludeModule("iblock");
        \Bitrix\Main\Loader::IncludeModule("highloadblock");
    }

    // проверяем наличие таблицы логов
    private static function checkLogTable() {

        //\Bitrix\Main\Application::getConnection()->query('DROP TABLE IF EXISTS neb_log_import_libs;');
        $sQuery = '
            CREATE TABLE IF NOT EXISTS neb_log_import_libs (
              ID int NOT NULL AUTO_INCREMENT,
              DATE timestamp NOT NULL,
              RGB_ID int NOT NULL,
              NAME varchar(5000) NOT NULL,
              ADDRESS varchar(5000) NOT NULL,
              STATUS varchar(255) DEFAULT NULL,
              INFO  varchar(5000) DEFAULT NULL,
              PRIMARY KEY (ID)
            )
            ENGINE = INNODB;
        ';
        \Bitrix\Main\Application::getConnection()->query($sQuery);
    }

    // очистка старых логов
    private static function clearLogTable($iMaxTime = 30) {
        $sQuery = 'DELETE FROM neb_log_import_libs';
        if ($iMaxTime)
            $sQuery .= ' WHERE DATE <=' . (strtotime('now') - $iMaxTime * 60);
        \Bitrix\Main\Application::getConnection()->query($sQuery);
    }

    // ================================================= EXCEL ======================================================

    // чтение Excel-файла
    private static function readExcel($filename)
    {
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $filename))
            die('File does not exist '. $_SERVER['DOCUMENT_ROOT'] . $filename);

        // получаем тип файла (xls, xlsx), чтобы правильно его обработать
        $file_type = PHPExcel_IOFactory::identify( $_SERVER['DOCUMENT_ROOT'] . $filename );

        // создаем объект для чтения
        $objReader = PHPExcel_IOFactory::createReader( $file_type );

        // загружаем данные файла в объект
        $objPHPExcel = $objReader->load( $_SERVER['DOCUMENT_ROOT'] . $filename );

        // выгружаем данные из объекта в массив
        $arResult = $objPHPExcel->getActiveSheet()->toArray();

        return $arResult;
    }

    // запись Excel-файла
    private static function writeExcel($arResult, $filename = '/saveLibs.xlsx')
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $filename))
            unlink($_SERVER['DOCUMENT_ROOT'] . $filename);

        if (empty($filename))
            die('Empty file name!');

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getActiveSheet()->fromArray($arResult, NULL, 'A1'); // A1 - ячейка с которой начинаем вставку

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save($_SERVER['DOCUMENT_ROOT'] . $filename);

        unset($objWriter);
    }

    // ========================================== СЛУЖЕБНЫЕ МЕТОДЫ ===================================================

    // собирает insert-запрос
    private static function buildInsertQuery($sTable, $arFields) {

        $sQuery = "INSERT INTO `".$sTable."` (";
        foreach ($arFields as $k => $v)
            if ($k == 'ID')
                $sQuery .= $k;
            else
                $sQuery .= ', '.$k;
        $sQuery .= ") VALUES (";
        foreach ($arFields as $k => $v)
            if ($k == 'ID')
                $sQuery .= "'0'";
            elseif ($k == 'DATE')
                $sQuery .= ", ".$v;
            else
                $sQuery .= ", '".$v."'";
        $sQuery .= ")";

        return $sQuery;
    }

    // подготовка названия организации к поиску: приведение к нижнему регистру и обрезка по краям
    private static function prepareStr4Search($str) {
        return self::trimSpaces(mb_strtolower(self::trimQuots($str)));
    }
    
    // вырезает любые кавычки из строки
    private static function trimQuots($str) {
        return str_replace(array('"', "'", '«', '»', '<', '>'), '', $str);
    }

    // удаление переносов строк, удаление лишних пробелов, обрезка по краям
    private static function trimSpaces($str) {
        return trim(preg_replace('/\s{2,}/', ' ', str_replace("\n", '', $str)));
    }
}