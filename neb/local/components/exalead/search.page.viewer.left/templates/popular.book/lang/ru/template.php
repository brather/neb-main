<?
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_AUTH'] = 'Требуется полная регистрация';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_PLAN'] = 'Добавить в План оцифровки';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_PLAN'] = 'Удалить из Плана оцифровки';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_LIB'] = '<span>Удалить</span> из Моей библиотеки';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_LIB'] = '<span>Добавить</span> в Мою библиотеку';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_AUTHOR'] = 'Автор';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_YEAR'] = 'Год публикации';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_PAGES'] = 'Количество страниц';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_PUBLISH'] = 'Издательство';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SOURCE'] = 'Источник';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_BBK'] = 'ББК';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_UDK'] = 'УДК';

$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_AUTHOR'] = 'По автору';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_NAME'] = 'По названию';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_DATE'] = 'По дате';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_SHOW'] = 'Показать';

$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT'] = 'Сортировать';

$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_READ'] = 'читать';
$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SHOW_ALL'] = 'Показать все результаты поиска на странице';
