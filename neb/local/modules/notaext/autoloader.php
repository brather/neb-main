<?

/**
 * Автозагрузчик, который подключает классы из модуля, если модуль не подключен
 */
spl_autoload_register(function ($className) {

    $className = strtolower($className);
    $file = ltrim($className, "\\");
    $file = str_replace('\\', '/', $file);
    $arFile = explode("/", $file);

    if ($arFile[0] === "bitrix") {
        array_shift($arFile);

        if (empty($arFile))
            return;

        $module = array_shift($arFile);
        if ($module == null || empty($arFile))
            return;
    }

    $filePathLocal  = $_SERVER['DOCUMENT_ROOT'] . "/local/modules/"  . $module . "/lib/" . implode("/", $arFile) . ".php";
    $filePathBitrix = $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/" . $module . "/lib/" . implode("/", $arFile) . ".php";

    if (file_exists($filePathLocal))
        require_once($filePathLocal);
    elseif (file_exists($filePathBitrix))
        require_once($filePathBitrix);
});