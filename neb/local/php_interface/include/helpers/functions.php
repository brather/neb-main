<?
use Bitrix\NotaExt\Utils as Ut;

if (class_exists('Bitrix\NotaExt\Utils')) {
    function pre($ar, $show = false)
    {
        Ut::pre($ar, $show);
    }

    function GetEnding($number, $zero = 'ий', $n2_1 = 'ие', $n2_4 = 'ия')
    {
        return Ut::GetEnding($number, $zero, $n2_1, $n2_4);
    }

    function trimming_line($string, $length = 70, $strEnd = '…')
    {
        return Ut::trimming_line($string, $length, $strEnd);
    }
}

function SortingExalead($By, $sByVar = "by", $sOrderVar = "order", $Anchor = "nav_start", $arRemoveParams = array())
{
    global $APPLICATION, $$sByVar, $$sOrderVar;

    $by = $$sByVar;
    $order = $$sOrderVar;
    $orderVar = 'asc';
    $class = '';
    if (strtoupper($By) == strtoupper($by)) {
        if (strtoupper($order) == "DESC") {
            $class = "sort up";
        } else {
            $class = "sort down";
            $orderVar = 'desc';
        }
    }

    $url = $APPLICATION->GetCurPageParam(
            $sByVar . '=' . $By . '&' . $sOrderVar . '=' . $orderVar,
            array_merge($arRemoveParams, array($sByVar, $sOrderVar, "dop_filter"))
        ) . '#' . $Anchor;

    return 'href="' . $url . '" class="' . $class . '"';
}


function getNumberNeb($num)
{
    if (empty($num))
        return false;

    $num = $num . '';
    $len = strlen($num) - 1;
    $result = '';
    for ($i = $len; $i >= 0; $i--) {
        $result .= '<span class="b-portalinfo_number' . ($i % 3 == 0 ? ' mr ' : '') . ' iblock">' . $num[$len - $i] . '</span>' . PHP_EOL;
    }
    return $result;
}


function image_to_base64($path_to_image)
{
    $type = pathinfo($path_to_image, PATHINFO_EXTENSION);
    $image = file_get_contents($path_to_image);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($image);
    return $base64;
}

class nebSort
{

    /*     производим сортировку массива по полю  */
    private static $DateSortField = '';

    private function SetdatesortField($field)
    {
        self::$DateSortField = $field;
    }

    // Сортировать массив по полю типа - дата
    static function SortFieldTypeDate($arr, $field)
    {
        self::SetdatesortField($field);
        uasort($arr, array(self, 'SortFieldTypeD'));
        return $arr;
    }

    private function SortFieldTypeD($f1, $f2)
    {
        if (strtotime($f1[self::$DateSortField]) < strtotime($f2[self::$DateSortField])) return 1;
        elseif (strtotime($f1[self::$DateSortField]) > strtotime($f2[self::$DateSortField])) return -1;
        else return 0;
    }

    // Сортировать массив по полю типа - число
    static function SortFieldTypeNum($arr, $field, $sort = 'desc')
    {
        self::SetdatesortField($field);
        if ($sort == 'desc')
            uasort($arr, array(self, 'SortFieldTypeNumberDesc'));
        else
            uasort($arr, array(self, 'SortFieldTypeNumber'));
        return $arr;

    }

    private function SortFieldTypeNumber($f1, $f2)
    {
        if ($f1[self::$DateSortField] > $f2[self::$DateSortField]) return 1;
        elseif ($f1[self::$DateSortField] < $f2[self::$DateSortField]) return -1;
        else return 0;
    }

    private function SortFieldTypeNumberDesc($f1, $f2)
    {
        if ($f1[self::$DateSortField] < $f2[self::$DateSortField]) return 1;
        elseif ($f1[self::$DateSortField] > $f2[self::$DateSortField]) return -1;
        else return 0;
    }
}

function plural($n, $forms)
{
    if ('ru' !== LANGUAGE_ID) {
        ngettext($forms[0], $forms[1], $n);
    }

    return $n % 10 == 1 && $n % 100 != 11
        ? $forms[0]
        : ($n % 10 >= 2
        && $n % 10 <= 4
        && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
}

function MbTruncateText($strText, $intLen)
{
    if (strlen($strText) > $intLen)
        return rtrim(mb_substr($strText, 0, $intLen), ".") . "...";
    else
        return $strText;
}

function nfDeleteParam($ParamNames)
{

    if (count($_GET) < 1)
        return "";

    $aParams = $_GET;

    foreach (array_keys($aParams) as $key) {

        foreach ($ParamNames as $param) {

            if (is_array($aParams[$key])) {

                foreach (array_keys($aParams[$key]) as $field) {
                    if (strcasecmp($param, $field) == 0) {
                        unset($aParams[$key][$field]);
                        break;
                    }
                }
            } else {
                if (strcasecmp($param, $key) == 0) {
                    unset($aParams[$key]);
                    break;
                }
            }
        }
    }
    return http_build_query($aParams, "", "&");
}

function debugPre($ar, $bVarDamp = false, $bHide = false) {

    if ($bHide)
        echo '<!-- Debug: ';
    else
        echo '<pre>';

    if ($bVarDamp)
        var_dump($ar);
    else
        print_r($ar);

    if ($bHide)
        echo '-->';
    else
        echo '</pre>';
}

function debugLog($ar, $sFile = '/debug.log') {
    $sFilePath = $_SERVER["DOCUMENT_ROOT"] . $sFile ;
    $current = file_get_contents($sFilePath);
    $current .= date("d.m.Y H:m:s")."\n".print_r($ar, true)."\n";
    file_put_contents($sFilePath, $current);
}

if (!function_exists('json_last_error_msg')) {
    function json_last_error_msg()
    {
        static $ERRORS
        = [
            JSON_ERROR_NONE           => 'No error',
            JSON_ERROR_DEPTH          => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH => 'State mismatch (invalid or malformed JSON)',
            JSON_ERROR_CTRL_CHAR      => 'Control character error, possibly incorrectly encoded',
            JSON_ERROR_SYNTAX         => 'Syntax error',
            JSON_ERROR_UTF8           => 'Malformed UTF-8 characters, possibly incorrectly encoded'
        ];
        $error = json_last_error();

        return isset($ERRORS[$error]) ? $ERRORS[$error] : 'Unknown error';
    }
}

function p($d) {
    global $USER;
//    if($USER->IsAdmin()){
        ?><pre style="color: green; border: 1px solid green; background: white;"><? print_r ($d) ?></pre><?
//    }
}

/** Функция подмены киррилических символов на схожие по написанию латинские
 * @param $string - строка для подмены
 * @return mixed - строка с только лишь латинскими буквами
 */
function transliteRuToEnString($string) {

    $arCode = [
        'a' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z',
        'и' => 'i', 'й' => 'i', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh',
        'ъ' => '',  'ы' => 'e', 'ь' => '',  'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E', 'Ж' => 'J', 'З' => 'Z',
        'И' => 'I', 'Й' => 'I', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SH',
        'Ъ' => '',  'Ы' => 'E', 'Ь' => '',  'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA'
    ];

    $resStr = str_replace(array_keys($arCode), array_values($arCode), $string);

    return $resStr;
}

/** Функция для очистки строки от символов для использования строки в названии классов
 * @param $string
 * @return mixed
 */
function checkStringForSetAttr($string) {
    $resStr = transliteRuToEnString($string);
    $resStr = str_replace(' ', '_', $resStr);
    $resStr = preg_replace('/\.+/i', '', $resStr);
    return $resStr;
}
