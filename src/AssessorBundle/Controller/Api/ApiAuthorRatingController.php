<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 13:23
 */

namespace AssessorBundle\Controller\Api;

use AssessorBundle\Form\TblAuthorRatingType;
use AssessorBundle\Traits\AssessorRatingTrait;
use Doctrine\ORM\QueryBuilder;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ApiQueryRatingController
 *
 * @DI\Service("assessor.api.author_rating", scope="request")
 */
class ApiAuthorRatingController implements RatingApiInterface
{
    use AssessorRatingTrait;

    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'AssessorBundle:TblAuthorRating';
    }

    /**
     * @return TblAuthorRatingType
     */
    public function getFormType()
    {
        return new TblAuthorRatingType();
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return void
     */
    public function handleAutoComplete($qb)
    {
        $query = $this->request->query->get('query');
        $qb->where('t.author LIKE :query');
        $qb->setParameter('query', $query . '%');
    }
}