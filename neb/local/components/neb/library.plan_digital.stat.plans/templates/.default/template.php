<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>

<? foreach($arResult['ERROR'] as $sError): ?>
    <?ShowError($sError);?>
<? endforeach; ?>

<form action="?" method="get" class="form-inline nrf p" data-feedback-filter-form>
    <h3>Выберите период</h3>
    <input type="hidden" name="DIGITIZATION[ACTION]" value="STAT" />

    С
    <!--<?=Loc::getMessage('DIGITIZATION_STAT_DATE_FROM')?>-->
    <div class="form-group">
        <div class="input-group">
            <input
                size="10"
                type="text"
                value="<?=$_REQUEST['DIGITIZATION']['DATE_FROM']?>"
                name="DIGITIZATION[DATE_FROM]"
                id="article_pub_date1"
                class="form-control"
            />
            <span class="input-group-btn">
                <a class="btn btn-default" onclick="BX.calendar({node: article_pub_date1, field: article_pub_date1, form: history, bTime: false, value: ''});">
                    <span class="glyphicon glyphicon-calendar"></span>
                </a>
            </span>
        </div>
    </div>
    по
    <!-- <?=Loc::getMessage('DIGITIZATION_STAT_DATE_TO')?> -->
    <div class="form-group">
        <div class="input-group">
            <input
                size="10"
                type="text"
                value="<?=$_REQUEST['DIGITIZATION']['DATE_TO']?>"
                name="DIGITIZATION[DATE_TO]"
                id="article_pub_date2"
                class="form-control"
            />
            <span class="input-group-btn">
                <a class="btn btn-default" onclick="BX.calendar({node: article_pub_date2, field: article_pub_date2, form: history, bTime: false, value: ''});">
                    <span class="glyphicon glyphicon-calendar"></span>
                </a>
            </span>
        </div>
    </div>
    <div class="form-group">
        <!-- input type="submit" value="<?=Loc::getMessage('DIGITIZATION_STAT_SHOW')?>" / -->
        <button class="btn btn-primary"><?=Loc::getMessage('DIGITIZATION_STAT_SHOW')?></button>
    </div>
</form>

<? if (empty($arResult['ITEMS'])): ?>

    <p>За данный период отсутствуют статистические данные.</p>

<? else: ?>
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th><?=Loc::getMessage('DIGITIZATION_STAT_LIBRARY')?></th>
                <th><?=Loc::getMessage('DIGITIZATION_STAT_COUNT')?></th>
                <th><?=Loc::getMessage('DIGITIZATION_STAT_DIGIT')?></th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                <tr>
                    <td><?=$arItem['NAME']?></td>
                    <td><?=$arItem['COUNT']?></td>
                    <td><?=$arItem['DIGIT']?></td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>
    <div>
        <a target="_blank" href="#" class="btn btn-default" data-get-stat-report data-onclick="get_stat_report(); return false;">
            <?=Loc::getMessage('DIGITIZATION_STAT_EXCEL')?>
        </a>
    </div>

    <script>
        $(function(){
            $(document).on('click','[data-get-stat-report]',function(e){
                e.preventDefault();
                get_stat_report();
            });
            function get_stat_report() {
                date_from = $('#article_pub_date1').val();
                date_to = $('#article_pub_date2').val();
                window.location.href = '?DIGITIZATION[ACTION]=STAT&DIGITIZATION[EXPORT]=Y'
                    + '&DIGITIZATION[DATE_FROM]=' + date_from + '&DIGITIZATION[DATE_TO]=' + date_to;
                return false;
            }
        });
    </script>
<? endif; ?>