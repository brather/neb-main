<?
namespace Bitrix\NotaExt\Iblock;

use Bitrix\NotaExt\Singleton;
use Bitrix\NotaExt\NPHPCacheExt;

/*
Класс расширение, для работы с разделами инфоблока
#use Bitrix\NotaExt\Iblock\Section;
#$arResult = Section::getList(array('IBLOCK_ID' => 2));
#$arResult = Section::getByID(1);
*/

class Section extends Singleton
{
    function __construct()
    {
        \Bitrix\Main\Loader::IncludeModule("iblock");
    }

    /*
    * @method getList - метод для выборки разделов инфоблока
    * @param array $arFilter
    * @param array $arSelect
    * @param array $arSort
    * @param array $arNavStartParams
    */
    public static function getList($arFilter, $arSelect = array(), $arSort = array(), $bIncCnt = false, $arNavStartParams = false)
    {
        return self::getInstance()->GetListPr($arFilter, $arSelect, $arSort, $bIncCnt, $arNavStartParams);
    }

    private function GetListPr($arFilter, $arSelect = array(), $arSort = array(), $bIncCnt = false, $arNavStartParams = false)
    {
        $arResult = array();

        $obCacheExt = new NPHPCacheExt();
        $arCacheParams = func_get_args();
        $arCacheParams['iblock_id'] = $arFilter['IBLOCK_ID'];  # если в параметрах указывается ID инфоблока
        $arCacheParams['SCRIPT_NAME'] = $_SERVER['SCRIPT_NAME'];  # Уникальный кешь для разных страниц где вызывается функция

        if (!$obCacheExt->InitCache(__CLASS__ . __function__, $arCacheParams)) {
            $obj = self::GetListRawPr($arFilter, $arSelect, $arSort, $bIncCnt, $arNavStartParams);

            if (!empty($arNavStartParams['nPageSize']))
                $arResult["NAV_STRING"] = $obj->GetPageNavStringEx($navComponentObject, $arNavStartParams['navigationTitle'], $arNavStartParams['templateName'], ($arNavStartParams['showAlways'] == 'Y' ? 'Y' : ''));

            if ($obj === false)
                return false;

            while ($ar_result = $obj->GetNext()) {
                if (!empty($arNavStartParams['nPageSize']))
                    $arResult['ITEMS'][] = $ar_result;
                else
                    $arResult[] = $ar_result;
            }

            $obCacheExt->StartDataCache($arResult);
        } else {
            $arResult = $obCacheExt->GetVars();
        }

        return $arResult;
    }

    /*
    * @method getListRaw метод для получения объекта выборки элементов инфоблока
    * @param array $arFilter
    * @param array $arSelect
    * @param array $arSort
    * @param array $arNavStartParams
    */
    public static function getListRaw($arFilter, $arSelect = array(), $arSort = array(), $bIncCnt = false, $arNavStartParams = false)
    {
        return self::getInstance()->GetListRawPr($arFilter, $arSelect, $arSort, $bIncCnt, $arNavStartParams);
    }

    private function GetListRawPr($arFilter, $arSelect = array(), $arSort = array(), $bIncCnt = false, $arNavStartParams = false)
    {

        /*if(empty($arFilter['IBLOCK_ID']))
        {
        $GLOBALS['APPLICATION']->ThrowException('IBLOCK_ID Empty.');
        return false;
        }*/

        $arSelect[] = 'ID';
        $arSelect[] = 'NAME';

        if (empty($arFilter['GLOBAL_ACTIVE']))
            $arFilter['GLOBAL_ACTIVE'] = 'Y';

        if (empty($arSort))
            $arSort = array('SORT' => 'DESC', 'NAME' => 'ASC');

        return \CIBlockSection::GetList($arSort, $arFilter, $bIncCnt, $arSelect, $arNavStartParams);
    }

    /*
    * @method getByID метод для получения полей раздела по его ID
    * @param integer $ID
    * @param array $arSelect
    * @param integer|null $IBLOCK_ID
    */
    public static function getByID($ID, $arSelect = array(), $IBLOCK_ID = null)
    {
        return self::getInstance()->GetByIDPr($ID, $arSelect, $IBLOCK_ID);
    }

    private function GetByIDPr($ID, $arSelect = array(), $IBLOCK_ID = null)
    {
        if (empty($ID)) {
            $GLOBALS['APPLICATION']->ThrowException('ID section empty.');
            return false;
        }

        if ($IBLOCK_ID == null) {
            $arResult = self::GetListPr(array('ID' => $ID), array('ID', 'IBLOCK_ID'));
            if (!empty($arResult))
                $IBLOCK_ID = $arResult[0]['IBLOCK_ID'];
        }

        if (empty($IBLOCK_ID)) {
            $GLOBALS['APPLICATION']->ThrowException('IBLOCK_ID Empty.');
            return false;
        }

        $arResult = self::GetListPr(array('IBLOCK_ID' => $IBLOCK_ID, 'ID' => $ID), $arSelect);
        if (!empty($arResult))
            return $arResult['0'];
        else
            return false;
    }

}
