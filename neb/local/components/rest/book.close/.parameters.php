<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'       => '/',
                'VARIABLES'     => array(
                    'token',
                    'book_id',
                    'close',
                    'comment',
                ),
                'TYPE'          => array('PUT', 'GET'),
                'DOCUMENTATION' => array(
                    'GROUP'       => 'CONTROL',
                    'DESCRIPTION' => '',
                    'PARAMETERS'  => array()
                ),
            ),
        ),
    ),
); 