<?
namespace Bitrix\NotaExt\Ext\DetailGallery;

use Bitrix\Main\Loader;

class Tags
{
    static public function getList()
    {

        $field_tags = \COption::GetOptionString('notaext', 'detailGallery_save_field_tags');
        if (strpos($field_tags, '_') !== false)
            return self::getListIb($field_tags);

    }

    private function getListIb($field_tags)
    {
        $ibId = \COption::GetOptionString('notaext', 'detailGallery_save_ib');
        if (intval($ibId) <= 0)
            return false;

        Loader::includeModule('iblock');

        $arSelect = Array("ID", "NAME", "IBLOCK_ID");
        $arFilter = Array("IBLOCK_ID" => $ibId, "ACTIVE" => "Y", "!{$field_tags}" => false);
        $res = \CIBlockElement::GetList(Array('CNT' => 'desc'), $arFilter, array($field_tags, "{$field_tags}.NAME"), false, $arSelect);
        $arTags = array();
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arTags[] = array('NAME' => $arFields['PROPERTY_TAGS_NAME'], 'ID' => $arFields['PROPERTY_TAGS_VALUE'], 'CNT' => $arFields['CNT']);
        }

        return $arTags;
    }
}