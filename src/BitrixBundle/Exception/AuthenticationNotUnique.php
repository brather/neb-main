<?php
/**
 * User: agolodkov
 * Date: 17.08.2016
 * Time: 12:30
 */

namespace BitrixBundle\Exception;


use Symfony\Component\Security\Core\Exception\AuthenticationException;

class AuthenticationNotUnique extends AuthenticationException
{
    public function getMessageKey()
    {
        return 'Not unique user found.';
    }
}