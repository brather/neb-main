<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

/**
 * @var array $arResult
 */
?>
<div class="b-form_header"><span
        class="iblock"><?= Loc::getMessage(
            'REGISTER_HEADER_ENTITY_FIELDS'
        ); ?></span></div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_PERSON_INN"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_INN'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <em class="error maxlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_MORE'
                ),
                ' 12 ',
                plural(
                    12, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <em class="error minlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_LESS'
                ),
                ' 10 ',
                plural(
                    10, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <input type="text"
                   data-required="required" data-validate="number"
                   value="<?= $arResult["VALUES"]['UF_PERSON_INN'] ?>"
                   id="UF_PERSON_INN" data-maxlength="12"
                   data-minlength="10" name="UF_PERSON_INN"
                   class="input">
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_PERSON_KPP"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_KPP'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <em class="error maxlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_MORE'
                ),
                ' 9 ',
                plural(
                    9, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <em class="error minlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_LESS'
                ),
                ' 9 ',
                plural(
                    9, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <input type="text" data-required="required" data-validate="number"
                   value="<?= $arResult["VALUES"]['UF_PERSON_KPP'] ?>"
                   id="UF_PERSON_KPP" data-maxlength="9"
                   data-minlength="9" name="UF_PERSON_KPP"
                   class="input">
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_PERSON_OKPO"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_OKPO'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <em class="error maxlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_MORE'
                ),
                ' 8 ',
                plural(
                    8, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <em class="error minlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_LESS'
                ),
                ' 8 ',
                plural(
                    8, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <input type="text" data-validate="number"
                   value="<?= $arResult["VALUES"]['UF_PERSON_OKPO'] ?>"
                   id="UF_PERSON_OKPO" data-maxlength="8"
                   data-minlength="8" data-required="true"
                   name="UF_PERSON_OKPO" class="input">
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_PERSON_OGRN"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_OGRN'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <em class="error maxlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_MORE'
                ),
                ' 13 ',
                plural(
                    13, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <em class="error minlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_LESS'
                ),
                ' 13 ',
                plural(
                    13, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <input type="text" data-validate="number"
                   value="<?= $arResult["VALUES"]['UF_PERSON_OGRN'] ?>"
                   id="UF_PERSON_OGRN" data-maxlength="13"
                   data-minlength="13" data-required="true"
                   name="UF_PERSON_OGRN" class="input">
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_PERSON_OKATO"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_OKATO'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <em class="error maxlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_MORE'
                ),
                ' 13 ',
                plural(
                    13, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <em class="error minlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_LESS'
                ),
                ' 2 ',
                plural(
                    2, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <input type="text" data-validate="number"
                   value="<?= $arResult["VALUES"]['UF_PERSON_OKATO'] ?>"
                   id="UF_PERSON_OKATO" data-maxlength="13"
                   data-minlength="2" data-required="true"
                   name="UF_PERSON_OKATO" class="input">
        </div>
    </div>
</div>
<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_PERSON_KBK"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_KBK'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <em class="error maxlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_MORE'
                ),
                ' 20 ',
                plural(
                    20, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <em class="error minlength">
                <?php
                echo Loc::getMessage(
                    'REGISTER_FIELD_ERROR_LESS'
                ),
                ' 20 ',
                plural(
                    20, explode(
                        ',',
                        Loc::getMessage('REGISTER_FIELD_ERROR_PLURAL_SYMBOLS')
                    )
                );
                ?>
            </em>
            <input type="text" data-validate="number"
                   value="<?= $arResult["VALUES"]['UF_PERSON_KBK'] ?>"
                   id="UF_PERSON_KBK" data-maxlength="20"
                   data-minlength="20" data-required="true"
                   name="UF_PERSON_KBK" class="input">
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_POSITION_HEAD"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_POSITION_HEAD'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <input type="text" data-validate="fio"
                   data-required="required"
                   value="<?= $arResult["VALUES"]['UF_POSITION_HEAD'] ?>"
                   id="UF_POSITION_HEAD" name="UF_POSITION_HEAD"
                   class="input">
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_FIO_HEAD"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_FIO_HEAD'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <input type="text" data-validate="fio"
                   data-required="required"
                   value="<?= $arResult["VALUES"]['UF_FIO_HEAD'] ?>"
                   id="UF_FIO_HEAD" name="UF_FIO_HEAD"
                   class="input">
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_CONTACT_INFO"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_CONTACT_INFO'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <input type="text" data-validate="fio"
                   data-required="required"
                   value="<?= $arResult["VALUES"]['UF_CONTACT_INFO'] ?>"
                   id="UF_CONTACT_INFO" name="UF_CONTACT_INFO"
                   class="input">
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_CONTACT_PHONE"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_CONTACT_PHONE'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <input type="text"
                   data-required="required"
                   value="<?= $arResult["VALUES"]['UF_CONTACT_PHONE'] ?>"
                   id="UF_CONTACT_PHONE" name="UF_CONTACT_PHONE"
                   class="input">
        </div>
    </div>
</div>

<div class="fieldrow nowrap">
    <div class="fieldcell iblock">
        <em class="hint">*</em>
        <label for="UF_CONTACT_MOBILE"><?= Loc::getMessage(
                'REGISTER_FIELD_ENTITY_CONTACT_MOBILE'
            ); ?></label>

        <div class="field validate">
            <em class="error required"><?= Loc::getMessage(
                    'REGISTER_FIELD_REQ'
                ); ?></em>
            <em class="error validate"><?= Loc::getMessage(
                    'REGISTER_FIELD_WRONG_FORMAT_CLEAR'
                ); ?></em>
            <input type="text"
                   data-required="required"
                   value="<?= $arResult["VALUES"]['UF_CONTACT_MOBILE'] ?>"
                   id="UF_CONTACT_MOBILE" name="UF_CONTACT_MOBILE"
                   class="input">
        </div>
    </div>
</div>