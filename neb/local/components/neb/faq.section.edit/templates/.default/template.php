<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);?>
<div class="container-fluid">
    <?if(!empty($arResult["ERROR"])) {?>
        <div class="row block-mess bg-danger">
            <h4 class="text-danger"><?= $arResult["ERROR"] ?></h4>
        </div>
        <div class="row"><hr></div>
    <?}?>
    <?if(!empty($arResult["SUCCESS"])) {?>
        <div class="row block-mess bg-success">
            <h4 class="text-success"><?= $arResult["SUCCESS"] ?></h4>
        </div>
        <div class="row"><hr></div>
    <?}?>
    <div class="row">
        <form action="<?= $arResult["FORM_URL"] ?>" method="POST" id="faq-form-update-section" class="nrf">
            <input type="hidden" name="CONTROL_UPDATE_SECTION" value="Y" />
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="NAME_SECTION">
                        <?= Loc::getMessage("NAME_SECTION"); ?>
                    </label>
                    <div class="col-lg-9">
                        <input type="text"
                               class="form-control"
                               name="NAME_SECTION"
                               id="NAME_SECTION"
                               placeholder="<?= Loc::getMessage("INPUT_NAME_SECTION") ?>"
                               value="<?= $arResult['SECTION']["NAME"] ?>"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <span class="col-lg-3" for="NAME_SECTION">
                        
                    </span>
                    <div class="col-lg-9">
                        <?$checked = '';
                        if($arResult['SECTION']["ACTIVE"] == "Y") $checked = ' checked';?>
                        <label class="pointer">
                            <input type="checkbox" name="ACTIVE_SECTION"<?= $checked ?>/>
                            <span class="lbl"><?= Loc::getMessage("ACTIVE_SECTION"); ?></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="DELETE_SECTION">
                        
                    </label>
                    <div class="col-lg-9">
                        <label>
                            <input type="checkbox"
                                   name="DELETE_SECTION"
                                   data-confirm="<?= Loc::getMessage('FAQ_QUESTION_DELETE_SECTION') ?>">
                            <span class="lbl"><?= Loc::getMessage("DELETE_SECTION") ?></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="DELETE_SECTION">
                        <?= Loc::getMessage("SORT_SECTION") ?>
                    </label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="SORT_SECTION" id="DELETE_SECTION" value="<?= $arResult['SECTION']["SORT"] ?>">
                    </div>
                </div>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary">
                    <?= Loc::getMessage("UPDATE_SECTION") ?>
                </button>
            </div>
        </form>
    </div>
</div>