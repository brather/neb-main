
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >

	<div class="b-portalinfo clearfix wrapper">
		<div class="b-portalinfo_num right">
			<span class="b-portalinfo_lb">Доступно</span>
			<span class="b-portalinfo_numbers"><span class="b-portalinfo_number mr  iblock">1</span><span class="b-portalinfo_number iblock">6</span><span class="b-portalinfo_number iblock">7</span><span class="b-portalinfo_number mr  iblock">1</span><span class="b-portalinfo_number iblock">8</span><span class="b-portalinfo_number iblock">7</span><span class="b-portalinfo_number mr  iblock">8</span></span>
			<span class="b-portalinfo_lb">электронных книг&nbsp;</span>
		</div>
		<div class="b-portalinfo_num right">
			<span class="b-portalinfo_lb">Доступно</span>
			<span class="b-portalinfo_numbers"><span class="b-portalinfo_number  iblock">2</span><span class="b-portalinfo_number iblock mr">6</span><span class="b-portalinfo_number iblock">9</span><span class="b-portalinfo_number  iblock">4</span><span class="b-portalinfo_number iblock mr">8</span><span class="b-portalinfo_number iblock">2</span><span class="b-portalinfo_number iblock">4</span><span class="b-portalinfo_number iblock mr">5</span></span>
			<span class="b-portalinfo_lb">записей каталогов</span>
		</div>
		<div class="leftblock iblock">
			<span style="
			background-color: #ee6334;
			color: #fff;
			margin: -25px 5px 0px 275px;
			padding: 3px;
			font-size: 12px;
			font-style: italic;
			display: block;
			width: 71px;
			">beta-версия</span>
			<a href="blind_index.php"  accesskey="0" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
				<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
				Национальная Электронная Библиотека
			</a>

		</div> 			

	</div><!-- /.b-portalinfo -->
	<form class="searchform" action="/special/search/">

		<div class="b-search wrapper">
			<div class="b-search_field">
				<div class="clearfix">
					<input type="submit" class="b-search_bth bbox" value="Найти">
					<span class="b-search_fieldbox">
						<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						<a class="clean-link js_cleaninp" title="Очистить поиск" href="#">Очистить поиск</a>
					</span>

				</div>


				<div class="b_search_set clearfix">
					<a href="#" class="b-search_exlink js_extraform" title="Расширенный поиск">Расширенный поиск</a>
					<div class="checkwrapper">
						<label for="cb1" class="black fz_mid">Искать только в отсканированных изданиях</label> <input class="checkbox" type="checkbox" name="" id="cb1">
						<label for="cb1" class="black fz_mid">Искать по точной фразе</label> <input class="checkbox" type="checkbox" name="" id="cb1">
					</div>


				</div> <!-- /.b_search_set -->
			</div>
		</div>
		<div class="b-search_ex">
			<div class="wrapper rel bbox">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
						<option value="">по месту издательства</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<a href="#" class="b-searchdelrow b-searchaddrow"><span class="b-searchaddrow_minus">-</span><span class="b-searchaddrow_lb js_delsearchrow">удалить условие</span></a>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option data-asrc="authors.php?session=MY_SESSION&moreparams=MORE_PARAMS" value="">по названию</option>
						<option data-asrc="search.php?session=MY_SESSION&moreparams=MORE_PARAMS" value="">по месту издательства</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="" >
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<input class="b-text" type="text" id="js_searchdate_prev" value="1700"  />
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="b-text"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right">
						<input class="checkbox" type="checkbox" name="" id="cb39"><label for="cb39" class="black">искать в авторефератах и диссертациях</label> <br/>
						<input class="checkbox" type="checkbox" name="" id="cb40"><label for="cb40" class="black">Искать по полному тексту издания</label>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>


</section>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		<nav class="b-commonnav">
			<a href="blind_about.php" class="current" title="О проекте">о проекте</a>
			<a href="#" title="право">право</a>
			<a href="#" title="участники">участники</a>
		</nav>
		<div class="b-plaintext">
			<h2>Цели НЭБ</h2>
			<p>Национальная электронная библиотека призвана собирать, архивировать, описывать электронные документы, способствующие сохранению и развитию национальной науки и культуры, и организовывать их общественное использование. Таким образом, должно сформироваться <a href="#">единое национальное собрание</a> полных текстов электронных документов, свободный доступ к которому осуществляется через интернет-портал Национальной электронной библиотеки, что обеспечит основу для развития в России единого социально-культурного пространства. </p>


			<div class="cols threecols">
				<div class="iblock bbox">
					<img src="../i/ico_book_big.png" alt="">
					<h4>1 000 000</h4>
					<p>В настоящее время проект НЭБ поддерживается тремя участниками: Российская государственная библиотека, Российская национальная библиотека и Государственная публичная научно-техническая библиотека России.</p>
				</div>
				<div class="iblock bbox">
					<img src="../i/ico_book_big.png" alt="">
					<h4>1 000 000</h4>
					<p>В настоящее время проект НЭБ поддерживается тремя участниками: Российская государственная библиотека, Российская национальная библиотека и Государственная публичная научно-техническая библиотека России.</p>
				</div>
				<div class="iblock bbox">
					<img src="../i/ico_book_big.png" alt="">
					<h4>1 000 000</h4>
					<p>В настоящее время проект НЭБ поддерживается тремя участниками: Российская государственная библиотека, Российская национальная библиотека и Государственная публичная научно-техническая библиотека России.</p>
				</div>
			</div><!-- /cols -->
			<h3>Подзаголовок</h3>
			<p>Национальная электронная библиотека призвана собирать, архивировать, описывать электронные документы, способствующие сохранению и развитию национальной науки и культуры, и организовывать их общественное использование. Таким образом, должно сформироваться единое национальное собрание полных текстов электронных документов, свободный доступ к которому осуществляется через интернет-портал Национальной электронной библиотеки, что обеспечит основу для развития в России <a href="#">единого социально-культурного</a> пространства. </p>
			<ul class="b-commonlist">
				<li>интеграция библиотек России в единую информационную сеть;</li>
				<li>разработка четких схем взаимодействия библиотек в рамках действующего законодательства, в том числе об авторском праве;</li>
				<li>развитие технической базы, позволяющей обеспечить;</li>
				<li>создание электронных копий высокого качества и единого формата;</li>
				<li>формирование стандартных библиографических описаний и организацию единого </li>
				<li>поиска по всем каталогам, отражающим распределенные фонды;</li>
				<li>возможность вечного хранения электронных документов и удобство работы с .</li>
			</ul>
		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
	

</section>

</div><!-- /.homepage -->

<? include("footer.php"); ?>
</body>
</html>