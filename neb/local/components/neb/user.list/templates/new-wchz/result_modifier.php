<?php
$userIds = [];
$userIdKeyMap = [];
foreach ($arResult['items'] as $key => $user) {
    if (!empty($user['ID'])) {
        $userIdKeyMap[$user['ID']] = $key;
        $userIds[] = $user['ID'];
    }
}
if (!empty($userIds)) {
    $rsUsers = $USER->GetList(
        $by,
        $order,
        [
            'ID' => $userIds,
        ],
        [
            'SELECT' => [
                'UF_STATUS',
                'UF_RGB_USER_ID',
                'UF_ID_REQUEST',
            ],
        ]
    );
    $nebUser = new nebUser();
    while ($userItem = $rsUsers->Fetch()) {
        $additionalFields = [
            'LOCKED'   => false,
            'VERIFIED' => (
                !empty($userItem['UF_RGB_USER_ID'])
                && $userItem['UF_STATUS'] == nebUser::USER_STATUS_VERIFIED
            ),
            'REQUEST'  => !empty($userItem['UF_ID_REQUEST'])
        ];
        $arResult['items'][$userIdKeyMap[$userItem['ID']]] = array_merge(
            $arResult['items'][$userIdKeyMap[$userItem['ID']]],
            $additionalFields
        );
    }
}
