<?php

namespace LbcBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;

/**
 * LbcMaindivisionintroduction
 *
 * @ORM\Table(name="lbc_maindivisionintroduction")
 * @ORM\Entity
 */
class LbcMaindivisionintroduction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serialization\Groups({"list", "details"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="MainDivisionId", type="integer", nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    private $maindivisionid;

    /**
     * @var string
     *
     * @ORM\Column(name="TextPath", type="text", nullable=false)
     */
    private $textpath;

    /**
     * @ORM\OneToMany(targetEntity="LbcMaindivisionintroductionimages", mappedBy="introduction")
     */
    private $images;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set maindivisionid
     *
     * @param integer $maindivisionid
     *
     * @return LbcMaindivisionintroduction
     */
    public function setMaindivisionid($maindivisionid)
    {
        $this->maindivisionid = $maindivisionid;

        return $this;
    }

    /**
     * Get maindivisionid
     *
     * @return integer
     */
    public function getMaindivisionid()
    {
        return $this->maindivisionid;
    }

    /**
     * Set textpath
     *
     * @param string $textpath
     *
     * @return LbcMaindivisionintroduction
     */
    public function setTextpath($textpath)
    {
        $this->textpath = $textpath;

        return $this;
    }

    /**
     * Get textpath
     *
     * @return string
     */
    public function getTextpath()
    {
        return $this->textpath;
    }
}
