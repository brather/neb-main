<?

/**
 * Class saveQuery - сохранение поискового запроса
 */
class saveQuery
{
    /**
     * Может литекущий пользователь сохранять запрос
     * @return bool
     */
    public static function isSave()
    {
        global $USER;
        if (!$USER->IsAuthorized())
            return false;

        $uobj = new nebUser();
        $role = $uobj->getRole();
        if ($role == 'user')
            return true;
        else
            return false;
    }
}