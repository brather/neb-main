<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage("ITEM_EDIT_PAGE") . ' id=' . $arResult['ITEM']['ID']);?>
<div class="container-fluid">
    <?if(!empty($arResult["ERROR"])) {?>
        <div class="row block-mess bg-danger">
            <h4 class="text-danger"><?= $arResult["ERROR"] ?></h4>
        </div>
        <div class="row"><hr></div>
    <?}?>
    <?if(!empty($arResult["SUCCESS"])) {?>
        <div class="row block-mess bg-success">
            <h4 class="text-success"><?= $arResult["SUCCESS"] ?></h4>
        </div>
        <div class="row"><hr></div>
    <?}?>
    <div class="row">
        <form action="<?= $arResult["FORM_URL"] ?>" method="POST" id="faq-form-update-item" class="nrf">
            <input type="hidden" name="CONTROL_UPDATE_ITEM" value="Y" />
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="NAME_ITEM"><?= Loc::getMessage("NAME_ITEM"); ?></label>
                    <div class="col-lg-9">
                        <input type="text"
                               class="form-control"
                               name="NAME_ITEM"
                               placeholder="<?= Loc::getMessage("INPUT_NAME_ITEM") ?>"
                               value="<?= $arResult['ITEM']["NAME"] ?>"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <span class="col-lg-3">
                        
                    </span>
                    <div class="col-lg-9">
                        <?$checked = '';
                        if($arResult['ITEM']["ACTIVE"] == "Y") $checked = ' checked';?>
                        <label for="ACTIVE_ITEM">
                            <input type="checkbox" id="ACTIVE_ITEM" name="ACTIVE_ITEM"<?= $checked ?>/>
                            <span class="lbl"><?= Loc::getMessage("ACTIVE_ITEM"); ?></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="SECTION_NAME_FOR_ITEM">
                        <?= Loc::getMessage("CHOISE_SECTION_FOR_ITEM") ?>
                    </label>
                    <div class="col-lg-9">
                        <select name="SECTION_NAME_FOR_ITEM" id="sect-name" class="form-control">
                            <?foreach ($arResult["SECTIONS"] as $idSect => $nSect){
                                $selected = '';
                                if($arResult['ITEM']["IBLOCK_SECTION_ID"] == $idSect)
                                    $selected = ' selected'?>
                                <option value="<?= $idSect ?>"<?= $selected ?>><?= $nSect ?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="SORT_ITEM">
                        <?= Loc::getMessage("SORT_ITEM") ?>
                    </label>
                    <div class="col-lg-9">
                        <input type="text"
                               name="SORT_ITEM"
                               class="form-control"
                               placeholder="<?= Loc::getMessage("SORT_ITEM_DEFAULT") ?>"
                               value="<?= $arResult['ITEM']["SORT"] ?>" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-lg-3 control-label" for="DETAIL_TEXT_ITEM">
                        <?= Loc::getMessage("DETAIL_TEXT_ITEM"); ?>
                    </label>
                    <div class="col-lg-9">
                        <?$APPLICATION->IncludeComponent(
                            "neb:faq.editor",
                            "",
                            array(
                                "TYPE_EDITOR" => "TYPE_1",
                                "TEXT" => $arResult['ITEM']["DETAIL_TEXT"], // контент из запроса который нужно вставить
                                "TEXTAREA_NAME" => "DETAIL_TEXT_ITEM", // имя поля
                                "TEXTAREA_ID" => "content",         // id поля
                                "TEXTAREA_WIDTH" => "100%",  // ширина
                                "TEXTAREA_HEIGHT" => "300px",    // высота
                                "INIT_ID" => "detail-text", // ID самого редактора
                            )
                        );?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <span class="col-lg-3 control-label">
                        
                    </span>
                    <div class="col-lg-9">
                        <label for="DELETE_ITEM">
                            <input type="checkbox"
                               name="DELETE_ITEM"
                               id="DELETE_ITEM"
                               data-confirm="<?= Loc::getMessage('FAQ_QUESTION_DELETE_ITEM') ?>">
                            <span class="lbl"><?= Loc::getMessage("DELETE_ITEM") ?></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary">
                    <?= Loc::getMessage("UPDATE_ITEM") ?>
                </button>
            </div>
        </form>
    </div>
    <div class="row"><hr></div>
    <div class="row"><p><small><?= Loc::getMessage("REQUIRED_FIELD") ?></small></p></div>
</div>