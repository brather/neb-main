<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('nota.userdata');
use Nota\UserData\Rgb;

if ($USER->IsAuthorized()) LocalRedirect('/');
$log = new CEventLog();

try {
    $arDefaultUrlTemplates404 = array(
        "simple" => "/",
        "full" => "full/",
        "rgb" => "rgb/",
        "ecia" => "ecia/",
    );

    $arDefaultVariableAliases404 = array();

    $arDefaultVariableAliases = array();

    $arComponentVariables = array();

    $SEF_FOLDER = "";
    $arUrlTemplates = array();

    if ($arParams["SEF_MODE"] == "Y") {
        $arVariables = array();

        $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
        $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

        $componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

        if (StrLen($componentPage) <= 0) $componentPage = "simple";

        CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

        $SEF_FOLDER = $arParams["SEF_FOLDER"];

    }

    $arResult = array(
        "FOLDER" => $SEF_FOLDER,
        "URL_TEMPLATES" => $arUrlTemplates,
        "VARIABLES" => $arVariables,
        "ALIASES" => $arVariableAliases
    );
    $type = htmlspecialcharsbx($_REQUEST['type']);
    if (empty($type) and !empty($_REQUEST['types']))
        $type = htmlspecialcharsbx($_REQUEST['types']);

    if ($componentPage == 'simple'){// обязательные к заполнению в битриксе поля :(
       $common = explode('@', $_REQUEST['REGISTER']['EMAIL']);
       $_REQUEST['REGISTER']['LOGIN']     = $common[0];
       $_REQUEST['REGISTER']['NAME']      = $common[0];
       $_REQUEST['REGISTER']['LAST_NAME'] = $common[0];
    }
    elseif ($componentPage == 'main' and !empty($type)) {
        if (!empty($arResult['URL_TEMPLATES'][$type])) {
            $USER->SetParam("REGISTER_TYPE", $type);
            LocalRedirect($arResult['FOLDER'] . $arResult['URL_TEMPLATES'][$type]);
        }
    } elseif ($componentPage == 'rgb') {
            require_once $_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/CAS-1.3.3' . '/CAS.php';
            //phpCAS::setDebug();
            phpCAS::client(CAS_VERSION_2_0, 'passport.rsl.ru/auth', 443, '');
            phpCAS::setNoCasServerValidation();
            phpCAS::forceAuthentication();


            $log->Log(5, 'Регистрация через РГБ', 'registration', 'rgb', print_r(phpCAS::getAttributes(), true));

            if(!phpCAS::hasAttribute('readerNumber') || !phpCAS::getAttribute('readerNumber')):
                throw new \Exception('Ошибка получения данных!');
            endif;

            $RGB_CARD_NUMBER = phpCAS::getAttribute('readerNumber');

            $rgb = new rgb();
            $res = $rgb->usersFind('reader_number', $RGB_CARD_NUMBER);

            if(isset($res['users'][0])):
                $res = $res['users'][0];
            else:
                throw new \Exception('Пользователь с данным номером читательского билета не найден в РГБ!');
            endif;

            if ((int)$res['uid'] > 0) {
                $arResult['CHECK_STATUS'] = true;

                if(isset($res['email']) && $res['email']):
                    $user = CUser::GetList($b, $o, array("=EMAIL" => $res['email']));
                    if ($user->Fetch()):
                        throw new \Exception('Пользователь с такими данными уже зарегистрирован! <br />Убедитесь в правильности введенных данных,<br /> либо пройдите авторизацию логин/email/пароль');
                    endif;
                endif;

                $nebUser = new nebUser();

                $rsUser = $nebUser->GetByLogin($RGB_CARD_NUMBER);
                $arUser = $rsUser->Fetch();

                if (!empty($arUser)) throw new \Exception('Пользователь с данным номером читательского билета уже зарегистрирован');

                if(!$res['email']) $res['email'] = $RGB_CARD_NUMBER.'@rusneb.ru';

                $birthday = DateTime::createFromFormat('H:i:s d/m/Y', $res['birthday']);
                $PASSWORD = $birthday->format('dmY');

                $arFieldsUser = array(
                    'LOGIN'       => $RGB_CARD_NUMBER,
                    'EMAIL'       => $res['email'],
                    'LAST_NAME'   => $res['lastname'],
                    'NAME'        => $res['firstname'],
                    'SECOND_NAME' => $res['middlename'],
                    'PERSONAL_BIRTHDAY'  => $birthday->format('d.m.Y'),
                    'PASSWORD'           => $PASSWORD,
                    'CONFIRM_PASSWORD'   => $PASSWORD,
                    'ACTIVE'             => 'Y',
                    'UF_RGB_CARD_NUMBER' => $RGB_CARD_NUMBER,
                    'UF_RGB_USER_ID'     => $res['uid'],
                    'UF_LIBRARY'         => RGB_LIB_ID,
                    'UF_LIBRARIES' => array(
                        RGB_LIB_ID
                    ),
                );
                $UID_BX = $nebUser->Add($arFieldsUser);

                if ($UID_BX > 0) {
                    $nebUser->setEICHB();

                    $nebUser->setStatus(nebUser::USER_STATUS_VERIFIED);

                    $arResult['RGB_CARD_NUMBER'] = $RGB_CARD_NUMBER;
                    $arResult['EICHB'] = $nebUser->EICHB;

                    $USER->Authorize($UID_BX);
                    CUser::SendUserInfo($UID_BX, 's1', "Приветствуем Вас как нового пользователя НЭБ!");

                    $componentPage = 'rgb_ok';

                    if($res['email'] == $RGB_CARD_NUMBER.'@rusneb.ru'):
                        LocalRedirect('/popup/', false);
                    endif;

                } else{
                    $log->Log(5, 'Регистрация через РГБ', 'registration', 'rgb', ' Пользователь не создан у нас!' . $nebUser->error );
                    $arResult['CHECK_STATUS'] = false;
                }

            } else{
                $log->Log(5, 'Регистрация через РГБ', 'registration', 'rgb', ' Пользователь не найден в РГБ!' . print_r($res, true));
                $arResult['CHECK_STATUS'] = false;
            }

        }
} catch (\Exception $e) {
    $arResult['CHECK_STATUS']  = false;
    $arResult['ERROR_MESSAGE'] = $e->getMessage();
}
$this->IncludeComponentTemplate($componentPage);

?>