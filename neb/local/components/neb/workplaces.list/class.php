<?php
/**
 * User: agolodkov
 * Date: 31.07.2015
 * Time: 10:37
 */

/**
 * Class NebLibraryPartyComponent
 */
\CModule::IncludeModule("highloadblock");
\CModule::IncludeModule("nota.exalead");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class WorkplacesListComponent extends \Neb\Main\ListComponent
{
    /**
     * @var array
     */
    public $_defaultResult
        = array(
            'IBLOCK_ID' => IBLOCK_ID_WORKPLACES,
        );

    /**
     * @return $this
     */
    public function prepareParams()
    {
        $this->_prepareNavigation()
            ->_prepareIblockListParams(IBLOCK_ID_WORKPLACES);

        $orderDirection = 'ASC';
        if ($this->arParams['ORDER'] && 'desc' === $this->arParams['ORDER'])
            $orderDirection = 'DESC';

        if ($this->arParams['BY'])
            $order[$this->arParams['BY']] = $orderDirection;

        if (empty($order))
            $order = ['DATE_CREATE' => 'DESC'];

        $this->arParams['listParams']['order'] = $order;

        if (mb_strlen($this->arParams['WCHZ_NAME']) > 0)
            $this->arParams['listParams']['filter']['NAME'] = '%' . $this->arParams['WCHZ_NAME'] . '%';

        return $this;
    }

    /**
     * @return $this
     */
    public function loadList()
    {
        $this->loadElemets();

        return $this;
    }
}