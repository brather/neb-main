<?
$MESS['RIGHTHOLDER_STAT_BY_ALL_FIELDS'] = 'by all fields';
$MESS['RIGHTHOLDER_STAT_BY_AUTHOR'] = 'by author';
$MESS['RIGHTHOLDER_STAT_BY_FIND'] = 'Find';
$MESS['RIGHTHOLDER_STAT_PERIOD_FROM'] = 'Total in a period from';
$MESS['RIGHTHOLDER_STAT_PERIOD_TO'] = 'to';
$MESS['RIGHTHOLDER_STAT_ON_PORTAL'] = 'in the NEB-portal placed';
$MESS['RIGHTHOLDER_STAT_BOOK_5'] = 'your creations';
$MESS['RIGHTHOLDER_STAT_BOOK_1'] = 'your creation';
$MESS['RIGHTHOLDER_STAT_BOOK_2'] = 'your creations';
$MESS['RIGHTHOLDER_STAT_NAME'] = 'Title';
$MESS['RIGHTHOLDER_STAT_AUTHOR'] = 'Author';
$MESS['RIGHTHOLDER_STAT_BBK'] = 'BBK';
$MESS['RIGHTHOLDER_STAT_DATE'] = 'Date added';
$MESS['RIGHTHOLDER_STAT_LINK'] = 'Link<br> to the pdf';
$MESS['RIGHTHOLDER_STAT_VIEWERS'] = 'Number of hits /<br> Last accessed date';
$MESS['RIGHTHOLDER_STAT_READERS'] = 'Number of readers';
$MESS['RIGHTHOLDER_STAT_VIEW'] = 'View';
