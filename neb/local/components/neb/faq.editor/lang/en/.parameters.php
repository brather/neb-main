<?php
$MESS["EDITOR_MODE"] = "editor mode";
$MESS["EDITOR_SIMPLIFIED"] = "Simplified editor";
$MESS["EDITOR_FULL"] = "full editor";
$MESS["EDITOR_ID"] = "ID editor (unique)";
$MESS["EDITOR_CONTENT_DEFAULT"] = "Content to be inserted into the editor";
$MESS["EDITOR_TEXTAREA_NAME"] = "Name TEXTAREA";
$MESS["EDITOR_TEXTAREA_ID"] = "ID TEXTAREA";
$MESS["EDITOR_WIDTH"] = "Width editor";
$MESS["EDITOR_HEIGHT"] = "Height editor";