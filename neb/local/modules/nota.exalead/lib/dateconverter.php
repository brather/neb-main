<?php
namespace Nota\Exalead;

class DateConverter extends \Bitrix\Main\Text\Converter
{
    public function encode($text, $textType = "")
    {
        if ($text instanceof \Bitrix\Main\Type\DateTime)
            return $text->format('d.m.Y');

        return \Bitrix\Main\Text\String::htmlEncode($text);
    }

    public function decode($text, $textType = "")
    {
        if (is_object($text))
            return $text;

        return \Bitrix\Main\Text\String::htmlDecode($text);
    }
}