<?php
/**
 * User: agolodkov
 * Date: 13.08.2015
 * Time: 13:48
 */

namespace Neb\Main;


class Csv
{
    public $handle;
    public $delimiter = ';';
    public $enclosure = '"';
    public $escape = '\\';
    public $length = 0;
    public $useHeaders = true;
    public $headers = array();
    private $_lineNum = 1;

    /**
     * @param string $filePath
     * @param array  $options
     *
     * @throws \Exception
     * @todo add write case
     */
    public function __construct($filePath, $options = array())
    {
        foreach ($options as $optionName => $optionValue) {
            if (property_exists($this, $optionName)) {
                $this->$optionName = $optionValue;
                continue;
            }
        }
        if (!file_exists($filePath)) {
            throw new \Exception('file not found');
        }
        if (false === ($this->handle = fopen($filePath, 'r+'))) {
            throw new \Exception('cannot open file');
        }

        if (true === $this->useHeaders) {
            $this->headers = $this->getNext();
        }
    }

    /**
     * @return array|bool
     */
    public function getNext()
    {
        $row = fgetcsv(
            $this->handle,
            $this->length,
            $this->delimiter,
            $this->enclosure,
            $this->escape
        );

        if (
            true === $this->useHeaders
            && $this->_lineNum > 1
            && is_array($row)
        ) {
            foreach ($row as $key => $value) {
                $row[$this->headers[$key]] = $value;
                unset($row[$key]);
            }
        }
        $this->_lineNum++;

        return is_array($row) ? $row : false;
    }
}