<?
namespace Nota\Collection;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class CollectionTable extends DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return 'neb_collection_collection';
	}

	public static function getMap()
	{
		return array(
			'ID'      => array(
				'data_type'    => 'integer',
				'primary'      => true,
				'autocomplete' => true,
				'title'        => Loc::getMessage('COLLECTION_ID'),
			),
			
			'LIBRARY_ID' => array(
				'data_type'  => 'string',
				'required'   => false,
				'title'      => Loc::getMessage('COLLECTION_LIBRARY_ID'),
			),
			
			'NAME' => array(
				'data_type'  => 'string',
				'required'   => true,
				'title'      => Loc::getMessage('COLLECTION_LIBRARY_NAME'),
			),
			
			'MAIN'    => array(
				'data_type'  => 'enum',
				'required'   => false,
				values => array(
					'Y',
					'N'
				),
				'title' => Loc::getMessage('COLLECTION_LIBRARY_MAIN'),
			), 
						   
			'BACKGROUND'    => array(
				'data_type'  => 'integer',
				'required'   => false,
				'title'      => Loc::getMessage('COLLECTION_COLLECTION_BACKGROUND'),
			),
			'SORT' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('COLLECTION_ENTITY_SORT_FIELD'),
			),
			'TIMESTAMP_X' => array(
				'data_type' => 'datetime',
				'required' => false,
				'title' => Loc::getMessage('COLLECTION_ENTITY_TIMESTAMP_X_FIELD'),
			),
		);
	}
}
