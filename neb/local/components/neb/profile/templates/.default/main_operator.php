<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

require(dirname(__FILE__) . '/includes/header.php');
?>
    Личный кабинет оператора НЭБ
<?php
require(dirname(__FILE__) . '/includes/footer.php');
