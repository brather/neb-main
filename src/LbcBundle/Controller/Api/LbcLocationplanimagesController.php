<?php
/**
 * User: agolodkov
 * Date: 16.08.2016
 * Time: 11:42
 */
namespace LbcBundle\Controller\Api;

use Doctrine\Bundle\DoctrineBundle\Registry;
use LbcBundle\Controller\Traits\DoctrineInjectTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class LbcMaindivisionimagesController
 *
 * @DI\Service("app.api.locationplans.images", scope="request")
 */
class LbcLocationplanimagesController extends AbstractImagesController
{
    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcLocationplanimages';
    }

    /**
     * @return string
     */
    public function getOwnFieldName()
    {
        return 'locationplanid';
    }
}