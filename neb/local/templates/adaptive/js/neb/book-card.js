/**
 * Старенькое
 */
//Обработчик всплывающей подсказки
$(function(){
    $.fn.exclamationOpener = function() { // выделение поисковых позиций при выборе с клавиатуре

        $(this).hover(function() {
            var popup = $(this).next();
            //Позиционирование всплывающего окна, чтобы не уползала за экран
            var popupHeight = popup.outerHeight();
            var openerCoords = $(this).offset();
            var topWindowCoords = $('body').scrollTop();
            var openerBottomCoords = $(this).height();
            var popupTop = openerBottomCoords + 15;

            if((openerCoords.top - topWindowCoords) < popupHeight){
                $(popup).addClass('popup-window-bottom');
                popup.css({
                    'top': popupTop+'px',
                    'bottom' : 'auto'
                });
            }
            else {
                $(popup).removeClass('popup-window-bottom');
                popup.css({
                    'top': 'auto',
                    'bottom': popupTop+'px',

                });
            }

            popup.fadeIn('fast', function(){
                popup.addClass('popup-hover');
            });
            event.stopPropagation;
        }, function() {
            var popup = $(this).next();
            setTimeout(function() {
                if(!popup.hasClass('popup-self')){
                    popup.fadeOut('fast', function(){
                        popup.removeClass('popup-hover');
                    });
                }
            }, 500);
        });

        $(this).next().hover(function() {
                $(this).fadeIn();
                $(this).addClass('popup-self');
                event.stopPropagation;
            },
            function() {
                var popup = $(this);
                popup.removeClass('popup-self')
                setTimeout(function() {
                    popup.fadeOut('slow', function(){
                        popup.removeClass('popup-self');
                    });
                }, 300);
            });

        $(this).click(function(){
            event.preventDefault();
        });

        $(document).click(function(event){
            if(event.target) {
                if($(event.target).closest('.popup-window').length==0  && !event.target.classList.contains('exclamation-opener')) {
                    $('.popup-window').fadeOut().removeClass('popup-hover');
                }
            }
        });
    };
});


$(function () {
    var mappage = false;
    $('.link-nearest-library').click(function () {
        var mapContainer = $('.library-map');
        if (false === mappage) {
            mapContainer.slideToggle({
                start: function () {
                    mapContainer.
                        mappage();
                    mappage = true;
                }
            });
        }
    });
});