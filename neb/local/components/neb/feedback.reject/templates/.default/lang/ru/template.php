<?php

$MESS['FEEDBACK_REJECT_DATE_CREATE'] = 'Дата обращения';
$MESS['FEEDBACK_REJECT_DATE_CLOSE']  = 'Дата ответа';
$MESS['FEEDBACK_REJECT_STATUS']      = 'Статус';
$MESS['FEEDBACK_REJECT_THEME']       = 'Тема';
$MESS['FEEDBACK_REJECT_BOOK']        = 'Книга';
$MESS['FEEDBACK_REJECT_MESSAGE']     = 'Сообщение';
$MESS['FEEDBACK_REJECT_ANSWER']      = 'Ответ';
$MESS['FEEDBACK_REJECT_FILE']        = 'Файл';

$MESS['FEEDBACK_REJECT_TITLE']       = 'Отклонение заявки №';
$MESS['FEEDBACK_REJECT_WRITE_CAUSE'] = 'Напишите причину отклонения';
$MESS['FEEDBACK_REJECT_SUBMIT']      = 'Отклонить заявку';