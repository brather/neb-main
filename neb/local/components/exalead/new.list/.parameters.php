<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader,
    \Bitrix\Main\LoaderException,
    \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__); 

try
{
	if (!Loader::includeModule('iblock'))
		throw new LoaderException(Loc::getMessage('NEW_LIST_PARAMETERS_IBLOCK_MODULE_NOT_INSTALLED'));
		
	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			
			"DATE" => array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('NEW_LIST_PARAMETRS_DATE'),
			"TYPE" => "NUMBER",
			"DEFAULT" => '',
			),
			
			"COUNT" => array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('NEW_LIST_PARAMETRS_COUNT'),
			"TYPE" => "STRING",
			"DEFAULT" => '',
			),
			
			"PAGE_URL" => array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage('NEW_LIST_PARAMETRS_LINK'),
			"TYPE" => "STRING",
			"DEFAULT" => '',
			),
			
			'CACHE_TIME' => array(
				'DEFAULT' => 3600
			)
		)
	);
}
catch (LoaderException $e)
{
	ShowError($e -> getMessage());
}
