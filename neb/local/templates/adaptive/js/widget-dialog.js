﻿function WidgetDialog(target){										
	this.target = target;
	this.Reload = false;
	this.VisibleSave = false;
	this.Execute = function(link){
						var aSTR = '<div id="WD-ui-widget-overlay" class="ui-widget-overlay ui-front dark" onClick="visibleDialog(false,'+this.Reload+');"></div>'+
												'<div id="WD-ui-dialog" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front no-close ajaxpopup" tabindex="-1" role="dialog" aria-describedby="ui-id-10" aria-labelledby="ui-id-11" style="height: auto; width: 100%; top: 0px; left: 0px; display: block; background: rgb(255, 255, 255);">'+
													'<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">'+
														'<span id="ui-id-11" class="ui-dialog-title">&nbsp;</span>'+
														'<button onClick="visibleDialog(false,'+this.Reload+');" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title="close">'+
															'<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>'+
															'<span class="ui-button-text">close</span>'+
														'</button>'+
													'</div>'+		
													'<iframe src="'+link+'" id="ifr_popup" scrolling="yes" frameborder="0" style="width:100%;" onload="iframeLoad(this);"></iframe>';
						if (this.VisibleSave){
							aSTR = aSTR+'<div style="width:100%;height:50px;padding: 2px 8px;"><button onClick="visibleDialog(false,'+this.Reload+');" type="button" class="button_mode bottompopupclose btn btn-primary" role="button" href="/" style="position: absolute;bottom:10px;right:35px;">Сохранить</button></div>';	
						};
						aSTR = aSTR+'</div>';
						$('#'+this.target).html(aSTR);
						visibleDialog(true, this.Reload);
					}
};
function visibleDialog(value, reload){
  if (value == true){
	$('#WD-ui-widget-overlay').show();
	$('#WD-ui-dialog').show(); 
  } else{
	$('#WD-ui-widget-overlay').hide();
	$('#WD-ui-dialog').hide();
	if (reload){location.reload()};
  };  
};
function iframeLoad(frame){
	(function($){$.fn.ZIAutoHeight = function(){var t = this, doc = 'contentDocument' in t[0]? t[0].contentDocument : t[0].contentWindow.document;t.css('height', doc.body.scrollHeight+'px');}})(jQuery);								
	$(frame).ZIAutoHeight();
};