<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $USER, $APPLICATION;
if (!$USER->IsAuthorized())
    $APPLICATION->AuthForm("У вас нет права доступа к данному файлу.");

CModule::IncludeModule("nota.exalead");
use Nota\Exalead\BooksAdd;

if ($_SERVER['REQUEST_METHOD'] == 'POST' and !empty($_REQUEST['add_book'])) {
    $libId = BooksAdd::setLibrary(LIB_ID_RIGHTHOLDER, 'Библиотека правообладателей');

    $arFields = array(
        'ALIS' => $libId,
        'Author' => htmlspecialcharsEx($_REQUEST['AUTHOR']),
        'Name' => htmlspecialcharsEx($_REQUEST['NAME']),
        'PublishYear' => htmlspecialcharsEx($_REQUEST['PUBLISH_YEAR']),
        'BBKText' => htmlspecialcharsEx($_REQUEST['BBK']),
        'pdfLink' => htmlspecialcharsEx($_REQUEST['FILE_PDF']),

    );

    $idBook = BooksAdd::add($arFields, LIB_ID_RIGHTHOLDER);
    if ($idBook > 0)
        LocalRedirect($APPLICATION->GetCurPage());
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' and !empty($_REQUEST['edit_book'])) {
    $libId = BooksAdd::setLibrary(LIB_ID_RIGHTHOLDER, 'Библиотека правообладателей');
    $book_id = (int)$_REQUEST['ID'];

    $arFields = array(
        'ALIS' => $libId,
        'Author' => htmlspecialcharsEx($_REQUEST['AUTHOR']),
        'Name' => htmlspecialcharsEx($_REQUEST['NAME']),
        'PublishYear' => htmlspecialcharsEx($_REQUEST['PUBLISH_YEAR']),
        'BBKText' => htmlspecialcharsEx($_REQUEST['BBK']),
        'pdfLink' => htmlspecialcharsEx($_REQUEST['FILE_PDF']),

    );

    BooksAdd::edit($book_id, $arFields);
    /*$idBook = BooksAdd::add($arFields, LIB_ID_RIGHTHOLDER);
    if($idBook > 0)
        LocalRedirect($APPLICATION->GetCurPage());*/
}

if ($_REQUEST['action'] == 'remove' and (int)$_REQUEST['id'] > 0) {
    BooksAdd::remove((int)$_REQUEST['id']);
    LocalRedirect($APPLICATION->GetCurPage());
}

if ($_REQUEST['action'] == 'todel' and (int)$_REQUEST['id'] > 0) {
    $arFields = array(
        'toDel' => htmlspecialcharsEx(1)
    );
    $bookId = (int)$_REQUEST['id'];
    //if (intval($bookId) <= 0 || empty($arFields))
    //				continue;
    global $DB;
    foreach ($arFields as &$Field) {
        $Field = '"' . $DB->ForSql($Field) . '"';
    }
    $DB->Update('tbl_common_biblio_card', $arFields, "WHERE ID='" . $bookId . "'", $err_mess . __LINE__);
    //BooksAdd::edit((int)$_REQUEST['id'], $arFields);
    LocalRedirect($APPLICATION->GetCurPage());
}
if ($_REQUEST['action'] == 'undotodel' and (int)$_REQUEST['id'] > 0) {
    $arFields = array(
        'toDel' => htmlspecialcharsEx(0)
    );
    $bookId = (int)$_REQUEST['id'];
    //if (intval($bookId) <= 0 || empty($arFields))
    //				continue;
    global $DB;
    foreach ($arFields as &$Field) {
        $Field = '"' . $DB->ForSql($Field) . '"';
    }
    $DB->Update('tbl_common_biblio_card', $arFields, "WHERE ID='" . $bookId . "'", $err_mess . __LINE__);
    //BooksAdd::edit((int)$_REQUEST['id'], $arFields);
    LocalRedirect($APPLICATION->GetCurPage());
}

$arFilter = array();

$q = htmlspecialcharsbx($_REQUEST['qbook']);
$type = htmlspecialcharsbx($_REQUEST['qbooktype']);
if (!empty($q)) {
    if ($type == 'all')
        $arFilter[] = ' and (Name like "%' . $q . '%" or Author like "%' . $q . '%" or BBKText like "%' . $q . '%" or PublishYear like "%' . $q . '%")';
    elseif ($type == 'Name')
        $arFilter[] = ' and Name like "%' . $q . '%"';
    elseif ($type == 'Author')
        $arFilter[] = ' and Author like "%' . $q . '%"';
}

$by = htmlspecialcharsEx($_REQUEST['by']);
if (empty($by))
    $by = 'BX_TIMESTAMP';
$order = htmlspecialcharsEx($_REQUEST['order']);
if (empty($order))
    $order = 'desc';

$arResult['BOOKS'] = BooksAdd::getListBooksCurrentUser($arFilter, array(), $by, $order);

$this->IncludeComponentTemplate();

$APPLICATION->SetTitle("Работа с фондами");