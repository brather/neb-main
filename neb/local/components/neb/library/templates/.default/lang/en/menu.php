<?
$MESS['LIBRARY_MENU_ABOUT']       = "About the library";
$MESS['LIBRARY_MENU_FUNDS']       = "Funds";
$MESS['LIBRARY_MENU_COLLECTIONS'] = "Collections";
$MESS['LIBRARY_MENU_NEWS']        = "News";
$MESS['LIBRARY_MENU_POPULAR']     = "Popular publications";