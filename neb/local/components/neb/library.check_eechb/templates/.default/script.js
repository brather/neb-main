$(function(){
    var sessid = $('.action_cell').data('sessid');
    $('.action-unbind').click(function(){
        var reader_id = $(this).closest("tr").find("td:first").text();
        if(confirm("Удалить читателя №"+reader_id+" из библиотеки?"))
        {
            var params = {
                'sessid' : sessid
            };
            var sender = $(this);
            $.post($(this).data("url"), params, function(data){
                if(data["error"]==0)
                {
                    sender.closest("tr").remove();
                }
                else
                {
                    alert(data["message"]);
                }
            },'json');
        }
    });

    $('.action-block').click(function () {
        var params = {
            'sessid': sessid
        };
        $.post($(this).data('url'), params, function (data) {
            if (data['error'] != 0) {
                alert(data['message']);
                return;
            }
            if (data.hasOwnProperty('block') && false === data.block) {
                $(this).parent().find('.checkbox').removeClass('checked');
            } else {
                $(this).parent().find('.checkbox').addClass('checked', true);
            }
        }.bind(this), 'json');
    });
    $('.checkbox-action input.checkbox').change(function() {
        $(this).parents('.checkbox-action').find('.action-block').click();
    });
});
