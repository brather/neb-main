<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

use Nota\UserData\Collections;
use \Neb\Main\Helper\MainHelper;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized())
    exit();

$name = htmlspecialcharsbx(trim($_REQUEST['name']));
if (empty($name))
    exit();

$id = htmlspecialcharsbx(trim($_REQUEST['id']));
$type = htmlspecialcharsbx(trim($_REQUEST['t']));

CModule::IncludeModule("nota.userdata");

$saveID = Collections::add($name, $id, $type);

MainHelper::showJson(array('ID' => $saveID));