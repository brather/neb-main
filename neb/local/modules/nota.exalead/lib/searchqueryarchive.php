<?php
namespace Nota\Exalead;

/**
 * Подсказки в поисковой строке по типу "Архивы"
 */
class SearchQueryArchive extends SearchQuery {

    public function getSuggestTitle($query) {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title_archive';
        if(empty($query))
                return false;
        $this->setQuery($query);
        return $this;
    }

    public function getSuggestTitleAll($query) {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title_archive_all';
        $this->setQuery($query);
        return $this;
    }
    
    public function getSuggestAuthor($query) {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author_archive';
        if(empty($query))
                return false;
        $this->setQuery($query);
        return $this;
    }

    public function getSuggestAuthorAll($query) {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author_archive_all';
        $this->setQuery($query);
        return $this;
    }
}