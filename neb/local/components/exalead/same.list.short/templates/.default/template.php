<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (empty($arResult['ITEMS']))
    return false;

if ($_REQUEST['Ajax_instances'] != 'Y' && 'same' !== $_REQUEST['listType']) {
    CJSCore::Init();
} else {
    $APPLICATION->RestartBuffer();
}

?>
<div class="content-instances same same-block">
    <ul class="b-samelist-list">
        <?
        foreach ($arResult['ITEMS'] as $arElement) {
            ?>

            <li class="<?= ($arResult['CURRENT_ID'] == $arElement['ID']) ? 'same-list-kind--active' : '' ?>">
                <?
                if ($arResult['CURRENT_ID'] != $arElement['ID']) {
                    ?>
                    <a
                            class="popup_opener ajax_opener coverlay no_scroll"
                            data-width="955"
                            data-page="<?= $arResult['CURRENT_PAGE'] ?>"
                            href="<?= $arElement['LINK'] ?>?PAGEN_1=<?= $arResult['CURRENT_PAGE'] ?>">
                        <? if ($arElement['PUBLISHYEAR'] != '') { ?>
                            Издание <?= $arElement['PUBLISHYEAR'] ?> года
                        <?
                        } else { ?>
                            Год издания не известен
                            <?
                        }
                        ?>
                    </a>
                    <?
                } else {
                    ?>
                    <span class="b-samelist-current">
							<? if ($arElement['PUBLISHYEAR'] != '') { ?>
                                Издание <?= $arElement['PUBLISHYEAR'] ?> года
                            <?
                            } else { ?>
                                Год издания не известен
                                <?
                            }
                            ?>
							</span>
                <?
                }
                ?>
                <? if (!empty($arElement['VIEWER_URL']) and $arElement['IS_READ'] == 'Y') {
                    ?>
                    <a href="javascript:void(0);"
                       class="b-samelist-read"
                       target="_blank"
                       title="Читать книгу"
                       onclick="readBook(event, this); return false;"
                       data-link="<?= $arElement['ID'] ?>"
                       data-id="<?= $arResult['BOOK']['id'] ?>"
                       data-options="<?php echo htmlspecialchars(
                           json_encode(
                               array(
                                   'title' => trim($arElement['AUTHOR']),
                                   'author' => trim(strip_tags($arElement['TITLE']))
                               )
                           ),
                           ENT_QUOTES,
                           'UTF-8'
                       ) ?>">
                    </a>
                <?
                } ?>
                <div class="same-book-lib">
                    <?php if (empty($arElement['DETAIL_PAGE_URL'])) { ?>
                        <?= $arElement['LIBRARY'] ?>
                    <?php } else { ?>
                        <a target="_blank" href="<?= $arElement['DETAIL_PAGE_URL'] ?>"><?= $arElement['LIBRARY'] ?></a>
                    <?php } ?>
                </div>
            </li>
            <?
        }
        ?>
    </ul>
    <span class="nav-instances same">
			<?= $arResult['STR_NAV'] ?>
		</span>
    <?
    if ($_REQUEST['Ajax_instances'] == 'Y' && 'same' === $_REQUEST['listType'])
        exit();
    ?>
</div>


<script type="text/javascript">
    $(function () {
        DOCUMENT.on('click', '.nav-instances.same a', function (e) {
            e.preventDefault();
            var href = $(this).attr('href');

            if (href == '#' || href == '')
                return false;

            BX.showWait(this);

            $.get(href, {Ajax_instances: "Y", listType: 'same'}, function (data) {
                $('.content-instances.same').html(data);
                BX.closeWait();
                $('.iblock').cleanWS();
                if ($('.b-addbook_popuptit.same')) {
                    var destination = $('.b-addbook_popuptit.same').offset().top();
                    if ($.browser.safari) {
                        $('body').animate({scrollTop: destination}, 1100);
                    } else {
                        $('html').animate({scrollTop: destination}, 1100);
                    }
                }
            });
            return false;
        });


        DOCUMENT.on('click', '.same-selector', function (e) {
            e.preventDefault();
            $('.same-selector').removeClass('same-selector--active');
            $(this).addClass('same-selector--active');
            var href = $(this).attr('href');

            if (href == '#' || href == '')
                return false;

            $.get(href, {Ajax_instances: "Y", listType: 'same', loadType: $(this).data('type')}, function (data) {
                $('.content-instances.same').html(data);
                if ($('.b-addbook_popuptit.same')) {
                    var destination = $('.b-addbook_popuptit.same').offset().top();
                    if ($.browser.safari) {
                        $('body').animate({scrollTop: destination}, 1100);
                    } else {
                        $('html').animate({scrollTop: destination}, 1100);
                    }
                }
            });

            return false;
        });
    });
</script>