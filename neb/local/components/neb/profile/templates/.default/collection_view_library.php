<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__DIR__.'/main_library.php'); // Для того, что б не копировать один и тот же текст
Loc::loadMessages(__FILE__);
?>

<?
$APPLICATION->IncludeComponent(
    "neb:collections.detail",
    "library_profile",
    array(
        "PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "IBLOCK_ID" => IBLOCK_ID_COLLECTION,
        "SECTION_ID" => $arResult["VARIABLES"]["ID"],
        "LIBRARY_ID" => $arResult['LIBRARY']['ID']
    ),
    $component
);
