<?php
/**
 * User: agolodkov
 * Date: 12.07.2016
 * Time: 10:52
 */

namespace AppBundle\Command;


use Neb\Main\Stat\Tracker;
use Nota\Exalead\SearchClient;
use Nota\Exalead\SearchQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckCollectionBookAvailable extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('neb:check-collection-books')
            ->setDescription('Check collection books existsing in exalead')
            ->addOption(
                'collection',
                null,
                InputOption::VALUE_REQUIRED
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);

        $this->getContainer()
            ->get('bitrix')
            ->requireProlog(
                $this->getContainer()
                    ->get('kernel')
                    ->getRootDir()
            );
        while (ob_get_level()) {
            ob_end_clean();
        }

        $element = new \CIBlockElement();
        $batch = 20;
        $iteration = 1;
        $deactivated = 0;
        $total = 0;
        $filter = ['IBLOCK_ID' => IBLOCK_ID_COLLECTION];
        if ($collectionId = (integer)$input->getOption('collection')) {
            $filter['SECTION_ID'] = $collectionId;
        }
        do {
            $ids = [];
            $continue = false;
            $rsBooks = $element->GetList(
                [],
                $filter,
                null,
                [
                    'iNumPage'        => $iteration,
                    'nPageSize'       => $batch,
                    'checkOutOfRange' => true,
                ],
                [
                    'ID',
                    'PROPERTY_BOOK_ID',
                ]
            );
            while ($book = $rsBooks->Fetch()) {
                $continue = true;
                $bookId = trim($book['PROPERTY_BOOK_ID_VALUE']);
                if (!empty($bookId)) {
                    $ids[$bookId] = $book['ID'];
                }
                $total++;
            }
            $iteration++;
            $query = new SearchQuery();
            $query->setParam('sl', 'sl_nofuzzy');
            $query->getByArIds(array_keys($ids));
            $client = new SearchClient();
            $result = $client->getResult($query);
            if (isset($result['ITEMS']) && is_array($result['ITEMS'])) {
                foreach ($result['ITEMS'] as $item) {
                    if (isset($ids[$item['id']])) {
                        unset($ids[$item['id']]);
                    }
                }
            }
            foreach ($ids as $id) {
                $element->Update($id, ['ACTIVE' => 'N']);
                $deactivated++;
            }
        } while (true === $continue);


        $output->writeln("Total elements: $total");
        $output->writeln("Deactivated elements: $deactivated");

        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}