<?php
$MESS['DATE_REGISTER'] = 'Дата регистрации';
$MESS['ACTION'] = 'Действие';
$MESS['RIGHTHOLDER_BOOKS'] = 'Список книг';
$MESS['RIGHTHOLDER_BOOKS_TITLE'] = 'Книги правообладателя';
$MESS["READERS_LIST_VERIFY"] = "Верифицировать";
$MESS["READERS_LIST_VERIFIED"] = "Верифицировано";

$MESS["WORK_COMPANY"] = "Название организации";
$MESS["STATUS_ALL"] = "Все статусы";
$MESS["STATUS_VERIFIED"] = "Верифицирован";
$MESS["STATUS_REGISTERED"] = "Не верифицирован";