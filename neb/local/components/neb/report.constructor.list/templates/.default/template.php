﻿<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<section class="innersection innerwrapper clearfix ">
    <div class="b-mainblock left">
		<?if (count($arResult['reportsList']) > 0):?>
        <h3>Сформировать отчет по шаблону</h3>
        <div class="report-list">
		<?
		foreach ($arResult['reportsList'] as $report):
		?>
        <p class="report-item">
            <a href="/local/tools/report_constructor/report.php?id=<?=$report['Id']?>" id='report<?=$report['Id']?>' target='_blank' style="vertical-align: middle"><?=$report['Name']?></a>
            <a href="<?if (trim($report["Query"] == '')):?>add/<?endif;?>?id=<?=$report['Id']?>" title="Редактировать шаблон" class="report-edit"></a>
            <a href="#" title="Удалить шаблон" class="report-remove" data-id="<?=$report['Id']?>" data-url='/local/tools/report_constructor/delete_report.php'></a>
        </p>
		<?endforeach;?>
        </div>
		<?endif;?>
        <br>
        <h3>Создать шаблон</h3>
        <form action="add/" method="get" class="report-name__form">
			<?if ($_REQUEST['id']):?><input type='hidden' name='report_id' value='<?=$_REQUEST['id']?>'><?endif;?>
            <input type="text" name="report_name" class="b-text"  placeholder="Введите название" style="height: 35px;padding: 0 7px 2px;width: 80%;" <?if ($_REQUEST['id']):?>value='<?=$arResult['reportsList'][$_REQUEST['id']]['Name']?>'<?endif;?>>
            <br><br>
			<textarea placeholder="Запрос" rows="5" name="select" class="form-control"><?if ($_REQUEST['id']):?><?=$arResult['reportsList'][$_REQUEST['id']]['Query']?><?endif;?></textarea><br>
           <button class="btn btn-primary report-name__submit">Далее</button>
        </form>
    </div>
</section>

<script>
    $(function(){
		
        $('.report-name__form input').on('input', function(){
            if($(this).val().length > 0) {
                $(this).removeClass('error');
            } else {
                $(this).addClass('error');
            }
        });

        $('.report-name__submit').click(function (e) {
            e.preventDefault();
            var $form = $(this).closest('form'),
                $input = $form.find('input');

            if($input.val().length > 0) {
                $input.removeClass('error');
                $form.submit();
            } else {
                $input.addClass('error');
            }
        });

        $('.report-remove').click(function (event) {
            event.preventDefault();

            var url = $(this).data('url'),
                id = $(this).data('id'),
                self = this;

            $.ajax({
                type: "GET",
                url: url,
                data: {
                    id: id
                },
                success: function (msg) {
                    if(msg === '1'){
                        $(self).parent().remove();
                        if($('.report-list').find('.report-item').length === 0) {
                            $('.report-list').text('Нет сохраненных шаблонов');
                        }
                    }
                }
            });
        });

    });
</script>