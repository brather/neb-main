<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 31.05.2017
 * Time: 14:13
 */
namespace Neb\Main\Library;

use Bitrix\Main\Entity\DataManager;

/**
 * Class LibsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> UF_ID int optional
 * <li> UF_PARENTID int optional
 * <li> UF_NAME string optional
 * <li> UF_DATE_OPEN string optional
 * <li> UF_STRUCTURE string optional
 * <li> UF_ADRESS string optional
 * <li> UF_REGION string optional
 * <li> UF_DISTRICT string optional
 * <li> UF_TOWN string optional
 * <li> UF_PHONE string optional
 * <li> UF_COMMENT_1 string optional
 * <li> UF_ADR_FIAS int optional
 * <li> UF_REC_ID int optional
 * <li> UF_ADMINISTRATIVEARE string optional
 * <li> UF_LOCALITY string optional
 * <li> UF_POS string optional
 * <li> UF_AREA string optional
 * <li> UF_AREA2 string optional
 * <li> UF_DESCRIPTION string optional
 * <li> UF_FULL_DESCRIPTION string optional
 * <li> UF_CITY int optional
 * <li> UF_DATE_JOIN datetime optional
 * <li> UF_FULL_NAME string optional
 * <li> UF_CONTRACT string optional
 * <li> UF_RGB_ID int optional
 * <li> UF_LIBRARY_TYPE int optional
 * <li> UF_LIBRARY_SCALE int optional
 * <li> UF_RSL_CODE string optional
 * <li> UF_DATE_MODIFY datetime optional
 * </ul>
 *
 * @package Bitrix\Libs
 **/

class LibsTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'neb_libs';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'UF_ID' => array(
                'data_type' => 'integer',
            ),
            'UF_PARENTID' => array(
                'data_type' => 'integer',
            ),
            'UF_NAME' => array(
                'data_type' => 'text',
            ),
            'UF_DATE_OPEN' => array(
                'data_type' => 'text',
            ),
            'UF_STRUCTURE' => array(
                'data_type' => 'text',
            ),
            'UF_ADRESS' => array(
                'data_type' => 'text',
            ),
            'UF_REGION' => array(
                'data_type' => 'text',
            ),
            'UF_DISTRICT' => array(
                'data_type' => 'text',
            ),
            'UF_TOWN' => array(
                'data_type' => 'text',
            ),
            'UF_PHONE' => array(
                'data_type' => 'text',
            ),
            'UF_COMMENT_1' => array(
                'data_type' => 'text',
            ),
            'UF_ADR_FIAS' => array(
                'data_type' => 'integer',
            ),
            'UF_REC_ID' => array(
                'data_type' => 'integer',
            ),
            'UF_ADMINISTRATIVEARE' => array(
                'data_type' => 'text',
            ),
            'UF_LOCALITY' => array(
                'data_type' => 'text',
            ),
            'UF_POS' => array(
                'data_type' => 'text',
            ),
            'UF_AREA' => array(
                'data_type' => 'text',
            ),
            'UF_AREA2' => array(
                'data_type' => 'text',
            ),
            'UF_DESCRIPTION' => array(
                'data_type' => 'text',
            ),
            'UF_FULL_DESCRIPTION' => array(
                'data_type' => 'text',
            ),
            'UF_CITY' => array(
                'data_type' => 'integer',
            ),
            'UF_DATE_JOIN' => array(
                'data_type' => 'datetime',
            ),
            'UF_FULL_NAME' => array(
                'data_type' => 'text',
            ),
            'UF_CONTRACT' => array(
                'data_type' => 'text',
            ),
            'UF_RGB_ID' => array(
                'data_type' => 'integer',
            ),
            'UF_LIBRARY_TYPE' => array(
                'data_type' => 'integer',
            ),
            'UF_LIBRARY_SCALE' => array(
                'data_type' => 'integer',
            ),
            'UF_RSL_CODE' => array(
                'data_type' => 'text',
            ),
            'UF_DATE_MODIFY' => array(
                'data_type' => 'datetime',
            ),
        );
    }
}