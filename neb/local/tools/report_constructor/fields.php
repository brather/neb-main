<?
/**
 * Загрузка связей и полей
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!TblEntities::checkAccess()) {
    die('Permissions denied!');
}

if ($_REQUEST['entities']) {
	$relations = TblEntities::getEntityRelations($_REQUEST['entities']);
	asort($_REQUEST['entities']);

	if (count($_REQUEST['entities']) > 1){
	?>
		<div class="filter-list">
		<h4 class="filter-list__title">Связи</h4>
		<?if (count($_REQUEST['entities']) > 1 and (array_diff($_REQUEST['entities'], $relations['entities'] )|| !$relations['entities'])):?>
		<p>Ошибка! Таблицы невозможно связать.</p>
		<?else:?>
			<?foreach ($relations['join'] as $relation):?>
				<label for="join<?=$relation['Id']?>">
					<input type='hidden' name='entity_1_<?=$relation['Id']?>' value='<?=$relation['tblid1']?>'>
					<input type='hidden' name='entity_2_<?=$relation['Id']?>' value='<?=$relation['tblid2']?>'>
					<input class="relation-checkbox" type="checkbox" id="join<?=$relation['Id']?>" name="join[]" value="<?=$relation['Id']?>">
					<span class="lbl"><?=$relation['Description']?></span>
				</label>
			<?endforeach;?>
		
		<?
		endif;
		?>
		</div>
	<?
	}
	?>

	<div class="filter-wrap">
	<?
    foreach ($_REQUEST['entities'] as $item) {
		$res = TblEntities::getEntities($item);
		$arFields = TblEntities::getEntityFields($item);
		?>

		<div class="filter-list">
        <h4 class="filter-list__title">
            <?=$res[$item];?>
        </h4>
		<?foreach ($arFields as $field):?>
			<div class="steps__row">
				<label for="fields<?=$field['Id']?>">
					<input type="checkbox" id="fields<?=$field['Id']?>" name="fields[]" value="<?=$field['Id']?>">
					<span class="lbl"><?=$field['Description']?></span>
				</label>
			</div>
		<?endforeach;?>
        </div>    
	<?
	}?>
	</div>
	<?
}
?>