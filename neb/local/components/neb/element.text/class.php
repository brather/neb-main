<?php

use \Bitrix\Main\Loader;
use \Bitrix\Iblock\IblockTable;
use \Bitrix\Iblock\ElementTable;

/**
 * User: agolodkov
 * Date: 27.06.2016
 * Time: 12:52
 */
class ElementTextComponent extends \CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $arParams = array_replace_recursive(
            [
                'XML_ID'      => '',
                'CACHE_TIME'  => 3600,
                'CACHE_TYPE'  => 'A',
                'SET_TITLE'   => 'N',
                'LANGUAGE_ID' => LANGUAGE_ID
            ], $arParams
        );
        $arParams['XML_ID'] = trim($arParams['XML_ID']);

        return $arParams;
    }

    public function executeComponent()
    {
        global $USER, $APPLICATION;

        try {
            if ($USER->IsAdmin() || $this->startResultCache()) {
                $this->_getData();
                $this->includeComponentTemplate();
            }

            if ('Y' === $this->arParams['SET_TITLE']) {
                $APPLICATION->SetTitle($this->arResult['ELEMENT']['NAME']);
            }

        } catch (Exception $e) {
            $this->abortResultCache();
            ShowError($e->getMessage());
        }
    }

    protected function _getData()
    {
        if (!Loader::includeModule('iblock')) {
            throw new Exception('Не подключен модуль iblock');
        }
        if (empty($this->arParams['XML_ID'])) {
            throw new Exception('Не задан XML_ID');
        }

        $arIBlock = [];
        $rsIBlocks = IblockTable::getList([
            "filter" => ['CODE' => [IBLOCK_CODE_TEXT_BLOCK, IBLOCK_CODE_TEXT_BLOCK_EN], 'ACTIVE' => 'Y'],
            "order"  => ["ID"],
            "select" => ["ID", "CODE"],
        ]);
        while ($arItem = $rsIBlocks->fetch()) {
            $arIBlock[$arItem['ID']] = $arItem['CODE'];
        }

        if (empty($arIBlock)) {
            throw new Exception('Не найдены инфоблоки "'. IBLOCK_CODE_TEXT_BLOCK . ', '.IBLOCK_CODE_TEXT_BLOCK_EN.'"');
        }

        // выборка элемента
        $rsElements = ElementTable::getList([
            "filter" => ['IBLOCK_ID' => array_keys($arIBlock), 'CODE' => $this->arParams['XML_ID'], 'ACTIVE' => 'Y'],
            "select" => ["ID", "IBLOCK_ID", "NAME", "DETAIL_TEXT"],
        ]);
        while ($arItem = $rsElements->fetch()) {
            if (LANGUAGE_ID != 'ru' && $arIBlock[$arItem['IBLOCK_ID']] == IBLOCK_CODE_TEXT_BLOCK_EN) {
                $this->arResult['ELEMENT'] = $arItem;
            } elseif (empty($this->arResult['ELEMENT']) && $arIBlock[$arItem['IBLOCK_ID']] == IBLOCK_CODE_TEXT_BLOCK) {
                $this->arResult['ELEMENT'] = $arItem;
            }
        }

        if (!$this->arResult['ELEMENT']['ID']) {
            throw new Exception( "Элемент '{$this->arParams['XML_ID']}' не найден");
        }

        $arButtons = CIBlock::GetPanelButtons($arIBlock['ID'], $this->arResult['ELEMENT']['ID']);
        $this->arResult['EDIT_LINK'] = $arButtons['edit']['edit_element']['ACTION_URL'];
    }
}