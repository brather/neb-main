<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Legal information");
?>
<p>The Project management of the NEB corresponds to the following legislative and regulatory documents of the Russian Federation:</p>
    <ol class="b-commonlist">
        <li>The Civil Code of the Russian Federation. Part 4. Section VII. Exclusive rights to the results of intellectual activity and means of individualizing</li>
        <li>Federal law of the Russian Federation (dated July, 27th 2010) № 210-Ф3 “About organization of provision of government and municipal services”</li>
        <li>The Edict of the President of the Russian Federation (dated May, 7th 2012) № 597 “About events concerning the implementation of state social policy” – prescribes to include in the National electronic library not less than 10 per cent of published in the Russian Federation items of books annually, provide support for creating public electronic libraries in the data telecommunications network the Internet;</li>
        <li>Federal Law (dated 29.12.1994) № 78-Ф3 (edited 02.07.2013) “About librarianship”</li>
        <li>Federal Law (dated 29.12.1994) № 77-Ф3 (edited 08.06.2011) “About necessary exemplars of documents” – sets institutional spheres of responsibility for the organization of the national library-informational fund of the Russian Federation;</li>
        <li>Federal Law (dated December, 29th 2010) № 436-Ф3 “About children’s protection from information causes harm to their health and development” – provides referring informational products to one of the five categories and forbidding their spread among children depending on their age.</li>
    </ol>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>