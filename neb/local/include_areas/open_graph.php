<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * Блок метатегов для расшаривания материалов в социальных сетях
 * Специально выполнен в виде отложенной буферизированной функции, т.к. может иметь значения по-умолчанию,
 * могут отсутствовать какие-то блоки, а так же заданы кастомные
 *
 * Документация:
 * http://ogp.me/
 * https://yandex.ru/support/webmaster/open-graph/intro-open-graph.xml
 *
 * https://developers.facebook.com/docs/reference/opengraph/object-type/book/
 * https://developers.facebook.com/docs/reference/opengraph/object-type/books.book
 * https://developers.facebook.com/tools/debug/sharing/
 * https://developers.facebook.com/tools/debug/og/object/
 *
 * Пример:
 * https://www.litmir.me/bd/?b=148768
 */

global $APPLICATION;

$APPLICATION->AddBufferContent(
    function () use ($APPLICATION) {

        $sHost = URL_SCHEME . '://' . SITE_HOST; //. $_SERVER['HTTP_HOST'];
        $arMetaProps = ['title', 'keywords', 'description'];
        $arOgProps = [
            'og:title'          => 'title',
            'og:description'    => 'description',
            'og:url'            => $APPLICATION->GetCurUri(),
            'og:image'          => '/local/templates/adaptive/img/logo-socialnetworks.png',
            'og:type'           => 'website',
            'og:site_name'      => 'НЭБ.РФ - Национальная электронная библиотека',

            'book:author'       => '',
            'book:isbn'         => '',
            'book:release_date' => '',
            'book:tag'          => '',
        ];

        $sResult = "<!-- Open Graph -->\n";
        foreach ($arOgProps as $sOgProp => $sProp) {
            $sValue = $APPLICATION->GetPageProperty($sOgProp);
            if (empty($sValue)) {
                $sValue = in_array($sProp, $arMetaProps) ? $APPLICATION->GetPageProperty($sProp) : $sProp;
            }
            if ('og:title' == $sOgProp && empty($sValue)) {
                $sValue = $APPLICATION->GetTitle();
            }
            if (in_array($sOgProp, ['og:url', 'og:image']) && !empty($sValue)) {
                $sValue = $sHost . $sValue;
            }
            if (!empty($sValue)) {
                if (is_array($sValue)) {
                    foreach ($sValue as $sVal) {
                        $sResult .= "<meta property='$sOgProp' content='$sVal' />\n";
                    }
                } else {
                    $sResult .= "<meta property='$sOgProp' content='$sValue' />\n";
                }
            }
        }

        return $sResult;
    }
);