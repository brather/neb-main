<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-headernav-apps right">
	<a href="/viewers/" target='_blank' class="b-neb_viewer" title="Программа просмотра НЭБ"></a>
	<a href="http://apps.microsoft.com/windows/ru-ru/app/fcfe5ad4-7016-4647-9438-c390d5c21b59" target='_blank' class="b-winphone_img" title="Приложение для Windows Phone"></a>
	<a href="https://play.google.com/store/apps/details?id=ru.elar.neb.viewer" target='_blank' class="b-headergplay_img" title="Приложение для Android"></a>
	<a href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=944682831&mt=8" target='_blank' class="b-headerapps_img" title="Приложение для iOS"></a>						
	<!--<a href="#" class="b-headerapps_img" title="<?=Loc::getMessage('TEXT_LONG')?>"><span><?=Loc::getMessage('TEXT_NUM')?></span></a>						
	<a href="#" class="b-headerapps_lnk"><?=Loc::getMessage('TEXT_SHORT')?></a>-->
</div>