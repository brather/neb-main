<?php

/**
 * User: agolodkov
 * Date: 21.10.2016
 * Time: 16:45
 */

namespace AppBundle\DependencyInjection\Search;

interface QueryBuilderInterface
{

    public function buildQuery();

    /**
     * @param ConditionInterface $condition
     *
     * @return mixed
     */
    public function setCondition(ConditionInterface $condition);

    public function addSort($order, $by);

    public function setLimit($value);

    public function setOffset($value);

    public function addParam($name, $value);

    /**
     * @return string
     */
    public function getMethod();

    /**
     * @param string $method
     */
    public function setMethod($method);
}