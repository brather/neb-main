<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CModule::IncludeModule('iblock');
CModule::IncludeModule('nota.library');

use Nota\Library;

$sTableID = 'tbl_library_news';

$oSort = new CAdminSorting($sTableID, "NAME", "desc");
$arOrder = (strtoupper($by) === "ID"? array($by => $order): array($by => $order, "ID" => "ASC"));
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = Array(
    "find_id",
    "find_name",
);

$lAdmin->InitFilter($arFilterFields);

$arFilter = array(
    "ID" => $find_id,
    "?NAME" => $find_name,
);

TrimArr($arFilter);

if($lAdmin->EditAction())
{
    foreach($FIELDS as $ID => $arFields)
    {
        $DB->StartTransaction();
        $ID = IntVal($ID);

        if(!$lAdmin->IsUpdated($ID))
            continue;

        if(!CollectionTable::Update($ID, $arFields))
        {
            $lAdmin->AddUpdateError('Udate error', $ID);
            $DB->Rollback();
        }
        $DB->Commit();
    }
}

if ($arID = $lAdmin->GroupAction())
{
    if ($_REQUEST['action_target'] == 'selected')
    {
        $rsData = CollectionTable::getList(array('select' => array('ID'), 'filter' => $arFilter));
        while ($arRes = $rsData->fetch())
            $arID[] = $arRes['ID'];
    }

    foreach ($arID as $ID)
    {
        if(strlen($ID)<=0)
            continue;

        switch($_REQUEST['action'])
        {
            case "delete":
                $DB->StartTransaction();
                $result = CollectionTable::delete($ID);
                if (!$result->isSuccess())
                {
                    $DB->Rollback();
                    $lAdmin->AddGroupError(
                        'Ошибка' .
                            ' (ID = ' . $ID . ': ' . implode('<br>', $result->getErrorMessages()) . ')',
                        $ID
                    );
                }

                $DB->Commit();
                break;
        }
    }
}

$arHeader = array(
    array(
        'id'      => 'ID',
        'content' => "ID",
        'sort'    => 'ID',
        'default' => true
    ),
    array(
        'id'      => 'NAME',
        'content' => "Название",
        'sort'    => 'NAME',
        'default' => true
    ),
    array(
        'id'      => 'SORT',
        'content' => "Сортировка",
        'sort'    => 'SORT',
        'default' => true
    ),
);


$lAdmin->AddHeaders($arHeader);

$arSelect = $lAdmin->GetVisibleHeaderColumns();
if(!in_array('ID', $arSelect))
    $arSelect[] = 'ID';

$rsData = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 3, "PROPERTY_LIBRARY" => CNotaLibrary::getLibraryID()));

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(''));

while ($arRes = $rsData->NavNext())
{
    $row = $lAdmin->AddRow($arRes['ID'], $arRes);

    $row->AddInputField('NAME', array('size' => 50));
    $row->AddInputField('SORT', array('size' => 10));
    //$row->AddCheckField("MAIN");

    $row->AddViewField('NAME', '<a href="iblock_element_edit.php?IBLOCK_ID=3&ID='. $arRes['ID'] .'&lang='. LANGUAGE_ID .'">'. $arRes['NAME'] .'</a>');

    $row->AddActions(
        array(
            array(
                'ICON'   => 'edit',
                'TEXT'   => 'редактировать',
                'ACTION' => $lAdmin->ActionRedirect('collection_edit.php?ID='.$arRes['ID'].'&lang='.LANGUAGE_ID)
            ),
            array(
                'ICON'   => 'delete',
                'TEXT'   => 'удалить',
                "ACTION" =>"if(confirm('".GetMessageJS("NOTA_COLLECTION_DELETE_CONFIRM")."')) ".$lAdmin->ActionDoGroup($arRes['ID'], "delete"),
            )
        )
    );
}


$lAdmin->AddAdminContextMenu(
    array(
        array(
            'TEXT'  => 'Добавить новость',
            'LINK'  => 'iblock_element_edit.php?IBLOCK_ID=3&type=news&ID=0&lang= ' . LANGUAGE_ID,
            'TITLE' => 'Добавить новость',
            'ICON'  => 'btn_new',
        )
    )
);

$lAdmin->AddGroupActionTable(array('delete' => 'удалить'));

$lAdmin->CheckListMode();
$APPLICATION->SetTitle('Новости библиотеки');

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';
?>
<form method="GET" action="" name="find_form">
    <input type="hidden" name="filter" value="Y">
    <?
    $oFilter = new CAdminFilter(
        $sTableID."_filter",
        array(
            "ID",
        )
    );

    $oFilter->Begin();
    ?>
    <tr>
        <td><b>Название</b></td>
        <td><input type="text" name="find_name" value="<?=htmlspecialcharsbx($find_name)?>" size="40"></td>
    </tr>
    <tr>
        <td>ID:</td>
        <td><input type="text" name="find_id" value="<?=htmlspecialcharsbx($find_id)?>" size="15"></td>
    </tr>
    <?
    $oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPageParam(), "form"=>"find_form"));
    $oFilter->End();
    ?>
</form>
<?
$lAdmin->DisplayList();
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
?>
