<?php
define('STOP_STATISTICS', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Neb\Main\Helper\MainHelper;

Loc::loadLanguageFile(__FILE__);

$context = Application::getInstance()->getContext();
$arServer = $context->getServer()->toArray();
$arRequest = $context->getRequest()->toArray();

?>
<div class="container-fluid">
    <div class="row"><hr></div>
    <div class="row">
        <?$obUser = new nebUser();
        if (!$obUser->IsAuthorized()) {?>
            <h2><?= Loc::getMessage('DOWNLOAD_BOOK_NOT_AUTHORIZED') ?></h2>
        <?} else {?>
            <?$arGroups = $obUser->getUserGroups();
            if(!in_array(UGROUP_OPERATOR_CODE, $arGroups) && !in_array(UGROUP_LIB_CODE_ADMIN, $arGroups)){?>
                <h2><?= Loc::getMessage('DOWNLOAD_BOOK_NOT_PERMISSION') ?></h2>
            <?} else {?>
                <?if(empty($arRequest['path'])){?>
                    <h2><?= Loc::getMessage('DOWNLOAD_BOOK_NOT_PATH') ?></h2>
                <?} else {?>
                    <?
                    MainHelper::clearBuffer();
                    $filename = $arRequest['path'];
                    $arPathInfo = pathinfo($filename);
                    header("Content-length:" . filesize($filename));
                    header('Content-Type: application/' . $arPathInfo['extension']);
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . $arPathInfo['basename'] . '"');
                    header('Content-Transfer-Encoding: binary');
                    readfile($filename);
                    exit();?>
                <?}?>
            <?}?>
        <?}?>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>