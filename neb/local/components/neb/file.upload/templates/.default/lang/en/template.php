<?php
$MESS['FILE_UPLOAD_T_TITLE'] = ' Upload file';
$MESS['FILE_UPLOAD_T_SET_FILE'] = 'Select a file';
$MESS['FILE_UPLOAD_T_TITLE_SHORT'] = 'Upload';
$MESS['FILE_UPLOAD_T_DELETE_FILE'] = ' Delete';
$MESS['FILE_UPLOAD_T_CONFIRM_TITLE'] = 'Delete?';
$MESS['FILE_UPLOAD_T_CONFIRM_YES'] = 'Delete';
$MESS['FILE_UPLOAD_T_CONFIRM_TEXT'] = 'Are you sure you want to delete this pdf?';