<?
$MESS['BOOKMARKS_SORT'] = 'Сортировать';
$MESS['BOOKMARKS_SORT_AUTHOR'] = 'По автору';
$MESS['BOOKMARKS_SORT_NAME'] = 'По названию';
$MESS['BOOKMARKS_SORT_DATE'] = 'По дате';
$MESS['BOOKMARKS_SHOW'] = 'Показать';
$MESS['BOOKMARKS_SETTINGS'] = 'Настройки';
$MESS['BOOKMARKS_REMOVE'] = 'Удалить';
$MESS['BOOKMARKS_FROM_BOOKMARKS'] = 'из Моих закладок';
$MESS['BOOKMARKS_AUTHOR'] = 'Автор';
$MESS['BOOKMARKS_BOOK'] = 'Книга';
$MESS['BOOKMARKS_ON_PAGE'] = 'Читать страницу';
$MESS['BOOKMARKS_MY_COLLECTIONS'] = 'Мои подборки';
$MESS['BOOKMARKS_CREATE_COLLECTION'] = 'cоздать подборку';
$MESS['PROFILE_MU_NUMBER_LC'] = 'Номер ЕЭЧБ';
?>