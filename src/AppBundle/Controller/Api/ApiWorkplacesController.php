<?php
/**
 * User: agolodkov
 * Date: 20.10.2016
 * Time: 15:26
 */

namespace AppBundle\Controller\Api;

use AppBundle\DependencyInjection\WorkplaceHelper;
use BitrixBundle\Annotations\IncludeBitrix;
use BitrixBundle\DependencyInjection\BitrixHelper;
use FOS\RestBundle\View\View;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;


/**
 * Class ApiWorkplacesController
 *
 * @DI\Service("neb.api.workplaces", scope="request")
 */
class ApiWorkplacesController
{
    /**
     * @DI\Inject("bitrix_helper")
     * @var BitrixHelper
     */
    public $bitrix;

    /**
     * @DI\Inject("request")
     * @var Request
     */
    public $request;

    /**
     * @DI\Inject("neb.workplace_helper")
     * @var WorkplaceHelper
     */
    public $workplace;

    /**
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return array
     */
    public function findWorkplacesAction()
    {
        $entities = $this->workplace->findWorkplaces($this->request->query->all());

        return View::create(['entities' => $entities]);
    }
}