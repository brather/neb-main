<?php
$MESS['UPLOAD_SCAN_WITH_RESIDENCE'] = 'Upload a scan reversal passport with residence permit';
$MESS['UPLOAD_IMAGE'] = 'Upload image';
$MESS['DRAG_INTO_FIELD'] = 'or drag it into this field';
$MESS['UPLOADING'] = 'Uploading';
$MESS['ERROR_UPLOAD_SCAN'] = 'Upload scan passports';
$MESS['UPLOAD_RIGHTHOLDER_DOCUMENTS'] = 'Upload the document proving the possession of exclusive rights: the agreement on transfer (alienation)<br>
the exclusive right, the license agreement, certificate of inheritance, any other document.';
