<?php
/**
 * User: agolodkov
 * Date: 13.08.2015
 * Time: 13:46
 */

namespace Neb\Main\Stat;


use Neb\Main\Csv;

/**
 * Class ExaleadQueryReport
 *
 * @package Neb\Main\Stat
 */
class ExaleadQueryReport
{

    /**
     * @var array
     */
    private $_files = array();

    /**
     * @var Csv
     */
    private $_csvManager;

    /**
     * @var string
     */
    private $_filePath;

    /**
     * @var bool|string
     */
    private $_date;


    /**
     * @param array $options
     *
     * @throws \Exception
     */
    public function __construct($options)
    {
        foreach ($options as $optionName => $optionValue) {
            $optionName = '_' . $optionName;
            if (property_exists($this, $optionName)) {
                $this->$optionName = $optionValue;
            }
        }
        if (null === $this->_date) {
            $this->_date = ConvertTimeStamp(false, 'SHORT');
        }
        if (!is_dir($this->_filePath)) {
            throw new \Exception('not found directory');
        }
        if (true === ($handle = opendir($this->_filePath))) {
            throw new \Exception('cannot open directory');
        }

        $dateTimeStamp = MakeTimeStamp($this->_date);
        while (false !== ($entry = readdir($handle))) {
            if (preg_match(
                '/\.csv.' . date('Ymd', $dateTimeStamp) . '.[\d]{5}/i', $entry
            )) {
                $this->_files[] = $this->_filePath . '/' . $entry;
            }
        }
        closedir($handle);
        reset($this->_files);
        if (!empty($this->_files)) {
            $this->_csvManager = new Csv(
                current($this->_files)
            );
        }
    }

    /**
     * @return mixed
     */
    public function getNextFile()
    {
        return next($this->_files);
    }


    /**
     * @return array|bool
     */
    public function getNextQuery()
    {
        if (!$this->_csvManager instanceof Csv) {
            return false;
        }
        if (false === ($row = $this->_csvManager->getNext())) {
            if (false === ($file = $this->getNextFile())) {
                return false;
            }
            $this->_csvManager = new Csv($file);
            $row = $this->_csvManager->getNext();
        }

        return $row;
    }

    /**
     * @param null|int $libraryId
     *
     * @return array
     */
    public function countByLibrary($libraryId = null)
    {
        $result = array();
        while ($row = $this->getNextQuery()) {
            $queryLibraryId = 0;
            if (preg_match(
                '/idlibrary:([\d]+)/',
                $row['query_querystring'],
                $matches
            )) {
                $queryLibraryId = intval($matches[1]);
            }
            if (null !== $libraryId && $queryLibraryId !== $libraryId) {
                continue;
            }
            if (!isset($result[$queryLibraryId])) {
                $result[$queryLibraryId] = array();
            }
            $date = ConvertTimeStamp(strtotime($row['#timestamp']), 'SHORT');
            if (!isset($result[$queryLibraryId][$date])) {
                $result[$queryLibraryId][$date] = 0;
            }
            $result[$queryLibraryId][$date]++;
        }

        return $result;
    }

    /**
     * @param int $id
     *
     * @return array
     * @throws \Exception
     */
    public function countByLibraryIblock($id = 0)
    {
        if (!\CModule::IncludeModule('iblock')) {
            throw new \Exception('module iblock not installed');
        }
        if ($id > 0) {
            $element = new \CIBlockElement();
            $result = $element->GetList(
                array(),
                array('ID' => $id),
                array('PROPERTY_LIBRARY_LINK')
            );
            $libraryId = null;
            if (!$result = $result->Fetch()) {
                $result = array();
            }
            if (isset($result['PROPERTY_LIBRARY_LINK_VALUE'])) {
                $libraryId = (integer)$result['PROPERTY_LIBRARY_LINK_VALUE'];
            }
            if (null === $libraryId) {
                return array();
            }
        } else {
            $libraryId = $id;
        }

        $data = $this->countByLibrary($libraryId);
        if ($id != $libraryId && isset($data[$libraryId])) {
            $data[$id] = $data[$libraryId];
            unset($data[$libraryId]);
        }

        return $data;
    }

    /**
     * @param string $date
     */
    public function deleteDayFiles($date = null)
    {
        if (null === $date) {
            $date = $this->_date;
        }
        $date = MakeTimeStamp($date);
        foreach ($this->_files as $filePath) {
            if (preg_match(
                '/\.csv.' . date('Ymd', $date) . '.[\d]{5}/i', $filePath
            )) {
                unlink($filePath);
            }
        }
    }
}