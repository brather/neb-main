<?php
/**
 * @global CMain $APPLICATION
 *
 * @param array  $arParams
 * @param array  $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$arResult['isESIAUser'] = $arResult['arUser']['UF_REGISTER_TYPE']
    == nebUser::REGISTER_TYPE_ESIA;
$arResult['arESIAFields'] = array();
if ($isESIAUser) {
    $arResult['arESIAFields'] = explode(
        '/', $arResult['arUser']['UF_ESIA_FIELDS']
    );
}
$arResult['USER_ROLE'] = nebUser::getCurrent()->getRole();