<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arParams['TYPE']))
    return false;

if ($_REQUEST['reloadRightCollection'] == 'Y') $APPLICATION->RestartBuffer();

\CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CModule::IncludeModule("nota.userdata");
use Nota\UserData\Books;

$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_COLLECTIONS_USERS)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$hlblockLinks = HL\HighloadBlockTable::getById(HIBLOCK_COLLECTIONS_LINKS_USERS)->fetch();
$entitykLinks = HL\HighloadBlockTable::compileEntity($hlblockLinks);
$entity_data_classkLinks = $entitykLinks->getDataClass();

$rsData = $entity_data_class::getList(array(
    "runtime" => array(
        'link' => array(
            "data_type" => $entity_data_classkLinks,
            'reference' => array('=this.ID' => 'ref.UF_COLLECTION_ID',),
            'join_type' => "RIGHT"
        ),
        'cnt' => array(
            "data_type" => "integer",
            "expression" => array("count(%s)", "link.ID")
        )
    ),
    "select" => array("ID", "UF_NAME", 'cnt'),
    "filter" => array('UF_UID' => $USER->GetID()),
    "group" => array('UF_NAME'),
    "order" => array('UF_SORT' => 'ASC', 'UF_DATE_ADD' => 'ASC')
));

$arResult = array();
$arIds = array();
while ($arData = $rsData->Fetch()) {
    if (!empty($arParams['COLLECTION_URL']))
        $arData['URL'] = str_replace('#COLLECTION_ID#', $arData['ID'], $arParams['COLLECTION_URL']);

    $arResult['COLLECTIONS'][] = $arData;
    $arIds[] = $arData['ID'];
}

/*
Получаем пустые подборки
*/
$rsData = $entity_data_class::getList(array(
    "select" => array("ID", "UF_NAME"),
    "filter" => array('UF_UID' => $USER->GetID(), '!ID' => $arIds),
    "order" => array('UF_SORT' => 'ASC', 'UF_DATE_ADD' => 'ASC')
));

while ($arData = $rsData->Fetch()) {
    if (!empty($arParams['COLLECTION_URL']))
        $arData['URL'] = str_replace('#COLLECTION_ID#', $arData['ID'], $arParams['COLLECTION_URL']);

    $arResult['COLLECTIONS'][] = $arData;
}

$arResult['ALL_BOOKS_CNT'] = Books::getAllCnt();
$arResult['READING_BOOKS_CNT'] = Books::getReadingCnt();
$arResult['READ_BOOKS_CNT'] = Books::getReadCnt();

$this->IncludeComponentTemplate();
