<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\NotaExt\Iblock\Element;

/**
 * Class LibraryRightCounterComponent
 */
class LibraryRightCounterComponent extends CBitrixComponent
{
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        if (!isset($arParams["CACHE_TIME"]))
            $arParams["CACHE_TIME"] = 3600;

        $arParams['LIBRARY_ID'] = intval($arParams['LIBRARY_ID']);
        $arParams['COLLECTION_URL'] = trim($arParams['COLLECTION_URL']);

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        if (empty($this->arParams['LIBRARY_ID']) || empty($this->arParams['IBLOCK_ID']))
            return ;

        $this->arResult = Element::getByID(
            $this->arParams['LIBRARY_ID'],
            array(
                'PREVIEW_PICTURE',
                'PROPERTY_PUBLICATIONS',
                'PROPERTY_PUBLICATIONS_DIGITIZED',
                'PROPERTY_COLLECTIONS',
                'PROPERTY_USERS',
                'PROPERTY_VIEWS',
                'skip_other'
            )
        );

        if (!empty($this->arResult['PREVIEW_PICTURE'])) {
            $this->arResult['PREVIEW_PICTURE'] = \CFile::ResizeImageGet(
                $this->arResult['PREVIEW_PICTURE'], ['width' => 150, 'height' => 150],
                BX_RESIZE_IMAGE_PROPORTIONAL, true
            );
        }

        $this->includeComponentTemplate();
    }
}
