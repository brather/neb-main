<?
/**
 * Добавление/изменение произведения в издании в открытом просмотровщике
 */
define('STOP_STATISTICS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\RslViewer;

global $USER;
if (!$USER->IsAuthorized() || !Loader::includeModule('nota.exalead'))
    exit();

$obUser       = new nebUser($USER->GetID());
$arToken      = $obUser->getToken();
$arUserGroups = $obUser->getUserGroups();

if (!in_array(UGROUP_MARKUP_EDITOR, $arUserGroups) && !in_array(UGROUP_MARKUP_APPROVER, $arUserGroups))
    exit();

$sBookId = trim($_REQUEST['book_id']);
$sMethod = trim(strtolower($_REQUEST['action'])) == 'update' ? 'PUT' : 'POST'; // метод
$arParams = ['author', 'title', 'articleAccess', 'startPage', 'finishPage', 'position' , 'rangePage'];
$arRequestFields = ['token' => $arToken['UF_TOKEN']];
foreach ($arParams as $sParam) {
    $sField = trim($_REQUEST[$sParam]);
    if (isset($sField))
        $arRequestFields[$sParam] = $sField;
}
$obRslViewer = new RslViewer($sBookId);
$arResult    = $obRslViewer->addMarkup($sMethod, $arRequestFields);
if (!empty($arResult))
    $arResult['action'] = 'update';

MainHelper::showJson($arResult);