<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<textarea id="<?= $arResult["ID"] ?>"
          class="<?= $arResult["CLASS"] ?>"
          name="<?= $arResult["NAME"] ?>"
          placeholder="<?= $arResult["PLACEHOLDER"]  ?>"
          style="width:<?= $arResult["WIDTH"] ?>; height: <?= $arResult["HEIGHT"] ?>;">
    <?= $arResult["TEXT"] ?>
</textarea>
<?= $arResult["SCRIPT"] ?>
