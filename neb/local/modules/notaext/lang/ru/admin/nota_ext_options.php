<?
$MESS ['MAIN_TITLE'] = "Настройки расширения";
$MESS ['MAIN_SELECT'] = "Выбрать";
$MESS ['MAIN_HINT_RESTORE_DEFAULTS'] = "Установить значения по умолчанию";
$MESS ['MAIN_HINT_RESTORE_DEFAULTS_WARNING'] = "Внимание! Все настройки будут перезаписаны значениями по умолчанию. Продолжить?";
$MESS ['MAIN_TAB_SET'] = "Настройки";
$MESS ['MAIN_TAB_TITLE_SET'] = "Настройка параметров расширения";
