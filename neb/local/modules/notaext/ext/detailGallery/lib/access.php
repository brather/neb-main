<?
namespace Bitrix\NotaExt\Ext\DetailGallery;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

/*
@class Access Проверка прав доступа для ИБ Указанного в настройках для хранения фотографий
*/

class Access
{
    static public function check()
    {
        $ib_save = Params::getParam('save_ib');
        if (empty($ib_save))
            return false;

        $iblock_permission = \CIBlock::GetPermission($ib_save);
        if ($iblock_permission >= 'W')
            return true;
        else
            return false;
    }
}
