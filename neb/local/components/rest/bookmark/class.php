<?php

CBitrixComponent::includeComponentClass("rest:rest.server");

use \Bitrix\Main\Loader,
	\Bitrix\Main\UserTable,
	\Nota\UserData\Bookmarks;

/**
 * Компонент организует API по приёму, передаче, удалению закладок
 *
 * Class RestBookmarks
 */
class RestBookmarks extends RestServer
{
	protected $token, $bookID, $type, $numPage, $numWord, $bookmarkID, $uid;

	/**
	 * Т.к. через ЧПУ мы не обработаем GET, PUT, DELETE - обрабатываем их тут.
	 * Но помним уже, что в параметрах у нас стоит ограничение, и другие REQUEST_METHOD не пройдут.
	 */
	public function processRequest($token = null, $bookID = null, $type = null, $numPage = null, $numWord = null, $ID = null)
	{
		if (!empty($token))
			$this->token = trim($token);

		if (!empty($bookID))
			$this->bookID = trim($bookID);

		if (!empty($type))
			$this->type = trim($type);

		if (!empty($numPage))
			$this->numPage = intval(trim($numPage));

		if (!empty($numWord))
			$this->numWord = trim(strval($numWord));

		if (!empty($ID))
			$this->bookmarkID = $ID;

		// Токен нам нужен везде - так что проверить его можно и тут.
		if ( strlen($this->token) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', ['#PARAM#' => 'token']));

		if (!Loader::includeModule("nota.userdata"))
			$this->restError(404, 'Module "nota.userdata" not installed!');

		$this->checkUser();

		switch ( $_SERVER['REQUEST_METHOD'] )
		{
			case 'PUT':
				return $this->PUTBookmarks();
				break;
			case 'GET':
				return $this->GETBookmarks();
				break;
			case 'DELETE':
				return $this->DELETEBookmarks();
				break;
		}
	}

	/**
	 * Проверка токена пользователя
	 */
	protected function checkUser() {

		$arUser = UserTable::getList([
			'filter' => ['ACTIVE' => 'Y', 'UF_TOKEN' => $this->token],
			'select' => ['ID', 'LOGIN']
		])->fetch();

		if ( empty($arUser['ID']) || $arUser['LOGIN'] == nebUser::ANONYMOUS_LOGIN )
			$this->restError(404, GetMessage('USER_NOT_FOUND'));
		else
			$this->uid = $arUser['ID'];
	}

	/**
	 * Находит UID по token
	 * Добавляет в избранное указанную книгу
	 */
	protected function PUTBookmarks()
	{
		if (empty($this->bookID))
			$this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'book_id')));

		if ($this->numPage <= 0 && empty($this->numWord))
			$this->restError(500, 'Должен быть заполнен один из параметров: num_page или num_word!');

		$resultID = Bookmarks::add($this->uid, $this->bookID, $this->numPage, $this->numWord);

		$this->arResult = ['resultID' => intval($resultID)];

		return 200;
	}

	/**
	 * Отдаёт список всех книг из избранного
	 */
	protected function GETBookmarks()
	{
		$result = Bookmarks::getBookmarkList($this->uid, $this->bookID, $this->type, $this->numPage, $this->numWord);
		if($result === false)
			$result = (object) array();

		$this->arResult = $result;

		return 200;
	}

	/**
	 * Удаляет из избранного по token
	 */
	protected function DELETEBookmarks()
	{
		if (empty($this->bookmarkID))
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'ID')));

		$result = false;

		if (is_array($this->bookmarkID))
			foreach ($this->bookmarkID as $id)
				$result = Bookmarks::deleteWithTest($id, $this->uid);
		// Удаляем с предварительным сопоставлением пользователю
		else
			$result = Bookmarks::deleteWithTest($this->bookmarkID, $this->uid);

		if (!$result)
			$this->restError(404, GetMessage('NOT_FOUND'));

		$this->arResult = true;

		return 200;
	}
}