<?php
/**
 * User: agolodkov
 * Date: 03.10.2016
 * Time: 18:14
 */

namespace BitrixBundle\Traits;


use Doctrine\Common\Annotations\CachedReader;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait AnnotationReaderTrait
{

    /**
     * @var CachedReader
     */
    private $_reader;

    /**
     * @var ControllerNameParser
     */
    private $_parser;

    /**
     * @var ContainerInterface
     */
    private $_container;

    /**
     * @var array
     */
    private $_methodAnnotations = [];

    /**
     * @param string $controllerMethod
     *
     * @return mixed
     */
    public function getControllerMethodAnnotations($controllerMethod)
    {
        $this->_checkOwnServices();
        if (!isset($this->_methodAnnotations[$controllerMethod])) {
            $actionMethod = null;
            if (3 === count($parts = explode(':', $controllerMethod))) {
                if (false === strpos($controllerMethod, '::') && false !== strpos($controllerMethod, ':')) {
                    $controllerMethod = $this->_parser->parse($controllerMethod);
                }
                $controller = explode('::', $controllerMethod);
                $actionMethod = new \ReflectionMethod($controller[0], $controller[1]);
            } else {
                $controller = explode(':', $controllerMethod);
                if (2 === count($controller)) {
                    $controllerService = $this->_container->get($controller[0]);
                    $actionMethod = new \ReflectionMethod(get_class($controllerService), $controller[1]);
                }
            }

            if ($actionMethod instanceof \ReflectionMethod) {
                $this->_methodAnnotations[$controllerMethod] = $this->_reader->getMethodAnnotations($actionMethod);
            }
        }

        return $this->_methodAnnotations[$controllerMethod];
    }

    /**
     * @throws \Exception
     */
    private function _checkOwnServices()
    {
        if (!$this->_reader instanceof CachedReader) {
            throw new \Exception('CachedReader not set');
        }
        if (!$this->_parser instanceof ControllerNameParser) {
            throw new \Exception('ControllerNameParser not set');
        }
        if (!$this->_container instanceof ContainerInterface) {
            throw new \Exception('ServiceContainer not set');
        }
    }
}