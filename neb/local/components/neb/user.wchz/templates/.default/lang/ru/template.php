<?php
$MESS['LIBRARY_LIST_FIND_CLOSEST'] = 'Найти ближайшую библиотеку в ';
$MESS['LIBRARY_LIST_FIND_BUTTON'] = 'Найти';
$MESS['LIBRARY_LIST_ENTER_ADDRESS'] = 'Введите адрес или название библиотеки';
$MESS['LIBRARY_LIST_CHANGE_CITY'] = '(изменить)';
$MESS['LIBRARY_LIST_LIBS'] = 'Библиотеки';
$MESS['LIBRARY_LIST_SHOW_ON_MAP'] = 'Показать на карте';
$MESS['LIBRARY_LIST_YOUR_CITY'] = 'Ваш город';
$MESS['LIBRARY_LIST_YES'] = 'Да, верно';
$MESS['LIBRARY_LIST_EDIT'] = 'Изменить';
$MESS['LIBRARY_LIST_ENTER_CITY'] = 'Введите название города';
$MESS['LIBRARY_LIST_CHOOSE'] = 'Быстрый выбор';
$MESS['LIBRARY_LIST_FAST_CHOOSE'] = 'Быстрый выбор';
$MESS['LIBRARY_LIST_ACCEPT'] = 'Принять';

//попап на карте
$MESS['LIBRARY_LIST_POPUP_ADDRESS'] = 'Адрес';
$MESS['LIBRARY_LIST_POPUP_WORK'] = 'График работы';
$MESS['LIBRARY_LIST_POPUP_USER'] = 'Участник';
$MESS['LIBRARY_LIST_POPUP_URL'] = 'перейти в библиотеку';
$MESS['LIBRARY_LIST_ADDRESS'] = 'Адрес';
$MESS['LIBRARY_LIST_MEMBER_NEL'] = 'Участник проекта НЭБ';