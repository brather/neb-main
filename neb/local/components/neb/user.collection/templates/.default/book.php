<li class="search-result__content-list-kind clearfix" id="<?=$arItem['ID']?>" data-favorite-item data-wtf>
    <button class="btn btn-default button-remove" data-id="<?=$arItem['ID']?>" data-item-type="books" data-delete-favorite-edition title="Удалить книгу" data-modal-title="Удалить книгу?" data-button-title="Удалить">&nbsp;</button>
    <div class="search-result__content-list-sidebar">
        <span class="search-result__content-list-number"><?php echo ($key+1)+($arResult['iNumPage']-1)*15 ; ?>.</span>
        <span class="search-result__content-list-status-icon search-result__content-list-status-icon--open"></span>
    </div>


    <div class="search-result__content-main">


        <div class="search-result__content-main-links">
            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay search-result__content-link-title" data-width="955" ><?=$arItem['title']?></a>
        </div>
        <p class="search-result__content-link-info">
            <?
            if(!empty($arItem['authorbook']))
            {
                ?><a title="<?=GetMessage("FAVORITES_AUTHOR");?>" href="/search/?q=&f_field[authorbook][]=<?=urlencode($arItem['authorbook'])?>"><?=rtrim($arItem['authorbook'])?></a><?
            }
            if(!empty($arItem['year']))
            {
                ?>, <a title="<?=GetMessage("FAVORITES_YEAR_PUBLICATION");?>" href="<?=(SITE_TEMPLATE_ID=="special")?"/special/search/":"/search/";?>?f_publishyear=<?=$arItem['year'].'&'.DeleteParam(array('f_authorbook', 'f_publishyear', 'f_publisher', 'dop_filter'))?>"><?=$arItem['year']?></a><?
            }
            if(intval($arItem['countpages']) > 0)
            {
                ?>, <span title="<?=GetMessage("FAVORITES_NUMBER_OF_PAGES");?>"><?=$arItem['countpages']?></span><?
            }
            if(!empty($arItem['publisher']))
            {
                ?>, <a title="<?=GetMessage("FAVORITES_PUBLISHER");?>" href="<?=(SITE_TEMPLATE_ID=="special")?"/special/search/":"/search/";?>
                ?f_field[publisher]=f/publisher/<?=urlencode(mb_strtolower(trim($arItem['publisher']))).'&'.DeleteParam(['f_field', 'dop_filter'])?>"><?=$arItem['publisher']?></a><?
            }
            ?>

        </p>

        <p><?=$arItem['text']?></p>

        <div class="search-result__my-bar" data-list-widget>
            <a 
                class="search-result__my-favorites" 
                href="#" 
                data-list-toggler
                data-list-src="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>"
                data-edition-id="<?=urlencode($arItem['id'])?>"
            >
                <?=GetMessage("FAVORITES_IN_MY_COLLECTIONS");?>
            </a>            

            <div class="my-favorites-content" data-list-wrapper>
                ...
            </div>
        </div>

    </div>
</li><!-- /.b-result-docitem -->