<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? $APPLICATION->SetTitle(""); ?>

<div class="container v-padding">
    <div class="button-bar" role="toolbar">
    	<a href="<?=$APPLICATION->GetCurDir()?>" class="btn btn-default">Вернуться на главную</a>
    </div>
    <form>
	    <dl class="form-dl">
	    	<dt><label class="for-lg" for="searchrub" aria-hidden="true" id="searchrublabel">Раздел для поиска</label></dt>
	    	<dd>
	    		<select tabindex="4" class="form-control input-lg" aria-labelledby="searchrublabel" id="searchrub">
	    			<option selected>Библиотеки</option>
	    			<option>Музеи</option>
	    			<option>Архивы</option>
	    		</select>
	    	</dd>
	    	<dt><label class="for-lg" for="searchtype" aria-hidden="true" id="searchtypelabel">Вид поиска</label></dt>
	    	<dd>
	    		<select tabindex="3" class="form-control input-lg" id="searchtype" aria-labelledby="searchtypelabel">
	    			<option>Одной строкой</option>
	    			<option selected>Расширенный поиск</option>
	    		</select>
	    	</dd>
	    	<dt><label for="author" id="authorlabel" class="for-lg">Автор</label></dt>
	    	<dd>
	    		<input tabindex="2" type="text" class="form-control input-lg" id="author" aria-labelledby="authorlabel" data-placeholder="Дмитриев Николай"/>
	    	</dd>
	    	<dt><label for="title" class="for-lg" id="titlelabel">Название</label></dt>
	    	<dd>
	    		<input tabindex="1" type="text" class="form-control input-lg" id="title" aria-labelledby="titlelabel" data-placeholder="Портрет императора Александра III"/>
	    	</dd>
	    	<dt><label>Доступ к изданиям</label></dt>
	    	<dd>
	    		<label class="for" for="allres">
	    			<input tabindex="5" type="radio" name="seqtype" value="all" checked="true" id="allres" aria-labelledby="allreslabel"/>
	    			<span class="lbl">
	    				<span aria-hidden="true">Все</span>
	    				<span class="screen-hide" id="allreslabel">Доступ ко всем изданиям</span>
	    			</span>
	    		</label>
	    		<label class="for" for="openedres">
	    			<input tabindex="6" type="radio" name="seqtype" value="opened" id="openedres" aria-labelledby="openedrelabel"/>
	    			<span class="lbl">
	    				<span aria-hidden="true">Открытые</span>
	    				<span class="screen-hide" id="openedrelabel">Доступ к открытым изданиям</span>
	    			</span>
	    		</label>
	    		<label class="for" for="closedres">
	    			<input tabindex="7" type="radio" name="seqtype" value="closed" id="closedres" aria-labeledby="closedreslabel"/>
	    			<span class="lbl">
	    				<span aria-hidden="true">Охраняемые авторским правом</span>
	    				<span class="screen-hide" id="closedreslabel">Доступ к изданиям, охраняемым авторским правом</span>
	    			</span>
	    		</label>
	    	</dd>
	    	<dt><label>Область поиска</label></dt>
	    	<dd>
	    		<label class="for" for="scannedmedia">
	    			<input tabindex="8" type="radio" name="mediatype" value="scanned" checked="true" id="scannedmedia" aria-labelledby="scannedmedialabel"/>
	    			<span class="lbl" id="scannedmedialabel">Издания с электронной копией</span>
	    		</label>
	    		<label class="for" for="printedmedia">
	    			<input tabindex="9" type="radio" name="mediatype" value="matherial" id="printedmedia" aria-labelledby="printedmedialabel"/>
	    			<span class="lbl" id="printedmedialabel">Каталог печатных изданий</span>
	    		</label>
	    	</dd>
	    	<dt><label class="for-lg" aria-hidden="true">Дата публикации</label></dt>
	    	<dd>
	    		<label class="for-lg" for="pubfrom" id="pubfromlabel">
	    			<span class="screen-hide">Дата публикации, </span>!от<span class="screen-hide">,</span>
	    		</label>
	    		<input tabindex="10" type="text" class="form-control input-lg" id="pubfrom" style="display: inline-block; width: 5em;" value="900" aria-labelledby="pubfromlabel"/>
	    		<label class="for-lg" for="pubto" id="pubtolabel">
	    			<span class="screen-hide">Дата публикации, </span>до<span class="screen-hide">,</span>
	    		</label>
	    		<input tabindex="11" type="text" class="form-control input-lg" id="pubto" style="display: inline-block; width: 5em;" value="2016" aria-labelledby="pubtolabel"/>
	    		<span class="instead-input-lg" style="margin-left: 1ex;" aria-hidden="true">года</span>
	    	</dd>
	    	<dt><label for="pubhouse" id="pubhouselabel" class="for-lg">Издательство</label></dt>
	    	<dd>
	    		<input tabindex="12" type="text" class="form-control input-lg" id="pubhouse" data-labelledby="pubhouselabel" data-placeholder="Российская государственная библиотека"/>
	    	</dd>
	    	<dt><label for="pubplace" id="pubplacelabel" class="for-lg">Место издания</label></dt>
	    	<dd>
	    		<input tabindex="13" type="text" class="form-control input-lg" id="pubplace" data-labelledby="pubplacelabel" data-placeholder="Москва"/>
	    	</dd>
	    	<dt><label for="biblio" id="bibliolabel" class="for-lg">Библиотека</label></dt>
	    	<dd>
	    		<input tabindex="14" type="text" class="form-control input-lg" id="biblio" data-labelledby="bibliolabel" data-placeholder="РГБ"/>
	    	</dd>
	    	<dt><label for="isbn" id="isbnlabel" class="for-lg">ISBN</label></dt>
	    	<dd>	
	    		<input tabindex="15" type="text" class="form-control input-lg" id="isbn" data-labelledby="isbnlabel" data-placeholder="226611156"/>
	    	</dd>
	    	<dt><label for="bbk" id="bbklabel" class="for-lg">Код ББК</label></dt>
	    	<dd>
	    		<input tabindex="16" type="text" class="form-control input-lg" id="bbk" data-labelledby="bbklabel" data-placeholder="83.3Р1"/>
	    	</dd>
	    	<dt><label for="udk" id="udklabel" class="for-lg">Код УД<span class="screen-hide">э</span>К</label></dt>
	    	<dd>
	    		<input tabindex="17" type="text" class="form-control input-lg" id="udk" data-labelledby="udklabel" data-placeholder="811.14"/>
	    	</dd>
	    	<hr/>
	    	<dt><label class="for-lg">Рубрики</label></dt>
	    	<dd>
	    		<a  tabindex="18" class="instead-input-lg" href="#">Выбрать рубрики</a>
	    	</dd>
	    	<hr/>
	    </dl>
	    <div class="button-bar" role="toolbar">
	    	<button tabindex="19" class="btn btn-primary">Найти</button><br/>
	    	<button tabindex="20" class="btn btn-default" type="reset">Очистить поиск</button>
	    </div>
	</form>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>