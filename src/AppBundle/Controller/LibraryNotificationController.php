<?php
/**
 * User: agolodkov
 * Date: 07.09.2016
 * Time: 12:07
 */

namespace AppBundle\Controller;


use AppBundle\Controller\Api\ApiLibraryNotificationController;
use AppBundle\DependencyInjection\BitrixMainPageNavigation;
use AppBundle\DependencyInjection\LibraryHelper;
use AppBundle\DependencyInjection\NebViewHelper;
use AppBundle\Security\UserProvider;
use AppBundle\Security\User;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use BitrixBundle\Annotations\Template as BitrixTemplate;
use Symfony\Component\Routing\Router;

class LibraryNotificationController
{

    /**
     * @DI\Inject("app.api.library_notification")
     * @var ApiLibraryNotificationController
     */
    public $api;

    /**
     * @DI\Inject("neb.library")
     * @var LibraryHelper
     */
    public $library;

    /**
     * @DI\Inject("request")
     * @var Request
     */
    public $request;

    /**
     * @DI\Inject("serializer")
     * @var Serializer
     */
    public $serializer;

    /**
     * @DI\Inject("neb.user_provider")
     * @var UserProvider
     */
    public $userProvider;

    /**
     * @DI\Inject("router")
     * @var Router
     */
    public $router;

    /**
     * @DI\Inject("neb.bitrix_main_pagenavigation")
     * @var BitrixMainPageNavigation
     */
    public $bitrixPager;


    /**
     * @BitrixTemplate("AppBundle:library-notifications:posting-create.html.php")
     *
     * @return array
     */
    public function createFormAction()
    {
        ob_start();
        \CFileMan::AddHTMLEditorFrame('body', '', 'text_type', 'html', ['width' => '100%']);
        $editor = ob_get_clean();
        /** @var User $user */
        $user = $this->userProvider->getCurrentUser();


        return [
            'query'       => $this->request->query->all(),
            'request'     => $this->request->request,
            'url'         => $this->_getUrls(),
            'htmlEditor'  => $editor,
            'libraries'   => $this->_getLibraries($this->request->query->get('query')),
            'senderEmail' => $user->getEmail(),
        ];
    }

    private function _getUrls()
    {
        $token = $this->userProvider->getCurrentUser()->getToken();

        return [
            'send'       => '/profile/notification/',
            'history'    => '/profile/notification/history/',
            'export'     => '/api/library-notification/history/export/?'
                . http_build_query($this->request->query->all())
                . '&token=' . $token,
            'fileUpload' => $this->router->generate('api_file_upload'),
        ];
    }

    private function _getLibraries($libraryName = '')
    {
        $libraryEmails = $this->request->query->get('library_email');
        if (!is_array($libraryEmails)) {
            $libraryEmails = [$libraryEmails];
        }
        $libraryEmails = array_filter($libraryEmails);
        $libraryEmails = array_flip($libraryEmails);

        $filter = [
            'IBLOCK_ID'       => IBLOCK_ID_LIBRARY,
            '!PROPERTY_EMAIL' => false,
        ];
        if (!empty($libraryName)) {
            $filter['%NAME'] = $libraryName;
        }
        /** @todo Запилить модель для библиотек */
        $dbResult = $this->library->forgeCIBlockElement()->GetList(
            ['NAME' => 'ASC'],
            $filter,
            false,
            false,
            [
                'NAME',
                'PROPERTY_EMAIL',
            ]
        );
        $libraries = [];
        while ($item = $dbResult->Fetch()) {
            $libraries[] = [
                'name'     => $item['NAME'],
                'email'    => $item['PROPERTY_EMAIL_VALUE'],
                'selected' => isset($libraryEmails[$item['PROPERTY_EMAIL_VALUE']]),
            ];
        }

        return $libraries;
    }

    /**
     * @BitrixTemplate("AppBundle:library-notifications:history.html.php")
     *
     * @return array
     */
    public function historyAction()
    {
        $pager = $this->bitrixPager;
        $posting = $this->api->libraryPosting;
        $pagerName = 'notifications';
        $pager->initPageNavigation($pagerName);
        $currentPage = $pager->getPageNavigation($pagerName)->getCurrentPage();
        $posting->setSearchLimit($pager->getPageSize());
        if ($currentPage > 1) {
            $posting->setSearchOffset($pager->getPageSize() * ($currentPage - 1));
        }
        $result = $this->api->historyAction();
        $pager->getPageNavigation($pagerName)->setRecordCount($posting->getTotalResult());
        $result['url'] = $this->_getUrls();
        $result['libraries'] = $this->_getLibraries();
        $result['query'] = $this->request->query->all();
        $result['paginationHtml'] = $pager->buildBitrixMainPageNavigation($pagerName);

        return $result;
    }

    /**
     * @BitrixTemplate("AppBundle:library-notifications:history.html.php")
     *
     * @return array
     */
    public function libraryHistoryAction()
    {
        /** @var User $user */
        $user = $this->userProvider->getCurrentUser();
        $this->api->libraryPosting->setLibraryAdmin($user);
        $result = $this->historyAction();
        $result['isLibrary'] = true;

        return $result;
    }
}