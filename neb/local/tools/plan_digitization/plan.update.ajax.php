<?
define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('highloadblock');
Loader::includeModule('nota.exalead');

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

function getIdBlock($codeHL) {
    $connection = Application::getConnection();
    $sSql = "SELECT ID FROM b_hlblock_entity WHERE NAME='" . $codeHL . "';";
    $arRecord = $connection->query($sSql)->fetch();
    return intval($arRecord['ID']);
}

function getClassHL($codeHL) {
    $iblockId  = getIdBlock($codeHL);
    $hlblock   = HighloadBlockTable::getById($iblockId)->fetch();
    $dataClass = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
    return $dataClass;
}

// Вернем класс для работы с HL блоком содержащих планы на оцифровку и для работы с блоком содержащем список изданий
$dataClassPlan = getClassHL(HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
$dataClassEdition = getClassHL(HIBLOCK_CODE_DIGITIZATION_PLAN);

$arReturn = array();
// обновление
if ($arRequest['DATA_PLAN_UPDATE_AJAX'] == 'Y') {

    // Сформируем новое имя
    $yearDigitization = $DB->FormatDate($arRequest["DATA_PLAN_DATE_F"], "DD.MM.YYYY", "YYYY");
    $arFilter = array(
        ">=UF_DATE_F_PLAN" => "01.01." . $yearDigitization,
        "<=UF_DATE_F_PLAN" => "31.12." . $yearDigitization
    );
    $numbPlan = $dataClassPlan::getCount($arFilter) + 1;
    $planNewName = $yearDigitization . '_' . $arRequest["DATA_UF_LIBRARY"] . '_' . $numbPlan;

    $arPlanUpdate = [];

    // Обновим значение планируемой даты оцифровки
    if(!empty($arRequest["DATA_PLAN_DATE_F"])) {

        $arPlanUpdate['UF_DATE_F_PLAN'] = $arRequest['DATA_PLAN_DATE_F'];
        $arReturn["DATA_PLAN_DATE_F"] = $arRequest['DATA_PLAN_DATE_F'];

        // Получим список книг у которых необходимо обновить так же плановую дату оцифровки
        $arFilterEdition = array('UF_PLAN_ID' => $arRequest['DATA_PLAN_ID']);
        $rsEditions = $dataClassEdition::getList(array(
            'select' => array('ID'),
            'filter' => $arFilterEdition
        ));
        while($obEdition = $rsEditions->fetch()) {
            $dataClassEdition::update($obEdition['ID'], array('UF_DATE_FINISH' => $arRequest['DATA_PLAN_DATE_F']));
        }
    }

    // Новое значение статуса
    if(!empty($arRequest["DATA_PLAN_STATUS"])) {
        $arPlanUpdate['UF_REQUEST_STATUS'] = $arRequest['DATA_PLAN_STATUS'];
        $arReturn["DATA_PLAN_STATUS"] = "Y";
    }

    // Обновим значение имени
    if($arRequest['DATA_PLAN_YEAR'] != $yearDigitization) {
        $arPlanUpdate['UF_PLAN_NAME'] = $planNewName;
        $arReturn["DATA_PLAN_NAME"] = $planNewName;
    }

    // ОБНОВЛЕНИЕ СВОЙСТВ ПЛАНА
    if (!empty($arPlanUpdate)) {
        $dataClassPlan::update($arRequest['DATA_PLAN_ID'], $arPlanUpdate);
    }

    if($arRequest['SET_STATUS_BOOK'] == 'Y') {

        // Получим значения свойства типа список "Отменено"
        $idBlockPlan = getIdBlock(HIBLOCK_CODE_DIGITIZATION_PLAN);
        $arProp = CUserTypeEntity::GetList(
            array(),
            array('FIELD_NAME' => 'UF_REQUEST_STATUS', 'ENTITY_ID' => 'HLBLOCK_' . $idBlockPlan)
        )->Fetch();

        $cUserFieldEnum = new CUserFieldEnum();
        $rsUFValues = $cUserFieldEnum->GetList(array(), array('USER_FIELD_ID' => $arProp['ID']));
        $idPropertyCanceled = '';
        while($obUFValues = $rsUFValues->Fetch()) {
            if($obUFValues['VALUE'] == DIGITIZATION_STATUS_CANCELED) {
                $idPropertyCanceled = $obUFValues['ID'];
            }
        }

        // Получим список книг у которых необходимо обновить так же плановую дату оцифровки
        $arFilterEdition = array('UF_PLAN_ID' => $arRequest['DATA_PLAN_ID'], '!UF_REQUEST_STATUS' => intval($idPropertyCanceled));
        $rsEditions = $dataClassEdition::getList(array(
            'select' => array('ID'),
            'filter' => $arFilterEdition
        ));
        while($obEdition = $rsEditions->fetch()) {
            $dataClassEdition::update($obEdition['ID'], array('UF_REQUEST_STATUS' => $arRequest['DATA_BOOK_STATUS']));
            $arReturn['LIST_BOOKS'][] = array(
                'ID' => $obEdition['ID'],
                'UF_REQUEST_STATUS' => $arRequest['DATA_BOOK_STATUS']
            );
        }
    }
}
// удаление
elseif($arRequest['DATA_PLAN_DELETE_AJAX'] == 'Y') {
    // Обновим свойство удаляемого плана у списка книг из HL изданий
    // Вернем список id администраторов библиотек (если издание добавлено администратором в план, то оно удаляется, если клиентом сайта, то обновляются свойства)
    $rsGroup = CGroup::GetList($by = "ID",$order = "asc", array("STRING_ID" => UGROUP_LIB_CODE_ADMIN, "Y"))->Fetch();
    $arLibAdmin = CGroup::GetGroupUser($rsGroup['ID']);

    // Вернем список книг для обновления свойства
    $arFilterEdition = array(
        "UF_PLAN_ID" => $arRequest['DATA_PLAN_ID']
    );
    $arFilterEdition = $dataClassEdition::getList(array(
        'select' => array('*'),
        'filter' => $arFilterEdition
    ));

    // Обновим у них всех свойство с планом
    while($arEdition = $arFilterEdition->fetch()){
        // Издание добавил администратор, удалим его
        if(in_array($arEdition['UF_NEB_USER'], $arLibAdmin)) {
            $dataClassEdition::delete($arEdition['ID']);
        }
        // Издание добавил пользователь, обновим свойства
        else {
            $dataClassEdition::update($arEdition['ID'], array('UF_PLAN_ID' => ''));
        }
    }
    // Удалим план
    $result = $dataClassPlan::delete($arRequest['DATA_PLAN_ID']);
    if(!$result->isSuccess()) {
        $arReturn['ERROR'] = implode(', ', $result->getErrorMessages());
    } else {
        $arReturn['SUCCESS'] = 'Y';
    }
}

MainHelper::showJson($arReturn);