<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>

<div class="col-md-3">
    <div class="b-search-filter search-result__category">
        <script type="text/javascript">
            /*(function(){
             $('.set_opener').openHideBlock();
             })();*/

        </script>
        <form method="post" id="dop_filter">
            <input type="hidden" name="dop_filter" value="Y">
            <div class="b-sidenav b-search-filter ">
                <?php if(!empty($arResult['GROUPS']['AUTHORBOOK'])):?>
                    <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/filter_row.php', array('field'=>'AUTHORBOOK','items' => $arResult['GROUPS']['AUTHORBOOK'],'message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_AUTHORS'), 'more_message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'), 'count' => $arResult['COUNT'])); ?>
                <?php endif; ?>

                <?php  if(!empty($arResult['GROUPS']['YEARRANGE'])):?>
                    <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/filter_row.php', array('field'=>'YEARRANGE','items' => $arResult['GROUPS']['YEARRANGE'],'message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_DATE'), 'more_message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'), 'count' => $arResult['COUNT'])); ?>
                <?php endif; ?>

                <?php if(!empty($arResult['GROUPS']['PUBLISHER'])):?>
                    <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/filter_row.php', array('field'=>'PUBLISHER', 'items' => $arResult['GROUPS']['PUBLISHER'],'message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_PUBLISH'), 'more_message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'), 'count' => $arResult['COUNT'])); ?>
                <?php endif; ?>

                <?php if(!empty($arResult['GROUPS']['LIBRARY'])): ?>
                    <?
                    $messageCode = 'LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS';
                    if (!empty($arResult['librarytype'])) {
                        $messageCode .= '_' . $arResult['librarytype'];
                    }
                    ?>
                    <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/filter_row.php', array('field'=>'LIBRARY', 'items' => $arResult['GROUPS']['LIBRARY'],'message' => Loc::getMessage($messageCode), 'more_message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'), 'count' => $arResult['COUNT'])); ?>
                <?php endif; ?>

                <?php if(!empty($arResult['GROUPS']['PUBLISHPLACE'])):?>
                    <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/filter_row.php', array('field'=>'PUBLISHPLACE', 'items' => $arResult['GROUPS']['PUBLISHPLACE'],'message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_PLACE'), 'more_message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'), 'count' => $arResult['COUNT'])); ?>
                <?php endif; ?>

                <?php if(!empty($arResult['GROUPS']['TERMS'])):?>
                    <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/filter_row.php', array('field'=>'TERMS', 'items' => $arResult['GROUPS']['TERMS'],'message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_TERMS'), 'more_message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'), 'count' => $arResult['COUNT'])); ?>
                <?php endif; ?>

                <?php if(!empty($arResult['GROUPS']['COLLECTION_NEW'])):?>
                    <?php $APPLICATION->IncludeFile('/local/components/exalead/search.page/templates/.default/filter_row.php', array('field'=>'COLLECTION_NEW', 'items' => $arResult['GROUPS']['COLLECTION_NEW'],'message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_COLLECTION'), 'more_message' => Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'), 'count' => $arResult['COUNT'])); ?>
                <?php endif; ?>

            </div>
        </form>
    </div>
</div>