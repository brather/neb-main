<?php
/**
 * User: agolodkov
 * Date: 03.08.2016
 * Time: 16:32
 */

namespace BitrixBundle\EventListener;

use BitrixBundle\Annotations\IncludeBitrix;
use BitrixBundle\Annotations\Template;
use BitrixBundle\DependencyInjection\BitrixHelper;
use BitrixBundle\Traits\AnnotationReaderTrait;
use Doctrine\Common\Annotations\CachedReader;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

/**
 * Class BitrixControllerListener
 *
 * @package BitrixBundle\EventListener
 */
class BitrixControllerListener
{
    use AnnotationReaderTrait;

    /**
     * @var BitrixHelper
     */
    private $_bitrix;


    /**
     * @param CachedReader       $reader
     * @param BitrixHelper       $bitrix
     * @param ContainerInterface $container
     */
    public function __construct(
        CachedReader $reader,
        BitrixHelper $bitrix,
        ContainerInterface $container,
        ControllerNameParser $parser
    ) {
        $this->_reader = $reader;
        $this->_bitrix = $bitrix;
        $this->_container = $container;
        $this->_parser = $parser;
    }

    /**
     * @param string $controllerMethod
     *
     * @return mixed
     */
    private function _getMethodAnnotations($controllerMethod)
    {
        return $this->getControllerMethodAnnotations($controllerMethod);
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $annotations = $this->_getMethodAnnotations($event->getRequest()->attributes->get('_controller'));
        foreach ($annotations as $configuration) {
            if ($configuration instanceof IncludeBitrix) {
                $this->_bitrix->requireProlog();
            }
        }
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
    }

    /**
     * @param PostResponseEvent $event
     */
    public function onKernelTerminate(PostResponseEvent $event)
    {
        if (defined('B_PROLOG_INCLUDED')) {
            $this->_bitrix->requireEpilog();
        }
    }
}