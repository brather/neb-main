<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//debugPre($arResult);

$arRequest = $component->getRequest();

?>
<form class="b-form b-form_common b-feedbackform" method="get" action="?" data-sess="<?=bitrix_sessid()?>">

    <? foreach($arResult['ERROR'] as $sError): ?>
        <?ShowError($sError);?>
    <? endforeach; ?>

    <div class="row">
        <div class="col-md-3">
            <label for="fb_theme"><?= GetMessage("FORM_FIELD_SUBJECT") ?>:</label>
            <div class="field">
                <? if (!empty($arResult['FORM']['FIELDS']['FB_THEME']['ANSWERS'])): ?>
                    <select name="FEEDBACK[THEME]" id="fb_theme" class="js_select w370 feedback-select">
                        <? foreach($arResult['FORM']['FIELDS']['FB_THEME']['ANSWERS'] as $arAnswer):
                            $sName = explode('/', $arAnswer['MESSAGE']);
                            $sName = LANGUAGE_ID == 'ru' ? $sName[0] : $sName[1];?>
                            <option value="<?=$arAnswer['ID']?>"<? if ($arRequest['FEEDBACK']['THEME']): ?>
                                selected="selected"<? endif; ?>><?=$sName ? : $arAnswer['MESSAGE']?></option>
                        <? endforeach; ?>
                    </select>
                <? endif; ?>
            </div>
        </div>
    </div>

    <? if (empty($arResult['USER']['EMAIL'])): ?>
        <div class="row">
            <div class="col-md-3">
                <em class="hint">*</em>
                <label for="fb_email"><?= GetMessage("FORM_FIELD_EMAIL") ?>:</label>
                <div class="field validate">
                    <input type="text" name="FEEDBACK[EMAIL]" class="form-control"
                           data-minlength="2" data-maxlength="50" id="fb_email" data-validate="email"
                           data-required="required" value="<?= htmlspecialcharsbx($arRequest['FEEDBACK']['EMAIL']) ?>" />
                </div>
            </div>
        </div>
    <? endif; ?>

    <div class="row" style="margin-top: 1em;">
        <div class="col-md-6">
            <em class="hint">*</em>
            <label for="fb_message"><?= GetMessage("FORM_FIELD_MESSAGE") ?>:</label>
            <textarea name="FEEDBACK[MESSAGE]" id="fb_message" class="form-control feedback__textarea"
                      rows="3"><?= htmlspecialcharsbx($arRequest['FEEDBACK']['MESSAGE']) ?></textarea>
        </div>
    </div>

    <div style="margin-top: 1em;">
        <button class="btn btn-primary" value="1" type="submit"
                name="web_form_submit"><?= GetMessage("FORM_BUTTON_SUBMIT") ?></button>
    </div>

</form>
