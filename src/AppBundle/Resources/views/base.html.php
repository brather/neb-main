<?php
/** @var \Symfony\Bundle\FrameworkBundle\Templating\PhpEngine $view */
/** @var \BitrixBundle\DependencyInjection\BitrixViewHelper $bitrixViewHelper */
/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
$bitrixViewHelper = $view['bitrix_view_helper'];
$slots = $view['slots'];
$bitrixViewHelper->showHeader();
$slots->output('body');
$slots->output('javascripts');
$bitrixViewHelper->showFooter();