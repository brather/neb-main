<?php
/**
 * User: agolodkov
 * Date: 12.07.2016
 * Time: 10:52
 */

namespace AppBundle\Command;


use Neb\Main\Helper\MainHelper;
use Neb\Main\Stat\Tracker;
use Nota\Exalead\SearchClient;
use Nota\Exalead\SearchQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FillCollectionByQuery extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('neb:collection:fill-by-query')
            ->setDescription('Filling collection from exalead by query')
            ->addOption(
                'collection',
                null,
                InputOption::VALUE_REQUIRED
            )
            ->addOption(
                'query',
                null,
                InputOption::VALUE_REQUIRED
            )
            ->addOption(
                'limit',
                null,
                InputOption::VALUE_REQUIRED,
                1000
            );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeStart = microtime(true);

        $this->getContainer()
            ->get('bitrix_helper')
            ->requireProlog();

        MainHelper::includeModule('nota.exalead');
        define('NOT_CHECK_PERMISSIONS', true);

        while (ob_get_level()) {
            ob_end_clean();
        }

        if (!$limit = (integer)$input->getOption('limit')) {
            $limit = 1000;
        }
        $collection = (integer)$input->getOption('collection');
        $query = trim($input->getOption('query'));
        $query = urldecode($query);
        if ($collection < 1) {
            throw new \Exception('required option "--collection"');
        }
        if (empty($query)) {
            throw new \Exception('required option "--query"');
        }
        $section = new \CIBlockSection();
        if (!$section->GetList([], ['ID' => $collection])->Fetch()) {
            throw new \Exception("Collection '$collection' not found!");
        }
        $batch = 200;
        $offset = 0;
        $savedCount = 0;

        $getBatch = function ($query, $offset) use ($batch) {
            $query = new SearchQuery($query);
            $query->setPageLimit($batch);
            $query->setNextPagePosition($offset);
            $client = new SearchClient();

            return $client->getResult($query);
        };

        $saveBatch = function ($itemsBatch) use ($collection, &$savedCount) {
            if (!empty($itemsBatch['ITEMS'])) {
                $itemsMap = [];
                foreach ($itemsBatch['ITEMS'] as $key => $item) {
                    if (!empty($item['id'])) {
                        $itemsMap[$item['id']] = $key;
                    }
                }
                $element = new \CIBlockElement();
                $rsItems = $element->GetList(
                    [],
                    [
                        'IBLOCK_ID'        => IBLOCK_ID_COLLECTION,
                        'SECTION_ID'       => $collection,
                        'PROPERTY_BOOK_ID' => array_keys($itemsMap),
                    ],
                    false,
                    false,
                    ['PROPERTY_BOOK_ID']
                );
                while ($item = $rsItems->Fetch()) {
                    $bookId = $item['PROPERTY_BOOK_ID_VALUE'];
                    if (isset($itemsMap[$bookId])) {
                        unset($itemsBatch['ITEMS'][$itemsMap[$bookId]]);
                    }
                }
                foreach ($itemsBatch['ITEMS'] as $item) {
                    $fields = [
                        'IBLOCK_ID'         => IBLOCK_ID_COLLECTION,
                        'NAME'              => $item['title'],
                        'IBLOCK_SECTION_ID' => $collection,
                        'PROPERTY_VALUES'   => [
                            'BOOK_ID' => $item['id'],
                        ],
                    ];
                    if ($element->Add($fields)) {
                        $savedCount++;
                        echo 'Saved items: ', $savedCount, "\r";
                    }
                }
            }
        };

        do {
            $result = $getBatch($query, $offset);
            $saveBatch($result);
            $offset += $batch;
        } while (
            isset($result['ITEMS'])
            && !empty($result['ITEMS'])
            && ($offset < $limit)
        );
        echo PHP_EOL;


        $output->writeln(
            'Time: ' . (microtime(true) - $timeStart) . ' seconds'
        );
        $output->writeln(
            'Memory: ' . (memory_get_peak_usage() / (1024 * 1024)) . ' Mb'
        );
    }
}