<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);

	$_SERVER["DOCUMENT_ROOT"] = __DIR__.'../../../neb';
	$_SERVER["DOCUMENT_ROOT"] = '/srv/sites/neb/a.eremichev/neb';

	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	$file = 'libs.csv';

	\CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 

	$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_LIBRARYS)->fetch();
	$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
	$entity_data_class = $entity->getDataClass();


	$arLibs = array();
	if (($handle = fopen($file, "r")) !== FALSE) 
	{
		while (($data = fgetcsv($handle, 0, ";")) !== FALSE) 
		{
			$rsData = $entity_data_class::getList(array(
				"select" => array("ID", "UF_ID"),
				"filter" => array('UF_ID' => (int)$data[0]),
			));

			if(!$arData = $rsData->Fetch())
			{
				$arFields = array(
					'UF_ID'				=> (int)$data[0], 
					'UF_PARENTID'		=> (int)$data[1], 
					'UF_NAME'			=> trim($data[2]), 
					'UF_DATE_OPEN'		=> trim($data[3]), 
					'UF_STRUCTURE'		=> trim($data[4]), 
					'UF_ADRESS'			=> trim($data[5]), 
					'UF_REGION'			=> trim($data[6]), 
					'UF_DISTRICT'		=> trim($data[7]), 
					'UF_TOWN'			=> trim($data[8]), 
					'UF_PHONE'			=> trim($data[10]), 
					'UF_COMMENT_1'		=> trim($data[11]), 
					'UF_ADR_FIAS'		=> intval($data[12]), 
					'UF_REC_ID'			=> intval($data[13]), 
				);

				$result = $entity_data_class::add($arFields);
			}
			#pre($arFields,1);
			#exit();
		}
		fclose($handle);
	}




?>