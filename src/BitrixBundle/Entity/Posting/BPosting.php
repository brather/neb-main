<?php
namespace BitrixBundle\Entity\Posting;


use BitrixBundle\Entity\AbstractBitrixEntity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BPosting
 *
 * @ORM\Table(name="b_posting")
 * @ORM\Entity
 */
class BPosting extends AbstractBitrixEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serialization\Groups({"list", "details"})
     */
    public $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TIMESTAMP_X", type="datetime", nullable=false)
     */
    public $timestampX;

    /**
     * @var string
     *
     * @ORM\Column(name="STATUS", type="string", length=1, nullable=false)
     * @Serialization\Groups({"list", "details"})
     */
    public $status;

    /**
     * @var string
     *
     * @ORM\Column(name="VERSION", type="string", length=1, nullable=true)
     */
    public $version;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_SENT", type="datetime", nullable=true)
     * @Serialization\Groups({"list", "details"})
     */
    public $dateSent;

    /**
     * @var string
     *
     * @ORM\Column(name="SENT_BCC", type="text", nullable=true)
     */
    public $sentBcc;

    /**
     * @var string
     *
     * @ORM\Column(name="FROM_FIELD", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    public $fromField;

    /**
     * @var string
     *
     * @ORM\Column(name="TO_FIELD", type="string", length=255, nullable=true)
     */
    public $toField;

    /**
     * @var string
     *
     * @ORM\Column(name="BCC_FIELD", type="text", nullable=true)
     */
    public $bccField;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL_FILTER", type="string", length=255, nullable=true)
     */
    public $emailFilter;

    /**
     * @var string
     *
     * @ORM\Column(name="SUBJECT", type="string", length=255, nullable=false)
     * @Serialization\Groups({"list", "details"})
     * @Assert\NotBlank()
     */
    public $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="BODY_TYPE", type="string", length=4, nullable=false)
     */
    public $bodyType;

    /**
     * @var string
     *
     * @ORM\Column(name="BODY", type="text", nullable=false)
     * @Serialization\Groups({"list", "details"})
     * @Assert\NotBlank()
     */
    public $body;

    /**
     * @var string
     */
    public $bodyText;

    /**
     * @var string
     *
     * @ORM\Column(name="DIRECT_SEND", type="string", length=1, nullable=false)
     */
    public $directSend;

    /**
     * @var string
     *
     * @ORM\Column(name="CHARSET", type="string", length=50, nullable=true)
     */
    public $charset;

    /**
     * @var string
     *
     * @ORM\Column(name="MSG_CHARSET", type="string", length=255, nullable=true)
     */
    public $msgCharset;

    /**
     * @var string
     *
     * @ORM\Column(name="SUBSCR_FORMAT", type="string", length=4, nullable=true)
     */
    public $subscrFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="ERROR_EMAIL", type="text", nullable=true)
     */
    public $errorEmail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="AUTO_SEND_TIME", type="datetime", nullable=true)
     */
    public $autoSendTime;

    /**
     * @var string
     *
     * @ORM\Column(name="BCC_TO_SEND", type="text", nullable=true)
     */
    public $bccToSend;

    /**
     * @var int
     * @Serialization\Groups({"list", "details"})
     */
    public $rubId;

    /**
     * @var array
     * @Serialization\Groups({"list", "details"})
     */
    public $postingEmails = [];

    /**
     * @var array
     * @Serialization\Groups({"list", "details"})
     */
    public $postingFiles = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getTimestampX()
    {
        return $this->timestampX;
    }

    /**
     * @param \DateTime $timestampX
     */
    public function setTimestampX($timestampX)
    {
        $this->timestampX = $timestampX;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return \DateTime
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }

    /**
     * @param \DateTime $dateSent
     */
    public function setDateSent($dateSent)
    {
        $this->dateSent = $dateSent;
    }

    /**
     * @return string
     */
    public function getSentBcc()
    {
        return $this->sentBcc;
    }

    /**
     * @param string $sentBcc
     */
    public function setSentBcc($sentBcc)
    {
        $this->sentBcc = $sentBcc;
    }

    /**
     * @return string
     */
    public function getFromField()
    {
        return $this->fromField;
    }

    /**
     * @param string $fromField
     */
    public function setFromField($fromField)
    {
        $this->fromField = $fromField;
    }

    /**
     * @return string
     */
    public function getToField()
    {
        return $this->toField;
    }

    /**
     * @param string $toField
     */
    public function setToField($toField)
    {
        $this->toField = $toField;
    }

    /**
     * @return string
     */
    public function getBccField()
    {
        return $this->bccField;
    }

    /**
     * @param string $bccField
     */
    public function setBccField($bccField)
    {
        $this->bccField = $bccField;
    }

    /**
     * @return string
     */
    public function getEmailFilter()
    {
        return $this->emailFilter;
    }

    /**
     * @param string $emailFilter
     */
    public function setEmailFilter($emailFilter)
    {
        $this->emailFilter = $emailFilter;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getBodyType()
    {
        return $this->bodyType;
    }

    /**
     * @param string $bodyType
     */
    public function setBodyType($bodyType)
    {
        $this->bodyType = $bodyType;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getBodyText()
    {
        return $this->bodyText;
    }

    /**
     * @param string $bodyText
     */
    public function setBodyText($bodyText)
    {
        $this->bodyText = $bodyText;
    }

    /**
     * @return string
     */
    public function getDirectSend()
    {
        return $this->directSend;
    }

    /**
     * @param string $directSend
     */
    public function setDirectSend($directSend)
    {
        $this->directSend = $directSend;
    }

    /**
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * @param string $charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    /**
     * @return string
     */
    public function getMsgCharset()
    {
        return $this->msgCharset;
    }

    /**
     * @param string $msgCharset
     */
    public function setMsgCharset($msgCharset)
    {
        $this->msgCharset = $msgCharset;
    }

    /**
     * @return string
     */
    public function getSubscrFormat()
    {
        return $this->subscrFormat;
    }

    /**
     * @param string $subscrFormat
     */
    public function setSubscrFormat($subscrFormat)
    {
        $this->subscrFormat = $subscrFormat;
    }

    /**
     * @return string
     */
    public function getErrorEmail()
    {
        return $this->errorEmail;
    }

    /**
     * @param string $errorEmail
     */
    public function setErrorEmail($errorEmail)
    {
        $this->errorEmail = $errorEmail;
    }

    /**
     * @return \DateTime
     */
    public function getAutoSendTime()
    {
        return $this->autoSendTime;
    }

    /**
     * @param \DateTime $autoSendTime
     */
    public function setAutoSendTime($autoSendTime)
    {
        $this->autoSendTime = $autoSendTime;
    }

    /**
     * @return string
     */
    public function getBccToSend()
    {
        return $this->bccToSend;
    }

    /**
     * @param string $bccToSend
     */
    public function setBccToSend($bccToSend)
    {
        $this->bccToSend = $bccToSend;
    }

    /**
     * @return BPostingEmail[]
     */
    public function getPostingEmails()
    {
        return $this->postingEmails;
    }

    /**
     * @param array $postingEmails
     */
    public function setPostingEmails($postingEmails)
    {
        $this->postingEmails = $postingEmails;
    }

    /**
     * @param BPostingEmail $entity
     */
    public function addPostingEmail(BPostingEmail $entity) {
        $this->postingEmails[] = $entity;
    }

    /**
     * @return array
     */
    public function getPostingFiles()
    {
        return $this->postingFiles;
    }

    /**
     * @param array $postingFiles
     */
    public function setPostingFiles($postingFiles)
    {
        $this->postingFiles = $postingFiles;
    }

    /**
     * @return int
     */
    public function getRubId()
    {
        return $this->rubId;
    }

    /**
     * @param int $rubId
     */
    public function setRubId($rubId)
    {
        $this->rubId = $rubId;
    }

    public function getRepository()
    {
        return 'posting';
    }

}
