<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<?
$APPLICATION->IncludeComponent(
    "neb:library.check_eechb",
    "",
    Array(
        "ACTION_UNBIND_URL" => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['readers_unbind'],
        "ACTION_EDIT_URL" => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['readers_edit'],
        "ACTION_BLOCK_URL" => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['readers_block'],
    ),
    $component
);
