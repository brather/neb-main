<?
$MESS ['EDBG_TAB_UPLOAD_TITLE'] = "Upload";
$MESS ['EDBG_TAB_SEARCH_TITLE'] = "Search";
$MESS ['EDBG_TAB_EDIT_TITLE'] = "Edit";
$MESS ['EDBG_DRAG_FILES'] = "Drag files here";
$MESS ['EDBG_UPLOAD_BUTTON'] = "Upload from disc...";
$MESS ['EDBG_DRAG_PHOTO'] = "Drag photos to change order";
$MESS ['EDBG_PHOTO_HIDE'] = "Hide";
$MESS ['EDBG_PHOTO_HIDE_TITLE'] = "Hide this photo";
$MESS ['EDBG_PHOTO_EDIT'] = "Edit";
$MESS ['EDBG_PHOTO_EDIT_TITLE'] = "Edit this photo";

$MESS ['EDBG_INSERT_TEMPLATE'] = "Template insert";
$MESS ['EDBG_INSERT_BUTTON'] = "Insert";
$MESS ['EDBG_MESS_ONE_PHOTO'] = "The selected template is possible to display only one picture. Resume?";
$MESS ['EDBG_MESS_ADD_PHOTO'] = "Add photo please.";

$MESS ['EDBG_SEARCH_BUTTON'] = "Search";
$MESS ['EDBG_CHOOSE_BUTTON'] = "Choose # files";
