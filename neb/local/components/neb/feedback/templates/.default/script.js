var FRONT = FRONT || {};

$(function(){

	/*================================== подготовленая функция динамического селектора ===============================*/
	var onChangeLibSelector = function(args) {
        $(document.body).off().on('change', '#responsibleLibSelect', function(){
            var appointItemURL = "/profile/feedback/appoint/?ajax=Y&id="
                + args.targetData.itemNumber + '&lib=' + this.value;
            $.when(ajaxFeedback(appointItemURL, '', args.modal, context.targetRow))
				.then(function () {
                	$('#responsibleLibSelect').selectpicker({liveSearch: true, size:10, dropupAuto: false});
            	});
        });

    };

	FRONT.feedback = FRONT.feedback || {};

	/* =========================================== назначение исполнителя =========================================== */
	function ruDate(dateString) {
        var d = parseInt(dateString[0] + dateString[1]),
            m = parseInt(dateString[3] + dateString[4]),
            y = parseInt(dateString[6] + dateString[7] + dateString[8] + dateString[9]),
            date = new Date(y, m - 1, d);
        return date;
    }

	jQuery.validator.addMethod(
		"olderThan", 
		function(value, element, params) {
			var form = $(element).closest('form');
		    if (!/Invalid|NaN/.test( ruDate(value) )) {
		        return ruDate(value) >= ruDate($(params).val());
		    }
		},
		'Must be greater than {0}.'
	);
	jQuery.validator.addMethod(
		"youngerThan", 
		function(value, element, params) {
			var form = $(element).closest('form');
		    if (!/Invalid|NaN/.test(ruDate(value))) {
		        return ruDate(value) <= ruDate($(params).val());
		    }
		},
		'Must be greater than {0}.'
	);
	
	FRONT.feedback.assign = function(itemKey, opt){
		var target = opt.$trigger,
        	targetData = $(target).data(),
        	that = this;

    	this.targetRow = target;

        FRONT.infomodal({
    		options: {
    			buttonBar: false
    		},
    		title: 'Назначить исполнителя',
    		context: that,
    		callback: function(modalContent, modal, context) {//callback fires once with show.bs.modal
    			var target = target;

				$(modal).find('.modal-dialog').addClass('fixed-modal modal-lg no-footer-bar')
				modal.one('shown.bs.modal',function(e){
					//this fires after, is on shown.bs.modal
				});

				var url = "/profile/feedback/appoint/?ajax=Y&id=" + targetData.itemNumber;

				$.when(ajaxFeedback(url, '', modal))
					.then(function(){
                        $('#responsibleLibSelect').selectpicker({liveSearch: true, size:10, dropupAuto: false});
                        onChangeLibSelector.call(this, {targetData: targetData, context: context, modal:modal});
					});

				// отправка формы
				$(document).one('click', '#appointFormFeedback input[type="submit"]' , function(e){
					e.preventDefault();
					var toggler = $(e.target),
						form = $(toggler).closest('form'),
						name = toggler.attr('name'),
						hidden = $('<input type="hidden" name="' + name + '"/>');

					form.append(hidden);

					ajaxFeedback(form.attr('action'), form.serialize(), modal, context.targetRow);
				});
    		}
        });
	};

	/* ==========================================   ответ на вопрос    ============================================= */

	FRONT.feedback.reply = function(itemKey, opt){

		var target = opt.$trigger,
			targetData = $(target).data(),
			that = this;

		this.targetRow = target;

		FRONT.infomodal({
			options: {
				buttonBar: false
			},
			title: 'Ответ на вопрос',
			context: that,
			callback: function(modalContent, modal, context) {//callback fires once with show.bs.modal
				$(modal).find('.modal-dialog').addClass('fixed-modal modal-lg no-footer-bar')
				modal.one('shown.bs.modal',function(e){
					//this fires after, is on shown.bs.modal
				});

				var url = "/profile/feedback/reply/?ajax=Y&id=" + targetData.itemNumber;
				ajaxFeedback(url, '', modal, context.targetRow);

				// отправка формы
				$(document).one('click', '#replyFormFeedback input[type="submit"]' , function(e){
					e.preventDefault();
					var toggler = $(e.target),
						form = $(toggler).closest('form'),
						name = toggler.attr('name'),
						hidden = $('<input type="hidden" name="' + name + '"/>');

					form.append(hidden);

					ajaxFeedback(form.attr('action'), form.serialize(), modal, context.targetRow);
				});
			}
		});
	}

	/* ==========================================  отклонение вопроса  ============================================= */

	FRONT.feedback.reject = function(itemKey, opt){

		var target = opt.$trigger,
			targetData = $(target).data(),
			that = this;

		this.targetRow = target;

		FRONT.infomodal({
			options: {
				buttonBar: false
			},
			title: 'Отклонение заявки',
			context: that,
			callback: function(modalContent, modal, context) {

				$(modal).find('.modal-dialog').addClass('fixed-modal modal-lg no-footer-bar')
				modal.one('shown.bs.modal',function(e){
					//this fires after, is on shown.bs.modal
				});

				var url = "/profile/feedback/reject/?ajax=Y&id=" + targetData.itemNumber;
				ajaxFeedback(url, '', modal, context.targetRow);

				// отправка формы
				$(document).one('click', '#rejectFormFeedback input[type="submit"]' , function(e){
					e.preventDefault();
					var toggler = $(e.target),
						form = $(toggler).closest('form'),
						name = toggler.attr('name'),
						hidden = $('<input type="hidden" name="' + name + '"/>');

					form.append(hidden);

					ajaxFeedback(form.attr('action'), form.serialize(), modal, context.targetRow);
				});
			}
		});
	}

	/* ==========================================  отклонение вопроса  ============================================= */

	FRONT.feedback.history = function(itemKey, opt){

		var target = opt.$trigger,
			targetData = $(target).data();

		FRONT.infomodal({
			options: {
				buttonBar: false
			},
			title: 'История заявки',
			callback: function(modalContent, modal) {

				$(modal).find('.modal-dialog').addClass('fixed-modal modal-lg no-footer-bar')
				modal.one('shown.bs.modal',function(e){
					//this fires after, is on shown.bs.modal
				});

				var url = "/profile/feedback/history/?ajax=Y&id=" + targetData.itemNumber;
				ajaxFeedback(url, '', modal);
			}
		});
	}

	/* ============================================================================================================== */

	function ajaxFeedback(url, data, modal, target, beforeFunc) {
		return $.ajax({
			url: url,
			data: data,
			dataType: "html",
			cache: false,
			beforeSend: function () {
				if (typeof  beforeFunc === "function"){
					console.log('beforeSend function');
					beforeFunc.call();
				}
            },
			success: function(data, textStatus, jqXHR){
				if (isJSON(data)) {
					data = JSON.parse(data);
					$(modal).modal('hide');
					target.find('[data-status]').html(data.TICKET.STATUS_TITLE).end()
						.find('[data-executor-text]').html('').end()
						.find('[data-executor-name]').html(data.TICKET.USER);
				}
				else {
					$(modal).find('.modal-body').empty().append(data);
				}
			},
			error: function(jqXHR,textStatus,errorThrown){
			}
		});
	}

    $.contextMenu({
        selector: '[data-feedback-item]',
		build: function($triggerElement, e){

			var classList = $triggerElement.context.classList;

			var itemsMenu = {};
			if (in_array('feedback-registered', classList))
				itemsMenu.assign  = {name: "Назначить исполнителя", callback: function(itemKey, opt) { FRONT.feedback.assign(itemKey, opt); } };
			if (in_array('feedback-reply', classList))
				itemsMenu.reply   = {name: "Ответить", callback: function(itemKey, opt) { FRONT.feedback.reply(itemKey, opt) } };
			if (in_array('feedback-reject', classList))
				itemsMenu.reject  = {name: "Отклонить", callback: function(itemKey, opt) { FRONT.feedback.reject(itemKey, opt) } };
			if (in_array('feedback-closed', classList))
				itemsMenu.reply  = {name: "Ответ", callback: function(itemKey, opt) { FRONT.feedback.reply(itemKey, opt) } };
			if (in_array('feedback-history', classList))
				itemsMenu.history = {name: "История", callback: function(itemKey, opt) { FRONT.feedback.history(itemKey, opt) } };

			return { items: itemsMenu };
		}
    });
	
	FRONT.feedback.initDateRange = (function() {
		var fromInput = $('[id="article_pub_date1"]'),
			toInput = $('[id="article_pub_date2"]'),
			now = new Date().getTime(),
			weekAgo = new Date( now - (1000*60*60*24*7) ).getTime();
		this.leadingZero = function(num) {
			return num = num < 10 ? '0' + num.toString() : num.toString();
		}
		this.month = function(month) {
			var month = month + 1;
			return month = this.leadingZero(month);
		}
		if (!fromInput.val()) {fromInput.val( this.leadingZero(new Date(weekAgo).getDate()) + '.' + this.month(new Date(weekAgo).getMonth()) + '.' + (new Date(weekAgo).getFullYear()).toString() ); }
		if (!toInput.val()) {toInput.val( this.leadingZero(new Date(now).getDate()) + '.' + this.month(new Date(now).getMonth()) + '.' + (new Date(now).getFullYear()).toString() );}
	})()

	FRONT.feedback.statistic = {}
	FRONT.feedback.statistic.filterFormValidation = (function(){
		var form = $('[data-feedback-filter-form]');
		var validator = form.validate({
			rules: {
                "FEEDBACK[DATE_FROM]": {
                    required: true,
                    youngerThan: "[name='FEEDBACK[DATE_TO]']"
                },
                "FEEDBACK[DATE_TO]": {
                    required: true,
                    olderThan: "[name='FEEDBACK[DATE_FROM]']"
                }
            },
            messages: {
                "FEEDBACK[DATE_FROM]": {
                    required: "Введите дату начала периода",
                    youngerThan : "Дата начала периода должна быть меньше даты окончания"
                },
                "FEEDBACK[DATE_TO]": {
                    required: "Введите дату окончания периода",
                    olderThan: "Дата окончания периода должна быть больше даты начала"
                }
            },
            errorPlacement: function(error, element) {
		        error.appendTo( $(element).closest('form') );
		        $(element).closest('.form-group').toggleClass('has-error', true);
		    },
		    success: function(label) {
		        label.remove();
		    },
		    highlight: function(element, errorClass, validClass) {        
		        $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
		        $(element).parent().find("." + errorClass).removeClass("resolved");
		    },
		    unhighlight: function(element, errorClass, validClass) {
		        $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
		    }

		});

		form.on('change, blur','[name="FEEDBACK[DATE_FROM]"], [name="FEEDBACK[DATE_TO]"]',function(){
			form.find('[name="FEEDBACK[DATE_FROM]"]').valid();
			form.find('[name="FEEDBACK[DATE_TO]"]').valid();
		});

	})()

});

$(function(){
	$('[data-select-two]')
		.find('[value="-1"]').detach().end()
		.select2();

	$(document).on('change', '[data-select-two]', function(){
	});

	$(document).on('click','[data-feedback-item]',function(e){
		var toggler = $(this),
			thead = toggler.closest('dl').find('dt'),
			author = toggler.find('span').eq(2).find('i').eq(1).find('a').text(),
			content = toggler.find('span').eq(4).text(),
			contentObj = $('<dl class="data-dl"/>');

		toggler.children('span').each(function(i){
			contentObj.append( $('<dt/>').html( thead.children('span').eq(i).text() ) );
			contentObj.append( $('<dd/>').html( $(this).html() ) )  
			
		});

		FRONT.infomodal({    		
    		title: author,
    		obj: contentObj
    	});
	});


});