<?
	function setIdFromALIS(&$arFields, $libId)
	{
		$balnk = '000000';
		$IdFromALIS = substr_replace($balnk, $libId, strlen($balnk) - strlen($libId));
		$IdFromALIS .= '_';
		$IdFromALIS .= substr_replace($balnk, $arFields['ALIS'], strlen($balnk) - strlen($arFields['ALIS']));
		$IdFromALIS .= '_';
		$IdFromALIS .= substr(md5($arFields['Author'].$arFields['Name'].$arFields['PublishYear']), 0, 10);

		$arFields['IdFromALIS'] = $IdFromALIS;
	}

	function exaleadLog($data){
		$dir = $_SERVER['DOCUMENT_ROOT'] . EXALED_INDEX_DIR_LOGS;
		if (!file_exists($dir)) {
			mkdir($dir, 0775, true);
		}

		file_put_contents($dir . date('d.m.Y') . ".txt", date("H:i:s => ") . $data . "\r\n", FILE_APPEND);
		chmod($dir . date('d.m.Y') . ".txt", 0775);
	}

	function pre($var,$show=false){
		if(@$_REQUEST['sadmin']=='Y'){
			echo "<pre>";
			print_r($var);
			echo "</pre>";
		}else{
			if(!$show) echo "<!--PRE<pre>"; else echo "<pre>";
			print_r($var);
			if(!$show) echo "</pre>-->"; else echo "</pre>";
		}
	}
?>