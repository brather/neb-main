<?php

namespace LbcBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LbcSpecialdivisionType extends AbstractLbcCategoryType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('maindivisionid');
    }

    public function getEntityName()
    {
        return 'LbcBundle\Entity\LbcSpecialdivision';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lbcbundle_lbcspecialdivision';
    }
}
