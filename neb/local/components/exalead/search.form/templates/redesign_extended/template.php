<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

?>
<div class="
    searches-block-inner
    <? if (false === strpos($_SERVER['REQUEST_URI'], '/search/extended/')): ?> container<? endif; ?>
    sbi<? if ($arResult['formType'] != 'E' && false === strpos($_SERVER['REQUEST_URI'], '/extended/')): ?> hidden<? endif; ?>"
    data-diet-target>

    <? if (false !== strpos($_SERVER['REQUEST_URI'], '/extended/')): ?>
        <h4><?=Loc::getMessage('EXTENDED_SEARCH_FORM_TITLE')?> <a class="tssfaqlabel" target="_blank" href="/how-to-search/#extsearch" title="<?= Loc::getMessage('EXTENDED_SEARCH_FORM_FAQ_TITLE') ?>"><?= Loc::getMessage('EXTENDED_SEARCH_FORM_FAQ_ANCHOR') ?></a></h4>
    <? endif; ?>

    <form action="<?=$arParams['ACTION_URL'] ? : '/search/'?>" class="extendedSearch searches-redesign-extended sre" data-three-form>

        <? if (!empty($arRequest['plan_id'])): ?>
            <input type="hidden" name="plan_id" value="<?= htmlspecialcharsbx($arRequest['plan_id']) ?>">
        <? endif; ?>
        <? if (!empty($arRequest['date'])): ?>
            <input type="hidden" name="date" value="<?= htmlspecialcharsbx($arRequest['date']) ?>">
        <? endif; ?>

        <dl class="form-dl">
            <? foreach ($arResult['searchFields'] as $arItem):

                if (in_array($arItem['name'], ['q', 'searchtype'])
                    // в плане оцифровки исключаются поля "Область поиска" и "Библиотека"
                    || $arParams['PLAN_DIGITIZATION'] == 'Y' && in_array($arItem['name'], ['searcharea', 'library'])
                ) {
                    continue;
                }

                $bAccess = true;
                if (!in_array('always', $arItem['access']) && !empty($_REQUEST['librarytype'])
                    && !in_array($_REQUEST['librarytype'], $arItem['access'])
                ){
                    $bAccess = false;
                }
                ?>
                <dt class="<?=implode(' ', $arItem['access'])?>"
                    <? if (!$bAccess): ?> style="display: none;"<? endif; ?>
                >
                    <? if ($arItem['type'] == 'text_extended'): ?>
                        <label class="for-lg one-line-height" <? if ($arItem['aria-hidden'] == true):
                            ?> aria-hidden="true"<? endif; ?>>
                            <?=$arItem['label']?><br />

                            <small>(<? $i=0; foreach ($arItem['input'] as $post => $arInput): ?>
                                <label id="<?=$arItem['id'].$post?>label" for="<?=$arItem['id'].$post?>">
                                    <span class="screen-hide"></span><?=$arInput['name']?>
                                </label><?=!$i?',':''?>
                             <? $i++; endforeach; ?>)</small>

                        </label>
                    <? elseif ($arItem['type'] == 'radio'):?>
                        <?=$arItem['label']?>
                    <? else: ?>
                        <label class="for-lg"<? if ($arItem['aria-hidden'] == true): ?> aria-hidden="true"<? endif;
                            ?> for="<?=$arItem['id']?>" id="<?=$arItem['id']?>label"><?=$arItem['label']?></label>
                    <? endif; ?>
                </dt>
                <dd class="relative" <? if (!$bAccess):
                    ?> style="display: none;"<?
                endif; ?>>
                    <? if ($arItem['type'] == 'tabs'): ?>

                        <? foreach ($arItem['value'] as $k => $v): ?>
                            <label>
                                <input
                                    type="radio"
                                    name="<?=$arItem['name']?>"
                                    value="<?=$k?>"
                                    <? if (empty($_REQUEST[$arItem['name']]) && $k == 'library' || $_REQUEST[$arItem['name']] == $k):?>
                                        checked="checked"
                                    <? endif; ?>/>
                                <span class="tablbl"><?=$v?></span>
                            </label>
                        <? endforeach; ?>

                    <? elseif ($arItem['type'] == 'select'):
                        $sName = str_replace('[]', '', $arItem['name']);?>

                        <select name="<?=$arItem['name']?>" class="form-control input-lg" id="<?=$arItem['id']?>"
                                tabindex="<?=$arItem['tabindex']?>" aria-labelledby="<?=$arItem['id']?>label" <?
                                if ($arItem['multiple'] == true):?> multiple<? endif; ?><? if (!empty($arItem['langcollection'])):
                            ?> langcollection="<?=$arItem['langcollection']?>"<? endif; ?> <?=$arItem['attributes']?>
                        >
                            <? if($arItem['id'] == 'lang'): // костылик для языков ?>
                                <option value=""><?= Loc::getMessage('EXTENDED_SEARCH_FORM_ALL') ?></option>
                            <? endif; ?>
                            <? foreach ($arItem['value'] as $arSelect):
                                $sView = (LANGUAGE_ID == 'en' && !empty($arSelect['NAME_EN'])) ? $arSelect['NAME_EN'] : $arSelect['NAME'];
                                ?>
                                <option value="<?=$arSelect['NAME']?>" <? if (
                                    empty($_REQUEST[$sName]) && 'Y' == $arSelect['SELECT']
                                        || is_array($_REQUEST[$sName]) && in_array($arSelect['NAME'], $_REQUEST[$sName])
                                        || $arSelect['NAME'] == $_REQUEST[$sName]
                                    ):
                                    ?> selected="selected"<? endif; ?>><?=$sView?></option>
                            <? endforeach; ?>
                        </select>

                    <? elseif ($arItem['type'] == 'select-checkbox'): ?>
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                                            Выбрать рубрики <span class="glyphicon glyphicon-plus pull-right"></span>
                                            <span class="glyphicon glyphicon-minus pull-right"></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne"
                                     class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <? foreach ($arItem['value'] as $k => $v): ?>
                                        <div>
                                            <label class="for" for="sc<?=$k?>">
                                                <input value="<?=$v?>" id="sc<?=$k?>" data-id="<?=$k?>" type="checkbox" name="<?=$arItem['name']?>[<?=$k?>]" data-name="<?=$arItem['name']?>" aria-labelledby="sc<?=$k?>label" data-treenum="1" />
                                                <span class="lbl">
                                                    <span><?=$v?></span>
                                                    <span class="screen-hide" id="sc<?=$k?>label"><?=$v?></span>
                                                </span>
                                            </label>
                                            <div class="treenum" style="margin-left: 30px"></div>
                                        </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <? elseif ($arItem['type'] == 'radio'): ?>

                        <? foreach ($arItem['value'] as $k => $v): ?>
                            <label class="for" for="<?=$k?>">
                                <input tabindex="<?=$arItem['tabindex']?>" type="radio" class="form-control"
                                    name="<?=$bAccess?$arItem['name']:''?>" data-name="<?=$arItem['name']?>"
                                    value="<?=$k?>" id="<?=$k?>" aria-labelledby="<?=$k?>label"
                                    <? if ($_REQUEST[$arItem['name']] == $k || empty($_REQUEST[$arItem['name']])
                                            && $v['default'] == true): ?> checked="true"<? endif; ?>
                                />
                                <span class="lbl">
                                    <span <? if ($v['aria-hidden'] == true):
                                        ?> aria-hidden="true"<? endif; ?>><?=$v['name']?></span>
                                    <span class="screen-hide" id="<?=$k?>label"><?=$v['screen']?></span>
                                </span>
                            </label>
                        <? endforeach; ?>

                    <? elseif ($arItem['type'] == 'checkbox'): ?>

                        <input type="checkbox" class="form-control input-lg" id="<?=$arItem['id']?>"
                               name="<?=$bAccess?$arItem['name']:''?>" data-name="<?=$arItem['name']?>"
                               value="<?=$arItem['value']?>"
                               aria-labelledby="<?=$arItem['id']?>label" style="opacity: 1;"
                                <? if ($_REQUEST[$arItem['name']] == 1): ?> checked="true"<? endif; ?>
                        />

                    <? elseif ($arItem['type'] == 'date'): ?>

                        <div class="extended-search-dates-range esdr clearfix">
                            <label for="exsearch-date-from" class="esdr-from-input !main-search__exsearch-date-from">
                                <span><?=Loc::getMessage('EXTENDED_SEARCH_FORM_YEARFROM')?></span>
                                <input id="exsearch-date-from" type="text" minlength="1" maxlength="4" class="main-search__exsearch-date-from-input" name="publishyear_prev"
                                       value="<?php echo $component->validatePrevDate($_REQUEST['publishyear_prev'], $_REQUEST['publishyear_next']) ? $_REQUEST['publishyear_prev'] : SEARCH_BEGIN_YEAR ?>" />
                            </label>
                            <div class="esdr-range-cell">
                                <div class="main-search__exsearch-date-range esdr-date-range"></div>
                            </div>
                            <label for="exsearch-date-to" class="esdr-to-input !main-search__exsearch-date-to">
                                <span><?=Loc::getMessage('EXTENDED_SEARCH_FORM_YEARTO')?></span>
                                <input id="exsearch-date-to" type="text" minlength="1" maxlength="4" class="main-search__exsearch-date-to-input" name="publishyear_next"
                                       value="<?php echo $component->validateNextDate($_REQUEST['publishyear_prev'], $_REQUEST['publishyear_next']) ? $_REQUEST['publishyear_next'] : date("Y") ?>" />
                                <span class="instead-input-lg" style="margin-left: 1ex;" aria-hidden="true"><?=Loc::getMessage('EXTENDED_SEARCH_FORM_YEARUNIT')?></span>
                            </label>
                        </div>

                        <?/* foreach ($arItem['value'] as $k => $v): ?>
                            <label class="for-lg" for="<?=$arItem['id'].$k?>" id="<?=$arItem['id'].$k?>label">
                                <span class="screen-hide"><?=$arItem['label']?>, </span><?=$v['name']?><span class="screen-hide">,</span>
                            </label>
                            <input tabindex="<?=$arItem['tabindex']+1?>" type="text" class="form-control input-lg"
                                   name="<?=$bAccess?$arItem['name'].$k:''?>" data-name="<?=$arItem['name'].$k?>"
                                   id="<?=$arItem['id'].$k?>"  aria-labelledby="<?=$arItem['id'].$k?>label"
                                   value="<?=htmlspecialchars($_REQUEST[$arItem['name'].$k]) ? : $v['value']?>"
                                   style="display: inline-block; width: 5em;"
                            />
                        <? endforeach; */?>
                    
                    <? elseif ($arItem['type'] == 'text_extended'): ?>

                        <div class="row">
                            <? $i=0; foreach ($arItem['input'] as $post => $arInput): ?>
                                <div class="col-xs-<?=!$i?'3':'9'?>">
                                    <input type="text" class="form-control input-lg" id="<?=$arItem['id'].$post?>"
                                        name="<?=$bAccess?$arInput['name'].$post:''?>" data-name="<?=$arInput['name'].$post?>"
                                        value="<?=htmlspecialchars($_REQUEST[$arInput['name'].$post])?>"
                                        <? foreach ($arInput['placeholder'] as $k => $v): ?>
                                            data-placeholder-<?=$k?>="<?=$v?>"
                                        <? endforeach; ?>
                                        aria-labelledby="<?=$arItem['id'].$post?>label" />
                                </div>
                            <? $i++; endforeach; ?>
                        </div>

                    <? elseif ($arItem['type'] == 'text'): ?>

                        <input type="text" class="form-control input-lg" id="<?=$arItem['id']?>" tabindex="<?=$arItem['tabindex']?>"
                               name="<?=$bAccess?$arItem['name']:''?>" data-name="<?=$arItem['name']?>"
                               value="<?=htmlspecialchars($_REQUEST[$arItem['name']])?>" aria-labelledby="<?=$arItem['id']?>label"

                               <? foreach ($arItem['placeholder'] as $k => $v): ?>
                                   data-placeholder-<?=$k?>="<?=$v?>"
                               <? endforeach; ?>
                               <?=$arItem['attributes']?> data-virtual-keyboard />

                    <? endif; ?>
                </dd>
            <? endforeach; ?>
        </dl>
        <div class="button-bar" role="toolbar">
            <button class="btn btn-lg btn-link btn-default" type="reset" tabindex="5"><?=Loc::getMessage('EXTENDED_SEARCH_FORM_CLEAN')?></button>
            <button class="btn btn-lg btn-primary" tabindex="4"><?=Loc::getMessage('EXTENDED_SEARCH_FORM_SEARCH')?></button>
        </div>

        <? if (!empty($_REQUEST['f_field'])): ?>
            <? foreach($_REQUEST['f_field'] as $key => $field): ?>
                <? if(!is_array($field)) {
                    $field = array($field);
                }?>
                <? foreach($field as $fieldValue): ?>
                    <input type="hidden" value="<?= htmlspecialchars($fieldValue) ?>" name="f_field[<?= htmlspecialchars($key) ?>][]" />
                <? endforeach; ?>
            <? endforeach; ?>
        <? endif; ?>
    </form>
</div>