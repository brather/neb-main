<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160402143836 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            <<<SQL
CREATE TABLE `plan_digitization_log` (
  `ID`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `TIMESTAMP`   TIMESTAMP                 DEFAULT NOW(),
  `ORDER_ID`    INT(11) UNSIGNED NOT NULL,
  `USER_ID`     INT(11) UNSIGNED NOT NULL,
  `COMMENT`     TEXT
                COLLATE utf8_unicode_ci,
  `STATUS_ID`   INT(11),
  `ACTION_NAME` VARCHAR(150)
                COLLATE utf8_unicode_ci   DEFAULT NULL,
  PRIMARY KEY (`ID`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql(
            <<<SQL
            DROP TABLE `plan_digitization_log`
SQL
        );

    }
}
