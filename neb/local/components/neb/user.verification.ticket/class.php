<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Type\DateTime,
    \Bitrix\Highloadblock\HighloadBlockTable;

CBitrixComponent::includeComponentClass('neb:user.verification');

class UserVerificationTicket extends UserVerificationComponent
{
    protected $arRequest = [], $arUser = [];

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->getRequest();
        $this->getUser();
        $this->getTicket();
        $this->switchAction();
        $this->includeComponentTemplate();
    }

    /**
     * Получение ЭЧБ
     */
    private function getTicket() {

        $id = intval($this->arParams['ID']);
        if ($id <= 0)
            return [];

        $this->includeModules(['highloadblock']);

        $arHLBlock        = HighloadBlockTable::getList(['filter' => ['NAME' => 'UsersTickets']])->fetchRaw();
        $obUsersTickets   = HighloadBlockTable::compileEntity($arHLBlock)->getDataClass();
        $arResult         = $obUsersTickets::getList(["filter" => ['ID' => $id]])->fetch();
        $arResult['USER'] = $this->getUserById($arResult['UF_USER_ID']);

        $this->arResult = $arResult;
    }

    /**
     * Выбор выполняемого действия
     */
    private function switchAction() {
        if ($this->arParams['DELETE'] == 'Y') {
            $this->deleteTicket();
        }
        else {
            $this->saveData();
        }
    }

    /**
     * Сохранине ЭЧБ в БД
     */
    private function saveData() {

        $arRequest = $this->arRequest['VERIFICATION'];

        if (empty($arRequest))
            return;

        $arUser = $arError = [];

        if (empty($arRequest['LOGIN'])) {
            $arError[] = 'Поле "Логин" обязательно для заполнения!';
        }

        if (empty($arRequest['NUMBER'])) {
            $arError[] = 'Поле "Номер ЧБ" обязательно для заполнения!';
        }

        if (empty($arRequest['DATE'])) {
            $arError[] = 'Поле "Дата окончания действия ЧБ" обязательно для заполнения!';
        } else {
            try {
                if ((new DateTime($arRequest['DATE']))->getTimestamp() <= (new DateTime())->getTimestamp()) {
                    $arError[] = 'Дата должна быть больше текущей даты!';
                }
            } catch (Exception $e) {
                $arError[] = str_replace('Incorrect date/time', 'Введена некорректная дата', $e->getMessage());
            }
        }

        // проверка пользователя
        if (!empty($arRequest['LOGIN'])) {
            $arUser = CUser::GetByLogin($arRequest['LOGIN'])->Fetch();
            if (empty($arUser))
                $arError[] = 'Пользователь с email "' . $arRequest['LOGIN'] . '" не найден!';
            if (!empty($arUser) && 'Y' != $arUser['ACTIVE'])
                $arError[] = 'Пользователь с email "' . $arRequest['LOGIN'] . '" не активирован!';
        }

        // проверка ЭЧЗ
        if (intval($this->arUser['UF_WCHZ']) <= 0) {
            $arError[] = 'Текущий пользователь не привязан к ЭЧЗ!';
        } else {
            $arWorkplace = $this->getWorkplace($this->arUser['UF_WCHZ']);
            if (empty($arWorkplace)) {
                $arError[] = 'ЭЧЗ оператора (ID: ' . $this->arUser['UF_WCHZ'] . ') отсуствует в списке ЭЧЗ!';
            }
            elseif ($arWorkplace['ACTIVE'] == 'N') {
                $arError[] = 'ЭЧЗ оператора (' . $arWorkplace['NAME'] . ') деактивирован!';
            }
            elseif (intval($arWorkplace['PROPERTY_LIBRARY_VALUE']) == 0) {
                $arError[] = 'В ЭЧЗ ' . $arWorkplace['NAME'] . ' не задана привязка к библиотеке!';
            }
        }

        if ($arUser['ID']) {

            $this->includeModules(['highloadblock']);

            $arHIblock      = HighloadBlockTable::getList(['filter' => ['NAME' => 'UsersTickets']])->fetchRaw();
            $obUsersTickets = HighloadBlockTable::compileEntity($arHIblock)->getDataClass();

            $arTicket = $obUsersTickets::getList(
                ["filter" => [
                    '>=UF_DATE'  => new DateTime(),
                    'UF_USER_ID' => $arUser['ID'],
                    'UF_WCHZ_ID' => $this->arUser['UF_WCHZ'],
                    '!ID'        => $this->arParams['ID']
                ]]
            )->fetch();

            if ($arTicket['ID']) {
                $arError[] = 'Для данного пользователя уже имеется активный ЭЧБ №'
                    . $arTicket['UF_NUMBER'] . ' до ' . $arTicket['UF_DATE'];
            }
        }

        if ($arUser['ID'] && empty($arError)) {

            $arUpdate = [
                'UF_USER_ID' => $arUser['ID'],
                'UF_NUMBER'  => $arRequest['NUMBER'],
                'UF_DATE'    => $arRequest['DATE'],
                'UF_WCHZ_ID' => $this->arUser['UF_WCHZ']
            ];

            // обновляем
            if ($arRequest['ID'] > 0)
                $result = $obUsersTickets::update($arRequest['ID'], $arUpdate);
            // создаем новый
            else
                $result = $obUsersTickets::add($arUpdate);

            if ($result->isSuccess())
                LocalRedirect($this->arParams['LIST_URL']);
            else
                $arError[] = implode(', ',$result->getErrorMessages());
        }

        $this->arResult['ERRORS'] = $arError;
    }

    /**
     * Удаление ЭЧБ
     */
    private function deleteTicket() {

        if ($this->arResult['ID'] > 0) {

            $this->includeModules(['highloadblock']);
            $arHLBlock      = HighloadBlockTable::getList(['filter' => ['NAME' => 'UsersTickets']])->fetchRaw();
            $obUsersTickets = HighloadBlockTable::compileEntity($arHLBlock)->getDataClass();
            $obUsersTickets::delete($this->arResult['ID']);
        }

        LocalRedirect($this->arParams['LIST_URL']);
    }
}