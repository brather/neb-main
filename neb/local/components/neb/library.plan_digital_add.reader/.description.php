<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    "NAME" => Loc::getMessage('LIB_PLAN_ADD_FROM_REARED_NAME'),
    "DESCRIPTION" => Loc::getMessage('LIB_PLAN_ADD_FROM_REARED_DESC'),
    "ICON" => "/images/icon.gif",
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "NEB"
    ),
    "COMPLEX" => "Y",
);