<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/prolog.php");

use Bitrix\Main\Loader;

Loader::includeModule('notaext');

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$idItem = intval($_REQUEST['idItem']);

if (!check_bitrix_sessid() or !Bitrix\NotaExt\Ext\DetailGallery\Access::check())
    die(GetMessage("ACCESS_DENIED"));
if ($idItem <= 0)
    die(GetMessage("Empty ID"));

?>
<iframe id="detailGallery-edit-frame" name="detailGallery-edit-frame"
        rel="/local/tools/notaext/detailGallery/editpicFrame.php?idItem=#IDITEM#&sessid=#SESSID#"
        src="/local/tools/notaext/detailGallery/editpicFrame.php?idItem=<?= $idItem ?>&<?= bitrix_sessid_get() ?>"
        class="detailGallery-edit-frame"></iframe>
<!--<iframe src="/_test/cropper-master/demo/" class="detailGallery-edit-frame"></iframe>-->