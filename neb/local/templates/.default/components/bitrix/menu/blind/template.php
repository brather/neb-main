<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if (!empty($arResult)):?>
    <nav class="index-menu">
        <? foreach($arResult as $k => $arItem): ?>
            <a href="<?= $arItem["LINK"] ?>" tabindex="<?= $k+1 ?>"><?= $arItem["TEXT"] ?></a>
        <?endforeach?>
    </nav>
<?endif?>