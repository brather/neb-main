<?

use \Bitrix\Main\Application;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\EventManager;
use \Neb\Main\Helper\IpHelper;
use \Neb\Main\Helper\FileHelper;

EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementAdd",    ["IBlockElementHandler", "OnBeforeIBlockElementUpdateHandler"]);
EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementUpdate", ["IBlockElementHandler", "OnBeforeIBlockElementUpdateHandler"]);

EventManager::getInstance()->addEventHandler("iblock", "OnAfterIBlockElementAdd",     ["IBlockElementHandler", "updateDateModify"]);
EventManager::getInstance()->addEventHandler("iblock", "OnAfterIBlockElementUpdate",  ["IBlockElementHandler", "updateDateModify"]);
EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementDelete", ["IBlockElementHandler", "updateDateModify"]);

EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementAdd",    ["IBlockElementHandler", "convertImageExtension"]);
EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementUpdate", ["IBlockElementHandler", "convertImageExtension"]);

EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementAdd",    ["IBlockElementHandler", "decodeBookId"]);
EventManager::getInstance()->addEventHandler("iblock", "OnBeforeIBlockElementUpdate", ["IBlockElementHandler", "decodeBookId"]);


class IBlockElementHandler
{
    /**
     * Проверка IP адресов на валидность для записи в инфоблок ВЧЗ
     *
     * @param $arFields
     * @return bool
     */
    public static function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        //
        if ($arFields["IBLOCK_ID"] == IBLOCK_ID_WORKPLACES) {
            global $APPLICATION;
            $result = true;

            foreach ($arFields["PROPERTY_VALUES"][24] as $ip) {
                if ($ip['VALUE'] != '') {

                    // диапазон значений ip
                    if (strpos($ip['VALUE'], '-') !== false) {
                        $parts = explode('-', $ip['VALUE']);
                        $leftIp = trim($parts[0]);
                        $rightIp = trim($parts[1]);
                        if (IpHelper::validateIpInRange(
                                $leftIp,
                                '10.0.0.0-10.255.255.255'
                            )
                            && IpHelper::validateIpInRange(
                                $rightIp,
                                '10.0.0.0-10.255.255.255'
                            )
                            && count($parts) === 2
                        ) {
                            $result = true;
                        } else {
                            $result = IpHelper::validateIpRange($ip['VALUE']);
                        }
                        // одиночный ip
                    } else {
                        if (IpHelper::validateIpInRange(
                            $ip['VALUE'],
                            '10.0.0.0-10.255.255.255'
                        )
                        ) {
                            $result = true;
                        } else {
                            $result = IpHelper::validatePrivateIp($ip['VALUE']);
                        }
                    }

                    if ($result == false) {
                        $APPLICATION->throwException("Некорректный ip-адрес");
                        return false;
                    }
                }
            }
        }
    }

    /**
     * Обновление поля UF_DATE_MODIFY в h-блоке NebLibs при изменении данных в ЭЧЗ и Доверенных машинах
     *
     * @param $arFields
     */
    public static function updateDateModify($arFields) {

        $arIBlocks = [IBLOCK_ID_WORKPLACES, IBLOCK_ID_TRUSTED_MACHINES];

        if (is_int($arFields) || in_array($arFields['IBLOCK_ID'], $arIBlocks)) {

            $connection = Application::getConnection();

            // силище-костылище: при событии OnBeforeIBlockElementDelete в $arFields идет ID элемента
            if (is_int($arFields)) {
                $arFields = $connection->query(
                    "SELECT ID, IBLOCK_ID FROM b_iblock_element WHERE ID = $arFields
                            AND ACTIVE = 'Y' AND IBLOCK_ID IN (" . implode(',', $arIBlocks) . ")"
                )->fetchRaw();
            }

            if ($arFields['IBLOCK_ID'] > 0 && $arFields['ID'] > 0) {

                // получение идентификатора библиотеки из NebLibs
                $arMachine = CIBlockElement::GetList(
                    [],
                    [
                        "IBLOCK_ID" => $arFields['IBLOCK_ID'],
                        "ID"        => $arFields['ID'],
                        "ACTIVE"    => 'Y'
                    ],
                    false,
                    false,
                    [
                        'ID',
                        'IBLOCK_ID',
                        ($arIBlocks[0] == $arFields['IBLOCK_ID'] ? 'PROPERTY_LIBRARY' : 'PROPERTY_WORKPLACE_ID.PROPERTY_LIBRARY')
                    ]
                )->Fetch();

                $iLibId = intval($arIBlocks[0] == $arFields['IBLOCK_ID']
                    ? $arMachine['PROPERTY_LIBRARY_VALUE'] : $arMachine['PROPERTY_WORKPLACE_ID_PROPERTY_LIBRARY_VALUE']);

                // обновление даты модификации в NebLibs
                if ($iLibId) {
                    $sDate = (new DateTime())->format('Y-m-d H:i:s');
                    $connection->query("UPDATE neb_libs SET UF_DATE_MODIFY = '$sDate' WHERE ID = $iLibId");
                }
            }
        }
    }

    /**
     * Преобразует mime-тип изображения в расширение файла
     *
     * @param $arFields
     */
    public function convertImageExtension(&$arFields) {

        // обход стандартных полей
        foreach ($arFields as &$arField) {
            if (is_array($arField) && !empty($arField['tmp_name'])) {
                $arField = FileHelper::convertTempImageFileExtension($arField);
            }
        }

        // обход свойств
        foreach ($arFields['PROPERTY_VALUES'] as &$arValues) {
            foreach ($arValues as &$arVal) {
                if (is_array($arVal['VALUE']) && !empty($arVal['VALUE']['tmp_name'])) {
                    $arVal['VALUE'] = FileHelper::convertTempImageFileExtension($arVal['VALUE']);
                }
            }
        }
    }

    /**
     * Преобразует в коллекциях BOOK_ID из urlencode в urldecode в случае сохранения книги в некорректном формате
     * @param $arFields
     */
    public function decodeBookId(&$arFields) {
        if ($arFields["IBLOCK_ID"] == IBLOCK_ID_COLLECTION ) {
            foreach ($arFields["PROPERTY_VALUES"][17] as &$arBook) {
                if (!empty($arBook['VALUE'])) {
                    $arBook['VALUE'] = trim(urldecode($arBook['VALUE']));
                }
            }
        }
    }
}