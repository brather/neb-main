<?php
/**
 * Константы для проекта.
 */

use \Bitrix\Main\Config\Option;

define('CACHE_DIRECTORY', '/cache_function');
define('MARKUP', '/local/templates/.default/markup/');

define('IBLOCK_CODE_COLLECTIONS',      'kollektsii_6');
define('IBLOCK_CODE_LIBRARY',          'biblioteki_4');
define('IBLOCK_CODE_TEXT_BLOCK',       'text_block');
define('IBLOCK_CODE_TEXT_BLOCK_EN',    'text_block_en');
define('IBLOCK_CODE_TRUSTED_MACHINES', 'trusted_machines');

define('IBLOCK_ID_NEWS',             11);
define('IBLOCK_ID_LIBRARY_NEWS',     3);
define('IBLOCK_ID_COLLECTION',       6);
define('IBLOCK_ID_LIBRARY',          4);
define('IBLOCK_PCC_ORDER',           8);
define('IBLOCK_ID_WORKPLACES',       7);
define('IBLOCK_ID_TRUSTED_MACHINES', 17);

define('HIBLOCK_PLAN_DIGIT',              1);
define('HIBLOCK_SEARCHSERS_USERS',        2);
define('HIBLOCK_COLLECTIONS_USERS',       3);
define('HIBLOCK_COLLECTIONS_LINKS_USERS', 5);
define('HIBLOCK_BOOKS_DATA_USERS',        4);
define('HIBLOCK_BOOKMARKS_DATA_USERS',    10);
define('HIBLOCK_NOTES_DATA_USERS',        11);
define('HIBLOCK_QUO_DATA_USERS',          9);
define('HIBLOCK_BOOKS_DATA',              14);

define('HIBLOCK_LIBRARY',      6);
define('HIBLOCK_LIBRARY_CITY', 8);
define('HIBLOCK_LIBRARY_AREA', 7);

define('PLAN_DIGIT_PERIOD_FINISH', 30); // на какой период ставить окончание периода оцифровки

define('UGROUP_ADMIN',               'admin');              // администратор
define('UGROUP_LIB_CODE_ADMIN',      'library_admin');      // администратор библиотеки
define('UGROUP_LIB_CODE_EDITOR',     'library_editor');     // редактор библиотеки
define('UGROUP_LIB_CODE_CONTROLLER', 'library_controller'); // контроллер библиотеки
define('UGROUP_RIGHTHOLDER_CODE',    'rightholder');        // правоообладатель
define('UGROUP_OPERATOR_CODE',       'operator');           // оператор библиотеки
define('UGROUP_OPERATOR_WCHZ',       'operator_wchz');      // оператор ВЧЗ
define('UGROUP_MARKUP_APPROVER',     'markup_approver');    // администратор аналитической росписи
define('UGROUP_MARKUP_EDITOR',       'markup_editor');      // редактор аналитической росписи
define('UGROUP_ASSESSOR',            'assessor');           // Assessor о_о

define('USER_ACTIVE_ENUM_ID', 4);

// Значения свойства 'Тип в слайдере' для книги в подборке
define('COLLECTION_SLIDER_TYPE_COVER_ENUM_ID', 2); //обложка коллекции
define('COLLECTION_SLIDER_TYPE_BASIC_ENUM_ID', 3); //обычная книга
define('COLLECTION_SLIDER_TYPE_COVER_XML_ID', 'cover'); //обложка коллекции
define('COLLECTION_SLIDER_TYPE_BASIC_XML_ID', 'basic'); //обычная книга

# ссылка на закрытый просмотровщик
#spd:https://relar.rsl.ru?id=01003489481
define('CLOSED_VIEWER_LINK', 'spd:https://выдача.нэб.рф/?id=');
define('ADD_COLLECTION_URL', '/local/tools/collections/');
define('DEFAULT_BOOK_PAGE_WIDTH', 800);

define('LIBRARY_DEFAULT_CITY', '592'); //город по-умолчанию для библиотеки. ID берется из HL блока NebLibsCity. 592 == Москва

define('LIB_ID_RIGHTHOLDER', 100000); # ID Библиотеки правообладателей

define('RGB_LIB_ID', 55); # ID Библиотеки РГБ в Инфоблоке IBLOCK_ID=4
/*
Емайл поддержки + Емайл для притензий, используется при заполнении договора, при добавлении пользователя через ЛК Библиотеки
*/
define('SUPPORT_EMAIL', 'support@elar.ru');
define('CLAIM_EMAIL', 'claim@elar.ru');

// Не забыть поправить слайдер выбора диапазона лет в расширенном поиске
// verstka/neb/js/script.js line 972 cont.slider min max
define('SEARCH_BEGIN_YEAR', 900);
define('SEARCH_END_YEAR', date('Y'));

// Статусы пользователей
define('USER_STATUS_VERIFIED', 4);
define('USER_STATUS_REGISTRED', 42);
define('USER_STATUS_VIP', 43);

// Коды highload блоков
define('HIBLOCK_CODE_DIGITIZATION_PLAN', 'PlanDigitization');
define('HIBLOCK_CODE_DIGITIZATION_PLAN_LIST', 'PlanDigitizationList');

// Статусы планов на оцифровку
define('DIGITIZATION_STATUS_ON_AGREEMENT', 'На согласовании оператором НЭБ');
define('DIGITIZATION_STATUS_AGREED', 'Согласовано');
define('DIGITIZATION_STATUS_ON', 'На оцифровке');
define('DIGITIZATION_STATUS_CANCELED', 'Отменено');
define('DIGITIZATION_STATUS_DIGITIZED', 'Оцифровано');
define('DIGITIZATION_STATUS_DONE', 'Выполнено');

define('USER_REGISTER_TYPE_SIMPLE', 37);

define('RIGHTHOLDER_CONTRACT_EMAIL_TEMPLATE_ID', 128);
define('RIGHTHOLDER_RH_VIEW_PRICE_REQUEST', 132);

define('URL_SCHEME', 'http');
define('SITE_HOST', 'xn--90ax2c.xn--p1ai');

define('DEFAULT_PAGE_SIZE', 20);

define('SEARCH_AUTO_COMPLETE_API_URL', '/local/components/exalead/search.form/autocomplete.php');

// Коды режимов отладки
define('EXALEAD_DEBUG_MODE',      $_REQUEST['debug'] == 1); // развернутый запрос к Exalead
define('EXALEAD_FULL_DEBUG_MODE', $_REQUEST['debug'] == 2); // полная строка запроса к Exalead
define('MARKUP_DEBUG_MODE',       $_REQUEST['debug'] == 3); // верстка: показ неминифицированных: css, js и html

// Режимы минификации кода
define('MINIFY_JS_CSS_MODE',        Option::get('neb.main', 'use_minified_css_js') == 'Y');
define('MINIFY_HTML_MODE',          Option::get('neb.main', 'use_minified_html') == 'Y');

// Безопасность
define('SECURITY_SSL_VERIFICATION', Option::get('neb.main', 'security_ssl_verification') == 'Y');
