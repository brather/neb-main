<?
/**
 * @global CMain $APPLICATION
 *
 * @param array $arParams
 * @param array $arResult
 * profile/readers/edit
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>
<div class="profile-container-readers-profile" data-component="profile/readers/edit">
    <h2><?= GetMessage('SETTING'); ?></h2>

    <? if (!empty($arResult['errors'])) {
        foreach ($arResult['errors'] as $error) {
            ShowError($error);
        }


    } ?>
    <?
    if ($arResult['arUser']['EXTERNAL_AUTH_ID'] != 'socservices') {
        ?>

        <? $APPLICATION->IncludeComponent("neb:main.password.reset", "", []); ?>

        <?
    }
    ?>
    <form method="post" name="form1" action="" class="exist-reader-form nrf" enctype="multipart/form-data">
        <script>
            var rulesAndMessages = {
                rules: {},
                messages: {}
            };
        </script>
        <?= $arResult["BX_SESSION_CHECK"] ?>
        <input type="hidden" name="lang" value="<?= LANG ?>"/>
        <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
        <!--div class="b-form_header"><?= GetMessage('GENERAL_CONST'); ?></div-->

        <div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_FIELD_PASSPORT'); ?></div>

        <div class="row">
            <div class="col-md-4 form-group">
                <label for="surname">
                    <?= Loc::getMessage('MAIN_REGISTER_LASTNAME'); ?>
                </label>
                <input type="text"
                       value="<?= $arResult["arUser"]['LAST_NAME'] ?>"
                       id="surname"
                       name="LAST_NAME"
                       class="form-control">
                <script>
                    rulesAndMessages.rules["LAST_NAME"] = {
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["LAST_NAME"] = {
                        maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?>",
                        minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_2_SYMBOLS');?>"
                    };
                </script>
            </div>
            <div class="col-md-4 form-group">
                <label for="firstname">
                    <?= Loc::getMessage('MAIN_REGISTER_FIRSTNAME'); ?>
                </label>
                <input
                        type="text"
                        data-validate="fio"
                        value="<?= $arResult["arUser"]['NAME'] ?>"
                        id="firstname"
                        name="NAME"
                        class="form-control">
                <script>
                    rulesAndMessages.rules["NAME"] = {
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["NAME"] = {
                        maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?>",
                        minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_2_SYMBOLS');?>"
                    };
                </script>
            </div>
            <div class="col-md-4 form-group">
                <label for="midname">
                    <?= Loc::getMessage('MAIN_REGISTER_MIDNAME'); ?>
                </label>
                <input type="text"
                       data-validate="fio"
                       value="<?= $arResult["arUser"]['SECOND_NAME'] ?>"
                       id="midname"
                       data-required="false"
                       name="SECOND_NAME"
                       class="form-control">
                <script>
                    rulesAndMessages.rules["SECOND_NAME"] = {
                        required: false,
                        maxlength: 30,
                        minlength: 2
                    };
                    rulesAndMessages.messages["SECOND_NAME"] = {
                        required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                        maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?>",
                        minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_2_SYMBOLS');?>"
                    };
                </script>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-xs-12">
                <?= Loc::getMessage('MAIN_REGISTER_GENDER'); ?>
            </label>

            <label class="radio-inline">
                <input type="radio"
                       name="PERSONAL_GENDER"
                    <?= ('M' === $arResult['arUser']['PERSONAL_GENDER'] ? 'checked' : '') ?>
                       value="M">
                <i class="lbl"></i>
                <?= Loc::getMessage('MAIN_REGISTER_GENDER_MALE'); ?>
            </label>
            <label class="radio-inline">
                <input type="radio"
                       name="PERSONAL_GENDER"
                    <?= ('F' === $arResult['arUser']['PERSONAL_GENDER'] ? 'checked' : '') ?>
                       value="F">
                <i class="lbl"></i>
                <?= Loc::getMessage('MAIN_REGISTER_GENDER_FEMALE'); ?>
            </label>
        </div>


        <div class="row">
            <div class="form-group col-md-4">
                <label>
                    <?= GetMessage('REGISTER_FIELD_PERSONAL_BIRTHDAY'); ?>
                </label>

                <div class="input-group">
                    <input
                            type="text"
                            class="form-control"
                            value="<?= $arResult['arUser']['PERSONAL_BIRTHDAY'] ?>"
                            id="userbirthday"
                            name="PERSONAL_BIRTHDAY"
                            data-yearsrestrict="12"
                    >
                    <span class="input-group-btn">
	                        <a class="btn btn-default"
                               id="calendarlabel"
                               onclick="BX.calendar({node: 'userbirthday', field: 'userbirthday',  form: '', bTime: false, value: ''});"
                            >
	                            <span class="glyphicon glyphicon-calendar"></span>
	                        </a>
	                    </span>
                </div>
            </div>
        </div>
        <script>
            /*rulesAndMessages.rules["PERSONAL_BIRTHDAY"] = {
                required: false,
                checkDateFormat: true,
                checkDateCorrect: true,
                checkAge: 12,
            };
            rulesAndMessages.messages["PERSONAL_BIRTHDAY"] = {
                checkDateFormat: "<?#=Loc::getMessage('MAIN_REGISTER_DOB_FORMAT');?>",
                checkDateCorrect: "<?#=Loc::getMessage('MAIN_REGISTER_DOB_INCORRECT');?>",
                checkAge: "<?#=Loc::getMessage('MAIN_REGISTER_AGE_WITH_PLACEHOLDER');?>",
            };*/
        </script>

        <div class="form-group">
            <label for="UF_CITIZENSHIP">
                <?= Loc::getMessage('MAIN_REGISTER_CITIZENSHIP'); ?>
            </label>
            <select name="UF_CITIZENSHIP" id="UF_CITIZENSHIP" class="form-control">
                <?
                $arUF_CITIZENSHIP = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'), array("VALUE" => "ASC"));
                if (!empty($arUF_CITIZENSHIP)) {
                    $last = count($arUF_CITIZENSHIP);
                    foreach ($arUF_CITIZENSHIP as $arItem) {
                        if ($arItem['XML_ID'] == $last) {
                            $other = $arItem;
                            continue;
                        }
                        if ($arItem['DEF'] == "Y") {
                            $ruCitizenship = $arItem['ID'];
                        }
                        ?>
                        <option
                                value="<?= $arItem['ID'] ?>"<?= ($arItem['ID']
                            == $arResult['userData']['UF_CITIZENSHIP']
                            || (!$arResult['userData']['UF_CITIZENSHIP']
                                && $arItem['DEF'] == "Y"))
                            ? ' selected="selected"'
                            : ''; ?>><?= $arItem['VALUE'] ?></option>
                        <?php
                    }
                    ?>
                    <option value="<?= $other['ID'] ?>"<?= ($other['DEF'] == "Y") ? ' selected="selected"' : ''; ?>><?= $other['VALUE'] ?></option>

                    <?
                }
                ?>
            </select>

        </div>

        <div class="form-group">
            <label for="PERSONAL_NOTES">
                <?= GetMessage('ABOUT'); ?>
            </label>
            <textarea class="form-control" name="PERSONAL_NOTES"
                      id="PERSONAL_NOTES" data-minlength="2"
                      data-maxlength="800"
                      rows="5"
            ><?= $arResult['arUser']['PERSONAL_NOTES'] ?></textarea>
        </div>

        <div class="nrf-fieldset-title"><?= Loc::getMessage('MAIN_REGISTER_SPECIALIZATION'); ?></div>

        <div class="form-group">
            <label for="mainemployment">
                <?= Loc::getMessage('MAIN_REGISTER_PLACE_OF_EMPLOYMENT'); ?>
            </label>
            <input type="text"
                   value="<?= $arResult["arUser"]['WORK_COMPANY'] ?>"
                   id="mainemployment"
                   name="WORK_COMPANY"
                   class="form-control">
            <script>
                rulesAndMessages.rules["WORK_COMPANY"] = {
                    maxlength: 60,
                    minlength: 2
                };
                rulesAndMessages.messages["WORK_COMPANY"] = {
                    maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                    minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                };
            </script>
        </div>

        <div class="form-group">
            <label for="branchknowledge">
                <?= Loc::getMessage('MAIN_REGISTER_KNOWLEDGE'); ?>
            </label>
            <select name="UF_BRANCH_KNOWLEDGE" id="branchknowledge" class="form-control">
                <option value=""><?= Loc::getMessage('MAIN_REGISTER_SELECT'); ?></option>
                <?
                $arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE'));
                if (!empty($arUF_BRANCH_KNOWLEDGE)) {
                    foreach ($arUF_BRANCH_KNOWLEDGE as $arItem) {
                        ?>
                        <option <?= $arResult["arUser"]['UF_BRANCH_KNOWLEDGE'] == $arItem['ID'] ? 'selected="selected"' : '' ?>
                                value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label for="maineducation">
                <?= Loc::getMessage('MAIN_REGISTER_EDUCATION'); ?>
            </label>
            <select name="UF_EDUCATION" id="maineducation" class="form-control">
                <option value=""><?= Loc::getMessage('MAIN_REGISTER_SELECT'); ?></option>
                <?
                $arUF_EDUCATION = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION'));
                if (!empty($arUF_EDUCATION)) {
                    foreach ($arUF_EDUCATION as $arItem) {
                        ?>
                        <option <?= $arResult["arUser"]['UF_EDUCATION'] == $arItem['ID'] ? 'selected="selected"' : '' ?>
                                value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
            <script>
                rulesAndMessages.rules["UF_EDUCATION"] = {
                    required: false
                };
                rulesAndMessages.messages["UF_EDUCATION"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>"
                };
            </script>
        </div>

        <div class="nrf-fieldset-title"><?= Loc::getMessage('REGISTER_GROUP_SIGN'); ?></div>

        <? /*div class="form-group">
				<label for="userlogin">
					<em class="hint">*</em>
					<?=Loc::getMessage('MAIN_REGISTER_LOGIN');?>
				</label>
				<input
				type="text"
				class="form-control"
				name="LOGIN"
				id="userlogin"
				value="<?=$arResult["arUser"]['LOGIN']?>"
				data-required="true">
				<script>
			        rulesAndMessages.rules["LOGIN"] = {
			            required: true,
			            maxlength: 50,
			            minlength: 3
			        };
			        rulesAndMessages.messages["LOGIN"] = {
			            required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
			            maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
			            minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
			        };
			    </script>
			</div*/ ?>
        <div class="form-group">
            <label for="useremail">
                <em class="hint">*</em>
                <?= Loc::getMessage('MAIN_REGISTER_EMAIL'); ?>
            </label>
            <input
                    type="text"
                    class="form-control"
                    name="EMAIL"
                    id="useremail"
                    value="<?= $arResult["arUser"]['EMAIL'] ?>"
                    data-required="true">
            <script>
                rulesAndMessages.rules["EMAIL"] = {
                    required: true,
                    email: true,
                    maxlength: 50,
                    minlength: 3
                };
                rulesAndMessages.messages["EMAIL"] = {
                    required: "<?=Loc::getMessage('MAIN_REGISTER_REQ');?>",
                    email: "<?=Loc::getMessage('MAIN_REGISTER_EMAIL_FORMAT');?>",
                    maxlength: "<?=Loc::getMessage('MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS');?>",
                    minlength: "<?=Loc::getMessage('MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS');?>"
                };
            </script>
        </div>

        <div class="wrapfield wrapfield-left">
            <?php $APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
                "SHOW_PROFILES" => "Y",
                "ALLOW_DELETE" => "Y"
            ),
                false
            ); ?>
        </div>


        <div class="nrf-fieldset-title"><?= GetMessage('SETTINGS'); ?></div>
        <div class="form-group" data-perpage-widget>
            <label><?= GetMessage('COUNT_SEARCH'); ?></label>
            <div class="perpage-setting-widget">
                <div class="radio">
                    <label>
                        <input type="radio" name="UF_SEARCH_PAGE_COUNT"
                               value="15"
                            <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT']
                                == 15
                            ) { ?> checked <? } ?>/>
                        <span class="lbl">15</span>
                    </label>
                    <br>
                    <label>
                        <input type="radio" name="UF_SEARCH_PAGE_COUNT"
                               value="30"
                            <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT']
                                == 30
                            ) { ?> checked <? } ?>/>
                        <span class="lbl">30</span>
                    </label>
                    <br>
                    <label>
                        <input type="radio" name="UF_SEARCH_PAGE_COUNT"
                               value="45"
                            <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT']
                                == 45
                            ) { ?> checked <? } ?>/>
                        <span class="lbl">45</span>
                    </label>
                </div>
            </div>
        </div>
        <? if ('Y' === $arParams['VERIFY']) { ?>
            <? if ('Y' === $arParams['ALLWAYS_VERIFY']) { ?>
                <input
                        type="hidden"
                        name="USER_VERIFY"
                        value="Y"
                />
            <? } else { ?>
                <div class="form-group">
                    <?
                    $verifyMessage = Loc::getMessage(
                        'REGISTER_FIELD_VERIFY'
                    );
                    if (!empty($arResult['arUser']['UF_ID_REQUEST'])
                        && nebUser::USER_STATUS_VERIFIED
                        !== (integer)$arResult['arUser']['UF_STATUS']
                    ) {
                        $verifyMessage = Loc::getMessage(
                            'REGISTER_REPEAT_VERIFY'
                        );
                        ?>
                        Отправлен запрос на верификацию&nbsp;
                    <? } ?>
                    <label>
                        <input
                                type="checkbox"
                                name="USER_VERIFY"
                                id="cb-verify"
                                value="Y"
                            <?php if (nebUser::USER_STATUS_VERIFIED
                                === (integer)$arResult['arUser']['UF_STATUS']
                            ) {
                                echo 'checked';
                            } ?>
                        >
                        <span class="lbl"><?= $verifyMessage; ?></span>
                    </label>
                </div>
            <? } ?>
        <? } ?>


        <div class="fieldrow nowrap fieldrowaction">
            <div class="fieldcell ">
                <div class="field clearfix">
                    <? if ('Y' === $arParams['VERIFY']) { ?>
                        <button name="action"
                                class="btn btn-primary"
                                value="save"
                                type="submit"><?= GetMessage(
                                'SAVE'
                            ); ?></button>
                        <?= GetMessage('GRB_REGISTRY') ?>
                    <? } else { ?>
                        <button name="action"
                                class="btn btn-primary"
                                value="save"
                                type="submit">Сохранить
                        </button>
                    <? } ?>
                </div>
            </div>
        </div>


        <script>
            $(function () {

                $('input[name="USER_VERIFY"]').on('change', function () {
                    if ($(this).is(':checked')) {
                        $('#hidden_user_verify').remove();
                    } else {
                        $(this).parent().append('<input id="hidden_user_verify" type="hidden" name="USER_VERIFY" value="N">');
                    }
                });

                function getDateAge(dateObj) {
                    var today = new Date();
                    var born = dateObj,
                        age = Math.ceil((today.getTime() - born) / (24 * 3600 * 365.25 * 1000));
                    return age;
                }

                function dateStringToObj(dateString) {
                    var a = dateString.split('.'),
                        dateObj = new Date(a[2], a[1] - 1, a[0]);
                    return dateObj;
                }

                jQuery.validator.addMethod("checkAge", function (value, element, params) {
                        var inputDateObjValue = dateStringToObj(value);
                        return ( getDateAge(inputDateObjValue) > Number(params) );
                    }
                );
                jQuery.validator.addMethod("checkDateFormat", function (value, element, params) {
                        var xxx = value.match(/^\d\d?\.\d\d?\.\d\d\d\d$/);
                        return xxx != null;
                    }
                );
                jQuery.validator.addMethod("checkDateCorrect", function (value, element, params) {
                        var dateObj = dateStringToObj(value),
                            dateObjToStrLeadingZero = ('0' + String(dateObj.getDate())).slice(-2) + '.'
                                + ('0' + String(dateObj.getMonth() + 1)).slice(-2) + '.'
                                + String(dateObj.getFullYear()),
                            dateObjToStr = String(dateObj.getDate()) + '.'
                                + String(dateObj.getMonth() + 1) + '.'
                                + String(dateObj.getFullYear());
                        if (value == dateObjToStr || value == dateObjToStrLeadingZero) {
                            return true
                        }
                    }
                );

                $('form[name="form1"]').validate({
                    rules: rulesAndMessages.rules,
                    messages: rulesAndMessages.messages,
                    ignore: '[type="hidden"]:not(.tovalidate)',
                    errorPlacement: function (error, element) {
                        if (element.attr('type') == "radio") {
                            error.appendTo($(element).closest('.form-group').find('label')[0]);
                        } else if (element.attr('type') == "hidden") {
                            error.appendTo($(element).closest('.setscan'));
                            $('body').scrollTo('.setscan');
                        } else {
                            error.appendTo($(element).closest('.form-group'));
                        }
                        $(element).closest('.form-group').toggleClass('has-error', true);
                    },
                    /* specifying a submitHandler prevents the default submit, good for the demo
                    submitHandler: function() {
                         alert("valid!");
                    },
                    set this class to error-labels to indicate valid fields*/
                    success: function (label) {
                        /* set &nbsp; as text for IE
                        label.html('"&nbsp;"').addClass("resolved");*/
                        label.remove();
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                        $(element).parent().find("." + errorClass).removeClass("resolved");
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
                    }
                });

                $('#userbirthday').on('input change', function () {
                    $(this).closest('form').validate().element(this);
                });

                /* $('[data-mirror]').each(function(){
                     var id = $(this).data('mirror'),
                         thisId = $(this).prop('id');
                     $('#'+id).attr('data-mirrored-by', thisId);
                });*/

                $(document).on('keyup change', '[data-mirrored-by]', function () {
                    var id = $(this).data('mirrored-by'),
                        sameAddresses = $('#livwhereregistered').prop('checked');
                    if (sameAddresses) {
                        $('#' + id).val($(this).val()).trigger('change');
                    }
                });

                $(document).on('click', '#livwhereregistered', function () {
                });
                $(document).on('change', '#livwhereregistered', function () {
                    var checked = $(this).prop('checked');
                    if (checked) {
                        $('[data-mirror]').each(function () {
                            var id = $(this).data('mirror');
                            $(this).val($('#' + id).val());
                        });
                        $('.living-address-group').addClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function () {
                            $(this).attr('readonly', 'true');
                        });
                    } else {
                        $('.living-address-group').removeClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function () {
                            $(this).removeAttr('readonly');
                        });
                    }
                });
                if ($('#livwhereregistered').prop('checked')) {
                    $('#livwhereregistered').trigger('change');
                }
                $(document).on('click', '[data-perpage-set]', function (e) {
                    e.preventDefault();
                    var toggler = $(this),
                        value = toggler.data('perpage-set'),
                        widget = toggler.closest('[data-perpage-widget]'),
                        input = widget.find('[name="UF_SEARCH_PAGE_COUNT"]');

                    widget.find('[data-perpage-set]').toggleClass('current', false);
                    toggler.toggleClass('current', true);
                    $(input).val(value);
                });

            });
        </script>

    </form>
</div><!-- profile-container -->

<!-- ВКЛЮЧИТЬ АПЛОАДИЛКУ -->
<?
$sImage = (intval($arResult['arUser']['PERSONAL_PHOTO']) > 0) ? CFile::GetPath($arResult['arUser']['PERSONAL_PHOTO']) : '';
if (isset($arResult['arUser']['PERSONAL_PHOTO']['SRC'])) {
    $sImage = $arResult['arUser']['PERSONAL_PHOTO']['SRC'];
}
?>
<? $APPLICATION->IncludeComponent(
    "notaext:plupload",
    "profile",
    array(
        "MAX_FILE_SIZE" => "24",
        "FILE_TYPES" => "jpg,jpeg,png",
        "DIR" => "tmp_register",
        "FILES_FIELD_NAME" => "profile_file",
        "MULTI_SELECTION" => "N",
        "CLEANUP_DIR" => "Y",
        "UPLOAD_AUTO_START" => "Y",
        "RESIZE_IMAGES" => "Y",
        "RESIZE_WIDTH" => "110",
        "RESIZE_HEIGHT" => "110",
        "RESIZE_CROP" => "Y",
        "RESIZE_QUALITY" => "98",
        "UNIQUE_NAMES" => "Y",
        "PERSONAL_PHOTO" => $sImage,
        "FULL_NAME" => $arResult['arUser']['NAME'] . ' ' . $arResult['arUser']['LAST_NAME'],
        "USER_EMAIL" => $arResult['arUser']['EMAIL'],
        "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
        "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
        "USER_ID" => $arParams["USER_ID"],
        "FOR_LIBRARY_USER" => "Y",
    ),
    false
); ?>
