<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 * @var array $arParams
 */
?>

<a type="button" class="btn btn-primary btadd closein"
   href="/bitrix/admin/user_edit.php?lang=ru" target="_blank">
        <?= Loc::getMessage('ADD_USER') ?>
</a>


<div class="reader-search clearfix">
    <h3>Поиск пользователей</h3>

    <form>
        <input type="text" name="FIO" class="b-text reader-search__input"
               value="<?= $arParams['FIO'] ?>"
               placeholder="<?= Loc::getMessage('FULL_NAME') ?>">
        <button class="btn btn-primary" type="submit"
                value="<?= Loc::getMessage('FIND') ?>"><?= Loc::getMessage(
                'FIND'
            ) ?></button>
    </form>
</div>

<h3>Список пользователей</h3>
<div class="lk-table">
    <div class="lk-table__column" style="width:50%"></div>
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('NAME') ?>
        </li>
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('DATE_REGISTER') ?>
        </li>
        <li class="lk-table__header-kind">
            <?= Loc::getMessage('DATE_LAST_LOGIN') ?>
        </li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('ACTION') ?></li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['items'] as $user) { ?>
            <ul class="lk-table__row">
                <li class="lk-table__col">
                    <a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $user['ID'] ?>"
                       target="_blank">
                        <?= $user['LAST_NAME'] ?> <?= $user['NAME'] ?> <?= $user['SECOND_NAME'] ?>
                    </a>
                </li>
                <li class="lk-table__col">
                    <?= $user['DATE_REGISTER'] ?>
                </li>
                <li class="lk-table__col">
                    <?= $user['LAST_LOGIN'] ?>
                </li>
                <li class="lk-table__col">
                    <button type="button"
                            class="btn btn-primary <? if ($user['LOCKED']
                                == 1
                            ): ?>unlock<? else: ?>block<? endif; ?>"
                            data-id="<?= $user['ID'] ?>"
                            data-user="<?= $user['LOGIN'] ?>"><? if ($user['LOCKED']
                            == 1
                        ): ?>Разблокировать<? else: ?>Заблокировать<? endif; ?></button>
                </li>
            </ul>
        <? } ?>
    </section>
</div>
<script type="text/javascript">
    /**
     * @todo выпилить из шаблона
     */
    $(function () {

        $('ul.lk-table__row .unlock').on('click', function () {

            var user_id = $(this).attr('data-id');
            var user_login = $(this).attr('data-user');

            if (confirm("Вы точно хотите разблокировать пользователя " + user_login + "?")) {

                $(this).html('Заблокировать');
                $(this).addClass("block");
                $(this).removeClass("unlock");

                /* запрос на активацию пользователя */
                $.ajax({
                    type: "PUT",
                    url: "/rest_api/users/status/",
                    data: "user_id=" + user_id + "&wchzLock=false&token=" + getUserToken(),
                    success: function (data) {
                        if (true === data.success) {
                        }

                    }
                });
            }

        });

        $('ul.lk-table__row .block').on('click', function () {

            var user_id = $(this).attr('data-id');

            if (confirm("Пользователю будет заблокирован доступ в НЭБ на <?=COption::GetOptionInt('neb.main', 'temporary_user_lock_hours')?> часа. Вы можете разблокировать из личного кабинета. Продолжить?")) {

                $(this).html('Разблокировать');
                $(this).removeClass("block");
                $(this).addClass("unlock");


                /* запрос на активацию пользователя */
                $.ajax({
                    type: "PUT",
                    url: "/rest_api/users/status/",
                    data: "user_id=" + user_id + "&wchzLock=true&token=" + getUserToken(),
                    success: function (data) {
                        if (true === data.success) {
                        }

                    }
                });
            }

        })

    })
</script>
<?= $arResult['nav'] ?>
