<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

// сохранение страницы 
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\RslViewer;

$book_id = trim($_REQUEST['book_id']);
$page = trim($_REQUEST['p']);

Loader::includeModule('nota.exalead');

$obRslViewer = new RslViewer($book_id);
/*if ($obRslViewer->isAvailableType() != 'open') {
    exit('Доступ к изданию запрещен');
}*/

//list($width) = explode("x",$obRslViewer->getGeometry($page));
$width = 2000;

// получение картинки в формае JFIF (JPEG)
$rwJpg = $obRslViewer->getDocumentPage(floor($width), $page);

// очистка буффера
MainHelper::clearBuffer();

// заставляем браузер показать окно сохранения файла
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="' . $book_id . '_' . $page . '.jpg"');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');

echo $rwJpg;