<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160914132103 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
UPDATE
lbc_maindivision
    set ParentId = 209695, Level = 2
WHERE Id IN (
  202237,
  202235,
  202222,
  202213,
  202195,
  202185,
  202184,
  202171,
  202153,
  202137,
  202130,
  202123,
  202083,
  202054,
  202036,
  202012,
  201998,
  201997
);
        ");

        $this->addSql("
UPDATE
  lbc_maindivision
set ParentId = 209696, Level = 2
WHERE Id IN (
  204400,
  204350,
  204278,
  204209,
  204156,
  204155,
  203523,
  203520,
  203286,
  203237,
  203184,
  203111,
  203037,
  202979,
  202978,
  202259,
  202249
);
        ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
