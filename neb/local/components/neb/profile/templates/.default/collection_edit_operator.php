<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__DIR__.'/main_library.php'); // Для того, что б не копировать один и тот же текст
Loc::loadMessages(__FILE__);
?>
<div class="collection_edit_library">
    <?
        $nebUser = new nebUser();
        $lib = $nebUser->getLibrary();
    ?>
    <?
    $APPLICATION->IncludeComponent(
        "neb:collections.add",
        "library_profile",
        array(
            "IBLOCK_ID" => IBLOCK_ID_COLLECTION,
            "LIBRARY_ID" => $lib['ID'],
            'COLLECTION' => $arResult["VARIABLES"]["ID"]
        ),
        $component
    );
    ?>
</div>
