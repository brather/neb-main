<?php
$MESS['DIGITIZATION_STAT_TITLE']      = 'Statistics by orders count';
$MESS['DIGITIZATION_STAT_DATE_FROM']  = 'Date from';
$MESS['DIGITIZATION_STAT_DATE_TO']    = 'Date to';

$MESS['DIGITIZATION_STAT_LIBRARY']    = 'Library';
$MESS['DIGITIZATION_STAT_COUNT']      = 'Total count';
$MESS['DIGITIZATION_STAT_DIGIT']      = 'Digitization count';
$MESS['DIGITIZATION_STAT_EXCEL']      = 'Export to .xls';
$MESS['DIGITIZATION_STAT_SHOW']       = 'Show statistics';