<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<a class="b-btcreateuser b-btcreate btn btn-primary" href="new/">Добавить читателя</a>
<br />
<br />
<?
$APPLICATION->IncludeComponent(
    "neb:readers.list",
    "",
    Array(
        "IBLOCK_ID"         => Bitrix\NotaExt\Iblock\IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY),
        "LIBRARY_ID"        => $arResult['LIBRARY']["ID"],
        "WORKPLACE_ID"      => $arResult['USER']["UF_WCHZ"],
        "CACHE_TIME"        => $arParams["CACHE_TIME"],
        "ACTION_UNBIND_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['readers_unbind'],
        "ACTION_EDIT_URL"   => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['readers_edit'],
        "ACTION_BLOCK_URL"  => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['readers_block'],
        "COUNTONPAGE"       => 30,
    ),
    $component
);