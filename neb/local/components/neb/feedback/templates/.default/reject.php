<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * Отклонение заявки
 */

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->IncludeComponent(
    "neb:feedback.reject",
    ".default",
    array(
        "COMPONENT_TEMPLATE"  => ".default",
        'FEEDBACK_URL'        => $arParams['SEF_FOLDER'],
        'FEEDBACK_REJECT_URL' => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['reject']
    ),
    $component
);