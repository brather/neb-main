<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 23.01.2016
 * Time: 16:00
 */

use \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\UI\PageNavigation,
    \Bitrix\Main\Loader,
    \Bitrix\Main\UserTable,
    \Bitrix\Main\FileTable,
    \Bitrix\Main\Entity\ExpressionField,

    \Bitrix\Posting\ListRubricTable as ListRubricTable,
    \Bitrix\Posting\PostingTable as PostingTable,
    \Bitrix\Posting\EmailTable as PostingEmailTable,
    \Bitrix\Posting\FileTable as PostingFileTable,

    \Neb\Main\Helper\MainHelper;


/**
 * Class NotificationHistory
 */
class NotificationHistory extends CBitrixComponent
{
    private
        $iPageCount = 10,
        $iRubric    = 0,
        $bExport    = false;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $this->bExport = $_REQUEST['export'] == 'Y';

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->_includeModules();

        $this->_getUserLibrary();
        $this->_getLibraries();
        $this->_getNotificationRubric();
        $this->_getSenders();
        $this->_getData();
        $this->_getFiles();
        $this->_historyExport();

        $this->includeComponentTemplate();
    }

    /**
     * Подключает модули
     * @throws \Bitrix\Main\LoaderException
     */
    private function _includeModules() {
        Loader::includeModule('neb.main');
        Loader::includeModule('iblock');
    }

    /**
     * Получение библиотеки пользователя, в случае если он еявляется её работником
     */
    private function _getUserLibrary() {
        $obUser = new nebUser();
        if ($obUser->isLibrary()) {
            $this->arResult['LIBRARY'] = $obUser->getLibrary();
        }
    }

    /**
     * Получение библиотек-адресатов (только для оператора)
     */
    private function _getLibraries() {

        if (!empty($this->arResult['LIBRARY']))
            return ;

        $rsLibs = \CIBlockElement::GetList(
            ['NAME' => 'ASC'],
            [
                'IBLOCK_ID'       => IBLOCK_ID_LIBRARY,
                'ACTIVE'          => 'Y',
                '!PROPERTY_EMAIL' => false
            ],
            false,
            false,
            [
                'ID',
                'IBLOCK_ID',
                'NAME',
                'PROPERTY_EMAIL'
            ]
        );
        while($arFields = $rsLibs->Fetch()) {
            $this->arResult['LIBRARIES'][] = [
                'NAME'  => $arFields['NAME'],
                'EMAIL' => $arFields['PROPERTY_EMAIL_VALUE']
            ];
        }
    }

    /**
     * Получение идентификатора рубрики "Оповещения библиотек"
     *
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getNotificationRubric() {

        $arNotification = ListRubricTable::getList([
            'select'  => ['ID'],
            'filter'  => ['CODE' => 'library_notification'],
        ])->fetchRaw();

        $this->iRubric = intval($arNotification['ID']);

        if ($this->iRubric <= 0) {
            die('Рассылка "Оповещения библиотек" не найдена!');
        }
    }

    /**
     * Формирование массива отправителей писем
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getSenders() {

        // шаг 1: сборка массива используемых e-mail адресов
        $arFilter = [
            '=rubric.LIST_RUBRIC_ID' => $this->iRubric
        ];
        // для библиотекаря предфильтр по его постингам
        if (!empty($this->arResult['LIBRARY'])) {
            $arFilter['=ID'] = $this->_getLibrariesPostings([$this->arResult['LIBRARY']['PROPERTY_EMAIL_VALUE']]);
        }
        $arItemPosting = PostingTable::getList([
            'order'   => ['ID' => 'DESC'],
            'select'  => ['FROM_FIELD'],
            'filter'  => $arFilter,
            'runtime' => array(
                'rubric' => array(
                    'data_type' => 'Bitrix\Posting\RubricTable',
                    'reference' => array('=this.ID' => 'ref.POSTING_ID'),
                    'join_type' => 'INNER'
                )
            ),
        ]);
        while ($arItem = $arItemPosting->fetch()) {
            $this->arResult['SENDERS'][$arItem['FROM_FIELD']] = $arItem['FROM_FIELD'];
        }

        if (empty($this->arResult['SENDERS']))
            return [];

        // шаг 3: выборка пользователей
        $res = UserTable::getList([
            'order'  => ['LAST_NAME' => 'ASC', 'NAME' => 'ASC', 'SECOND_NAME' => 'ASC'],
            'select' => ['ID', 'EMAIL', 'LOGIN', 'LAST_NAME', 'NAME', 'SECOND_NAME'],
            'filter' => [[
                    'LOGIC'=>'OR',
                    ['=EMAIL' => array_values($this->arResult['SENDERS'])],
                    ['=LOGIN' => array_values($this->arResult['SENDERS'])]
                ],
                'ACTIVE' => 'Y'
            ],
        ]);
        while ($arFields = $res->fetch()) {

            if (!empty($this->arResult['SENDERS'][$arFields['EMAIL']]))
                $key = $arFields['EMAIL'];
            elseif (!empty($this->arResult['SENDERS'][$arFields['LOGIN']]))
                $key = $arFields['LOGIN'];
            else
                continue;

            $this->arResult['SENDERS'][$key]
                = trim($arFields['LAST_NAME'] . ' ' . $arFields['NAME'] . ' ' . $arFields['SECOND_NAME']) ? : $key;
        }
    }

    /**
     * Получение массива идентификаторов рассылок по e-mail-ам отправителей
     * @param $arEmail
     * @return array
     */
    private static function _getLibrariesPostings($arEmail) {

        $arResult = [];

        if (empty($arEmail))
            return [];

        $arItemPosting = PostingEmailTable::getList([
            'order'   => ['ID' => 'ASC'],
            'select'  => ['POSTING_ID'],
            'filter'  => ['EMAIL' => $arEmail],
            'runtime' => [new ExpressionField('POSTING_ID', 'DISTINCT(POSTING_ID)')]
        ]);
        while ($arItem = $arItemPosting->fetch()) {
            $arResult[] = $arItem['POSTING_ID'];
        }

        return $arResult;
    }

    /**
     * Получение массива получаетелей писем
     *
     * @return string
     */
    private static function _getPostingEmails($arPosting) {

        $arResult = [];

        if (empty($arPosting))
            return $arResult;

        $arItemPosting = PostingEmailTable::getList([
            'order'   => ['ID' => 'DESC'],
            'select'  => ['POSTING_ID', 'EMAIL'],
            'filter'  => ['POSTING_ID' => $arPosting],
        ]);
        while ($arItem = $arItemPosting->fetch()) {
            $arResult[$arItem['POSTING_ID']][trim($arItem['EMAIL'])] = trim($arItem['EMAIL']);
        }

        return $arResult;
    }

    /**
     * Формирование запроса к БД
     *
     * @return string
     */
    private function _getQuery() {

        $arQueryParams = [
            'order'   => ['ID' => 'DESC'],
            'select'  => ['ID', 'DATE_SENT', 'BODY', 'FROM_FIELD'],
            'filter'  => ['=rubric.LIST_RUBRIC_ID' => $this->iRubric],
            'runtime' => array(
                'rubric' => array(
                    'data_type' => 'Bitrix\Posting\RubricTable',
                    'reference' => array('=this.ID' => 'ref.POSTING_ID'),
                    'join_type' => 'INNER'
                )
            )
        ];

        // по номеру заявки
        if ($number = intval($_REQUEST['number'])) {
            $arQueryParams['filter']['=ID'] = $number;
        }

        // по датам
        if (!empty($_REQUEST['date_send_from'])) {
            $arQueryParams['filter']['>=DATE_SENT'] = new DateTime($_REQUEST['date_send_from']);
        }
        if (!empty($_REQUEST['date_send_to'])) {
            $arQueryParams['filter']['<=DATE_SENT'] = new DateTime($_REQUEST['date_send_to']);
        }

        // по библиотеке-адресату (для оператора)
        if (!empty($_REQUEST['library'])) {
            $arQueryParams['filter']['=ID'] = $this->_getLibrariesPostings($_REQUEST['library']);
        }

        // по библиотеке-адресату (для библиотеки)
        if (!empty($this->arResult['LIBRARY'])) {
            $arQueryParams['filter']['=ID'] = $this->_getLibrariesPostings($this->arResult['LIBRARY']['PROPERTY_EMAIL_VALUE']);
            $arQueryParams['filter']['!DATE_SENT'] = false;
        }

        // по отправителю
        if (!empty($_REQUEST['sender'])) {
            $arQueryParams['filter']['=FROM_FIELD'] = $_REQUEST['sender'];
        }

        // по теме или тексту письма
        if (!empty($_REQUEST['query'])) {
            $arQueryParams['filter'][] = array(
                'LOGIC' => 'OR',
                '%=SUBJECT' => '%'.$_REQUEST['query'].'%',
                '%=BODY'    => '%'.$_REQUEST['query'].'%'
            );
        }

        return $arQueryParams;
    }

    /**
     * Получение данных по оповещениям
     */
    private function _getData() {

        // получение SQL-запроса
        $arQueryParams = $this->_getQuery();

        // формирование постраничной навигации
        if (!$this->bExport) {

            $iCount = PostingTable::getList($arQueryParams)->getSelectedRowsCount();

            $obPageNavigation = new PageNavigation('page');
            $obPageNavigation->allowAllRecords(true)->initFromUri();
            $obPageNavigation->setRecordCount($iCount)->setPageSize($this->iPageCount);
            $this->arResult['NAV'] = $obPageNavigation;

            if (strpos($_SERVER['REQUEST_URI'], 'page-all') === false) {
                $arQueryParams['limit']  = $obPageNavigation->getLimit();
                $arQueryParams['offset'] = $obPageNavigation->getOffset();
            }
        }

        // получение писем
        $arItemPosting = PostingTable::getList($arQueryParams);
        while ($arItem = $arItemPosting->fetch()) {

            if ($arItem['DATE_SENT'] instanceof DateTime) {
                $arItem['DATE_SENT'] = $arItem['DATE_SENT']->toString();
            }
            $this->arResult['ITEMS'][$arItem['ID']] = $arItem;
        }

        // получение адресатов писем
        if (!empty($this->arResult['ITEMS'])) {
            $arPosting = array_keys($this->arResult['ITEMS']);
            $arPostingEmails = $this->_getPostingEmails($arPosting);
            foreach ($arPostingEmails as $iPosting => $arEmails) {
                foreach ($arEmails as $sEmail) {
                    $this->arResult['ITEMS'][$iPosting]['EMAIL'][] = $sEmail;
                }
            }
        }
    }

    /**
     * Получение прикрепленных к постингам файлов
     *
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _getFiles() {

        if ($this->bExport)
            return;

        $arFileMap = [];

        // формирование массива идентификаторов постинкгов
        $arPosting = array_keys($this->arResult['ITEMS']);

        if (empty($arPosting))
            return;

        // получение идентификаторов файлов
        $arItemPosting = PostingFileTable::getList(['filter' => ['POSTING_ID' => $arPosting]]);
        while ($arItem = $arItemPosting->fetch()) {
            $arFileMap[$arItem['FILE_ID']] = $arItem['POSTING_ID'];
        }

        if (empty($arFileMap))
            return;

        // получение информации по файлам из таблицы b_file
        $rsFile = FileTable::getList(['filter' => ['ID' => array_keys($arFileMap)]]);
        while ($arItem = $rsFile->fetch()) {
            $arItem['SRC'] = CFile::GetFileSRC($arItem);
            $this->arResult['ITEMS'][$arFileMap[$arItem['ID']]]['FILES'][$arItem['ID']] = $arItem;
        }
    }

    /**
     * Экспорт в Эксель
     */
    public function _historyExport()
    {
        if (!$this->bExport)
            return;

        // Выполняет сбпрос буффера
        MainHelper::clearBuffer();

        // Генерация Excel-файла
        require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/PHPExcel.php');
        require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/PHPExcel/IOFactory.php');

        $excel = new \PHPExcel();
        $excel->getProperties()
            ->setTitle('Оповещения')
            ->setSubject('Оповещения');
        $sheet = $excel->setActiveSheetIndex(0);
        $sheet
            ->setCellValue('A1', '№')
            ->setCellValue('B1', 'Дата отправки')
            ->setCellValue('C1', 'Текст письма')
            ->setCellValue('D1', 'Отправитель');

        $arHeadStyle = array('font' => array('bold' => true));
        $sheet->getStyle('A1')->applyFromArray($arHeadStyle);
        $sheet->getStyle('B1')->applyFromArray($arHeadStyle);
        $sheet->getStyle('C1')->applyFromArray($arHeadStyle);
        $sheet->getStyle('D1')->applyFromArray($arHeadStyle);

        $sheet->getColumnDimension('A')->setWidth(6);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(70);
        $sheet->getColumnDimension('D')->setWidth(30);

        if (empty($this->arResult['LIBRARY'])) {
            $sheet->setCellValue('E1', 'Получатели');
            $sheet->getStyle('E1')->applyFromArray($arHeadStyle);
            $sheet->getColumnDimension('E')->setWidth(30);
        }

        $row = 2;
        foreach ($this->arResult['ITEMS'] as $iPosting => $arItem) {
            $sheet
                ->setCellValue('A' . $row, $arItem['ID'])
                ->setCellValue('B' . $row, $arItem['DATE_SENT'])
                ->setCellValue('C' . $row, strip_tags(html_entity_decode($arItem['BODY'])))
                ->setCellValue('D' . $row, $arItem['FROM_FIELD']);

            if (empty($this->arResult['LIBRARY'])) {
                $sheet->setCellValue('E' . $row, implode(', ', $arItem['EMAIL']));
            }

            $row++;
        }

        $sheet->calculateColumnWidths();
        $excel->getActiveSheet()->setTitle('Statistics');
        $excel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'. 'history-export-' . microtime(true) .'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save('php://output');

        exit;
    }
}