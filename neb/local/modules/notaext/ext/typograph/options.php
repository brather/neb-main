<?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CModule::IncludeModule('iblock');

// Получаем список доступных инфоблоков
$arrIBlock = array();
$iBlockRes = CIBlock::GetList(array('sort' => 'asc'));
while ($iBlockItem = $iBlockRes->Fetch()) {
    $arrIBlock[$iBlockItem['ID']] = '[' . $iBlockItem['ID'] . '] ' . $iBlockItem['NAME'];
}

// Теперь получаем для каждого выбранного инфоблока типографируемые свойства, если выбран хотя бы 1 инфоблок
$iblockProps = array();
$iBlocks = \COption::GetOptionString('notaext', 'typograph_iblock_list');
if (strlen($iBlocks) > 0) {
    $iBlocksArray = unserialize($iBlocks);
    if (!empty($iBlocksArray) and is_array($iBlocksArray)) {
        foreach ($iBlocksArray as $iblockID) {
            $propsResult = CIBlockProperty::GetList(
                array('SORT' => 'ASC'),
                array(
                    'IBLOCK_ID' => $iblockID,
                    'USER_TYPE' => 'HTML'
                )
            );

            $iblockProps[$iblockID] = array(
                'PREVIEW_TEXT' => 'Текст для анонса',
                'DETAIL_TEXT' => 'Детальный текст',
            );

            while ($iBlockPropItem = $propsResult->Fetch()) {
                $iblockProps[$iblockID][$iBlockPropItem['ID'] . '|' . $iBlockPropItem['CODE']] = '[' . $iBlockPropItem['CODE'] . '] ' . $iBlockPropItem['NAME'];
            }
        }
    }
}

$arOptions = array(
    'arTabs' => array(
        array(
            'DIV' => 'edit1',
            'TAB' => 'Настройки расширения',
            'ICON' => '',
            'TITLE' => 'Настройки расширения'
        ),
        array(
            'DIV' => 'edit2',
            'TAB' => 'Настройка правил',
            'ICON' => '',
            'TITLE' => 'Настройка правил'
        ),
        array(
            'DIV' => 'edit3',
            'TAB' => 'Отладка',
            'ICON' => '',
            'TITLE' => 'Отладка онлайн'
        )
    ),

    'arOptions' => array(
        'iblock_list' => array(
            #'GROUP'		=> 'IBLOCK_TYPES',
            'TITLE' => GetMessage("IBLOCK"),
            'TYPE' => 'MSELECT',
            'VALUES' => \CExtOptions::prepareArrayForSelect($arrIBlock),
            'SORT' => 1,
            'NOTES' => GetMessage("IBLOCK_NOTE"),
            'REFRESH' => 'Y'
        ),
    )
);

if (!empty ($iblockProps)) {
    // Добавляем группу
    $arOptions['arGroups'] = array(
        'IBLOCK_TYPES' => array(
            'TITLE' => GetMessage("IBLOCS_PROPS"),
            'SORT' => 1
        ),
        'PROPERTIES' => array(
            'TITLE' => GetMessage("IBLOCS_PROPS"),
            'SORT' => 2
        ),
    );

    // И добавляем в группу списки со свойствами
    foreach ($iblockProps as $iblockID => $propsArray) {
        $arOptions['arOptions']["iblock_{$iblockID}_props"] = array(
            'TITLE' => GetMessage("IBLOCK_PROP", array('IB_NAME' => $arrIBlock[$iblockID])),
            'GROUP' => 'PROPERTIES',
            'TYPE' => 'MSELECT',
            'VALUES' => \CExtOptions::prepareArrayForSelect($propsArray),
        );
    }
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/local/modules/notaext/ext/typograph/lib/emt.php");
$typograph = new EMTypograph();
$tret_list = $typograph->get_trets_list();

/*
Формируем опции типографа
*/
foreach ($tret_list as $tret) {
    $tret_obj = $typograph->get_tret($tret);

    $rhtml = "";
    $arOptions['arGroups'][$tret] = array('TITLE' => $tret_obj->title . ' (' . $tret . ')', 'TAB' => '1', 'SORT' => $i);

    foreach ($tret_obj->rules as $rulename => $rule) {
        $checked = "";
        if ($options) {
            if ($options[$tret][$rulename]) $checked = "checked";
        } else {
            if (!$rule['disabled']) $checked = "checked";
        }
        #explode('_', $tret)[2].'|'. - убираем т.к. название опции не прохоит по длинне 50 символов
        $arOptions['arOptions'][$rulename] = array(
            'GROUP' => $tret,
            'TAB' => '1',
            'TITLE' => htmlspecialcharsEx($rule['description']) . ' (' . $rulename . ')',
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => !empty($checked) ? 'Y' : 'N',
        );
    }

}

$val = COption::GetOptionString('notaext', 'typograph_mdash_test');

/*
Отладка
*/

global $APPLICATION;

if ($_REQUEST['debug'] == 'Y') {
    $APPLICATION->RestartBuffer();

    $debuglist = "";

    $typograph->debug_on();
    $typograph->log_on();
    if (!empty($_REQUEST['inputdata'])) {
        $typograph->set_text($_REQUEST['inputdata']);

        $arTypographOptions = Bitrix\NotaExt\Ext\typograph\EmtOptions::getOptions();
        $typograph->setup($arTypographOptions);
        $result = $typograph->apply();

        function draw_debug_item($tret_title, $tret_name, $rule_title, $rule_name, $result, $something_changed, $result_raw)
        {
            $rule_name = htmlspecialcharsEx($rule_name);
            $tret_name = htmlspecialcharsEx($tret_name);
            $tret_title = htmlspecialcharsEx($tret_title);
            $rule_title = htmlspecialcharsEx($rule_title);

            $h = "<div " . (!$something_changed ? " style='display:none' class='debughidden'" : "") . ">";
            $h .= " <h3>$tret_title " . ($tret_name ? "<sub><small>$tret_name</small></sub>" : "") . "</h3>";
            $h .= "  <div  style='margin-left:30px;'>";
            if ($rule_name) $h .= "    <h4>$rule_title" . ($rule_name ? "<sub><small>$rule_name</small></sub>" : "") . "</h4>";
            $code = htmlspecialchars($result);
            $code_raw = htmlspecialchars($result_raw);
            $h .= "    Результат: <div style='margin-left:30px;'>$result</div><br />";
            $h .= "    HTML-код: <pre style='margin-left:30px;'>$code</pre>";
            $h .= "    <div class='rawhtmlcode' style='display:none'>Не обработанный HTML-код: <pre style='margin-left:30px;'>$code_raw</pre></div>";
            $h .= "  </div>";
            $h .= "</div>";
            return $h;
        }

        $prev = "";
        foreach ($typograph->debug_info as $debug) {
            if ($debug['tret']) {
                $tr = $typograph->get_tret($debug['class']);
                $tt = "Трэт: " . $tr->title;
                $tn = $debug['class'];
                $rt = $tr->rules[$debug['place']]['description'];
                $rn = $debug['place'];
            } else {
                $rt = "";
                $rn = "";
                $tn = "";
                switch ($debug['place']) {
                    case "init":
                        $tt = "До обработки типографом";
                        break;
                    case "safe_blocks":
                        $tt = "Включение безопасных блоков";
                        break;
                    case "unsafe_blocks":
                        $tt = "Возврат безопасных блоков";
                        break;
                    case "safe_tag_chars":
                        $tt = "Сохранение содержимого тэгов";
                        break;
                    case "unsafe_tag_chars":
                        $tt = "Восстановление содержимого тэгов";
                        break;
                    case "clear_special_chars":
                        $tt = "Замена всех специальных символов";
                        break;
                    default:
                        $tt = $debug['place'];
                }
            }
            $debuglist .= draw_debug_item($tt, $tn, $rt, $rn, $debug['text'], $prev != $debug['text'], $debug['text_raw']);
            $prev = $debug['text'];
        }

        $logs = "";
        foreach ($typograph->logs as $log) {
            $prefix = "";
            if ($log['class']) {
                $tr = $typograph->get_tret($log['class']);
                $prefix = "Трэт " . $tr->title . " (" . $log['class'] . ") ";
            }
            $data = "";
            if ($log['data']) {
                $data = (is_string($log['data']) ? $log['data'] : print_r($log['data'], true));
            }
            $logs .= $prefix . $log['info'] . ($data ? ": " . $data : "") . "\n";
        }


        $html = $result;
        $code = htmlspecialchars($html);

    }
    $typograph_style = $typograph->get_style();
    ?>
    <script>
        $(function () {
            $('#showlog').click(function () {
                $('#log').toggle();
            });
            $('#showfulldebug').click(function () {
                $('.debughidden').toggle();
            });
            $('#showrawdebug').click(function () {
                $('.rawhtmlcode').toggle();
            });

            $('#showoptions').click(function () {
                $('#typooptions').toggle();
                if ($('#typooptions').is(":visible")) {
                    $('#useoptions').val('1');
                } else {
                    $('#useoptions').val('0');
                }
            });
            $('.opttrets').click(function () {
                if ($(this).is(':checked')) {
                    $(".opt" + $(this).attr('data')).prop('checked', true);
                } else {
                    $(".opt" + $(this).attr('data')).prop('checked', false);
                }
            });
            $('#applytypograph1').click(function () {

            });
        });
    </script>
    <style>
        <?=$typograph_style?>
    </style>
    <br/>
    Результат: <br/>
    <div id="result"><?= $html ?></div>
    <br/>
    <br/>
    HTML code: <br/>
    <span style="font-family: monospace; font-weight: bold" id="htmlcode"><?= $code ?></span>
    <br>
    <br>
    <hr>
    <br>
    <h2 id="debug">Отладка</h2> <a href="#debug" id="showfulldebug">показать всё</a> <a href="#debug" id="showrawdebug">показать
        необработанные рездуьтаты</a>
    <div style='margin-left:30px;'>
        <?= $debuglist ?>
    </div>
    <br>
    <hr>
    <br>
    <h2>Логи</h2> <a href="#log" id="showlog">Посмотреть</a>
    <pre id="log" style="display: none"><?= $logs ?></pre>
    <?

    exit();
}
\CUtil::InitJSCore(array("jquery"));
ob_start();

?>
    <div id="typograf_debug">
        <textarea name="inputdata" style="width: 500px; height: 200px;" id="inputdata"
                  placeholder="Введите текст для типографирования">"Сохранить поисковый запрос" при наведении становится более ярким</textarea>
        <br/>
        <input type="submit" id="applytypograph" value="Сотворить"/>
        <div id="debug_result"></div>

        <script type="text/javascript">
            $(function () {
                $("#applytypograph").click(function () {
                    ShowWaitWindow();
                    $.post("<?=$APPLICATION->GetCurPageParam('debug=Y', array('debug'))?>", {inputdata: $('#inputdata').val()}, function (data) {
                        $('#debug_result').html(data);
                        $('#wait_window_div').hide();
                    });
                    return false;
                });
            });
        </script>
    </div>
<?
$content = ob_get_contents();
ob_end_clean();

$arOptions['arOptions']['debug'] = array(
    'TAB' => '2',
    'TITLE' => 'Высота',
    'TYPE' => 'CUSTOM',
    'VALUE' => $content,
    'SORT' => 3,
);
