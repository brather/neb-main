<?
namespace Bitrix\NotaExt;
/**
 *
 * Class Sort
 * @package Bitrix\NotaExt
 * Класс, для сортировки массива по ключам.
 */

class Sort
{

    /*     производим сортировку массива по полю  */
    private static $DateSortField = '';

    private function SetdatesortField($field)
    {
        self::$DateSortField = $field;
    }

    // Сортировать массив по полю типа - дата
    static function SortFieldTypeDate($arr, $field)
    {
        self::SetdatesortField($field);
        uasort($arr, array(self, 'SortFieldTypeD'));
        return $arr;
    }

    private function SortFieldTypeD($f1, $f2)
    {
        if (strtotime($f1[self::$DateSortField]) < strtotime($f2[self::$DateSortField])) return 1;
        elseif (strtotime($f1[self::$DateSortField]) > strtotime($f2[self::$DateSortField])) return -1;
        else return 0;
    }

    // Сортировать массив по полю типа - число
    static function SortFieldTypeNum($arr, $field, $sort = 'desc')
    {
        self::SetdatesortField($field);
        if ($sort == 'desc')
            uasort($arr, array(self, 'SortFieldTypeNumberDesc'));
        else
            uasort($arr, array(self, 'SortFieldTypeNumber'));
        return $arr;

    }

    private function SortFieldTypeNumber($f1, $f2)
    {
        if ($f1[self::$DateSortField] > $f2[self::$DateSortField]) return 1;
        elseif ($f1[self::$DateSortField] < $f2[self::$DateSortField]) return -1;
        else return 0;
    }

    private function SortFieldTypeNumberDesc($f1, $f2)
    {
        if ($f1[self::$DateSortField] < $f2[self::$DateSortField]) return 1;
        elseif ($f1[self::$DateSortField] > $f2[self::$DateSortField]) return -1;
        else return 0;
    }
}
