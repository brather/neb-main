<?
/**
 * Удаление отчета
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!TblEntities::checkAccess()) {
    die('Permissions denied!');
}

echo TblReport::deleteReport($_REQUEST['id']);
