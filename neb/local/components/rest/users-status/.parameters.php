<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'   => '/',
                'VARIABLES' => array(
                    'token',
                    'active',
                    'wchzLock',
                    'verify',
                    'user_id',
                    'preVerify',
                    'libraryId',
                ),
                'TYPE'      => array('PUT'),
            ),
        ),
    ),
); 