<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if (!$USER->IsAuthorized()) {
    exit();
}

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('highloadblock');
Loader::includeModule('nota.journal');
Loader::includeModule('nota.exalead');

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
$result = ['data' => []];
$book_id = trim(urldecode($arRequest['BOOK_ID']));

try {

    if ($_SERVER['REQUEST_METHOD'] == 'POST' and check_bitrix_sessid()
        and !empty($book_id)
    ) {

        $obUser = new nebUser();
        if (!$obUser->IsAuthorized()) {
            throw new Exception('Вы не авторизованы!');
        }

        //region Вернем id блока с планами оцифровки
        $idHLBlockDigitization = '';
        $connection = Application::getConnection();
        $sql = "SELECT ID,NAME FROM b_hlblock_entity WHERE NAME='" . HIBLOCK_CODE_DIGITIZATION_PLAN . "';";
        $recordset = $connection->query($sql);
        while($record = $recordset->fetch()) {
            $idHLBlockDigitization = $record['ID'];
        }
        //endregion
        $idNewStatus = '';
        if(empty($arRequest['STATUS'])) {
            //region Получим данные по свойству "Статус"
            //region Возвращаем id самого свойства
            $rsData = CUserTypeEntity::GetList(
                array("SORT" => "ASC"),
                array("ENTITY_ID" => "HLBLOCK_" . $idHLBlockDigitization, "FIELD_NAME" => "UF_REQUEST_STATUS")
            );
            $idPropStatus = '';
            if($arRes = $rsData->Fetch()) {
                $idPropStatus = $arRes["ID"];
            }
            //endregion
            //region Возвращаем массив значений свойства с id

            $arPropStatus = array();
            if($idPropStatus != '') {
                $obUserFieldEnum = new CUserFieldEnum();
                $rsStatus = $obUserFieldEnum->GetList(['SORT' => 'ASC'], ['USER_FIELD_ID' => $idPropStatus]);
                while ($arStatus = $rsStatus->Fetch()) {
                    if($arStatus['VALUE'] == DIGITIZATION_STATUS_ON_AGREEMENT) {
                        $idNewStatus = $arStatus['ID'];
                    }
                    $arPropStatus[$arStatus['ID']] = $arStatus;
                }
            }
            //endregion
            //endregion
        } else {
            $idNewStatus = $arRequest['STATUS'];
        }

        $Library = $obUser->getLibrary();

        $hlblock = HighloadBlockTable::getById($idHLBlockDigitization)->fetch();
        $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        $rsData = $entity_data_class::getList(
            array(
                'select' => array('ID'),
                'filter' => array('UF_LIBRARY' => $Library['ID'], 'UF_EXALEAD_BOOK' => $book_id)
            )
        );

        if ($arData = $rsData->Fetch()) {
            throw new Exception(
                "Издание уже добавлено в план оцифровки(Номер заявки {$arData['ID']})"
            );
        }

        $dt = new DateTime();
        if (!empty($arRequest['date'])) {
            $dtPlus = new DateTime($arRequest['date'], 'd.m.Y');
        } else {
            $dtPlus = new DateTime();
            $dtPlus->add('+' . PLAN_DIGIT_PERIOD_FINISH . ' days');
        }

        $arFields = array(
            //'UF_LIBRARY'        => $Library['PROPERTY_LIBRARY_LINK_VALUE'],
            'UF_PLAN_ID'        => $arRequest['plan_id'],
            'UF_LIBRARY'        => $Library['ID'],
            'UF_EXALEAD_BOOK'   => $book_id,
            'UF_DATE_ADD'       => $dt,
            'UF_DATE_FINISH'    => $dtPlus,
            'UF_EXALEAD_PARENT' => $arRequest['PARENT_ID'],
            'UF_COMMENT'        => $arRequest['comment'],
            'UF_NEB_USER'       => $obUser->GetID(),
            'UF_REQUEST_STATUS' => $idNewStatus,
            'UF_ADD_TYPE'       =>
                UGROUP_LIB_CODE_ADMIN === nebUser::getCurrent()->getRole()
                    ? 'library'
                    : ('user' === nebUser::getCurrent()->getRole()
                    ? 'user'
                    : null),
        );

        $client = new \Nota\Exalead\SearchClient();
        $query = new \Nota\Exalead\SearchQuery();
        $query->getById($book_id);
        $eBook = $client->getResult($query);
        if ($eBook = $client->getResult($query)) {
            $arFields['UF_BOOK_NAME'] = $eBook['title'];
            $arFields['UF_BOOK_AUTHOR'] = $eBook['authorbook'];
        }


        if (!$ID = $entity_data_class::add($arFields)->getId()) {
            throw new Exception(
                'Не удалось добавить книгу в план. Попробуйте позднее.'
            );
        }
        $result['data']['id'] = $ID;
    } else {
        throw new Exception('Нерпавильный метод запроса');
    }
} catch (Exception $e) {
    $result['error'] = true;
    $result['messages'] = [$e->getMessage()];
}

MainHelper::showJson($result);