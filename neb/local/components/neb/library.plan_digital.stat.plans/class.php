<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\Iblock\ElementTable,
    \Neb\Main\Helper\MainHelper;

/**
 * Class StatisticsPlansComponent
 */
class StatisticsPlansComponent extends CBitrixComponent
{
    private $arDigitizationPlan;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent() {

        $this->includeModules(['iblock', 'highloadblock']);
        $this->getDigitizationPlan();
        $this->getDigitizationBooks();
        $this->getLibraries();
        $this->getResult();
        $this->getExport();

        $this->includeComponentTemplate();
    }

    /**
     * Подключение модулей
     *
     * @param $arModules - массив модулей
     * @throws \Bitrix\Main\LoaderException
     */
    private static function includeModules($arModules) {
        foreach ($arModules as $sModule) {
            if (!Loader::includeModule($sModule))
                die('Module ' . $sModule . 'not installed!');
        }
    }

    private function getDigitizationPlan() {

        $arResult = $arFilter = [];

        $sDateFrom = $_REQUEST['DIGITIZATION']['DATE_FROM'];
        $sDateTo   = $_REQUEST['DIGITIZATION']['DATE_TO'];

        if (empty($sDateFrom) && $sDateTo)
            return $arResult;

        if (!empty($sDateFrom))
            $arFilter['>=UF_DATE_AGREEMENT'] = $sDateFrom;

        if (!empty($sDateTo))
            $arFilter['<=UF_DATE_AGREEMENT'] = $sDateTo;

        $iHIblock             = self::getIdBlock(HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
        $digitizationPlanList = self::getDataClassHL($iHIblock);

        $libraryData = $digitizationPlanList::getList([
            'order'  => ['ID' => 'DESC'],
            'select' => ['ID', 'UF_LIBRARY'],
            'filter' => $arFilter,
        ]);
        while ($arFields = $libraryData->fetch())
            $arResult[$arFields['ID']] = $arFields;

        $this->arDigitizationPlan = $arResult;
    }

    /**
     * Получение количества книг по планам оцифровкки
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    private function getDigitizationBooks() {

        $arResult = [];

        $arPlanList = array_keys($this->arDigitizationPlan);

        if (empty($arPlanList))
            return $arResult;

        $iHIblock         = self::getIdBlock(HIBLOCK_CODE_DIGITIZATION_PLAN);
        $digitizationPlan = self::getDataClassHL($iHIblock);
        $arEnum           = self::getUserFieldsEnum('HLBLOCK_' . $iHIblock, 'UF_REQUEST_STATUS');

        $digitizationPlanData = $digitizationPlan::getList([
            'order'  => ['ID' => 'DESC'],
            'select' => ['ID', 'UF_PLAN_ID', 'UF_REQUEST_STATUS'],
            'filter' => ['UF_PLAN_ID' => $arPlanList]
        ]);
        while ($arFields = $digitizationPlanData->fetch()) {

            if ($arEnum[$arFields['UF_REQUEST_STATUS']]['VALUE'] == 'Оцифровано')
                $this->arDigitizationPlan[$arFields['UF_PLAN_ID']]['DIGIT']++;

            $this->arDigitizationPlan[$arFields['UF_PLAN_ID']]['COUNT']++;
        }
    }

    /**
     * Получение библиотек
     *
     * @throws \Bitrix\Main\ArgumentException
     */
    private function getLibraries() {

        $arLibs = [];

        foreach ($this->arDigitizationPlan as $arPlan)
            $arLibs[$arPlan['UF_LIBRARY']] = $arPlan['UF_LIBRARY'];

        if (empty($arLibs))
            return $arLibs;

        $rsElements = ElementTable::getList([
            'order'  => ['NAME' => 'ASC'],
            'select' => ['ID', 'NAME'],
            'filter' => ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ID' => array_values($arLibs), 'ACTIVE' => 'Y'],
        ]);
        while ($arFields = $rsElements->fetch())
            $this->arResult['ITEMS'][$arFields['ID']] = $arFields;
    }

    /**
     * Формирование результирующего массива
     */
    private function getResult() {
        foreach ($this->arDigitizationPlan as $iPlan => $arPlan) {
            if (empty($this->arResult['ITEMS'][$arPlan['UF_LIBRARY']]) || (empty($arPlan['COUNT']) && empty($arPlan['DIGIT'])))
                continue;
            $this->arResult['ITEMS'][$arPlan['UF_LIBRARY']]['COUNT'] += intval($arPlan['COUNT']);
            $this->arResult['ITEMS'][$arPlan['UF_LIBRARY']]['DIGIT'] += intval($arPlan['DIGIT']);
        }
    }

    /**
     * Экспорт данных в Excel
     */
    private function getExport() {

        if ($_REQUEST['DIGITIZATION']['EXPORT'] != 'Y')
            return;

        $arExcel = [];

        $arExcel['META']['TITLE'] = 'Статистика по планам оцифровки';
        $arExcel['META']['DESCRIPTION'] =  'Статистика по планам оцифровки '
            . $_REQUEST['DIGITIZATION']['DATE_FROM'] . ' - ' . $_REQUEST['DIGITIZATION']['DATE_TO'];
        $arExcel['META']['FILENAME'] = $arExcel['META']['TITLE'];

        $arExcel['ITEMS'][] = [
            'A' => 'Библиотека',
            'B' => 'Количество изданий в планах',
            'C' => 'Количество оцифрованных изданий',
        ];
        foreach ($this->arResult['ITEMS'] as $arItems) {
            $arExcel['ITEMS'][] = [
                'A' => $arItems['NAME'],
                'B' => $arItems['COUNT'],
                'C' => $arItems['DIGIT'],
            ];
        }

        static::export2Excel($arExcel);
    }

    /** ============================================ СЛУЖЕБНЫЕ МЕТОДЫ =============================================== */

    /**
     * Экспорт данных в Excel
     */
    private static function export2Excel($arExcel) {

        // Выполняет сбпрос буффера
        MainHelper::clearBuffer();

        // Генерация Excel-файла
        require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/PHPExcel.php');
        require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/PHPExcel/IOFactory.php');

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator('НЭБ.РФ')
            ->setLastModifiedBy('НЭБ.РФ')
            ->setTitle($arExcel['META']['TITLE'])
            ->setSubject($arExcel['META']['TITLE'])
            ->setDescription($arExcel['META']['DESCRIPTION']);

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Статистика по планам оцифровки');

        foreach ($arExcel['ITEMS'] as $row => $arItem)
            foreach ($arItem as $sCode => $sValue)
                $objPHPExcel->getActiveSheet()->setCellValue($sCode . ($row + 1), $sValue);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$arExcel['META']['FILENAME'].'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

        exit;
    }

    /**
     * Функция возвращает id HL блока по его коду
     *
     * @param $codeHL - код HL блока
     * @return mixed
     */
    private function getIdBlock($codeHL) {
        $digitizationPlanHLBlock = HighloadBlockTable::getList(['filter' => ['NAME' => $codeHL]])->fetchRaw();
        return $digitizationPlanHLBlock['ID'];
    }

    /**
     * Функция возвращает сущность HL блока
     *
     * @param $iblockId - id HL блока
     * @return \Bitrix\Main\Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    private function getDataClassHL($iblockId) {
        $hlblock   = HighloadBlockTable::getById($iblockId)->fetch();
        $dataClass = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $dataClass;
    }

    /**
     * Получениет значения пользовательского свойства типа 'список'
     *
     * @param $sEntity - сущность
     * @param $sCode - символьный код свойства
     * @return array - массив значений списка
     */
    private static function getUserFieldsEnum($sEntity, $sCode)
    {
        $arResult = [];

        // поиск ID свойства типа 'список'
        $iField = \CUserTypeEntity::GetList(
            ['ID' => 'ASC'],
            ['ENTITY_ID' => $sEntity, 'FIELD_NAME' => $sCode, 'USER_TYPE_ID' => 'enumeration']
        )->Fetch();

        if ($iField <= 0)
            return $arResult;

        // выборка значений свойства типа 'список'
        $obUserFieldEnum  = new \CUserFieldEnum();
        $rsUserFieldEnum = $obUserFieldEnum->GetList(
            ['SORT' => 'ASC'],
            ['USER_FIELD_ID' => $iField]
        );
        while ($arFields = $rsUserFieldEnum->Fetch())
            $arResult[$arFields['ID']] = $arFields;

        return $arResult;
    }
}