<?
$pagename='';
?>
<? include("header_main.php"); ?>

<section class="mainsection" >			
	<div class="b-portalinfo clearfix wrapper">
		<div class="b-portalinfo_num right">
			<span class="b-portalinfo_lb">Доступно</span>
			<span class="b-portalinfo_numbers"><span class="b-portalinfo_number iblock">1</span><span class="b-portalinfo_number iblock">3</span><span class="b-portalinfo_number mr  iblock">1</span><span class="b-portalinfo_number iblock">7</span><span class="b-portalinfo_number iblock">5</span><span class="b-portalinfo_number mr  iblock">4</span></span>
			<span class="b-portalinfo_lb">электронных книг&nbsp;</span>
		</div>
		<div class="b-portalinfo_num right">
			<span class="b-portalinfo_lb">Доступно</span>
			<span class="b-portalinfo_numbers"><span class="b-portalinfo_number iblock">2</span><span class="b-portalinfo_number mr  iblock">5</span><span class="b-portalinfo_number iblock">7</span><span class="b-portalinfo_number iblock">8</span><span class="b-portalinfo_number mr  iblock">4</span><span class="b-portalinfo_number iblock">8</span><span class="b-portalinfo_number iblock">0</span><span class="b-portalinfo_number mr  iblock">2</span></span>
			<span class="b-portalinfo_lb">записей каталогов</span>
		</div>
		<a href="blind_index.php" accesskey="0" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
			<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
			Национальная Электронная Библиотека
		</a>
		<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>


	</div><!-- /.b-portalinfo -->


	<form action="" class="searchform">
		<div class="b-search wrapper">
			<div class="b-search_field">
				<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
				<div class="b_search_set clearfix">
					<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>
					<div class="checkwrapper">
						<input class="checkbox js_btnsub" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
					</div>
				</div> <!-- /.b_search_set -->

			</div>
		</div> <!-- /.b-search -->
	</form>			

</section>	
<section class="b-bookinfo">
	<div class="wrapper">
			<h2 class="b-mainslider_tit"><a href="blind_collections.php" class="b-collectlink" title="коллекции">коллекции</a></h2>
			<div class="b-mainslider">
					<div>
						<div class="sliderinner">
							<div class="b-mainslider_photo iblock"><img class="loadingimg" src="../pic/pic_15.jpg" alt=""></div>
							<div class="b-mainslider_descr iblock">
								<h3>500 рекомендованных книг для школьников от национальной электронной библиотеки</h3>
							</div>
						</div>
					</div>
					<div>
						<div class="sliderinner">
							<div class="b-mainslider_photo iblock"><img class="loadingimg" src="../pic/pic_15.jpg" alt=""></div>
							<div class="b-mainslider_descr iblock">
								<h3>100 рекомендованных книг для школьников от национальной электронной библиотеки</h3>
							</div>
						</div>
					</div>
				</div> <!-- /.b-mainslider -->
				<div class="b-bookinfo_wrapper">
			<div class="b-bookinfo_new iblock">
				<div class="b-bookinfo_tit">
				<h2><a href="blind_new.php" title="новые поступления">новые поступления</a></h2> <span class="b-searchpage_lnk">Май: добавлено <strong>1405 книги</strong> </span>
				</div>
				<ul class="b-bookboard_list">
						<li>
								<div class="b_popular_descr">
									<h4>Сумма технологии</h4>
									<span class="b-autor">Автор: Станислав Лем</span>
									<div class="b-result_sorce_info"><em>Источник:</em> Рязанская областная библиотека</div>

								</div>
						</li>
						<li>
								<div class="b_popular_descr">
									<h4>Гроздья гнева</h4>
									<span class="b-autor">Автор: Джон Стейнбек</span>
									<div class="b-result_sorce_info"><em>Источник:</em> Рязанская областная библиотека</div>
								</div>
						</li>
						<li>
								<div class="b_popular_descr">
									<h4>Сумма технологии</h4>
									<span class="b-autor">Автор: Станислав Лем</span>
									<div class="b-result_sorce_info"><em>Источник:</em> Рязанская областная библиотека</div>

								</div>
						</li>
						
					</ul>
				</div>
				<div class="b-bookinfo_popular iblock">
					<div class="b-bookinfo_tit"><h2><a href="blind_pop.php" title="популярные издания">популярные издания</a></h2></div>
					<ul class="b_bookpopular">
						<li>
							<div class="b_popular_descr">
								<h4>Сумма технологии</h4>
								<span class="b-autor">Автор: Станислав Лем</span>
								<div class="b-result_sorce_info"><em>Источник:</em> Рязанская областная библиотека</div>

							</div>
						</li>
						<li>
							<div class="b_popular_descr">
								<h4>Гроздья гнева</h4>
								<span class="b-autor">Автор: Джон Стейнбек</span>
								<div class="b-result_sorce_info"><em>Источник:</em> Рязанская областная библиотека</div>
							</div>
						</li>
					</ul> <!-- /.b_bookpopular -->
				</div> <!-- /.b-bookinfo_popular -->
			</div> <!-- /.b-bookinfo_wrapper -->
			<h2 class="b-mainslider_tit"><a href="blind_advantages.php" title="преимущества нэб">преимущества нэб</a></h2>
			<div class="b-mainslider">
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock"><img class="loadingimg" src="../pic/pic_14.jpg" alt=""></div>
						<div class="b-mainslider_descr iblock">
							<h3>Единая система поиска по полным текстам всех фондов</h3>
							<p>Вводите любой текст в поисковую строку и получает наиболее релевантные выдачи по полнотекстовому индексу (не только совпадения слов, но и семантика). </p>
						</div>
					</div>
				</div>
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock"><img class="loadingimg" src="../pic/pic_14.jpg" alt=""></div>
						<div class="b-mainslider_descr iblock">
							<h3>Слайд 2</h3>
							<p>Вводите любой текст в поисковую строку и получает наиболее релевантные выдачи по полнотекстовому индексу (не только совпадения слов, но и семантика). </p>
						</div>
					</div>
				</div>
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock"><img class="loadingimg"  src="../pic/pic_14.jpg" alt=""></div>
						<div class="b-mainslider_descr iblock">
							<h3>Слайд 3</h3>
							<p>Вводите любой текст в поисковую строку и получает наиболее релевантные выдачи по полнотекстовому индексу (не только совпадения слов, но и семантика). </p>
						</div>
					</div>
				</div>
			</div> <!-- /.b-mainslider -->
		</div><!-- /.wrapper -->
	</section>
</div><!-- /.homepage -->
<? include("footer.php"); ?>
</body>
</html>