<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h2><?echo $arResult["title"] ?></h2>
<?
if(is_array($arResult["item"])):
foreach($arResult["item"] as $arItem):?>

<div class="rss-item">
	<?if(strlen($arItem["enclosure"]["url"])>0):?>
		<img class="rss-item-img" src="<?=$arItem["enclosure"]["url"]?>" alt="<?=$arItem["enclosure"]["url"]?>" />
	<?endif;?>
	<?if(strlen($arItem["pubDate"])>0):?>
		<p class="rss-item-data"><?=CIBlockRSS::XMLDate2Dec($arItem["pubDate"], FORMAT_DATE)?></p>
	<?endif;?>
	<?if(strlen($arItem["link"])>0):?>
		<a  class="rss-item-title" href="<?=$arItem["link"]?>"><?=$arItem["title"]?></a>
	<?else:?>
		<p class="rss-item-title"><?=$arItem["title"]?></p>
	<?endif;?>
	<p class="rss-item-desc">
	<?echo $arItem["description"];?>
	</p>
</div>
<?endforeach;
endif;?>