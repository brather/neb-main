<div class="blindmenu">
    <div class="access wrapper">
        <a href="#" title="Настройки версии для слабовидящих"><p class="a-settings">Показать настройки для слабовидящих</p></a>

        <div class="popped">
            <h2>Настройки шрифта:</h2>

            <p class="choose-font-family">
                Выберите шрифт 
                <a href="#" rel="sans-serif" id="sans-serif" class="font-family" title="Шрифт Arial">Arial</a> 
                <a href="#" id="serif" rel="serif" class="font-family" title="Шрифт Times New Roman">Times New Roman</a>
            </p>
            <p class="choose-letter-spacing">
            Интервал между буквами <span>(Кернинг)</span>: 
            <a href="#" rel="spacing-small" id="spacing-small" class="letter-spacing" title="Стандартный интервал между буквами">Стандартный</a> 
            <a href="#" id="spacing-normal" class="letter-spacing" rel="spacing-normal" title="Средний интервал между буквами">Средний</a> 
            <a href="#" id="spacing-big" class="letter-spacing" rel="spacing-big" title="Большой интервал между буквами">Большой</a></p>

            <h2>Выбор цветовой схемы:</h2>
            <ul class="choose-colors">
                <li id="color1"><a href="#" rel="color1" title="Цветовая схема черным по белому"><span>&mdash;</span>Черным по бежевому</a></li>
                <li id="color2"><a href="#" rel="color2" title="Цветовая схема белым по черному"><span>&mdash;</span>Белым по черному</a></li>
                <li id="color3"><a href="#" rel="color3" title="Цветовая схема темно-синим по голубому"><span>&mdash;</span>Желтым по синему</a></li>
                <li id="color4"><a href="#" rel="color4" title="Цветовая схема коричневым по бежевому"><span>&mdash;</span>Коричневым по бежевому</a></li>
                <li id="color5"><a href="#" rel="color5" title="Цветовая схема зеленым по темно-коричневому"><span>&mdash;</span>Зеленым по темно-коричневому</a></li>
            </ul>
            <p class="saveit">
                <a href="#" class="closepopped" title="Закрыть панель настройки"><span>Закрыть панель</span></a> 
                <a href="#" class="default" title="Вернуть стандартные настройки"><span>Вернуть стандартные настройки</span></a>
            </p>
        </div>

    </div>
</div>