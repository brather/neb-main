<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>

<h2 class="mode"><?= Loc::getMessage('MAIN_REGISTER_TITLE'); ?></h2>

<? ShowError($arResult["strProfileError"]); ?>
<?
if ($arResult['DATA_SAVED'] == 'Y') {
    $message
        = 'MAIN_REGISTER_SUCCESS_NOTE' . ((true
            === @$arResult['USER_VERIFIED'])
            ? '_ESIA' : '');
    ShowNote(Loc::getMessage($message));
}
?>
<p class="note">
    <?= Loc::getMessage('MAIN_REGISTER_NOTE'); ?>
</p>

<form method="post" action="<?= $arResult["FORM_TARGET"] ?>"
      class="profile-form nrf"
      name="readerselfupdatetofullaccess" enctype="multipart/form-data" id="regform">
    <?
    if (count($arResult["ERRORS"]) > 0) {
        foreach ($arResult["ERRORS"] as $key => $error) {
            if (intval($key) == 0 && $key !== 0) {
                $arResult["ERRORS"][$key] = str_replace(
                    "#FIELD_NAME#",
                    "&quot;" . GetMessage("REGISTER_FIELD_" . $key)
                    . "&quot;", $error
                );
            }
        }
        ShowError(
            '<br /><br />' . implode("<br />", $arResult["ERRORS"])
            . '<br /><br />'
        );
    }
    ?>
    <?
    if ($arResult["BACKURL"] <> '') {
        ?>
        <input type="hidden" name="backurl"
               value="<?= $arResult["BACKURL"] ?>"/>
        <?
    }

    $date = explode('.', $arResult['arUser']['PERSONAL_BIRTHDAY']);
    foreach ($date as $dates) {
        $arDate[] = intval($dates);
    }
    ?>
    <?= $arResult["BX_SESSION_CHECK"] ?>
    <input type="hidden" name="lang" value="<?= LANG ?>"/>
    <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
    <input type="hidden" class="input" name="LOGIN"
           value="<?= $arResult['arUser']['LOGIN'] ?>">
    <input type="hidden" id='mail' class="input"
           value="<?= $arResult['arUser']['EMAIL'] ?>" name="EMAIL">
    <? include(__DIR__ . '/../form-parts/user-info.php') ?>

    <hr>

    <!-- ВКЛЮЧИТЬ АПЛОАДИЛКУ -->
    <?
        $sImage = (intval($arResult['arUser']["UF_SCAN_PASSPORT1"]) > 0)? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT1"]):'';
        if (isset($arResult['arUser']["UF_SCAN_PASSPORT1"]["SRC"]))
        {
            $sImage = $arResult['arUser']["UF_SCAN_PASSPORT1"]["SRC"];
        }
    ?>
    <?$APPLICATION->IncludeComponent(
        "notaext:plupload",
        "scan_passport1",
        array(
            "MAX_FILE_SIZE" => "24",
            "FILE_TYPES" => "jpg,jpeg,png",
            "DIR" => "tmp_register",
            "FILES_FIELD_NAME" => "profile_file",
            "MULTI_SELECTION" => "N",
            "CLEANUP_DIR" => "Y",
            "UPLOAD_AUTO_START" => "Y",
            "UNIQUE_NAMES" => "Y",
            "PERSONAL_PHOTO" => $sImage,
            "FULL_NAME" => $arResult['arUser']['NAME'].' '.$arResult['arUser']['LAST_NAME'],
            "USER_EMAIL" => $arResult['arUser']['EMAIL'],
            "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
            "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
            "USER_ID" => $arParams["USER_ID"],
        ),
        false
    );?>

    <!-- ВКЛЮЧИТЬ АПЛОАДИЛКУ -->
    <?
        $sImage = (intval($arResult['arUser']["UF_SCAN_PASSPORT2"]) > 0)? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT2"]):'';
        if (isset($arResult['arUser']["UF_SCAN_PASSPORT2"]["SRC"]))
        {
            $sImage = $arResult['arUser']["UF_SCAN_PASSPORT2"]["SRC"];
        }
    ?>
    <?$APPLICATION->IncludeComponent(
        "notaext:plupload",
        "scan_passport2",
        array(
            "MAX_FILE_SIZE" => "24",
            "FILE_TYPES" => "jpg,jpeg,png",
            "DIR" => "tmp_register",
            "FILES_FIELD_NAME" => "profile_file",
            "MULTI_SELECTION" => "N",
            "CLEANUP_DIR" => "Y",
            "UPLOAD_AUTO_START" => "Y",
            "UNIQUE_NAMES" => "Y",
            "PERSONAL_PHOTO" => $sImage,
            "FULL_NAME" => $arResult['arUser']['NAME'].' '.$arResult['arUser']['LAST_NAME'],
            "USER_EMAIL" => $arResult['arUser']['EMAIL'],
            "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
            "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
            "USER_ID" => $arParams["USER_ID"],
        ),
        false
    );?>
    
    <hr>

    <input type="hidden" name="termsreaded" value="false"/>
    <input type="hidden" name="termsagreed" value="false"/>
    <script>
        $(function(){
            if ( $.cookie('termsagreed') == 'true' ) {
                $('[name="termsagreed"]').val('true');
                $('[name="readerselfupdatetofullaccess"] button[type="submit"]').removeAttr('disabled');
            }
            /*$.cookie('termsagreed', 'true', {expires: 7, path: '/'});*/
        });
    </script>

    <p>
        <?= Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP'); ?> 
        <a href="#" data-agree-dialog>
            <?= Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP_TERMS'); ?>
        </a>
    </p>

    <p>
        <button type="submit" class="btn btn-primary" disabled>
            <?= Loc::getMessage('MAIN_REGISTER_UPDATE'); ?>
        </button>
        <button class="btn btn-default" id="cancelbutton">Отмена</button>
    </p>

    <!-- <div class="checkwrapper">
        <p class="offerta-declare">
            <?= Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP'); ?>
            <a href="/local/components/neb/registration/templates/.default/ajax_agreement.php"
               target="_blank" class="popup_opener ajax_opener closein"
               data-width="955" data-height="auto">
                <?= Loc::getMessage(
                    'MAIN_REGISTER_AGREE_FOR_POPUP_TERMS'
                ); ?>
            </a>
        </p>
    </div> -->

    <!-- <div class="fieldrow nowrap fieldrowaction">
        <div class="fieldcell clearfix">
            <div class="field clearfix divdisable b-form-btn-block">
                <button name="save" type="submit"
                        value="1"
                        class="btn btn-primary"><?= Loc::getMessage(
                        'MAIN_REGISTER_UPDATE'
                    ); ?></button>
            </div>
        </div>
    </div> -->
    <input type="hidden" name="save" value="save" />
</form>
