<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serialization;

/**
 * LbcLocationplanimages
 *
 * @ORM\Table(name="lbc_locationplanimages")
 * @ORM\Entity
 */
class LbcLocationplanimages extends AbstractLbcImages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="LocationPlanId", type="integer", nullable=false)
     */
    private $locationplanid;

    /**
     * Set locationplanid
     *
     * @param integer $locationplanid
     * @return LbcLocationplanimages
     */
    public function setLocationplanid($locationplanid)
    {
        $this->locationplanid = $locationplanid;

        return $this;
    }

    /**
     * Get locationplanid
     *
     * @return integer 
     */
    public function getLocationplanid()
    {
        return $this->locationplanid;
    }
}
