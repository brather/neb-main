<?namespace Intervolga\RemoteUserClient;

use \Bitrix\Main\SystemException;
use Intervolga\RemoteUserClient\Tools\ExceptionHandler;
use Intervolga\RemoteUserClient\Tools\Request;
use \Intervolga\RemoteUserClient\Tools\SoapClient;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Works with remote user server.
 *
 * @package Intervolga\RemoteUserClient
 */
class User
{
	/**
	 * Returns fields and UF's, that can be saved locally.
	 *
	 * @return string[]
	 */
	protected static function getThisSiteFields()
	{
		return array(
			"LOGIN",
			"PASSWORD",
			"CONFIRM_PASSWORD",
			"EMAIL",
			"NAME",
			"SECOND_NAME",
			"LAST_NAME",
			"CHECKWORD",
			"~CHECKWORD_TIME",
			"ACTIVE",
			"CONFIRM_CODE",
			"LID",
			"USER_IP",
			"USER_HOST",
			"AUTO_TIME_ZONE",
			"GROUP_ID",
			"ID",
			"RESULT",
			"EXTERNAL_AUTH_ID",
			"XML_ID",
			"DATE_REGISTER",
			"TIMESTAMP_X",
			"LAST_LOGIN",
		);
	}

	/**
	 * Returns extra fields, that must be uploaded to remote site.
	 *
	 * @return string[]
	 */
	protected static function getExtraRemoteSiteFields()
	{
		return array(
			"ID",
			"LOGIN",
			"PASSWORD",
			"CONFIRM_PASSWORD",
			"EMAIL",
			"NAME",
			"SECOND_NAME",
			"LAST_NAME",
			"EXTERNAL_AUTH_ID",
			"XML_ID",
			"DATE_REGISTER",
			"TIMESTAMP_X",
			"LAST_LOGIN",
		);
	}

	/**
	 * Returns \CUser::GetList arParams value for uploading user to remote server.
	 * @see \CUser::GetList()
	 * @see upload()
	 *
	 * @return array
	 */
	public static function getUploadParams()
	{
		return array(
			"FIELDS" => array(
				"ID", "ACTIVE", "LAST_LOGIN", "LOGIN", "EMAIL", "NAME",
				"LAST_NAME", "SECOND_NAME", "TIMESTAMP_X", "PERSONAL_BIRTHDAY",
				"DATE_REGISTER", "PERSONAL_PROFESSION", "PERSONAL_WWW", "PERSONAL_ICQ",
				"PERSONAL_GENDER", "PERSONAL_PHOTO", "PERSONAL_PHONE", "PERSONAL_FAX",
				"PERSONAL_MOBILE", "PERSONAL_PAGER", "PERSONAL_STREET", "PERSONAL_MAILBOX",
				"PERSONAL_CITY", "PERSONAL_STATE", "PERSONAL_ZIP", "PERSONAL_COUNTRY", "PERSONAL_NOTES",
				"WORK_LOGO", "WORK_COMPANY", "WORK_DEPARTMENT", "WORK_POSITION", "WORK_WWW",
				"WORK_PHONE", "WORK_FAX", "WORK_PAGER", "WORK_STREET", "WORK_MAILBOX", "WORK_CITY",
				"WORK_STATE", "WORK_ZIP", "WORK_COUNTRY", "WORK_PROFILE", "WORK_NOTES", "ADMIN_NOTES",
				"XML_ID", "LAST_NAME", "SECOND_NAME", "EXTERNAL_AUTH_ID", "LAST_ACTIVITY_DATE", "TITLE",
			),
			"SELECT" => array("UF_*"),
		);
	}

	/**
	 * Filters user array and leaves only remote site fields and UF's
	 *
	 * @param array $arUser user record (@see \CUser::Add, @see \CUser::Update)
	 *
	 * @return array
	 */
	protected static function getFieldsForRemoteSite($arUser)
	{
		$arResult = array();

		foreach ($arUser as $key => $value)
		{
			if (!in_array($key, static::getThisSiteFields()))
			{
				$arResult[$key] = $value;
			}
		}
		foreach (static::getExtraRemoteSiteFields() as $key)
		{
			if ($arUser[$key])
			{
				$arResult[$key] = $arUser[$key];
			}
		}

		return $arResult;
	}

	/**
	 * Filters user array and leaves only fields and UF's, that can be saved locally.
	 *
	 * @param array $arUser user record (@see \CUser::Add, \CUser::Update)
	 *
	 * @return array
	 */
	public static function getFieldsForThisSite($arUser)
	{
		$arResult = array();

		foreach ($arUser as $key => $value)
		{
			if (in_array($key, static::getThisSiteFields()))
			{
				$arResult[$key] = $value;
			}
		}

		return $arResult;
	}

	/**
	 *
	 * @return string[]
	 */
	public static function getFileFields()
	{
		global $USER_FIELD_MANAGER;
		static $arReturn = array();
		if (!$arReturn)
		{
			$arReturn = array("PERSONAL_PHOTO", "WORK_LOGO");
			$arUserFields = $USER_FIELD_MANAGER->GetUserFields("USER", 0, "ru");
			foreach ($arUserFields as $arUserField)
			{
				if ($arUserField["USER_TYPE_ID"] == "file")
				{
					$arReturn[] = $arUserField["FIELD_NAME"];
				}
			}
		}

		return $arReturn;
	}

	/**
	 * Creates user field for user on remote site (if not exists).
	 *
	 * @param array $arUserUserField user user field array
	 *
	 * @return int remote field id or 0
	 */
	public static function uploadUserUserField($arUserUserField)
	{
		try
		{
			$obRequest = new Request("wsAddOrUpdateUserUserField", array(
				"userfield" => serialize($arUserUserField),
			));
			$obResponse = $obRequest->run();
			if ($obResponse && $arValue = $obResponse->value())
			{
				return $arValue["id"];
			}
		}
		catch (SystemException $obException)
		{
			ExceptionHandler::getInstance()->write($obException, ExceptionHandler::CAUGHT_EXCEPTION);
		}

		return 0;
	}

	/**
	 * Checks add user data on remote site.
	 *
	 * @param array $arUser user data
	 *
	 * @return string[] errors
	 */
	public static function checkAdd(array $arUser)
	{
		try
		{
			$arUser = static::getFieldsForRemoteSite($arUser);
			$obRequest = new Request("wsCheckAdd", array(
				"user" => serialize($arUser),
			));
			$obResponse = $obRequest->run();
			if ($obResponse && $arValue = $obResponse->value())
			{
				return unserialize($arValue["errors"]);
			}
		}
		catch (SystemException $obException)
		{
			ExceptionHandler::getInstance()->write($obException, ExceptionHandler::CAUGHT_EXCEPTION);
		}

		return array();
	}

	/**
	 * Checks update user data on remote site.
	 *
	 * @param array $arUser user data
	 *
	 * @return string[] errors
	 */
	public static function checkUpdate(array $arUser)
	{
		try
		{
			$arUser = static::getFieldsForRemoteSite($arUser);
			$obRequest = new Request("wsCheckUpdate", array(
				"id" => $arUser["ID"],
				"user" => serialize($arUser),
			));
			$obResponse = $obRequest->run();
			if ($obResponse && $arValue = $obResponse->value())
			{
				return unserialize($arValue["errors"]);
			}
		}
		catch (SystemException $obException)
		{
			ExceptionHandler::getInstance()->write($obException, ExceptionHandler::CAUGHT_EXCEPTION);
		}

		return array();
	}

	/**
	 * Adds user on remote site.
	 *
	 * @param array $arUser user array
	 *
	 * @return int user id in remote DB or 0
	 */
	public static function add(array $arUser)
	{
		try
		{
			$arUser = static::getFieldsForRemoteSite($arUser);
			$obRequest = new Request("wsAdd", array(
				"user" => serialize($arUser),
			));
			$obResponse = $obRequest->run();
			if ($obResponse && $arValue = $obResponse->value())
			{
				return $arValue["id"];
			}
		}
		catch (SystemException $obException)
		{
			ExceptionHandler::getInstance()->write($obException, ExceptionHandler::CAUGHT_EXCEPTION);
		}
		return 0;
	}

	/**
	 * Updates user on remote site.
	 *
	 * @param int $iUser user id
	 * @param array $arUser user array
	 *
	 * @return int user id in remote DB or 0
	 */
	public static function update($iUser, array $arUser)
	{
		try
		{
			$arUser = static::getFieldsForRemoteSite($arUser);
			$obRequest = new Request("wsUpdate", array(
				"id" => $iUser,
				"user" => serialize($arUser),
			));
			$obResponse = $obRequest->run();
			if ($obResponse && $arValue = $obResponse->value())
			{
				return $arValue["id"];
			}
		}
		catch (SystemException $obException)
		{
			ExceptionHandler::getInstance()->write($obException, ExceptionHandler::CAUGHT_EXCEPTION);
		}

		return 0;
	}

	/**
	 * Deletes user from remote site.
	 *
	 * @param int $iUser user id
	 */
	public static function delete($iUser)
	{
		try
		{
			$obRequest = new Request("wsDelete", array(
				"user" => $iUser
			));
			$obRequest->run();
		}
		catch (SystemException $obException)
		{
			ExceptionHandler::getInstance()->write($obException, ExceptionHandler::CAUGHT_EXCEPTION);
		}
	}

	/**
	 * Adds or updates existing user on remote site.
	 *
	 * $arUser is returned by \CUser::GetList()->Fetch().
	 * \CUser::GetList() must be called with arParams from getUploadParams()
	 *
	 * @see getUploadParams()
	 * @see \CUser::GetList()
	 *
	 * @param array $arUser user array
	 *
	 * @return int|string[] user remote id or error messages array
	 */
	public static function upload(array $arUser)
	{
		$arProblems = array();
		try
		{
			$arFileFields = static::getFileFields();
			foreach ($arFileFields as $sFileField)
			{
				if ($arUser[$sFileField])
				{
					$sPath = \CFile::GetPath($arUser[$sFileField]);
					if ($sPath)
					{
						if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/" . $sPath))
						{
							if ($sContent = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/" . $sPath))
							{
								$arUser["~" . $sFileField] = $arUser[$sFileField];
								$arUser[$sFileField] = array(
									"NAME" => basename($sPath),
									"CONTENT" => base64_encode($sContent),
								);
							}
							else
							{
								$arProblems[] = Loc::getMessage("intervolga.remoteuserclient.FILE_NOT_OPENED", array("#FILE#" => $sPath));
								$arUser[$sFileField] = FALSE;
							}
						}
						else
						{
							$arProblems[] = Loc::getMessage("intervolga.remoteuserclient.FILE_NOT_FOUND", array("#FILE#" => $sPath));
							$arUser[$sFileField] = FALSE;
						}
					}
					else
					{
						$arProblems[] = Loc::getMessage("intervolga.remoteuserclient.FILE_PATH_NOT_FOUND", array("#ID#" => $arUser[$sFileField]));
					}
				}
			}
			if (!$arProblems)
			{
				$obRequest = new Request("wsUpload", array(
					"user" => serialize($arUser),
				));
				$obResponse = $obRequest->run();
				if ($obResponse && $arValue = $obResponse->value())
				{
					if ($arValue["id"])
					{
						return $arValue["id"];
					}
				}
				$arProblems[] = Loc::getMessage("intervolga.remoteuserclient.CANT_GET_RESPONSE");
			}
		}
		catch (SystemException $obException)
		{
			$arProblems[] = Loc::getMessage("intervolga.remoteuserclient.SOAP_ERROR", array(
				"#CODE#" => $obException->getCode(),
				"#STRING#" => $obException->getMessage(),
				)
			);
		}

		return $arProblems;
	}

	/**
	 * Returns remote users.
	 *
	 * @param string $by
	 * @param string $order
	 * @param array $arFilter
	 * @param array $arParameters
	 *
	 * @return array users
	 */
	public static function getList($by = "timestamp_x", $order = "DESC", $arFilter = array(), $arParameters = array())
	{
		try
		{
			$obRequest = new Request("wsGetList", array(
				"by" => $by,
				"order" => $order,
				"filter" => serialize($arFilter),
				"params" => serialize($arParameters),
			));
			$obResponse = $obRequest->run();

			if ($obResponse && $arValue = $obResponse->value())
			{
				if ($arValue["result"])
				{
					return unserialize($arValue["result"]);
				}
			}
		}
		catch (SystemException $obException)
		{
			ExceptionHandler::getInstance()->write($obException, ExceptionHandler::CAUGHT_EXCEPTION);
		}

		return array();
	}

	/**
	 * Returns first user from getList.
	 *
	 * @param string $by
	 * @param string $order
	 * @param array $arFilter
	 * @param array $arParameters
	 *
	 * @return array
	 */
	public static function getFirst($by = "timestamp_x", $order = "DESC", $arFilter = array(), $arParameters = array())
	{
		$arUsers = static::getList($by, $order, $arFilter, $arParameters);
		if ($arUser = array_shift($arUsers))
		{
			return $arUser;
		}
		else
		{
			return array();
		}
	}

	/**
	 * Returns count of users.
	 *
	 * @param string $by
	 * @param string $order
	 * @param array $arFilter
	 * @param array $arParameters
	 *
	 * @return int
	 */
	public static function countList($by = "timestamp_x", $order = "DESC", $arFilter = array(), $arParameters = array())
	{
		try
		{
			$obRequest = new Request("wsCountList", array(
				"by" => $by,
				"order" => $order,
				"filter" => serialize($arFilter),
				"params" => serialize($arParameters),
			));
			$obResponse = $obRequest->run();

			if ($obResponse && $arValue = $obResponse->value())
			{
				if ($arValue["result"])
				{
					return $arValue["result"];
				}
			}
		}
		catch (SystemException $obException)
		{
			ExceptionHandler::getInstance()->write($obException, ExceptionHandler::CAUGHT_EXCEPTION);
		}
		return 0;
	}

	/**
	 * Returns user with all UF's.
	 *
	 * @param int $iUser user id
	 *
	 * @return array
	 */
	public static function getById($iUser)
	{
		if (intval($iUser))
		{
			$arUsers = static::getList("ID", "DESC", array("=ID" => intval($iUser)), array("SELECT" => array("UF_*")));
			if ($arUsers[$iUser])
			{
				return $arUsers[$iUser];
			}
		}
		return array();
	}

	/**
	 * Returns user with all UF's.
	 *
	 * @param string $sLogin user login
	 *
	 * @return array
	 */
	public static function getByLogin($sLogin)
	{
		if (strlen($sLogin) > 0)
		{
			$arUsers = static::getList("ID", "DESC", array("LOGIN_EQUAL_EXACT" => $sLogin), array("SELECT" => array("UF_*")));
			foreach ($arUsers as $arUser)
			{
				if ($arUser["LOGIN"] == $sLogin)
				{
					return $arUser;
				}
			}
		}
		return array();
	}

	/**
	 * Cleans local user fields.
	 *
	 * @param array $arUser
	 *
	 * @return string[]
	 */
	public static function cleanLocalUser(array $arUser)
	{
		$arLocalFields = static::getThisSiteFields();
		$arUpdate = array();
		foreach ($arUser as $sField => $sValue)
		{
			if (!in_array($sField, $arLocalFields) && $arUser[$sField])
			{
				if (in_array($sField, static::getFileFields()))
				{
					$arUpdate[$sField] = array("del" => "Y", "old_file" => $arUser[$sField]);
				}
				else
				{
					$arUpdate[$sField] = FALSE;
				}
			}
		}

		if ($arUpdate)
		{
			$obUser = new \CUser();
			$result = $obUser->Update($arUser["ID"], $arUpdate);
			if (!$result)
			{
				return explode("<br>", $obUser->LAST_ERROR);
			}
		}
		return array();
	}
}