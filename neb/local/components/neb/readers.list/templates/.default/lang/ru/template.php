<?
$MESS["READERS_LIST_FIRST_NAME"] = "Имя";
$MESS["READERS_LIST_LAST_NAME"] = "Фамилия";
$MESS["READERS_LIST_MIDDLE_NAME"] = "Отчество";
$MESS["READERS_LIST_FIO"] = "ФИО";
$MESS["READERS_LIST_ELP_NUMBER"] = "Номер ЕЭЧБ";
$MESS["READERS_LIST_REGISTRATION_DATE"] = "Дата <br>регистрации";
$MESS["READERS_LIST_STATUS"] = "Статус";
$MESS["READERS_LIST_ACTION"] = "Действие";
$MESS["READERS_LIST_EDIT"] = "Изменить";
$MESS["READERS_LIST_DELETE"] = "Удалить";
$MESS["READERS_LIST_BLOCK"] = "Заблокировать";
$MESS["READERS_LIST_VERIFY"] = "Верифицировать";
$MESS["READERS_LIST_VERIFIED"] = "Верифицировано";
$MESS["READERS_LIST_FIND"] = "Найти";
?>
