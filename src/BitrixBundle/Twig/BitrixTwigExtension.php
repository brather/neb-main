<?php
/**
 * User: agolodkov
 * Date: 10.08.2016
 * Time: 15:33
 */

namespace BitrixBundle\Twig;


use BitrixBundle\DependencyInjection\BitrixViewHelper;

class BitrixTwigExtension extends \Twig_Extension
{

    /**
     * @var BitrixViewHelper
     */
    private $_viewHelper;

    /**
     * @param BitrixViewHelper $bitrix
     */
    public function __construct(BitrixViewHelper $bitrix)
    {
        $this->_viewHelper = $bitrix;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [];
    }


    public function getName()
    {
        return 'bitrix_twig_extension';
    }
}