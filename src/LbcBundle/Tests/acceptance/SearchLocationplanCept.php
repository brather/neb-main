<?php use LbcBundle\AcceptanceTester;

$I = new AcceptanceTester($scenario);
$I->wantTo('Search locationplan');
$query = 'Отдельные технологические процессы. Отходы и их использование';
$I->amOnPage('/bbk/search/?locationplan=' . $query);
$I->see($query);