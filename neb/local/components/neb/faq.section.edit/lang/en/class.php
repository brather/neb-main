<?php
$MESS["ERROR_MIN_COUNT_SYMBOL"] = "Error! You have entered too few characters.";
$MESS["ERROR_EMPTY_STRING"] = "Error! You did not fill one of the fields.";
$MESS["SECTION_NOT_FOUND"] = "Error! Section not found.";
$MESS["ERROR"] = "Error!";
$MESS["ERROR_SECTION_NOT_EMPTY_1"] = "Error! Section with id=<b>";
$MESS["ERROR_SECTION_NOT_EMPTY_2"] = "</b> not empty.";
$MESS["ERROR_SECTION_EXIST"] = "Error! This section exist. Please, enter another section name.";
$MESS["SECTION_UPDATE_SUCCESS_1"] = "Update section with id=<b>";
$MESS["SECTION_UPDATE_SUCCESS_2"] = "</b> was success!";
$MESS["SECTION_UPDATE_ERROR_1"] = "Error! Update section with id=<b>";
$MESS["SECTION_UPDATE_ERROR_1"] = "</b> was not.";
$MESS["SECTION_DELETE_SUCCESS_1"] = "Delete section with id=<b>";
$MESS["SECTION_DELETE_SUCCESS_2"] = "</b> was success!";
$MESS["SECTION_DELETE_ERROR_1"] = "Error! Delete section with id=<b>";
$MESS["SECTION_DELETE_ERROR_2"] = "</b> was not.";
$MESS["SECTION_DELETE_ERROR_NOT_PERMISSION"] = "Error! Section removal has taken place. Not permission.";