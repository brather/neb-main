<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$arTemplateParams = array(
		'template'  => '.default',
		'width'  	=> 600,
		'height' 	=> 300,
		'exact' 	=> false,
		'one_photo'	=> false,
		'align'		=> 'middle',
		'SORT'		=> 10,
	);

	$arTemplateDescription = array(
		"NAME" => 'Gallery in the center',
		"SORT" => '10',
		"DESCRIPTION" => Cutil::PhpToJSObject($arTemplateParams),
	);
?>