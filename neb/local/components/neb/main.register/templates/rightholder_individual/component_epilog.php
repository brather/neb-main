<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	/*
	Устанавливаем тип регистрации
	*/
	$USER->SetParam("REGISTER_TYPE", 'rightholder');

	/*
	загружаем фотографию
	*/

	if(intval($arResult['VALUES']['USER_ID']) > 0)
	{
        $arFields = array();
        if(!empty($_REQUEST['PERSONAL_PHOTO'])) {
            $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_REQUEST['PERSONAL_PHOTO']);
            $arFile["MODULE_ID"] = "main";
            $arFields['PERSONAL_PHOTO'] = $arFile;
        }
		RightHolder::setUserGroup($arResult['VALUES']['USER_ID']);

        $document = 1;
        while (isset($_REQUEST['rightholder_document' . $document])) {
            if(!isset($arFields['UF_DOCUMENTS'])) {
                $arFields['UF_DOCUMENTS'] = array();
            }
            $arFile = CFile::MakeFileArray(
                $_SERVER["DOCUMENT_ROOT"] . $_REQUEST['rightholder_document'
                . $document]
            );
            $arFile['MODULE_ID'] = 'main';
            $arFields['UF_DOCUMENTS'][] = $arFile;
            $document++;
        }

        if(!empty($arFields)) {
            $user = new CUser;
            $user->Update($arResult['VALUES']['USER_ID'], $arFields);
        }
	}

	$bDesignMode = $APPLICATION->GetShowIncludeAreas() && is_object($USER) && $USER->IsAdmin();
	if($USER->IsAuthorized() and !$bDesignMode){
		CUser::SendUserInfo($arResult['VALUES']['USER_ID'], 's1', "Приветствуем Вас как нового пользователя НЭБ!");
		LocalRedirect("/");
		exit();
	}
?>
