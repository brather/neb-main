<?
if (!$USER->IsAuthorized())
    return false;

$obNUser = new nebUser();
$obNUser->getRole();

if($obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR or $obNUser->role == UGROUP_LIB_CODE_CONTROLLER)
{
    /* массивы меню фондов*/
    $aMenuLinksExt = array(
        array('Фонды библиотеки для оцифровки', '#', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
        array('План оцифровки', '#', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
        array('Ведение фондов', '/profile/funds/manage/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => true, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
    );
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>