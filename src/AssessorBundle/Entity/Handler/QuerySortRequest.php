<?php
/**
 * User: agolodkov
 * Date: 11.10.2016
 * Time: 13:31
 */

namespace AssessorBundle\Entity\Handler;

use JMS\Serializer\Annotation as Serialization;
use Symfony\Component\Validator\Constraints as Assert;

class QuerySortRequest
{
    /**
     * @Assert\NotBlank()
     * @Serialization\Groups({"list", "details"})
     * @var array
     */
    public $editions;

    /**
     * @Assert\NotBlank()
     * @Serialization\Groups({"list", "details"})
     * @var array
     */
    public $total;

    /**
     * @Assert\NotBlank()
     * @Serialization\Groups({"list", "details"})
     * @var array
     */
    public $pageNumber;

    /**
     * @Assert\NotBlank()
     * @Serialization\Groups({"list", "details"})
     * @var array
     */
    public $queryId;

    /**
     * @return array
     */
    public function getEditions()
    {
        return $this->editions;
    }

    /**
     * @param array $editions
     */
    public function setEditions($editions)
    {
        $this->editions = $editions;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * @param int $pageNumber
     */
    public function setPageNumber($pageNumber)
    {
        $this->pageNumber = $pageNumber;
    }

    /**
     * @return int
     */
    public function getQueryId()
    {
        return $this->queryId;
    }

    /**
     * @param int $queryId
     */
    public function setQueryId($queryId)
    {
        $this->queryId = $queryId;
    }
}