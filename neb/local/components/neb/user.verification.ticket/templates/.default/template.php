<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));
?>
<div class="profile-container-readers-profile">

    <?foreach($arResult['ERRORS'] as $error) ShowError($error); ?>

    <form method="post" name="form1" action="" class="exist-reader-form nrf">

        <input type="hidden" name="VERIFICATION[ID]" value=<?=intval($arResult["ID"])?> />

        <div class="form-group">
            <label for="userlogin">
                <em class="hint">*</em>
                <?=Loc::getMessage('USER_VERIFICATION_LOGIN');?>
            </label>
            <input type="text"
                   value="<?=$_REQUEST["VERIFICATION"]['LOGIN']?:$arResult["USER"]['LOGIN']?>"
                   id="userlogin"
                   name="VERIFICATION[LOGIN]"
                   class="form-control" />
        </div>

        <div class="form-group">
            <label for="usernumber">
                <em class="hint">*</em>
                <?=Loc::getMessage('USER_VERIFICATION_NUMBER');?>
            </label>
            <input type="text"
                   value="<?=$_REQUEST["VERIFICATION"]['NUMBER']?:$arResult['UF_NUMBER']?>"
                   id="usernumber"
                   name="VERIFICATION[NUMBER]"
                   class="form-control" />
        </div>

        <div class="form-group ">
            <label>
                <em class="hint">*</em>
                <?=Loc::getMessage('USER_VERIFICATION_DATE');?>
            </label>
            <div class="input-group">
                <input
                        type="text"
                        class="form-control"
                        value="<?=$_REQUEST["VERIFICATION"]['DATE']?:$arResult['UF_DATE']?>"
                        id="userdate"
                        name="VERIFICATION[DATE]"
                >
                <span class="input-group-btn">
                    <a class="btn btn-default"
                       onclick="BX.calendar({node: 'userdate', field: 'userdate',  form: 'history', bTime: false, value: ''});"
                    >
                        <span class="glyphicon glyphicon-calendar"></span>
                    </a>
                </span>
            </div>
        </div>

        <div class="fieldrow nowrap fieldrowaction">
            <div class="fieldcell ">
                <div class="field clearfix">
                    <button name="action" class="btn btn-primary" value="save" type="submit">
                        <?=Loc::getMessage('USER_VERIFICATION_SAVE');?>
                    </button>
                </div>
            </div>
        </div>

    </form>
</div>