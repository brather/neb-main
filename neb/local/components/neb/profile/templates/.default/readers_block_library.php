<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @todo перенести все это в REST
 */
/**
 * @var array $arResult
 * @var array $arParams
 * @var CMain $APPLICATION
 * @var       $USER CUser
 */
$APPLICATION->RestartBuffer();

$statuses = array(
    $arParams['READER_BLOCK_STATUS']   => true,
    $arParams['READER_UNBLOCK_STATUS'] => true,
);
$block = true;
$jsonResult = array();
if (!check_bitrix_sessid() || !$USER->IsAuthorized()) {
    \Neb\Main\Helper\MainHelper::showJson(array(
        'error'   => 1,
        'message' => GetMessage('NOT_AUTHORIZED'),
    ));
}
$userId = intval($arResult['VARIABLES']['USER_ID']);
CModule::IncludeModule('nota.userdata');
$obEUser = new nebUser($userId);
$user = $obEUser->getUser();
if (!isset($statuses[(integer)$user['UF_STATUS']])) {
    \Neb\Main\Helper\MainHelper::showJson(array(
        'error'   => 5,
        'message' => GetMessage('READER_VERIFY_NO_ALLOWED_STATUS'),
    ));
}
$status = $arParams['READER_BLOCK_STATUS'];
if ($arParams['READER_BLOCK_STATUS'] === (integer)$user['UF_STATUS']) {
    $block = false;
    $status = $arParams['READER_UNBLOCK_STATUS'];
}
if (in_array($arResult['LIBRARY']['ID'], $user['UF_LIBRARIES'])) {
    $nebUser = nebUser::forgeUser($userId);
    $jsonResult = array(
        'error'   => 2,
        'message' => GetMessage('READER_VERIFY_ERROR'),
    );
    try {
        if ($nebUser->setStatus($status)) {
            $jsonResult = array(
                'error'   => 0,
                'block'   => $block,
                'message' => GetMessage(
                    true === $block ? 'READER_BLOCK_SUCCESS'
                        : 'READER_UNBLOCK_SUCCESS'
                ),
            );

        }
    } catch (Exception $e) {
    }
} else {
    $jsonResult = array(
        'error'   => 3,
        'message' => GetMessage('READER_ACCESS_ERROR'),
    );
}
\Neb\Main\Helper\MainHelper::showJson($jsonResult);
