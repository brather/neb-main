<?
$MESS ['AUTH_LOGIN_BUTTON'] = "Login";
$MESS ['AUTH_CLOSE_WINDOW'] = "Close";
$MESS ['AUTH_LOGIN'] = "Login";
$MESS ['AUTH_PASSWORD'] = "Password";
$MESS ['AUTH_REMEMBER_ME'] = "Remember me on this computer";
$MESS ['AUTH_FORGOT_PASSWORD_2'] = "Forgot your password?";
$MESS ['AUTH_REGISTER'] = "Register";
$MESS ['AUTH_LOGOUT_BUTTON'] = "Logout";
$MESS ['AUTH_CAPTCHA_PROMT'] = "Type text from image";
$MESS ['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND'] = "E-mail not found";
$MESS ['AUTH_PASS_RECOVERY_TITLE'] = "Password restore";
$MESS ['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC'] = "Enter the e-mail specified at registration";
$MESS ['AUTH_PASS_RECOVERY_SUCCESS'] = "You will receive an email with a link to reset your password";
$MESS ['AUTH_PASS_RECOVERY_RECOVER'] = "Restore";
$MESS ['AUTH_CLOSE'] = "Close";
?>