<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult["FORM_HEADER"] = str_replace('<form', '<form class="b-form b-form_common b-feedbackform"', $arResult["FORM_HEADER"]);

// битриксы - говнокодеры.... (не спрашивайте за чем это, просто примите за данность суровой реальности)
global $USER;
if (!$USER->IsAuthorized() && !empty($_SESSION["SESS_LAST_USER_ID"]))
    unset($_SESSION["SESS_LAST_USER_ID"]);

//debugPre($arResult);

?>
<!-- <h2><?= GetMessage("FORM_FEEDBACK_TITLE"); ?></h2> -->

<? if ($arResult["isFormErrors"] == "Y"): ?>
    <?= $arResult["FORM_ERRORS_TEXT"]; ?>
<? endif; ?>

<?= $arResult["FORM_HEADER"] ?>

<div class="row">
    <div class="col-md-4">
        <em class="hint">*</em>
        <label for="fb_email"><?= GetMessage("FORM_FIELD_EMAIL") ?>:</label>
        <div class="field validate">
            <input type="text" class="form-control" name="form_email_1" data-minlength="2" data-maxlength="50"
                   id="fb_email" data-validate="email" data-required="required"
                   <? if ($USER->IsAuthorized()): ?>
                       value="<?= $USER->GetEmail() ?>" readonly="readonly"
                   <? else: ?>
                       value="<?= $arResult['arrVALUES']['form_email_1'] ?>"
                   <? endif; ?> />
        </div>
    </div>

    <div class="col-md-3">
        <label for="fb_theme"><?= GetMessage("FORM_FIELD_SUBJECT") ?>:</label>
        <div class="field">
            <? if (!empty($arResult['arDropDown']['FB_THEME']["reference"])): ?>
                <select name="form_dropdown_FB_THEME" id="fb_theme" data-required="required" class="js_select w370 feedback-select">
                    <? foreach($arResult['arDropDown']['FB_THEME']["reference"] as $k => $sAnswer):
                        $id = $arResult['arDropDown']['FB_THEME']["reference_id"][$k];
                        $sAnswer = explode('/', $sAnswer);
                        $sAnswer = LANGUAGE_ID == 'ru' ? $sAnswer[0] : $sAnswer[1];?>
                        <option value="<?=$id?>"<? if ($_REQUEST['form_dropdown_FB_THEME'] == $id): ?>
                            selected="selected"<? endif; ?>><?=$sAnswer?></option>
                    <? endforeach; ?>
                </select>
            <? endif; ?>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 1em;">
    <div class="col-md-6">
        <em class="hint">*</em>
        <label for="fb_message"><?= GetMessage("FORM_FIELD_MESSAGE") ?>:</label>
        <textarea id="fb_message" class="form-control feedback__textarea" rows="3" name="form_textarea_6"
                  data-required="required"><?= $arResult['arrVALUES']['form_textarea_6'] ?></textarea>
    </div>
</div>

<?if ($arResult['arForm']['USE_CAPTCHA'] == 'Y'):?>
    <input type="hidden" name="captcha_sid" value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
    <div class="fieldcell iblock" style="margin-top: 1em;">
        <label for="bt_captcha" class="col-xs-12">
            <em class="hint">*</em>
            <?= GetMessage("FORM_FIELD_CAPTCHA_CODE") ?>:
        </label>
        <div class="row">
            <div class="field validate captcha col-sm-6" style="margin-bottom: 1ex;">
                <input type="text" class="form-control" name="captcha_word" id="bt_captcha" value=""
                       data-required="required">
            </div>
            <div class="col-sm-6" style="margin-bottom: 1ex;">
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"
                     width="180" height="40"/>
            </div>
        </div>
    </div>
<?endif;?>

<div style="margin-top: 1em;">
    <button class="btn btn-primary" value="1" type="submit"
            name="web_form_submit"><?= GetMessage("FORM_BUTTON_SUBMIT") ?></button>
</div>

<?= $arResult["FORM_FOOTER"] ?>