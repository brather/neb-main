<?php
/**
 * User: agolodkov
 * Date: 21.10.2016
 * Time: 15:16
 */

namespace AppBundle\Controller\Api;


use AppBundle\DependencyInjection\NebBookSearcher;
use AppBundle\DependencyInjection\NebSearcher;
use AppBundle\DependencyInjection\Search\Exalead\Condition;
use AppBundle\DependencyInjection\Search\Exalead\EditionsSearchResult;
use BitrixBundle\Annotations\IncludeBitrix;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class LbcHistoryController
 *
 * @DI\Service("neb.api.search_auto_complete", scope="request")
 */
class ApiSearchController
{
    /**
     * @DI\Inject("neb.searcher")
     * @var NebSearcher
     */
    public $searcher;

    /**
     * @DI\Inject("request")
     * @var Request
     */
    public $request;

    /**
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return array
     */
    public function autoCompleteAction()
    {
        $query = $this->request->query->all();
        $cond = trim($query['term']);
        $group = null;
        $qb = $this->searcher->buildSearchQuery([]);
        if (!empty($cond)) {
            $cond = "($cond*)";
        }
        if (isset($query['field'])) {
            $cond = [$query['field'] => $cond];
            if ('title' !== $query['field']) {
                $group = $query['field'];
                $qb->addParam('sl', 'sl_suggests2');
            }
        }

        $condition = new Condition($cond);
        if (isset($query['filter'])) {
            foreach ($query['filter'] as $field => $value) {
                $value = trim($value);
                if(empty($value)) {
                    unset($query['filter'][$field]);
                }
                $query['filter'][$field] = "($value*)";
            }
            if(!empty($query['filter'])) {
                $condition->andCondition($query['filter']);
            }
        }
        $qb->setCondition($condition);


        /** @var EditionsSearchResult $result */
        $result = $this->searcher->search($qb);
        if (null === $group) {
            $entities = $result->buildEntities();
        } else {
            $entities = $result->buildAnswerGroupEntities($group);
        }

        return View::create(
            [
                'query'    => $qb->buildQuery(),
                'field'    => 'title',
                'entities' => $entities,
            ]
        );
    }

    /**
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return array
     */
    public function searchAction()
    {
        return View::create(['entities' => []]);
    }
}