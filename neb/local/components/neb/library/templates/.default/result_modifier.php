<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;

global $APPLICATION;
Loc::loadMessages(__FILE__);

$sSection = Loc::getMessage('LIBRARY_MENU_' . strtoupper($this->__page));
$APPLICATION->SetTitle(($sSection ? $sSection . ' — ' : '') . $arResult['LIBRARY']['NAME']);

$APPLICATION->SetPageProperty('hide-title', true);
echo '<h1>'.$arResult['LIBRARY']['NAME'].'</h1>';
