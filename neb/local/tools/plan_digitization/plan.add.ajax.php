<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

global $DB;

use \Bitrix\Main\Loader,
    \Bitrix\Main\Application,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('highloadblock');
Loader::includeModule('nota.exalead');
Loader::includeModule('iblock');

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

if ($arRequest['DATA_AJAX']) {

    $arResult = [];

    // Вернем id HL блока
    $sSql     = "SELECT ID FROM b_hlblock_entity WHERE NAME='" . HIBLOCK_CODE_DIGITIZATION_PLAN_LIST . "';";
    $arHBlock = Application::getConnection()->query($sSql)->fetch();

    // Вернем класс для работы с HL блоком
    $hlblock      = HighloadBlockTable::getById($arHBlock['ID'])->fetch();
    $dataClassAdd = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

    // Вернем текущую дату и время
    $curDate = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time());

    // Сформируем название плана исходя из шаблона имени *год оцифровки*_*id библиотеки*_*порядковый номер плана для текущего года*
    $yearDigitization = $DB->FormatDate($arRequest["DATA_UF_DATE_F_PLAN"], "DD.MM.YYYY", "YYYY");
    $arFilter = [
        ">=UF_DATE_F_PLAN" => "01.01." . $yearDigitization,
        "<=UF_DATE_F_PLAN" => "31.12." . $yearDigitization
    ];
    //$numbPlan = $dataClassAdd::getCount($arFilter) + 1;
    $preffixName = $yearDigitization . '_' . $arRequest["DATA_UF_LIBRARY"] . '_';

    // Вернем список планов с префиксом $preffixName
    $arFilter['%UF_PLAN_NAME'] = $preffixName;
    $arPlanList = $dataClassAdd::getList([
        'select' => ['UF_PLAN_NAME'],
        'filter' => $arFilter
    ]);
    $maxPostfix = 0;
    while($rsPlan = $arPlanList->fetch()) {
        $planPostfix = str_replace($preffixName, "", $rsPlan['UF_PLAN_NAME']);
        if($maxPostfix <= $planPostfix) {
            $maxPostfix = 1 + intval($planPostfix);
        }
    }
    $arResult['SUCCESS']['LAST_PLAN'] = $preffixName . $maxPostfix;
    $planName = $preffixName . $maxPostfix;

    // Добавление плана
    $resultID = $dataClassAdd::add([
        "UF_DATE_CREATE" => $curDate,
        "UF_PLAN_NAME"   => $planName,
        "UF_DATE_F_PLAN" => $arRequest["DATA_UF_DATE_F_PLAN"],
        "UF_LIBRARY"     => $arRequest["DATA_UF_LIBRARY"]
    ])->getId();

    $arDataLang = $arRequest["DATA_AR_LANG"];
    if(!empty($resultID)) {

        $obUserFieldEnum = new CUserFieldEnum();

        // Вернем информацию по статусу "На согласовании оператором НЭБ"
        $arProp = CUserTypeEntity::GetList(array(), array('FIELD_NAME' => "UF_REQUEST_STATUS"))->Fetch();
        $rsUFValues = $obUserFieldEnum->GetList(array(), array('USER_FIELD_ID' => $arProp['ID']));
        $idStatusAgreement = '';
        while($obUFValues = $rsUFValues->Fetch()) {
            if($obUFValues["VALUE"] == "IN_AGREEING_NEL_OPERATOR") {
                $idStatusAgreement = $obUFValues["ID"];
                break;
            }
        }

        $preffixPlan = 'plan-' . $resultID . '-';

        $arResult['SUCCESS']['NEW_PLAN_ID'] = $resultID;
    } else {
        $arResult['ERROR'] = '<p class="text-danger">' . $arDataLang['T_PLAN_ADD_AJAX_ERROR'] . '</p>';
    }

    MainHelper::showJson($arResult);
}