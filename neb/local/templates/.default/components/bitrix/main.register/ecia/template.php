<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>
<?
	if ( (int)$arResult['VALUES']['USER_ID'] > 0 && $arResult["USE_EMAIL_CONFIRMATION"] === "Y" )
	{ 
	?>
	<section class="innersection innerwrapper clearfix">
		<div class="b-formsubmit">
			<span class="b-successlink"><?=GetMessage("REGISTER_SUCCESSFUL")?></span>
			<p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
		</div>
	</section>
	<?
		return;
	}
?>
<?
	#pre($this->__folder,1);
?>

<script type="text/javascript">
	function setlogin(){
		$('input#LOGIN').val($('input#settings04').val());
		$('input#PERSONAL_NOTES').val($('input#settings05').val());
	}
</script>
<section class="innersection innerwrapper clearfix">



	<script type="text/javascript">
		$(function() {
			$('.popup_esia').esiaPopup();

			$(window).on('resize', function(){
				$('.ui-widget-overlay').remove();
				$('.ui-dialog').remove();
				$('.popup_esia').esiaPopup();
			});

			var sendForm = false;

			$("form#authnFrm").submit(function( event ) {

				var form = this;
				var password = $('input#password', form).val();
				var idType = $('input#idType', form).val();
				var value =  $('input#'+idType, form).val();

				if(sendForm == true)
					return false;

				$('.line-error', form).hide();

				if(password.length >0 && value.length >0)
				{

					sendForm = true;

					BX.showWait();

					$.ajax({
						type: "POST",
						url: "<?=$this->__folder?>/ecia.php",
						data: {password : password, idType : idType, value : value},
						cache:false,
						timeout:120000,
						dataType: 'json'
					})
					.done(function( resp ) {
						sendForm = false;
						if(resp.error == true)
						{
							$('.line-error', form).show();
						}
						else
						{
							/*					 ФИО	*/
							if(resp.result.Surname.length > 0)
							{
								$('#settings01').val(resp.result.Surname);
								cloneInput($('#settings01'));
							}  

							if(resp.result.Name.length > 0)
							{
								$('#settings02').val(resp.result.Name);
								cloneInput($('#settings02'));
							}

							if(resp.result.Patronomic.length > 0)
							{
								$('#settings22').val(resp.result.Patronomic);
								cloneInput($('#settings22'));
							}

							/*						   Паспорт		*/
							if(resp.result.PassportSeries.length > 0)
							{
								$('#settings011').val(resp.result.PassportSeries);
								cloneInput($('#settings011'));
							}
							if(resp.result.PassportNumber.length > 0)
							{
								$('#settings022').val(resp.result.PassportNumber);
								cloneInput($('#settings022'));
							}

							if(resp.result.EMail.length > 0)	
							{
								$('#settings04').val(resp.result.EMail);
								cloneInput($('#settings04'));
							}

							if(resp.result.MobilePhone7Digits.length > 0)
							{
								$('#settings118').val('+7 ('+resp.result.MobilePhone3Digits+') '+resp.result.MobilePhone7Digits);
								cloneInput($('#settings118'));
							}

							/*							Адрес регистрации	*/
							if(resp.result.Registration.index.length > 0)	
							{
								$('#settings03').val(resp.result.Registration.index);
								cloneInput($('#settings03'));
							}		

							if(resp.result.Registration.city.length > 0)	
							{
								$('#settings055').val(resp.result.Registration.city);
								cloneInput($('#settings055'));
							}		

							if(resp.result.Registration.street.length > 0)	
							{
								$('#settings07').val(resp.result.Registration.street);
								cloneInput($('#settings07'));
							}		

							if(resp.result.Registration.house.length > 0)	
							{
								$('#settings099').val(resp.result.Registration.house);
								cloneInput($('#settings099'));
							}

							if(resp.result.Registration.korp.length > 0)	
							{
								$('#settings101').val(resp.result.Registration.korp);
								cloneInput($('#settings101'));
							}		

							if(resp.result.Registration.kv.length > 0)
							{	
								$('#settings111').val(resp.result.Registration.kv);
								cloneInput($('#settings111'));		
							}
							/* Адрес проживания*/

							if(resp.result.Living.kv.length > 0)	
							{
								$('#settings044').val(resp.result.Living.index);
								cloneInput($('#settings044'));
							}		

							if(resp.result.Living.city.length > 0)
							{	
								$('#settings066').val(resp.result.Living.city);
								cloneInput($('#settings066'));
							}		

							if(resp.result.Living.street.length > 0)	
							{
								$('#settings086').val(resp.result.Living.street);
								cloneInput($('#settings086'));
							}		

							if(resp.result.Living.house.length > 0)	
							{
								$('#settings0911').val(resp.result.Living.house);
								cloneInput($('#settings0911'));
							}

							if(resp.result.Living.korp.length > 0)	
							{
								$('#settings102').val(resp.result.Living.korp);
								cloneInput($('#settings102'));
							}		

							if(resp.result.Living.kv.length > 0)	
							{
								$('#settings114').val(resp.result.Living.kv);
								cloneInput($('#settings114'));
							}	

							if(resp.result.arBirthDate.d > 0)
							{
								var selDay = $('select#PERSONAL_BIRTHDAY_D option[value="' + resp.result.arBirthDate.d +'"]')
								$(selDay).attr('selected', 'selected');
								$(selDay).closest('.inpselblock').find('.inpseltxt').html(resp.result.arBirthDate.d );

								$('#' + $(selDay).parent().attr('name') + 'opts a.selected').removeClass('selected');
								$('#' + $(selDay).parent().attr('name') + 'opts a[title="' + resp.result.arBirthDate.d +'"]').addClass('selected');

								cloneSelect($('select#PERSONAL_BIRTHDAY_D'));
							}

							if(resp.result.arBirthDate.m > 0)
							{
								var selMonth = $('select#PERSONAL_BIRTHDAY_M option[value="' + resp.result.arBirthDate.m +'"]')
								$(selMonth).attr('selected', 'selected');
								$(selMonth).closest('.inpselblock').find('.inpseltxt').html(resp.result.arBirthDate.m );

								$('#' + $(selMonth).parent().attr('name') + 'opts a.selected').removeClass('selected');
								$('#' + $(selMonth).parent().attr('name') + 'opts a[title="' + resp.result.arBirthDate.m +'"]').addClass('selected');

								cloneSelect($('select#PERSONAL_BIRTHDAY_M'));
							}
							if(resp.result.arBirthDate.Y > 0)
							{
								var selYear = $('select#PERSONAL_BIRTHDAY_Y option[value="' + resp.result.arBirthDate.Y +'"]')
								$(selYear).attr('selected', 'selected');
								$(selYear).closest('.inpselblock').find('.inpseltxt').html(resp.result.arBirthDate.Y );

								$('#' + $(selYear).parent().attr('name') + 'opts a.selected').removeClass('selected');
								$('#' + $(selYear).parent().attr('name') + 'opts a[title="' + resp.result.arBirthDate.Y +'"]').addClass('selected');

								cloneSelect($('select#PERSONAL_BIRTHDAY_Y'));
							}


							$('.ecia-dialog-close').click();

						}

						BX.closeWait();
					});
				}

				return false;
			});

			function cloneInput($elem)
			{
				/*var $elem = $('#settings02');*/
				$clone = $elem.clone();

				$clone.attr('id', 'del_'+$clone.attr('id'));
				$clone.attr('name', 'del_'+$clone.attr('name'));
				$clone.attr('disabled', 'disabled');

				$elem.attr('type', 'hidden');
				$elem.after($clone);
			}

			function cloneSelect($elem)
			{
				/*var $elem = $('select[name="birthday"]');*/
				var val = $elem.val();

				$elem.attr('disabled', 'disabled');
				$elem.closest('.inpselblock').find('.selopener').addClass('disabled');

				$elem.after('<input type="hidden" name="'+$elem.attr('name')+'" id="'+$elem.attr('id')+'" value="'+val+'">');
				$elem.attr('name', 'del_'+$elem.attr('name'));
				$elem.attr('id', 'del_'+$elem.attr('id'));
			}

		});
		$(function() { /*create closure*/
			$.fn.esiaPopup = function() { /* попап регистрации */

				this.each(function() {
					var popup = $(this).clone( true );
					popup.removeClass('hidden');
					wW= $(window).outerWidth();
					var pw= (wW > 990)? 975 : 445;
					var id = popup.find('input[type="checkbox"]').attr('id');
					popup.dialog({
						closeOnEscape: true,	                   
						modal: true,
						draggable: false,
						resizable: false,             
						closeOnEscape: false,             
						width: pw,
						heigth: 668,
						dialogClass: 'esia-popup',
						position: { my: "center 88px", at: "center 88px", of: window },
						open: function(){

							popup.find('input[type="checkbox"]').attr('id',	id+'_1');
							popup.find('label[for="'+id+'"]').attr('for',	id+'_1');
							popup.find("#snils").inputmask({"mask" : "999-999-999 99", "clearIncomplete": false});
							popup.find("#phone").inputmask("+7(999) 999-99-99");
							$('.ui-widget-overlay').addClass('black');
							popup.find("dl.login label").each(function(){
								var a =$(this);
								var cont = $(this).closest('dl.login');
								a.click(function(){
									var id = $(this).attr('data');

									$('input#idType').val(id);
									cont.find('label').removeClass('active');
									$(this).addClass('active');
									cont.find('.ui-inputfield').hide();
									cont.find('.ui-inputfield#'+id).show();

								});
							});
						},
						close: function() {
							popup.dialog("close").remove();
						}
					});

					/*$('.ui-widget-overlay').click(function() {*/
					/*Close the dialog*/
					popup.dialog("close");

					}); */
					$('.ecia-dialog-close').click(function() {				
						/*Close the dialog*/
						popup.dialog("close");
					});  

				});
			}
			/*end of closure*/
		});
	</script>

	<a style="display: none;" class="ecia-dialog-close">close</a>

	<!--[if lt IE 9]>
	<script type="text/javascript">
		$(function () {
			$("label[for='saveId']").bind('click', function () {
				$(this).toggleClass('label-checked');
			})
			$("#saveId").bind('keypress', function (e) {
				if (e.which == 32) {
					$("label[for='saveId']").toggleClass('label-checked');
				}
			})
		});
	</script>
	<![endif]-->


	<div class="b-registration rel">


		<div class="popup_esia hidden">
			<div class="simple-cover authn-cover">

				<div class="column-right">

					<h1 class="page-title">
						<a href="https://esia.gosuslugi.ru/registration/" target="_blank" class="goto-reg">Регистрация</a>
						Вход
					</h1>
					<form id="authnFrm" method="post" action="">
						<div class="data-form data-form-authn">
							<dl class="login">
								<dt>
									<label class="active" data="phone">Телефон</label>
									<label class="" data="email">E-mail</label>
									<label class="" data="snils">СНИЛС</label>
								</dt>
								<dd>
									<input id="phone" name="phone" class="ui-inputfield">
									<div style="display: none;" class="field-error">
										<div class="ui-message-error">
											<span class="ui-message-error-detail"></span>
										</div>
									</div>
									<input style="display: none;" id="email" name="email" class="ui-inputfield">
									<div style="display: none;" class="field-error">
										<div class="ui-message-error">
											<span class="ui-message-error-detail"></span>
										</div>
									</div>
									<input style="display: none;" id="snils" name="snils" class="ui-inputfield">
									<div style="display: none;" class="field-error">
										<div class="ui-message-error">
											<span class="ui-message-error-detail"></span>
										</div>
									</div>
								</dd>
							</dl>
							<dl class="password">
								<dt>
									<label>Пароль</label>
								</dt>
								<dd>
									<input id="password" name="password" class="ui-inputfield ui-inputtext" type="password">
									<div style="display: none;" class="field-error">
										<div class="ui-message-error">
											<span class="ui-message-error-detail"></span>
										</div>
									</div>
									<input id="username" name="username" type="hidden">
									<input id="idType" name="idType" type="hidden" value="phone">
								</dd>
							</dl>

							<dl class="line-error" style="display: none;">
								<dd>
									<div class="field-error">
										<div class="ui-message-error">
											<span class="ui-message-error-detail">Введено неверное имя пользователя или&nbsp;пароль</span>
										</div>
									</div>
								</dd>
							</dl>

							<dl class="button-authn">
								<button id="authnBtn" name="authnBtn" class="ui-button ui-button-text-only button-big" type="submit">
									<span class="ui-button-text">Войти</span>
								</button>
							</dl>
							<a href="https://esia.gosuslugi.ru/recovery" target="_blank" class="fail-login">Не удается войти?</a>
						</div>
					</form>
				</div>
				<!-- end .column-right -->
				<div class="column-left">
					<h1 class="page-title">Необходима авторизация</h1>
					<div class="content-styles">
						<p class="big">
							Для доступа к&nbsp;системам и&nbsp;сервисам Электронного правительства вам нужно пройти авторизацию.
						</p>
					</div>
				</div>
				<!-- end .column-left -->
			</div>
		</div>


		<h2 class="mode">регистрация на портале</h2>
		<?
			if (count($arResult["ERRORS"]) > 0){
				foreach ($arResult["ERRORS"] as $key => $error){
					if (intval($key) == 0 && $key !== 0) {
						$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
					}
				}

				ShowError(implode("<br />", $arResult["ERRORS"]).'<br /><br />');

			}elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && (int)$arResult['VALUES']['USER_ID'] > 0 ){
			?>
			<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
			<?
			}
		?>
		<form method="post" action="<?=POST_FORM_ACTION_URI?>" class="b-form b-form_common b-regform" name="regform" enctype="multipart/form-data" onsubmit="setlogin()">

			<?
				if($arResult["BACKURL"] <> ''){
				?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?
				}
			?>
			<input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value="" />
			<input type="hidden" name="REGISTER[PERSONAL_NOTES]" id="PERSONAL_NOTES" value="" />

			<p class="note">Просим обратить особое внимание на заполнение анкеты корректными данными. <br/>В случае выявления несоответствий Ваш доступ может быть заблокирован.</p>
			<div class="wrapfield">
				<div class="b-form_header"><span class="iblock">Паспортные данные</span></div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings01"><?=Loc::getMessage('MAIN_REGISTER_LASTNAME');?></label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
							<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
							<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>	
							<input type="text" data-validate="fio" data-required="required" value="<?=$arResult["VALUES"]['LAST_NAME']?>"  id="settings01" data-maxlength="30" data-minlength="2" name="REGISTER[LAST_NAME]" class="input">										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings02"><?=Loc::getMessage('MAIN_REGISTER_FIRSTNAME');?></label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
							<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
							<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>		
							<input type="text" data-required="required" data-validate="fio" value="<?=$arResult["VALUES"]['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="REGISTER[NAME]" class="input" >										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings22"><?=Loc::getMessage('MAIN_REGISTER_MIDNAME');?></label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
							<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
							<input type="text" data-validate="fio" value="<?=$arResult["VALUES"]['SECOND_NAME']?>" id="settings22" data-maxlength="30" data-minlength="2"data-required="false"  name="REGISTER[SECOND_NAME]" class="input" >										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings06"><?=Loc::getMessage('MAIN_REGISTER_BIRTHDAY');?></label>
						<div class="field validate iblock seldate" data-yearsrestrict="12">
							<em class="error dateformat"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT');?></em>
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
							<em class="error yearsrestrict"><?=Loc::getMessage('MAIN_REGISTER_AGE');?></em>
							<input type="hidden" class="realseldate" data-required="true" value="<?=$arResult["VALUES"]['PERSONAL_BIRTHDAY']?>" id="settings06" name="REGISTER[PERSONAL_BIRTHDAY]">
							<select name="birthday" id="PERSONAL_BIRTHDAY_D" class="js_select w102 sel_day"  data-required="true">
								<option value="-1"><?=Loc::getMessage('MAIN_REGISTER_DAY');?></option>
								<?
									for($i=1;$i<=31;$i++)
									{
									?>
									<option value="<?=$i?>"><?=$i?></option>
									<?
									}
								?>
							</select>

							<select name="birthmonth" id="PERSONAL_BIRTHDAY_M" class="js_select w165 sel_month"  data-required="true">
								<option value="-1"><?=Loc::getMessage('MAIN_REGISTER_MONTH');?></option>
								<?
									for($i=1;$i<=12;$i++)
									{
									?>
									<option value="<?=$i?>"><?=FormatDate('f', MakeTimeStamp('01.'.$i.'.'.date('Y')))?></option>
									<?
									}
								?>
							</select>

							<select name="birthyear" id="PERSONAL_BIRTHDAY_Y" class="js_select w102 sel_year"  data-required="true">
								<option value="-1"><?=Loc::getMessage('MAIN_REGISTER_YEAR');?></option>
								<?
									for($i=(date('Y')-14);$i>=(date('Y')-95);$i--)
									{
									?>
									<option value="<?=$i?>"><?=$i?></option>
									<?
									}
								?>
							</select>
						</div>									

					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings011">Серия и номер паспорта</label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<div class="field iblock"><input type="text"  data-required="required" value="<?=$arResult["VALUES"]['UF_PASSPORT_SERIES']?>" id="settings011" name="UF_PASSPORT_SERIES" class="input w110"></div>					  
							<div class="field iblock"><input type="text"  data-required="required" value="<?=$arResult["VALUES"]['UF_PASSPORT_NUMBER']?>" id="settings022" name="UF_PASSPORT_NUMBER" class="input w190"></div>						  
						</div>
					</div>
				</div>
			</div>

			<div class="wrapfield">
				<div class="b-form_header"><span class="iblock">Сфера специализации</span></div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings10">Место работы/учебы</label>
						<div class="field validate">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<input type="text" value="<?=$arResult["VALUES"]['WORK_COMPANY']?>" id="settings10" data-minlength="2" data-required="true" name="REGISTER[WORK_COMPANY]" class="input" >
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings08">Отрасль знаний</label>
						<div class="field validate iblock">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<select name="UF_BRANCH_KNOWLEDGE" id="settings08"  class="js_select w370"  data-required="true">
								<option value="">выберите</option>
								<?
									$arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE'));
									if(!empty($arUF_BRANCH_KNOWLEDGE))
									{
										foreach($arUF_BRANCH_KNOWLEDGE as $arItem)
										{
										?>
										<option value="<?=$arItem['ID']?>"><?=$arItem['VALUE']?></option>
										<?
										}
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings09">Образование. учёная степень</label>
						<div class="field validate iblock">
							<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
							<select name="UF_EDUCATION" id="settings09"  class="js_select w370"  data-required="true">
								<option value="">выберите</option>
								<?
									$arUF_EDUCATION = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION'));
									if(!empty($arUF_EDUCATION))
									{
										foreach($arUF_EDUCATION as $arItem)
										{
										?>
										<option <?=$arResult["VALUES"]['UF_EDUCATION'] == $arItem['ID'] ? 'selected="selected"':''?> value="<?=$arItem['ID']?>"><?=$arItem['VALUE']?></option>
										<?
										}
									}
								?>
							</select>
						</div>
					</div>
				</div>
			</div>


			<div class="b-form_header">Вход на сайт</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings04"><?=Loc::getMessage('MAIN_REGISTER_EMAIL');?></label>
					<div class="field validate">
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_EMAIL_FORMAT');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<input type="text" class="input" name="REGISTER[EMAIL]" data-minlength="2" data-maxlength="50" id="settings04" data-validate="email" value="<?=$arResult["VALUES"]['EMAIL']?>" data-required="true">
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<div class="passecurity">
						<div class="passecurity_lb">Защищенность пароля</div>

					</div>
					<label for="settings05"><span>не менее 6 символов</span>Пароль</label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="true" value="<?=$arResult["VALUES"]['PASSWORD']?>" id="settings05" data-maxlength="30" data-minlength="6" name="REGISTER[PASSWORD]" class="pass_status input">										
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings55">Подтвердить пароль</label>
					<div class="field validate">
						<em class="error identity "><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_MISMATCH');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
						<input data-identity="#settings05" type="password" data-required="true" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" id="settings55" data-maxlength="30" data-minlength="2" name="REGISTER[CONFIRM_PASSWORD]" class="input" data-validate="password">								
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings118">Мобильный телефон</label>
					<div class="field validate">
						<input type="text" class="input maskphone" name="REGISTER[PERSONAL_MOBILE]"  id="settings118"  value="<?=$arResult["VALUES"]['PERSONAL_MOBILE']?>">	
						<span class="phone_note">для смс-уведомлений о статусе Ваших заявок  на сайте РГБ</span>
					</div>
				</div>
			</div>



			<div class="wrapfield">
				<div class="b-form_header"><span class="iblock">Адрес регистрации</span></div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings03">Индекс <span class="minscreen">регистрации</span></label>
						<div class="field validate w140">
							<em class="error required">Заполните</em>
							<em class="error number">Некорректный почтовый индекс</em>
							<input type="text" data-validate="number" data-required="required" value="<?=$arResult["VALUES"]['PERSONAL_ZIP']?>" id="settings03" name="REGISTER[PERSONAL_ZIP]" class="input ">			  
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings055">Город <span class="minscreen">регистрации</span></label>
						<div class="field validate">
							<em class="error required">Заполните</em>
							<input type="text" data-required="required" value="<?=$arResult["VALUES"]['PERSONAL_CITY']?>" id="settings055" data-maxlength="30" data-minlength="2" name="REGISTER[PERSONAL_CITY]" class="input" >										
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings07">Улица <span class="minscreen">регистрации</span></label>
						<div class="field validate">
							<input type="text" value="<?=$arResult["VALUES"]['PERSONAL_STREET']?>" id="settings07" data-maxlength="30" data-minlength="2" data-required="true"  name="REGISTER[PERSONAL_STREET]" class="input">					
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<div class="field iblock fieldthree ">
							<label for="settings099">Дом:</label>
							<div class="field validate">
								<input type="text" class="input" name="UF_CORPUS" data-maxlength="20" id="settings099" value="<?=$arResult["VALUES"]['UF_CORPUS']?>"  data-required="true"/>
								<em class="error required">Заполните</em>
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings101">Строение: </label>
							<div class="field">
								<input type="text" class="input" name="UF_STRUCTURE" data-maxlength="20" maxlength="4" id="settings101" value="<?=$arResult["VALUES"]['UF_STRUCTURE']?>" />
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings111">Квартира: </label>
							<div class="field">
								<input type="text" class="input" name="UF_FLAT" data-maxlength="20" maxlength="4" id="settings111" value="<?=$arResult["VALUES"]['UF_FLAT']?>" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapfield">
				<div class="b-form_header">
					<span class="iblock">Адрес проживания</span>
					<div class="checkwrapper">
						<input class="checkbox addrindently" type="addr" name="UF_PLACE_REGISTR" value="1" id="cb3r"><label for="cb3r" class="black">по месту регистрации</label>
						<?
							if(!empty($arResult["VALUES"]['UF_PLACE_REGISTR']))
							{
							?>
							<script type="text/javascript">
								$(function(){
									$('input[name="UF_PLACE_REGISTR"]').click();
								});
							</script>
							<?

							}
						?>

					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings044">Индекс <span class="minscreen">проживания</span></label>
						<div class="field validate w140">
							<input type="text" data-validate="number" value="<?=$arResult["VALUES"]['WORK_ZIP']?>" id="settings044" name="REGISTER[WORK_ZIP]" class="input">			
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings066">Город <span class="minscreen">проживания</span></label>
						<div class="field validate">
							<input type="text" value="<?=$arResult["VALUES"]['WORK_CITY']?>" id="settings066" data-maxlength="30" data-minlength="2" name="REGISTER[WORK_CITY]" class="input" >							
						</div>
					</div> 
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<label for="settings086">Улица <span class="minscreen">проживания</span></label>
						<div class="field validate">
							<input type="text" value="<?=$arResult["VALUES"]['WORK_STREET']?>" id="settings086" data-maxlength="30" data-minlength="2" name="REGISTER[WORK_STREET]" class="input">					
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock inly">
						<div class="field iblock fieldthree ">
							<label for="settings0911">Дом:</label>
							<div class="field validate">
								<input type="text" class="input" name="UF_HOUSE2" data-maxlength="20" id="settings0911" value="<?=$arResult["VALUES"]['UF_HOUSE2']?>"/>
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings102">Строение: </label>
							<div class="field">
								<input type="text" class="input" name="UF_STRUCTURE2" data-maxlength="20" maxlength="4" id="settings102" value="<?=$arResult["VALUES"]['UF_STRUCTURE2']?>" />
							</div>
						</div>
						<div class="field iblock fieldthree">
							<label for="settings114">Квартира: </label>
							<div class="field">
								<input type="text" class="input" name="UF_FLAT2" data-maxlength="20" maxlength="4" id="settings114" value="<?=$arResult["VALUES"]['UF_FLAT2']?>" />
							</div>
						</div>
					</div>
				</div>
			</div>

			<hr>

			<?$APPLICATION->IncludeComponent(
					"notaext:plupload", 
					"scan_passport1_reg", 
					array(
						"MAX_FILE_SIZE" => "10",
						"FILE_TYPES" => "jpg,jpeg,png",
						"DIR" => "tmp_register",
						"FILES_FIELD_NAME" => "profile_file",
						"MULTI_SELECTION" => "N",
						"CLEANUP_DIR" => "Y",
						"UPLOAD_AUTO_START" => "Y",
						"RESIZE_IMAGES" => "Y",
						"RESIZE_WIDTH" => "4000",
						"RESIZE_HEIGHT" => "4000",
						"RESIZE_CROP" => "Y",
						"RESIZE_QUALITY" => "98",
						"UNIQUE_NAMES" => "Y",
					),
					false
				);?>


			<?$APPLICATION->IncludeComponent(
					"notaext:plupload", 
					"scan_passport2_reg", 
					array(
						"MAX_FILE_SIZE" => "10",
						"FILE_TYPES" => "jpg,jpeg,png",
						"DIR" => "tmp_register",
						"FILES_FIELD_NAME" => "profile_file",
						"MULTI_SELECTION" => "N",
						"CLEANUP_DIR" => "Y",
						"UPLOAD_AUTO_START" => "Y",
						"RESIZE_IMAGES" => "Y",
						"RESIZE_WIDTH" => "4000",
						"RESIZE_HEIGHT" => "4000",
						"RESIZE_CROP" => "Y",
						"RESIZE_QUALITY" => "98",
						"UNIQUE_NAMES" => "Y",
					),
					false
				);?>

			<hr>

			<div class="checkwrapper ">
				<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb33"><label for="cb33" class="black">Согласен с </label><a href="/user-agreement/" target="_blank">условиями использования</a> <label for="cb3" class="black">портала НЭБ</label>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix">
						<button disabled="disabled" type="submit" value="1" class="formbutton btdisable">Зарегистрироваться</button>
						<input type="hidden" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->
</section>