<?
$MESS['LIB_SEARCH_FORM_TEMPLATE_ALL'] = 'по всем полям';
$MESS['LIB_SEARCH_FORM_TEMPLATE_NAME'] = 'название';
$MESS['LIB_SEARCH_FORM_TEMPLATE_MPUBLISHPLACE'] = 'место издательства';
$MESS['LIB_SEARCH_FORM_TEMPLATE_MPUBLISHER'] = 'издательство';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AUTHOR'] = 'автор';
$MESS['LIB_SEARCH_FORM_TEMPLATE_ISBN'] = 'ISBN';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TEXT'] = 'по тексту';
$MESS['LIB_SEARCH_FORM_TEMPLATE_SEARCH'] = 'Расширенный поиск';
// $MESS['LIB_SEARCH_FORM_TEMPLATE_PORTAL'] = 'Искать только в отсканированных изданиях';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PORTAL'] = 'Искать в каталогах печатных книг';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TEXT'] = 'По каталогу печатных изданий';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FORACCESS'] = 'по доступу';
$MESS['LIB_SEARCH_FORM_TEMPLATE_OPEN_ACCESS'] = 'Открытый доступ';
$MESS['LIB_SEARCH_FORM_TEMPLATE_CLOSE_ACCESS'] = 'Закрытый доступ';
$MESS['LIB_SEARCH_FORM_TEMPLATE_EXAMPLES'] = 'Пример: биография писателей';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FIND'] = 'Найти';

$MESS['LIB_SEARCH_FORM_TEMPLATE_OR'] = 'или';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AND'] = 'и';
$MESS['LIB_SEARCH_FORM_TEMPLATE_NO'] = 'не';
$MESS['LIB_SEARCH_FORM_TEMPLATE_AUTHORBOOK'] = 'по автору';
$MESS['LIB_SEARCH_FORM_TEMPLATE_TITLE'] = 'по названию';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PUBLISHER'] = 'по издательствам';
$MESS['LIB_SEARCH_FORM_TEMPLATE_PUBLISHPLACE'] = 'по месту издания';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE'] = 'по дате';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE_PUBLIC'] = 'Дата публикации';

$MESS['LIB_SEARCH_FORM_TEMPLATE_ADD'] = 'добавить условие';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DEL'] = 'удалить условие';
$MESS['LIB_SEARCH_FORM_TEMPLATE_DATE_SUBMIT'] = 'Принять';

$MESS['LIB_SEARCH_FORM_TEMPLATE_ABOUT'] = 'О проекте';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FULLTEXT_SEARCH_ONLY'] = 'Искать только в отсканированных изданиях';
$MESS['LIB_SEARCH_FORM_TEMPLATE_STRICT_SEARCH'] = 'Искать по точной фразе';

$MESS['LIB_SEARCH_FORM_TEMPLATE_FIND_IN'] = 'Искать в найденном';

// $MESS['LIB_SEARCH_FORM_TEMPLATE_FOR_FULL_TEXT'] = 'Искать по полному тексту издания';
$MESS['LIB_SEARCH_FORM_TEMPLATE_FOR_FULL_TEXT'] = 'Искать по электронным копиям изданий';
$MESS['LIB_SEARCH_FORM_TEMPLATE_IN_DISERTATIONS'] = 'Искать в авторефератах и диссертациях';
