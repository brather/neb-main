<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;

$APPLICATION->IncludeComponent(
    "neb:library.plan_digital_add",
    "",
    Array(
        "CACHE_TIME" => $arParams["CACHE_TIME"],
    ),
    $component
);