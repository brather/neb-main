<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$date = explode('.', $arResult['arUser']['PERSONAL_BIRTHDAY']);

foreach ($date as $dates) {
    $arDate[] = intval($dates);
}
?>
<script>
    $(function () {
        if ($('#isESIAUser').val()) {
            var fields = $('#UF_ESIA_FIELDS').val();
            var arFields = fields.split('/');
            $.each(arFields, function (key, value) {
                if (value != "LOGIN") {
                    $('input[name="' + value + '"]').attr("disabled", "disabled");
                    /* $('[name="'+value+'"]').removeAttr("name"); */
                }
            });
        }
    });
</script>

<section class="innersection innerwrapper clearfix">
    <div class="b-searchresult" style="border: medium none;">
        <? $APPLICATION->ShowViewContent('menu_top') ?>
    </div>
    <div class="b-mainblock left">
        <div class="b-profile_set b-profile_brd rel">
            <h2><?= GetMessage('SETTING'); ?></h2>

            <? ShowError($arResult["strProfileError"]); ?>
            <?
            if ($arResult['DATA_SAVED'] == 'Y')
                ShowNote(GetMessage('PROFILE_DATA_SAVED'));
            ?>
            <?
            if ($arResult['arUser']['EXTERNAL_AUTH_ID'] != 'socservices') {
                ?>

                <? $APPLICATION->IncludeComponent("neb:main.password.reset", "", Array()); ?>

                <?
            }

            //тип регистрации ЕСИА? Скорем атрибут name у инпутов. ДЛя ЕСИА пользователей нельзя обновлять некоторые поля
            $isESIAUser = $arResult['arUser']['UF_REGISTER_TYPE'] == 40;
            if ($isESIAUser) {
                $ESIAField = $arResult['arUser']['UF_ESIA_FIELDS'];
                $arESIAFields = explode('/', $ESIAField);
            }
            ?>

            <form method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>"
                  class="b-form b-form_common b-profile_setform" enctype="multipart/form-data">
                <?= $arResult["BX_SESSION_CHECK"] ?>
                <input type="hidden" name="lang" value="<?= LANG ?>"/>
                <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
                <input type="hidden" id="UF_ESIA_FIELDS" value="<?= $arResult['arUser']['UF_ESIA_FIELDS'] ?>"/>
                <input type="hidden" id="isESIAUser" value="<?= $isESIAUser ?>"/>
                <div class="wrapfield wrapfield-left">
                    <div class="b-form_header"><?= GetMessage('GENERAL_CONST'); ?></div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings03"><?= GetMessage('LAST_NAME'); ?></label>
                            <div class="field validate">
                                <input type="text" value="<?= $arResult['arUser']['LAST_NAME'] ?>" id="settings03"
                                       data-maxlength="30" data-minlength="2" name="LAST_NAME" class="input">
                            </div>
                        </div>
                    </div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings03"><?= GetMessage('NAME'); ?></label>
                            <div class="field validate">
                                <input type="text" value="<?= $arResult['arUser']['NAME'] ?>" id="settings03"
                                       data-maxlength="30" data-minlength="2" name="NAME" class="input">
                            </div>
                        </div>
                    </div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings03"><?= GetMessage('SECOND_NAME'); ?></label>
                            <div class="field validate">
                                <input type="text" value="<?= $arResult['arUser']['SECOND_NAME'] ?>" id="settings03"
                                       data-maxlength="30" data-minlength="2" name="SECOND_NAME" class="input">
                            </div>
                        </div>
                    </div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings05"><?= GetMessage('POL'); ?></label>
                            <div class="field validate">
                                <select <? if (in_array("PERSONAL_GENDER", $arESIAFields)): ?>disabled="disabled"
                                        <? else: ?>name="PERSONAL_GENDER"<? endif; ?> id="" class="js_select w270">
                                    <option <? if ($arResult['arUser']['PERSONAL_GENDER'] == 'M') { ?> selected="selected" <? } ?>
                                            value="M"><?= GetMessage('MEN'); ?></option>
                                    <option <? if ($arResult['arUser']['PERSONAL_GENDER'] == 'F') { ?> selected="selected" <? } ?>
                                            value="F"><?= GetMessage('WOMEN'); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings06"><?= GetMessage('DATE'); ?></label>
                            <div class="field validate iblock seldate">
                                <em class="error dateformat"><?= GetMessage('ERROR_DATE_FORMAT'); ?></em>
                                <em class="error required"><?= GetMessage('CHOSE_DATE'); ?></em>
                                <em class="error yearsrestrict"><?= GetMessage('12_OLD'); ?></em>
                                <input type="hidden" class="realseldate" value="" id="settings06"
                                       <? if (in_array("PERSONAL_BIRTHDAY", $arESIAFields)): ?>disabled="disabled"
                                       <? else: ?>name="PERSONAL_BIRTHDAY"<? endif; ?>>
                                <select <? if (in_array("PERSONAL_BIRTHDAY", $arESIAFields)): ?>disabled="disabled"
                                        <? else: ?>name="birthday"<? endif; ?> id="PERSONAL_BIRTHDAY_D"
                                        class="js_select w102 sel_day">
                                    <option value=""><?= GetMessage('DAY'); ?></option>
                                    <? for ($i = 1; $i <= 31; $i++) {
                                        ?>
                                        <option <? if ($arDate['0'] == $i) {
                                            ?> selected='selected' <? } ?> value="<?= $i ?>"><?= $i ?></option>
                                    <? } ?>
                                </select>
                                <select <? if (in_array("PERSONAL_BIRTHDAY", $arESIAFields)): ?>disabled="disabled"
                                        <? else: ?>name="birthmonth"<? endif; ?> id="PERSONAL_BIRTHDAY_M"
                                        class="js_select w165 sel_month">
                                    <option value=""><?= GetMessage('MONTH'); ?></option>
                                    <? for ($i = 1; $i <= 12; $i++) { ?>
                                        <option <? if ($arDate['1'] == $i) { ?> selected='selected' <? } ?>
                                                value="<?= $i ?>"><?= FormatDate('f', MakeTimeStamp('01.' . $i . '.' . date('Y'))) ?></option>
                                    <? } ?>
                                </select>
                                <select <? if (in_array("PERSONAL_BIRTHDAY", $arESIAFields)): ?>disabled="disabled"
                                        <? else: ?>name="birthyear"<? endif; ?> id="PERSONAL_BIRTHDAY_Y"
                                        class="js_select w102 sel_year">
                                    <option value=""><?= GetMessage('YEAR'); ?></option>
                                    <? for ($i = (date('Y') - 14); $i >= (date('Y') - 95); $i--) {
                                        ?>
                                        <option <? if ($arDate['2'] == $i) {
                                            ?> selected='selected' <? } ?> value="<?= $i ?>"><?= $i ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings011"><?= GetMessage('MAIN_REGISTER_PASSPORT_NUMBER'); ?></label>
                            <div class="field validate">
                                <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                <div class="field iblock"><input type="text"
                                                                 value="<?= $arResult['arUser']['UF_PASSPORT_SERIES'] ?>"
                                                                 id="settings011" name="UF_PASSPORT_SERIES"
                                                                 class="input w110"></div>
                                <div class="field iblock"><input type="text"
                                                                 value="<?= $arResult['arUser']['UF_PASSPORT_NUMBER'] ?>"
                                                                 id="settings022" name="UF_PASSPORT_NUMBER"
                                                                 class="input w190"></div>
                            </div>
                        </div>
                    </div>

                    <div class="fieldcell iblock">
                        <label for="settings08"><?= GetMessage('MAIN_REGISTER_CITIZENSHIP'); ?></label>
                        <div class="field validate iblock">
                            <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                            <select <? if (in_array("UF_CITIZENSHIP", $arESIAFields)): ?>disabled="disabled"
                                    <? else: ?>name="UF_CITIZENSHIP"<? endif; ?> id="UF_CITIZENSHIP"
                                    class="js_select w370">
                                <?
                                $arUF_CITIZENSHIP = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'), array("VALUE" => "ASC"));
                                if (!empty($arUF_CITIZENSHIP)) {
                                    $last = count($arUF_CITIZENSHIP);
                                    foreach ($arUF_CITIZENSHIP as $arItem) {
                                        if ($arItem['XML_ID'] == $last) {
                                            $other = $arItem;
                                            continue;
                                        }
                                        if ($arItem['DEF'] == "Y") {
                                            $ruCitizenship = $arItem['ID'];
                                        }
                                        if (!empty($arResult["arUser"]['UF_CITIZENSHIP'])) {
                                            ?>
                                            <option value="<?= $arItem['ID'] ?>"<?= ($arResult["arUser"]['UF_CITIZENSHIP'] == $arItem['ID']) ? ' selected="selected"' : ''; ?>><?= $arItem['VALUE'] ?></option>
                                        <?
                                        } else {
                                            ?>
                                            <option value="<?= $arItem['ID'] ?>"<?= ($arItem['DEF'] == "Y") ? ' selected="selected"' : ''; ?>><?= $arItem['VALUE'] ?></option>
                                        <?
                                        }
                                    }
                                    ?>
                                    <option value="<?= $other['ID'] ?>"<?= ($other['DEF'] == "Y") ? ' selected="selected"' : ''; ?>><?= $other['VALUE'] ?></option>

                                    <?
                                }
                                ?>
                            </select>
                            <script type="text/javascript">
                                $(function () {
                                    var copyEducation = $('select[name="UF_EDUCATION"]').clone().removeClass("custom");

                                    function correctEducationField() {
                                        var cit = $('select[name="UF_CITIZENSHIP"] option:selected').val();
                                        var parentEducation = $('select[name="UF_EDUCATION"]').closest("div.field");
                                        parentEducation.children("span").remove();
                                        parentEducation.append(copyEducation.clone());
                                        $('#UF_EDUCATIONopts').remove();
                                        if (cit == <?php echo $ruCitizenship;?>) {
                                            $('select[name="UF_EDUCATION"] option').filter('[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
                                        }
                                        else {
                                            $('select[name="UF_EDUCATION"] option').not('[VALUE=""],[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
                                        }
                                        $('select[name="UF_EDUCATION"]').selectReplace();
                                    }

                                    correctEducationField();
                                    $('select[name="UF_CITIZENSHIP"]').change(function () {
                                        correctEducationField();
                                    });
                                    $('select[name="birthday"]').trigger("change");
                                });
                            </script>
                        </div>
                    </div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings07"><?= GetMessage('ABOUT'); ?></label>
                            <div class="field validate">
                                <textarea class="textarea" name="PERSONAL_NOTES" id="settings07" data-minlength="2"
                                          data-maxlength="800"><?= $arResult['arUser']['PERSONAL_NOTES'] ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="wrapfield">
                    <div class="b-form_header"><span
                                class="iblock"><?= GetMessage('MAIN_REGISTER_SPECIALIZATION'); ?></span></div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings10"><?= GetMessage('MAIN_REGISTER_PLACE_OF_EMPLOYMENT'); ?></label>
                            <div class="field validate">
                                <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                <input type="text" value="<?= $arResult["arUser"]['WORK_COMPANY'] ?>" id="settings10"
                                       data-maxlength="60" data-minlength="2" name="WORK_COMPANY" class="input">
                            </div>
                        </div>
                    </div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings08"><?= GetMessage('MAIN_REGISTER_KNOWLEDGE'); ?></label>
                            <div class="field validate iblock">
                                <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                <select name="UF_BRANCH_KNOWLEDGE" id="settings08" class="js_select w370">
                                    <option value=""><?= GetMessage('MAIN_REGISTER_SELECT'); ?></option>
                                    <?
                                    $arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE'));
                                    if (!empty($arUF_BRANCH_KNOWLEDGE)) {
                                        foreach ($arUF_BRANCH_KNOWLEDGE as $arItem) {
                                            ?>
                                            <option <?= $arResult["arUser"]['UF_BRANCH_KNOWLEDGE'] == $arItem['ID'] ? 'selected="selected"' : '' ?>
                                                    value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
                                            <?
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings09"><?= GetMessage('MAIN_REGISTER_EDUCATION'); ?></label>
                            <div class="field validate iblock">
                                <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                <select name="UF_EDUCATION" id="settings09" class="js_select w370">
                                    <option value=""><?= GetMessage('MAIN_REGISTER_SELECT'); ?></option>
                                    <?
                                    $arUF_EDUCATION = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION'));
                                    if (!empty($arUF_EDUCATION)) {
                                        foreach ($arUF_EDUCATION as $arItem) {
                                            ?>
                                            <option <?= $arResult["arUser"]['UF_EDUCATION'] == $arItem['ID'] ? 'selected="selected"' : '' ?>
                                                    value="<?= $arItem['ID'] ?>"><?= $arItem['VALUE'] ?></option>
                                            <?
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="b-form_header"><?= GetMessage('REGISTER_GROUP_SIGN'); ?></div>
                <?
                if (($arResult['arUser']['EXTERNAL_AUTH_ID'] != 'socservices')) {
                    ?>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings04"><?= GetMessage('MAIN_REGISTER_LOGIN'); ?></label>
                            <div class="field validate">
                                <em class="error validate"><?= GetMessage('MAIN_REGISTER_LOGIN_LENGTH_MIN'); ?></em>
                                <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                <input type="text" class="input" name="LOGIN" data-minlength="3" data-maxlength="50"
                                       id="settings04" data-validate="login"
                                       value="<?= $arResult["arUser"]['LOGIN'] ?>">
                            </div>
                        </div>
                    </div>
                    <?
                } else {
                    ?>
                    <input type="hidden" name="LOGIN" id="settings04" value="<?= $arResult["arUser"]['LOGIN'] ?>">
                <?
                }
                ?>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <label for="settings04"><?= GetMessage('MAIN_REGISTER_EMAIL'); ?></label>
                        <div class="field validate">
                            <em class="error validate"><?= GetMessage('MAIN_REGISTER_EMAIL_FORMAT'); ?></em>
                            <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                            <input type="text" class="input" name="EMAIL" data-minlength="3" data-maxlength="50"
                                   id="settings04" data-validate="email" value="<?= $arResult["arUser"]['EMAIL'] ?>">
                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <label for="settings11"><?= GetMessage('REGISTER_FIELD_PERSONAL_MOBILE'); ?></label>
                        <div class="field validate">
                            <input type="text" class="input maskphone" name="PERSONAL_MOBILE" id="settings11"
                                   value="<?= $arResult["arUser"]['PERSONAL_MOBILE'] ?>">
                            <span class="phone_note"><?= GetMessage('REGISTER_FIELD_PERSONAL_MOBILE_NOTE'); ?></span>
                        </div>
                    </div>
                </div>

                <div class="wrapfield wrapfield-left">
                    <div class="b-form_header"><span
                                class="iblock"><?= GetMessage('REGISTER_GROUP_ADDRESS_REGISTERED'); ?></span></div>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings03"><?= GetMessage('REGISTER_FIELD_PERSONAL_ZIP'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?></span></label>
                            <div class="field validate w140">
                                <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                <em class="error number"><?= GetMessage('REGISTER_FIELD_PERSONAL_ZIP_INCORRECT'); ?></em>
                                <input type="text" data-validate="number"
                                       value="<?= $arResult["arUser"]['PERSONAL_ZIP'] ?>" id="settings03"
                                       name="PERSONAL_ZIP" class="input ">
                            </div>
                        </div>
                    </div>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings033"><?= GetMessage('MAIN_REGISTER_REGION'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?></span></label>
                            <div class="field validate">
                                <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                <input type="text" value="<?= $arResult["arUser"]['UF_REGION'] ?>" id="settings033"
                                       data-maxlength="30" data-minlength="2" name="UF_REGION" class="input">
                            </div>
                        </div>
                    </div>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings013"><?= GetMessage('MAIN_REGISTER_AREA'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?></span></label>
                            <div class="field validate">
                                <input type="text" value="<?= $arResult["arUser"]['UF_AREA'] ?>" id="settings013"
                                       data-maxlength="30" data-minlength="2" name="UF_AREA" class="input">
                            </div>
                        </div>
                    </div>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings055"><?= GetMessage('REGISTER_FIELD_PERSONAL_CITY'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?></span></label>
                            <div class="field validate">
                                <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                <input type="text" value="<?= $arResult["arUser"]['PERSONAL_CITY'] ?>" id="settings055"
                                       data-maxlength="30" data-minlength="2" name="PERSONAL_CITY" class="input">
                            </div>
                        </div>
                    </div>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <label for="settings07"><?= GetMessage('REGISTER_FIELD_PERSONAL_STREET'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_REG_NOTE'); ?></span></label>
                            <div class="field validate">
                                <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                <input type="text" value="<?= $arResult["arUser"]['PERSONAL_STREET'] ?>" id="settings07"
                                       data-maxlength="30" data-minlength="2" name="PERSONAL_STREET" class="input">
                            </div>
                        </div>
                    </div>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock">
                            <div class="field iblock fieldthree ">
                                <label for="settings09"><?= GetMessage('REGISTER_FIELD_PERSONAL_HOUSE'); ?>:</label>
                                <div class="field validate">
                                    <input type="text" class="input" name="UF_CORPUS" data-maxlength="20"
                                           id="settings09" value="<?= $arResult["arUser"]['UF_CORPUS'] ?>"/>
                                    <em class="error required"><?= GetMessage('MAIN_REGISTER_REQ'); ?></em>
                                </div>
                            </div>
                            <div class="field iblock fieldthree">
                                <label for="settings10"><?= GetMessage('REGISTER_FIELD_PERSONAL_BUILDING'); ?>: </label>
                                <div class="field">
                                    <input type="text" class="input" name="UF_STRUCTURE" data-maxlength="20"
                                           maxlength="4" id="settings10"
                                           value="<?= $arResult["arUser"]['UF_STRUCTURE'] ?>"/>
                                </div>
                            </div>
                            <div class="field iblock fieldthree">
                                <label for="settings11"><?= GetMessage('REGISTER_FIELD_PERSONAL_APPARTMENT'); ?>
                                    : </label>
                                <div class="field">
                                    <input type="text" class="input" name="UF_FLAT" data-maxlength="20" maxlength="4"
                                           id="settings11" value="<?= $arResult["arUser"]['UF_FLAT'] ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="wrapfield">
                    <div class="b-form_header">
                        <span class="iblock"><?= GetMessage('REGISTER_GROUP_ADDRESS_RESIDENCE'); ?></span>
                        <div class="checkwrapper">
                            <input class="checkbox addrindently" type="addr" name="UF_PLACE_REGISTR" id="cb3r"><label
                                    for="cb3r"
                                    class="black"><?= GetMessage('REGISTER_GROUP_ADDRESS_CHECKBOX_NOTE'); ?></label>
                            <?
                            if (!empty($arResult["arUser"]['UF_PLACE_REGISTR'])) {
                                ?>
                                <script type="text/javascript">
                                    $(function () {
                                        $('input[name="UF_PLACE_REGISTR"]').click();
                                    });
                                </script>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                    <input type="hidden" class="hidden-fieldgroup" value="" name="WORK_ZIP"/>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock inly">
                            <label for="settings044"><?= GetMessage('REGISTER_FIELD_PERSONAL_ZIP'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?></span></label>
                            <div class="field validate w140">
                                <input type="text" data-validate="number" value="<?= $arResult["arUser"]['WORK_ZIP'] ?>"
                                       id="settings044" name="WORK_ZIP" class="input">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="hidden-fieldgroup" value="" name="UF_REGION2"/>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock inly">
                            <label for="settings033r"><?= GetMessage('MAIN_REGISTER_REGION'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?></span></label>
                            <div class="field validate">
                                <input type="text" value="<?= $arResult["arUser"]['UF_REGION2'] ?>" id="settings033r"
                                       data-maxlength="30" data-minlength="2" name="UF_REGION2" class="input">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="hidden-fieldgroup" value="" name="UF_AREA2"/>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock inly">
                            <label for="settings013r"><?= GetMessage('MAIN_REGISTER_AREA'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?></span></label>
                            <div class="field validate">
                                <input type="text" value="<?= $arResult["arUser"]['UF_AREA2'] ?>" id="settings013r"
                                       data-maxlength="30" data-minlength="2" name="UF_AREA2" class="input">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="hidden-fieldgroup" value="" name="WORK_CITY"/>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock inly">
                            <label for="settings066"><?= GetMessage('REGISTER_FIELD_PERSONAL_CITY'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?></span></label>
                            <div class="field validate">
                                <input type="text" value="<?= $arResult["arUser"]['WORK_CITY'] ?>" id="settings066"
                                       data-maxlength="30" data-minlength="2" name="WORK_CITY" class="input">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="hidden-fieldgroup" value="" name="WORK_STREET"/>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock inly">
                            <label for="settings08r"><?= GetMessage('REGISTER_FIELD_PERSONAL_STREET'); ?> <span
                                        class="minscreen"><?= GetMessage('REGISTER_GROUP_ADDRESS_RES_NOTE'); ?></span></label>
                            <div class="field validate">
                                <input type="text" value="<?= $arResult["arUser"]['WORK_STREET'] ?>" id="settings08r"
                                       data-maxlength="30" data-minlength="2" name="WORK_STREET" class="input">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="hidden-fieldgroup" value="" name="UF_HOUSE2"/>
                    <input type="hidden" class="hidden-fieldgroup" value="" name="UF_STRUCTURE2"/>
                    <input type="hidden" class="hidden-fieldgroup" value="" name="UF_FLAT2"/>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock inly">
                            <div class="field iblock fieldthree ">
                                <label for="settings09r"><?= GetMessage('REGISTER_FIELD_PERSONAL_HOUSE'); ?>:</label>
                                <div class="field validate">
                                    <input type="text" class="input" name="UF_HOUSE2" data-maxlength="20"
                                           id="settings09r" value="<?= $arResult["arUser"]['UF_HOUSE2'] ?>"/>
                                </div>
                            </div>
                            <div class="field iblock fieldthree">
                                <label for="settings10"><?= GetMessage('REGISTER_FIELD_PERSONAL_BUILDING'); ?>: </label>
                                <div class="field">
                                    <input type="text" class="input" name="UF_STRUCTURE2" data-maxlength="20"
                                           maxlength="4" id="settings10"
                                           value="<?= $arResult["arUser"]['UF_STRUCTURE2'] ?>"/>
                                </div>
                            </div>
                            <div class="field iblock fieldthree">
                                <label for="settings11"><?= GetMessage('REGISTER_FIELD_PERSONAL_APPARTMENT'); ?>
                                    : </label>
                                <div class="field">
                                    <input type="text" class="input" name="UF_FLAT2" data-maxlength="20" maxlength="4"
                                           id="settings11" value="<?= $arResult["arUser"]['UF_FLAT2'] ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="wrapfield wrapfield-left">
                    <?php $APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
                        "SHOW_PROFILES" => "Y",
                        "ALLOW_DELETE" => "Y"
                    ),
                        false
                    ); ?>
                </div>

                <div class="wrapfield">
                    <div class="b-form_header"><br><?= GetMessage('SETTINGS'); ?></div>
                    <div class="fieldrow nowrap">
                        <div class="fieldcell iblock b-srch_numset">
                            <label for="settings08"><?= GetMessage('COUNT_SEARCH'); ?></label>
                            <div class="field validate searchnum_set">
                                <a href="#" data='15'
                                   class="cnts b-search_numset iblock <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT'] == 15) { ?> current <? } ?>">15</a>
                                <a href="#" data='30'
                                   class="cnts b-search_numset iblock <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT'] == 30) { ?> current <? } ?>">30</a>
                                <a href="#" data='45'
                                   class="cnts b-search_numset iblock <? if ($arResult['arUser']['UF_SEARCH_PAGE_COUNT'] == 45) { ?> current <? } ?>">45</a>
                            </div>
                            <input type="hidden" class="" value="<?= $arResult['arUser']['UF_SEARCH_PAGE_COUNT'] ?>"
                                   id="cnts" name="UF_SEARCH_PAGE_COUNT">
                        </div>
                        <? /**
                         * <div class="fieldcell iblock hidden">
                         * <label for="settings09"><?=GetMessage('MAX_COUNT');?></label>
                         * <div class="field validate">
                         * <input type="text" value="" id="settings09" data-maxlength="300" data-minlength="2" name="pass" class="input docnum">
                         * </div>
                         * </div>
                         */ ?>
                    </div>
                </div>

                <?
                $sImage = (intval($arResult['arUser']["UF_SCAN_PASSPORT1"]) > 0) ? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT1"]) : '';
                if ($arResult['arUser']["UF_SCAN_PASSPORT1"]["SRC"]) {
                    $sImage = $arResult['arUser']["UF_SCAN_PASSPORT1"]["SRC"];
                }
                ?>
                <? $APPLICATION->IncludeComponent(
                    "notaext:plupload",
                    "scan_passport1",
                    array(
                        "MAX_FILE_SIZE" => "10",
                        "FILE_TYPES" => "jpg,jpeg,png",
                        "DIR" => "tmp_register",
                        "FILES_FIELD_NAME" => "profile_file",
                        "MULTI_SELECTION" => "N",
                        "CLEANUP_DIR" => "Y",
                        "UPLOAD_AUTO_START" => "Y",
                        "UNIQUE_NAMES" => "Y",
                        "PERSONAL_PHOTO" => $sImage,
                        "FULL_NAME" => $arResult['arUser']['NAME'] . ' ' . $arResult['arUser']['LAST_NAME'],
                        "USER_EMAIL" => $arResult['arUser']['EMAIL'],
                        "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
                        "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
                    ),
                    false
                ); ?>
                <?
                $sImage = (intval($arResult['arUser']["UF_SCAN_PASSPORT2"]) > 0) ? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT2"]) : '';
                if ($arResult['arUser']["UF_SCAN_PASSPORT2"]["SRC"]) {
                    $sImage = $arResult['arUser']["UF_SCAN_PASSPORT2"]["SRC"];
                }
                ?>
                <? $APPLICATION->IncludeComponent(
                    "notaext:plupload",
                    "scan_passport2",
                    array(
                        "MAX_FILE_SIZE" => "10",
                        "FILE_TYPES" => "jpg,jpeg,png",
                        "DIR" => "tmp_register",
                        "FILES_FIELD_NAME" => "profile_file",
                        "MULTI_SELECTION" => "N",
                        "CLEANUP_DIR" => "Y",
                        "UPLOAD_AUTO_START" => "Y",
                        "UNIQUE_NAMES" => "Y",
                        "PERSONAL_PHOTO" => $sImage,
                        "FULL_NAME" => $arResult['arUser']['NAME'] . ' ' . $arResult['arUser']['LAST_NAME'],
                        "USER_EMAIL" => $arResult['arUser']['EMAIL'],
                        "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
                        "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
                    ),
                    false
                ); ?>

                <? if ($arResult["arLibrary"]): ?>
                    <div class="b-form_header"><?= GetMessage('PROPERTY_LIBRARY'); ?></div>
                    <div class="field iblock">
                        <label for="settings11"><?= GetMessage('PROPERTY_LIBRARY_SCAN_COST'); ?> </label>
                        <div class="field">
                            <input type="text" class="input" name="PROPERTY_SCAN_COST" data-maxlength="20" maxlength="4"
                                   id="settings11" value="<?= $arResult["arLibrary"]['SCAN_COST'] ?>"/>
                        </div>
                    </div>
                <? endif; ?>

                <div class="fieldrow nowrap fieldrowaction">
                    <div class="fieldcell ">
                        <div class="field clearfix">
                            <button name="save" class="formbtn" value="1"
                                    type="submit"><?= GetMessage('SAVE'); ?></button>
                        </div>
                    </div>
                </div>

            </form>

        </div><!-- /.b-profilesettings-->
    </div><!-- /.b-mainblock -->
    <? #pre($arResult['arUser'],1);?>
    <?
    $sImage = (intval($arResult['arUser']['PERSONAL_PHOTO']) > 0) ? CFile::GetPath($arResult['arUser']['PERSONAL_PHOTO']) : '';
    if ($arResult['arUser']["PERSONAL_PHOTO"]["SRC"]) {
        $sImage = $arResult['arUser']["PERSONAL_PHOTO"]["SRC"];
    }
    ?>
    <?
    if ('N' !== @$arParams['SHOW_FACE']) {
        $APPLICATION->IncludeComponent(
            "notaext:plupload",
            "profile",
            array(
                "MAX_FILE_SIZE" => "24",
                "FILE_TYPES" => "jpg,jpeg,png",
                "DIR" => "tmp_register",
                "FILES_FIELD_NAME" => "profile_file",
                "MULTI_SELECTION" => "N",
                "CLEANUP_DIR" => "Y",
                "UPLOAD_AUTO_START" => "Y",
                "RESIZE_IMAGES" => "Y",
                "RESIZE_WIDTH" => "110",
                "RESIZE_HEIGHT" => "110",
                "RESIZE_CROP" => "Y",
                "RESIZE_QUALITY" => "98",
                "UNIQUE_NAMES" => "Y",
                "PERSONAL_PHOTO" => $sImage,
                "FULL_NAME" => $arResult['arUser']['NAME'] . ' '
                    . $arResult['arUser']['LAST_NAME'],
                "USER_EMAIL" => $arResult['arUser']['EMAIL'],
                "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
                "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
                "UF_REGISTER_TYPE" => $arResult['arUser']['UF_REGISTER_TYPE'],
                "UF_RGB_CARD_NUMBER" => $arResult['arUser']['UF_RGB_CARD_NUMBER'],
            ),
            false
        );
    }
    ?>
</section>
</div><!-- /.homepage -->
