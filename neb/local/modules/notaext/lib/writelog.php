<?
namespace Bitrix\NotaExt;

/*
 * Класс для логгирования в файл
 *
 * Пример использования:
 *      $logF=new WriteLog($_SERVER['DOCUMENT_ROOT'].'/voteLog.txt');
 *      $logF->addLog('$USER_ALREADY_VOTE='.$USER_ALREADY_VOTE.' $USER_GROUP_PERMISSION'.$USER_GROUP_PERMISSION.' '.time());
 */
Class WriteLog
{
    protected $file = '';
    protected $pLog;

    public function __construct($file)
    {
        if (strlen($file) <= 0)
            return false;
        $this->file = $file;
        $this->pLog = fopen($this->file, "a+");
        //$this->addLog('Start');
    }

    public function addLog($text)
    {
        fputs($this->pLog, date("d.m.Y H:i:s ") . $text . "\n");
    }

    private function CloseLogFile()
    {
        fclose($this->pLog);
    }

    public function __destruct()
    {
        $this->CloseLogFile();
    }
}
