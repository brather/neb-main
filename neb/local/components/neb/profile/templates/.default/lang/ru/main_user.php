<?php

$MESS['PROFILE_MU_CLOSE_WINDOW'] = 'Закрыть окно';
$MESS['PROFILE_MU_ACCESS_QUESTION'] = 'Доступ к закрытому изданию возможен только для пользователей НЭБ, прошедших полную регистрацию. Перейти к регистрации?';
$MESS['PROFILE_MU_YES'] = 'Да';
$MESS['PROFILE_MU_NO'] = 'Нет';
$MESS['PROFILE_MU_NUMBER_LC'] = 'Номер ЕЭЧБ';
