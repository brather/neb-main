<?php
/**
 * User: agolodkov
 * Date: 15.08.2016
 * Time: 16:32
 */

namespace LbcBundle\Entity;

use Gedmo\Tree\Entity\MappedSuperclass\AbstractClosure;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="lbc_specialdivisionsections_closure")
 * @ORM\Entity
 */
class LbcSpecialdivisionsectionsClosure extends AbstractClosure
{

}