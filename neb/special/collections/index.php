<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Выставки");
?>

<?$APPLICATION->IncludeComponent(
    "neb:collections",
    "special",
    array(
        "IBLOCK_TYPE" => "collections",
        "IBLOCK_ID" => "6",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/special/collections/",
        "SEF_URL_TEMPLATES" => array(
            "list" => "",
            "seaction" => "#ID#_#CODE#/",
        )
    ),
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>