<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\Main\Entity,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Application,
    \Bitrix\NotaExt\Iblock\Element;

if (!empty($arResult['ITEMS'])) {

    $arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

    function getClassHL($codeHL) {

        $connection = Application::getConnection();
        $sql = "SELECT ID FROM b_hlblock_entity WHERE NAME='" . $codeHL . "';";
        $recordset  = $connection->query($sql)->fetch();

        Loader::includeModule('highloadblock');
        $hlblock    = HighloadBlockTable::getById($recordset['ID'])->fetch();
        $dataClass  = HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $dataClass;
    }

    $dClassPlan = getClassHL(HIBLOCK_CODE_DIGITIZATION_PLAN_LIST);
    $dClassBooks = getClassHL(HIBLOCK_CODE_DIGITIZATION_PLAN);

    $arInfoPlans = array();

    foreach($arResult['ITEMS'] as $iItem => $arItem) {

        $arCurrent = $arlibrary = array();

        if($arItem['idparent2']) {
            $arCurrent = $dClassBooks::getList(array(
                "select" => array("*"),
                "filter" => array('=UF_EXALEAD_PARENT' => $arItem['idparent2']),
            ))->fetch();
        }

        if(!$arCurrent) {
            $arCurrent  = $dClassBooks::getList(array(
                "select" => array("*"),
                "filter" => array('=UF_EXALEAD_BOOK' => $arItem['id']),
            ))->fetch();
        }
        $arResult['ITEMS'][$iItem]["UF_DATE_FINISH_CURRENT"] = $arCurrent["UF_DATE_FINISH"];
        
        // Если данное издание существует в базе сайта но оцифровывается другой библиотекой
        if(!empty($arCurrent) && intval($arCurrent['UF_LIBRARY']) != intval($arParams['UF_LIBRARY']["ID"])) {

            // Вернем информацию по библиотеке
            $arLibrary = Element::getByID($arCurrent['UF_LIBRARY'], array('NAME', 'DETAIL_PAGE_URL', 'PROPERTY_STATUS', 'PROPERTY_LIBRARY_LINK',));

            $arResult['ITEMS'][$iItem]['BUTTON'] = array(
                "CLASS" => "",
                "MESSAGE" => Loc::getMessage("RM_BOOK_DIGITIZED"),
                "LIB_NAME" => $arLibrary["NAME"]
            );
        }

        // Если данное издание существует в базе сайта и оцифровывается текущей библиотекой
        elseif(!empty($arCurrent) && intval($arCurrent["UF_LIBRARY"]) == intval($arParams['UF_LIBRARY']["ID"])) {

            // Если издание находится в текущем плане
            if($arRequest['plan_id'] == $arCurrent['UF_PLAN_ID']) {
                $arResult['ITEMS'][$iItem]['BUTTON'] = array(
                    "CLASS" => "minus",
                    "MESSAGE" => Loc::getMessage("RM_BOOK_DELETE"),
                    "LIB_NAME" => ""
                );
                $arResult['ITEMS'][$iItem]['PLAN'] = $arCurrent;
            }

            // Если издание находится в другом плане плане
            elseif(!empty($arCurrent['UF_PLAN_ID'])) {

                // У нас уже записана инфорамция по данному плану
                if(!empty($arInfoPlans[$arCurrent["UF_PLAN_ID"]])) {
                    $arResult['ITEMS'][$iItem]['BUTTON'] = array(
                        "CLASS" => "",
                        "MESSAGE" => Loc::getMessage("RM_BOOK_IN_PLAN") . $arInfoPlans[$arCurrent["UF_PLAN_ID"]]["UF_PLAN_NAME"],
                        "LIB_NAME" => ""
                    );
                }
                // Информация по данному плану у нас отсутствут, вернем ее и запишем в массив $arInfoPlans
                else {
                    $arInfoPlans[$arCurrent["UF_PLAN_ID"]] = $dClassPlan::getList(array(
                        "filter" => array("ID" => $arCurrent["UF_PLAN_ID"]),
                        "select" => array('UF_PLAN_NAME')
                    ))->fetch();

                    if (empty($arInfoPlans[$arCurrent["UF_PLAN_ID"]])) {
                        $arResult['ITEMS'][$iItem]['BUTTON'] = array(
                            "CLASS" => "plus",
                            "MESSAGE" => Loc::getMessage("RM_BOOK_ADD_TO_PLAN"),
                            "LIB_NAME" => ""
                        );
                    } else {
                        $arResult['ITEMS'][$iItem]['BUTTON'] = array(
                            "CLASS" => "",
                            "MESSAGE" => Loc::getMessage("RM_BOOK_IN_PLAN") . $arInfoPlans[$arCurrent["UF_PLAN_ID"]]["UF_PLAN_NAME"],
                            "LIB_NAME" => ""
                        );
                    }
                }
            }
            // Издание не находится ни в одном из планов
            else {
                $arResult['ITEMS'][$iItem]['BUTTON'] = array(
                    "CLASS" => "plus",
                    "MESSAGE" => Loc::getMessage("RM_BOOK_ADD_TO_PLAN"),
                    "LIB_NAME" => ""
                );
            }
        }
        // Издание не оцифровывается ни одной из библиотек и его можно добавить в план
        else {
            $arResult['ITEMS'][$iItem]['BUTTON'] = array(
                "CLASS" => "plus",
                "MESSAGE" => Loc::getMessage("RM_BOOK_ADD_TO_PLAN"),
                "LIB_NAME" => ""
            );
        }
    }
}