<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Loader,
    Nota\UserData\rgb;

Loader::includeModule('nota.userdata');

/**
 * Если пользователь регится по упрощенной ,но его емейл есть в
 * РГБ- выводим уведомление, что он является читателем РГБ и может получить
 * полный доступ если захочет. "ДА" - регим его как читателя РГБ, "НЕТ" - он остается упрощенцем
 */

if (intval($arResult['VALUES']['USER_ID']) > 0):

    $rgb = new rgb();

    $RGBuser = $rgb->usersFind('email', $arFields['email']);

    if ($RGBuser['count']):
        $user = new nebUser();
        $res = $user->Authorize($arResult['VALUES']['USER_ID']);

        LocalRedirect('/popup?action=fullaccess');
    endif;
endif;
