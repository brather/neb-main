<?php use LbcBundle\AcceptanceTester;
$I = new AcceptanceTester($scenario);
$I->wantTo('Search specialdivision');
$query = 'Публицистические произведения древних и средневековых авторов';
$I->amOnPage('/bbk/search/?specialdivision=' . $query);
$I->see($query);
