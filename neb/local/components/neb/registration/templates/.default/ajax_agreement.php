<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-bookpopup popup offerta-popup" >
	<a href="#" id="closebtn" class="closepopup close_in"><?=Loc::getMessage('AJAX_AGREEMENT_CLOSE_POPUP');?></a>
	<div class="b-bookpopup_in bbox offerta-popup-in">
		<div id="div_iframe">
			<!--iframe id="iframe" src="/local/components/neb/main.register/templates/lib_new_user/agreement.php" height="500px" width="100%"></iframe-->
			<iframe id="iframe" src="/user-agreement/agreement-text.php" height="500px" width="100%"></iframe>
		</div>			
		<div class="checkwrapper">
			<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb1"><label for="cb1" class="black"><?=Loc::getMessage('AJAX_AGREEMENT_AGREE_WITH_PROCESSING_DATA');?></label>
			<div class="field clearfix divdisable">
				<a href="javascript:void(0)" data-onclick="sign_up();" data-click-signup class="formbutton btdisable close_popup_ok"><?=Loc::getMessage('AJAX_AGREEMENT_AGREE_WITH_TERMS');?></a>
				<div class="b-hint bhint_right bhint_right_agreement"><?=Loc::getMessage('AJAX_AGREEMENT_READ_TERMS');?></div>
			</div>
		</div>
	</div>
</div>
<script>
$(function(){
	var scroll_bool = false;
	var check_bool = false;
	$('#iframe').load(function(){
		if (!($('button.formbutton').hasClass('btdisable')))
		{
			scroll_bool = true;
	        $('.b-bookpopup_in .checkbox').trigger("click");
		}
		addDisable($('#regform button.formbutton'));
	});
	/* Определяем IE или нет */
	var isIE = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
	var isIElt10 = /*@cc_on!@*/false;
	var fr = document.getElementById('iframe');
	var fw;
	if(isIE || isIElt10){
		fw = fr.contentDocument;
	} 
	else{
		fw = fr.contentWindow ;
	}
	/*if ( !!!fw.document) fw = fw.defaultView;*/

	/*Костыль для сафари и оперы до 12 версии*/
	var isSafari = /constructor/i.test(window.HTMLElement);
	var isOpera = !!window.opera;
	if(isSafari || isOpera){
		setTimeout(function(){
			scroll_bool = true;
			check_status();}, 6000);
	}

	fw.onscroll = function(){
		if($(fw).scrollTop() >= ($(fw).height() - $(fr).height()) - 200)
		{
			scroll_bool = true;
			check_status();
		}
	}

	$('.agreebox').on('change',function(){
		if($('.agreebox').attr("checked"))
			check_bool = true;
		else
			check_bool = false;
		check_status();
	});

	function check_status(){
		if ((scroll_bool == true) && (check_bool == true))
		{
			removeDisable($('a.formbutton'));
		}
		else
		{
			addDisable($('a.formbutton'));
		}
	}

	function sign_up(){
		if (!($('a.formbutton').hasClass("btdisable")))
		{
			$('.uipopupcontainer').dialog("close");
			removeDisable($('#regform button.formbutton'));
		}
		return false;
	}
	$(document).on('click','[data-click-signup]',function(){
		sign_up();
	});

	function removeDisable(elem){
		elem.removeClass('btdisable');
		elem.prop('disabled', false);
		elem.parent().removeClass('divdisable');
	}
	function addDisable(elem){
		elem.addClass('btdisable');
		elem.prop('disabled', true);
		elem.parent().addClass('divdisable');
	}
	(function(){
		$('.offerta-popup').parent().parent().css('min-width','440px');
	})();
});
</script>