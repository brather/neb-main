<?php

namespace Application\Migrations;

use AppBundle\Db\AbstractBitrixDoctrineMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160620161530 extends AbstractBitrixDoctrineMigration
{

    private $_migrationData
        = [
            'HBLOCK'      => [
                [
                    'NAME'       => 'BooksData',
                    'TABLE_NAME' => 'neb_books_data'
                ],
            ],
            'USER_FIELDS' => [
                [
                    'ENTITY_ID'     => 'HLBLOCK_BooksData',
                    'FIELD_NAME'    => 'UF_BOOK_ID',
                    'USER_TYPE_ID'  => 'string',
                    'XML_ID'        => 'book_id',
                    'SORT'          => '5',
                    'MULTIPLE'      => 'N',
                    'MANDATORY'     => 'N',
                    'SHOW_FILTER'   => 'N',
                    'SHOW_IN_LIST'  => 'Y',
                    'EDIT_IN_LIST'  => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS'      => [
                        'SIZE'          => '0',
                        'ROWS'          => '1',
                        'REGEXP'        => '',
                        'MIN_LENGTH'    => '0',
                        'MAX_LENGTH'    => '0',
                        'DEFAULT_VALUE' => ''
                    ]
                ],
                [
                    'ENTITY_ID'     => 'HLBLOCK_BooksData',
                    'FIELD_NAME'    => 'UF_BOOK_NAME',
                    'USER_TYPE_ID'  => 'string',
                    'XML_ID'        => 'book_name',
                    'SORT'          => '10',
                    'MULTIPLE'      => 'N',
                    'MANDATORY'     => 'N',
                    'SHOW_FILTER'   => 'N',
                    'SHOW_IN_LIST'  => 'Y',
                    'EDIT_IN_LIST'  => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS'      => [
                        'SIZE'          => '0',
                        'ROWS'          => '1',
                        'REGEXP'        => '',
                        'MIN_LENGTH'    => '0',
                        'MAX_LENGTH'    => '0',
                        'DEFAULT_VALUE' => ''
                    ]
                ],
                [
                    'ENTITY_ID'     => 'HLBLOCK_BooksData',
                    'FIELD_NAME'    => 'UF_BOOK_AUTHOR',
                    'USER_TYPE_ID'  => 'string',
                    'XML_ID'        => 'book_author',
                    'SORT'          => '20',
                    'MULTIPLE'      => 'N',
                    'MANDATORY'     => 'N',
                    'SHOW_FILTER'   => 'N',
                    'SHOW_IN_LIST'  => 'Y',
                    'EDIT_IN_LIST'  => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS'      => [
                        'SIZE'          => '0',
                        'ROWS'          => '1',
                        'REGEXP'        => '',
                        'MIN_LENGTH'    => '0',
                        'MAX_LENGTH'    => '0',
                        'DEFAULT_VALUE' => ''
                    ]
                ],
                [
                    'ENTITY_ID'     => 'HLBLOCK_BooksData',
                    'FIELD_NAME'    => 'UF_BOOK_PUBLISH_YEAR',
                    'USER_TYPE_ID'  => 'integer',
                    'XML_ID'        => 'book_publish_year',
                    'SORT'          => '30',
                    'MULTIPLE'      => 'N',
                    'MANDATORY'     => 'N',
                    'SHOW_FILTER'   => 'N',
                    'SHOW_IN_LIST'  => 'Y',
                    'EDIT_IN_LIST'  => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS'      => [
                        'SIZE'          => '20',
                        'MIN_VALUE'     => '0',
                        'MAX_VALUE'     => '0',
                        'DEFAULT_VALUE' => ''
                    ]
                ],
            ],
        ];

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->_includeBitrix();

        error_reporting(E_ERROR);
        ini_set('display_errors', 1);


        $obNebMigration = new \NebMigration();
        $obNebMigration->setData($this->_migrationData);
        $this->write(print_r($obNebMigration->getReport(), true));

        $this->addSql(
            <<<SQL
ALTER TABLE neb_books_data MODIFY
UF_BOOK_ID TEXT
COLLATE utf8_unicode_ci;
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->_includeBitrix();

        error_reporting(E_ERROR);
        ini_set('display_errors', 1);

        $obNebMigration = new \NebMigration();
        $obNebMigration->deleteData($this->_migrationData);
        $this->write(print_r($obNebMigration->getReport(), true));
    }
}
