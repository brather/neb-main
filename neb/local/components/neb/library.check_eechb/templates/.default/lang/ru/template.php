<?
$MESS['LIBRARY_CHECK_EECHB_CHECK_ELP'] = 'Проверка ЕЭЧБ';
$MESS['LIBRARY_CHECK_EECHB_GIVEN_DATA'] = 'Предоставленные данные';
$MESS['LIBRARY_CHECK_EECHB_FIO_SHORT'] = 'ФИО';
$MESS['LIBRARY_CHECK_EECHB_REQ'] = 'Поле обязательно для заполнения';
$MESS['LIBRARY_CHECK_EECHB_INCORRECT'] = 'Поле заполнено неверно';
$MESS['LIBRARY_CHECK_EECHB_ELP_NUMBER'] = 'Номер Единого Читательского билета';
$MESS['LIBRARY_CHECK_EECHB_CHECK'] = 'Проверить';
$MESS['LIBRARY_CHECK_EECHB_404'] = 'В базе данных НЭБ не найдено соответствий';
$MESS['LIBRARY_CHECK_EECHB_CHECK_CORRECT'] = 'Проверьте правильность введенных даннных или';
$MESS['LIBRARY_CHECK_EECHB_CREATE_NEW'] = 'заведите нового читателя';
$MESS['LIBRARY_CHECK_EECHB_FIO'] = 'Фамилия <br />Имя Отчество';
$MESS['LIBRARY_CHECK_EECHB_ELP_NUMBER_SHORT'] = 'Номер ЕЧБ';
$MESS['LIBRARY_CHECK_EECHB_REG_DATE'] = 'Дата <br>регистрации ';
$MESS['LIBRARY_CHECK_EECHB_STATUS'] = 'Статус';
$MESS['LIBRARY_CHECK_EECHB_VERIFIED'] = 'Верифицированный';
$MESS['LIBRARY_CHECK_EECHB_ACTION'] = 'Действие';
$MESS['LIBRARY_CHECK_EECHB_EDIT'] = 'Изменить';
$MESS['LIBRARY_CHECK_EECHB_DELETE'] = 'Удалить';
$MESS['LIBRARY_CHECK_EECHB_BLOCK'] = 'Заблокировать';
?>
