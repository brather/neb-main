<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

$libraryId = 0;
if(isset($_REQUEST['library'])) {
    $libraryId = $_REQUEST['library'];
}
if(UGROUP_LIB_CODE_ADMIN === nebUser::getCurrent()->getRole()) {
    $user = nebUser::getCurrent()->getUser();
    if(isset($user['UF_LIBRARY'])) {
        $libraryId = $user['UF_LIBRARY'];
    }
}
Loc::loadMessages(__FILE__);
$APPLICATION->IncludeComponent(
    'neb:digitization.plan',
    '',
    [
        'DATE_FROM'      => $_REQUEST['dateFrom'],
        'DATE_TO'        => $_REQUEST['dateTo'],
        'LIBRARY_ID'     => $libraryId,
        'REQUEST_STATUS' => $_REQUEST['request_status'],
        'EXPORT_URL'     => '/profile/digitization-export/'
    ],
    $component
);
