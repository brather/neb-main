$(function(){

	if ( $('[name="UF_PLACE_REGISTR"]').attr('checked') == 'checked' ) {
        // $('[name="UF_PLACE_REGISTR"]').
    }

    function getAge(dateString) {
        var today = new Date();
        var d = parseInt(dateString[0] + dateString[1]),
            m = parseInt(dateString[3] + dateString[4]),
            y = parseInt(dateString[6] + dateString[7] + dateString[8] + dateString[9]),
            born = new Date(y, m - 1, d),
            age = Math.ceil( (today.getTime() - born) / (24 * 3600 * 365.25 * 1000) );
        return age;
    }
    jQuery.validator.addMethod("checkAge", function(value, element, params) {
            return ( getAge(value) > Number(params) );
        }
    );

    $('form[name="readerselfupdatetofullaccess"]').validate({

        rules: rulesAndMessages.rules,
        messages: rulesAndMessages.messages,
        ignore: '[type="hidden"]:not(.tovalidate)',
        errorPlacement: function(error, element) {
        	var el = $(element);
            if ( element.attr('type') == "radio" ) {
                error.appendTo( $(element).closest('.form-group').find('label')[0] );
            } else if ( element.attr('type') == "hidden" ) {
                error.appendTo( $(element).closest('.setscan') );
                $('body').scrollTo('.setscan');
            } else {
                error.appendTo( el.closest('.form-group') );
            }
            $(element).closest('.form-group').toggleClass('has-error', true);
        },
        // specifying a submitHandler prevents the default submit, good for the demo
        // submitHandler: function() {
        //     alert("valid!");
        // },
        // set this class to error-labels to indicate valid fields
        success: function(label) {
            // set &nbsp; as text for IE
            //label.html('"&nbsp;"').addClass("resolved");
            label.remove();
        },
        highlight: function(element, errorClass, validClass) {        
            $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
            $(element).parent().find("." + errorClass).removeClass("resolved");
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
        }
    });

    $('#userbirthday').on('input change', function(){
        $('form[name="readerselfupdatetofullaccess"]').validate().element(this);
    });

    if ( $('#livwhereregistered').prop('checked') ) {
        $('.living-address-group input:not(:hidden)').attr('readonly','true');
    }

    // $('[data-mirror]').each(function(){
    //     var id = $(this).data('mirror'),
    //         thisId = $(this).prop('id'),
    //         setMirror = $('#livwhereregistered').prop('checked');
    //     $('#'+id).attr('data-mirrored-by', thisId);
    //     if (setMirror) {
    //         $(this).val( ''+ $('#'+id).val() );
    //     }
    // });

    $(document).on('keyup', '[data-mirrored-by]', function(){
        var id = $(this).data('mirrored-by'),
            sameAddresses = $('#livwhereregistered').prop('checked');
        if (sameAddresses) {
            $('#'+id).val( $(this).val() ).trigger('change');
        }
    });

    $(document).on('click', '#livwhereregistered', function(){
        // console.log('checkbox click');
    });
    $(document).on('change', '#livwhereregistered', function(){
        var checked = $(this).prop('checked');
        if ( checked ) {
            $('[data-mirror]').each(function(){
                var id = $(this).data('mirror');
                $(this).val( $('#'+id).val() );
            });
            $('.living-address-group').addClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function(){
                $(this).attr('readonly','true');
            });
        } else {
            $('.living-address-group').removeClass('undisplay-validation').find('input[type="text"],input[type="number"]').each(function(){
                $(this).removeAttr('readonly');
            });
        }            
    });


    $(document).on('click','[data-perpage-set]', function(e){
        e.preventDefault();
        var toggler = $(this),
            value = toggler.data('perpage-set'),
            widget = toggler.closest('[data-perpage-widget]'),
            input = widget.find('[name="UF_SEARCH_PAGE_COUNT"]');
        
        widget.find('[data-perpage-set]').toggleClass('current', false);
        toggler.toggleClass('current',true);
        $(input).val(value);
    });

    $(document).on('click','#cancelbutton', function(e){
    	e.preventDefault();
    	window.location.href = '/profile/';
    });

	var regForm = $('form[name="readerselfupdatetofullaccess"]');
    /* пользовательское соглашение */
    $(document).on('click', '[data-agree-dialog]', function(e){
        e.preventDefault();
        $('#universal-modal').modal();
    }).on('change', '#agreecheckbox', function(){
        var val = $(this).prop('checked');
        $('[name="termsagreed"]', regForm).val( val ).trigger('change');
        $.cookie('termsagreed', val, {expires: 7, path: '/'});
    }).on('change', '[name="termsagreed"]', function(){
        if ( JSON.parse( $('[name="termsagreed"]').val() ) ) {
            $('[type="submit"]', regForm).removeAttr('disabled');
        } else {
            $('[type="submit"]', regForm).attr('disabled','disabled');
        }
    });
    $('#universal-modal').on('show.bs.modal', function(){
        var modal = $(this);
        modal.find('.modal-dialog').toggleClass('modal-lg', true).toggleClass('fixed-modal', true);
        modal.find('.modal-title').text('Пользовательское соглашение');
        var opts = {
              lines: 17 // The number of lines to draw
            , length: 18 // The length of each line
            , width: 7 // The line thickness
            , radius: 19 // The radius of the inner circle
            , scale: 0.55 // Scales overall size of the spinner
            , corners: 0 // Corner roundness (0..1)
            , color: '#00708c' // #rgb or #rrggbb or array of colors
            , opacity: 0 // Opacity of the lines
            , rotate: 0 // The rotation offset
            , direction: 1 // 1: clockwise, -1: counterclockwise
            , speed: 1 // Rounds per second
            , trail: 99 // Afterglow percentage
            , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            , zIndex: 1 // The z-index (defaults to 2000000000)
            , className: 'spinner' // The CSS class to assign to the spinner
            , top: '50%' // Top position relative to parent
            , left: '50%' // Left position relative to parent
            , shadow: false // Whether to render a shadow
            , hwaccel: false // Whether to use hardware acceleration
            , position: 'absolute' // Element positioning
            };
        var target = modal.find('.modal-body')[0];
        var spinner = new Spinner(opts).spin(target);

        if ( $.cookie('termsagreed') == 'true' ) {
            $('[name="termsagreed"]').val('true');
        }

        var checkbox = $('<input>')
            .attr('type','checkbox')
            .prop('id','agreecheckbox')
            .prop('checked', JSON.parse( $('[name="termsagreed"]', regForm).val() ) );

        if ( $(checkbox).prop('checked') === false && $('[name="termsreaded"').val() == 'false' ) {
            $(checkbox).attr('disabled','disabled');
        }
        var checkboxlabel = $('<span>')
            .attr('class','lbl')
            .text('Я согласен с правилами');
        var checkboxcontrol = $('<label>')
            .attr('class','agree-control')
            .append(checkbox)
            .append(checkboxlabel);
        modal.find('.modal-body').load('/user-agreement/agree-text.php', function(){
            modal.find('.modal-footer').prepend(checkboxcontrol);
            spinner.stop;
        }).on('scroll', function(){
            var docHeight = $(this).find('.agree-list').height(),
                viewportHeight = $(this).height(),
                scrolled = $(this)[0].scrollTop;

            if ( scrolled > (docHeight - viewportHeight - 200) && $(checkbox).attr('disabled') == 'disabled' ) {
                $('[name="termsreaded"').val('true');
                $(checkbox).removeAttr('disabled');
            }
        });
    }).on('hide.bs.modal', function(){
        $(this)
            .find('.modal-dialog').toggleClass('modal-lg', false).toggleClass('fixed-modal', false).end()
            .find('.modal-title').text('').end()
            .find('.modal-body').text('').off('scroll').end()
            .find('.agree-control').remove();
    });
    /*./пользовательское соглашение*/



});