<?
namespace Intervolga\RemoteUserClient;

use Bitrix\Main\SystemException;
use Intervolga\RemoteUserClient\Tools\Request;

class File
{
	/**
	 * Uploads file to remote user server.
	 *
	 * @param string $sFileCreatePath file extension
	 * @param string $sContentSrc absolute file path
	 * @param bool $bAddToFile flag for adding content to file, not replacing
	 *
	 * @return array file array with "src", "height", "width" keys
	 * @throws SystemException
	 */
	public static function upload($sFileCreatePath, $sContentSrc, $bAddToFile)
	{
		if (file_exists($sContentSrc) && is_uploaded_file($sContentSrc))
		{
			$obRequest = new Request("wsUploadFile", array(
				"path" => $sFileCreatePath,
				"content" => base64_encode(file_get_contents($sContentSrc)),
				"append" => $bAddToFile ? 1 : 0,
			));

			$obResponse = $obRequest->run();
			if ($obResponse && $obResponse->value())
			{
				$arValue = $obResponse->value();
				if ($arValue["file"])
				{
					return unserialize($arValue["file"]);
				}
			}
		}
		else
		{
			throw new SystemException("Failed to open input stream.", 101);
		}
		throw new SystemException("Unknown upload error.");
	}

	/**
	 * Changes size of image on remote site.
	 *
	 * @param string $sImage relative image path and name
	 * @param int $iWidth resize width
	 * @param int $iHeight resize height
	 * @param bool $bExactSize flag for exact size
	 * @param int $iQuality image quality
	 * @param string $sSavePath path to save resized image
	 *
	 * @return array file array with "src", "height", "width" keys
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function resize($sImage, $iWidth, $iHeight, $bExactSize = TRUE, $iQuality = 99, $sSavePath = '/upload/resize_cache_imm/')
	{
		$obRequest = new Request("wsResizeFile", array(
			"image" => $sImage,
			"width" => $iWidth,
			"height" => $iHeight,
			"exact" => $bExactSize ? 1 : 0,
			"quality" => $iQuality,
			"path" => $sSavePath,
		));

		$obResponse = $obRequest->run();
		if ($obResponse && $obResponse->value())
		{
			$arValue = $obResponse->value();
			if ($arValue["file"])
			{
				return unserialize($arValue["file"]);
			}
		}
		throw new SystemException("Unknown resize error.");
	}

	/**
	 * Checks if remote file exists.
	 *
	 * @param string $sFile
	 *
	 * @return bool
	 */
	public static function exists($sFile)
	{
		return (strlen(file_get_contents($sFile, null, null, 0, 1)) > 0);
	}
}