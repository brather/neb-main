<?php

$MESS['EXTENDED_SEARCH_ALL_SECTION_SEARCH']           = 'Раздел для поиска';
$MESS['EXTENDED_SEARCH_ALL_SECTION_LIBS']             = 'Библиотеки';
$MESS['EXTENDED_SEARCH_ALL_SECTION_MUSEUMS']          = 'Музеи';
$MESS['EXTENDED_SEARCH_ALL_SECTION_ARCHIVES']         = 'Архивы';

$MESS['EXTENDED_SEARCH_ALL_SEARCH_TYPE']              = 'Вид поиска';
$MESS['EXTENDED_SEARCH_ALL_SEARCH_SIMPLE']            = 'Одной строкой';
$MESS['EXTENDED_SEARCH_ALL_SEARCH_EXENDED']           = 'Расширенный поиск';

$MESS['EXTENDED_SEARCH_ALL_FULLQUERY']                = 'Введите запрос';
$MESS['EXTENDED_SEARCH_ALL_AUTHOR']                   = 'Автор';
$MESS['EXTENDED_SEARCH_ALL_AUTHOR_PH_LIBRARY']        = 'Пушкин';
$MESS['EXTENDED_SEARCH_ALL_AUTHOR_PH_MUSEUM']         = 'Дмитриев Николай';
$MESS['EXTENDED_SEARCH_ALL_TITLE']                    = 'Название';
$MESS['EXTENDED_SEARCH_ALL_TITLE_PH_LIBRARY']         = 'Сказка о царе Салтане';
$MESS['EXTENDED_SEARCH_ALL_TITLE_PH_MUSEUM']          = 'Портрет императора Александра III';
$MESS['EXTENDED_SEARCH_ALL_TITLE_PH_ARCHIVE']         = 'Газета Возрождение';
$MESS['EXTENDED_SEARCH_ALL_TEXT_PH_LIBRARY']          = 'Три девицы под окном';

$MESS['EXTENDED_SEARCH_ALL_DATE']                     = 'Год издания';
$MESS['EXTENDED_SEARCH_ALL_LANGUAGE']                 = 'Язык издания';

$MESS['EXTENDED_SEARCH_ALL_DECLARANT']                = 'Заявитель';
$MESS['EXTENDED_SEARCH_ALL_DECLARANT_PH_LIBRARY']     = 'Центральное конструкторское бюро';
$MESS['EXTENDED_SEARCH_ALL_PATENTHPLDER']             = 'Патентообладатель';
$MESS['EXTENDED_SEARCH_ALL_PATENTHPLDER_PH_LIBRARY']  = '';

$MESS['EXTENDED_SEARCH_LIB_ACCESS']                   = 'Доступ к изданиям';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_ALL_NAME']          = 'Все';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_ALL_SCREEN']        = 'Доступ ко всем изданиям';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_OPEN_NAME']         = 'Открытые';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_OPEN_SCREEN']       = 'Доступ к открытым изданиям';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_CLOSED_NAME']       = 'Охраняемые авторским правом';
$MESS['EXTENDED_SEARCH_LIB_ACCESS_CLOSED_SCREEN']     = 'Доступ к изданиям, охраняемым авторским правом';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA']              = 'Область поиска';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA_EBOOK_NAME']   = 'Издания с электронной копией';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA_EBOOK_SCREEN'] = 'Издания с электронной копией';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA_BOOK_NAME']    = 'Каталог печатных изданий';
$MESS['EXTENDED_SEARCH_LIB_SEARCH_AREA_BOOK_SCREEN']  = 'Каталог печатных изданий';

$MESS['EXTENDED_SEARCH_COLLECTIONS']                  = 'Коллекции';
$MESS['EXTENDED_SEARCH_CHOISE_COLLECTION']            = 'Выберите коллекции';

$MESS['EXTENDED_SEARCH_LIB_TEXT']                     = 'Текст';
$MESS['EXTENDED_SEARCH_LIB_PUBLISHING']               = 'Издательство';
$MESS['EXTENDED_SEARCH_LIB_PUBLISHING_PH']            = 'Астрель';
$MESS['EXTENDED_SEARCH_LIB_PUBPLACE']                 = 'Место издания';
$MESS['EXTENDED_SEARCH_LIB_PUBPLACE_PH']              = 'Москва';
$MESS['EXTENDED_SEARCH_LIB_LIBRARY']                  = 'Библиотека';
$MESS['EXTENDED_SEARCH_LIB_LIBRARY_PH']               = 'РГБ';
$MESS['EXTENDED_SEARCH_LIB_ISBN']                     = 'ISBN';
$MESS['EXTENDED_SEARCH_LIB_ISBN_PH']                  = '978-5-17-086006-7';
$MESS['EXTENDED_SEARCH_LIB_BBK']                      = 'Индекс ББК';
$MESS['EXTENDED_SEARCH_LIB_BBK_PH']                   = '84(2Рос=Рус)5-5 Ш9(2)6=Р50-2';
$MESS['EXTENDED_SEARCH_SECTION_BBK']                  = 'Раздел ББК';
$MESS['EXTENDED_SEARCH_SECTION_BBK_PH']               = 'Художественная литература';
$MESS['EXTENDED_SEARCH_LIB_UD']                       = 'УДК';
$MESS['EXTENDED_SEARCH_LIB_UD_PH']                    = '821.161.1-31-053.2';
$MESS['EXTENDED_SEARCH_BBK_SECTIONS']                 = 'Рубрики';

$MESS['EXTENDED_SEARCH_MUSEUM_MUSEUM']                = 'Музей';
$MESS['EXTENDED_SEARCH_MUSEUM_MUSEUM_PH']             = 'Государственный исторический музей';
$MESS['EXTENDED_SEARCH_MUSEUM_MATERIALS']             = 'Материал, техника исполнения';
$MESS['EXTENDED_SEARCH_MUSEUM_MATERIALS_PH']          = 'Холста, масло';
$MESS['EXTENDED_SEARCH_MUSEUM_INVERNUMBER']           = 'Инвентарный номер';
$MESS['EXTENDED_SEARCH_MUSEUM_INVERNUMBER_PH']        = 'И I 5964';

$MESS['EXTENDED_SEARCH_ARCHIVE_ARCHIVE']              = 'Архив';
$MESS['EXTENDED_SEARCH_ARCHIVE_ARCHIVE_PH']           = 'Государственный архив Российской Федерации (ГАРФ)';
$MESS['EXTENDED_SEARCH_ARCHIVE_FOUND']                = 'Фонд';
$MESS['EXTENDED_SEARCH_NUMBER_FOUND']                 = 'номер фонда';
$MESS['EXTENDED_SEARCH_ARCHIVE_NAME']                 = 'название';
$MESS['EXTENDED_SEARCH_ARCHIVE_NUMBER']               = 'номер';
$MESS['EXTENDED_SEARCH_ARCHIVE_FOUNDNUMBER_PH']       = 'Р5850';
$MESS['EXTENDED_SEARCH_ARCHIVE_NAME_PH']              = 'Редакция газеты Возрождение';
$MESS['EXTENDED_SEARCH_ARCHIVE_INVENTORY']         	  = 'Опись';
$MESS['EXTENDED_SEARCH_ARCHIVE_CASE']           	  = 'Дело';
$MESS['EXTENDED_SEARCH_ARCHIVE_INVNAME_PH']           = 'Опись дел личного происхождения';
$MESS['EXTENDED_SEARCH_ARCHIVE_UNITNAME_PH']          = 'Газета возрождение';

$MESS['EXTENDED_SEARCH_DEBUG_MODE']                   = 'Режим отладки';