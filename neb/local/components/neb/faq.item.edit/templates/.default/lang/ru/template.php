<?php
$MESS["NAME_ITEM"] = "Вопрос*: ";
$MESS["INPUT_NAME_ITEM"] = "вопрос...";
$MESS["ADD_ITEM"] = "Добавить";
$MESS["UPDATE_ITEM"] = "Сохранить";
$MESS["DELETE_ITEM"] = "Удалить вопрос: ";
$MESS["BACK_TO_LIST"] = "К списку вопросов";
$MESS["DETAIL_TEXT_ITEM"] = "Детальное описание ответа на вопрос*: ";
$MESS["CHOISE_SECTION_FOR_ITEM"] = "Выберите раздел в который поместите вопрос*: ";
$MESS["ACTIVE_ITEM"] = "Вопрос активен";
$MESS["FAQ_QUESTION_DELETE_ITEM"] = "Удалить вопрос?";
$MESS["SORT_ITEM"] = "Индекс сортировки: ";
$MESS["SORT_ITEM_DEFAULT"] = "500";
$MESS["ITEM_EDIT_PAGE"] = "Редактирование вопроса";
$MESS['REQUIRED_FIELD'] = "* - Обязательно к заполнению";