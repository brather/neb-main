<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 14:46
 */

namespace AssessorBundle\Controller\Api;


use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\AbstractType;

interface RatingApiInterface
{
    CONST ENTITY_QUERY_FREQUENCY = 'query_frequency';
    CONST ENTITY_AUTHOR = 'author_rating';
    CONST ENTITY_EDITIONS = 'edition_rating';
    CONST ENTITY_SUBSTITUTION = 'substitution';
    const DEFAULT_ROUTE = 'assessor_queries';

    /**
     * @return string
     */
    public function getEntityName();

    /**
     * @return AbstractType
     */
    public function getFormType();

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    public function getEntityManager();

    /**
     * @param QueryBuilder $qb
     *
     * @return void
     */
    public function handleAutoComplete($qb);

}