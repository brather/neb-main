<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arMenu = array();
foreach($arResult['URL_TEMPLATES'] as $page => $url){
    if ($page == 'list')
        continue;
    $arMenu[$page] = $arResult['FOLDER'] . str_replace('#CODE#', $arResult['VARIABLES']['CODE'], $url);
}

?>
<div class="lk-sidebar__menu">
    <div class="lk-sidebar__menu-adaptive">
	    <a class='lk-sidebar__menu-link <?=$this->__page == 'detail' ? 'lk-sidebar__menu-link--active' : ''?>' href="<?=$arMenu['detail']?>" ><?=Loc::getMessage('LIBRARY_MENU_ABOUT')?></a>
	    <? if(nebLibrary::showPublication($arResult['VARIABLES']['CODE']) === true): ?>
		    <a class='lk-sidebar__menu-link <?=$this->__page == 'funds' ? 'lk-sidebar__menu-link--active' : ''?>' href="<?=$arMenu['funds']?>" id="menu_funds"><?=Loc::getMessage('LIBRARY_MENU_FUNDS')?></a>
		<? endif; ?>
		<? if(nebLibrary::showCollection($arResult['VARIABLES']['CODE']) === true): ?>
		    <a class='lk-sidebar__menu-link <?=$this->__page == 'collections' ? 'lk-sidebar__menu-link--active' : ''?>' href="<?=$arMenu['collections']?>" id="menu_collections" ><?=Loc::getMessage('LIBRARY_MENU_COLLECTIONS')?></a>
		<? endif; ?>
        <? if(nebLibrary::showNews($arResult['VARIABLES']['ID']) === true): ?>
            <a class='lk-sidebar__menu-link <?=$this->__page == 'news' ? 'lk-sidebar__menu-link--active' : ''?>' href="<?=$arMenu['news']?>"><?=Loc::getMessage('LIBRARY_MENU_NEWS')?></a>
        <? endif; ?>
        <a class='lk-sidebar__menu-link <?=$this->__page == 'popular' ? 'lk-sidebar__menu-link--active' : ''?>' href="<?=$arMenu['popular']?>"><?=Loc::getMessage('LIBRARY_MENU_POPULAR')?></a>
    </div>
</div>
