<?php
/**
 * User: agolodkov
 * Date: 10.10.2016
 * Time: 18:18
 */

namespace AssessorBundle\Traits;


trait AssessorEntityTrait
{
    /**
     * @var string
     */
    public $updateUrl;

    /**
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->updateUrl;
    }

    /**
     * @param string $updateUrl
     */
    public function setUpdateUrl($updateUrl)
    {
        $this->updateUrl = $updateUrl;
    }
}