<?	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("nota.exalead");
use Nota\Exalead\BooksAdd;
use Nota\Exalead\BiblioCardTable;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$BOOK_ID = isset($_REQUEST['BOOK_ID']) ? (int)$_REQUEST['BOOK_ID'] : false;
if($BOOK_ID){
    $book = BiblioCardTable::getRow(Array('filter' => Array('Id' => $BOOK_ID)));
}
?>
<div class="b-addbook" id="drop_upload">
    <?if($BOOK_ID):?>
		<h2><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_EDIT')?></h2>
    <?else:?>
	    <h2><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_NEW')?></h2>
    <?endif;?>
	<form action="" class="b-form b-form_common b-addbookform" method="post">
        <?if($BOOK_ID):?>
		    <input type="hidden" name="edit_book" value="Y"/>
		    <input type="hidden" name="ID" value="<?=$BOOK_ID?>"/>
        <?else:?>
            <input type="hidden" name="add_book" value="Y"/>
        <?endif;?>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">			
				<label for="settings02"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_FULL_TITLE')?></label>
				<div class="field validate">
					<input type="text" data-required="required" value="<?=$book['Name']?>" id="settings02" data-maxlength="255" data-minlength="2" name="NAME" class="input" >
					<em class="error required"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_REQ_FIELD')?></em>								
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50">			
				<label for="settings03"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_AUTHOR')?></label>
				<div class="field validate">
					<input type="text"  data-validate="fio" data-required="required" value="<?=$book['Author']?>" id="settings03" data-maxlength="60" data-minlength="2" name="AUTHOR" class="input w50p" >
					<em class="error required"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_REQ_FIELD')?></em>		
					<em class="error validate"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_INCORRECT_FIELD')?></em>						
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50">			
				<label for="settings04"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_CODE_BBK')?></label>
				<div class="field validate">
					<input type="text"  data-validate="number" data-required="required" value="<?=$book['BBKText']?>" id="settings04" data-maxlength="30" data-minlength="2" name="BBK" class="input w50p" > <span class="example"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_BBK_EXAMPLE')?> </span>
					<em class="error required"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_REQ_FIELD')?></em>		
					<em class="error validate"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_INCORRECT_FIELD')?></em>						
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w20pc">			
				<label for="settings05"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_PUB_YEAR')?></label>
				<div class="field validate">
					<input type="text"  data-validate="number" data-required="required" value="<?=$book['PublishYear']?>" id="settings5" data-maxlength="40" data-minlength="4" name="PUBLISH_YEAR" class="input b-yeartb" >
					<em class="error required"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_REQ_FIELD')?></em>		
					<em class="error validate"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_INCORRECT_FIELD')?></em>						
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock b-pdfadd">
				<?$APPLICATION->IncludeComponent(
					"notaext:plupload", 
					"add_books", 
					array(
						"MAX_FILE_SIZE" => "512",
						"FILE_TYPES" => "pdf",
						"DIR" => "tmp_books_pdf",
						"FILES_FIELD_NAME" => "books_pdf",
						"MULTI_SELECTION" => "N",
						"CLEANUP_DIR" => "Y",
						"UPLOAD_AUTO_START" => "Y",
						"RESIZE_IMAGES" => "N",
						"UNIQUE_NAMES" => "Y",
                        "ALREADY_UPLOADED_FILE" => $BOOK_ID ? $book['pdfLink'] : false
					),
					false
				);?>

				<div class="right">
					<p><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_DATE_ADD')?> </p>
					<div class="b-fieldeditable">
                        <?if($BOOK_ID):?>
                            <div class="b-fondtitle"><span class="b-fieldtext"><?=$book['BX_TIMESTAMP']->format('d.m.Y')?></span>
                                <!--<a class="b-fieldeedit" href="#"></a>-->
                            </div>
                            <div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" value="<?=$book['BX_TIMESTAMP']->format('d.m.Y')?>" class="txt txt_size2" name="DATE_ADD"></span>
                                <!--<a class="b-editablsubmit iblock" href="#"></a>-->
                            </div>
                        <?else:?>
                            <div class="b-fondtitle"><span class="b-fieldtext"><?=date('d.m.Y')?></span>
                            <!--<a class="b-fieldeedit" href="#"></a>-->
                            </div>
                            <div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" value="<?=date('d.m.Y')?>" class="txt txt_size2" name="DATE_ADD"></span>
                            <!--<a class="b-editablsubmit iblock" href="#"></a>-->
                            </div>
                        <?endif;?>
					</div>
				</div>


			</div>
		</div>
		<div class="fieldrow nowrap fieldrowaction">
			<div class="fieldcell ">
				<div class="field clearfix">
					<a href="#" class="formbutton gray right btrefuse"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_REFUSE')?></a>
                    <?if($BOOK_ID):?>
                        <button class="formbutton left" value="1" type="submit"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_UPDATE')?></button>
                    <?else:?>
					    <button class="formbutton left" value="1" type="submit"><?=GetMessage('RIGHTHOLDER_BOOKS_ADD_POST')?></button>
                    <?endif;?>
				</div>
			</div>
		</div>
	</form>
</div>
