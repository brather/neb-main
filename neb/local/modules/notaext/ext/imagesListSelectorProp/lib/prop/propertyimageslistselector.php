<?
namespace Bitrix\NotaExt\Ext\imagesListSelectorProp\Prop;

/*
Свойство инфоблока для вывода поля с выбором цвета
*/
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class PropertyImagesListSelector extends \CUserTypeEnum
{
    public function GetUserTypeDescription()
    {
        return array(
            'PROPERTY_TYPE' => 'L',
            'USER_TYPE' => 'prop_images_list_selector',
            'DESCRIPTION' => '[NotaExt]ImagesListSelector',
            "CLASS_NAME" => __CLASS__,
            "BASE_TYPE" => "enum",
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetPropertyFieldHtmlMulty' => array(__CLASS__, 'GetPropertyFieldHtml'),
            //'GetSettingsHTML' 		=> array(__CLASS__, 'GetSettingsHTML'),
            //'PrepareSettings' 		=> array(__CLASS__, 'PrepareSettings'),
            //'GetAdminListViewHTML' 	=> array(__CLASS__, 'GetAdminListViewHTML'),
            //'GetAdminFilterHTML' 	=> array(__CLASS__, 'GetAdminFilterHTML'),
        );
    }

    public function GetPropertyFieldHtml($property_fields, $value, $strHTMLControlNames)
    {
        return self::custom_ShowListPropertyField("PROP[{$property_fields['ID']}]", $property_fields, $value);
    }

    private function custom_ShowListPropertyField($name, $property_fields, $values, $bInitDef = false, $def_text = false)
    {
        if (!is_array($values)) $values = array();
        foreach ($values as $key => $value) {
            if (is_array($value) && array_key_exists("VALUE", $value))
                $values[$key] = $value["VALUE"];
        }

        $id = $property_fields["ID"];
        $multiple = $property_fields["MULTIPLE"];
        $res = "";
        if ($property_fields["LIST_TYPE"] == "C") //list property as checkboxes
        {
            $cnt = 0;
            $wSel = false;
            $prop_enums = \CIBlockProperty::GetPropertyEnum($id);
            while ($ar_enum = $prop_enums->Fetch()) {
                $cnt++;
                if ($bInitDef)
                    $sel = ($ar_enum["DEF"] == "Y");
                else
                    $sel = in_array($ar_enum["ID"], $values);
                if ($sel)
                    $wSel = true;

                // Подготовим $arEnum['VALUE'], преобразовав изображения
                preg_match_all("!\[img:(.*?)\]!is", $ar_enum["VALUE"], $matches);
                // Приводим к удобному виду
                $arEnumValueFormatted = htmlspecialcharsex($ar_enum["VALUE"]);
                if (count($matches[0])) {
                    foreach ($matches[0] as $key => $match) {
                        $match = htmlspecialchars($match);
                        $imgString = \CFile::ShowImage($matches[1][$key], 100, 100, null, "", true);
                        $arEnumValueFormatted = str_replace($match, $imgString, $arEnumValueFormatted);
                    }
                }

                $uniq = md5(uniqid(rand(), true));
                if ($multiple == "Y") //multiple
                    $res .= '<input type="checkbox" name="' . $name . '[]" value="' . htmlspecialcharsbx($ar_enum["ID"]) . '"' . ($sel ? " checked" : "") . ' id="' . $uniq . '"><label for="' . $uniq . '">' . $arEnumValueFormatted . '</label><br>';
                else //if(MULTIPLE=="Y")
                    $res .= '<input type="radio" name="' . $name . '[]" id="' . $uniq . '" value="' . htmlspecialcharsbx($ar_enum["ID"]) . '"' . ($sel ? " checked" : "") . '><label for="' . $uniq . '">' . $arEnumValueFormatted . '</label><br>';

                if ($cnt == 1)
                    $res_tmp = '<input type="checkbox" name="' . $name . '[]" value="' . htmlspecialcharsbx($ar_enum["ID"]) . '"' . ($sel ? " checked" : "") . ' id="' . $uniq . '"><br>';
            }


            $uniq = md5(uniqid(rand(), true));

            if ($cnt == 1)
                $res = $res_tmp;
            elseif ($multiple != "Y")
                $res = '<input type="radio" name="' . $name . '[]" value=""' . (!$wSel ? " checked" : "") . ' id="' . $uniq . '"><label for="' . $uniq . '">' . htmlspecialcharsex(($def_text ? $def_text : GetMessage("IBLOCK_AT_PROP_NO"))) . '</label><br>' . $res;

            if ($multiple == "Y" || $cnt == 1)
                $res = '<input type="hidden" name="' . $name . '" value="">' . $res;
        } else //list property as list
        {
            $bNoValue = true;
            $prop_enums = \CIBlockProperty::GetPropertyEnum($id);
            while ($ar_enum = $prop_enums->Fetch()) {
                if ($bInitDef)
                    $sel = ($ar_enum["DEF"] == "Y");
                else
                    $sel = in_array($ar_enum["ID"], $values);
                if ($sel)
                    $bNoValue = false;
                $res .= '<option value="' . htmlspecialcharsbx($ar_enum["ID"]) . '"' . ($sel ? " selected" : "") . '>' . htmlspecialcharsex($ar_enum["VALUE"]) . '</option>';
            }

            if ($property_fields["MULTIPLE"] == "Y" && IntVal($property_fields["ROW_COUNT"]) < 2)
                $property_fields["ROW_COUNT"] = 5;
            if ($property_fields["MULTIPLE"] == "Y")
                $property_fields["ROW_COUNT"]++;
            $res = '<select name="' . $name . '[]" size="' . $property_fields["ROW_COUNT"] . '" ' . ($property_fields["MULTIPLE"] == "Y" ? "multiple" : "") . '>' .
                '<option value=""' . ($bNoValue ? ' selected' : '') . '>' . htmlspecialcharsex(($def_text ? $def_text : GetMessage("IBLOCK_AT_PROP_NA"))) . '</option>' .
                $res .
                '</select>';
        }
        return $res;
    }
}