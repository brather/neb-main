<?

/**
 * Class NebLang
 * Определение\установка ткущего языка сайта
 */
class NebLang
{
    public static function get()
    {
        $lang = $_COOKIE['NEB_LANG'];

        // опрделеяем язык по AcceptLanguage установленный в браузере
        if (empty($lang) && !empty($_SERVER["HTTP_ACCEPT_LANGUAGE"])) {
            $arUserLang = explode(",", $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
            $sUserLang  = strtolower(substr($arUserLang[0], 0, 2));
            if (!empty($sUserLang)) {
                if ('ru' == $sUserLang) {
                    $lang = 'ru';
                } else {
                    $lang = 'en';
                }
            }
        }
        if (empty($lang)) {
            $lang = 'ru';
        }

        return $lang;
    }

    public static function set($lang)
    {
        $lang = $lang == 'en' ? 'en' : 'ru';
        setcookie("NEB_LANG", $lang, time() + 3600 * 24 * 90);
        $_COOKIE['NEB_LANG'] = $lang;
        self::setLang($lang);
    }

    public static function setLang($lang)
    {
        define('LANGUAGE_ID', $lang);
        $site = $lang == 'en' ? 's2' : 's1';
        if (!defined('ADMIN_SECTION') || true !== ADMIN_SECTION) {
            define('SITE_ID', $site);
        }
    }
}

if (stripos($_SERVER{"REQUEST_URI"}, "/special/") === 0) {
    if (NebLang::get() != "ru") {
        NebLang::set("ru");
    }
}

if (!empty($_REQUEST['setlang'])) {
    NebLang::set($_REQUEST['setlang']);
}

if (!defined('LANGUAGE_ID')) {
    NebLang::setLang(NebLang::get());
}