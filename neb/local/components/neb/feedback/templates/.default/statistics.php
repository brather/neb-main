<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->SetTitle("Статистика обратной связи");
/**
 * Вывод всех заявок
 */

?>
<p>
	<a href='<?=$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['statistics_total']?>'>Статистика по количеству</a>
	|
	<a href='<?=$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['statistics_time']?>'>Статистика по времени исполнения</a>
</p>