<?php
$MESS['REG_RGB_OK_MESSAGE']
    = 'Specified in the questionnaire data is successfully verified.<br/>';
$MESS['REG_RGB_OK_LOGIN'] = 'Your Login';
$MESS['REG_RGB_OK_CARD_NUMBER'] = 'Your Card Number';
$MESS['REG_RGB_OK_LINK_LK'] = 'Private office';
$MESS['REG_RGB_OK_LINK_SEARCH'] = 'Skip to search';
$MESS['REG_RGB_OK_PRINT_AGREE'] = 'Print an agreement with the NEB';
?>