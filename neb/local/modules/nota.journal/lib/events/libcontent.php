<?php

namespace Nota\Journal\Events;

use Nota\Journal\J;
use Bitrix\Main\Type;

class Libcontent
{
    function onAdd(&$arFields){
        if($arFields['IBLOCK_CODE'] == IBLOCK_CODE_LIBRARY)
            J::add('libcontent', 'add', Array('IBLOCK_ID' => $arFields['ID']));
    }

    function onEdit(&$arFields){
        if($arFields['IBLOCK_CODE'] == IBLOCK_CODE_LIBRARY)
            J::add('libcontent', 'edit', Array('IBLOCK_ID' => $arFields['ID']));
    }

    function onDelete($arFields){
        if($arFields['IBLOCK_CODE'] == IBLOCK_CODE_LIBRARY)
            J::add('libcontent', 'delete', Array('IBLOCK_ID' => $arFields['ID']));
    }
}