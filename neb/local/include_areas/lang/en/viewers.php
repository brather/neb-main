<?php
//---------
$MESS['APPLICATIONS_DESC'] = 'To work on mobile devices, setup one of our applications.'; //Для работы в НЭБ на мобильных устройствах установите одно из наших приложений.
$MESS['APPLICATIONS_WIN'] = 'Windows application';
$MESS['APPLICATIONS_MAC'] = 'Mac OS application';
$MESS['APPLICATIONS_LINUX'] = 'Linux application';
$MESS['APPLICATIONS_ANDROID'] = 'Android application';
$MESS['APPLICATIONS_IOS'] = 'iOS application';
$MESS['APPLICATIONS_WINPHONE'] = 'Windows Phone application';
