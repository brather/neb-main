<?php

namespace AssessorBundle\Entity;

use AssessorBundle\Traits\AssessorEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as Serialization;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TblBookRaiting
 *
 * @ORM\Table(name="tbl_book_raiting", indexes={@ORM\Index(name="UK_tbl_author_raiting_Author", columns={"Author"}), @ORM\Index(name="UK_tbl_author_raiting_Title", columns={"Title"})})
 * @ORM\Entity(repositoryClass="AssessorBundle\Entity\Repository\CommonRepository")
 * @UniqueEntity(fields={"author", "title"}, message="Такие автор и название уже существуют")
 */
class TblBookRating implements AssessorRatingEntityInterface
{
    use AssessorEntityTrait;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serialization\Groups({"list", "details"})
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Author", type="string", length=330, nullable=true)
     * @Serialization\Groups({"list", "details"})
     */
    public $author;

    /**
     * @var string
     *
     * @ORM\Column(name="Title", type="string", length=300, nullable=false)
     * @Serialization\Groups({"list", "details"})
     * @Assert\NotBlank()
     */
    public $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="Raiting", type="integer", nullable=false)
     * @Serialization\Groups({"list", "details"})
     * @Assert\NotBlank()
     */
    public $rating;


    /**
     * @var string
     *
     * @ORM\Column(name="Keywords", type="string")
     * @Serialization\Groups({"list", "details"})
     */
    public $keywords;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }
}
