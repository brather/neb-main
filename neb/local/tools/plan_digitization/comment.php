<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Loader,
    \Bitrix\Main\Application,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

Loader::includeModule('highloadblock');

$obUser = new nebUser();

$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();

$iPlanId  = intval($arRequest['plan_id']);
$sBookId  = trim($arRequest['book_id']);
$sComment = trim($arRequest['comment']);

$arResult = [];

try {

    if ($iPlanId <= 0)
        throw new Exception('Параметр plan_id задан некорректно!');

    if (empty($sBookId))
        throw new Exception('Параметр book_id не может быть пустым!');

    if (empty($sComment))
        throw new Exception('Параметр comment не может быть пустым!');

    if (!$obUser->IsAuthorized())
        throw new Exception('Вы не авторизованы!');

    $connection = Application::getConnection();

    // Вернем id блока с планами оцифровки
    $sql = "SELECT ID FROM b_hlblock_entity WHERE NAME='" . HIBLOCK_CODE_DIGITIZATION_PLAN . "';";
    $record = $connection->query($sql)->fetch();
    $iDigitizationBlock = intval($record['ID']);

    // Получим классы HL-блока
    $hlblock = HighloadBlockTable::getById($iDigitizationBlock)->fetch();
    $entity_data_class = HighloadBlockTable::compileEntity($hlblock)->getDataClass();

    $arLibrary = $obUser->getLibrary();
    $arData = $entity_data_class::getList(array(
        'select' => array('ID'),
        'filter' => array(
            'UF_LIBRARY' => $arLibrary['ID'],
            'UF_EXALEAD_BOOK' => $sBookId,
            'UF_PLAN_ID' => $iPlanId
        )
    ))->fetch();

    if (!empty($arData)) {
        $result = $entity_data_class::update($arData['ID'], ['UF_COMMENT' => $sComment]);
        if (!$result->isSuccess()){
            throw new Exception(implode(', ', $result->getErrorMessages()));
        } else {
            $arResult['result'] = true;
        }
    } else {
        throw new Exception('Данная книга не найдена в данном плане!');
    }

} catch (Exception $e) {
    $arResult['error'] = true;
    $arResult['message'] = [$e->getMessage()];
}

MainHelper::showJson($arResult);