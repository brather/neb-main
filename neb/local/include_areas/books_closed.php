<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init();

?>
<script type="text/javascript">
	$(function() {

		$('.popup_autoreg_form .closed-link').click(function(){
			$('.closepopup').click();
		});

		var sendForm = false;

		$('form.b-formautoreg').submit(function( event ) {

			var form = this;
			if(sendForm == true)
				return false;

			$('em.error', form).addClass('hidden');

			if($('.autoreglogin', form).val().length <=0 || $('.autoregpass', form).val().length <=0)
				return false;

			sendForm = true;				

			BX.showWait();

			$.ajax({
				type: "POST",
				url: form.action,
				data: $(form).serialize(),
				cache:false,
				timeout:120000,
				dataType: 'json'
			})
			.done(function( resp ) {
				BX.closeWait();
				sendForm = false;
                if(resp.redirect){
                    window.location.replace(resp.redirect);
                    return false;
                }
				if(resp.error == true)
				{
                    var errorClass = 'wrong-password-error';
                    if(resp.result.hasOwnProperty('code')
                        && 'access_deny' == resp.result.code) {
                        errorClass = 'access-deny-error';
                        var registerButton = $('.popup_autoreg_form a.register-button');
                        var rbHref = registerButton.attr('href');
                        registerButton.attr('href', registerButton.data('href-toggle'));
                        registerButton.data('href-toggle', rbHref);
                    }
					$('.error.'+errorClass, form).removeClass('hidden');
				}
				else
				{
					$(form).addClass('hidden');
					$('.popup_autoreg_form .ok.tcenter').removeClass('hidden');

					$('.closed_book').each(function() {
						$( this ).removeClass( "closed_book" );
						var aLink = $( this ).find('a');
						if(aLink.length <= 0)
							aLink = this;
						$(aLink).attr('href', $(aLink).data('link'));

					});


				}				
			});
			return false;
		});
	});
</script>

<!--только с мобильных устройств.-->
<div class="b-message hidden">
    <a href="#" class="closepopup">Закрыть окно</a>
    <p>Уважаемый читатель. В связи с изменением регламента доступа к изданиям, защищенным авторским правом, их просмотр возможен только со стационарных компьютеров до момента выхода новой версии НЭБ для мобильных устройств. Приносим Вам свои извинения.</p>
</div>
<!--только с мобильных устройств.-->

<!--popup доступ к закрытому изданию-->
<div class="popup_autoreg_form hidden">
	<a href="#" class="closepopup">Закрыть окно</a>
	<div class="ok tcenter hidden">
		<p><?=GetMessage('BOOKS_CLOSED_SUCCESS_AUTH')?></p>
		<p><a class="closed-link" href="#" target="_blank"><button type="submit" value="1" class="formbutton"><?=GetMessage('BOOKS_CLOSED_READ')?></button></a></p>
	</div>

	<form action="/local/tools/viewer/auth_user.php" method="post" class="b-form b-formautoreg">
		<?=bitrix_sessid_post()?>
		<?/*?><p>Доступ к закрытому изданию возможен только для читателей РГБ. Если Вы являетесь читателем РГБ, то введите номер читательского билета и пароль РГБ. Иначе пройдите полную регистрацию.</p><?*/?>
		<p><?=GetMessage('BOOKS_CLOSED_AUTHORIZATION_REQUIRED');?></p>
        <p class="get-viewer red"><?=GetMessage('WARNING_PROGRAM_VIEWING');?>
            <a target="_blank" href="/viewers/">
                <?=GetMessage('WARNING_PROGRAM_VIEWING2');?></a>
        </p>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings11">E-mail</label>
				<div class="field validate">
					<input type="text"  value="" id="settings11"  name="login" class="input autoreglogin" data-minlength="2" data-maxlength="30" data-required="required" autocomplete="off">
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings10"><?=GetMessage('BOOKS_CLOSED_FORM_PASS');?></label>
				<div class="field validate">
					<input type="password"  value="" id="settings10" name="password" class="input autoregpass" data-minlength="2" data-maxlength="30" data-required="required" autocomplete="off">
				</div>
			</div>
			<a href="/registration/" target="_blank" data-href-toggle="/profile/update_select/" class="register-button formbutton iblock"><?=GetMessage('BOOKS_CLOSED_FORM_REG');?></a>
		</div>
		<em class="error hidden wrong-password-error"><?=GetMessage('BOOKS_CLOSED_WRONG_PASS');?></em>
		<em class="error hidden access-deny-error"><?=GetMessage('BOOKS_CLOSED_ACCESS_DENY');?></em>
		<div class="fieldrow nowrap fieldrowaction">
			<div class="fieldcell ">
				<div class="field clearfix">
					<button class="formbutton left" value="1" type="submit"><?=GetMessage('BOOKS_CLOSED_FORM_ENTER');?></button>
				</div>
			</div>
		</div>
	</form>
</div><!-- /.popup_autoreg_form -->
<!--/popup доступ к закрытому изданию-->

<!--popup нет прав на чтение книги-->
<div class="popup_book_access_deny hidden">
    <a href="#" class="closepopup">Закрыть окно</a>
    <div class="ok tcenter">
        <br>
        <p><?=GetMessage('BOOKS_CLOSED_ACCESS_DENY')?></p>
        <? if ('user' === nebUser::getCurrent()->getRole()) { ?>
            <p><a href="/profile/update_select/" target="_blank">
                    <button type="submit" value="1"
                            class="formbutton"><?= GetMessage(
                            'BOOKS_CLOSED_GET_FULL_ACCESS'
                        ) ?></button>
                </a></p>
        <? } ?>
    </div>
</div><!-- /.popup_autoreg_form -->
<!--/popup нет прав на чтение книги-->


<!--предупреждение о необходимости доп программы.-->
<div class="b-message_warning hidden">
	<a href="#" class="closepopup">Закрыть окно</a>
	<p>Программа, нужная для просмотра, не установлена.</p>
	<p>Скачайте её здесь:
		<?
			$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
			if(strpos($userAgent, 'windows') !== false)
			{
			?>
			<br/>Для Windows: <a target="_blank" href="http://нэб.рф/distribs/viewer_windows.rar">http://нэб.рф/distribs/viewer_windows.rar</a>
			<?
			}
			elseif(strpos($userAgent, 'android') !== false)
			{
			?>
			<br/>Для Android: <a target="_blank" href="http://нэб.рф/distribs/viewer_Android.rar">http://нэб.рф/distribs/viewer_Android.rar</a>
			<?
			}
			elseif(strpos($userAgent, 'mac') !== false)
			{
			?>
			<br/>Для IOS: <a target="_blank" href="http://нэб.рф/distribs/viewer_IOS.rar">http://нэб.рф/distribs/viewer_IOS.rar</a>
			<?
			}
			else
			{
			?>
			<br/>Для Windows: <a target="_blank" href="http://нэб.рф/distribs/viewer_windows.rar">http://нэб.рф/distribs/viewer_windows.rar</a>
			<br/>Для IOS: <a target="_blank" href="http://нэб.рф/distribs/viewer_IOS.rar">http://нэб.рф/distribs/viewer_IOS.rar</a>
			<br/>Для Android: <a target="_blank" href="http://нэб.рф/distribs/viewer_Android.rar">http://нэб.рф/distribs/viewer_Android.rar</a>
			<?
			}
		?>
	</p>
</div>
<!--/предупреждение о необходимости доп программы.-->
