<?
$pagename='';
?>
<? include("header_main.php"); ?>

<section class="mainsection" >			
	<div class="b-portalinfo clearfix wrapper">
		<div class="b-portalinfo_num right">
			<span class="b-portalinfo_lb">Доступно</span>
			<span class="b-portalinfo_numbers"><span class="b-portalinfo_number iblock">1</span><span class="b-portalinfo_number iblock">3</span><span class="b-portalinfo_number mr  iblock">1</span><span class="b-portalinfo_number iblock">7</span><span class="b-portalinfo_number iblock">5</span><span class="b-portalinfo_number mr  iblock">4</span></span>
			<span class="b-portalinfo_lb">электронных книг&nbsp;</span>
		</div>
		<div class="b-portalinfo_num right">
			<span class="b-portalinfo_lb">Доступно</span>
			<span class="b-portalinfo_numbers"><span class="b-portalinfo_number iblock">2</span><span class="b-portalinfo_number mr  iblock">5</span><span class="b-portalinfo_number iblock">7</span><span class="b-portalinfo_number iblock">8</span><span class="b-portalinfo_number mr  iblock">4</span><span class="b-portalinfo_number iblock">8</span><span class="b-portalinfo_number iblock">0</span><span class="b-portalinfo_number mr  iblock">2</span></span>
			<span class="b-portalinfo_lb">записей каталогов</span>
		</div>
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin:15px 1px -24px 64px; margin-left:0px; font-size:14px;">Отсканированные книги российских библиотек	</span>
				<!--<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		-->


	</div><!-- /.b-portalinfo -->


	<form action="" class="searchform">
		<div class="b-search wrapper">
			<div class="b-search_field">
				<div class="clearfix">
					<input type="submit" class="b-search_bth bbox" value="Найти">
					<span class="b-search_fieldbox">
						<input type="text" name="" class=" bbox b-search_fieldtb b-text" id="asearch" placeholder="Пример: биография Гагарина" autocomplete="off" data-src="search.php?session=MY_SESSION&moreparams=MORE_PARAMS">
						<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
					</span>
					<!--<select name="" id="" class="select js_select">
						<option value="1">по всем полям</option>
						<option value="2">название</option>
						<option value="3">автор</option>
						<option value="4">ISBN</option>
						<option value="5">по тексту</option>
						<option value="6">место издательства</option>
					</select>-->
					
				</div>
				<div class="b_search_set clearfix">
					<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>
					<div class="checkwrapper">
						<label for="cb3" class="black">Искать только в отсканированных изданиях</label><input class="checkbox js_btnsub" type="checkbox" name="" id="cb3"><br/>
						<label for="cb4" class="black">Искать по точной фразе</label><input class="checkbox js_btnsub" type="checkbox" name="" id="cb4">

					</div>

				</div> <!-- /.b_search_set -->
			</div>
		</div> <!-- /.b-search -->
		<div class="b-search_ex">
			<div class="wrapper rel bbox">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
						<option value="">по месту издательства</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<a href="#" class="b-searchdelrow b-searchaddrow"><span class="b-searchaddrow_minus">-</span><span class="b-searchaddrow_lb js_delsearchrow">удалить условие</span></a>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option data-asrc="authors.php?session=MY_SESSION&moreparams=MORE_PARAMS" value="">по названию</option>
						<option data-asrc="search.php?session=MY_SESSION&moreparams=MORE_PARAMS" value="">по месту издательства</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="" >
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<input class="b-text" type="text" id="js_searchdate_prev" value="1700"  />
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="b-text"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right fln">						
						
						<span class="nowrap"><input class="checkbox" type="checkbox" name="" id="cb41"><label for="cb41" class="black">Искать по точной фразе</label></span>
						<span class="nowrap"><input class="checkbox" type="checkbox" name="" id="cb40"><label for="cb40" class="black">Искать по полному тексту издания</label></span>
						<span class="nowrap"><input class="checkbox" type="checkbox" name="" id="cb39"><label for="cb39" class="black">искать в авторефератах и диссертациях</label> </span>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="b-bookboard b-bookboardlg bbox" id="collections">

	<div class="b-bookboard_main clearfix rel">
		<div class="wrapper b-bookboard_main_tit"><a href="#" class="b-collectlink">коллекции</a></div>

		<div class="b-boardslider narrow_panel js_slider_single_nodots">
			<div>			
				<div class="js_flexbackground">
					<img src="./pic/slide/img_1.jpg" alt="" data-bgposition="50% 0" class="js_flex_bgimage" />
					<div class="wrapperboard bbox">
						<h3>100 рекомендованных книг для школьников
						от национальной электронной библиотеки</h3>
						<div class="b-bookboardmain iblock">
							<a href="#"><img src="./pic/pic_15.jpg" class="b-bookboard_img loadingimg " alt=""></a>
							<div class="b-bookhover bbox">
									<div class="meta">
											<div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
											<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a>
										</div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
						</div>
						<div class="b-bookboard_cl iblock">
							<ul class="b-bookboard_list">
								<li>
									<a href="#"><img src="./pic/pic_16.jpg" class="loadingimg" alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta">
											<div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
											<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a>
										</div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_17.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_18.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_19.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_22.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_23.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_20.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
											<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
											<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_21.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку"  class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
							</ul>
							<!--<a href="#" class="b-bookboard_more button_mode">посмотреть все</a>-->
						</div>
					</div>
				</div><!-- /.wrapper -->

			</div>
			<div>			
				<div class="js_flexbackground">
					<img src="./pic/slide/img_1.jpg" alt="" data-bgposition="50% 0" class="js_flex_bgimage" />
					<div class="wrapperboard bbox">
						<h3>500 рекомендованных книг для школьников
					от национальной электронной библиотеки</h3>
						<div class="b-bookboardmain iblock"><a href="#"><img src="./pic/pic_15.jpg" class="b-bookboard_img loadingimg " alt=""></a></div>
						<div class="b-bookboard_cl iblock">
							<ul class="b-bookboard_list">
								<li>
									<a href="#"><img src="./pic/pic_16.jpg" class="loadingimg" alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_17.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_18.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_19.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_22.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_23.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_20.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
								<li>
									<a href="#"><img src="./pic/pic_21.jpg" class="loadingimg"  alt=""></a>
									<div class="b-bookhover bbox">
										<div class="meta"><div data-minus="<span>Удалить</span> из Моей библиотеки" data-plus="Добавить в Мою библиотеку" class="b-hint rel">Добавить в Мою библиотеку</div>
										<a href="#" data-collection="ajax_favs.html" class="b-bookadd"></a></div>
										<div class="b-bookhover_tit black">Дневник кости рябцева</div>
										<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="ajax_bookview.php">Н. Огнев</a></span>
									</div>
								</li>
							</ul>
							<!--<a href="#" class="b-bookboard_more button_mode">посмотреть все</a>-->
						</div>
					</div>
				</div><!-- /.wrapper -->

			</div>
		</div> <!-- /.boardslider -->



	</div><!-- /.b-bookboard_main -->
</section>
<section class="b-bookinfo">
	<div class="wrapper">
		<div class="b-bookinfo_wrapper js_bookinfo_height">
			<div class="b-bookinfo_new iblock">
				<div class="b-bookinfo_tit">
				<h2>новые поступления</h2> <a href="#" class="b-searchpage_lnk">Май: добавлено <strong>1405 книги</strong> </a>
				</div>
				<ul class="b-bookboard_list">
						<li>
								<div class="b_popular_descr">
									<h4 class="iblock"><a href="#">Сумма технологии</a></h4>
									<span class="b-autor iblock">Автор: <a href="#" class="lite">Станислав Лем</a></span>
									<div class="b-result_sorce_info iblock"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>

								</div>
						</li>
						<li>
								<div class="b_popular_descr">
									<h4 class="iblock"><a href="#">Гроздья гнева</a></h4>
									<span class="b-autor iblock">Автор: <a href="#" class="lite">Джон Стейнбек</a></span>
									<div class="b-result_sorce_info iblock"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>
								</div>
						</li>
						<li>
								<div class="b_popular_descr">
									<h4 class="iblock"><a href="#">Сумма технологии</a></h4>
									<span class="b-autor iblock">Автор: <a href="#" class="lite">Станислав Лем</a></span>
									<div class="b-result_sorce_info iblock"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>

								</div>
						</li>
						
					</ul>
					<a class="button_mode js_ajax_morebook" href="ajax_fake_books.html">следующие 3</a>
				</div> <!-- /.b-bookinfo_new -->

				<div class="b-bookinfo_popular iblock">
					<div class="b-bookinfo_tit"><h2>популярные издания</h2></div>
					<ul class="b_bookpopular">
						<li>
							<div class="b_popular_descr">
								<h4><a href="#">Сумма технологии</a></h4>
								<span class="b-autor">Автор: <a href="#" class="lite">Станислав Лем</a></span>
								<div class="b-result_sorce_info"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>

							</div>
						</li>
						<li>
							<div class="b_popular_descr">
								<h4><a href="#">Гроздья гнева</a></h4>
								<span class="b-autor">Автор: <a href="#" class="lite">Джон Стейнбек</a></span>
								<div class="b-result_sorce_info"><em>Источник:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>
							</div>
						</li>
					</ul> <!-- /.b_bookpopular -->
					<a href="ajax_fake_popular.html" class="button_mode js_ajax_bookupload">обновить</a>
				</div> <!-- /.b-bookinfo_popular -->
			</div> <!-- /.b-bookinfo_wrapper -->
			<h2 class="b-mainslider_tit">Преимущества НЭБ</h2>
			<div class="b-mainslider js_slider_single">
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock"><a href="#"><img class="loadingimg" src="./pic/pic_14.jpg" alt=""></a></div>
						<div class="b-mainslider_descr iblock">
							<h3><a href="#">Единая система поиска по полным текстам всех фондов</a></h3>
							<p>Вводите любой текст в поисковую строку и получает наиболее релевантные выдачи по полнотекстовому индексу (не только совпадения слов, но и семантика). </p>
						</div>
					</div>
				</div>
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock"><a href="#"><img class="loadingimg" src="./pic/pic_14.jpg" alt=""></a></div>
						<div class="b-mainslider_descr iblock">
							<h3><a href="#">Слайд 2</a></h3>
							<p>Вводите любой текст в поисковую строку и получает наиболее релевантные выдачи по полнотекстовому индексу (не только совпадения слов, но и семантика). </p>
						</div>
					</div>
				</div>
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock"><a href="#"><img class="loadingimg"  src="./pic/pic_14.jpg" alt=""></a></div>
						<div class="b-mainslider_descr iblock">
							<h3><a href="#">Слайд 3</a></h3>
							<p>Вводите любой текст в поисковую строку и получает наиболее релевантные выдачи по полнотекстовому индексу (не только совпадения слов, но и семантика). </p>
						</div>
					</div>
				</div>
			</div> <!-- /.b-mainslider -->
		</div><!-- /.wrapper -->
	</section>
</div><!-- /.homepage -->
<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu">мои подборки</a>
	

	<form class="b-selectionadd" action="">
		<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
		<span class="b-selectionaddtxt">Cоздать подборку</span>
		<input type="text" class="input hidden">
	</form>
</div><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
<!--popup Удалить из подборки--><div class="b-removepopup hidden"><p>Удалить книгу из Моей библиотеки?</p><a href="#" class="formbutton btremove">Удалить</a><a href="#" class="formbutton gray">Оставить</a></div><!--/popup Удалить из подборки-->
<!--popup доступ к закрытому изданию-->
<div class="popup_autoreg_form hidden">
	<a href="#" class="closepopup">Закрыть</a>
	
	<div class="ok tcenter" style="display:none;">
		<p>Вы успешно зарегистрированы как читатель РГБ</p>
		<p><button type="submit" value="1" class="formbutton">Читать</button></p>
	</div>
	
	<form action="" class="b-form b-formautoreg">
	<p>Доступ к закрытому изданию возможен только для читателей РГБ. Если Вы являетесь читателем РГБ, то введите логин и пароль РГБ. Иначе пройдите полную регистрацию.</p>
									<div class="fieldrow nowrap">
										<div class="fieldcell iblock">
											<label for="settings11">Логин</label>
											<div class="field validate">
												<input type="text"  value="" id="settings11"  name="login" class="input">										
											</div>
										</div>
											<a href="#" class="formbutton iblock">Помощь</a>	

									</div>
									<div class="fieldrow nowrap">

										<div class="fieldcell iblock">
											<label for="settings10">Пароль</label>
											<div class="field validate">
												<input type="password"  value="" id="settings10" name="pass" class="input">									
											</div>
										</div>
											<a href="#" class="formbutton iblock">Зарегистрироваться</a>	
										
									</div>
									<em class="error">Неверный логин или пароль</em>
									<div class="fieldrow nowrap fieldrowaction">
										<div class="fieldcell ">
											<div class="field clearfix">
												<button class="formbutton left" value="1" type="submit">Войти</button>
											</div>
										</div>
									</div>
</form>
</div><!-- /.popup_autoreg_form -->

</body>
</html>