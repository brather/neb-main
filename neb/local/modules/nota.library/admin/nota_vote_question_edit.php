<?
/*
##############################################
# Bitrix: SiteManager						#
# Copyright (c) 2004 - 2009 Bitrix			#
# http://www.bitrix.ru						#
# mailto:admin@bitrix.ru					#
##############################################
*/
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vote/prolog.php");

$NUser = new nebUser();
$UserRole = $NUser->isLibrary();
$arLibrary = $NUser->getLibrary();

if ($UserRole === false or empty($arLibrary))
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$VOTE_RIGHT = nebLibrary::getVoteRight();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/vote/include.php");

ClearVars();

IncludeModuleLangFile(__FILE__);
$err_mess = "File: " . __FILE__ . "<br>Line: ";
define("HELP_FILE", "vote_list.php");
$old_module_version = CVote::IsOldVersion();

$aTabs = array(
    array("DIV" => "edit2", "TAB" => GetMessage("VOTE_QUESTION"), "ICON" => "vote_question_edit", "TITLE" => GetMessage("VOTE_QUESTION_TEXT")),
    array("DIV" => "edit3", "TAB" => GetMessage("VOTE_ANSWERS"), "ICON" => "vote_question_edit", "TITLE" => GetMessage("VOTE_ANSWER_LIST")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$message = null;
$arSort = array(0);

$ID = intval($ID);
$arQuestion = array();
$arAnswers = array();
$arAnswersFields = array();


if ($ID > 0) {
    $db_res = CVoteQuestion::GetByID($ID);
    if (!($db_res && $arQuestion = $db_res->Fetch())) {
        $ID = 0;
    } else {
        $ii = 1;
        $VOTE_ID = intVal($arQuestion["VOTE_ID"]);
        $db_res = CVoteAnswer::GetList($ID);
        if ($db_res && $res = $db_res->Fetch()) {
            do {
                $arAnswers[$ii] = $res;
                $ii++;
            } while ($res = $db_res->Fetch());
        }
    }
}

if ($ID <= 0) {
    $arQuestion = array(
        "ACTIVE" => "Y",
        "VOTE_ID" => $VOTE_ID,
        "C_SORT" => CVoteQuestion::GetNextSort($VOTE_ID),
        "QUESTION" => "",
        "QUESTION_TYPE" => "html",
        "IMAGE_ID" => "",
        "DIAGRAM" => "Y",
        "REQUIRED" => "N",
        "DIAGRAM_TYPE" => VOTE_DEFAULT_DIAGRAM_TYPE,
        "TEMPLATE" => "default.php",
        "TEMPLATE_NEW" => "default.php");
}

$VOTE_ID = intVal($VOTE_ID);
$arVote = array();
$db_res = CVote::GetByID($VOTE_ID);
if (!($db_res && $arVote = $db_res->Fetch())) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    echo "<a href='nota_vote_list.php?lang=" . LANGUAGE_ID . "' class='navchain'>" . GetMessage("VOTE_VOTE_LIST") . "</a>";
    echo ShowError(GetMessage("VOTE_NOT_FOUND"));
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
    die();
}
$arVote["TITLE"] = (strlen($arVote["TITLE"]) > 0 ? $arVote["TITLE"] : TruncateText(
    ($arVote["DESCRIPTION_TYPE"] == "html" ? strip_tags($arVote["DESCRIPTION"]) : $arVote["DESCRIPTION"]), 200));

$db_res = CVoteChannel::GetByID($arVote["CHANNEL_ID"]);
$arChannel = $db_res->Fetch();

if ($arChannel['SYMBOLIC_NAME'] != 'LIBRARY_' . $arLibrary['ID'])
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$adminChain->AddItem(array(
    "TEXT" => htmlspecialcharsbx($arVote["TITLE"]),
    "LINK" => "nota_vote_edit.php?ID=$arVote[ID]&lang=" . LANGUAGE_ID));

$sDocTitle = ($ID > 0 ? str_replace("#ID#", $ID, GetMessage("VOTE_EDIT_RECORD")) : GetMessage("VOTE_NEW_RECORD"));
$APPLICATION->SetTitle($sDocTitle);

/********************************************************************
 * ACTIONS
 ********************************************************************/
if (!($_SERVER["REQUEST_METHOD"] == "POST" && (strlen($save) > 0 || strlen($apply) > 0))) {
} elseif ($VOTE_RIGHT < "W") { /* bad rights */
} elseif (!check_bitrix_sessid()) { /* bad sessid */
} else {
    $bVarsFromForm = false;
    $_FILES["IMAGE_ID"] = (is_array($_FILES["IMAGE_ID"]) ? $_FILES["IMAGE_ID"] : array());
    $arFields = array(
        "ACTIVE" => (isset($_REQUEST["ACTIVE"]) ? $_REQUEST["ACTIVE"] : 'N'),
        "VOTE_ID" => $VOTE_ID,
        "C_SORT" => $_REQUEST["C_SORT"],
        "QUESTION" => $_REQUEST["QUESTION"],
        "QUESTION_TYPE" => $_REQUEST["QUESTION_TYPE"],
        "IMAGE_ID" => ($_FILES["IMAGE_ID"] + ($_REQUEST["IMAGE_ID_del"] == "Y" ? array("del" => "Y") : array())),
        "DIAGRAM" => (isset($_REQUEST["DIAGRAM"]) ? $_REQUEST["DIAGRAM"] : 'N'),
        "REQUIRED" => (isset($_REQUEST["REQUIRED"]) ? $_REQUEST["REQUIRED"] : 'N'),
        "DIAGRAM_TYPE" => $_REQUEST["DIAGRAM_TYPE"],
        "TEMPLATE" => $_REQUEST["TEMPLATE"],
        "TEMPLATE_NEW" => $_REQUEST["TEMPLATE_NEW"]);
    foreach ($_REQUEST["ANSWER"] as $pid) {
        if (intval($pid) <= 0)
            continue;
        $arAnswer = array(
            "ID" => intVal($_REQUEST["ANSWER_ID_" . $pid]),
            "QUESTION_ID" => $ID,
            "ACTIVE" => ($_REQUEST["ACTIVE_" . $pid] == 'Y' ? 'Y' : 'N'),
            "C_SORT" => $_REQUEST["C_SORT_" . $pid],
            "MESSAGE" => ($_REQUEST["MESSAGE_" . $pid] != ' ') ? trim($_REQUEST["MESSAGE_" . $pid]) : ' ',
            "FIELD_TYPE" => $_REQUEST["FIELD_TYPE_" . $pid],
            "FIELD_WIDTH" => intVal($_REQUEST["FIELD_WIDTH_" . $pid]),
            "FIELD_HEIGHT" => intVal($_REQUEST["FIELD_HEIGHT_" . $pid]),
            "FIELD_PARAM" => trim($_REQUEST["FIELD_PARAM_" . $pid]),
            "COLOR" => trim($_REQUEST["COLOR_" . $pid]));
        $arAnswersFields[$pid] = $arAnswer;
        if ($arAnswer["ID"] <= 0 && empty($arAnswer["MESSAGE"])):
            unset($arAnswersFields[$pid]);
        endif;
    }

    if ($ID > 0):
        $result = CVoteQuestion::Update($ID, $arFields);
    else:
        $result = $ID = CVoteQuestion::Add($arFields);
    endif;

    $aMsg = array();
    if (!$result)
        $bVarsFromForm = true;
    else {
        foreach ($arAnswersFields as $pid => $arAnswer) {
            $bResult = true;
            $APPLICATION->ResetException();
            if ($_REQUEST["del_" . $pid] == "Y"):
                if ($arAnswer["ID"] > 0):
                    CVoteAnswer::Delete($arAnswer["ID"]);
                endif;
                unset($arAnswersFields[$pid]);
            elseif ($arAnswer["ID"] > 0):
                $bResult = CVoteAnswer::Update($arAnswer["ID"], $arAnswer);
            else:
                $arAnswer["QUESTION_ID"] = $ID;
                $bResult = CVoteAnswer::Add($arAnswer);
                if ($bResult):
                    $arAnswersFields[$pid]["ID"] = $bResult;
                endif;
            endif;
            if (!$bResult):
                $e = $APPLICATION->GetException();
                $aMsg[] = array(
                    "id" => "ANSWER_ID_" . $pid,
                    "text" => ($e ? $e->Getstring() : "Error"));
            endif;
            $bVarsFromForm = ($bVarsFromForm ? $bVarsFromForm : !$bResult);
        }
    }
    if (!$bVarsFromForm):
        if (strlen($save) > 0):
            LocalRedirect("nota_vote_question_list.php?lang=" . LANGUAGE_ID . "&VOTE_ID=" . $VOTE_ID);
        endif;
        LocalRedirect("nota_vote_question_edit.php?lang=" . LANGUAGE_ID . "&ID=$ID&VOTE_ID=" . $VOTE_ID . "&" . $tabControl->ActiveTabParam());
    elseif (!empty($aMsg)):
        $e = new CAdminException($aMsg);
    else:
        $e = $APPLICATION->GetException();
    endif;
    $message = new CAdminMessage(GetMessage("VOTE_GOT_ERROR"), $e);
    $arFields["IMAGE_ID"] = (intVal($arQuestion["IMAGE_ID"]) > 0 ? $arQuestion["IMAGE_ID"] : "");
    $arQuestion = $arFields;
    $arAnswers = $arAnswersFields;
}
/********************************************************************
 * /ACTIONS
 ********************************************************************/

/********************************************************************
 * Data
 ********************************************************************/
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

if ($message):
    echo $message->Show();
endif;

$aMenu = array(
    array(
        "TEXT" => GetMessage("VOTE_LIST"),
        "TITLE" => GetMessage("VOTE_QUESTIONS_LIST"),
        "LINK" => "/bitrix/admin/nota_vote_question_list.php?lang=" . LANGUAGE_ID . "&VOTE_ID=" . $VOTE_ID,
        "ICON" => "btn_list"));

if ($VOTE_RIGHT >= "W" && $ID > 0) {
    $aMenu[] = array(
        "TEXT" => GetMessage("VOTE_CREATE"),
        "TITLE" => GetMessage("VOTE_CREATE_NEW_RECORD"),
        "LINK" => "/bitrix/admin/nota_vote_question_edit.php?VOTE_ID=$VOTE_ID&lang=" . LANGUAGE_ID,
        "ICON" => "btn_new");

    $aMenu[] = array(
        "TEXT" => GetMessage("VOTE_DELETE"),
        "TITLE" => GetMessage("VOTE_DELETE_RECORD"),
        "LINK" => "javascript:if(confirm('" . GetMessage("VOTE_DELETE_RECORD_CONFIRM") . "')) window.location='/bitrix/admin/nota_vote_question_list.php?action=delete&ID=$ID&VOTE_ID=$VOTE_ID&" . bitrix_sessid_get() . "&lang=" . LANGUAGE_ID . "';",
        "ICON" => "btn_delete");
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

$z = CVoteAnswer::GetList($ID);

?>
<form name="form1" method="POST" action="" enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="ID" value="<?= $ID ?>"/>
    <input type="hidden" name="VOTE_ID" value="<?= $VOTE_ID ?>"/>
    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
    <input type="hidden" value="Y" name="DIAGRAM">

    <?
    $tabControl->Begin();
    ?>

    <?
    /************** General Tab ****************************************/
    $tabControl->BeginNextTab();
    ?>

    <tr>
        <td><?= GetMessage("VOTE_VOTE") ?></td>
        <td>[<a href="nota_vote_edit.php?ID=<?= $arVote["ID"] ?>&lang=<?= LANGUAGE_ID ?>"
                title="<?= GetMessage("VOTE_CONF") ?>"><?= $arVote["ID"] ?></a>]&nbsp;
            <?= htmlspecialcharsbx($arVote["TITLE"]) ?></td>
    </tr>
    <? if (strlen($arQuestion["TIMESTAMP_X"]) > 0): ?>
        <tr>
            <td><?= GetMessage("VOTE_TIMESTAMP") ?></td>
            <td><?= $arQuestion["TIMESTAMP_X"] ?></td>
        </tr>
        <tr>
            <td><?= GetMessage("VOTE_COUNTER_QUESTION") ?></td>
            <td><?= $arQuestion["COUNTER"] ?></td>
        </tr>
    <? endif; ?>
    <tr>
        <td width="40%"><?= GetMessage("VOTE_ACTIVE") ?></td>
        <td width="60%"><?= InputType("checkbox", "ACTIVE", "Y", $arQuestion["ACTIVE"], false) ?></td>
    </tr>
    <tr>
        <td><?= GetMessage("VOTE_C_SORT") ?></td>
        <td><input type="text" id="C_SORT" name="C_SORT" size="5" maxlength="18" value="<?= $arQuestion["C_SORT"] ?>"/>
        </td>
    </tr>
    <tr>
        <td><?= GetMessage("VOTE_REQUIRED") ?></td>
        <td><input type="checkbox" name="REQUIRED" id="REQUIRED" value="Y" onclick="OnDiagramFlagChange()" <?
            ?> <?= ($arQuestion["REQUIRED"] == "Y" ? "checked='checked'" : "") ?> /></td>
    </tr>

    <? if (COption::GetOptionString("vote", "VOTE_COMPATIBLE_OLD_TEMPLATE", "Y") == "Y"): ?>
        <? if ($old_module_version == "Y"): ?>
            <tr>
                <td><?= GetMessage("VOTE_TEMPLATE") ?></td>
                <td><? echo SelectBoxFromArray("TEMPLATE", GetTemplateList("RQ"), $arQuestion["TEMPLATE"], " ");
                    ?></td>
            </tr>
            <?
        else:
            $arr = CMainAdmin::GetTemplateList(COption::GetOptionString("vote", "VOTE_TEMPLATE_PATH_QUESTION_NEW"));
            $arrTemplates = array("reference" => $arr, "reference_id" => $arr);
            ?>
            <tr>
                <td><?= GetMessage("VOTE_TEMPLATE") ?></td>
                <td><? echo SelectBoxFromArray("TEMPLATE_NEW", $arrTemplates, $arQuestion["TEMPLATE_NEW"], " ");
                    ?></td>
            </tr>
        <? endif; ?>
    <? endif ?>

    <tr class="heading" id="tr_QUESTION_LABEL">
        <td colspan="2"><?= GetMessage("VOTE_QUESTION_TEXT") ?></td>
    </tr>
    <?
    if (COption::GetOptionString("vote", "USE_HTML_EDIT") == "Y" && CModule::IncludeModule("fileman")):?>
        <tr>
            <td align="center" colspan="2"><?
                CFileMan::AddHTMLEditorFrame("QUESTION", $arQuestion["QUESTION"], "QUESTION_TYPE", $arQuestion["QUESTION_TYPE"], array('height' => '200', 'width' => '100%'));
                ?></td>
        </tr>
    <? else:?>
        <tr>
            <td align="center"
                colspan="2"><?= InputType("radio", "QUESTION_TYPE", "text", $arQuestion["QUESTION_TYPE"], false) ?>Text
                &nbsp;/&nbsp;<?= InputType("radio", "QUESTION_TYPE", "html", $arQuestion["QUESTION_TYPE"], false) ?>HTML
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2"><textarea name="QUESTION" style="width:100%"
                                                     rows="23"><?= $arQuestion["QUESTION"] ?></textarea></td>
        </tr>
    <? endif; ?>



    <?
    /************** Answers Tab ****************************************/
    $tabControl->BeginNextTab();
    ?>
    <tr class="adm-detail-required-field">
        <td colspan="2">
            <?
            #pre(GetAnswerTypeList(),1);
            $arrAnswerTypeList = array(
                "reference_id" => array(0, 1),
                "reference" => array("radio", "checkbox")
            );
            ?>
            <table border="0" cellspacing="0" cellpadding="0" width="100%" class="internal" id='answerlist'>
                <tr class="heading">
                    <td>ID</td>
                    <td nowrap width="95%"><?= GetMessage("VOTE_MESSAGE") ?><span class="required"><sup>1</sup></span>
                    </td>
                    <td><?= GetMessage("VOTE_FIELD_TYPE") ?></td>
                    <td><?= GetMessage("VOTE_SORT") ?></td>
                    <td><?= GetMessage("VOTE_ACT") ?></td>
                    <td><?= GetMessage("VOTE_DEL") ?></td>
                </tr>
                <?

                $arSort = array(0);
                foreach ($arAnswers as $i => $arAnswer) {
                    $arSort[] = intval($arAnswer["C_SORT"]);
                    ?>
                    <tr>
                        <td>
                            <input type="hidden" name="ANSWER[]" value="<?= $i ?>"/>
                            <input type="hidden" name="ANSWER_ID_<?= $i ?>" value="<?= intVal($arAnswer["ID"]) ?>"/>
                            <?= (intVal($arAnswer["ID"]) > 0 ? $arAnswer["ID"] : "") ?></td>
                        <td><input type="text" name="MESSAGE_<?= $i ?>"
                                   value="<?= htmlspecialcharsbx($arAnswer["MESSAGE"]) ?>" style="width:100%;"/></td>
                        <td><?= SelectBoxFromArray("FIELD_TYPE_" . $i, $arrAnswerTypeList, $arAnswer["FIELD_TYPE"], "", "OnChange=\"FIELD_TYPE_CHANGE(" . $i . ")\" class='typeselect'") ?></td>
                        <td><input type="text" name="C_SORT_<?= $i ?>"
                                   value="<?= htmlspecialcharsbx($arAnswer["C_SORT"]) ?>" size="3"/></td>
                        <td><?= InputType("checkbox", "ACTIVE_" . $i, "Y", $arAnswer["ACTIVE"], false); ?></td>
                        <td><input type="checkbox" name="del_<?= $i ?>" value="Y"/></td>
                    </tr>
                    <?
                }
                $i = 0;
                if (!empty($arAnswers)):
                    $i = max(array_keys($arAnswers));
                endif;
                $s = intval(max($arSort));
                for ($ii = 1; $ii <= 10; $ii++) {
                    $i++;
                    $s += 100;
                    ?>
                    <tr>
                        <td>
                            <input type="hidden" name="ANSWER[]" value="<?= $i ?>"/>
                            <input type="hidden" name="ANSWER_ID_<?= $i ?>" value="0"/>
                        </td>
                        <td><input type="text" name="MESSAGE_<?= $i ?>" value="" style="width:100%;"/></td>
                        <td><?= SelectBoxFromArray("FIELD_TYPE_" . $i, $arrAnswerTypeList, "radio", "", "onchange=\"FIELD_TYPE_CHANGE(" . $i . ")\" class='typeselect'");
                            ?></td>
                        <td><input type="text" name="C_SORT_<?= $i ?>" value="<?= $s ?>" size="3"/></td>
                        <td><?= InputType("checkbox", "ACTIVE_" . $i, "Y", "Y", false) ?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <?
                }
                ?>
            </table>
        </td>
    </tr>
    <?
    $tabControl->EndTab();
    $tabControl->Buttons(array("disabled" => ($VOTE_RIGHT < "W"), "back_url" => "nota_vote_question_list.php?lang=" . LANGUAGE_ID . "&VOTE_ID=" . $VOTE_ID));
    $tabControl->End();
    ?>
</form>
<? $tabControl->ShowWarnings("form1", $message); ?>
<style type="text/css">
    table #answerlist td {
        vertical-align: middle !important;
    }
</style>

<?= BeginNote(); ?>
<span class="required"><sup>1</sup></span> -  <?= GetMessage("VOTE_MESSAGE_SPACE") ?>
<?= EndNote(); ?>
<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
