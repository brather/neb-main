<?php
/**
 * User: agolodkov
 * Date: 28.04.2015
 * Time: 17:50
 */
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

$documentRoot = realpath(dirname(__FILE__) . '/../../../neb');
$_SERVER['DOCUMENT_ROOT'] = $documentRoot;
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

$arParams = array(
    'SELECT' => array(
        'UF_LIBRARY',
        'UF_LIBRARIES',
    ),
);
$arFilter = array(
    '>UF_LIBRARY' => '0'
);
$user = new CUser();
$users = $user->GetList($by, $order, $arFilter, $arParams);
$i = 0;

while ($item = $users->Fetch()) {
    $i++;
    if (!isset($item['UF_LIBRARIES'])) {
        $item['UF_LIBRARIES'] = array();
    }
    if (!in_array($item['UF_LIBRARY'], $item['UF_LIBRARIES'])) {
        $item['UF_LIBRARIES'][] = $item['UF_LIBRARY'];
        if ($user->Update(
            $item['ID'],
            array('UF_LIBRARIES' => $item['UF_LIBRARIES'])
        )
        ) {
            echo 'Update user ', $item['ID'], PHP_EOL;
        }
    }
}

require_once($documentRoot . '/bitrix/modules/main/include/epilog_after.php');