/**
 * Из старого
 */

/**
 * Смена пароля
 */
$(function(){
    $.fn.initPassswordForm = function () {
        var form = $(this);
        form.submit(function (e) {
            e.preventDefault();
            var bsubmit = form.find('button');
            var c = bsubmit.parent().find('.messege_ok').clone(true);
            c.removeClass('hidden');
            $.ajax({
                url: form.attr('action'),
                type: 'post',
                data: form.serialize(),
                cache: false
            }).done(function (text) {
                if (text == '1') {
                    form.addClass('pass_changed');
                    form.find('input').val('').removeClass('ok');
                    form.find('.validate').removeClass('ok');
                    $('.messege_ok').removeClass('hidden');

                    return false;
                } else {
                    if (!form.hasClass('pass_changed')) {
                        $('<em class="error" style="display:block;">' + text + '</em>').insertBefore(form.find('.fieldrow:last-child button'));
                        return false;
                    } else {
                        $('<em class="error" style="display:block;">Вы уже отправили запрос на смену пароля</em>').insertBefore(form.find('.fieldrow:last-child button'));
                        return false;
                    }

                }
            });
        });
    }
});

$(function(){
    $.fn.identRegisterAddress = function () {
        var addr = $(this);
        addr.change(function () {
            if ($(this).attr('checked')) {
                $('.register-address-block input').attr('disabled', 'disabled').data('required', '');
            } else {
                $('.register-address-block input').removeAttr('disabled').data('required', 'required');
            }
        });
    }
});

$(function(){
    $.fn.seldate = function () {
        var block = $(this), realinp = block.find('.realseldate'), presetdate = realinp.val();
        if (presetdate && presetdate != '') {
            presetdate = presetdate.split('.');
            var dd = block.find('.sel_day'), mm = block.find('.sel_month'), yy = block.find('.sel_year');
            dd.val(presetdate[0]);
            mm.val(presetdate[1]);
            yy.val(presetdate[2]);
        }
    }
});

function testSelDate(block) { // проверка даты из 3-x select на валидность и ограничение по возрасту(если необходимо)
    var dd = block.find('.sel_day'), mm = block.find('.sel_month'), yy = block.find('.sel_year'), realinp = block.find('.realseldate'),
        dateformat = block.find('.dateformat'), yearsrestrict = block.find('.yearsrestrict'), required = block.find('.required');
    dateformat.hide(); //required.hide();
    yearsrestrict.hide();
    block.removeClass('error'); // ограничение возраста
    block.find('select').each(function () {
        var inp = $(this);
        inp.removeClass('error').trigger('error', ['off']);
    });
    realinp.val();


    if (dd.val() > -1 && mm.val() > -1 && yy.val() > -1) {
        var testDate = new Date(yy.val(), mm.val() - 1, dd.val());
        if (testDate.getDate() != dd.val() || (testDate.getMonth() + 1) != mm.val() || testDate.getFullYear() != yy.val()) {
            required.hide();
            block.addClass('error'); // не валидная дата
            dateformat.show();
            return 'error';

        }
        if (block.data('yearsrestrict')) {
            var today = new Date();
            var result = today.getTime() - testDate.getTime();
            if (Math.floor(result / (1000 * 60 * 60 * 24 * 365)) < block.data('yearsrestrict')) {
                block.addClass('error'); // ограничение возраста
                yearsrestrict.show();
                return 'error';
            }
        }
    }

    // заполнение скрытого поля
    realinp.val(dd.val() + '.' + mm.val() + '.' + yy.val());
    return 'ok';
}

$(function(){
    var form = $('#regform');
    form.find('.b-profile_passform').initPassswordForm();
    $('#cb3r').identRegisterAddress();
    $('.seldate').seldate()
    $('.seldate .js_select').change(function () {
        testSelDate($('.seldate'));
    });
});
    