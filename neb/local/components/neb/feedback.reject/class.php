<? use Bitrix\Main\Loader;
use Nota\Exalead\SearchClient;
use Nota\Exalead\SearchQuery;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 13.09.2016
 * Time: 14:00
 */

CBitrixComponent::includeComponentClass('neb:feedback');

/**
 * Class FeedbackRejectComponent
 */
class FeedbackRejectComponent extends FeedbackComponent
{
    private $arRequestForm;

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent() {
        $this->includeModules(['form', 'nota.exalead']);
        $this->getRequest();
        $this->getUser();
        $this->getTicket();
        $this->getForm();
        $this->getRequestForm();
        $this->setActions();

        $this->includeComponentTemplate();
    }

    /**
     * Получение данных формы
     */
    private function getRequestForm() {

        $arResult = [];

        if ($this->arRequest['FEEDBACK']['ACTION'] == 'REJECT')
            foreach ($this->arRequest['FEEDBACK'] as $sCode => $sVal)
                $arResult[$sCode] = $sVal;

        $this->arRequestForm = $arResult;
    }

    /**
     * Действия формы
     */
    private function setActions() {

        $this->arResult['TICKET'] = $this->arTicket;

        $iUser    = $this->arUser['ID'];
        $sMessage = trim($this->arRequestForm['MESSAGE']);

        if (empty($iUser) || empty($this->arTicket) || empty($sMessage))
            return;

        // установка идентификатора ответчика
        if (empty($this->arResult['ERROR']) && !$this->setTicketField('FB_USER_ID', $iUser))
            $this->arResult['ERROR'][] = 'Не удалось записать отетственного у заявки!';

        // установка ответа на заявку
        if (empty($this->arResult['ERROR']) && !$this->setTicketField('FB_REPLY_TEXT', $sMessage))
            $this->arResult['ERROR'][] = 'Не удалось добавить ответ к заявке!';

        // установка статуса для заявки
        if (empty($this->arResult['ERROR']) && !$this->setTicketStatus('REJECTED'))
            $this->arResult['ERROR'][] = 'Не удалось установить статус "Отклонена" для данной заявки!';

        // добавление записи в лог
        if (empty($this->arResult['ERROR']) && !$this->setHistoryItem('Отклонена', $this->arUser['ID']))
            $this->arResult['ERROR'][] = 'Не удалось записать лог-историю заявки!';

        // отправка сообщение на почту заявителю
        if (empty($this->arResult['ERROR']) && !empty($this->arTicket['FIELDS']['FB_EMAIL']['USER_TEXT'])) {

            $this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']
                = trim($this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']);

            $arMail = [
                "NUMBER" => $this->arTicket['ID'],
                "EMAIL"  => $this->arTicket['FIELDS']['FB_EMAIL']['USER_TEXT'],
                "THEME"  => $this->arTicket['FIELDS']['FB_THEME']['ANSWER_TEXT'],
                "TEXT"   => $this->arTicket['FIELDS']['FB_TEXT']['USER_TEXT'],
                "INFO"   => $sMessage,
                "DATE_CREATE" => $this->arTicket['DATE_CREATE'],

            ];

            if (!empty($this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']))
            {
                Loader::includeModule('nota.exalead');

                $query = new SearchQuery();
                $query->getById($this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT']);

                $client = new SearchClient();
                $res = $client->getResult($query);

                unset ($query, $client);

                $arMail['EX_BOOK_LINK'] = 'Издание: <a href="'.$_SERVER['HTTP_ORIGIN'].'/catalog/'.$this->arTicket['FIELDS']['FB_BOOK_ID']['USER_TEXT'].'/">' . $res['authorbook_original'] . ': ' . $res['name'] . '</a>';
            }

            $this->sendMail("NEB_FEEDBACK_REJECT", $arMail);
        }

        $this->getTicket(); // для показа результата в JSON
        $this->arResult['TICKET'] = $this->arTicket;

        // выдача результата: в режиме аякса вывод в виде JSON, иначе - редирект
        if ($this->arRequest['ajax'] == 'Y') {
            $this->arResult['TICKET']['USER'] = trim($this->arUser['LAST_NAME']
                . ' '. $this->arUser['NAME'] .' '. $this->arUser['SECOND_NAME']) ? : $this->arUser['LOGIN'];
            $this->arResult['json'] = true;
        } elseif (empty($this->arResult['ERROR'])) {
            LocalRedirect($this->arParams['FEEDBACK_URL']);
        }
    }
}