<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 25.01.2017
 * Time: 16:18
 */

namespace Bitrix\Posting;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class RubricTable
 *
 * Fields:
 * <ul>
 * <li> POSTING_ID int mandatory
 * <li> LIST_RUBRIC_ID int mandatory
 * </ul>
 *
 * @package Bitrix\Posting
 **/

class RubricTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_posting_rubric';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'POSTING_ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'title' => Loc::getMessage('RUBRIC_ENTITY_POSTING_ID_FIELD'),
            ),
            'LIST_RUBRIC_ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'title' => Loc::getMessage('RUBRIC_ENTITY_LIST_RUBRIC_ID_FIELD'),
            ),
        );
    }
}