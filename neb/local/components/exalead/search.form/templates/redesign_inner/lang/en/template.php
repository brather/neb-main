<?php

$MESS["SEARCH_FORM_DEBUG"] = 'Debug mode'; // Режим отладки
$MESS["SEARCH_FORM_BUTTON"] = "Search";

$MESS["SEARCH_FORM_SIMPLE_SEARCH"] = 'Simple search';
$MESS["SEARCH_FORM_MORE_SIMPLE_PLACEHOLDER"] = ''; //To be or not to be
$MESS["SEARCH_FORM_COLLECTIONS"] = "Material type"; //Искать по коллекциям;
$MESS["SEARCH_FORM_CHOISE_COLLECTION"] = 'choise collection';
$MESS["SEARCH_FORM_MORE"] = 'More';
$MESS["SEARCH_FORM_MORE_MOBILE"] = 'Select collections';

$MESS["SEARCH_FORM_DOUBLE_SEARCH"] = 'Search by author and/or title';
$MESS["SEARCH_FORM_FAQ_ANCHOR"] = '';
$MESS["SEARCH_FORM_FAQ_TITLE"] = '';
$MESS['SEARCH_FORM_AUTHOR'] = 'author';
$MESS["SEARCH_FORM_AUTHOR_PLACEHOLDER"] = ''; //William Shakespeare
$MESS['SEARCH_FORM_TITLE'] = 'main title';
$MESS["SEARCH_FORM_TITLE_PLACEHOLDER"] = ''; //Hamlet
$MESS["SEARCH_FORM_EXTENDED_SEARCH"] = "Advanced search"; // Расширенный поиск
$MESS["SEARCH_FORM_EXTENDED_SEARCH_BY"] = "Search by fields";

$MESS["SEARCH_FORM_CUSTOMIZE"] = 'Cuztomize';
