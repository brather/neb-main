<?php
/**
 * User: D-Efremov
 * Date: 05.04.2017
 * Time: 12:20
 */

namespace Neb\Main\Agents;

use \Bitrix\Main\Application,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\FileTable,
    \Bitrix\Main\UserTable,

    \Neb\Main\Helper\DbHelper,
    \Neb\Main\Helper\FileHelper,
    \Neb\Main\Helper\Debug;


/**
 * Class Main
 *
 * @package Neb\Main\Agents
 */
class Main
{
    /**
     * Обновляет ключ у анонимного читателя электронного читального зала
     *
     * @return string
     */
    public static function updateAnonymousTokenDate() {

        $sResult = "\\" . __METHOD__ . '();';

        $obNebUser = new \nebUser();

        // получение идентификатора анонимного читателя
        $arAnonymousUser = $obNebUser->getAnonymousUser();

        if (intval($arAnonymousUser['ID']) <= 0)
            return $sResult;

        // установка идентификатора пользователя
        $obNebUser->setId($arAnonymousUser['ID']);

        // если токен истекает - обновить только дату
        if (!$obNebUser->checkToken()) {
            $obNebUser->setNewToken(true);
        }

        return $sResult;
    }

    /**
     * Удаляет закешированные изображения скан-заказов
     *
     * @return string
     */
    public static function removeCacheFiles() {

        $sResult = "\\" . __METHOD__ . '();';

        Service::removeDirectory($_SERVER["DOCUMENT_ROOT"] . '/upload/resize_cache/scanned/', false);

        return $sResult;
    }

    /**
     * Оповещает правообладателей
     *
     * @param int $iDays
     * @return string
     */
    public static function rightholderNotify($iDays = 10) {

        $sResult =  "\\" . __METHOD__ . '(' . $iDays . ');';

        $obRightHolder = new \RightHolder();
        $arContracts   = $obRightHolder->getRemindContracts($iDays);
        $arEntities    = $obRightHolder->getContractsEntities($arContracts);
        $arBooks       = $obRightHolder->getBooksFromExalead($arEntities['BOOKS']);
        $arFiles       = $obRightHolder->getFiles($arEntities['FILES']);
        $arUsers       = $obRightHolder->getUsers($arEntities['USERS']);

        $iSend         = $obRightHolder->sendRightholderNotify($arContracts, $arUsers, $arFiles, $arBooks);

        return $sResult;
    }

    /**
     * Деактивирует пользователей, которые были добавлены оператором ЭЧЗ
     * и в течении суток не подтвердили электронную почту
     * @return string
     */
    public static function deactivateNotConfirmedUsers() {

        $sResult = "\\" . __METHOD__ . '();';

        $arUsers = UserTable::getList([
            'order'  => ['ID' => 'ASC'],
            'filter' => ['ACTIVE' => 'Y', 'XML_ID' => 'workplace'],
            'select' => ['ID', 'DATE_REGISTER']
        ])->fetchAll();

        $obUser = new \CUser();
        $sLastDate = (new DateTime())->add('+1 day')->getTimestamp();
        foreach ($arUsers as $arUser) {
            $sRegDate = (new DateTime($arUsers['DATE_REGISTER']))->getTimestamp();
            if ($sRegDate < $sLastDate)
                $obUser->Update($arUser['ID'], ['XML_ID' => '', 'ACTIVE' => 'N']);
        }

        return $sResult;
    }

    /**
     * Конвертирует в БД Битрикса у таблиц collation => "utf8_unicode_ci" и engine => "InnoDB"
     * Написан для случая появления новых таблиц с некорректными collation и engine после обновления Битрикса.
     * Причина: разные collations приводили к ошибке Mysql query error: (1267) Illegal mix of collations
     * (utf8_general_ci,IMPLICIT) and (utf8_unicode_ci,IMPLICIT) for operation '=' (400)
     *
     * @return string
     */
    public static function convertDbTablesProps() {

        $sResult = "\\" . __METHOD__ . '();';

        // корректные значения
        $sCollation = 'utf8_unicode_ci';
        $sEngine    = 'InnoDB';

        $connection = Application::getConnection();

        // получение информациии о отличающихся collation и engine таблиц и формирование запросов
        $arQuery = [];
        $sDatabase = $connection->getDatabase();
        $rsRecords = $connection->query("
          SELECT
            TABLE_NAME,
            TABLE_COLLATION,
            ENGINE
          FROM
            information_schema.TABLES
          WHERE
            TABLE_SCHEMA = '$sDatabase' AND (TABLE_COLLATION != '$sCollation' OR ENGINE != '$sEngine')
          ORDER BY
            TABLE_NAME
        ");
        while ($arFields = $rsRecords->fetch()) {
            $sTable = $arFields['TABLE_NAME'];
            if ($sCollation != $arFields['TABLE_COLLATION'])
                $arQuery[] = "ALTER TABLE $sTable CONVERT TO CHARACTER SET utf8 COLLATE $sCollation;";
            if ($sEngine != $arFields['ENGINE'])
                $arQuery[] = "ALTER TABLE $sTable ENGINE = $sEngine;";
        }

        // применение изменений в БД и запись события в журнал
        if (!empty($arQuery)) {

            foreach ($arQuery as $sQuery) {
                $connection->query($sQuery);
            }

            $sMess = "К базе данных автоматически произведены следующие запросы для унификации "
                . "collation и engine у следующих таблиц базы данных: \n\n\r" . implode("\n\r", $arQuery);
            $log = new \CEventLog();
            $log->Log('DEBUG', 'main', 'neb.main', __FUNCTION__, $sMess);
        }

        return $sResult;
    }

    /**
     * Оптимизирует размер и расширение изображений, загруженных в Битрикс за предыдущий день
     * во всех модулях, кроме медиабиблиотеки
     *
     * @return string
     */
    public static function optimizeImgFiles() {

        $sResult = "\\" . __METHOD__ . '();';

        try {

            $sLastDate = (new DateTime())->add('-1 day')->format('d.m.Y');
            $connection = Application::getConnection();

            // получение изображений из таблицы файлов за последние сутки, медиабиблиотека исключается
            $rsFiles = FileTable::getList([
                'select' => ['ID', 'FILE_SIZE', 'CONTENT_TYPE', 'SUBDIR', 'FILE_NAME', 'ORIGINAL_NAME'],
                'order'  => ['ID' => 'ASC'],
                'filter' => [
                    '!MODULE_ID'     => 'fileman',
                    '%=CONTENT_TYPE' => 'image/%',
                    '>=TIMESTAMP_X'  => $sLastDate . ' 00:00:00',
                    '<=TIMESTAMP_X'  => $sLastDate . ' 23:59:59',
                ]
            ]);
            $arSourceFiles = [];
            while ($arFields = $rsFiles->fetch()) {
                $arSourceFiles[$arFields['ID']] = $arFields;
            }

            // формирование массива преобразования данных
            foreach ($arSourceFiles as $id => $arFile) {

                $sFilePath = FileHelper::getFilePath($arFile, true);

                // оптимизация размера файлов
                $sInfo = FileHelper::optimizeImage($sFilePath);

                // установка корректных myme-type и расширений файлов
                $arNewFile = FileHelper::convertDbImageFileParams($arFile);

                // запись
                if (!empty($arNewFile)) {

                    // переименование физического файла
                    if (!empty($arNewFile['PATH']) && !rename($sFilePath, $arNewFile['PATH'])) {
                        unset($arNewFile['FILE']['CONTENT_TYPE'], $arNewFile['FILE']['FILE_NAME'], $arNewFile['FILE']['ORIGINAL_NAME']);
                    }

                    // обновление в БД
                    if (!empty($arNewFile['FILE'])) {
                        $strSql = DbHelper::getUpdateQuery('b_file', $arNewFile['FILE'], ['ID' => $id]);
                        $connection->queryExecute($strSql);
                    }
                }
            }

        } catch (\Exception $e) {
            $log = new \CEventLog();
            $log->Log('ERROR', 'optimizeImgFiles', 'neb.main', __FUNCTION__, strval($e));
        }

        return $sResult;
    }

    /**
     * Поиск однострочных комментариев в скриптах и стилях проекта (нужно для минификатора HTML-кода)
     */
    public static function searchSingleLineComments() {

        $sResult = "\\" . __METHOD__ . '();';

        if (!MINIFY_HTML_MODE) { // проверка скриптов имеет смысл только при работе сайта в режиме минификации
            return $sResult;
        }

        try {
            // поиск всех пхп-файлов
            function findPhpFiles($dir) {
                $result = [];
                $root = scandir($dir);
                $file = str_replace('\\', '/', __FILE__);
                foreach($root as $value) {
                    if($value === '.' || $value === '..' || is_file("$dir/$value")) {
                        if (false !== stripos($value, '.php') && $file !== "$dir/$value") {
                            $result[] = "$dir/$value";
                        }
                        continue;
                    }
                    foreach(findPhpFiles("$dir/$value") as $value) {
                        $result[] = $value;
                    }
                }
                return $result;
            }
            // проверка на наличие комментария в скрипте
            function containsComment(&$content) {
                $bResult = false;
                $content = str_ireplace(['://', 'src="//', "src='//", '\/\//', '//rawgithub.com'], '', $content);
                if (false !== mb_stripos($content, '<script') && false !== mb_stripos($content, '//')) {
                    while(true) {
                        $iStart = mb_stripos($content, '<script');
                        $iEnd = mb_stripos($content, '</script>');
                        if (false !== $iStart && false !== $iEnd) {
                            $s = mb_substr($content, $iStart, $iEnd - $iStart);
                            if (false !== mb_stripos($s, '//')) {
                                $bResult = true; break;
                            }
                            $content = mb_substr($content, $iEnd + 9);
                        }
                        else {
                            break;
                        }
                    }
                }
                return $bResult;
            }

            // поиск однострочных комментариев в скриптах php-файлов
            $arResult = [];
            $arFiles = findPhpFiles($_SERVER["DOCUMENT_ROOT"] . '/local');
            foreach ($arFiles as $k => $sFile) {
                if (containsComment(file_get_contents($sFile))) {
                    $arResult[] = $sFile;
                }
            }

            // запись события в журнал
            if (!empty($arResult)) {
                $arResult = "В проекте в следующих файлах появились однострочные комментарии в тегах script"
                    . " (запрещены в включенном режиме минификации html-кода в модуле neb.main): \n\n\r"
                    . implode("\n\r", $arResult);
                $log = new \CEventLog();
                $log->Log('DEBUG', 'main', 'neb.main', __FUNCTION__, $arResult);
            }

        } catch (\Exception $e) {
            $log = new \CEventLog();
            $log->Log('ERROR', 'optimizeImgFiles', 'neb.main', __FUNCTION__, strval($e));
        }

        return $sResult;
    }

    /**
     * Отправка ошибок в Error log на почту администратору сайта
     *
     * @return string
     */
    public static function sendDailyDebugMessage() {

        $sResult = "\\" . __METHOD__ . '();';

        $sLastDate = (new DateTime())->add('-1 day')->format('d.m.Y');

        $rsLog = \CEventLog::GetList(['ID' => 'DESC'], [
            "TIMESTAMP_X_1" => $sLastDate . '00:00:00',
            "TIMESTAMP_X_2" => $sLastDate . '23:59:59',
            "SEVERITY"      => 'ERROR|WARNING|INFO|DEBUG'
        ]);
        $sSend = '';
        while($arFields = $rsLog->Fetch()) {
            $sSend .=  "ID: "    . $arFields['ID']
                . "\nВремя: "    . $arFields['TIMESTAMP_X']
                . "\nУровень: "  . $arFields['SEVERITY']
                . "\nСобытие: "  . $arFields['AUDIT_TYPE_ID']
                . "\nМодуль:  "  . $arFields['MODULE_ID']
                . "\nОбъект: "   . $arFields['ITEM_ID']
                . "\nОписание: " . $arFields['DESCRIPTION']
                . "\n\n\n";
        }

        if (!empty($sSend)) {
            $sSend .= "\nСсылка: http://" . SITE_HOST . '/bitrix/admin/event_log.php?PAGEN_1=1&SIZEN_1=50&lang=ru&set_filter=Y&adm_filter_applied=0'
                ."&find_type=audit_type_id&find_timestamp_x_1=$sLastDate+00%3A00%3A00&find_timestamp_x_2=$sLastDate+23%3A59%3A59"
                .'&find_severity%5B0%5D=ERROR&find_severity%5B1%5D=WARNING&find_severity%5B2%5D=INFO&find_severity%5B3%5D=DEBUG';

            Debug::sendMail('Ошибки за ' . $sLastDate, $sSend);
        }

        return $sResult;
    }
}