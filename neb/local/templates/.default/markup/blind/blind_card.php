
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<a href="blind_index.php"  accesskey="0" class="b_logo iblock" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					<img src="../i/logo_1.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА">
					Национальная Электронная Библиотека
				</a>
				<a href="blind_about.php" class="b-portalabout_lnk iblock" title="О проекте">О проекте</a>
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="b-search_fieldtb b-text bbox" id="" value="Станислав Лем">
						</span>
						
					</div>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
				    	<a href="blind_find_e.php" class="b-search_exlink" title="Расширенный поиск">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>			

		</div><!-- /.b-portalinfo -->
		
	</form>			

</section>
	<section class="innersection innerwrapper clearfix">
		<div class="b-bookpopup b-bookpage" >
			<div class="b-bookpopup_in bbox">
				<div class="b-bookframe iblock">
					<img alt="" class="b-bookframe_img" src="./pic/pic_15.jpg">
	
				</div>
				<div class="b-onebookinfo iblock">
					<div class="b-bookhover">
						<span class="b-autor">Станислав Лем</span>
						<div class="b-bookhover_tit black">Сумма технологии</div>
						<p>Основная цель книги — попытка прогностического анализа научно-технических, морально-этических и философских проблем, связанных с функционированием цивилизации в условиях свободы от технологических и материальных ограничений (по образному выражению автора, «исследование шипов ещё несуществующих роз»</p>
					</div>		
					<div class="clearfix">
						<a href="#" class="formbutton left">Читать</a>	
						<div class="b-result-type right">
							<span class="b-result-type_txt">pdf</span>
							<span class="b-result-type_txt">текст</span>
						</div>	
					</div>		
					<div class="rel">
						<ul class="b-resultbook-info">
							<li><span>Количество страниц: </span> 427 </li>
							<li><span>Год публикации:</span> 1964</li>
							<li><span>Издательство:</span> Азбука</li>
						</ul>
						<div class="b-infobox b-descrinfo">						
							<div class="b-infoboxitem">
								<span class="tit iblock">Автор: </span>
								<span class="iblock val">Горшков, Евгений Андреевич</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Место издания: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">ISBN:</span>
								<span class="iblock val">2-266-11156-6</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Язык: </span>
								<span class="iblock val">русский</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тираж: </span>
								<span class="iblock val">427</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Номер тома: </span>
								<span class="iblock val">1</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Серия: </span>
								<span class="iblock val">123</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие тома: </span>
								<span class="iblock val">Сумма технологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Неконтролируемое имя: </span>
								<span class="iblock val">Институт психологии Российской академии наук</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Заглавие: </span>
								<span class="iblock val">Становление социальной психологии США : автореферат дис. ... кандидата психологических наук : 19.00.01</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Выходные данные: </span>
								<span class="iblock val">Москва 2011</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Физическое описание: </span>
								<span class="iblock val">23 с.</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">Общая психология, психология личности, история психологии</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Тема: </span>
								<span class="iblock val">История психологии -- История психологии в Соединенных Штатах Америки (США) -- История психологии в новейшее время -- Психологические направления</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Ключевые слова: </span>
								<span class="iblock val">социальная психология</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Хранение:</span>
								<span class="iblock val">9 11-3/12;</span>
							</div>
							<div class="b-infoboxitem">
								<span class="tit iblock">Электронный адрес:</span>
								<span class="iblock val">Электронный ресурс</span>
							</div>							
						</div><!-- /b-infobox -->
					</div>
				</div>
				
			

		</div> 	<!-- /.bookpopup -->

		

	</section>
</div><!-- /.homepage -->

<? include("footer.php"); ?>
</body>
</html>