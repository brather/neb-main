<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    \Neb\Main\Helper\MainHelper,
    Bitrix\NotaExt\Utils,
    Nota\Exalead\SearchQueryLibrary,
    Nota\Exalead\SearchClient;

Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__ . '/autocomplete.php');

$q = trim(htmlspecialcharsEx($_REQUEST['term']));
if (empty($q))
    exit();

Loader::includeModule('nota.exalead');

if (check_bitrix_sessid()) {

    $query = new SearchQueryLibrary();
    $arResult = array();

    $query->getSuggestAuthor($q);
    $client = new SearchClient();
    $result = $client->getResult($query);
    if (!empty($result)) {
        foreach ($result as $v) {
            $_v = Utils::trimming_line($v, 60, '');
            $arResult[] = array('label' => $_v, 'original' => $v, 'category' => Loc::getMessage('LIB_SEARCH_FORM_AUTOCOMPLETE_AUTHOR'));
        }
    }

    if (!empty($arResult)) {
        MainHelper::showJson($arResult);
    }
}