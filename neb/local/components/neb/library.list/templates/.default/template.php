<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if ($arParams["LIBRARY_TYPE"] == 'library')
	$APPLICATION->SetPageProperty('hide-title', true);

?>
<div class="row">
	<div class="b-map rel mappage">
		<form method="get" action="." class="b-map_search_filtr">
			<div class="row">

				<div class="col-md-6 b-cytyinfo">
					<span class="b-map_search_txt"><?=Loc::getMessage('LIBRARY_LIST_FIND_CLOSEST');?></span>
					<a href="#" class="b-citycheck setLinkPoint" data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>" data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>"><?=$arResult["CITY"]["UF_CITY_NAME"]?></a>
					<a href="<?= $arParams["LIBRARY_TYPE"] == 'library'	? '/library/regions/' : '/participants/cities/';
					?>" class="b-changecity js_changecity"><?=Loc::getMessage('LIBRARY_LIST_CHANGE_CITY');?></a>
				</div>

				<div class="col-md-6 b-citysearch">
					<input
							type="text"
							class="b-map_tb ui-autocomplete-input searchonmap form-control"
							name="mapq"
							id="qcity"
							autocomplete="off"
							placeholder="<?= Loc::getMessage('LIBRARY_LIST_ENTER_ADDRESS'); ?>"
							value="<?= isset($_REQUEST['mapq']) ? htmlentities($_REQUEST['mapq']) : null ?>" />

					<span class="button_d" title="<?=Loc::getMessage('LIBRARY_LIST_CLEAR_SEARCH');?>"><span class="fa fa-times"></span></span>
					<button type="submit" class="button_b btn btn-default" value="<?=Loc::getMessage('LIBRARY_LIST_FIND_BUTTON');?>" name="submit"
							title="<?=Loc::getMessage('LIBRARY_LIST_SEARCH_START');?>"><span class="fa fa-search"></span></button>
				</div>
			</div>
		</form>

		<div class="ymap libraries-map" id="ymap"
			 data-path="<?= $APPLICATION->GetCurPageParam('json=Y'); ?>"
			 data-lat="<?= $arResult["CITY"]["UF_POS"][1] ? : '63.931126'?>"
			 data-lng="<?= $arResult["CITY"]["UF_POS"][0] ? : '99.282838'?>"
			 data-zoom="<?= $arResult["CITY"]["UF_POS"][0] ? '10' : '3'?>"
			 data-show-nearly="<?= (isset($_REQUEST['show-nearly']) && $_REQUEST['show-nearly'] === 'Y') ? 1 : 0 ?>"
			 data-find-location="<?= $arResult["FIND_LOCATION"] ?>"
		></div>
        <script src="//api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
	<span class="marker_lib hidden">
		<a href="#" class="b-elar_name_txt"></a><br />
		<span class="b-elar_status"></span><br />
		<span class="b-map_elar_info">
			<span class="b-map_elar_infoitem addr"><span><?=Loc::getMessage('LIBRARY_LIST_POPUP_ADDRESS');?>:</span></span><br />
			<span class="b-map_elar_infoitem graf"><span><?=Loc::getMessage('LIBRARY_LIST_POPUP_WORK');?>:</span></span>
		</span>
		<span class="b-mapcard_act clearfix">
			<span class="right neb b-mapcard_status"><?=Loc::getMessage('LIBRARY_LIST_POPUP_USER');?></span>
			<a href="#" class="button_mode"><?=Loc::getMessage('LIBRARY_LIST_POPUP_URL');?></a>
		</span>
	</span>
	</div>
</div>
<div class="row">
	<section class="rel innerwrapper clearfix" id="library-list">
		<div class="b-elar_usrs">
			<h2><?=Loc::getMessage('LIBRARY_LIST_LIBS');?></h2>
			<ol class="b-elar_usrs_list">
				<?
				if (isset($arResult['ITEMS_PAGEN'])) {
					$i = $arResult["NAV_OFFSET"];
					foreach($arResult['ITEMS_PAGEN'] as $arItem) {
						$i++;
						?>
						<li itemscope itemtype="http://schema.org/Organization" class="clearfix">
							<span class="num"><?=$i?>.</span>

							<div class="b-elar_name">
								<? if(!empty($arItem['URL'])): ?>
									<a itemprop="name" href="<?=$arItem['URL']?>" class="b-elar_name_txt"><?=$arItem['NAME']?></a>
								<? else: ?>
									<span itemprop="name" class="b-elar_name_txt"><?=$arItem['NAME']?></span>
								<? endif; ?>

								<?if($arItem['IS_NEB']):?>
									<div class="b-elar_status neb">
										<div><?=$arItem['STATUS']?>
											<br /><?=Loc::getMessage('LIBRARY_LIST_MEMBER_NEL');?>
											<?if (isset($arItem['WCHZ'])):?><br>ЭЧЗ<?endif;?>
										</div>
									</div>
								<?endif;?>
							</div>
							<div class="b-elar_address">
								<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="b-elar_address_txt">
									<div class="icon">
										<div class="b-hint">
											<?=Loc::getMessage('LIBRARY_LIST_ADDRESS');?>
										</div>
									</div>
									<?=$arItem['ADDRESS']?>
								</div>
								<a href="#" class="b-elar_address_link" data-lat="<?=$arItem["UF_POS"][1]?>" data-lng="<?=$arItem["UF_POS"][0]?>"><?=Loc::getMessage('LIBRARY_LIST_SHOW_ON_MAP');?></a>
							</div>
						</li>
						<?
					}
				}
				?>
			</ol>
			<?=$arResult["NAVIGATION"]?>
		</div>
	</section>
</div>
