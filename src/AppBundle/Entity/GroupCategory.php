<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 20:01
 */

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serialization;


class GroupCategory
{
    /**
     * @var string
     * @Serialization\Groups({"list", "details"})
     */
    public $path;

    /**
     * @var string
     * @Serialization\Groups({"list", "details"})
     */
    public $fullPath;

    /**
     * @var string
     * @Serialization\Groups({"list", "details"})
     */
    public $id;

    /**
     * @var string
     * @Serialization\Groups({"list", "details"})
     */
    public $zapId;

    /**
     * @var string
     * @Serialization\Groups({"list", "details"})
     */
    public $title;
}