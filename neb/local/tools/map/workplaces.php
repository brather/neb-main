<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Loader,
    \Bitrix\Main\Data\Cache,
    \Bitrix\Highloadblock\HighloadBlockTable,
    \Neb\Main\Helper\MainHelper;

class WorkplacesJson {

    /**
     * Главный метод, использует кеширование
     */
    public static function main() {

        /*$arResult = [];

        $cacheTime = 3600;
        $cacheDir  = CACHE_DIRECTORY . '/' . __CLASS__ . '/'. __FUNCTION__;
        $cacheId = $cacheDir;
        $obCache = Cache::createInstance();

        if ($obCache->initCache($cacheTime, $cacheId, $cacheDir)) {

            $arResult = $obCache->getVars();

        } elseif ($obCache->startDataCache()) {

            $arResult = static::getNetLibs();
            $obCache->endDataCache($arResult);
        }*/

        $arResult = static::getNetLibs();

        static::getResult($arResult);
    }

    /**
     * Выборка всех библиотек, для которых есть ВЧЗ
     */
    private static function getWorkplacesLibs() {

        $arNebLibs = [];

        if (!Loader::includeModule('iblock'))
            return $arNebLibs;

        $rsWorkPlace = \CIBlockElement::GetList(
            ['PROPERTY_LIBRARY' => 'ASC'],
            [
                'IBLOCK_ID'         => IBLOCK_ID_WORKPLACES,
                'ACTIVE'            => 'Y',
                '!PROPERTY_LIBRARY' => false
            ],
            ['PROPERTY_LIBRARY']
        );
        while ($arFields = $rsWorkPlace->Fetch())
            $arNebLibs[] = $arFields['PROPERTY_LIBRARY_VALUE'];

        return $arNebLibs;
    }

    /**
     * Выборка ссылок на библиотеки
     * @return array
     */
    private static function getParticipants($arWorkplacesLibs) {

        $arResult = [];

        if (empty($arWorkplacesLibs) || !Loader::includeModule('iblock'))
            return $arResult;

        $rsParticipants = \CIBlockElement::GetList(
            ['PROPERTY_LIBRARY_LINK' => 'ASC'],
            [
                'IBLOCK_ID'             => IBLOCK_ID_LIBRARY,
                'PROPERTY_LIBRARY_LINK' => $arWorkplacesLibs,
                'ACTIVE'                => 'Y'
            ],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'DETAIL_PAGE_URL', 'PROPERTY_LIBRARY_LINK']
        );
        while ($arFields = $rsParticipants->GetNext(true, false))
            $arResult[$arFields['PROPERTY_LIBRARY_LINK_VALUE']] = $arFields['DETAIL_PAGE_URL'];
        ksort($arResult);

        return $arResult;
    }

    /**
     * Выбираем HL-библиотеки, для которых есть ВЧЗ, и отдаем их на клиент
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    private static function getNetLibs() {

        $arResult = [];

        $arWorkplacesLibs = static::getWorkplacesLibs();
        $arParticipants   = static::getParticipants($arWorkplacesLibs);

        if (empty($arWorkplacesLibs) || !Loader::includeModule('highloadblock'))
            return $arResult;

        $libraryHLBlock  = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $libraryEntity   = HighloadBlockTable::compileEntity($libraryHLBlock)->getDataClass();

        $libraryData = $libraryEntity::getList(array(
            'select' => ['ID', 'UF_NAME', 'UF_ADRESS', 'UF_POS'],
            'order'  => ['ID'],
            'filter' => ['ID' => $arWorkplacesLibs],
        ));
        while ($arLib = $libraryData->fetch()) {

            $map = explode(' ', $arLib['UF_POS']);
            $point = [
                'id'          => $arLib['ID'],
                'address'     => $arLib['UF_ADRESS'],
                'name'        => $arLib['UF_NAME'],
                'coordinates' => array($map[1], $map[0]),
            ];
            // ставим ссылку на библиотеку-участницу НЭБ
            if (!empty($arParticipants[$arLib['ID']]))
                $point['link'] = $arParticipants[$arLib['ID']];

            $arResult[] = $point;
        }

        return $arResult;
    }

    private static function getResult($arNetLibs) {

        $arResult = [];

        $arResult['libs'][] = array(
            'id' => 1,
            'placemarksize' => array(34, 48),
            'placemarkicon' => '/local/templates/.default/markup/i/pin.png',
            'regions' => array(
                array(
                    'current'     => true,
                    'city'        => '',
                    'id'          => 592,
                    'coordinates' => array('55.753676', '37.619899'),
                    'metro' =>
                        array(
                            array(
                                'station' => '',
                                'id' => 1,
                                'points' => $arNetLibs,
                            )
                        )
                )
            )
        );

        static::showResult($arResult);
    }

    private static function showResult($arResult) {
        MainHelper::showJson($arResult);
    }
}

WorkplacesJson::main();