<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Neb\Main\Helper\DbHelper,
    \Neb\Main\Helper\MainHelper;

Loc::loadLanguageFile(__FILE__);
function getSizeFile($file) {
    $fileSize = filesize($file);
    // Размер больше 1 Кб
    if($fileSize > 1000) {
        $fileSize = ($fileSize/1000);
        // Размер больше 1 Мегабайта
        if($fileSize > 1000) {
            $fileSize = ($fileSize/1000);
            // Размер больше 1 Гигабайта
            if($fileSize > 1000) {
                $fileSize = ($fileSize/1000);
                $fileSize = round($fileSize, 1) . Loc::getMessage('ADD_FILE_AJAX_GB');
            } else {
                $fileSize = round($fileSize, 1) . Loc::getMessage('ADD_FILE_AJAX_MB');
            }
        } else {
            $fileSize = round($fileSize, 1) . Loc::getMessage('ADD_FILE_AJAX_KB');
        }
    } else {
        $fileSize = round($fileSize, 1) . Loc::getMessage('ADD_FILE_AJAX_BYTE');
    }
    return $fileSize;
}
function setNewName($fileName, $filePath, $i = 1) {
    $file = $filePath . $fileName;
    $resName = '';
    $fileInfo = pathinfo($file);
    if(file_exists($file)) {
        $fileNewName = $fileInfo['filename'] . '_' . $i . '.' . $fileInfo['extension'];
        if(file_exists($filePath . $fileNewName)) {
            $i = $i + 1;
            $resName = setNewName($fileName, $filePath, $i);
        } else {
            $resName = $fileInfo['filename'] . '_' . $i;
        }
    } else {
        $resName = $fileInfo['filename'];
    }
    return $resName;
}
$context = Application::getInstance()->getContext();
$arRequest = $context->getRequest()->toArray();
$arServer = $context->getServer()->toArray();
$documentRoot = $arServer['DOCUMENT_ROOT'];
$arData = array();
$arFiles = array();
$htmlRow = '';

$idForm = 'pdf-upload';
$uploadBlock = 'upload-files';
if(!empty($arRequest['subFolder'])) {
    $idForm .= '-' . str_replace(' ', '', trim($arRequest['subFolder']));
    $uploadBlock .= '-' . str_replace(' ', '', trim($arRequest['subFolder']));
}
if(!empty($arRequest['fileName'])){
    $idForm .= '-' . str_replace(' ', '', trim($arRequest['fileName']));
    $uploadBlock .= '-' . str_replace(' ', '', trim($arRequest['fileName']));
}

//region Задание папки для хранения файла pdf
if(!empty($arRequest['folder'])) {
    //region Указана папка раздела
    if(!empty($arRequest['subFolder'])) {
        $uploadNewDir = $arRequest['folder'] . $arRequest['subFolder'] . '/';
        //region Создадим папку раздела если она не существует
        if(!is_dir($uploadNewDir)) {
            mkdir($uploadNewDir, 0777);
        }
        //endregion
        $uploadDirRel = $arRequest['folder'] . $arRequest['subFolder'] . '/';
        $uploadDirAbs = $uploadNewDir;
    }
    //endregion
    //region Папка раздела отсутствует
    else {
        $uploadDirRel = $arRequest['folder'];
        $uploadDirAbs = $arRequest['folder'];
    }
    //endregion
    $error = false;
    $arType = explode(',', $arRequest['fileType']);
    foreach($_FILES as $file){
        $arPathInfoFile = pathinfo($uploadDirAbs . basename($file['name']));
        if(empty($arPathInfoFile['extension'])) {
            $extFile = substr(strrchr($file['name'], "."), 1);
        } else {
            $extFile = $arPathInfoFile['extension'];
        }
        if (in_array($extFile, $arType)) {
            if($file['size'] <= $arRequest['fileMaxSize']) {
                $fileName = setNewName($arPathInfoFile['basename'], $uploadDirAbs);
                $baseName = $fileName . '.' . $extFile;
                if(!empty($arRequest['fileName'])) {
                    $newFile = $arRequest['fileName'] . '.' . $extFile;
                } else {
                    $newFile = $baseName;
                }
                
                if(move_uploaded_file($file['tmp_name'], $uploadDirAbs . $newFile)) {
                    
                    $fileSize = getSizeFile($uploadDirAbs . $newFile);
                    $urlDownload = $uploadDirRel . $newFile;
                    $arFiles[] = array(
                        'ALIAS' => $arRequest['aliasName'],
                        'NAME_SHORT' => $fileName,
                        'NAME_FULL' => $baseName,
                        'EXTENSION' => $extFile,
                        'FILE_SIZE' => $fileSize,
                        'URL_DOWNLOAD' => $urlDownload
                    );

                    $connection = DbHelper::getLibraryConnection();

                    $sql = "UPDATE tbl_common_biblio_card SET pdfLink='" . $urlDownload . "', UpdatingDateTime='" . date('Y-m-d H:i:s') . "' WHERE FullSymbolicId='" . $arRequest['addParamOriginalName'] . "';";
                    $resultSql = $connection->query($sql);
                    if($resultSql) {
                        if(!empty($arRequest['aliasName'])) {
                            $titleFile = $arRequest['aliasName'];
                        } else {
                            $titleFile = $fileName;
                        }
                        $htmlRow .= '
                            <tr class="template-download fade in">
                                <td>
                                    <p class="name">';
                        if($arRequest['urlDownload']) {
                            $htmlRow .= '   <a class="file-fa" 
                                            href="' . $urlDownload . '" 
                                            title="' . $newFile . '" 
                                            download="' . $newFile . '">
                                            ' . $titleFile . '                                        
                                        </a>';
                        } else {
                            $htmlRow .= '<span class="file-fa">' . $titleFile . '</span>';
                        }
                        $htmlRow .= '</p>
                                </td>
                                <td>
                                    <span class="size">' . $fileSize . '</span>
                                </td>
                                <td>
                                    <a class="btn btn-default js-download-book"
                                       target="_blank"
                                       href="/local/tools/plan_digitization/download.book.php?path=' . $urlDownload . '">
                                        <i class="fa fa-download"></i> <span>Скачать файл</span>
                                    </a>
                                </td>
                                <td>
                                    <a  data-form="#' . $idForm . '" 
                                        data-container="#' . $uploadBlock . '" 
                                        data-lang-confirm-title="' . Loc::getMessage('ADD_FILE_AJAX_CONFIRM_TITLE') . '"
                                        data-lang-confirm-text="' . Loc::getMessage('ADD_FILE_AJAX_CONFIRM_YES') . '" 
                                        data-lang-confirm-yes="' . Loc::getMessage('ADD_FILE_AJAX_CONFIRM_TEXT') . '"                                     
                                        data-ajax-delete="' . $arRequest['ajaxDeleteFile'] . '" 
                                        data-delete="' . $urlDownload . '"
                                        href="#" 
                                        class="btn btn-danger btn-delete-file">
                                        <i class="glyphicon glyphicon-trash"></i><span>' . Loc::getMessage('ADD_FILE_AJAX_DELETE') . '</span>
                                    </a>
                                </td>
                            </tr>'; 
                    } else {
                        unlink($urlDownload);
                        $arData['ERROR'] = Loc::getMessage('ADD_FILE_AJAX_ERROR_NOT_CONNECTION_TO_DB');
                        break;
                    }
                    //$files[] = realpath($uploadDirAbs . $file['name']);
                } else {
                    $arData['ERROR'] = Loc::getMessage('ADD_FILE_AJAX_ERROR_NOT_SAVE');
                    break;
                }
            } else {
                $arData['ERROR'] = Loc::getMessage('ADD_FILE_AJAX_ERROR_FILE_SIZE');
                break;
            }
        } else {
            $arData['ERROR'] = Loc::getMessage('ADD_FILE_AJAX_ERROR_EXTENSION');
            break;
        }
    }
} else {
    $arData['ERROR'] = Loc::getMessage('ADD_FILE_AJAX_ERROR_EMPTY_DIRECTORY');
}
//endregion
if(empty($arData['ERROR'])) {
    $arData = array(
        'files' => $arFiles,
        'html' => $htmlRow
    );
}

MainHelper::showJson($arData);