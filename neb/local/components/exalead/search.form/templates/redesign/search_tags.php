<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?><!-- Поисковая форма -->

<!-- ТЭГИ ПОИСКА -->
<section class="filter-tags container">

    <?php if(!empty($_REQUEST['category_name'])):?>
        <div data-tag-value="" data-tag-name="category_name" class="filter-tags__item">
            <?=getMessage('SEARCH_CATEGORY')?>: <a href="/category/?category_id=<?=htmlspecialcharsbx($_REQUEST['category_id']);
            ?>&category_name=<?=htmlspecialcharsbx($_REQUEST['category_name']);?>"><?=htmlspecialcharsbx($_REQUEST['category_name'])?></a>
            <a class="filter-tags__remove-link" href="javascript:void(0);" data-checkbox-id="cb4">
                <img src="/local/templates/adaptive/img/filter-close.png" alt="<?=getMessage('RESET_FILTER')?>" />
            </a>
        </div>
    <?php endif;?>

    <?php if ($_REQUEST['publishyear_prev'] != SEARCH_BEGIN_YEAR && $component->validatePrevDate($_REQUEST['publishyear_prev'], $_REQUEST['publishyear_next'])): ?>
        <div class="filter-tags__item">
            <?=getMessage('FROM')?> <?php echo $_REQUEST['publishyear_prev']?> <?=getMessage('YEAR_OF_PUBLICATION')?>
            <a class="filter-tags__remove-link" href="javascript:void(0);" data-range="exsearch-date-from" data-range-init="900">
                <img src="/local/templates/adaptive/img/filter-close.png" alt="<?=getMessage('RESET_FILTER')?>" />
            </a>
        </div>
    <?php endif;?>

    <?php if ($_REQUEST['publishyear_next'] != date("Y") && $component->validateNextDate($_REQUEST['publishyear_prev'], $_REQUEST['publishyear_next'])): ?>
        <div class="filter-tags__item">
            <?=getMessage('TO')?> <?php echo $_REQUEST['publishyear_next']?> <?=getMessage('YEAR_OF_PUBLICATION')?>
            <a class="filter-tags__remove-link" href="javascript:void(0);" data-range="exsearch-date-to" data-range-init="<?php echo date('Y', time()); ?>">
                <img src="/local/templates/adaptive/img/filter-close.png" alt="<?=getMessage('RESET_FILTER')?>" />
            </a>
        </div>
    <?php endif;?>

    <?php foreach($_REQUEST['newcollection'] as $sCollection):?>
        <div class="filter-tags__item">
            <?=getMessage('COLLECTION')?>:
            <? if (LANGUAGE_ID == 'en' && !empty($arResult['newcollection'][$sCollection])): ?>
                <?= htmlspecialcharsbx($arResult['newcollection'][$sCollection])?>
            <? else: ?>
                <?= htmlspecialcharsbx($sCollection)?>
            <? endif; ?>
            <a class="filter-tags__remove-link" href="javascript:void(0);" data-collection-name="<?php echo htmlspecialcharsbx($sCollection)?>">
                <img src="/local/templates/adaptive/img/filter-close.png" alt="<?=getMessage('RESET_FILTER')?>" />
            </a>
        </div>
    <?php endforeach;?>

    <?php if(!empty($_REQUEST['s_strict'])):?>
        <div class="filter-tags__item">
            <?=getMessage('SEARCH_EXACT_PHRASE')?>
            <a class="filter-tags__remove-link" href="javascript:void(0);" data-checkbox-id="cb4">
                <img src="/local/templates/adaptive/img/filter-close.png" alt="<?=getMessage('RESET_FILTER')?>" />
            </a>
        </div>
    <?php endif;?>

    <?php if(!empty($_REQUEST['s_in_results'])):?>
        <div class="filter-tags__item">
            <?=getMessage('SEARCH_WITHIN_RESULTS')?>
            <a class="filter-tags__remove-link" href="javascript:void(0);" data-checkbox-id="cb2">
                <img src="/local/templates/adaptive/img/filter-close.png" alt="<?=getMessage('RESET_FILTER')?>" />
            </a>
        </div>
    <?php endif;?>

    <?php if(!empty($_REQUEST['f_publishyear'])):?>
        <div class="filter-tags__item">
            <?=getMessage('PUBLISHYEAR') .' : '. $_REQUEST['f_publishyear']?>
            <a class="filter-tags__remove-link" href="javascript:void(0);" data-checkbox-id="cb5">
                <img src="/local/templates/adaptive/img/filter-close.png" alt="<?=getMessage('RESET_FILTER')?>" />
            </a>
        </div>
    <?php endif;?>

    <?php if(!empty($_REQUEST['f_field'])):?>
        <?php foreach($_REQUEST['f_field'] as $key => $field):?>
            <?php if(!is_array($field)): $field = array($field); endif;?>

            <?php foreach($field as $fieldValue):?>
                <div data-tag-value="<?php echo htmlspecialcharsbx($fieldValue); ?>" data-tag-name="f_field[<?php echo htmlspecialcharsbx($key); ?>][]" class="filter-tags__item">

                    <?php $filter_text = explode('/', $fieldValue);?>

                    <?php if(!empty($filter_text[1]) && (in_array($filter_text[1] , array('authorbook')))): ?>
                        <?php echo Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AUTHORSBOOK') .' &#062; '.htmlspecialcharsbx($filter_text[2]); ?>
                    <?php else: ?>
                        <?php echo htmlspecialcharsbx(array_pop($filter_text))?>
                    <?php endif; ?>

                    <a href="javascript:void(0);" class="filter-tags__remove-link">
                        <img alt="<?=getMessage('RESET_FILTER')?>" src="/local/templates/adaptive/img/filter-close.png" />
                    </a>
                </div>
             <?php endforeach;?>
        <?php endforeach;?>
    <?php endif;?>

</section>
<!-- /ТЭГИ ПОИСКА -->