// device detector
function testDevice() {
    var _doc_element, _find, _user_agent;
    window.device={};
    _doc_element=window.document.documentElement;
    _user_agent=window.navigator.userAgent.toLowerCase();
    device.ios=function(){return device.iphone() || device.ipod() || device.ipad();};
    device.iphone=function(){return _find('iphone');};
    device.ipod=function(){return _find('ipod');};
    device.ipad=function(){return _find('ipad');};
    device.android=function(){return _find('android');};
    device.androidPhone=function(){return device.android() && _find('mobile');};
    device.androidTablet=function(){return device.android() && !_find('mobile');};
    device.blackberry=function(){return _find('blackberry') || _find('bb10') || _find('rim');};
    device.blackberryPhone=function(){return device.blackberry() && !_find('tablet');};
    device.blackberryTablet=function(){return device.blackberry() && _find('tablet');};
    device.windows=function(){return _find('windows');};
    device.windowsPhone=function(){return device.windows() && _find('phone');};
    device.windowsTablet=function(){return device.windows() && _find('touch');};
    device.fxos=function(){return (_find('(mobile;') || _find('(tablet;')) && _find('; rv:');};
    device.fxosPhone=function(){return device.fxos() && _find('mobile');};
    device.fxosTablet=function(){return device.fxos() && _find('tablet');};
    device.meego=function(){return _find('meego');};
    device.mobile=function(){return device.androidPhone() || device.iphone() || device.ipod() || device.windowsPhone() || device.blackberryPhone() || device.fxosPhone() || device.meego();};
    device.tablet=function(){return device.ipad() || device.androidTablet() || device.blackberryTablet() || device.windowsTablet() || device.fxosTablet();};
    device.portrait=function(){return Math.abs(window.orientation)!==90;};
    device.landscape=function(){return Math.abs(window.orientation)===90;};
    _find = function(needle){return _user_agent.indexOf(needle) !== -1;};
}
testDevice();

(function () {

    var _builPopup = function (popup) {
        popup.popup = $(
            '<div class="modal fade" id="access-closed-popup" >' +
            '</div>'
        );
        popup.popupDialog = $('<div class="modal-dialog" role="document"></div>');
        popup.popupContent = $('<div class="modal-content"></div>');
        popup.popupBody = $('<div class="modal-body"></div>');

        popup.popup.append(popup.popupDialog);
        popup.popupDialog.append(popup.popupContent);
        popup.popupContent.append(popup.popupBody);
    };

    var _openPopup = function (popup, popupMethod, args) {
        $('body').append(popup.popup);
        if ('function' === typeof popup[popupMethod]) {
            popup[popupMethod].apply(popup, args);
        }
        popup.popup.modal();
    };

    var _setHeader = function (popup) {
        popup.popupHeader = $('<div class="modal-header"></div>');
        popup.popupHeader.append(
            $('<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                '<br>'
            )
        );
        popup.popupContent.prepend(popup.popupHeader);
    };
    var _setFooter = function (popup) {
        popup.popupFooter = $('<div class="modal-footer"></div>');
        popup.popupContent.append(popup.popupFooter);
    };

    var _createPopup = function (popupMethod, args) {
        return new ReadPopup(popupMethod, args);
    };

    function ReadPopup(popupMethod, args) {
        _builPopup(this);
        _openPopup(this, popupMethod, args);
    }

    ReadPopup.prototype = $.extend(ReadPopup.prototype, {
        popup: null,
        popupDialog: null,
        popupContent: null,
        popupHeader: null,
        popupBody: null,
        popupFooter: null,
        copyright: function (comment) {
            _setHeader(this);
            if(comment) {
                this.popupBody.append(comment);
            } else {
                this.popupBody.append('<p>07.06.2016 года в ФГБУ «РГБ» поступило письмо от  Некоммерческого  Партнерства «Ассоциация правообладателей, лицензиаров и лицензиатов по защите авторских прав  в интернете», представляющего интересы ООО «Издательство Лань», об удалении произведений доступ к которым предоставляется посредством  интернет-портала «Национальная электронная библиотека» (нэб.рф)</p>' +
                    '<p>В данном письме содержится перечень  литературных произведений, которые, по мнению НП «АЗАПИ», неправомерно используются интернет-порталом НЭБ.</p>' +
                    '<p>ФГБУ «РГБ», являясь оператором «Национальной электронной библиотеки»,  приостановило  доступ пользователей к произведениям, указанным в письме, до решения вопроса о правомерности их использования.</p>');
            }
        },
        ticketRequired: function () {
            _setHeader(this);
            _setFooter(this);
            this.popupBody.append('<p class="text-info">Для чтения закрытой книги из Электронного читального зала требуется внести в систему НЭБ.РФ ' +
                'актуальный электронный читательский билет. Обратитесь пожалуйста к администратору зала.</p>');
        },
        desktopApp: function () {
            _setHeader(this);
            this.popupBody.append('<p>Издание будет открыто во внешнем приложении через некоторое время.</p>' +
                '<p>Если приложение не запустилось в автоматическом режиме, убедитесь, что <a href="/viewers/" target="_blank">приложение «НЭБ РФ»</a> установлено или запустите в ручном режиме.</p>');
        },
        protocolError: function () {
            _setHeader(this);
            _setFooter(this);
            var link,downtext;
            if ( window.navigator.platform.indexOf("Win") != -1 ) {
                link = '<a href="/upload/distribs/nebviewer-windows.exe" target="_blank">файл установки для Windows</a>'
            } else if ( window.navigator.platform.indexOf("Mac") != -1 ) {
                link = '<a href="/upload/distribs/nebviewer-macos-x86_64.dmg" target="_blank">файл установки для Mac</a>'
            } else if ( window.navigator.platform.indexOf("Linux i686") != -1 ) {
                link = '<a href="/upload/distribs/nebviewer-linux-i686.run" target="_blank">файл установки для 32-битных Linux</a>'
            } else if ( window.navigator.platform.indexOf("Linux x86_64") != -1 ) {
                link = '<a href="/upload/distribs/nebviewer-linux-x86_64.run" target="_blank">файл установки для 64-битных Linux</a>'
            }
            if (link) {
                downtext = '<p>Скачать ' + link + '.</p>'
            }
            this.popupBody.append('<p>Требуется установка приложения для просмотра изданий, охраняемых авторским правом. Файл установки можно скачать в разделе <a href="/viewers/" target="_blank">программное обеспечение</a>.</p>' + downtext);
        },
        notFound: function () {
            _setHeader(this);
            this.popupBody.append('<p>Сервис библиотеки в настоящее время не доступен, повторите попытку позже. Приносим свои извинения за доставленные неудобства.</p>');
        }
    });

    window.readPopup = _createPopup;
})();
function readBook(event, element) {
    $('.boockard-error-msg').hide();
    $('.read-button-text').hide();
    $('.read-button-preloader').show();
    var data = {};
    var options = $(element).attr('data-options');
    var typeSearch = {
        strict: $(element).data('strict-search'),
        full: $(element).data('full-search')
    };
    var isBlind = element.hasAttribute('data-reader-blind');
    try {
        options = $.parseJSON(options);
    } catch (e) {
        options = {};
    }
    options.id = $(element).attr('data-link');
    options.token = '';
    options.appUrl = '';

    var getAction = function () {
        var action = null;

        // ошибка -> вывод окна
        if ('error' == data.result) {
            action = 'error';
        }
        // полностью закрытая копирайтом книга -> попап с запрещающим текстом
        else if ('copyright' == data.result) {
            action = 'copyright';
        }
        // открытое издание и не Android и не iOS -> открытый просмотровщик
        else if ('open' === data.result && !device.ios() && !device.android()) {
            action = 'webView';
        }
        // закрытое издание и доступ из ВЧЗ и отсутствует ЧБ  и есть токен у пользователя -> сообщение с требованием ЧБ
        else if ('open' !== data.result && true === data.isWorkplace
            && data.ticket === 'required' && data.token !== null && data.token.length !== 0) {
            action = 'ticketRequired';
        }
        // закрытое издание и нахождение пользователя в зале РГБ -> редирект на РГБ
        else if ('open' !== data.result && data.hasOwnProperty('redirectUrl')) {
            action = 'redirectUrl';
        }
        // закрытое издание и доступ из ВЧЗ и есть токен у пользователя -> десктопный просмотр
        else if ('open' !== data.result) {
            action = 'desktopApp';
        }
        // мобильное устройство: iOS
        else if (device.ios()) {
            action = 'iosApp';
        }
        // мобильное устройство: Android
        else if (device.android()) {
            action = 'andriodApp';
        }
        else {
            action = 'error';
        }
        return action;
    };


    var action = function () {
        var autoRegForm =  $('.popup_autoreg_form');
        var closeLink = autoRegForm.find('.closed-link');
        var action = getAction();
        var queryParams = {};
        var hideAutoregViewerLink = function() {
            $('.popup_autoreg_form p.get-viewer').hide();
        };
        var uri;

        if (options.hasOwnProperty('page')) {
            queryParams.page = options.page;
        }
        switch (action) {
            case 'webView':
                if (typeSearch.full == 1 || typeSearch.strict == 1){
                    queryParams.t_search = 1;
                }
                if (options.hasOwnProperty('positionpart')) {
                    queryParams.positionpart = options.positionpart;
                }
                if (isBlind) {
                    queryParams.isBlind = 'true'
                }
                queryParams = $.param(queryParams);
                if (queryParams) {
                    queryParams = '?' + queryParams;
                }
                uri = '/catalog/' + options.id + '/viewer/' + queryParams;
                // window.open('/catalog/' + options.id + '/viewer/' + queryParams);
                window.open(uri);
                break;
            case 'ticketRequired':
                window.readPopup('ticketRequired');
                break;
            case 'copyright':
                window.readPopup('copyright', [data.accesscomment]);
                break;
            case 'iosApp':
                options.appUrl = 'http://itunes.apple.com/app/id944682831';
                startViewerApp(options.id, options.token, options);
                break;
            case 'andriodApp':
                options.appUrl = 'https://play.google.com/store/apps/details?id=ru.elar.neb.viewer';
                startViewerApp(options.id, options.token, options);
                break;
            case 'redirectUrl':
                if (data.hasOwnProperty('redirectUrl')) {
                    window.open(data.redirectUrl);
                }
                break;
            case 'desktopApp':

                queryParams = $.param(queryParams);
                if (queryParams)
                    queryParams = '&' + queryParams;
                var protocolLink = 'spd:https://выдача.нэб.рф/?id=' + options.id + '&token=' + options.token + queryParams;

                // todo Приложение "Просмотровщиr" пока плохо работает с параметром q в запросое на открытие онного
                /*var sQuery = window.location.search.match(new RegExp('q=([^&=]+)'));
                sQuery = sQuery ? sQuery[1] : '';
                if (sQuery != undefined && sQuery != '') {
                    protocolLink += '&q=' + encodeURI(sQuery);
                }*/

                window.protocolCheck(
                    protocolLink,
                    function () { window.readPopup('protocolError'); },
                    function () {
                        window.readPopup('desktopApp');
                        window.open(protocolLink, '_self');
                        setTimeout(function () {
                            $('button.close').click();
                        }, 7000);
                    },
                    function () {
                        window.readPopup('desktopApp');
                        // window.open(protocolLink, '_self');
                        setTimeout(function () {
                            $('button.close').click();
                        }, 7000);
                    }
                );

                break;
            default:
                window.readPopup('notFound');
                break;
        }
    };

    $.ajax({
        url: '/local/tools/access_closed_books.php',
        async: false,
        dataType: 'json',
        data: {book_id: options.id},
        success: function (result) {
            data = result;

            if (data.hasOwnProperty('token')) {
                options.token = data.token;
            }
            options.isOpen = 'open' === data.result;
            options.status = 'verified' === data.status ? 1 : 0;

            action();

            $('.boockard-read-button').attr('data-load','load');
            $('.read-button-preloader').hide();
            $('.read-button-text').show();
        },
        error: function () {
            $('.boockard-read-button').attr('data-load','load');
            $('.read-button-preloader').hide();
            $('.read-button-text').show();
            $('.boockard-error-msg').text('Произошла ошибка, повторите попытку').show();
        }
    });
}

/**
 * @param id
 * @param token
 * @param options
 */
function startViewerApp(id, token, options) {
    options = options || {};
    if (!token) {
        token = '';
    }
    var params = [
        'book_id=' + id,
        'token=' + token,
    ];
    if (options.hasOwnProperty('author')) {
        params.push('author=' + options.author);
    }
    if (options.hasOwnProperty('title')) {
        params.push('title=' + options.title);
    }
    if (options.hasOwnProperty('isOpen')) {
        params.push('is_open=' + (options.isOpen ? '1' : '0'));
    }
    if (options.hasOwnProperty('status')) {
        params.push('status=' + options.status);
    }
    var time = (new Date()).getTime();
    if (device.android() && navigator.userAgent.match(/Chrome/)) {
        window.open('intent://?' + params.join('&') + '#Intent;scheme=viewer;package=ru.elar.neb.viewer;end');
    } else {
        var to = setTimeout(function () {
            if (confirm('Приложение не установлено на вашем устройстве, вы хотите установить его?')) {
                document.location = options.appUrl;
            }
        }, 7000);
        $(window).one('blur', function() {
            clearTimeout(to);
        });
        location.href = 'viewer://?' + params.join('&');
    }
}
