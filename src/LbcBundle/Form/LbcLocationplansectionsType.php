<?php

namespace LbcBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LbcLocationplansectionsType extends AbstractLbcCategoryType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('locationplanid')
            ->add('code');
    }

    public function getEntityName()
    {
        return 'LbcBundle\Entity\LbcLocationplansections';
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'lbcbundle_lbclocationplansections';
    }
}
