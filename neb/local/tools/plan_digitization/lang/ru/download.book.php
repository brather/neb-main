<?php
$MESS['DOWNLOAD_BOOK_NOT_AUTHORIZED'] = 'Пожалуйста авторизуйтесь для доступа к файлу';
$MESS['DOWNLOAD_BOOK_NOT_PERMISSION'] = 'У Вас не хватает прав доступа для получения файла';
$MESS['DOWNLOAD_BOOK_NOT_PATH'] = 'Не указан путь к скачиванию файла';