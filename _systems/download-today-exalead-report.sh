function startDownload {
	serverAddress=$1
	currentDate=$2
	remotePath="/home/neb/Exalead/neb11000/run/searchserver-ss$3/search-reporting/"
	ssh neb@$serverAddress find $remotePath -type f | while read fileName
	do
	fileName=$(basename $fileName)
	fileDate=$(echo $fileName | sed 's/.*\([0-9]\{8\}\)\.[0-9]\{5\}.*/\1/');
	if [ $currentDate == $fileDate ]
    	    then scp neb@$serverAddress:$remotePath$fileName ~/www/tmp/exalead-search-logs/ss$3_$fileName
	fi
	done
}

startDownload 10.250.98.118 $(date -d "yesterday" +%Y%m%d) 1

startDownload 10.250.98.119 $(date -d "yesterday" +%Y%m%d) 2

startDownload 10.250.98.120 $(date -d "yesterday" +%Y%m%d) 0
