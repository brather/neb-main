<?php

CBitrixComponent::includeComponentClass("rest:rest.server");

use \Bitrix\Main\UserTable,
    \Bitrix\Main\Loader;

/**
 * Class RestBlock
 */
class RestBlock extends RestServer
{
	protected $token, $action, $ip, $email, $fingerprint;
    protected $arAccessGroups = ['admin', 'operator'];

	public function processRequest($token = null, $action = null, $ip = null, $email = null, $fingerprint = null)
	{
        $this->token       = !empty($token)       ? trim($token)  : false;
        $this->action      = !empty($action)      ? trim($action) : false;
        $this->ip          = !empty($ip)          ? trim($ip)     : false;
        $this->email       = !empty($email)       ? trim($email)  : false;
        $this->fingerprint = !empty($fingerprint) ? trim($fingerprint)  : false;

        $this->checkUser();

        switch ($this->action) {
            case 'blockechz':
                $this->blockWorkplace();
                break;
            case 'blockuser':
                $this->blockUser();
                break;
            case 'blockfp':
                $this->blockMaсhine();
                break;
            default:
                $this->restError(403, 'Parameter "action" incorrect!');
        }
	}

    /**
     * Проверка токена пользователя и принадлежность его к группам администраторов или операторов
     *
     * @return bool
     */
    protected function checkUser() {

        if (!$this->token || strlen($this->token) <= 0)
            return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));

        $arUser = UserTable::getList([
            'filter' => ['ACTIVE' => 'Y', 'UF_TOKEN' => $this->token],
            'select' => ['ID']
        ])->fetch();

        if (empty($arUser))
            $this->restError(403, GetMessage('USER_NOT_FOUND'));

        $obUser   = new nebUser($arUser['ID']);
        $arGroups = $obUser->getUserGroups();
        $arGroups = array_intersect($arGroups, $this->arAccessGroups);

        if (empty($arGroups))
            $this->restError(403, 'Role access error!');
    }

    /**
     * Блокировка ЭЧЗ по IP-адресу
     * 
     * @return int
     * @throws Exception
     * @throws \Bitrix\Main\LoaderException
     */
    protected function blockWorkplace() {

        if (empty($this->ip))
            throw new Exception('Parameter "ip" is not exist!', 403);

        $arWorkPlaces = NebWorkplaces::getWorkplacesByIp($this->ip, null);

        if (empty($arWorkPlaces))
            $this->restError(404, 'Workplace not found!');

        if (!Loader::includeModule('iblock'))
            throw new Exception('Module "iblock" not installed', 500);

        $obIBlockElement = new \CIBlockElement();

        $this->arResult['result'] = false;
        $iErrorCode = $iErrorMess = null;

        foreach ($arWorkPlaces as $arWorkPlace) {
            
            // деактивировать
            if ($arWorkPlace['ACTIVE'] == 'Y') {
                $bRes = $obIBlockElement->Update($arWorkPlace['ID'], ['ACTIVE' => 'N']);
                if ($bRes) {
                    $this->arResult['result'] = true;
                } else {
                    $iErrorCode = 500;
                    $iErrorMess = $obIBlockElement->LAST_ERROR;
                }
            }
            // вернуть true, т.к. уже деактивирован
            else {
                $this->arResult['result'] = true;
            }
        }

        if (!$this->arResult['result'] && $iErrorCode > 0 && !empty($iErrorMess)) {
            $this->restError($iErrorCode, $iErrorMess);
        }

        return 200;
    }

    /**
     * Блокировка пользователя по e-mail адресу.
     *
     * @return int
     * @throws Exception
     * @throws \Bitrix\Main\ArgumentException
     */
    protected function blockUser() {

        if (empty($this->email))
            throw new Exception('Parameter "email" is not exist!', 403);

        $this->arResult['result'] = false;

        // выборка пользователей по email или логину (могут отличаться в редких случаях)
        $rsUsers = UserTable::getList([
            'filter' => [["LOGIC" => "OR", [['LOGIN' => $this->email]], [['EMAIL' => $this->email]]]],
            'select' => ['ID', 'ACTIVE']
        ]);

        if ($rsUsers->getSelectedRowsCount() < 1)
            $this->restError(404, 'User not found!');

        while ($arUser = $rsUsers->fetch()) {
            if ($arUser['ACTIVE'] == 'Y') {
                $obUser = new CUser();
                if ($obUser->Update($arUser['ID'], ['ACTIVE' => 'N']))
                    $this->arResult['result'] = true;
                else
                    $this->restError(500, $obUser->LAST_ERROR);
            } else {
                $this->arResult['result'] = true;
            }
        }

        return 200;
    }

    /**
     * Блокирование машины по ее fingerprint
     * 
     * @throws Exception
     * @throws \Bitrix\Main\LoaderException
     */
    protected function blockMaсhine() {

        if (empty($this->fingerprint))
            throw new Exception('Parameter "fingerprint" is not exist!', 403);

        if (!Loader::includeModule("iblock"))
            throw new Exception('Module "iblock" not installed', 500);

        $iErrorCode = $iErrorMess = null;

        $obIBlockElement = new \CIBlockElement();

        $rsMachines = $obIBlockElement->GetList(
            [],
            [
                'IBLOCK_CODE'          => IBLOCK_CODE_TRUSTED_MACHINES,
                'PROPERTY_FINGERPRINT' => $this->fingerprint
            ],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'ACTIVE']
        );
        while ($arMachine = $rsMachines->Fetch()) {
            if ($arMachine['ID'] && $arMachine['ACTIVE'] == 'Y') {
                if ($obIBlockElement->Update($arMachine['ID'], ['ACTIVE' => 'N'])) {
                    $this->arResult['result'] = true;
                } else {
                    $iErrorCode = 500;
                    $iErrorMess = $obIBlockElement->LAST_ERROR;
                }
            } elseif ($arMachine['ID'] && $arMachine['ACTIVE'] == 'N') {
                $iErrorCode = 406;
                $iErrorMess = 'Trusted machine was deactivated!';
            } else {
                $iErrorCode = 404;
                $iErrorMess = 'Trusted machine not found!';
            }
        }

        if (!$this->arResult['result'] && $iErrorCode > 0 && !empty($iErrorMess)) {
            $this->restError($iErrorCode, $iErrorMess);
        }

        return 200;
    }
}