<?php
/**
 * User: agolodkov
 * Date: 05.10.2016
 * Time: 16:03
 */

namespace ElarUtilsBundle\DependencyInjection;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\RouterHelper;

/**
 * Class MustacheEngine
 *
 */
class MustacheHelper extends \Mustache_Engine
{
    /**
     * @var RouterHelper
     */
    private $_router;

    /**
     * @var string
     */
    private $_path;


    /**
     * @param string       $path
     * @param RouterHelper $routerHelper
     * @param array        $options
     */
    public function __construct($path, RouterHelper $routerHelper, array $options = array())
    {
        $this->_path = $path;

        $this->_router = $routerHelper;
        if (!isset($options['loader'])) {
            $options['loader'] = new \Mustache_Loader_FilesystemLoader($path);
        }
        parent::__construct($options);
    }

    /**
     * @param string $template
     * @param array  $context
     *
     * @return string
     */
    public function render($template, $context = [])
    {
        $router = $this->_router;
        $context['router'] = function ($data, \Mustache_LambdaHelper $helper) use ($router) {
            $data = $helper->render($data);
            $data = json_decode(trim($data), true);

            return call_user_func_array([$router, 'generate'], $data);
        };

        return parent::render($template, $context);
    }

    /**
     * @param string $path
     *
     * @return $this
     */
    public function setPartial($path)
    {
        $this->setPartialsLoader(new \Mustache_Loader_FilesystemLoader($this->_path . '/' . $path));

        return $this;
    }
}