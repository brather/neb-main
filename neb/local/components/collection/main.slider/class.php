<?php

use \Bitrix\Main\Loader;

/**
 * Class MainSliderComponent
 * Refactored by EfremovDm
 */
class MainSliderComponent extends CBitrixComponent
{
    private $atTreeList = [];
    private $arCollections = [];
    private $arAccess = [];

    /**
     * Подготовка параметров компонента.
     *
     * @param $arParams
     * @return mixed
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams['CACHE_TIME'] = intval($arParams['CACHE_TIME']);
        $arParams['LIBRARY_ID'] = intval($arParams['LIBRARY_ID']);
        $arParams['IBLOCK_ID']  = intval($arParams['IBLOCK_ID']);

        return $arParams;
    }

    /**
     * Исполнение компонента. Общий алгоритм:
     * 1. Выбираем все корневые коллекции, отмеченные для вывода на главной странице.
     * 2. Отдельным запросом выбираем элементы этих коллекций для проверку на пустые коллекции и неактивные элементы
     * 3. Формируем результат.
     */
    public function executeComponent()
    {
        global $CACHE_MANAGER;

        if ($this->startResultCache(false)) {

            $CACHE_MANAGER->RegisterTag('IBLOCK_ID_' . $this->arParams['IBLOCK_ID']);
            //$CACHE_MANAGER->RegisterTag('collection_main_slider');

            $this->getCollections();
            $this->getNotEmptyCollections();
            $this->getResult();

            $this->endResultCache();
        }

        $this->includeComponentTemplate();
    }

    /**
     * 1. Фомрирование массива сопоставления всех разделов коллекции с вершинами коллекции
     * 2. Формирование предварительного списка коллекций без учета активности вложенных элементов и их количества
     */
    private function getCollections() {

        $iLevelOne = 0;

        $arCollections = [];

        if (!Loader::includeModule('iblock'))
            return $arCollections;

        $rsSection = \CIBlockSection::GetList(
            ['LEFT_MARGIN' => 'ASC'],
            ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE' => 'Y'],
            false,
            ['ID', 'SORT', 'NAME', 'LEFT_MARGIN', 'DEPTH_LEVEL',
                'DETAIL_PICTURE', 'SECTION_PAGE_URL', 'UF_SLIDER', 'UF_SHOW_NAME', 'UF_BG_POSITION']
        );
        while ($arSection = $rsSection->GetNext(true, false))
        {
            // построение дерева сопоставления листьев дерева с вершинами
            if (1 == $arSection['DEPTH_LEVEL'])
                $iLevelOne = $arSection['ID'];
            $this->atTreeList[$arSection['ID']] = $iLevelOne;

            // формирование предварительного результата подоборок
            if (1 == $arSection['DEPTH_LEVEL'] && 1 == $arSection['UF_SLIDER']) {
                $arCollections[$arSection['ID']] = [
                    'ID'               => $arSection['ID'],
                    'NAME'             => $arSection['NAME'],
                    'SORT'             => $arSection['SORT'],
                    'SHOW_NAME'        => $arSection['UF_SHOW_NAME'],
                    'SECTION_PAGE_URL' => $arSection['SECTION_PAGE_URL'],
                    'DETAIL_PICTURE'   => $arSection['DETAIL_PICTURE'],
                    'POSITION'         => $arSection['UF_BG_POSITION'] ? : 'center center',
                ];
            }
        }
        $this->arCollections = $arCollections;
    }

    /**
     * Получение массива идентификаторов коллекций, в которых имеются аткивные книги
     */
    private function getNotEmptyCollections()
    {
        $arAccess = [];

        $arSections = array_keys($this->arCollections);

        if (empty($arSections) || !Loader::includeModule('iblock'))
            return $arAccess;

        $obCIBlockElement = new \CIBlockElement();
        $rsElements = $obCIBlockElement->GetList(
            [],
            [
                'IBLOCK_ID'           => $this->arParams['IBLOCK_ID'],
                'SECTION_ID'   => $arSections,
                'INCLUDE_SUBSECTIONS' => 'Y',
                'ACTIVE'              => 'Y'
            ],
            ['IBLOCK_SECTION_ID']
        );
        while($arElement = $rsElements->Fetch())
        {
            if (!$arElement['IBLOCK_SECTION_ID']) continue;

            $iSection = $this->atTreeList[$arElement['IBLOCK_SECTION_ID']];
            if ($iSection > 0) {
                $arAccess[$iSection] = $iSection;
            }
        }

        $this->arAccess = $arAccess;
    }

    /**
     * Формирование итогового массива
     * @return array
     */
    private function getResult() {

        $arResult = [];

        foreach ($this->arAccess as $iSection)
            if (!empty($this->arCollections[$iSection]))
                $arResult[$iSection] = $this->arCollections[$iSection];

        // сортировка результата по индексу сотрировки
        usort($arResult, function($a, $b) {
            if ($a['SORT'] == $b['SORT']) {
                return 0;
            }
            return ($a['SORT'] < $b['SORT']) ? -1 : 1;
        });

        return $this->arResult['ITEMS'] = $arResult;
    }
}