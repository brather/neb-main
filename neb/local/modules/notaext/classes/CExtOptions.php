<?
class CExtOptions
{
	public $arCurOptionValues = array();
	
	private $module_id 			= ''; 		// ID модуля
	private $ext_id 			= ''; 		// ID расширения
	private $arTabs 			= array(); 	// Массив табов
	private $arGroups 			= array(); 	// Массив групп
	private $arOptions 			= array(); 	// Массив опций
	private $defaultSort 		= 9999; 	// Если нет сортировки - ты в коце! )
	private $tabControl; 	
	
	public function CExtOptions($module_id, $ext_id, $arTabs = array(), $arGroups = array(), $arOptions)
	{
		$this->module_id 		= $module_id;
		$this->ext_id 			= $ext_id;
		$this->arTabs 			= $arTabs;
		$this->arGroups 		= $arGroups;
		$this->arOptions 		= $arOptions;
		
		// Если кто-то вдруг забыл добавить хотя бы один таб
		if ( empty($this->arTabs) )
		{
			$this->arTabs[] = array(
				'DIV' => 'default_options_tab',
				'TAB' => 'Настройки расширения',
				'ICON' => '',
				'TITLE'=> 'Настройки расширения',
			);
		}
		
		$this->tabControl = new CAdminTabControl('tabControl', $this->arTabs);
		
		if($_REQUEST['update'] == 'Y' && check_bitrix_sessid()){
			global $APPLICATION;
			$this->SaveOptions();
			LocalRedirect($APPLICATION->GetCurPageParam($this->tabControl->ActiveTabParam(),array('tabControl_active_tab')));
		}
			
		
		$this->GetCurOptionValues();
	}
	
	private function SaveOptions()
	{
		foreach($this->arOptions as $opt => $arOptParams)
		{
			if($arOptParams['TYPE'] != 'CUSTOM')
			{
				$val = $_REQUEST[$opt];
	
				if($arOptParams['TYPE'] == 'CHECKBOX' && $val != 'Y')
					$val = 'N';
				elseif(is_array($val))
					$val = serialize($val);

				COption::SetOptionString($this->module_id, $opt, $val);
			}
		}
		global $CACHE_MANAGER;
		
		$CACHE_MANAGER->ClearByTag($this->ext_id);
	}   
	
	private function GetCurOptionValues()
	{
		foreach($this->arOptions as $opt => $arOptParams)
		{
			if($arOptParams['TYPE'] != 'CUSTOM')
			{
				$this->arCurOptionValues[$opt] = COption::GetOptionString($this->module_id, $opt, $arOptParams['DEFAULT']);
				if(in_array($arOptParams['TYPE'], array('MSELECT')))
					$this->arCurOptionValues[$opt] = unserialize($this->arCurOptionValues[$opt]);
			}
		}
	}
	
	public function ShowHTML()
	{
		global $APPLICATION;

		$arP = array();
		
		foreach ( $this->arTabs as $key => $arTab )
		{
			// Для опций без групп нам нужна такая структура
			$arP[$key] = array(
				'OPTIONS' 		=> array(),
				'GROUPS' 		=> array(),
				'ENTITY_SORT' 	=> array()
			);
		}
		
		foreach ( $this->arGroups as $group_id => &$group_params )
		{
			if ( !isset($group_params['TAB']) )
				$group_params['TAB'] = 0;
				
			if ( !isset($group_params['SORT']) )
				$group_params['SORT'] = $this->defaultSort;
				
			$arP[$group_params['TAB']]['GROUPS'][$group_id] = array();
			$arP[$group_params['TAB']]['ENTITY_SORT']["GROUPS|{$group_id}"] = $group_params['SORT'];
			
		} unset($group_params);
		
		if(is_array($this->arOptions))
		{
			foreach($this->arOptions as $option => $arOptParams)
			{
				$val = $this->arCurOptionValues[$option];

				if($arOptParams['SORT'] < 0 || !isset($arOptParams['SORT']))
					$arOptParams['SORT'] = 0;
				
				$label = (isset($arOptParams['TITLE']) && $arOptParams['TITLE'] != '') ? $arOptParams['TITLE'] : '';
				$opt = htmlspecialchars($option);

				switch($arOptParams['TYPE'])
				{
					case 'CHECKBOX':
						$input = '<input type="checkbox" name="'.$opt.'" id="'.$opt.'" value="Y"'.($val == 'Y' ? ' checked' : '').' '.($arOptParams['REFRESH'] == 'Y' ? 'onclick="document.forms[\''.$this->module_id.'_'.$this->ext_id.'\'].submit();"' : '').' />';
						break;
					case 'TEXT':
						if(!isset($arOptParams['COLS']))
							$arOptParams['COLS'] = 25;
						if(!isset($arOptParams['ROWS']))
							$arOptParams['ROWS'] = 5;
						$input = '<textarea rows="'.$type[1].'" cols="'.$arOptParams['COLS'].'" rows="'.$arOptParams['ROWS'].'" name="'.$opt.'">'.htmlspecialchars($val).'</textarea>';
						if($arOptParams['REFRESH'] == 'Y')
							$input .= '<input type="submit" name="refresh" value="OK" />';
						break;
					case 'SELECT':
						$input = SelectBoxFromArray($opt, $arOptParams['VALUES'], $val, '', '', ($arOptParams['REFRESH'] == 'Y' ? true : false), ($arOptParams['REFRESH'] == 'Y' ? $this->module_id.'_'.$this->ext_id : ''));
						if($arOptParams['REFRESH'] == 'Y')
							$input .= '<input type="submit" name="refresh" value="OK" />';
						break;
					case 'MSELECT':
						$input = SelectBoxMFromArray($opt.'[]', $arOptParams['VALUES'], $val, '', false, $arOptParams['SIZE'], $arOptParams['ADD_TO_TAG']);
						if($arOptParams['REFRESH'] == 'Y')
							$input .= '<input type="submit" name="refresh" value="OK" />';
						break;
					case 'COLORPICKER':
						if(!isset($arOptParams['FIELD_SIZE']))
							$arOptParams['FIELD_SIZE'] = 25;
						ob_start();
						echo     '<input id="__CP_PARAM_'.$opt.'" name="'.$opt.'" size="'.$arOptParams['FIELD_SIZE'].'" value="'.htmlspecialchars($val).'" type="text" style="float: left;" '.($arOptParams['FIELD_READONLY'] == 'Y' ? 'readonly' : '').' />
								<script>
									function onSelect_'.$opt.'(color, objColorPicker)
									{
										var oInput = BX("__CP_PARAM_'.$opt.'");
										oInput.value = color;
									}
								</script>';
						$APPLICATION->IncludeComponent('bitrix:main.colorpicker', '', Array(
								'SHOW_BUTTON' => 'Y',
								'ID' => $opt,
								'NAME' => 'Выбор цвета',
								'ONSELECT' => 'onSelect_'.$opt
							), false
						);
						$input = ob_get_clean();
						if($arOptParams['REFRESH'] == 'Y')
							$input .= '<input type="submit" name="refresh" value="OK" />';
						break;
					case 'FILE':
						if(!isset($arOptParams['FIELD_SIZE']))
							$arOptParams['FIELD_SIZE'] = 25;
						if(!isset($arOptParams['BUTTON_TEXT']))
							$arOptParams['BUTTON_TEXT'] = '...';
						CAdminFileDialog::ShowScript(Array(
							'event' => 'BX_FD_'.$opt,
							'arResultDest' => Array('FUNCTION_NAME' => 'BX_FD_ONRESULT_'.$opt),
							'arPath' => Array(),
							'select' => 'F',
							'operation' => 'O',
							'showUploadTab' => true,
							'showAddToMenuTab' => false,
							'fileFilter' => '',
							'allowAllFiles' => true,
							'SaveConfig' => true
						));
						$input =     '<input id="__FD_PARAM_'.$opt.'" name="'.$opt.'" size="'.$arOptParams['FIELD_SIZE'].'" value="'.htmlspecialchars($val).'" type="text" style="float: left;" '.($arOptParams['FIELD_READONLY'] == 'Y' ? 'readonly' : '').' />
									<input value="'.$arOptParams['BUTTON_TEXT'].'" type="button" onclick="window.BX_FD_'.$opt.'();" />
									<script>
										setTimeout(function(){
											if (BX("bx_fd_input_'.strtolower($opt).'"))
												BX("bx_fd_input_'.strtolower($opt).'").onclick = window.BX_FD_'.$opt.';
										}, 200);
										window.BX_FD_ONRESULT_'.$opt.' = function(filename, filepath)
										{
											var oInput = BX("__FD_PARAM_'.$opt.'");
											if (typeof filename == "object")
												oInput.value = filename.src;
											else
												oInput.value = (filepath + "/" + filename).replace(/\/\//ig, \'/\');
										}
									</script>';
						if($arOptParams['REFRESH'] == 'Y')
							$input .= '<input type="submit" name="refresh" value="OK" />';
						break;
					case 'CUSTOM':
						$input = $arOptParams['VALUE'];
						break;
					default:
						if(!isset($arOptParams['SIZE']))
							$arOptParams['SIZE'] = 25;
						if(!isset($arOptParams['MAXLENGTH']))
							$arOptParams['MAXLENGTH'] = 255;
						$input = '<input type="'.($arOptParams['TYPE'] == 'INT' ? 'number' : 'text').'" size="'.$arOptParams['SIZE'].'" maxlength="'.$arOptParams['MAXLENGTH'].'" value="'.htmlspecialchars($val).'" name="'.htmlspecialchars($option).'" />';
						if($arOptParams['REFRESH'] == 'Y')
							$input .= '<input type="submit" name="refresh" value="OK" />';
						break;
				}

				if(isset($arOptParams['NOTES']) && $arOptParams['NOTES'] != '')
					$input .=     '<div class="notes">
									<table cellspacing="0" cellpadding="0" border="0" class="notes">
										<tbody>
											<tr class="top">
												<td class="left"><div class="empty"></div></td>
												<td><div class="empty"></div></td>
												<td class="right"><div class="empty"></div></td>
											</tr>
											<tr>
												<td class="left"><div class="empty"></div></td>
												<td class="content">
													'.$arOptParams['NOTES'].'
												</td>
												<td class="right"><div class="empty"></div></td>
											</tr>
											<tr class="bottom">
												<td class="left"><div class="empty"></div></td>
												<td><div class="empty"></div></td>
												<td class="right"><div class="empty"></div></td>
											</tr>
										</tbody>
									</table>
								</div>';
				
				// Проверяем, если опция принадлежит к какой-либо группе - добавляем её в группу
				if ( isset( $arOptParams['GROUP'] ) )
				{
					$arP[$this->arGroups[$arOptParams['GROUP']]['TAB']]['GROUPS'][$arOptParams['GROUP']]['OPTIONS'][] = $label != '' ? '<tr><td valign="top" width="40%">'.$label.'</td><td valign="top" nowrap>'.$input.'</td></tr>' : '<tr><td valign="top" colspan="2" align="center">'.$input.'</td></tr>';
					$arP[$this->arGroups[$arOptParams['GROUP']]['TAB']]['GROUPS'][$arOptParams['GROUP']]['OPTIONS_SORT'][] = $arOptParams['SORT'];
				}
				// Если нет группы, но есть таб - добавляем опцию в таб, так же добавляем сортировку поля
				elseif ( isset( $arOptParams['TAB'] ) )
				{
					$arP[$arOptParams['TAB']]['OPTIONS'][$option] = $label != '' ? '<tr><td valign="top" width="40%">'.$label.'</td><td valign="top" nowrap>'.$input.'</td></tr>' : '<tr><td valign="top" colspan="2" align="center">'.$input.'</td></tr>';
					$arP[$arOptParams['TAB']]['ENTITY_SORT']["OPTIONS|{$option}"] = isset($arOptParams['SORT']) ? $arOptParams['SORT'] : $this->defaultSort;
				}
				// Крайний вариант, когда у опции не указана ни группа, ни таб - добавляем в первый таб опцию
				else
				{
					$tabKey = 0;
					
					$arP[$tabKey]['OPTIONS'][$option] = $label != '' ? '<tr><td valign="top" width="40%">'.$label.'</td><td valign="top" nowrap>'.$input.'</td></tr>' : '<tr><td valign="top" colspan="2" align="center">'.$input.'</td></tr>';
					$arP[$tabKey]['ENTITY_SORT']["OPTIONS|{$option}"] = isset($arOptParams['SORT']) ? $arOptParams['SORT'] : $this->defaultSort;
				}
			}

			//$tabControl = new CAdminTabControl('tabControl', $this->arTabs);
			$tabControl = $this->tabControl;
			$tabControl->Begin();
			echo '<form name="'.$this->module_id.'_'.$this->ext_id.'" method="POST" action="'.$APPLICATION->GetCurPage().'?mid='.$this->module_id.'&ext_id='.$this->ext_id.'&lang='.LANGUAGE_ID.'" enctype="multipart/form-data">'.bitrix_sessid_post();
			
			foreach ( $arP as $tab => $tabItem )
			{
				$tabControl->BeginNextTab();
				if ( empty ( $tabItem['ENTITY_SORT'] ) )
					continue;
				asort($tabItem['ENTITY_SORT']);
				
				foreach ( $tabItem['ENTITY_SORT'] as $entityCode => $sort )
				{
					// Получаем тип и код сущности
					$entityInfo = explode('|', $entityCode);
					
					$entityType = $entityInfo[0];
					$entityCode = $entityInfo[1];
					
					switch ( $entityType )
					{
						case 'OPTIONS':
							echo $tabItem[$entityType][$entityCode];
							break;
						case 'GROUPS':
							$group = $tabItem[$entityType][$entityCode];
						
							if(sizeof($group['OPTIONS_SORT']) > 0)
							{
								echo '<tr class="heading"><td colspan="2">'.$this->arGroups[$entityCode]['TITLE'].'</td></tr>';
								
								array_multisort($group['OPTIONS_SORT'], $group['OPTIONS']);
								foreach($group['OPTIONS'] as $opt)
									echo $opt;
							}
							break;
					}
				}
			}
			
			$tabControl->Buttons();
			
			echo     '<input type="hidden" name="update" value="Y" />
					<input type="submit" name="save" value="Сохранить" />
					<input type="reset" name="reset" value="Отменить" />
					</form>';

			$tabControl->End();
		}
	}
	
	/*
	* Подготавливает ассоциативный массив key=>value для селектора
	*
	* @param array $array
	* @return array
	*/
	public function prepareArrayForSelect(array $array)
	{
		$resultArray = array();
		foreach ( $array as $key => $value )
		{
			$resultArray['REFERENCE_ID'][] 	= $key;
			$resultArray['REFERENCE'][] 	= $value;
		}
		
		return $resultArray;
	}
}
?>