<?

/**
 * Трек полного количества изданий библиотек из Exalead в Битрикс
 * Перенесен в \Neb\Main\Agents\StatsSaver::saveLibraryBooksDigitizedCount();
 */

if ('cli' !== php_sapi_name()) {
	die('Only CLI');
}
$query="
SELECT 
`DATE_STAT`
FROM neb_stat_edition
WHERE `DATE_STAT`= CURDATE()
";
$sql = mysql_query ($query ,$link);

if(!mysql_num_rows($sql ))
exit;


$query="
SELECT 
`bitlib`.`ID`  `id`, 
 UPPER(`bitlib`.`NAME`)  `libname`, 
`bitlib`.`ACTIVE`  `active`, 
`bitlib`.`SHOW_COUNTER_START`  `showcountstart`, 
`exalib`.`ID`  `ex_id`, 
`exalib`.`UF_NAME`  `ex_name`,
`exalib`.`UF_DATE_JOIN`   `date_join`,
`elprop`.`IBLOCK_ELEMENT_ID`  `iblockelid`
FROM `b_iblock_element` as `bitlib`
LEFT JOIN `b_iblock_element_prop_s4` as `elprop` ON `elprop`.`IBLOCK_ELEMENT_ID` = `bitlib`.`ID`
LEFT JOIN `neb_libs` as `exalib` ON  `exalib`.`ID` = `elprop`.`PROPERTY_25`
WHERE `bitlib`.`IBLOCK_ID`= '4' AND `bitlib`.`ACTIVE` = 'Y'
";

$data = file_get_contents('http://10.4.130.3:81/search-api/search?q=%23all%20AND%20filesize>0&sl=sl_statistic3');
$pos0=strpos($data, '</hits>');
$pos1=strpos($data, '<AnswerGroup id="library"', $pos0);
$pos2=strpos($data, '</AnswerGroup>', $pos1);
$leng=$pos2-$pos1 + strlen('</AnswerGroup>');
$str=substr($data, $pos1, $leng);

$xml=simplexml_load_string($str);
$obg =$xml->categories->Category;

$arr_count = Array();
$arr_title = Array();


foreach($obg as $valobj)
{
 $arr_count[]= $valobj['count']; 
 $arr_title[] = strtoupper(trim($valobj['title']));
}
//print_r( $arr_title2);

$sql = mysql_query ($query ,$link);
 while ( $result = mysql_fetch_array($sql) ) 
	{ 
	
		 $result['ex_name'] = strtoupper(trim($result['ex_name']));
		 $key = array_search($result['ex_name'], $arr_title);		 
		 if( $key!==false)
		 {
		   $flag=false;
		   foreach($arr_result as $value)
		   {			
		    if($value['bitrix_id']==$result['id'])
			$flag = true;		
		   } 
		     
		   if($flag)
		   $query = "
		   UPDATE neb_stat_edition
		   SET `COUNT_EDITION_DIG` = '".$arr_count[$key]."'
		   WHERE `ID_BITRIXDB` = '".$result['id']."'  AND `DATE_STAT` = CURDATE() 
			";		   
		   else
		   $query = "
			INSERT INTO neb_stat_edition
			(`ID_BITRIXDB`, `ID_EXALED`, `TITLE_LIB`, `COUNT_EDITION_DIG`, `DATE_STAT`, `ACTIVE_IN_BITRIX`)
			VALUES ('".$result['id']."', '".$result['ex_id']."', '".$result['ex_name']."', '".$arr_count[$key]."', CURDATE(), 'Y')
			";
			mysql_query ($query ,$link);
		 }
		
	}	

?>