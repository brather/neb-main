<?php
/**
 * @var array $arResult
 */
if (isset($arResult['userData'])) {
    $arResult['VALUES'] = $arResult['userData'];
} else {
    $arResult['VALUES'] = [];
}

$arResult['ERRORS'] = $arResult['errors'];