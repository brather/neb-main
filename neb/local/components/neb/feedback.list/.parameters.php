<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        "SEF_MODE" => array(
            "list" => array(
                "NAME" => "LIST PAGE",
                "DEFAULT" => "/",
                "VARIABLES" => array()
            ),
            "detail" => array(
                "NAME" => "DETAIL PAGE",
                "DEFAULT" => "#CODE#/",
                "VARIABLES" => array("CODE")
            ),
            "funds" => array(
                "NAME" => "FUNDS PAGE",
                "DEFAULT" => "#CODE#/funds/",
                "VARIABLES" => array("CODE")
            ),
            "collections" => array(
                "NAME" => "COLLECTIONS PAGE",
                "DEFAULT" => "#CODE#/collections/",
                "VARIABLES" => array("CODE")
            ),
            "news_detail" => array(
                "NAME" => "NEWS DETAIL PAGE",
                "DEFAULT" => "#CODE#/news-#ELEMENT_ID#-#ELEMENT_CODE#/",
                "VARIABLES" => array("CODE")
            ),
        ),
    ),
);
?>