<?// СМЕНА ПАРОЛЯ
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
<?
if(isset($arResult["SUCCESS_MESSAGE"]))
{
    ShowNote($arResult["SUCCESS_MESSAGE"]);
    die();
}
if(isset($arResult["ERROR_MESSAGE"]))
{
    ShowError($arResult["ERROR_MESSAGE"]);
    die();
}
?>
<form method="post" name="form0" action="" class="edit-login-password-form nrf" id='regform' enctype="multipart/form-data">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="action" value="changePasswordRequest" />
    <?php# if($arResult["CURRENT_USER"]):?>
    <div class="nrf-fieldset-title"><?=GetMessage('CHANGE_PASSW');?></div>
    
    <div class="form-group">
        <label for="currentpassword">
            <em class="hint">*</em>
            <?=GetMessage('CHANGE_PASS_CURRENT_PASS');?>
        </label>
        <input
            data-validate="password"
            data-required="required"
            data-maxlength="30"
            data-minlength="2"
            value=""
            type="password"
            id="currentpassword"
            name="CURRENT_PASSWORD"
            class="form-control">
    </div>
    <?php# endif;?>
    
    <div class="form-group">
        <label for="enternewpassword">
            <em class="hint">*</em>
            <?=GetMessage('NEW_PASS');?>
        </label>
        <input 
        type="password" 
        data-validate="password" 
        data-required="required" 
        value="" 
        id="enternewpassword" 
        data-maxlength="30" 
        data-minlength="2" 
        name="NEW_PASSWORD" 
        class="form-control">
        <!-- <div class="passecurity">
            <div class="passecurity_lb"><?=GetMessage('DEFENSE_PASS');?></div>
        </div> -->
    </div>
    
    <div class="form-group">
        <label for="reenternewpassword">
            <em class="hint">*</em>
            <?=GetMessage('PASS_TWO');?>
        </label>
        <input 
        data-identity="#settings05" 
        type="password" 
        data-required="required" 
        value="" 
        id="reenternewpassword" 
        data-maxlength="30" 
        data-minlength="2" 
        name="NEW_PASSWORD_CONFIRM" 
        class="form-control" 
        data-validate="password">        
    </div>
    
    <div class="fieldrow nowrap fieldrowaction">
        <div class="fieldcell ">
            <div class="field clearfix">
                <button name="save" class="btn btn-default" value="1" type="submit"><?=GetMessage('CONFIRM');?></button>
                <div class="messege_ok hidden">
                    <a href="#" class="closepopup">Закрыть окно</a>
                    <p>На Вашу электронную почту, указанную при регистрации, отправлено письмо. <br/>Для подтверждения нового пароля воспользуйтесь ссылкой из письма. <br/>Срок действия ссылки - 24 часа.</p>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- <?=GetMessage('CHANGE_PASS_VALIDATE');?> -->
<script>
    $(function(){
        jQuery.validator.addMethod("latin", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9\w]{6,20}/.test(value);
        }, "Используйте только латинские буквы и цифры");
        
        var validator = $('form[name="form0"]').validate({
            rules: {
                /*"CURRENT_PASSWORD": {
                     required: true
                 },*/
                "NEW_PASSWORD": {
                    required: true,
                    minlength: 6,
                    /* latin: true */
                },
                "NEW_PASSWORD_CONFIRM": {
                    required: true,
                    minlength: 6,
                    /* latin: true, */
                    equalTo: "#enternewpassword"
                }
            },
            messages: {
                /* "CURRENT_PASSWORD": {
                     required: "<?=GetMessage('REQUIRED_PASS');?>"
                */ },
                "NEW_PASSWORD": {
                    required: "<?=GetMessage('REQUIRED_PASS');?>",
                    minlength: "<?=GetMessage('PASS_MIN_PLACEHOLDER');?>"
                },
                "NEW_PASSWORD_CONFIRM": {
                    required: "Повторите пароль",
                    minlength: jQuery.validator.format("не менее {0} символов"),
                    equalTo: "Пароль должен совпасть с паролем, введённым выше"

                }
            },
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
                $(element).closest('.form-group').toggleClass('has-error', true);
            },
            /* specifying a submitHandler prevents the default submit, good for the demo */
            submitHandler: function(form) {
                /* console.log("submitted!"); */
                var postData = {},
                    form = $(form);
                postData.sessid = $('[name="sessid"]', form).val();
                postData["action"] = $('[name="action"]', form).val();
                postData.CURRENT_PASSWORD = $('[name="CURRENT_PASSWORD"]', form).val();
                postData.NEW_PASSWORD = $('[name="NEW_PASSWORD"]', form).val();
                postData.NEW_PASSWORD_CONFIRM = $('[name="NEW_PASSWORD_CONFIRM"]', form).val();
                $.ajax({
                    type: "POST",
                    data: postData,
                    url: window.location.href,
                    success: function(data){
                        /*console.log('success');
                        console.log(data); */
                        var message = {
                            title: "Внимание",
                            text: data,
                            button: "ОК"
                        };

                        /*validator.resetForm(); */
                        FRONT.infomodal(message);
                        $('[name="NEW_PASSWORD"]', form).val('');
                        $('[name="NEW_PASSWORD_CONFIRM"]', form).val('');

                    },
                    error: function(xhr, textStatus, errorThrown){}
                });
            },
            /* set this class to error-labels to indicate valid fields */
            success: function(label) {
                /*set &nbsp; as text for IE
                label.html("&nbsp;").addClass("resolved"); */
                label.remove();
            },
            highlight: function(element, errorClass, validClass) {        
                $(element).closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);
                $(element).parent().find("." + errorClass).removeClass("resolved");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);
            }
        });

    });
</script>