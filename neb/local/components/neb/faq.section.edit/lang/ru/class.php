<?php
$MESS["ERROR_MIN_COUNT_SYMBOL"] = "Ошибка! Вы ввели слишком мало символов.";
$MESS["ERROR_EMPTY_STRING"] = "Ошибка! Вы не заполнили одно из полей.";
$MESS["SECTION_NOT_FOUND"] = "Ошибка! В базе нет данного раздела.";
$MESS["ERROR"] = "Ошибка!";
$MESS["ERROR_SECTION_NOT_EMPTY_1"] = "Ошибка! Раздел с id=<b>";
$MESS["ERROR_SECTION_NOT_EMPTY_2"] = "</b> не пустой.";
$MESS["ERROR_SECTION_EXIST"] = "Ошибка! Раздел с таким именем уже существует. Пожалуйста введите другое название раздела.";
$MESS["SECTION_UPDATE_SUCCESS_1"] = "Изменение раздела с id=<b>";
$MESS["SECTION_UPDATE_SUCCESS_2"] = "</b> произведено успешно!";
$MESS["SECTION_UPDATE_ERROR_1"] = "Ошибка! Изменение раздела с id=<b>";
$MESS["SECTION_UPDATE_ERROR_1"] = "</b> не произведено.";
$MESS["SECTION_DELETE_SUCCESS_1"] = "Удаление раздела с id=<b>";
$MESS["SECTION_DELETE_SUCCESS_2"] = "</b> произведено успешно!";
$MESS["SECTION_DELETE_ERROR_1"] = "Ошибка! Удаления раздела с id=<b>";
$MESS["SECTION_DELETE_ERROR_2"] = "</b> не произведено.";
$MESS["SECTION_DELETE_ERROR_NOT_PERMISSION"] = "Ошибка! Удаления раздела не произошло, поскольку Вам не хватает прав доступа.";