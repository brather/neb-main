$(function() {
	$(document).on('submit', '.b-passrecoveryform', function(e) {
		$.ajax({
			type: "POST",
			url: $(this).attr('action'),
			data: $(this).serialize(),
            dataType: 'json'
		}).done(function(data) {
			if (data.error_message != '')
			{
				$('.b-passrecoveryform .b-warning').html(data.error_message);
				$('.b-passrecoveryform label').html(data.description);
				$('.b-passrecoveryform .b-warning').removeClass('hidden');
				$('.b-passrecoveryform .validate').removeClass('ok').addClass('error');
			}
			else
			{
				$('.b-passrecoveryform .b-warning').addClass('hidden');
				$('.b-passrecoveryform').addClass('hidden');
				$('.b-passconfirm').removeClass('hidden');
			}
		});

		return false;
	});
});
