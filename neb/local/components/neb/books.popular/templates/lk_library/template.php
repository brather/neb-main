<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__DIR__.'/template.php');
if(empty($arResult['POPULAR_BOOKS']))
    return false;
?>
<?
	if(!empty($arResult['POPULAR_BOOKS']))
	{
	?>

		<h3><?=Loc::getMessage('NEB_BOOKS_POPULAR_TITLE_HEAD')?></h3>
        <div class="lk-table">
            <div class="lk-table__column" style="width:20%"></div>
            <div class="lk-table__column" style="width:50%"></div>
            <div class="lk-table__column" style="width:10%"></div>
            <div class="lk-table__column" style="width:20%"></div>
            <div class="lk-table__column" style="width:10%"></div>
            <ul class="lk-table__header">
                <li class="lk-table__header-kind"><?=Loc::getMessage('NEB_BOOKS_POPULAR_AUTHOR')?></li>
                <li class="lk-table__header-kind"><?=Loc::getMessage('NEB_BOOKS_POPULAR_NAME')?></li>
                <li class="lk-table__header-kind"><?=Loc::getMessage('NEB_BOOKS_POPULAR_DATE')?></li>
                <li class="lk-table__header-kind"><?=Loc::getMessage('NEB_BOOKS_POPULAR_LIB')?></li>
                <li class="lk-table__header-kind"></li>
            </ul>
            <section class="lk-table__body">





			<?
				foreach($arResult['POPULAR_BOOKS'] as $arItem)
				{
				?>
                    <ul class="lk-table__row">
                        <li class="lk-table__col"><?=$arItem['authorbook']?></li>
                        <li class="lk-table__col"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955" ><?=$arItem['title']?></a></li>
                        <li class="lk-table__col"><?=$arItem['publishyear']?></li>
                        <li class="lk-table__col"><?=$arItem['library']?></li>
                        <li class="lk-table__col"><a href="javascript:void(0);" data-options="{}" onclick="readBook(event, this); return false;" data-link="<?= $arItem['id'] ?>"><button type="button" class="btn btn-primary">Читать</button></a></li>
                    </ul>

				<?
				}
			?>
            </section>
        </div>

	<?
	}
?>