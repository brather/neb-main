<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<section class="innersection innerwrapper clearfix">
    <div class="b-registration rel">

        <h2 class="mode">Вход пользователя портала</h2>
        <form action="<?=$arResult["AUTH_URL"]?>" class="b-form b-form_common b-regform">
            <input type="hidden" name="backurl" value="/special/" />

            <?foreach ($arResult["POST"] as $key => $value) {?>
                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
            <?}?>

            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />
            <input type="hidden" name="USER_REMEMBER" value="Y" />

            <div class="fieldrow nowrap">
                <div class="field">
                    <p><a href="/special/registration/" title="Регистрация на портале">Регистрация на портале</a></p>
                </div>
            </div>
            <?
            if(!empty($arResult['ERROR_MESSAGE']['MESSAGE']) ||
                (is_string($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE'])))
            {
                if(is_array($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']['MESSAGE']))
                {
                    $arResult['ERROR_MESSAGE'] = $arResult['ERROR_MESSAGE']['MESSAGE'];
                }
                ?>
                <em class="error_message"><?=$arResult['ERROR_MESSAGE'];?></em>
                <script type="text/javascript">
                    $(function() {
                        $('#login-link').click();
                    });
                </script>
            <?
            }
            ?>
            <div class="fieldrow nowrap">
                <div class="fieldcell iblock">
                    <em class="hint">*</em>
                    <label for="settings04">Электронная почта для входа на портал</label>
                    <div class="field validate">
                        <em class="error required">Заполните</em>
                        <em class="error validate">Некорректный e-mail</em>
                        <input type="text" class="input" name="USER_LOGIN" data-minlength="2" data-maxlength="50" id="settings04" data-validate="email" value="<?=$_REQUEST['USER_LOGIN']?>" data-required="true">
                    </div>
                </div>
            </div>
            <div class="fieldrow nowrap">
                <div class="fieldcell iblock">
                    <em class="hint">*</em>
                    <label for="settings05"><span>не менее 6 символов</span>Пароль</label>
                    <div class="field validate">
                        <input type="password" data-validate="password" data-required="true" value="<?=$_REQUEST['USER_PASSWORD']?>" id="settings05" data-maxlength="30" data-minlength="6" name="USER_PASSWORD" class="pass_status input">
                        <em class="error">Заполните</em>
                    </div>
                </div>
            </div>
            <hr>
            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell ">
                    <div class="field clearfix">
                        <button type="submit" value="1" class="formbutton">Войти</button>

                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.b-registration-->



</section>
