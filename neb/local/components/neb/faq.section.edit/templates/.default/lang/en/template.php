<?php
$MESS["NAME_SECTION"] = "Name of section: ";
$MESS["INPUT_NAME_SECTION"] = "input name of section...";
$MESS["ADD_SECTION"] = "Add";
$MESS["UPDATE_SECTION"] = "Update";
$MESS["DELETE_SECTION"] = "Delete section";
$MESS["BACK_TO_LIST"] = "To list sections";
$MESS["ACTIVE_SECTION"] = "Active section ";
$MESS["FAQ_QUESTION_DELETE_SECTION"] = "Delete section?";
$MESS["SORT_SECTION"] = "Sort index: ";