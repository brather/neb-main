<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Neb\Main\Helper\MainHelper,
    \Nota\Exalead\RslViewer;

Loader::includeModule('nota.exalead');

$book_id = trim($_REQUEST['book_id']);
$page = trim($_REQUEST['p']);

$Viewer = new RslViewer($book_id);
$jpg = $Viewer->getPageThumbl(100, $page);

MainHelper::clearBuffer();
header('Content-Type: image/jpeg;charset=utf-8');
echo $jpg;