module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        mangle: true, // to prevent changes to variable and function names
        sourceMap: true
      },
      viewer: {
        files: {
          'local/templates/adaptive/js/viewer.min.js': [
            'local/templates/adaptive/js/jquery-1.11.1.js',
            'local/templates/adaptive/js/jquery/postjquery.js',
            'local/templates/adaptive/js/spin.js',
            'local/templates/adaptive/js/jquery/jquery.validate.js',
            'local/templates/adaptive/js/jquery/additional-methods.validate.js',
            'local/components/exalead/search.page.detail/templates/.default/js/jquery.simplePagination.js',
            'local/templates/adaptive/js/jquery/jquery.rotate.js',
            //'bitrix/js/jqRotate.js',
            'bitrix/js/jquery.Jcrop.js',
            'local/components/exalead/search.page.detail/templates/.default/js/jquery.mousewheel.js',
            'local/templates/adaptive/js/bootstrap-3.3.7.js',
            'local/components/exalead/search.page.detail/templates/.default/script.js'
          ]
        }
      }
      ,siteTemplate: {
        files: {
          'local/templates/adaptive/js/site.min.js': [            
            'local/templates/adaptive/js/jquery-1.11.3.js',
            'local/templates/adaptive/vendor/select2-4.0.3/dist/js/select2.js',
            'local/templates/adaptive/vendor/custom-select/bootstrap-select.custom.min.js',
            'local/templates/adaptive/js/jquery/postjquery.js',
            'local/templates/adaptive/js/jquery/jquery-ui-1.12.1.custom.min.js',
            'local/templates/adaptive/vendor/jquery.ui.touch-punch.min.js',
            'local/templates/adaptive/vendor/dc-accordeon/jquery.cookie.js',
            'local/templates/adaptive/vendor/dc-accordeon/jquery.dcjqaccordion.2.7.min.js',
            'local/templates/adaptive/js/jquery/jquery.scrollTo.js',
            'local/templates/adaptive/js/jquery/jquery.validate.min.js',
            'local/templates/adaptive/js/jquery/jquery.tmpl.js',
            'local/templates/adaptive/js/jquery/additional-methods.validate.min.js',
            'local/templates/adaptive/vendor/charts.js/Chart.min.js',
            'local/templates/adaptive/js/neb/charts.js',
            'local/templates/adaptive/vendor/bootstrap-contextmenu/jquery.contextMenu.min.js',
            'local/templates/adaptive/js/neb/operator-neb.js',
            'local/templates/adaptive/vendor/slick/slick.min.js',
            'local/templates/adaptive/js/bootstrap.min.js',
            'local/templates/adaptive/js/protocolcheck.js',
            'local/templates/adaptive/js/neb/jq-select-replace.js',
            'local/templates/adaptive/js/neb/library-map.js',
            'local/templates/adaptive/js/neb/book-read-check.js',
            'local/templates/adaptive/js/jquery/jquery.fileupload.js',
            'local/templates/adaptive/js/blind.js',
            'local/templates/adaptive/js/spin.js',
            'local/templates/.default/markup/js/libs/jquery.maskedinput.min.js',
            'local/templates/adaptive/vendor/keyboard/keyboard.js',
            'local/templates/adaptive/js/script.js',
            'local/templates/adaptive/js/neb/auth.js',
            'bitrix/js/phpsolutions.backtotop/backtotop.js'
          ]
        }
      }
    },
    cssmin: {
      options: {
        sourceMap: true,
        // mergeIntoShorthands: false,
        // mergeMedia: false
      },
      viewer: {
        files: {
          'local/templates/adaptive/css/viewer.min.css': [
            'local/templates/adaptive/css/bootstrap.css',
            'local/components/exalead/search.page.detail/templates/.default/styles.css'
          ]
        }
      }
      ,siteTemplate: {
        files: {
          'local/templates/adaptive/css/site.min.css': [
            'local/templates/adaptive/css/bootstrap.min.css',
            'local/templates/adaptive/css/font-awesome.min.css',
            'local/templates/adaptive/vendor/slick/slick.css',
            'local/templates/adaptive/vendor/slick/slick-theme.css', 
            'local/templates/adaptive/vendor/custom-select/bootstrap-select.min.css',
            'local/templates/adaptive/vendor/select2-4.0.3/dist/css/select2.css',
            'local/templates/adaptive/vendor/keyboard/keyboard.css',
            'local/templates/adaptive/vendor/bootstrap-contextmenu/jquery.contextMenu.css',
            'local/templates/adaptive/css/style.css',
            'local/templates/adaptive/css/low_vision.css',
            'bitrix/js/phpsolutions.backtotop/backtotop.css'
          ]
        }
      }
    },
    watch: {
      scripts: {
        files: [
          'local/templates/adaptive/js/*.js',
          'local/templates/adaptive/js/jquery/*.js',
          'local/components/exalead/search.page.detail/templates/.default/script.js',
          'local/templates/adaptive/js/script.js',
          'gruntfile.js' 
        ],
        tasks: ['uglify'],
        options: {}
      },
      styles: {
        files: [
          'local/components/exalead/search.page.detail/templates/.default/styles.css',
          'local/templates/adaptive/css/style.css',
          'gruntfile.js' 
        ],
        tasks: ['cssmin'],
        options: {}
      }
    }
  });

  // плагины для сжатия js и css
  
  grunt.loadNpmTasks('grunt-contrib-uglify');
  //https://www.npmjs.com/package/grunt-contrib-uglify

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  //https://www.npmjs.com/package/grunt-contrib-cssmin

  grunt.loadNpmTasks('grunt-contrib-watch');
  //https://github.com/gruntjs/grunt-contrib-watch

  // Default task(s).
  grunt.registerTask('default', ['uglify','cssmin']);

};