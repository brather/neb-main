<?php
/**
 * User: agolodkov
 * Date: 28.09.2016
 * Time: 15:32
 */

namespace LbcBundle\Traits;


trait LbcMigrationsTrait
{
    /**
     * @param $string
     *
     * @return bool
     */
    public function validInsertString($string)
    {
        $string = trim($string);

        return 0 === strpos($string, 'INSERT INTO');
    }

    public function prepareInsertString($string)
    {
        $string = str_replace(
            [
                '`lbc_LocationPlanImages`',
                '`lbc_LocationPlan`',
                '`lbc_LocationPlans`',
                '`lbc_LocationPlanSectionImages`',
                '`lbc_LocationPlanSections`',
                '`lbc_MainDivision`',
                '`lbc_MainDivisionImages`',
                '`lbc_MainDivisionIntroduction`',
                '`lbc_MainDivisionIntroductionImages`',
                '`lbc_SpecialDivision`',
                '`lbc_SpecialDivisionImages`',
                '`lbc_SpecialDivisionSectionImages`',
                '`lbc_SpecialDivisionSections`',
                'INSERT INTO `lbc_locationplans` (`id`,',
            ],
            [
                '`lbc_locationplanimages`',
                '`lbc_locationplans`',
                '`lbc_locationplans`',
                '`lbc_locationplansectionimages`',
                '`lbc_locationplansections`',
                '`lbc_maindivision`',
                '`lbc_maindivisionimages`',
                '`lbc_maindivisionintroduction`',
                '`lbc_maindivisionintroductionimages`',
                '`lbc_specialdivision`',
                '`lbc_specialdivisionimages`',
                '`lbc_specialdivisionsectionimages`',
                '`lbc_specialdivisionsections`',
                'INSERT INTO `lbc_locationplans` (`Id`,',
            ],
            $string
        );

        return $string;
    }
}