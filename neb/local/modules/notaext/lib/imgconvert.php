<?
namespace Bitrix\NotaExt;
/*
* Class ImgConvert Класс для работы с изображениями.
*/

class ImgConvert
{

    /*
    @method Crop - кроп, поворот, ресайз изображения
    @parameter $image - ID файла или путь
    @parameter $arParams array() -
    array(
    'x' 		=> intval($_REQUEST['x']),
    'y' 		=> intval($_REQUEST['y']),
    'width' 	=> intval($_REQUEST['width']),
    'height' 	=> intval($_REQUEST['height']),
    'wResize' 	=> intval($_REQUEST['wResize']),
    'hResize' 	=> intval($_REQUEST['hResize']),
    'rotate' 	=> intval($_REQUEST['rotate']),
    );
    @parameter $SavePatch - в какую папку сохранять
    */
    static public function Crop($image, $arParams, $SavePatch = '/upload/resize_cache_imm')
    {
        if (strlen($image) <= 1)
            return false;

        if (intval($image) > 0) {
            $arFile = \CFile::GetFileArray($image);
            $image = $arFile['SRC'];
        }

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $image))
            return false;

        $SavePatch = rtrim($SavePatch, '/');
        $SavePatch .= '/' . $arFile['SUBDIR'] . '/';

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $SavePatch))
            \CheckDirPath($_SERVER['DOCUMENT_ROOT'] . $SavePatch);

        $SavePatchImage = $SavePatch . md5(serialize($arParams)) . '_' . $arFile['FILE_NAME'];

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $SavePatchImage)) {
            $imagick = new \Imagick($_SERVER['DOCUMENT_ROOT'] . $image);
            if (intval($arParams['rotate']) <> 0) {
                $imagick->rotateImage(new \ImagickPixel('#FFFFFF'), $arParams['rotate']);
                $imagick->writeImage($_SERVER['DOCUMENT_ROOT'] . $SavePatchImage);
                $imagick->clear();
                $imagick->destroy();

                $imagick = new \Imagick($_SERVER['DOCUMENT_ROOT'] . $SavePatchImage);
            }

            $canvas = new \Imagick();
            $width = $imagick->getImageWidth();
            $height = $imagick->getImageHeight();
            $xt1 = 0;
            $yt1 = 0;

            if ($arParams['x'] < 0) {
                # Если ширина выделенной области больше ширины картинки
                if ($arParams['width'] > $width)
                    $width = $arParams['width'];
                else
                    $width = $width - $arParams['x'];

                $xt1 = -$arParams['x'];
                $arParams['x'] = 0;
            } # Если выделенная область выходит за правый край картинки
            elseif (($arParams['x'] + $arParams['width']) > $width) {
                $width = $arParams['x'] + $arParams['width'];
            }

            if ($arParams['y'] < 0) {
                # Если высота выделенной области больше высоты картинки
                if ($arParams['height'] > $height)
                    $height = $arParams['height'];
                else
                    $height = $height - $arParams['y'];

                $yt1 = -$arParams['y'];
                $arParams['y'] = 0;
            } # Если выделенная область выходит за нижний край картинки
            elseif (($arParams['y'] + $arParams['height']) > $height) {
                $height = $arParams['y'] + $arParams['height'];
            }

            $canvas->newImage($width, $height, new \ImagickPixel("white"));
            $canvas->compositeImage($imagick, $imagick->getImageCompose(), $xt1, $yt1);
            $canvas->cropImage($arParams['width'], $arParams['height'], $arParams['x'], $arParams['y']);
            $canvas->writeImage($_SERVER['DOCUMENT_ROOT'] . $SavePatchImage);

            $imagick->clear();
            $imagick->destroy();

            $canvas->clear();
            $canvas->destroy();
        }

        if ($arParams['wResize'] > 0 and $arParams['hResize'] > 0) {
            if (empty($arParams['exact_size']) or $arParams['exact_size'] === false)
                $arParams['exact_size'] = false;
            else
                $arParams['exact_size'] = true;

            if (empty($arParams['quality']))
                $arParams['quality'] = 99;
            else
                $arParams['quality'] = intval($arParams['quality']);

            $arResize = self::Resize($SavePatchImage, $arParams['wResize'], $arParams['hResize'], $arParams['exact_size'], $arParams['quality'], $SavePatchImage);
            return $arResize['src'];
        }

        return $SavePatchImage;
    }

    /*
    @method Resize Ресайз изображения (повтореый ресайз не производится)
    @parameter $image - ID файла или путь
    @parameter $w - Ширина
    @parameter $h - Высота
    @parameter $exact_size - обрезать ?
    @parameter $Quality - качество
    @parameter $SavePatch - в какую папку сохранить, если указать путь до файла, то сохранение произойдет в него

    */
    static public function Resize($image, $w, $h, $exact_size = true, $Quality = 99, $SavePatch = '/upload/resize_cache_imm/')
    {
        if (strlen($image) <= 1)
            return false;

        if (intval($image) > 0) {
            $arFile = \CFile::GetFileArray($image);
            $image = $arFile['SRC'];
        }

        if (strpos($image, $_SERVER['DOCUMENT_ROOT']) !== false)
            $image = str_replace($_SERVER['DOCUMENT_ROOT'], '', $image);

        if (strpos($SavePatch, $_SERVER['DOCUMENT_ROOT']) !== false)
            $SavePatch = str_replace($_SERVER['DOCUMENT_ROOT'], '', $SavePatch);

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $image))
            return false;

        $path_info = pathinfo($_SERVER['DOCUMENT_ROOT'] . $image);

        $image_name = $path_info['basename'];
        $image_path = $path_info['dirname'];

        if (strlen($image_path) <= 1)
            return false;

        $pict_size = getimagesize($_SERVER['DOCUMENT_ROOT'] . $image);

        $width = $pict_size[0];
        $height = $pict_size[1];

        $new_width = $width;
        $new_height = $height;

        if (!$exact_size) {
            if ($width > $w || $height > $h) {
                if ($width < $height) {
                    $ratio = $height / $h;
                    $new_height = $h;
                    $new_width = intval($width / $ratio);
                    if ($new_width > $w) {
                        $ratio = $width / $w;
                        $new_width = $w;
                        $new_height = intval($height / $ratio);
                    }
                } else {
                    $ratio = $width / $w;
                    $new_width = $w;
                    $new_height = intval($height / $ratio);
                    if ($new_height > $h) {
                        $ratio = $height / $h;
                        $new_height = $h;
                        $new_width = intval($width / $ratio);
                    }
                }
            }
            $w = $new_width;
            $h = $new_height;
        }

        if (substr($SavePatch, -4, 1) == '.') {
            $path_info = pathinfo($_SERVER['DOCUMENT_ROOT'] . $SavePatch);
            $path_info['dirname'] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $path_info['dirname']);
            $cache = $path_info['dirname'] . '/resize' . $w . "x" . $h . '_' . $path_info['basename'];
        } else {
            $image_path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $image_path);
            $cache_path = $image_path;
            $cache_path = str_replace('/upload/', $SavePatch, $cache_path);

            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $cache_path))
                \CheckDirPath($_SERVER['DOCUMENT_ROOT'] . $cache_path);

            if (substr($image_name, -3, 3) == 'tif')
                $image_name = str_replace('.tif', '.jpg', $image_name);

            $cache_name = $w . "x" . $h . "_Quality" . $Quality . "_" . $image_name;

            if ($exact_size)
                $cache_name = "exact_" . $cache_name;
            $cache = $cache_path . $cache_name;
        }

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $image)) {
            die('ERROR: That image does not exist.');
            return false;
        }

        $return["src"] = $cache;
        $return["width"] = $w;
        $return["height"] = $h;

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $cache))
            return $return;

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $cache)) {
            $thumb = new \Imagick($_SERVER['DOCUMENT_ROOT'] . $image);
            $thumb->setCompression(\Imagick::COMPRESSION_JPEG);
            $thumb->setCompressionQuality($Quality);
            if (!$exact_size)
                $thumb->resizeImage($w, $h, \Imagick::FILTER_LANCZOS, 1);
            else
                $thumb->cropThumbnailImage($w, $h);

            $thumb->writeImage($_SERVER['DOCUMENT_ROOT'] . $cache);
            $thumb->destroy();
        }

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $cache))
            die('ERROR: Image conversion failed.');
        return $return;
    }
}
