<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//var_dump($arResult);
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<script src="<?=MARKUP?>js/libs/modernizr.min.js"></script>
		<meta name="viewport" content="width=device-width"/>

		<title><?$APPLICATION->ShowTitle()?></title>
		<?$APPLICATION->ShowHead();?>
		<link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />
		<?$APPLICATION->SetAdditionalCSS(MARKUP.'css/style.css');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/libs/jquery.min.js');?>
		<?$APPLICATION->AddHeadScript('/local/templates/.default/js/script.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/slick.min.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/plugins.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.knob.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/script.js');?>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<?if($APPLICATION->GetCurPage() == '/'){?>
			<script>
				$(function() {
					$('html').addClass('mainhtml'); /* это для морды */
				}); /* DOM loaded */
			</script>
			<?}?>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
		<script type="text/javascript">VK.init({apiId: 4525156, onlyWidgets: true});</script>

	</head>

	<body>
		<base target="_self">
		<div class="b_bookfond_popup_content js_scroll">
			<?
				global $navParent;
				$navParent = '_self';
			?>
			<form action="/profile/funds/manage/" target="_top" method="post" class="b-form b-form_common b-addbookform">
			<input type="hidden" name="action" value="addBooksXLS">
			<?=bitrix_sessid_post()?>
			<div class="fieldrow nowrap">
				<div class="fieldcell clearfix">
					<div class="left">
						<?$APPLICATION->IncludeComponent(
							"notaext:plupload",
							"fond_add_books_xls",
							array(
								"MAX_FILE_SIZE" => "512",
								"DIR" => "tmp_books_xls",
								//"FILES_FIELD_NAME" => "books_pdf",
								"MULTI_SELECTION" => "N",
								"CLEANUP_DIR" => "Y",
								"UPLOAD_AUTO_START" => "Y",
								"RESIZE_IMAGES" => "N",
								"UNIQUE_NAMES" => "Y",
							),
							false
						);?>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix">
						<a href="#" class="formbutton gray right btrefuse">Отменить</a>
						<button class="formbutton left" value="1" type="submit">Загрузить</button>
					</div>
				</div>
			</div>
			</form>
		</div>
	</body>
</html>