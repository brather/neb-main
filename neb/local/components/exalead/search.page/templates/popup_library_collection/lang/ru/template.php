<?php
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO'] = 'Найдено 0 документов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND'] = 'К сожалению, ничего не найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SUGGEST'] = 'Возможно вы искали';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'Найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'Найден';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'Найдено';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'] = 'результатов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'] = 'результата';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'] = 'результат';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY'] = 'Сохранить поисковый запрос';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED'] = 'Поисковый запрос сохранен';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE'] = 'Добавить название';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SPECIFY_SEARCH'] = 'уточнить поиск';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE'] = 'Вы искали на портале';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE'] = 'Искать только в полнотекстовых изданиях';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY'] = 'По каталогу печатных изданий';

$MESS['LIB_SEARCH_PAGE_POPUP_CHOOSE_BOOK'] = 'Перед добавлением издания необходимо выбрать книгу из списка.';
$MESS['LIB_SEARCH_PAGE_BOOKS_EXIST_IN_COLLECTION'] = ' (уже находится в текущей коллекции)';

$MESS['LIB_SEARCH_AUTHOR'] = 'Автор: ';
$MESS['LIB_SEARCH_PUBLIC_YEAR'] = 'Год публикации: ';
$MESS['LIB_SEARCH_COUNT_OF_PAGES'] = 'Количество страниц: ';
$MESS['LIB_SEARCH_PUBLIC_HOUSE'] = 'Издательство: ';
$MESS['LIB_SEARCH_SOURCE'] = 'Источник: ';
$MESS['LIB_SEARCH_CANCEL_SELECT'] = 'Отменить выбор';
$MESS['LIB_SEARCH_ADD_SELECT'] = 'Добавить выбранные';


