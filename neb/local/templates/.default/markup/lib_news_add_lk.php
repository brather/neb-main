
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			
		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<div class="checkwrapper right">
						<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
					</div>
					<input type="submit" class="formbutton" value="Принять">

				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix ">
	
	<div class="b-mainblock left">
		<div class="b-searchresult">
			<ul class="b-profile_nav">
				<li>
					<a href="#" class="b-profile_navlk js_profilemenu">личный кабинет</a>
					<ul class="b-profile_subnav">
						<li><span class="b-profile_subnav_border"><a href="#">Просмотренные книги</a></span></li>
						<li><span class="b-profile_subnav_border"><span class="b-profile_msgnum">3</span></a><a href="#">Личные сообщения</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Моя активность </a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">История поиска</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Настройка профиля</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#">Помощь</a></span></li>
						<li><span class="b-profile_subnav_border"><a href="#"><strong>Выйти</strong></a></span></li>	
					</ul>
				</li>
				<li><a class="" href="#">статистика</a></li>
				<li><a href="#" class="">фонды</a></li>
				<li><a href="#" class=" ">читатели</a></li>
				<li><a href="#" class="">коллекции</a></li>
				<li><a href="#" class="current">новости</a></li>
			</ul>                 
		</div><!-- /.b-searchresult-->
		<div class="b-newsformpage">
			<span class="right regfield_label">* – обязательные поля</span>
			<h2 class="mode">добавление новости библиотеки</h2>
			<form action="" class="b-form b-form_common b-newsform">
		
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="settings02">Заголовок</label>
					<div class="field validate">
						<input type="text" data-required="required" data-validate="alpha" value="" id="settings02" data-maxlength="30" data-minlength="2" name="name" class="input" >										
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock mt10">
					<label for="settings22">Краткое описание</label>
					<div class="field validate">
						<textarea name="" id="" value="" id="settings22" data-maxlength="1500" data-minlength="2" name="name" class="input tasmall" ></textarea>											
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock mt10">			
					<label for="settings05">Добавить изображение</label>
					<div class="field validate">
						<div class="setscan iblock">
							<input type="file" class="photofile" name="" id="">
							<a href="#">Загрузить изображение</a>
							<div class="setphoto_lb">или перетащите его на это поле</div>
						</div>
						</div>
						<em class="error errsize">Ваше изображение имеет слишком маленький размер</em>
				</div>
			</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock mt28">
					<em class="hint">*</em>
					<label for="settings04">Текст новости</label>
					<div class="field validate">
						<textarea name="" id="" value="" id="settings22" data-maxlength="1500" data-minlength="2" name="name" class="input" data-required="required" ></textarea>										
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock mt10">
			<div class="b-newsdateset">Дата новости</div>
			<div class="b-newsdateinfo">
				<span class="today">Сегодня 20.12.2015</span>
				<span>или выбрать другую дату</span>
				<div class="calendar right rel">
					<a class="btcalendar button_mode black" href="#">Выбрать дату</a>
					<input type="text" name="" class="tbcalendar" id="">
				</div>
				
			</div>
		</div>
	</div>
		<div class="fieldcell nowrap photorow">
				<label>Прикрепить фотогалерею</label>
				<div class="field validate">
				<div class="setscan iblock">
					<input type="file" class="photofile" name="" id="" multiple >
					<a href="#">Загрузить изображение (одно или несколько)</a>
					<div class="setphoto_lb">или перетащите на это поле</div>
				</div>
				</div>
				<div class="b_photobl">
					<div class="b_photo iblock">
						<img src="./pic/pic_43.jpg" alt="photo">
					</div>
					<div class="descrphoto iblock">
						<label for="settings13">Описание фотографии</label>
						<div class="field validate">
							<textarea class="input" data-minlength="2" data-maxlength="1500" value="" id="settings13" name=""></textarea>										
						</div>
					</div>
				</div>
				<div class="b_photobl">
					<div class="b_photo iblock">
						<img src="./pic/pic_44.jpg" alt="photo">
						<div class="progressbar"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружается <span class="num">33</span>%</div></div>
					</div>
					<div class="descrphoto iblock">
						<label for="settings12">Описание фотографии</label>
						<div class="field validate">
							<textarea class="input" rows="1" cols="1" data-minlength="2" data-maxlength="1500" value="" id="settings12" name=""></textarea>										
						</div>
					</div>
				</div>
			</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock w700">
					<label for="settings21">Прикрепить видео (вставьте ссылку)</label>
					<div class="field validate">
						<input type="text" class="input" name="addres" data-minlength="2" data-maxlength="30" id="settings21"  value="" >									
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock w700 mt25">
					<label for="settings24">Прикрепить файл к новости</label>
					<div class="field validate">
						<div class="b-filechoose mt5">
							<a class="button_mode black" href="#">Выбрать файл</a>
							<span class="b-fileformat">форматы: .jpg / .png  / .doc  / .ppt  / .xls  / .pdf </span>
						</div>	
						<em class="error errformat">Файл данного формата не поддерживается</em>					
					</div>
				</div>
			</div>
			<hr>
		<div class="b-newspublication">
			<a href="#" class="right">Посмотреть превью новости</a>
			<div class="checkwrapper ">				
				<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="lite">Опубликовать на сайте</label>
			</div>
		</div>
			
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell iblock w700">
					<div class="field clearfix">
						<button class="formbutton" value="1" type="submit">сохранить</button>
						<span class="b-listlink right">Перейти к <a href="#">списку новостей</a></span>
					</div>
				</div>
			</div>
		</form>
		</div><!-- /.b-newsform-->
		
		
	
	</div><!-- /.b-mainblock -->
	<div class="b-side right b-sidesmall">
		<a href="#" class="b-btlibpage">Страница библитеки<br>на портале НЭБ</a>
		<div class="b-libfond">
			<div class="b-loadlogo rel">
				<a href="#">загрузить логотип</a> 
				<input type="file" name="" id="" class="b-loadlogofield">
			</div>
			<div class="b-fond_img">
				<img src="./pic/pic_31.png" alt="">
			</div>
			<span class="b-fondinfo_number_lb">В фонде библиотеки</span>
			<span class="b-portalinfo_numbers">
				<span class="b-portalinfo_number mr iblock">1</span>
				<span class="b-portalinfo_number iblock">0</span>
				<span class="b-portalinfo_number iblock">5</span>
				<span class="b-portalinfo_number mr iblock">1</span>
				<span class="b-portalinfo_number iblock">2</span>
				<span class="b-portalinfo_number iblock">1</span>
				<span class="b-portalinfo_number iblock">7</span></span>
				<span class="b-fondinfo_number_lb">изданий</span>
				<div class="b-lib_collect">
					<span class="b-fondinfo_number_lb">Библиотека собрала</span>
					<span class="b-portalinfo_numbers">

						<span class="b-portalinfo_number iblock">2</span>
						<span class="b-portalinfo_number iblock">1</span>
						<span class="b-portalinfo_number iblock">7</span></span>
						<span class="b-fondinfo_number_lb">коллекций</span>
						<a href="#" class="button_mode button_revers">посмотреть все</a>
					</div>
					<div class="b-lib_counter">
						<div class="b-lib_counter_lb">Количество читателей</div>
						<div class="icouser">х 108</div>
					</div>
					<div class="b-lib_counter">
						<div class="b-lib_counter_lb">Количество просмотров</div>
						<div class="icoviews">х 15 678</div>
					</div>
					<a href="#" class="button_mode black">полная статистика</a>
				</div>
			</div><!-- /.b-side -->

		</section>

	</div><!-- /.homepage -->

	<? include("footer.php"); ?>
	<!--popup добавить в подборки-->
<!--<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu js_openmenu">мои подборки</a>
	
	<ul class="b-selectionlist">
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb8">	<label for="cb8" class="black">Любимые авторы</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb9">	<label for="cb9" class="black">Научно-популярная фантастика</label>

		</li>
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb10"><label for="cb10" class="black">Ракеты и люди</label>

		</li>	
		<li class="checkwrapper">									
			<input class="checkbox" type="checkbox" name="" id="cb14"><label for="cb14" class="lite">Отметить как прочитанное</label>

		</li>
		<li><a href="#" class="b-selection_add"><span>+</span>Cоздать подборку</a></li>
	</ul>
</div>--><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
</body>
</html>