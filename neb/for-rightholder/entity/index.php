<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Регистрация правообладателя (юридическое лицо)");
?>
<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('REGISTRATION_RIGHTHOLDER_PAGE_TITLE'));
$USER->SetParam('REGISTER_TYPE', 'rightholder_entity');

$APPLICATION->IncludeComponent(
    'neb:main.register',
    'rightholder_entity',
    array(
        'USER_PROPERTY_NAME' => '',
        'SHOW_FIELDS'        => array(
            'EMAIL',
            'NAME',
            'SECOND_NAME',
            'LAST_NAME',
            'PERSONAL_GENDER',
            'PERSONAL_BIRTHDAY',
            'PERSONAL_PHOTO',
            'PERSONAL_ZIP',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
            'PERSONAL_GENDER',
            'WORK_ZIP',
            'WORK_CITY',
            'WORK_STREET',
        ),
        'REQUIRED_FIELDS'    => array(
            'EMAIL',
            'PERSONAL_ZIP',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
        ),
        'USER_PROPERTY'      => array(
            'UF_LIBRARY',
            'UF_STATUS',
            'UF_PASSPORT_NUMBER',
            'UF_ISSUED',
            'UF_CORPUS',
            'UF_STRUCTURE',
            'UF_FLAT',
            'UF_PASSPORT_SERIES',
            'UF_EDUCATION',
            'UF_HOUSE2',
            'UF_STRUCTURE2',
            'UF_FLAT2',
            'UF_PLACE_REGISTR',
            'UF_SCAN_PASSPORT1',
            'UF_SCAN_PASSPORT2',
            'UF_CITIZENSHIP',
            'UF_STATUS',
            'UF_BANK_NAME',
            'UF_BANK_INN',
            'UF_BANK_KPP',
            'UF_BANK_BIC',
            'UF_COR_ACCOUNT',
            'UF_PERSONAL_ACCOUNT',
            'UF_FOUNDATION',
            'UF_PERSON_INN',
            'UF_PERSON_KPP',
            'UF_PERSON_OKPO',
            'UF_PERSON_OGRN',
            'UF_PERSON_OKATO',
            'UF_PERSON_KBK',
            'UF_POSITION_HEAD',
            'UF_FIO_HEAD',
            'UF_CONTACT_INFO',
            'UF_CONTACT_PHONE',
            'UF_CONTACT_MOBILE',
        ),
        'AUTH'               => 'Y',
        'USE_BACKURL'        => 'Y',
        'SUCCESS_PAGE'       => '',
        'SET_TITLE'          => 'N',
        'NEED_CONFIRM'       => 'Y',
        'MAIL_EVENTS'        => array(
            'NEW_USER'
        ),
    )
); ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>