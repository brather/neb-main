$(function(){
    var $formUpdate = $('#faq-form-update-section'),
        $inpDel = $formUpdate.find('input[name=DELETE_SECTION]'),
        textConfirm = $inpDel.data('confirm'),
        $inptAct = $formUpdate.find('input[name=ACTIVE_SECTION]'),
        blkAct = $inptAct.closest('.form-group'),
        speedToggle = 300;
    //region При клике на чекбокс "Удалить раздел" скрывается чекбокс "Раздел активен"
    $inpDel.on('change', function(e){
        if($inpDel.is(':checked')) {
            blkAct.hide(speedToggle);
        } else {
            blkAct.show(speedToggle);
        }
    });
    //endregion
    $formUpdate.on('submit', function(e){
        if($inpDel.is(':checked')) {
            var isDelete = confirm(textConfirm);
            if(!isDelete) e.preventDefault();
        }
    });

});