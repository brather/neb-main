<?php
/**
 * User: agolodkov
 * Date: 26.08.2015
 * Time: 10:21
 */

namespace Neb\Main\Bitrix;


use Bitrix\Main\Entity\DataManager;

/**
 * Class FormResultTable
 *
 * @package Neb\Main\Bitrix
 */
class FormResultTable extends DataManager
{

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'b_form_result';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID'              => array(
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
            ),
            'TIMESTAMP_X'     => array(
                'data_type' => 'date'
            ),
            'DATE_CREATE'     => array(
                'data_type' => 'date'
            ),
            'STATUS_ID'       => array(
                'data_type'     => 'integer',
                'default_value' => 0,
            ),
            'FORM_ID'         => array(
                'data_type'     => 'integer',
                'default_value' => 0,
            ),
            'USER_ID'         => array(
                'data_type' => 'integer',
            ),
            'USER_AUTH'       => array(
                'data_type'     => 'string',
                'default_value' => 'N',
            ),
            'STAT_GUEST_ID'   => array(
                'data_type' => 'integer',
            ),
            'STAT_SESSION_ID' => array(
                'data_type' => 'integer',
            ),
            'SENT_TO_CRM'     => array(
                'data_type'     => 'string',
                'default_value' => 'N',
            ),
        );
    }
}