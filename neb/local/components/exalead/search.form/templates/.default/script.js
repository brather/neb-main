$(function() {
	url = window.location.href;
	if (url.indexOf('/search/') + 1)
	{
		var offset = $("#fixed_form").offset();
		if (offset) {
			$(window).scroll(function()	{
				if ($(window).scrollTop() >= offset.top) {
					$("#fixed_form").addClass("b-fixedhead");
					
					if ($('.js_extraform').hasClass("open")) {
						$("#fixed_height").css("height","365px");
					} else {
						$("#fixed_height").css("height","106px");
					}
				}
				else
				{
					$("#fixed_form").removeClass("b-fixedhead");
					$("#fixed_height").css("height","auto");
				}

	            $('.b-search_ex').slideUp();
	            $('#fixed_height').css('height','auto');
	        });
		}
	}
	
	$('#cb38, #cb1').change(function(){
		var checkbox;

		if (this.id == 'cb38'){
			checkbox = 'cb1';
		}else{
			checkbox = 'cb38';
		}
				
		if ($(this).prop("checked") === false){
			$("#"+checkbox).attr("checked","checked");
			$("#"+checkbox).parent('span').parent('.radio').addClass('checked');
		}else{
			$("#"+checkbox).removeAttr("checked");
			$("#"+checkbox).parent('span').parent('.radio').removeClass('checked');


		}
		
	});

	var checkSearchLock = function () {
		var
			author_search = $('#author_search'),
			name_search = $('#name_search'),
			asearch = $('#asearch'),
			s_strict = $('#cb4');
		if (asearch.val()) {
			author_search.attr('disabled', true);
			name_search.attr('disabled', true);
			s_strict.removeAttr('disabled');
		} else if ( author_search.val() || name_search.val() ) {
			asearch.attr('disabled', true);
			s_strict.prop('checked',false).attr('disabled', true);
		} else {
			author_search.removeAttr('disabled');
			name_search.removeAttr('disabled');
			asearch.removeAttr('disabled');
			s_strict.removeAttr('disabled');
		}
	};
	$('.main-search_field-input').keydown(function () {
		setTimeout(checkSearchLock, 50);
	});
	$('.main-search__general-clean').one('click', function () {
		var date = new Date(new Date().getTime() + 60 * 1000);
		document.cookie = "cleanSearchString=1; path=/; expires=" + date.toUTCString();
		checkSearchLock();
	});
	$('.main-search__exsearch-close').one('click', function () {
		var date = new Date(new Date().getTime() + 60 * 1000);
		document.cookie = "cleanSearchAll=1; path=/; expires=" + date.toUTCString();
	});
});



