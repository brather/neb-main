<?php
/**
 * User: agolodkov
 * Date: 03.09.2015
 * Time: 16:10
 */

$arResult['FILE'] = array();
if (isset($_POST['FILE']) || !empty($arParams['FILE'])) {
    $arResult['FILE'] = array(
        'FILE'             => isset($_POST['FILE']) ? $_POST['FILE']
            : @$arParams['FILE']['SRC'],
        'FILE_DESCRIPTION' => isset($_POST['FILE_DESCRIPTION']) ? $_POST['FILE_DESCRIPTION']
            : @$arParams['FILE']['DESCRIPTION'],
    );
    $arResult['FILE']['FILE_NAME'] = GetFileName($arResult['FILE']['FILE']);
}