<?
namespace Nota\Exalead;

use \Bitrix\Main\Data\Cache,
    \Bitrix\Main\Web\HttpClient,
    \Neb\Main\Helper\MainHelper;

class SearchClient
{
    private $obQuery;
    private $cacheEnabled = false;
    private $cacheTime = 3600;
    public  $strNav;
    public  $arNavParam;

    public function getResult(SearchQuery $query)
    {
        $this->obQuery = $query;

        $result = null;
        $url = $query->getUrl();
        $params = self::prepParameters($query->getParameters());

        // настройки кеширования
        $cacheDir  = CACHE_DIRECTORY . '/' . __CLASS__ . '/'. __FUNCTION__; // мб имеет смысл ещё попдпаки организовать
        $cacheId   = md5( $url . $params . $query->getQueryType());

        $obCache = Cache::createInstance();
        if (
            $this->cacheEnabled !== true || EXALEAD_DEBUG_MODE === true || EXALEAD_FULL_DEBUG_MODE === true
                || !$obCache->initCache($this->cacheTime, $cacheId, $cacheDir)
        ) {
            $obHttp = new HttpClient(
                array(
                    'socketTimeout' => $query->getTimeout(),
                    'streamTimeout' => $query->getTimeout() * 2,
                )
            );

            if (strpos($url, 'http') === false)
                $requestUrl = 'http://' . $query->ip . ':' . $query->port . $url . '?' . $params;
            else
                $requestUrl = $url . '?' . $params;

            if (!MainHelper::checkSslVerification($requestUrl)) {
                $obHttp->disableSslVerification();
            }

            $result = $obHttp->post($requestUrl);

            if (EXALEAD_DEBUG_MODE === true) {
                debugPre($query->getParameters());
            } elseif (EXALEAD_FULL_DEBUG_MODE === true) {
                debugPre($requestUrl);
            }
            //debugPre('*****'); debugPre($query->ip); debugPre($query->port); debugPre($url); debugPre($result);

            if (empty($result))
                return false;

            switch ($query->getQueryType()) {
                case 'raw':
                    return $result;
                    break;
                case 'json':
                    $result = json_decode($result, true);
                    break;
                case 'search':
                    $obXml = new XmlParser($result);
                    if ($obXml) {
                        $obXml->search();
                        $result = $obXml->arResult;
                    } else
                        $result = false;
                    break;

                case 'spellcheck':
                    $obXml = new XmlParser($result);
                    if ($obXml) {
                        $obXml->spellcheck();
                        $result = $obXml->arResult;
                    } else
                        $result = false;
                    break;

                case 'getById':
                    $obXml = new XmlParser($result);
                    if ($obXml) {
                        $obXml->search();
                        $result = $obXml->arResult['ITEMS'][0];
                    } else
                        $result = false;
                    break;

                case 'suggest':
                    $obXml = new XmlParser($result);
                    if ($obXml) {
                        $obXml->autocomplete();
                        $result = $obXml->arResult;
                    } else
                        $result = false;
                    break;
            }

            if ($this->cacheEnabled === true) {
                $obCache->startDataCache();
                $obCache->endDataCache($result);
            }

        } else {
            $result = $obCache->getVars();
        }

        // подставляем параметр token для закрытого просмотровщика
        global $USER;
        if (!empty($result) && $USER->IsAuthorized()) {
            if ($query->getQueryType() == 'search' && !empty($result['ITEMS'])) {

                $nebUser = new \nebUser();
                $arUser = $nebUser->getUser();

                if (!empty($arUser['UF_TOKEN'])) {
                    foreach ($result['ITEMS'] as &$arItem)
                        if (!empty($arItem['PROTECTED_URL']))
                            $arItem['PROTECTED_URL'] .= '&token=' . $arUser['UF_TOKEN'];
                }
            } elseif ($query->getQueryType() == 'getById') {
                if (!empty($result['PROTECTED_URL'])) {
                    $nebUser = new \nebUser();
                    $arUser = $nebUser->getUser();
                    if (!empty($arUser['UF_TOKEN']))
                        $result['PROTECTED_URL'] .= '&token=' . $arUser['UF_TOKEN'];
                }
            }
        }

        return $result;
    }

    public static function prepParameters($arParam)
    {
        $strQuery = http_build_query($arParam);
        //$strQuery = preg_replace('/r\[[0-9]\]/iu', 'r', $strQuery);
        //заменяем r[0] на r для того что бы задать несколько уточнений т.к. формат r[0] экзалида не понимает
        $strQuery = preg_replace('/r%5B[0-9]%5D/iu', 'r', $strQuery);
        return $strQuery;
    }

    /**
     * Постраничная навигация
     *
     * @param $arResult
     * @return bool|\CDBResult
     */
    public function getDBResult($arResult)
    {
        if (empty($arResult['ITEMS']))
            return false;

        unset($arResult['GROUPS_KEY_VAL'], $arResult['GROUPS']);

        //формируем массив для постранички
        $arResultFetch = array();
        if (intval($arResult['START_ITEM']) > 0)
            $arResultFetch = array_fill(0, ($arResult['START_ITEM']), ['id' => '1', 'title' => '2', 'publisher' => '3']);

        $arResultFetch = array_merge($arResultFetch, $arResult['ITEMS']);

        $endar = ($arResult['NHITS'] - ($this->obQuery->nav['SIZEN'] + $arResult['START_ITEM']));

        if ($endar > 0) {
            // ставим ограничение, иначе заканчивается память в array_fill
            if ($endar > 2000)
                $endar = 2000;
            $arTmp = array_fill(0, $endar, ['id' => '1', 'title' => '2', 'publisher' => '3']);
            $arResultFetch = array_merge($arResultFetch, $arTmp);
        }

        $rs = new \CDBResult;
        $rs->InitFromArray($arResultFetch);
        $rs->NavStart($this->obQuery->nav['SIZEN']);
        $this->strNav = $rs->GetPageNavString('');

        $this->arNavParam = [
            'NavNum' => $rs->NavNum,
            'NavPageCount' => $rs->NavPageCount,
            'NavPageNomer' => $rs->NavPageNomer,
        ];

        return $rs;
    }

    /**
     * @return boolean
     */
    public function isCacheEnabled()
    {
        return $this->cacheEnabled;
    }

    /**
     * Установка кеширования и его времени
     *
     * @param $bCacheEnabled
     * @param int $iCacheTime
     */
    public function setCacheEnabled($bCacheEnabled, $iCacheTime = 3600)
    {
        $this->cacheEnabled = $bCacheEnabled;
        $this->cacheTime    = $iCacheTime;
    }
}
