<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LbcMaindivisionimages
 *
 * @ORM\Table(name="lbc_maindivisionimages")
 * @ORM\Entity
 */
class LbcMaindivisionimages extends AbstractLbcImages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="MainDivisionId", type="integer", nullable=false)
     */
    private $maindivisionid;

    /**
     * Set maindivisionid
     *
     * @param integer $maindivisionid
     *
     * @return LbcMaindivisionimages
     */
    public function setMaindivisionid($maindivisionid)
    {
        $this->maindivisionid = $maindivisionid;

        return $this;
    }

    /**
     * Get maindivisionid
     *
     * @return integer
     */
    public function getMaindivisionid()
    {
        return $this->maindivisionid;
    }
}
