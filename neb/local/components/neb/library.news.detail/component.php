<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


if (empty($arParams['ID']))
    return false;


if (empty($arParams['IBLOCK_ID']))
    return false;

use Bitrix\NotaExt\Iblock\Element;

// Если превью - своя обработка
if ($arParams['ID'] == 'preview') {
    // Подготавливаем массив $arResult['ITEM'] по правилам компонента
    $arItem = array(
        'NAME' => $_REQUEST['NAME'],
        'ACTIVE_FROM' => $_REQUEST['DATE_ACTIVE_FROM'],
        'DETAIL_TEXT' => $_REQUEST['DETAIL_TEXT'],
        '~DETAIL_TEXT' => $_REQUEST['DETAIL_TEXT'],
        'DETAIL_PICTURE' => '', // Дальше по коду
        'GALLERY' => array(),
        'YOUTUBE' => $_REQUEST['YOUTUBE'],
        'PROPERTY_LIBRARY_VALUE' => $_REQUEST['LIBRARY_ID'],
        // Файл
    );

    if (!empty($arItem['ACTIVE_FROM'])) {
        $arResult['ITEM']['FORMAT_ACTIVE_FROM'] = FormatDate(array(
            "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
            "today" => "today",              // выведет "сегодня", если дата текущий день
            "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
            "d" => 'j F',                   // выведет "9 июля", если месяц прошел
            "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
        ), MakeTimeStamp($arResult['ITEM']['ACTIVE_FROM']), time()
        );
    }

    if (!empty($arItem['YOUTUBE'])) {
        $t1 = parse_url($arItem['YOUTUBE'], PHP_URL_QUERY);
        parse_str($t1, $output);
        if (!empty($output['v']))
            $arItem['PROPERTY_YOUTUBE_CODE'] = $output['v'];
    }

    if ((!empty($_REQUEST['FILE']) || !empty($_REQUEST['FILE_old'])) && $_REQUEST['FILE_del'] != 'Y') {
        if (!empty($_REQUEST['FILE'])) {
            $arItem['PROPERTY_FILE_VALUE'] = $_REQUEST['FILE'];
        } else {
            $arItem['PROPERTY_FILE_VALUE'] = $_REQUEST['FILE_old'];
        }

        if (!empty($arItem['PROPERTY_FILE_VALUE']))
            $arItem['PROPERTY_FILE_VALUE_EXT'] = GetFileExtension($arItem['PROPERTY_FILE_VALUE']);

        $arItem['PROPERTY_FILE_DESCRIPTION'] = $_REQUEST['FILE_DESCRIPTION'];
    }

    // Теперь разбираем галлерею и формируем список, необходимый компоненту
    if (!empty($_REQUEST['GALLERY_old'])) {
        foreach ($_REQUEST['GALLERY_old'] as $galItem) {
            if ($galItem['del'] == 'Y') continue;

            $file = CFile::ResizeImageGet($galItem['VALUE'], array('width' => 600, 'height' => 340), BX_RESIZE_IMAGE_EXACT);
            $desc = $galItem['DESCRIPTION'];
            $arItem['GALLERY'][] = array('FILE' => $file['src'], 'DESC' => $desc);
        }
    }
    if (!empty($_REQUEST['GALLERY'])) {
        foreach ($_REQUEST['GALLERY'] as $galItem) {
            if (empty($galItem['VALUE']) || $galItem['del'] == 'Y') continue;

            $file['src'] = $galItem['VALUE'];
            $desc = $galItem['DESCRIPTION'];
            $arItem['GALLERY'][] = array('FILE' => $file['src'], 'DESC' => $desc);
        }
    }

    // Не сработает
    if (!empty($_REQUEST['DETAIL_PICTURE'])) {
        $arItem['DETAIL_PICTURE']['src'] = $_REQUEST['DETAIL_PICTURE'];
    } elseif (!empty($_REQUEST['DETAIL_PICTURE_old'])) {
        $arItem['DETAIL_PICTURE']['src'] = $_REQUEST['DETAIL_PICTURE_old'];
    }

    $arResult['ITEM'] = $arItem;

    $arParams['NEWS_DETAIL_URL'] = str_replace('#CODE#', $arParams['LIBRARY_CODE'], $arParams['NEWS_DETAIL_URL']);
    $this->IncludeComponentTemplate();

    $APPLICATION->SetTitle($arResult['ITEM']['NAME']);
    // Дальше нам ничего не нужно
    return true;
}

$arResult = Element::getList(
    array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arParams['ID']),
    1,
    array(
        'ACTIVE_FROM',
        'DETAIL_TEXT',
        'DETAIL_PICTURE',
        'PROPERTY_GALLERY',
        'PROPERTY_LIBRARY',
        'PROPERTY_YOUTUBE',
        'PROPERTY_FILE',
    )
);

if (empty($arResult['ITEM'])) {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
    return;
}

if (!empty($arResult['ITEM']['ACTIVE_FROM'])) {
    $arResult['ITEM']['FORMAT_ACTIVE_FROM'] = FormatDate(array(
        "tommorow" => "tommorow",      // выведет "завтра", если дата завтрашний день
        "today" => "today",              // выведет "сегодня", если дата текущий день
        "yesterday" => "yesterday",       // выведет "вчера", если дата прошлый день
        "d" => 'j F',                   // выведет "9 июля", если месяц прошел
        "" => 'j F Y',                    // выведет "9 июля 2012", если год прошел
    ), MakeTimeStamp($arResult['ITEM']['ACTIVE_FROM']), time()
    );
}

if (!empty($arResult['ITEM']['DETAIL_PICTURE']))
    $arResult['ITEM']['DETAIL_PICTURE'] = CFile::ResizeImageGet($arResult['ITEM']['DETAIL_PICTURE'], array('width' => 300, 'height' => 300), BX_RESIZE_IMAGE_PROPORTIONAL);


if (!empty($arResult['ITEM']['PROPERTY_GALLERY_VALUE'])) {
    foreach ($arResult['ITEM']['PROPERTY_GALLERY_VALUE'] as $k => $fid) {
        $file = CFile::ResizeImageGet($fid, array('width' => 600, 'height' => 340), BX_RESIZE_IMAGE_EXACT);
        $desc = $arResult['ITEM']['~PROPERTY_GALLERY_DESCRIPTION'][$k];
        $arResult['ITEM']['GALLERY'][] = array('FILE' => $file['src'], 'DESC' => $desc);
    }
}

if (!empty($arResult['ITEM']['PROPERTY_YOUTUBE_VALUE'])) {
    $t1 = parse_url($arResult['ITEM']['PROPERTY_YOUTUBE_VALUE'], PHP_URL_QUERY);
    parse_str($t1, $output);
    if (!empty($output['v']))
        $arResult['ITEM']['PROPERTY_YOUTUBE_CODE'] = $output['v'];

}

if (!empty($arResult['ITEM']['PROPERTY_FILE_VALUE'])) {
    $arResult['ITEM']['PROPERTY_FILE_VALUE'] = CFile::GetPath($arResult['ITEM']['PROPERTY_FILE_VALUE']);
    if (!empty($arResult['ITEM']['PROPERTY_FILE_VALUE']))
        $arResult['ITEM']['PROPERTY_FILE_VALUE_EXT'] = GetFileExtension($arResult['ITEM']['PROPERTY_FILE_VALUE']);
}

$arParams['NEWS_DETAIL_URL'] = str_replace('#CODE#', $arParams['LIBRARY_CODE'], $arParams['NEWS_DETAIL_URL']);

#pre($arResult,1);

$this->IncludeComponentTemplate();

$APPLICATION->SetTitle($arResult['ITEM']['NAME']);
