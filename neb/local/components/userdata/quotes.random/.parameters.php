<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__); 

try
{
	if (!Main\Loader::includeModule('iblock'))
		throw new Main\LoaderException(Loc::getMessage('QUOTES_RANDOM_IBLOCK_MODULE_NOT_INSTALLED'));
	
	$iblockTypes = \CIBlockParameters::GetIBlockTypes(Array("-" => " "));
	
	$iblocks = array(0 => " ");
	if (isset($arCurrentValues['IBLOCK_TYPE']) && strlen($arCurrentValues['IBLOCK_TYPE']))
	{
	    $filter = array(
	        'TYPE' => $arCurrentValues['IBLOCK_TYPE'],
	        'ACTIVE' => 'Y'
	    );
	    $rsIBlock = \CIBlock::GetList(array('SORT' => 'ASC'), $filter);
	    while ($arIBlock = $rsIBlock -> GetNext())
	    {
	        $iblocks[$arIBlock['ID']] = $arIBlock['NAME'];
	    }
	}
	
	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			'USER_ID' => array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('QUOTES_RANDOM_PARAMETERS_USER_ID'),
				'TYPE' => 'STRING',
			),
			'CACHE_TIME' => array(
				'DEFAULT' => 3600
			)
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e -> getMessage());
}
