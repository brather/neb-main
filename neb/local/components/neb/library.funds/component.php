<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arParams['CODE']))
    return false;

if (empty($arParams['IBLOCK_ID']))
    return false;

$arParams['SHOW_ALL_FUNDS'] = (
    isset($_REQUEST['all_funds'])
    && !empty($_REQUEST['all_funds'])
);

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

use Bitrix\NotaExt\Iblock\Element;

$arResult = Element::getList(
    array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'CODE' => $arParams['CODE']),
    1,
    array(
        'ID',
        'PROPERTY_EXALEAD_FILTER',
        'PROPERTY_LIBRARY_LINK',
        'skip_other',
    )
);

if (empty($arResult['ITEM'])) {
    $this->AbortResultCache();
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
    require_once $_SERVER['DOCUMENT_ROOT'] . '/404.php';
    return;
}

if (!empty($arResult['ITEM']['PROPERTY_LIBRARY_LINK_VALUE'])) {
    $arLibExRes = \Neb\Main\Library\LibsTable::getById($arResult['ITEM']['PROPERTY_LIBRARY_LINK_VALUE']);
    $arLibEx = $arLibExRes->fetch();
}

if (empty($arLibEx['UF_ID']))
    return false;

/*
Если данные по библиотеке получены
ищем ее книги
*/

if (!CModule::IncludeModule('nota.exalead'))
    return false;

use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$arParams['ITEM_COUNT'] = intval($_REQUEST['pagen']) > 0 ? intval($_REQUEST['pagen']) : 15;
if ($arParams['ITEM_COUNT'] > 60) $arParams['ITEM_COUNT'] = 60;
$arParams['NEXT_ITEM'] = intval($_REQUEST['next']);
$arParams['MODE'] = empty($_REQUEST['mode']) ? 'list' : trim($_REQUEST['mode']);

$arParams['IS_SHOW_PROTECTED'] = nebUser::isShowProtectedContent();

$by = trim(htmlspecialcharsEx($_REQUEST['by']));
$order = trim(htmlspecialcharsEx($_REQUEST['order']));

$arParams['q'] = '#all AND idlibrary:' . $arLibEx['UF_ID'];
if (!$arParams['SHOW_ALL_FUNDS']) {
    $arParams['q'] .= ' AND filesize > 0';
}

$query = new SearchQuery($arParams['q']);
$query->setPageLimit($arParams['ITEM_COUNT']);
$query->setNavParams();
#if(!empty($arResult['ITEM']['PROPERTY_EXALEAD_FILTER_VALUE']))
#	$query->setParam('r', '+'.$arResult['ITEM']['PROPERTY_EXALEAD_FILTER_VALUE'], true);

if (!empty($by) and !empty($order)) {
    $query->setParam('s', $order . '(' . $by . ')');
}

$query->setParam('sl', 'sl_nofuzzy');

#$arParam = $query->getParameters(); pre($arParam,1);

$client = new SearchClient();
$arSearchResult = $client->getResult($query);

if (intval($arSearchResult['COUNT']) > 0) {
    $client->getDBResult($arSearchResult);
    $arResult['STR_NAV'] = $client->strNav;

    $arResult += $arSearchResult;
}

$this->IncludeComponentTemplate();