<?php
/**
 * User: agolodkov
 * Date: 02.09.2016
 * Time: 16:04
 */

namespace LbcBundle\DependencyInjection;


use Gedmo\Tree\Entity\Repository\ClosureTreeRepository;
use LbcBundle\Controller\AbstractCategoryController;
use LbcBundle\Entity\AbstractLbcCategory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LbcCategory
{

    /**
     * @var ContainerInterface
     */
    private $_container;

    private $_entityCategories
        = [
            'LbcMaindivision'            => 'maindivision',
            'LbcLocationplans'           => 'locationplans',
            'LbcLocationplansections'    => 'locationplansections',
            'LbcSpecialdivision'         => 'specialdivision',
            'LbcSpecialdivisionsections' => 'specialdivisionsections',
        ];

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->_container = $container;
    }

    /**
     * @param AbstractLbcCategory $entity
     *
     * @throws \Exception
     */
    public function prepareEntity(AbstractLbcCategory $entity)
    {
        $controllerService = $this->getEntityControllerService($entity);
        if (!$controllerService instanceof AbstractCategoryController) {
            throw new \Exception('Attempt to prepare undefined entity');
        }
        $controllerService->prepareEntity($entity);
    }

    /**
     * @param AbstractLbcCategory $entity
     *
     * @return AbstractCategoryController
     */
    public function getEntityControllerService(AbstractLbcCategory $entity)
    {
        $controllerService = null;
        if ($category = $this->getEntityCategoryName($entity)) {
            $controllerService = $this->_container->get('app.' . $category);
        }
        if ($controllerService instanceof AbstractCategoryController) {
            return $controllerService;
        }

        return null;
    }

    /**
     * @param string $category
     *
     * @return AbstractCategoryController
     */
    public function getCategoryControllerService($category)
    {
        $controllerService = $this->_container->get('app.' . $category);
        if ($controllerService instanceof AbstractCategoryController) {
            return $controllerService;
        }

        return null;
    }

    /**
     * @param AbstractLbcCategory $entity
     *
     * @return null|string
     */
    public function getEntityCategoryName(AbstractLbcCategory $entity)
    {
        $reflect = new \ReflectionClass($entity);
        $categoryName = null;
        $shortName = $reflect->getShortName();

        return $this->getCategoryNameByEntityName($shortName);
    }

    /**
     * @param $entityName
     *
     * @return null
     */
    public function getCategoryNameByEntityName($entityName)
    {
        if (false !== strpos($entityName, ':')) {
            $entityName = explode(':', $entityName);
            $entityName = end($entityName);
        }

        return isset($this->_entityCategories[$entityName]) ? $this->_entityCategories[$entityName] : null;
    }

    /**
     * @param string $categoryName
     *
     * @return null|\LbcBundle\Controller\Api\AbstractCategoryController
     */
    public function getCategoryApiControllerService($categoryName)
    {
        $serviceId = 'app.api.' . $categoryName;
        if ($this->_container->has($serviceId)) {
            $service = $this->_container->get($serviceId);
            if ($service instanceof \LbcBundle\Controller\Api\AbstractCategoryController) {
                return $service;
            }
        }

        return null;
    }

    /**
     * @param string $category
     *
     * @return ClosureTreeRepository|null
     */
    public function getCategoryRepository($category)
    {
        if ($entityName = $this->getCategoryEntityName($category)) {
            return $this->_container->get('doctrine')->getRepository('LbcBundle:' . $entityName);
        }

        return null;
    }

    /**
     * @param string $category
     *
     * @return null
     */
    public function getCategoryEntityName($category)
    {
        $categoryEntities = array_flip($this->_entityCategories);
        if (isset($categoryEntities[$category])) {
            return $categoryEntities[$category];
        }

        return null;
    }
}