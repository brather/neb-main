<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

\CUtil::InitJSCore(array("jquery"));
$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);

?>

<div class="fieldcell nowrap photorow" id="drag_progressbar2">
	<em class="hint">*</em>
	<label><?=GetMessage("UPLOAD_SCAN_WITH_RESIDENCE");?></label>
	<div class="field">
		<div class="setscan iblock" id="container_<?=$arParams['RAND_STR']?>">
			<div class="field validate setscan-validate">
				<input type="hidden" class="photofile tovalidate readerts-new-file-2" name="scan2" value="<?=(isset($_REQUEST['scan2'])?$_REQUEST['scan2']:'')?>" data-required="true">
			</div>
			<a href="#" id="setphoto_scan2"><?=GetMessage("UPLOAD_IMAGE");?></a>
			<div class="setphoto_lb"><?=GetMessage("DRAG_INTO_FIELD");?></div>
			<div class="uploader-error uploader-err-<?=$arParams['RAND_STR']?>"></div>
			<div class="progressbar" id="progressbar2" style="display: none;">
				<div class="progress">
					<span style="width:0px;"></span>
				</div>
				<div class="text"><?=GetMessage("UPLOADING");?> <span class="num">0</span>%</div>
			</div>
		</div>      
		<div class="b-scan_photo iblock">
			<img src="<?=(isset($_REQUEST['scan2'])?$_REQUEST['scan2']:'/bitrix/images/1.gif')?>" alt=""  id="scan2_photo" height="161">
		</div>
	</div>
</div>
<script>
    /* rulesAndMessages.rules['scan1'] = {
         required: true
    };*/
    rulesAndMessages.messages['scan2'] = {
        required: '<?=GetMessage("ERROR_UPLOAD_SCAN_WITH_RESIDENCE");?>'
    };
</script>

<div style="display: none;" class="pl_button"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>

<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'setphoto_scan2',
				drop_element: "drag_progressbar2",
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'), 
				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
					unique_names: true,
					<?
					}
				?>

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				<?
					if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
					{
					?>
					resize: {
						width: <?=intval($arParams['RESIZE_WIDTH'])?>,
						height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
						<?
							if($arParams['RESIZE_CROP'] == 'Y')
							{
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=intval($arParams['RESIZE_QUALITY'])?>
					},
					<?
					}
				?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '100kb',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>',
					aTHUMBNAIL_HEIGHT : 161,
					aTHUMBNAIL_WIDTH : 226
				},
				multi_selection: false,
				init: {

					FilesAdded: function(up, files) {
						$('.uploader-err-<?=$arParams['RAND_STR']?>').hide();
						up.start();
					},
					FileUploaded: function(up, file, response) {
						var result = response.response;
						if (result) {
							var obResponse = JSON.parse(result);
							$('#scan2_photo').attr('src', obResponse.thumbnail);
							$('input[name="scan2"]').val(obResponse.file);
						}
					},
					UploadProgress: function(up, file) {
						$('#progressbar2').show();
						$('#progressbar2 .progress span').css('width' , file.percent+"%");
						$('#progressbar2 .text .num').text(file.percent);
					},
					Error: function(up, err) {
						if(err.code=='-600'){
							$('.uploader-err-<?=$arParams['RAND_STR']?>').fadeOut();							
							$('.uploader-err-<?=$arParams['RAND_STR']?>').text('Размер загружаемого изображения не должен превышать 10 мегабайт');
							$('.uploader-err-<?=$arParams['RAND_STR']?>').fadeIn();
						}
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
					}
				}
			}
		);

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>