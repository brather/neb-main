<?
$MESS["intervolga.remoteuserclient.MODULE_NAME"] = "Хранение данных о пользователях на другом сайте";
$MESS["intervolga.remoteuserclient.MODULE_DESC"] = "Хранение данных о пользователях на другом сайте";
$MESS["intervolga.remoteuserclient.PARTNER_NAME"] = "ИНТЕРВОЛГА";
$MESS["intervolga.remoteuserclient.PARTNER_URI"] = "http://www.intervolga.ru";
$MESS["intervolga.remoteuserclient.USER_UPLOAD_DATE"] = "Дата последней выгрузки пользователя";
$MESS["intervolga.remoteuserclient.USER_UPLOAD_PREVENT"] = "Не выгружать пользователя";
$MESS["intervolga.remoteuserclient.USER_REMOTE_ID"] = "ID пользователя в удаленной БД";