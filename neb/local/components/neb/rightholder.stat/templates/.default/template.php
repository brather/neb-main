<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
//debugPre($arResult);
?>
<div class="b-addsearch clearfix">
    <!-- <div class="b-search_field left">
        <form method="get">
            <input type="text" name="qbook" class="b-search_fieldtb b-text" id="asearch" value="<?=htmlspecialcharsbx($_REQUEST['qbook'])?>" autocomplete="off">
            <select name="qbooktype" id=""class="js_select b_searchopt b_searchopt-rightholder">
                <option <?if($_REQUEST['qbooktype'] == 'All'):?>selected="selected"<?endif;?> value="All"><?=Loc::getMessage('RIGHTHOLDER_STAT_BY_ALL_FIELDS');?></option>
                <option <?if($_REQUEST['qbooktype'] == 'Author'):?>selected="selected"<?endif;?> value="Author"><?=Loc::getMessage('RIGHTHOLDER_STAT_BY_AUTHOR');?></option>
            </select>
            <input type="submit" class="b-search_bth bbox" value="<?=Loc::getMessage('RIGHTHOLDER_STAT_BY_FIND');?>">
        </form>
    </div> -->
    <div class="b-pravostat right">
        <!--<?if(count($arResult['BOOKS']) > 0):?>
            <?=Loc::getMessage('RIGHTHOLDER_STAT_PERIOD_FROM');?> <input type="text" class="b-tbpravostat" value="<?=$arResult['FIRST_BOOK_DATE']?>"> <?=Loc::getMessage('RIGHTHOLDER_STAT_PERIOD_TO');?> <input type="text" class="b-tbpravostat" value="<?=$arResult['LAST_BOOK_DATE']?>">
            <?=Loc::getMessage('RIGHTHOLDER_STAT_ON_PORTAL');?> <a href="#"><strong><?=count($arResult['BOOKS'])?></strong> <?=GetEnding(count($arResult['BOOKS']), Loc::getMessage('RIGHTHOLDER_STAT_BOOK_5'), Loc::getMessage('RIGHTHOLDER_STAT_BOOK_1'), Loc::getMessage('RIGHTHOLDER_STAT_BOOK_2'))?></a>.
        <?endif;?>-->

        <?if(count($arResult['BOOKS']) > 0):?>
            <?=Loc::getMessage('RIGHTHOLDER_STAT_PERIOD_FROM');?> <?=$arResult['FIRST_BOOK_DATE']?> <?=Loc::getMessage('RIGHTHOLDER_STAT_PERIOD_TO');?> <?=$arResult['LAST_BOOK_DATE']?>
            <?=Loc::getMessage('RIGHTHOLDER_STAT_ON_PORTAL');?> <?=count($arResult['BOOKS'])?> <?=GetEnding(count($arResult['BOOKS']), Loc::getMessage('RIGHTHOLDER_STAT_BOOK_5'), Loc::getMessage('RIGHTHOLDER_STAT_BOOK_1'), Loc::getMessage('RIGHTHOLDER_STAT_BOOK_2'))?>.
        <?endif;?>
    </div>
</div>

<div class="b-fondtable">

    <table class="b-usertable b-fondtb b-fordstats">
        <tr>
            <th class="title_cell"><a <?=SortingExalead("Name")?>><?=Loc::getMessage('RIGHTHOLDER_STAT_NAME')?></a></th>
            <th class="autor_cell"><a <?=SortingExalead("Author")?>><?=Loc::getMessage('RIGHTHOLDER_STAT_AUTHOR')?></a></th>
            <th class="bbk_cell"><a <?=SortingExalead("BBKText")?>><?=Loc::getMessage('RIGHTHOLDER_STAT_BBK')?></a></th>
            <!--<th class="exs_cell"><a <?=SortingExalead("BX_TIMESTAMP")?>><?=Loc::getMessage('RIGHTHOLDER_STAT_DATE')?></a></th>
            <th class="exs_cell"><span><?=Loc::getMessage('RIGHTHOLDER_STAT_LINK')?></span></th>-->
            <th class="date_cell"><a <?=SortingExalead("VIEW_COUNT")?>><?=Loc::getMessage('RIGHTHOLDER_STAT_VIEWERS')?></a> </th>
            <th class="user_cell"><a <?=SortingExalead("READ_COUNT")?>><?=Loc::getMessage('RIGHTHOLDER_STAT_READERS')?></a> </th>
        </tr>
        
        <?foreach($arResult['BOOKS'] as $sBook => $arBook):
            $arStat = $arResult['STATISTICS'][$sBook];?>
            <tr>
                <td class="pl15">
                    <a href="/catalog/<?=$arBook['id']?>/"><?=$arBook['title']?></a>
                </td>
                <td class="pl15">
                    <?=$arBook['authorbook']?>
                </td>
                <td class="pl15">
                    <?=$arBook['bbk']?>
                </td>
                <!--<td class="pl15">
                    <?if (!empty($arStat['DATE'])):?>
                        <?=$arStat['DATE']?>
                    <? endif; ?>
                </td>
                <td class="pl15">
                    <?if (!empty($arStat['PDF'])):?>
                        <span class="nowrap"> <a href="<?=$arStat['PDF']?>" class="b-viewpdf"><?=Loc::getMessage('RIGHTHOLDER_STAT_VIEW')?></a></span>
                    <? endif; ?>
                </td>-->
                <td class="pl15">
                    <?if($arResult['VIEW'][$sBook] > 0):?>
                        <a class="popup_opener ajax_opener closein" href="<?=$this->__folder?>/graph_json.php?ACTION=VIEW&BOOK_ID=<?=$arStat["ID"]?>&TITLE=<?=$arStat['NAME']?>" data-graph-x="Март,Апр,Май,Июнь,Июль,Авг,Сент,Окт,Ноя,Дек,Янв" data-graph-y ="Просмотры" data-graph-name="<?=$arStat['NAME']?>"
                           data-graph-title="Просмотры" data-width="728" data-posmy="center-345 top-230" data-posat="center right"><?=$arResult['VIEW'][$sBook]?></a>
                    <?else:?>
                        <?=$arResult['VIEW'][$sBook]?>
                    <?endif;?><br />
                    <span class="datein">
                        <?=$arStat['VIEW_LAST_VIEW']?>
                    </span>
                </td>
                <td class="pl15">
                    <?if($arResult['READ'][$sBook] > 0):?>
                        <a class="popup_opener ajax_opener closein" href="<?=$this->__folder?>/graph_json.php?BOOK_ID=ACTION=READ&<?=$arStat["ID"]?>&TITLE=<?=$arStat['NAME']?>" data-graph-x="Март,Апр,Май,Июнь,Июль,Авг,Сент,Окт,Ноя,Дек,Янв" data-graph-y ="Прочтения" data-graph-name="<?=$arStat['NAME']?>"
                           data-graph-title="Прочтения" data-width="728" data-posmy="center-345 top-230" data-posat="center right"><?=$arResult['READ'][$sBook]?></a>
                    <?else:?>
                        <?=$arResult['READ'][$sBook]?>
                    <?endif;?>
                </td>
            </tr>
        <?endforeach;?>
        
    </table>
</div>