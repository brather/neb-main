<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Вывод всех заявок
 */

?>
<p>
    <a href='<?=$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['statistics_total']?>'>Статистика</a>
</p>

<?
$APPLICATION->IncludeComponent("neb:feedback.list",
    ".default",
    array(
        "COMPONENT_TEMPLATE"   => ".default",
        'FEEDBACK_URL'         => $arParams['SEF_FOLDER'],
        'FEEDBACK_APPOINT_URL' => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['appoint'],
        'FEEDBACK_REPLY_URL'   => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['reply'],
        'FEEDBACK_REJECT_URL'  => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['reject'],
        'FEEDBACK_HISTORY_URL' => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['history']
    ),
    $component
);