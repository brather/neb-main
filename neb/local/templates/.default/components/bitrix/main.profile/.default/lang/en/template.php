<?
$MESS ['CHANGE_PASSW'] = "Change Password";
$MESS ['NEW_PASS'] = "New password";
$MESS ['PASS_TWO'] = "Confirm new password";
$MESS ['CONFIRM'] = "Confirm";
$MESS ['GENERAL_CONST'] = "General settings";
$MESS ['NAME'] = "Name";
$MESS ['LAST_NAME'] = "Last name";
$MESS ['SECOND_NAME'] = "Second name";
$MESS ['MEN'] = "male";
$MESS ['WOMEN'] = "female";
$MESS ['DATE'] = "Birthday";
$MESS ['DAY'] = "day";
$MESS ['MONTH'] = "month";
$MESS ['YEAR'] = "year";
$MESS ['ABOUT'] = "A little about yourself";
$MESS ['SETTINGS'] = "Search settings";
$MESS ['COUNT_SEARCH'] = "Number <br />of search results";
$MESS ['MAX_COUNT'] = "The maximum number <br />of documents on request";
$MESS ['SAVE'] = "Save";
$MESS ['SETTING'] = "Profile settings";
$MESS ['MAIL'] = "E-mail/ Login/ SECL";
$MESS ['POL'] = "Sex";
$MESS ['ERROR_DATE_FORMAT'] = "Invalid date format";
$MESS ['CHOSE_DATE'] = "Select date";
$MESS ['12_OLD'] = "You must be <br />at least 12 years of age";
$MESS ['REQUIRED_PASS'] = "The field is required";
$MESS ['NOT_MATCH'] = "Password mismatch";

$MESS["MAIN_REGISTER_REQ"] = "Required";
$MESS["MAIN_REGISTER_FILL"] = "Must be filled in";
$MESS["MAIN_REGISTER_MORE_PLACEHOLDER_SYMBOLS"] = "Must be not more {0} characters long";
$MESS["MAIN_REGISTER_LESS_PLACEHOLDER_SYMBOLS"] = "Must be at least {0} characters long";
$MESS["MAIN_REGISTER_AGE_WITH_PLACEHOLDER"] = 'You must be atleast {0} years old';
?>