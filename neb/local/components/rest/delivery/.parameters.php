<?php
$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => Array(
            'processRequest' => array(
                'DEFAULT'   => '/',
                'VARIABLES' => array(
                    'token',
                    'action',
                ),
                'TYPE'      => array('GET'),
            ),
        ),
    ),
);