<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Page\Asset;

class FaqEditor extends CBitrixComponent {
    //region Задание свойств для класса
    private $page = 'template';
    private $sNameTextArea = '';
    private $sIdTextArea = '';
    private $sEditorID = '';
    private $iTextareaWidth = '';
    private $iTextareaHeight = '';
    private $sText = '';
    private $sScript = '';
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        $this->includeComponentLang(basename(__FILE__));
    }
    //endregion
    //region Исполнение компонента
    /**
     * Исполнение компонента
     */
    public function executeComponent() {
        $this->_getFilesEditor();
        $this->_setParamsForEditor();
        $this->_setResultParams();
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Задание массива параметров в результирующем выводе
    /**
     * Задание массива параметров в результирующем выводе
     */
    private function _setResultParams() {
        $this->_setScriptsEditor();
        $this->arResult = array(
            "SCRIPT" => $this->sScript,
            "ID" => $this->sIdTextArea,
            "CLASS" => $this->sEditorID,
            "NAME" => $this->sNameTextArea,
            "WIDTH" => $this->iTextareaWidth,
            "HEIGHT" => $this->iTextareaHeight,
            "TEXT" => $this->sText,
        );
    }
    //endregion
    //region Подключение файлов редактора
    /**
     * Подключение файлов редактора
     */
    private function _getFilesEditor() {
        Asset::getInstance()->addJs('/local/templates/adaptive/vendor/tinymce/tinymce.min.js');
    }
    //endregion
    //region Задание параметров редактора
    /**
     * Задание параметров редактора
     */
    private function _setParamsForEditor() {
        $this->sNameTextArea = (isset($this->arParams['TEXTAREA_NAME']) == false) ? 'content' : $this->arParams['TEXTAREA_NAME'];
        $this->sIdTextArea = (isset($this->arParams['TEXTAREA_ID']) == false) ? '' : $this->arParams['TEXTAREA_ID'];
        if ('' == trim($this->sIdTextArea)) {
            $this->sIdTextArea = 'content';
        }
        $this->sEditorID = (isset($this->arParams['INIT_ID']) == false) ? 'textareas' : $this->arParams['INIT_ID'];
        $this->iTextareaWidth = (isset($this->arParams['TEXTAREA_WIDTH']) == false) ? '100%' : $this->arParams['TEXTAREA_WIDTH'];
        $this->iTextareaHeight = (isset($this->arParams['TEXTAREA_HEIGHT']) == false) ? '300' : $this->arParams['TEXTAREA_HEIGHT'];
        $this->sText = (isset($this->arParams['TEXT']) == false) ? '' : $this->arParams['TEXT'];
    }
    //endregion
    //region Задание скриптов для редактора
    /**
     * Задание скриптов для редактора
     */
    private function _setScriptsEditor() {
        $menubar = '';
        $toolbar = 'undo redo | fontsizeselect | bold italic underline | alignleft aligncenter alignright | bullist numlist | link unlink | image table | forecolor backcolor | fullscreen';
        $plugins = 'hr,fullpage,fullscreen,link,insertdatetime,paste,table,image,emoticons,preview,textcolor';
        if($this->arParams["TYPE_EDITOR"] == "TYPE_2") {
            $menubar = 'file edit insert view format table tools';
            $plugins = 'advlist,anchor,autolink,autoresize,autosave,bbcode,charmap,code,codesample,colorpicker,contextmenu,directionality,emoticons,example,fullpage,fullscreen,hr,image,imagetools,importcss,insertdatetime,layer,legacyoutput,link,lists,media,nonbreaking,noneditable,pagebreak,paste,preview,print,save,searchreplace,spellchecker,tabfocus,table,template,textcolor,textpattern,visualblocks,visualchars,wordcount';
            $toolbar = 'undo redo | save cancel | newdocument | bold italic underline strikethrough | ltr rtl | alignleft aligncenter alignright alignnone | styleselect formatselect fontselect fontsizeselect | cut copy paste | bullist numlist | table tabledelete tablecellprops tablemergecells tablesplitcells tableinsertrowbefore tableinsertrowafter tabledeleterow tablerowprops tableinsertcolbefore tableinsertcolafter tabledeletecol quicktable | rotateleft rotateright | flipv fliph | outdent indent | blockquote | removeformat subscript superscript visualaid | hr link unlink quicklink | image editimage imageoptions quickimage | media charmap | pastetext | print preview | anchor pagebreak nonbreaking spellchecker searchreplace visualblocks visualchars code fullscreen fullpage insertdatetime emoticons template restoredraft insertfile | forecolor backcolor';
        }
        $this->sScript .= '
            <script type="text/javascript">        
                tinyMCE.init ({
                    language                            : "ru",
                    mode                                : "textareas",
                    plugins                             : "' . $plugins . '",
                    toolbar                             : "' . $toolbar . '",
                    menubar                             : "' . $menubar . '",
                    theme                               : "modern",                 
                    resize                              : false,
                    skin                                : "lightgray",
                    editor_selector                     : "' . $this->sEditorID . '",                
                    width                               : "' . $this->iTextareaWidth . '",
                    height                              : "' . $this->iTextareaHeight . '",
                    spellchecker_languages              : "+Русский=ru,English=en",
                    spellchecker_word_separator_chars   : \'\\s!\"#$%&()*+,-./:;<=>?@[\]^_{|}\'
                });
            </script>';
    }
    //endregion
}