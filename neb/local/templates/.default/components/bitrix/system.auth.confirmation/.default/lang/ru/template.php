<?
$MESS ['CT_BSAC_CONFIRM'] = "Подтвердить";
$MESS ['CT_BSAC_LOGIN'] = "Логин";
$MESS ['CT_BSAC_CONFIRM_CODE'] = "Код подтверждения";

$MESS['ACCOUNT_ACTIVATION'] = 'Активация учетной записи.';
$MESS['USER_NOT_FOUND'] = 'Пользователь не найден.';
$MESS['SUCCESSFULLY_AUTHORIZED'] = 'Вы были успешно авторизованы.';
$MESS['READY_CONFIRM'] = 'Учетная запись уже активирована.';
$MESS['MISSED_CODE'] = 'Не указан код подтверждения.';
$MESS['WRONG_CODE'] = 'Неверный код подтверждения.';
$MESS['SUCCESSFUL_CONFIRMATION'] = 'Учетная запись успешно активирована.';
$MESS['SOME_ERROR'] = 'Произошла непредвиденная ошибка.';