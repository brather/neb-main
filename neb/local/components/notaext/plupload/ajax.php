<?
if (empty($_SERVER['HTTP_REFERER']))
    die('refError');

define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if ($_POST['plupload_ajax'] == 'Y' and check_bitrix_sessid()) {
    $arPatch = parse_url($_SERVER['HTTP_REFERER']);
    if (!empty($arPatch['path']))
        $strAccessPermission = $APPLICATION->GetFileAccessPermission($arPatch['path']);

    if ($strAccessPermission < 'R')
        die('Error AccessPermission');

    $APPLICATION->IncludeComponent('notaext:plupload', 'admin',
        array(
            'FILE_TYPES' => $_REQUEST['aFILE_TYPES'],
            'DIR' => rtrim($_REQUEST['aDIR'], '/'),
            'MAX_FILE_SIZE' => $_REQUEST['aMAX_FILE_SIZE'],
            'MAX_FILE_AGE' => $_REQUEST['aMAX_FILE_AGE'],
            'FILES_FIELD_NAME' => $_REQUEST['aFILES_FIELD_NAME'],
            'MULTI_SELECTION' => $_REQUEST['aMULTI_SELECTION'],
            'CLEANUP_DIR' => $_REQUEST['aCLEANUP_DIR'],
            'RAND_STR' => $_REQUEST['aRAND_STR'],
            'THUMBNAIL_HEIGHT' => $_REQUEST['aTHUMBNAIL_HEIGHT'],
            'THUMBNAIL_WIDTH' => $_REQUEST['aTHUMBNAIL_WIDTH'],
        )
    );
}