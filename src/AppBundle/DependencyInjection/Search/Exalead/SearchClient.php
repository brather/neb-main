<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 12:14
 */

namespace AppBundle\DependencyInjection\Search\Exalead;


use AppBundle\DependencyInjection\Search\AbstractSearchClient;
use BitrixBundle\DependencyInjection\BitrixHelper;
use GuzzleHttp\Client;

class SearchClient extends AbstractSearchClient
{
    const DEFAULT_TIMEOUT = 30;
    private $_bitrix;
    private $_ip;
    private $_port;
    private $_timeout;

    /**
     * @param BitrixHelper $bitrix
     */
    public function __construct(BitrixHelper $bitrix)
    {
        $this->_bitrix = $bitrix;
        if (!$bitrix->checkIncluded()) {
            $bitrix->requireProlog();
        }
        $this->_ip = $bitrix->getOption('nota.exalead', 'exalead_ip');
        $this->_port = $bitrix->getOption('nota.exalead', 'exalead_port');
        $this->_timeout = static::DEFAULT_TIMEOUT;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public function getResult($url)
    {
        $http = new Client(
            [
                'base_uri' => 'http://' . $this->getIp() . ':' . $this->getPort(),
                'timeout'  => $this->getTimeout(),
            ]
        );
        $response = $http->request('POST', $url);

        return $response->getBody()->getContents();
    }

    /**
     * @return bool|null|string
     */
    public function getIp()
    {
        return $this->_ip;
    }

    /**
     * @param bool|null|string $ip
     */
    public function setIp($ip)
    {
        $this->_ip = $ip;
    }

    /**
     * @return bool|null|string
     */
    public function getPort()
    {
        return $this->_port;
    }

    /**
     * @param bool|null|string $port
     */
    public function setPort($port)
    {
        $this->_port = $port;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->_timeout;
    }

    /**
     * @param int $timeout
     */
    public function setTimeout($timeout)
    {
        $this->_timeout = $timeout;
    }
}