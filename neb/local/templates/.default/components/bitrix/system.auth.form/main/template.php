<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('popup'));

?>

<?if($_GET['PASSWORD_CHANGED'] == 'Y'):?>
    <!--Этот параметр подставляется после успешного изменения пароля пользователем.-->
    <script>
        $(function() {
            $('.b_login_popup').addClass('change_log_pass');
            $('.b-headernav_login_lnk').trigger('click');
        });
    </script>
<?endif;?>


	<?
		if($arResult["FORM_TYPE"] == "login")
		{
		?>
        <div class="b-headernav_login right clearfix">
		<a href="#" class="b-headernav_login_lnk <?=strpos($APPLICATION->GetCurPage(), '/registration') !== false ? 'current':''?> js_register" data-width="255" data-height="170" title="Регистрация"><?=Loc::getMessage('AUTH_REISTER_BUTTON');?></a>

		<div class="b_register_popup b_login_popup" data-class="clone-register-popup">
			<div class="b_login_popup_in bbox">
				<h4><?=Loc::getMessage('AUTH_REISTER_AS');?></h4>
				<a href="<?=$arResult['AUTH_REGISTER_URL']?>" class="formbutton" title="Регистрация как читатель"><?=Loc::getMessage('AUTH_READER');?></a>
			</div>
		</div> 	<!-- /.b_login_popup -->

		<a href="#" id="login-link" class="b-headernav_login_lnk <?=strpos($APPLICATION->GetCurPage(), '/registration') === false ? 'current':''?> popup_opener" data-height="320"  data-width="550" title="Войти"><?=Loc::getMessage('AUTH_LOGIN_BUTTON')?></a> <!-- data-width="550"  -->

		<div class="b_login_popup popup clearfix" align="center" data-class="clone-login-popup">
			<div class="b_login_popup_in bbox clearfix">
				<h4><?=Loc::getMessage('AUTH_TITLE')?></h4>
				<?/*<div class="b-login_txt hidden"><a href="#">вход для библиотек</a> <a href="#">для правообладателей</a></div>*/?>

				<div class="b-loginform iblock rel b-loginform-column">
                    <p class="change_pass_mess"><?=Loc::getMessage('AUTH_PASSWORD_CHANGED')?></p>
					<form action="<?=$arResult["AUTH_URL"]?>" class="b-form b-formlogin" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

						<?if($arResult["BACKURL"] <> '') {?>
							<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
							<?}?>

						<?foreach ($arResult["POST"] as $key => $value) {?>
							<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
							<?}?>

						<input type="hidden" name="AUTH_FORM" value="Y" />
						<input type="hidden" name="TYPE" value="AUTH" />
						<input type="hidden" name="USER_REMEMBER" value="Y" />

						<div class="fieldrow nowrap">
							<div class="fieldcell iblock login-mail">
								<label for="settings11"><?=Loc::getMessage('AUTH_MAIL')?></label>
								<div class="field validate">
									<input type="text" data-required="required" value="<?=$_REQUEST['USER_LOGIN']?>" id="settings11" data-maxlength="50" data-minlength="2" name="USER_LOGIN" class="input">
								</div>
							</div>

						</div>
						<div class="fieldrow nowrap">

							<div class="fieldcell iblock pass">
								<label for="settings10"><?=Loc::getMessage('AUTH_PASS')?></label>
								<div class="field validate">
									<input type="password" data-required="required" value="<?=$_REQUEST['USER_PASSWORD']?>" id="settings10" data-maxlength="30" data-minlength="2" name="USER_PASSWORD" class="input">
								</div>
							</div>
						</div>
						<em class="error_message">
						<?
							if(!empty($arResult['ERROR_MESSAGE']['MESSAGE']) ||
								(is_string($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE'])))
							{
								if(is_array($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']['MESSAGE']))
								{
									$arResult['ERROR_MESSAGE'] = $arResult['ERROR_MESSAGE']['MESSAGE'];
								}
								?>
								<?=$arResult['ERROR_MESSAGE'];?>
								<script type="text/javascript">
									$(function() {
										$('#login-link').click();
										$('.b_login_popup .error_message').show();
									});
								</script>
								<?
							}
						?>
						</em>
						<div class="fieldrow nowrap fieldrowaction">
							<div class="fieldcell ">
								<div class="field clearfix">
									<button class="formbutton left" value="1" type="submit"><?=Loc::getMessage('AUTH_LOGIN_BUTTON')?></button>
									<a href="#" class="formlink right js_passrecovery" title="Забыли пароль?"><?=Loc::getMessage('AUTH_FORGOT_PASS')?></a>
								</div>
							</div>
						</div>
					</form>

					<div class="b-passrform">
						<a href="#" class="closepopup"><?=Loc::getMessage('AUTH_CLOSE')?></a>
						<form action="/auth/?forgot_password=yes" method="post" class="b-passrecoveryform b-form">
							<input type="hidden" name="AUTH_FORM" value="Y">
							<input type="hidden" name="TYPE" value="SEND_PWD">
							<input type="hidden" name="backurl" value="/">
							<input type="hidden" name="send" value="1">
							<input type="hidden" name="IS_AJAX" value="Y">

							<div class="fieldrow nowrap">
								<div class="fieldcell iblock">
									<div class="b-warning hidden"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND')?></div>
									<label for="settings11"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC')?></label>
									<div class="field validate">
										<input type="text" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="50" data-minlength="2" name="USER_EMAIL" class="input">
										<em class="error required"><?=Loc::getMessage('AUTH_PASS_RECOVERY_REQUIRED_FIELD')?></em>
									</div>
								</div>
								<div class="fieldrow nowrap fieldrowaction">
									<div class="fieldcell ">
										<div class="field clearfix">
											<button class="formbutton js-recovery-submit" value="1" type="submit"><?=Loc::getMessage('AUTH_PASS_RECOVERY_RECOVER')?></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<p class="b-passconfirm hidden"><?=Loc::getMessage('AUTH_PASS_RECOVERY_SUCCESS')?></p>
					</div>
				</div>

				<!-- /.b-loginform -->
				<!-- /.b-login_social -->
			
				<div class="b-loginform-column">
					<div class="socials">
						<label><?=Loc::getMessage('AUTH_SOCSERVICES')?></label>
					    <div class="social-wrap">
					        <? foreach ($arResult['AUTH_SERVICES'] as $authService):
					        	if ($authService['ID'] == 'Esia')
					        		continue;
				        		?>
					            <a href="javascript:void(0)" 
					            	title="<?=$authService['NAME']?>"
					            	onclick="<?=$authService['AUTH_LINK']?>"
					            	class="<?=$authService['CSS_CLASS']?> <?=strtolower($authService['ICON'])?>"></a>
					        <? endforeach; ?>
					    </div>
					</div>
					<? if (!empty($arResult['AUTH_SERVICES']['Esia'])): ?>
						<div class='esia-login'>
							<a href="javascript:void(0)" title="Авторизация через портал госуслуг"
							onclick="BX.util.popup('/WebServices/Esia.php?act=do_login', 800, 600)"
							class="esialink esia clearfix">
								<img src="/local/templates/.default/markup/i/esia-logo-2.png" alt="" />
								<label><?=Loc::getMessage('AUTH_ESIA')?></label>
							</a>
						</div>
					<? endif; ?>
					<div class='rgb-login'>
						<a href="javascript:void(0)" title="Авторизация для читателей РГБ" 
						onclick="BX.util.popup('/auth/rgb.php?redirect=1&show_errors=1', 800, 600);" 
						class="rgblink clearfix">
							<img src="/local/templates/.default/markup/i/rgb-logo.png" alt="" />
							<label><?=Loc::getMessage('AUTH_RGB')?></label>
						</a>
					</div>
				</div>

			</div>

		</div> 	<!-- /.b_login_popup -->
        </div>
		<?
		}
		else
		{
		?>
            <header>
                <div class="container lk-header">
                    <div class="row">
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <a href="/">
                                        <img style="height:50px;" src="/local/templates/adaptive/img/logo-header.png" alt="" class="lk-header__logo">
                                    </a>
                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-12">
                                    <img style="height:50px;" src="<?if ($arResult['USER_PHOTO']):?><?=$arResult['USER_PHOTO']?><?else:?>/local/templates/adaptive/img/default_avatar.png<?endif;?>" alt="" class="lk-header__avatar-img">
                                    <p class="lk-header__avatar-text">Добрый день, <?=$arResult["USER_NAME"]?>!<br /> Вы зашли как <?=Loc::getMessage('ROLE_'.strtoupper($arResult["ROLE"]))?>.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-3 lk-header__link-block">
                            <!--a href="" class="lk-header__link lk-header__link--info"></a-->
                            <a href="/profile/edit/" class="lk-header__link lk-header__link--settings"></a>
                            <a href="/?logout=yes" class="lk-header__link lk-header__link--logout"></a>
                        </div>
                    </div>
                </div>
            </header>
            <div class="container">
                <h1 class="lk-title">Личный кабинет <?=Loc::getMessage('TITLE_'.strtoupper($arResult["ROLE"]))?></h1>
            </div>
		<?
		}

		if($_REQUEST['login'] == 'yes' and $arResult['ERROR'])
		{
		?>
		<script>
			$(function(){
				$('#auth_login').click();
			});
		</script>
		<?
		}
	?>

