<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$arRequest = $component->getRequest();

$APPLICATION->SetTitle(Loc::getMessage('FEEDBACK_HISTORY_TITLE') . $arRequest['id']);

if ($arRequest['ajax'] == 'Y')
    $component->restartBuffer();

?>

<!--<br /><a href="#"><?=Loc::getMessage('FEEDBACK_HISTORY_ORDER')?></a><br /><br />-->

<? foreach($arResult['ERROR'] as $sError): ?>
    <?ShowError($sError);?>
<? endforeach; ?>

<? if (!empty($arResult['ITEMS'])): ?>
    <div style="width: 750px;">
        <dl class="responsive-table-layout responsive-hover feedback-history">
            <dt>
                <span><?=Loc::getMessage('FEEDBACK_HISTORY_DATE')?></span>
                <span><?=Loc::getMessage('FEEDBACK_HISTORY_STATUS')?></span>
                <span><?=Loc::getMessage('FEEDBACK_HISTORY_WHO_CHANGED')?></span>
            </dt>
            <? foreach ($arResult['ITEMS'] as $arItem): ?>
                <dd>
                    <span><?=$arItem['DATE']?></span>
                    <span><?=$arItem['STATUS']?></span>
                    <span><?=trim($arItem['USER']['LAST_NAME'] . ' ' . $arItem['USER']['NAME']
                        . ' ' . $arItem['USER']['SECOND_NAME']) . ' [' . $arItem['USER']['ID'] . ']'?></span>
                </dd>
            <? endforeach; ?>
        </dl>
        <hr />
    </div>
<? endif; ?>

<?if ($arRequest['ajax'] == 'Y')
    die();