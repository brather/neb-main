<?php
$MESS = array(
    'GRAPHIC_USERS_TITLE'                     => 'Зарегистрированные пользователи',
    'GRAPHIC_ACTIVE_USERS_TITLE'              => 'Активные пользователи',
    'GRAPHIC_PUBLICATIONS_TITLE'              => 'Изданий',
    'GRAPHIC_PUBLICATIONS_DD_TITLE'           => 'Изданий (дедуплицированно)',
    'GRAPHIC_SEARCH_COUNT_TITLE'              => 'Поисковых запросов',
    'GRAPHIC_VIEWS_TITLE'                     => 'Просмотров страниц',
    'GRAPHIC_DOWNLOADS_TITLE'                 => 'Скачиваний',
    'GRAPHIC_FEEDBACK_COUNT_TITLE'            => 'Обращений в обратую связь',
    'GRAPHIC_VIEWS_BOOK_TITLE'                => 'Просмотры карточек изданий',
    'GRAPHIC_PUBLICATIONS_DIGITIZED_TITLE'    => 'Оцифрованных изданий',
    'GRAPHIC_PUBLICATIONS_DIGITIZED_DD_TITLE' => 'Оцифрованных изданий (дедуплицированно)',
);