<?php
namespace Bitrix\Posting;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class FileTable
 *
 * Fields:
 * <ul>
 * <li> POSTING_ID int mandatory
 * <li> FILE_ID int mandatory
 * </ul>
 *
 * @package Bitrix\Posting
 **/

class FileTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_posting_file';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'POSTING_ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'title' => Loc::getMessage('FILE_ENTITY_POSTING_ID_FIELD'),
            ),
            'FILE_ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'title' => Loc::getMessage('FILE_ENTITY_FILE_ID_FIELD'),
            ),
        );
    }
}