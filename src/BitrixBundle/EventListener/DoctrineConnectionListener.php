<?php
/**
 * User: agolodkov
 * Date: 03.10.2016
 * Time: 18:00
 */

namespace BitrixBundle\EventListener;


use BitrixBundle\Annotations\IncludeBitrix;
use BitrixBundle\Db\DBAL\ConnectionWrapper;
use BitrixBundle\Db\DBAL\Driver\Mysqli\MysqliConnection;
use BitrixBundle\DependencyInjection\BitrixHelper;
use BitrixBundle\Traits\AnnotationReaderTrait;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\DBAL\Event\ConnectionEventArgs;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class DoctrineConnectionListener
{
    use AnnotationReaderTrait;

    /**
     * @var BitrixHelper
     */
    private $_bitrix;

    /**
     * @param CachedReader $reader
     * @param BitrixHelper $bitrix
     */
    public function __construct(
        CachedReader $reader,
        BitrixHelper $bitrix,
        ContainerInterface $container,
        ControllerNameParser $parser
    ) {
        $this->_reader = $reader;
        $this->_bitrix = $bitrix;
        $this->_container = $container;
        $this->_parser = $parser;
    }

    /**
     * Берет битриксовое соединение с базой данных для того, чтобы не создавать новое
     * Если в методе контроллера задана аннтация @IncludeBitrix то берется mysqli соединение из битрикса
     *
     * @param ConnectionEventArgs $event
     */
    public function preConnect(ConnectionEventArgs $event)
    {
        $requestStack = $this->_container->get('request_stack');
        $request = $requestStack->getCurrentRequest();
        if ($request instanceof Request) {
            $controller = $request->attributes->get('_controller');
            $annotations = $this->getControllerMethodAnnotations($controller);
            foreach ($annotations as $configuration) {
                if ($configuration instanceof IncludeBitrix) {
                    $this->_applyBitrixConnection($event);
                }
            }
        } else {
            $this->_bitrix->requireProlog();
            $this->_applyBitrixConnection($event);
        }
    }

    /**
     * @param ConnectionEventArgs $event
     */
    private function _applyBitrixConnection(ConnectionEventArgs $event)
    {
        /** @var ConnectionWrapper $connection */
        $connection = $event->getConnection();
        $connectionName = '';
        $params = $connection->getParams();
        if (isset($params['driverOptions']['connection_name'])) {
            $connectionName = $params['driverOptions']['connection_name'];
        }
        $dbConnection = $this->_bitrix->getDbConnection($connectionName);
        $mysqli = $dbConnection->getResource();
        if ($mysqli instanceof \mysqli) {
            if(isset($params['charset'])) {
                $mysqli->set_charset($params['charset']);
            }
            $connection->setConn(new MysqliConnection($mysqli));
            $connection->setIsConnected(true);
        }
    }
}