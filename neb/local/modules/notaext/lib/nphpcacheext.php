<?
namespace Bitrix\NotaExt;
/*
* Класс для работы с кэшом.
* Полезен для того, чтобы упростить работу с тегированным кэшом в компонентах.
*/

class NPHPCacheExt
{

    private $obCache = null;

    public $tag;
    public $cache_dir;

    /*
    * @method  InitCache инициализируем объект кеширования с заданными параметрами
    * @param string $object_name имя функции результат которой нужно поместить в кешь, может быть использовано (__function__ или __CLASS__.__function__)
    * @param array $params параметры которые участвуют в формировании кеша, можно получить с помощью func_get_args();
    * @param string|array $tagKey масив или единичное значение тегов, для задания тегированного кеша
    * @param integer $cache_life_time время жизни кеша
    */
    public function InitCache($object_name, $params = array(), $tagKey = '', $cache_life_time = 3600)
    {
        $object_name = str_replace('\\', '/', $object_name);
        $cache_id = md5(serialize($params) . $object_name . serialize($tagKey));

        $this->tag = array();
        if (!empty($tagKey)) {
            if (is_array($tagKey))
                $this->tag = $tagKey;
            else
                $this->tag[] = $tagKey;
        }

        if (isset($params['iblock_id']) and !is_array($params['iblock_id']))
            $this->tag[] = 'iblock_id_' . $params['iblock_id'];
        /* Если  iblock_id	 передается в виде массива */
        elseif (isset($params['iblock_id']) and is_array($params['iblock_id']))
            foreach ($params['iblock_id'] as $iblock_id)
                $this->tag[] = 'iblock_id_' . $iblock_id;

        $this->cache_dir = (defined('CACHE_DIRECTORY') ? CACHE_DIRECTORY : '/cache_function') . '/' . $object_name;

        $this->obCache = new \CPHPCache;

        return $this->obCache->InitCache($cache_life_time, $cache_id, $this->cache_dir);

    }

    /*
    * @method GetVars Получаем содержимое кеша
    */
    function GetVars()
    {
        return $this->obCache->GetVars();
    }

    /**
     * @method StartDataCache Сохранение кэша с возможностью до добавить теги.
     * @param array $arValues - массив со значениями, которые нужно закэшировать.
     * @param array|boolean $tags - строка или массив с доп. тегами для кэша.
     */
    public function StartDataCache($arValues = array(), $tags = false)
    {

        if (!$this->obCache->StartDataCache())
            return false;

        if ($tags !== false) {
            if (!is_array($tags))
                $tags = array($tags);

            if (!empty($this->tag))
                $this->tag = array_merge($this->tag, $tags);
            else
                $this->tag = $tags;
        }

        if (!empty($this->tag)) {
            global $CACHE_MANAGER;

            $CACHE_MANAGER->StartTagCache($this->cache_dir);

            foreach ($this->tag as $tag)
                $CACHE_MANAGER->RegisterTag($tag);

            $CACHE_MANAGER->EndTagCache();
        }

        $this->obCache->EndDataCache($arValues);
    }
}

/*
#Пример!!!!
function testFunction($arg1,$arParams)
{
$arResult = array();

use Bitrix\NotaExt\NPHPCacheExt;

$obCacheExt    	= new NPHPCacheExt();
$arCacheParams 	= func_get_args();
$arCacheParams['iblock_id'] = $arParams['IBLOCK_ID'];  # если в параметрах указывается ID инфоблока
$cacheTag 	   	= "tag";

if ( !$obCacheExt->InitCache(__function__, $arCacheParams, $cacheTag) )
{

$arResult['TIME'] = time();
$arResult['ARG1'] = $arg1;
$arResult['ARPARAMS'] = $arParams;

$obCacheExt->StartDataCache(
$arResult,
'tag2_'.$arg1
);
}
else
{
$arResult = $obCacheExt->GetVars();
}

return $arResult;
}

$arResult =  testFunction(456, array('a' => 1, 'b' => 2, 'IBLOCK_ID' => 5));
pre($arResult, 1);

# очищаем кешь по тегу
#$CACHE_MANAGER->ClearByTag("iblock_id_5");
#$CACHE_MANAGER->ClearByTag("tag2_456");
#$CACHE_MANAGER->ClearByTag((defined('CACHE_DIRECTORY') ? CACHE_DIRECTORY : '/cache_function') . "/testFunction");
*/
