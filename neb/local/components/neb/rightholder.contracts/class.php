<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * Class RightholderContracts
 */
class RightholderContracts extends CBitrixComponent
{
    private $componentPage = 'list', $contractId = 0;
    
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        // ЧПУ
        $arDefaultUrlTemplates404 = array(
            "list" => "",
            "detail" => "#ID#/"
        );

        $arDefaultVariableAliases404 = array();
        $arDefaultVariableAliases = array();
        $arComponentVariables = array("ID");

        $SEF_FOLDER = "";
        $arUrlTemplates = array();

        if ($arParams["SEF_MODE"] == "Y")
        {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

            $componentPage = CComponentEngine::parseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

            if (strlen($componentPage) <= 0) $componentPage = "list";

            CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

            $SEF_FOLDER = $arParams["SEF_FOLDER"];

        } else {

            $arVariables = array();

            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
            CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

            $componentPage = "";
            if (IntVal($arVariables["ELEMENT_ID"]) > 0) {
                $componentPage = "detail";
            } else {
                $componentPage = "list";
            }
        }

        $this->componentPage = $componentPage;
        $this->contractId = intval($arVariables['ID']);

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->_getData();
        $this->_setTitle();

        $this->includeComponentTemplate($this->componentPage);
    }

    private function _getData() {

        $obRightHolder = new \RightHolder($this->contractId);
        $arContracts   = $obRightHolder->getContracts();
        $arHistory     = $obRightHolder->getHistory($arContracts);
        $arEntities    = $obRightHolder->getContractsEntities($arContracts);
        $arBooks       = $obRightHolder->getBooksWithCopyright($arEntities['BOOKS']);
        $arFiles       = $obRightHolder->getFiles($arEntities['FILES']);
        $arActions     = $obRightHolder->getActions($arContracts, $arBooks);
        $iUpdate       = $obRightHolder->updateContract($arContracts, $arFiles);

        $this->arResult = array(
            'GROUPS'    => $obRightHolder->arUserGroups,
            'ENUMS'     => $obRightHolder->arEnum,
            'STATUS'    => $obRightHolder->arStatus,
            'CONTRACTS' => $arContracts,
            'HISTORY'   => $arHistory,
            'BOOKS'     => $arBooks,
            'FILES'     => $arFiles,
            'ACTIONS'   => $arActions
        );
    }

    /**
     * Установка тайтлов
     */
    private function _setTitle() {

        global $APPLICATION;

        // установка тайтлов
        if ($this->componentPage == 'detail') {
            $arContract = current($this->arResult['CONTRACTS']);
            $APPLICATION->SetTitle(
                str_replace(
                    array('#ID#', '#DATE_FROM#'),
                    array(
                        $arContract['UF_NUMBER'] ? '№ '.$arContract['UF_NUMBER'] : 'ID #'.$arContract['ID'],
                        $arContract['UF_DATE_FROM']
                    ),
                    GetMessage('TITLE_INFO_CONTRACT')
                )
            );
        } else  {
            if ($this->arResult['GROUPS']['rightholder'])
                $APPLICATION->SetTitle(GetMessage('TITLE_MY_CONTRACTS'));
            else
                $APPLICATION->SetTitle(GetMessage('TITLE_RIGHTHOLDERS_CONTRACTS'));
        }
    }
}