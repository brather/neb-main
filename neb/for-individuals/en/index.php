<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("For Individuals");
?><p>
	 NEB is aimed at information service of the widest range for Russian and foreign citizens and meant for “common readers”, students of secondary schools and higher education institutions, and also for research workers and teachers.
</p>
<p>
	<br>
</p>
<p>
</p>
<p>
	 In terms of national strategy of forming informational community in Russia by means of providing citizens with the rights on unlimited access to information, the development of NEB is aimed at solving the main problem that is allowing access to information, improvement of the integrated dispersed automated library-informational system uniting electronic resources of libraries and other library stock holders available for all categories of users, and supporting life of digital documents. This aim presupposes the following:
</p>
<p>
	<br>
</p>
<p>
</p>
<p>
	 — to make a unified informational and searching system for all parts of the NEB-fund. The function of this system is to make the content clear for readers and allow them to execute a multifold search or use means of navigation;<br>
 <br>
</p>
<p>
	 — to solve a legally important problem of supplying NEB-users with access to documents protected by copyright;<br>
 <br>
</p>
<p>
	 — to supply users with access to electronic documents, including those ones that are protected by copyright;<br>
 <br>
</p>
<p>
	 — to make, collect and ensure long-term safety of the national fund of electronic documents;<br>
 <br>
</p>
<p>
	 — to develop systems of effective informing about NEB-activity , its resources and services, and rules and terms of their usage.<br>
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>