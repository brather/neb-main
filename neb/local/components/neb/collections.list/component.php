<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// Подключение классов
use \Nota\Library\Collection,
    \Bitrix\Main\Application,
    \Bitrix\NotaExt\Iblock\Section,
    \Bitrix\NotaExt\Iblock\Element,
    \Nota\Exalead\SearchQuery,
    \Nota\Exalead\SearchClient,
    \Nota\UserData\UsersBooksCollectionDataTable,
    \Bitrix\NotaExt\Iblock\IblockTools,
    \Bitrix\Iblock\IblockTable,
    \Bitrix\Iblock\SectionTable,
    \Bitrix\Main\Loader;

// Подключение модулей
Loader::includeModule('nota.library');
Loader::includeModule('nota.userdata');
Loader::includeModule('nota.exalead');
Loader::includeModule('iblock');
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

global $USER;

// Возвращаем id инфоблока подборок
if(!empty($arParams['IBLOCK_ID'])) {
    $idIblockCollection = $arParams['IBLOCK_ID'];
} else {
    $rsBlock = IblockTable::getList(array(
        'filter' => array('CODE' => IBLOCK_CODE_COLLECTIONS),
        'select' => array('NAME', 'ID')
    ))->fetch();
    $idIblockCollection = $rsBlock['ID'];
}
$arResult['IBLOCK_ID_COLLECTIONS'] = $idIblockCollection;

// Обработка входящих параметров
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
if ($arRequest['action'] == 'addBook' && isset($arRequest['COLLECTION_ID']) && $arRequest['BOOK_NUM_ID'] > 0 && check_bitrix_sessid()) {
    foreach ($arRequest['BOOK_NUM_ID'] as $num_id) {
        Collection::addBook(
            $arRequest['COLLECTION_ID'],
            $arRequest['BOOK_ID_' . $num_id],
            $arRequest['BOOK_TITLE_' . $num_id],
            $arRequest['BOOK_AUTHOR_' . $num_id]
        );
    }
    LocalRedirect($arParams['ADD_REDIRECT_URL']);
}
if ($arRequest['action'] == 'remove' && isset($arRequest['COLLECTION_ID']) > 0 && check_bitrix_sessid()) {
    Collection::remove($arRequest['COLLECTION_ID']);
    LocalRedirect($APPLICATION->GetCurPage());
}
// количество для вывода записать в куки
if ($_GET['pagen'] > 0) {
    $APPLICATION->set_cookie("PAGEN", $_GET['pagen'], time() + 3600, $APPLICATION->GetCurPage());
    $arRequest['pagen'] = $_GET['pagen'];
} else {
    $arRequest['pagen'] = $APPLICATION->get_cookie("PAGEN", "NEB_SM");;
}

// Задание параметров
if ($arParams["IBLOCK_ID"] < 1) {
    ShowError("IBLOCK_ID IS NOT DEFINED");
    return false;
}
if(!isset($arParams['ADD_REDIRECT_URL'])) {
    $arParams['ADD_REDIRECT_URL'] = $APPLICATION->GetCurPage();
}
$arParams["LIBRARY_ID"] = intval($arParams["LIBRARY_ID"]);
if (empty($arParams["LIBRARY_ID"]) and !empty($arRequest['library'])) {
    $arParams["LIBRARY_ID"] = intval($arRequest['library']);
}
$arParams['ITEM_COUNT'] = intval($arRequest['pagen']) > 0 ? intval($arRequest['pagen']) : 10;
if ($arParams['ITEM_COUNT'] > 60) {
    $arParams['ITEM_COUNT'] = 60;
}

// Определяем параметры сортировки
$by = ($arRequest['by'] ? trim(htmlspecialcharsEx($arRequest['by'])) : $arParams['SORT']);
$order = ($arRequest['order'] ? trim(htmlspecialcharsEx($arRequest['order'])) : $arParams['ORDER']);

// Возвращаем список подборок
// Определим массив фильтрации
$arFilterCollections = array('IBLOCK_ID' => $idIblockCollection);
if($arParams['PUBLIC_PATH'] == 'Y') {
    $arFilterCollections['ACTIVE'] = 'Y';
} else {
    $arFilterCollections['GLOBAL_ACTIVE'] = array('Y', 'N');
}
if (!empty($arParams["LIBRARY_ID"])) {
    $arFilterCollections['UF_LIBRARY'] = $arParams["LIBRARY_ID"];
}
if(!empty($arParams["PARENT_COLLECTION"])) {
    $arFilterCollections['SECTION_ID'] = $arParams["PARENT_COLLECTION"];
} else {
    $arFilterCollections['DEPTH_LEVEL'] = 1;
}

// Определим массив выборки данных по подборке
$arSelectCollections = array(
    'ID',
    'NAME',
    'DESCRIPTION',
    'ACTIVE',
    'GLOBAL_ACTIVE',
    'SECTION_PAGE_URL',
    'DEPTH_LEVEL',
    'UF_LIBRARY',               // Библиотека
    'UF_SLIDER',                // Показывать в слайдере
    'UF_SHOW_NAME',             // Показывать название подборки в слайдере
    'UF_ADDED_FAVORITES',       // Добавленно в избранное
    'UF_VIEWS',                 // Количество просмотров
    'skip_other'
);

// Определим массив сортировки
if (empty($by) && empty($order)) {
    $arSortCollections = array('SORT' => 'ASC', $arParams['SORT'] => $arParams['ORDER']);
} else {
    $arSortCollections = array($by => $order, "ID" => "DESC");
}

// Определим массив постраничной навигации
$arNavParams = array("nPageSize" => $arParams['ITEM_COUNT']);
$arNavigation = CDBResult::GetNavParams($arNavParams);
$arParams['PAGEN'] = $arNavigation['PAGEN'];
$arParams['SIZEN'] = $arNavigation['SIZEN'];
$arNavParams = array_merge($arNavigation, $arNavParams);

$arSelTest = array();
$arCollections = Section::getList($arFilterCollections, $arSelectCollections, $arSortCollections, true, $arNavParams);
$arResult['NAV_STRING'] = $arCollections['NAV_STRING'];
if (empty($arCollections['ITEMS'])) {
    $this->IncludeComponentTemplate();
    return false;
}

// Заполнение массива результирующего массива подборок $arResult['SECTIONS']
$arLibraryId = array();         // массив id библиотек
$arSectionsId = array();        // массив id подборок
$arSectionIdWithSub = array();  // массив id подборок вместе с подразделами
foreach ($arCollections['ITEMS'] as $arItem) {
    if(!is_null($arItem['UF_LIBRARY']) && !empty($arItem['UF_LIBRARY'])) {
        if(!in_array($arItem['UF_LIBRARY'], $arLibraryId)) {
            $arLibraryId[] = $arItem['UF_LIBRARY'];
        }
    }
    $arSectionsId[] = $arItem['ID'];
    $arItem['IN_MY_COLLECTIONS'] = false;
    $arResult['SECTIONS'][$arItem['ID']] = $arItem;
}

// Добавление параметра IN_MY_COLLECTIONS в $arResult['SECTIONS'] определяющего вхождение в подборку текущего пользователя
if (!empty($arSectionsId) && $USER->IsAuthorized()) {
    $collectionsResult = UsersBooksCollectionDataTable::getList(array(
            'filter' => array('UF_UID' => $USER->GetID(), 'UF_COLLECTION_ID' => $arSectionsId),
            'select' => array('UF_COLLECTION_ID')
        ));
    while ($collection = $collectionsResult->fetch()) {
        if (isset($arResult['SECTIONS'][$collection['UF_COLLECTION_ID']])) {
            $arResult['SECTIONS'][$collection['UF_COLLECTION_ID']]['IN_MY_COLLECTIONS'] = true;
        }
    }
}

// Получаем массив с данными по библиотекам используемым в подборках
$arLibrary = array();   // массив с данными по библиотекам
if (!empty($arLibraryId)) {
    $resLibrary = Element::getList(
        array('IBLOCK_ID' => IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY), 'ID' => $arLibraryId),
        false,
        array('PROPERTY_COLLECTIONS', 'ID', 'DETAIL_PAGE_URL', 'skip_other')
    );
    if (!empty($resLibrary['ITEMS']))
        foreach ($resLibrary['ITEMS'] as $arItem) {
            $arLibrary[$arItem['ID']] = array(
                'NAME'              => $arItem['NAME'],
                'DETAIL_PAGE_URL'   => $arItem['DETAIL_PAGE_URL'],
                'CNT_COLLECTIONS'   => $arItem['PROPERTY_COLLECTIONS_VALUE']
            );
        }
}
$arResult['LIBRARY'] = $arLibrary;

$cacheId = 'collection.list' . $by . $order . $USER->GetID();
$cacheTime = $arParams['CACHE_TIME'];
if ($this->StartResultCache($cacheTime, $cacheId)) {
    // Возвращаем список книг
    // Массив фильтра книг подборок
    $arFilterBooks = array(
        'IBLOCK_ID' => $idIblockCollection,
        '!PROPERTY_BOOK_ID' => false,
        //'GLOBAL_ACTIVE' => array('Y', 'N')
    );
    // Массив выборки свойств книг
    $arSelectBooks = array(
            'ID',
            'NAME',
            'PROPERTY_BOOK_ID',
            'IBLOCK_SECTION_ID',
            'PROPERTY_TYPE_IN_SLIDER_XML_ID',
            'DETAIL_PAGE_URL'
            //'skip_other',
        );
    
    // Массив сортировки книг
    $arOrderBooks = array('PROPERTY_TYPE_IN_SLIDER' => 'ASC', 'SORT' => 'ASC');
    //$arOrderBooks = array('ID' => 'ASC');
    
    // Массив сортировки книг
    $arNavParamsBook = array();
    if($arParams["COUNT_BOOKS_IN_COLLECTION"] > 0) {
        $arNavParamsBook = array(
            'nTopCount' => $arParams['COUNT_BOOKS_IN_COLLECTION']
        );
    }
    
    $arBooksAllCollections = array();
    foreach($arSectionsId as $idSect) {
        $obIBlockElement = new CIBlockElement();
        // Вернем подразделы подбороки  запишем в общий массив id разделов инфоблока коллекций
        $arSectionIdWithSub = array();
        $rsSubCollections = SectionTable::getList(array(
            'filter' => array('IBLOCK_ID' => $idIblockCollection, 'IBLOCK_SECTION_ID' => $idSect),
            'select' => array('ID', 'NAME'),
            'order' => array('SORT' => 'ASC')
        ));
        while($obSubSection = $rsSubCollections->fetch()) {
            $arSectionIdWithSub[] = $obSubSection['ID'];
        }
        if(!empty($arSectionIdWithSub)) {
            $arSectionIdWithSub[] = $idSect;
            $arFilterBooks['SECTION_ID'] = $arSectionIdWithSub;
        } else {
            $arFilterBooks['SECTION_ID'] = $idSect;
        }
        $rsBooksColection = $obIBlockElement->GetList($arOrderBooks, $arFilterBooks, false, $arNavParamsBook, $arSelectBooks);
        while($obBookCollection = $rsBooksColection->GetNext()) {
            //$arBooksAllCollections[$idSect][$obBookCollection['ID']] = $obBookCollection;
            // Проверим на наличие пробелов по краям строки с exalead_id (в некоторых изданиях пробелы наличествовали)
            foreach($obBookCollection as $iB => $vB) {
                if(!is_array($vB)) {
                    $vB = trim(htmlspecialchars($vB));
                }
                $arBooksAllCollections[$idSect][$obBookCollection['ID']][$iB] = $vB;
            }
        }
    }
    
    // Сортируем книги по подборкам
    $arBooksId = array();           // массив содержащий id для запроса к exalead
    $arBooksIBlockId = array();     // массив содержащий id книг с ключами == exalead_id
    $arBooksIdSection = array();    // массив вида [exalead_id => array(section_id)]
    if(!empty($arBooksAllCollections)) {
        foreach($arBooksAllCollections as $idCollection => $arCollection) {
            foreach($arCollection as $idBook => $arBook) {
                $arBooksId[] = $arBook['PROPERTY_BOOK_ID_VALUE'];
                $arBooksIBlockId[$arBook['PROPERTY_BOOK_ID_VALUE']] = $arBook['ID'];
                if(!in_array($arBook['IBLOCK_SECTION_ID'], $arBooksIdSection[$arBook['PROPERTY_BOOK_ID_VALUE']])) {
                    $arBooksIdSection[$arBook['PROPERTY_BOOK_ID_VALUE']][] = $idCollection;
                }

                if (!empty($arBook['PROPERTY_TYPE_IN_SLIDER_VALUE']) && empty($arResult['SECTIONS_BOOKS']['COVER'][$idCollection])) {
                    $arResult['SECTIONS_BOOKS']['COVER'][$idCollection] = $arBook['PROPERTY_BOOK_ID_VALUE'];
                } else {
                    $arResult['SECTIONS_BOOKS']['ITEMS'][$idCollection][] = $arBook['PROPERTY_BOOK_ID_VALUE'];
                }
            }
        }
        $arResult['BOOKS_IN_SECTIONS'] = array();
        if(!empty($arBooksId)) {
            $arBooksId = array_chunk($arBooksId, 100);
            $query = new SearchQuery();
            $client = new SearchClient();
            $query->setParam('sl', 'sl_nofuzzy');
            foreach ($arBooksId as $ar) {
                $query->getByArIds($ar);
                $query->setPageLimit(count($ar));
                $result = $client->getResult($query);
                if (!empty($result['ITEMS'])) {
                    foreach ($result['ITEMS'] as $arElement) {
                        $idBookInIBlock = $arBooksIBlockId[$arElement['id']];
                        // Вернем статистику по изданию подборки
                        if ($this->__templateName == "library_profile") {
                            $arElement["STAT"] = Collection::getBookStat($arElement['id'], true);
                        }
                        
                        $arElement["BOOK_ID_IN_IBLOCK"] = $arBooksIBlockId[$arElement['id']];
                        $arResult['BOOKS'][$arElement['id']] = $arElement;

                        foreach($arBooksIdSection[$arElement['id']] as $sectionId) {
                            $arResult['BOOKS_IN_SECTIONS'][$sectionId][] = $arElement['id'];
                            $arResult['BOOKS'][$arElement['id']]['SECTIONS'][] = $sectionId;
                        }
                    }
                }
            }
        }
    }

    $this->EndResultCache();
}

// Задаем заголовок раздела и запускаем шаблон
if ('N' !== $arParams['TITLE']) {
    $APPLICATION->SetTitle(GetMessage('COLLECTION_TITLE'));
}

$this->IncludeComponentTemplate();