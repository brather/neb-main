<?php
/**
 * User: agolodkov
 * Date: 29.08.2016
 * Time: 10:45
 */

namespace LbcBundle\Controller\Api;


use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\Mapping\ClassMetadata;
use LbcBundle\Controller\Traits\ServiceContainerInjectTrait;
use LbcBundle\Entity\AbstractLbcCategory;
use LbcBundle\Entity\LbcCategoryHistory;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use BitrixBundle\Annotations\IncludeBitrix;

/**
 * Class LbcHistoryController
 *
 * @DI\Service("app.api.history", scope="request", public=true)
 */
class LbcHistoryController
{
    use ServiceContainerInjectTrait;

    /**
     * @DI\Inject("doctrine")
     * @var Registry
     */
    public $doctrine;

    /**
     * @param string $category
     * @param int    $id
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @throws NotFoundHttpException
     * @return array
     */
    public function listAction($category, $id)
    {
        if (!$this->container->has('app.api.' . $category)) {
            throw new NotFoundHttpException("Category $category not found!");
        }
        /** @var AbstractCategoryController $categoryApi */
        $categoryApi = $this->container->get('app.api.' . $category);
        if (!$categoryApi->getRepository()->find($id)) {
            throw new NotFoundHttpException("Category $category id:$id not found!");
        }
        $items = [];
        if ($historyEntities = $this->doctrine->getRepository('LbcBundle:LbcCategoryHistory')->findBy(
            [
                'category'   => $category,
                'categoryId' => $id,
            ]
        )
        ) {
            $lastData = null;
            /** @var LbcCategoryHistory $entity */
            foreach ($historyEntities as $entity) {
                if ($data = $entity->getData()) {
                    $fields = [];
                    $data = json_decode($data, true);
                    foreach ($data as $key => $value) {
                        $fields[] = [
                            'name'    => $key,
                            'value'   => $value,
                            'changed' => (isset($lastData[$key]) && $value !== $lastData[$key]) ? true : false
                        ];
                    }
                    $lastData = $data;
                    if (!empty($fields)) {
                        $items[] = ['fields' => $fields];
                    }
                }
            }
        }

        return ['items' => $items];
    }
}