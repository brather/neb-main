<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetPageProperty('hide-title', true);
?>
<div class="news-detail">
    <a href="/news/" class="news-detail__link">Cписок новостей НЭБ</a>
    <div class="news-detail__nav">
        <?if ($arResult["TOLEFT"]):?><i class="fa fa-long-arrow-left"></i><a href="<?=$arResult["TOLEFT"]["URL"]?>" class="news-detail__nav-link news-detail__nav-link--prev">Предыдущая</a><?endif;?>
        <?if ($arResult["TORIGHT"]):?><a href="<?=$arResult["TORIGHT"]["URL"]?>" class="news-detail__nav-link">Следующая</a><i class="fa fa-long-arrow-right"></i><?endif;?>
    </div>
    <br>
    <h1><?=$arResult["NAME"]?></h1>
    <?if ($arResult['PROPERTIES']['type']['VALUE']):?><p>метка новости: <?=$arResult['PROPERTIES']['type']['VALUE']?></p><?endif;?>
    <div class="news-item">
        <?=$arResult['DETAIL_TEXT']?>
    </div>
    <div class="blog-post-share">
        <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
        <div class="yashare-auto-init" data-yashareL10n="ru"
             data-yashareType="none"
             data-yashareQuickServices="vkontakte,facebook,odnoklassniki" data-lang=""></div>
    </div>
</div>


