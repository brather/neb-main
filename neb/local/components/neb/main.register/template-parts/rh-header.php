<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

/**
 * Нам не нужна куча блоков при успешной регистрации, а также
 * будет не очень хорошо, если мы по USE_EMAIL_CONFIRMATION будем писать сообщение и закрывать section
 * потому я вынес это сюда
 */
if ( (int)$arResult['VALUES']['USER_ID'] > 0 && $arResult["USE_EMAIL_CONFIRMATION"] === "Y" )
{ ?>
    <section class="innersection innerwrapper clearfix">
        <div class="b-formsubmit">
            <span class="b-successlink"><?=GetMessage("REGISTER_SUCCESSFUL")?></span>
            <p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
        </div>
    </section>
    <?
    return;
}?>

<section class="innersection innerwrapper clearfix">
    <div class="b-registration rel">
        <script type="text/javascript">
            function setlogin(){
                $('input#LOGIN').val($('input#settings04').val());
            }
        </script>
        <h2 class="mode"><?=Loc::getMessage('TITLE_PAGE');?></h2>
        <?
        if (count($arResult["ERRORS"]) > 0){
            foreach ($arResult["ERRORS"] as $key => $error){
                if (intval($key) == 0 && $key !== 0) {
                    $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".Loc::getMessage("REGISTER_FIELD_".$key)."&quot;", $error);
                }
            }

            ShowError(implode("<br />", $arResult["ERRORS"]).'<br /><br />');

        }elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && (int)$arResult['VALUES']['USER_ID'] > 0 ){
            ?>
            <p><?=Loc::getMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
        <?
        }
        ?>

        <form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data" class="b-form b-form_common b-regform" onsubmit="setlogin()">
            <?
            if($arResult["BACKURL"] <> ''){
                ?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?
            }
            ?>
            <input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value="" />

            <div class="b-form_header"><?=GetMessage("MAIN_REGISTER_DATA");?></div>

            <?$APPLICATION->IncludeComponent(
                "notaext:plupload",
                "register",
                array(
                    "MAX_FILE_SIZE" => "10",
                    "FILE_TYPES" => "jpg,jpeg,png",
                    "DIR" => "tmp_register",
                    "FILES_FIELD_NAME" => "register_file",
                    "MULTI_SELECTION" => "N",
                    "CLEANUP_DIR" => "Y",
                    "UPLOAD_AUTO_START" => "Y",
                    "RESIZE_IMAGES" => "Y",
                    "RESIZE_WIDTH" => "110",
                    "RESIZE_HEIGHT" => "110",
                    "RESIZE_CROP" => "Y",
                    "RESIZE_QUALITY" => "98",
                    "UNIQUE_NAMES" => "Y"
                ),
                false
            );?>
