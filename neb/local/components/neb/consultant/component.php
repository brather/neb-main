<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($_REQUEST['cons_disable'] == 'Y')
    return false;

if ($USER->IsAuthorized()) {
    $obNUser = new nebUser();
    $obNUser->getRole();

    $arResult["IS_ADMIN"] = in_array($obNUser->role, Array(UGROUP_LIB_CODE_ADMIN, UGROUP_LIB_CODE_EDITOR, UGROUP_LIB_CODE_CONTROLLER));


    if (!$arResult["IS_ADMIN"] and $USER->IsAdmin()) # Если пользователь не работник библиотеки, но при этом админ на портале, - то невыводим для него консультант
        return false;

    $arResult["USER"] = $obNUser->getUser();
    $status = $arResult["USER"]["UF_STATUS"];
    $type_reg = $arResult["USER"]["UF_REGISTER_TYPE"];
    $arResult["USER"]["FULL_NAME"] = $arResult["USER"]["NAME"] . " " . $arResult["USER"]["LAST_NAME"];

    // выводить консультанта только для библиотекарей РГБ
    if (!in_array('55', $arResult['USER']['UF_LIBRARIES']))
        return false;
}
//vip
if ($status == 43) $statusForChat = 1;
//ЕЭЧБ
elseif ($type_reg == 40) $statusForChat = 2;
//зарегистрированный
elseif ($status == 42 || $status == 4) $statusForChat = 3;
//анонимный
else $statusForChat = 4;

$arResult["STATUS"] = $statusForChat;
$arResult["URL"] = $APPLICATION->GetCurUri();
$arResult["TITLE"] = $APPLICATION->GetTitle();
/*
if(SITE_HOST === $_SERVER['SERVER_NAME']) {
    $APPLICATION->AddHeadString('<link rel="stylesheet" href="https://xn--80atdhccwjcf4h.xn--90ax2c.xn--p1ai:8443/styles/importer.css">');
} else {
    $APPLICATION->AddHeadString('<link rel="stylesheet" href="https://84.47.187.73:4247/styles/importer.css">');
}*/

$this->IncludeComponentTemplate();