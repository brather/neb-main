<div class="b-addbook">
	<h2>Добавление новой книги</h2>
	<form action="" class="b-form b-form_common b-addbookform">
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">			
				<label for="settings02">Полное название произведения</label>
				<div class="field validate">
					<input type="text" data-required="required" value="" id="settings02" data-maxlength="30" data-minlength="2" name="name" class="input" >		
					<em class="error required">Поле обязательно для заполнения</em>								
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50">			
				<label for="settings03">Автор</label>
				<div class="field validate">
					<input type="text"  data-validate="fio" data-required="required" value="" id="settings03" data-maxlength="30" data-minlength="2" name="name" class="input w50p" >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock bbkfield">			
				<label for="settings04">Код ББК</label>
				<div class="field validate ">
					<input type="text"  data-validate="number" data-required="required" value="" id="settings04" data-maxlength="30" data-minlength="2" name="name" class="input bbk_field" > <span class="example">Пример ББК: 81.411.2-4 </span>		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w20pc">			
				<label for="settings05">Год издания</label>
				<div class="field validate">
					<input type="text"  data-validate="number" data-required="required" value="" id="settings5" data-maxlength="30" data-minlength="2" name="name" class="input b-yeartb" >		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Поле заполнено неверно</em>						
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock b-pdfadd">
				<div class="left">
					<p>Ссылка на pdf-версию книги</p>
					<div class="b-pdfaddwrapper">
					<a href="#" class="tdu">Добавить pdf</a> <em class="errorformat file">Ошибка. Неверный формат файла</em>
					</div>
					<div class="field validate addpdfield hidden">
						<input type="text" data-required="required" value="" id="settings5" data-maxlength="30" data-minlength="2" name="name" class="input " >		

					</div>	
					<div class="b-pdfwrapper hidden">
						<div class="b-fondtitle b-filename"><span class="b-fieldtext">Слово о полку Игореве.pdf</span><a class="b-fieldeedit" href="#"></a></div>					

						<div class="b-loaded">Загружено: 100%</div>
					</div>

					<div class="progressbar hidden"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружено: <span class="num">33</span>%</div></div>
				</div>
				<div class="right">
					<p>Дата добавления </p>
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><span class="b-fieldtext">12.12.2014</span><a class="b-fieldeedit" href="#"></a></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" class="txt txt_size2"></span><a class="b-editablsubmit iblock" href="#"></a></div>
					</div>
				</div>


			</div>
		</div>
		<div class="fieldrow nowrap fieldrowaction">
			<div class="fieldcell ">
				<div class="field clearfix">
					<a href="#" class="formbutton gray right btrefuse">Отказаться</a>
					<button class="formbutton left" value="1" type="submit">Разместить произведение</button>

				</div>
			</div>
		</div>
	</form>
</div>