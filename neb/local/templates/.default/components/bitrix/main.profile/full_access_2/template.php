<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

?>
<? ShowError($arResult["strProfileError"]); ?>
<?
if ($arResult['DATA_SAVED'] == 'Y') {
    LocalRedirect($arParams['STEP_3_PAGE']);
    exit();
}
?>
<form class="b-form b-form_common b-regform" method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>"
      enctype="multipart/form-data">

    <?= $arResult["BX_SESSION_CHECK"] ?>
    <input type="hidden" name="lang" value="<?= LANG ?>"/>
    <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
    <input type="hidden" name="EMAIL" value="<?= $arResult["arUser"]["EMAIL"] ?>"/>
    <input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>"/>


    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings04">Серия и номер паспорта</label>
            <div class="field validate">
                <input type="text" class="input tbpasport" name="UF_PASSPORT_NUMBER" data-minlength="2"
                       data-maxlength="30" id="settings04" value="<?= $arResult['arUser']['UF_PASSPORT_NUMBER'] ?>">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock mt10 w700">
            <em class="hint">*</em>
            <label for="settings05">Кем выдан / как в паспорте</label>
            <div class="field validate">
                <input type="text" class="input" name="UF_ISSUED" data-minlength="2" data-maxlength="30" id="settings05"
                       value="<?= $arResult['arUser']['UF_ISSUED'] ?>">
            </div>
        </div>
    </div>
    <div class="b-form_header">
        <span class="iblock">Адрес регистрации</span><span class="iblock">Адрес проживания</span>
        <div class="checkwrapper">
            <input class="checkbox addrindently" type="addr" name="UF_PLACE_REGISTR" value="1" id="cb3"><label for="cb3"
                                                                                                               class="black">по
                месту регистрации</label>
            <?
            if (!empty($arResult["arUser"]['UF_PLACE_REGISTR'])) {
                ?>
                <script type="text/javascript">
                    $(function () {
                        $('input[name="UF_PLACE_REGISTR"]').click();
                    });
                </script>
                <?

            }
            ?>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings03">Индекс</label>
            <div class="field validate w140">
                <em class="error required">Заполните поле</em>
                <em class="error number">Некорректный почтовый индекс</em>
                <input type="text" data-validate="number" data-required="required"
                       value="<?= $arResult["arUser"]["PERSONAL_ZIP"] ?>" id="settings03" name="PERSONAL_ZIP"
                       class="input ">
            </div>
        </div>
        <div class="fieldcell iblock inly">
            <label for="settings04">Индекс</label>
            <div class="field validate w140">
                <input type="text" data-validate="number" value="<?= $arResult["arUser"]["WORK_ZIP"] ?>" id="settings04"
                       name="WORK_ZIP" class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings05">Город</label>
            <div class="field validate">
                <em class="error required">Заполните поле</em>
                <input type="text" data-required="required" value="<?= $arResult["arUser"]['PERSONAL_CITY'] ?>"
                       id="settings05" data-maxlength="30" data-minlength="2" name="PERSONAL_CITY" class="input">
            </div>
        </div>
        <div class="fieldcell iblock inly">
            <label for="settings06">Город</label>
            <div class="field validate">
                <input type="text" value="<?= $arResult["arUser"]['WORK_CITY'] ?>" id="settings06" data-maxlength="30"
                       data-minlength="2" name="WORK_CITY" class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock ">
            <em class="hint">*</em>
            <label for="settings07">Улица</label>
            <div class="field validate">
                <em class="error required">Заполните поле</em>
                <input type="text" value="<?= $arResult["arUser"]['PERSONAL_STREET'] ?>" id="settings07"
                       data-maxlength="30" data-minlength="2" data-required="true" name="PERSONAL_STREET" class="input">
            </div>
        </div>
        <div class="fieldcell iblock inly">
            <label for="settings08">Улица</label>
            <div class="field validate">
                <input type="text" value="<?= $arResult["arUser"]['WORK_STREET'] ?>" id="settings08" data-maxlength="30"
                       data-minlength="2" name="WORK_STREET" class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <div class="field iblock fieldthree ">
                <label for="settings09">Дом:</label>
                <div class="field validate">
                    <input type="text" class="input" name="UF_CORPUS" data-maxlength="20" id="settings09"
                           value="<?= $arResult["arUser"]['UF_CORPUS'] ?>" data-required="true"/>
                    <em class="error required">Заполните</em>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings10">Строение: </label>
                <div class="field">
                    <input type="text" class="input" name="UF_STRUCTURE" data-maxlength="20" maxlength="4"
                           id="settings10" value="<?= $arResult["arUser"]['UF_STRUCTURE'] ?>"/>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings11">Квартира: </label>
                <div class="field">
                    <input type="text" class="input" name="UF_FLAT" data-maxlength="20" maxlength="4" id="settings11"
                           value="<?= $arResult["arUser"]['UF_FLAT'] ?>"/>
                </div>
            </div>
        </div>
        <div class="fieldcell iblock inly">
            <div class="field iblock fieldthree ">
                <label for="settings09">Дом:</label>
                <div class="field validate">
                    <input type="text" class="input" name="UF_HOUSE2" data-maxlength="20" id="settings09"
                           value="<?= $arResult["arUser"]['UF_HOUSE2'] ?>"/>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings10">Строение: </label>
                <div class="field">
                    <input type="text" class="input" name="UF_STRUCTURE2" data-maxlength="20" maxlength="4"
                           id="settings10" value="<?= $arResult["arUser"]['UF_STRUCTURE2'] ?>"/>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings11">Квартира: </label>
                <div class="field">
                    <input type="text" class="input" name="UF_FLAT2" data-maxlength="20" maxlength="4" id="settings11"
                           value="<?= $arResult["arUser"]['UF_FLAT2'] ?>"/>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <?
    $sImage = (intval($arResult['arUser']['UF_SCAN_PASSPORT1']) > 0) ? CFile::GetPath($arResult['arUser']['UF_SCAN_PASSPORT1']) : '';
    if ($arResult['arUser']['UF_SCAN_PASSPORT1']['SRC']) {
        $sImage = $arResult['arUser']['UF_SCAN_PASSPORT1']['SRC'];
    }
    ?>
    <? $APPLICATION->IncludeComponent(
        "notaext:plupload",
        "scan_passport1",
        array(
            "MAX_FILE_SIZE" => "10",
            "FILE_TYPES" => "jpg,jpeg,png",
            "DIR" => "tmp_register",
            "FILES_FIELD_NAME" => "profile_file",
            "MULTI_SELECTION" => "N",
            "CLEANUP_DIR" => "Y",
            "UPLOAD_AUTO_START" => "Y",
            "RESIZE_IMAGES" => "Y",
            "RESIZE_WIDTH" => "4000",
            "RESIZE_HEIGHT" => "4000",
            "RESIZE_CROP" => "Y",
            "RESIZE_QUALITY" => "98",
            "UNIQUE_NAMES" => "Y",
            "SCAN1_PHOTO" => $sImage,
        ),
        false
    ); ?>

    <?
    $sImage = (intval($arResult['arUser']['UF_SCAN_PASSPORT2']) > 0) ? CFile::GetPath($arResult['arUser']['UF_SCAN_PASSPORT2']) : '';
    if ($arResult['arUser']['UF_SCAN_PASSPORT2']['SRC']) {
        $sImage = $arResult['arUser']['UF_SCAN_PASSPORT2']['SRC'];
    }
    ?>
    <? $APPLICATION->IncludeComponent(
        "notaext:plupload",
        "scan_passport2",
        array(
            "MAX_FILE_SIZE" => "10",
            "FILE_TYPES" => "jpg,jpeg,png",
            "DIR" => "tmp_register",
            "FILES_FIELD_NAME" => "profile_file",
            "MULTI_SELECTION" => "N",
            "CLEANUP_DIR" => "Y",
            "UPLOAD_AUTO_START" => "Y",
            "RESIZE_IMAGES" => "Y",
            "RESIZE_WIDTH" => "4000",
            "RESIZE_HEIGHT" => "4000",
            "RESIZE_CROP" => "Y",
            "RESIZE_QUALITY" => "98",
            "UNIQUE_NAMES" => "Y",
            "SCAN1_PHOTO" => $sImage,
        ),
        false
    ); ?>

    <div class="fieldrow nowrap fieldrowaction">
        <div class="fieldcell">
            <div class="field clearfix">
                <a href="<?= $arParams['STEP_1_PAGE'] ?>" class="button_back">Назад</a>
                <button class="formbtn btdisable" value="1" name="save" type="submit" disabled="disabled">Перейти к шагу
                    3
                </button>
            </div>
        </div>
    </div>
</form>
