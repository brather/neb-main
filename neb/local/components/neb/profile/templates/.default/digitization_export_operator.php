<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$APPLICATION->IncludeComponent(
    'neb:library.plan_digital',
    'operator',
    Array(
        'DATE_FROM'    => $_REQUEST['from'],
        'DATE_TO'      => $_REQUEST['to'],
        'LIBRARY_ID'   => $_REQUEST['library'],
        'ADD_PAGE_URL' => $arResult['FOLDER']
            . $arResult['URL_TEMPLATES']['plan_digitization_add'],
        'CACHE_TIME'   => $arParams['CACHE_TIME'],
    ),
    $component
);
