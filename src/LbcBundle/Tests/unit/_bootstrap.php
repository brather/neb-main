<?php
define('BITRIX_USE_TEST_DATABASE', 'neb_test_database');
$documentRoot = realpath(
    __DIR__ . '/../../../../neb/'
);
$_SERVER['DOCUMENT_ROOT'] = $documentRoot;
require_once $documentRoot . '/bitrix/modules/main/include/prolog_before.php';
