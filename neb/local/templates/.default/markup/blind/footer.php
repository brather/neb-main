<footer>
	<div class="wrapper">
		<div class="b-footer_row clearfix">
			<a href="#" class="b-footerlogo" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА"><img src="../i/logo_2.png" alt="" title="НАЦИОНАЛЬНАЯ ЭЛЕКТРОННАЯ БИБЛИОТЕКА"></a>
			<ul class="b-footer_nav">
				<li><a href="#" title="Правовая информация">Правовая информация</a></li>
				<li><a href="#" title="Пользовательское соглашение">Пользовательское соглашение</a></li>
			</ul>
			<ul class="b-footer_nav">
				<li><a href="#">Часто задаваемые вопросы</a></li>
				<li><a href="mailto:">Обратная связь</a></li>
			</ul>
		</div>
		<div class="b-footer_row clearfix">					
			<div class="b-copy">Все права защищены. Полное или частичное копирование материалов запрещено,<br> при согласованном использовании материалов необходима ссылка на ресурс. <br>
				Полное или частичное копирование произведений запрещено, согласование <br> использования произведений производится с их авторами.</div>					
				<div class="b-footer_support">
					<div class="b-footer_support_tit">Разработчик АО «ЭЛАР»<br>По заказу</div>
					<a href="http://mkrf.ru/" title="МИНИСТЕРСТВО КУЛЬТУРЫ РОССИЙСКОЙ ФЕДЕРАЦИИ"><img src="../i/ministr.png" alt="" title="МИНИСТЕРСТВО КУЛЬТУРЫ РОССИЙСКОЙ ФЕДЕРАЦИИ"></a>
				</div>
			</div>
		</div><!-- /.wrapper -->
	</footer>