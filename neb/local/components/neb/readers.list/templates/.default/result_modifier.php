<?php
/**
 * User: agolodkov
 * Date: 02.05.2015
 * Time: 16:22
 */
/**
 * @var array            $arResult
 * @var CBitrixComponentTemplate $this
 */
$parentParams = $this->__component->getParent()->arParams;
$statuses = array(
    $parentParams['READER_BLOCK_STATUS'] => true,
    $parentParams['READER_UNBLOCK_STATUS'] => true,
);
foreach ($arResult['USERS'] as &$user) {
    $user['CAN_LOCK'] = false;
    $user['LOCK'] = false;
    if(isset($statuses[(integer)$user['UF_STATUS_ID']])) {
        $user['CAN_LOCK'] = true;
    }
    if ($parentParams['READER_BLOCK_STATUS'] === (integer)$user['UF_STATUS_ID']
    ) {
        $user['LOCK'] = true;
    }
}
unset($user);