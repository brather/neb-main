<?
// Без файла не работает autoload

\Bitrix\Main\Loader::registerAutoLoadClasses("neb.main", array(
    "Bitrix\\Posting\\ListRubricTable"          => "lib/posting/listrubric.php",
    "Bitrix\\Posting\\PostingTable"             => "lib/posting/posting.php",
    "Bitrix\\Posting\\RubricTable"              => "lib/posting/rubric.php",
    "Bitrix\\Posting\\EmailTable"               => "lib/posting/email.php",
    "Bitrix\\Posting\\FileTable"                => "lib/posting/file.php",

    "Neb\\Main\\Library\\LibsTable"             => "lib/library/libstable.php",
    "Neb\\Main\\Library\\NebLibsCityTable"      => "lib/library/neblibscitytable.php",
    "Neb\\Main\\Library\\NebLibsAreaTable"      => "lib/library/neblibsareatable.php",

    "Neb\\Main\\Iblock\\SectionUfPropertyTable" => "lib/sectionufpropertytable.php",
    "Neb\\Main\\SearchCollectionsTable"         => "lib/searchcollectionstable.php",
    "Neb\\Main\\SearchLanguagesTable"           => "lib/searchlanguagestable.php",
));