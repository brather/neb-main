<?

use \Bitrix\Main\Localization\Loc;

$obUser = new nebUser();

$arGroups = array_flip(explode(',', $obUser->GetGroups()));

if ( !$obUser->IsAdmin() && !isset($arGroups[$obUser->GROUP_OPERATOR_NEB]) )
    return false;

Loc::loadMessages(__FILE__);

IncludeModuleLangFile( $_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php" );
IncludeModuleLangFile( __FILE__ );

$mainOptions = array(
    Loc::getMessage("MAIN_SETTINGS_USERS"),
    array("temporary_verification_hours", Loc::getMessage("TEMPORARY_VERIFICATION"), false, array("text", 10)),
    array("temporary_user_lock_hours", Loc::getMessage("TEMPORARY_USER_LOCK"), false, array("text", 10)),
    array("auth_access_mode", Loc::getMessage("AUTH_ACCESS_MODE"), false, array("checkbox"),  '1'),

    Loc::getMessage("MAIN_SETTINGS_MINIFY"),
    array("use_minified_css_js", Loc::getMessage("MAIN_SETTINGS_MINIFY_CSS_JS"), false, array("checkbox"), '1'),
    array("use_minified_html", Loc::getMessage("MAIN_SETTINGS_MINIFY_HTML"), false, array("checkbox"), '0'),

    Loc::getMessage("MAIN_SETTINGS_SECURITY"),
    array("security_ssl_verification", Loc::getMessage("MAIN_SETTINGS_SECURITY_SSL"), false, array("checkbox"), '0'),
);
$mainPageOptions = array(

    Loc::getMessage("MAIN_PAGE_NEWS_SETTINGS"),
    array("main_page_news_count", Loc::getMessage("MAIN_PAGE_NEWS_COUNT"), false,
          array(
              'selectbox',
              array(
                  "3" => "3",
                  "6" => "6",
                  "9" => "9",
              )
          )
    ),
    array("main_page_news_count_yl", Loc::getMessage("MAIN_PAGE_NEWS_COUNT_YL"),
          false,
          array(
              'selectbox',
              array(
                  "4" => "4",
                  "8" => "8",
              )
          )
    ),

    Loc::getMessage("MAIN_PAGE_MESSAGE_SETTINGS"),
    array("main_page_message",      Loc::getMessage("MAIN_PAGE_MESSAGE"), false, array("checkbox"), '1'),
    array("main_page_message_test", Loc::getMessage("MAIN_PAGE_MESSAGE_TEXT"), false, array('textarea', 10, 50)),

);
$statsOptions = array(
    array("stats_host", Loc::getMessage("STATS_HOST"), false, array("text", 50)),
    array("stats_writer_token", Loc::getMessage("STATS_WRITER_TOKEN"), false, array("text", 50)),
    array("stats_reader_token", Loc::getMessage("STATS_READER_TOKEN"), false, array("text", 50)),
);
$otherOptions = array(
    array("debug_email", Loc::getMessage("DEBUG_EMAIL"), false, array("text", 50)),
);



$aTabs = array(
    array(
        "DIV"     => "main_settings",
        "TAB"     => Loc::getMessage("MAIN_SETTINGS"),
        "TITLE"   => Loc::getMessage("MAIN_SETTINGS"),
        "OPTIONS" => array(),
    ),
    array(
        "DIV"     => "main_page_settings",
        "TAB"     => Loc::getMessage("MAIN_PAGE_SETTINGS"),
        "TITLE"   => Loc::getMessage("MAIN_PAGE_SETTINGS"),
        "OPTIONS" => array(),
    ),
    array(
        "DIV"     => "stats_settings",
        "TAB"     => Loc::getMessage("STATS_SETTINGS"),
        "TITLE"   => Loc::getMessage("STATS_SETTINGS"),
        "OPTIONS" => array(),
    ),
    array(
        "DIV"     => "other_settings",
        "TAB"     => Loc::getMessage("OTHER_SETTINGS"),
        "TITLE"   => Loc::getMessage("OTHER_SETTINGS"),
        "OPTIONS" => array(),
    ),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid()) {

    if (strlen($RestoreDefaults) > 0) {
        COption::RemoveOption($mid);
    } else {
        __AdmSettingsSaveOptions($mid, $mainOptions);
        __AdmSettingsSaveOptions($mid, $mainPageOptions);
        __AdmSettingsSaveOptions($mid, $statsOptions);
        __AdmSettingsSaveOptions($mid, $otherOptions);
    }

    if (strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0) {
        LocalRedirect($_REQUEST["back_url_settings"]);
    } else {
        LocalRedirect(
            $APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang="
            . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode(
                $_REQUEST["back_url_settings"]
            ) . "&" . $tabControl->ActiveTabParam()
        );
    }
}

$tabControl->Begin();
?>

<form method="post" action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>">
    <?
    $tabControl->BeginNextTab();
    __AdmSettingsDrawList($mid, $mainOptions);
    $tabControl->BeginNextTab();
    __AdmSettingsDrawList($mid, $mainPageOptions);
    $tabControl->BeginNextTab();
    __AdmSettingsDrawList($mid, $statsOptions);
    $tabControl->BeginNextTab();
    __AdmSettingsDrawList($mid, $otherOptions);

    $tabControl->Buttons(); ?>

    <input type="submit" name="Update" value="<?= Loc::getMessage("MAIN_SAVE") ?>"
           title="<?= Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save" />

    <input type="submit" name="Apply"
           value="<?= Loc::getMessage("MAIN_OPT_APPLY") ?>"
           title="<?= Loc::getMessage("MAIN_OPT_APPLY_TITLE") ?>" />

    <? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
        <input type="button" name="Cancel"
               value="<?= Loc::getMessage("MAIN_OPT_CANCEL") ?>"
               title="<?= Loc::getMessage("MAIN_OPT_CANCEL_TITLE") ?>"
               onclick="window.location='<? echo htmlspecialcharsbx(
                   CUtil::addslashes($_REQUEST["back_url_settings"])
               ) ?>'" />
        <input type="hidden" name="back_url_settings"
               value="<?= htmlspecialcharsbx(
                   $_REQUEST["back_url_settings"]
               ) ?>" />
    <? endif ?>

    <input type="submit" name="RestoreDefaults"
           title="<? echo Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           OnClick="return confirm('<? echo AddSlashes(
               Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")
           ) ?>')" value="<? echo Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>" />

    <?= bitrix_sessid_post(); ?>

    <? $tabControl->End(); ?>
</form>