<?php
$MESS['LIBRARY_FUNDS_SORT_BY_AUTHOR'] = 'Author';
$MESS['LIBRARY_FUNDS_SORT_BY_NAME'] = 'Main title';
$MESS['LIBRARY_FUNDS_SORT_BY_DATE'] = 'Date';
$MESS['LIBRARY_FUNDS_SORT_BY_SHOW'] = 'Show';

$MESS['LIBRARY_FUNDS_LEFT_AUTH'] = 'Authorization required';
$MESS['LIBRARY_FUNDS_REMOVE_FROM_LIB'] = '<span>Remove</span> from my Library';
$MESS['LIBRARY_FUNDS_ADD_TO_LIB'] = '<span>Add</span> to my Library';
$MESS['LIBRARY_FUNDS_AUTHOR'] = 'Author';
$MESS['LIBRARY_FUNDS_YEAR'] = 'Year of publication';
$MESS['LIBRARY_FUNDS_PAGES'] = 'Pages';
$MESS['LIBRARY_FUNDS_PUBLISH'] = 'Publisher';
$MESS['LIBRARY_FUNDS_SHOW_ALL_FUNDS'] = 'Show all (With publications catalog)';