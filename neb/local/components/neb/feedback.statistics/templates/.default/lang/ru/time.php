<?php
$MESS['FEEDBACK_STAT_TITLE']      = 'Статистика по времени, потребовавшемуся на обработку каждой заявки';
$MESS['FEEDBACK_STAT_DATE_FROM']  = 'Начало периода';
$MESS['FEEDBACK_STAT_DATE_TO']    = 'Конец периода';
$MESS['FEEDBACK_STAT_PERIOD']     = 'Статистика за период';
$MESS['FEEDBACK_STAT_SHOW']       = 'Показать статистику';

$MESS['FEEDBACK_STAT_ID']         = 'ID';
$MESS['FEEDBACK_STAT_USER']       = 'Пользователь';
$MESS['FEEDBACK_STAT_TIME']       = 'Отработана за';
$MESS['FEEDBACK_STAT_AVERAGE']    = 'Средний срок отработки заявки';

$MESS['FEEDBACK_STAT_DAYS']       = 'д';
$MESS['FEEDBACK_STAT_HOURS']      = 'ч';
$MESS['FEEDBACK_STAT_MINUTES']    = 'мин';
$MESS['FEEDBACK_STAT_SECONDS']    = 'сек';

$MESS['FEEDBACK_STAT_EXCEL']      = 'Выгрузить в .xls';