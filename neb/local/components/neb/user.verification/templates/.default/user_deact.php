<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$APPLICATION->IncludeComponent(
    'neb:user.verification.user',
    "",
    array(
        'ID'         => $arParams['VARIABLES']['ID'],
        'DEACTIVATE' => 'Y',
        "LIST_URL"   => $arParams['SEF_FOLDER']
    ),
    $component
);