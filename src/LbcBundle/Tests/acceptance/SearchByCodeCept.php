<?php use LbcBundle\AcceptanceTester;
$I = new AcceptanceTester($scenario);
$I->wantTo('Search by code');
$code = 'Т3(4/9)';
$I->amOnPage('/bbk/search/?code=' . $code);
$I->see('ИСТОРИЯ ЗАРУБЕЖНЫХ СТРАН');
