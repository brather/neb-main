<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

?>
<p><?= Loc::getMessage('COLLECTION_PAGE_DESCRIPTION') ?></p>
<section class="innerwrapper clearfix searchempty ">
<?
    /*$APPLICATION->IncludeComponent(
        "collection:main.slider",
        "collections",
            array(
                "PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['seaction'],
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
                "IBLOCK_ID" => $arParams['IBLOCK_ID']
             ),
         false
      );*/

    $APPLICATION->IncludeComponent(
        "neb:collections.list",
        "",
        array(
            "PAGE_URL" => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['seaction'],
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
            "IBLOCK_ID" => $arParams['IBLOCK_ID'],
            "TITLE" => $arParams['TITLE'],
            "PUBLIC_PATH" => "Y",
            "SORT" => ($_REQUEST['by']) ? $_REQUEST['by'] : 'SORT', //created
            "ORDER" => ($_REQUEST['order']) ? $_REQUEST['order'] : 'ASC',
            "COUNT_BOOKS_IN_COLLECTION" => 6
        ),
        false
    );
?>
</section>
