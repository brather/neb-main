
<?
$pagename='';
?>
<? include("header_main.php"); ?>
<section class="mainsection innerpage" >	
	<form action="" class="searchform">		
		<div class="b-portalinfo clearfix wrapper">
			<div class="leftblock iblock">
				<span style="
						background-color: #ee6334;
						color: #fff;
						margin: -25px 5px 0px 275px;
						padding: 3px;
						font-size: 12px;
						font-style: italic;
						display: block;
						width: 71px;
					">beta-версия</span>
				<a href="" class="b_logo iblock">
					<img src="./i/logo_1.png" alt="">
				</a>
				<span style="display: block; margin: 15px 1px -24px 64px;"><a style="text-decoration: none;" href="#" class="b-portalabout_lnk iblock">О проекте</a></span>		
			</div> 
			<div class=" rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">
							<input type="text" name="" class="bbox b-search_fieldtb b-text" id="" value="Станислав Лем">
							<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
						</span>					
												
					</div>
					
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb1"><label for="cb1" class="black fz_mid">Искать в найденном</label>
						</div>
						<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="" id="cb3"><label for="cb3" class="black">Искать только в полнотекстовых изданиях</label>
						</div>

					</div> <!-- /.b_search_set -->
				</div>
			</div>
			
		</div><!-- /.b-portalinfo -->


		<div class="b-search wrapper">

		</div> <!-- /.b-search -->
		
		<div class="b-search_ex">
			<div class="wrapper rel">
				<div class="b_search_row js_search_row hidden" >
					<select name="logic" disabled="disabled" id="" class="ddl_logic">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme" disabled="disabled" id="" class="ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
					<select name="taccess" disabled="disabled" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
				</div>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="">или</option>
						<option value="">и</option>
						<option value="">не</option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme">
						<option value="">по всем полям</option>
						<option value="">по дате</option>
						<option value="foraccess">по доступу</option>
						<option value="">по названию</option>
					</select>
					<input type="text" name="text[0]" class="b-text b_search_txt" id="">
					<select name="taccess" id="" class="js_select b-access hidden">
						<option value="">Свободный доступ</option>
						<option value="">Частичный доступ</option>
					</select>
					<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
				</div>						

				<div class="b_search_date">
					<div class="b_search_datelb iblock">Дата публикации</div>
					<div class="b_searchslider iblock js_searchslider"></div>
					<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
					<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

				</div>
				<div class="b_search_row clearfix">
					<input type="submit" class="formbutton" value="Принять">
				</div>
			</div>
		</div><!-- /.b-search_ex -->
	</form>			

</section>
<section class="innersection innerwrapper clearfix searchempty ">
	<nav class="b-commonnav noborder">
		<a href="#" class="current">новые</a>
		<a href="#">популярные</a>
		<a href="#">рекомендованные</a>
	</nav>
	<div class="b-collectionpage rel">
		
			
			<div class="b-boardslider narrow_panel js_slider_single_nodots">
				<div>
					<div class="js_flexbackground">
						<img src="./pic/slide/img_2.jpg" alt="" data-bgposition="50% 0" class="js_flex_bgimage" />
						<div class="wrapper bbox">
							<h3>100 рекомендованных книг для школьников
						от национальной электронной библиотеки</h3>
							<div class="b-bookboardmain iblock">
								<a href="#"><img src="./pic/pic_15.jpg" class="b-bookboard_img" alt=""></a>
								<a href="#" class="button_mode button_revers">15 книг в подборке</a>	
							</div>
							<div class="b-bookboard_cl iblock">
								<ul class="b-bookboard_list">
									<li><a href="#"><img class="loadingimg" src="./pic/pic_16.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_17.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_18.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_19.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_22.jpg" alt=""></a></li>
									<!-- <li><a href="#"><img class="loadingimg" src="./pic/pic_23.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_20.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_21.jpg" alt=""></a></li> -->
								</ul>
								<!--<a href="#" class="b-bookboard_more button_mode">посмотреть все</a>-->
							</div>
						</div>
					</div><!-- /.wrapper -->

				</div>
				<div>
					<div class="js_flexbackground">
						<img src="./pic/slide/img_2.jpg" alt="" data-bgposition="50% 0" class="js_flex_bgimage" />
						<div class="wrapper bbox">
						<h3>300 рекомендованных книг для школьников
						от национальной электронной библиотеки</h3>
							<div class="b-bookboardmain iblock">
								<a href="#"><img src="./pic/pic_15.jpg" class="loadingimg b-bookboard_img" alt=""></a>
								<a href="#" class="button_mode button_revers">15 книг в подборке</a>
							</div>
							<div class="b-bookboard_cl iblock">
								<ul class="b-bookboard_list">
									<li><a href="#"><img class="loadingimg" src="./pic/pic_16.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_17.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_18.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_19.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_22.jpg" alt=""></a></li>
									<!-- <li><a href="#"><img class="loadingimg" src="./pic/pic_23.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_20.jpg" alt=""></a></li>
									<li><a href="#"><img class="loadingimg" src="./pic/pic_21.jpg" alt=""></a></li> -->
								</ul>
								<!--<a href="#" class="b-bookboard_more button_mode">посмотреть все</a>-->
							</div>
						</div>
					</div><!-- /.wrapper -->

				</div>			
			</div> <!-- /.boardslider -->
			<a href="#" class="set_opener iblock right">Настройки</a>

		</div>
		<div class="b-mainblock left">		

			<div class="b-filter js_filter b-filternums">
				<div class="b-filter_wrapper">	
					<a href="#" class="sort sort_opener">Сортировать</a>
					<span class="sort_wrap">				
					<a href="#" class="sort up">По количеству книгу</a>
					<a href="#" class="sort">По названию</a>
					<a href="#" class="sort">По дате</a>
					</span>
					<span class="b-filter_act">					
						<span class="b-filter_show">Показать</span>
						<a href="#" class="b-filter_num b-filter_num_paging current">10</a>
						<a href="#" class="b-filter_num b-filter_num_paging ">25</a>
						<a href="#" class="b-filter_num b-filter_num_paging">50</a>				
					</span>
				</div>
			</div><!-- /.b-filter -->
			<div class="b-collection_list mode">
				<div class="b-collection-item rel renumber">
				<span class="num">1.</span>
					<div class="b-addcollecion rel right">
						<span class="metalabel">Добавить в мои подборки</span>
						<div class="meta"><a class="b-bookadd" data-collection="ajax_favs.html"  data-plus="Добавить в мои подборки" data-minus="Удалить из моих подборок" href="#"></a></div>
					</div>
					<h2><a href="#">Серебряный век: быт, нравы, моды, знаменитости</a></h2>
					<a href="#" class="button_mode b-bookincoll">15 книг в подборке</a>
					<div class="b-result_sorce_info"><em>Автор:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>
					<div class="clearfix">
						<div class="b-collcover iblock">
							<a href="#" class="b_bookpopular_photolg"><img src="./pic/pic_42.jpg" class="loadingimg b-bookboard_img" alt=""></a>
							<h3>путь к звездам</h3>
							<a href="#" class="b-autor">Циолковский К.Э.</a>
						</div>
						<div class="b-boardslider_min iblock">							
						
						<div class="b-boardslider js_slider_single_nodots  iblock">
							<div>	
								<div class="wrapper bbox">

									<div class="b-bookboard_cl iblock">
										<ul class="b-bookboard_list">
											<li><a href="#" class="b_bookpopular_photo"><img class="loadingimg" src="./pic/pic_12.jpg" alt=""></a></li>
											<li><a href="#" class="b_bookpopular_photo"><img class="loadingimg" src="./pic/pic_13.jpg" alt=""></a></li>
											<li><a href="#" class="b_bookpopular_photo"><img class="loadingimg" src="./pic/pic_5.jpg" alt=""></a></li>
											<li><a href="#" class="b_bookpopular_photo"><img class="loadingimg" src="./pic/pic_6.jpg" alt=""></a></li>										
										</ul>
										<!--<a href="#" class="b-bookboard_more button_mode">посмотреть все</a>-->
									</div>
								</div>	
							</div>
							<div>

								<div class="wrapper bbox">									
									<div class="b-bookboard_cl iblock">
										<ul class="b-bookboard_list">
											<li><a href="#" class="b_bookpopular_photo"><img class="loadingimg" src="./pic/pic_12.jpg" alt=""></a></li>
											<li><a href="#" class="b_bookpopular_photo"><img class="loadingimg" src="./pic/pic_13.jpg" alt=""></a></li>
											<li><a href="#" class="b_bookpopular_photo"><img class="loadingimg" src="./pic/pic_5.jpg" alt=""></a></li>
											<li><a href="#" class="b_bookpopular_photo"><img class="loadingimg" src="./pic/pic_6.jpg" alt=""></a></li>										
										</ul>
										<!--<a href="#" class="b-bookboard_more button_mode">посмотреть все</a>-->
									</div>
								</div>

							</div>
						</div> <!-- /.boardslider -->
						</div>
					</div><!-- /.clearfix -->


				</div><!-- /.b-collection-item -->
				<div class="b-collection-item rel renumber">
				<span class="num">2.</span>
					<div class="b-addcollecion rel right">
						<span class="metalabel">Добавить в мои подборки</span>
						<div class="meta"><a class="b-bookadd" data-collection="ajax_favs.html"  data-plus="Добавить в мои подборки" data-minus="Удалить из моих подборок" href="#"></a></div>
					</div>
					<h2><a href="#">Серебряный век: быт, нравы, моды, знаменитости</a></h2>
					<a href="#" class="button_mode b-bookincoll">15 книг в подборке</a>
					<div class="b-result_sorce_info"><em>Автор:</em> <a href="#" class="b-sorcelibrary">Рязанская областная библиотека</a></div>
					<div class="clearfix">
						<div class="b-collcover iblock">
							<a href="#" class="b_bookpopular_photolg"><img src="./pic/pic_42.jpg" class="loadingimg b-bookboard_img" alt=""></a>
							<h3>путь к звездам</h3>
							<a href="#" class="b-autor">Циолковский К.Э.</a>
						</div>
						<div class="b-boardslider_min iblock">							
						
						<div class="b-boardslider js_slider_single_nodots  iblock">
							<div>	
								<div class="wrapper bbox">

									<div class="b-bookboard_cl iblock">
										<ul class="b-bookboard_list">
										</ul>
										<!--<a href="#" class="b-bookboard_more button_mode">посмотреть все</a>-->
									</div>
								</div>	
							</div>
						</div> <!-- /.boardslider -->
						</div>
					</div><!-- /.clearfix -->


				</div><!-- /.b-collection-item -->

			</div><!-- /.b-collection_list -->

		</div><!-- /.b-mainblock -->
		<div class="b-side right mtm10">

			<!--<div class="b-libfond nomargin">
			<div class="b-loadlogo rel">
				<a href="#">загрузить логотип</a> 
				<input type="file" name="" id="" class="b-loadlogofield">
			</div>
			<div class="b-fond_img">
				<img src="./pic/pic_31.png" alt="">
			</div>
			<span class="b-fondinfo_number_lb">В фонде библиотеки</span>
			<span class="b-portalinfo_numbers">
				<span class="b-portalinfo_number mr iblock">1</span>
				<span class="b-portalinfo_number iblock">0</span>
				<span class="b-portalinfo_number iblock">5</span>
				<span class="b-portalinfo_number mr iblock">1</span>
				<span class="b-portalinfo_number iblock">2</span>
				<span class="b-portalinfo_number iblock">1</span>
				<span class="b-portalinfo_number iblock">7</span></span>
				<span class="b-fondinfo_number_lb">изданий</span>
				<div class="b-lib_collect">
					<span class="b-fondinfo_number_lb">Библиотека собрала</span>
					<span class="b-portalinfo_numbers">

						<span class="b-portalinfo_number iblock">2</span>
						<span class="b-portalinfo_number iblock">1</span>
						<span class="b-portalinfo_number iblock">7</span></span>
						<span class="b-fondinfo_number_lb">коллекций</span>
						<a href="#" class="button_mode button_revers">посмотреть все</a>
					</div>
					<div class="b-lib_counter">
						<div class="b-lib_counter_lb">Количество читателей</div>
						<div class="icouser">х 108</div>
					</div>
					<div class="b-lib_counter">
						<div class="b-lib_counter_lb">Количество просмотров</div>
						<div class="icoviews">х 15 678</div>
					</div>
					<a href="#" class="button_mode black">полная статистика</a>
				</div><!-- /.b-libfond -->
				<div class="b-sidenav">
					<a href="#" class="b-sidenav_title">Библиотеки</a>
				<div class="b-sidenav_cont">	
					<ul class="b-sidenav_cont_list">
						<li class="clearfix">
							<div class="b-sidenav_value left">Библиотеки</div>
							<div class="checkwrapper type2 right">
								<label for="cb2" class="black">1</label><input class="checkbox" type="checkbox" name="" id="cb2">
							</div>

						</li>
					</ul>
				</div>
				</div>

		</div><!-- /.b-side -->

	</section>

</div><!-- /.homepage -->


<? include("footer.php"); ?>
<!--popup добавить в подборки-->
<div class="selectionBlock">
	
	<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu">мои подборки</a>
	

	<form class="b-selectionadd" action="">
		<input type="submit" class="b-selectionaddsign" data-collection="ajax_favs.html" value="+">
		<span class="b-selectionaddtxt">Cоздать подборку</span>
		<input type="text" class="input hidden">
	</form>
</div><!-- /.selectionBlock -->
<!--/popup добавить в подборки-->
<!--popup Удалить из подборки--><div class="b-removepopup hidden"><p>Удалить книгу из Моей библиотеки?</p><a href="#" class="formbutton btremove">Удалить</a><a href="#" class="formbutton gray">Оставить</a></div><!--/popup Удалить из подборки-->

</html>