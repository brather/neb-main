<?php
/**
 * User: agolodkov
 * Date: 06.10.2016
 * Time: 16:46
 */

namespace AssessorBundle\Controller\Api;


use AssessorBundle\Entity\AssessorRatingEntityInterface;
use AssessorBundle\Entity\Repository\CommonRepository;
use AppBundle\Security\UserProvider;
use Doctrine\Bundle\DoctrineBundle\Registry;
use FOS\RestBundle\View\View;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use BitrixBundle\Annotations\IncludeBitrix;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ApiCommonController
 *
 * @DI\Service("assessor.api.common", scope="request")
 */
class ApiCommonController
{
    /**
     * @DI\Inject("service_container")
     * @var ContainerInterface
     */
    public $container;

    /**
     * @DI\Inject("doctrine")
     * @var Registry
     */
    public $doctrine;

    /**
     * @DI\Inject("form.factory")
     * @var FormFactory
     */
    public $formFactory;

    /**
     * @DI\Inject("request")
     * @var Request
     */
    public $request;


    /**
     * @param string  $entity
     * @param Request $request
     *
     * @IncludeBitrix()
     *
     * @return static
     * @throws \Exception
     */
    public function createAction($entity, Request $request)
    {
        $api = $this->_getEntityApi($entity);
        $em = $api->getEntityManager();
        $entityName = $api->getEntityName();
        $entityName = explode(':', $entityName);
        $entityName = end($entityName);
        $entityClass = 'AssessorBundle\Entity\\' . $entityName;
        if (!class_exists($entityClass)) {
            throw new \Exception('Entity not found!');
        }
        $entityInstance = new $entityClass();
        $form = $this->formFactory->create(
            $api->getFormType(),
            $entityInstance,
            ['method' => 'POST']
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($form->getData());
            $em->flush();

            return View::create($entityInstance, 200);
        }

        return View::create($form, 400);
    }

    /**
     * @param string $entity
     * @param int    $id
     *
     * @IncludeBitrix()
     *
     * @return static
     * @throws \Exception
     */
    public function patchAction($entity, $id)
    {
        $api = $this->_getEntityApi($entity);
        $em = $api->getEntityManager();
        $entity = $em->getRepository($api->getEntityName())->find($id);
        if (!$entity instanceof AssessorRatingEntityInterface) {
            throw new NotFoundHttpException('Entity not found!');
        }

        $form = $this->formFactory->create(
            $api->getFormType(),
            $entity,
            ['method' => 'PATCH']
        );
        $form->handleRequest($this->request);

        if ($form->isValid()) {
            $em->merge($form->getData());
            $em->flush();

            return View::create($entity, 200);
        }

        return View::create($form, 400);
    }

    /**
     * @param string $entity
     * @param int    $id
     *
     * @IncludeBitrix()
     *
     * @return static
     * @throws \Exception
     */
    public function deleteAction($entity, $id)
    {
        $api = $this->_getEntityApi($entity);
        $em = $api->getEntityManager();
        $entity = $em->getRepository($api->getEntityName())->find($id);
        if (!$entity instanceof AssessorRatingEntityInterface) {
            throw new NotFoundHttpException('Entity not found!');
        }

        $em->remove($entity);
        $em->flush();
        if ($entity = $em->getRepository($api->getEntityName())->find($id)) {
            throw new UnprocessableEntityHttpException('Cannot delete entity: ' . $id);
        }

        return View::create(null, 200);
    }

    /**
     * @param string $entity
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return array
     */
    public function autoCompleteAction($entity)
    {
        $api = $this->_getEntityApi($entity);
        $query = $this->request->query->get('query');
        $entities = [];
        if (!empty($query)) {
            /** @var CommonRepository $repository */
            $repository = $api
                ->getEntityManager()
                ->getRepository($api->getEntityName());
            $qb = $repository->createQueryBuilder('t');
            $qb->setMaxResults(10);
            $api->handleAutoComplete($qb);
            $entities = $qb->getQuery()->getResult();
        }

        return [
            'entities' => $entities
        ];
    }

    /**
     * @param string $entity
     *
     * @Method("GET")
     * @Rest\View(serializerGroups={"list"})
     * @IncludeBitrix()
     *
     * @return array
     */
    public function searchAutoCompleteAction($entity)
    {
        $api = $this->_getEntityApi($entity);
        $query = $this->request->query->get('query');
        $entities = [];
        if (!empty($query)) {
            /** @var CommonRepository $repository */
            $repository = $api
                ->getEntityManager()
                ->getRepository($api->getEntityName());
            $qb = $repository->createQueryBuilder('t');
            $qb->setMaxResults(10);
            $api->handleAutoComplete($qb);
            $entities = $qb->getQuery()->getResult();
        }

        return [
            'entities' => $entities
        ];
    }


    /**
     * @param string $entity
     *
     * @return RatingApiInterface
     * @throws \Exception
     */
    private function _getEntityApi($entity)
    {
        $serviceName = 'assessor.api.' . $entity;
        if (!$this->container->has($serviceName)) {
            throw new \Exception("Not found service for entity $entity");
        }

        return $this->container->get($serviceName);
    }
}