<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__DIR__.'/template.php');
?>

<?if(!empty($arResult['POPULAR_BOOKS'])):?>
	<h2 style="margin-left: 15px;"><?=Loc::getMessage('NEB_BOOKS_POPULAR_TITLE');?></h2>
	<?foreach($arResult['POPULAR_BOOKS'] as $arItem):?>
        <div class="catalog__item">
            <?/* <a class="catalog__item__link" href="http://electronic-library.ru/?id=<?=$arItem['~id']?>"> */?>
            <a class="catalog__item__link" href="/catalog/<?=$arItem['~id']?>/">
                <div class="catalog__item__image">
                    <img src="/local/tools/exalead/thumbnail.php?url=<?=$arItem['URL']?>&source=<?=$arItem['source']?>">
                </div>
                <span class=""><?=$arItem['title']?></span>
            </a>
            
            <span class="catalog__item-description">Автор:
                <a class="catalog__item-author"><?=$arItem['authorbook']?></a>
            </span>                   
        </div><?/* class="catalog__item" */?>
    <?endforeach;?>
<?endif;?>

<?//echo '<pre>'; print_r($arResult); echo '</pre>'?>