<?php

namespace Nota\Journal\Agents;

class ClearJournal
{
    public static function clear(){

        $daysToKeep = \COption::GetOptionString("nota.journal", "neb_journal_days");
        $journalConnection = \Bitrix\Main\Application::getConnection('journal');

        $result = $journalConnection->query('show tables');
        while($res = $result->fetch()){
            $journalConnection->query('DELETE FROM ' . $res['Tables_in_neb_journal'] . ' WHERE TIMESTAMPDIFF(DAY, TIMESTAMP, NOW()) > ' . $daysToKeep);
        }

        return 'Nota\Journal\Agents\ClearJournal::clear();';
    }
}