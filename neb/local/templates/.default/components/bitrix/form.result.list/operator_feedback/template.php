<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
define('FB_STATUS_REGISTERED', 1); // статус "Зарегистрирована"
define('FB_STATUS_CLOSED', 4); // статус "Закрыта (ответ напарвлен)"
define('FB_STATUS_IN_PROCESS', 6); // статус "В работе"
define('FB_STATUS_REJECTED', 7); // статус "Отклонена"
?>
<script>
	function Form_Filter_Click_<?=$arResult["filter_id"]?>()
	{
		var sName = "<?=$arResult["tf_name"]?>";
		var filter_id = "form_filter_<?=$arResult["filter_id"]?>";
		var form_handle = document.getElementById(filter_id);

		if (form_handle)
		{
			if (form_handle.className != "form-filter-none")
			{
				form_handle.className = "form-filter-none";
				document.cookie = sName+"="+"none"+"; expires=Fri, 31 Dec 2030 23:59:59 GMT;";
			}
			else
			{
				form_handle.className = "form-filter-inline";
				document.cookie = sName+"="+"inline"+"; expires=Fri, 31 Dec 2030 23:59:59 GMT;";
			}
		}
	}
</script>
<p>
	<?=($arResult["is_filtered"] ? "<span class='form-filteron'>".GetMessage("FORM_FILTER_ON") : "<span class='form-filteroff'>".GetMessage("FORM_FILTER_OFF"))?></span>&nbsp;&nbsp;&nbsp;
	[ <a href="javascript:void(0)" OnClick="Form_Filter_Click_<?=$arResult["filter_id"]?>()"><?=GetMessage("FORM_FILTER")?></a> ]
</p>
<?
/***********************************************
filter
 ************************************************/
?>

<form name="form1" method="GET" action="<?=$APPLICATION->GetCurPageParam("", array("sessid", "delete", "del_id", "action"), false)?>?" id="form_filter_<?=$arResult["filter_id"]?>" class="form-filter-<?=htmlspecialcharsbx($arResult["tf"]);?>">
	<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>" />
	<?if ($arParams["SEF_MODE"] == "N"):?><input type="hidden" name="action" value="list" /><?endif?>
	<table class="form-filter-table data-table">
		<thead>
		<tr>
			<th colspan="2">&nbsp;</th>
		</tr>
		</thead>
		<tbody>
		<?
		if (strlen($arResult["str_error"]) > 0)
		{
			?>
			<tr>
				<td class="errortext" colspan="2"><?=$arResult["str_error"]?></td>
			</tr>
			<?
		} // endif (strlen($str_error) > 0)
		?>
		<tr>
			<td><?=GetMessage("FORM_F_ID")?></td>
			<td><?=CForm::GetTextFilter("id", 45, "", "")?></td>
		</tr>
		<?
		if ($arParams["SHOW_STATUS"]=="Y")
		{
			?>
			<tr>
				<td><?=GetMessage("FORM_F_STATUS")?></td>
				<td><select name="find_status" id="find_status">
						<option value="NOT_REF"><?=GetMessage("FORM_ALL")?></option>
						<?
						foreach ($arResult["arStatuses_VIEW"] as $arStatus)
						{
							?>
							<option value="<?=$arStatus["REFERENCE_ID"]?>"<?=($arStatus["REFERENCE_ID"]==$arResult["__find"]["find_status"] ? " SELECTED=\"1\"" : "")?>><?=$arStatus["REFERENCE"]?></option>
							<?
						}
						?>
					</select></td>
			</tr>

			<?
		} //endif ($SHOW_STATUS=="Y");
		?>
		<tr>
			<td><?=GetMessage("FORM_F_DATE_CREATE")." (".CSite::GetDateFormat("SHORT")."):"?></td>
			<td><?=CForm::GetDateFilter("date_create", "form1", "Y", "", "")?></td>
		</tr>
		<tr>
			<td><?=GetMessage("FORM_F_TIMESTAMP")." (".CSite::GetDateFormat("SHORT")."):"?></td>
			<td><?=CForm::GetDateFilter("timestamp", "form1", "Y", "", "")?></td>
		</tr>
		<?
		if ($arParams["F_RIGHT"] >= 25)
		{
			?>

			<tr>
				<td><?=GetMessage("FORM_F_USER")?></td>
				<td><?=CForm::GetTextFilter("user_id", 45, "", "")?></td>
			</tr>

			<?
		} // endif($F_RIGHT>=25);
		?>
		<?
		if (is_array($arResult["arrFORM_FILTER"]) && count($arResult["arrFORM_FILTER"])>0)
		{
		?>
		<?
		if ($arParams["F_RIGHT"] >= 25)
		{
			?>
			<tr>
				<th colspan="2"><?=GetMessage("FORM_QA_FILTER_TITLE")?></th>
			</tr>
			<?
		} // endif ($F_RIGHT>=25);
		?><?
		foreach ($arResult["arrFORM_FILTER"] as $arrFILTER)
		{
		$prev_fname = "";

		foreach ($arrFILTER as $arrF)
		{
		if ($arParams["SHOW_ADDITIONAL"] == "Y" || $arrF["ADDITIONAL"] != "Y")
		{
		$i++;
		if ($arrF["SID"]!=$prev_fname)
		{
		if ($i>1)
		{
			?>
			</td>
			</tr>
			<?
		} //endif($i>1);
		?>
		<tr>
			<td>
				<?=$arrF["FILTER_TITLE"] ? $arrF['FILTER_TITLE'] : $arrF['TITLE']?>
				<?=($arrF["FILTER_TYPE"]=="date" ? " (".CSite::GetDateFormat("SHORT").")" : "")?>
			</td>
			<td>
				<?
				} //endif ($fname!=$prev_fname) ;
				?>
				<?
				switch($arrF["FILTER_TYPE"]){
					case "text":
						echo CForm::GetTextFilter($arrF["FID"]);
						break;
					case "date":
						echo CForm::GetDateFilter($arrF["FID"]);
						break;
					case "integer":
						echo CForm::GetNumberFilter($arrF["FID"]);
						break;
					case "dropdown":
						echo CForm::GetDropDownFilter($arrF["ID"], $arrF["PARAMETER_NAME"], $arrF["FID"]);
						break;
					case "exist":
						?>
						<?=CForm::GetExistFlagFilter($arrF["FID"])?>
						<?=GetMessage("FORM_F_EXISTS")?>
						<?
						break;
				} // endswitch
				?>
				<?
				if ($arrF["PARAMETER_NAME"]=="ANSWER_TEXT")
				{
					?>
					&nbsp;[<span class='form-anstext'>...</span>]
					<?
				}
				elseif ($arrF["PARAMETER_NAME"]=="ANSWER_VALUE")
				{
					?>
					&nbsp;(<span class='form-ansvalue'>...</span>)
					<?
				}
				?>
				<br />
				<?
				$prev_fname = $arrF["SID"];
				} //endif (($arrF["ADDITIONAL"]=="Y" && $SHOW_ADDITIONAL=="Y") || $arrF["ADDITIONAL"]!="Y");

				} // endwhile (list($key, $arrF) = each($arrFILTER));

				} // endwhile (list($key, $arrFILTER) = each($arrFORM_FILTER));
				} // endif(is_array($arrFORM_FILTER) && count($arrFORM_FILTER)>0);
				?></td>
		</tr>
		</tbody>
		<tfoot>
		<tr>
			<th colspan="2">
				<input type="submit" name="set_filter" value="<?=GetMessage("FORM_F_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("FORM_F_DEL_FILTER")?>" />
			</th>
		</tr>
		</tfoot>
	</table>
</form>
<br />


<?
if ($arParams["can_delete_some"])
{
	?>
	<SCRIPT LANGUAGE="JavaScript">
		function OnDelete_<?=$arResult["filter_id"]?>()
		{
			var show_conf;
			var arCheckbox = document.forms['rform_<?=$arResult["filter_id"]?>'].elements["ARR_RESULT[]"];
			if(!arCheckbox) return;
			if(arCheckbox.length>0 || arCheckbox.value>0)
			{
				show_conf = false;
				if (arCheckbox.value>0 && arCheckbox.checked) show_conf = true;
				else
				{
					for(i=0; i<arCheckbox.length; i++)
					{
						if (arCheckbox[i].checked)
						{
							show_conf = true;
							break;
						}
					}
				}
				if (show_conf)
					return confirm("<?=GetMessage("FORM_DELETE_CONFIRMATION")?>");
				else
					alert('<?=GetMessage("FORM_SELECT_RESULTS")?>');
			}
			return false;
		}

		function OnSelectAll_<?=$arResult["filter_id"]?>(fl)
		{
			var arCheckbox = document.forms['rform_<?=$arResult["filter_id"]?>'].elements["ARR_RESULT[]"];
			if(!arCheckbox) return;
			if(arCheckbox.length>0)
				for(i=0; i<arCheckbox.length; i++)
					arCheckbox[i].checked = fl;
			else
				arCheckbox.checked = fl;
		}
	</SCRIPT>
	<?
} //endif($can_delete_some);
?>

<dl class="responsive-table-layout biblio-feedback">
	<dt>
		<span><a href="#">Id</a></span>
		<span><a class="sort down">Дата&nbsp;созд.</a>&nbsp;/<br/><a>Дата&nbsp;закр.</a></span>
		<span><a>№&nbsp;ч.б.</a>&nbsp;/<br/><a>E-mail</a></span>
		<span>Тема</span>
		<span>Содержимое</span>
		<span><a>Статус</a>&nbsp;/<br/><a>Исполнитель</a></span>
	</dt>

	<?php
	foreach ($arResult["arrResults"] as $arRes)
	{
		?>
		<dd>
			<span><?=$arRes["ID"] ?></span>
			<span><?=$arRes["TSX_0"]?> <?=$arRes["TSX_1"]?></span>
			<span>
				<?
				if ($arRes["USER_ID"]>0)
				{
					$userName = array("NAME" => $arRes["USER_FIRST_NAME"], "LAST_NAME" => $arRes["USER_LAST_NAME"], "SECOND_NAME" => $arRes["USER_SECOND_NAME"], "LOGIN" => $arRes["LOGIN"]);
					?>
					<a title="<?=GetMessage("FORM_EDIT_USER")?>" href="/bitrix/admin/user_edit.php?lang=<?=LANGUAGE_ID?>&ID=<?=$arRes["USER_ID"]?>"><?=$arRes["USER_ID"]?></a>
					<i><?=$arRes["LOGIN"]?></i>
					<?if($arRes["USER_AUTH"]=="N") { ?><?=GetMessage("FORM_NOT_AUTH")?><?}?>
					<?
				}
				else
				{
					?>
					<?=GetMessage("FORM_NOT_REGISTERED")?>
					<?
				} // endif ($GLOBALS["f_USER_ID"]>0);
				?>

			</span>
			<span>
				<?
				// todo заменить магические цифры на константы
				reset($arResult["arrAnswers"][$arRes["ID"]][2]);
				$ans = explode('/', current($arResult["arrAnswers"][$arRes["ID"]][2])['ANSWER_TEXT']);
				$ans = $ans[0];
				echo $ans;
				?>
			</span>
            <span>
                <strong data-view-detail-message>
                    <?=current($arResult["arrAnswers"][$arRes["ID"]][3])['USER_TEXT']?>
                </strong>
            </span>
			<span>
				<?=$arRes["STATUS_TITLE"]?><br/>
				<?

				// если закрыта или отклонена, то только история
				// если в работе, то нельзя отклонить и нельзя назначить
				if ($arRes["STATUS_ID"] == FB_STATUS_REGISTERED)
				{
					?>
					<a href="<?=$arParams['SEF_FOLDER']?>appoint/?id=<?=$arRes["ID"]?>">Назначить</a><br>
					<?
				}
				if ($arRes["STATUS_ID"] == FB_STATUS_IN_PROCESS|| $arRes["STATUS_ID"] == FB_STATUS_REGISTERED)
				{
					?>
					<a href="<?=$arParams['SEF_FOLDER']?>reply/?id=<?=$arRes["ID"]?>">Ответить</a><br>

					<?
				}
				if ($arRes["STATUS_ID"] == FB_STATUS_REGISTERED)
				{
					?>
					<a href="<?=$arParams['SEF_FOLDER']?>reject/?id=<?=$arRes["ID"]?>">Отклонить</a><br>

					<?
				}
				?>
				<a href="<?=$arParams['SEF_FOLDER']?>history/?id=<?=$arRes["ID"]?>">История</a><br>
			</span>
		</dd>
		<?
	} // foreach ($arResult["arrResults"] as $arRes)
	?>
</dl>
<p><?=$arResult["pager"]?></p>
<?
if (intval($arResult["res_counter"])>0 && $arParams["SHOW_STATUS"]=="Y" && $arParams["F_RIGHT"] >= 15)
{
	?>
	<p>
	</p>
	<?
} //endif (intval($res_counter)>0 && $SHOW_STATUS=="Y" && $F_RIGHT>=15);
?>

<?
if ($arParams["can_delete_some"])
{
	?>
	<p><input type="submit" name="delete" value="<?=GetMessage("FORM_DELETE_SELECTED")?>" onClick="return OnDelete_<?=$arResult["filter_id"]?>()" /></p>
	<?
} //endif ($can_delete_some);
?>


