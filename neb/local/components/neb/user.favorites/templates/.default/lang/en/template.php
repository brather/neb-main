<?
$MESS['FAVORITES_SORT'] = 'Sort';
$MESS['FAVORITES_SORT_AUTHOR'] = 'Author';
$MESS['FAVORITES_SORT_MAIN_TITLE'] = 'Main title';
$MESS['FAVORITES_SORT_DATE'] = 'Date';
$MESS['FAVORITES_SHOW'] = 'Show';
$MESS['FAVORITES_SETTINGS'] = 'Settings';
$MESS['FAVORITES_REMOVE'] = 'Remove';
$MESS['FAVORITES_FROM_MY_LIBRARY'] = 'from my library';
$MESS['FAVORITES_AUTHOR'] = 'Author';
$MESS['FAVORITES_YEAR_PUBLICATION'] = 'Year of publication';
$MESS['FAVORITES_NUMBER_OF_PAGES'] = 'Number of pages';
$MESS['FAVORITES_PUBLISHER'] = 'Publisher';
$MESS['FAVORITES_MY_COLLECTIONS'] = 'my collections';
$MESS['FAVORITES_CREATE_COLLECTION'] = 'create a collection';
$MESS['FAVORITES_READ'] = 'Read';
$MESS['FAVORITES_SERVER_ERROR'] = 'Server error. Please try again';
$MESS['FAVORITES_DELETE_CONFIRM'] = 'Remove from profile?';
$MESS['FAVORITES_DELETE_YES'] = 'Delete';
$MESS['FAVORITES_DELETE_NO'] = 'Cancel';
$MESS['FAVORITES_DELETE_TITLE'] = 'Please confirm action';
$MESS['FAVORITES_DELETE_TEXT'] = 'Are you sure you want to delete edition from profile?';
?>