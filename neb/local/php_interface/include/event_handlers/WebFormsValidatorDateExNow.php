<?php

/**
 * Валидация даты относительно текущей даты
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/form/validators/val_date_ex.php';

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\EventManager;

Loc::loadMessages(__FILE__);

EventManager::getInstance()->addEventHandler('form', 'onFormValidatorBuildList', ['WebFormsValidatorDateExNow', 'GetDescription']);

class WebFormsValidatorDateExNow extends CFormValidatorDateEx
{
    public function GetDescription()
    {
        return array(
            'NAME'            => 'date_ext_now',
            'DESCRIPTION'     => 'Date validation',
            'TYPES'           => array('date'),
            'SETTINGS'        => array('WebFormsValidatorDateExNow', 'GetSettings'),
            'CONVERT_TO_DB'   => array('WebFormsValidatorDateExNow', 'ToDB'),
            'CONVERT_FROM_DB' => array('WebFormsValidatorDateExNow', 'FromDB'),
            'HANDLER'         => array('WebFormsValidatorDateExNow', 'DoValidate')
        );
    }

    public function ToDB($arParams)
    {
        if (strlen($arParams['DATE_FROM']) > 0) {
            $arParams['DATE_FROM'] = '{now}' === $arParams['DATE_FROM']
                ? $arParams['DATE_FROM']
                : MakeTimeStamp(
                    $arParams['DATE_FROM']
                );
        }
        if (strlen($arParams['DATE_TO']) > 0) {
            $arParams['DATE_TO'] = '{now}' === $arParams['DATE_TO']
                ? $arParams['DATE_TO']
                : MakeTimeStamp(
                    $arParams['DATE_TO']
                );
        }

        if ($arParams['DATE_FROM'] > $arParams['DATE_TO']
            && strlen($arParams['DATE_TO']) > 0
        ) {
            $tmp = $arParams['DATE_FROM'];
            $arParams['DATE_FROM'] = $arParams['DATE_TO'];
            $arParams['DATE_TO'] = $tmp;
        }

        return serialize($arParams);
    }

    public function FromDB($strParams)
    {
        $arParams
            = unserialize($strParams);
        if ('{now}' === $arParams['DATE_FROM']) {
            $arParams['DATE_FROM']
                = time();
        }

        if ('{now}' === $arParams['DATE_TO']) {
            $arParams['DATE_TO'] = time();
        }

        if (strlen($arParams['DATE_FROM']) > 0) {
            $arParams['DATE_FROM'] = ConvertTimeStamp(
                $arParams['DATE_FROM'], 'SHORT'
            );
        }
        if (strlen($arParams['DATE_TO']) > 0) {
            $arParams['DATE_TO'] = ConvertTimeStamp(
                $arParams['DATE_TO'], 'SHORT'
            );
        }

        return $arParams;
    }
    function DoValidate($arParams, $arQuestion, $arAnswers, $arValues)
    {
        global $APPLICATION;

        foreach ($arValues as $value)
        {
            // check minimum date
            if (strlen($arParams["DATE_FROM"]) > 0 && MakeTimeStamp($value) < MakeTimeStamp($arParams["DATE_FROM"]))
            {
                $APPLICATION->ThrowException(GetMessage("FORM_VALIDATOR_VAL_DATE_EX_ERROR_LESS_NOW"));
                return false;
            }

            // check maximum date
            if (strlen($arParams["DATE_TO"]) > 0 && MakeTimeStamp($value) > MakeTimeStamp($arParams["DATE_TO"]))
            {
                $APPLICATION->ThrowException(GetMessage("FORM_VALIDATOR_VAL_DATE_EX_ERROR_MORE_NOW"));
                return false;
            }
        }

        return true;
    }
}