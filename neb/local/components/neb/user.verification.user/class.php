<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Iblock\ElementTable;
use \Bitrix\Main\Config\Option,
    \Bitrix\Main\UserTable,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\GroupTable,
    \Bitrix\Main\UserGroupTable,
    \Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;

CBitrixComponent::includeComponentClass('neb:user.verification');

class UserVerificationUser extends UserVerificationComponent
{
    protected $arRequest = [], $arUser = [];

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $this->getRequest();
        $this->getUser();

        foreach ($this->arRequest['EDIT'] as $k => $v)
            $this->arResult['EDIT'][$k] = trim(htmlspecialchars($v));

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->_switchAction();
        $this->includeComponentTemplate();
    }

    private function _switchAction() {
        if ($this->arParams['ID'] > 0 && $this->arParams['DEACTIVATE'] == 'Y') {
            $this->_deactivateUser();
        }
        else {
            $this->_setData();
        }
    }

    /**
     * Получение идентификаторов групп пользователей, защищенных от деактивации
     *
     * @return array
     */
    private static function _getProtectedGroups() {

        $arResult = $arGroups = [];
        $arConstants = get_defined_constants();

        foreach ($arConstants as $key => $val)
            if (false !== strpos($key, 'UGROUP_'))
                $arGroups[] = $val;

        if (empty($arGroups))
            $arResult;

        $rsUserGroups = GroupTable::getList([
            'filter' => ['STRING_ID' => $arGroups, 'ACTIVE' => 'Y'],
            'select' => ['ID', 'STRING_ID']
        ]);
        while ($arFields = $rsUserGroups->fetch())
            $arResult[$arFields['ID']] = $arFields['STRING_ID'];

        return $arResult;
    }

    /**
     * Деактивация пользователя
     */
    private function _deactivateUser() {

        $iUser = intval($this->arParams['ID']);
        if ($iUser <= 0)
            return;

        $arHIblock      = HighloadBlockTable::getList(['filter' => ['NAME' => 'UsersTickets']])->fetchRaw();
        $obUsersTickets = HighloadBlockTable::compileEntity($arHIblock)->getDataClass();
        $arTicket       = $obUsersTickets::getList([
            "order"  => ["ID" => "DESC"],
            "filter" => ['UF_USER_ID' => $iUser],
            "select" => ['ID', 'UF_USER_ID']
        ])->fetch();

        if ($arTicket['ID'] >= 0) {

            $arUser =  UserTable::getById($arTicket['UF_USER_ID'])->fetch();

            if ($arUser['ID'] > 0) {

                $rsUserGroups = UserGroupTable::getList(['filter' => ['USER_ID' => $arUser['ID']], 'select' => ['GROUP_ID']]);
                while ($arFields = $rsUserGroups->fetch())
                    $arUserGroups[] = $arFields['GROUP_ID'];

                if (!empty($arUserGroups)) {
                    $arGroups = static::_getProtectedGroups();
                    $arProtected = array_intersect($arUserGroups, array_keys($arGroups));
                    if (!empty($arProtected))
                        unset($arUser['ACTIVE']);
                }

                if ($arUser['ACTIVE'] == 'Y') {
                    $obUser = new CUser();
                    $obUser->Update($arUser['ID'], ['ACTIVE' => 'N']);
                }
            }
        }

        LocalRedirect($this->arParams['LIST_URL']);
    }

    /**
     * Записывает данные в БД
     */
    private function _setData() {

        $arRequest = $this->arResult['EDIT'];

        if ($arRequest['EDIT'] != 'Y') {
            return;
        }

        if (empty($arRequest['EMAIL'])) {
            $arError[] = 'Поле "Электронная почта" обязательно для заполнения!';
        }
        if (empty($arRequest['PASSWORD'])) {
            $arError[] = 'Поле "Пароль" обязательно для заполнения!';
        }
        if (empty($arRequest['CONFIRM_PASSWORD'])) {
            $arError[] = 'Поле "Подтверждение пароля" обязательно для заполнения!';
        }

        if (!empty($arRequest['PASSWORD']) && !empty($arRequest['CONFIRM_PASSWORD'])
                && $arRequest['PASSWORD'] != $arRequest['CONFIRM_PASSWORD']) {
            $arError[] = 'Пароль и его подтверждение не совпадают!';
        }

        if (!empty($arRequest['NUMBER']) || !empty($arRequest['DATE'])) {
            if (empty($arRequest['NUMBER'])) {
                $arError[] = 'Поле "Номер" обязательно для заполнения при заполненном поле "Дата"!';
            }
            if (empty($arRequest['DATE'])) {
                $arError[] = 'Поле "Дата" обязательно для заполнения при заполненном поле "Номер"!';
            }
            if (!empty($arRequest['DATE'])) {
                try {
                    new DateTime($arRequest['DATE']);
                } catch (Exception $e) {
                    $arError[] = str_replace('Incorrect date/time', 'Введена некорректная дата', $e->getMessage());
                }
            }
        }

        // поиск пользователя
        if (!empty($arRequest['EMAIL'])) {
            $arUser = CUser::GetByLogin($arRequest['EMAIL'])->Fetch();
            if (!empty($arUser))
                $arError[] = 'Пользователь с email "' . $arRequest['EMAIL'] . '" уже существует в системе!';
        }

        // проверка ЭЧЗ
        if (intval($this->arUser['UF_WCHZ']) <= 0) {
            $arError[] = 'Текущий пользователь не привязан к ЭЧЗ!';
        } else {
            $arWorkplace = $this->getWorkplace($this->arUser['UF_WCHZ']);
            if (empty($arWorkplace)) {
                $arError[] = 'ЭЧЗ оператора (ID: ' . $this->arUser['UF_WCHZ'] . ') отсуствует в списке ЭЧЗ!';
            }
            elseif ($arWorkplace['ACTIVE'] == 'N') {
                $arError[] = 'ЭЧЗ оператора (' . $arWorkplace['NAME'] . ') деактивирован!';
            }
            elseif (intval($arWorkplace['PROPERTY_LIBRARY_VALUE']) == 0) {
                $arError[] = 'В ЭЧЗ ' . $arWorkplace['NAME'] . ' не задана привязка к библиотеке!';
            }
        }

        if (empty($arError)) {

            // добавление пользователя
            $arFields = [
                'EMAIL'             => $arRequest['EMAIL'],
                'LOGIN'             => $arRequest['EMAIL'],
                'LID'               => SITE_ID,
                'ACTIVE'            => 'Y',
                'GROUP_ID'          => [],
                'PASSWORD'          => $arRequest['PASSWORD'],
                'CONFIRM_PASSWORD'  => $arRequest['CONFIRM_PASSWORD'],
                'CONFIRM_CODE'      => randString(8),
                'XML_ID'            => 'workplace'
            ];
            $obUser = new CUser;
            $iUser = $obUser->Add($arFields);

            if (intval($iUser) <= 0) {
                $arError[] = $obUser->LAST_ERROR;
            }

            // добавление ЭЧБ
            if ($iUser > 0 && !empty($arRequest['NUMBER']) && !empty($arRequest['DATE'])) {

                $this->includeModules(['highloadblock', 'iblock']);

                $arHIblock      = HighloadBlockTable::getList(['filter' => ['NAME' => 'UsersTickets']])->fetchRaw();
                $obUsersTickets = HighloadBlockTable::compileEntity($arHIblock)->getDataClass();

                $arTicket = [
                    'UF_USER_ID' => $iUser,
                    'UF_NUMBER'  => $arRequest['NUMBER'],
                    'UF_DATE'    => $arRequest['DATE'],
                    'UF_WCHZ_ID' => $this->arUser['UF_WCHZ']
                ];
                $result = $obUsersTickets::add($arTicket);
                if (!$result->isSuccess())
                    $arError[] = implode(', ', $result->getErrorMessages());
            }

            // отправка писем на почту
            if ($iUser > 0)
            {
                $aUser = CUser::GetByID($iUser)->Fetch(); // массив только что добавленного пользователя
                $this->getUser(); // массив авторизированного пользователя

                // выборка библиотеки
                $this->includeModules(['highloadblock']);
                $arLibsHIblock = HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
                $obLibsTable   = HighloadBlockTable::compileEntity($arLibsHIblock)->getDataClass();
                $arLibrary = $obLibsTable::getById($arWorkplace['PROPERTY_LIBRARY_VALUE'])->fetch();

                $arEventFields                  = $arFields;
                $arEventFields['USER_ID']       = $iUser;
                $arEventFields['DATE_CREATE']   = $aUser['DATE_REGISTER'];
                $arEventFields['LIBRARY_NAME']  = $arLibrary['UF_NAME'];

                unset($arEventFields['PASSWORD'], $arEventFields['CONFIRM_PASSWORD'], $aUser, $arLibrary);

                $obEvent = new CEvent;
                $obEvent->Send('NEW_USER', SITE_ID, $arEventFields);
                if (Option::get('main', 'new_user_registration_email_confirmation', 'N') == 'Y')
                    $obEvent->SendImmediate('NEW_USER_CONFIRM_WCHZ', SITE_ID, $arEventFields);
            }

            // редирект
            if (empty($arError))
                LocalRedirect($this->arParams['LIST_URL']);
        }

        $this->arResult['ERRORS'] = $arError;
    }
}