<div class="b-clause-title_img-slider b-clause-title_img b-clause-title_img__right js-clause-title_img-slider">
	<ul>
		<?
			foreach($arResult['ITEMS'] as $arItem)
			{
			?>
			<li>
				<img alt="" src="<?=$arItem['PICTURE']?>">
				<div class="b-clause-title_img__caption">
					<?=$arItem['PREVIEW_TEXT']?>
				</div>
			</li>
			<?
			}
		?>
	</ul>
</div>