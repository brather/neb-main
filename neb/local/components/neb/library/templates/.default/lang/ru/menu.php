<?
$MESS['LIBRARY_MENU_ABOUT']       = "О библиотеке";
$MESS['LIBRARY_MENU_FUNDS']       = "Фонды";
$MESS['LIBRARY_MENU_COLLECTIONS'] = "Подборки";
$MESS['LIBRARY_MENU_NEWS']        = "Новости";
$MESS['LIBRARY_MENU_POPULAR']     = "Популярные издания";
