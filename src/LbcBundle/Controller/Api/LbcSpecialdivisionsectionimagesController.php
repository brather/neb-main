<?php
/**
 * User: agolodkov
 * Date: 17.08.2016
 * Time: 17:20
 */

namespace LbcBundle\Controller\Api;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("app.api.specialdivisionsection.images")
 */
class LbcSpecialdivisionsectionimagesController extends AbstractImagesController
{
    /**
     * @return string
     */
    public function getEntityName()
    {
        return 'LbcBundle:LbcSpecialdivisionsectionimages';
    }

    /**
     * @return string
     */
    public function getOwnFieldName()
    {
        return 'specialdivisionsectionid';
    }
}