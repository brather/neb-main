<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>

<? if (count($arResult['SECTIONS']) > 0): ?>
    <div class="b-bookboard_main clearfix rel b-bookboard_main--special">
        <h2 class="b-mainslider_tit"><a href="/special/collections/" class="b-collectlink"
                                        title="коллекции">коллекции</a></h2>
        <div class="b-boardslider js_slider_single_nodots narrow_panel">
            <?
            foreach ($arResult['SECTIONS'] as $arSection) {
                if (empty($arResult['BOOKS']['IDS'][$arSection['ID']]))
                    continue;
                ?>
                <div>
                    <div class="js_flexbackground">
                        <img src="<?= $arSection['BACKGROUND'] ?>" alt="" data-bgposition="50% 0"
                             class="js_flex_bgimage"/>
                        <div class="wrapperboard bbox">
                            <?
                            if ($arSection['SHOW_NAME']) {
                                ?>
                                <h3><?= $arSection['NAME'] ?></h3>
                                <?
                            }
                            ?>
                        </div>
                    </div><!-- /.wrapper -->
                </div>
                <?
            }
            ?>
        </div>
    </div> <!-- /.b-mainslider -->
    <? //print_r($arResult); ?>
<? endif; ?>


