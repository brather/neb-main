<?php
namespace Nota\Exalead;

/**
 * Подсказки в поисковой строке по типу "Музеи"
 */
class SearchQueryMuseum extends SearchQuery {

    public function getSuggestTitle($query) {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title_museum';
        if(empty($query))
                return false;
        $this->setQuery($query);
        return $this;
    }

    public function getSuggestTitleAll($query) {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_title_museum_all';
        $this->setQuery($query);
        return $this;
    }
    
    public function getSuggestAuthor($query) {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author_museum';
        if(empty($query))
                return false;
        $this->setQuery($query);
        return $this;
    }

    public function getSuggestAuthorAll($query) {
        $this->queryType = 'suggest';
        $this->url = '/suggest/service/suggest_author_museum_all';
        $this->setQuery($query);
        return $this;
    }
}