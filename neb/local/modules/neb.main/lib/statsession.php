<?php
/**
 * User: agolodkov
 * Date: 05.02.2016
 * Time: 17:22
 */

namespace Neb\Main;


use Bitrix\Main\Entity\DataManager;

class StatSessionTable extends DataManager
{
    public static function getTableName()
    {
        return 'b_stat_session';
    }

    public static function getMap()
    {
        return array(
            'ID'              => array(
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
            ),
            'USER_ID'         => array(
                'data_type' => 'integer'
            ),
            'IP_FIRST'        => array(
                'data_type' => 'string'
            ),
            'IP_FIRST_NUMBER' => array(
                'data_type' => 'integer'
            ),
            'IP_LAST'         => array(
                'data_type' => 'string'
            ),
            'IP_LAST_NUMBER'  => array(
                'data_type' => 'integer'
            ),
            'DATE_LAST'       => array(
                'data_type' => 'date'
            ),
        );
    }

}