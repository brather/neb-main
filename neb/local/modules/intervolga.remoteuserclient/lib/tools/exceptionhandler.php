<?namespace Intervolga\RemoteUserClient\Tools;

use Bitrix\Main\Diag\ExceptionHandlerLog;

/**
 * Handles exceptions.
 *
 * @package Intervolga\RemoteUserClient\Tools
 */
class ExceptionHandler extends ExceptionHandlerLog
{
	/**
	 * @var ExceptionHandler Stored instance for Singleton-like use
	 */
	private static $instance = null;

	public function write(\Exception $obException, $sLogType)
	{
		\Bitrix\Main\Diag\Debug::dumpToFile("Exception at: " . date("Y-m-d H:i:s"), "", "intervolga.remoteuserclient.exceptions.txt");
		\Bitrix\Main\Diag\Debug::dumpToFile($obException->getFile(), "file", "intervolga.remoteuserclient.exceptions.txt");
		\Bitrix\Main\Diag\Debug::dumpToFile($obException->getLine(), "line", "intervolga.remoteuserclient.exceptions.txt");
		\Bitrix\Main\Diag\Debug::dumpToFile($obException->getCode(), "code", "intervolga.remoteuserclient.exceptions.txt");
		\Bitrix\Main\Diag\Debug::dumpToFile($obException->getMessage(), "message", "intervolga.remoteuserclient.exceptions.txt");
	}

	public function initialize(array $arOptions = array()) {}

	/**
	 * Returns stored instance of class.
	 *
	 * @param array $arOptions
	 *
	 * @return ExceptionHandler
	 */
	public static function getInstance(array $arOptions = array())
	{
		if (self::$instance == null)
		{
			self::$instance = new self();
		}
		return self::$instance;
	}
}