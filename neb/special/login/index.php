<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Вход");
?>

<section class="innersection innerwrapper clearfix">
    <div class="b-registration rel">

        <h2 class="mode">Вход</h2>
        <div class="b-form b-form_common b-regform">
            <hr/>
            <div class="fieldrow nowrap radiolist">
                <div class="fieldcell iblock mt10">
                    <div class="field validate">
                        <div class="b-radio">
                            <a href="/special/login/user/" title="Вход пользователя портала">Вход пользователя портала</a>
                        </div>
                        <div class="b-radio">
                            <a href="/special/login/social/" title="Вход через социальные сети">Вход через социальные сети</a>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
        </div>
    </div><!-- /.b-registration-->



</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
