<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$rsGroups = CGroup::GetList ($by = "c_sort", $order = "asc", Array ("ACTIVE" => "Y"));
$arGroupId = array();
while($obGroup = $rsGroups->Fetch()) {
    $arGroupId[] = $obGroup["ID"];
}

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "HL_IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("LIB_PLAN_ADD_HIGHLOAD_ID"),
            "TYPE" => "LIST",
            "DEFAULT" => "1",
            "REFRESH" => "Y",
        ),
        "COUNT_OF_PLANS" => Array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("LIB_PLAN_ADD_COUNT_OF_PLANS"),
            "TYPE" => "STRING",
            "DEFAULT" => "20",
        ),
        "SEF_FOLDER" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("LIB_PLAN_ADD_SEF_FOLDER"),
            "TYPE" => "TEXT",
            "DEFAULT" => "",
        ),
        "USER_GROUPS" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("LIB_PLAN_ADD_USER_GROUPS"),
            "TYPE" => "LIST",
            "VALUES" => $arGroupId,
            "DEFAULT" => '={$arRequest["ID"]}'
        ),
    ),
);