<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/modules/nota.collection/classes/general/collection.php");

CModule::AddAutoloadClasses(
    'nota.collection',
    array(

        'Nota\Collection\LibraryTable' => 'lib/library.php',
        '\Nota\Collection\LibraryTable' => 'lib/library.php',

        'Nota\Collection\CollectionTable' => 'lib/collection.php',
        '\Nota\Collection\CollectionTable' => 'lib/collection.php',

        'Nota\Collection\ElementsTable' => 'lib/elements.php',
        '\Nota\Collection\ElementsTable' => 'lib/elements.php',
    )
);


use Bitrix\Main;
use Bitrix\Main\Entity;

$eventManager = Main\EventManager::getInstance();
$eventManager->addEventHandler("nota.collection", "ElementsOnUpdate", "CollectionElementsClearCache");
$eventManager->addEventHandler("nota.collection", "ElementsOnAdd", "CollectionElementsClearCache");
$eventManager->addEventHandler("nota.collection", "ElementsOnDelete", "CollectionElementsClearCache");

$eventManager->addEventHandler("nota.collection", "CollectionOnUpdate", "CollectionElementsClearCache");
$eventManager->addEventHandler("nota.collection", "CollectionOnAdd", "CollectionElementsClearCache");
$eventManager->addEventHandler("nota.collection", "CollectionOnDelete", "CollectionElementsClearCache");
$eventManager->addEventHandler("nota.collection", "CollectionOnBeforeDelete", "CollectionDelete");

function CollectionElementsClearCache(Entity\Event $event)
{
    global $CACHE_MANAGER;
    $CACHE_MANAGER->ClearByTag("collection_main_slider");
}

function CollectionDelete(Entity\Event $event)
{
    $primary = $event->getParameter("id");

    if ($primary['ID'] > 0) {
        $result = Nota\Collection\CollectionTable::getById($primary['ID']);
        if ($arRow = $result->fetch()) {
            if ((int)$arRow['BACKGROUND'] > 0)
                CFile::Delete((int)$arRow['BACKGROUND']);

            $rsData = Nota\Collection\ElementsTable::getList(array('select' => array('ID'), 'filter' => array('COLLECTION_ID' => $primary['ID'])));
            while ($arRes = $rsData->fetch()) {
                Nota\Collection\ElementsTable::delete($arRes['ID']);
            }
        }

    }

}
