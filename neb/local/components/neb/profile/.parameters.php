<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "VARIABLE_ALIASES" => array(),
        "SEF_MODE" => array(
            "main" => array(
                "NAME" => "MAIN PAGE",
                "DEFAULT" => "/",
                "VARIABLES" => array()
            ),
            "profile_edit" => array(
                "NAME" => "PROFILE PAGE",
                "DEFAULT" => "edit/",
                "VARIABLES" => array()
            ),
            "statistics" => array(
                "NAME" => "statistics PAGE",
                "DEFAULT" => "statistics/",
                "VARIABLES" => array()
            ),
            /* Библиотека				*/
            "funds_manage" => array(
                "NAME" => "FUNDS MANAGE PAGE",
                "DEFAULT" => "funds/manage/",
                "VARIABLES" => array()
            ),
            "readers" => array(
                "NAME" => "READERS PAGE",
                "DEFAULT" => "readers/",
                "VARIABLES" => array()
            ),
            "readers_new" => array(
                "NAME" => "READERS NEW PAGE",
                "DEFAULT" => "readers/new/",
                "VARIABLES" => array()
            ),
            "collection" => array(
                "NAME" => "COLLECTION PAGE",
                "DEFAULT" => "collection/",
                "VARIABLES" => array()
            ),
            "collection_edit" => array(
                "NAME" => "COLLECTION EDIT PAGE",
                "DEFAULT" => "collection/edit/#ID#/",
                "VARIABLES" => array('ID')
            ),
            "news" => array(
                "NAME" => "NEWS PAGE",
                "DEFAULT" => "news/",
                "VARIABLES" => array()
            ),
            "news_add" => array(
                "NAME" => "NEWS ADD PAGE",
                "DEFAULT" => "news/add/",
                "VARIABLES" => array()
            ),
            "news_edit" => array(
                "NAME" => "NEWS EDIT PAGE",
                "DEFAULT" => "news/edit/#ID#/",
                "VARIABLES" => array('ID')
            ),
            "full_access" => array(
                "NAME" => "FULL_ACCESS PAGE",
                "DEFAULT" => "full_access/",
                "VARIABLES" => array()
            ),
            "full_access_2" => array(
                "NAME" => "FULL_ACCESS PAGE 2",
                "DEFAULT" => "full_access_2/",
                "VARIABLES" => array()
            ),
            "full_access_3" => array(
                "NAME" => "FULL_ACCESS PAGE 3",
                "DEFAULT" => "full_access_3/",
                "VARIABLES" => array()
            ),
            "full_access_4" => array(
                "NAME" => "FULL_ACCESS PAGE 4",
                "DEFAULT" => "full_access_4/",
                "VARIABLES" => array()
            ),
            "check_eechb" => array(
                "NAME" => "Check EECHB PAGE",
                "DEFAULT" => "check_eechb/",
                "VARIABLES" => array()
            ),
            "plan_digitization" => array(
                "NAME" => "Plan digitization PAGE",
                "DEFAULT" => "plan_digitization/",
                "VARIABLES" => array()
            ),
            "plan_digitization_add" => array(
                "NAME" => "Plan digitization add PAGE",
                "DEFAULT" => "plan_digitization/add/",
                "VARIABLES" => array()
            ),

            /* Читатель				*/
            "searches" => array(
                "NAME" => "Searches PAGE",
                "DEFAULT" => "searches/",
                "VARIABLES" => array()
            ),
            "searches_collection" => array(
                "NAME" => "Searches collection PAGE",
                "DEFAULT" => "searches/#COLLECTION_ID#/",
                "VARIABLES" => array()
            ),

            "my_library" => array(
                "NAME" => "My library PAGE",
                "DEFAULT" => "my-library/",
                "VARIABLES" => array()
            ),
            "my_library_collection" => array(
                "NAME" => "My library collection PAGE",
                "DEFAULT" => "my-library/#COLLECTION_ID#/",
                "VARIABLES" => array()
            ),
            "my_library_reading" => array(
                "NAME" => "My library reading PAGE",
                "DEFAULT" => "my-library/reading/",
                "VARIABLES" => array()
            ),
            "my_library_read" => array(
                "NAME" => "My library read PAGE",
                "DEFAULT" => "my-library/read/",
                "VARIABLES" => array()
            ),

            "quote" => array(
                "NAME" => "quote PAGE",
                "DEFAULT" => "quote/",
                "VARIABLES" => array()
            ),
            "quote_collection" => array(
                "NAME" => "quote collection PAGE",
                "DEFAULT" => "quote/#COLLECTION_ID#/",
                "VARIABLES" => array()
            ),

            "bookmark" => array(
                "NAME" => "bookmark PAGE",
                "DEFAULT" => "bookmark/",
                "VARIABLES" => array()
            ),
            "bookmark_collection" => array(
                "NAME" => "bookmark collection PAGE",
                "DEFAULT" => "bookmark/#COLLECTION_ID#/",
                "VARIABLES" => array()
            ),
            "note" => array(
                "NAME" => "note PAGE",
                "DEFAULT" => "note/",
                "VARIABLES" => array()
            ),
            "note_collection" => array(
                "NAME" => "note collection PAGE",
                "DEFAULT" => "note/#COLLECTION_ID#/",
                "VARIABLES" => array()
            ),

            "scanning" => array(
                "NAME" => "scanning PAGE",
                "DEFAULT" => "scanning/",
                "VARIABLES" => array()
            ),

            /* Правообладатель */
            "work_fonds" => array(
                "NAME" => "work-fonds PAGE",
                "DEFAULT" => "work-fonds/",
                "VARIABLES" => array()
            ),
        ),
    ),
);
