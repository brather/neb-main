<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 10.05.2017
 * Time: 16:44
 */

namespace Neb\Main\Helper;

use \Bitrix\Main\Config\Option,
    \Bitrix\Main\Web\HttpClient;

class LangHelper
{
    /**
     * Получает адрес API Яндекс.Переводчика
     * @return string
     */
    public static function getYandexPath() {
        return 'https://translate.yandex.net/api/v1.5/tr.json/translate';
    }

    /**
     * Получает ключ из настроек главного модуля для Яндекс.Переводчика
     * @return string
     */
    public static function getYandexKey() {
        //'trnsl.1.1.20130927T150719Z.e1b94f416d288730.37f893076a3a0ffa51bff5edc35f4167393ecbf6'; // был зашит в коде
        return Option::get('main', 'translate_key_yandex', '');
    }

    /**
     * Яндекс.Перевод текста
     * https://tech.yandex.ru/translate/doc/dg/concepts/About-docpage/
     *
     * @param $sText
     * @param string $sLang
     * @param string $sFormat
     * @return string
     */
    public static function yandexTranslateText($sText, $sLang = 'ru-en', $sFormat = 'plain') {

        if (empty($sText)) {
            return $sText;
        }

        $sPath = self::getYandexPath();
        $arQuery['key']    = self::getYandexKey();
        $arQuery['text']   = $sText;
        $arQuery['lang']   = $sLang;
        $arQuery['format'] = $sFormat;

        $http = new HttpClient();
        if (!MainHelper::checkSslVerification($sPath)) {
            $http->disableSslVerification();
        }
        $http->setTimeout(10);
        $arResponse = $http->post($sPath, $arQuery);
        $arResponse = json_decode($arResponse, true);

        if ($arResponse['code'] == '200' && !empty($arResponse['text'][0])) {
            $sText = urldecode($arResponse['text'][0]);
        }

        return $sText;
    }
}