<?php
CBitrixComponent::includeComponentClass('rest:rest.server');
CModule::IncludeModule('iblock');

/**
 * Class RestLibraryNebParty
 */
class RestRightholderRequestViewPrice extends RestServer
{
    /**
     * @var array
     */
    protected $_params
        = array(
            'book_id' => null,
        );

    protected $_paramsMap
        = array(
            'book_id' => array('required' => true),
        );

    protected $_methodAllowGroups
        = array(
            'postAction' => array(
                9 => true,
            ),
        );

    /**
     * @return $this
     */
    protected function _prepareParams()
    {
        $this->_params['confirm'] = filter_var(
            $this->_params['confirm'],
            FILTER_VALIDATE_BOOLEAN
        );
        $this->_params['request_id'] = (integer)$this->_params['request_id'];

        return $this;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function postAction()
    {
        NebMainHelper::includeModule('nota.exalead');
        $bookRow = \Nota\Exalead\BiblioCardTable::getRow(
            array(
                'filter' => array(
                    'Id' => $this->_params['book_id'],
                )
            )
        );
        if (null === $bookRow) {
            throw new Exception('Book not found', 404);
        }
        $eventFields = array(
            'FULL_NAME' => $this->_user['NAME'] . ' '
                . $this->_user['LAST_NAME'],
            'EMAIL'     => $this->_user['EMAIL'],
            'PHONE'     => $this->_user['PERSONAL_PHONE'],
            'BOOK_URL'  => URL_SCHEME . '://'
                . SITE_HOST . '/catalog/'
                . $bookRow['FullSymbolicId'] . '/',
            'BOOK_NAME' => $bookRow['Name'],
        );
        $event = new CEvent();
        if($event->Send(
            'RH_VIEW_PRICE_REQUEST',
            's1',
            $eventFields,
            'Y',
            RIGHTHOLDER_RH_VIEW_PRICE_REQUEST
        )) {
            $this->arResult['success'] = true;
        } else {
            throw new Exception('Cannot sent request');
        }

        return 200;
    }
}