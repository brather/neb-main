<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @todo remove fucking $APPLICATION->RestartBuffer() */
if (isset($arResult['parts']['ITEMS'])
    && !empty($arResult['parts']['ITEMS'])
) { ?>
    <dt class="additional-expandable__--expanded"><span>Содержание</span></dt>
    <dd>
        <div class="edition-parts-container">
            <? if ('Y' === $_REQUEST['Ajax_instances']
                && 'parts' === $_REQUEST['listType']
            ) {
                $APPLICATION->RestartBuffer();
            } ?>
            <table class="neb-table">
                <tr>
                    <th>Название</th>
                    <th colspan="2">Страница</th>
                </tr>
                <? foreach (
                    $arResult['parts']['ITEMS'] as $bookPart
                ) { ?>
                    <tr>
                        <td>
                            <a href="<?= $bookPart['DETAIL_PAGE_URL'] ?>">
                                <?= $bookPart['title'] ?>
                            </a>
                        </td>
                        <td><?= $bookPart['startpageparts'] ?></td>
                        <td>
                            <? if ($bookPart['isprotected'] > 0
                                && !$arResult['allowReadClosedBooks']
                            ) { ?>
                                <a
                                    class="btn btn-primary btn-lg pull-right clearfix search-result__content-main-read-btn--close popover200"
                                    data-hover-toggle="popover"
                                    data-placement="left"
                                    data-content="Этот объект охраняется авторским правом"
                                    >Читать</a>
                            <? } else { ?>
                                <a
                                    class="btn btn-primary btn-lg pull-right boockard-read-button clearfix<?php if (!$arResult['isInWorkplace']
                                        && ($arResult['nebUser']['UF_STATUS']
                                            !== nebUser::USER_STATUS_VIP)
                                    ): ?> boockard-read-button--disabled <?php endif; ?>"
                                    href="javascript:void(0);"
                                    data-link="<?= $arParams['BOOK_ID'] ?>"
                                    data-options="<?php echo htmlspecialchars(
                                        json_encode(
                                            array(
                                                'title'  => $bookPart['title'],
                                                'author' => $bookPart['authorbook'],
                                                'page'   => $bookPart['startpageparts'],
                                            )
                                        ), ENT_QUOTES, 'UTF-8'
                                    ) ?>">Читать</a>
                            <? } ?>
                        </td>
                    </tr>
                <? } ?>
            </table>

            <?= $arResult['navStr'] ?>
            <? if ('Y' === $_REQUEST['Ajax_instances']
                && 'parts' === $_REQUEST['listType']
            ) {
                die();
            } ?>
        </div>
    </dd>
    <script type="text/javascript">
        $(function () {

             /*function parseUrlQuery(loc) {
                 var data = {};
                 if(loc.search) {
                     var pair = (loc.search.substr(1)).split('&');
                     for(var i = 0; i < pair.length; i ++) {
                         var param = pair[i].split('=');
                         data[param[0]] = param[1];
                     }
                 }
                 return data;
             }*/

            $(document).on('click', '.edition-parts-container ul.pagination a', function (e) {
                e.preventDefault();
                var linkURL = $(this)[0],
                    parsedSearch = FRONT.assets.parseURLGetQuery(linkURL),
                    fakeURL = linkURL.pathname + '?listType=' + parsedSearch["listType"] + '&PAGEN_1=' + (parsedSearch["PAGEN_1"] !== undefined ? parsedSearch["PAGEN_1"] : 1),
                    href = $(this).attr('href'),
                    container = $('.edition-parts-container'),
                    containerHeight = $(container).height();
                    
                if (href == "#") {return};

                $.ajax({
                    url: href,
                    data: {
                        Ajax_instances: "Y",
                        listType: 'parts'
                    },
                    beforeSend: function(){
                        container
                            .css({'position':'relative', 'height': containerHeight + 'px' })                
                            .append('<div class="milk-shadow draw"/>');
                        FRONT.spinner.execute( $(container)[0] );
                    },
                    success: function(data){
                        container.removeAttr('style');
                        $(container).html(data);
                        if(history.pushState) {
                            history.pushState(null, null, fakeURL);
                        }
                    }
                });
            });
        });
    </script>
<? } ?>