<?php

define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

$documentRoot = realpath(dirname(__FILE__) . '/../neb/');
$_SERVER['DOCUMENT_ROOT'] = $documentRoot;
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');
while (ob_get_level()) {
    ob_end_clean();
}
/**
 * @var CDatabase $DB
 */

$timeStart = microtime(true);
$affectedRows = 0;
$query
    = "
SELECT
nl.ID,
nl.UF_NAME,
nl.UF_ADRESS
FROM neb_libs nl;
";
$result = $DB->Query($query, true);
$url = 'https://geocode-maps.yandex.ru/1.x/?geocode=';
$getProp = function ($object, $path) {
    $path = explode('.', $path);
    foreach ($path as $item) {
        if (property_exists($object, $item)) {
            $object = $object->$item;
        } else {
            return null;
        }
    }

    return $object;
};
$i = 0;
$batch = 0;
$batchCount = 500;
$updateQuery = "
UPDATE neb_libs
SET UF_REGION = '%s'
WHERE ID = %d;
";
$getXml = function ($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPGET, true);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
};
while ($item = $result->Fetch()) {
    $batch++;
    if($batch > $batchCount) {
        $batch = 0;
//        sleep(3600);
    }
    $xml = $getXml($url . urlencode($item['UF_ADRESS']));
    $xml = new SimpleXMLElement($xml);
    $region = $getProp(
        $xml,
        'GeoObjectCollection'
        . '.featureMember'
        . '.GeoObject'
        . '.metaDataProperty'
        . '.GeocoderMetaData'
        . '.AddressDetails'
        . '.Country'
        . '.AdministrativeArea'
        . '.AdministrativeAreaName'
    );
    if ((integer)$item['ID'] > 0) {
        $DB->Query(
            sprintf($updateQuery, $region, (integer)$item['ID']), true
        );
    }
    $i++;
    echo $i, "\r";
}
echo PHP_EOL;
echo 'Time: ', microtime(true) - $timeStart, ' seconds', PHP_EOL;
echo 'Memory: ', memory_get_peak_usage() / (1024 * 1024), ' Mb', PHP_EOL;
echo PHP_EOL;