<?php
/**
 * Построено на ЯС-библиотеке Plupload
 *
 * @see http://plupload.com/
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
if($_REQUEST['mode'] == 'settings')
    return false;

#\CUtil::InitJSCore(array("jquery"));
$APPLICATION->AddHeadString('<script>window.jQuery || document.write(\'<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js">\x3C/script>\')</script>',true);
$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);
?>
<div id="plupload_block_<?=$arParams['RAND_STR']?>">
	<?
		if(!empty($arParams['OLD_FILES']))
		{
		?>
		<ul class="pl_filelist">
			<?
				if($arParams['MULTI_SELECTION'] == 'N' and !empty($arParams['OLD_FILES']['VALUE']))
				{
				?>
				<li>
					<table>
						<tr>
							<td>
								<?=CFileInput::Show(
									'_'.$k,
									$arParams['OLD_FILES']['VALUE'],
									array(
										'IMAGE' => 'Y',
										'PATH' => 'Y',
										'FILE_SIZE' => 'Y',
										'DIMENSIONS' => 'Y',
										'IMAGE_POPUP' => 'Y',
										'MAX_SIZE' => array(
											'W' => COption::GetOptionString('iblock', 'detail_image_size'),
											'H' => COption::GetOptionString('iblock', 'detail_image_size'),
										),
									),
									array(
										'upload' => false,
										'medialib' => false,
										'file_dialog' => false,
										'cloud' => false,
										'del' => false,
										'description' => false,
									)
								);
								?>
							</td>
							<td valign="top">

								<?
									if(!empty($arParams['FILES_FIELD_DESCRIPTION']))
									{
									?>
									<input type="text" size="<?=$arParams['FIELD_DESCRIPTION_LEN']?>" name="<?=$arParams['FILES_FIELD_DESCRIPTION']?>" value="<?=htmlspecialcharsbx($arParams['OLD_FILES']['DESCRIPTION'])?>"/><br /><br />
									<?
									}
								?>

								<input type="hidden" name="<?=$arParams['FILES_FIELD_NAME']?>" value="<?=$arParams['OLD_FILES']['VALUE']?>"/>
								<input type="hidden" name="<?=str_replace('[VALUE]', '[old_file]',$arParams['FILES_FIELD_NAME'])?>" value="<?=$arParams['OLD_FILES']['VALUE']?>"/>
								<label><input type="checkbox" name="<?=str_replace('[VALUE]', '[del]',$arParams['FILES_FIELD_NAME'])?>" value="Y"/> Удалить</label>
							</td>
						</tr>
					</table>
				</li>
				<?
				}elseif($arParams['MULTI_SELECTION'] == 'Y'){
					foreach($arParams['OLD_FILES'] as $k => $v){
						if(empty($v['VALUE']))
							continue;

					?>
					<li>
						<table>
							<tr>
								<td>
									<?=CFileInput::Show(
										'_'.$k,
										$v['VALUE'],
										array(
											'IMAGE' => 'Y',
											'PATH' => 'Y',
											'FILE_SIZE' => 'Y',
											'DIMENSIONS' => 'Y',
											'IMAGE_POPUP' => 'Y',
											'MAX_SIZE' => array(
												'W' => COption::GetOptionString('iblock', 'detail_image_size'),
												'H' => COption::GetOptionString('iblock', 'detail_image_size'),
											),
										),
										array(
											'upload' => false,
											'medialib' => false,
											'file_dialog' => false,
											'cloud' => false,
											'del' => false,
											'description' => false,
										)
									);
									?>
								</td>
								<td valign="top">
									<?
										if(!empty($arParams['FILES_FIELD_DESCRIPTION']))
										{
										?>
										<input type="text" size="<?=$arParams['FIELD_DESCRIPTION_LEN']?>" name="<?=$arParams['FILES_FIELD_DESCRIPTION']?>[<?=$k?>][DESCRIPTION]" value="<?=htmlspecialcharsbx($v['DESCRIPTION'])?>"/><br /><br />
										<?
										}
									?>

									<input type="hidden" name="<?=$arParams['FILES_FIELD_NAME']?>[<?=$k?>][VALUE]" value="<?=$v['VALUE']?>"/>
									<input type="hidden" name="<?=$arParams['FILES_FIELD_NAME']?>[<?=$k?>][old_file]" value="<?=$v['VALUE']?>"/>
									<label><input type="checkbox" name="<?=$arParams['FILES_FIELD_NAME']?>[<?=$k?>][del]" value="Y"/> Удалить</label>
								</td>
							</tr>
						</table>
					</li>
					<?
					}
				}
			?>
		</ul>
		<?
		}
	?>

	<div id="container_<?=$arParams['RAND_STR']?>" class="pl_button">
		<input type="button" id="plupload_pickfiles_<?=$arParams['RAND_STR']?>" value="<?=$arParams['MULTI_SELECTION'] == 'Y' ? GetMessage('SELECT_FILES') : GetMessage('SELECT_FILE')?>"/>&nbsp;
		<input type="button" id="plupload_uploadfiles_<?=$arParams['RAND_STR']?>" value="<?=GetMessage('UPLOAD')?>"/><br />
	</div>

	<div id="plupload_filelist_<?=$arParams['RAND_STR']?>"><?=GetMessage('BROWSER_NOT_SUPPORTED')?></div>
	<pre id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>
</div>

<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',

				browse_button : 'plupload_pickfiles_<?=$arParams['RAND_STR']?>',
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'),

				drop_element: "plupload_block_<?=$arParams['RAND_STR']?>",

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},

				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '1mb',

				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				<?
					if(!empty($arParams['IMG_WIDTH']) and !empty($arParams['IMG_HEIGHT']))
					{
					?>
					resize: {
						width: <?=$arParams['IMG_WIDTH']?>,
						height: <?=$arParams['IMG_HEIGHT']?>,
						<?
							if($arParams['IMG_CROP'] == 'Y'){
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=$arParams['IMG_QUALITY']?>
					},
					<?
					}
				?>

				multipart_params: 
				{
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'
				},
				multi_selection: <?=$arParams['MULTI_SELECTION'] == 'Y' ? 'true' : 'false'?>,
				init: 
				{
					PostInit: function(up) 
					{
						document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').innerHTML = '';
						document.getElementById('plupload_uploadfiles_<?=$arParams['RAND_STR']?>').onclick = function() 
						{
							up.start();
							return false;
						}
					},
					FilesAdded: function(up, files) {
						<?
							if($arParams['MULTI_SELECTION'] != 'Y'){
							?>
							document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').innerHTML = '';
							<?
							}
						?>
						plupload.each(files, function(file) {
							document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
						});
					},
					UploadProgress: function(up, file) {
						<?
							if($arParams['MULTI_SELECTION'] != 'Y'){
							?>
							document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
							<?
							} else {
							?>
							document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
							<?
							}
						?>
					},
					FileUploaded: function(up, file, response) {
						var result = response.response;
						if (result) {
							var obResponse = JSON.parse(result);
							<?
								if($arParams['MULTI_SELECTION'] == 'Y'){
									$arParams['FILES_FIELD_NAME'] = $arParams['FILES_FIELD_NAME']."[][VALUE]";	
								}

							?>
							document.getElementById('plupload_filelist_<?=$arParams['RAND_STR']?>').innerHTML += '<input type="hidden" name="<?=$arParams['FILES_FIELD_NAME']?>" value="' + obResponse.file + '">';
						}
					},
					Error: function(up, err) {
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
					}
				}
		});

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>
