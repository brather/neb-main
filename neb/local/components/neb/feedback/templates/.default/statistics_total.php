<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->SetTitle("Статистика по количеству поступивших, отработанных и находящихся в работе заявок");

/**
 * Вывод заявок
 */
?>
<p>
	<span data-href='<?=$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['statistics_total']?>'>Статистика по количеству</span>
	|
	<a href='<?=$arParams['SEF_FOLDER'].$arParams['SEF_URL_TEMPLATES']['statistics_time']?>'>Статистика по времени исполнения</a>
</p>

<?php

$APPLICATION->IncludeComponent(
    "neb:feedback.statistics",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        'FEEDBACK_STAT_URL'  => $arParams['SEF_FOLDER'] . $arParams['SEF_URL_TEMPLATES']['statistics_total']
    ),
    $component
);
