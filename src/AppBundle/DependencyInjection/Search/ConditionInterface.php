<?php
/**
 * User: agolodkov
 * Date: 21.10.2016
 * Time: 16:52
 */

namespace AppBundle\DependencyInjection\Search;


interface ConditionInterface
{
    /**
     * @return string
     */
    public function buildCondition();

    public function andCondition($condition);

    public function orCondition($condition);

    /**
     * @param string             $logic
     * @param ConditionInterface $condition
     *
     * @throws SearchException
     */
    public function addCondition($logic, $condition);
}