<?php

namespace LbcBundle\DependencyInjection;

use BitrixBundle\DependencyInjection\BitrixHelper;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Gedmo\Tree\Entity\Repository\ClosureTreeRepository;
use LbcBundle\Common\ArrayCollection;
use Nota\Exalead\SearchClient;
use Nota\Exalead\SearchQuery;
use Symfony\Component\HttpFoundation\Request;

/**
 * User: agolodkov
 * Date: 01.09.2016
 * Time: 16:31
 */
class ExaleadSearchHelper
{
    /**
     * @var BitrixHelper
     */
    private $_bitrix;

    /**
     * @var Request
     */
    private $_request;

    /**
     * @var Registry
     */
    private $_doctrine;

    /**
     * @var array
     */
    private $_searchResult;

    /**
     * @var int
     */
    private $_pageSize;

    /**
     * @var string
     */
    private $_pageParameterName = 'page';

    /**
     * @param BitrixHelper $bitrix
     * @param Request      $request
     * @param Registry     $doctrine
     * @param int          $pageSize
     */
    public function __construct(
        BitrixHelper $bitrix,
        Request $request,
        Registry $doctrine,
        $pageSize = 15
    ) {
        $this->_bitrix = $bitrix;
        $this->_request = $request;
        $this->_doctrine = $doctrine;
        $this->_pageSize = $pageSize;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function search()
    {
        $this->_bitrix->requireProlog();
        $this->_bitrix->includeModule('nota.exalead');
        $client = new SearchClient();
        $this->_searchResult = $client->getResult($this->_buildQuery());

        return $this;
    }

    /**
     * @return array
     */
    public function getEntities()
    {
        $entities = [];
        if (!empty($this->_searchResult)) {
            /** @var ClosureTreeRepository[] $repositories */
            $repositories = [
                'm' => $this->_doctrine->getManager()->getRepository('LbcBundle:LbcMaindivision'),
                'l' => $this->_doctrine->getManager()->getRepository('LbcBundle:LbcLocationplansections'),
                's' => $this->_doctrine->getManager()->getRepository('LbcBundle:LbcSpecialdivisionsections'),
            ];
            foreach ($this->_searchResult['hits'] as $hit) {
                $item = [];
                foreach ($hit['metas'] as $meta) {
                    $item[$meta['name']] = $meta['value'];
                }
                $item['bbkfull_id'] = substr($item['bbkfull_id'], 1);
                $entities[] = $repositories[$item['bbkfull_type']]->find($item['bbkfull_id']);
            }
        }

        return $entities;
    }

    /**
     * @return ArrayCollection
     */
    public function getCollection()
    {
        $collection = new ArrayCollection($this->getEntities());
        $collection->setCount($this->_searchResult['nhits']);

        return $collection;
    }

    /**
     * @return SearchQuery
     */
    private function _buildQuery()
    {
        $query = $this->_request->query;
        $searchQuery = new SearchQuery();
        $searchQuery->setQueryType('json');
        $searchQuery->setParam('of', 'json');
        $searchQuery->setParam('sl', 'sl_bbkfull');
        $searchQuery->setParam('st', 'st_bbkfull');
        $searchQuery->setParam('hf', $this->getPageSize());
        $page = $query->get($this->getPageParameterName());
        if ($page > 1) {
            $searchQuery->setParam('b', (
                    (
                        $page * $searchQuery->getParameter('hf')
                    )
                - $searchQuery->getParameter('hf')
                )
            );
        }
        $querySimpleParamPatterns = [
            'query'           => 'bbksimple:(%s)',
            'code'            => 'bbkfull_code:"%s"',
            'maindivision'    => 'bbkfull_name:"%s" AND bbkfull_type:m',
            'locationplan'    => 'bbkfull_name:"%s" AND bbkfull_type:l',
            'specialdivision' => 'bbkfull_name:"%s" AND bbkfull_type:s',
            'bbkhidden'       => 'bbkfull_errors:%s',
        ];
        $queryParams = [
            '#all'
        ];
        foreach ($querySimpleParamPatterns as $paramName => $pattern) {
            if ($value = $query->get($paramName)) {
                /* В запросе могут прийти символы [ ], следует заменить их на пробелы */
                if ($paramName == 'query' && preg_match("#[\[\]]#", $value) === 1)
                    $value = preg_replace("#[\[\]]#", " ", $value);
                $queryParams[] = sprintf($pattern, $value);
            }
        }
        if($description = $query->get('description')) {
            $queryParams[] = sprintf('(bbkfull_keywords:%s OR bbkfull_comments:%s)', $description, $description);
        }
        $searchQuery->setParam('q', implode(' AND ', $queryParams));

        return $searchQuery;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->_pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->_pageSize = $pageSize;
    }

    /**
     * @return string
     */
    public function getPageParameterName()
    {
        return $this->_pageParameterName;
    }

    /**
     * @param string $pageParameterName
     */
    public function setPageParameterName($pageParameterName)
    {
        $this->_pageParameterName = $pageParameterName;
    }

}