<?php
/**
 * Параметры компонента
 * 
 * @author Ефремов Д.В.
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arServices = [
	'collections' => 'Яндекс.Коллекции',
	'vkontakte' => 'Вконтакте',
	'facebook' => 'Facebook',
	'odnoklassniki' => 'Одноклассники',
	'moimir' => 'МойМир',
	'gplus' => 'Google+',
	'twitter' => 'Twitter',
	'blogger' => 'Blogger',
	'delicious' => 'Delicious',
	'digg' => 'Digg',
	'reddit' => 'Reddit',
	'evernote' => 'Evernote',
	'linkedin' => 'LinkedIn',
	'lj' => 'Livejournal',
	'pocket' => 'Pocket',
	'qzone' => 'Qzone',
	'renren' => 'Renren',
	'sinaWeibo' => 'Sina Weibo',
	'surfingbird' => 'Surfingbird',
	'tencentWeibo' => 'Tencent Weibo',
	'tumblr' => 'Tumblr',
	'viber' => 'Viber',
	'whatsapp' => 'WhatsApp',
	'skype' => 'Skype',
	'telegram' => 'Telegram',
];

$arLang = [
	'az' => 'азербайджанский',
	'be' => 'белорусский',
	'en' => 'английский',
	'hy' => 'армянский',
	'ka' => 'грузинский',
	'kk' => 'казахский',
	'ro' => 'румынский',
	'ru' => 'русский',
	'tr' => 'турецкий',
	'tt' => 'татарский',
	'uk' => 'украинский',
];


$arComponentParameters = Array(
	"GROUPS" => array(
		"DATA" => array(
			"NAME" => GetMessage("SHARE_GROUP_DATA"),
		),
		"THEME" => array(
			"NAME" => GetMessage("SHARE_GROUP_THEME"),
		),
	),

	"PARAMETERS" => Array(
		'DATA_URL' => array(
			"PARENT" => "DATA",
			"NAME" => GetMessage("SHARE_DATA_URL"),
			"TYPE" => "STRING",
			"DEFAULT" => ''
		),
		'DATA_TITLE' => array(
			"PARENT" => "DATA",
			"NAME" => GetMessage("SHARE_DATA_TITLE"),
			"TYPE" => "STRING",
			"DEFAULT" => ''
		),
		'DATA_DESCRIPTION' => array(
			"PARENT" => "DATA",
			"NAME" => GetMessage("SHARE_DATA_DESCRIPTION"),
			"TYPE" => "STRING",
			"DEFAULT" => ''
		),
		'DATA_IMAGE' => array(
			"PARENT" => "DATA",
			"NAME" => GetMessage("SHARE_DATA_IMAGE"),
			"TYPE" => "STRING",
			"DEFAULT" => ''
		),

		'THEME_SERVICES' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_SERVICES"),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $arServices,
			"DEFAULT" => ''
		),
		'THEME_BARE' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_BARE"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		'THEME_COPY' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_COPY"),
			"TYPE" => "LIST",
			"VALUES" => [
				'last' => 'кнопка вверху списка',
				'first' => 'кнопка внизу списка',
				'hidden' => 'кнопка не отображается',
			],
			"DEFAULT" => ''
		),
		'THEME_COUNTER' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_COUNTER"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		'THEME_DIRECTION' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_DIRECTION"),
			"TYPE" => "LIST",
			"VALUES" => [
				'horizontal' => 'горизонтальное',
				'vertical' => 'вертикальное',
			],
			"DEFAULT" => ''
		),
		'THEME_LANG' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_LANG"),
			"TYPE" => "LIST",
			"VALUES" => $arLang,
			"DEFAULT" => 'ru'
		),
		'THEME_LIMIT' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_LIMIT"),
			"TYPE" => "STRING",
			"DEFAULT" => ''
		),
		'THEME_POPUP_DIRECTION' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_POPUP_DIRECTION"),
			"TYPE" => "LIST",
			"VALUES" => [
				'bottom' => 'низ',
				'top'    => 'top',
			],
			"DEFAULT" => ''
		),
		'THEME_POPUP_POSITION' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_POPUP_POSITION"),
			"TYPE" => "LIST",
			"VALUES" => [
				'inner' => 'внутри контейнера',
				'outer' => 'снаружи контейнера',
			],
			"DEFAULT" => ''
		),
		'THEME_SIZE' => array(
			"PARENT" => "THEME",
			"NAME" => GetMessage("THEME_SIZE"),
			"TYPE" => "LIST",
			"VALUES" => [
				'm' => 'большой',
				's' => 'маленький',
			],
			"DEFAULT" => ''
		),
	)
);