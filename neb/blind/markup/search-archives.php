<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? $APPLICATION->SetTitle(""); ?>

<div class="container v-padding">
    <div class="button-bar" role="toolbar">
    	<a href="<?=$APPLICATION->GetCurDir()?>" class="btn btn-default">Вернуться на главную</a>
    </div>
    <form>
	    <dl class="form-dl">
	    	<dt><label class="for-lg" for="searchrub" aria-hidden="true" id="searchrublabel">Раздел для поиска</label></dt>
	    	<dd>
	    		<select class="form-control input-lg" aria-labelledby="searchrublabel" id="searchrub">
	    			<option>Библиотеки</option>
	    			<option>Музеи</option>
	    			<option selected>Архивы</option>
	    		</select>
	    	</dd>
	    	<dt><label class="for-lg" for="searchtype" aria-hidden="true" id="searchtypelabel">Вид поиска</label></dt>
	    	<dd>
	    		<select class="form-control input-lg" id="searchtype" aria-labelledby="searchtypelabel">
	    			<option>Одной строкой</option>
	    			<option selected>Расширенный поиск</option>
	    		</select>
	    	</dd>
	    	<dt><label for="title" class="for-lg" aria-hidden="true" id="titlelabel">Название</label></dt>
	    	<dd>
	    		<input type="text" class="form-control input-lg" id="title" tabindex="1" data-placeholder="Газета возрождение" aria-labelledby="titlelabel"/>
	    	</dd>
	    	<dt><label class="for-lg" aria-hidden="true">Дата публикации</label></dt>
	    	<dd>
	    		<label class="for-lg" for="pubfrom" aria-hidden="true" id="pubfromlabel">от</label><input type="text" class="form-control input-lg" id="pubfrom" style="display: inline-block; width: 5em;" value="900" aria-labelledby="pubfromlabel"/>
	    		<label class="for-lg" for="pubto" aria-hidden="true" id="pubtolabel">до</label><input type="text" class="form-control input-lg" id="pubto" style="display: inline-block; width: 5em;" value="2016" aria-labelledby="pubtolabel"/>
	    		<span class="instead-input-lg" style="margin-left: 1ex;" aria-hidden="true">года</span>
	    	</dd>
	    	<dt><label class="for-lg" aria-hidden="true" for="archivetitle" id="archivetitlelabel">Архив</label></dt>
	    	<dd>
	    		<input type="text" class="form-control input-lg" id="archivetitle" aria-labelledby="archivetitlelabel" data-placeholder="Государственный архив Российской Федерации (ГАРФ)"/>
	    	</dd>
	    	<dt>
	    		<label class="for-lg one-line-height" aria-hidden="true">
	    			Фонд<br/>
	    			<small>(
	    				<label id="foundnumberlabel" for="foundnumber">номер <span class="screen-hide"></span>фонда</label>, 
	    				<label for="foundtitle" id="foundtitlelabel">название <span class="screen-hide">фонда</span></label>
	    			)</small>
	    		</label>
	    	</dt>
	    	<dd>
	    		<div class="row">
	    			<div class="col-xs-3">
	    				<input type="text" class="form-control input-lg" data-placeholder="Р5850" aria-labelledby="foundnumberlabel" id="foundnumber" />
	    			</div>
	    			<div class="col-xs-9">
	    				<input type="text" class="form-control input-lg" data-placeholder="Редакция газеты Возрождение" id="foundtitle" aria-labelledby="foundtitlelabel"/>
	    			</div>
	    		</div>
	    	</dd>
	    	<dt>
	    		<label class="for-lg one-line-height" aria-hidden="true">
	    			Опись<br/>
	    			<small>(
	    				<label id="recordnumberlabel" for="recordnumber">номер <span class="screen-hide">описи</span></label>, 
	    				<label for="recordtitle" id="recordtitlelabel">название <span class="screen-hide">описи</span></label>
	    			)</small>
	    		</label>
	    	</dt>
	    	<dd>
	    		<div class="row">
	    			<div class="col-xs-3">
	    				<input type="text" class="form-control input-lg" data-placeholder="1" id="recordnumber" aria-labelledby="recordnumberlabel"/>
	    			</div>
	    			<div class="col-xs-9">
	    				<input type="text" class="form-control input-lg" data-placeholder="Опись дел личного происхождения" id="recordtitle" aria-labelledby="recordtitlelabel"/>
	    			</div>
	    		</div>
	    	</dd>
	    	<dt>
	    		<label class="for-lg one-line-height" aria-hidden="true">
	    			Дело<br/>
	    			<small>(
	    				<label id="filenumberlabel" for="filenumber">номер <span class="screen-hide">дела</span></label>, 
	    				<label for="filetitle" id="filetitlelabel" title="название дела">название <span class="screen-hide">дела</span></label>
	    			)</small>
	    		</label>
	    	</dt>
	    	<dd>
	    		<div class="row">
	    			<div class="col-xs-3">
	    				<input type="text" class="form-control input-lg" data-placeholder="336" id="filenumber" aria-labelledby="filenumberlabel"/>
	    			</div>
	    			<div class="col-xs-9">
	    				<input type="text" class="form-control input-lg" data-placeholder="Газета возрождение" id="filetitle" aria-labelledby="filetitlelabel"/>
	    			</div>
	    		</div>
	    	</dd>
	    </dl>
	    <div class="button-bar" role="toolbar">
	    	<button class="btn btn-primary" tabindex="2">Найти</button><br/>
	    	<button class="btn btn-default" type="reset">Очистить поиск</button>
	    </div>
	</form>
</div>
<script>
	$(function(){
		$('#title').focus();
	});
</script>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>