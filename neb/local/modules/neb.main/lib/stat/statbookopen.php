<?php
/**
 * User: agolodkov
 * Date: 21.09.2015
 * Time: 15:01
 */

namespace Neb\Main\Stat;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\Entity\EventResult;

class StatBookOpenTable extends DataManager
{
    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'neb_stat_book_open';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID'           => array(
                'data_type' => 'integer',
                'primary'   => true,
            ),
            'BOOK_ID'      => array(
                'data_type' => 'string',
            ),
            'REFERER'      => array(
                'data_type' => 'string',
            ),
            'SEARCH_QUERY' => array(
                'data_type' => 'string',
            ),
            'TIMESTAMP'    => array(
                'data_type' => 'datetime'
            ),
        );
    }

    /**
     * @param Event $event
     *
     * @return EventResult
     */
    public static function onBeforeAdd(Event $event)
    {
        $fields = $event->getParameter('fields');
        if (!isset($fields['SEARCH_QUERY'])
            && isset($fields['REFERER'])
            && !(
            $fields['SEARCH_QUERY']
                = static::extractSearchQuery($fields['REFERER'])
            )
        ) {
            unset($fields['SEARCH_QUERY']);
        }
        $result = new EventResult();
        $result->modifyFields($fields);

        return $result;
    }

    /**
     * @param $url
     *
     * @return null
     */
    public static function extractSearchQuery($url)
    {
        $searchQuery = null;
        $urlData = parse_url($url);
        if (isset($urlData['query'])
            && isset($urlData['path'])
            && 'search' === trim($urlData['path'], '/')
        ) {
            $query = array();
            parse_str($urlData['query'], $query);
            if (isset($query['q'])) {
                $searchQuery = $query['q'];
            }
        }

        return $searchQuery;
    }

}