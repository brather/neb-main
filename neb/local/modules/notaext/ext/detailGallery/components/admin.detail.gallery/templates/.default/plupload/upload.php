<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$IBLOCK_GALLERY_ID = intval($_REQUEST['IBLOCK_GALLERY_ID']);
if ($IBLOCK_GALLERY_ID <= 0)
    die('{"jsonrpc" : "2.0", "error" : {"code": 1, "message": "Empty parameter IBLOCK_GALLERY_ID."}, "id" : "id"}');

global $USER;
if (!$USER->IsAuthorized())
    die('{"jsonrpc" : "2.0", "error" : {"code": 2, "message": "Current user unautorized."}, "id" : "id"}');

CModule::IncludeModule("iblock");

$iblock_permission = CIBlock::GetPermission($IBLOCK_GALLERY_ID);
if ($iblock_permission < "W")
    die('{"jsonrpc" : "2.0", "error" : {"code": 3, "message": "Permission error."}, "id" : "id"}');

#print_r($_REQUEST);
// 5 minutes execution time
@set_time_limit(5 * 60);

// Uncomment this one to fake upload time
// usleep(5000);

// Settings
$targetDir = $_SERVER["DOCUMENT_ROOT"] . "/upload/tmp_plupload";

$cleanupTargetDir = true; // Remove old files
$maxFileAge = 5 * 3600; // Temp file age in seconds

// Create target dir
if (!file_exists($targetDir)) {
    @mkdir($targetDir);
}

// Get a file name
if (isset($_REQUEST["name"])) {
    $fileName = $_REQUEST["name"];
} elseif (!empty($_FILES)) {
    $fileName = $_FILES["file"]["name"];
} else {
    $fileName = uniqid("file_");
}

$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

// Chunking might be enabled
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


// Remove old temp files
if ($cleanupTargetDir) {
    if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
    }

    while (($file = readdir($dir)) !== false) {
        $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

        // If temp file is current file proceed to the next
        if ($tmpfilePath == "{$filePath}.part") {
            continue;
        }

        // Remove temp file if it is older than the max age and is not the current file
        if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
            @unlink($tmpfilePath);
        }
    }
    closedir($dir);
}


// Open temp file
if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
}

if (!empty($_FILES)) {
    if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
    }

    // Read binary input stream and append it to temp file
    if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
    }
} else {
    if (!$in = @fopen("php://input", "rb")) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
    }
}

while ($buff = fread($in, 4096)) {
    fwrite($out, $buff);
}

@fclose($out);
@fclose($in);

// Check if file has been uploaded
if (!$chunks || $chunk == $chunks - 1) {
    // Strip the temp .part suffix off
    rename("{$filePath}.part", $filePath);
    chmod($filePath, 0664);
    $intSectionGalleryIDValue = intval($_REQUEST['SectionGalleryIDValue']);
    $intIDMaterial = intval($_REQUEST['ID']);
    $intMAX_WIDTH_ORIGINAL = intval($_REQUEST['MAX_WIDTH_ORIGINAL']);
    $intMAX_HEIGHT_ORIGINAL = intval($_REQUEST['MAX_HEIGHT_ORIGINAL']);
    $intWIDTH = intval($_REQUEST['WIDTH']);
    $intHEIGHT = intval($_REQUEST['HEIGHT']);

    if ($intSectionGalleryIDValue > 0) {
        $res = CIBlockSection::GetByID($intSectionGalleryIDValue);
        if (!$arFields = $res->GetNext()) {
            $intSectionGalleryIDValue = 0;
        }
    }

    if ($intSectionGalleryIDValue <= 0) {
        $res = CIBlockElement::GetByID($intIDMaterial);
        if (!$ar_res = $res->GetNext()) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Not found material id =' . $intIDMaterial . '."}, "id" : "id"}');
        }

        $bs = new CIBlockSection;
        $arFields = Array(
            "ACTIVE" => 'Y',
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $IBLOCK_GALLERY_ID,
            "NAME" => $ar_res['NAME'],
            "EXTERNAL_ID" => $intIDMaterial
        );

        $intSectionGalleryIDValue = $bs->Add($arFields);

        if ($intSectionGalleryIDValue <= 0) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Error add section - ' . $bs->LAST_ERROR . '."}, "id" : "id"}');
        }
    }

    if ($intSectionGalleryIDValue > 0) {
        $el = new CIBlockElement;

        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => $intSectionGalleryIDValue,
            "IBLOCK_ID" => $IBLOCK_GALLERY_ID,
            "NAME" => $arFields['NAME'],
            "EXTERNAL_ID" => $intIDMaterial,
            "ACTIVE" => "Y",
        );

        $filePathInfo = pathinfo($filePath);

        /*
        Оригинал
        */
        $fileOriginalResizePatch = $filePathInfo['dirname'] . '/ro_' . $filePathInfo['basename'];
        if (!CFile::ResizeImageFile($filePath, $fileOriginalResizePatch, array('width' => $intMAX_WIDTH_ORIGINAL, 'height' => $intMAX_HEIGHT_ORIGINAL), BX_RESIZE_IMAGE_PROPORTIONAL)) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Error resize original photo - ' . $fileOriginalResizePatch . '."}, "id" : "id"}');
        }
        $arFileOriginal = CFile::MakeFileArray($fileOriginalResizePatch);
        $arFileOriginal['MODULE_ID'] = 'iblock';
        $arLoadProductArray['PROPERTY_VALUES'] = array('REAL_PICTURE' => $arFileOriginal);
        /*
        Детальная
        */
        $fileDetailResizePatch = $filePathInfo['dirname'] . '/detail_' . $filePathInfo['basename'];
        if (!CFile::ResizeImageFile($filePath, $fileDetailResizePatch, array('width' => $intWIDTH, 'height' => $intHEIGHT), BX_RESIZE_IMAGE_PROPORTIONAL)) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Error resize detail photo - ' . $fileDetailResizePatch . '."}, "id" : "id"}');
        }

        $arFileDetail = CFile::MakeFileArray($fileDetailResizePatch);
        $arFileDetail['MODULE_ID'] = 'iblock';
        $arLoadProductArray['DETAIL_PICTURE'] = $arFileDetail;
        /*
        Превью
        */
        $filePreviewResizePatch = $filePathInfo['dirname'] . '/preview_' . $filePathInfo['basename'];
        if (!CFile::ResizeImageFile($filePath, $filePreviewResizePatch, array('width' => 200, 'height' => 200), BX_RESIZE_IMAGE_PROPORTIONAL)) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Error resize detail photo - ' . $fileDetailResizePatch . '."}, "id" : "id"}');
        }

        $arFilePreview = CFile::MakeFileArray($filePreviewResizePatch);
        $arFilePreview['MODULE_ID'] = 'iblock';
        $arLoadProductArray['PREVIEW_PICTURE'] = $arFilePreview;

        $PRODUCT_ID = $el->Add($arLoadProductArray);
        if (!empty($el->LAST_ERROR)) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Error add photo - ' . $el->LAST_ERROR . '."}, "id" : "id"}');
        } else {
            @unlink($filePath);
            @unlink($fileOriginalResizePatch);
            @unlink($filePreviewResizePatch);
            @unlink($fileDetailResizePatch);
        }

    }

    #print_r($_REQUEST);
    #print_r($filePath.'+++');
}


// Return Success JSON-RPC response
die('{"jsonrpc" : "2.0", "result" : null, "id" : "id", "SectionGalleryIDValue" : ' . $intSectionGalleryIDValue . '}');
