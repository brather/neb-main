<?php
namespace LbcBundle\Common;

/**
 * User: agolodkov
 * Date: 02.09.2016
 * Time: 10:49
 */
class ArrayCollection extends \Doctrine\Common\Collections\ArrayCollection
{
    private $_count;

    /**
     * @inheritdoc
     */
    public function count()
    {
        if (null === $this->_count) {
            $this->_count = parent::count();
        }

        return $this->_count;
    }

    /**
     * @inheritdoc
     */
    public function slice($offset, $length = null)
    {
        $offset = ($offset - (integer)$this->_count);
        if ($offset < 0) {
            $offset = 0;
        }

        return parent::slice($offset, $length);
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->_count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->_count = $count;
    }
}