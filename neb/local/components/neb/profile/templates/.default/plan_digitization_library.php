<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $APPLICATION;

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->IncludeComponent(
	'neb:library.plan',
	'',
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"SEF_MODE"   => "Y",
		"SEF_FOLDER" => $arParams['SEF_FOLDER'] . str_replace('(.*)', '/', $arParams['SEF_URL_TEMPLATES']['plan_digitization']),
		"SEF_URL_TEMPLATES" => array(
			"list"   => "/",
			"detail" => "info/#ELEMENT_ID#/",
			"add"    => "add/",
			"reader" => "add_from_reader/",
		)
	),
	false
);