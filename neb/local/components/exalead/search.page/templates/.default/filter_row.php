<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<a href="javascript:void(0);" class="b-sidenav_title search-result__category-item"><?php echo $message; ?></a>
<ul class="b-sidenav_cont">
    <?php foreach($items as $k => $v): ?>
        <li class="first-level-li clearfix <?php echo $k > 4 ? 'hidden' : ''?>" id="lia<?php echo $k ?>">

            <a 
                href="javascript:void(0);"
                class="b-sidenav_title"
                <?php if(empty($v['data'])): ?>
                    data-tag-add-submit
                <?php endif; ?>
                data-tag-text="<?php echo $v['title']?>"
                data-tag-name="<?php echo 'f_field['.$field.'][]'; //mb_strtolower($field, 'UTF-8')?>"
                data-tag-value="<?php echo str_replace('"', '&quot;', $v['id'])?>"
            >
                <div class="b-sidenav_value left">
                    <?php echo $v['title']?>
                    <i>(<?php echo $v['count'];?><?php if($count > 300000): echo ' + '; endif; ?>)</i>
                </div>
            </a>
            <?php if(!empty($v['data'])):?>
                <ul class="b-sidenav_cont">
                    <?php foreach($v['data'] as $k2 => $v2): ?>
                        <li>
                            <a 
                                href="javascript:void(0);"
                                data-tag-add-submit
                                data-tag-text="<?php echo $v['title']?> > <?php echo $v2['title']?>"
                                data-tag-name="<?php echo 'f_field['.$field.'][]'; //mb_strtolower($field, 'UTF-8') ?>"
                                data-tag-value="<?php echo $v2['id']  ?>"
                            ><div class="b-sidenav_value left deepinside"><?php echo $v2['title']?> <i>(<?php echo $v2['data']?><?php if($count > 300000): echo ' + '; endif; ?>)</i></div></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

        </li>
    <?php endforeach; ?>
    <?php if(count($items) > 5): ?>
        <li><a class="b_sidenav_contmore js_moreopen" href="#">+ <?echo $more_message; ?></a></li>
    <?php endif; ?>
</ul>