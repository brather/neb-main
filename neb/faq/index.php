<?define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вопросы и ответы");?>
 <?$APPLICATION->IncludeComponent(
	"neb:faq.list",
	"",
	Array(
        "IBLOCK_CODE" => "FAQ",
        "IBLOCK_TYPE" => "site_content",
		"NAME_OF_PROJECT" => "НЭБ",
		"PAGE_DESCRIPTION" => "В этом разделе можно найти ответы на наиболее часто задаваемые вопросы пользователей.",
		"PAGE_TITLE" => "Вопросы и ответы",
		"SHOW_TITLE" => "N",
		"URL_FEEDBACK_PAGE" => "/feedback/"
	)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>