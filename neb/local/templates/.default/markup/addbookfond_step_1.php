<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title></title>
	
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width"/>
	<link rel="icon" href="favicon.ico" type="image/x-icon" />

	<script src="js/libs/modernizr.min.js"></script>

	<link rel="stylesheet" href="css/style.css">
	
	<script src="js/libs/jquery.min.js"></script>
	
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>	
	<script>window.jQuery.ui || document.write('<script src="js/libs/jquery.ui.min.js">\x3C/script>')</script>
	<script src="js/plugins.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/blind.js"></script>
	<script src="js/script.js"></script>
</head>
<body class="">
	<div class="b_bookfond_popup_content js_scroll">
		<form method="get" action="addbookfond_step_2.php" class="searchform">		
			<div class="b-portalinfo clearfix wrapper">
				
					<div class="b-search_field">
						<div class="clearfix">
						<input type="submit" class="b-search_bth bbox" value="Найти">
						<span class="b-search_fieldbox">

							<input type="text" name="" class="b-search_fieldtb bbox b-text" id="asearch" value="" placeholder="Пример: биография Гагарина" autocomplete="off" data-src="search.php?session=MY_SESSION&moreparams=MORE_PARAMS">
							<select name="searchopt" id=""class="js_select b_searchopt">
								<option value="1">по всем полям</option>
								<option value="2">по дате</option>

							</select>
						</span>
							
						</div>
						
						<div class="b_search_set clearfix">
							<a href="#" class="b-search_exlink js_extraform">Расширенный поиск</a>

						</div> <!-- /.b_search_set -->
					</div>


			</div><!-- /.b-portalinfo -->


			<div class="b-search wrapper">

			</div> <!-- /.b-search -->
			
			<div class="b-search_ex">
				<div class="wrapper rel">
					<div class="b_search_row js_search_row hidden" >
						<select name="logic" disabled="disabled" id="" class="ddl_logic">
							<option value="">или</option>
							<option value="">и</option>
							<option value="">не</option>
						</select>
						<select name="theme" disabled="disabled" id="" class="ddl_theme">
							<option value="">по всем полям</option>
							<option value="">по дате</option>
							<option value="foraccess">по доступу</option>
							<option value="">по названию</option>
						</select>
						<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
						<select name="taccess" disabled="disabled" id="" class="b-access hidden">
							<option value="">Свободный доступ</option>
							<option value="">Частичный доступ</option>
						</select>
						<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->
					</div>
					<div class="b_search_row visiblerow">
						<select name="logic[0]" id="" class="js_select ddl_logic ">
							<option value="">или</option>
							<option value="">и</option>
							<option value="">не</option>
						</select>
						<select name="theme[0]" id="" class="js_select ddl_theme">
							<option value="">по всем полям</option>
							<option value="">по дате</option>
							<option value="foraccess">по доступу</option>
							<option value="">по названию</option>
						</select>
						<input type="text" name="text[0]" class="b-text b_search_txt" id="">
						<select name="taccess" id="" class="js_select b-access hidden">
							<option value="">Свободный доступ</option>
							<option value="">Частичный доступ</option>
						</select>
						<!--<div class="b-list hidden"><input type="text" name="theme" class="b-text" id=""><a href="#"></a></div>-->

						<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">добавить условие</span></a>
					</div>						

					<div class="b_search_date">
						<div class="b_search_datelb iblock">Дата публикации</div>
						<div class="b_searchslider iblock js_searchslider"></div>
						<input class="hidden" type="text" id="js_searchdate_prev" value="1700"  />
						<input class="hidden"  type="text" id="js_searchdate_next" value="2014" />

					</div>
					<div class="b_search_row clearfix">
						<input type="submit" class="formbutton" value="Принять">
					</div>
				</div>
			</div><!-- /.b-search_ex -->
		</form>		
	</div>
</body>
</html>