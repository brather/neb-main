<?php
CBitrixComponent::includeComponentClass('rest:rest.server');
CBitrixComponent::includeComponentClass('neb:feedback');

use \Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc;

IncludeModuleLangFile(__FILE__);

/**
 * Class RestBookError
 */
class RestBookError extends RestServer
{
    /**
     * @var array
     */
    protected $_params = ['book_id' => null, 'comment' => null];

    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    protected static function getRequest() {
        return Application::getInstance()->getContext()->getRequest()->toArray();
    }

    /**
     * @return $this
     */
    protected function _prepareParams()
    {
        $arRequest = static::getRequest();

        /* user_email если есть, обязателен для заполнения */
        if (isset($arRequest['user_email']) && empty($arRequest['user_email']))
            $this->restError(403, Loc::getMessage( "BOOK_ERROR_EMAIL_REQUIRE" ));
        else if (isset($arRequest['user_email']) && !empty($arRequest['user_email']))
            $this->_params['user_email'] = $arRequest['user_email'];

        foreach ($this->_params as $sParam => $sValue)
            if ($sParam == 'token')
                continue;
            elseif (!isset($arRequest[$sParam]))
                $this->restError(403, Loc::getMessage( "BOOK_ERROR_PROPERTY" ) . $sParam . Loc::getMessage(
                        "BOOK_ERROR_REQUIRED"
                    ));
            elseif (empty($arRequest[$sParam]))
                $this->restError(403, Loc::getMessage( "BOOK_ERROR_PROPERTY" ) . $sParam . Loc::getMessage(
                        "BOOK_ERROR_IS_EMPTY"
                    ));
            else
                $this->_params[$sParam] = trim($arRequest[$sParam]);



        return $this;
    }

    /**
     * Добавление тикета обратной связи с темой "Исправление"
     *
     * @return int
     */
    public function postAction()
    {
        $this->arResult['success'] = false;

        $arUser = [];
        $obUser = new nebUser();
        if ($obUser->IsAuthorized())
            $arUser = $obUser->getUser();
        else if (isset($this->_params['user_email']) && !empty($this->_params['user_email']))
        {
            $u = CUser::GetList(($by = "ID"), ($order = "DESC"), array("EMAIL" => $this->_params['user_email']))->Fetch();

            if ($u)
            {
                $arUser["EMAIL"] = $u['EMAIL'];
                $arUser["ID"] = $u['ID'];
            }
            else
            {
                $arUser["EMAIL"] = $this->_params['user_email'];
                $arUser["ID"] = false;
            }

        }
        else
            $this->restError(403, Loc::getMessage( "BOOK_ERROR_NOT_AUTHORIZED" ));

        $obFeedback = new FeedbackComponent();

        $obFeedback->includeModules(['form']);

        // получение формы обратной связи
        $arForm = $obFeedback->getForm(['SID'=> 'SIMPLE_FORM_1']);

        // формирование массива полей результата
        $arValues = [];
        // e-mail
        $arAnswer = $arForm['FIELDS']['FB_EMAIL']['ANSWERS'];
        if (!empty($arAnswer)) {
            reset($arAnswer); $arAnswer = current($arAnswer);
            $arValues['form_' . $arAnswer['FIELD_TYPE'] . '_' . $arAnswer['ID']] = $arUser['EMAIL'];
        }
        // theme
        foreach ($arForm['FIELDS']['FB_THEME']['ANSWERS'] as $arAnswer)
            if ('CORRECTION' == $arAnswer['VALUE'])
                $arValues['form_' . $arAnswer['FIELD_TYPE'] . '_' . $arForm['FIELDS']['FB_THEME']['SID']] = $arAnswer['ID'];
        // message
        $arAnswer = $arForm['FIELDS']['FB_TEXT']['ANSWERS'];
        if (!empty($arAnswer)) {
            reset($arAnswer); $arAnswer = current($arAnswer);
            $arValues['form_' . $arAnswer['FIELD_TYPE'] . '_' . $arAnswer['ID']] = $this->_params['comment'];
        }
        // book
        $arAnswer = $arForm['FIELDS']['FB_BOOK_ID']['ANSWERS'];
        if (!empty($arAnswer)) {
            reset($arAnswer); $arAnswer = current($arAnswer);
            $arValues['form_' . $arAnswer['FIELD_TYPE'] . '_' . $arAnswer['ID']] = $this->_params['book_id'];
        }

        // запишем в базу результат
        $obFormResult = new CFormResult();
        if ($iResult = $obFormResult->Add($arForm['ID'], $arValues, "N", $arUser['ID'])) {
            $obFormResult->SetEvent($iResult);
            $obFormResult->Mail($iResult);
            $this->arResult['success'] = true;
        }
        else {
            global $strError;
            $this->restError(500, $strError);
        }

        return 200;
    }
}