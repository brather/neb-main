<?php
$MESS['BOOK_UPDATE_MESS_SUCCESS'] = 'Обновление параметров издания произошло успешно!';
$MESS['BOOK_UPDATE_MESS_ERROR'] = 'Ошибка! Обновления параметров издания не произошло.';
$MESS['BOOK_UPDATE_IBLOCK_ID_NOT_DEFINED'] = 'Ошибка! IBLOCK_ID не был определен.';