<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    "NAME" => Loc::getMessage('FAQ_LIST_FOR_EDIT_OR_DELETE_NAME'),
    "DESCRIPTION" => Loc::getMessage('FAQ_LIST_FOR_EDIT_OR_DELETE_DESC'),
    "ICON" => "/images/icon.gif",
    "SORT" => 10,
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "NEB"
    ),
    "COMPLEX" => "Y",
);