<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
Use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-copy"><?=Loc::getMessage('INCLUDE_COPYRIGHT_TEXT')?></div>