<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

//echo '<pre>', print_r($arResult, true), '</pre>';
Loc::loadMessages(__FILE__);
?>
<?
if ((int)$arResult['VALUES']['USER_ID'] > 0
    && $arResult["USE_EMAIL_CONFIRMATION"] === "Y"
) {
    ?>
    <section class="innersection innerwrapper clearfix">
        <div class="b-formsubmit">
            <span class="b-successlink"><?= GetMessage(
                    "REGISTER_SUCCESSFUL"
                ) ?></span>

            <p><?= GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
        </div>
    </section>
    <?
    return;
}
?>


<script type="text/javascript">
    function setlogin() {
        $('input#LOGIN').val($('input#settings04').val());
    }
</script>
<section class="innersection innerwrapper clearfix">
    <div class="b-registration rel">
        <h2 class="mode"><?= GetMessage("REGISTER_TITLE") ?></h2>
        <?
        if (count($arResult["ERRORS"]) > 0) {
            foreach ($arResult["ERRORS"] as $key => $error) {
                if (intval($key) == 0 && $key !== 0) {
                    $arResult["ERRORS"][$key] = str_replace(
                        "#FIELD_NAME#",
                        "&quot;" . GetMessage("REGISTER_FIELD_" . $key)
                        . "&quot;", $error
                    );
                }
            }
            ShowError(implode("<br />", $arResult["ERRORS"]) . '<br /><br />');
            ?>
        <?
        }
        ?>
        <?
        if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"
            && (int)$arResult['VALUES']['USER_ID'] > 0
        ) {
            ?>
            <p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
        <?
        }
        ?>
        <form method="post" action="<?= POST_FORM_ACTION_URI ?>"
              class="b-form b-form_common b-regform" name="regform"
              enctype="multipart/form-data" onsubmit="setlogin()"
              id="regform">
            <div class="fieldrow nowrap library-list-select">
                <div class="fieldcell iblock">
                    <em class="hint">*</em>
                    <label><?= Loc::getMessage(
                            'MAIN_REGISTER_LIBRARY'
                        ); ?></label>

                    <div class="field validate iblock">
                        <em class="error required"><?= Loc::getMessage(
                                'MAIN_REGISTER_REQ'
                            ); ?></em>
                        <select name="REGISTER[UF_LIBRARY]"
                                class="js_select" data-required="true">
                            <option value=""><?= Loc::getMessage(
                                    'MAIN_REGISTER_SELECT'
                                ); ?></option>
                            <?
                            foreach ($arResult['LIBRARIES'] as $arItem) {
                                ?>
                                <option <?=
                                $arResult['VALUES']['UF_LIBRARY']
                                == $arItem['ID'] ? 'selected="selected"'
                                    : '' ?>
                                    value="<?php echo $arItem['ID'] ?>"><?php echo $arItem['NAME'] ?></option>
                            <?
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div id="full-register-form">
                <?php require(dirname(__FILE__)
                    . '/../../template-parts/full-form.php'); ?>

            </div>
            <input type="hidden" name="REGISTER[UF_STATUS]"
                   value="<?php echo USER_STATUS_REGISTRED ?>"/>
        </form>
    </div>
    <!-- /.b-registration-->
</section>
