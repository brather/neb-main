<?php
/**
 * @var array $arResult
 */

CJSCore::Init(['popup']);

if (isset($arResult['userData'])) {
    $arResult['VALUES'] = $arResult['userData'];
} else {
    $arResult['VALUES'] = [];
}
$arResult['ERRORS'] = $arResult['errors'];

if (isset($_SESSION["EXTERNAL_AUTH_ERROR"])
    && !empty($_SESSION["EXTERNAL_AUTH_ERROR"])
) {
    $arResult['ERROR_MESSAGE'] = $_SESSION["EXTERNAL_AUTH_ERROR"];
    unset($_SESSION["EXTERNAL_AUTH_ERROR"]);
}
