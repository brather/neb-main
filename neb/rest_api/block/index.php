<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;
$APPLICATION->IncludeComponent(
	"rest:block",
	"", 
	array(
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/rest_api/block/",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"SEF_URL_TEMPLATES" => array(
			"processRequest" => "",
		)
	),
	false
);