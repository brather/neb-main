var FRONT = FRONT || {};
var neb = neb || {};
$(function () {
    FRONT.bbk = {};
    FRONT.bbk.divisionList = {};
    FRONT.bbk.divisionList.parentWidget = function (obj) {
        var widgetObj,
            $obj = $(obj);
        if ($obj.is('[data-division-list-widget]')) {
            widgetObj = $obj;
        } else {
            widgetObj = $obj.closest('[data-division-list-widget]');
        }
        return widgetObj;
    }
    FRONT.bbk.divisionList.item = {}
    FRONT.bbk.divisionList.item.activate = function (divisionItem) {
        $(divisionItem).toggleClass('active', true);
    }
    FRONT.bbk.divisionList.item.deactivate = function (divisionItem) {
        var item = $(divisionItem);
        item.find('.spinner').detach();
        item.find('.spinned').removeClass('spinned');
        item.toggleClass('active', false);
        $('[data-division-description-toggle]').toggleClass('active', false);
    }
    FRONT.bbk.divisionList.item.checkActions = function (divisionItem, callback) {
        if (true === divisionItem.validActions) {
            callback();
        } else {
            callback();
            var item = $(divisionItem).parent(),
                options = $(item).data('item-options'),
                actions = $(item).find('[data-division-actions]'),
                widget = $(item).closest('[data-division-list-widget]'),
                toggler = $(divisionItem)
                ;
            FRONT.spinner.execute(toggler.find('[data-toggle]').get(0), FRONT.spinner.btnOpts);
            toggler.find('[data-toggle]').toggleClass('spinned', true);
            actions.find('ul li').hide();
            actions.find('ul').hide();
            widget.toggleClass('requests',true);
            $.ajax({
                url: buildApiUrl(options.urlCheckActions),
                dataType: 'json',
                method: 'GET',
                data: {},
                success: function (data) {
                    if (data.actions) {
                        for (var key in data.actions) {
                            var code = data.actions[key].code;
                            var action = actions.find('[data-' + code + ']');
                            if (action.length) {
                                action.parent().show();
                            }
                        }
                        divisionItem.validActions = true;
                        actions.find('ul').show();
                        actions.find('.spinner').detach();
                        actions.find('.spinned').removeClass('spinned');
                    }
                }
            }).fail(function( jqXHR, textStatus, errorThrown ) {
                if (console && console.log) {
                    console.log('textStatus is %s\n\nerrorThrowns is %s', textStatus, errorThrown);
                    console.dir(jqXHR);
                }
                divisionItem.validActions = true;
                actions
                    .find('ul').show().end()
                    .find('ul').css('background-color','#eee').end()
                    .find('.spinner').detach().end()
                    .find('.spinned').removeClass('spinned').end()
                    .find('a').attr('title','не отфильтровано').end()
                    .find('a').parent().show().end();
            })
            .always(function(){
                widget.toggleClass('requests',false);
            });
        }
    }
    FRONT.bbk.divisionList.item.moveItem = function (divisionItem, direction) {
        var options = divisionItem.data('item-options'),
            url = ('up' === direction ? options.urlMoveUp : options.urlMoveDown);
        //console.log(arguments);
        $.ajax({
            url: buildApiUrl(url),
            dataType: 'json',
            method: 'PUT',
            data: {},
            success: function () {
                //console.log(divisionItem);
                //console.log(divisionItem.prev('dt:first'));
                if ('up' === direction) {
                    divisionItem.insertBefore(divisionItem.prevAll('dt:first'));
                } else {
                    divisionItem.insertAfter(divisionItem.nextAll('dt:first'));
                }
            }
        });
    }
    FRONT.bbk.divisionTemplates = {};
    FRONT.bbk.divisionTemplatesMustache = {
        card: '',
        editForm: '',
        approveForm: ''
    };

    var buildApiUrl = function (url) {
        return url + (-1 === url.indexOf('?') ? '?' : '&') + 'token=' + getUserToken()
    };

    var _createPopup = function (callback) {
        var modalMarkup = $('#universal-modal').clone(true).insertAfter('#universal-modal');
        modalMarkup.removeAttr('id');
        return $(modalMarkup)
            .one('show.bs.modal', callback)
            .one('hidden.bs.modal', function () {
                $(modalMarkup).detach();
            });
    };

    var _renderCard = function (modal, data, okCallback) {

        if (!neb.objectPathSearch(data, 'entity.fieldList')) {
            data.entity.fieldList = {};
            for (var fieldName in data.entity) {
                data.entity.fieldList[fieldName] = true
            }
        }

        modal.find('.modal-dialog').addClass('modal-lg').end()
            .find('.modal-title').text('Карточка раздела').end()
            .find('.modal-body').html(Mustache.render(
                data.canEdit
                    ? FRONT.bbk.divisionTemplatesMustache.editForm
                    : FRONT.bbk.divisionTemplatesMustache.card,
                data.entity)).end()
            .find('.modal-footer').hide();

        FRONT.autocomplete(modal.find(
            '[data-auto-complete]'), {
            success: function (data, entityField, queries, entityList) {
                if (data.entities) {
                    for (var key in data.entities) {
                        if (data.entities[key]['code']) {
                            var result = data.entities[key]['code'] + ' - ' + data.entities[key]['name'];
                            queries.push(result);
                            entityList[result] = data.entities[key].id;
                        }
                    }
                }
            }
        });

        modal.find('.buttons-bar button.btn').on('click', function (e) {
            e.preventDefault();
            var divisionData = {};
            divisionData.code = modal.find('input[name="code"]').val();
            divisionData.name = modal.find('input[name="name"]').val();
            divisionData.parent = modal.find('input[name="parent"]').val();
            divisionData.comments = modal.find('textarea[name="comments"]').val();
            divisionData.links = modal.find('textarea[name="links"]').val();
            divisionData.keywords = modal.find('textarea[name="keywords"]').val();
            var iscommon = modal.find('input[name="iscommon"]');
            if (iscommon.length) {
                divisionData.iscommon = 0;
                if (modal.find('input[name="iscommon"]').is(':checked')) {
                    divisionData.iscommon = 1;
                }
            }

            okCallback(divisionData);
        });
    };

    var _openCreatePopup = function (options) {
        _createPopup(function () {
            var modal = $(this);
            _renderCard(
                modal,
                {
                    canEdit: true,
                    entity: {
                        fieldList: options.fieldList,
                        parent: {
                            id: options.categoryId,
                            name: $('.bbk-crumbs').find('span:last').text()
                        }
                    }
                },
                function (patchData) {
                    var data = {};
                    data[options.formName] = patchData;
                    $.ajax({
                        url: buildApiUrl(options.saveUrl),
                        dataType: 'json',
                        method: 'POST',
                        data: data,
                        success: function () {
                            modal.modal('hide');
                            window.location.reload();
                        }
                    });
                });
        })
            .modal('show');

    };

    var init = function initTemplates() {
        $.ajax({
            url: '/local/templates/adaptive/mustache/bbk/category-card.mustache',
            dataType: 'html',
            success: function (data) {
                FRONT.bbk.divisionTemplatesMustache.card = data;
            }
        });
        $.ajax({
            url: '/local/templates/adaptive/mustache/bbk/category-edit-form.mustache',
            dataType: 'html',
            success: function (data) {
                FRONT.bbk.divisionTemplatesMustache.editForm = data;
            }
        });
        $.ajax({
            url: '/local/templates/adaptive/mustache/bbk/approve-form.mustache',
            dataType: 'html',
            success: function (data) {
                FRONT.bbk.divisionTemplatesMustache.approveForm = data;
            }
        });
        $.ajax({
            url: '/local/templates/adaptive/mustache/bbk/history-list.mustache',
            dataType: 'html',
            success: function (data) {
                FRONT.bbk.divisionTemplatesMustache.history = data;
            }
        });

    }();



    $(document)
        .on('click', '[data-bbk-search-form]', function (e) {
            var widget = $(this),
                extendedFieldset = widget.find('[data-bbk-extended-block]'),
                togglers = widget.find('[data-bbk-extended-toggler]');
            togglersAssets = togglers.find('*');
            if ($(e.target).is(togglers) || $(e.target).is(togglersAssets)) {
                e.preventDefault();
                extendedFieldset.stop().slideToggle({
                    start: function () {
                        widget.toggleClass('bbk-extended', true);
                        togglers.toggleClass('focus', true);
                    },
                    done: function () {
                        widget.toggleClass('bbk-extended', extendedFieldset.is(':visible'));
                        togglers.toggleClass('focus', extendedFieldset.is(':visible')).blur();
                    }
                });
            }
        })
        .on('click', '[data-division-description-toggle]', function () {
            var item = $(this).closest('[data-division-item]'),
                description = item.next('[data-division-description]'),
                widget = item.closest('[data-division-list-widget]'),
                items = widget.find('[data-division-item]'),
                toggler = $(this);
            items.each(function () {
                var i = $(this),
                    d = i.next('[data-division-description]');
                d.not(description).slideUp();
                // i.next('[data-division-description]').slideUp();
                FRONT.bbk.divisionList.item.deactivate(i);
            });
            if (description.length) {
                toggler.toggleClass('active', true);
                FRONT.bbk.divisionList.item.activate(item);
                description.slideToggle(function () {
                    if (!description.is(':visible')) {
                        FRONT.bbk.divisionList.item.deactivate(item);
                    } else {
                    }
                });
            }
        });

    (function(){
        var filled = false;
        $('[data-bbk-extended-block] input').each(function(){
            if ( this.type == 'text' && this.value != '' || this.type == 'checkbox' && this.checked ) {
                filled = true;
                return false;
            }
        });
        if (filled) {
            $('[data-bbk-extended-toggler]').trigger('click')
        }
    })();

    $('[data-division-actions]').on('show.bs.dropdown', function () {
        if (console && console.log) {
            // console.log('fire2');
        }
        FRONT.bbk.divisionList.item.checkActions(this, function () {
            var item = $(this).closest('[data-division-item]'),
                itemdesc = item.next('[data-division-description]'),
                items = item.closest('[data-division-list-widget]').find('[data-division-item]'),
                itemsDesc = item.closest('[data-division-list-widget]').find('[data-division-item]').next('[data-division-description]'),
                descToggler = item.find('[data-division-description-toggle]');
            FRONT.bbk.divisionList.item.deactivate(items.not(item));
            FRONT.bbk.divisionList.item.activate(item);
            itemsDesc.slideUp();
            descToggler.toggleClass('active', false);
        }.bind(this));
    });

    $(document).on('click', '[data-next-toggle]', function (e) {
        e.preventDefault();
        $(this).next().toggle();
    });

    $('[data-division-actions]').on('hide.bs.dropdown', function (e) {
        var item = $(this).closest('[data-division-item]');
        FRONT.bbk.divisionList.item.deactivate(item);
    });

    $(document).on('click', '[data-division-actions]', function (e) {
        if (console && console.log) {
            // console.log('fire');
        }
        var item = $(this).closest('[data-division-item]');
        var options = item.data('item-options');
        var divisionId = $(this).data('division-id');
        var toggler = $(this),
            widget = toggler.closest('[data-division-list-widget]');
        var openPopup = function (callback) {
            _createPopup(callback)
                .modal('show', toggler);
        };
        var openDivisionPopup = function (data) {
            openPopup(function () {
                var modal = $(this);
                data.entity.fieldList = {};
                for (var fieldName in data.entity) {
                    data.entity.fieldList[fieldName] = true
                }
                _renderCard(
                    modal,
                    data,
                    function (patchData) {
                        var data = {};
                        data[options.formName] = patchData;
                        $.ajax({
                            url: buildApiUrl(options.urlUpdate),
                            dataType: 'json',
                            method: 'PATCH',
                            data: data,
                            success: function (data) {
                                modal.modal('hide');
                                window.location.reload();
                            }
                        });
                    });
            });
        };

        var openIntroduction = function (data) {
            openPopup(function () {
                var modal = $(this);

                modal.find('.modal-dialog').addClass('modal-lg').end()
                    .find('.modal-title').text('Введение раздела').end()
                    .find('.modal-body').html(data.content).end();
            });
        };

        var openCategoryImages = function (data) {
            openPopup(function () {
                var modal = $(this);
                var dataBody = $('<div class="images-list"></div>');
                var width = $(this).find('.modal-dialog').width();
                if (data.entities) {
                    $.each(data.entities, function (key, entity) {
                        var slide = $('<div><img style="width:100%" data-lazy="' + entity.imagepath + '"/></div>');
                        //slide.width(width);
                        //slide.find('img').width(width);
                        dataBody.append(slide);
                    });
                }
                modal.find('.modal-dialog').addClass('modal-lg').end()
                    .find('.modal-title').text('Сканы введения').end()
                    .find('.modal-body').html(dataBody).end();
                modal.on('shown.bs.modal', function () {
                    dataBody.slick({
                        lazyLoad: 'ondemand',
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true
                    });
                    window.setTimeout(function () {
                        modal.find('.slick-list').attr('tabindex', 0).focus();
                    }, 1000);
                });
            });
        };

        var openGrPopup = function (data, params) {
            openPopup(function () {
                var modal = $(this);

                modal.find('.modal-dialog').addClass('modal-lg').end()
                    .find('.modal-title').text(params.title).end()
                    .find('.modal-body').html(Mustache.render(FRONT.bbk.divisionTemplatesMustache.approveForm, data.entity)).end()
                    .find('.modal-footer').hide();

                modal.find('.buttons-bar button.btn').on('click', function (e) {
                    e.preventDefault();
                    var divisionData = {};
                    divisionData.grerr = modal.find('textarea[name="grerr"]').val();
                    divisionData.gr = params.gr;

                    var patchData = {};
                    patchData[options.formName] = divisionData;
                    $.ajax({
                        url: buildApiUrl(options.urlUpdate),
                        dataType: 'json',
                        method: 'PATCH',
                        data: patchData,
                        success: function () {
                            modal.modal('hide');
                            window.location.reload();
                        }
                    });
                });
            });
        };

        var openApprovePopup = function (data) {
            openGrPopup(
                data,
                {
                    title: 'Утверждение',
                    gr: 0
                });
        };

        var openDeletePopup = function (data) {
            openGrPopup(
                data,
                {
                    title: 'Удаление',
                    gr: 1
                });
        };

        var openRecoveryPopup = function (data) {
            openGrPopup(
                data,
                {
                    title: 'Восстановление',
                    gr: 0
                }
            )
        };

        var openHistoryPopup = function (data) {
            openPopup(function () {
                var modal = $(this);

                modal.find('.modal-dialog').addClass('modal-lg').end()
                    .find('.modal-title').text('История раздела').end()
                    .find('.modal-body').html(Mustache.render(FRONT.bbk.divisionTemplatesMustache.history, data)).end()
                    .find('.modal-footer').hide();
            });
        };

        if ($(e.target).is($('[data-division-card]'))) {
            e.preventDefault();
            e.stopPropagation();
            FRONT.spinner.execute(toggler.find('[data-toggle]').get(0), FRONT.spinner.btnOpts);
            toggler.find('[data-toggle]').toggleClass('spinned', true);

            $.getJSON(
                buildApiUrl(options.urlDetail),
                openDivisionPopup
            )
                .fail(function () {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                    FRONT.infomodal({title: "Карточка раздела недоступна, обратитесь к оператору НЭБ"});
                })
                .done(function () {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                });
        }

        if ($(e.target).is($('[data-division-introduction]'))) {
            e.preventDefault();
            e.stopPropagation();

            FRONT.spinner.execute(toggler.find('[data-toggle]').get(0), FRONT.spinner.btnOpts);
            toggler.find('[data-toggle]').toggleClass('spinned', true);

            $.getJSON(
                buildApiUrl('/api/bbk/maindivision/' + divisionId + '/introduction/'),
                openIntroduction
            )
                .fail(function (data) {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                    FRONT.infomodal({title: "Просмотр введения недоступен, обратитесь к оператору НЭБ"});
                })
                .done(function (data) {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                });
        }

        if ($(e.target).is($('[data-division-introduction-images]'))) {
            e.preventDefault();
            e.stopPropagation();
            FRONT.spinner.execute(toggler.find('[data-toggle]').get(0), FRONT.spinner.btnOpts);
            toggler.find('[data-toggle]').toggleClass('spinned', true);

            $.getJSON(
                buildApiUrl('/api/bbk/maindivision/' + divisionId + '/introduction/images/'),
                openCategoryImages
            )
                .fail(function () {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                    FRONT.infomodal({title: "Скан введения недоступен, обратитесь к оператору НЭБ"});
                })
                .done(function () {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                });
        }

        if ($(e.target).is($('[data-division-images]'))) {
            e.preventDefault();
            e.stopPropagation();
            FRONT.spinner.execute(toggler.find('[data-toggle]').get(0), FRONT.spinner.btnOpts);
            toggler.find('[data-toggle]').toggleClass('spinned', true);

            $.getJSON(
                buildApiUrl(options.urlImages),
                openCategoryImages
            )
                .fail(function () {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                    FRONT.infomodal({title: "Скан основного деления недоступен, обратитесь к оператору НЭБ"});
                })
                .done(function () {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                });
        }

        if ($(e.target).is($('[data-link-action]'))) {
            e.preventDefault();
            e.stopPropagation();
            FRONT.spinner.execute(toggler.find('[data-toggle]').get(0), FRONT.spinner.btnOpts);
            toggler.find('[data-toggle]').toggleClass('spinned', true);
            location.href = $(e.target).data('url');
        }

        if ($(e.target).is($('[data-approve]'))) {
            e.preventDefault();
            $.getJSON(
                buildApiUrl(options.urlDetail),
                openApprovePopup
            );
        }

        if ($(e.target).is($('[data-recovery]'))) {
            e.preventDefault();
            $.getJSON(
                buildApiUrl(options.urlDetail),
                openRecoveryPopup
            );
        }

        if ($(e.target).is($('[data-delete]'))) {
            e.preventDefault();
            $.getJSON(
                buildApiUrl(options.urlDetail),
                openDeletePopup
            );
        }

        if ($(e.target).is($('[data-history]'))) {
            if (console && console.log) {
                // console.log('not spinned');
            }
            e.preventDefault();
            e.stopPropagation();
            FRONT.spinner.execute(toggler.find('[data-toggle]').get(0), FRONT.spinner.btnOpts);
            toggler.find('[data-toggle]').toggleClass('spinned', true);

            $.getJSON(
                buildApiUrl('/api' + options.urlCategory + 'history/'),
                openHistoryPopup
            )
                .fail(function () {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                    FRONT.infomodal({title: "История карточки раздела недоступна, обратитесь к оператору НЭБ"});
                })
                .done(function () {
                    FRONT.bbk.divisionList.item.deactivate(item);
                    toggler.click();
                });
        }
    });

    $('.show-item-path').click(function () {
        $(this).parent().find('.path-container').slideToggle();
    });

    $('.sort-arrow').click(function () {
        var item = $(this).parents('[data-division-item]'),
            direction = $(this).hasClass('sort-down') ? 'down' : 'up'
            ;
        FRONT.bbk.divisionList.item.moveItem(item, direction);
    });

    $('[data-create-category]').click(function () {
        _openCreatePopup({
            saveUrl: $(this).data('url-save'),
            formName: $(this).data('form-name'),
            categoryId: $(this).data('category-id'),
            fieldList: $(this).data('field-list')
        });
    });

    function clickDelay(el) {
        $(el).toggleClass('clickdelay',true);
        setTimeout(function(){
            $(el).toggleClass('clickdelay',false);
        }, 500);
    }

    $('[data-division-list-widget]').on('touchstart','[data-division-item]',function(e){
        // $(this).toggleClass('hover',true);
        // $(this).siblings().toggleClass('hover',false);
        if ( $(this).hasClass('hover') ) {

        } else {
            clickDelay(this);
        }
    });

    $('[data-division-list-widget]').on('touchend','[data-division-item]',function(e){
        // clickDelay(this);
    });

    $('.bx-touch [data-division-list-widget]').on('click','.bbk-main-uli',function(e){
        // console.log('click');
        var row = $(this).closest('[data-division-item]'),
            has = row.hasClass('clickdelay'); // флаг что тронул тока что
        // console.log(has);
        if (has) {
            e.preventDefault();
            // console.log('hasclass - prevent');
            $(row).toggleClass('hover',true);
            $(row).siblings().toggleClass('hover',false);
        } else {
            // console.log('else - go');
        }
    });

});

var clearForm = function (oForm) {
    var elements = oForm.elements;
    oForm.reset();
    for(var i=0; i < elements.length; i++) {
        field_type = elements[i].type.toLowerCase();
        switch(field_type) {
            case "text":
            case "password":
            case "textarea":
            case "hidden":
                elements[i].value = "";
                break;
            case "radio":
            case "checkbox":
                if (elements[i].checked) {
                    elements[i].checked = false;
                }
                break;
            case "select-one":
            case "select-multi":
                elements[i].selectedIndex = -1;
                break;
            default:
                break;
        }
    }

    return false;
};
