<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER["DOCUMENT_ROOT"]
    . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized()) {
    exit();
}

CModule::IncludeModule("nota.userdata");
use Nota\UserData\Collections;
use Nota\UserData\Books;
use Bitrix\NotaExt\Iblock\Element;

$error = null;
try {
    $idObject = htmlspecialcharsbx(trim(urldecode($_REQUEST['id'])));
    $idCollection = intval($_REQUEST['idCollection']);
    $isaddLink = (!empty($_REQUEST['action']) and $_REQUEST['action']
        == 'checked');
    $type = htmlspecialcharsbx(trim($_REQUEST['t']));

    if ('books_collection' === $type) {
        $arBooks = Element::getList(
            array('IBLOCK_CODE'             => IBLOCK_CODE_COLLECTIONS,
                  'SECTION_ID'              => (int)$idObject,
                  '!PROPERTY_BOOK_ID_VALUE' => false),
            false,
            array('PROPERTY_BOOK_ID', 'IBLOCK_ID', 'skip_other')
        );
    }
    if (!empty($idObject) and !empty($idCollection) and !empty($type)) {
        if ($type
            == 'books_collection'
        ) # добавление\удаление связи книг скопом из коллекции
        {
            if (!empty($arBooks['ITEMS'])) {
                foreach ($arBooks['ITEMS'] as $arItem) {
                    if ($isaddLink) {
                        Collections::addLinkObject(
                            $arItem['PROPERTY_BOOK_ID_VALUE'], $idCollection,
                            'books'
                        );
                    } else {
                        Collections::removeLinkObject(
                            $arItem['PROPERTY_BOOK_ID_VALUE'], $idCollection,
                            'books'
                        );
                    }
                }
            }
        } else {
            if ($isaddLink) {
                Collections::addLinkObject($idObject, $idCollection, $type);
            } else {
                Collections::removeLinkObject($idObject, $idCollection, $type);
            }
        }
        exit();
    }

    # если вызвали диалог из объекта книги, значит добавим ее в подборку
    if ($type == 'books') {
        $saveID = Books::add($idObject);
    } elseif ($type == 'books_collection') # добавление книг скопом из коллекции
    {
        \Nota\UserData\UsersBooksCollectionDataTable::applyCollection(
            array(
                'UF_UID'           => $USER->GetID(),
                'UF_COLLECTION_ID' => $idObject
            )
        );
        if (!empty($arBooks['ITEMS'])) {
            foreach ($arBooks['ITEMS'] as $arItem) {
                $saveID = Books::add($arItem['PROPERTY_BOOK_ID_VALUE']);
            }
        }

        $idObject = false;
    }

    $arCollections = Collections::getListCurrentUser($idObject, $type);
} catch (Exception $e) {
    $error = $e->getMessage();
}
if ($arCollections !== false) {
    if (null !== $error) {
        ShowError($error);
    }
    ?>
    <ul class="tag-list" data-tag-list>
        <?
        foreach ($arCollections as $arData) {
            ?>
            <li class="tag-list__item">

                <label
                    <?/*for="col<?= $arData['ID'] ?>"*/ ?>
                    >
                    <input class="checkbox InCollection"
                           type="checkbox"
                           name="collections[]" value="<?= $arData['ID'] ?>"
                           data-toggle-tag
                        <?/*id="col<?= $arData['ID'] ?>"*/ ?>
                        <?= !empty($arData['CHECKED']) ? ' checked="checked"'
                            : '' ?>
                        >
                    <span class="lbl"><?= $arData['UF_NAME'] ?></span>
                </label>
            </li>
            <?
        }
        ?>
    </ul>
    <?
}