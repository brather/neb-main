<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
    \Neb\Main\Helper\LangHelper;

Loc::loadMessages(__FILE__);

?>

<header class="search-result__header">
    <div class="row">
        <div class="search-result__header-info col-md-8">

            <?php if (!empty($_REQUEST['converted_to_all'])): ?>
                <p><?php echo Loc::getMessage('CONVERTED_TO_ALL'); ?></p>
            <?php endif; ?>

            <?php if ($arResult['COUNT'] < 300000) {
                ?>
                <p>
                    <?php if (true === $arResult['FULL_QUERY_SEARCH']): // третий этап поиска ?>

                        <?php echo Loc::getMessage('LIB_SEARCH_PAGE_FULL_SEARCH'); ?>.

                        <?= GetEnding($arResult['NHITS'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_2')) ?>
                        <?= number_format($arResult['NHITS'], 0, '', ' ') ?>
                        <?= GetEnding($arResult['NHITS'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_RESULT_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_RESULT_2'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_RESULT_1')) ?>.

                    <?php elseif (true === $arResult['STRICT_QUERY_SEARCH']): // второй этап поиска ?>

                        <?= GetEnding($arResult['NHITS'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_2')) ?>
                        <?= number_format($arResult['NHITS'], 0, '', ' ') ?>
                        <?= GetEnding($arResult['NHITS'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_RESULT_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_RESULT_2'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_RESULT_1')) ?>
                        <?= Loc::getMessage('SEARCH_STRICT_RESULTS'); ?>

                        <a href="<?= $arResult['FULL_QUERY_SEARCH_URL'] ?>"><?= Loc::getMessage('SHOW_MORE'); ?></a>

                    <?php else: // первый этап поиска ?>

                        <?= GetEnding($arResult['NHITS'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_2')) ?>
                        <?= number_format($arResult['NHITS'], 0, '', ' ') ?>
                        <?= GetEnding($arResult['NHITS'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1')) ?>.

                        <p><?php echo str_replace('#LINK#', $arResult['STRICT_QUERY_SEARCH_URL'], Loc::getMessage('SEARCH_STRICT')); ?></p>

                    <?php endif; ?>
                </p>
            <?php } ?>

            <?php if ('archive' == $_REQUEST['librarytype']): ?>

                <?php if (empty($arResult['COUNT'])): ?>
                    <p>
                        <?=Loc::getMessage('MORE_RESULTS')?> <a
                            href="http://opisi.garf.su/default.asp?base=garf&menu=3&nebquery=<?php echo urlencode(iconv('utf-8', 'windows-1251', $arResult["REQUEST"]['QUERY'])) ?>"><?=Loc::getMessage('ARCHIVE_OF_THE_RUSSIAN_FEDERATION')?></a>.
                    </p>
                <?php else: ?>
                    <p>
                        <?=Loc::getMessage('MORE_RESULTS')?> <a
                            href="http://opisi.garf.su/default.asp?base=garf&menu=3&nebquery=<?php echo urlencode(iconv('utf-8', 'windows-1251', $arResult["REQUEST"]['QUERY'])) ?>"><?=Loc::getMessage('ARCHIVE_OF_THE_RUSSIAN_FEDERATION')?></a>.
                    </p>
                <?php endif; ?>
            <?php endif; ?>

            <?php if (isset($arResult["REQUEST"]['ORIGINAL_QUERY'])): ?>
                <p><?=Loc::getMessage('LAYOUT_CHANGED')?> "<?php echo $arResult["REQUEST"]['ORIGINAL_QUERY'] ?>"
                    <a href="<?php echo $_SERVER['REQUEST_URI'] . '&notransform=1' ?>"><?=Loc::getMessage('SEARCH_BY_ORIGINAL_QUERY')?></a>
                </p>
            <?php elseif (
                false === mb_stripos($_SERVER['HTTP_HOST'], SITE_HOST) && $arResult["spellcheckSuggestion"]
                ): // просили убрать в задаче #20879, для разработки оставил везде кроме прода ?>
                <p>
                    <? echo GetMessage("LIB_SEARCH_PAGE_TEMPLATE_SUGGEST") ?>
                    <a href="?<?= str_replace($_REQUEST['q'], $arResult["REQUEST"]['QUERY_SUGGEST'], urldecode(nfDeleteParam(array('PAGEN_' . $arResult['NavNum'])))) ?>"
                       id="spellcheckSuggestion">
                        <?
                        $i = 0;
                        if ($arResult["REQUEST"]["QUERY_SUGGEST"]):
                            $i = 1;
                            ?>
                            <?= $arResult["REQUEST"]["QUERY_SUGGEST"] ?>
                        <? endif; ?>
                        <?
                        foreach ($arResult['SUGGEST_PARAMS'] as $param):
                            $i++;
                            ?>
                            <?= ($i > 1) ? $param['logic'] . ' ' : '' ?> <?= $param['text'] ?>
                        <? endforeach; ?>
                    </a>
                </p>
            <?php endif; ?>

            <p class="" id="spellcheckSuggestion_ru" style="display: none"><?=Loc::getMessage('SEARCH_IN_RUSSIAN')?>:
                <a href="#"></a>
            </p>

            <p class="" id="spellcheckSuggestion_en" style="display: none"><?=Loc::getMessage('SEARCH_IN_ENGLISH')?>:
                <a href="#"></a>
            </p>

            <p class="" id="spellcheckSuggestion_de" style="display: none"><?=Loc::getMessage('SEARCH_IN_DEUTSCH')?>:
                <a href="#"> </a>
            </p>

        </div>
        <div class="search-result__header-save-btn col-md-4">
            <?php if ($USER->IsAuthorized()): ?>
                <a href="/profile/searches/" data-widget-save-query>
                    <?=Loc::getMessage('SAVE_QUERY')?>
                </a>
                <form id="searchsave_form" method="POST"
                      action="/local/components/exalead/search.page/templates/.default/save_query.php">
                    <input id="sessid" type="hidden" value="<?php echo bitrix_sessid(); ?>" name="sessid" />
                    <input type="hidden" value="<?php echo $_SERVER['REQUEST_URI'] ?>" name="url" />
                    <input type="hidden" value="<?php echo $_REQUEST['q'] ?>" name="q" />
                    <input type="hidden" value="" name="more_options" />
                    <input type="hidden" value="<?php echo $arResult['COUNT']; ?>" name="found" />
                    <input type="hidden" name="exalead_param"
                           value='<?php echo json_encode(htmlspecialcharsEx($arResult['query']->getParameters())) ?>' />
                </form>
            <?php endif; ?>
        </div>
    </div>
</header>

<script type="text/javascript">
    $(function(){
        $(document).on('click', '[data-widget-save-query]', function(e){
            e.preventDefault();
            var toggler = $(this),
                saved = toggler.data('saved') || false;

            if ( !saved ) {
                $.post("/local/components/exalead/search.page/templates/.default/save_query.php", $("#searchsave_form").serialize()).done(function (data) {
                    toggler.text('<?=Loc::getMessage('SAVED')?>');
                    toggler.data('saved',true);
                    toggler.removeAttr('href').css('text-decoration','none');
                });
            }
            /* window.location.href = toggler.attr('href'); */
        });
    });
</script>

<? if ($arParams['arParams']['SEARCH_QUERY_TRANSLATE'] && '#all' !== $arResult["REQUEST"]['QUERY']): ?>
    <script type="text/javascript">
        $(function () {
            translate('<?php echo $arResult["REQUEST"]['QUERY'] ?>');
        });

        function translate(text) {

            if (!text)
                return false;

            <? foreach (['ru', 'en', 'de'] as $sLang): ?>
            
                $.getJSON('<?=LangHelper::getYandexPath()?>', {
                    key:  '<?=LangHelper::getYandexKey()?>',
                    lang: '<?=$sLang?>',
                    text: text
                }, function (data) {
                    if (typeof data.text !== 'undefined') {
                        url = '?q=' + encodeURIComponent(data.text) + '&' + '<?= DeleteParam(['q'])?>';
                        if ('de' == '<?=$sLang?>' && -1 !== text.toLowerCase().indexOf('русский') && -1 !== data.text[0].indexOf('Deutsch')) {
                            data.text[0] = data.text[0].replace('Deutsch', 'Russisch');
                        }
                        if(data.text != text) {
                            $('#spellcheckSuggestion_<?=$sLang?> a').text(data.text).attr('href', url);
                            $('#spellcheckSuggestion_<?=$sLang?>').show();
                        }
                    }
                });

            <? endforeach; ?>
        }
    </script>
<? endif; ?>