<?php
$MESS['USER_VERIFICATION_LIST_USER_ADD']  = 'Добавить читателя';
$MESS['USER_VERIFICATION_LIST_ADD']       = 'Добавить ЭЧБ';
$MESS['USER_VERIFICATION_LIST_EDIT']      = 'Редактировать ЭЧБ';
$MESS['USER_VERIFICATION_LIST_DELETE']    = 'Удалить ЭЧБ';
$MESS['USER_VERIFICATION_LIST_DEACT']     = 'Деактивировать пользователя';
$MESS['USER_VERIFICATION_LIST_LOGIN']     = 'E-mail';
$MESS['USER_VERIFICATION_LIST_FULLNAME']  = 'ФИО';
$MESS['USER_VERIFICATION_LIST_EECHB']     = 'Номер ЕЭЧБ';
$MESS['USER_VERIFICATION_LIST_NUMBER']    = 'Номер ЭЧБ';
$MESS['USER_VERIFICATION_LIST_DATE']      = 'Дата окончания ЭЧБ';
$MESS['USER_VERIFICATION_LIST_VER']       = 'Верифицирован';
$MESS['USER_VERIFICATION_LIST_NOT_VER']   = 'Не верифицирован';
$MESS['USER_VERIFICATION_LIST_SAVE']      = 'Сохранить';