<?php
if (!check_bitrix_sessid()) {
	return;
}

$entity = CUserTypeEntity::GetList(array(), array('ENTITY_ID' => 'BOOK_COLLECTION'));
if(!count($entity->arResult))
{		
	$arFields = array(
	  'ENTITY_ID' => 'BOOK_COLLECTION', # объект
	  'FIELD_NAME' => 'UF_EXALEAD_LINK', # название поля
	  'USER_TYPE_ID' => 'exalead_book', #тип свойства
	  'EDIT_FORM_LABEL' => array('ru' => 'Привязка к книге в Exalead'), 
	 );
	 
	 $obEntity =  new CUserTypeEntity();
	 $obEntity->Add($arFields);
}

$entity = CUserTypeEntity::GetList(array(), array('ENTITY_ID' => 'LIBS_MAP'));
if(!count($entity->arResult))
{		
	$arFields = array(
	  'ENTITY_ID' => 'LIBS_MAP', # объект
	  'FIELD_NAME' => 'UF_LIBS_MAP', # название поля
	  'USER_TYPE_ID' => 'user_map_yandex', #тип свойства
	  'EDIT_FORM_LABEL' => array('ru' => 'Координаты библиотеки на карте'), 
	 );
	 
	 $obEntity =  new CUserTypeEntity();
	 $obEntity->Add($arFields);
}

if ($ex = $APPLICATION->GetException()) {
	echo CAdminMessage::ShowMessage(
		array(
			'TYPE'    => 'ERROR',
			'MESSAGE' => GetMessage('MOD_INST_ERR'),
			'DETAILS' => $ex->GetString(),
			'HTML'    => true
		)
	);
} else {
	echo CAdminMessage::ShowNote(GetMessage('MOD_INST_OK'));
}
?>

<form action="<?= $APPLICATION->GetCurPage() ?>">
	<input type="hidden" name="lang" value="<?= LANG ?>">
	<input type="submit" name="" value="<?= GetMessage("MOD_BACK") ?>">
</form>