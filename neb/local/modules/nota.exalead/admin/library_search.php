<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!Main\Loader::includeModule('highloadblock'))
	throw new Main\LoaderException('Модуль highloadblock не установлен!');

// Идентификатор Highload блока библиотек
$libraryHlblockId = 6;

$hlblock = HL\HighloadBlockTable::getById($libraryHlblockId)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$sTableID = $hlblock['TABLE_NAME'];

$lAdmin = new CAdminList($sTableID, new CAdminSorting($sTableID, 'NAME', 'DESC'));

/**
 * Сортировка
 */
$by = 'UF_NAME';
$order = 'ASC';

if (!empty($_REQUEST['by']))
	$by = htmlspecialcharsbx($_REQUEST['by']);

if (!empty($_REQUEST['order']))
	$order = htmlspecialcharsbx($_REQUEST['order']);

/**
 * Подготовка фильтра
 */
$filter = array();
if (!empty($_REQUEST['find']))
    $filter['=%UF_NAME'] = "%" . htmlspecialcharsbx($_REQUEST['find']) . "%";

if (!empty($_REQUEST['address']))
    $filter['=%UF_ADRESS'] = "%" . htmlspecialcharsbx($_REQUEST['address']) . "%";

$params = array(
	'order' => array(
		$by => $order
	),
	'select' => array(
		'ID',
		'UF_NAME',
		'UF_ADRESS'
	),
	'filter' => $filter
);

$rsData = $entity_data_class::getList($params);

$arHeaders = array(
	array(
		'id' => 'ID',
		'content' => 'ID',
		'sort' => 'ID',
		'default' => true
	),
	array(
		'id' => 'UF_NAME',
		'content' => 'Название',
		'sort' => 'UF_NAME',
		'default' => true
	),
	array(
		'id' => 'UF_ADRESS',
		'content' => 'Адрес',
		'sort' => 'UF_ADRESS',
		'default' => true
	),
);

$lAdmin->AddHeaders($arHeaders);
$lAdmin->AddAdminContextMenu(array(), false);

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

$lAdmin->NavText($rsData->GetNavPrint(''));

while ($element = $rsData->Fetch())
{
	$row = $lAdmin->AddRow($element['ID'], $element);

	$row->AddViewField('ID', $element['ID']);
	$row->AddViewField('UF_NAME', $element['UF_NAME']);
	$row->AddViewField('UF_ADRESS', $element['UF_ADRESS']);

	$row->AddActions(array(
		array(
			"DEFAULT" => "Y",
			"TEXT" => 'Выбрать',
			"ACTION"=>"javascript:SelEl('".htmlspecialcharsbx(CUtil::JSEscape($element['ID']))."', '".htmlspecialcharsbx(CUtil::JSEscape($element['UF_NAME'])). "')",
		),
	));
}
define('LANG_CHARSET', 'UTF-8');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php");
$lAdmin->CheckListMode();
?>
	<script type="text/javascript">
		function SelEl(id, name)
		{
			window.opener.document.getElementById('<?=$_REQUEST['code']?>').value = id;
			window.opener.getExaleadLibrary(id);
			window.close();
		}
	</script>
	<form method="GET" name="find_exalead_library_form" action="<?=$APPLICATION->GetCurPageParam('find='.$_REQUEST['find'], array('find'));?>">
		<?
		$arFindFields = Array(
			'find' => 'Название',
            'address' => 'Адрес'
		);

		$oFilter = new CAdminFilter($sTableID."_filter", $arFindFields);
		$oFilter->Begin();
		?>
		<tr>
			<td><b><?=$arFindFields['find']?>:</b></td>
			<td><input type="text" name="find" value="<?=htmlspecialcharsbx($_REQUEST['find'])?>" size="47"></td>
		</tr>
        <tr>
            <td><b><?=$arFindFields['address']?>:</b></td>
            <td><input type="text" name="address" value="<?=htmlspecialcharsbx($_REQUEST['address'])?>" size="47"></td>
        </tr>
		<?
		$oFilter->Buttons(
			array(
				"table_id"=>$sTableID,
				"url"=>$APPLICATION->GetCurPage().'?find=' . htmlspecialcharsbx($_REQUEST['find']) . '&code=' . $_REQUEST['code'] . '&order=' . $_REQUEST['order'],
				"form"=>"find_exalead_library_form"
			)
		);
		$oFilter->End();
		?>
		<input type='hidden' name='code' value='<?=$_REQUEST['code']?>'>
		<input type='hidden' name='order' value='<?=$_REQUEST['order']?>'>
	</form>
<?
$lAdmin->DisplayList();
echo ShowError($strWarning);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");
?>
