<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if($_REQUEST['mode'] == 'settings')
    return;
?>
<?#$APPLICATION->AddHeadString('<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />',true)?>
<?$APPLICATION->AddHeadString('<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" type="text/css" />',true)?>
<?$APPLICATION->AddHeadString('<link rel="stylesheet" href="'.$templateFolder.'/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css">',true)?>
<?	\CUtil::InitJSCore(array("jquery"));?>
<?$APPLICATION->AddHeadString('<script>window.jQuery || document.write(\'<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">\x3C/script>\')</script>',true)?>
<?$APPLICATION->AddHeadString('<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>',true)?>

<?$APPLICATION->AddHeadString('<script type="text/javascript" src="'.$templateFolder.'/plupload/plupload.full.min.js"></script>',true)?>
<?$APPLICATION->AddHeadString('<script type="text/javascript" src="'.$templateFolder.'/plupload/jquery.ui.plupload/jquery.ui.plupload.js"></script>',true)?>
<?$APPLICATION->AddHeadString('<script type="text/javascript" src="'.$templateFolder.'/plupload/i18n/ru.js"></script>',true)?>

<?$APPLICATION->AddHeadString('<script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>',true)?>

<style type="text/css">
	.gallery-list{
		height: 300px;
		overflow: auto;
	}
	.gallery-list ul{
		padding: 0px;
		margin: 0px;
	}
	.gallery-list ul li{
		float:left;
		list-style: none;
		margin: 5px;
		position: relative;
	}
	.gallery-list ul li img{
		height: 115px;
		border:1px solid #e6e6e6;
	}
	.gallery-list-controll{
		margin-top: 5px;
	}
	.gallery-list-controll span{
		margin-right: 5px;	
	}
	.gallery-list ul li .img-controll{
		position: absolute;
		top:95px;
	}
	.gallery-list ul li .img-controll span{
		margin-right: 5px;
	}
	.gallery-list ul li .img-controll a{
		color:white;
		text-decoration: none;
	}
	.gallery-list-controll>div{
		margin-top:5px;
	}
	#dialog_delete div{
		text-align: center;
		margin:5px;

	}
	#dialog_delete .img img
	{
		border:1px solid #e6e6e6;
	}
	.hide
	{
		display: none;
	}

	#dialog_edit textarea
	{
		resize:none;
	}
</style>

<script type="text/javascript">
	/* Initialize the widget when the DOM is ready */
	$(function() {
		/*	
		* Убираем левую колонку
		*/
		<?
			if(!empty($arParams['PROPERTY_ID']))
			{
			?>
			var parent_tr = $('#uploader').closest('tr#tr_PROPERTY_<?=$arParams['PROPERTY_ID']?>');
			$('td', parent_tr)[0].remove();
			$('td', parent_tr).attr('width','100%')
			$('td', parent_tr).attr('colspan','2')
			<?
			}
		?>
		/*
		Формируем табы
		*/
		$( "#tabs_uploader" ).tabs();

		/*		
		Загрузяик файлов картинок
		*/
		function initPlupload(){
			$("#uploader").plupload({
				/* General settings */
				runtimes : 'html5,flash,html4',
				url : "<?=$templateFolder?>/plupload/upload.php",

				multipart_params: {
					SectionGalleryIDValue 	: $('#SectionGalleryIDValue').val(),
					IBLOCK_GALLERY_ID 		: <?=$arParams['IBLOCK_ID']?>,
					ID 						: <?=$arParams['ID']?>,
					WIDTH 					: <?=$arParams['WIDTH']?>,
					HEIGHT 					: <?=$arParams['HEIGHT']?>,
					MAX_WIDTH_ORIGINAL 		: <?=$arParams['MAX_WIDTH_ORIGINAL']?>,
					MAX_HEIGHT_ORIGINAL 	: <?=$arParams['MAX_HEIGHT_ORIGINAL']?>
				},

				/* Maximum file size */
				max_file_size : '10mb',

				chunk_size: '1mb',

				/* Resize images on clientside if we can */
				resize : {
					width : <?=$arParams['MAX_WIDTH_ORIGINAL']?>,
					height : <?=$arParams['MAX_HEIGHT_ORIGINAL']?>,
					quality : 99,
					crop: true /* crop to exact dimensions */
				},


				/* Specify what files to browse for */
				filters : [
					{title : "Image files", extensions : "<?=$arParams['FILE_EXTENSION']?>"},
				],

				/* Rename files by clicking on their titles */
				rename: true,

				unique_names: true,
				/* Sort files */
				sortable: true,

				/* Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that) */
				dragdrop: true,

				/* Views to activate */
				views: {
					list: true,
					thumbs: true, /* Show thumbs */
					active: 'thumbs'
				},

				/*
				Добавляем кастомную кнопку - Очистить список
				*/
				init: function(uploader, args) {
					$('#uploader_buttons .plupload_stop').after('<a id="uploader_hide" class="plupload_button plupload_hide ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" style="z-index: 1;"><span class="ui-button-icon-primary ui-icon ui-icon-circle-minus"></span><span class="ui-button-text">Очистить список</span></a>');

					$("#uploader_hide" ).click(function() {
						$("#uploader").plupload('destroy');
						initPlupload();
					});

					/*					
					перезагружаем список галерей, и выбираем вновь добавленную
					*/
					uploader.bind('FileUploaded', function(up, file, response) {
						var res = response.response;
						if (res) {
							var objResponse = JSON.parse(res);
							$('#SectionGalleryIDValue').val(objResponse.SectionGalleryIDValue);
							getSectionGallery();
							var pluploadObj = $('#uploader').plupload('getUploader');
							pluploadObj.settings.multipart_params.SectionGalleryIDValue = objResponse.SectionGalleryIDValue;
							getListGallery();
						}
					});
				},

				/* Flash settings */
				flash_swf_url : '//rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',
			});
		}

		/*
		Получаем спсиок разделов галереи
		*/
		function getSectionGallery(){
			BX.showWait('tabs_uploader');
			$.getJSON( "<?=$templateFolder?>/ajax/getSectionGallery.php?IBLOCK_GALLERY_ID=<?=$arParams['IBLOCK_ID']?>", function( json ) {

				$( "select#uploader_gallery option" ).remove();
				var find = false;
				var selected = '';

				$.each( json, function( id, value ) {
					if($('#SectionGalleryIDValue').val() ==  id){
						selected = ' selected';
					}
					if(<?=$arParams['ID']?> ==  value.XML_ID){
						find = true;
					}
					$( "select#uploader_gallery" ).append( '<option' + selected + ' value="' + id + '">' + value.NAME + '</option>' );
				});

				if(find == false){
					$( "select#uploader_gallery" ).prepend( '<option '+(selected == ''?'selected':'')+' value="0">Создать галерею</option>' );
				}
				BX.closeWait();
			});
		}

		/*
		Применяем выбранную галерею
		*/
		$( "select#uploader_gallery" ).change(function() {
			$('#SectionGalleryIDValue').val($(this).val());
			var pluploadObj = $('#uploader').plupload('getUploader');
			pluploadObj.settings.multipart_params.SectionGalleryIDValue = $(this).val();
			getListGallery();
		});

		/*		
		Загружаем фото из галереи
		*/
		function getListGallery(){
			BX.showWait('tabs_uploader');
			var sectionGallery = $('#SectionGalleryIDValue').val() * 1;
			if(sectionGallery <=0){
				$("#tabs_uploader").tabs( "option", "active", 1 );
				$(".gallery-list ul li").remove();
				$(".gallery-list ul").append( '<li style="margin-top:20px;">Загрузите фотографии</li>');
				return false;
			}
			$( ".gallery-list ul li" ).remove();
			$.getJSON("<?=$templateFolder?>/ajax/getListGallery.php?IBLOCK_GALLERY_ID=<?=$arParams['IBLOCK_ID']?>&SECTION_ID=" + sectionGallery, function( json ) {
				$.each( json, function( id, value ) {
					if(value.PREVIEW_TEXT == null)
						value.PREVIEW_TEXT = '';
					var licontent = 
					'<li data-detail_picture="'+value.DETAIL_PICTURE+'" id="'+value.ID+'">'+
					'<img class="img" src="'+value.PREVIEW_PICTURE+'"/>'+
					'<div class="img-controll">'+
					'<span><input title="Выбрать" type="checkbox" name="gimg[]"></span>'+
					'<span><a title="Редактировать" class="edit" href="#">E</a></span>'+
					'<span><a title="Удалить" class="delete" href="#">D</a></span>'+					
					'<span class="hide fields">'+
					'<textarea style="width: 100%; height: 200px;" name="DETAIL_GALLERY_PREVIEW_TEXT">'+value.PREVIEW_TEXT+'</textarea>'+
					'</span>'+
					'</div>'+
					'</li>';
					$( ".gallery-list ul" ).append( licontent );
					BX.closeWait();
				});
			});
		}

		/*		
		Удаление картинки
		*/
		$( "#dialog_delete" ).dialog({ autoOpen: false });
		$( ".gallery-list ul li .delete" ).live( "click", function(ob) {
			var liimg = $(this).closest('li');

			$( "#dialog_delete .img" ).html('<img src="'+$(liimg).find('.img').attr('src')+'"/>');

			var button = this;
			$( "#dialog_delete" ).dialog({ 
				buttons: [ { text: "Удалить", click: function() {
					BX.showWait('tabs_uploader');
					$.get("<?=$templateFolder?>/ajax/deleteImg.php?<?=bitrix_sessid_get()?>", { ELEMENT_ID: $(liimg).attr('id')}, function() { BX.closeWait(); });
					$(this).dialog( "close" ); 
					$(liimg).remove();
				}} ], 
				modal: true, 
				resizable: false,
				position: { my: "center", at: "center", of: button },
				closeText: "Закрыть",
				draggable: false
			});
			$( "#dialog_delete" ).dialog( "open" );
			return false;
		});

		/*		
		Редактирование описания картинки
		*/
		$( "#dialog_edit" ).dialog({ autoOpen: false });
		$( ".gallery-list ul li .edit" ).live( "click", function(ob) {


			var liimg = $(this).closest('li');

			var edit_html = '<?=bitrix_sessid_post()?><input type="hidden" name="ELEMENT_ID" value="'+$(liimg).attr('id')+'"/>'+$(liimg).find('.fields').html();
			$("#dialog_edit").html('<form>'+edit_html+'</form>');

			var button = this;
			$( "#dialog_edit" ).dialog({ 
				buttons: [ { text: "Сохранить", click: function() {
					BX.showWait('tabs_uploader');
					$.post( "<?=$templateFolder?>/ajax/editImg.php", $("#dialog_edit").find('form').serialize(), function() { BX.closeWait(); });
					getListGallery();
					$(this).dialog("close");

				}} ], 
				modal: true, 
				resizable: false,
				position: { my: "center", at: "center", of: button },
				closeText: "Закрыть",
				draggable: false,
				width: 400
			});
			$( "#dialog_edit" ).dialog( "open" );
			return false;
		});

		initPlupload();
		getSectionGallery();
		getListGallery();

		var galsettings = $('#SectionGalleryIDDesc').val();
		if(galsettings.length > 0){
			$.each( eval(galsettings), function( id, value ) {
				$('[name='+value.name+']',$('.gallery-list-controll')).val(value.value);
			});
		}
	});

	/*
	Выбрать все картинк	
	*/
	function gallerySelectAll(){
		$( ".gallery-list ul input[type='checkbox']" ).prop('checked',true);
	}

	/*
	Снять выбор со всех картинок	
	*/
	function galleryDeSelectAll(){
		$( ".gallery-list ul input[type='checkbox']" ).prop('checked',false);
	}

	/*	
	Подготовить код для вставки в визуальный редактор
	*/
	function galleryInsert(){
		var galleryHtml = '';
		var firstImg = '';
		var width = $('input[name=gwidth]').val();
		var height = $('input[name=gheight]').val();
		var align = $('select[name=gallery-align]').val();
		var GParam = '{width:'+width+', height:'+height+', align:\''+align+'\', IBLOCK_GALLERY_ID:<?=$arParams['IBLOCK_ID']?>, SectionGalleryIDValue : '+$('#SectionGalleryIDValue').val()+' }';
		var arItems = new Array();

		$( ".gallery-list ul input:checked" ).each(function() {
			var parent = $(this).closest('li');
			if(firstImg.length <= 0)
				firstImg = parent.data('detail_picture');

			arItems.push(parent.attr('id'));
		});

		if(arItems.length > 0)
		{
			var GalSettins = $('.gallery-list-controll :input').serializeArray();
			$('#SectionGalleryIDDesc').val(JSON.stringify(GalSettins));

			galleryHtml = '<img class="gallery-detail-object" src="'+firstImg+'" align="'+align+'" width="'+width+'" rel="'+GParam+'" alt="{'+arItems+'}"/>'
			insertwf(galleryHtml);
		}
		else
		{
			alert('Выберите фото');
		}
	}

	/*	
	Вставить код в виз редактор
	*/
	function insertwf(text){
		<?
			if(COption::GetOptionString("fileman", "use_editor_3") == 'Y'){
			?>
			/* new editor */
			window.BXHtmlEditor.Get('DETAIL_TEXT').selection.InsertHTML(text);
			<?
			}
			else{
			?>
			GLOBAL_pMainObj['DETAIL_TEXT'].insertHTML(text);
			<?
			}
		?>
	}

</script>

<input type="hidden" id="SectionGalleryIDValue" name="<?=$arParams['HTMLControlName']?>" value="<?=intval($arParams['HTMLControlValue'])?>"/>
<input type="hidden" id="SectionGalleryIDDesc" name="<?=$arParams['HTMLControlNameDesc']?>" value='<?=trim($arParams['HTMLControlDesc'])?>'/>

<div id="dialog_delete" class="hide" title="Удалить картинку">
	<div>Вы действительно хотите удалить файл картинки?</div>
	<div class="img"></div>
</div>
<div id="dialog_edit" class="hide" title="Редактировать описание картинки"></div>

<div id="tabs_uploader">
	<ul>
		<li><a href="#tabs_uploader-1">Фото</a></li>
		<li><a href="#tabs_uploader-2">Загрузить</a></li>
	</ul>
	<div id="tabs_uploader-1">
		<div class="gallery-list-sections">
			<select name="uploader_gallery" id="uploader_gallery">
			</select>
		</div>
		<div class="gallery-list">
			<ul></ul>
		</div>
		<div class="gallery-list-controll">

			<div>
				<?
					if($arParams['SHOW_ALIGN'] == 'Y'){
					?>
					<span>
						Выравнивание: 
						<select name="gallery-align">
							<option value="">---</option>
							<option value="left">Влево</option>
							<option value="right">Вправо</option>
							<option value="center">По центру</option>
						</select>
					</span>
					<?
					}
					else
					{
					?>
					<input name="gallery-align" type="hidden" value=""/>
					<?
					}
				?>
				<span>
					<?
						if($arParams['SHOW_WIDTH'] == 'Y'){
						?>
						Ширина: <input name="gwidth" size="4" value="<?=$arParams['WIDTH']?>"/>
						<?
						}
						else
						{
						?>
						<input name="gwidth" type="hidden" size="4" value="<?=$arParams['WIDTH']?>"/>
						<?
						}
					?> 
					<?
						if($arParams['SHOW_HEIGHT'] == 'Y'){
						?>
						Высота: <input name="gheight" size="4" value="<?=$arParams['HEIGHT']?>"/>
						<?
						}
						else
						{
						?>
						<input name="gheight" type="hidden" size="4" value="<?=$arParams['HEIGHT']?>"/>
						<?
						}
					?> 
				</span>
			</div>
			<div>
				<span><input type="button" name="gSelect" onclick="galleryInsert();" value="вставить"/></span>
				<span><input type="button" name="gSelectAll" onclick="gallerySelectAll();" value="выбрать все"/></span>
				<span><input type="button" name="gSelectAll" onclick="galleryDeSelectAll();" value="отменить выбор"/></span>
			</div>

		</div>
	</div>
	<div id="tabs_uploader-2">
		<div id="uploader">
			<p>Your browser doesn't have Flash or HTML5 support.</p>
		</div>
	</div>
</div>

