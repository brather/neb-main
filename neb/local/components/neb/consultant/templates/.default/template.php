<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if(SITE_HOST === $_SERVER['SERVER_NAME']) {?>
	<div id="script_ad" style="display:none;">
		<script async data-main='https://xn--80atdhccwjcf4h.xn--90ax2c.xn--p1ai:8443/js/data-main.js' extsrc='https://xn--80atdhccwjcf4h.xn--90ax2c.xn--p1ai:8443/js/dependencies/requirejs/require.js'></script>  
	</div>
	<script type="text/javascript">
		$(function() {
		    $("body").append("<link rel='stylesheet' href='https://xn--80atdhccwjcf4h.xn--90ax2c.xn--p1ai:8443/styles/importer.css'>");
		   	$('#script_ad').show();
		});
	</script>
<?} else { ?>
	<div id="script_ad" class="script_ad" style="display:none;">
		<script async data-main='https://84.47.187.73:4247/js/data-main.js' extsrc='https://84.47.187.73:4247/js/dependencies/requirejs/require.js'></script> 
	</div>
	<script type="text/javascript">
		$(function() {
		    $("body").append("<link rel='stylesheet' href='https://84.47.187.73:4247/styles/importer.css'>");
		   	$('#script_ad').show();
		});
	</script> 
<?}
?>

<?if($arResult["IS_ADMIN"]):?>
	<script type="text/javascript">

		document.addEventListener('ChatLoaded', function() {
			Chat.startConsultant({
				id: "<?=urlencode($arResult["USER"]["ID"])?>",
				fio: "<?=urlencode($arResult["USER"]["FULL_NAME"])?>",
				token: "<?=urlencode($arResult["USER"]["UF_TOKEN"])?>"
			});
		}, false);

	</script>
<?else:?>
	<script type="text/javascript">

	if(document.addEventListener) {
		document.addEventListener('ChatLoaded', function() {
		Chat.start({
			id: "<?=$arResult["USER"]["ID"]?>",
			fio: "<?=urlencode($arResult["USER"]["FULL_NAME"])?>",
			themeUrl: "<?=urlencode($arResult["URL"])?>",
			topicId: "<?=urlencode($arResult["TITLE"])?>",
			priority: "<?=$arResult["STATUS"]?>" 
		});
		}, false);
	}

</script>


<?endif;?>