<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/rest_api/favorites/#",
		"RULE" => "",
		"ID" => "rest:favorites",
		"PATH" => "/rest_api/favorites/index.php",
	),
	array(
		"CONDITION" => "#^/rest_api/bookmark/#",
		"RULE" => "",
		"ID" => "rest:bookmark",
		"PATH" => "/rest_api/bookmark/index.php",
	),
	array(
		"CONDITION" => "#^/rest_api/notes/#",
		"RULE" => "",
		"ID" => "rest:notes",
		"PATH" => "/rest_api/notes/index.php",
	),
	array(
		"CONDITION" => "#^/rest_api/library-statistic/export/([A-Z_]+)/#",
		"RULE"      => "FIELD_NAME=$1",
		"ID"        => "rest:library-statistic",
		"PATH"      => "/rest_api/library-statistic/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "neb:news",
		"PATH" => "/news/index.php",
	),
    array(
        "CONDITION" => "#^/for-libraries/#",
        "RULE" => "",
        "ID" => "neb:for-libraries",
        "PATH" => "/for-libraries/index.php",
    ),
	array(
		"CONDITION" => "#^/registration/#",
		"RULE" => "",
		"ID" => "neb:registration",
		"PATH" => "/registration/index.php",
	),
    array(
        "CONDITION" => "#^/special/registration/#",
        "RULE" => "",
        "ID" => "neb:registration",
        "PATH" => "/special/registration/index.php",
    ),
	array(
		"CONDITION" => "#^/collections/#",
		"RULE" => "",
		"ID" => "neb:collections",
		"PATH" => "/collections/index.php",
	),
    array(
        "CONDITION" => "#^/special/collections/#",
        "RULE" => "",
        "ID" => "neb:collections",
        "PATH" => "/special/collections/index.php",
    ),
	array(
		"CONDITION" => "#^/en/forum/#",
		"RULE" => "",
		"ID" => "bitrix:forum",
		"PATH" => "/en/forum/index.php",
	),
	array(
		"CONDITION" => "#^/profile/#",
		"RULE" => "",
		"ID" => "neb:profile",
		"PATH" => "/profile/index.php",
	),	array(
		"CONDITION" => "#^/special/profile/#",
		"RULE" => "",
		"ID" => "neb:profile",
		"PATH" => "/special/profile/index.php",
	),
	array(
		"CONDITION" => "#^/library/#",
		"RULE" => "",
		"ID" => "neb:library",
		"PATH" => "/library/index.php",
	),
	array(
		"CONDITION" => "#^/special/library/#",
		"RULE" => "",
		"ID" => "neb:library",
		"PATH" => "/special/library/index.php",
	),
	array(
		"CONDITION" => "#^/blind/catalog/#",
		"RULE" => "",
		"ID" => "exalead:search.page.detail",
		"PATH" => "/blind/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "exalead:search.page.detail",
		"PATH" => "/catalog/index.php",
	),
    array(
        "CONDITION" => "#^/special/catalog/#",
        "RULE" => "",
        "ID" => "exalead:search.page.detail",
        "PATH" => "/special/catalog/index.php",
    ),
	array(
		"CONDITION" => "#^/forum/#",
		"RULE" => "",
		"ID" => "bitrix:forum",
		"PATH" => "/forum/index.php",
	),
	array(
		"CONDITION" => "#^/special/forum/#",
		"RULE" => "",
		"ID" => "bitrix:forum",
		"PATH" => "/special/forum/index.php",
	),
	array(
		"CONDITION" => "#^/sso/#",
		"RULE" => "",
		"ID" => "neb:sso",
		"PATH" => "/sso/index.php",
	),
);

?>