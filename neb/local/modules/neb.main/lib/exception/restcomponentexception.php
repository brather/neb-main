<?php
namespace Neb\Main\Exception;

/**
 * User: agolodkov
 * Date: 01.10.2015
 * Time: 15:59
 */
class RestComponentException extends \Exception
{
    const DEFAULT_CODE = 500;

    /**
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(
        $message = "",
        $code = 0,
        \Exception $previous = null
    ) {
        $allowedCodes = array(
            400,
            403,
            404,
        );
        $allowedCodes = array_flip($allowedCodes);
        if (!isset($allowedCodes[$code])) {
            $code = static::DEFAULT_CODE;
        }
        parent::__construct($message, $code, $previous);
    }
}