<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

global $APPLICATION;

/**
 * @var array $arResult
 * @var array $arParams
 */
?>

<a class="b-btcreateuser b-btcreate btn btn-primary" href="<?=$arParams['USER_ADD']?>">
    <?=Loc::getMessage('USER_VERIFICATION_LIST_USER_ADD');?>
</a>

<a href="<?=$arParams['TICKET_ADD']?>" type="button" class="btn btn-primary">
    <?=Loc::getMessage('USER_VERIFICATION_LIST_ADD');?>
</a>
<br /><br />

<div class="b-readersearch clearfix">
    <form>
        <div class="row">
            <div class="col-xs-4">
                <input type="text" name="READERS[FIO]" class="form-control" value="<?=$arParams['READERS']['FIO']?>"
                       placeholder="<?= Loc::getMessage('USER_VERIFICATION_LIST_FULLNAME'); ?>" />
            </div>
            <div class="col-xs-4">
                <input type="text" name="READERS[EMAIL]" class="form-control" value="<?=$arParams['READERS']['EMAIL']?>"
                       placeholder="<?= Loc::getMessage('USER_VERIFICATION_LIST_LOGIN'); ?>" />
            </div>
            <div class="col-xs-2">
                <input type="text" name="READERS[ECHB]" class="form-control" value="<?=$arParams['READERS']['ECHB']?>"
                       placeholder="<?= Loc::getMessage('USER_VERIFICATION_LIST_EECHB'); ?>" />
            </div>
            <div class="col-xs-2">
                <input type="text" name="READERS[NUMBER]" class="form-control" value="<?=$arParams['READERS']['NUMBER']?>"
                       placeholder="<?= Loc::getMessage('USER_VERIFICATION_LIST_NUMBER'); ?>" />
            </div>
            <div class="col-xs-4">
                <select name="READERS[STATUS]" style="width:200px;">
                    <option value="Y"<? if ($arParams['READERS']['STATUS'] == 'Y'): ?> selected="selected"<? endif; ?>>
                        <?= Loc::getMessage('USER_VERIFICATION_LIST_VER'); ?>
                    </option>
                    <option value="N"<? if ($arParams['READERS']['STATUS'] == 'N'): ?> selected="selected"<? endif; ?>>
                        <?= Loc::getMessage('USER_VERIFICATION_LIST_NOT_VER'); ?>
                    </option>
                </select>
            </div>
            <div class="col-xs-4">
                <select name="READERS[ACTIVE]" style="width:200px;">
                    <option value="Y"<? if ($arParams['READERS']['ACTIVE'] == 'Y'): ?> selected="selected"<? endif; ?>>
                        Активные
                    </option>
                    <option value="N"<? if ($arParams['READERS']['ACTIVE'] == 'N'): ?> selected="selected"<? endif; ?>>
                        Не активные
                    </option>
                </select>
            </div>
            <div class="col-xs-2">
                <input type="submit" value="Искать" class="b-search_bth bbox btn btn-primary" />
            </div>
        </div>
    </form>
</div>

<div class="lk-table">
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:50%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind"><?= Loc::getMessage('USER_VERIFICATION_LIST_LOGIN'); ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('USER_VERIFICATION_LIST_FULLNAME'); ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('USER_VERIFICATION_LIST_NUMBER'); ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('USER_VERIFICATION_LIST_DATE'); ?></li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('ACTION'); ?></li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['ITEMS'] as $arItem) : ?>
            <ul class="lk-table__row">
                <li class="lk-table__col">
                    <a href="mailto:<?= $arItem['LOGIN'] ?>"><?= $arItem['LOGIN'] ?></a><br />
                    <? if ($arItem['ACTIVE'] == 'Y'): ?>
                        активный
                    <? else: ?>
                        не активный
                    <? endif; ?>
                </li>
                <li class="lk-table__col">
                    <?= $arItem['LAST_NAME'] ?> <?= $arItem['NAME'] ?> <?= $arItem['SECOND_NAME'] ?>
                </li>
                <li class="lk-table__col">
                    <?= $arItem['UF_NUMBER'] ?>
                    <? if (!empty($arItem['UF_NUM_ECHB'])):?>
                        /  <?= $arItem['UF_NUM_ECHB'] ?>
                    <? endif; ?>
                </li>
                <li class="lk-table__col">
                    <?= $arItem['UF_DATE'] ?>
                </li>
                <li class="lk-table__col">
                    <a href="<?=str_replace('#ID#', $arItem['ID'], $arParams['TICKET_EDIT']);?>"
                        type="button" class="btn btn-primary"><?=Loc::getMessage('USER_VERIFICATION_LIST_EDIT');?></a><br />
                    <a href="<?=str_replace('#ID#', $arItem['ID'], $arParams['TICKET_DELETE'])?>"
                       type="button" class="btn btn-primary"><?=Loc::getMessage('USER_VERIFICATION_LIST_DELETE');?></a><br />
                    <? if ($arItem['ACTIVE'] == 'Y'): ?>
                        <a href="<?=str_replace('#ID#', $arItem['UF_USER_ID'], $arParams['USER_DEACT'])?>"
                           type="button" class="btn btn-primary"><?=Loc::getMessage('USER_VERIFICATION_LIST_DEACT');?></a><br />
                    <? endif; ?>
                </li>
            </ul>
        <?endforeach;?>
    </section>
</div>

<?$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $arResult['NAV'],
        "SEF_MODE" => "Y",
    ),
    false
);?>