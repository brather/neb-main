<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>

<div class="b-addsearchmode clearfix">
    <div class="b-search_field right">
        <form method="get">
            <input type="text" name="qbook" class="b-search_fieldtb b-text"
                   id="asearch"
                   value="<?= htmlspecialcharsbx($_REQUEST['qbook']) ?>"
                   autocomplete="off">

            <input type="submit" class="btn btn-primary"
                   value="<?= GetMessage('LIBRARY_FUNDS_MANAGE_SEARCH'); ?>">
        </form>
    </div>
    <!-- /.b-search_field-->
</div>

<br>
<br>
<div class="lk-table">
    <div class="lk-table__column" style="width:55%"></div>
    <div class="lk-table__column" style="width:15%"></div>
    <div class="lk-table__column" style="width:15%"></div>
    <div class="lk-table__column" style="width:15%"></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind">
            <a <?= SortingExalead("document_authorsort") ?>><?= GetMessage(
                    'LIBRARY_FUNDS_MANAGE_AUTHOR'
                ); ?></a>
        </li>
        <li class="lk-table__header-kind">
            <a <?= SortingExalead("document_titlesort") ?>><?= GetMessage(
                    'LIBRARY_FUNDS_MANAGE_TITLE'
                ); ?></a>
        </li>
        <li class="lk-table__header-kind">
            <a <?= SortingExalead("document_dateindexsort") ?>><?= GetMessage(
                    'LIBRARY_FUNDS_MANAGE_DATE_ADDED'
                ); ?></a>
        </li>
        <li class="lk-table__header-kind">
            <th class="recycle_cell"><span class="b-recyclebin"><?= GetMessage(
                        'LIBRARY_FUNDS_MANAGE_REQUEST_DEL'
                    ); ?></span></th>
        </li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['ITEMS'] as $book) { ?>
            <ul class="lk-table__row">
                <li class="lk-table__col">
                    <?= $book['authorbook'] ?>
                </li>
                <li class="lk-table__col">
                    <?= $book['title'] ?>
                </li>
                <li class="lk-table__col">
                    <?= $DB->FormatDate(
                        $book['dateindex'], 'MM/DD/YYYY HH:MI:SS', 'DD.MM.YYYY'
                    ) ?>
                </li>
                <li class="lk-table__col">
                    <div class="rel plusico_wrap minus">
                        <? if (true === $book['isDeleted']) {
                            echo GetMessage(
                                'LIBRARY_FUNDS_DELETED'
                            );
                        } else { ?>
                            <a
                                href="#"
                                class="js-vedeniefondov-deletebook plus_ico lib rest-action delete-row"
                                id="<?= $book['Id'] ?>"
                                data-method="DELETE"
                                data-confirm-msg="<?= GetMessage(
                                    'LIBRARY_FUNDS_DELETE_CONFIRM'
                                ); ?>"
                                data-delete-msg="<?= GetMessage(
                                    'LIBRARY_FUNDS_DELETED'
                                ) ?>"
                                data-url="<?= $book['deleteUrl'] ?>"><?= GetMessage(
                                    'LIBRARY_FUNDS_MANAGE_DO_REQUEST_DEL'
                                ); ?></a>
                        <? } ?>
                    </div>
                </li>
            </ul>
        <? } ?>

    </section>
</div>
<?= htmlspecialcharsBack($arResult['STR_NAV']) ?>