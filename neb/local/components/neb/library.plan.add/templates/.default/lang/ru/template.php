<?php
$MESS["T_PLAN_ADD_DATE_FINISH_PLAN"] = "Плановая дата выполнения: ";
$MESS["T_PLAN_ADD_SUBMIT"] = "Добавить";
$MESS["T_PLAN_ADD_TITLE"] = "Добавить план оцифровки";

$MESS["T_PLAN_ADD_AJAX_DATE_CREATE"] = "Дата создания плана: ";
$MESS["T_PLAN_ADD_AJAX_TITLE_DATE_F_PLAN"] = "Планируемая дата оцифровки: ";
$MESS["T_PLAN_ADD_AJAX_STATUS"] = "Статус: ";
$MESS["T_PLAN_ADD_AJAX_CHANGE"] = "Изменить";
$MESS["T_PLAN_ADD_AJAX_DELETE"] = "Удалить";
$MESS["T_PLAN_ADD_AJAX_DELETE_CONFIRM"] = "Вы действительно решили удалить план?";
$MESS["T_PLAN_ADD_AJAX_STATUS_AGREEMENT"] = "На согласовании оператором НЭБ";
$MESS["T_PLAN_ADD_AJAX_RESTART_PAGE"] = "Для просмотра, добавления и редактирования изданий в плане, пожалуйста, обновите страницу";
$MESS["T_PLAN_ADD_AJAX_ERROR"] = "Ошибка добавления плана. Попробуйте позже.";