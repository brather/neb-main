<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/**
 * User: d-efremov
 * Date: 20.01.2017
 * Time: 11:00
 */

/**
 * Class LibraryPlan
 */
class LibraryPlan extends CBitrixComponent
{
    private $componentPage = '';

    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arDefaultUrlTemplates404 = array(
            "list"   => "/",
            "detail" => 'info#ELEMENT_ID#/',
            "add"    => 'add/',
            "reader" => 'add_from_reader/'
        );

        $arDefaultVariableAliases404 = array();

        $arDefaultVariableAliases = array();

        $arComponentVariables = array('SECTION', 'CODE');

        $SEF_FOLDER = "";
        $arUrlTemplates = array();

        if ($arParams["SEF_MODE"] == "Y") {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

            $componentPage = CComponentEngine::parseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

            if (strlen($componentPage) <= 0) $componentPage = "template";

            CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

            $SEF_FOLDER = $arParams["SEF_FOLDER"];

        } else {

            $arVariables = array();

            $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
            CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

            $componentPage = "";
            if (intval($arVariables["ELEMENT_ID"]) > 0) {
                $componentPage = "detail";
            } else {
                $componentPage = "list";
            }
        }

        $arParams['ELEMENT_ID'] = $arVariables['ELEMENT_ID'];

        $this->componentPage = $componentPage;

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->includeComponentTemplate($this->componentPage);
    }
}