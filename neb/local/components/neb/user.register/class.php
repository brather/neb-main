<?php

CBitrixComponent::includeComponentClass('neb:user.edit');

use \Bitrix\Main\Loader;

class UserRegisterComponent extends UserEditComponent
{
    /**
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params = array_replace_recursive(
            [
                'NEED_CONFIRM' => 'Y',
            ],
            $params
        );

        return parent::onPrepareComponentParams($params);
    }


    protected function _prepareResults()
    {
        // хардкод, но он нужен в частном случае
        if ($_REQUEST['success'] == 'Y') {
            $this->includeComponentTemplate($this->arParams['SUCCESS_TEMPLATE']);
            die();
        }

        $this->_prepareSocServices();

        return $this;
    }

    /**
     * Copy-paste from bitrix:system.auth.form
     */
    protected function _prepareSocServices()
    {
        global $USER, $APPLICATION;

        $this->arResult['AUTH_SERVICES'] = false;
        $this->arResult['CURRENT_SERVICE'] = false;

        if (!$USER->IsAuthorized() && Loader::includeModule('socialservices')) {
            
            $oAuthManager = new CSocServAuthManager();
            $arServices = $oAuthManager->GetActiveAuthServices(
                [
                    'BACKURL' => $APPLICATION->GetCurPageParam(
                        '', [
                            'login',
                            'login_form',
                            'logout',
                            'register',
                            'forgot_password',
                            'change_password',
                            'confirm_registration',
                            'confirm_code',
                            'confirm_user_id',
                            'logout_butt',
                            'auth_service_id',
                        ]
                    )
                ]
            );

            if (!empty($arServices)) {
                $this->arResult['AUTH_SERVICES'] = $arServices;
                if (isset($_REQUEST['auth_service_id'])
                    && $_REQUEST['auth_service_id'] <> ''
                    && isset($this->arResult['AUTH_SERVICES'][$_REQUEST['auth_service_id']])
                ) {
                    $this->arResult['CURRENT_SERVICE']
                        = $_REQUEST['auth_service_id'];
                    if (isset($_REQUEST['auth_service_error'])
                        && $_REQUEST['auth_service_error'] <> ''
                    ) {
                        $this->arResult['ERROR_MESSAGE']
                            = $oAuthManager->GetError(
                            $this->arResult['CURRENT_SERVICE'],
                            $_REQUEST['auth_service_error']
                        );
                    } elseif (!$oAuthManager->Authorize(
                        $_REQUEST['auth_service_id']
                    )
                    ) {
                        $ex = $APPLICATION->GetException();
                        if ($ex) {
                            $this->arResult['ERROR_MESSAGE'] = $ex->GetString();
                        }
                    }
                }
            }
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function _sendEmails()
    {
        parent::_sendEmails();
        if ('Y' === $this->arParams['NEED_CONFIRM']) {
            $user = $this->_getUser();
            $user->setDataField('USER_ID', $user->USER_ID);
            $user->sendEmails(['NEW_USER_CONFIRM']);
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function _applyRequestUserData()
    {
        parent::_applyRequestUserData();
        if ('Y' === $this->arParams['NEED_CONFIRM']) {
            $this->_getUser()->applyUserData(
                [
                    'ACTIVE'       => 'N',
                    'CONFIRM_CODE' => randString(8),
                ]
            );
        }

        return $this;
    }
}