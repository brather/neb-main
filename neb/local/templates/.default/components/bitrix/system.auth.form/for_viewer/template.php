<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('popup'));
?>
<div class="b-headernav_login right">
	<? if($arResult["FORM_TYPE"] == "login"): ?>
		<!--script src="/local/templates/.default/markup/js/libs/modernizr.min.js" type="text/javascript"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js" type="text/javascript"></script>
		<script src="/local/templates/.default/markup/js/libs/jquery.ui.min.js" type="text/javascript"></script-->
		
		<!--script src="/local/templates/.default/js/script.js" type="text/javascript"></script-->
		
		<!--script src="/local/templates/.default/markup/js/slick.min.js" type="text/javascript"></script-->
		<!--script src="/local/templates/.default/markup/js/plugins.js" type="text/javascript"></script-->
		<!--script src="/local/templates/.default/markup/js/jquery.knob.js" type="text/javascript"></script-->
		<!--script src="/local/templates/.default/markup/js/jquery.cookie.js" type="text/javascript"></script-->
		<!--script src="/local/templates/.default/markup/js/blind.js" type="text/javascript"></script-->
		<!--script src="/local/templates/.default/markup/js/script.js" type="text/javascript"></script-->
		
		<!--script src="<?=$templateFolder?>/script.js" type="text/javascript"></script-->
		<!-- он же -->
		<!--script src="/local/templates/.default/components/bitrix/system.auth.form/for_viewer/script.js" type="text/javascript"></script-->

		<script>
			/*$(document).on('click','.add-bookmark', function(e){
				e.preventDefault();
				$(this).uipopup();
				$('.commonpopup').css({'top':'9px', 'left':'80px'});
				$('.b_login_popup').css({'height':'auto'});
				$('.socials').css({'line-height':'57px'});
			});*/
			/*$(document).on('click','.save-book-link', function(e){
				e.preventDefault();
				$(this).uipopup();
				$('.commonpopup').css({'top':'9px', 'left':'80px'});
				$('.b_login_popup').css({'height':'auto'});
				$('.socials').css({'line-height':'57px'});
			});*/
		</script>

		<div class="b_login_popup popup clearfix" align="center" data-class="clone-login-popup">
			<div class="b_login_popup_in bbox clearfix">
				<h4>Необходимо авторизоваться</h4>
				<div class="b-loginform rel b-loginform-column">
                    <p class="change_pass_mess"><?=Loc::getMessage('AUTH_PASSWORD_CHANGED')?></p>
					<form 
						action="<?=$arResult["AUTH_URL"]?>" 
						class="b-form b-formlogin" 
						name="system_auth_form<?=$arResult["RND"]?>" 
						method="post" target="_top" 
						>

						<? if($arResult["BACKURL"] <> ''): ?>
							<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
						<? endif; ?>

						<? foreach ($arResult["POST"] as $key => $value): ?>
							<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
						<? endforeach; ?>

						<input type="hidden" name="AUTH_FORM" value="Y" />
						<input type="hidden" name="TYPE" value="AUTH" />
						<input type="hidden" name="USER_REMEMBER" value="Y" />

						<div class="fieldrow nowrap">
							<div class="fieldcell iblock login-mail">
								<label for="settings11"><?=Loc::getMessage('AUTH_MAIL')?></label>
								<div class="field validate">
									<input type="text" data-required="required" value="<?=$_REQUEST['USER_LOGIN']?>" id="settings11" data-maxlength="50" data-minlength="2" name="USER_LOGIN" class="input">
								</div>
							</div>
						</div>
						<div class="fieldrow nowrap">
							<div class="fieldcell iblock pass">
								<label for="settings10"><?=Loc::getMessage('AUTH_PASS')?></label>
								<div class="field validate">
									<input type="password" data-required="required" value="<?=$_REQUEST['USER_PASSWORD']?>" id="settings10" data-maxlength="30" data-minlength="2" name="USER_PASSWORD" class="input">
								</div>
							</div>
						</div>
						<em class="error_message">
						<? if (!empty($arResult['ERROR_MESSAGE']['MESSAGE'])
							|| (is_string($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']))):
							if(is_array($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']['MESSAGE']))
								$arResult['ERROR_MESSAGE'] = $arResult['ERROR_MESSAGE']['MESSAGE'];
								?>
								<?=$arResult['ERROR_MESSAGE'];?>
								<script type="text/javascript">
									$(function() {
										$('#login-link').click();
										$('.b_login_popup .error_message').show();
									});
								</script>
						<? endif; ?>
						</em>
						<div class="fieldrow nowrap fieldrowaction">
							<div class="fieldcell ">
								<div class="field clearfix">
									<button class="formbutton left" value="1" type="submit"><?=Loc::getMessage('AUTH_LOGIN_BUTTON')?></button>
									<a href="#" class="formlink right js_passrecovery"><?=Loc::getMessage('AUTH_FORGOT_PASS')?></a>
								</div>
							</div>
						</div>
					</form>

					<div class="b-passrform">
						<a href="#" class="closepopup"><?=Loc::getMessage('AUTH_CLOSE')?></a>
						<form action="/auth/forgot_password.php?forgot_password=yes" method="post" class="b-passrecoveryform b-form">
							<input type="hidden" name="AUTH_FORM" value="Y" />
							<input type="hidden" name="TYPE" value="SEND_PWD" />
							<input type="hidden" name="backurl" value="/" />
							<input type="hidden" name="send" value="1" />
							<input type="hidden" name="IS_AJAX" value="Y" />

							<div class="fieldrow nowrap">
								<div class="fieldcell iblock">
									<div class="b-warning hidden"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND')?></div>
									<label for="settings111"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC')?></label>
									<div class="field validate">
										<input type="text" data-required="required" value="" data-validate="email" id="settings111" data-maxlength="50" data-minlength="2" name="USER_EMAIL" class="input">
										<em class="error required"><?=Loc::getMessage('AUTH_PASS_RECOVERY_REQUIRED_FIELD')?></em>
									</div>
								</div>
								<div class="fieldrow nowrap fieldrowaction">
									<div class="fieldcell ">
										<div class="field clearfix">
											<button class="formbutton js-recovery-submit" value="1" type="submit"><?=Loc::getMessage('AUTH_PASS_RECOVERY_RECOVER')?></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<p class="b-passconfirm hidden" style="width:240px;"><?=Loc::getMessage('AUTH_PASS_RECOVERY_SUCCESS')?></p>
						<div class="close-window clear-form"></div>
					</div>
				</div>

				<div class="b-loginform-column">
					<div class="socials">
						<label><?=Loc::getMessage('AUTH_SOCSERVICES')?></label>
					    <div class="social-wrap">
					        <?foreach ($arResult['AUTH_SERVICES'] as $authService):
					        	if($authService['ID'] == 'Esia')
					        		continue;
				        		?>
					            <a href="javascript:void(0)" title="<?=  $authService['NAME'] ?>"
					            onclick="<?=$authService['AUTH_LINK']?>"
								class="<?=$authService['CSS_CLASS']?> <?=strtolower($authService['ICON'])?>"></a>
					        <? endforeach; ?>
					    </div>
					</div>
					<? if (!empty($arResult['AUTH_SERVICES']['Esia'])): ?>
						<div class='esia-login'>
							<a href="javascript:void(0)" title="Авторизация через портал госуслуг"
							   onclick="BX.util.popup('/WebServices/Esia.php?act=do_login', 800, 600)"
							   class="esialink esia clearfix">
								<img src="/local/templates/.default/markup/i/esia-logo-2.png" alt="ГосУслуги" />
								<label><?=Loc::getMessage('AUTH_ESIA')?></label>
							</a>
						</div>
					<? endif; ?>
					<div class='rgb-login'>
						<a href="javascript:void(0)" title="Авторизация для читателей РГБ"
						   onclick="BX.util.popup('/auth/rgb.php?redirect=1&show_errors=1', 800, 600);"
						   class="rgblink clearfix">
							<img src="/local/templates/.default/markup/i/rgb-logo.png" alt="РГБ" />
							<label><?=Loc::getMessage('AUTH_RGB')?></label>
						</a>
					</div>
				</div>
			</div>

			<div class="close-window clear-form"></div>
		</div>
	<? endif; ?>
</div>