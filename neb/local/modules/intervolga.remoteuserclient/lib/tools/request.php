<?namespace Intervolga\RemoteUserClient\Tools;

use \Bitrix\Main\SystemException;
use \Bitrix\Main\Localization\Loc;

class Request
{
	/**
	 * Remote method to call.
	 *
	 * @var string
	 */
	protected $sMethod = "";

	/**
	 * Remote method params.
	 *
	 * @var array
	 */
	protected $arParams = array();

	public function __construct($sMethod = "", $arParams = array())
	{
		if ($sMethod)
		{
			$this->setMethod($sMethod);
		}
		if ($arParams)
		{
			$this->setParams($arParams);
		}
	}

	public function setMethod($sMethod)
	{
		$this->sMethod = $sMethod;
	}

	public function setParams($arParams)
	{
		$this->arParams = $arParams;
	}

	public function getMethod()
	{
		return $this->sMethod;
	}

	public function getParams()
	{
		return $this->arParams;
	}

	/**
	 * Connects to SOAP server and calls method.
	 *
	 * SOAP request wrapper.
	 *
	 * @return \CSOAPResponse|null 0
	 * @throws SystemException
	 */
	public function run()
	{
		global $APPLICATION;

		$sServer = \COption::GetOptionString("intervolga.remoteuserclient", "server");
		$sPage = \COption::GetOptionString("intervolga.remoteuserclient", "page");
		$sLogin = \COption::GetOptionString("intervolga.remoteuserclient", "server_login");
		$sPassword = \COption::GetOptionString("intervolga.remoteuserclient", "server_password");

		// Check options
		if (!$sServer)
		{
			throw new SystemException(Loc::getMessage("intervolga.remoteuserclient.OPTION_SERVER_NOT_SET"));
		}
		if (!$sPage)
		{
			throw new SystemException(Loc::getMessage("intervolga.remoteuserclient.OPTION_PAGE_NOT_SET"));
		}
		if (!$sLogin)
		{
			throw new SystemException(Loc::getMessage("intervolga.remoteuserclient.OPTION_LOGIN_NOT_SET"));
		}
		if (!$sPassword)
		{
			throw new SystemException(Loc::getMessage("intervolga.remoteuserclient.OPTION_PASSWORD_NOT_SET"));
		}

		if (\Bitrix\Main\Loader::includeModule("webservice"))
		{
			// Prepare url
			$arParsedUrl = parse_url($sServer);
			if (!$arParsedUrl["port"] && in_array($arParsedUrl["scheme"], array("https", "ssl")))
			{
				$arParsedUrl["port"] = 443;
			}
			elseif (!$arParsedUrl["port"])
			{
				$arParsedUrl["port"] = 80;
			}

			if ($arParsedUrl["host"] && $arParsedUrl["port"] && $sPage)
			{
				// Set options
				$obClient = new SoapClient($arParsedUrl["host"], $sPage, $arParsedUrl["port"]);

				$obClient->setLogin($sLogin);
				$obClient->setPassword($sPassword);

				$obRequest = new \CSOAPRequest($this->getMethod(), $sServer);

				if ($this->getParams())
				{
					foreach ($this->getParams() as $sParamName => $paramValue)
					{
						$obRequest->addParameter($sParamName, $paramValue);
					}
				}

				// Execute request
				$APPLICATION->ResetException();
				$obResponse = $obClient->send($obRequest);

				if ($obBitrixException = $APPLICATION->getException())
				{
					$APPLICATION->resetException();
					throw new SystemException($obBitrixException->GetString());
				}

				if ($obResponse)
				{
					if ($obResponse->isFault())
					{
						if ($obResponse->faultCode() && is_numeric($obResponse->faultCode()))
						{
							throw new SystemException($obResponse->faultString(), intval($obResponse->faultCode()));
						}
						else
						{
							throw new SystemException($obResponse->faultString());
						}
					}
				}

				return $obResponse;
			}
		}
		else
		{
			throw new SystemException(Loc::getMessage("intervolga.remoteuserclient.WEBSERVICES_NOT_INSTALLED"));
		}

		return null;
	}
}