<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;
use \Neb\Main\Library\LibsTable;

Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle('Информация об ЭЧЗ');

/**
 * @var array $arResult
 */
$obCIBlockElement = new CIBlockElement();
$arWorkplace = $obCIBlockElement->GetList(
    [],
    [
        'IBLOCK_ID' => IBLOCK_ID_WORKPLACES,
        'ID'        => $arResult['USER']['UF_WCHZ'],
    ],
    false,
    false,
    [
        'ID',
        'NAME',
        'PROPERTY_LIBRARY',
        'PROPERTY_IP',
        'PROPERTY_AGREEMENT_NUMBER',
        //'PROPERTY_AGREEMENT_TIME',
    ]
)->Fetch();

$error = null;
try {
    if (!$arWorkplace)
        throw new Exception('ЭЧЗ не найден');

    if (is_string($arWorkplace['PROPERTY_IP_VALUE']))
        $arWorkplace['PROPERTY_IP_VALUE'] = unserialize($arWorkplace['PROPERTY_IP_VALUE']);

    if (!isset($arWorkplace['PROPERTY_IP_VALUE']) || !is_array($arWorkplace['PROPERTY_IP_VALUE']))
        $arWorkplace['PROPERTY_IP_VALUE'] = '';

    $library = false;
    if (!empty($arWorkplace['PROPERTY_LIBRARY_VALUE'])) {
        $library = LibsTable::getList([
            'filter' => [
                'ID' => $arWorkplace['PROPERTY_LIBRARY_VALUE'],
            ]
        ])->fetch();
    }

    if ($library)
        $arWorkplace['LIBRARY'] = $library;

} catch (Exception $e) {
    $error = $e->getMessage();
}
/* // енто больше не используется
$warning = '';
$expired = false;
$timestamp = MakeTimeStamp($arWorkplace['PROPERTY_AGREEMENT_TIME_VALUE']);
$timeRemains = $timestamp - time();
if(($timeRemains) < (86400 * 30)) {
    if(0 == ceil(($timeRemains) / 86400)) {
         $warning = 'Срок действия договора истекает сегодня';
    } elseif($timeRemains <= 0) {
        $warning = 'Срок действия договора истек';
    } else {
        $warning = 'Срок действия договора истекает через '.(ceil(($timestamp - time()) / 86400)).' дней';
    }
}*/
if ($error) {
    ShowError($error);
} else {
    ?>
    <h3><?= $arWorkplace['NAME'] ?></h3>
    <p>
        Номер договора: <b><?= $arWorkplace['PROPERTY_AGREEMENT_NUMBER_VALUE'] ?></b>
    </p>
    <? if(!empty($warning)): ?>
        <p style="color: red;">
            <?= $warning?>
        </p>
    <? endif; ?>
    <!--<p>
        Срок действия договора: <b><?= $arWorkplace['PROPERTY_AGREEMENT_TIME_VALUE'] ?></b>
    </p>-->
    <p>
        IP адреса:
        <ul>
            <?foreach ($arWorkplace['PROPERTY_IP_VALUE'] as $ip):?>
                <li><?= $ip?></li>
            <? endforeach; ?>
        </ul>
    </p>
    <?
    if(isset($arWorkplace['LIBRARY']['ID']))
        echo "<p> Адрес: {$arWorkplace['LIBRARY']['UF_ADRESS']}</p>";
}