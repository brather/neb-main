<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 * @var array $arParams
 */

?>
<div class="b-readersearch clearfix">
    <form>
        <input type="text" name="FIO" class=" b-text " value="<?= $arParams['FIO'] ?>"
               placeholder="<?= Loc::getMessage('FULL_NAME') ?>" style="width:250px;" />
        <input type="text" name="WORK_COMPANY" class=" b-text " value="<?= $arParams['WORK_COMPANY'] ?>"
               placeholder="<?= Loc::getMessage('WORK_COMPANY') ?>" style="width:250px;" />

        <select name="STATUS">
            <option value=""><?= Loc::getMessage('STATUS_ALL') ?></option>
            <option value="VERIFIED"<?= $arParams['STATUS'] == 'VERIFIED' ? ' selected="selected"' : '' ?>><?= Loc::getMessage('STATUS_VERIFIED') ?></option>
            <option value="REGISTERED"<?= $arParams['STATUS'] == 'REGISTERED' ? ' selected="selected"' : '' ?>><?= Loc::getMessage('STATUS_REGISTERED') ?></option>
        </select>

        <input type="submit" value="<?= Loc::getMessage('FIND') ?>" class="btn btn-primary" />
    </form>
</div>
<br>
<div class="lk-table">
    <div class="lk-table__column" style="width:60%"></div>
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:20%"></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind">
            <a <?= SortingExalead('NAME'); ?>><?= Loc::getMessage('NAME') ?></a></li>
        <!--<li class="lk-table__header-kind">
            <?= Loc::getMessage('RIGHTHOLDER_BOOKS_TITLE') ?>
        </li>-->
        <li class="lk-table__header-kind">
            <a <?= SortingExalead('DATE_REGISTER'); ?>><?= Loc::getMessage(
                    'DATE_REGISTER'
                ) ?></a>
        </li>
        <li class="lk-table__header-kind">Статус</li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('ACTION') ?></li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['items'] as $user) {
            $sFullName = trim($user['LAST_NAME'].' '.$user['NAME'].' '.$user['SECOND_NAME']);
            if (empty($sFullName))
                $sFullName = trim($user['UF_FIO_HEAD'] . ' ' . $user['UF_POSITION_HEAD']);
            ?>
            <ul class="lk-table__row">
                <li class="lk-table__col">
                    <?=$sFullName?>
                </li>
                <!--<li class="lk-table__col">
                    <a href="/profile/rightholder-books/?UID=<?= $user['ID'] ?>"
                       target="_blank">
                        <?= Loc::getMessage('RIGHTHOLDER_BOOKS') ?>
                    </a>
                </li>-->
                <li class="lk-table__col">
                    <?= $user['DATE_REGISTER'] ?>
                </li>
                <li class="lk-table__col">
                    <div class="checkbox-action checkbox">
                        <label>
                            <input
                                class="checkbox"
                                type="checkbox"
                                data-verify-toggler
                                data-verified-text="<?= GetMessage('READERS_LIST_VERIFIED'); ?>"
                                data-verify-text="<?= GetMessage('READERS_LIST_VERIFY'); ?>"
                                data-url="/rest_api/users/status/"
                                data-user-id="<?= $user['ID'] ?>"
                                <?= $arResult['statuses'][$user['UF_STATUS']]['XML_ID'] == 'user_status_verified' ? 'checked' : '' ?>
                            />
                            <span class="lbl"><?= $arResult['statuses'][$user['UF_STATUS']]['XML_ID'] == 'user_status_verified'
                                    ? GetMessage('READERS_LIST_VERIFIED')
                                    : GetMessage('READERS_LIST_VERIFY') ?></span>
                    </div>
                </li>
                <li class="lk-table__col">
                    <a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $user['ID'] ?>"
                       target="_blank">
                        <?= Loc::getMessage('EDIT') ?>
                    </a>
                </li>
            </ul>
        <? } ?>

    </section>
    <?= $arResult['nav'] ?>
</div>