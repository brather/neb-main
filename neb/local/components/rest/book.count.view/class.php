<?php
CBitrixComponent::includeComponentClass('rest:rest.server');


/**
 * Class RestBookCountView
 */
class RestBookCountView extends RestServer
{
    /**
     * @var array
     */
    protected $_params
        = array(
            'bookId'  => null,
            'session' => null,
        );

    /**
     * @param $component
     */
    public function __construct($component = null)
    {
        \CModule::IncludeModule('nota.exalead');
        parent::__construct($component);
    }


    /**
     * @return int
     * @throws Exception
     */
    public function putAction()
    {
        if (empty($this->_params['bookId'])) {
            throw new Exception('parameter "bookId" is required');
        }
        if (empty($this->_params['session'])) {
            throw new Exception('parameter "session" is required');
        }

        $this->arResult = \Nota\Exalead\LibraryBiblioCardTable::applyViewBook(
            $this->_params['bookId'],
            $this->_params['session']
        );

        return 200;
    }
}