<?php

/**
 * Импорт городов Библиотек
 */
class ImportCities
{
    /**
     * Обновление списка городов библиотек
     *
     * @param $arLocation
     * @return bool
     */
    public static function updateLocality($arLocation) {

        $adAddCity = $arDeleteCity = array();

        // преобразование в формат для поиска
        foreach ($arLocation as $k => $v) {
            $v = self::prepareLocality4Search($v);
            if (!empty($v))
                $arLocalitySearch[$v] = $k;
        }
        ksort($arLocalitySearch);
        if (empty($arLocalitySearch))
            return false;

        // получение гордов из БД
        $arCityFromDb = self::getNebLibsCity();

        // формирование массива на удаление из БД отсутствующих у библиотек местоположений
        foreach ($arCityFromDb as $arFields) {
            $arFields['SEARCH'] = self::prepareLocality4Search($arFields['UF_CITY_NAME']);
            if (empty( $arLocalitySearch[$arFields['SEARCH']] ))
                $arDeleteCity[] = $arFields['ID'];
            else
                $arFindCity[$arFields['SEARCH']] = $arFields['ID'];
        }

        // формирование массива на добавление отсутствующих в БД местоположений
        foreach ($arLocalitySearch as $sSearchLocality => $sLocality) {
            if (empty($arFindCity[$sSearchLocality]))
                $adAddCity[$sLocality] = $sLocality;
        }

        self::addNebLibsCity($adAddCity);
        self::deleteNebLibsCity($arDeleteCity);

        return true;
    }

    /**
     * Получает список городов библиотек-участниц НЭБ
     *
     * @return array
     */
    public static function getNebLibsCity() {

        $arResult = array();

        $libraryCityHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY_CITY)->fetch();
        $libraryCityEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryCityHLBlock)->getDataClass();
        $libraryCityData = $libraryCityEntity::getList(array(
            //"filter" => $arFilter,
            "order"  => array("ID"),
            "select" => array("ID", "UF_LIB_AREA", "UF_CITY_NAME", "UF_POS"),
        ));
        while ($arItem = $libraryCityData->Fetch()) {
            $arItem['SEARCH'] = self::prepareLocality4Search($arItem['UF_CITY_NAME']);
            $arResult[$arItem['ID']] = $arItem;
        }
        unset($libraryCityHLBlock, $libraryCityEntity, $libraryCityData, $arItem);

        return $arResult;
    }

    /**
     * Добавляет местоположения в h-блок NebLibsCity из массива
     *
     * @param $arCity
     * @return bool
     */
    private static function addNebLibsCity($arCity) {

        if (empty($arCity))
            return false;

        $libraryCityHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY_CITY)->fetch();
        $libraryCityEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryCityHLBlock)->getDataClass();

        if (is_integer($arCity)) {

            $libraryCityEntity::add(array(
                'UF_LIB_AREA'  => null,
                'UF_CITY_NAME' => $arCity
            ));

        } elseif (is_array($arCity)) {

            foreach ($arCity as $sCity) {
                $libraryCityEntity::add(array(
                    'UF_LIB_AREA'  => null,
                    'UF_CITY_NAME' => $sCity
                ));
            }
        }

        unset($libraryCityHLBlock, $libraryCityEntity);
    }

    /**
     * Удаляет города из h-блока NebLibsCity по списку идентификаторов
     *
     * @param $arCity
     * @return bool
     */
    private static function deleteNebLibsCity($arCity) {

        if (empty($arCity))
            return false;

        $libraryCityHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_LIBRARY_CITY)->fetch();
        $libraryCityEntity  = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($libraryCityHLBlock)->getDataClass();

        if (is_integer($arCity)) {

            $libraryCityEntity::delete($arCity);

        } elseif (is_array($arCity)) {

            foreach ($arCity as $iCity)
                $libraryCityEntity::delete($iCity);
        }

        unset($libraryCityHLBlock, $libraryCityEntity);
    }

    // ========================================== СЛУЖЕБНЫЕ МЕТОДЫ ===================================================

    /**
     * Получает название местоположения
     * 
     * @param $sName
     * @return mixed
     */
    public static function prepareLocality4Search($sName) {

        $arReplace = array(
            // с точками
            'пос.' => '', 'пгт.' => '', 'п.г.т.' => '', 'р.п.' => '', 'рп.' => '', 'ст.' => '', 'ст-ца' => '',
            'аул'  => '', 'пер.' => '', 'п/о.' => '', 'г.' => '',   'С. ' => '', 'с.' => '', 'C.' => '', 'c.' => '',
            'п.' => '', 'о.' => '',   'г ' => '',   ' г' => '', 'х.' => '', 'д.' => '', ' - ' => '-',
            // без точек
            'пгт ' => '', 'п/о ' => '', 'рп ' => '', 'пр-д' => '',
            // прочие
            'Мск' => 'Москва', 'СПб' => 'Санкт-Петербург',
        );

        return self::trimLastPoint(mb_strtolower(str_replace(
            array_keys($arReplace),
            array_values($arReplace),
            $sName
        )));
    }

    /**
     * Удаление точки в конце строки
     *
     * @param $str
     * @return string
     */
    private static function trimLastPoint($str) {
        $str = trim($str);
        if (substr($str, strlen($str)-1, 1) == '.')
            $str = substr($str, 0, strlen($str)-1);
        return $str;
    }
}