<?php
$MESS['COLLECTION_DETAIL_ADD_TO_MY_COL'] = 'Add to my collections';
$MESS['COLLECTION_DETAIL_REMOVE_FROM_MY_COL'] = 'Remove from my collections';
$MESS['COLLECTION_DETAIL_UPDATED'] = 'Updated';
$MESS['COLLECTION_DETAIL_AUTHOR'] = 'Author';
$MESS['COLLECTION_DETAIL_BOOK_5'] = 'books';
$MESS['COLLECTION_DETAIL_BOOK_1'] = 'book';
$MESS['COLLECTION_DETAIL_BOOK_2'] = 'books';
$MESS['COLLECTION_DETAIL_IN_COL'] = 'in collection';
$MESS['COLLECTION_DETAIL_ADDED_TO_FAV'] = 'Added to favourites';
$MESS['COLLECTION_DETAIL_VIEWS'] = 'Number of views';
$MESS['COLLECTION_DETAIL_SETTINGS'] = 'Settings';
$MESS['COLLECTION_DETAIL_REQ'] = "* - required field";
$MESS['COLLECTION_DETAIL_CREATING'] = "Making collection";
$MESS['COLLECTION_DETAIL_EDITING'] = "Editing a collection";
$MESS['COLLECTION_DETAIL_SHOW_NAME'] = "Show the title of the collection in the slider";
$MESS['COLLECTION_DETAIL_SAVE'] = "save";
$MESS['PROFILE_ML_PAGE'] = 'The page of the library<br/> in the NEB-portal';
$MESS['COLLECTION_SUB_SECTION_LIST'] = 'List of subsections ';
$MESS['COLLECTION_SUB_SECTION_EDIT'] = 'Edit ';
$MESS['COLLECTION_SUB_SECTION_DELETE'] = 'Delete ';

$MESS['COLLECTION_DETAIL_COLLECTION_NAME'] = "Title of collection: ";
$MESS['COLLECTION_DETAIL_ACTIVE'] = 'section is active';
$MESS['COLLECTION_DETAIL_SHOW_IN_SLIDER'] = 'show in slider';
$MESS['COLLECTION_DETAIL_SHOW_TITLE_IN_SLIDER'] = 'show title in slider';
$MESS['COLLECTION_PARENT_SECTION'] = 'Parent section: ';
$MESS['COLLECTION_DELETE_IMAGE_SECCTION'] = 'Delete image: ';
$MESS['COLLECTION_DELETE_IMAGE_SECCTION_CONFIRM'] = 'This operation delete image. Are you sure? ';
$MESS['COLLECTION_DETAIL_DESCRIPTION'] = "Description of the collection: ";
?>
