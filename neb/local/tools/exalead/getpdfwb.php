<?
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
define('RSL_WB', true);

function getmicrotime2()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$ptime = getmicrotime2();

#session_write_close();
#require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
#session_write_close();
#CModule::IncludeModule('nota.exalead');

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/tools.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/modules/nota.exalead/lib/rslviewer.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/modules/notaext/lib/writelog.php");

use \Bitrix\NotaExt\WriteLog,
    \Neb\Main\Helper\MainHelper,
    Nota\Exalead\RslViewer;

$book_id = trim($_REQUEST['book_id']);

$logF = new WriteLog($_SERVER['DOCUMENT_ROOT'] . '/pdfload.log');
#$logF->addLog($book_id.' start');


if (empty($book_id))
    $book_id = '01003741195';

$Viewer = new RslViewer($book_id);
/*$DocumentInfo = $Viewer->getDocumnetInfo();
if($DocumentInfo !== false){
$resources = $Viewer->getResources();
if(in_array('pdf', $resources))
{*/
//$size = $Viewer->getResourcesExtSize();

MainHelper::clearBuffer();
$Viewer->getResourcesExt();

/*}
else
{
die('Not found pdf resource');
}
}
else{
die('Error');
}*/

$logF->addLog($book_id . ' ' . round(getmicrotime2() - $ptime, 3));