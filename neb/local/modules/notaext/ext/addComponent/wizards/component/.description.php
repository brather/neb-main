<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arWizardDescription = Array(
	"NAME" => GetMessage("MASTER_COMP_TITLE"), 
	"DESCRIPTION" => GetMessage("MASTER_COMP_DESCR"), 
	"ICON" => "icon.png",
	"VERSION" => "1.0.0",
	"SORT" => "10",
	"DEPENDENCIES" => Array(
	),
	"STEPS" => Array("WelcomeStep", "NameStep", "ModuleStep", "TemplateStep", "InstallStep", "FinalStep", "CancelStep"),
);

?>