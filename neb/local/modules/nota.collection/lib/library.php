<?
namespace Nota\Collection;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class LibraryTable extends DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'neb_collection_library';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('NOTA_LIBRARY_ID'),
            ),

            'NAME' => array(
                'data_type' => 'string',
                'required' => true,
                'title' => Loc::getMessage('NOTA_LIBRARY_NAME'),
            ),
            'SIGNATURE' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('NOTA_LIBRARY_SIGNATURE_FIELD'),
            ),
            'POSTAL_ADDRESS' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('NOTA_LIBRARY_POSTAL_ADDRESS_FIELD'),
            ),
            'SCHEDULE' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('NOTA_LIBRARY_SCHEDULE_FIELD'),
            ),
            'PHONE' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('NOTA_LIBRARY_PHONE_FIELD'),
            ),
            'EMAIL' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('NOTA_LIBRARY_EMAIL_FIELD'),
            ),
            'SKYPE' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('NOTA_LIBRARY_SKYPE_FIELD'),
            ),
            'URL' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('NOTA_LIBRARY_URL_FIELD'),
            ),
            'DESCRIPTION' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('NOTA_LIBRARY_DESCRIPTION_FIELD'),
            ),
            'MAP' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('NOTA_LIBRARY_MAP_FIELD'),
            ),
            'TIMESTAMP_X' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('NOTA_LIBRARY_TIMESTAMP_X_FIELD'),
            ),
            'SORT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('NOTA_LIBRARY_SORT_FIELD'),
            ),
            'CODE' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('NOTA_LIBRARY_CODE_FIELD'),
            ),
            'LOGO' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('NOTA_LIBRARY_LOGO_FIELD'),
            ),
        );
    }
}
