<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application,
    \Bitrix\Highloadblock as HL;

class PlanAddComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';                                 // шаблон компонента
    private $idHLBlock = '';                                    // id HL блока
    private $dataHLBlock = '';                                  // класс HL блока
    private $arRequest = array();                               // массив с данными $_REQUEST
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        //$this->_includeModule();                            // Подключаем модули

        $this->_getRequest();                           // Получаем и обрабатываем данные REQUEST
        $this->_getIdLibraryCurrent();                  // Возвращаем id библиотеки текущего администратора

        $this->_getIdHLBlock();                         // Возвращаем id HL блока
        $this->_getDataClass();                         // Возвращаем обхект для работы с данными HL блока

        $this->includeComponentTemplate($this->page);   // Подключение шаблона
    }
    //endregion
    //region Возвращаем id библиотеки текущего администратора
    /**
     * Возвращаем данные текущего пользователя
     */
    private function _getIdLibraryCurrent() {
        if(!empty($this->arParams["UF_LIBRARY"])) {
            $this->arResult["UF_LIBRARY"] = $this->arParams["UF_LIBRARY"];
        } else {
            if(UGROUP_LIB_CODE_ADMIN === nebUser::getCurrent()->getRole()) {
                $arUser = nebUser::getCurrent()->getUser();
                $this->arResult["UF_LIBRARY"] = $arUser["UF_LIBRARY"];
            }
        }
    }
    //endregion
    //region Получение массива $_REQUEST (обертка D7)
    /**
     * Получение массива $_REQUEST (обертка D7)
     */
    private function _getRequest() {
        return $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }
    //endregion
    //region Получение id hl блока по его коду
    /**
     * Получение id hl блока по его коду
     */
    private function _getIdHLBlock() {
        if(!empty($this->arParams["HL_ID"])) {
            $this->idHLBlock = $this->arParams["HL_ID"];
        } else {
            $connection = Application::getConnection();
            $sql = "SELECT ID FROM b_hlblock_entity WHERE NAME='" . $this->arParams["HL_CODE"] . "';";
            $recordset = $connection->query($sql)->fetch();
            $this->idHLBlock = $recordset['ID'];
            $this->arResult['HL_ID'] = $this->idHLBlock;
        }
    }
    //endregion
    //region Возвращаем класс для работы с HL блоком
    /** Возвращаем класс для работы с HL блоком
     * @throws \Bitrix\Main\SystemException
     */
    function _getDataClass() {
        $hlblock = HL\HighloadBlockTable::getById($this->idHLBlock)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $this->dataHLBlock = $entity->getDataClass();
    }
    //endregion
}