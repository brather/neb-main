<?

$MESS ['MASTER_COMP_ERROR_TYPE'] = "Задайте тип компонента";
$MESS ['MASTER_COMP_ERROR_NS'] = "Задайте пространство имен";
$MESS ['MASTER_COMP_ERROR_NS_INVALID_CHARS'] = "Недопустимые символы в названии пространства имен: ";
$MESS ['MASTER_COMP_ERROR_COMP'] = "Задайте идентификатор";
$MESS ['MASTER_COMP_ERROR_COMP_INVALID_CHARS'] = "Недопустимые символы в идентификаторе компонента: ";
$MESS ['MASTER_COMP_ERROR_COMP_EXIST'] = "Компонент '#PATH#' уже существует";
$MESS ['MASTER_COMP_ERROR_COMP_NAME'] = "Задайте название";

$MESS ['MASTER_COMP_NEW_NS'] = "Новое &rarr;";

$MESS ['MASTER_COMP_STEP_WELCOME'] = "Мастер создания компонента";
$MESS ['MASTER_COMP_STEP_NAME'] = "Базовые настройки";
$MESS ['MASTER_COMP_STEP_COMPONENT'] = "Компонент";
$MESS ['MASTER_COMP_STEP_TEMPLATE'] = "Шаблон";
$MESS ['MASTER_COMP_STEP_INSTALL'] = "Проверка";
$MESS ['MASTER_COMP_STEP_FINAL'] = "Компонент успешно создан";
$MESS ['MASTER_COMP_STEP_CANCEL'] = "Работа мастера остановлена";

$MESS ['MASTER_COMP_NAMESPACE'] = "Пространство имен";
$MESS ['MASTER_COMP_NEW_NAMESPACE'] = "Новое пространство имен";
$MESS ['MASTER_COMP_TYPE'] = "Тип компонента";
$MESS ['MASTER_COMP_TYPE_SIMPLE'] = "Простой";
$MESS ['MASTER_COMP_TYPE_COMPLEX'] = "Комплексный";
$MESS ['MASTER_COMP_ID'] = "Идентификатор <small>(напр. users.list)</small>";
$MESS ['MASTER_COMP_NAME'] = "Название";
$MESS ['MASTER_COMP_DESCRIPTION'] = "Описание";
$MESS ['MASTER_COMP_ICON'] = "Иконка <small>(32&times;16 px, .gif)</small>";
$MESS ['MASTER_COMP_PATH_ID'] = "Категория в списке компонентов <small>(напр. my_project)</small>";

$MESS ['MASTER_COMP_SNIPPETS'] = "Опции компонента";
$MESS ['MASTER_COMP_LANG'] = "Создать языковые файлы компонента";
$MESS ['MASTER_COMP_CACHE'] = "Кеширование";
$MESS ['MASTER_COMP_IBLOCK'] = "Запрос списка элементов ИБ";
$MESS ['MASTER_COMP_404'] = "Установка 404 ошибки";
$MESS ['MASTER_COMP_TEMPLATE'] = "Подключить шаблон (будет создан шаблон)";
$MESS ['MASTER_COMP_TEMPLATE_OPTIONS'] = "Опции шаблона";
$MESS ['MASTER_COMP_TEMPLATE_LANG'] = "Создать языковые файлы шаблона";
$MESS ['MASTER_COMP_CSS'] = "Создать style.css";
$MESS ['MASTER_COMP_JS'] = "Создать script.js";
$MESS ['MASTER_COMP_EPILOG'] = "Создать component_epilog.php";

$MESS ['MASTER_COMP_INSTALL_PARAM'] = "Параметр";
$MESS ['MASTER_COMP_INSTALL_VALUE'] = "Значение";

$MESS ['MASTER_COMP_WELCOME_MESSAGE'] = "<p>Вас приветствует Мастер создания компонента.</p>
<p>Данный мастер позволяет создавать в системе необходимые файлы компонента, которые уже будут содержать типовые примеры кода.<br/>
Вы так же можете указать создавать ли шаблон для компонента, файлы стилей, языковые файлы и многое другое.</p>
";

$MESS ['MASTER_COMP_INSTALL_MESSAGE'] = "Проверьте правильность указанных вами параметров и нажмите кнопку \"Создать\".";

$MESS ['MASTER_COMP_NAME_MESSAGE'] = "Укажите базовые значения.";
$MESS ['MASTER_COMP_FINAL_MESSAGE'] = '<p><b>Готово!</b></p>';
$MESS ['MASTER_COMP_CANCELED_MESSAGE'] = "Работа мастера остановлена. Компонент не был создан.";

$MESS ['MASTER_COMP_CREATE'] = "Создать";
$MESS ['MASTER_COMP_CLOSE'] = "Закрыть";

?>