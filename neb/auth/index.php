<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

LocalRedirect(
    '/auth/login.php?_backUrl=' . urlencode($_SERVER["HTTP_REFERER"])
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");