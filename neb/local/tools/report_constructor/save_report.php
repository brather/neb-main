<?
/**
 * добавление / редактирование отчета
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!TblEntities::checkAccess()) {
    die('Permissions denied!');
}

if ($_REQUEST['add_report'] == 'Y'){
	$arResult['idReport'] = TblReport::newReport($_REQUEST);
}
// редактирование отчета
if ($_REQUEST['edit_report'] == 'Y' && $_REQUEST['report_id'] > 0) {
	$arResult['idReport'] = TblReport::editReport($_REQUEST);
}

echo "/local/tools/report_constructor/report.php?id=" . $arResult['idReport'];
