<?php
$MESS["FAQ_ADD_ELEMENT"] = "Add element";
$MESS["FAQ_EDIT_ELEMENT"] = "Edit element";
$MESS["FAQ_ADD_SECTION"] = "Add section";
$MESS["FAQ_EDIT_SECTION"] = "Edit section";
$MESS["FAQ_ADD_ITEM"] = "Add";
$MESS["FAQ_ADD"] = "Add";
$MESS["FAQ_EDIT"] = "Edit";