<?php
$APPLICATION->IncludeComponent(
    "neb:faq",
    "",
    Array(
        "IBLOCK_CODE" => "FAQ",
        "IBLOCK_TYPE" => "site_content",
        "ROOT_PATH" => "/profile/faq/",
        "MIN_COUNT_SYMBOL" => 5,
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => $arParams['SEF_FOLDER'] . str_replace('(.*)', '/', $arParams['SEF_URL_TEMPLATES']['faq']),
        "SEF_URL_TEMPLATES" => array(
            "list" => "/",
            "item" => "item/?id=#ID#",
            "section" => "section/?sect_id=#SECTION_ID#",
            "add_item" => "add_item/",
            "add_section" => "add_section/",
        )
    )
);?>