<?php
CBitrixComponent::includeComponentClass('rest:rest.server');

use \Bitrix\Main\Application,
    \Bitrix\Highloadblock as HL,
    \Bitrix\Main\Loader,
    Bitrix\NotaExt\Iblock\IblockTools;
Loader::includeModule('iblock');

/**
 * Class RestLibraryNebParty
 */
class RestPlanDigitization extends RestServer {
    /**
     * @var array
     */
    protected $_params = array(
            'book_id'           => null,
            'idparent'          => null,
            'comment'           => null,
            'actionType'        => null,
            'ID'                => null,
            'UF_DATE_FINISH'    => null,
            'UF_LIBRARY'        => null,
            'UF_REQUEST_STATUS' => null,
            'UF_BOOK_FILE_path' => null,
        );
    protected $_methodAllowGroups = ['postAction' => '*', 'putAction'  => '*'];
    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams() {
        $this->_params['idparent'] = (integer)$this->_params['idparent'];
        $this->_params['comment'] = trim($this->_params['comment']);
        if (is_array($this->_params['book_id'])) {
            $this->_params['book_id'] = array_filter($this->_params['book_id']);
        }
        $this->_params['ID'] = intval($this->_params['ID']);
        $this->_params['UF_LIBRARY'] = intval($this->_params['UF_LIBRARY']);
        $this->_params['UF_REQUEST_STATUS'] = intval($this->_params['UF_REQUEST_STATUS']);
        return $this;
    }
    /**
     * @return int
     * @throws Exception
     */
    public function postAction() {
        if (!isset($this->_user['ID'])) {
            throw new Exception('Не найден пользователь');
        }
        $requiredFields = ['book_id', 'comment'];
        foreach ($requiredFields as $fieldName) {
            if (empty($this->_params[$fieldName])) {
                throw new Exception(
                    "Не передан обязательный параметр '$fieldName'"
                );
            }
        }

        $this->arResult = ['data' => [],];
        NebMainHelper::includeModule('nota.exalead');
        NebMainHelper::includeModule('highloadblock');

        $idHLBlock = self::getHLBlockId(HIBLOCK_CODE_DIGITIZATION_PLAN);
        $entity_data_class = self::getHLBlockDataClass($idHLBlock);

        //$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_PLAN_DIGIT)->fetch();
        //$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        //$entity_data_class = $entity->getDataClass();

        $book = \Nota\Exalead\LibraryBiblioCardTable::getByFullSymbolicId($this->_params['book_id']);
        $libraryId = 0;
        $book = $book[0];
        if ($book['ALIS']) {
            $library = \Nota\Exalead\BiblioAlisTable::getById($book['ALIS'])->fetch();
            $libraryId = (integer)$library['Library'];
        }
        $checkParameters = [
            'filter' => [
                'UF_NEB_USER' => nebUser::getCurrent()->GetID(),
            ]
        ];
        $checkParameters['filter'][] = [
            'LOGIC' => 'OR',
            ['UF_REQUEST_STATUS' => null],
            ['!=UF_REQUEST_STATUS' => PlanDigitalization::getStatusValueId(
                PlanDigitalization::STATUS_CANCELED
            )],
        ];
        if (!empty($this->_params['idparent'])) {
            $checkParameters['filter'][] = [
                'LOGIC' => 'OR',
                ['UF_EXALEAD_BOOK' => $this->_params['book_id']],
                ['UF_EXALEAD_PARENT' => $this->_params['idparent']],
            ];
        } else {
            $checkParameters['filter']['UF_EXALEAD_BOOK']
                = $this->_params['book_id'];
        }

        $relatedOrders = PlanDigitalization::getRelatedOrders(
            $this->_params['book_id'],
            $this->_params['idparent'],
            $checkParameters
        );

        if (!empty($relatedOrders)) {
            throw new Exception(
                "Издание уже добавлено в план оцифровки(Номер заявки {$relatedOrders[0]['ID']})"
            );
        }
        $dt = new \Bitrix\Main\Type\DateTime();
        $dtPlus = new \Bitrix\Main\Type\DateTime();
        $dtPlus->add('+' . PLAN_DIGIT_PERIOD_FINISH . ' days');

        $currentUser = nebUser::getCurrent();
        $arFields = [
            'UF_LIBRARY'        => $libraryId,
            'UF_EXALEAD_BOOK'   => $this->_params['book_id'],
            'UF_DATE_ADD'       => $dt,
            'UF_DATE_FINISH'    => $dtPlus,
            'UF_EXALEAD_PARENT' => $this->_params['idparent'],
            'UF_COMMENT'        => $this->_params['comment'],
            'UF_NEB_USER'       => $this->_user['ID'],
            'UF_BOOK_AUTHOR'    => $book['Author'],
            'UF_BOOK_NAME'      => $book['Name'],
            'UF_ADD_TYPE'       =>
                UGROUP_LIB_CODE_ADMIN === $currentUser->getRole()
                    ? 'library'
                    : ('user' === $currentUser->getRole()
                    ? 'user'
                    : null),
        ];
        unset($checkParameters['filter']['UF_NEB_USER']);
        $relatedOrders = PlanDigitalization::getRelatedOrders(
            $this->_params['book_id'],
            $this->_params['idparent'],
            $checkParameters
        );
        if (isset($relatedOrders[0]) && $arData = $relatedOrders[0]) {
            $arFields['UF_DATE_ADD'] = $arData['UF_DATE_ADD'];
            $arFields['UF_DATE_FINISH'] = $arData['UF_DATE_FINISH'];
            $arFields['UF_REQUEST_STATUS'] = $arData['UF_REQUEST_STATUS'];
            if ((integer)$arFields['UF_LIBRARY'] !== (integer)$arData['UF_LIBRARY']) {
                $arFields['UF_LIBRARY'] = $arData['UF_LIBRARY'];
            }
            if ((integer)$arFields['UF_EXALEAD_PARENT'] !== (integer)$arData['UF_EXALEAD_PARENT']) {
                $arFields['UF_EXALEAD_PARENT'] = $arData['UF_EXALEAD_PARENT'];
            }
            if ($arFields['UF_EXALEAD_BOOK'] !== $arData['UF_EXALEAD_BOOK']) {
                $arFields['UF_EXALEAD_BOOK'] = $arData['UF_EXALEAD_BOOK'];
            }
            $arFields['UF_BOOK_FILE'] = $arData['UF_BOOK_FILE'];

            if (PlanDigitalization::getStatusValueId(PlanDigitalization::STATUS_DONE) == $arData['UF_REQUEST_STATUS']) {
                $this->arResult['data']['orderIsDone'] = true;
            }
        }


        $ID = $entity_data_class::add($arFields)->getId();
        if ($ID > 0) {
            \Neb\Main\PlanDigitizationLogTable::add(
                [
                    'ORDER_ID'    => $ID,
                    'USER_ID'     => $this->_user['ID'],
                    'COMMENT'     => $this->_params['comment'],
                    'STATUS_ID'   => PlanDigitalization::STATUS_ON_AGREEMENT,
                    'ACTION_NAME' => 'create',
                ]
            );

            \Neb\Main\UserDigitizationTable::add(
                array(
                    'ID'      => $ID,
                    'USER_ID' => $this->_user['ID'],
                )
            );

            $emails = [
                $this->_user['EMAIL'],
            ];
            $group = new CGroup();
            $groupData = $group->GetList(
                $by = [], $order = [], [
                    'STRING_ID' => UGROUP_OPERATOR_CODE,
                ]
            )->Fetch();
            if ($groupData) {
                $user = new CUser();
                $resultUsers = $user->GetList(
                    $by,
                    $order,
                    [
                        'GROUPS_ID' => [$groupData['ID']]
                    ]
                );
                while ($userData = $resultUsers->Fetch()) {
                    if (!empty($userData['EMAIL'])) {
                        $emails[] = $userData['EMAIL'];
                    }
                }
            }
            $event = new CEvent();
            foreach ($emails as $email) {
                $event->Send(
                    'DIGITIZATION_ORDER_ACTION',
                    SITE_ID,
                    [
                        'ID'        => $ID,
                        'EMAIL'     => $email,
                        'BOOK_NAME' => $book['Name'],
                        'BOOK_ID'   => $book['FullSymbolicId'],
                    ],
                    'Y',
                    PlanDigitalization::CREATE_EMAIL_TEMPLATE_ID
                );
            }
        } else {
            throw new Exception(
                'Не удалось добавить книгу в план. Попробуйте позднее.'
            );
        }

        $this->arResult['data']['id'] = $ID;

        return 200;
    }
    public function putAction() {
        if ($this->_params['ID'] <= 0) {
            throw new Exception('Не передан идентификатор заявки');
        }
        if (!isset($this->_user['ID'])) {
            throw new Exception('Не найден пользователь');
        }
        $this->_loadDigitizationOrder();
        if (!isset($this->arResult['ORDER']['ID'])) {
            throw new Exception("Заявка №{$this->_params['ID']} не найдена");
        }

        $allowdStatus = PlanDigitalization::getAllwedChangeStatus(
            $this->_params['ID'],
            $this->_user['ID']
        );
        $statusId = $this->_checkAndGetStatus($allowdStatus);
        switch ($statusId) {
            case PlanDigitalization::STATUS_DONE:
                if (!file_exists(
                    $_SERVER['DOCUMENT_ROOT']
                    . $this->_params['UF_BOOK_FILE_path']
                )
                ) {
                    throw new Exception('Файл не найден');
                }
                $file = new CFile();
                $fileArray = $file->MakeFileArray(
                    $_SERVER['DOCUMENT_ROOT']
                    . $this->_params['UF_BOOK_FILE_path']
                );
                $fileArray['MODULE_ID'] = 'main';
                $fileFields = [
                    'UF_BOOK_FILE' => $fileArray,
                ];
                if ($file->SaveForDB(
                    $fileFields,
                    'UF_BOOK_FILE',
                    'plan_digitization'
                )
                ) {
                    $idHLBlock = self::getHLBlockId(HIBLOCK_CODE_DIGITIZATION_PLAN);
                    $planDigitization = NebMainHelper::getHIblockDataClassName($idHLBlock);


                    if (false === (
                        $planDigitization::update(
                            $this->_params['ID'],
                            [
                                'UF_BOOK_FILE' => $fileFields['UF_BOOK_FILE']
                            ]
                        )->getAffectedRowsCount() > 0
                            ? true
                            : false
                        )
                    ) {
                        throw new Exception('Не удалось сохранить файл');
                    } else {
                        $relatedOrders
                            = PlanDigitalization::getRelatedOrdersById(
                            $this->_params['ID']
                        );
                        foreach ($relatedOrders as $orderItem) {
                            $planDigitization::update(
                                $orderItem['ID'],
                                [
                                    'UF_BOOK_FILE' => $fileFields['UF_BOOK_FILE']
                                ]
                            );
                        }
                    }
                } else {
                    throw new Exception('Не удалось сохранить файл');
                }
                break;
        }
        $this->_changeStatus($statusId);
        if ($this->_params['actionType']) {
            $comment = $this->_params['comment'];
            if (!empty($this->_params['UF_LIBRARY'])
                && PlanDigitalization::STATUS_AGREED === (integer)$statusId
            ) {
                $element = new CIBlockElement();
                $libraryElement = $element->GetByID(
                    $this->_params['UF_LIBRARY']
                );
                if ($libraryElement = $libraryElement->Fetch()) {
                    $comment .= (
                        '<br> Заявка назначена на библиотеку: '
                        . $libraryElement['NAME']
                    );
                }
            }
            \Neb\Main\PlanDigitizationLogTable::add(
                [
                    'ORDER_ID'    => $this->_params['ID'],
                    'USER_ID'     => $this->_user['ID'],
                    'COMMENT'     => $comment,
                    'STATUS_ID'   => $statusId,
                    'ACTION_NAME' => $this->_params['actionType'],
                ]
            );
        }

        return 200;
    }
    /**
     * @return $this
     */
    protected function _loadDigitizationOrder() {
        $idHLBlock = self::getHLBlockId(HIBLOCK_CODE_DIGITIZATION_PLAN);
        $tableClass = NebMainHelper::getHIblockDataClassName($idHLBlock);
        $this->arResult['ORDER'] = $tableClass::getById($this->_params['ID'])->fetch();
        return $this;
    }
    protected function _checkAndGetStatus(array $allowed, $checkFields = true) {
        $allowed = array_flip($allowed);
        if (!isset($allowed[$this->_params['UF_REQUEST_STATUS']])) {
            $statusNames = [];
            foreach ($allowed as $statusId) {
                $statusNames[] = PlanDigitalization::getStatusName($statusId);
            }
            throw new Exception("Данное действие не разрешено. Доступные статусы: " . implode(', ', $statusNames));
        }

        if (true === $checkFields) {
            switch ($this->_params['UF_REQUEST_STATUS']) {
                case PlanDigitalization::STATUS_ON_AGREEMENT:
                case PlanDigitalization::STATUS_CANCELED:
                    if (empty($this->_params['comment'])) {
                        throw new Exception(
                            'Не указана причина отклонения заявки'
                        );
                    }
                    break;
                case PlanDigitalization::STATUS_AGREED:
                    if ($this->_params['UF_LIBRARY'] <= 0) {
                        throw new Exception(
                            'Не указана библиотека исполнитель'
                        );
                    }
                    if ($this->_params['UF_DATE_FINISH'] <= 0) {
                        throw new Exception(
                            'Не указана плановый срок оцифровки'
                        );
                    }
                    break;
                case PlanDigitalization::STATUS_DONE:
                    if (empty($this->_params['UF_BOOK_FILE_path'])) {
                        throw new Exception(
                            'Не передан файл'
                        );
                    }
                    break;
            }
        }


        return $this->_params['UF_REQUEST_STATUS'];
    }
    /**
     * @param int $statusId
     *
     * @return bool
     * @throws Exception
     */
    protected function _changeStatus($statusId) {
        $idHLBlock = self::getHLBlockId(HIBLOCK_CODE_DIGITIZATION_PLAN);
        $planDigitization = NebMainHelper::getHIblockDataClassName($idHLBlock);
        $params = ['UF_REQUEST_STATUS' => PlanDigitalization::getStatusValueId($statusId)];
        //$iBlockLibraryId = IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY);
        //if (!empty($this->_params['UF_LIBRARY'])) {
        //    $rsProp = CIBlockElement::GetProperty(
        //        $iBlockLibraryId,
        //        $this->_params['UF_LIBRARY'],
        //        [],
        //        ['CODE' => 'LIBRARY_LINK']
        //    );
        //    if ($libLink = $rsProp->Fetch()) {
        //        $params['UF_LIBRARY'] = $libLink['VALUE'];
        //    }
        //}
        if(!empty($this->_params['UF_LIBRARY'])) {
            $params['UF_LIBRARY'] = $this->_params['UF_LIBRARY'];
        }
        if (!empty($this->_params['UF_DATE_FINISH'])) {
            $params['UF_DATE_FINISH'] = $this->_params['UF_DATE_FINISH'];
        }
        $affectedRowsCount = $planDigitization::update(
            $this->_params['ID'],
            $params
        )->getAffectedRowsCount();
        if ($affectedRowsCount > 0) {
            $relatedOrders = PlanDigitalization::getRelatedOrdersById(
                $this->_params['ID']
            );
            foreach ($relatedOrders as $orderItem) {
                $planDigitization::update(
                    $orderItem['ID'],
                    $params
                );
            }
            if (!$orderData = $planDigitization::getById($this->_params['ID'])
                ->fetch()
            ) {
                throw new Exception(
                    "Заявка №{$this->_params['ID']} не найдена"
                );
            }
            $email = '';
            if ($orderData['UF_NEB_USER']) {
                $user = new CUser();
                $userData = $user->GetByID($orderData['UF_NEB_USER'])->Fetch();
                if (isset($userData['EMAIL'])) {
                    $email = $userData['EMAIL'];
                }
            }
            if (!empty($email)) {
                NebMainHelper::includeModule('nota.exalead');
                $book = \Nota\Exalead\LibraryBiblioCardTable::getByFullSymbolicId($orderData['UF_EXALEAD_BOOK']);
                if (empty($book)) {
                    throw new Exception("Книга {$orderData['UF_EXALEAD_BOOK']} не найдена");
                }
                $book = $book[0];
                $event = new CEvent();
                $event->Send(
                    'DIGITIZATION_ORDER_ACTION',
                    SITE_ID,
                    [
                        'ID'          => $this->_params['ID'],
                        'EMAIL'       => $email,
                        'STATUS_NAME' => PlanDigitalization::getStatusName($statusId),
                        'BOOK_NAME'   => $book['Name'],
                        'BOOK_ID'     => $book['FullSymbolicId'],
                    ],
                    'Y',
                    PlanDigitalization::CHANGE_STATUS_EMAIL_TEMPLATE_ID
                );
            }

            return true;
        }

        return false;
    }
    /**
     * @return bool|string
     * @throws Exception
     */
    protected function _getUserRole() {
        if (!isset($this->_user['ID'])) {
            throw new Exception('Не найден пользователь!');
        }
        $user = new nebUser($this->_user['ID']);

        return $user->getRole();
    }
    public function getHLBlockId($hlBlockCode) {
        $connection = Application::getConnection();
        $sql = "SELECT ID FROM b_hlblock_entity WHERE NAME='" . $hlBlockCode . "';";
        $recordset = $connection->query($sql)->fetch();
        return $recordset['ID'];
    }

    public function getHLBlockDataClass($idBlock) {
        $hlblock = HL\HighloadBlockTable::getById($idBlock)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }
}