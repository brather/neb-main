<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 * @var array $arParams
 */
?>

<a type="button" class="btn btn-primary btadd closein" href="/bitrix/admin/user_edit.php?lang=ru" target="_blank">
    <?=Loc::getMessage('ADD_USER')?>
</a>

<div class="reader-search clearfix">
    <h3>Поиск пользователей</h3>
    <form>
        <input type="text" name="FIO" class="b-text reader-search__input"
               value="<?= $arParams['FIO'] ?>"
               placeholder="<?= Loc::getMessage('FULL_NAME') ?>">
        <button class="btn btn-primary" type="submit"  value="<?= Loc::getMessage('FIND') ?>"><?= Loc::getMessage('FIND') ?></button>
    </form>
</div>

<h3>Список пользователей</h3>
<div class="lk-table">
    <div class="lk-table__column" style="width:20%"></div>
    <div class="lk-table__column" style="width:40%"></div>
    <div class="lk-table__column" style="width:30%"></div>
    <div class="lk-table__column" style="width:10%"></div>
    <ul class="lk-table__header">
        <li class="lk-table__header-kind">
            <a class="lk-table__header-link" <?= SortingExalead('EMAIL'); ?>>E-mail</a>
        </li>
        <li class="lk-table__header-kind">
            <a class="lk-table__header-link" <?= SortingExalead('NAME'); ?>><?= Loc::getMessage('NAME') ?></a>
        </li>
        <li class="lk-table__header-kind">
            <a class="lk-table__header-link" <?= SortingExalead('DATE_REGISTER'); ?>><?= Loc::getMessage(
                    'DATE_REGISTER'
                ) ?></a>
        </li>
        <li class="lk-table__header-kind"><?= Loc::getMessage('ACTION') ?></li>
    </ul>
    <section class="lk-table__body">
        <? foreach ($arResult['items'] as $user) { ?>
            <ul class="lk-table__row">
                <li class="lk-table__col">
                    <a href="mailto:<?= $user['EMAIL'] ?>"><?= $user['EMAIL'] ?></a>
                </li>
                <li class="lk-table__col">
                    <a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $user['ID'] ?>"
                       target="_blank">
                        <?= $user['LAST_NAME'] ?> <?= $user['NAME'] ?> <?= $user['SECOND_NAME'] ?>
                    </a>
                </li>
                <li class="lk-table__col">
                    <?= $user['DATE_REGISTER'] ?>
                </li>
                <li class="lk-table__col">
                    <a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?= $user['ID'] ?>"
                       target="_blank">
                        <?= Loc::getMessage('EDIT') ?>
                    </a>
                </li>
            </ul>
        <? } ?>
    </section>
</div>

<?= $arResult['nav'] ?>
