<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * Class SearchPageViewerRightComponent
 */
class SearchPageViewerRightComponent extends CBitrixComponent
{
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $this->arResult = $arParams['RESULT'];
        $arParams = $arParams['PARAMS'];

        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * Исполнение компонента
     */
    public function executeComponent()
    {
        $this->includeComponentTemplate();
    }
}
