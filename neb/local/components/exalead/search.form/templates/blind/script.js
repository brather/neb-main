$(function () {
	// переключение полей формы
	$(document).on('change','.extendedSearch [name="librarytype"], .extendedSearch [name="searchtype"]',redrawFields);
	redrawFields();

	function redrawFields() {
		var fSearchtype  = $('select[name="searchtype"]').val();
		var fLibrarytype = $('select[name="librarytype"]').val();
		var fAccess;
		setTimeout(function(){
			$('#fieldListForm > .form-dl').prependTo('#fieldListForm');
		},100);
		$('.extendedSearch dt').each(function () {
			fAccess = false;
			if ($(this).hasClass('always') || $(this).hasClass(fSearchtype) && $(this).hasClass(fLibrarytype))
				fAccess = true;

			$(this).css('display', fAccess ? 'block' : 'none')
				.next("dd")
		    	.css('display', fAccess ? 'block' : 'none');

			$.each( $(this).next("dd").find('.form-control'), function() {
				$(this).attr('name', fAccess ? $(this).attr('data-name') : '');
			});

		});
	}
});