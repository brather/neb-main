<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_LICENSE_AGREEMENT')?></b>
</p>
<p align="center">
</p>
<p align="center">                                                                                                                                 
	<?=GetMessage('CONDITIONSINNDER_CITY_YEAR');?>
</p>
<p>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_IN_AGREEMENT');?>
</p>
<p>
</p>
<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_GRANT_OF_LICENSE');?></b>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_GRANT_OF_LICENSE_1_1');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_GRANT_OF_LICENSE_1');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_GRANT_OF_LICENSE_2');?>
</p>
<p>
 <b><?=GetMessage('CONDITIONSINNDER_VARIANT');?></b><?=GetMessage('CONDITIONSINNDER_VARIANT_TEXT');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_GRANT_OF_LICENSE_1_2');?>
</p>
<p>
</p>
<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_TRANSFERRED_RIGHTS');?></b>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_TRANSFERRED_RIGHTS_1');?>
</p>
<p>
	       <?=GetMessage('CONDITIONSINNDER_TRANSFERRED_RIGHTS_1_1');?>
</p>
<p>
	    <?=GetMessage('CONDITIONSINNDER_TRANSFERRED_RIGHTS_1_2');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_TRANSFERRED_RIGHTS_1_3');?>
</p>
<p>
	    <?=GetMessage('CONDITIONSINNDER_TRANSFERRED_RIGHTS_1_4');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_TRANSFERRED_RIGHTS_2');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_TRANSFERRED_RIGHTS_3');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_TRANSFERRED_RIGHTS_4');?>
</p>
<p align="center">
 <b> </b>
</p>
<p align="center">
 <b> </b>
</p>
<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_TERRITORY_AND_TERMS');?></b>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_TERRITORY_AND_TERMS_1');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_TERRITORY_AND_TERMS_2');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_TERRITORY_AND_TERMS_3');?>
</p>
<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_REWARD');?></b>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_REWARD_1');?>
</p>
<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_LIABILITY');?></b>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_LIABILITY_1');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_LIABILITY_2');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_LIABILITY_3');?>
</p>
<p>
</p>
<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_CONFLICT_RESOLUTION');?></b>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_CONFLICT_RESOLUTION_1');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_CONFLICT_RESOLUTION_2');?>
</p>
<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_DISSOLUTION');?></b>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_DISSOLUTION_1');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_DISSOLUTION_2');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_DISSOLUTION_3');?>
</p>
<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_ADDITIONAL_TERMS');?></b>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_ADDITIONAL_TERMS_1');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_ADDITIONAL_TERMS_2');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_ADDITIONAL_TERMS_4');?>
</p>
<p>
	 <?=GetMessage('CONDITIONSINNDER_ADDITIONAL_TERMS_5');?>
</p>
<p>
</p>
<p align="center">
 <b><?=GetMessage('CONDITIONSINNDER_ADDRESSES_REQUISITES');?></b>
</p>
<p align="center">
</p>
<table style="width: 487.35pt;" width="650" cellpadding="0" cellspacing="0">
<tbody>
<tr style="height: 247.6pt;">
	<td style="width: 296.4pt; height: 247.6pt;" width="399">
		<p>
			 <?=GetMessage('CONDITIONSINNDER_LICENSOR');?>:
		</p>
		<p>
			 <?=GetMessage('CONDITIONSINNDER_ADDRESS');?>:
		</p>
		<p>
			 <?=GetMessage('CONDITIONSINNDER_PASSPORT');?>:
		</p>
		<p>
			 <?=GetMessage('CONDITIONSINNDER_TELEPHONE');?>:
		</p>
		<p>
			 <?=GetMessage('CONDITIONSINNDER_TIN');?>
		</p>
		<p>
			 <?=GetMessage('CONDITIONSINNDER_INSURANCE');?>
		</p>
		<p>
		</p>
	</td>
	<td style="width: 240.95pt; height: 247.6pt;" width="321">
		<p>
			 <?=GetMessage('CONDITIONSINNDER_LICENSEE');?>:
	ФГБУ «Российская государственная библиотека»
Банк: Отделение 1 Главного управления Центрального банка Российской Федерации по Центральному федеральному округу г. Москва
Р/сч 40501810600002000079
БИК 044583001
ИНН 7704097560
КПП 770401001
Адрес: 119019, г. Москва, ул. Воздвиженка, д. 3/5
ОКПО 02175175
ОКТМО 45374000000
ОКАТО 45286552000

	</td>
</tr>
</tbody>
</table>
<p>
	 <?=GetMessage('CONDITIONSINNDER_SIGNATURES');?>:
</p>
<table cellpadding="0" cellspacing="0">
<tbody>
<tr>
	<td style="width: 299.3pt;" width="399">
		<p>
			 <?=GetMessage('CONDITIONSINNDER_LICENSOR');?>:
		</p>
		<p>
		</p>
		<p>
			 _________________
		</p>
		<p>
		</p>
	</td>
	<td style="width: 239.35pt;" width="319">
		<p>                                                                                       
			 <?=GetMessage('CONDITIONSINNDER_LICENSEE');?>
		</p>
		<p>
		</p>
		<p>
			 ________________<?=GetMessage('CONDITIONSINNDER_LICENSEE_NAME');?>
		</p>
		<p>
			 <?=GetMessage('CONDITIONSINNDER_STAMP_PLACE');?>
		</p>
	</td>
</tr>
</tbody>
</table>
<p>
</p>
