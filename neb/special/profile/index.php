<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('PROFILE_PAGE_TITLE'));
?>
<?$APPLICATION->IncludeComponent(
	"neb:profile", 
	".default", 
	array(
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/special/profile/",
		"SEF_URL_TEMPLATES" => array(
			"main" => "/",
			"profile_edit" => "edit/",
			"profile_update" => "update/",
			"profile_update_rgb" => "update_rgb/",
			"profile_update_select" => "update_select/",
			"statistics" => "statistics/",
			"funds_manage" => "funds/manage/",
			"readers" => "readers/",
			"readers_new" => "readers/new/",
			// "collections" => "collections/",
			"news" => "news/",
			"news_add" => "news/add/",
			"news_edit" => "news/edit/#ID#/",
			"collection" => "collection/",
			"collection_view" => "collection/#ID#/",
			"collection_edit" => "collection/edit/#ID#/",
			"collection_add" => "collection/add/",
			"full_access" => "full_access/",
			"full_access_2" => "full_access_2/",
			"full_access_3" => "full_access_3/",
			"full_access_4" => "full_access_4/",
			"check_eechb" => "check_eechb/",
			"plan_digitization" => "plan_digitization/",
			"plan_digitization_add" => "plan_digitization/add/",
			"searches" => "searches/",
			"searches_collection" => "searches/#COLLECTION_ID#/",
			"my_library" => "my-library/",
			"my_library_collection" => "my-library/#COLLECTION_ID#/",
			"my_library_reading" => "my-library/reading/",
			"my_library_read" => "my-library/read/",
			"quote" => "quote/",
			"quote_collection" => "quote/#COLLECTION_ID#/",
			"bookmark" => "bookmark/",
			"bookmark_collection" => "bookmark/#COLLECTION_ID#/",
			"note" => "note/",
			"note_collection" => "note/#COLLECTION_ID#/",
			"scanning" => "scanning/",
			"work_fonds" => "work-fonds/",
		)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>