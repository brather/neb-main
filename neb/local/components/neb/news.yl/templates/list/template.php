<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>
<section class="main-news">
    <div class="row">
        <div class="main-news__gallery main-news__gallery--year clearfix">
            <? $i = 0; foreach ($arResult['item'] as $arItem): ?>
                <div
                    class="col-md-3 col-sm-6 col-xs-6 main-news__gallery-item" <? if ($i % 4 == 0): ?> style="clear:both;"<? endif; ?>>
                    <a href="<?= $arItem['link']; ?>"
                       class="main-news__gallery-link main-news__gallery-link--year" title="<?= $arItem['title']; ?>"<?
                        if (!empty($arItem['enclosure']['local'])) { ?> style="background-image: url('<?= $arItem['enclosure']['local'] ?>')"<? } ?>>
                        <img src="<?= $arItem['enclosure']['local'] ?>" alt="<?= $arItem['title']; ?>" class="main-news__gallery-image" /><br />
                    </a>
                    <a href="<?= $arItem['link']; ?>"><?= $arItem['title']; ?></a>
                </div>
            <? $i++ ; endforeach ?>
        </div>
    </div>
</section>