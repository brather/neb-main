<?php
$MESS['ADD_FILE_AJAX_DELETE'] = ' Удалить';
$MESS['ADD_FILE_AJAX_ERROR_NOT_SAVE'] = 'Ошибка сохранения файла, попробуйте позже';
$MESS['ADD_FILE_AJAX_ERROR_FILE_SIZE'] = 'Превышен максимальный размер файла';
$MESS['ADD_FILE_AJAX_ERROR_EXTENSION'] = 'Неверный тип файла';
$MESS['ADD_FILE_AJAX_ERROR_NOT_CONNECTION_TO_DB'] = 'Ошибка подключения к базе данных';
$MESS['ADD_FILE_AJAX_ERROR_EMPTY_DIRECTORY'] = 'Отсутствует корневая папка для сохранения файлов';
$MESS['ADD_FILE_AJAX_BYTE'] = 'Байт';
$MESS['ADD_FILE_AJAX_KB'] = 'Кб';
$MESS['ADD_FILE_AJAX_MB'] = 'Мб';
$MESS['ADD_FILE_AJAX_GB'] = 'Гб';
$MESS['ADD_FILE_AJAX_CONFIRM_TITLE'] = 'Удалить?';
$MESS['ADD_FILE_AJAX_CONFIRM_TEXT'] = 'Удалить';
$MESS['ADD_FILE_AJAX_CONFIRM_YES'] = 'Вы уверены, что хотите удалить данный pdf?';