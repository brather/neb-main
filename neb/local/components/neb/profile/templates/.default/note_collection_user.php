<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
#	pre($arResult,1)
?>
<section class="innersection innerwrapper clearfix">
	<div class="b-searchresult noborder">
		<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
				"ROOT_MENU_TYPE" => "left_".LANGUAGE_ID,	// Тип меню для первого уровня
				"MENU_CACHE_TYPE" => "N",	// Тип кеширования
				"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
				"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
					0 => "",
				),
				"MAX_LEVEL" => "2",	// Уровень вложенности меню
				"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
				"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
			),
			false
		);?>
	</div><!-- /.b-searchresult-->
	<div class="b-mainblock left">
		<?$APPLICATION->IncludeComponent("neb:user.notes", "", Array('COLLECTION_ID' => $arResult['VARIABLES']['COLLECTION_ID']),false	);?>
	</div><!-- /.b-mainblock -->
	<div class="b-side right">
		<?$APPLICATION->IncludeComponent("neb:user.right.menu", "",
			Array(
				'TYPE' 				=> 'notes',
				'COLLECTION_URL' 	=> $arResult['FOLDER'].$arResult['URL_TEMPLATES']['note_collection'],
				'COLLECTION_ID' 	=> $arResult['VARIABLES']['COLLECTION_ID'],
				'ALL_BOOKS_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['my_library'],
				'READING_BOOKS_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['my_library_reading'],
				'READ_BOOKS_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['my_library_read'],

			)
			,false	);?>
	</div><!-- /.b-side -->

</section>