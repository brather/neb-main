<?php
$MESS['NEB_BOOKS_POPULAR_AUTHOR'] = 'Автор';
$MESS['NEB_BOOKS_POPULAR_NAME'] = 'Название';
$MESS['NEB_BOOKS_POPULAR_DATE'] = 'Дата публикации';
$MESS['NEB_BOOKS_POPULAR_LIB'] = 'Библиотека';
$MESS['NEB_BOOKS_POPULAR_READ'] = 'Читать';
$MESS['NEB_BOOKS_POPULAR_TITLE_HEAD'] = '12 самых популярных изданий';
?>