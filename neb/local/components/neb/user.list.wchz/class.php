<?php
/**
 * User: agolodkov
 * Date: 31.07.2015
 * Time: 10:37
 */

CBitrixComponent::includeComponentClass('neb:user.list');

/**
 * Class NebLibraryPartyComponent
 */
class UserWchzListComponent extends UserListComponent
{
    /**
     * @return $this
     */
    public function prepareParams()
    {
        parent::prepareParams();

        $user = nebUser::getCurrent()->getUser();
        $wchzIps = CIBlockElement::GetProperty(
            IBLOCK_ID_WORKPLACES, $user['UF_WCHZ'], array('sort' => 'asc'),
            Array('CODE' => 'IP')
        );
        $filterItem = ['LOGIC' => 'OR'];
        while ($ip = $wchzIps->Fetch()) {
            $ip = $ip['VALUE'];
            if (false === strpos($ip, '-')) {
                $filterItem[] = [
                    'USER_STAT_SESSION.IP_LAST_NUMBER' => ip2long($ip)
                ];
            } else {
                $ip = explode('-', $ip);
                $filterItem[] = [
                    'LOGIC' => 'AND',
                    ['>=USER_STAT_SESSION.IP_LAST_NUMBER' => ip2long($ip[0])],
                    ['<=USER_STAT_SESSION.IP_LAST_NUMBER' => ip2long($ip[1])],
                ];
            }
        }
        $this->arParams['listParams']['select'][] = 'LAST_LOGIN';
        $this->arParams['listParams']['filter']['>=USER_STAT_SESSION.DATE_LAST']
            = new \Bitrix\Main\Type\DateTime(ConvertTimeStamp(time() - 3600, 'FULL'));
        $this->arParams['listParams']['filter'][] = $filterItem;
        $this->arParams['listParams']['group'] = ['ID'];

        return $this;
    }

    /**
     * @return $this
     */
    public function loadList()
    {
        $this->_loadTable(new \Neb\Main\NebUserTable());
        return $this;
    }
}