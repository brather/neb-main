<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;
use \Neb\Main\Helper\UtilsHelper;

Loc::loadMessages(__FILE__);

$bIndexPage = $APPLICATION->GetCurPage(false) == SITE_DIR;

?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>" <? require $_SERVER["DOCUMENT_ROOT"].'/local/include_areas/open_graph_prefix.php';?>>
<head>
    <meta charset="<?=SITE_CHARSET?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><? $APPLICATION->ShowTitle() ?><?=($bIndexPage ? '' : ' — НЭБ.РФ')?></title>
    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="57x57" href="/local/templates/adaptive/favicon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="/local/templates/adaptive/favicon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/local/templates/adaptive/favicon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/local/templates/adaptive/favicon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/local/templates/adaptive/favicon/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="/local/templates/adaptive/favicon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/local/templates/adaptive/favicon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/local/templates/adaptive/favicon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/local/templates/adaptive/favicon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="/local/templates/adaptive/favicon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/local/templates/adaptive/favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/local/templates/adaptive/favicon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/local/templates/adaptive/favicon/favicon-16x16.png" />
    <link rel="manifest" href="/local/templates/adaptive/favicon/manifest.json" />

    <meta name="msapplication-config" content="/browserconfig.xml" />
    <meta name="msapplication-TileImage" content="/local/templates/adaptive/favicon/ms-icon-144x144.png" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="theme-color" content="#ffffff" />

    <!-- /FAVICON END -->

    <?if (!MINIFY_JS_CSS_MODE || MARKUP_DEBUG_MODE) { ?>

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/css/bootstrap.min.css" />
        <!-- Fonts -->
        <!--link
            href='//fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic,800,800italic,300,300italic&amp;subset=latin,cyrillic-ext,cyrillic'
            rel='stylesheet' type='text/css' /-->
        <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/css/font-awesome.min.css" />
        <!-- jquery ui -->
        <!--link rel="stylesheet" type="text/css" href="/local/templates/adaptive/vendor/jquery-ui/jquery-ui.css" /-->
        <!-- Important Owl stylesheet -->
        <!-- <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css" />
        <link rel="stylesheet" type="text/css" href="vendor/owl-carousel/owl.theme.css" /> -->
        <!-- slick slider -->
        <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/vendor/slick/slick.css" />
        <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/vendor/slick/slick-theme.css" />

        <link rel="stylesheet" type="text/css"
              href="/local/templates/adaptive/vendor/custom-select/bootstrap-select.min.css" />
        <link rel="stylesheet" type="text/css"
              href="/local/templates/adaptive/vendor/select2-4.0.3/dist/css/select2.css" />
        <link rel="stylesheet" type="text/css"
              href="/local/templates/adaptive/vendor/keyboard/keyboard.css" />

        <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/css/style.css" />
        <!-- Стили для слабовидящих -->
        <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/css/low_vision.css" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script type="text/javascript" src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script type="text/javascript" src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/css/ie.css" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="/bitrix/js/phpsolutions.backtotop/backtotop.css" />
    <? } else { ?>
        <!--[if lt IE 9]>
            <script type="text/javascript" src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script type="text/javascript" src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="/local/templates/adaptive/css/site.min.css" />
    <? };?>

    <?php $userToken = nebUser::getCurrent()->getToken(); ?>
    <meta name="user-token" content="<?= $userToken['UF_TOKEN'] ?>">
    <script type="text/javascript" src="/local/templates/adaptive/js/jquery/prejquery.js"></script>
    <meta name="search-auto-complete-source" content="<?= SEARCH_AUTO_COMPLETE_API_URL ?>">
    <meta name="bitrix-sessid" content="<?= bitrix_sessid() ?>">

    <? $APPLICATION->ShowHead(); ?>

     <? require $_SERVER["DOCUMENT_ROOT"] . '/local/include_areas/open_graph.php';?>
</head>
<body class="<?=UtilsHelper::getBlindSettings()?>">
<? $APPLICATION->ShowPanel() ?>

<!-- менюшка в версии для слепых -->
<?php if(!empty($_COOKIE['blind_version'])):?>
    <?php $APPLICATION->IncludeFile('/local/templates/adaptive/_shards/blind.php', array()); ?>
<?php endif;?>

<? if (false === strpos($_SERVER['REQUEST_URI'], 'profile')): ?>

    <div class="lte8"><div class="lte8mes"></div><div class="milkie"></div></div>
    <header>
        <?php if ('/auth/' !== substr($APPLICATION->GetCurPage(), 0, 6)): ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top",
                array(
                    "ROOT_MENU_TYPE" => "top_" . LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left_" . LANGUAGE_ID,
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            ); ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:system.auth.form",
                "socservices",
                array()
            ); ?>
        <?php endif; ?>
        <?php if ($bIndexPage): ?>
            <div class="container main-header">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-10">
                        <a href="/">
                            <img src="/local/templates/adaptive/img/logo-header.png" alt="" class="main-header__logo">
                        </a>
                    </div>
                    <div class="col-md-9 col-sm-9 hidden-xs">
                        <p class="main-header__desc">
                            <?
                            $APPLICATION->IncludeComponent(
                                'neb:element.text',
                                '',
                                [
                                    'XML_ID' => 'main_welcome',
                                ],
                                false
                            );
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </header>

<? else: ?>
    <? // шапка в ЛК
    $APPLICATION->IncludeComponent("bitrix:system.auth.form", "main",
        Array(
            "FORGOT_PASSWORD_URL" => "/auth/",
            "SHOW_ERRORS" => "N"
        ),
        false
    );
    ?>
<? endif; ?>


<? if (($APPLICATION->GetCurPage(false) != SITE_DIR)
    && (false === strpos($_SERVER['REQUEST_URI'], 'profile')) && (false === strpos($_SERVER['REQUEST_URI'], '/extended/'))
    && (false === strpos($_SERVER['REQUEST_URI'], '/auth/'))  && (false === strpos($APPLICATION->GetCurPage(), '/library/'))
    && (false === strpos($APPLICATION->GetCurPage(), '/bbk/'))  && (false === strpos($_SERVER['REQUEST_URI'], 'forum'))):
?>
    <? $APPLICATION->IncludeComponent(
        "exalead:search.form",
        "redesign_inner",
        Array(
            "PAGE" => (''),
            "POPUP_VIEW" => 'Y',
            "ACTION_URL" => '/search/',
            "REQUEST_PARAMS" => $requestParams
        )
    ); ?>
<? endif; ?>

<? if ($APPLICATION->GetCurPage(false) != SITE_DIR && (false === strpos($_SERVER['REQUEST_URI'], 'profile'))): ?>
<div class="container">
    <?
    /**
     * Силище кастылище скрывающий тайтл
     * На странице вызвать $APPLICATION->SetPageProperty('hide-title', true);
     */
    $APPLICATION->AddBufferContent(
        function () use ($APPLICATION) {
            if (true !== $APPLICATION->GetPageProperty('hide-title')) {
                return '<h1>' . $APPLICATION->GetTitle() . '</h1>';
            }
            return '';
        }
    ); ?>
<? endif; ?>

<? if (Option::get("neb.main", "main_page_message") == 'Y'): ?>
    <div  class="container" style="padding:0">
        <p style="padding: 10px; border:red solid 1px; color:red">
            <?=Option::get("neb.main", "main_page_message_test");?>
        </p>
    </div>
<? endif; ?>