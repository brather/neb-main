<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once(substr(__DIR__, 0, strrpos(__DIR__, '/')) . "/prolog.php");

$module_id = ADMIN_MODULE_NAME;
\Bitrix\Main\Loader::IncludeModule(ADMIN_MODULE_NAME);

use \Bitrix\NotaExt\Utils as Ut;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");

$MOD_RIGHT = $APPLICATION->GetGroupRight(ADMIN_MODULE_NAME);

if ($MOD_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$ext_id = $_REQUEST["ext_id"];

$strFolder = substr(__DIR__, 0, strrpos(__DIR__, '/')) . '/ext';

if (!file_exists($strFolder . '/' . $ext_id)) {
    echo "Ext not found";
    exit();
}

$ext_dir = $strFolder . "/" . $ext_id;
$ext_dir_info = $ext_dir . '/index.php';

if (!file_exists($ext_dir_info)) {
    echo "Ext index not found";
    exit();
}

include_once($ext_dir_info);
if (!class_exists($ext_id)) {
    echo "class " . $ext_id . " not found";
    exit();
}

$info = new $ext_id;
if (!$info->IsInstalled()) {
    echo "Ext " . $ext_id . " not installed";
    exit();
}

$APPLICATION->SetTitle(GetMessage("MAIN_TITLE") . ': ' . $info->EXT_NAME);


require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

// Выбираем парамеры для расширения
$arAllOptions = $info->getOptions();

// Присутствует указатель на новый алгоритм ( с использованием CModuleOptions: http://dev.1c-bitrix.ru/community/webdev/user/104863/blog/5296/?sphrase_id=3152112 )

foreach ($arAllOptions['arOptions'] as $optionKey => $optionItem) {
    $arAllOptions['arOptions']["{$ext_id}_{$optionKey}"] = $optionItem;
    unset($arAllOptions['arOptions'][$optionKey]);
}

$opt = new CExtOptions($module_id, $ext_id, $arAllOptions['arTabs'], $arAllOptions['arGroups'], $arAllOptions['arOptions']);
$opt->ShowHtml();

require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin.php");