<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CJSCore::Init(array('date'));

?>
<!--<script src="/local/templates/.default/markup/js/libs/jquery.fileupload.js" charset="utf-8"></script>-->
<style>
	.plus_ico {
		display: inline-block;
		width: 18px;
		height: 18px;
		background: url("/local/templates/adaptive/img/icons.png") no-repeat 0 -533px;
		margin-left: 34px;
		margin-top: 3px;
		cursor: pointer;
	}
	.minus .plus_ico { background-position: 0 -563px; }
</style>
<div class="b-mainblock left digitizing-block">
	<div class="b-searchresult clearfix">
		<?$APPLICATION->ShowViewContent('menu_top')?>
	</div><!-- /.b-searchresult-->
<!--
	<div id="ui-widget-overlay" class="ui-widget-overlay ui-front digitoverlay" style="display:none;"></div>
	<div id="digit_popup_loading" style="min-height:400px;heigth:auto;width:100%;display:none;" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front" tabindex="-1" role="dialog" aria-describedby="ui-id-2" aria-labelledby="ui-id-3">
		<div id="digit_popup_loading-title" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
			<span id="ui-id-3" class="ui-dialog-title">&nbsp;</span>
			<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title="close">
			<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>
			<span class="ui-button-text">close</span></button></div><div class="digitizing_popup ui-dialog-content ui-widget-content" id="ui-id-2" style="width: auto; min-height: 40px; max-height: none; height: auto;">
			<span class="digitizing_popup_loading" style="display: none;">загрузка...</span>
			<h2>Добавить издания</h2>
			<div id="iframe-content" height="100%"></div>
			<br>
			<button type="button" class="button_mode bottompopupclose btn btn-primary" role="button" href="/" style="position: absolute;bottom:10px;right: 35px;padding: 2px 8px;">Сохранить</button>	
		</div>
	</div>
-->
	<h3>Комплекс самостоятельного сканирования</h3>
	<form action="<?= $APPLICATION->GetCurPage() ?>" method="POST" name="form2" class="digitizing-cost-form form-inline">
		<p>Стоимость сканируемой страницы</p>
		<div class="form-group">
			<input type="text" value="<?=$arResult['SCAN_COST_1']?>" name="scan_cost_1" size="3" maxlength="3" class="form-control">
		</div>
		<div class="form-group">
			руб.
		</div>
		<div class="form-group">
			<input type="text" value="<?=$arResult['SCAN_COST_2']?>" name="scan_cost_2" size="2" maxlength="2" class="form-control">
		</div>
		<div class="form-group">
			коп.
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-default" value="Сохранить">Сохранить</button>
		</div>
	</form>

	<hr>

	<h3>Загрузка планов на оцифровку</h3>
	<p>Выберите дату до которой будут оцифрованы загружаемые издания</p>
	<form class="digit-plan-date__form form-inline" action="<?= $APPLICATION->GetCurPage() ?>" method="GET" name="form3">
		<div class="form-group">
			<div class="input-group">
				<input type="text" name="digitizing-plan" id="digitizing-plan" size="10" value="<?=$_GET['digitizing-plan']?>" class="form-control" data-masked="99.99.9999">
				<span class="input-group-btn">
					<a class="btn btn-default" id="calendarlabel" onclick="BX.calendar({node: 'digitizing-plan', field: 'digitizing-plan',  form: '', bTime: false, value: ''});">
		                <span class="glyphicon glyphicon-calendar"></span>
		            </a>
				</span>
			</div>
		</div>
		<div class="form-group">
			<button class="btn btn-default digit-plan-date__form-save" value="Сохранить" style='float:none;'>Назначить</button>
		</div>
	</form>
	<br>

	<div class="digit-plan-load" style="display:none;">
		<p>Выберите способ загрузки плана оцифровки</p>
		<div class="row">
			<div class="col-md-6">
				<p>Из каталога НЭБ</p>
				<button disabled 
					class="digit-plan-load__add-book btn btn-default" 
					href="<?=$arParams['ADD_PAGE_URL']?>"
					data-add-books-to-date="<?=$arParams['ADD_PAGE_URL']?>"
				>
					<?=GetMessage('LIBRARY_PLAN_DIGITAL_ADD_BOOKS');?>
				</button>
			</div>
			
			<div class="col-md-6">
				<p>Импорт плана оцифровки(только xlsx)</p>
				<form id="add_excel" class="digit-plan-load__add-excel-form" name="add_excel" action="/profile/plan_digitization/" method="post" enctype="multipart/form-data">
					<div class="fileinput fileinput-new" data-provides="fileinput">
					    <span class="btn btn-default btn-file">
					    	<span>Выберите файл</span>					    	
							<input data-upload-digitizing-plan type="file" name="excel" data-url="/profile/plan_digitization/" disabled>
					    </span>
					    <span class="fileinput-filename"></span><span class="fileinput-new">Файл не выбран</span>
					</div>


					<input data-upload-digitizing-plan-date type="hidden" name="plan-date" value="">
					<!-- <button class="digitizing-plan-submit button_mode" disabled type="submit" value="Загрузить">Загрузить</button> -->
				</form>				
				<img class="digit-plan-load__progress" src="/local/templates/.default/markup/i/horizontal-loader.gif" alt="">
				<div class="digit-plan-load__message"></div>
				<br>
				<form id="get_excel" class="digit-plan-load__get-excel-form" name="add_excel" action="/profile/plan_digitization/" method="post" enctype="multipart/form-data">
					<input type="hidden" class="digit-plan-load__get-excel-input" name="digitizing-plan" value="">
					<button type="submit" class="btn btn-default digit-plan-load__get-excel-btn" style="position:relative; z-index: 100;" target="_blank" href="#"><?=GetMessage('LIBRARY_PLAN_DIGITAL_EXPORT_EXCEL_PROTOCOL');?></button>
				</form>				
			</div>
		</div>

		<script type="text/javascript" src="/local/templates/adaptive/vendor/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="/local/templates/adaptive/vendor/jquery.fileupload.js"></script>
		
		<script type="text/javascript" src="/local/templates/adaptive/js/widget-dialog.js"></script>
		<div id="ui-WD"></div>
		<script>
			$(function(){
				$('.digit-plan-load__get-excel-btn').hide();

				/*Проверка формы «Статистика за период»*/
				$('.stats-period-submit').click(function(event) {
					event.preventDefault();

					var dateFromRaw = $('#date-from').val();
					var dateFrom = new Date(dateFromRaw.split('.')[2], dateFromRaw.split('.')[1]-1, dateFromRaw.split('.')[0]).valueOf();

					var dateToRaw = $('#date-to').val();
					var dateTo = new Date(dateToRaw.split('.')[2], dateToRaw.split('.')[1]-1, dateToRaw.split('.')[0]).valueOf();

					if(dateFrom <= dateTo){
						$('.stats-period-message').hide();
						$('#date-from').removeClass('error');
						$('#date-to').removeClass('error');
						$('.stats-period-form').submit();
					}
					else {
						$('#date-from').addClass('error');
						$('#date-to').addClass('error');
						$('.stats-period-message').show();
					}
				});

				/*Загрузчик планов на оцифровку*/
				$('#add_excel').fileupload({
					dataType: 'json',
					submit: function(e, data) {
						$('.digit-plan-load__add-excel-form').hide();
						$('.digit-plan-load__progress').show();
					},
					done: function (e, data) {
						$('.digit-plan-load__progress').hide();
						$('.digit-plan-load__add-excel-form').show();
						$('.digit-plan-load__get-excel-btn').show().removeAttr('disabled');

						var response = data._response.result;
						$('.digit-plan-load__get-excel-input').val(JSON.stringify(response));						
						if(!response.error){
							var loaded = ''; var err = ''; var already = ''; var notfound = '';
							(response.added) ? loaded = '<li>Успешно загружено <b>'+response.added.length+'</b> записей</li>' : loaded = '';
							(response.error) ? err = '<li>Ошибок <b>'+response.error.length+'</b> записей</li>' : err = '';
							(response.already) ? already = '<li>Уже внесены в план оцифровки другой библиотеки <b>'+response.already.length+'</b> записей</li>' : already = '';
							(response.notfound) ? notfound  = '<li><b>'+response.notfound .length+'</b> изданий нет в фонде НЭБ</li>' : notfound = '';
							$('.digit-plan-load__message').html('<ul>'+loaded+err+already+notfound+'</ul>');
						}
						else {
							$('.digit-plan-load__message').html(response.error_message);
						};
						BX.showWait(document.body.querySelector('.b-add_digital-wrapper'));
						$.ajax({
							url: "/profile/plan_digitization/",
							data: {},
							type: "GET",
							beforeSend: function(xhr){xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');},
							success: function(data) {
								$('.b-add_digital-wrapper').html('').prepend(data);
								BX.closeWait(document.body.querySelector('.b-add_digital-wrapper'));
							}
						});

					},
					fail: function(e, data) {
						$('.digit-plan-load__add-excel-form').show();
						$('.digit-plan-load__progress').hide();
						$('.digit-plan-load__message').html('<p>Ошибка загрузки, повторите еще раз</p>');
					}
				});
			});

			/*Выбор даты оцифровки*/
			$('.digit-plan-date__form-save').click(function(event) {
				event.preventDefault();
				var now = new Date(),
					today = new Date(now.getFullYear(), now.getMonth(), now.getDate()).valueOf(),
					dateInput = $('#digitizing-plan'),
					targetDateRaw = dateInput.val(),
					targetDate = new Date(targetDateRaw.split('.')[2], targetDateRaw.split('.')[1]-1, targetDateRaw.split('.')[0]).valueOf(),
					addButton = $('[data-add-books-to-date]'),
					inputFile = $('[data-upload-digitizing-plan]');

				if(targetDateRaw && today < targetDate){
					dateInput.removeClass('error');
					addButton.removeAttr('disabled');
					inputFile.removeAttr('disabled');
					/*$('.digitizing-plan-submit').removeAttr('disabled');*/
					$('.digit-plan-load').slideDown();
					$('[data-upload-digitizing-plan-date]').val($('#digitizing-plan').val());
					addButton.attr('href','/profile/plan_digitization/add/?date='+$('#digitizing-plan').val()+'');
				}
				else {
					$('#digitizing-plan').addClass('error');
					addButton.attr('disabled','disabled');
					inputFile.attr('disabled','disabled');
					/*$('.digitizing-plan-submit').attr('disabled','disabled');*/
				}
			});			
			/*Добаление издания вручную*/
			$('[-data-add-books-to-date]').click(function(event){
				var aWD = new WidgetDialog('ui-WD');
				aWD.VisibleSave = true;
				aWD.Reload = true;
				aWD.Execute($('[data-add-books-to-date]').attr('href'));
			});
			
		</script>
	</div>

	<hr>

	<h3>Статистика и отчетность</h3>
	<p>
		<?=GetMessage('LIBRARY_PLAN_DIGITAL_IN_LIST');?>: <span class="digitizing-count"><?=$arResult['NAV_COUNT']?></span>
	</p>

	<h4>Статистика за период</h4>
	<form class="digitizing-form stats-period-form form-inline" action="<?= $APPLICATION->GetCurPage() ?>" method="GET" name="form1">
		<div class="form-group">
			C 
			<div class="input-group">
				<input 
					type="text" 
					id="date-from" 
					name="from" 
					size="10" 
					value="<?php echo date('d.m.Y', $arResult['dateFrom']) ?>" 
					class="form-control"
					data-masked="99.99.9999"					
				>
				<span class="input-group-btn">
					<a class="btn btn-default" id="calendarlabel" 
					onclick="BX.calendar({node: 'date-from', field: 'date-from',  form: '', bTime: false, value: ''});"
					>
		                <span class="glyphicon glyphicon-calendar"></span>
		            </a>
				</span>
			</div>
		</div>
		<div class="form-group">
			по 
			<div class="input-group">
				<input 
					type="text" 
					name="to" 
					id="date-to" 
					size="10" 
					value="<?php echo date('d.m.Y', $arResult['dateTo']) ?>" 
					class="form-control"
					data-masked="99.99.9999"					
				>
				<span class="input-group-btn">
					<a class="btn btn-default" id="calendarlabel" 
					onclick="BX.calendar({node: 'date-to', field: 'date-to',  form: '', bTime: false, value: ''});"
					>
		                <span class="glyphicon glyphicon-calendar"></span>
		            </a>
				</span>
			</div>
		</div>
		<div class="form-group"><button type="submit" class="btn btn-default stats-period-submit" value="Обновить">Обновить</button></div>
	</form>
	<div class="stats-period-message">
		Задан не верный период дат
	</div>
	<br>
	<a class="btn btn-default" target="_blank" href="<?=$APPLICATION->GetCurPageParam('print=Y',array('print'));?>"><?=GetMessage('LIBRARY_PLAN_DIGITAL_PRINT');?></a>
	<a class="btn btn-default" target="_blank" href="<?=$APPLICATION->GetCurPageParam('export=Y', array('export'));?>"><?=GetMessage('LIBRARY_PLAN_DIGITAL_EXPORT_EXCEL');?></a>
	<hr>
	<h3>План оцифровки</h3>
	<?
	if($arParams['ajax'])
		$APPLICATION->RestartBuffer();
	?>
	<table class="table">
		<tr>
			<th>
				<?=GetMessage('LIBRARY_PLAN_DIGITAL_AUTHOR');?>
			</th>
			<th>
				<?=GetMessage('LIBRARY_PLAN_DIGITAL_NAMEDIG');?>
			</th>
			<th>
				<?=GetMessage('LIBRARY_PLAN_DIGITAL_DATEDIG');?>
			</th>
			<th>
				<?=GetMessage('LIBRARY_PLAN_DIGITAL_COMMENT');?>
			</th>
			<th>
				<?=GetMessage('LIBRARY_PLAN_DIGITAL_REMOVE_FROM_PLAN');?>
			</th>
		</tr>
		<?
		if(!empty($arResult['BOOKS'])){

			foreach($arResult['BOOKS'] as $arItem)
			{
				?>
				<tr class="this-n-next-row" data-item-widget id="<?=$arItem['BOOK_ID']?>"  <?php if ($arItem['COMMENT']&& !preg_match('#массовая загрузка#iu', $arItem['COMMENT'])):?> style="background-color: #f06735;" <?php endif; ?> >
					<td><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['authorbook']?></td>
					<td>
						<?if (isset($arResult['ITEMS'][$arItem['BOOK_ID']])): ?>
							<?=$arResult['ITEMS'][$arItem['BOOK_ID']]['title']?>
							<div class="b-digital_act">
								<a href="#" data-nex-row-toggle><?=GetMessage('LIBRARY_PLAN_DIGITAL_DESCRIPTION');?></a>
								<?
								if(!empty($arResult['BOOKS_OTHER'][$arItem['BOOK_ID']])){
									?>
									<a href="#" class="b-digital_desc b-digital"><?=GetMessage('LIBRARY_PLAN_DIGITAL_IN_DIG');?></a>
									<?
								}
								?>
							</div>
						<?else:?>
							Книга с идентификатором <?=$arItem['BOOK_ID']?> не найдена в НЭБ
						<?endif;?>
					</td>
					<td><?=$arItem['DATE_FINISH']?></td>
					<td><?=$arItem['COMMENT']?></td>
					<td>
						<div class="rel plusico_wrap plan-digitalization minus">
							<div class="plus_ico"></div>
							<div class="b-hint del"><a href="#"><?=GetMessage('LIBRARY_PLAN_DIGITAL_REMOVE');?></a> <?=GetMessage('LIBRARY_PLAN_DIGITAL_FROM_PLAN');?></div>
						</div>
					</td>
				</tr>
				<tr style="display: none;">
					<td colspan="5">
						<div class="b-infobox rel b-infoboxdescr"  data-link="descr">
							<a href="#" class="close"></a>
							<?
							if(!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['authorbook'])){
								?>
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=GetMessage('LIBRARY_PLAN_DIGITAL_AUTHOR');?>: </span>
									<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['authorbook']?></span>
								</div>
								<?
							}
							?>
							<div class="b-infoboxitem">
								<span class="tit iblock"><?=GetMessage('LIBRARY_PLAN_DIGITAL_HEADING');?>: </span>
								<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['title']?></span>
							</div>
							<?
							if(!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['year'])){
								?>
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=GetMessage('LIBRARY_PLAN_DIGITAL_OUTPUT');?>: </span>
									<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['year']?> г.</span>
								</div>
								<?
							}
							if(!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['countpages']))
							{
								?>
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=GetMessage('LIBRARY_PLAN_DIGITAL_PHYS_DESC');?>: </span>
									<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['countpages']?> с.</span>
								</div>
								<?
							}
							if(!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['library']))
							{
								?>
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=GetMessage('LIBRARY_PLAN_DIGITAL_LIB');?>: </span>
									<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['library']?></span>
								</div>
								<?
							}
							?>
						</div><!-- /b-infobox -->
						<?
						if(!empty($arResult['BOOKS_OTHER'][$arItem['BOOK_ID']])){
						?>
						<div class="b-infobox rel" data-link="digital">
							<a href="#" class="close"></a>
							<?
							foreach($arResult['BOOKS_OTHER'][$arItem['BOOK_ID']] as $arOther)
							{
								?>
								<div class="b-infoboxitem b-infoboxmain">
									<span class="tit iblock"><?=GetMessage('LIBRARY_PLAN_DIGITAL_WHO_DIG');?></span>
									<span class="iblock val"><strong><?=$arOther['NAME']?></strong><span class="date">до <?=$arOther['DATE']?></span></span>
								</div>

								<?
							}
							?>
							<?
							}
							?>
						</div><!-- /b-infobox -->
					</td>
				</tr>
				<?
			}
		}
		?>

	</table>
	<?=htmlspecialcharsBack($arResult['STR_NAV'])?>
</div><!-- /.b-mainblock -->

<?
if($arParams['ajax']) exit;
?>
