<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (isset($_SESSION["EXTERNAL_AUTH_ERROR"])
    && !empty($_SESSION["EXTERNAL_AUTH_ERROR"])
) {
    $arResult['ERROR_MESSAGE'] = $_SESSION["EXTERNAL_AUTH_ERROR"];
    unset($_SESSION["EXTERNAL_AUTH_ERROR"]);
}
