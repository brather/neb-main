<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//here you can place your own messages
$messageLocCode = 'SOME_ERROR';
$errorFlag = true;
switch ($arResult["MESSAGE_CODE"]) {
    case "E01":
        $messageLocCode = 'USER_NOT_FOUND'; //When user not found
        $errorFlag = true;
        break;
    case "E02":
        $messageLocCode = 'SUCCESSFULLY_AUTHORIZED'; //User was successfully authorized after confirmation
        $errorFlag = false;
        break;
    case "E03":
        $messageLocCode = 'READY_CONFIRM'; //User already confirm his registration
        $errorFlag = false; // Ну, не будем считать это ошибкой
        break;
    case "E04":
        $messageLocCode = 'MISSED_CODE'; //Missed confirmation code
        $errorFlag = true;
        break;
    case "E05":
        $messageLocCode = 'WRONG_CODE'; //Confirmation code provided does not match stored one
        $errorFlag = true;
        break;
    case "E06":
        $messageLocCode = 'SUCCESSFUL_CONFIRMATION'; //Confirmation was successfull
        $errorFlag = false;
        break;
    case "E07":
        $messageLocCode = 'SOME_ERROR'; //Some error occured during confirmation
        $errorFlag = true;
        break;
}
?>
<section class="innersection innerwrapper clearfix">
    <div class="b-formsubmit">
        <span class="b-successlink"><?= GetMessage("ACCOUNT_ACTIVATION") ?></span>
        <?
        if ($errorFlag) {
            ?>
            <? ShowError(GetMessage($messageLocCode)); ?>
            <?
        } else {
            ?>
            <p><?= GetMessage($messageLocCode); ?></p>
            <?
        }
        ?>
    </div>
</section>
