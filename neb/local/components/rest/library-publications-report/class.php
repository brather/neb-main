<?php

use \Bitrix\Main\Loader,
    \Bitrix\Main\Type\Date,
    \Bitrix\Main\Type\DateTime,
    \Bitrix\Main\Entity\ExpressionField,
    \Nota\Collection\LibraryStatTable;

require_once(__DIR__ . '/../../../php_interface/include/PHPExcel.php');

CBitrixComponent::includeComponentClass("rest:rest.server");

/**
 * Class RestLibraryPublicationsReport
 */
class RestLibraryPublicationsReport extends RestServer
{
    /**
     * @var array
     */
    protected $_methodAllowGroups
        = array(
            'getAction' => array(
                13 => true,
            )
        );

    protected $_params
        = array(
            'dateFrom' => null,
            'dateTo'   => null,
        );

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareParams()
    {
        if (!Loader::includeModule('nota.collection')) {
            throw new Exception('Module "nota.collection" not installed');
        }
        if (!isset($this->_params['dateFrom']) || empty($this->_params['dateFrom'])) {
            $this->_params['dateFrom'] = ConvertTimeStamp( time() - 86400 * LibraryStatTable::DEFAULT_STAT_DAYS );
        }
        if (!isset($this->_params['dateTo']) || empty($this->_params['dateTo'])) {
            $this->_params['dateTo'] = ConvertTimeStamp();
        }

        return $this;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function getAction()
    {
        // определение минимальной даты
        $minDate = LibraryStatTable::getList(
            array(
                'filter'  => array(
                    '>=DATE' => new Date($this->_params['dateFrom']),
                    '<=DATE' => new Date($this->_params['dateTo'])
                ),
                'select'  => array(
                    'MIN_DATE',
                ),
                'runtime' => array(
                    new ExpressionField('MIN_DATE', 'MIN(DATE)')
                )
            )
        )->fetch();
        $minDate = $minDate['MIN_DATE'] ? : new Date($this->_params['dateFrom']);

        // определение максимальной даты
        $maxDate = LibraryStatTable::getList(
            array(
                'filter'  => array(
                    '>=DATE' => new Date($this->_params['dateFrom']),
                    '<=DATE' => new Date($this->_params['dateTo'])
                ),
                'select'  => array(
                    'MAX_DATE'
                ),
                'runtime' => array(
                    new ExpressionField('MAX_DATE', 'MAX(DATE)')
                ),
            )
        )->fetch();
        $maxDate = $maxDate['MAX_DATE'] ? : new Date($this->_params['dateTo']);

        // Статистические данные и идентификаторы библиотек из статистики
        $arStatData = $arLibraryIds = array();
        $dbResult = LibraryStatTable::getList(array(
            'filter' => array(
                array(
                    'LOGIC' => 'OR',
                    array('DATE' => $maxDate),
                    array('DATE' => $minDate)
                )
            ),
            'select' => array(
                'DATE',
                'LIBRARY_ID',
                'PUBLICATIONS',
                'PUBLICATIONS_DD',
                'PUBLICATIONS_DIGITIZED_DD',
                'PUBLICATIONS_DIGITIZED',
            )
        ));
        while ($arItem = $dbResult->fetch()) {
            $arStatData[] = $arItem;
            $arLibraryIds[$arItem['LIBRARY_ID']] = true;
        }

        $arLibraryIds = array_filter(array_keys($arLibraryIds));
        /* if (empty($arLibraryIds)) { #19212
         *   throw new Exception('Empty library list', 500);
         * } */
        if (!Loader::includeModule('iblock')) {
            throw new Exception('Module "iblock" not installed');
        }

        // выборка библиотек
        $arLibraryData = array();
        $obIBlockElement = new CIBlockElement();
        $dbResult = $obIBlockElement->GetList(
            array(),
            array(
                'IBLOCK_CODE'            => IBLOCK_CODE_LIBRARY,
                'ID'                     => $arLibraryIds,
                '>PROPERTY_LIBRARY_LINK' => 0
            ),
            false,
            false,
            array('ID', 'NAME', 'PROPERTY_LIBRARY_LINK')
        );
        while ($arItem = $dbResult->Fetch()) {
            $arLibraryData[$arItem['ID']] = $arItem;
        }

        // формирование результирующего массива
        $this->arResult['statData'] = array(
            'from'      => array(),
            'to'        => array(),
            'libraries' => array(),
        );
        foreach ($arStatData as $arItem) {
            if (isset($arLibraryData[$arItem['LIBRARY_ID']])
                || $arItem['LIBRARY_ID'] == LibraryStatTable::FEDERAL_LIBRARIES_ID
                || '0' === $arItem['LIBRARY_ID']
            ) {
                if ($arItem['DATE'] instanceof DateTime) {
                    $arItem['DATE'] = $arItem['DATE']->format('d.m.Y');
                }
                if (LibraryStatTable::FEDERAL_LIBRARIES_ID == $arItem['LIBRARY_ID']) {
                    $exaleadLibId = LibraryStatTable::FEDERAL_LIBRARIES_ID;
                    $arLibraryData[$arItem['LIBRARY_ID']] = array();
                } elseif ('0' === $arItem['LIBRARY_ID']) {
                    $exaleadLibId = 0;
                    $arLibraryData[$arItem['LIBRARY_ID']] = array();
                } elseif (isset($arLibraryData[$arItem['LIBRARY_ID']]['PROPERTY_LIBRARY_LINK_VALUE'])) {
                    $exaleadLibId = intval($arLibraryData[$arItem['LIBRARY_ID']]['PROPERTY_LIBRARY_LINK_VALUE']);
                } else {
                    continue;
                }

                $this->arResult['statData']['libraries'][$exaleadLibId] = $arLibraryData[$arItem['LIBRARY_ID']];

                if ($minDate->format('d.m.Y') === $arItem['DATE']) {
                    $this->arResult['statData']['from'][$exaleadLibId] = $arItem;
                }
                elseif ($maxDate->format('d.m.Y') === $arItem['DATE']) {
                    $this->arResult['statData']['to'][$exaleadLibId] = $arItem;
                }
            }
        }

        $this->_buildReport();

        return 200;
    }


    protected function _buildReport()
    {
        $changedColor = 'FF0000';
        $arStatData = $this->arResult['statData'];
        $title = 'Publications statistics' . ' ' . $this->_params['dateFrom'] . ' - ' . $this->_params['dateTo'];
        $staticCells = array(
            'A1' => array(
                'value' => 'Дата отчета: '
                    . $this->_params['dateFrom'],
                'merge' => 'J1',
            ),
            'A2' => array(
                'value' => 'Дата предыдущего отчета: '
                    . $this->_params['dateTo'],
                'merge' => 'J2',
            ),
            'A3' => array(
                'merge' => 'J3',
            ),
            'A4' => array(
                'value' => "Идентификатор библиотеки",
                'merge' => 'A6',
            ),
            'B4' => array(
                'value' => 'Библиотека',
                'merge' => 'B6',
            ),
            'C4' => array(
                'value' => 'Оцифрованные издания',
                'merge' => 'F4',
            ),
            'C5' => array(
                'value' => 'Было',
                'merge' => 'D5',
            ),
            'E5' => array(
                'value' => 'Стало',
                'merge' => 'F5',
            ),
            'G4' => array(
                'value' => 'Библиографические карточки',
                'merge' => 'J4',
            ),
            'G5' => array(
                'value' => 'Было',
                'merge' => 'H5',
            ),
            'I5' => array(
                'value' => 'Стало',
                'merge' => 'J5',
            ),
            'C6' => array(
                'value' => 'не дедупл.',
            ),
            'D6' => array(
                'value' => 'дедупл. *',
            ),
            'E6' => array(
                'value' => 'не дедупл.',
            ),
            'F6' => array(
                'value' => 'дедупл. *',
            ),
            'G6' => array(
                'value' => 'не дедупл.',
            ),
            'H6' => array(
                'value' => 'дедупл. *',
            ),
            'I6' => array(
                'value' => 'не дедупл.',
            ),
            'J6' => array(
                'value' => 'дедупл. *',
            ),
        );
        $chackChenged = array(
            'PUBLICATIONS_DIGITIZED'    => 'E',
            'PUBLICATIONS_DIGITIZED_DD' => 'F',
            'PUBLICATIONS'              => 'I',
            'PUBLICATIONS_DD'           => 'J',
        );

        $excel = new PHPExcel();
        $excel->getProperties()
            ->setTitle($title)
            ->setSubject($title);
        $sheet = $excel->setActiveSheetIndex(0);

        foreach ($staticCells as $cell => $data) {
            if (isset($data['merge'])) {
                $sheet->mergeCells($cell . ':' . $data['merge']);
            }
            if (isset($data['value'])) {
                $sheet->setCellValue($cell, $data['value']);
            }
        }

        /**
         * Записывает строку данных в отчет
         *
         * @param int         $index
         * @param int         $libraryId
         * @param null|string $sumName
         *
         * @return bool
         * @throws PHPExcel_Exception
         */
        $setRow = function ($index, $libraryId, $sumName = null) use (
            &$arStatData, $sheet, $changedColor, $chackChenged
        ) {
            $library = @$arStatData['libraries'][$libraryId];
            $from = @$arStatData['from'][$libraryId];
            $to = @$arStatData['to'][$libraryId];
            unset($arStatData['libraries'][$libraryId]);
            if (isset($library)) {
                if (null !== $sumName) {
                    $sheet->mergeCells("A$index:B$index");
                    $sheet->setCellValue(
                        'A' . $index,
                        $sumName
                    );
                } else {
                    $sheet->setCellValue(
                        'A' . $index,
                        $libraryId
                    );
                    $sheet->setCellValue(
                        'B' . $index,
                        $library['NAME']
                    );
                }

                $sheet
                    ->setCellValue(
                        'C' . $index, $from['PUBLICATIONS_DIGITIZED']
                    )->setCellValue(
                        'D' . $index, $from['PUBLICATIONS_DIGITIZED_DD']
                    )->setCellValue(
                        'E' . $index, $to['PUBLICATIONS_DIGITIZED']
                    )->setCellValue(
                        'F' . $index, $to['PUBLICATIONS_DIGITIZED_DD']
                    )->setCellValue(
                        'G' . $index, $from['PUBLICATIONS']
                    )->setCellValue(
                        'H' . $index, $from['PUBLICATIONS_DD']
                    )->setCellValue(
                        'I' . $index, $to['PUBLICATIONS']
                    )->setCellValue(
                        'J' . $index, $to['PUBLICATIONS_DD']
                    );

                foreach ($chackChenged as $fieldName => $column) {
                    if ($to[$fieldName] !== $from[$fieldName]) {
                        $sheet->getStyle($column . $index)
                            ->getFont()
                            ->getColor()
                            ->setRGB($changedColor);
                    }
                }

                return true;
            }

            return false;
        };

        $rowIndex = 7;
        foreach (nebLibrary::$federalLibraries as $libraryId) {
            if (true === $setRow($rowIndex, $libraryId)) {
                $rowIndex++;
            }
        }
        $setRow(
            $rowIndex,
            LibraryStatTable::FEDERAL_LIBRARIES_ID,
            'Итого по федеральным библиотекам'
        );
        $rowIndex++;
        $sheet->mergeCells("A$rowIndex:J$rowIndex");
        $rowIndex++;

        foreach (array_keys($arStatData['libraries']) as $libraryId) {
            if (0 === $libraryId) {
                continue;
            }
            if (true === $setRow($rowIndex, $libraryId)) {
                $rowIndex++;
            }
        }
        $sheet->mergeCells("A$rowIndex:J$rowIndex");
        $rowIndex++;
        $setRow(
            $rowIndex,
            0,
            'ИТОГО'
        );

        $fotnoteRow = $rowIndex + 2;
        $sheet
            ->mergeCells("A{$fotnoteRow}:J$fotnoteRow")
            ->setCellValue(
                'A' . $fotnoteRow,
                '* В таблице итоговые значения по дедуплицированным показателям не являются суммой дедуплицированных показателей'
            );


        /** styles */
        foreach (
            array('B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J')
            as $column
        ) {
            $sheet->getColumnDimension($column)
                ->setAutoSize(true);
        }
        $sheet->getColumnDimension('A')
            ->setWidth(30);

        $sheet
            ->getStyle("A4:J$rowIndex")
            ->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                )
            );
        $sheet
            ->getStyle("A4:J$rowIndex")
            ->applyFromArray(
                array(
                    'alignment' => array(
                        'indent'     => 1,
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                )
            );


        $excel->getActiveSheet()->setTitle('Statistics');
        $excel->setActiveSheetIndex(0);

        /** output */
        header('Content-Type: application/vnd.ms-excel');
        header(
            'Content-Disposition: attachment;filename="' . $title . '.xlsx"'
        );
        header(
            'Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'
        );
        $excelWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $excelWriter->save('php://output');
        die();
    }
}