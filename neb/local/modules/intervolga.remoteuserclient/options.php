<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;

global $APPLICATION;

Loc::loadMessages(__FILE__);
\Bitrix\Main\Loader::includeModule("intervolga.remoteuserclient");

$arTabs = array(
	array(
		"DIV" => "general_tab",
		"TAB" => Loc::getMessage("intervolga.remoteuserclient.OPTIONS_GENERAL_TAB"),
		"OPTIONS" => array(
			array("server", Loc::getMessage("intervolga.remoteuserclient.OPTION_SERVER"),
				"", array("text", 90)),
			array("page", Loc::getMessage("intervolga.remoteuserclient.OPTION_PAGE"),
				"", array("text", 90)),
		),
	),
	array(
		"DIV" => "access_tab",
		"TAB" => Loc::getMessage("intervolga.remoteuserclient.OPTIONS_ACCESS_TAB"),
		"OPTIONS" => array(
			array("server_login", Loc::getMessage("intervolga.remoteuserclient.OPTION_LOGIN"),
				"", array("text", 30)),
			array("server_password", Loc::getMessage("intervolga.remoteuserclient.OPTION_PASSWORD"),
				"", array("password", 30)),
		),
	),
);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && strlen($_REQUEST['save']) > 0 && check_bitrix_sessid())
{

	foreach ($arTabs as $arTab)
	{
		__AdmSettingsSaveOptions("intervolga.remoteuserclient", $arTab['OPTIONS']);
	}

	LocalRedirect($APPLICATION->GetCurPage() . '?lang=' . LANGUAGE_ID . '&mid=' . urlencode("intervolga.remoteuserclient"));
}


$obTabControl = new CAdminTabControl("tabControl", $arTabs);
?>
<form method='post' action='' name='remoteuserclient'>
	<?=bitrix_sessid_post()?>
	<?$obTabControl->Begin()?>
	<? foreach ($arTabs as $arTab): ?>
		<?$obTabControl->BeginNextTab()?>
		<?__AdmSettingsDrawList("intervolga.remoteuserclient", $arTab["OPTIONS"])?>
	<? endforeach; ?>

	<?$obTabControl->Buttons(array('btnApply' => false, 'btnCancel' => false, 'btnSaveAndAdd' => false)); ?>

	<?$obTabControl->End()?>
</form>