<?php
/**
 * User: agolodkov
 * Date: 23.10.2016
 * Time: 14:06
 */

namespace AppBundle\Traits;


trait ObjectHelperTrait
{
    /**
     * @param object       $object
     * @param string|array $path
     *
     * @return null
     */
    public function getObjectPathData($object, $path)
    {
        if (!is_array($path)) {
            $path = explode('->', $path);
        }
        foreach ($path as $property) {
            if (is_object($object) && property_exists($object, $property)) {
                $object = $object->$property;
            } else {
                $object = null;
                break;
            }
        }

        return $object;
    }
}