<?php
/** @var \Symfony\Bundle\FrameworkBundle\Templating\PhpEngine $view */
/** @var \Symfony\Component\Templating\Helper\SlotsHelper $slots */
/** @var \LbcBundle\DependencyInjection\MustacheEngine $mustache */
/** @var \Symfony\Bundle\FrameworkBundle\Templating\GlobalVariables $app */
/** @var array $data */