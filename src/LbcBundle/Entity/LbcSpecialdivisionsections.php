<?php

namespace LbcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use LbcBundle\Entity\Traits\CategoryCodeTrait;
use JMS\Serializer\Annotation as Serialization;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * LbcSpecialdivisionsections
 *
 * @Gedmo\Tree(type="closure")
 * @Gedmo\TreeClosure(class="LbcBundle\Entity\LbcLocationplansectionsClosure")
 * @ORM\Table(name="lbc_specialdivisionsections")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\ClosureTreeRepository")
 */
class LbcSpecialdivisionsections extends AbstractLbcCategory
{
    use CategoryCodeTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="SpecialDivisionId", type="integer", nullable=false)
     */
    private $specialdivisionid;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="LbcSpecialdivisionsections", inversedBy="children")
     * @ORM\JoinColumn(name="parentid", referencedColumnName="Id", onDelete="CASCADE")
     */
    public $parent;

    /**
     * @ORM\OneToMany(targetEntity="LbcSpecialdivisionsections", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    public $children;


    /**
     * Set specialdivisionid
     *
     * @param integer $specialdivisionid
     *
     * @return LbcSpecialdivisionsections
     */
    public function setSpecialdivisionid($specialdivisionid)
    {
        $this->specialdivisionid = $specialdivisionid;

        return $this;
    }

    /**
     * Get specialdivisionid
     *
     * @return integer
     */
    public function getSpecialdivisionid()
    {
        return $this->specialdivisionid;
    }
}
