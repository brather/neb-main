<?php
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
if($USER->IsAuthorized()) {
    LocalRedirect('/');
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");